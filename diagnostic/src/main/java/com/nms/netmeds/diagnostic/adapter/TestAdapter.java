package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterSearchItemBinding;
import com.nms.netmeds.diagnostic.databinding.AdpterFilterTestItemBinding;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.List;
import java.util.Locale;


public class TestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Test> testList;
    private TestAdapterListener testAdapterListener;
    public static final int POPULAR_TEST = 0;
    public static final int SEARCH = 1;
    private int layoutType;

    public TestAdapter(Context context, List<Test> testList, int layoutType, TestAdapterListener testAdapterListener) {
        this.mContext = context;
        this.testList = testList;
        this.layoutType = layoutType;
        this.testAdapterListener = testAdapterListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case POPULAR_TEST:
                AdpterFilterTestItemBinding adpterFilterTestItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adpter_filter_test_item, viewGroup, false);
                return new PopularTestHolder(adpterFilterTestItemBinding);
            case SEARCH:
                AdapterSearchItemBinding adapterSearchItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_search_item, viewGroup, false);
                return new SearchHolder(adapterSearchItemBinding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Test test = testList.get(position);
        switch (getItemViewType(position)) {
            case POPULAR_TEST:
                PopularTestHolder popularTestHolder = (PopularTestHolder) holder;
                popularTestHolder.adpterFilterTestItemBinding.textTestName.setText(test.getTestName());
                if (test.isChecked()) {
                    popularTestHolder.adpterFilterTestItemBinding.imgFilterCheck.setImageResource(R.drawable.ic_selected_primary);
                    popularTestHolder.adpterFilterTestItemBinding.textTestName.setTypeface(CommonUtils.getTypeface(mContext, "font/Lato-Bold.ttf"));
                } else {
                    popularTestHolder.adpterFilterTestItemBinding.imgFilterCheck.setImageResource(R.drawable.ic_add_circle);
                    popularTestHolder.adpterFilterTestItemBinding.textTestName.setTypeface(CommonUtils.getTypeface(mContext, "font/Lato-Regular.ttf"));
                }

                popularTestHolder.adpterFilterTestItemBinding.imgFilterCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (test.isChecked()) {
                            test.setChecked(false);
                            DiagnosticHelper.removeSelectedTest(test);
                            testAdapterListener.onTestUnChecked();
                        } else {
                            if (DiagnosticHelper.getSelectedList().size() == 0) {
                                test.setChecked(true);
                                DiagnosticHelper.setTest(test);
                                testAdapterListener.onTestChecked(test);
                            } else {
                                if (!TextUtils.isEmpty(DiagnosticHelper.getSelectedList().get(0).getCategory()) && DiagnosticHelper.getSelectedList().get(0).getCategory()
                                        .equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)) {
                                    showAlertDialog(mContext, test);
                                    return;
                                } else {
                                    test.setChecked(true);
                                    DiagnosticHelper.setTest(test);
                                    testAdapterListener.onTestChecked(test);
                                }
                            }
                        }
                        notifyDataSetChanged();
                    }
                });
                popularTestHolder.adpterFilterTestItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        testAdapterListener.onTestClicked(test);
                    }
                });
                break;
            case SEARCH:
                SearchHolder searchHolder = (SearchHolder) holder;
                searchHolder.adapterSearchItemBinding.textName.setText(mContext.getResources().getString(R.string.txt_lft));
                searchHolder.adapterSearchItemBinding.textAvailableLab.setText(mContext.getResources().getString(R.string.txt_available_three_labs));
                searchHolder.adapterSearchItemBinding.textPrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, mContext.getResources().getString(R.string.txt_price_200)));
                searchHolder.adapterSearchItemBinding.textType.setText(mContext.getResources().getString(R.string.txt_test_type_fasting));
                break;
            default:
                break;
        }
    }

    private void showAlertDialog(Context context, final Test test) {
        new AlertDialog.Builder(context)
                .setMessage(context.getResources().getString(R.string.alert_dialog_msg))
                .setPositiveButton(context.getResources().getString(R.string.alert_dialog_Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DiagnosticHelper.clearSelectedList();
                        test.setChecked(true);
                        DiagnosticHelper.setTest(test);
                        testAdapterListener.onTestChecked(test);
                        dialog.dismiss();
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(context.getResources().getString(R.string.alert_dialog_No), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    private boolean testExistOrNot(Test selectedTest) {
        for (Test test : DiagnosticHelper.getSelectedList()) {
            if (test.getTestId() == selectedTest.getTestId())
                return true;
        }
        return false;
    }

    public void updateTestAdapter(List<Test> updateList) {
        this.testList = updateList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (layoutType == POPULAR_TEST)
            return POPULAR_TEST;
        else return SEARCH;
    }

    class PopularTestHolder extends RecyclerView.ViewHolder {
        AdpterFilterTestItemBinding adpterFilterTestItemBinding;

        PopularTestHolder(@NonNull AdpterFilterTestItemBinding adpterFilterTestItemBinding) {
            super(adpterFilterTestItemBinding.getRoot());
            this.adpterFilterTestItemBinding = adpterFilterTestItemBinding;
        }
    }

    class SearchHolder extends RecyclerView.ViewHolder {
        AdapterSearchItemBinding adapterSearchItemBinding;

        SearchHolder(@NonNull AdapterSearchItemBinding adapterSearchItemBinding) {
            super(adapterSearchItemBinding.getRoot());
            this.adapterSearchItemBinding = adapterSearchItemBinding;
        }
    }

    public interface TestAdapterListener {
        void onTestChecked(Test selectedTest);

        void onTestUnChecked();

        void onTestClicked(Test test);
    }
}