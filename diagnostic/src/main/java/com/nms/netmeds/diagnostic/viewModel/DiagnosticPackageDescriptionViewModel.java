package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.LocationServiceCheckListener;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.AvailableLabsAdapter;
import com.nms.netmeds.diagnostic.adapter.DiagnosticPackageDescriptionFragmentAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticPackageBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.LabResult;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResult;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.model.TestDetailResponse;
import com.nms.netmeds.diagnostic.ui.DiagnosticPackageActivity;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.Locale;

import static com.nms.netmeds.diagnostic.DiagnosticServiceManager.DIAGNOSTIC_TEST_DETAILS;


public class DiagnosticPackageDescriptionViewModel extends AppViewModel implements TabLayout.OnTabSelectedListener, DiagnosticTestDescriptionViewModel.TestDetailListener {

    public DiagnosticPackage diagnosticPackage;
    private ActivityDiagnosticPackageBinding diagnosticPackageBinding;
    private DiagnosticPackageDescriptionFragmentAdapter adapter;
    private TabLayout tabLayout;
    public TestDetailResponse testDetailResponse;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private TestDetailListener testDetailListener;
    private int failedTransactionId;
    private Activity activity;
    private boolean fromDeepLink;
    private MutableLiveData<PinCodeResponse> pinCodeResponseMutableLiveData = new MutableLiveData<>();
    private static LocationServiceCheckListener locationServiceCheckListener;

    public DiagnosticPackageDescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticPackageBinding diagnosticPackageBinding, TestDetailListener testDetailListener, boolean fromDeepLink) {
        this.context = context;
        this.diagnosticPackageBinding = diagnosticPackageBinding;
        this.testDetailListener = testDetailListener;
        this.fromDeepLink = fromDeepLink;
        setPinCode();
    }

    @Override
    public void onSyncData(String data, int transactionId) {

        switch (transactionId){
            case DIAGNOSTIC_TEST_DETAILS:{
                if(!TextUtils.isEmpty(data)){
                    testDetailResponse = new Gson().fromJson(data, TestDetailResponse.class);
                    setViewPagerAndCustomTabs();
                }
                break;
            }
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK: {
                if (data != null) {
                    PinCodeResponse pinCodeResponse = new Gson().fromJson(CommonUtils.isJSONValid(data) ? data : "", PinCodeResponse.class);
                    DiagnosticHelper.setDefaultAddressToEmpty(testDetailListener.getContext());
                    pinCodeResponseMutableLiveData.setValue(pinCodeResponse);
                }
                break;
            }
            default:{
                testDetailListener.vmDismissProgress();
                testDetailListener.vmFinishActivity();
                break;
            }
        }
    }

    public static void setServiceCheckListener(LocationServiceCheckListener successListener) {
        locationServiceCheckListener = successListener;
    }


    public MutableLiveData<PinCodeResponse> getPinCodeResponseMutableLiveData() {
        return pinCodeResponseMutableLiveData;
    }

    private void setViewPagerAndCustomTabs() {
        setPackageName();
        displayMinPrice();
        setPackageBanner();
        if(adapter == null){
            ViewPager viewPager = diagnosticPackageBinding.pager;
            tabLayout = diagnosticPackageBinding.tabLayout;
            adapter = new DiagnosticPackageDescriptionFragmentAdapter(getActivity(), ((DiagnosticPackageActivity) getActivity()).getSupportFragmentManager());
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.addOnTabSelectedListener(this);
            setCustomTabs();
        }
        testDetailListener.vmDismissProgress();
        // Test/Package Viewed WebEngage Event
        WebEngageHelper.getInstance().testOrPackageViewed(BasePreference.getInstance(context), getTestNameForWebEngage(), getTestTypeForWebEngage(), labs.toString(), prices.toString(), context.getResources().getString(R.string.source)
                , testDetailListener.getContext());
    }

    public void changeAddress() {
        testDetailListener.vmChangeAddress();
    }

    private String getTestNameForWebEngage() {
        if (!isTestAvailable() || TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestName()))
            return "";
        return testDetailResponse.getResult().getTests().get(0).getTestName();
    }

    private String getTestTypeForWebEngage() {
        if (!isTestAvailable() || TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getType()))
            return "";
        getLabsAndPricesForWebEngage();
        return testDetailResponse.getResult().getTests().get(0).getType();
    }

    private StringBuilder labs = new StringBuilder();
    private StringBuilder prices = new StringBuilder();

    private void getLabsAndPricesForWebEngage() {
        if (!isLabAvailable()) return;
        ArrayList<LabDescription> labDescriptions = new ArrayList<>();
        ArrayList<PriceDescription> priceDescriptions = new ArrayList<>();
        for (LabResult labResult : testDetailResponse.getResult().getLabs()) {
            if (labResult != null) {
                if (labResult.getLabDescription() != null) {
                    LabDescription labDescription = new LabDescription();
                    if (!TextUtils.isEmpty(labResult.getLabDescription().getLabName())) {
                        labDescription.setLabName(labResult.getLabDescription().getLabName());
                    }
                    if (labResult.getLabDescription().getLabAddressList() != null && labResult.getLabDescription().getLabAddressList().size() > 0) {
                        labDescription.setLabAddressList(labResult.getLabDescription().getLabAddressList());
                    }
                    if (!TextUtils.isEmpty(labResult.getLabDescription().getShortDetails())) {
                        labDescription.setShortDetails(labResult.getLabDescription().getShortDetails());
                    }
                    labDescription.setId(labResult.getLabDescription().getId());
                    labDescription.setActive(labResult.getLabDescription().isActive());
                    labDescriptions.add(labDescription);
                }
                if (labResult.getPriceDescription() != null) {
                    priceDescriptions.add(labResult.getPriceDescription());
                }
            }
        }
        labs.append(new Gson().toJson(labDescriptions));
        prices.append(new Gson().toJson(priceDescriptions));
    }


    private void initLabAdapter() {
        if (isLabAvailable()) {
            AvailableLabsAdapter adapter = new AvailableLabsAdapter(testDetailResponse.getResult().getLabs(), context, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
            diagnosticPackageBinding.labList.setLayoutManager(layoutManager);
            diagnosticPackageBinding.labList.setAdapter(adapter);
        }
    }

    private void setPackageBanner() {
        if (getTestType().equalsIgnoreCase(context.getResources().getString(R.string.txt_profile_type)) && isLabAvailable()) {
            diagnosticPackageBinding.backdrop.setVisibility(View.GONE);
            diagnosticPackageBinding.llLabsAvailable.setVisibility(View.VISIBLE);
            initLabAdapter();
            return;
        }
        if (isTestAvailable() && !TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getImageUrl())) {
            diagnosticPackageBinding.backdrop.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions().placeholder(R.drawable.ic_default_lab)
                    .error(R.drawable.ic_default_lab)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(testDetailListener.getContext().getResources().getDimensionPixelSize(R.dimen.density_size_550), testDetailListener.getContext().getResources().getDimensionPixelSize(R.dimen.density_size_300))
                    .priority(Priority.HIGH);
            Glide.with(context).load(testDetailResponse.getResult().getTests().get(0).getImageUrl()).apply(options).into(diagnosticPackageBinding.backdrop);
        } else {
            diagnosticPackageBinding.backdrop.setVisibility(View.GONE);
        }
    }

    public String getTestType() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getTests() != null
                && testDetailResponse.getResult().getTests().size() > 0 && !TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getType()) ? testDetailResponse.getResult().getTests().get(0).getType() : "";
    }

    private void setPackageName() {
        if (getTestType().equalsIgnoreCase(context.getResources().getString(R.string.txt_profile_type))) {
            diagnosticPackageBinding.txtViewLabName.setVisibility(View.GONE);
            diagnosticPackageBinding.txtViewPackageName.setVisibility(View.VISIBLE);
        } else if (getTestType().equalsIgnoreCase(context.getResources().getString(R.string.txt_package_type)) ||
                getTestType().equalsIgnoreCase(context.getResources().getString(R.string.txt_packages_type))) {
            diagnosticPackageBinding.txtViewLabName.setVisibility(View.VISIBLE);
            diagnosticPackageBinding.txtViewPackageName.setVisibility(View.VISIBLE);
        }
        if (isLabAvailable() && !TextUtils.isEmpty(testDetailResponse.getResult().getLabs().get(0).getLabDescription().getLabName())) {
            diagnosticPackageBinding.txtViewLabName.setText(!TextUtils.isEmpty(testDetailResponse.getResult().getLabs().get(0).getLabDescription().getLabName()) ? String.format("%s %s", testDetailListener.getContext().getResources().getString(R.string.text_by), testDetailResponse.getResult().getLabs().get(0).getLabDescription().getLabName()) : "");
        }
        if (isTestAvailable() && !TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestName())) {
            diagnosticPackageBinding.txtViewPackageName.setText(testDetailResponse.getResult().getTests().get(0).getTestName());
        }
    }

    public boolean isLabAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getLabs() != null
                && testDetailResponse.getResult().getLabs().size() > 0;
    }

    public boolean isTestAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getTests() != null
                && testDetailResponse.getResult().getTests().size() > 0;
    }

    private void displayMinPrice() {
        if (isLabAvailable()) {
            diagnosticPackageBinding.llNoLabFound.setVisibility(View.GONE);
            diagnosticPackageBinding.llBottomPriceAndBook.setVisibility(View.VISIBLE);
        } else {
            diagnosticPackageBinding.llNoLabFound.setVisibility(View.VISIBLE);
            diagnosticPackageBinding.llBottomPriceAndBook.setVisibility(View.GONE);
        }
        double minAmount = Integer.MAX_VALUE;
        double strikeAmount = 0;
        if (testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getLabs() != null && testDetailResponse.getResult().getLabs().size() > 0) {
            for (int i = 0; i < testDetailResponse.getResult().getLabs().size(); i++) {
                if (testDetailResponse.getResult().getLabs().get(i).getPriceDescription() != null && !TextUtils.isEmpty(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getFinalPrice())
                        && Double.parseDouble(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getFinalPrice()) < minAmount) {
                    minAmount = Math.ceil(Double.parseDouble(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getFinalPrice()));
                    strikeAmount =  Math.ceil(Double.parseDouble(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getOfferedPrice()));
                }
            }
        }
        diagnosticPackageBinding.textPrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, String.valueOf((int) minAmount)));
        diagnosticPackageBinding.textStrikePrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, String.valueOf((int) strikeAmount)));
        diagnosticPackageBinding.textStrikePrice.setPaintFlags(diagnosticPackageBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        CommonUtils.setSelectedTabCustom(tab, context);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        CommonUtils.setUnselectedTabCustom(tab, context);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    private void setCustomTabs() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(adapter.getTabView(i));
            }
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        testDetailListener.vmDismissProgress();
        failedTransactionId = transactionId;
        switch (transactionId){
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:{
                locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
                break;
            }
            case DIAGNOSTIC_TEST_DETAILS:{
                if(fromDeepLink){
                    testDetailListener.vmFinishActivity();
                }else{
                    showWebserviceErrorView(true);
                }
                break;
            }
        }
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        testDetailListener.vmDismissProgress();
        diagnosticPackageBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == DIAGNOSTIC_TEST_DETAILS) {
            getTestDetails();
        }
    }

    public void getTestDetails() {
        if (diagnosticPackage == null || diagnosticPackage.getTestList() == null || diagnosticPackage.getTestList().size() == 0)
            return;
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        showNoNetworkView(isConnected);
        if (isConnected && context != null && diagnosticPackage != null && diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 && !TextUtils.isEmpty(BasePreference.getInstance(context).getDiagnosticPinCode())) {
            testDetailListener.vmShowProgress();
            DiagnosticServiceManager.getInstance().getTestDetails(this, diagnosticPackage.getTestList().get(0).getUrl(), BasePreference.getInstance(context).getDiagnosticPinCode(), BasePreference.getInstance(context));
        } else failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_TEST_DETAILS;
    }



    private void showNoNetworkView(boolean isConnected) {
        diagnosticPackageBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    public DiagnosticPackage getDiagnosticPackage() {
        return diagnosticPackage;
    }

    public void setDiagnosticPackage(DiagnosticPackage diagnosticPackage) {
        this.diagnosticPackage = diagnosticPackage;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void bookTest() {
        testDetailListener.navigateToBookTest();
    }

    public void searchPackage() {
        testDetailListener.vmSearchPackage();
    }

    public TestDetailResponse getTestDetailResponse() {
        return testDetailResponse;
    }

    @Override
    public void vmShowProgress() {

    }

    @Override
    public void vmDismissProgress() {

    }

    @Override
    public void vmFinishActivity() {

    }

    @Override
    public void vmNavigateToSearch() {

    }

    @Override
    public void vmNavigateToPatientDetails(int position) {
        testDetailListener.vmNavigateToPatientDetails(position);
    }

    @Override
    public Context getContext() {
        return testDetailListener.getContext();
    }

    @Override
    public void vmChangeAddress() {

    }

    public void setPinCode() {
        diagnosticPackageBinding.textPinCode.setText(DiagnosticHelper.getFormattedAddress(testDetailListener.getContext()));
    }

    public void pinCodeServiceCheck(String code) {
        boolean isConnected = NetworkUtils.isConnected(testDetailListener.getContext());
        if (isConnected) {
            PinCodeServiceCheckRequest serviceCheckRequest = new PinCodeServiceCheckRequest();
            serviceCheckRequest.setPinCode(code);
            DiagnosticServiceManager.getInstance().pinCodeServiceAvailabilityCheck(this, serviceCheckRequest, BasePreference.getInstance(testDetailListener.getContext()));
        } else
            locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_nonetwork_error_message));
    }

    public void pinCodeDataAvailable(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse != null) {
            checkPinCodeServiceStatus(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
    }

    private void checkPinCodeServiceStatus(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getServiceStatus() != null && pinCodeResponse.getServiceStatus().getStatusCode() != null && pinCodeResponse.getServiceStatus().getStatusCode() == DiagnosticConstant.STATUS_CODE_200) {
            getPinCodeResponseResult(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(pinCodeResponse.getResult() != null && !TextUtils.isEmpty(pinCodeResponse.getResult().getMessage()) ? pinCodeResponse.getResult().getMessage() : testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeResponseResult(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getResult() != null) {
            getPinCodeService(pinCodeResponse.getResult());
        } else
            locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
    }


    private void getPinCodeService(PinCodeResult pinCodeResult) {
        if (pinCodeResult.getService()) {
            String pinCode = !TextUtils.isEmpty(pinCodeResult.getPinCode()) ? pinCodeResult.getPinCode() : "";
            String city = !TextUtils.isEmpty(pinCodeResult.getCity()) ? pinCodeResult.getCity() : "";
            DiagnosticHelper.setCity(city);
            BasePreference.getInstance(testDetailListener.getContext()).setDiagnosticPinCode(pinCode);
            BasePreference.getInstance(testDetailListener.getContext()).setDiagnosticCity(city);
            BasePreference.getInstance(testDetailListener.getContext()).setLabPinCode(pinCode);
            getTestDetails();
            locationServiceCheckListener.onPinCodeSuccess(pinCode);
        } else
            locationServiceCheckListener.onPinCodeFailed();
    }

    public interface TestDetailListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmFinishActivity();

        void navigateToBookTest();

        void vmSearchPackage();

        void vmNavigateToPatientDetails(int position);

        Context getContext();

        void vmChangeAddress();
    }
}
