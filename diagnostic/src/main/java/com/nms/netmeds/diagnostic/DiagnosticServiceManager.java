package com.nms.netmeds.diagnostic;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.algolia.search.saas.AlgoliaException;
import com.algolia.search.saas.Client;
import com.algolia.search.saas.CompletionHandler;
import com.algolia.search.saas.Index;
import com.algolia.search.saas.Query;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseServiceManager;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.BaseApiClient;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderCreateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderHistoryResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderTrackResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPaymentCreateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticUpdateResponse;
import com.nms.netmeds.diagnostic.model.OfferBannerResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PopularTestResponse;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticCouponDetails;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderCreateRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderHistoryRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderTrackRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticPaymentCreateRequest;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.model.Request.SearchRequest;
import com.nms.netmeds.diagnostic.model.Request.SelectLabRequest;
import com.nms.netmeds.diagnostic.model.Request.TimeSlotRequest;
import com.nms.netmeds.diagnostic.model.SearchDataResponse;
import com.nms.netmeds.diagnostic.model.SelectLabResponse;
import com.nms.netmeds.diagnostic.model.ShowCouponResponse;
import com.nms.netmeds.diagnostic.model.TestDetailResponse;
import com.nms.netmeds.diagnostic.model.TimeSlotResponse;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticHomeViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticOrderHistoryViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticSearchTestViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticSelectLabViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTimeSlotViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiagnosticServiceManager extends BaseServiceManager {
    private static DiagnosticServiceManager diagnosticServiceManager;
    public final static int POPULAR_TEST = 501;
    public final static int PIN_CODE_SERVICE_CHECK = 502;
    public final static int OFFER_BANNER = 503;
    public final static int GET_LABS = 504;
    public final static int GET_SEARCH = 505;
    public final static int POPULAR_PACKAGE = 506;
    public final static int TIME_SLOT = 507;
    public final static int DIAGNOSTIC_ORDER_CREATION = 508;
    public final static int DIAGNOSTIC_ORDER_HISTORY = 509;
    public final static int DIAGNOSTIC_ORDER_TRACK = 510;
    public final static int DIAGNOSTIC_PAYMENT_CREATION = 511;
    public final static int DIAGNOSTIC_PAYMENT_UPDATE = 512;
    public final static int DIAGNOSTIC_ORDER_UPDATE = 513;
    public final static int SEARCH_PACKAGE = 514;
    public final static int DIAGNOSTIC_PAYMENT_FAILURE_UPDATE = 515;
    public final static int DIAGNOSTIC_TEST_DETAILS = 516;
    public static final int ALGOLIA_SEARCH = 516;
    public static final int NMS_WALLET_BALANCE = 202;
    public static final int DIAGNOSIS_APPLY_COUPON = 517;
    public static final int DIAGNOSIS_SHOW_COUPON = 518;
    public static final int CUSTOMER_ME = 519;
    public static final int C_MSTAR_PAYMENT_LIST = 50200;
    public final static int MOST_POPULAR_PACKAGE = 520;

    public static DiagnosticServiceManager getInstance() {
        if (diagnosticServiceManager == null)
            diagnosticServiceManager = new DiagnosticServiceManager();
        return diagnosticServiceManager;
    }

    public void getPopularTest(final DiagnosticHomeViewModel diagnosticHomeViewModel, String pinCode, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<PopularTestResponse> call = apiService.popularTest(getDiagnosticAuthorization(basePreference), pinCode);
        call.enqueue(new Callback<PopularTestResponse>() {
            @Override
            public void onResponse(@NonNull Call<PopularTestResponse> call, @NonNull Response<PopularTestResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    diagnosticHomeViewModel.onSyncData(new Gson().toJson(response.body()), POPULAR_TEST);
                } else if (checkErrorCode(response.code())) {
                    diagnosticHomeViewModel.onFailed(POPULAR_TEST, null);
                } else {
                    diagnosticHomeViewModel.onSyncData(errorHandling(response.errorBody()), POPULAR_TEST);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PopularTestResponse> call, @NonNull Throwable t) {
                diagnosticHomeViewModel.onFailed(POPULAR_TEST, null);
            }
        });
    }

    public <T extends AppViewModel> void pinCodeServiceAvailabilityCheck(final T viewModel, PinCodeServiceCheckRequest pinCodeServiceCheckRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<PinCodeResponse> call = apiService.pinCodeServiceCheck(getDiagnosticAuthorization(basePreference), pinCodeServiceCheckRequest);
        call.enqueue(new Callback<PinCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<PinCodeResponse> call, @NonNull Response<PinCodeResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PIN_CODE_SERVICE_CHECK);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(PIN_CODE_SERVICE_CHECK, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), PIN_CODE_SERVICE_CHECK);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PinCodeResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PIN_CODE_SERVICE_CHECK, null);
            }
        });
    }

    public <T extends AppViewModel> void getDiagnosticOfferBanner(final T viewModel, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL), ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<OfferBannerResponse> call = apiService.offerBanner(getDiagnosticAuthorization(basePreference));
        call.enqueue(new Callback<OfferBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<OfferBannerResponse> call, @NonNull Response<OfferBannerResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), OFFER_BANNER);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(OFFER_BANNER, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), OFFER_BANNER);
                }
            }

            @Override
            public void onFailure(@NonNull Call<OfferBannerResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(OFFER_BANNER, null);
            }
        });
    }

    public void getLabs(final DiagnosticSelectLabViewModel selectLabViewModel, SelectLabRequest selectLabRequest, BasePreference basePreference, String authToken) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL), TextUtils.isEmpty(authToken) ? ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL):authToken ).create(RetrofitDiagnosticInterface.class);
        Call<SelectLabResponse> call = apiService.getLabsByTestId(getDiagnosticAuthorization(basePreference), selectLabRequest);
        call.enqueue(new Callback<SelectLabResponse>() {
            @Override
            public void onResponse(@NonNull Call<SelectLabResponse> call, @NonNull Response<SelectLabResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    selectLabViewModel.onSyncData(new Gson().toJson(response.body()), GET_LABS);
                } else if (checkErrorCode(response.code())) {
                    selectLabViewModel.onFailed(GET_LABS, null);
                } else {
                    selectLabViewModel.onFailed(GET_LABS, null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SelectLabResponse> call, @NonNull Throwable t) {
                selectLabViewModel.onFailed(GET_LABS, null);
            }
        });
    }

    public <T extends AppViewModel> void getSearchData(final T viewModel, SearchRequest searchRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<SearchDataResponse> call = apiService.getSearch(getDiagnosticAuthorization(basePreference), searchRequest);
        call.enqueue(new Callback<SearchDataResponse>() {
            @Override
            public void onResponse(@NonNull Call<SearchDataResponse> call, @NonNull Response<SearchDataResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), GET_SEARCH);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(GET_SEARCH, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), GET_SEARCH);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchDataResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(GET_SEARCH, null);
            }
        });
    }

    public <T extends AppViewModel> void getPopularPackage(final T viewModel, String pinCode, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticPackageResponse> call = apiService.popularPackage(getDiagnosticAuthorization(basePreference), pinCode);
        call.enqueue(new Callback<DiagnosticPackageResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticPackageResponse> call, @NonNull Response<DiagnosticPackageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), POPULAR_PACKAGE);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(POPULAR_PACKAGE, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), POPULAR_PACKAGE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticPackageResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(POPULAR_PACKAGE, null);
            }
        });
    }


    public <T extends AppViewModel> void getMostPopularPackage(final T viewModel, String pinCode, boolean toppack, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticPackageResponse> call = apiService.mostPopularPackage(getDiagnosticAuthorization(basePreference), pinCode, toppack);
        call.enqueue(new Callback<DiagnosticPackageResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticPackageResponse> call, @NonNull Response<DiagnosticPackageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MOST_POPULAR_PACKAGE);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(MOST_POPULAR_PACKAGE, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), MOST_POPULAR_PACKAGE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticPackageResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(MOST_POPULAR_PACKAGE, null);
            }
        });
    }

    public <T extends AppViewModel> void getTimeSlot(final T viewModel, TimeSlotRequest timeSlotRequest, BasePreference basePreference, String authToken) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL),
                authToken).create(RetrofitDiagnosticInterface.class);
        Call<TimeSlotResponse> call = apiService.timeSlot(getDiagnosticAuthorization(basePreference), timeSlotRequest);
        call.enqueue(new Callback<TimeSlotResponse>() {
            @Override
            public void onResponse(@NonNull Call<TimeSlotResponse> call, @NonNull Response<TimeSlotResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), TIME_SLOT);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(TIME_SLOT, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), TIME_SLOT);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TimeSlotResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(TIME_SLOT, null);
            }
        });
    }

    public <T extends AppViewModel> void diagnosticOrderCreation(final T viewModel, DiagnosticOrderCreateRequest orderCreateRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticOrderCreateResponse> call = apiService.diagnosticOrderCreation(getDiagnosticAuthorization(basePreference), orderCreateRequest);
        call.enqueue(new Callback<DiagnosticOrderCreateResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticOrderCreateResponse> call, @NonNull Response<DiagnosticOrderCreateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_ORDER_CREATION);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(DIAGNOSTIC_ORDER_CREATION, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_ORDER_CREATION);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticOrderCreateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DIAGNOSTIC_ORDER_CREATION, null);
            }
        });
    }

    public void diagnosticOrderHistory(final DiagnosticOrderHistoryViewModel historyViewModel, DiagnosticOrderHistoryRequest orderHistoryRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticOrderHistoryResponse> call = apiService.diagnosticOrderHistory(getDiagnosticAuthorization(basePreference), orderHistoryRequest);
        call.enqueue(new Callback<DiagnosticOrderHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticOrderHistoryResponse> call, @NonNull Response<DiagnosticOrderHistoryResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    historyViewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_ORDER_HISTORY);
                } else if (checkErrorCode(response.code())) {
                    historyViewModel.onFailed(DIAGNOSTIC_ORDER_HISTORY, null);
                } else {
                    historyViewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_ORDER_HISTORY);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticOrderHistoryResponse> call, @NonNull Throwable t) {
                historyViewModel.onFailed(DIAGNOSTIC_ORDER_HISTORY, null);
            }
        });
    }

    public <T extends AppViewModel> void diagnosticOrderTrack(final T viewModel, DiagnosticOrderTrackRequest orderTrackRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticOrderTrackResponse> call = apiService.diagnosticOrderTrack(getDiagnosticAuthorization(basePreference), orderTrackRequest);
        call.enqueue(new Callback<DiagnosticOrderTrackResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticOrderTrackResponse> call, @NonNull Response<DiagnosticOrderTrackResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_ORDER_TRACK);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(DIAGNOSTIC_ORDER_TRACK, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_ORDER_TRACK);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticOrderTrackResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DIAGNOSTIC_ORDER_TRACK, null);
            }
        });
    }

    public <T extends AppViewModel> void diagnosticPaymentCreation(final T viewModel, DiagnosticPaymentCreateRequest paymentCreateRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticPaymentCreateResponse> call = apiService.diagnosticPaymentCreation(getDiagnosticAuthorization(basePreference), paymentCreateRequest);
        call.enqueue(new Callback<DiagnosticPaymentCreateResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticPaymentCreateResponse> call, @NonNull Response<DiagnosticPaymentCreateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_PAYMENT_CREATION);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(DIAGNOSTIC_PAYMENT_CREATION, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_PAYMENT_CREATION);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticPaymentCreateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DIAGNOSTIC_PAYMENT_CREATION, null);
            }
        });
    }

    public <T extends AppViewModel> void diagnosticPaymentUpdate(final T viewModel, DiagnosticPaymentCreateRequest paymentCreateRequest, final String updateType, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticUpdateResponse> call = apiService.diagnosticPaymentUpdate(getDiagnosticAuthorization(basePreference), paymentCreateRequest);
        call.enqueue(new Callback<DiagnosticUpdateResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticUpdateResponse> call, @NonNull Response<DiagnosticUpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    response.body().setPaymentUpdateType(updateType);
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_PAYMENT_UPDATE);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(updateType.equals(DiagnosticConstant.SUCCESS) ? DIAGNOSTIC_PAYMENT_UPDATE : DIAGNOSTIC_PAYMENT_FAILURE_UPDATE, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_PAYMENT_UPDATE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticUpdateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(updateType.equals(DiagnosticConstant.SUCCESS) ? DIAGNOSTIC_PAYMENT_UPDATE : DIAGNOSTIC_PAYMENT_FAILURE_UPDATE, null);
            }
        });
    }

    public <T extends AppViewModel> void diagnosticOrderUpdate(final T viewModel, DiagnosticOrderCreateRequest orderCreateRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticUpdateResponse> call = apiService.diagnosticOrderUpdate(getDiagnosticAuthorization(basePreference), orderCreateRequest);
        call.enqueue(new Callback<DiagnosticUpdateResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticUpdateResponse> call, @NonNull Response<DiagnosticUpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_ORDER_UPDATE);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(DIAGNOSTIC_ORDER_UPDATE, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_ORDER_UPDATE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticUpdateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DIAGNOSTIC_ORDER_UPDATE, null);
            }
        });
    }

    public <T extends AppViewModel> void getDiagnosticPackage(final T viewModel, SearchRequest searchRequest, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<DiagnosticPackageResponse> call = apiService.getSearchPackage(getDiagnosticAuthorization(basePreference), searchRequest);
        call.enqueue(new Callback<DiagnosticPackageResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiagnosticPackageResponse> call, @NonNull Response<DiagnosticPackageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), SEARCH_PACKAGE);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(SEARCH_PACKAGE, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), SEARCH_PACKAGE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiagnosticPackageResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(SEARCH_PACKAGE, null);
            }
        });
    }

    public <T extends AppViewModel> void getTestDetails(final T viewModel, final String testUrl, final String pinCode, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<TestDetailResponse> call = apiService.getTestDetail(testUrl, pinCode, getDiagnosticAuthorization(basePreference));
        call.enqueue(new Callback<TestDetailResponse>() {
            @Override
            public void onResponse(Call<TestDetailResponse> call, Response<TestDetailResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSTIC_TEST_DETAILS);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(DIAGNOSTIC_TEST_DETAILS, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DIAGNOSTIC_TEST_DETAILS);
                }
            }

            @Override
            public void onFailure(Call<TestDetailResponse> call, Throwable t) {
                viewModel.onFailed(DIAGNOSTIC_TEST_DETAILS, null);
            }
        });

    }

    private Map<String, String> getDiagnosticAuthorization(BasePreference basePreference) {
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(basePreference.getDiagnosticLoginResponse(), JustDocUserResponse.class);
        String diagnosticToken = diagnosticLoginResult != null && diagnosticLoginResult.getToken() != null ? diagnosticLoginResult.getToken() : "";
        String sessionId = diagnosticLoginResult != null && diagnosticLoginResult.getSession() != null && diagnosticLoginResult.getSession().getId() != null ? diagnosticLoginResult.getSession().getId() : "";
        Map<String, String> header = new HashMap<>();
        header.put(AppConstant.KEY_AUTHORIZATION, AppConstant.KEY_BEARER + diagnosticToken);
        header.put(AppConstant.KEY_SESSION, sessionId);
        return header;
    }

    private boolean checkErrorCode(int errorCode) {
        return errorCode == 502 || errorCode == 500 || errorCode == 404;
    }

    public void algoliaSearch(String filter, final DiagnosticSearchTestViewModel viewModel, String searchQuery, int page, int algoliaHitCount, String algoliaApiKey, String algoliaIndex, String algoliaAppId) {
        Client client = new Client(algoliaAppId, algoliaApiKey);
        Index index = client.getIndex(algoliaIndex);
        Query query;
        query = new Query(searchQuery);
        query.setPage(page);
        query.setFacets("*");
        if (!TextUtils.isEmpty(filter)) {
            query.setFilters(filter);
        }
        query.setHitsPerPage(algoliaHitCount);

        index.searchAsync(query, new CompletionHandler() {
            @Override
            public void requestCompleted(JSONObject content, AlgoliaException error) {
                if (content == null) {
                    viewModel.onFailed(ALGOLIA_SEARCH, null);
                } else {
                    viewModel.onSyncData(content.toString(), ALGOLIA_SEARCH);
                }
            }
        });
    }


    public <T extends AppViewModel> void getDiagnosisPaymentList(final T paymentViewModel, String authToken, boolean from_radio, String payMode, int cId, int labId, BasePreference basePreference, String grandTotal, String userId, String appVersion
            , List<String> testIds) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty(AppConstant.FROM_RADIO, from_radio);
            jsonObject.addProperty(DiagnosticConstant.KEY_LAB_ID, labId);
            jsonObject.addProperty(AppConstant.ORDER_VALUE, grandTotal);
            jsonObject.addProperty(AppConstant.BOOMRANG_AUTH_TOKEN, authToken);
            jsonObject.addProperty(AppConstant.APP_SOURCE, AppConstant.ANDROID);
            jsonObject.addProperty(AppConstant.APP_VERSION, appVersion);
            jsonObject.addProperty(AppConstant.AMAZON_FLAG, AppConstant.FALSE);
            jsonObject.addProperty(AppConstant.MSTAR_API_HEADER_USER_ID, userId);
            jsonObject.addProperty(AppConstant.CID, cId);
            jsonObject.addProperty(AppConstant.PAY_MODE, payMode);
            jsonObject.addProperty(AppConstant.TEST_IDS, testIds.toString());

        } catch (JsonIOException e) {
            e.printStackTrace();
        }
        Call<MStarBasicResponseTemplateModel> call = apiService.getDiagnosisPaymentList(jsonObject, getDiagnosticAuthorization(basePreference));
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null)
                    paymentViewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAYMENT_LIST);
                else
                    paymentViewModel.onSyncData(errorHandling(response.errorBody()), C_MSTAR_PAYMENT_LIST);
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                paymentViewModel.onFailed(C_MSTAR_PAYMENT_LIST, null);
            }
        });
    }

    public <T extends AppViewModel> void getNMSBalanceForDiagnosis(final T viewModel, String authToken, boolean from_radio, String payMode, int cId, int labId, BasePreference basePreference, String grandTotal, String userId, String appVersion
            , List<String> testIds) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty(AppConstant.MID, basePreference.getMstarCustomerId());
            jsonObject.addProperty(AppConstant.SESSION_ID, basePreference.getMStarSessionId());
            jsonObject.addProperty(AppConstant.FROM_RADIO, from_radio);
            jsonObject.addProperty(DiagnosticConstant.KEY_LAB_ID, labId);
            jsonObject.addProperty(AppConstant.ORDER_VALUE, grandTotal);
            jsonObject.addProperty(AppConstant.BOOMRANG_AUTH_TOKEN, authToken);
            jsonObject.addProperty(AppConstant.APP_SOURCE, AppConstant.ANDROID);
            jsonObject.addProperty(AppConstant.APP_VERSION, appVersion);
            jsonObject.addProperty(AppConstant.AMAZON_FLAG, AppConstant.FALSE);
            jsonObject.addProperty(AppConstant.MSTAR_API_HEADER_USER_ID, userId);
            jsonObject.addProperty(AppConstant.CID, cId);
            jsonObject.addProperty(AppConstant.PAY_MODE, payMode);
            jsonObject.addProperty(AppConstant.TEST_IDS, testIds.toString());
        } catch (JsonIOException e) {
            e.printStackTrace();
        }
            Call<MStarBasicResponseTemplateModel> call = apiService.nmsDiagnosisWalletBalance(jsonObject);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), NMS_WALLET_BALANCE);
                } else {
                    viewModel.onFailed(NMS_WALLET_BALANCE, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(NMS_WALLET_BALANCE, null);
            }
        });
    }

    public <T extends AppViewModel> void applyCoupon(final T viewModel, BasePreference basePreference, DiagnosticCouponDetails couponDetails) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<TestDetailResponse> call = apiService.applyDiagnosisCouponCode(getDiagnosticAuthorization(basePreference), couponDetails);
        call.enqueue(new Callback<TestDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<TestDetailResponse> call, @NonNull Response<TestDetailResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSIS_APPLY_COUPON);
                } else {
                    viewModel.onFailed(DIAGNOSIS_APPLY_COUPON, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<TestDetailResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DIAGNOSIS_APPLY_COUPON, null);
            }
        });
    }

    public <T extends AppViewModel> void showCoupon(final T viewModel, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<ShowCouponResponse> call = apiService.couponBox(getDiagnosticAuthorization(basePreference));
        call.enqueue(new Callback<ShowCouponResponse>() {
            @Override
            public void onResponse(@NonNull Call<ShowCouponResponse> call, @NonNull Response<ShowCouponResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DIAGNOSIS_SHOW_COUPON);
                } else {
                    viewModel.onFailed(DIAGNOSIS_SHOW_COUPON, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ShowCouponResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DIAGNOSIS_SHOW_COUPON, null);
            }
        });
    }

    public <T extends AppViewModel> void getAllAddress(final T viewModel, BasePreference basePreference) {
        RetrofitDiagnosticInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitDiagnosticInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.customerAddress(basePreference.getMstarBasicHeaderMap());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), CUSTOMER_ME);
                } else {
                    viewModel.onFailed(CUSTOMER_ME, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(CUSTOMER_ME, null);
            }
        });
    }
}
