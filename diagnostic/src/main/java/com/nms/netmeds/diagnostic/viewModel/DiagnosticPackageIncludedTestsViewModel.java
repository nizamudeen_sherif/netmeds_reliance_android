package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.font.LatoTextView;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.FragmentDiagnosticIncludedTestsBinding;

public class DiagnosticPackageIncludedTestsViewModel extends AppViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private FragmentDiagnosticIncludedTestsBinding includedTestsBinding;
    private String includeTestString;


    public DiagnosticPackageIncludedTestsViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, FragmentDiagnosticIncludedTestsBinding includedTestsBinding, String includeTestString) {
        this.context = context;
        this.includedTestsBinding = includedTestsBinding;
        this.includeTestString = includeTestString;
        setIncludeTest();
    }


    private void setIncludeTest() {
        if (TextUtils.isEmpty(includeTestString)) {
            includedTestsBinding.lLayoutIncludedTest.setVisibility(View.GONE);
            includedTestsBinding.llIncludedTests.setVisibility(View.GONE);
            return;
        }
        includedTestsBinding.llIncludedTests.setVisibility(View.VISIBLE);
        includedTestsBinding.lLayoutIncludedTest.setVisibility(View.VISIBLE);
        includedTestsBinding.lLayoutIncludedTest.removeAllViews();
        String[] tests = includeTestString.split(",");
        if (tests.length > 0) {
            for (int i = 0; i < tests.length; i++) {
                LayoutInflater li = LayoutInflater.from(context);
                View childView = li.inflate(R.layout.included_test, null, false);
                LatoTextView textView = childView.findViewById(R.id.txtViewTestName);
                textView.setText(CommonUtils.fromHtml(tests[i]));
                View lineView = childView.findViewById(R.id.viewLine);
                if (i == tests.length - 1) {
                    lineView.setVisibility(View.INVISIBLE);
                } else {
                    lineView.setVisibility(View.VISIBLE);
                }
                includedTestsBinding.lLayoutIncludedTest.addView(childView, i);
            }
        }
    }

    public String getIncludeTestString() {
        return includeTestString;
    }

    public void setIncludeTestString(String includeTestString) {
        this.includeTestString = includeTestString;
    }
}
