package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterSelectLabUnavailableTestBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.TestStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SelectLabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Context mContext;
    private static final int TEST_UNAVAILABLE = 1;
    private List<AvailableLab> labList, filterList;
    private LabAdapterListener labAdapterListener;
    private LabFilter mLabFilter;
    private List<Test> selectedList;
    private int lastPosition = -1;
    private static final int POSITION = 4, OFFSET = 200;

    public SelectLabAdapter(Context context, List<AvailableLab> labList, List<Test> selectedList, int type, LabAdapterListener labAdapterListener) {
        this.mContext = context;
        this.labList = labList;
        this.filterList = labList;
        this.labAdapterListener = labAdapterListener;
        this.selectedList = selectedList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        AdapterSelectLabUnavailableTestBinding unavailableTestBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_select_lab_unavailable_test, viewGroup, false);
        return new DiagnosticTestUnAvailableViewHolder(unavailableTestBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final AvailableLab availableLab = labList.get(position);
        LabDescription labDescription = availableLab.getLabDescription();
        PriceDescription priceDescription = availableLab.getPriceDescription();
        DiagnosticTestUnAvailableViewHolder unAvailableViewHolder = (DiagnosticTestUnAvailableViewHolder) holder;
        if (availableLab.isChecked())
            unAvailableViewHolder.unavailableTestBinding.imgFilterCheck.setImageResource(R.drawable.ic_selected_primary);
        else
            unAvailableViewHolder.unavailableTestBinding.imgFilterCheck.setImageResource(R.drawable.ic_add_circle);
        unAvailableViewHolder.unavailableTestBinding.textLabName.setText(!TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName().trim() : "");
        unAvailableViewHolder.unavailableTestBinding.textOffer.setText(!TextUtils.isEmpty(priceDescription.getDiscountPercentInfo()) ? priceDescription.getDiscountPercentInfo() : "");
        unAvailableViewHolder.unavailableTestBinding.textPrice.setText(!TextUtils.isEmpty(priceDescription.getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getFinalPrice()) : "");
        unAvailableViewHolder.unavailableTestBinding.textStrikePrice.setText(!TextUtils.isEmpty(priceDescription.getOfferedPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getOfferedPrice()) : "");
        unAvailableViewHolder.unavailableTestBinding.textStrikePrice.setPaintFlags(unAvailableViewHolder.unavailableTestBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        unAvailableViewHolder.unavailableTestBinding.availableTest.setText(availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? String.format(Locale.getDefault(), "%s%s%d%s", "Tests Available", "(", availableLab.getTestList().size(), ")") : "");
        if (TextUtils.isEmpty(labDescription.getRating()))
            unAvailableViewHolder.unavailableTestBinding.rating.setVisibility(View.INVISIBLE);
        else
            unAvailableViewHolder.unavailableTestBinding.rating.setText(labDescription.getRating());

        if (!TextUtils.isEmpty(getAccreditation(labDescription))) {
            unAvailableViewHolder.unavailableTestBinding.txtViewAccreditation.setText(getAccreditation(labDescription));
        } else {
            unAvailableViewHolder.unavailableTestBinding.txtViewAccreditation.setText("");
        }

        if (availableLab.getTestList() != null && availableLab.getTestList().size() > 0)
            setTestList(unAvailableViewHolder, availableLab);
        unAvailableViewHolder.unavailableTestBinding.imgFilterCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                labCheck(availableLab);
            }
        });
        if (TextUtils.isEmpty(labDescription.getLogo()))
            unAvailableViewHolder.unavailableTestBinding.labLogo.setImageResource(R.drawable.ic_lab_report);
        else {
            labLogoOrTags(labDescription.getLogo(), unAvailableViewHolder.unavailableTestBinding.labLogo, true);
        }
        unAvailableViewHolder.unavailableTestBinding.priceBreakup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                labAdapterListener.OnPriceBreakupClicked(availableLab.getTestList());
            }
        });

        if (availableLab.getTestList() != null && availableLab.getTestList().size() > 0) {
            if (!TextUtils.isEmpty(availableLab.getTestList().get(0).getCategory()) && availableLab.getTestList().get(0).getCategory().equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)
                    && availableLab.getLabDescription() != null && availableLab.getLabDescription().getLabAddressList() != null && availableLab.getLabDescription().getLabAddressList().size() > 0
                    && !TextUtils.isEmpty(availableLab.getLabDescription().getLabAddressList().get(0).getFullAddress())) {
                unAvailableViewHolder.unavailableTestBinding.rLayoutAddress.setVisibility(View.VISIBLE);
                unAvailableViewHolder.unavailableTestBinding.txtViewAddress.setText(availableLab.getLabDescription().getLabAddressList().get(0).getFullAddress());
            } else {
                unAvailableViewHolder.unavailableTestBinding.rLayoutAddress.setVisibility(View.GONE);
                unAvailableViewHolder.unavailableTestBinding.txtViewAddress.setText("");
            }
        }
        if (availableLab.getLabDescription() != null && availableLab.getLabDescription().getExtras() != null
                && availableLab.getLabDescription().getExtras().getTags() != null && availableLab.getLabDescription().getExtras().getTags().size() > 0
                && availableLab.getLabDescription().getExtras().getTags().get(0).isStatus()) {
            unAvailableViewHolder.unavailableTestBinding.imgLabTag.setVisibility(View.VISIBLE);
            labLogoOrTags(!TextUtils.isEmpty(availableLab.getLabDescription().getExtras().getTags().get(0).getImgUrl()
            ) ? availableLab.getLabDescription().getExtras().getTags().get(0).getImgUrl() : "", unAvailableViewHolder.unavailableTestBinding.imgLabTag, false);
        } else {
            unAvailableViewHolder.unavailableTestBinding.imgLabTag.setVisibility(View.GONE);
        }
        // apply fall down animation
        setAnimation(unAvailableViewHolder.itemView, position, mContext);
    }

    private void setAnimation(View viewToAnimate, int position, Context context) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.fall_down);
            animation.setInterpolator(new DecelerateInterpolator());
            if(position < POSITION)
                animation.setStartOffset(position * OFFSET);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    private void labLogoOrTags(String logo, ImageView imageView, boolean defaultLogoFlag) {
        RequestOptions options = new RequestOptions().placeholder(defaultLogoFlag ? R.drawable.ic_default_lab : R.drawable.ic_lab_report)
                .error(defaultLogoFlag ? R.drawable.ic_default_lab : R.drawable.ic_lab_report)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(mContext).load(TextUtils.isEmpty(logo.trim())
                ? defaultLogoFlag ? R.drawable.ic_default_lab : R.drawable.ic_lab_report : logo.trim()).apply(options).into(imageView);
    }


    // get Accreditation for Labs
    private String getAccreditation(LabDescription labDescription) {
        StringBuilder stringBuilder = new StringBuilder();
        if (labDescription != null && labDescription.getAccreditation() != null && labDescription.getAccreditation().size() > 0) {
            if (labDescription.getAccreditation().size() == 1)
                stringBuilder.append(labDescription.getAccreditation().get(0).getName());
            for (int i = 0; i < labDescription.getAccreditation().size(); i++) {
                stringBuilder.append(labDescription.getAccreditation().get(i).getName());
                if (i < labDescription.getAccreditation().size() - 1)
                    stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }


    private void labCheck(AvailableLab availableLab) {
        if (availableLab.isChecked()) {
            availableLab.setChecked(false);
            labAdapterListener.onLabUnChecked();
        } else {
            labUnCheck();
            availableLab.setChecked(true);
            labAdapterListener.onLabChecked(availableLab);
        }
        notifyDataSetChanged();
    }

    private void labUnCheck() {
        for (AvailableLab lab : labList)
            lab.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return labList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TEST_UNAVAILABLE;
    }

    public void updateLabAdapter(List<AvailableLab> updateList, int layoutType) {
        this.labList = updateList;
        this.filterList = updateList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if (mLabFilter == null)
            mLabFilter = new LabFilter(SelectLabAdapter.this);
        return mLabFilter;
    }

    private class LabFilter extends Filter {
        private SelectLabAdapter selectLabAdapter;

        LabFilter(SelectLabAdapter selectLabAdapter) {
            super();
            this.selectLabAdapter = selectLabAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResult = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                filterResult.values = filterList;
                filterResult.count = filterList.size();
            } else {
                List<AvailableLab> filteredList = new ArrayList<>();
                final String filterPattern = constraint.toString().toLowerCase();
                for (AvailableLab availableLab : filterList) {
                    LabDescription labDescription = availableLab.getLabDescription();
                    if (labDescription.getLabName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(availableLab);
                    }
                }
                filterResult.values = filteredList;
                filterResult.count = filteredList.size();
            }
            return filterResult;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            selectLabAdapter.labList = (ArrayList<AvailableLab>) results.values;
            selectLabAdapter.notifyDataSetChanged();
        }
    }

    public class DiagnosticTestUnAvailableViewHolder extends RecyclerView.ViewHolder {
        private AdapterSelectLabUnavailableTestBinding unavailableTestBinding;

        DiagnosticTestUnAvailableViewHolder(@NonNull AdapterSelectLabUnavailableTestBinding unavailableTestBinding) {
            super(unavailableTestBinding.getRoot());
            this.unavailableTestBinding = unavailableTestBinding;

        }
    }

    private void setTestList(DiagnosticTestUnAvailableViewHolder unAvailableViewHolder, AvailableLab availableLab) {
        List<TestStatus> testList = new ArrayList<>();
        List<TestStatus> availableTestList = new ArrayList<>();
        List<TestStatus> unavailableTestList = new ArrayList<>();
        List<TestStatus> sortedTestList = new ArrayList<>();

        boolean isTestAvailable;
        for (Test test : selectedList) {
            isTestAvailable = false;
            for (Test availableTest : availableLab.getTestList())
                if (test.getTestId().equals(availableTest.getTestId()))
                    isTestAvailable = true;
            if (isTestAvailable)
                testList.add(new TestStatus(test.getTestName(), true));
            else testList.add(new TestStatus(test.getTestName(), false));
        }
        for (TestStatus test : testList) {
            if (test.isAvailable())
                availableTestList.add(test);
            else unavailableTestList.add(test);
        }
        sortedTestList.addAll(availableTestList);
        sortedTestList.addAll(unavailableTestList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 1);
        unAvailableViewHolder.unavailableTestBinding.availableTestListView.setLayoutManager(gridLayoutManager);
        unAvailableViewHolder.unavailableTestBinding.availableTestListView.setNestedScrollingEnabled(false);
        AvailableTestsAdapter availableTestsAdapter = new AvailableTestsAdapter(sortedTestList, mContext);
        unAvailableViewHolder.unavailableTestBinding.availableTestListView.setAdapter(availableTestsAdapter);
    }

    public interface LabAdapterListener {
        void onLabChecked(AvailableLab availableLab);

        void onLabUnChecked();

        void OnPriceBreakupClicked(List<Test> tests);
    }
}