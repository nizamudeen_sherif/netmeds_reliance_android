package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AvailableTestBinding;
import com.nms.netmeds.diagnostic.model.TestStatus;

import java.util.List;

/**
 * @author Adesh
 * Adapter class for inflating Available/Unavailable test within the selected Lab
 */
public class AvailableTestsAdapter extends RecyclerView.Adapter<AvailableTestsAdapter.AvailableTestsHolder> {
    private List<TestStatus> testStatuses;
    private Context mContext;

    AvailableTestsAdapter(List<TestStatus> testStatuses, Context mContext) {
        this.testStatuses = testStatuses;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AvailableTestsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AvailableTestBinding availableTestBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.available_test, viewGroup, false);
        return new AvailableTestsHolder(availableTestBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableTestsHolder holder, @SuppressLint("RecyclerView") final int position) {
        final TestStatus testStatus = testStatuses.get(position);
        if (testStatus.isAvailable()) {
            holder.availableTestBinding.textDetail.setText(CommonUtils.validateString(testStatus.getTestName()));
            holder.availableTestBinding.testIcon.setText(R.string.test_available_check);
            return;
        }
        holder.availableTestBinding.textDetail.setTextColor(mContext.getResources().getColor(R.color.colorLightBlueGrey));
        holder.availableTestBinding.textDetail.setText(CommonUtils.validateString(testStatus.getTestName()));
        holder.availableTestBinding.testIcon.setText(R.string.test_not_available_uncheck);

    }

    @Override
    public int getItemCount() {
        return testStatuses.size();
    }


    static class AvailableTestsHolder extends RecyclerView.ViewHolder {
        AvailableTestBinding availableTestBinding;

        private AvailableTestsHolder(@NonNull AvailableTestBinding availableTestBinding) {
            super(availableTestBinding.getRoot());
            this.availableTestBinding = availableTestBinding;
        }
    }
}
