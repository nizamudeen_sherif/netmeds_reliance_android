package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class SelectLabResponse extends BaseResponse {
    @SerializedName("result")
    private SelectLabResult selectLabResult;

    @SerializedName("updatedOn")
    private String updatedOn;

    public SelectLabResult getSelectLabResult() {
        return selectLabResult;
    }

    public void setSelectLabResult(SelectLabResult selectLabResult) {
        this.selectLabResult = selectLabResult;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }



}
