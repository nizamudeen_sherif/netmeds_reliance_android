package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DiagnosticPackageResult implements Serializable {
    @SerializedName("products")
    private List<DiagnosticPackage> packageList;
    @SerializedName("message")
    private String message;

    public List<DiagnosticPackage> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<DiagnosticPackage> packageList) {
        this.packageList = packageList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
