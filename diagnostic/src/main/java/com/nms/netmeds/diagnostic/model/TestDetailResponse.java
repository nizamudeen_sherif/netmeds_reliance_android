package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

import java.io.Serializable;

public class TestDetailResponse implements Serializable {

    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private TestResult result;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public TestResult getResult() {
        return result;
    }

    public void setResult(TestResult result) {
        this.result = result;
    }


}
