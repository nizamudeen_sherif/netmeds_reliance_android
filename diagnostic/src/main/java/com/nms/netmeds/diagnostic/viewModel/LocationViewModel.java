package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DefaultAddressAdapter;
import com.nms.netmeds.diagnostic.databinding.DialogxLocationBinding;

import java.util.ArrayList;
import java.util.List;

public class LocationViewModel extends AppViewModel implements DefaultAddressAdapter.DefaultAddressListener {
    private DialogxLocationBinding locationBinding;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private LocationModelListener locationModelListener;
    private MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel;
    private MStarAddressModel customerSelectedAddress;
    private static final int PIN_CODE_LENGTH = 6;

    public LocationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DialogxLocationBinding locationBinding, LocationModelListener locationModelListener) {
        this.mContext = context;
        this.locationBinding = locationBinding;
        this.locationModelListener = locationModelListener;
        if (!BasePreference.getInstance(mContext).isGuestCart()) {
            getCustomerDeliveryAddress();
        }
    }

    public void onPinCodeEntered() {
        if (!TextUtils.isEmpty(locationBinding.edtPinCode.getText()) && locationBinding.edtPinCode.getText().toString().trim().length() == PIN_CODE_LENGTH)
            locationModelListener.initPinCode(locationBinding.edtPinCode.getText().toString());
        else
            SnackBarHelper.snackBarCallBack(locationBinding.lytMain, mContext, mContext.getResources().getString(R.string.enter_pin_code_msg));
    }

    public void onCurrentLocation() {
        locationModelListener.initLocationPermission();
    }

    public void onLocationClose() {
        locationModelListener.close();
    }

    @Override
    public void diagnosticPinCodeCheck(MStarAddressModel mStarAddressModel) {
        updateAddressAndContinue(mStarAddressModel);
    }

    @Override
    public void addNewAddress() {
        locationModelListener.addAddress();
    }

    public interface LocationModelListener {
        void initLocationPermission();

        void initPinCode(String code);

        void close();

        void showLoader();

        void dismissLoader();

        void defaultAddressChanged(MStarAddressModel customerSelectedAddress);

        void addAddress();

    }

    public String setSamplePickupText() {
        return mContext.getResources().getString(R.string.choose_your_location);
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case DiagnosticServiceManager.CUSTOMER_ME: {
                if (!TextUtils.isEmpty(data)) {
                    getCustomerDeliverAddressResponse(data);
                }
                break;
            }
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                locationModelListener.dismissLoader();
                locationModelListener.defaultAddressChanged(customerSelectedAddress);
                break;
        }
    }

    private void getCustomerDeliverAddressResponse(String addressList) {
        locationBinding.lLayoutSavedAddress.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(addressList)) {
            mstarBasicResponseTemplateModel = new Gson().fromJson(addressList, MStarBasicResponseTemplateModel.class);
            MStarAddressModel mStarAddressModel = new MStarAddressModel();
            mStarAddressModel.setItemType(1);
            if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getResult() != null && mstarBasicResponseTemplateModel.getResult().getAddressList() != null && mstarBasicResponseTemplateModel.getResult().getAddressList().size() > 0) {
                mstarBasicResponseTemplateModel.getResult().getAddressList().add(mStarAddressModel);
                DefaultAddressAdapter defaultAddressAdapter = new DefaultAddressAdapter(reArrangeAddressList(mstarBasicResponseTemplateModel.getResult().getAddressList()), mContext, this);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true);
                mLayoutManager.setReverseLayout(false);
                mLayoutManager.setStackFromEnd(false);
                locationBinding.listAddress.setLayoutManager(mLayoutManager);
                locationBinding.listAddress.setAdapter(defaultAddressAdapter);
                locationModelListener.dismissLoader();
            } else {
                ArrayList<MStarAddressModel> customerAddresses = new ArrayList<>();
                customerAddresses.add(mStarAddressModel);
                DefaultAddressAdapter defaultAddressAdapter = new DefaultAddressAdapter(customerAddresses, mContext, this);
                locationBinding.listAddress.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
                locationBinding.listAddress.setAdapter(defaultAddressAdapter);
                locationModelListener.dismissLoader();
            }
        }
    }

    private List<MStarAddressModel> reArrangeAddressList(List<MStarAddressModel> addresses) {
        ArrayList<MStarAddressModel> tempArrayList = new ArrayList<>();
        for (int i = 0; i < addresses.size(); i++) {
            MStarAddressModel mStarAddressModel = addresses.get(i);
            if (mStarAddressModel.getId() == BasePreference.getInstance(mContext).getDiagDefaultAddressId()) {
                mStarAddressModel.setChecked(true);
                tempArrayList.add(mStarAddressModel);
                addresses.remove(mStarAddressModel);
            } else {
                mStarAddressModel.setChecked(false);
            }
        }
        tempArrayList.addAll(addresses);
        return tempArrayList;
    }

    private void getCustomerDeliveryAddress() {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        if (isConnected) {
            locationModelListener.showLoader();
            DiagnosticServiceManager.getInstance().getAllAddress(this, BasePreference.getInstance(mContext));
        }
    }


    private void updateAddressAndContinue(MStarAddressModel customerAddress) {
        this.customerSelectedAddress = customerAddress;
        locationModelListener.showLoader();
        initiateAPICall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS, customerAddress);
        initiateAPICall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS, customerAddress);
        initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS, customerAddress);
        initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS, customerAddress);
    }


    private void initiateAPICall(int transactionId, MStarAddressModel customerAddress) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, BasePreference.getInstance(mContext).getMstarBasicHeaderMap(), customerAddress.getId(), APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                    break;
                case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, BasePreference.getInstance(mContext).getMstarBasicHeaderMap(), customerAddress.getId(), APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId,
                            0);
                    break;
            }
        }
    }
}