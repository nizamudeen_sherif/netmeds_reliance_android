package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelectLabRequest {
    @SerializedName("testIds")
    private List<String> testIdList;
    @SerializedName("pincode")
    private String pinCode;
    @SerializedName("userId")
    private String userId;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTestIdList(List<String> testIdList) {
        this.testIdList = testIdList;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}
