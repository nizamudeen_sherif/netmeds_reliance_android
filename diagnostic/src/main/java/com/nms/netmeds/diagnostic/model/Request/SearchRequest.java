package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

public class SearchRequest {
    @SerializedName("pincode")
    private String pinCode;
    @SerializedName("query")
    private String query;
    @SerializedName("type")
    private String type;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
