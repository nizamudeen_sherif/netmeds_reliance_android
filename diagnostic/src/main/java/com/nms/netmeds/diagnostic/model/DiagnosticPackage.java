package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.Test;

import java.io.Serializable;
import java.util.List;

public class DiagnosticPackage implements Serializable {
    @SerializedName("tests")
    private List<Test> testList;
    @SerializedName("labDescription")
    private LabDescription labDescription;
    @SerializedName("priceDescription")
    private PriceDescription priceDescription;
    private boolean checked;

    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    public LabDescription getLabDescription() {
        return labDescription;
    }

    public void setLabDescription(LabDescription labDescription) {
        this.labDescription = labDescription;
    }

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
