package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.DiagnosticPromoCodeItemBinding;

public class DiagnosticPromoCodeListAdapter extends RecyclerView.Adapter<DiagnosticPromoCodeListAdapter.PromoCodeHolder> {

    private Context context;

    public DiagnosticPromoCodeListAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public PromoCodeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        DiagnosticPromoCodeItemBinding promoCodeItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.diagnostic_promo_code_item, viewGroup, false);
        return new PromoCodeHolder(promoCodeItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PromoCodeHolder promoCodeHolder, int i) {
        promoCodeHolder.promoCodeItemBinding.textPromoCode.setText(context.getResources().getString(R.string.txt_ea_twenty));
        promoCodeHolder.promoCodeItemBinding.textCouponMessage.setText(context.getResources().getString(R.string.get_flat_off));
        promoCodeHolder.promoCodeItemBinding.promoCodeChecked.setImageResource(R.drawable.ic_radio_unchecked);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class PromoCodeHolder extends RecyclerView.ViewHolder {
        private DiagnosticPromoCodeItemBinding promoCodeItemBinding;

        PromoCodeHolder(@NonNull DiagnosticPromoCodeItemBinding promoCodeItemBinding) {
            super(promoCodeItemBinding.getRoot());
            this.promoCodeItemBinding = promoCodeItemBinding;
        }
    }
}
