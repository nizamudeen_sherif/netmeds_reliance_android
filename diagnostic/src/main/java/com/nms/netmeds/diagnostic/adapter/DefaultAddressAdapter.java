package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.diagnostic.R;

import java.util.List;

public class DefaultAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<MStarAddressModel> mAddressList;
    private final Context mContext;
    private final DefaultAddressListener mAddressListListener;
    private static final int DEFAULT_ADDRESS = 0;
    private static final int ADD_ADDRESS_ITEM = 1;


    public DefaultAddressAdapter(List<MStarAddressModel> addressList, Context context, DefaultAddressListener addressListListener) {
        this.mAddressList = addressList;
        this.mContext = context;
        this.mAddressListListener = addressListListener;
    }


    // specify the row layout file and click for each row
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == DEFAULT_ADDRESS) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.default_address, parent, false);
            return new DefaultAddressViewHolder(view);
        } else if (viewType == ADD_ADDRESS_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_address, parent, false);
            return new AddAddressViewHolder(view);
        } else {
            throw new RuntimeException("The type has to be ONE or TWO");
        }
    }


    // load data in each row element
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int listPosition) {
        switch (holder.getItemViewType()) {
            case DEFAULT_ADDRESS:
                initLayoutOne((DefaultAddressViewHolder) holder, listPosition);
                break;
            case ADD_ADDRESS_ITEM:
                ((AddAddressViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAddressListListener.addNewAddress();
                    }
                });
                break;
            default:
                break;
        }
    }

    private void initLayoutOne(final DefaultAddressViewHolder holder, final int pos) {
        final MStarAddressModel customerAddress = mAddressList.get(pos);
        holder.txtAddressLocation.setText(mContext.getResources().getString(R.string.home_text));
        holder.txtAddressLocation.setVisibility(View.GONE);
        holder.txtViewActualAddress.setText(getCustomerAddress(customerAddress));

        if (customerAddress.isChecked()) {
            customerAddress.setChecked(true);
            holder.addressCheckbox.setImageResource(R.drawable.ic_radio_checked);
        } else {
            customerAddress.setChecked(false);
            holder.addressCheckbox.setImageResource(R.drawable.ic_radio_unchecked);
        }

        holder.lMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAdapter(pos);
                mAddressListListener.diagnosticPinCodeCheck(customerAddress);
            }
        });
    }

    private void resetAdapter(int adapterPosition) {
        for (int i = 0; i < mAddressList.size(); i++) {
            MStarAddressModel customerAddress = mAddressList.get(i);
            if (i == adapterPosition) {
                customerAddress.setChecked(true);
            } else customerAddress.setChecked(false);
            notifyDataSetChanged();
        }
    }

    private String getCustomerAddress(MStarAddressModel addressModel) {
        StringBuilder formattedAddress = new StringBuilder();
        formattedAddress.append(!TextUtils.isEmpty(addressModel.getFirstname()) ? addressModel.getFirstname() : "").append(" ").append(!TextUtils.isEmpty(addressModel.getFirstname()) ? addressModel.getLastname() : "");
        formattedAddress.append("\n");
        formattedAddress.append(CommonUtils.getAddressFromObject(addressModel));
        return formattedAddress.toString();
    }

    @Override
    public int getItemCount() {
        return mAddressList == null ? 0 : mAddressList.size();
    }


    // determine which layout to use for the row
    @Override
    public int getItemViewType(int position) {
        MStarAddressModel customerAddress = mAddressList.get(position);
        if (customerAddress.getItemType() == 0) return DEFAULT_ADDRESS;
        else if (customerAddress.getItemType() == 1) return ADD_ADDRESS_ITEM;
        else return -1;
    }


    public interface DefaultAddressListener {
        void diagnosticPinCodeCheck(MStarAddressModel customerAddress);

        void addNewAddress();
    }


    // Static inner class to initialize the views of rows
    static class DefaultAddressViewHolder extends RecyclerView.ViewHolder {
        public TextView txtAddressLocation, txtViewActualAddress;
        public ImageView addressCheckbox;
        public LinearLayout lMainLayout;

        public DefaultAddressViewHolder(View itemView) {
            super(itemView);
            txtAddressLocation = itemView.findViewById(R.id.txtAddressLocation);
            txtViewActualAddress = itemView.findViewById(R.id.txtViewActualAddress);
            addressCheckbox = itemView.findViewById(R.id.addressCheckbox);
            lMainLayout = itemView.findViewById(R.id.lMainLayout);
        }
    }

    static class AddAddressViewHolder extends RecyclerView.ViewHolder {

        public AddAddressViewHolder(View itemView) {
            super(itemView);
        }
    }

}
