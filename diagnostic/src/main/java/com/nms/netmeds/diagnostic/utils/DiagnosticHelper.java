package com.nms.netmeds.diagnostic.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabResult;
import com.nms.netmeds.diagnostic.model.PriceDescription;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DiagnosticHelper {
    private static List<Test> selectedList = new ArrayList<>();

    public static void setTest(Test test) {
        if (selectedList.size() > 0) {
            if (selectedList.contains(test)) return;
            selectedList.add(test);
        } else {
            selectedList.add(test);
        }
    }

    public static List<Test> getSelectedList() {
        return selectedList;
    }

    public static void setSelectedList(List<Test> list) {
        selectedList = list;
    }

    private static String selectedSlotDay = "";
    private static String city = "";
    private static boolean isNMSCashApplied = false;
    private static double appliedNMSCash = 0;
    private static double nmsWalletAmount = 0;
    private static double remainingPaidAmount = 0;
    private static boolean isRemovedElementPackage = false;


    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        DiagnosticHelper.city = city;
    }

    public static String getSelectedSlotDay() {
        return selectedSlotDay;
    }

    public static void setSelectedSlotDay(String selectedSlotDay) {
        DiagnosticHelper.selectedSlotDay = selectedSlotDay;
    }

    public static void removeSelectedTest(Test test) {
        isRemovedElementPackage = selectedList != null && selectedList.size() == 1 && test.getType().equalsIgnoreCase(DiagnosticConstant.PACKAGES) || test.getType().equalsIgnoreCase(DiagnosticConstant.PACKAGE);
        for (Iterator<Test> iterator = selectedList.iterator(); iterator.hasNext(); ) {
            Test removeTest = iterator.next();
            if (removeTest.getTestId().equals(test.getTestId()))
                iterator.remove();
        }
    }

    public static boolean IsRemovedElementPackage() {
        return DiagnosticHelper.isRemovedElementPackage;
    }


    public static boolean setIsRemovedElementPackage(Boolean flag) {
        return DiagnosticHelper.isRemovedElementPackage = flag;
    }

    public static void clearSelectedList() {
        selectedList.clear();
        setSelectedSlotDay("");
    }

    public static String getTestType() {
        String type = DiagnosticConstant.TEST;
        if (DiagnosticHelper.getSelectedList() != null) {
            if (DiagnosticHelper.getSelectedList().size() == 1 && !TextUtils.isEmpty(DiagnosticHelper.getSelectedList().get(0).getType())
                    && (DiagnosticHelper.getSelectedList().get(0).getType().equalsIgnoreCase(DiagnosticConstant.PACKAGE)
                    || DiagnosticHelper.getSelectedList().get(0).getType().equalsIgnoreCase(DiagnosticConstant.PACKAGES))) {
                type = DiagnosticHelper.getSelectedList().get(0).getType();
            }
        }
        return type;
    }

    public static PriceInfo getPriceInfo(PriceDescription priceDescription) {
        PriceInfo priceInfo = new PriceInfo();
        priceInfo.setDiscountAmount(priceDescription.getHc());
        priceInfo.setOfferedPrice(priceDescription.getOfferedPrice());
        priceInfo.setFinalPrice(priceDescription.getFinalPrice());
        priceInfo.setDiscountAmount(priceDescription.getDiscountAmount());
        priceInfo.setDiscountAmountInfo(priceDescription.getDiscountAmountInfo());
        priceInfo.setDiscountPercent(priceDescription.getDiscountPercent());
        return priceInfo;
    }


    public static void setIsNMSCashApplied(boolean isNMSCashApplied) {
        DiagnosticHelper.isNMSCashApplied = isNMSCashApplied;
    }

    public static boolean isNMSCashApplied() {
        return isNMSCashApplied;
    }

    public static double getAppliedNMSCash() {
        return appliedNMSCash;
    }

    public static void setAppliedNMSCash(double appliedNMSCash) {
        DiagnosticHelper.appliedNMSCash = appliedNMSCash;
    }

    public static double getNmsWalletAmount() {
        return nmsWalletAmount;
    }

    public static void setNmsWalletAmount(double nmsWalletAmount) {
        DiagnosticHelper.nmsWalletAmount = nmsWalletAmount;
    }

    public static double getremainingPaidAmount() {
        return remainingPaidAmount;
    }

    public static void setremainingPaidAmount(double remainingPaidAmount) {
        DiagnosticHelper.remainingPaidAmount = remainingPaidAmount;
    }

    public static DefaultAddress setDefaultAddress(MStarAddressModel customerAddress, Context context) {
        DefaultAddress defaultAddress = new Gson().fromJson(BasePreference.getInstance(context).getDefaultAddress(), DefaultAddress.class);

        defaultAddress.getResult().getCustomer_details().getAddress().setPostcode(
                !TextUtils.isEmpty(customerAddress.getPin()) ? customerAddress.getPin() : ""
        );
        defaultAddress.getResult().getCustomer_details().getAddress().setCity(
                !TextUtils.isEmpty(customerAddress.getCity()) ? customerAddress.getCity() : ""
        );
        defaultAddress.getResult().getCustomer_details().getAddress().setStreet(
                !TextUtils.isEmpty(customerAddress.getStreet()) ? customerAddress.getStreet() : "");

        defaultAddress.getResult().getCustomer_details().getAddress().setState(
                !TextUtils.isEmpty(customerAddress.getState()) ? customerAddress.getState() : "");

        defaultAddress.getResult().getCustomer_details().getAddress().setMobileNo(
                !TextUtils.isEmpty(customerAddress.getMobileNo()) ? customerAddress.getMobileNo() : "");

        defaultAddress.getResult().getCustomer_details().getAddress().setLandmark(
                !TextUtils.isEmpty(customerAddress.getLandmark()) ? customerAddress.getLandmark() : "");

        BasePreference.getInstance(context).saveDefaultAddress(new Gson().toJson(defaultAddress));
        return defaultAddress;
    }

    public static boolean isRadioTestExist() {
        return getSelectedList() != null && getSelectedList().size() > 0 && !TextUtils.isEmpty(getSelectedList().get(0).getCategory())
                && getSelectedList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_RADIO);
    }

    public static Test addATest(DiagnosticPackage diagnosticPackage) {
        if (DiagnosticHelper.isRadioTestExist()) DiagnosticHelper.clearSelectedList();
        Test test = new Test();
        if (diagnosticPackage != null && diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0) {
            test.setTestId(diagnosticPackage.getTestList().get(0).getTestId());
            test.setTestName(diagnosticPackage.getTestList().get(0).getTestName());
            test.setType(diagnosticPackage.getTestList().get(0).getType());
            test.setCategory(diagnosticPackage.getTestList().get(0).getCategory());
            if (!TextUtils.isEmpty(diagnosticPackage.getTestList().get(0).getFastingRequired())) {
                test.setFastingRequired(diagnosticPackage.getTestList().get(0).getFastingRequired());
            } else {
                test.setFastingRequired("");
            }
            PriceInfo priceInfo = new PriceInfo();
            if (diagnosticPackage.getTestList().get(0).getPriceInfo() != null && !TextUtils.isEmpty(diagnosticPackage.getTestList().get(0).getPriceInfo().getFinalPrice())) {
                priceInfo.setFinalPrice(diagnosticPackage.getTestList().get(0).getPriceInfo().getFinalPrice());
            } else {
                priceInfo.setFinalPrice("");
            }
            test.setPriceInfo(priceInfo);
        }

        return test;
    }

    public static boolean isPathoTestExist() {
        return getSelectedList() != null && getSelectedList().size() > 0 && !TextUtils.isEmpty(getSelectedList().get(0).getCategory())
                && getSelectedList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_PATH);
    }

    // get Formatted address (address with pin code)
    public static String getFormattedAddress(Context context){
        if (context == null || TextUtils.isEmpty(BasePreference.getInstance(context).getDefaultAddress())) return "";
        StringBuilder addressBuilder = new StringBuilder();
        DefaultAddress defaultAddress = new Gson().fromJson(BasePreference.getInstance(context).getDefaultAddress(), DefaultAddress.class);
        addressBuilder.append(!TextUtils.isEmpty(defaultAddress.getResult().getCustomer_details().getAddress().getStreet())
                ? defaultAddress.getResult().getCustomer_details().getAddress().getStreet() : "");
        addressBuilder.append(!TextUtils.isEmpty(defaultAddress.getResult().getCustomer_details().getAddress().getCity())
                ? " " + defaultAddress.getResult().getCustomer_details().getAddress().getCity() : "");
        addressBuilder.append(!TextUtils.isEmpty(defaultAddress.getResult().getCustomer_details().getAddress().getState())
                ? " " + defaultAddress.getResult().getCustomer_details().getAddress().getState() : "");
        return  addressBuilder.toString().length() >= 5 ? addressBuilder.toString().substring(0, 4) + "..." + BasePreference.getInstance(context).getDiagnosticPinCode()
                : addressBuilder.toString().length() == 0 ? BasePreference.getInstance(context).getDiagnosticPinCode() : addressBuilder.append("...").append(BasePreference.getInstance(context).getDiagnosticPinCode()).toString();
    }

    // set default address
    public static void setDefaultAddressToEmpty(Context context){
        if(context ==  null) return;
        DefaultAddress defaultAddress = new Gson().fromJson(BasePreference.getInstance(context).getDefaultAddress(), DefaultAddress.class);
        defaultAddress.getResult().getCustomer_details().getAddress().setStreet("");
        defaultAddress.getResult().getCustomer_details().getAddress().setLandmark("");
        defaultAddress.getResult().getCustomer_details().getAddress().setCity("");
        defaultAddress.getResult().getCustomer_details().getAddress().setState("");
        defaultAddress.getResult().getCustomer_details().getAddress().setPostcode("");
        defaultAddress.getResult().getCustomer_details().getAddress().setMobileNo("");
        BasePreference.getInstance(context).saveDefaultAddress(new Gson().toJson(defaultAddress));
    }

    // check for lab description for particular lab exist or not
    public static boolean isLabDescriptionExist(final LabResult labResult){
        return labResult!=null && labResult.getLabDescription()!=null;
    }

    // check for lab's price description for particular lab exist or not
    public static boolean isPriceDescriptionExist(final LabResult labResult){
        return labResult!=null && labResult.getPriceDescription()!=null;
    }

    // Common Method to populate Image throughout
    public static void populateImage(final Context context, final String imageUrl, final ImageView containerImageView, final int defaultIcon){
        RequestOptions options = new RequestOptions().placeholder(defaultIcon)
                .error(defaultIcon)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(!TextUtils.isEmpty(imageUrl) ? imageUrl : defaultIcon).apply(options).into(containerImageView);
    }

    public static String validatePrice(final String value){
        return TextUtils.isEmpty(value) ? "0" : value;
    }

}
