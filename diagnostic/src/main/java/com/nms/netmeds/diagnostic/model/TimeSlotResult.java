package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TimeSlotResult implements Serializable {
    @SerializedName("pincode")
    private String pinCode;
    @SerializedName("slotsAvailable")
    private List<AvailableSlot> availableSlotList;
    @SerializedName("labId")
    private String labId;
    @SerializedName("message")
    private String message;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public List<AvailableSlot> getAvailableSlotList() {
        return availableSlotList;
    }

    public void setAvailableSlotList(List<AvailableSlot> availableSlotList) {
        this.availableSlotList = availableSlotList;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
