package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AvailableLabItemBinding;
import com.nms.netmeds.diagnostic.model.LabResult;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTestDescriptionViewModel;

import java.util.List;
import java.util.Locale;

public class AvailableLabsAdapter extends RecyclerView.Adapter<AvailableLabsAdapter.AvailableLabsHolder> {

    private List<LabResult> labList;
    private Context context;
    private DiagnosticTestDescriptionViewModel.TestDetailListener testDetailListener;


    public AvailableLabsAdapter(List<LabResult> labList, Context context, DiagnosticTestDescriptionViewModel.TestDetailListener testDetailListener) {
        this.labList = labList;
        this.context = context;
        this.testDetailListener = testDetailListener;
    }

    @NonNull
    @Override
    public AvailableLabsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AvailableLabItemBinding labItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.available_lab_item, viewGroup, false);
        return new AvailableLabsHolder(labItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableLabsHolder holder, final int position) {
        LabResult labItem = labList.get(position);
        if (DiagnosticHelper.isLabDescriptionExist(labItem)) {
            holder.labItemBinding.textLabName.setText(CommonUtils.validateString(labItem.getLabDescription().getLabName()));
            DiagnosticHelper.populateImage(context, labItem.getLabDescription().getLogo(), holder.labItemBinding.labIcon, R.drawable.ic_default_lab);
        }
        if (DiagnosticHelper.isPriceDescriptionExist(labItem)) {
            holder.labItemBinding.textDiscountPercentage.setText(CommonUtils.validateString(labItem.getPriceDescription().getDiscountPercentInfo()));
            holder.labItemBinding.textPrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, (int) Math.ceil(Double.parseDouble(DiagnosticHelper.validatePrice(labItem.getPriceDescription().getFinalPrice())))));
            holder.labItemBinding.textStrikePrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, DiagnosticHelper.validatePrice(labItem.getPriceDescription().getOfferedPrice())));
            holder.labItemBinding.textStrikePrice.setPaintFlags(holder.labItemBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        holder.labItemBinding.txtViewBookTest.setOnClickListener(new BookTestClickListener(position));
    }

    @Override
    public int getItemCount() {
        return labList.size();
    }

    // Book Test Click Listener
    private class BookTestClickListener implements View.OnClickListener{

        private int mPosition;

        private BookTestClickListener(final  int mPosition){
            this.mPosition = mPosition;
        }

        @Override
        public void onClick(View view) {
            testDetailListener.vmNavigateToPatientDetails(mPosition);
        }
    }

    // Available Lab View Holder
    static class AvailableLabsHolder extends RecyclerView.ViewHolder {
        AvailableLabItemBinding labItemBinding;

        private AvailableLabsHolder(@NonNull AvailableLabItemBinding labItemBinding) {
            super(labItemBinding.getRoot());
            this.labItemBinding = labItemBinding;
        }
    }
}
