package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class PopularTestResponse extends BaseResponse {
    @SerializedName("result")
    private PopularTestResult popularTestResult;
    @SerializedName("updatedOn")
    private String updatedOn;

    public PopularTestResult getPopularTestResult() {
        return popularTestResult;
    }

    public void setPopularTestResult(PopularTestResult popularTestResult) {
        this.popularTestResult = popularTestResult;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
