package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.PriceBreakupBottomViewBinding;

import java.util.List;
import java.util.Locale;

public class PriceBreakupViewModel extends AppViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private PriceBreakupBottomViewBinding priceBreakupBottomViewBinding;
    private PriceBreakupViewModelListener priceBreakupViewModelListener;
    private List<Test> tests;
    private int strikePriceValue = 0;
    private int finalPriceValue = 0;

    public PriceBreakupViewModel(@NonNull Application application) {
        super(application);
    }

    public void onBottomViewClose() {
        priceBreakupViewModelListener.onCloseDialog();
    }

    public void init(Context context, PriceBreakupBottomViewBinding priceBreakupBottomViewBinding, List<Test> tests, PriceBreakupViewModelListener priceBreakupViewModelListener) {
        this.context = context;
        this.priceBreakupBottomViewBinding = priceBreakupBottomViewBinding;
        this.priceBreakupViewModelListener = priceBreakupViewModelListener;
        this.tests = tests;
        if (tests == null)
            onBottomViewClose();
        initList();
    }

    private void initList() {
        for (Test test : tests) {
            if (test == null || TextUtils.isEmpty(test.getTestName()) || test.getPriceInfo() == null)
                break;
            PriceInfo priceInfo = test.getPriceInfo();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View testPriceRoot = inflater.inflate(R.layout.price_breakup_test_item_layout, null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            TextView testName = testPriceRoot.findViewById(R.id.test_name);
            testName.setText(test.getTestName());

            TextView strikePrice = testPriceRoot.findViewById(R.id.textStrikePrice);
            strikePrice.setText(!TextUtils.isEmpty(priceInfo.getOfferedPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, test.getPriceInfo().getOfferedPrice()) : "");
            strikePrice.setPaintFlags(strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            TextView priceView = testPriceRoot.findViewById(R.id.textPrice);
            priceView.setText(!TextUtils.isEmpty(priceInfo.getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, test.getPriceInfo().getFinalPrice()) : "");
            priceBreakupBottomViewBinding.labTestPriceList.addView(testPriceRoot, params);
        }
        displaySavings();
    }

    private void displaySavings() {
        for (Test test : tests) {
            if (test == null || test.getPriceInfo() == null || TextUtils.isEmpty(test.getPriceInfo().getFinalPrice()) || TextUtils.isEmpty(test.getPriceInfo().getOfferedPrice()))
                break;
            PriceInfo priceInfo = test.getPriceInfo();

            strikePriceValue = strikePriceValue + Integer.parseInt(priceInfo.getOfferedPrice());
            finalPriceValue = finalPriceValue + Integer.parseInt(priceInfo.getFinalPrice());
        }
        int savings = strikePriceValue - finalPriceValue;
        priceBreakupBottomViewBinding.savings.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, String.valueOf(savings)));

    }

    public interface PriceBreakupViewModelListener {
        void onCloseDialog();
    }
}
