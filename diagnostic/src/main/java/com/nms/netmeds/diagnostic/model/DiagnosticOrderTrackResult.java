package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DiagnosticOrderTrackResult implements Serializable {
    @SerializedName("hardCopy")
    private String hardCopy;
    @SerializedName("amount")
    private String amount;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("trackDetails")
    private List<OrderTrackDetail> trackDetailList;
    @SerializedName("patientDetails")
    private PatientDetail patientDetails;
    @SerializedName("slot")
    private SlotTime slot;
    @SerializedName("type")
    private String type;
    @SerializedName("userId")
    private String userId;
    @SerializedName("userDetails")
    private DiagnosticUserDetail userDetails;
    @SerializedName("bookId")
    private String bookId;
    @SerializedName("orderDetails")
    private DiagnosticOrderDetail orderDetails;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("labId")
    private String labId;
    @SerializedName("labOrderStatus")
    private String labOrderStatus;
    @SerializedName("id")
    private String id;
    @SerializedName("labDetails")
    private List<LabDetails> labDetailsList;
    @SerializedName("status")
    private String status;
    @SerializedName("updatedAt")
    private String updatedAt;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("message")
    private String message;
    @SerializedName("paymentDetails")
    private PriceDescription priceDescription;

    public String getHardCopy() {
        return hardCopy;
    }

    public void setHardCopy(String hardCopy) {
        this.hardCopy = hardCopy;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<OrderTrackDetail> getTrackDetailList() {
        return trackDetailList;
    }

    public void setTrackDetailList(List<OrderTrackDetail> trackDetailList) {
        this.trackDetailList = trackDetailList;
    }

    public PatientDetail getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(PatientDetail patientDetails) {
        this.patientDetails = patientDetails;
    }

    public SlotTime getSlot() {
        return slot;
    }

    public void setSlot(SlotTime slot) {
        this.slot = slot;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DiagnosticUserDetail getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(DiagnosticUserDetail userDetails) {
        this.userDetails = userDetails;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public DiagnosticOrderDetail getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(DiagnosticOrderDetail orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getLabOrderStatus() {
        return labOrderStatus;
    }

    public void setLabOrderStatus(String labOrderStatus) {
        this.labOrderStatus = labOrderStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<LabDetails> getLabDetailsList() {
        return labDetailsList;
    }

    public void setLabDetailsList(List<LabDetails> labDetailsList) {
        this.labDetailsList = labDetailsList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }
}
