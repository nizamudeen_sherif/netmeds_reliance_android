package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.diagnostic.model.DiagnosticCoupon;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderDetail;
import com.nms.netmeds.diagnostic.model.DiagnosticUserDetail;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.SlotTime;

import java.util.List;

public class DiagnosticOrderCreateRequest {
    @SerializedName("hardCopy")
    private String hardCopy;
    @SerializedName("orderDetails")
    private DiagnosticOrderDetail orderDetail;
    @SerializedName("amount")
    private double amount;
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("labId")
    private int labId;
    @SerializedName("patientDetails")
    private PatientDetail patientDetails;
    @SerializedName("slot")
    private SlotTime slot;
    @SerializedName("type")
    private String type;
    @SerializedName("userId")
    private String userId;
    @SerializedName("userDetails")
    private DiagnosticUserDetail userDetails;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("testIds")
    private List<String> testIdList;
    @SerializedName("paymentDetails")
    private PriceDescription priceDescription;
    @SerializedName("sourceType")
    private int sourceType;
    @SerializedName("coupon")
    private DiagnosticCoupon coupon;
    @SerializedName("pid")
    private int pid;

    public int getPdi() {
        return pid;
    }

    public void setPdi(int pid) {
        this.pid = pid;
    }

    public void setHardCopy(String hardCopy) {
        this.hardCopy = hardCopy;
    }

    public void setOrderDetail(DiagnosticOrderDetail orderDetail) {
        this.orderDetail = orderDetail;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }

    public void setPatientDetails(PatientDetail patientDetails) {
        this.patientDetails = patientDetails;
    }

    public void setSlot(SlotTime slot) {
        this.slot = slot;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserDetails(DiagnosticUserDetail userDetails) {
        this.userDetails = userDetails;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setTestIdList(List<String> testIdList) {
        this.testIdList = testIdList;
    }

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }

    public int getSourceType() {
        return sourceType;
    }

    public void setSourceType(int sourceType) {
        this.sourceType = sourceType;
    }

    public DiagnosticCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(DiagnosticCoupon coupon) {
        this.coupon = coupon;
    }
}
