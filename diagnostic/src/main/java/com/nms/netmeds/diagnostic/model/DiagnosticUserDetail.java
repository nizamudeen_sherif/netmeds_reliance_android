package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.MStarAddressModel;

import java.io.Serializable;

public class DiagnosticUserDetail implements Serializable {
    @SerializedName("address")
    private MStarAddressModel address;

    public MStarAddressModel getAddress() {
        return address;
    }

    public void setAddress(MStarAddressModel address) {
        this.address = address;
    }
}
