package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivitySelectLabBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.SelectLabResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticSelectLabViewModel;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;

public class DiagnosticSelectLabActivity extends BaseViewModelActivity<DiagnosticSelectLabViewModel> implements DiagnosticSelectLabViewModel.SelectLabViewModelListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener, LocationFragment.LocationFragmentListener {

    private ActivitySelectLabBinding selectLabBinding;
    private DiagnosticSelectLabViewModel labViewModel;
    private List<Test> selectedList = null;
    private static final String PRICE_BREAKUP_FRAG_TAG = "Bottom_fragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectLabBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_lab);
        toolBarSetUp(selectLabBinding.toolBarCustomLayout.toolbar);
        initToolBar(selectLabBinding.toolBarCustomLayout.collapsingToolbar,getResources().getString(R.string.text_select_lab));
        intentValue();
        labViewModel = onCreateViewModel();
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST))
                selectedList = (List<Test>) intent.getSerializableExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST);
        }
    }

    @Override
    protected DiagnosticSelectLabViewModel onCreateViewModel() {
        labViewModel = ViewModelProviders.of(this).get(DiagnosticSelectLabViewModel.class);
        labViewModel.getSelectLabMutableLiveData().observe(this, new SelectLabObserver());
        labViewModel.init(this, selectLabBinding, labViewModel, selectedList, this);
        selectLabBinding.setViewModel(labViewModel);
        labViewModel.getPinCodeResponseMutableLiveData().observe(this, new PinCodeResponseObserver());
        labViewModel.getLabsByTestId(selectedList);
        return labViewModel;
    }

    @Override
    public void vmShowProgress() {
        selectLabBinding.toolBarCustomLayout.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        selectLabBinding.toolBarCustomLayout.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void vmInitBottomView(List<Test> testList) {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), testList, DiagnosticConstant.TEST, DiagnosticConstant.LAB_PAGE, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, PRICE_BREAKUP_FRAG_TAG).commitAllowingStateLoss();
    }

    @Override
    public void vmPatientDetailCallback() {
        // Redirect the Diagnostic Guest User to sign-in page
        if (BasePreference.getInstance(this).isGuestCart()) {
            CommonUtils.redirectToSignIn(this, true);
            return;
        }
        /*Diagnostic WebEngage Lab Updated Event*/
        setWebEngageLabUpdatedEvent(labViewModel.getSelectedLab());
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_LAB, labViewModel.getSelectedLab());
        intent.putExtra(DiagnosticConstant.KEY_INTENT_FROM, DiagnosticConstant.LAB_PAGE);
        LaunchIntentManager.routeToActivity(DiagnosticSelectLabActivity.this.getResources().getString(R.string.route_diagnostic_patient_detail), intent, DiagnosticSelectLabActivity.this);
    }

    @Override
    public void onPriceBreakUpClicked(List<Test> tests) {
        PriceBreakupBottomFragment bottomFragment = new PriceBreakupBottomFragment(getApplication(), tests);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, PRICE_BREAKUP_FRAG_TAG).commitAllowingStateLoss();
    }

    @Override
    public void vmChangeAddress() {
        LocationFragment locationFragment = new LocationFragment(false);
        getSupportFragmentManager().beginTransaction().add(locationFragment, "Location_fragment").commitNowAllowingStateLoss();
    }

    private void setWebEngageLabUpdatedEvent(AvailableLab selectedLab) {
        if (selectedLab != null) {
            LabDescription labDescription = selectedLab.getLabDescription() != null ? selectedLab.getLabDescription() : new LabDescription();
            String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
            PriceDescription priceDescription = selectedLab.getPriceDescription() != null ? selectedLab.getPriceDescription() : new PriceDescription();
            double price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? Double.parseDouble(priceDescription.getFinalPrice()) : 0;
            List<Test> testList = selectedLab.getTestList() != null && selectedLab.getTestList().size() > 0 ? selectedLab.getTestList() : new ArrayList<Test>();
            WebEngageHelper.getInstance().diagnosticsLabUpdatedEvent(BasePreference.getInstance(this),
                    !TextUtils.isEmpty(BasePreference.getInstance(this).getDiagnosticCity()) ? BasePreference.getInstance(this).getDiagnosticCity() : DiagnosticHelper.getCity(), testList, price, labName, this);
        }
    }

    @Override
    public void onNavigation() {
        vmPatientDetailCallback();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {

    }

    @Override
    public void onUpdateSearchList(Test removeTest) {

    }

    @Override
    public void onUpdateLabList() {
        selectLabBinding.testCount.lytTestCount.setVisibility(View.GONE);
        if (DiagnosticHelper.getSelectedList().size() > 0)
            labViewModel.getLabsByTestId(DiagnosticHelper.getSelectedList());
        else {
            LaunchIntentManager.routeToActivity(DiagnosticSelectLabActivity.this.getResources().getString(R.string.route_diagnostic_search_test), this);
            DiagnosticSelectLabActivity.this.finish();
        }
    }

    @Override
    public void onPinCodeCheck(String pinCode) {
        labViewModel.pinCodeServiceCheck(pinCode);
    }

    @Override
    public void onLocationPermission() {

    }

    @Override
    public void onLocationDialogClose() {

    }

    @Override
    public void onLocationServiceResponse() {
        labViewModel.setAddressViewVisibility();
    }

    @Override
    public void onChangedDefaultAddress() {
        labViewModel.setAddressViewVisibility();
        if (labViewModel.mSelectedTestList == null || labViewModel.mSelectedTestList.size() == 0)
            return;
        labViewModel.getLabsByTestId(labViewModel.mSelectedTestList);
    }

    public class SelectLabObserver implements Observer<SelectLabResponse> {

        @Override
        public void onChanged(@Nullable SelectLabResponse selectLabResponse) {
            labViewModel.selectLabDataAvailable(selectLabResponse);
            selectLabBinding.setViewModel(labViewModel);
        }
    }

    public class PinCodeResponseObserver implements Observer<PinCodeResponse> {

        @Override
        public void onChanged(@Nullable PinCodeResponse pinCodeResponse) {
            labViewModel.pinCodeDataAvailable(pinCodeResponse);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_SELECT_LAB);
    }

    @Override
    protected void onResume() {
        super.onResume();
        labViewModel.setAddressViewVisibility();
        if (BasePreference.getInstance(this).getChangesDone() && labViewModel.mSelectedTestList != null && labViewModel.mSelectedTestList.size() > 0) {
            DefaultAddress defaultAddress = new Gson().fromJson(BasePreference.getInstance(this).getDefaultAddress(), DefaultAddress.class);
            BasePreference.getInstance(this).setLabPinCode(defaultAddress.getResult().getCustomer_details().getAddress().getPostcode());
            labViewModel.getLabsByTestId(labViewModel.mSelectedTestList);
            BasePreference.getInstance(this).setChangesDone(false);
        }
        BasePreference.getInstance(this).setPreviousActivitiesCleared(false);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }
}