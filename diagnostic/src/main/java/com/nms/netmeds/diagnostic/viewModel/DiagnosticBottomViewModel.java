package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;

import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DiagnosticBottomViewAdapter;
import com.nms.netmeds.diagnostic.databinding.DiagnosticBottomViewBinding;

import java.util.List;
import java.util.Locale;

public class DiagnosticBottomViewModel extends BaseViewModel implements DiagnosticBottomViewAdapter.CountUpdateListener {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private DiagnosticBottomViewBinding diagnosticBottomViewBinding;
    private List<Test> testList;
    private BottomViewModelListener bottomViewModelListener;
    private int testCount;
    private String type;
    private String page;

    public int getTestCount() {
        return testCount;
    }

    public void setTestCount(int testCount) {
        this.testCount = testCount;
    }

    public DiagnosticBottomViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DiagnosticBottomViewBinding diagnosticBottomViewBinding, List<Test> testList, String type, String page, BottomViewModelListener bottomViewModelListener) {
        this.context = context;
        this.diagnosticBottomViewBinding = diagnosticBottomViewBinding;
        this.testList = testList;
        this.bottomViewModelListener = bottomViewModelListener;
        this.type = type;
        this.page = page;
        setTestCount(testList.size());
        initAdapter();
        showCountView();
        setLabTestCount();
        initListener();
    }

    public String getHeader() {
        int headerText = (getType().equalsIgnoreCase(DiagnosticConstant.PACKAGE) ||
                getType().equalsIgnoreCase(context.getResources().getString(R.string.txt_packages_type))) ? R.plurals.text_select_package_item_with_count : R.plurals.text_selected_test_item_with_count;
        return context.getResources().getQuantityString(headerText, getTestCount(), getTestCount());
    }

    private void initAdapter() {
        DiagnosticBottomViewAdapter adapter = new DiagnosticBottomViewAdapter(context, testList, page, this);
        diagnosticBottomViewBinding.selectedTestList.setLayoutManager(new GridLayoutManager(context, 2));
        diagnosticBottomViewBinding.selectedTestList.setNestedScrollingEnabled(false);
        diagnosticBottomViewBinding.selectedTestList.setAdapter(adapter);
    }

    private void showCountView() {
        diagnosticBottomViewBinding.testCount.lytTestCount.setVisibility(View.VISIBLE);
    }

    private void setLabTestCount() {
        if (!showLabTestCount()) {
            diagnosticBottomViewBinding.testCount.lytTestCount.setVisibility(View.VISIBLE);
            setLabel();
        } else {
            diagnosticBottomViewBinding.testCount.lytTestCount.setVisibility(View.GONE);
            bottomViewModelListener.onCloseDialog();
        }
    }

    private void setLabel() {
        if (getType().equalsIgnoreCase(context.getResources().getString(R.string.txt_package_type)) ||
                getType().equalsIgnoreCase(context.getResources().getString(R.string.txt_packages_type))) {
            diagnosticBottomViewBinding.testCount.textLabel.setText(context.getResources().getQuantityString(R.plurals.text_selected_package_label, getTestCount()));
            diagnosticBottomViewBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%02d", testList.size()));
        } else if ((getType().equalsIgnoreCase(DiagnosticConstant.TEST_TYPE_PROFILE) || getType().equalsIgnoreCase(DiagnosticConstant.TEST)) && (page.equals(DiagnosticConstant.HOME_PAGE) || page.equals(DiagnosticConstant.SEARCH_TEST_PAGE))) {
            diagnosticBottomViewBinding.testCount.textLabel.setText(context.getResources().getQuantityString(R.plurals.text_selected_test_label, getTestCount()));
            diagnosticBottomViewBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%02d", testList.size()));
        } else {
            diagnosticBottomViewBinding.testCount.textLabel.setText(context.getResources().getString(R.string.text_total_amount));
            diagnosticBottomViewBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, getTotalAmount()));
        }
    }

    private boolean showLabTestCount() {
        return getTestCount() == 0;
    }

    private double getTotalAmount() {
        double totalAmount = 0;
        for (Test test : testList) {
            PriceInfo priceInfo = test.getPriceInfo() != null ? test.getPriceInfo() : new PriceInfo();
            if (!TextUtils.isEmpty(priceInfo.getFinalPrice()))
                totalAmount = (totalAmount + Double.parseDouble(priceInfo.getFinalPrice()));
        }
        return totalAmount;
    }

    public void onBottomViewClose() {
        bottomViewModelListener.onCloseDialog();
    }

    private void initListener() {
        diagnosticBottomViewBinding.testCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomViewModelListener.onNext();
            }
        });
    }

    @Override
    public void onUpdateCount(int size, Test removeTest) {
        setTestCount(size);
        setLabTestCount();
        diagnosticBottomViewBinding.setViewModel(DiagnosticBottomViewModel.this);
        bottomViewModelListener.onUpdatePopularTest(removeTest);
    }

    @Override
    public void onUpdateLab() {
        bottomViewModelListener.onUpdateLabCallback();
    }

    private String getType() {
        return testList != null && testList.size() > 0 ? testList.get(0).getType() : "";
    }

    public interface BottomViewModelListener {
        void onCloseDialog();

        void onNext();

        void onUpdatePopularTest(Test removeTest);

        void onUpdateLabCallback();
    }
}
