package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterSearchItemBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticSearchResult;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.List;
import java.util.Locale;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {
    private Context mContext;
    private List<DiagnosticSearchResult> searchList;
    private SearchAdapterListener searchAdapterListener;

    public SearchAdapter(Context context, List<DiagnosticSearchResult> searchList, SearchAdapterListener searchAdapterListener) {
        this.mContext = context;
        this.searchList = searchList;
        this.searchAdapterListener = searchAdapterListener;
    }

    @NonNull
    @Override
    public SearchAdapter.SearchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        AdapterSearchItemBinding adapterSearchItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_search_item, viewGroup, false);
        return new SearchHolder(adapterSearchItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchHolder searchHolder, @SuppressLint("RecyclerView") final int position) {
        final DiagnosticSearchResult searchResult = searchList.get(position);
        int availableAt = !TextUtils.isEmpty(searchResult.getAvailableAt()) ? Integer.parseInt(searchResult.getAvailableAt()) : 0;
        searchHolder.adapterSearchItemBinding.textName.setText(!TextUtils.isEmpty(searchResult.getName()) ? searchResult.getName() : "");
        searchHolder.adapterSearchItemBinding.textAvailableLab.setText(!TextUtils.isEmpty(searchResult.getAvailableAt()) ? mContext.getResources().getQuantityString(R.plurals.text_test_available_lab, availableAt, availableAt) : "");
        searchHolder.adapterSearchItemBinding.textPrice.setText(!TextUtils.isEmpty(searchResult.getPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, searchResult.getPrice()) : "");
        searchHolder.adapterSearchItemBinding.textType.setText(!TextUtils.isEmpty(searchResult.getType()) ? String.format(Locale.getDefault(), "%s: %s%s", mContext.getResources().getString(R.string.text_type), searchResult.getType(), setFastingRequiredText(searchResult.getFastingRequired())) : "");
        searchHolder.adapterSearchItemBinding.textName.setVisibility(TextUtils.isEmpty(searchResult.getName()) ? View.GONE : View.VISIBLE);
        searchHolder.adapterSearchItemBinding.textAvailableLab.setVisibility(TextUtils.isEmpty(searchResult.getAvailableAt()) ? View.GONE : View.VISIBLE);
        searchHolder.adapterSearchItemBinding.textPrice.setVisibility(TextUtils.isEmpty(searchResult.getPrice()) ? View.GONE : View.VISIBLE);
        searchHolder.adapterSearchItemBinding.textType.setVisibility(TextUtils.isEmpty(searchResult.getType()) && TextUtils.isEmpty(setFastingRequiredText(searchResult.getFastingRequired())) ? View.GONE : View.VISIBLE);
        searchHolder.adapterSearchItemBinding.imgCheck.setImageResource(searchResult.isChecked() ? R.drawable.ic_selected_primary : R.drawable.ic_add_circle);

        searchHolder.adapterSearchItemBinding.lLayoutSearchTest.setOnClickListener(new KnowMoreClickListener(searchResult));
        searchHolder.adapterSearchItemBinding.textKnowMore.setOnClickListener(new KnowMoreClickListener(searchResult));
        searchHolder.adapterSearchItemBinding.textName.setOnClickListener(new KnowMoreClickListener(searchResult));
        searchHolder.adapterSearchItemBinding.textAvailableLab.setOnClickListener(new KnowMoreClickListener(searchResult));
        searchHolder.adapterSearchItemBinding.textType.setOnClickListener(new KnowMoreClickListener(searchResult));

        searchHolder.adapterSearchItemBinding.imgCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Test test = new Test();
                test.setTestId(searchResult.getNetmedsId());
                test.setTestName(searchResult.getName());
                test.setType(searchResult.getType());
                test.setCategory(searchResult.getCategory());

                if (!TextUtils.isEmpty(searchResult.getFastingRequired())) {
                    test.setFastingRequired(searchResult.getFastingRequired());
                } else {
                    test.setFastingRequired("");
                }

                PriceInfo priceInfo = new PriceInfo();
                if (!TextUtils.isEmpty(searchResult.getPrice())) {
                    priceInfo.setFinalPrice(searchResult.getPrice());
                } else {
                    priceInfo.setFinalPrice("");
                }
                test.setPriceInfo(priceInfo);

                if (searchResult.isChecked()) {
                    searchResult.setChecked(false);
                    DiagnosticHelper.removeSelectedTest(test);
                    searchAdapterListener.onTestUnChecked();
                } else {
                    if (DiagnosticHelper.getSelectedList().size() == 0) {
                        searchResult.setChecked(true);
                        DiagnosticHelper.setTest(test);
                        searchAdapterListener.onTestChecked(test);
                    } else {
                        if (test.getCategory().equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)) {
                            if (!TextUtils.isEmpty(DiagnosticHelper.getSelectedList().get(0).getCategory()) && DiagnosticHelper.getSelectedList().get(0).getCategory()
                                    .equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)) {
                                searchResult.setChecked(true);
                                DiagnosticHelper.setTest(test);
                                searchAdapterListener.onTestChecked(test);
                            } else {
                                showAlertDialog(mContext, test, searchResult);
                                return;
                            }
                        } else {
                            if (!TextUtils.isEmpty(DiagnosticHelper.getSelectedList().get(0).getCategory()) && DiagnosticHelper.getSelectedList().get(0).getCategory()
                                    .equalsIgnoreCase(DiagnosticConstant.CATEGORY_PATH)) {
                                searchResult.setChecked(true);
                                DiagnosticHelper.setTest(test);
                                searchAdapterListener.onTestChecked(test);
                            } else {
                                showAlertDialog(mContext, test, searchResult);
                                return;
                            }
                        }
                    }
                }
                notifyDataSetChanged();
            }
        });
    }

    public void updateSearchAdapter(List<DiagnosticSearchResult> updateList) {
        this.searchList = updateList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    // Know More Click Listener
    class KnowMoreClickListener  implements View.OnClickListener{

        DiagnosticSearchResult searchResult;

        KnowMoreClickListener(DiagnosticSearchResult searchResult){
            this.searchResult = searchResult;
        }

        @Override
        public void onClick(View v) {
            searchAdapterListener.onSearchedTestClicked(searchResult);
        }
    }


    class SearchHolder extends RecyclerView.ViewHolder {
        AdapterSearchItemBinding adapterSearchItemBinding;

        SearchHolder(@NonNull AdapterSearchItemBinding adapterSearchItemBinding) {
            super(adapterSearchItemBinding.getRoot());
            this.adapterSearchItemBinding = adapterSearchItemBinding;
        }
    }


    private void showAlertDialog(Context context, final Test test, final DiagnosticSearchResult searchResult) {
        new AlertDialog.Builder(context)
                .setMessage(context.getResources().getString(R.string.alert_dialog_msg))
                .setPositiveButton(context.getResources().getString(R.string.alert_dialog_Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DiagnosticHelper.clearSelectedList();
                        searchResult.setChecked(true);
                        DiagnosticHelper.setTest(test);
                        searchAdapterListener.onTestChecked(test);
                        dialog.dismiss();
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(context.getResources().getString(R.string.alert_dialog_No), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    public interface SearchAdapterListener {
        void onTestChecked(Test selectedTest);

        void onTestUnChecked();

        void onSearchedTestClicked(DiagnosticSearchResult searchResult);

    }

    private String setFastingRequiredText(String fastingRequired) {
        if (!TextUtils.isEmpty(fastingRequired)) {
            int fasting = Integer.parseInt(fastingRequired);
            if (fasting == 1)
                return ", " + mContext.getResources().getString(R.string.text_fasting_required);
            else if (fasting == 2)
                return ", " + mContext.getResources().getString(R.string.text_fasting_required);
            else return "";
        } else return "";
    }


}