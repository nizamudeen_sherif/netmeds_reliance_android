package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterBannerItemBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// Diagnostic Home Top Banner Adapter
public class DiagnosticHomeBannerAdapter extends RecyclerView.Adapter<DiagnosticHomeBannerAdapter.BannerAdapterViewHolder> {
    private Context mContext;
    private List<MstarBanner> offerList;

    public DiagnosticHomeBannerAdapter(Context context, List<MstarBanner> offerList) {
        this.mContext = context;
        this.offerList = offerList;
    }

    @NonNull
    @Override
    public BannerAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterBannerItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_banner_item, viewGroup, false);
        return new BannerAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerAdapterViewHolder bannerAdapterViewHolder, int position) {

        final MstarBanner mstarBanner = offerList.get(position);
        DiagnosticHelper.populateImage(mContext, mstarBanner.getImageUrl(), bannerAdapterViewHolder.bannerItemBinding.imgOffer,R.drawable.ic_default_lab);
        bannerAdapterViewHolder.bannerItemBinding.imgOffer.setOnClickListener(new BannerClickListener(mstarBanner));
    }

    // Banner Click Listener
    private class BannerClickListener implements View.OnClickListener{

        private MstarBanner mstarBanner;

        private BannerClickListener(final MstarBanner mstarBanner){
            this.mstarBanner = mstarBanner;
        }

        @Override
        public void onClick(View view) {
            redirectToTestOrPackageDetail(mstarBanner);
        }
    }


    // Redirect the user to Test/Package Details
    private void redirectToTestOrPackageDetail(MstarBanner mstarBanner){
        if(mstarBanner ==  null || TextUtils.isEmpty(mstarBanner.getUrl()) || TextUtils.isEmpty(mstarBanner.getType())) return;
        Intent intent = new Intent();
        Test test = new Test();
        test.setUrl(mstarBanner.getUrl());
        test.setTestName("");
        if(mstarBanner.getType().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_profile_type)) || mstarBanner.getType().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_packages_type))
                || mstarBanner.getType().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_package_type))){
            final DiagnosticPackage diagnosticPackage = new DiagnosticPackage();
            ArrayList<Test> arrayList = new ArrayList<>();
            arrayList.add(test);
            diagnosticPackage.setTestList(arrayList);
            intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED, diagnosticPackage);
            LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_package_detail), intent, mContext);
        }else{
            intent.putExtra(DiagnosticConstant.INTENT_TEST_SELECTED, test);
            intent.putExtra(DiagnosticConstant.TEST, (Serializable) null);
            LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_test_detail), intent, mContext);
        }
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    static class BannerAdapterViewHolder extends RecyclerView.ViewHolder {
        private AdapterBannerItemBinding bannerItemBinding;

        private BannerAdapterViewHolder(AdapterBannerItemBinding binding) {
            super(binding.getRoot());
            this.bannerItemBinding = binding;
        }
    }
}
