package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.EncryptionUtils;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.SearchAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivitySearchTestBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticAlgoliaSearchResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticSearchResult;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.nms.netmeds.base.utils.DiagnosticConstant.FILTER_TYPE_ALL;
import static com.nms.netmeds.base.utils.DiagnosticConstant.FILTER_TYPE_HEALTH_PACKAGE;
import static com.nms.netmeds.base.utils.DiagnosticConstant.FILTER_TYPE_PATHOLOGY;
import static com.nms.netmeds.base.utils.DiagnosticConstant.FILTER_TYPE_RADIOLOGY;

public class DiagnosticSearchTestViewModel extends AppViewModel implements SearchAdapter.SearchAdapterListener {
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivitySearchTestBinding searchTestBinding;
    private DiagnosticSearchTestViewModel searchTestViewModel;
    private DiagnosticSearchTestListener searchTestListener;
    private SearchAdapter mSearchAdapter;
    private List<DiagnosticSearchResult> searchList;
    private int searchLimit = 0;
    private boolean loadMore = false;
    private int searchCount = 0;
    private boolean isAlgoliaSearching = false;
    private LinearLayoutManager mLinearLayoutManager;
    private MutableLiveData<DiagnosticAlgoliaSearchResponse> searchDataMutableLiveData = new MutableLiveData<>();
    private boolean fromTestOrPackageSearch;
    private String filter;
    private String filterType;


    public DiagnosticSearchTestViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivitySearchTestBinding searchTestBinding, DiagnosticSearchTestViewModel searchTestViewModel, DiagnosticSearchTestListener searchTestListener) {
        this.context = context;
        this.searchTestBinding = searchTestBinding;
        this.searchTestViewModel = searchTestViewModel;
        this.searchTestListener = searchTestListener;
        searchList = new ArrayList<>();
        initSearchAdapter();
        initListener();
    }

    private void initListener() {
        searchTestBinding.testCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchTestListener.vmNavigateToLab();
            }
        });

        searchTestBinding.testCount.lytSelectedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchTestListener.vmInitBottomView();
            }
        });
        searchTestBinding.testListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (!loadMore && searchLimit < searchCount) {
                        searchLimit += 1;
                        loadMore = true;
                        if(!TextUtils.isEmpty(searchTestBinding.edtFilter.getText())){
                            searchApiCall(apiFilterString(true));
                        }else {
                            searchApiCall(apiFilterString(false));
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    @Override
    public void onRetryClickListener() {
        if (!TextUtils.isEmpty(searchTestBinding.edtFilter.getText()))
            searchApiCall(apiFilterString(false));
        showWebserviceErrorView(false);
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == DiagnosticServiceManager.ALGOLIA_SEARCH) {
            isAlgoliaSearching = false;
            DiagnosticAlgoliaSearchResponse searchDataResponse = new Gson().fromJson(data, DiagnosticAlgoliaSearchResponse.class);
            searchDataMutableLiveData.setValue(searchDataResponse);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        if (transactionId == DiagnosticServiceManager.ALGOLIA_SEARCH) {
            vmOnError();
        }
    }


    private void showNoNetworkView(boolean isConnected) {
        searchTestBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        searchTestBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
        searchTestBinding.diagnosticSearchEmptyView.setVisibility(View.GONE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        searchTestBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        searchTestBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
        searchTestBinding.diagnosticSearchEmptyView.setVisibility(View.GONE);
    }

    private void vmOnError() {
        isAlgoliaSearching = false;
        loadMore = false;
        showWebserviceErrorView(true);
    }

    public MutableLiveData<DiagnosticAlgoliaSearchResponse> getSearchDataMutableLiveData() {
        return searchDataMutableLiveData;
    }

    @Override
    public void onTestChecked(Test selectedTest) {
        setLabTestCount();
    }

    @Override
    public void onTestUnChecked() {
        setLabTestCount();
    }

    @Override
    public void onSearchedTestClicked(DiagnosticSearchResult searchResult) {
        searchTestListener.vmNavigateToTestDetail(searchResult);
    }

    public void setLabTestCount() {
        if (!showLabTestCount()) {
            if (DiagnosticHelper.getSelectedList() != null && DiagnosticHelper.getSelectedList().size() == 1 &&
                    (DiagnosticHelper.getSelectedList().get(0).getType().equalsIgnoreCase(context.getResources().getString(R.string.txt_package_type)) || DiagnosticHelper.getSelectedList().get(0).getType().equalsIgnoreCase(context.getResources().getString(R.string.txt_packages_type)))) {
                searchTestBinding.testCount.lytTestCount.setVisibility(View.VISIBLE);
                searchTestBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%02d", selectedCount()));
                searchTestBinding.testCount.textLabel.setText(context.getResources().getQuantityString(R.plurals.text_selected_package_label, selectedCount()));
                return;
            }
            searchTestBinding.testCount.lytTestCount.setVisibility(View.VISIBLE);
            searchTestBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%02d", selectedCount()));
            searchTestBinding.testCount.textLabel.setText(context.getResources().getQuantityString(R.plurals.text_selected_test_label, selectedCount()));
        } else
            searchTestBinding.testCount.lytTestCount.setVisibility(View.GONE);
    }

    private String getTotalAmount() {
        if (DiagnosticHelper.getSelectedList() == null || DiagnosticHelper.getSelectedList().size() == 0 || DiagnosticHelper.getSelectedList().get(0) == null ||
                DiagnosticHelper.getSelectedList().get(0).getPriceInfo() == null || TextUtils.isEmpty(DiagnosticHelper.getSelectedList().get(0).getPriceInfo().getFinalPrice()))
            return "";
        return DiagnosticHelper.getSelectedList().get(0).getPriceInfo().getFinalPrice();
    }

    private boolean showLabTestCount() {
        return selectedCount() == 0;
    }

    private int selectedCount() {
        return DiagnosticHelper.getSelectedList().size();
    }

    public interface DiagnosticSearchTestListener {

        void vmShowProgress();

        void vmDismissProgress();

        void vmNavigateToLab();

        void vmInitBottomView();

        void vmNavigateToTestDetail(DiagnosticSearchResult searchResult);
    }

    private void initSearchAdapter() {
        mLinearLayoutManager = new LinearLayoutManager(context);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        searchTestBinding.testListView.setLayoutManager(mLinearLayoutManager);
        searchTestBinding.testListView.setNestedScrollingEnabled(false);
        searchTestBinding.testListView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        mSearchAdapter = new SearchAdapter(context, searchList, this);
        searchTestBinding.testListView.setAdapter(mSearchAdapter);
    }

    public void onFilterQueryChanged(final CharSequence s) {
        if (!TextUtils.isEmpty(s.toString())) {
            searchCount = 0;
            searchLimit = 0;
            loadMore = false;
            searchList = new ArrayList<>();
            searchApiCall(apiFilterString(true));
        } else {
            searchCount = 0;
            searchLimit = 0;
            loadMore = false;
            searchList = new ArrayList<>();
            searchApiCall(apiFilterString(false));
        }
        searchTestBinding.imgSearch.setImageDrawable(ContextCompat.getDrawable(context, s.toString().isEmpty() ? R.drawable.ic_search : R.drawable.ic_clear));
        searchTestBinding.imgSearch.setTag(!TextUtils.isEmpty(s.toString()) ? DiagnosticConstant.TAG_CLEAR : DiagnosticConstant.TAG_SEARCH);
    }


    private void clearResult(){
        searchTestBinding.edtFilter.setText("");
        searchTestBinding.imgSearch.setTag(DiagnosticConstant.TAG_SEARCH);
        CommonUtils.hideKeyboard(context, searchTestBinding.lytViewContent);
        searchCount = 0;
        searchLimit = 0;
        loadMore = false;
        searchList = new ArrayList<>();
        searchApiCall(apiFilterString(false));
    }

    public void onClear(View v) {
        String tag = v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString()) ? v.getTag().toString() : "";
        if (tag.equals(DiagnosticConstant.TAG_CLEAR)) {
            clearResult();
        }
    }

    private void checkTestExistOrNot(Test test) {
        for (Test selectedTest : DiagnosticHelper.getSelectedList()) {
            if (test.getTestId().equals(selectedTest.getTestId()))
                test.setChecked(true);
        }
    }

    public void searchApiCall(String filter) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (!isAlgoliaSearching && isConnected && searchTestBinding.edtFilter.getText() != null) {
            isAlgoliaSearching = true;
            BasePreference basePreference = BasePreference.getInstance(context);
            if (!TextUtils.isEmpty(basePreference.getConfiguration())) {
                ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
                DiagnosticServiceManager.getInstance().algoliaSearch(filter, searchTestViewModel, searchTestBinding.edtFilter.getText().toString(), searchLimit,
                        20, EncryptionUtils.decryptAESCBCEncryption(configurationResponse.getResult().getConfigDetails().getAlgoliaCredentialDetail().getAlgoliaDiagnosticApiKey()),
                        configurationResponse.getResult().getConfigDetails().getAlgoliaCredentialDetail().getAlgoliaDiagnosticIndex(),
                        EncryptionUtils.decryptAESCBCEncryption(configurationResponse.getResult().getConfigDetails().getAlgoliaCredentialDetail().getAlgoliaAppId()));
            }
        }
    }

    public void searchResponseDataAvailable(DiagnosticAlgoliaSearchResponse searchResponse) {
        if (searchResponse != null && searchResponse.getDiagnosticSearchResultList() != null && searchResponse.getDiagnosticSearchResultList().size() > 0) {
            if (!loadMore)
                searchList.clear();
            searchCount = searchResponse.getNbPages() - 1;
            for (DiagnosticSearchResult searchResult : searchResponse.getDiagnosticSearchResultList()) {
                for (Test selectedTest : DiagnosticHelper.getSelectedList()) {
                    if (selectedTest.getTestId().equals(searchResult.getNetmedsId()))
                        searchResult.setChecked(true);
                }
                searchList.add(searchResult);
            }

            mSearchAdapter.updateSearchAdapter(searchList);
            searchTestBinding.diagnosticSearchEmptyView.setVisibility(View.GONE);
            searchTestBinding.cardViewTest.setVisibility(View.VISIBLE);
            loadMore = false;
        } else {
            searchTestBinding.diagnosticSearchEmptyView.setVisibility(View.VISIBLE);
            searchTestBinding.cardViewTest.setVisibility(View.GONE);
        }
    }

    public void onUpdateSearchList(Test removeTest) {
        for (DiagnosticSearchResult searchResult : searchList) {
            if (searchResult.getNetmedsId().equals(removeTest.getTestId())) {
                searchResult.setChecked(false);
                break;
            }
        }
        mSearchAdapter.updateSearchAdapter(searchList);
        setLabTestCount();
    }

    public boolean isFromTestOrPackageSearch() {
        return fromTestOrPackageSearch;
    }

    public void setFromTestOrPackageSearch(boolean fromTestOrPackageSearch) {
        this.fromTestOrPackageSearch = fromTestOrPackageSearch;
    }

    public String emptySearchResultMessage() {
        if (isFromTestOrPackageSearch())
            return context.getResources().getString(R.string.err_msg_search_empty_update);
        return context.getResources().getString(R.string.err_msg_search_empty);
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    private String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String apiFilterString(boolean flag) {
        if (TextUtils.isEmpty(getFilterType())) return "";
        switch (getFilterType()) {
            case FILTER_TYPE_ALL: {
                return "";
            }
            case FILTER_TYPE_RADIOLOGY: {
                if (flag) {
                    return context.getResources().getString(R.string.filter_search_radiology);
                }
                return context.getResources().getString(R.string.filter_radiology);
            }
            case FILTER_TYPE_PATHOLOGY: {
                if (flag) {
                    return context.getResources().getString(R.string.filter_search_pathology);
                }
                return context.getResources().getString(R.string.filter_pathology);
            }
            case FILTER_TYPE_HEALTH_PACKAGE: {
                if (flag) {
                    return context.getResources().getString(R.string.filter_search_package);
                }
                return context.getResources().getString(R.string.filter_packages);
            }
        }
        return "";
    }

    public void viewALL() {
        setFilterType(FILTER_TYPE_ALL);
        setSelectedFilter();
        clearResult();

    }

    public void pathology() {
        setFilterType(FILTER_TYPE_PATHOLOGY);
        setSelectedFilter();
        clearResult();
    }

    public void radioLogy() {
        setFilterType(FILTER_TYPE_RADIOLOGY);
        setSelectedFilter();
        clearResult();
    }

    public void healthPackage() {
        setFilterType(FILTER_TYPE_HEALTH_PACKAGE);
        setSelectedFilter();
        clearResult();
    }

    public void setSelectedFilter() {
        if (TextUtils.isEmpty(getFilterType())) {
            searchTestBinding.txtFilterAll.setBackgroundColor(context.getResources().getColor(R.color.colorBlueLight));
            searchTestBinding.txtFilterAll.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            searchTestBinding.txtFilterPathology.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            searchTestBinding.txtFilterPathology.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
            searchTestBinding.txtFilterRadiology.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            searchTestBinding.txtFilterRadiology.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
            searchTestBinding.txtHealthPackages.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            searchTestBinding.txtHealthPackages.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
            return;
        }

        switch (getFilterType()) {
            case FILTER_TYPE_ALL: {
                searchTestBinding.txtFilterAll.setBackground(ContextCompat.getDrawable(context, R.drawable.accent_button));
                searchTestBinding.txtFilterAll.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                searchTestBinding.txtFilterPathology.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterPathology.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtFilterRadiology.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterRadiology.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtHealthPackages.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtHealthPackages.setTextColor(context.getResources().getColor(R.color.colorAccent));
                break;
            }
            case FILTER_TYPE_RADIOLOGY: {

                searchTestBinding.txtFilterRadiology.setBackground(ContextCompat.getDrawable(context, R.drawable.accent_button));
                searchTestBinding.txtFilterRadiology.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                searchTestBinding.txtFilterAll.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterAll.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtFilterPathology.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterPathology.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtHealthPackages.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtHealthPackages.setTextColor(context.getResources().getColor(R.color.colorAccent));
                break;
            }
            case FILTER_TYPE_PATHOLOGY: {
                searchTestBinding.txtFilterPathology.setBackground(ContextCompat.getDrawable(context, R.drawable.accent_button));
                searchTestBinding.txtFilterPathology.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                searchTestBinding.txtFilterAll.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterAll.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtFilterRadiology.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterRadiology.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtHealthPackages.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtHealthPackages.setTextColor(context.getResources().getColor(R.color.colorAccent));
                break;
            }
            case FILTER_TYPE_HEALTH_PACKAGE: {
                searchTestBinding.txtHealthPackages.setBackground(ContextCompat.getDrawable(context, R.drawable.accent_button));
                searchTestBinding.txtHealthPackages.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                searchTestBinding.txtFilterAll.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterAll.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtFilterPathology.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterPathology.setTextColor(context.getResources().getColor(R.color.colorAccent));
                searchTestBinding.txtFilterRadiology.setBackground(ContextCompat.getDrawable(context, R.drawable.delete_button));
                searchTestBinding.txtFilterRadiology.setTextColor(context.getResources().getColor(R.color.colorAccent));
                break;
            }
        }
    }
}