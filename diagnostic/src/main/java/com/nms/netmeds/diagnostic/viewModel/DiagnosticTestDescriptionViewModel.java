package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Paint;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.font.LatoTextView;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.LocationServiceCheckListener;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.AvailableLabsAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticTestDescriptionBinding;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.LabResult;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResult;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.model.TestDetailResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import static com.nms.netmeds.diagnostic.DiagnosticServiceManager.DIAGNOSTIC_TEST_DETAILS;


public class DiagnosticTestDescriptionViewModel extends AppViewModel {

    public TestDetailResponse testDetailResponse;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityDiagnosticTestDescriptionBinding testDescriptionBinding;
    private TestDetailListener testDetailListener;
    private int failedTransactionId;
    private Test selectedTest;
    private int minimumAmountSelectedPosition = -1;
    private boolean fromDeepLink = false;
    private static LocationServiceCheckListener locationServiceCheckListener;
    private MutableLiveData<PinCodeResponse> pinCodeResponseMutableLiveData = new MutableLiveData<>();

    public DiagnosticTestDescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticTestDescriptionBinding testDescriptionBinding, TestDetailListener testDetailListener, boolean fromDeepLink) {
        this.context = context;
        this.testDescriptionBinding = testDescriptionBinding;
        this.testDetailListener = testDetailListener;
        this.fromDeepLink = fromDeepLink;
        setPinCode();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId){
            case DIAGNOSTIC_TEST_DETAILS:{
                testDetailResponse = new Gson().fromJson(data, TestDetailResponse.class);
                testDataAvailable();
                break;
            }
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK: {
                if (data != null) {
                    PinCodeResponse pinCodeResponse = new Gson().fromJson(CommonUtils.isJSONValid(data) ? data : "", PinCodeResponse.class);
                    DiagnosticHelper.setDefaultAddressToEmpty(testDetailListener.getContext());
                    pinCodeResponseMutableLiveData.setValue(pinCodeResponse);
                }
                break;
            }
            default:{
                testDetailListener.vmDismissProgress();
                testDetailListener.vmFinishActivity();
                break;
            }
        }
    }

    private void testDataAvailable() {
        initTestName();
        initAvailableLabs();
        initTabDescriptions();
        initBottomPriceView();
        setMeantFor();
        setTestReq();
        testDetailListener.vmDismissProgress();

        // Test/Package Viewed WebEngage Event
        WebEngageHelper.getInstance().testOrPackageViewed(BasePreference.getInstance(context),
                getTestNameForWebEngage(), getTestTypeForWebEngage(), labs.toString(), prices.toString(), context.getResources().getString(R.string.source), testDetailListener.getContext()
        );
    }

    public MutableLiveData<PinCodeResponse> getPinCodeResponseMutableLiveData() {
        return pinCodeResponseMutableLiveData;
    }

    private String getTestNameForWebEngage() {
        if (!isTestAvailable() || TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestName()))
            return "";
        return testDetailResponse.getResult().getTests().get(0).getTestName();
    }

    private String getTestTypeForWebEngage() {
        if (!isTestAvailable() || TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getType()))
            return "";
        getLabsAndPricesForWebEngage();
        return testDetailResponse.getResult().getTests().get(0).getType();
    }

    public void changeAddress() {
        testDetailListener.vmChangeAddress();
    }

    public static void setServiceCheckListener(LocationServiceCheckListener successListener) {
        locationServiceCheckListener = successListener;
    }

    public void pinCodeServiceCheck(String code) {
        boolean isConnected = NetworkUtils.isConnected(testDetailListener.getContext());
        if (isConnected) {
            PinCodeServiceCheckRequest serviceCheckRequest = new PinCodeServiceCheckRequest();
            serviceCheckRequest.setPinCode(code);
            DiagnosticServiceManager.getInstance().pinCodeServiceAvailabilityCheck(this, serviceCheckRequest, BasePreference.getInstance(testDetailListener.getContext()));
        } else
            locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_nonetwork_error_message));
    }

    public void pinCodeDataAvailable(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse != null) {
            checkPinCodeServiceStatus(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
    }

    private void checkPinCodeServiceStatus(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getServiceStatus() != null && pinCodeResponse.getServiceStatus().getStatusCode() != null && pinCodeResponse.getServiceStatus().getStatusCode() == DiagnosticConstant.STATUS_CODE_200) {
            getPinCodeResponseResult(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(pinCodeResponse.getResult() != null && !TextUtils.isEmpty(pinCodeResponse.getResult().getMessage()) ? pinCodeResponse.getResult().getMessage() : testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeResponseResult(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getResult() != null) {
            getPinCodeService(pinCodeResponse.getResult());
        } else
            locationServiceCheckListener.onShowMessage(testDetailListener.getContext().getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeService(PinCodeResult pinCodeResult) {
        if (pinCodeResult.getService()) {
            String pinCode = !TextUtils.isEmpty(pinCodeResult.getPinCode()) ? pinCodeResult.getPinCode() : "";
            String city = !TextUtils.isEmpty(pinCodeResult.getCity()) ? pinCodeResult.getCity() : "";
            DiagnosticHelper.setCity(city);
            BasePreference.getInstance(testDetailListener.getContext()).setDiagnosticPinCode(pinCode);
            BasePreference.getInstance(testDetailListener.getContext()).setDiagnosticCity(city);
            BasePreference.getInstance(testDetailListener.getContext()).setLabPinCode(pinCode);
            getTestDetails();
            locationServiceCheckListener.onPinCodeSuccess(pinCode);
        } else
            locationServiceCheckListener.onPinCodeFailed();
    }


    private StringBuilder labs = new StringBuilder();
    private StringBuilder prices = new StringBuilder();

    private void getLabsAndPricesForWebEngage() {
        if (!isLabAvailable()) return;
        ArrayList<LabDescription> labDescriptions = new ArrayList<>();
        ArrayList<PriceDescription> priceDescriptions = new ArrayList<>();
        for (LabResult labResult : testDetailResponse.getResult().getLabs()) {
            if (labResult != null) {
                if (labResult.getLabDescription() != null) {
                    LabDescription labDescription = new LabDescription();
                    if (!TextUtils.isEmpty(labResult.getLabDescription().getLabName())) {
                        labDescription.setLabName(labResult.getLabDescription().getLabName());
                    }
                    if (labResult.getLabDescription().getLabAddressList() != null && labResult.getLabDescription().getLabAddressList().size() > 0) {
                        labDescription.setLabAddressList(labResult.getLabDescription().getLabAddressList());
                    }
                    if (!TextUtils.isEmpty(labResult.getLabDescription().getShortDetails())) {
                        labDescription.setShortDetails(labResult.getLabDescription().getShortDetails());
                    }
                    labDescription.setId(labResult.getLabDescription().getId());
                    labDescription.setActive(labResult.getLabDescription().isActive());
                    labDescriptions.add(labDescription);
                }
                if (labResult.getPriceDescription() != null) {
                    priceDescriptions.add(labResult.getPriceDescription());
                }
            }
        }
        labs.append(new Gson().toJson(labDescriptions));
        prices.append(new Gson().toJson(priceDescriptions));
    }

    private void initBottomPriceView() {
        if (!isLabAvailable()) {
            testDescriptionBinding.llBottomPriceAndBook.setVisibility(View.GONE);
            testDescriptionBinding.llNoLabFound.setVisibility(View.VISIBLE);
            return;
        }
        displayMinPrice();
    }


    private void displayMinPrice() {
        testDescriptionBinding.llNoLabFound.setVisibility(View.GONE);
        testDescriptionBinding.llBottomPriceAndBook.setVisibility(View.VISIBLE);
        int minAmount = Integer.MAX_VALUE;
        int strikeAmount = 0;
        for (int i = 0; i < testDetailResponse.getResult().getLabs().size(); i++) {
            if (testDetailResponse.getResult().getLabs().get(i).getPriceDescription() != null && !TextUtils.isEmpty(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getFinalPrice())
                    && Integer.parseInt(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getFinalPrice()) < minAmount) {
                minAmount = Integer.parseInt(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getFinalPrice());
                strikeAmount = Integer.parseInt(testDetailResponse.getResult().getLabs().get(i).getPriceDescription().getOfferedPrice());
                minimumAmountSelectedPosition = i;
            }
        }
        testDescriptionBinding.textPrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, String.valueOf(minAmount)));
        testDescriptionBinding.textStrikePrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, String.valueOf(strikeAmount)));
        testDescriptionBinding.textStrikePrice.setPaintFlags(testDescriptionBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    @SuppressLint("SetTextI18n")
    private void setMeantFor() {
        if (testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getTests() != null && !TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getWhenToGetTested())) {
            testDescriptionBinding.llPackageMeantFor.setVisibility(View.VISIBLE);
            testDescriptionBinding.tvPackageMeantForContent.setVisibility(View.VISIBLE);
            testDescriptionBinding.tvPackageMeantForContent.removeAllViews();
            String[] whenToGetTest = testDetailResponse.getResult().getTests().get(0).getWhenToGetTested().split(",");
            for (String string : whenToGetTest) {
                LayoutInflater li = LayoutInflater.from(context);
                View childView = li.inflate(R.layout.meant_for, null, false);
                LatoTextView textView = childView.findViewById(R.id.txtViewMeantFor);
                textView.setText(CommonUtils.fromHtml(string));
                testDescriptionBinding.tvPackageMeantForContent.addView(childView);
            }
        } else {
            testDescriptionBinding.llPackageMeantFor.setVisibility(View.GONE);
            testDescriptionBinding.tvPackageMeantForContent.setVisibility(View.GONE);
        }
    }

    private void setTestReq() {
        if (testDetailResponse == null || testDetailResponse.getResult() == null
                || testDetailResponse.getResult().getTests() == null || testDetailResponse.getResult().getTests().size() == 0
                || (TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestSample())
                && TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestPreparation()))) {
            testDescriptionBinding.cardTestReq.setVisibility(View.GONE);
            testDescriptionBinding.llTestRequirements.setVisibility(View.GONE);
            return;
        }

        testDescriptionBinding.cardTestReq.setVisibility(View.VISIBLE);
        testDescriptionBinding.llTestRequirements.setVisibility(View.VISIBLE);
        testDescriptionBinding.llTestRequirements.removeAllViews();

        if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestSample())) {
            LayoutInflater li = LayoutInflater.from(context);
            View childView = li.inflate(R.layout.test_sample_prepartion, null, false);
            ImageView imgTestSamplePrep = childView.findViewById(R.id.imgTestSamplePrep);
            LatoTextView txtTestSamplePrep = childView.findViewById(R.id.txtTestSamplePrep);
            LatoTextView txtTestSamplePrepValue = childView.findViewById(R.id.txtTestSamplePrepValue);
            imgTestSamplePrep.setImageResource(R.drawable.experience_bachelor);
            txtTestSamplePrep.setText(context.getResources().getString(R.string.diag_test_sample_string));
            txtTestSamplePrepValue.setText(CommonUtils.fromHtml(testDetailResponse.getResult().getTests().get(0).getTestSample()));
            testDescriptionBinding.llTestRequirements.addView(childView);
        }

        if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestPreparation())) {
            LayoutInflater li = LayoutInflater.from(context);
            View childView = li.inflate(R.layout.test_sample_prepartion, null, false);
            ImageView imgTestSamplePrep = childView.findViewById(R.id.imgTestSamplePrep);
            LatoTextView txtTestSamplePrep = childView.findViewById(R.id.txtTestSamplePrep);
            LatoTextView txtTestSamplePrepValue = childView.findViewById(R.id.txtTestSamplePrepValue);
            imgTestSamplePrep.setImageResource(R.drawable.ic_no_image);
            txtTestSamplePrep.setText(context.getResources().getString(R.string.diag_test_sample_test_prep));
            txtTestSamplePrepValue.setText(CommonUtils.fromHtml(testDetailResponse.getResult().getTests().get(0).getTestPreparation()));
            testDescriptionBinding.llTestRequirements.addView(childView);
        }
    }


    private void initTabDescriptions() {
        if (!isTestAvailable() || TextUtils.isEmpty(getTest().getTestDescription())) {
            testDescriptionBinding.tabDetails.setVisibility(View.GONE);
            testDescriptionBinding.cardTestDecription.setVisibility(View.GONE);
            return;
        }
        final LinkedHashMap<String, String> detailsMap = new LinkedHashMap<>();
            detailsMap.put(context.getResources().getString(R.string.test_details_txt_test_desc), testDetailResponse.getResult().getTests().get(0).getTestDescription());
        if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getWhenToGetTested()))
            detailsMap.put(context.getResources().getString(R.string.test_when_to_get_tested), testDetailResponse.getResult().getTests().get(0).getWhenToGetTested());
        testDescriptionBinding.tabDetails.setVisibility(View.VISIBLE);
        testDescriptionBinding.cardTestDecription.setVisibility(View.VISIBLE);
        LinearLayout parent = testDescriptionBinding.llFragmentNames;
        parent.removeAllViews();
        int indexCount = 0;
        for (Map.Entry<String, String> entry : detailsMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (!TextUtils.isEmpty(value)) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final TextView child = (TextView) inflater.inflate(R.layout.diagnostic_test_tab_item, null);
                child.setText(key);
                if (indexCount == 0) {
                    child.setTextColor(context.getResources().getColor(R.color.colorDeepAqua));
                    child.setBackground(context.getResources().getDrawable(R.drawable.white_rectangle_rounded_top));
                    testDescriptionBinding.tvDescriptionContent.setText(Html.fromHtml(value));
                }
                child.setTag(key);
                child.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String key = (String) v.getTag();
                        TextView selectedView = testDescriptionBinding.getRoot().findViewWithTag(key);
                        selectedView.setTextColor(context.getResources().getColor(R.color.colorDeepAqua));
                        selectedView.setBackground(context.getResources().getDrawable(R.drawable.white_rectangle_rounded_top));
                        testDescriptionBinding.tvDescriptionContent.setText(Html.fromHtml(detailsMap.get(key)));
                        for (Map.Entry<String, String> entry : detailsMap.entrySet()) {
                            String k = entry.getKey();
                            String value = entry.getValue();
                            if (!key.equals(k)) {
                                TextView notSelectedTV = testDescriptionBinding.getRoot().findViewWithTag(k);
                                notSelectedTV.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
                                notSelectedTV.setBackground(context.getResources().getDrawable(R.drawable.grey_rectangle_rounded_top));
                            }
                        }
                    }
                });
                indexCount++;
                parent.addView(child);
            }
        }
    }

    private void initAvailableLabs() {
        if (isLabAvailable()) {
            testDescriptionBinding.llLabsAvailable.setVisibility(View.VISIBLE);
            initLabAdapter();
        }else{
            testDescriptionBinding.llLabsAvailable.setVisibility(View.GONE);
        }
    }

    private void initLabAdapter() {
        if (isLabAvailable()) {
            AvailableLabsAdapter adapter = new AvailableLabsAdapter(testDetailResponse.getResult().getLabs(), context, testDetailListener);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            testDescriptionBinding.labList.setLayoutManager(layoutManager);
            testDescriptionBinding.labList.setAdapter(adapter);
        }
    }

    public boolean isLabAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getLabs() != null
                && testDetailResponse.getResult().getLabs().size() > 0;
    }

    public boolean isTestAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getTests() != null
                && testDetailResponse.getResult().getTests().size() > 0;
    }

    public Test getTest() {
        return testDetailResponse.getResult().getTests().get(0);
    }

    private void initTestName() {
        if (!isTestAvailable()) {
            testDescriptionBinding.testName.setVisibility(View.GONE);
            return;
        }
        if (!TextUtils.isEmpty(getTest().getTestName())) {
            testDescriptionBinding.testName.setVisibility(View.VISIBLE);
            testDescriptionBinding.testName.setText(testDetailResponse.getResult().getTests().get(0).getTestName());
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        testDetailListener.vmDismissProgress();
        failedTransactionId = transactionId;
        if(fromDeepLink){
            testDetailListener.vmFinishActivity();
            return;
        }
        showWebserviceErrorView(true);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        testDetailListener.vmDismissProgress();
        testDescriptionBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        testDescriptionBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == DIAGNOSTIC_TEST_DETAILS) {
            getTestDetails();
        }
    }

    public void getTestDetails() {
        if (selectedTest == null || TextUtils.isEmpty(selectedTest.getUrl())) return;
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        showNoNetworkView(isConnected);
        if (isConnected) {
            testDetailListener.vmShowProgress();
            DiagnosticServiceManager.getInstance().getTestDetails(this, selectedTest.getUrl(), BasePreference.getInstance(context).getDiagnosticPinCode(), BasePreference.getInstance(context));
        } else failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_TEST_DETAILS;
    }

    public void onSearchClicked() {
        testDetailListener.vmNavigateToSearch();
    }

    public void onBookTest() {
        testDetailListener.vmNavigateToPatientDetails(minimumAmountSelectedPosition);
    }

    public void setSelectedTest(Test selectedTest) {
        this.selectedTest = selectedTest;
    }

    private void showNoNetworkView(boolean isConnected) {
        testDescriptionBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    public void setPinCode() {
        testDescriptionBinding.textPinCode.setText(DiagnosticHelper.getFormattedAddress(testDetailListener.getContext()));
    }

    public interface TestDetailListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmFinishActivity();

        void vmNavigateToSearch();

        void vmNavigateToPatientDetails(int position);

        Context getContext();

        void vmChangeAddress();
    }

}
