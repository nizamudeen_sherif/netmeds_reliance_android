package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.OrderHistoryItemBinding;

import java.util.List;
import java.util.Locale;


public class DiagnosticOrderItemAdapter extends RecyclerView.Adapter<DiagnosticOrderItemAdapter.OrderItemHolder> {
    private List<Test> orderDetailList;
    private Context mContext;
    private boolean isShowPriceInfo;

    public DiagnosticOrderItemAdapter(Context context, List<Test> orderDetailList, boolean isShowPriceInfo) {
        this.mContext = context;
        this.orderDetailList = orderDetailList;
        this.isShowPriceInfo = isShowPriceInfo;
    }

    @NonNull
    @Override
    public OrderItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        OrderHistoryItemBinding historyItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.order_history_item, viewGroup, false);
        return new OrderItemHolder(historyItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Test orderDetail = orderDetailList.get(position);
        PriceInfo priceInfo = orderDetail.getPriceInfo() != null ? orderDetail.getPriceInfo() : new PriceInfo();
        String itemName = !TextUtils.isEmpty(orderDetail.getTestName()) ? orderDetail.getTestName() : "";
        holder.orderHistoryItemBinding.textName.setText(isShowPriceInfo ? itemName : position > 1 ? mContext.getResources().getQuantityString(R.plurals.text_test_count, (orderDetailList.size() - position), (orderDetailList.size() - position)) : itemName);
        holder.orderHistoryItemBinding.textName.setVisibility(TextUtils.isEmpty(orderDetail.getTestName()) ? View.GONE : View.VISIBLE);
        holder.orderHistoryItemBinding.textPrice.setText(!TextUtils.isEmpty(priceInfo.getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceInfo.getFinalPrice()) : "");
        holder.orderHistoryItemBinding.textStrikePrice.setText(!TextUtils.isEmpty(priceInfo.getOfferedPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceInfo.getOfferedPrice()) : "");
        holder.orderHistoryItemBinding.textStrikePrice.setPaintFlags(holder.orderHistoryItemBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.orderHistoryItemBinding.textPrice.setVisibility(TextUtils.isEmpty(priceInfo.getFinalPrice()) ? View.GONE : View.VISIBLE);
        holder.orderHistoryItemBinding.textStrikePrice.setVisibility(TextUtils.isEmpty(priceInfo.getOfferedPrice()) ? View.GONE : View.VISIBLE);
        holder.orderHistoryItemBinding.lytPriceDetail.setVisibility(isShowPriceInfo ? View.VISIBLE : View.GONE);
        holder.orderHistoryItemBinding.orderItemHorizontalView.setVisibility(isShowPriceInfo ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return isShowPriceInfo ? orderDetailList.size() : orderDetailList.size() > 3 ? 3 : orderDetailList.size();
    }

    class OrderItemHolder extends RecyclerView.ViewHolder {
        private OrderHistoryItemBinding orderHistoryItemBinding;

        OrderItemHolder(@NonNull OrderHistoryItemBinding orderHistoryItemBinding) {
            super(orderHistoryItemBinding.getRoot());
            this.orderHistoryItemBinding = orderHistoryItemBinding;
        }
    }
}