package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.font.LatoTextView;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.ui.DiagnosticIncludedTestsFragment;
import com.nms.netmeds.diagnostic.ui.DiagnosticPackageDescriptionFragment;

public class DiagnosticPackageDescriptionFragmentAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] tabTitles;

    public DiagnosticPackageDescriptionFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        tabTitles = new String[]{mContext.getString(R.string.description_title), mContext.getString(R.string.included_test)};
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) return new DiagnosticPackageDescriptionFragment();
        else return new DiagnosticIncludedTestsFragment();
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    public View getTabView(int position) {
        LatoTextView titleView = (LatoTextView) LayoutInflater.from(mContext).inflate(R.layout.diagnostic_package_fragment_tab_title_layout, null);
        titleView.setText(tabTitles[position]);
        if (position == 0)
            CommonUtils.setBoldFace(titleView, mContext);
        else CommonUtils.setRegularFace(titleView, mContext);
        titleView.setGravity(Gravity.CENTER);
        return titleView;
    }


}
