package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityPatientDetailsBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.TimeSlotRequest;
import com.nms.netmeds.diagnostic.model.TimeSlotResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiagnosticPatientDetailsViewModel extends AppViewModel {
    private ActivityPatientDetailsBinding patientDetailsBinding;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private boolean isMaleChecked = false;
    private boolean isFemaleChecked = false;
    private double totalAmount = 0;
    private DiagnosticPatientDetailsListener patientDetailsListener;
    private AvailableLab availableLab;
    private PatientDetail patientDetail;
    private DiagnosticPackage selectedPackage;
    private String diagnosticType = "";
    private MutableLiveData<TimeSlotResponse> timeSlotMutableLiveData = new MutableLiveData<>();
    private TimeSlotResponse timeSlotResponse;

    public TimeSlotResponse getTimeSlotResponse() {
        return timeSlotResponse;
    }

    public void setTimeSlotResponse(TimeSlotResponse timeSlotResponse) {
        this.timeSlotResponse = timeSlotResponse;
    }

    public String getDiagnosticType() {
        return diagnosticType;
    }

    public void setDiagnosticType(String diagnosticType) {
        this.diagnosticType = diagnosticType;
    }

    public DiagnosticPackage getSelectedPackage() {
        return selectedPackage;
    }

    public void setSelectedPackage(DiagnosticPackage selectedPackage) {
        this.selectedPackage = selectedPackage;
    }

    public AvailableLab getAvailableLab() {
        return availableLab;
    }

    public PatientDetail getPatientDetail() {
        return patientDetail;
    }

    public void setPatientDetail(PatientDetail patientDetail) {
        this.patientDetail = patientDetail;
    }

    public void setAvailableLab(AvailableLab availableLab) {
        this.availableLab = availableLab;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public DiagnosticPatientDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityPatientDetailsBinding patientDetailsBinding, DiagnosticPatientDetailsListener patientDetailsListener) {
        this.mContext = context;
        this.patientDetailsBinding = patientDetailsBinding;
        this.patientDetailsListener = patientDetailsListener;
        setAsteriskSymbol();
        setErrorWatcher();
        initListener();
        setUserDetails();
        setBottomView();
    }

    private void setAsteriskSymbol() {
        patientDetailsBinding.diagnosticPatientNameLayout.setHint(Html.fromHtml(mContext.getResources().getString(R.string.text_diagnostic_patient_name) + DiagnosticConstant.ASTERISK_SYMBOL));
        patientDetailsBinding.diagnosticMobileNumberLayout.setHint(Html.fromHtml(mContext.getResources().getString(R.string.text_patient_mobile_no) + DiagnosticConstant.ASTERISK_SYMBOL));
        patientDetailsBinding.diagnosticAgeLayout.setHint(Html.fromHtml(mContext.getResources().getString(R.string.text_patient_age) + DiagnosticConstant.ASTERISK_SYMBOL));
        patientDetailsBinding.diagnosticEmailLayout.setHint(Html.fromHtml(mContext.getResources().getString(R.string.text_patient_email) + DiagnosticConstant.ASTERISK_SYMBOL));
        patientDetailsBinding.textGender.setText(String.format("%s%s", patientDetailsListener.getContext().getResources().getString(R.string.text_patient_gender), DiagnosticConstant.ASTERISK_SYMBOL));
    }

    private void setBottomView() {
        patientDetailsBinding.testCount.lytTestCount.setVisibility(View.VISIBLE);
        if (getDiagnosticType().equalsIgnoreCase(DiagnosticConstant.TEST))
            setBottomViewTest();
        else
            setBottomViewPackage();
    }

    private void setBottomViewTest() {
        if (getAvailableLab() != null && getAvailableLab().getPriceDescription() != null) {
            PriceDescription priceDescription = getAvailableLab().getPriceDescription();
            if (!TextUtils.isEmpty(priceDescription.getFinalPrice()))
                patientDetailsBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, Double.parseDouble(priceDescription.getFinalPrice())));
            patientDetailsBinding.testCount.textLabel.setText(mContext.getResources().getString(R.string.text_total_amount));
        }
    }

    private void setBottomViewPackage() {
        if (getSelectedPackage() != null && getSelectedPackage().getPriceDescription() != null) {
            PriceDescription priceDescription = getSelectedPackage().getPriceDescription();
            if (!TextUtils.isEmpty(priceDescription.getFinalPrice()))
                patientDetailsBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, Double.parseDouble(priceDescription.getFinalPrice())));
            patientDetailsBinding.testCount.textLabel.setText(mContext.getResources().getString(R.string.text_total_amount));
        }
    }

    private void setUserDetails() {
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(mContext).getDiagnosticLoginResponse(), JustDocUserResponse.class);
        String firstName = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getName()) ? diagnosticLoginResult.getName() : "";
        String email = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getEmail()) ? diagnosticLoginResult.getEmail() : "";

        String phoneNo = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getMobile()) ? diagnosticLoginResult.getMobile() : "";
        String age = BasePreference.getInstance(mContext).getPatientAge();
        String gender = BasePreference.getInstance(mContext).getPatientGender();
        patientDetailsBinding.diagnosticAge.setText(age);
        if(!TextUtils.isEmpty(gender)) {
            patientDetailsBinding.textGender.setText(gender);
        }
        patientDetailsBinding.diagnosticPatientName.setText(firstName);
        patientDetailsBinding.diagnosticMobileNumber.setText(phoneNo);
        patientDetailsBinding.diagnosticPatientEmail.setText(email);

        if (TextUtils.isEmpty(gender)) return;
        if (DiagnosticConstant.KEY_MALE.equals(gender)) {
            setFemaleUnChecked();
            if (isMaleChecked)
                setMaleUnChecked();
            else
                setMaleChecked();
        } else {
            setMaleUnChecked();
            if (isFemaleChecked)
                setFemaleUnChecked();
            else
                setFemaleChecked();
        }
    }

    private void initListener() {
        patientDetailsBinding.textMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            patientDetailsBinding.textFeMale.setBackgroundDrawable(ContextCompat.getDrawable(patientDetailsListener.getContext(), R.drawable.color_grey_curved_rectangle));
            patientDetailsBinding.textMale.setBackgroundDrawable(ContextCompat.getDrawable(patientDetailsListener.getContext(), R.drawable.color_grey_curved_rectangle));
            patientDetailsBinding.textGender.setText(patientDetailsListener.getContext().getString(R.string.text_gender_male));
            patientDetailsBinding.textGender.setTextColor(ContextCompat.getColor(patientDetailsListener.getContext(), R.color.colorAccent));
                setFemaleUnChecked();
                if (isMaleChecked)
                    setMaleUnChecked();
                else
                    setMaleChecked();
            }
        });
        patientDetailsBinding.textFeMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patientDetailsBinding.textFeMale.setBackgroundDrawable(ContextCompat.getDrawable(patientDetailsListener.getContext(), R.drawable.color_grey_curved_rectangle));
                patientDetailsBinding.textMale.setBackgroundDrawable(ContextCompat.getDrawable(patientDetailsListener.getContext(), R.drawable.color_grey_curved_rectangle));
                patientDetailsBinding.textGender.setText(patientDetailsListener.getContext().getString(R.string.text_gender_female));
                patientDetailsBinding.textGender.setTextColor(ContextCompat.getColor(patientDetailsListener.getContext(), R.color.colorAccent));
                setMaleUnChecked();
                if (isFemaleChecked)
                    setFemaleUnChecked();
                else
                    setFemaleChecked();
            }
        });
        patientDetailsBinding.testCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePatientDetail()) {
                    setPatientDetail(generatePatientDetail());
                    patientDetailsListener.vmInitAddressNavigation();
                }
            }
        });
        patientDetailsBinding.testCount.lytSelectedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.hideKeyboard(mContext, patientDetailsBinding.testCount.lytSelectedTest);
                List<Test> testList = null;
                if (getDiagnosticType().equalsIgnoreCase(DiagnosticConstant.TEST))
                    testList = getAvailableLab() != null && getAvailableLab().getTestList() != null && getAvailableLab().getTestList().size() > 0 ? getAvailableLab().getTestList() : new ArrayList<Test>();
                else
                    testList = getSelectedPackage() != null && getSelectedPackage().getTestList() != null && getSelectedPackage().getTestList().size() > 0 ? getSelectedPackage().getTestList() : new ArrayList<Test>();
                patientDetailsListener.vmInitBottomView(testList);
            }
        });
    }

    private void setErrorWatcher() {
        CommonUtils.errorWatcher(patientDetailsBinding.diagnosticPatientName, patientDetailsBinding.diagnosticPatientNameLayout);
        CommonUtils.errorWatcher(patientDetailsBinding.diagnosticMobileNumber, patientDetailsBinding.diagnosticMobileNumberLayout);
        CommonUtils.errorWatcher(patientDetailsBinding.diagnosticPatientEmail, patientDetailsBinding.diagnosticEmailLayout);
        CommonUtils.errorWatcher(patientDetailsBinding.diagnosticAge, patientDetailsBinding.diagnosticAgeLayout);
    }

    private void setMaleChecked() {
        isMaleChecked = true;
        patientDetailsBinding.textMale.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.accent_curved_rectangle));
        patientDetailsBinding.textMale.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    }

    private void setMaleUnChecked() {
        isMaleChecked = false;
        patientDetailsBinding.textMale.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.color_grey_curved_rectangle));
        patientDetailsBinding.textMale.setTextColor(mContext.getResources().getColor(R.color.colorDarkBlueGrey));
    }

    private void setFemaleChecked() {
        isFemaleChecked = true;
        patientDetailsBinding.textFeMale.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.accent_curved_rectangle));
        patientDetailsBinding.textFeMale.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    }

    private void setFemaleUnChecked() {
        isFemaleChecked = false;
        patientDetailsBinding.textFeMale.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.color_grey_curved_rectangle));
        patientDetailsBinding.textFeMale.setTextColor(mContext.getResources().getColor(R.color.colorDarkBlueGrey));
    }

    private boolean validatePatientDetail() {
        if (TextUtils.isEmpty(patientDetailsBinding.diagnosticPatientName.getText().toString().trim())) {
            patientDetailsBinding.diagnosticPatientNameLayout.setError(mContext.getString(R.string.err_msg_patient_name));
            return false;
        } else if (patientDetailsBinding.diagnosticPatientName.getText().toString().length() < 3) {
            patientDetailsBinding.diagnosticPatientNameLayout.setError(mContext.getString(R.string.err_msg_patient_name_length));
            return false;
        } else if (TextUtils.isEmpty(patientDetailsBinding.diagnosticMobileNumber.getText())) {
            patientDetailsBinding.diagnosticMobileNumberLayout.setError(mContext.getString(R.string.err_msg_patient_mobile));
            return false;
        } else if (patientDetailsBinding.diagnosticMobileNumber.getText().toString().length() != 10) {
            patientDetailsBinding.diagnosticMobileNumberLayout.setError(mContext.getString(R.string.err_msg_patient_mobile_length));
            return false;
        } else if (TextUtils.isEmpty(patientDetailsBinding.diagnosticAge.getText().toString().trim())) {
            patientDetailsBinding.diagnosticAgeLayout.setError(mContext.getString(R.string.err_msg_patient_age));
            return false;
        } else if (Integer.parseInt(patientDetailsBinding.diagnosticAge.getText().toString()) <= 0) {
            patientDetailsBinding.diagnosticAgeLayout.setError(mContext.getString(R.string.err_msg_patient_valid_age));
            return false;
        } else if (!isMaleChecked && !isFemaleChecked) {
            patientDetailsBinding.textMale.setBackgroundDrawable(ContextCompat.getDrawable(patientDetailsListener.getContext(), R.drawable.color_red_curved_rectangle));
            patientDetailsBinding.textFeMale.setBackgroundDrawable(ContextCompat.getDrawable(patientDetailsListener.getContext(), R.drawable.color_red_curved_rectangle));
            patientDetailsBinding.textGender.setTextColor(ContextCompat.getColor(patientDetailsListener.getContext(), R.color.colorRed));
            patientDetailsBinding.textGender.setText(patientDetailsListener.getContext().getString(R.string.err_msg_patient_gender));
            CommonUtils.hideKeyboard(mContext, patientDetailsBinding.lytViewContent);
            return false;
        } else return validateEmailAddress();
    }

    private boolean validateEmailAddress() {
        return ValidationUtils.checkEmailValidation(patientDetailsBinding.diagnosticPatientEmail, patientDetailsBinding.diagnosticEmailLayout, mContext);
    }

    public interface DiagnosticPatientDetailsListener {
        void vmInitBottomView(List<Test> list);

        void vmInitAddressNavigation();

        Context getContext();
    }

    private PatientDetail generatePatientDetail() {
        PatientDetail patientDetail = new PatientDetail();
        patientDetail.setName(patientDetailsBinding.diagnosticPatientName.getText().toString());
        patientDetail.setTelephone(patientDetailsBinding.diagnosticMobileNumber.getText().toString());
        patientDetail.setEmail(patientDetailsBinding.diagnosticPatientEmail.getText().toString());
        String age = patientDetailsBinding.diagnosticAge.getText().toString();
        patientDetail.setAge(age.trim().length() == 1 ? String.format(Locale.getDefault(), "%02d", Integer.parseInt(age)) : age);
        patientDetail.setGender(isMaleChecked ? mContext.getResources().getString(R.string.text_gender_male) : isFemaleChecked ? mContext.getResources().getString(R.string.text_gender_female) : "");
        BasePreference.getInstance(mContext).setPatientAge(age);
        BasePreference.getInstance(mContext).setPatientGender(isMaleChecked ? mContext.getResources().getString(R.string.text_gender_male) : isFemaleChecked ? mContext.getResources().getString(R.string.text_gender_female) : "");
        return patientDetail;
    }

    public void onBottomNavigation() {
        if (validatePatientDetail()) {
            setPatientDetail(generatePatientDetail());
            patientDetailsListener.vmInitAddressNavigation();
        }
    }

    public MutableLiveData<TimeSlotResponse> getTimeSlotMutableLiveData() {
        return timeSlotMutableLiveData;
    }

    private String getDiagnosticAuthToken() {
        if(BasePreference.getInstance(patientDetailsListener.getContext()) == null) return "";
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(patientDetailsListener.getContext()).getDiagnosticLoginResponse(), JustDocUserResponse.class);
        if(diagnosticLoginResult == null || TextUtils.isEmpty(diagnosticLoginResult.getToken())) return "";
        return diagnosticLoginResult.getToken();
    }

    private TimeSlotRequest timeSlotRequest() {
        TimeSlotRequest timeSlotRequest = new TimeSlotRequest();
        timeSlotRequest.setPinCode(BasePreference.getInstance(patientDetailsListener.getContext()).getDiagnosticPinCode());
        if (DiagnosticHelper.isPathoTestExist()) {
            timeSlotRequest.setLabId(getLabId());
            timeSlotRequest.setcId(0);
            timeSlotRequest.setCategory(DiagnosticConstant.CATEGORY_PATH);
        } else {
            timeSlotRequest.setLabId(getLabPid());
            timeSlotRequest.setcId(getLabId());
            timeSlotRequest.setCategory(DiagnosticConstant.CATEGORY_RADIO);
        }
        ArrayList<String> testIds = new ArrayList<>();
        if (availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0) {
            for (Test test : availableLab.getTestList()) {
                testIds.add(test.getTestId());
            }
        }
        timeSlotRequest.setTestids(testIds);
        return timeSlotRequest;
    }

    private int getLabId() {
        int labId = 0;
        if (getDiagnosticType().equals(DiagnosticConstant.TEST))
            labId = availableLab != null && availableLab.getLabDescription() != null ? availableLab.getLabDescription().getId() : 0;
        else
            labId = getSelectedPackage() != null && getSelectedPackage().getLabDescription() != null ? getSelectedPackage().getLabDescription().getId() : 0;
        return labId;
    }

    private int getLabPid() {
        if (availableLab == null || availableLab.getLabDescription() == null) return 0;
        return availableLab.getLabDescription().getPid();
    }

}
