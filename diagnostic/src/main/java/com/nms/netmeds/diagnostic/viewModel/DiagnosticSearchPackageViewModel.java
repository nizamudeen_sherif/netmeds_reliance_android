package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.PackageAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivitySearchPackageBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResult;
import com.nms.netmeds.diagnostic.model.Request.SearchRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiagnosticSearchPackageViewModel extends AppViewModel implements PackageAdapter.PackageAdapterListener {
    private int failedTransactionId = 0;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivitySearchPackageBinding searchPackageBinding;
    private DiagnosticSearchPackageViewModel searchPackageViewModel;
    private DiagnosticSearchPackageListener searchPackageListener;
    private PackageAdapter mPackageAdapter;
    private List<DiagnosticPackage> packageList;
    private static final String TAG_SEARCH = "SEARCH";
    private static final String TAG_CLEAR = "CLEAR";
    private MutableLiveData<DiagnosticPackageResponse> searchPackageMutableLiveData = new MutableLiveData<>();
    private DiagnosticPackage selectedPackage;
    private boolean isSearchPackageApiResponse = false;
    private List<DiagnosticPackage> popularPackageList;

    public List<DiagnosticPackage> getPopularPackageList() {
        return popularPackageList;
    }

    public void setPopularPackageList(List<DiagnosticPackage> popularPackageList) {
        this.popularPackageList = popularPackageList;
    }

    public DiagnosticPackage getSelectedPackage() {
        return selectedPackage;
    }

    public void setSelectedPackage(DiagnosticPackage selectedPackage) {
        this.selectedPackage = selectedPackage;
    }

    public DiagnosticSearchPackageViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivitySearchPackageBinding searchPackageBinding, DiagnosticSearchPackageViewModel searchPackageViewModel, DiagnosticSearchPackageListener searchPackageListener) {
        this.context = context;
        this.searchPackageBinding = searchPackageBinding;
        this.searchPackageViewModel = searchPackageViewModel;
        this.searchPackageListener = searchPackageListener;
        packageList = new ArrayList<>();
        if (getPopularPackageList() != null)
            packageList.addAll(getPopularPackageList());
        initPackageAdapter();
        initListener();
    }

    private void initListener() {
        searchPackageBinding.packageCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPackageListener.vmPatientDetailCallback();
            }
        });

        searchPackageBinding.packageCount.lytSelectedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Test> testList = getSelectedPackage() != null && getSelectedPackage().getTestList() != null && getSelectedPackage().getTestList().size() > 0 ? getSelectedPackage().getTestList() : new ArrayList<Test>();
                searchPackageListener.vmInitBottomView(testList);
            }
        });
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == DiagnosticServiceManager.SEARCH_PACKAGE) {
            isSearchPackageApiResponse = false;
            DiagnosticPackageResponse diagnosticPackageResponse = new Gson().fromJson(data, DiagnosticPackageResponse.class);
            searchPackageMutableLiveData.setValue(diagnosticPackageResponse);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        searchPackageListener.vmDismissProgress();
        if (transactionId == DiagnosticServiceManager.SEARCH_PACKAGE) {
            vmOnError(transactionId);
        }
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == DiagnosticServiceManager.SEARCH_PACKAGE) {
            getPackage(searchPackageBinding.edtFilter.getText().toString());
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        searchPackageBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        searchPackageBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
        searchPackageBinding.searchPackageEmptyView.setVisibility(View.GONE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        searchPackageBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        searchPackageBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
        searchPackageBinding.searchPackageEmptyView.setVisibility(View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    public MutableLiveData<DiagnosticPackageResponse> getSearchPackageMutableLiveData() {
        return searchPackageMutableLiveData;
    }

    private void setBottomView() {
        if (!TextUtils.isEmpty(getTotalAmount())) {
            searchPackageBinding.packageCount.lytTestCount.setVisibility(View.VISIBLE);
            searchPackageBinding.packageCount.textTestCount.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, getTotalAmount()));
            searchPackageBinding.packageCount.textLabel.setText(context.getResources().getString(R.string.text_total_amount));
        } else
            searchPackageBinding.packageCount.lytTestCount.setVisibility(View.GONE);
    }

    private String getTotalAmount() {
        return getSelectedPackage() != null && getSelectedPackage().getPriceDescription() != null && !TextUtils.isEmpty(getSelectedPackage().getPriceDescription().getFinalPrice()) ? getSelectedPackage().getPriceDescription().getFinalPrice() : "";
    }

    @Override
    public void onPackageChecked(DiagnosticPackage diagnosticPackage) {
        setSelectedPackage(diagnosticPackage);
        setBottomView();
    }

    @Override
    public void onPackageUnChecked() {
        setSelectedPackage(new DiagnosticPackage());
        setBottomView();
    }

    public interface DiagnosticSearchPackageListener {

        void vmShowProgress();

        void vmDismissProgress();

        void vmInitBottomView(List<Test> testList);

        void vmPatientDetailCallback();
    }

    private void initPackageAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        searchPackageBinding.packageListView.setLayoutManager(linearLayoutManager);
        searchPackageBinding.packageListView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        searchPackageBinding.packageListView.setNestedScrollingEnabled(false);
        mPackageAdapter = new PackageAdapter(context, packageList, PackageAdapter.SEARCH_PACKAGE, this);
        searchPackageBinding.packageListView.setAdapter(mPackageAdapter);
    }

    public void onFilterQueryChanged(final CharSequence s) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(s)) {
                    if (!isSearchPackageApiResponse)
                        getPackage(s);
                } else
                    setDefaultPackageList();
            }
        }, 800);
        searchPackageBinding.imgSearch.setImageDrawable(ContextCompat.getDrawable(context, s.toString().isEmpty() ? R.drawable.ic_search : R.drawable.ic_clear));
        searchPackageBinding.imgSearch.setTag(!TextUtils.isEmpty(s.toString()) ? TAG_CLEAR : TAG_SEARCH);
    }

    public void onClear(View v) {
        String tag = v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString()) ? v.getTag().toString() : "";
        if (tag.equals(TAG_CLEAR)) {
            searchPackageBinding.edtFilter.setText("");
            searchPackageBinding.imgSearch.setTag(TAG_SEARCH);
            CommonUtils.hideKeyboard(context, searchPackageBinding.lytViewContent);
            setDefaultPackageList();
        }
    }

    private void setDefaultPackageList() {
        if (searchPackageBinding.searchPackageEmptyView.getVisibility() == View.VISIBLE && getPopularPackageList() != null) {
            packageList.addAll(getPopularPackageList());
            mPackageAdapter.updatePackageAdapter(getPopularPackageList(), PackageAdapter.SEARCH_PACKAGE);
            searchPackageBinding.searchPackageEmptyView.setVisibility(View.GONE);
            searchPackageBinding.cardViewPackage.setVisibility(View.VISIBLE);
        }
    }

    private void getPackage(CharSequence s) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            isSearchPackageApiResponse = true;
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setPinCode(BasePreference.getInstance(context).getDiagnosticPinCode());
            searchRequest.setQuery(s.toString());
            searchRequest.setType(DiagnosticConstant.PACKAGE.toLowerCase());
            DiagnosticServiceManager.getInstance().getDiagnosticPackage(searchPackageViewModel, searchRequest,BasePreference.getInstance(context));
        } else
            failedTransactionId = DiagnosticServiceManager.SEARCH_PACKAGE;
    }

    public void searchPackageDataAvailable(DiagnosticPackageResponse packageResponse) {
        if (packageResponse != null) {
            searchServiceStatus(packageResponse);
        } else
            vmOnError(DiagnosticServiceManager.SEARCH_PACKAGE);

    }

    private void searchServiceStatus(DiagnosticPackageResponse packageResponse) {
        if (packageResponse.getServiceStatus() != null && packageResponse.getServiceStatus().getStatusCode() != null && packageResponse.getServiceStatus().getStatusCode() == 200) {
            packageResult(packageResponse);
        } else
            setPackageError(packageResponse);
    }

    private void setPackageError(DiagnosticPackageResponse packageResponse) {
        if (packageResponse.getDiagnosticPackageResult() != null && !TextUtils.isEmpty(packageResponse.getDiagnosticPackageResult().getMessage())) {
            searchPackageBinding.testEmpty.setText(packageResponse.getDiagnosticPackageResult().getMessage());
            searchPackageBinding.searchPackageEmptyView.setVisibility(View.VISIBLE);
            searchPackageBinding.cardViewPackage.setVisibility(View.GONE);
        }
    }

    private void packageResult(DiagnosticPackageResponse packageResponse) {
        if (packageResponse.getDiagnosticPackageResult() != null) {
            setPackageAdapter(packageResponse.getDiagnosticPackageResult());
        } else
            vmOnError(DiagnosticServiceManager.SEARCH_PACKAGE);
    }

    private void setPackageAdapter(DiagnosticPackageResult packageResult) {
        if (packageResult.getPackageList() != null && packageResult.getPackageList().size() > 0) {
            packageList.clear();
            packageList.addAll(packageResult.getPackageList());
            mPackageAdapter.updatePackageAdapter(packageList, PackageAdapter.SEARCH_PACKAGE);
            searchPackageBinding.cardViewPackage.setVisibility(View.VISIBLE);
            searchPackageBinding.searchPackageEmptyView.setVisibility(View.GONE);
        } else {
            searchPackageBinding.cardViewPackage.setVisibility(View.GONE);
            searchPackageBinding.searchPackageEmptyView.setVisibility(View.VISIBLE);
            searchPackageBinding.testEmpty.setText(context.getResources().getString(R.string.err_msg_search_package));
        }
    }
}
