package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Accreditation implements Serializable {

    @SerializedName("logo")
    private String logo;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
