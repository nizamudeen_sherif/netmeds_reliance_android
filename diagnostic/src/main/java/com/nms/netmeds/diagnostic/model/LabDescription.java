package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LabDescription implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("cId")
    private int cId;
    @SerializedName("labName")
    private String labName;
    @SerializedName("logo")
    private String logo;
    @SerializedName("short_Details")
    private String shortDetails;
    @SerializedName("description")
    private String description;
    @SerializedName("rating")
    private String rating;
    @SerializedName("rating_count")
    private String rating_count;
    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("labAddress")
    private List<LabAddress> labAddressList;
    @SerializedName("accreditation")
    private List<Accreditation> accreditation;
    @SerializedName("pid")
    private int pid;
    @SerializedName("extras")
    private Extra extras;
    @SerializedName("payMode")
    private String payMode;

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public Extra getExtras() {
        return extras;
    }

    public void setExtras(Extra extras) {
        this.extras = extras;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public List<Accreditation> getAccreditation() {
        return accreditation;
    }

    public void setAccreditation(List<Accreditation> accreditation) {
        this.accreditation = accreditation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShortDetails() {
        return shortDetails;
    }

    public void setShortDetails(String shortDetails) {
        this.shortDetails = shortDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating_count() {
        return rating_count;
    }

    public void setRating_count(String rating_count) {
        this.rating_count = rating_count;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<LabAddress> getLabAddressList() {
        return labAddressList;
    }

    public void setLabAddressList(List<LabAddress> labAddressList) {
        this.labAddressList = labAddressList;
    }
}
