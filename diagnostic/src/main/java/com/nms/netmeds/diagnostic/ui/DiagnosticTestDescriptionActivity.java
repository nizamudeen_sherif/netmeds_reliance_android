package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticTestDescriptionBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTestDescriptionViewModel;

import java.util.ArrayList;

/**
 * Test Description Activity
 */
public class DiagnosticTestDescriptionActivity extends BaseViewModelActivity<DiagnosticTestDescriptionViewModel> implements DiagnosticTestDescriptionViewModel.TestDetailListener, LocationFragment.LocationFragmentListener {
    private ActivityDiagnosticTestDescriptionBinding testDescriptionBinding;
    private DiagnosticTestDescriptionViewModel viewModel;
    private View.OnClickListener backListener;
    private Test selectedTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intentValue();
        testDescriptionBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_test_description);
        testDescriptionBinding.setViewModel(onCreateViewModel());
        onRetry(viewModel);
        setBackListener();
        testDescriptionBinding.arrowBack.setOnClickListener(backListener);
    }

    private void intentValue() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM)) {
                ArrayList<String> arrayList = getIntent().getStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM);
                if (arrayList != null && arrayList.size() > 0 && !TextUtils.isEmpty(arrayList.get(0))) {
                    selectedTest = new Test();
                    selectedTest.setTestName("");
                    selectedTest.setUrl(arrayList.get(0));
                } else {
                    finish();
                }
            }
            if (getIntent().hasExtra(DiagnosticConstant.INTENT_TEST_SELECTED)) {
                selectedTest = (Test) getIntent().getSerializableExtra(DiagnosticConstant.INTENT_TEST_SELECTED);
                if (selectedTest != null && !TextUtils.isEmpty(selectedTest.getCategory())) {
                    if ((!selectedTest.getCategory().equals(DiagnosticConstant.CATEGORY_RADIO) || !DiagnosticHelper.isRadioTestExist()) &&
                            (!selectedTest.getCategory().equals(DiagnosticConstant.CATEGORY_PATH) || !DiagnosticHelper.isPathoTestExist())) {
                        DiagnosticHelper.clearSelectedList();
                    }
                }
            }
        }
    }

    protected DiagnosticTestDescriptionViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(DiagnosticTestDescriptionViewModel.class);
        viewModel.init(this, testDescriptionBinding, this, getIntent() != null && (getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM)));
        viewModel.setSelectedTest(selectedTest);
        viewModel.getPinCodeResponseMutableLiveData().observe(this, new PinCodeResponseObserver());
        viewModel.getTestDetails();
        return viewModel;
    }

    @Override
    public void vmShowProgress() {
        testDescriptionBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        testDescriptionBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void vmFinishActivity() {
        finish();
    }

    @Override
    public void vmNavigateToSearch() {
        Intent intent = new Intent();
        LaunchIntentManager.routeToActivity(DiagnosticTestDescriptionActivity.this.getResources().getString(R.string.route_diagnostic_search_test), intent, DiagnosticTestDescriptionActivity.this);
    }

    private PriceInfo getPriceInfo(PriceDescription priceDescription) {
        PriceInfo priceInfo = new PriceInfo();
        priceInfo.setDiscountAmount(priceDescription.getHc());
        priceInfo.setOfferedPrice(priceDescription.getOfferedPrice());
        priceInfo.setFinalPrice(priceDescription.getFinalPrice());
        priceInfo.setDiscountAmount(priceDescription.getDiscountAmount());
        priceInfo.setDiscountAmountInfo(priceDescription.getDiscountAmountInfo());
        priceInfo.setDiscountPercent(priceDescription.getDiscountPercent());
        return priceInfo;
    }

    @Override
    public void vmNavigateToPatientDetails(int position) {
        if (!viewModel.isLabAvailable() || !viewModel.isTestAvailable()) return;
        Test testToBook = viewModel.getTest();
        PriceDescription priceDescription = viewModel.testDetailResponse.getResult().getLabs().get(position).getPriceDescription();
        testToBook.setPriceInfo(getPriceInfo(priceDescription));
        Intent globalIntent = new Intent();
        if (testToBook.getType().equalsIgnoreCase(getResources().getString(R.string.txt_test_type))) {
            DiagnosticHelper.setTest(testToBook);
            ArrayList<Test> arrayList = new ArrayList<>(DiagnosticHelper.getSelectedList());
            globalIntent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, arrayList);
            LaunchIntentManager.routeToActivity(DiagnosticTestDescriptionActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), globalIntent, DiagnosticTestDescriptionActivity.this);
        } else if (testToBook.getType().equalsIgnoreCase(getResources().getString(R.string.txt_packages_type)) || testToBook.getType().equalsIgnoreCase(getResources().getString(R.string.txt_package_type)) || testToBook.getType().equalsIgnoreCase(getResources().getString(R.string.txt_profile_type))) {
            AvailableLab availableLab = new AvailableLab();
            availableLab.setTestList(viewModel.testDetailResponse.getResult().getTests());
            availableLab.setLabDescription(viewModel.testDetailResponse.getResult().getLabs().get(position).getLabDescription());
            availableLab.setPriceDescription(viewModel.testDetailResponse.getResult().getLabs().get(position).getPriceDescription());
            globalIntent.putExtra(DiagnosticConstant.KEY_SELECTED_LAB, availableLab);
            globalIntent.putExtra(DiagnosticConstant.KEY_INTENT_FROM, DiagnosticConstant.LAB_PAGE);
            LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_diagnostic_patient_detail), globalIntent, this);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    private void setBackListener() {
        backListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    @Override
    public void vmChangeAddress() {
        LocationFragment locationFragment = new LocationFragment(false);
        getSupportFragmentManager().beginTransaction().add(locationFragment, "Location_fragment").commitNowAllowingStateLoss();
    }

    @Override
    public void onPinCodeCheck(String pinCode) {
        if(isDiagnosticTestDescViewModelNotNull())
            viewModel.pinCodeServiceCheck(pinCode);
    }

    @Override
    public void onLocationPermission() {

    }

    @Override
    public void onLocationDialogClose() {

    }

    @Override
    public void onLocationServiceResponse() {
        if(isDiagnosticTestDescViewModelNotNull()) {
            viewModel.getTestDetails();
            viewModel.setPinCode();
        }
    }

    @Override
    public void onChangedDefaultAddress() {
        changeSelectedPinCode();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (BasePreference.getInstance(this).getChangeDiagnosticHome()) {
            changeSelectedPinCode();
        }
    }

    // check if viewModel is null
    private boolean isDiagnosticTestDescViewModelNotNull() {
        return viewModel!=null;
    }

    // Pin Code Response Observer
    public class PinCodeResponseObserver implements Observer<PinCodeResponse> {
        @Override
        public void onChanged(@Nullable PinCodeResponse pinCodeResponse) {
            viewModel.pinCodeDataAvailable(pinCodeResponse);
        }
    }

    // change selected pin code (either by entering manual or by selecting the address)
    private void changeSelectedPinCode() {
        BasePreference basePreference = BasePreference.getInstance(this);
        DefaultAddress address = new Gson().fromJson(basePreference.getDefaultAddress(), DefaultAddress.class);
        basePreference.setDiagnosticPinCode(TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())
                ? TextUtils.isEmpty(basePreference.getDiagnosticPinCode()) ? DiagnosticConstant.DEFAULT_PIN_CODE : basePreference.getDiagnosticPinCode() : address.getResult().getCustomer_details().getAddress().getPostcode());
        basePreference.setLabPinCode(TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())
                ? TextUtils.isEmpty(basePreference.getDiagnosticPinCode()) ? DiagnosticConstant.DEFAULT_PIN_CODE : basePreference.getDiagnosticPinCode() : address.getResult().getCustomer_details().getAddress().getPostcode());
        onLocationServiceResponse();
    }

}
