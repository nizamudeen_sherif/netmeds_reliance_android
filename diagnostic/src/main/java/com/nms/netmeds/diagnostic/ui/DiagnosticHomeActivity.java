package com.nms.netmeds.diagnostic.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticHomeBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResponse;
import com.nms.netmeds.diagnostic.model.OfferBannerResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PopularTestResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticHomeViewModel;
import com.webengage.sdk.android.WebEngage;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiagnosticHomeActivity extends BaseViewModelActivity<DiagnosticHomeViewModel> implements DiagnosticHomeViewModel.DiagnosticHomeListener, LocationFragment.LocationFragmentListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener, LocationListener {

    private ActivityDiagnosticHomeBinding homeBinding;
    private DiagnosticHomeViewModel homeViewModel;
    private GoogleApiClient mGoogleApiClient;
    private boolean isAlreadyRedirected = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // store initial object to avoid null - > default address
        if (TextUtils.isEmpty(BasePreference.getInstance(this).getDefaultAddress())) {
            DefaultAddress defaultAddress = new DefaultAddress();
            BasePreference.getInstance(this).saveDefaultAddress(new Gson().toJson(defaultAddress));
        }

        // handle app link depp link
        if (getIntent() != null) {
            if (getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM) && isPinCodeAvailable()) {
                isFromAppLinkOrDeepLink();
            }
            if (getIntent().hasExtra(DiagnosticConstant.KEY_LAB_TEST_FROM_CONSULTATION) && isPinCodeAvailable()) {
                isFromConsultationAndRedirect();
            }
        }

        homeBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_home);
        toolBarSetUp(homeBinding.toolbar);
        initGoogleApiClient();
        homeViewModel = onCreateViewModel();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        initDiagnosticLogin();
    }

    // logout user forcefully
    public void logoutUserForceFully() {
        pushUnRegisterFromConsultation();
        WebEngageHelper.getInstance().getUser().logout();
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
        CommonUtils.redirectToSignIn(this, false);
        BasePreference.getInstance(this).clear();
        BasePreference.getInstance(this).setClearPreference(true);
    }

    private void pushUnRegisterFromConsultation() {
        if(TextUtils.isEmpty(BasePreference.getInstance(this).getJustDocUserResponse())) return;
        boolean isConnected = NetworkUtils.isConnected(this);
        if (isConnected) {
            MStarCustomerDetails mStarCustomerDetails = new Gson().fromJson(BasePreference.getInstance(this).getCustomerDetails(), MStarCustomerDetails.class);
            SendFcmTokenRequest sendFcmTokenRequest = new SendFcmTokenRequest();
            sendFcmTokenRequest.setEmail(mStarCustomerDetails != null ? !TextUtils.isEmpty(mStarCustomerDetails.getEmail()) ? mStarCustomerDetails.getEmail() : "" : "");
            sendFcmTokenRequest.setToken(BasePreference.getInstance(this).getFcmToken());
            sendFcmTokenRequest.setUserType(ConsultationConstant.ROLE_USER);
            if (!TextUtils.isEmpty(BasePreference.getInstance(this).getFcmToken()))
                ConsultationServiceManager.getInstance().pushUnRegisterFromConsultationServer(sendFcmTokenRequest, BasePreference.getInstance(this));
        }
    }

    @Override
    public void finishMe() {
        DiagnosticHelper.clearSelectedList();
        this.finish();
    }

    @Override
    public void forceLogout() {
        logoutUserForceFully();
        return;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected DiagnosticHomeViewModel onCreateViewModel() {
        homeViewModel = ViewModelProviders.of(this).get(DiagnosticHomeViewModel.class);
        homeViewModel.init(this, homeBinding, homeViewModel, this);
        homeViewModel.getPopularTestMutableLiveData().observe(this, new PopularTestObserver());
        homeViewModel.getPinCodeResponseMutableLiveData().observe(this, new PinCodeResponseObserver());
        homeViewModel.getOfferBannerMutableLiveData().observe(this, new OfferBannerObserver());
        homeViewModel.getPopularPackageMutableLiveData().observe(this, new PopularPackageObserver());
        homeViewModel.getMostPopularPackageLiveData().observe(this, new MostPopularPackageObserver());
        homeViewModel.getLoginResponseMutableLiveData().observe(this, new LoginObserver());
        homeViewModel.getLoganMutableLiveData().observe(this, new LoganTokenObserver());
        homeViewModel.getDefaultAddressMutableLiveData().observe(this, new DefaultAddressObserver());
        onRetry(homeViewModel);
        return homeViewModel;
    }


    @Override
    public void navigateToSearch(String filter) {
        if (TextUtils.isEmpty(filter)) return;
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.INTENT_KEY_FILTER_TYPE, filter);
        LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_search_test), intent, DiagnosticHomeActivity.this);
    }

    @Override
    public void vmViewAllPackageCallback(List<DiagnosticPackage> popularPackageList) {
        Intent intent = new Intent();
        if (isPackageListAvailiable()) {
            intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_LIST, (Serializable) homeViewModel.getPopularPackageMutableLiveData().getValue().getDiagnosticPackageResult().getPackageList());
        }
        intent.putExtra(DiagnosticConstant.INTENT_KEY_FILTER_TYPE, getResources().getString(R.string.filter_type_health_package));
        intent.putExtra(DiagnosticConstant.INTENT_FROM_PACKAGE_SEARCH, true);
        LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_search_test), intent, DiagnosticHomeActivity.this);
    }

    @Override
    public void vmLocationDialog() {
        openLocationFragment();
    }

    @Override
    public void vmShowProgress() {
        showProgress(DiagnosticHomeActivity.this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    private boolean isPackageListAvailiable() {
        return homeViewModel.getPopularPackageMutableLiveData() != null && homeViewModel.getPopularPackageMutableLiveData().getValue() != null
                && homeViewModel.getPopularPackageMutableLiveData().getValue().getDiagnosticPackageResult() != null && homeViewModel.getPopularPackageMutableLiveData()
                .getValue().getDiagnosticPackageResult().getPackageList() != null && homeViewModel.getPopularPackageMutableLiveData()
                .getValue().getDiagnosticPackageResult().getPackageList().size() > 0;
    }


    @Override
    public void vmNavigateToSearch(List<Test> popularTestList, boolean flag) {
        Intent intent = new Intent();
        if (isPackageListAvailiable()) {
            intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_LIST, (Serializable) homeViewModel.getPopularPackageMutableLiveData().getValue().getDiagnosticPackageResult().getPackageList());
        }
        if (flag) {
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FILTER_TYPE, getResources().getString(R.string.filter_type_pathology));
        } else {
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FILTER_TYPE, getResources().getString(R.string.filter_type_all));
        }
        LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_search_test), intent, DiagnosticHomeActivity.this);
    }

    @Override
    public void vmNavigateToLab() {
        /*Diagnostic WebEngage Test Added Event*/
        WebEngageHelper.getInstance().diagnosticTestAddedEvent(getPreference(), DiagnosticHelper.getCity(), DiagnosticHelper.getSelectedList(), this);
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
        LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), intent, DiagnosticHomeActivity.this);
    }

    @Override
    public void vmInitBottomView() {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), DiagnosticHelper.getSelectedList(), DiagnosticHelper.getTestType(), DiagnosticConstant.HOME_PAGE, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, "Bottom_fragment").commitAllowingStateLoss();
    }

    @Override
    public void vmInitPinCodeView() {
        changeSelectedPinCode();
    }

    @Override
    public void vmNavigateToTestDetail(List<Test> popularTestList, Test test) {
        if (test == null || popularTestList == null || popularTestList.size() == 0) return;
        Intent intent = new Intent();
        if (test.getType().equalsIgnoreCase(getResources().getString(R.string.txt_test_type))) {
            intent.putExtra(DiagnosticConstant.INTENT_TEST_SELECTED, test);
            intent.putExtra(DiagnosticConstant.TEST, (Serializable) popularTestList);
            LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_test_detail), intent, DiagnosticHomeActivity.this);
        } else if (test.getType().equalsIgnoreCase(getResources().getString(R.string.txt_profile_type)) || test.getType().equalsIgnoreCase(getResources().getString(R.string.txt_packages_type))
                || test.getType().equalsIgnoreCase(getResources().getString(R.string.txt_package_type))) {
            final DiagnosticPackage diagnosticPackage = new DiagnosticPackage();
            ArrayList<Test> arrayList = new ArrayList<>();
            arrayList.add(test);
            diagnosticPackage.setTestList(arrayList);
            intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED, diagnosticPackage);
            LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_package_detail), intent, this);
        }
    }

    private void openLocationFragment() {
        LocationFragment locationFragment = new LocationFragment(true);
        getSupportFragmentManager().beginTransaction().add(locationFragment, "Location_fragment").commitAllowingStateLoss();
    }

    // change selected pin code (either by entering manual or by selecting the address)
    private void changeSelectedPinCode() {
        BasePreference basePreference = BasePreference.getInstance(this);
        DefaultAddress address = new Gson().fromJson(basePreference.getDefaultAddress(), DefaultAddress.class);
        getPreference().setDiagnosticPinCode(TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())
                ? TextUtils.isEmpty(getPreference().getDiagnosticPinCode()) ? DiagnosticConstant.DEFAULT_PIN_CODE : getPreference().getDiagnosticPinCode() : address.getResult().getCustomer_details().getAddress().getPostcode());
        getPreference().setLabPinCode(TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())
                ? TextUtils.isEmpty(getPreference().getDiagnosticPinCode()) ? DiagnosticConstant.DEFAULT_PIN_CODE : getPreference().getDiagnosticPinCode() : address.getResult().getCustomer_details().getAddress().getPostcode());
        onLocationServiceResponse();
        if (TextUtils.isEmpty(getPreference().getDiagnosticPinCode())) {
            openLocationFragment();
        }
        homeViewModel.showPinCode();
    }

    private boolean isPinCodeAvailable() {
        return !TextUtils.isEmpty(getPreference().getDiagnosticPinCode());
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(this);
    }

    @Override
    public void onPinCodeCheck(String pinCode) {
        homeViewModel.pinCodeServiceCheck(pinCode);
    }

    @Override
    public void onLocationPermission() {
        if (checkLocationPermission()) {
            locationRequest();
        } else
            requestPermission();
    }

    @Override
    public void onLocationDialogClose() {
        if (!isPinCodeAvailable())
            DiagnosticHomeActivity.this.finish();
    }

    @Override
    public void onLocationServiceResponse() {
        homeBinding.setViewModel(homeViewModel);
        homeViewModel.getPopularPackage();
        homeViewModel.getMostPopularPackage();
        if (getIntent() != null) {
            if (getIntent().hasExtra(DiagnosticConstant.KEY_LAB_TEST_FROM_CONSULTATION)) {
                isFromConsultationAndRedirect();
            }
            if (getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM)) {
                isFromAppLinkOrDeepLink();
            }
        }
    }

    @Override
    public void onChangedDefaultAddress() {
        changeSelectedPinCode();
    }

    private void isFromAppLinkOrDeepLink() {
        if (!isAlreadyRedirected) {
            Intent intent = new Intent();
            ArrayList<String> uriList = getIntent().getStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM);
            if (uriList != null && uriList.size() > 1 && !TextUtils.isEmpty(uriList.get(0)) && !TextUtils.isEmpty(uriList.get(1))) {
                if (uriList.get(1).equalsIgnoreCase(DiagnosticConstant.TEST_TYPE_TEST)) {
                    intent.putStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM, getIntent().getStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM));
                    LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_test_detail), intent, this);
                } else if (uriList.get(1).equalsIgnoreCase(getResources().getString(R.string.txt_profile_type)) || uriList.get(1).equalsIgnoreCase(getResources().getString(R.string.txt_packages_type))
                        || uriList.get(1).equalsIgnoreCase(getResources().getString(R.string.txt_package_type))) {
                    intent.putStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM, getIntent().getStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM));
                    LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_package_detail), intent, this);
                }
            }
        }
        isAlreadyRedirected = true;
    }

    private void isFromConsultationAndRedirect() {
        if(!isAlreadyRedirected) {
            Intent intent = new Intent();
            List<Test> selectedList = (List<Test>) getIntent().getSerializableExtra(DiagnosticConstant.KEY_LAB_TEST_FROM_CONSULTATION);
            intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) selectedList);
            LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), intent, DiagnosticHomeActivity.this);
            isAlreadyRedirected = true;
            finish();
        }
    }


    private boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PermissionConstants.LOCATION_PERMISSION);
    }

    private boolean checkPermissionResponse(int[] grantResults) {
        boolean permissionDenied = true;
        if (grantResults.length > 0) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED)
                    permissionDenied = false;
            }
        }
        return permissionDenied;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (checkPermissionResponse(grantResults)) {
            if (PermissionConstants.LOCATION_PERMISSION == requestCode)
                locationRequest();
        } else
            homeViewModel.onPermissionDenied();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClientConnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("Connection failed:", " ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void googleApiClientConnect() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    private void locationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(DiagnosticConstant.INTERVAL);
        locationRequest.setFastestInterval(DiagnosticConstant.FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        checkLocation(getLocation());
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(DiagnosticHomeActivity.this, DiagnosticConstant.REQUEST_CHECK_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public Location getLocation() {
        try {
            return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == DiagnosticConstant.REQUEST_CHECK_LOCATION_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    checkLocation(getLocation());
                    break;
                case Activity.RESULT_CANCELED:
                    homeViewModel.onPermissionDenied();
                    break;
                default:
                    break;
            }
        }
    }

    private void checkLocation(Location location) {
        if (location != null) {
            Address locationAddress = getAddress(location.getLatitude(), location.getLongitude());
            getPinCode(locationAddress);
        } else {
            locationRequest();
        }
    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
            return addressList.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getPinCode(Address locationAddress) {
        String pinCode = locationAddress != null && !TextUtils.isEmpty(locationAddress.getPostalCode()) ? locationAddress.getPostalCode() : "";
        homeViewModel.onCurrentLocationDataAvailable(pinCode);
    }

    @Override
    public void onNavigation() {
        vmNavigateToLab();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {
        homeViewModel.onUpdatePopularList(removeTest);
    }

    @Override
    public void onUpdateSearchList(Test removeTest) {

    }

    @Override
    public void onUpdateLabList() {

    }

    public class PopularTestObserver implements Observer<PopularTestResponse> {

        @Override
        public void onChanged(@Nullable PopularTestResponse popularTestResponse) {
            homeViewModel.onPopularTestDataAvailable(popularTestResponse);
        }
    }

    public class PinCodeResponseObserver implements Observer<PinCodeResponse> {

        @Override
        public void onChanged(@Nullable PinCodeResponse pinCodeResponse) {
            homeViewModel.pinCodeDataAvailable(pinCodeResponse);
        }
    }

    public class OfferBannerObserver implements Observer<OfferBannerResponse> {

        @Override
        public void onChanged(@Nullable OfferBannerResponse offerBannerResponse) {
            homeViewModel.offerBannerDataAvailable(offerBannerResponse);
        }
    }

    public class PopularPackageObserver implements Observer<DiagnosticPackageResponse> {

        @Override
        public void onChanged(@Nullable DiagnosticPackageResponse diagnosticPackageResponse) {
            homeViewModel.onPopularPackageDataAvailable(diagnosticPackageResponse);
        }
    }

    public class MostPopularPackageObserver implements Observer<DiagnosticPackageResponse> {

        @Override
        public void onChanged(@Nullable DiagnosticPackageResponse diagnosticPackageResponse) {
            homeViewModel.onMostPopularPackageDataAvailable(diagnosticPackageResponse);
        }
    }

    private class LoginObserver implements Observer<LoginResponse> {

        @Override
        public void onChanged(@Nullable LoginResponse loginResponse) {
            homeViewModel.diagnosticLoginDataAvailable(loginResponse);
        }
    }

    private class LoganTokenObserver implements Observer<LoganResponse> {

        @Override
        public void onChanged(@Nullable LoganResponse loganResponse) {
            homeViewModel.onLoganTokenDataAvailable(loganResponse);
        }
    }

    private class DefaultAddressObserver implements Observer<DefaultAddress> {
        @Override
        public void onChanged(@Nullable DefaultAddress loganResponse) {
            changeSelectedPinCode();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        homeViewModel.setLabTestCount();
        homeViewModel.updatePopularTestAdapter();
        if (BasePreference.getInstance(this).getChangeDiagnosticHome()) {
            changeSelectedPinCode();
            BasePreference.getInstance(this).setChangeDiagnosticHome(false);
        }
    }

    private void initDiagnosticLogin() {
        if (BasePreference.getInstance(this).isGuestCart()) {
            changeSelectedPinCode();
            return;
        }
        homeViewModel.setLoganSession(getPreference().getLoganSession());
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(getPreference().getDiagnosticLoginResponse(), JustDocUserResponse.class);
        String diagnosticToken = diagnosticLoginResult != null && diagnosticLoginResult.getToken() != null ? diagnosticLoginResult.getToken() : "";
        if (!TextUtils.isEmpty(diagnosticToken)) {
            BasePreference.getInstance(this).setDiaFirstTimeLogin(false);
            changeSelectedPinCode();
        } else {
            homeViewModel.doDiagnosticLogin();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DiagnosticHelper.clearSelectedList();
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_HOME_PAGE);
        googleApiClientConnect();
    }

    @Override
    public void vmNavigateDiagGuestUserToLabPage() {
        // redirect user to lab page after logan
        if (CommonUtils.isFirstTimeLoginToDiagnostic(DiagnosticHomeActivity.this) && DiagnosticHelper.getSelectedList() != null && DiagnosticHelper.getSelectedList().size() > 0) {
            final Intent intent = new Intent();
            intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
            LaunchIntentManager.routeToActivity(DiagnosticHomeActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), intent, DiagnosticHomeActivity.this);
            BasePreference.getInstance(DiagnosticHomeActivity.this).setDiaFirstTimeLogin(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    homeBinding.frameGuestUser.setVisibility(View.GONE);
                }
            }, DiagnosticConstant.DIAG_DELAY_FROM_HOME_TO_LAB);
        }
    }
}
