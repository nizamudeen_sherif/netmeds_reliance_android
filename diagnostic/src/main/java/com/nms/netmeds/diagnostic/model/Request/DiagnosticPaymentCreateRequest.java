package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.diagnostic.model.DiagnosticPaymentGatewayResponse;

public class DiagnosticPaymentCreateRequest {
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("pg")
    private String pg;
    @SerializedName("status")
    private String status;
    @SerializedName("amount")
    private double amount;
    @SerializedName("pg_response")
    private DiagnosticPaymentGatewayResponse paymentGatewayResponse;
    @SerializedName("userId")
    private String userId;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("nmscash")
    private boolean nmscash;
    @SerializedName("nmscash_amount")
    private double nmscash_amount;
    @SerializedName("paid_amount")
    private double paid_amount;

    public boolean isNmscash() {
        return nmscash;
    }

    public void setNmscash(boolean nmscash) {
        this.nmscash = nmscash;
    }

    public double getNmscash_amount() {
        return nmscash_amount;
    }

    public void setNmscash_amount(double nmscash_amount) {
        this.nmscash_amount = nmscash_amount;
    }

    public double getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(double paid_amount) {
        this.paid_amount = paid_amount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setPaymentGatewayResponse(DiagnosticPaymentGatewayResponse paymentGatewayResponse) {
        this.paymentGatewayResponse = paymentGatewayResponse;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
