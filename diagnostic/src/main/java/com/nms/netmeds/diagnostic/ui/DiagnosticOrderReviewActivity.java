package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticOrderReviewBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.OfferBannerResponse;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticOrderReviewViewModel;

import static com.nms.netmeds.base.utils.DiagnosticConstant.INTENT_KEY_COUPON_OBJECT;

public class DiagnosticOrderReviewActivity extends BaseViewModelActivity<DiagnosticOrderReviewViewModel> implements DiagnosticOrderReviewViewModel.OrderReviewListener {

    private ActivityDiagnosticOrderReviewBinding diagnosticOrderReviewBinding;
    private DiagnosticOrderReviewViewModel diagnosticOrderReviewViewModel;
    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        diagnosticOrderReviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_order_review);
        toolBarSetUp(diagnosticOrderReviewBinding.toolBarCustomLayout.toolbar);
        initToolBar(diagnosticOrderReviewBinding.toolBarCustomLayout.collapsingToolbar, DiagnosticOrderReviewActivity.this.getResources().getString(R.string.text_order_review));
        diagnosticOrderReviewBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected DiagnosticOrderReviewViewModel onCreateViewModel() {
        diagnosticOrderReviewViewModel = ViewModelProviders.of(this).get(DiagnosticOrderReviewViewModel.class);
        diagnosticOrderReviewViewModel.init(this, diagnosticOrderReviewBinding, this);
        intentValue();
        diagnosticOrderReviewViewModel.getLabDetailsMutableLiveData().observe(this, new LabDetailsObserver());
        return diagnosticOrderReviewViewModel;
    }

    public class LabDetailsObserver implements Observer<OfferBannerResponse> {
        @Override
        public void onChanged(@Nullable OfferBannerResponse offerBannerResponse) {
            diagnosticOrderReviewViewModel.setLabDetails(offerBannerResponse);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void intentValue() {
        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL)) {
            bundle = getIntent().getBundleExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL);
            if (bundle == null) return;
            if (bundle.containsKey(DiagnosticConstant.KEY_SELECTED_TIME_SLOT)) {
                diagnosticOrderReviewViewModel.setSlotTime((SlotTime) bundle.getSerializable(DiagnosticConstant.KEY_SELECTED_TIME_SLOT));
            }
            if (bundle.containsKey(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT)) {
                diagnosticOrderReviewViewModel.setTotalAmount(bundle.getDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT));
            }
            if (bundle.containsKey(DiagnosticConstant.KEY_SELECTED_LAB)) {
                diagnosticOrderReviewViewModel.setAvailableLab((AvailableLab) bundle.getSerializable(DiagnosticConstant.KEY_SELECTED_LAB));
            }
        }
    }

    @Override
    public void proceedForPayment() {
        if (diagnosticOrderReviewViewModel == null || getIntent() == null || !getIntent().hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL) || bundle == null)
            return;
        Intent intent = getIntent();
        if (diagnosticOrderReviewViewModel.isCouponAvailable()) {
            intent.putExtra(INTENT_KEY_COUPON_OBJECT, diagnosticOrderReviewViewModel.getCouponObject());
            bundle.putDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT, diagnosticOrderReviewViewModel.originalAmount);
        } else {
            bundle.putDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT, diagnosticOrderReviewViewModel.getTotalAmount());
        }
        intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, bundle);
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_payment_activity), intent, this);
    }

    @Override
    public void vmShowProgress() {
        diagnosticOrderReviewBinding.toolBarCustomLayout.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        diagnosticOrderReviewBinding.toolBarCustomLayout.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void finishMe(boolean flag) {
        if(flag){
            finish();
            return;
        }
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_diagnostic_search_test), this);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DiagnosticHelper.setIsNMSCashApplied(false);
        diagnosticOrderReviewViewModel.couponBox();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
