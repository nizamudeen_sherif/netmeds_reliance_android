package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.PackageDetailBinding;

import java.util.List;


public class ViewDetailAdapter extends RecyclerView.Adapter<ViewDetailAdapter.ViewDetailHolder> {
    private List<String> packageDetailsList;
    private Context mContext;

    ViewDetailAdapter(Context context, List<String> packageDetailsList) {
        this.mContext = context;
        this.packageDetailsList = packageDetailsList;
    }


    @NonNull
    @Override
    public ViewDetailHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        PackageDetailBinding packageDetailBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.package_detail, viewGroup, false);
        return new ViewDetailHolder(packageDetailBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewDetailHolder holder, @SuppressLint("RecyclerView") final int position) {
        final String detail = packageDetailsList.get(position);
        holder.packageDetailBinding.textDetail.setText(String.format("%s  %s", mContext.getResources().getString(R.string.text_bullet_point_diagnostic), detail));
    }

    @Override
    public int getItemCount() {
        return packageDetailsList.size();
    }


    class ViewDetailHolder extends RecyclerView.ViewHolder {
        PackageDetailBinding packageDetailBinding;

        ViewDetailHolder(@NonNull PackageDetailBinding packageDetailBinding) {
            super(packageDetailBinding.getRoot());
            this.packageDetailBinding = packageDetailBinding;
        }
    }
}