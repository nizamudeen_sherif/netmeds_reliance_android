package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

public class DiagnosticOrderTrackRequest {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("userId")
    private String userId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
