package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DiagnosticOrderItemAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticViewOrderDetailBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;
import com.nms.netmeds.diagnostic.model.DiagnosticPaymentCreateResult;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiagnosticOrderDetailViewModel extends AppViewModel {
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private ActivityDiagnosticViewOrderDetailBinding viewOrderDetailBinding;
    private DiagnosticOrderDetailViewModel orderDetailViewModel;
    private DiagnosticOrderDetailListener orderDetailListener;
    private DiagnosticOrder diagnosticViewOrderDetail;

    public DiagnosticOrderDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticViewOrderDetailBinding viewOrderDetailBinding, DiagnosticOrderDetailViewModel orderDetailViewModel, DiagnosticOrderDetailListener orderDetailListener) {
        this.mContext = context;
        this.viewOrderDetailBinding = viewOrderDetailBinding;
        this.orderDetailViewModel = orderDetailViewModel;
        this.orderDetailListener = orderDetailListener;
        setTitle();
        viewOrderDetailBinding.textSubTotalLabel.setText(String.format("%s%s%s%s", mContext.getResources().getString(R.string.text_sub_total), "(", DiagnosticConstant.INR, ")"));
        viewOrderDetailBinding.textDiscountLabel.setText(String.format("%s%s%s%s", mContext.getResources().getString(R.string.text_diagnostic_discount), "(", DiagnosticConstant.INR, ")"));
        if (getOrderDetail() != null && getOrderDetail().size() > 0)
            setOrderItemAdapter();
        isShowLabName();
        isShowOfferedPrice();
        isShowDiscountAmount();
        isShowFinalPrice();
    }

    private DiagnosticOrder getDiagnosticViewOrderDetail() {
        return diagnosticViewOrderDetail;
    }

    public void setDiagnosticViewOrderDetail(DiagnosticOrder diagnosticViewOrderDetail) {
        this.diagnosticViewOrderDetail = diagnosticViewOrderDetail;
    }

    public interface DiagnosticOrderDetailListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmCallbackCancelOrder(String orderId);

        void vmCallbackNeedHelp();

        void vmCallbackShowStatus(String orderId);
    }

    private String getCustomerAddress(MStarAddressModel customerAddress) {
        return CommonUtils.getAddressFromObject(customerAddress);
    }

    public String setPatientName(boolean showPatientName) {
        if (showPatientName)
            return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getName()) ? CommonUtils.convertToTitleCase(getPatientDetail().getName()) : "";
        if (diagnosticViewOrderDetail != null && diagnosticViewOrderDetail.getRadio()) {
            if (diagnosticViewOrderDetail.getDiagnosticOrderDetail() == null || diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription() == null
                    || TextUtils.isEmpty(diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription().getLabName()))
                return "";
            return diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription().getLabName();
        }
        return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getName()) ? CommonUtils.convertToTitleCase(getPatientDetail().getName()) : "";
    }

    private String getPatientMobileNo() {
        return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getTelephone()) ? getPatientDetail().getTelephone() : "";
    }

    private PatientDetail getPatientDetail() {
        return getDiagnosticViewOrderDetail() != null && getDiagnosticViewOrderDetail().getPatientDetails() != null ? getDiagnosticViewOrderDetail().getPatientDetails() : new PatientDetail();
    }

    public String setOrderId() {
        return getDiagnosticViewOrderDetail() != null && !TextUtils.isEmpty(getDiagnosticViewOrderDetail().getOrderId()) ? getDiagnosticViewOrderDetail().getOrderId() : "";
    }

    public String setOrderPlaced() {
        return getDiagnosticViewOrderDetail() != null && !TextUtils.isEmpty(getDiagnosticViewOrderDetail().getCreatedAt()) ? DateTimeUtils.getInstance().utcToLocal(DateTimeUtils.yyyyMMddTHHmmssSSS, DateTimeUtils.MMMddyyyyhhmma, getDiagnosticViewOrderDetail().getCreatedAt()) : "";
    }

    public MStarAddressModel getAddress() {
        return getPatientDetail() != null && getPatientDetail().getAddress() != null ? getPatientDetail().getAddress() : new MStarAddressModel();
    }

    public String setPickupAddress() {
        if (diagnosticViewOrderDetail != null && diagnosticViewOrderDetail.getRadio() && diagnosticViewOrderDetail.getDiagnosticOrderDetail() != null
                && diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription() != null
                && diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription().getLabAddressList() != null
                && diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription().getLabAddressList().size() > 0
                && !TextUtils.isEmpty(diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription().getLabAddressList().get(0).getFullAddress())) {
            return diagnosticViewOrderDetail.getDiagnosticOrderDetail().getLabDescription().getLabAddressList().get(0).getFullAddress();
        }
        return getCustomerAddress(getAddress());
    }

    public String getPickUpAddOrLabAdd() {
        if (diagnosticViewOrderDetail != null && diagnosticViewOrderDetail.getRadio())
            return mContext.getResources().getString(R.string.txt_lab_address);
        return mContext.getResources().getString(R.string.text_pickup_address);
    }

    public void setTitle() {
        viewOrderDetailBinding.collapsingToolbar.setTitle(setPatientName(true));
    }

    public String setPaymentMode() {
        return getPaymentDetails() != null && !TextUtils.isEmpty(getPaymentDetails().getPaymentMode()) ? getPaymentDetails().getPaymentMode() : "";
    }

    public void onHelpCallBack() {
        orderDetailListener.vmCallbackNeedHelp();
    }

    public void onCancelCallBack() {
        orderDetailListener.vmCallbackCancelOrder(getOrderId());
    }

    public void onShowOrderStatusCallBack() {
        orderDetailListener.vmCallbackShowStatus(getOrderId());
    }

    private List<Test> getOrderDetail() {
        return getDiagnosticViewOrderDetail() != null && getDiagnosticViewOrderDetail().getDiagnosticOrderDetail() != null && getDiagnosticViewOrderDetail().getDiagnosticOrderDetail().getTestList() != null && getDiagnosticViewOrderDetail().getDiagnosticOrderDetail().getTestList().size() > 0 ? getDiagnosticViewOrderDetail().getDiagnosticOrderDetail().getTestList() : new ArrayList<Test>();
    }

    private void setOrderItemAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        viewOrderDetailBinding.orderListView.setLayoutManager(linearLayoutManager);
        viewOrderDetailBinding.orderListView.setNestedScrollingEnabled(false);
        DiagnosticOrderItemAdapter orderItemAdapter = new DiagnosticOrderItemAdapter(mContext, getOrderDetail(), true);
        viewOrderDetailBinding.orderListView.setAdapter(orderItemAdapter);
    }

    private PriceDescription getPriceDetails() {
        return getDiagnosticViewOrderDetail() != null && getDiagnosticViewOrderDetail().getPriceDescription() != null ? getDiagnosticViewOrderDetail().getPriceDescription() : new PriceDescription();
    }

    public String getOfferedPrice() {
        return getPriceDetails() != null && !TextUtils.isEmpty(getPriceDetails().getOfferedPrice()) ? getPriceDetails().getOfferedPrice() : "";
    }

    public String getFinalPrice() {
        return getPriceDetails() != null && !TextUtils.isEmpty(getPriceDetails().getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, getPriceDetails().getFinalPrice()) : "";
    }

    public String getDiscountAmount() {
        return getPriceDetails() != null && !TextUtils.isEmpty(getPriceDetails().getDiscountAmount()) ? String.format("%s%s", "-", getPriceDetails().getDiscountAmount()) : "";
    }

    public void isShowOfferedPrice() {
        if (TextUtils.isEmpty(getOfferedPrice())) {
            viewOrderDetailBinding.lOfferedPrice.setVisibility(View.GONE);
        } else {
            viewOrderDetailBinding.lOfferedPrice.setVisibility(View.VISIBLE);
        }
    }

    public void isShowFinalPrice() {
        if (TextUtils.isEmpty(getFinalPrice())) {
            viewOrderDetailBinding.cardViewNetAmount.setVisibility(View.GONE);
        } else {
            viewOrderDetailBinding.cardViewNetAmount.setVisibility(View.VISIBLE);
        }
    }

    public void isShowDiscountAmount() {
        if (TextUtils.isEmpty(getDiscountAmount())) {
            viewOrderDetailBinding.lytDiscount.setVisibility(View.GONE);
        } else {
            viewOrderDetailBinding.lytDiscount.setVisibility(View.VISIBLE);
        }
    }

    private LabDescription getLabDetails() {
        return getDiagnosticViewOrderDetail() != null && getDiagnosticViewOrderDetail().getDiagnosticOrderDetail() != null && getDiagnosticViewOrderDetail().getDiagnosticOrderDetail().getLabDescription() != null ? getDiagnosticViewOrderDetail().getDiagnosticOrderDetail().getLabDescription() : new LabDescription();
    }

    public String getLabName() {
        return getLabDetails() != null && !TextUtils.isEmpty(getLabDetails().getLabName()) ? getLabDetails().getLabName() : "";
    }

    public void isShowLabName() {
        if (TextUtils.isEmpty(getLabName())) {
            viewOrderDetailBinding.lLayoutLabName.setVisibility(View.GONE);
        } else {
            viewOrderDetailBinding.lLayoutLabName.setVisibility(View.VISIBLE);
        }
    }

    private DiagnosticPaymentCreateResult getPaymentDetails() {
        return getDiagnosticViewOrderDetail() != null && getDiagnosticViewOrderDetail().getPayment() != null ? getDiagnosticViewOrderDetail().getPayment() : new DiagnosticPaymentCreateResult();
    }

    public String getOrderId() {
        return getDiagnosticViewOrderDetail() != null && !TextUtils.isEmpty(getDiagnosticViewOrderDetail().getOrderId()) ? getDiagnosticViewOrderDetail().getOrderId() : "";
    }
}
