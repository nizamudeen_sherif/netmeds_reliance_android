package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.MStarAddressModel;

import java.io.Serializable;

public class PatientDetail implements Serializable {
    @SerializedName("gender")
    private String gender;
    @SerializedName("name")
    private String name;
    @SerializedName("telephone")
    private String telephone;
    @SerializedName("age")
    private String age;
    @SerializedName("email")
    private String email;
    @SerializedName("address")
    private MStarAddressModel address;


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MStarAddressModel getAddress() {
        return address;
    }

    public void setAddress(MStarAddressModel address) {
        this.address = address;
    }
}
