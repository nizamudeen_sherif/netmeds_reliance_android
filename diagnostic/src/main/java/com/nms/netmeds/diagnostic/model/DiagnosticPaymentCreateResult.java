package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiagnosticPaymentCreateResult implements Serializable {
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("amount")
    private String amount;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("pg")
    private String pg;
    @SerializedName("id")
    private String id;
    @SerializedName("pg_response")
    private DiagnosticPaymentGatewayResponse pgResponse;
    @SerializedName("status")
    private String status;
    @SerializedName("updatedAt")
    private String updatedAt;
    @SerializedName("paymentMode")
    private String paymentMode;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DiagnosticPaymentGatewayResponse getPgResponse() {
        return pgResponse;
    }

    public void setPgResponse(DiagnosticPaymentGatewayResponse pgResponse) {
        this.pgResponse = pgResponse;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
}
