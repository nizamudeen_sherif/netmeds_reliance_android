package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;
import com.nms.netmeds.diagnostic.model.Request.ShowCouponResult;

public class ShowCouponResponse {

    @SerializedName("updatedOn")
    private long updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private ShowCouponResult result;

    public long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public ShowCouponResult getResult() {
        return result;
    }

    public void setResult(ShowCouponResult result) {
        this.result = result;
    }
}
