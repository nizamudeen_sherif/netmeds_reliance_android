package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiagnosticOrderCreateResult implements Serializable {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("message")
    private String message;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
