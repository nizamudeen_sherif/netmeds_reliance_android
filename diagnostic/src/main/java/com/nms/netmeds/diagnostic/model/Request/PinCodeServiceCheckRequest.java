package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

public class PinCodeServiceCheckRequest {
    @SerializedName("pincode")
    private String pinCode;
    @SerializedName("labId")
    private int labId;

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }
}
