package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DiagnosticOrderTrackStatusAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticTrackOrderBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderDetail;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderTrackResponse;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderTrackRequest;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DiagnosticTrackOrderViewModel extends AppViewModel implements DiagnosticOrderTrackStatusAdapter.OrderTrackStatusAdapterListener {
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private DiagnosticTrackOrderViewModel trackOrderViewModel;
    private ActivityDiagnosticTrackOrderBinding trackOrderBinding;
    private DiagnosticTrackOrderListener trackOrderListener;
    private int failedTransactionId;
    private MutableLiveData<DiagnosticOrderTrackResponse> diagnosticOrderTrackMutableLiveData = new MutableLiveData<>();
    private DiagnosticOrder orderTrackResult;
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    private DiagnosticOrder getOrderTrackResult() {
        return orderTrackResult;
    }

    private void setOrderTrackResult(DiagnosticOrder orderTrackResult) {
        this.orderTrackResult = orderTrackResult;
    }

    public DiagnosticTrackOrderViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticTrackOrderBinding trackOrderBinding, DiagnosticTrackOrderViewModel trackOrderViewModel, String orderId, DiagnosticTrackOrderListener trackOrderListener) {
        this.mContext = context;
        this.trackOrderBinding = trackOrderBinding;
        this.trackOrderViewModel = trackOrderViewModel;
        this.trackOrderListener = trackOrderListener;
        setOrderId(orderId);
        getOrderTrack();
    }

    @Override
    public void onReportGenerated() {
        /*Diagnostic WebEngage Report Generated Event*/
        if (getOrderTrackResult() != null) {
            PatientDetail patientDetail = getOrderTrackResult().getPatientDetails() != null ? getOrderTrackResult().getPatientDetails() : new PatientDetail();
            MStarAddressModel collectionAddress = getOrderTrackResult().getPatientDetails() != null && getOrderTrackResult().getPatientDetails().getAddress() != null ? getOrderTrackResult().getPatientDetails().getAddress() : new MStarAddressModel();
            SlotTime slotTime = getOrderTrackResult().getSlot() != null ? getOrderTrackResult().getSlot() : new SlotTime();
            DiagnosticOrderDetail diagnosticOrderDetail = getOrderTrackResult().getDiagnosticOrderDetail() != null ? getOrderTrackResult().getDiagnosticOrderDetail() : new DiagnosticOrderDetail();
            List<Test> testList = diagnosticOrderDetail != null && diagnosticOrderDetail.getTestList() != null && diagnosticOrderDetail.getTestList().size() > 0 ? diagnosticOrderDetail.getTestList() : new ArrayList<Test>();
            LabDescription labDescription = diagnosticOrderDetail != null && diagnosticOrderDetail.getLabDescription() != null ? diagnosticOrderDetail.getLabDescription() : new LabDescription();
            String orderId = !TextUtils.isEmpty(getOrderTrackResult().getOrderId()) ? getOrderTrackResult().getOrderId() : "";
            String type = !TextUtils.isEmpty(getOrderTrackResult().getType()) ? getOrderTrackResult().getType() : "";
            double totalAmount = !TextUtils.isEmpty(getOrderTrackResult().getAmount()) ? Double.parseDouble(getOrderTrackResult().getAmount()) : 0;
            String orderDate = !TextUtils.isEmpty(getOrderTrackResult().getCreatedAt()) ? getOrderTrackResult().getCreatedAt() : "";
            String patientName = patientDetail != null && !TextUtils.isEmpty(patientDetail.getName()) ? patientDetail.getName() : "";
            String age = patientDetail != null && !TextUtils.isEmpty(patientDetail.getAge()) ? patientDetail.getAge() : "";
            String gender = patientDetail != null && !TextUtils.isEmpty(patientDetail.getGender()) ? patientDetail.getGender() : "";
            String selectedDay = slotTime != null && !TextUtils.isEmpty(slotTime.getSlotDay()) ? slotTime.getSlotDay() : "";
            String selectedTime = slotTime != null && !TextUtils.isEmpty(slotTime.getTimeRange()) ? slotTime.getTimeRange() : "";
            String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
            PriceDescription priceDescription = getOrderTrackResult().getPriceDescription() != null ? getOrderTrackResult().getPriceDescription() : new PriceDescription();
            String price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? priceDescription.getFinalPrice() : "";
            HashMap<String, Object> addressMap = null;
            if (collectionAddress != null) {
                String jsonObject = new Gson().toJson(collectionAddress);
                Type addressType = new TypeToken<HashMap<String, Object>>() {
                }.getType();
                addressMap = new Gson().fromJson(jsonObject, addressType);
            }

            Bundle bundle = new Bundle();
            bundle.putString(DiagnosticConstant.KEY_PATIENT_NAME, patientName);
            bundle.putString(DiagnosticConstant.KEY_AGE, age);
            bundle.putString(DiagnosticConstant.KEY_GENDER, gender);
            bundle.putSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS, addressMap);
            bundle.putString(DiagnosticConstant.KEY_SELECTED_DAY, selectedDay);
            bundle.putString(DiagnosticConstant.KEY_SELECTED_TIME, selectedTime);
            bundle.putString(DiagnosticConstant.KEY_LAB_NAME, labName);
            bundle.putDouble(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE, !TextUtils.isEmpty(price) ? Double.parseDouble(price) : 0);
            bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) testList);
            bundle.putString(DiagnosticConstant.KEY_ORDER_ID, orderId);
            bundle.putDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT, totalAmount);
            bundle.putString(DiagnosticConstant.KEY_ORDER_SUCCESS, DiagnosticConstant.YES);
            bundle.putString(DiagnosticConstant.KEY_ORDER_DATE, orderDate);

            WebEngageHelper.getInstance().reportGenerateEvent(BasePreference.getInstance(mContext), DiagnosticHelper.getCity(), testList, type, bundle, trackOrderListener.getContext());
        }
    }

    public interface DiagnosticTrackOrderListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmCallbackCancelOrder(String orderId);

        void vmCallbackNeedHelp();

        void vmCallbackViewOrder(DiagnosticOrder diagnosticOrder);

        Context getContext();
    }

    public MutableLiveData<DiagnosticOrderTrackResponse> getDiagnosticOrderTrackMutableLiveData() {
        return diagnosticOrderTrackMutableLiveData;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK) {
            DiagnosticOrderTrackResponse orderTrackResponse = new Gson().fromJson(data, DiagnosticOrderTrackResponse.class);
            diagnosticOrderTrackMutableLiveData.setValue(orderTrackResponse);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        trackOrderListener.vmDismissProgress();
        if (transactionId == DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK) {
            vmOnError(transactionId);
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK) {
            getOrderTrack();
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        trackOrderBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        trackOrderBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        trackOrderBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        trackOrderBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void getOrderTrack() {
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(mContext).getDiagnosticLoginResponse(), JustDocUserResponse.class);
            String userId = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getId()) ? diagnosticLoginResult.getId() : "";
            DiagnosticOrderTrackRequest orderTrackRequest = new DiagnosticOrderTrackRequest();
            orderTrackRequest.setOrderId(getOrderId());
            orderTrackRequest.setUserId(userId);
            trackOrderListener.vmShowProgress();
            DiagnosticServiceManager.getInstance().diagnosticOrderTrack(trackOrderViewModel, orderTrackRequest, BasePreference.getInstance(mContext));
        } else
            failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK;
    }

    public void trackOrderDataAvailable(DiagnosticOrderTrackResponse orderTrackResponse) {
        trackOrderListener.vmDismissProgress();
        if (orderTrackResponse != null) {
            trackOrderServiceStatus(orderTrackResponse);
        } else
            vmOnError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK);
    }

    private void trackOrderServiceStatus(DiagnosticOrderTrackResponse orderTrackResponse) {
        if (orderTrackResponse.getServiceStatus() != null && orderTrackResponse.getServiceStatus().getStatusCode() != null && orderTrackResponse.getServiceStatus().getStatusCode() == 200) {
            trackOrderResult(orderTrackResponse);
        } else
            setErrorMessage(orderTrackResponse);
    }

    private void setErrorMessage(DiagnosticOrderTrackResponse orderTrackResponse) {
        if (orderTrackResponse.getOrderTrackResult() != null && !TextUtils.isEmpty(orderTrackResponse.getOrderTrackResult().getMessage())) {
            SnackBarHelper.snackBarCallBack(trackOrderBinding.lytViewContent, mContext, orderTrackResponse.getOrderTrackResult().getMessage());
            trackOrderBinding.lytViewContent.setVisibility(View.GONE);
        } else
            vmOnError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK);
    }

    private void trackOrderResult(DiagnosticOrderTrackResponse orderTrackResponse) {
        if (orderTrackResponse.getOrderTrackResult() != null) {
            DiagnosticOrder trackResult = orderTrackResponse.getOrderTrackResult();
            setOrderTrackResult(trackResult);
            setTrackOrderStatus(trackResult);
            trackOrderBinding.setViewModel(trackOrderViewModel);
            showTimeSlot();
            showTrackOrderNote();
        } else
            vmOnError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_TRACK);
    }

    private void setTrackOrderStatus(DiagnosticOrder trackResult) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        trackOrderBinding.orderStatusList.setLayoutManager(linearLayoutManager);
        trackOrderBinding.orderStatusList.setNestedScrollingEnabled(false);
        DiagnosticOrderTrackStatusAdapter diagnosticOrderTrackStatusAdapter = new DiagnosticOrderTrackStatusAdapter(mContext, trackResult.getTrackDetailList(), this);
        trackOrderBinding.orderStatusList.setAdapter(diagnosticOrderTrackStatusAdapter);
    }

    public String setTimeSlot() {
        return getSlotDate() + "\n" + getTimeRage();
    }

    public String trackNote() {
        return trackOrderListener.getContext().getResources().getString(R.string.text_track_order_note);
    }

    private String getSlotDate() {
        return getSlot() != null && !TextUtils.isEmpty(getSlot().getSlotDate()) ? DateTimeUtils.getInstance().stringDate(getSlot().getSlotDate(), DateTimeUtils.yyyyMMdd, DateTimeUtils.MMMMddyyyy).toUpperCase() : "";
    }

    private String getTimeRage() {
        return getSlot() != null && !TextUtils.isEmpty(getSlot().getTimeRange()) ? getSlot().getTimeRange() : "";
    }


    private void showTrackOrderNote(){
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(trackOrderListener.getContext()).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse ==  null || configurationResponse.getResult() == null || configurationResponse.getResult().getConfigDetails() == null
                || !configurationResponse.getResult().getConfigDetails().isDiagTrackOrderEnableFlag()) {
            trackOrderBinding.cardTrackNote.setVisibility(View.GONE);
            return;
        }
        trackOrderBinding.cardTrackNote.setVisibility(View.VISIBLE);
    }

    private void showTimeSlot() {
        if(TextUtils.isEmpty(getSlotDate()) && TextUtils.isEmpty(getTimeRage())){
            trackOrderBinding.cvPrescriptionView.setVisibility(View.GONE);
        }else{
            trackOrderBinding.cvPrescriptionView.setVisibility(View.VISIBLE);
        }
    }

    private SlotTime getSlot() {
        return getOrderTrackResult() != null && getOrderTrackResult().getSlot() != null ? getOrderTrackResult().getSlot() : new SlotTime();
    }

    public String setNoLabTest() {
        return mContext.getResources().getQuantityString(R.plurals.text_lab_test, getOrderDetail() != null && getOrderDetail().size() > 0 ? getOrderDetail().size() : 0, getOrderDetail() != null && getOrderDetail().size() > 0 ? getOrderDetail().size() : 0);
    }

    private List<Test> getOrderDetail() {
        return getOrderTrackResult() != null && getOrderTrackResult().getDiagnosticOrderDetail() != null && getOrderTrackResult().getDiagnosticOrderDetail().getTestList() != null && getOrderTrackResult().getDiagnosticOrderDetail().getTestList().size() > 0 ? getOrderTrackResult().getDiagnosticOrderDetail().getTestList() : new ArrayList<Test>();
    }

    private String getCustomerAddress(MStarAddressModel customerAddress) {
        String formattedAddress = "";

        if(!TextUtils.isEmpty(customerAddress.getStreet())){
            formattedAddress = String.format("%s%s, ", formattedAddress, customerAddress.getStreet());
        }

        if (!TextUtils.isEmpty(customerAddress.getCity()))
            formattedAddress = formattedAddress + "\n" + customerAddress.getCity();

        if (!TextUtils.isEmpty(customerAddress.getPin()))
            formattedAddress = formattedAddress + " - " + customerAddress.getPin() + ".\n";

        if (!TextUtils.isEmpty(getPatientMobileNo()))
            formattedAddress = formattedAddress + String.format(mContext.getString(R.string.text_state_code), getPatientMobileNo());

        return formattedAddress;
    }

    private PatientDetail getPatientDetail() {
        return getOrderTrackResult() != null && getOrderTrackResult().getPatientDetails() != null ? getOrderTrackResult().getPatientDetails() : new PatientDetail();
    }

    public MStarAddressModel getAddress() {
        return getPatientDetail() != null && getPatientDetail().getAddress() != null ? getPatientDetail().getAddress() : new MStarAddressModel();
    }

    public String setPickupAddress() {
        if (orderTrackResult != null && orderTrackResult.getRadio() && orderTrackResult.getDiagnosticOrderDetail() != null
                && orderTrackResult.getDiagnosticOrderDetail().getLabDescription() != null
                && orderTrackResult.getDiagnosticOrderDetail().getLabDescription().getLabAddressList() != null
                && orderTrackResult.getDiagnosticOrderDetail().getLabDescription().getLabAddressList().size() > 0
                && !TextUtils.isEmpty(orderTrackResult.getDiagnosticOrderDetail().getLabDescription().getLabAddressList().get(0).getFullAddress())) {
            return orderTrackResult.getDiagnosticOrderDetail().getLabDescription().getLabAddressList().get(0).getFullAddress();
        }
        return getCustomerAddress(getAddress());
    }

    public String getPickUpAddOrLabAdd() {
        if (orderTrackResult != null && orderTrackResult.getRadio())
            return mContext.getResources().getString(R.string.txt_lab_address);
        return mContext.getResources().getString(R.string.text_pickup_address);
    }

    public String setPatientName() {
        if (getOrderTrackResult() != null && getOrderTrackResult().getRadio() && getOrderTrackResult().getDiagnosticOrderDetail() != null
                && getOrderTrackResult().getDiagnosticOrderDetail().getLabDescription() != null
                && !TextUtils.isEmpty(getOrderTrackResult().getDiagnosticOrderDetail().getLabDescription().getLabName())) {
            return getOrderTrackResult().getDiagnosticOrderDetail().getLabDescription().getLabName();
        }
        return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getName()) ? CommonUtils.convertToTitleCase(getPatientDetail().getName()) : "";
    }

    private String getPatientMobileNo() {
        return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getTelephone()) ? getPatientDetail().getTelephone() : "";
    }

    public void onHelpCallBack() {
        trackOrderListener.vmCallbackNeedHelp();
    }

    public void onCancelCallBack() {
        trackOrderListener.vmCallbackCancelOrder(getOrderId());
    }

    public void onShowOrderStatusCallBack() {
        trackOrderListener.vmCallbackViewOrder(getOrderTrackResult());
    }
}
