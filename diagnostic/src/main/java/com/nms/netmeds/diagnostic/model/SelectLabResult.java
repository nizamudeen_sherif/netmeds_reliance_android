package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.Subscription;

import java.io.Serializable;
import java.util.List;

public class SelectLabResult implements Serializable {
    @SerializedName("products")
    private List<AvailableLab> resultList;
    @SerializedName("missingTestDetails")
    private String unAvailableTest;
    @SerializedName("missingTest")
    private boolean missingTest;
    @SerializedName("missingCount")
    private String missingCount;
    @SerializedName("message")
    private String message;
    @SerializedName("subscription")
    private Subscription subscription;

    public List<AvailableLab> getResultList() {
        return resultList;
    }

    public void setResultList(List<AvailableLab> resultList) {
        this.resultList = resultList;
    }

    public String getUnAvailableTest() {
        return unAvailableTest;
    }

    public void setUnAvailableTest(String unAvailableTest) {
        this.unAvailableTest = unAvailableTest;
    }

    public boolean isMissingTest() {
        return missingTest;
    }

    public void setMissingTest(boolean missingTest) {
        this.missingTest = missingTest;
    }

    public String getMissingCount() {
        return missingCount;
    }

    public void setMissingCount(String missingCount) {
        this.missingCount = missingCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
}
