package com.nms.netmeds.diagnostic.model;

// Available/Unavailable Test Status
public class TestStatus {

    private String testName;
    private boolean isAvailable;

    public TestStatus(String testName, boolean isAvailable) {
        this.testName = testName;
        this.isAvailable = isAvailable;
    }

    public String getTestName() {
        return testName;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

}
