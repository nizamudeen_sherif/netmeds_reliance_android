package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.MostPoplularPackageItemBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Adesh
 * Adapter class for inflating Available/Unavailable test within the selected Lab
 */
public class MostPopularPackageAdapter extends RecyclerView.Adapter<MostPopularPackageAdapter.MostPopularItemViewHolder> {
    private List<DiagnosticPackage> mostPopularList;
    private Context mContext;

    public MostPopularPackageAdapter(List<DiagnosticPackage> mostPopularList, Context mContext) {
        this.mostPopularList = mostPopularList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MostPopularItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        MostPoplularPackageItemBinding mostPoplularPackageItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.most_poplular_package_item, viewGroup, false);
        return new MostPopularItemViewHolder(mostPoplularPackageItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MostPopularItemViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final DiagnosticPackage diagnosticPackage = mostPopularList.get(position);
        final Test test = diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 ? diagnosticPackage.getTestList().get(0) : new Test();
        final PriceDescription priceDescription = diagnosticPackage.getPriceDescription() != null ? diagnosticPackage.getPriceDescription() : new PriceDescription();
        final LabDescription labDescription = diagnosticPackage.getLabDescription() != null ? diagnosticPackage.getLabDescription() : new LabDescription();
        holder.mostPoplularPackageItemBinding.packageName.setText(!TextUtils.isEmpty(test.getTestName()) ? test.getTestName() : "");
        holder.mostPoplularPackageItemBinding.textDiscountPercentage.setText(!TextUtils.isEmpty(priceDescription.getDiscountPercentInfo()) ? priceDescription.getDiscountPercentInfo() : "");
        holder.mostPoplularPackageItemBinding.textPrice.setText(!TextUtils.isEmpty(priceDescription.getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getFinalPrice()) : "");
        holder.mostPoplularPackageItemBinding.textStrikePrice.setText(!TextUtils.isEmpty(priceDescription.getOfferedPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getOfferedPrice()) : "");
        holder.mostPoplularPackageItemBinding.textStrikePrice.setPaintFlags(holder.mostPoplularPackageItemBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        labLogo(!TextUtils.isEmpty(test.getImageUrl()) ? test.getImageUrl() : "", holder.mostPoplularPackageItemBinding.packageImage);
        holder.mostPoplularPackageItemBinding.labName.setText(!TextUtils.isEmpty(labDescription.getLabName()) ? String.format("%s %s", mContext.getResources().getString(R.string.text_by), labDescription.getLabName()) : "");

        holder.mostPoplularPackageItemBinding.cardViewMostPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToPackageDetails(test);
            }
        });

        holder.mostPoplularPackageItemBinding.txtBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToLabPage(test, diagnosticPackage);
            }
        });
    }

    // redirect the user to package details
    private void redirectToPackageDetails(final Test selectedTest) {
        if (selectedTest == null) return;
        Intent intent = new Intent();
        if (!TextUtils.isEmpty(selectedTest.getType()) && selectedTest.getType().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_profile_type)) || selectedTest.getType().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_packages_type))
                || selectedTest.getType().equalsIgnoreCase(mContext.getResources().getString(R.string.txt_package_type))) {
            final DiagnosticPackage diagnosticPackage = new DiagnosticPackage();
            ArrayList<Test> arrayList = new ArrayList<>();
            arrayList.add(selectedTest);
            diagnosticPackage.setTestList(arrayList);
            intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED, diagnosticPackage);
            LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_package_detail), intent, mContext);
        }
    }

    // redirect the user to lab page
    private void redirectToLabPage(final Test test, DiagnosticPackage diagnosticPackage) {
        if (test == null || diagnosticPackage == null) return;
        DiagnosticHelper.setTest(DiagnosticHelper.addATest(diagnosticPackage));
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
        LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_select_lab), intent, mContext);
    }

    private void labLogo(String logo, ImageView imageView) {
        RequestOptions options = new RequestOptions().placeholder(R.drawable.ic_default_lab)
                .error(R.drawable.ic_default_lab)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(mContext).load(logo.equals("") ? R.drawable.ic_default_lab:logo).apply(options).into(imageView);
    }

    @Override
    public int getItemCount() {
        return mostPopularList.size();
    }


    class MostPopularItemViewHolder extends RecyclerView.ViewHolder {
        MostPoplularPackageItemBinding mostPoplularPackageItemBinding;

        MostPopularItemViewHolder(@NonNull MostPoplularPackageItemBinding mostPoplularPackageItemBinding) {
            super(mostPoplularPackageItemBinding.getRoot());
            this.mostPoplularPackageItemBinding = mostPoplularPackageItemBinding;
        }
    }
}
