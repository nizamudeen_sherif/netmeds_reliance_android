package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

public class ShowCouponResult {

    @SerializedName("ios")
    private boolean ios;
    @SerializedName("and")
    private boolean and;
    @SerializedName("msite")
    private boolean msite;

    public boolean isIos() {
        return ios;
    }

    public void setIos(boolean ios) {
        this.ios = ios;
    }

    public boolean isAnd() {
        return and;
    }

    public void setAnd(boolean and) {
        this.and = and;
    }

    public boolean isMsite() {
        return msite;
    }

    public void setMsite(boolean msite) {
        this.msite = msite;
    }
}
