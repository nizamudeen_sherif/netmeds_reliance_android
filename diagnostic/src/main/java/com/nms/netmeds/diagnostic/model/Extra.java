package com.nms.netmeds.diagnostic.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Extra implements Serializable {

    private ArrayList<Tag> tags;

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }
}
