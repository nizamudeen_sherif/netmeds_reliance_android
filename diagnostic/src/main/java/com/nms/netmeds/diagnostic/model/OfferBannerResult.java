package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.MstarBanner;

import java.io.Serializable;
import java.util.List;

public class OfferBannerResult implements Serializable {
    @SerializedName("homeBanners")
    private List<MstarBanner> mstarBanner;
    @SerializedName("feature")
    private LabDetails lab_details;
    @SerializedName("isFirstOrder")
    private boolean isFirstOrder;

    public Boolean getFirstOrder() {
        return isFirstOrder;
    }

    public void setFirstOrder(Boolean firstOrder) {
        isFirstOrder = firstOrder;
    }

    public LabDetails getLab_details() {
        return lab_details;
    }

    public void setLab_details(LabDetails lab_details) {
        this.lab_details = lab_details;
    }

    public List<MstarBanner> getMstarBanner() {
        return mstarBanner;
    }

    public void setMstarBanner(List<MstarBanner> mstarBanner) {
        this.mstarBanner = mstarBanner;
    }
}
