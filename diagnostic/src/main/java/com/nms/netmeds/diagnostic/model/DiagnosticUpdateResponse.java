package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class DiagnosticUpdateResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("result")
    private DiagnosticUpdateResult paymentUpdateResult;
    private String paymentUpdateType;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public DiagnosticUpdateResult getPaymentUpdateResult() {
        return paymentUpdateResult;
    }

    public void setPaymentUpdateResult(DiagnosticUpdateResult paymentUpdateResult) {
        this.paymentUpdateResult = paymentUpdateResult;
    }

    public String getPaymentUpdateType() {
        return paymentUpdateType;
    }

    public void setPaymentUpdateType(String paymentUpdateType) {
        this.paymentUpdateType = paymentUpdateType;
    }
}
