package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.DiagnosticCartItemBinding;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.List;
import java.util.Locale;

public class DiagnosticCartItemAdapter extends RecyclerView.Adapter<DiagnosticCartItemAdapter.DiagnosticCartViewHolder> {

    private Context context;
    private List<Test> selectedList;
    private static final int FASTING_TYPE_1 = 1;
    private static final int FASTING_TYPE_2 = 2;

    public DiagnosticCartItemAdapter(Context context, List<Test> selectedList) {
        this.context = context;
        this.selectedList = selectedList;
    }

    @NonNull
    @Override
    public DiagnosticCartViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        DiagnosticCartItemBinding cartItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.diagnostic_cart_item, viewGroup, false);
        return new DiagnosticCartViewHolder(cartItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DiagnosticCartViewHolder cartHolder, int i) {
        final Test test = selectedList.get(i);
        cartHolder.diagnosticCartItemBinding.textTestName.setText(CommonUtils.validateString(test.getTestName()));
        if (test.getPriceInfo() != null) {
            cartHolder.diagnosticCartItemBinding.textPrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, DiagnosticHelper.validatePrice(test.getPriceInfo().getFinalPrice())));
            cartHolder.diagnosticCartItemBinding.textStrikePrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, DiagnosticHelper.validatePrice(test.getPriceInfo().getOfferedPrice())));
            cartHolder.diagnosticCartItemBinding.textStrikePrice.setPaintFlags(cartHolder.diagnosticCartItemBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        cartHolder.diagnosticCartItemBinding.textFastingRequired.setText(!TextUtils.isEmpty(test.getType()) ? String.format(Locale.getDefault(), "%s: %s%s", context.getResources().getString(R.string.text_type), test.getType(), setFastingRequiredText(test.getFastingRequired())) : "");
        cartHolder.diagnosticCartItemBinding.textFastingRequired.setVisibility(TextUtils.isEmpty(test.getType()) && TextUtils.isEmpty(setFastingRequiredText(test.getFastingRequired())) ? View.GONE : View.VISIBLE);
    }

    private String setFastingRequiredText(String fastingRequired) {
        if(TextUtils.isEmpty(fastingRequired)) return "";
        int fasting = Integer.parseInt(fastingRequired);
        switch (fasting){
            case FASTING_TYPE_1:{
                return ", " + context.getResources().getString(R.string.text_fasting_required);
            }
            case FASTING_TYPE_2:{
                return ", " + context.getResources().getString(R.string.text_fasting_required);
            }
            default: return "";
        }
    }


    public void updateList() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return selectedList.size();
    }

    static class DiagnosticCartViewHolder extends RecyclerView.ViewHolder {
        private DiagnosticCartItemBinding diagnosticCartItemBinding;

        DiagnosticCartViewHolder(@NonNull DiagnosticCartItemBinding diagnosticCartItemBinding) {
            super(diagnosticCartItemBinding.getRoot());
            this.diagnosticCartItemBinding = diagnosticCartItemBinding;
        }
    }
}
