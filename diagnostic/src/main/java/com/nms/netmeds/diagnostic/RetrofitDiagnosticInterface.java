package com.nms.netmeds.diagnostic;

import com.google.gson.JsonObject;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.PutMagentoAddressRequest;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.SendFcmTokenResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderCreateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderHistoryResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderTrackResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPaymentCreateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticUpdateResponse;
import com.nms.netmeds.diagnostic.model.OfferBannerResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PopularTestResponse;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticCouponDetails;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderCreateRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderHistoryRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderTrackRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticPaymentCreateRequest;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.model.Request.SearchRequest;
import com.nms.netmeds.diagnostic.model.Request.SelectLabRequest;
import com.nms.netmeds.diagnostic.model.Request.TimeSlotRequest;
import com.nms.netmeds.diagnostic.model.SearchDataResponse;
import com.nms.netmeds.diagnostic.model.SelectLabResponse;
import com.nms.netmeds.diagnostic.model.ShowCouponResponse;
import com.nms.netmeds.diagnostic.model.TestDetailResponse;
import com.nms.netmeds.diagnostic.model.TimeSlotResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitDiagnosticInterface {
    @GET(DiagnosticAPIPathConstant.ACTION_POPULAR_TEST)
    Call<PopularTestResponse> popularTest(@HeaderMap Map<String, String> authorization, @Query("pincode") String pinCode);

    @POST(DiagnosticAPIPathConstant.ACTION_PIN_CODE_STATUS)
    Call<PinCodeResponse> pinCodeServiceCheck(@HeaderMap Map<String, String> authorization, @Body PinCodeServiceCheckRequest checkRequest);

    @GET(DiagnosticAPIPathConstant.ACTION_BANNER)
    Call<OfferBannerResponse> offerBanner(@HeaderMap Map<String, String> authorization);

    @POST(DiagnosticAPIPathConstant.ACTION_GET_LABS)
    Call<SelectLabResponse> getLabsByTestId(@HeaderMap Map<String, String> authorization, @Body SelectLabRequest selectLabRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_SEARCH)
    Call<SearchDataResponse> getSearch(@HeaderMap Map<String, String> authorization, @Body SearchRequest searchRequest);

    @GET(DiagnosticAPIPathConstant.ACTION_POPULAR_PACKAGE)
    Call<DiagnosticPackageResponse> popularPackage(@HeaderMap Map<String, String> authorization, @Query("pincode") String pinCode);

    @GET(DiagnosticAPIPathConstant.ACTION_MOST_POPULAR_PACKAGE)
    Call<DiagnosticPackageResponse> mostPopularPackage(@HeaderMap Map<String, String> authorization, @Query("pincode") String pinCode, @Query("toppack") boolean toppack);

    @POST(DiagnosticAPIPathConstant.ACTION_TIME_SLOT)
    Call<TimeSlotResponse> timeSlot(@HeaderMap Map<String, String> authorization, @Body TimeSlotRequest timeSlotRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_ORDER_CREATE)
    Call<DiagnosticOrderCreateResponse> diagnosticOrderCreation(@HeaderMap Map<String, String> authorization, @Body DiagnosticOrderCreateRequest orderCreateRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_ORDER_HISTORY)
    Call<DiagnosticOrderHistoryResponse> diagnosticOrderHistory(@HeaderMap Map<String, String> authorization, @Body DiagnosticOrderHistoryRequest orderHistoryRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_ORDER_TRACK)
    Call<DiagnosticOrderTrackResponse> diagnosticOrderTrack(@HeaderMap Map<String, String> authorization, @Body DiagnosticOrderTrackRequest orderTrackRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_PAYMENT_CREATION)
    Call<DiagnosticPaymentCreateResponse> diagnosticPaymentCreation(@HeaderMap Map<String, String> authorization, @Body DiagnosticPaymentCreateRequest diagnosticPaymentCreateRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_PAYMENT_UPDATE)
    Call<DiagnosticUpdateResponse> diagnosticPaymentUpdate(@HeaderMap Map<String, String> authorization, @Body DiagnosticPaymentCreateRequest diagnosticPaymentCreateRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_ORDER_UPDATE)
    Call<DiagnosticUpdateResponse> diagnosticOrderUpdate(@HeaderMap Map<String, String> authorization, @Body DiagnosticOrderCreateRequest diagnosticOrderCreateRequest);

    @POST(DiagnosticAPIPathConstant.ACTION_SEARCH)
    Call<DiagnosticPackageResponse> getSearchPackage(@HeaderMap Map<String, String> authorization, @Body SearchRequest searchRequest);

    @GET(DiagnosticAPIPathConstant.ACTION_TEST_DETAIL + "/" + "{url}")
    Call<TestDetailResponse> getTestDetail(@Path("url") final String url, @Query("pincode") String pinCode, @HeaderMap Map<String, String> authorization);

    @POST(DiagnosticAPIPathConstant.DIAGNOSIS_PAYMENT_GATEWAY_LIST)
    Call<MStarBasicResponseTemplateModel> getDiagnosisPaymentList(@Body JsonObject request, @HeaderMap Map<String, String> header);

    @POST(DiagnosticAPIPathConstant.DIAGNOSIS_NMS_WALLET_BALANCE)
    Call<MStarBasicResponseTemplateModel> nmsDiagnosisWalletBalance(@Body JsonObject request);

    @POST(DiagnosticAPIPathConstant.DIAGNOSIS_APPLY_COUPON)
    Call<TestDetailResponse> applyDiagnosisCouponCode(@HeaderMap Map<String, String> authorization, @Body DiagnosticCouponDetails request);

    @GET(DiagnosticAPIPathConstant.DIAGNOSIS_SHOW_COUPON)
    Call<ShowCouponResponse> couponBox(@HeaderMap Map<String, String> authorization);

    @GET(DiagnosticAPIPathConstant.CUSTOMER_ADDRESS)
    Call<MStarBasicResponseTemplateModel> customerAddress(@HeaderMap Map<String, String> authorization);

    @PUT(DiagnosticAPIPathConstant.CUSTOMER_ADDRESS)
    Call<Object> putMagentoAddress(@Body PutMagentoAddressRequest request);


}
