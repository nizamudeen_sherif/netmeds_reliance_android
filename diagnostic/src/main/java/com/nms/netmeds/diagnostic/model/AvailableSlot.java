package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AvailableSlot implements Serializable {
    @SerializedName("availableSlotsCount")
    private int availableSlotsCount;
    @SerializedName("slotDay")
    private String slotDay;
    @SerializedName("slotTime")
    private List<SlotTime> slotTimeList;
    @SerializedName("slotDate")
    private String slotDate;
    @SerializedName("slotDay_short")
    private String slotDayShort;
    @SerializedName("slotDayDate")
    private String slotDayDate;

    public int getAvailableSlotsCount() {
        return availableSlotsCount;
    }

    public void setAvailableSlotsCount(int availableSlotsCount) {
        this.availableSlotsCount = availableSlotsCount;
    }

    public String getSlotDay() {
        return slotDay;
    }

    public void setSlotDay(String slotDay) {
        this.slotDay = slotDay;
    }

    public List<SlotTime> getSlotTimeList() {
        return slotTimeList;
    }

    public void setSlotTimeList(List<SlotTime> slotTimeList) {
        this.slotTimeList = slotTimeList;
    }

    public String getSlotDate() {
        return slotDate;
    }

    public void setSlotDate(String slotDate) {
        this.slotDate = slotDate;
    }

    public String getSlotDayShort() {
        return slotDayShort;
    }

    public void setSlotDayShort(String slotDayShort) {
        this.slotDayShort = slotDayShort;
    }

    public String getSlotDayDate() {
        return slotDayDate;
    }

    public void setSlotDayDate(String slotDayDate) {
        this.slotDayDate = slotDayDate;
    }
}
