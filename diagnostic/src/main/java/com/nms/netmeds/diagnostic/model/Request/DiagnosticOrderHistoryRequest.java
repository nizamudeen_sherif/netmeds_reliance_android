package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

public class DiagnosticOrderHistoryRequest {
    @SerializedName("userId")
    private String userId;

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
