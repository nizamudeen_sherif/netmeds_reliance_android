package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;
import com.nms.netmeds.base.model.Test;

import java.util.List;

public class SearchDataResponse extends BaseResponse {
    @SerializedName("result")
    private List<Test> result;
    @SerializedName("updatedOn")
    private String updatedOn;

    public List<Test> getResult() {
        return result;
    }

    public void setResult(List<Test> result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
