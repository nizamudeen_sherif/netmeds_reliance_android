package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.text.util.Linkify;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.diagnostic.databinding.DiagnosticNeedHelpBinding;

public class DiagnosticNeedHelpViewModel extends BaseViewModel {
    private DiagnosticNeedHelpViewModelListener needHelpViewModelListener;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private DiagnosticNeedHelpBinding diagnosticNeedHelpBinding;

    public DiagnosticNeedHelpViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DiagnosticNeedHelpBinding diagnosticNeedHelpBinding, DiagnosticNeedHelpViewModelListener needHelpViewModelListener) {
        this.mContext = context;
        this.diagnosticNeedHelpBinding = diagnosticNeedHelpBinding;
        this.needHelpViewModelListener = needHelpViewModelListener;
        if (!TextUtils.isEmpty(getNeedHelpContent()))
            setNeedHelpAutoLink();
    }

    private void setNeedHelpAutoLink() {
        diagnosticNeedHelpBinding.textNeedHelpInfo.setText(getNeedHelpContent());
        Linkify.addLinks(diagnosticNeedHelpBinding.textNeedHelpInfo, Linkify.EMAIL_ADDRESSES | Linkify.PHONE_NUMBERS);
    }

    public void onNeedHelpClose() {
        needHelpViewModelListener.onClose();
    }

    public interface DiagnosticNeedHelpViewModelListener {
        void onClose();
    }

    private String getNeedHelpContent() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(getApplication()).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDiagnosticsNeedHelpContent()) ? configurationResponse.getResult().getConfigDetails().getDiagnosticsNeedHelpContent() : "";
    }
}
