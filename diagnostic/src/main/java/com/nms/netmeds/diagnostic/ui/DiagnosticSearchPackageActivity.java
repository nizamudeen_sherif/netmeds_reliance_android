package com.nms.netmeds.diagnostic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivitySearchPackageBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResponse;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticSearchPackageViewModel;
import com.webengage.sdk.android.WebEngage;

import java.util.List;

public class DiagnosticSearchPackageActivity extends BaseViewModelActivity<DiagnosticSearchPackageViewModel> implements DiagnosticSearchPackageViewModel.DiagnosticSearchPackageListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener {

    private ActivitySearchPackageBinding searchPackageBinding;
    private DiagnosticSearchPackageViewModel searchPackageViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchPackageBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_package);
        toolBarSetUp(searchPackageBinding.toolbar);
        searchPackageBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected DiagnosticSearchPackageViewModel onCreateViewModel() {
        searchPackageViewModel = ViewModelProviders.of(this).get(DiagnosticSearchPackageViewModel.class);
        searchPackageViewModel.getSearchPackageMutableLiveData().observe(this, new DiagnosticSearchPackageObserver());
        intentValue();
        searchPackageViewModel.init(this, searchPackageBinding, searchPackageViewModel, this);
        return searchPackageViewModel;
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(DiagnosticConstant.PACKAGE))
                searchPackageViewModel.setPopularPackageList((List<DiagnosticPackage>) intent.getSerializableExtra(DiagnosticConstant.PACKAGE));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void vmShowProgress() {
        showProgress(DiagnosticSearchPackageActivity.this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmInitBottomView(List<Test> testList) {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), testList, DiagnosticConstant.PACKAGE, DiagnosticConstant.SEARCH_PACKAGE, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, "Bottom_fragment").commitAllowingStateLoss();
    }

    @Override
    public void vmPatientDetailCallback() {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_PACKAGE, searchPackageViewModel.getSelectedPackage());
        intent.putExtra(DiagnosticConstant.KEY_INTENT_FROM, DiagnosticConstant.SEARCH_PACKAGE);
        LaunchIntentManager.routeToActivity(DiagnosticSearchPackageActivity.this.getResources().getString(R.string.route_diagnostic_patient_detail), intent, DiagnosticSearchPackageActivity.this);
    }

    @Override
    public void onNavigation() {
        vmPatientDetailCallback();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {

    }

    @Override
    public void onUpdateSearchList(Test removeTest) {

    }

    @Override
    public void onUpdateLabList() {

    }

    public class DiagnosticSearchPackageObserver implements Observer<DiagnosticPackageResponse> {

        @Override
        public void onChanged(@Nullable DiagnosticPackageResponse diagnosticPackageResponse) {
            searchPackageViewModel.searchPackageDataAvailable(diagnosticPackageResponse);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_PACKAGE_SELECTION);
    }
}
