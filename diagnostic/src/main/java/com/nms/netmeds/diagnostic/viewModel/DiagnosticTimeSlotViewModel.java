package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DiagnosticTimeSlotViewPagerAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivitySelectTimeSlotBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.TimeSlotRequest;
import com.nms.netmeds.diagnostic.model.TimeSlotResponse;
import com.nms.netmeds.diagnostic.model.TimeSlotResult;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiagnosticTimeSlotViewModel extends AppViewModel {
    private int failedTransactionId = 0;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private ActivitySelectTimeSlotBinding activitySelectTimeSlotBinding;
    private DiagnosticTimeSlotViewPagerAdapter pagerAdapter;
    private DiagnosticTimeSlotViewModel timeSlotViewModel;
    private DiagnosticTimeSlotListener timeSlotListener;
    private MutableLiveData<TimeSlotResponse> timeSlotMutableLiveData = new MutableLiveData<>();
    private TimeSlotResult slotResult;
    private Bundle diagnosticOrderCreationDetail;
    private AvailableLab availableLab;
    private double totalAmount;
    private String diagnosticSelectedType = "";
    private DiagnosticPackage diagnosticPackage;
    private boolean skipTimeSlotApi = false;

    public boolean isSkipTimeSlotApi() {
        return skipTimeSlotApi;
    }

    public void setSkipTimeSlotApi(boolean skipTimeSlotApi) {
        this.skipTimeSlotApi = skipTimeSlotApi;
    }

    public String getDiagnosticSelectedType() {
        return diagnosticSelectedType;
    }

    public void setDiagnosticSelectedType(String diagnosticSelectedType) {
        this.diagnosticSelectedType = diagnosticSelectedType;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Bundle getDiagnosticOrderCreationDetail() {
        return diagnosticOrderCreationDetail;
    }

    public void setDiagnosticOrderCreationDetail(Bundle diagnosticOrderCreationDetail) {
        this.diagnosticOrderCreationDetail = diagnosticOrderCreationDetail;
    }

    public TimeSlotResult getSlotResult() {
        return slotResult;
    }

    public void setSlotResult(TimeSlotResult slotResult) {
        this.slotResult = slotResult;
    }

    public DiagnosticTimeSlotViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivitySelectTimeSlotBinding activitySelectTimeSlotBinding, DiagnosticTimeSlotViewModel timeSlotViewModel, DiagnosticTimeSlotListener timeSlotListener) {
        this.mContext = context;
        this.activitySelectTimeSlotBinding = activitySelectTimeSlotBinding;
        this.timeSlotViewModel = timeSlotViewModel;
        this.timeSlotListener = timeSlotListener;
        initBottomViewListener();
        setBottomView();
        if(isSkipTimeSlotApi()) return;
        getTimeSlot();
    }

    private void setBottomView() {
        if (getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST))
            setBottomViewTest();
        else
            setBottomViewPackage();
    }

    private void setBottomViewTest() {
        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_LAB)) {
            availableLab = (AvailableLab) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB);
            PriceDescription priceDescription = availableLab != null && availableLab.getPriceDescription() != null ? availableLab.getPriceDescription() : new PriceDescription();
            if (!TextUtils.isEmpty(priceDescription.getFinalPrice())) {
                setTotalAmount(Double.parseDouble(priceDescription.getFinalPrice()));
                activitySelectTimeSlotBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, Double.parseDouble(priceDescription.getFinalPrice())));
            }
            activitySelectTimeSlotBinding.testCount.textLabel.setText(mContext.getResources().getString(R.string.text_total_amount));
            noteVisibility();
        }
    }

    private void setBottomViewPackage() {
        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_PACKAGE)) {
            diagnosticPackage = (DiagnosticPackage) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE);
            PriceDescription priceDescription = diagnosticPackage != null && diagnosticPackage.getPriceDescription() != null ? diagnosticPackage.getPriceDescription() : new PriceDescription();
            if (!TextUtils.isEmpty(priceDescription.getFinalPrice())) {
                setTotalAmount(Double.parseDouble(priceDescription.getFinalPrice()));
                activitySelectTimeSlotBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, Double.parseDouble(priceDescription.getFinalPrice())));
            }
            activitySelectTimeSlotBinding.testCount.textLabel.setText(mContext.getResources().getString(R.string.text_total_amount));
            noteVisibility();
        }
    }

    private String getDiagnosticAuthToken() {
        if(BasePreference.getInstance(mContext) == null) return "";
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(mContext).getDiagnosticLoginResponse(), JustDocUserResponse.class);
        if(diagnosticLoginResult == null || TextUtils.isEmpty(diagnosticLoginResult.getToken())) return "";
        return diagnosticLoginResult.getToken();
    }


    private int getLabId() {
        int labId = 0;
        if (getDiagnosticSelectedType().equals(DiagnosticConstant.TEST))
            labId = availableLab != null && availableLab.getLabDescription() != null ? availableLab.getLabDescription().getId() : 0;
        else
            labId = diagnosticPackage != null && diagnosticPackage.getLabDescription() != null ? diagnosticPackage.getLabDescription().getId() : 0;
        return labId;
    }

    private int getLabPid() {
        if (availableLab == null || availableLab.getLabDescription() == null) return 0;
        return availableLab.getLabDescription().getPid();
    }

    private void initBottomViewListener() {
        activitySelectTimeSlotBinding.testCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeSlotListener.vmInitPaymentNavigation();
            }
        });
        activitySelectTimeSlotBinding.testCount.lytSelectedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Test> testList = null;
                if (getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST))
                    testList = availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? availableLab.getTestList() : new ArrayList<Test>();
                else
                    testList = diagnosticPackage != null && diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 ? diagnosticPackage.getTestList() : new ArrayList<Test>();

                timeSlotListener.vmInitBottomView(testList);
            }
        });
    }


    private TimeSlotRequest timeSlotRequest() {
        TimeSlotRequest timeSlotRequest = new TimeSlotRequest();
        timeSlotRequest.setPinCode(BasePreference.getInstance(mContext).getDiagnosticPinCode());
        if (isPathTestExist()) {
            timeSlotRequest.setLabId(getLabId());
            timeSlotRequest.setcId(0);
            timeSlotRequest.setCategory(DiagnosticConstant.CATEGORY_PATH);
        } else {
            timeSlotRequest.setLabId(getLabPid());
            timeSlotRequest.setcId(getLabId());
            timeSlotRequest.setCategory(DiagnosticConstant.CATEGORY_RADIO);
        }
        ArrayList<String> testIds = new ArrayList<>();
        if (availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0) {
            for (Test test : availableLab.getTestList()) {
                testIds.add(test.getTestId());
            }
        }
        timeSlotRequest.setTestids(testIds);
        return timeSlotRequest;
    }

    private boolean isPathTestExist(){
        return availableLab != null && availableLab.getTestList().size() > 0 && !TextUtils.isEmpty(availableLab.getTestList().get(0).getCategory())
                && availableLab.getTestList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_PATH);
    }

    private void getTimeSlot() {
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            timeSlotListener.vmShowProgress();
            DiagnosticServiceManager.getInstance().getTimeSlot(timeSlotViewModel, timeSlotRequest(), BasePreference.getInstance(mContext), getDiagnosticAuthToken());
        } else
            failedTransactionId = DiagnosticServiceManager.TIME_SLOT;
    }

    private void showNoNetworkView(boolean isConnected) {
        activitySelectTimeSlotBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        activitySelectTimeSlotBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        activitySelectTimeSlotBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        activitySelectTimeSlotBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }


    private void noteVisibility() {
        if(availableLab != null && availableLab.getTestList() != null
                && availableLab.getTestList().size() > 0 && !TextUtils.isEmpty(availableLab.getTestList().get(0).getCategory())
                && availableLab.getTestList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_RADIO)){
            activitySelectTimeSlotBinding.txtViewRequireFasting.setVisibility(View.GONE);
            activitySelectTimeSlotBinding.tvTimeSlotHint.setVisibility(View.GONE);
        }else{
            activitySelectTimeSlotBinding.txtViewRequireFasting.setVisibility(View.VISIBLE);
            activitySelectTimeSlotBinding.tvTimeSlotHint.setVisibility(View.VISIBLE);
        }
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    public MutableLiveData<TimeSlotResponse> getTimeSlotMutableLiveData() {
        return timeSlotMutableLiveData;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == DiagnosticServiceManager.TIME_SLOT) {
            TimeSlotResponse timeSlotResponse = new Gson().fromJson(data, TimeSlotResponse.class);
            timeSlotMutableLiveData.setValue(timeSlotResponse);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        timeSlotListener.vmDismissProgress();
        if (transactionId == DiagnosticServiceManager.TIME_SLOT) {
            vmOnError(transactionId);
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == DiagnosticServiceManager.TIME_SLOT) {
            getTimeSlot();
        }
    }

    public interface DiagnosticTimeSlotListener {

        void vmShowProgress();

        void vmDismissProgress();

        void vmInitViewPager(TimeSlotResult slotResult);

        void vmInitPaymentNavigation();

        void vmInitBottomView(List<Test> testList);
    }

    public void timeSlotDataAvailable(TimeSlotResponse timeSlotResponse) {
        timeSlotListener.vmDismissProgress();
        if (timeSlotResponse != null) {
            timeSlotServiceStatus(timeSlotResponse);
        } else
            vmOnError(DiagnosticServiceManager.TIME_SLOT);
    }

    private void timeSlotServiceStatus(TimeSlotResponse timeSlotResponse) {
        if (timeSlotResponse.getServiceStatus() != null && timeSlotResponse.getServiceStatus().getStatusCode() != null && timeSlotResponse.getServiceStatus().getStatusCode() == 200) {
            timeSlotResult(timeSlotResponse);
        } else
            vmOnError(DiagnosticServiceManager.TIME_SLOT);
    }

    private void timeSlotResult(TimeSlotResponse timeSlotResponse) {
        if (timeSlotResponse.getResult() != null) {
            setSlotResult(timeSlotResponse.getResult());
            timeSlotListener.vmInitViewPager(getSlotResult());
        } else
            vmOnError(DiagnosticServiceManager.TIME_SLOT);
    }
}
