package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.Test;

import java.io.Serializable;
import java.util.List;

public class PopularTestResult implements Serializable {
    @SerializedName("products")
    private List<Test> popularTestList;

    public List<Test> getPopularTestList() {
        return popularTestList;
    }

    public void setPopularTestList(List<Test> popularTestList) {
        this.popularTestList = popularTestList;
    }
}
