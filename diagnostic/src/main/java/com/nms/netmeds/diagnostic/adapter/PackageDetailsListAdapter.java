package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterPackageListItemBinding;


class PackageDetailsListAdapter extends RecyclerView.Adapter<PackageDetailsListAdapter.PackageDetailsListAdapterViewHolder> {

    private Context context;

    public PackageDetailsListAdapter(Context context, Object obj) {
        this.context = context;
    }

    @NonNull
    @Override
    public PackageDetailsListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterPackageListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_package_list_item, viewGroup, false);
        return new PackageDetailsListAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageDetailsListAdapterViewHolder holder, int position) {
        holder.binding.tvItemName.setText("Routine body check-up" + position);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class PackageDetailsListAdapterViewHolder extends RecyclerView.ViewHolder {
        private AdapterPackageListItemBinding binding;

        public PackageDetailsListAdapterViewHolder(@NonNull AdapterPackageListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
