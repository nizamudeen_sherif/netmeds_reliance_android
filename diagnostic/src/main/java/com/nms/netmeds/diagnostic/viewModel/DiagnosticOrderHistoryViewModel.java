package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.consultation.model.request.LoginRequest;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.adapter.DiagnosticOrderHistoryAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticHistoryBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderHistoryResponse;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderHistoryRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiagnosticOrderHistoryViewModel extends AppViewModel {
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private ActivityDiagnosticHistoryBinding diagnosticHistoryBinding;
    private DiagnosticOrderHistoryViewModel diagnosticOrderHistoryViewModel;
    private DiagnosticOrderHistoryListener diagnosticOrderHistoryListener;
    private int failedTransactionId;
    private MutableLiveData<DiagnosticOrderHistoryResponse> orderHistoryMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<LoginResponse> diagnosticLoginResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<LoganResponse> loganMutableLiveData = new MutableLiveData<>();
    private DiagnosticOrderHistoryAdapter mOrderHistoryAdapter;
    private List<DiagnosticOrder> diagnosticOrderList;
    private BasePreference mBasePreference;
    private String loganSession;

    private String getLoganSession() {
        return loganSession;
    }

    private void setLoganSession(String loganSession) {
        this.loganSession = loganSession;
    }

    public DiagnosticOrderHistoryViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticHistoryBinding diagnosticHistoryBinding, DiagnosticOrderHistoryViewModel diagnosticOrderHistoryViewModel, DiagnosticOrderHistoryListener diagnosticOrderHistoryListener) {
        this.mContext = context;
        this.diagnosticHistoryBinding = diagnosticHistoryBinding;
        this.diagnosticOrderHistoryViewModel = diagnosticOrderHistoryViewModel;
        this.diagnosticOrderHistoryListener = diagnosticOrderHistoryListener;
        diagnosticOrderList = new ArrayList<>();
        mBasePreference = BasePreference.getInstance(mContext);
        setLoganSession(mBasePreference.getLoganSession());
        initHistoryAdapter();
        checkDiagnosticToken();
    }

    private void initHistoryAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        diagnosticHistoryBinding.diagnosticOrderList.setLayoutManager(linearLayoutManager);
        mOrderHistoryAdapter = new DiagnosticOrderHistoryAdapter(mContext, diagnosticOrderList);
        diagnosticHistoryBinding.diagnosticOrderList.setAdapter(mOrderHistoryAdapter);
    }

    private void checkDiagnosticToken() {
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(mBasePreference.getDiagnosticLoginResponse(), JustDocUserResponse.class);
        String diagnosticToken = diagnosticLoginResult != null && diagnosticLoginResult.getToken() != null ? diagnosticLoginResult.getToken() : "";
        if (!TextUtils.isEmpty(diagnosticToken))
            getOrderHistory();
        else
            diagnosticLogin();
    }

    public MutableLiveData<DiagnosticOrderHistoryResponse> getOrderHistoryMutableLiveData() {
        return orderHistoryMutableLiveData;
    }

    public MutableLiveData<LoginResponse> getLoginResponseMutableLiveData() {
        return diagnosticLoginResponseMutableLiveData;
    }

    public MutableLiveData<LoganResponse> getLoganMutableLiveData() {
        return loganMutableLiveData;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_HISTORY:
                DiagnosticOrderHistoryResponse orderHistoryResponse = new Gson().fromJson(data, DiagnosticOrderHistoryResponse.class);
                orderHistoryMutableLiveData.setValue(orderHistoryResponse);
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
                diagnosticLoginResponseMutableLiveData.setValue(loginResponse);
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                LoganResponse loganResponse = new Gson().fromJson(data, LoganResponse.class);
                loganMutableLiveData.setValue(loganResponse);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        diagnosticOrderHistoryListener.vmDismissProgress();
        switch (transactionId) {
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_HISTORY:
                vmOnError(transactionId);
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                vmOnError(transactionId);
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                diagnosticOrderHistoryListener.forceLogout();
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_HISTORY:
                getOrderHistory();
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                diagnosticLogin();
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                diagnosticOrderHistoryListener.vmShowProgress();
                getLogan();
                break;
        }
    }

    public interface DiagnosticOrderHistoryListener {
        void vmShowProgress();

        void vmDismissProgress();

        void forceLogout();

        Context getContext();
    }

    private void showNoNetworkView(boolean isConnected) {
        diagnosticHistoryBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        diagnosticHistoryBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
        diagnosticHistoryBinding.diagnosticEmptyHistoryView.setVisibility(View.GONE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        diagnosticHistoryBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        diagnosticHistoryBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
        diagnosticHistoryBinding.diagnosticEmptyHistoryView.setVisibility(View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void getOrderHistory() {
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(mContext).getDiagnosticLoginResponse(), JustDocUserResponse.class);
            String userId = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getId()) ? diagnosticLoginResult.getId() : "";
            DiagnosticOrderHistoryRequest orderHistoryRequest = new DiagnosticOrderHistoryRequest();
            orderHistoryRequest.setUserId(userId);
            diagnosticOrderHistoryListener.vmShowProgress();
            DiagnosticServiceManager.getInstance().diagnosticOrderHistory(diagnosticOrderHistoryViewModel, orderHistoryRequest, BasePreference.getInstance(mContext));
        } else
            failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_ORDER_HISTORY;
    }

    public void orderHistoryDataAvailable(DiagnosticOrderHistoryResponse historyResponse) {
        diagnosticOrderHistoryListener.vmDismissProgress();
        if (historyResponse != null) {
            orderHistoryServiceStatus(historyResponse);
        } else
            vmOnError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_HISTORY);
    }

    private void orderHistoryServiceStatus(DiagnosticOrderHistoryResponse historyResponse) {
        if (historyResponse.getServiceStatus() != null && historyResponse.getServiceStatus().getStatusCode() != null && historyResponse.getServiceStatus().getStatusCode() == 200) {
            historyResult(historyResponse);
        } else
            setHistoryError(historyResponse);
    }

    private void setHistoryError(DiagnosticOrderHistoryResponse historyResponse) {
        if (historyResponse.getServiceStatus() != null && historyResponse.getServiceStatus().getStatusCode() != null && historyResponse.getServiceStatus().getStatusCode() == 400) {
            historyResult(historyResponse);
        } else
            vmOnError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_HISTORY);
    }

    private void historyResult(DiagnosticOrderHistoryResponse historyResponse) {
        if (historyResponse.getOrderResultList() != null && historyResponse.getOrderResultList().size() > 0) {
            diagnosticOrderList.clear();
            diagnosticOrderList.addAll(historyResponse.getOrderResultList());
            mOrderHistoryAdapter.updateOrderHistoryAdapter(diagnosticOrderList);
            diagnosticHistoryBinding.diagnosticEmptyHistoryView.setVisibility(View.GONE);
            diagnosticHistoryBinding.diagnosticOrderList.setVisibility(View.VISIBLE);
        } else {
            diagnosticHistoryBinding.diagnosticEmptyHistoryView.setVisibility(View.VISIBLE);
            diagnosticHistoryBinding.diagnosticOrderList.setVisibility(View.GONE);
        }
    }

    private void diagnosticLogin() {
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            LoginRequest diagnosticLoginRequest = new LoginRequest();
            diagnosticLoginRequest.setSessionId(getLogonSessionId());
            diagnosticLoginRequest.setSource(DiagnosticConstant.SOURCE);
            diagnosticLoginRequest.setAppversion(CommonUtils.getAppVersionCode(mContext));
            diagnosticLoginRequest.setMedium(DiagnosticConstant.APP_SOURCE);
            diagnosticLoginRequest.setCustomer_id(!TextUtils.isEmpty(BasePreference.getInstance(mContext).getMstarCustomerId())
                    ? BasePreference.getInstance(mContext).getMstarCustomerId() : "");
            diagnosticOrderHistoryListener.vmShowProgress();
            ConsultationServiceManager.getInstance().consultationDoLogin(this, diagnosticLoginRequest, ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL));
        } else
            failedTransactionId = ConsultationServiceManager.TRANSACTION_LOGIN;
    }

    public void diagnosticLoginDataAvailable(LoginResponse loginResponse) {
        if (loginResponse != null) {
            loginServiceStatus(loginResponse);
        } else
            vmOnError(ConsultationServiceManager.TRANSACTION_LOGIN);
    }

    private void loginServiceStatus(LoginResponse loginResponse) {
        if (loginResponse.getServiceStatus() != null && loginResponse.getServiceStatus().getStatusCode() != null && loginResponse.getServiceStatus().getStatusCode() == 200) {
            loginResult(loginResponse);
        } else {
            diagnosticHistoryBinding.lytViewContent.setVisibility(View.GONE);
            validateLoginError(loginResponse);
        }
    }

    private void loginResult(LoginResponse loginResponse) {
        if (loginResponse.getResult() != null) {
            mBasePreference.setDiagnosticLoinResponse(new Gson().toJson(loginResponse.getResult()));
            diagnosticToken(loginResponse.getResult());
            /*WebEngage diagnostic register event*/
            WebEngageHelper.getInstance().diagnosticsRegistrationEvent(mBasePreference, diagnosticOrderHistoryListener.getContext());
        } else {
            diagnosticOrderHistoryListener.vmDismissProgress();
            diagnosticHistoryBinding.lytViewContent.setVisibility(View.GONE);
        }
    }

    private void diagnosticToken(JustDocUserResponse result) {
        diagnosticOrderHistoryListener.vmDismissProgress();
        if (!TextUtils.isEmpty(result.getToken())) {
            diagnosticHistoryBinding.lytViewContent.setVisibility(View.VISIBLE);
            getOrderHistory();
        } else {
            diagnosticHistoryBinding.lytViewContent.setVisibility(View.GONE);
        }
    }

    private void validateLoginError(LoginResponse loginResponse) {
        if (loginResponse.getResult() != null && !TextUtils.isEmpty(loginResponse.getResult().getMessage()))
            validateSessionToken(loginResponse.getResult());
        else
            diagnosticOrderHistoryListener.vmDismissProgress();
    }

    private void validateSessionToken(JustDocUserResponse justDocUserResponse) {
        if (justDocUserResponse.getMessage().equals(ConsultationConstant.SESSION_ID_NOT_FOUND) || justDocUserResponse.getMessage().contains(ConsultationConstant.INVALID_SESSION_ID))
            getLogan();
        else {
            diagnosticOrderHistoryListener.vmDismissProgress();
            SnackBarHelper.snackBarCallBack(diagnosticHistoryBinding.lytViewContent, mContext, justDocUserResponse.getMessage());
        }
    }

    private void getLogan() {
        if (TextUtils.isEmpty(mBasePreference.getMstarCustomerId()) || TextUtils.isEmpty(mBasePreference.getMStarSessionId())) {
            diagnosticOrderHistoryListener.vmDismissProgress();
            diagnosticOrderHistoryListener.forceLogout();
            return;
        }
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            Map<String, String> header = new HashMap<>();
            header.put(AppConstant.MSTAR_AUTH_TOKEN, mBasePreference.getMStarSessionId());
            header.put(AppConstant.MSTAR_API_HEADER_USER_ID, mBasePreference.getMstarCustomerId());
            ConsultationServiceManager.getInstance().getLoganToken(this, header);
        } else
            failedTransactionId = ConsultationServiceManager.LOGAN_TOKEN;
    }

    public void onLoganTokenDataAvailable(LoganResponse loganResponse) {
        if (loganResponse != null) {
            logonServiceStatus(loganResponse);
        } else {
            diagnosticOrderHistoryListener.forceLogout();
        }
    }

    private void logonServiceStatus(LoganResponse loganResponse) {
        if (!TextUtils.isEmpty(loganResponse.getStatus()) && loganResponse.getStatus().equalsIgnoreCase(DiagnosticConstant.SUCCESS)) {
            validateSessionId(loganResponse);
        } else {
            diagnosticOrderHistoryListener.forceLogout();
        }
    }

    private void validateSessionId(LoganResponse loganResponse) {
        diagnosticOrderHistoryListener.vmDismissProgress();
        if (loganResponse.getLoganResult() != null && !TextUtils.isEmpty(loganResponse.getLoganResult().getSessionToken())) {
            setLoganSession(loganResponse.getLoganResult().getSessionToken());
            BasePreference.getInstance(mContext).setLoganSession(loganResponse.getLoganResult().getSessionToken());
            diagnosticLogin();
        }
    }

    private String getLogonSessionId() {
        return getLoganSession() != null ? getLoganSession() : "";
    }
}
