package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class DiagnosticPaymentCreateResponse extends BaseResponse {
    @SerializedName("result")
    private DiagnosticPaymentCreateResult result;
    @SerializedName("updatedOn")
    private String updatedOn;

    public DiagnosticPaymentCreateResult getResult() {
        return result;
    }

    public void setResult(DiagnosticPaymentCreateResult result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
