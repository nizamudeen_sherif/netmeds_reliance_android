package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SlotTime implements Serializable {
    @SerializedName("expectedTime")
    private String expectedTime;
    @SerializedName("slotDay")
    private String slotDay;
    @SerializedName("slotDate")
    private String slotDate;
    @SerializedName("slotDay_short")
    private String slotDayShort;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("endTime")
    private String endTime;
    @SerializedName("timeRange")
    private String timeRange;
    @SerializedName("slotId")
    private String slotId;
    @SerializedName("slotDayDate")
    private String slotDayDate;
    @SerializedName("state_id")
    private String stateId;
    @SerializedName("state")
    private String state;
    private boolean checked;

    public String getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(String expectedTime) {
        this.expectedTime = expectedTime;
    }

    public String getSlotDay() {
        return slotDay;
    }

    public void setSlotDay(String slotDay) {
        this.slotDay = slotDay;
    }

    public String getSlotDate() {
        return slotDate;
    }

    public void setSlotDate(String slotDate) {
        this.slotDate = slotDate;
    }

    public String getSlotDayShort() {
        return slotDayShort;
    }

    public void setSlotDayShort(String slotDayShort) {
        this.slotDayShort = slotDayShort;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getSlotDayDate() {
        return slotDayDate;
    }

    public void setSlotDayDate(String slotDayDate) {
        this.slotDayDate = slotDayDate;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
