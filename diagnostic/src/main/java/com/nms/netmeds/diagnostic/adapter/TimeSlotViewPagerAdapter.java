package com.nms.netmeds.diagnostic.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nms.netmeds.diagnostic.model.WeekTabView;

import java.util.ArrayList;
import java.util.List;

public class TimeSlotViewPagerAdapter extends FragmentStatePagerAdapter {
    public List<WeekTabView> tabs = new ArrayList<>();

    public TimeSlotViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position).getTimeSlotFragment();
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    public void addFragment(WeekTabView tab) {
        tabs.add(tab);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).getTabName();
    }

    public void updateTabAdapter(List<WeekTabView> updateTabs){
        this.tabs=updateTabs;
        notifyDataSetChanged();
    }

    public void clearTimeSlot(){
        tabs.clear();
    }
}

