package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PinCodeResult implements Serializable {
    @SerializedName("service")
    private boolean service;
    @SerializedName("pinCode")
    private String pinCode;
    @SerializedName("message")
    private String message;
    @SerializedName("city")
    private String city;
    @SerializedName("state")
    private String state;

    public boolean getService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
