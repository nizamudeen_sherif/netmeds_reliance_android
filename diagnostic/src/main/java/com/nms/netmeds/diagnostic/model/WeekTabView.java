package com.nms.netmeds.diagnostic.model;

import com.nms.netmeds.diagnostic.ui.DiagnosticTimeSlotFragment;

public class WeekTabView {
    private String tabName;
    private DiagnosticTimeSlotFragment timeSlotFragment;

    public WeekTabView(String tabName, DiagnosticTimeSlotFragment timeSlotFragment) {
        this.tabName = tabName;
        this.timeSlotFragment = timeSlotFragment;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public DiagnosticTimeSlotFragment getTimeSlotFragment() {
        return timeSlotFragment;
    }

    public void setTimeSlotFragment(DiagnosticTimeSlotFragment timeSlotFragment) {
        this.timeSlotFragment = timeSlotFragment;
    }
}
