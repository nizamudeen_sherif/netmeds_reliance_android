package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DiagnosticCartItemAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticOrderReviewBinding;
import com.nms.netmeds.diagnostic.model.Accreditation;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticCoupon;
import com.nms.netmeds.diagnostic.model.OfferBannerResponse;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticApplyCouponRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticCouponDetails;
import com.nms.netmeds.diagnostic.model.ShowCouponResponse;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.model.TestDetailResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;

public class DiagnosticOrderReviewViewModel extends AppViewModel{
    private ActivityDiagnosticOrderReviewBinding diagnosticOrderReviewBinding;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private OrderReviewListener listener;
    private SlotTime slotTime;
    private double totalAmount;
    private AvailableLab availableLab;
    private int failedTransactionId;
    private TestDetailResponse testDetailResponse;
    private ShowCouponResponse showCouponResponse;
    public double mrpTotalAmount, discountAmount, originalAmount, additionalAmount, strikePrice;
    private int percentage;
    private MutableLiveData<OfferBannerResponse> labDetailsMutableLiveData= new MutableLiveData<>();

    public DiagnosticOrderReviewViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticOrderReviewBinding diagnosticOrderReviewBinding, OrderReviewListener listener) {
        this.mContext = context;
        this.diagnosticOrderReviewBinding = diagnosticOrderReviewBinding;
        this.listener = listener;
        if(!isLabCertificationAvailable()) {
            initiateAPICall(DiagnosticServiceManager.OFFER_BANNER, null);
        }
    }

    // check if lab certification data already available
    private boolean isLabCertificationAvailable(){
        return labDetailsMutableLiveData!=null && labDetailsMutableLiveData.getValue()!=null
                && labDetailsMutableLiveData.getValue().getOfferBannerResult()!=null
                && labDetailsMutableLiveData.getValue().getOfferBannerResult().getLab_details()!=null;
    }

    private void initCartAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        diagnosticOrderReviewBinding.labTestList.setNestedScrollingEnabled(false);
        diagnosticOrderReviewBinding.labTestList.setLayoutManager(linearLayoutManager);
        ArrayList<Test> tempList = new ArrayList<>();
        if(isLabAvailable() && isLabTestAvailable()){
            DiagnosticHelper.getSelectedList().clear();
            DiagnosticHelper.getSelectedList().addAll(getAvailableLab().getTestList());
            tempList.addAll(getAvailableLab().getTestList());
        }
        DiagnosticCartItemAdapter diagnosticCartItemAdapter = new DiagnosticCartItemAdapter(mContext, tempList);
        diagnosticOrderReviewBinding.labTestList.setAdapter(diagnosticCartItemAdapter);
    }

    public String labTestOrPackage() {
        sampleCollectionChargesVisibility();
        if (isLabAvailable() && isLabTestAvailable() && getAvailableLab().getTestList().size() == 1
                && !TextUtils.isEmpty(getAvailableLab().getTestList().get(0).getType()) && (getAvailableLab().getTestList().get(0).getType()
                .equalsIgnoreCase(DiagnosticConstant.PACKAGE) || getAvailableLab().getTestList().get(0).getType().equalsIgnoreCase(DiagnosticConstant.PACKAGES)))
            return DiagnosticConstant.PACKAGES;
        return mContext.getResources().getString(R.string.text_lab_test);
    }


    public void sampleCollectionChargesVisibility() {
        if (availableLab != null && availableLab.getTestList() != null
                && availableLab.getTestList().size() > 0 && !TextUtils.isEmpty(availableLab.getTestList().get(0).getCategory())
                && availableLab.getTestList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_RADIO)){
            diagnosticOrderReviewBinding.lytDeliveryCharge.setVisibility(View.GONE);
            diagnosticOrderReviewBinding.deliveryChargeInfo.setVisibility(View.GONE);
        }else{
            diagnosticOrderReviewBinding.lytDeliveryCharge.setVisibility(View.VISIBLE);
            diagnosticOrderReviewBinding.deliveryChargeInfo.setVisibility(View.VISIBLE);
        }
    }

    public String sampleAddress() {
        if (isLabTestAvailable() && !TextUtils.isEmpty(getAvailableLab().getTestList().get(0).getCategory())
                && getAvailableLab().getTestList().get(0).getCategory().equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)) {
            diagnosticOrderReviewBinding.txtSamplePickUpAddress.setText(mContext.getResources().getString(R.string.txt_lab_address));
            diagnosticOrderReviewBinding.patientName.setVisibility(View.GONE);
            if (getAvailableLab().getLabDescription() != null && getAvailableLab().getLabDescription().getLabAddressList() != null
                    && !TextUtils.isEmpty(getAvailableLab().getLabDescription().getLabAddressList().get(0).getFullAddress())) {
                return getAvailableLab().getLabDescription().getLabAddressList().get(0).getFullAddress();
            }
            return "";
        }
        if (PaymentHelper.getCustomerAddress() == null) return "";
        setBillingAndShippingAddress(PaymentHelper.getCustomerAddress());
        StringBuilder patientName = new StringBuilder();
        MStarAddressModel address = PaymentHelper.getCustomerAddress();
        patientName.append(CommonUtils.validateString(address.getFirstname()));
        patientName.append(" ");
        patientName.append(CommonUtils.validateString(address.getLastname()));
        diagnosticOrderReviewBinding.patientName.setText(CommonUtils.validateString(patientName.toString()));
        return CommonUtils.getAddressFromObject(address);
    }

    public String couponName() {
        if (isCouponAvailable() && !TextUtils.isEmpty(testDetailResponse.getResult().getCoupon().getCode()))
            return testDetailResponse.getResult().getCoupon().getCode().toUpperCase();
        return "";
    }

    public void removeCoupon() {
        discountAmount = 0;
        originalAmount = totalAmount;
        percentage = 0;
        diagnosticOrderReviewBinding.lLayoutApplyNow.setVisibility(View.VISIBLE);
        diagnosticOrderReviewBinding.lLayoutApplied.setVisibility(View.GONE);
        diagnosticOrderReviewBinding.lytDiscount.setVisibility(View.GONE);
        diagnosticOrderReviewBinding.editTextCoupon.setText("");
        diagnosticOrderReviewBinding.textTotalAmount.setText(CommonUtils.getPriceInFormat(getTotalAmount()));
        diagnosticOrderReviewBinding.totalAmount.setText(CommonUtils.getPriceInFormat(getTotalAmount()));
        testDetailResponse = null;
        diagnosticOrderReviewBinding.textPromoCode.setText(couponName());
        diagnosticOrderReviewBinding.txtViewOfferApplied.setText(appliedOfferText());
        diagnosticOrderReviewBinding.textAdditionalDiscount.setText(discountAmount());
        totalSaving();
        diagnosticOrderReviewBinding.btnProceed.setText(mContext.getResources().getString(R.string.text_pay));
    }

    private boolean isTestResultNotEmpty() {
        return testDetailResponse != null && testDetailResponse.getResult() != null;
    }

    public boolean isCouponAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getCoupon() != null;
    }

    public DiagnosticCoupon getCouponObject() {
        return testDetailResponse.getResult().getCoupon();
    }

    private boolean isOriginalPriceAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getOriginalPrice() != null;
    }

    private boolean isPriceDescriptionAvailable() {
        return testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getPriceDescription() != null;
    }

    private boolean isFinalPriceAvailable() {
        return isPriceDescriptionAvailable() && !TextUtils.isEmpty(testDetailResponse.getResult().getPriceDescription().getFinalPrice());
    }


    private boolean isLabTestAvailable() {
        return getAvailableLab() != null && getAvailableLab().getTestList() != null && getAvailableLab().getTestList().size() > 0;
    }

    private boolean isLabAvailable() {
        return getAvailableLab() != null && getAvailableLab().getTestList() != null;
    }

    public SlotTime getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(SlotTime slotTime) {
        this.slotTime = slotTime;
    }

    public String labName() {
        setLabLogo();
        if (isLabAvailable() && availableLab.getLabDescription() != null && !TextUtils.isEmpty(availableLab.getLabDescription().getLabName())) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(availableLab.getLabDescription().getLabName());
            if (availableLab.getLabDescription().getAccreditation() != null && availableLab.getLabDescription().getAccreditation().size() > 0) {
                stringBuilder.append(" (");
                for (Accreditation accreditation : availableLab.getLabDescription().getAccreditation()) {
                    if (accreditation != null && !TextUtils.isEmpty(accreditation.getName())) {
                        stringBuilder.append(accreditation.getName());
                        stringBuilder.append(", ");
                    }
                }
                stringBuilder.append(")");
            }
            return stringBuilder.toString();
        }
        return "";
    }

    private void setLabLogo() {
        try {
            if (isLabAvailable() && availableLab.getLabDescription() != null && !TextUtils.isEmpty(availableLab.getLabDescription().getLogo())) {
                DiagnosticHelper.populateImage(listener.getContext(),availableLab.getLabDescription().getLogo(), diagnosticOrderReviewBinding.labLogo, R.drawable.ic_lab_report);
            }
        }catch (Exception e){
            diagnosticOrderReviewBinding.labLogo.setImageResource(R.drawable.ic_lab_report);
            e.printStackTrace();
        }
    }


    public String timeSlot() {
        if (getSlotTime() == null || TextUtils.isEmpty(getSlotTime().getStartTime()) || TextUtils.isEmpty(getSlotTime().getEndTime())
                || TextUtils.isEmpty(getSlotTime().getSlotDate()))
            return "";

        return String.format("%s, %s - %s", DateTimeUtils.getInstance().stringDate(getSlotTime().getSlotDate(), DateTimeUtils.yyyyMMdd,
                DateTimeUtils.dd_MMM), getSlotTime().getStartTime(), getSlotTime().getEndTime());
    }

    public String discountAmount() {
        if (isCouponAvailable() && testDetailResponse.getResult().getCoupon().getDiscountPercent() > 0) {
            percentage = testDetailResponse.getResult().getCoupon().getDiscountPercent();
            discountAmount = (originalAmount * percentage / 100);
            return CommonUtils.getPriceInFormat((discountAmount));
        }
        return CommonUtils.getPriceInFormat(0);
    }

    public String appliedOfferText() {
        if (isCouponAvailable() && !TextUtils.isEmpty(testDetailResponse.getResult().getCoupon().getCode()))
            return mContext.getResources().getString(R.string.txt_coupon_code_applied);
        return mContext.getResources().getString(R.string.txt_apply_coupon_code);
    }

    public String deliveryCharges() {
        return CommonUtils.getPriceInFormat(0);
    }

    public String mrpAmount() {
        if (isLabAvailable() && isLabTestAvailable()) {
            for (Test test : getAvailableLab().getTestList()) {
                if (test.getPriceInfo() != null) {
                    mrpTotalAmount = mrpTotalAmount + Integer.parseInt(test.getPriceInfo().getOfferedPrice());
                }
            }
        }
        additionalAmount();
        return CommonUtils.getPriceInFormat(mrpTotalAmount);
    }


    private void additionalAmount() {
        if (isLabAvailable() && isLabTestAvailable()) {
            for (Test test : getAvailableLab().getTestList()) {
                if (test.getPriceInfo() != null) {
                    strikePrice = strikePrice + Integer.parseInt(test.getPriceInfo().getFinalPrice());
                }
            }
        }
        additionalAmount = mrpTotalAmount - strikePrice;
        diagnosticOrderReviewBinding.additionalDiscount.setText(CommonUtils.getPriceInFormat(strikePrice - mrpTotalAmount));
        totalSaving();
    }

    public String totalAmount() {
        if (isCouponAvailable() && isFinalPriceAvailable()) {
            originalAmount = Double.parseDouble(testDetailResponse.getResult().getPriceDescription().getFinalPrice());
            return CommonUtils.getPriceInFormat(originalAmount);
        }
        return CommonUtils.getPriceInFormat(originalAmount);
    }

    private void totalSaving() {
        if (isCouponAvailable() && testDetailResponse.getResult().getCoupon().getDiscountPercent() > 0) {
            percentage = testDetailResponse.getResult().getCoupon().getDiscountPercent();
            discountAmount = (originalAmount * percentage / 100);
            diagnosticOrderReviewBinding.textTotalSavings.setText(CommonUtils.getPriceInFormat(additionalAmount + discountAmount));
            return;
        }
        diagnosticOrderReviewBinding.textTotalSavings.setText(CommonUtils.getPriceInFormat(additionalAmount));
    }

    public void proceedForPayment() {
        listener.proceedForPayment();
    }

    public interface OrderReviewListener {
        void proceedForPayment();

        void vmShowProgress();

        void vmDismissProgress();

        void finishMe(boolean flag);

        Context getContext();
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.originalAmount = totalAmount;
        this.totalAmount = totalAmount;
    }

    public AvailableLab getAvailableLab() {
        return availableLab;
    }

    public void setAvailableLab(AvailableLab availableLab) {
        this.availableLab = availableLab;
    }

    public void applyCoupon() {
        if (TextUtils.isEmpty(diagnosticOrderReviewBinding.editTextCoupon.getText())) return;
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(mContext).getDiagnosticLoginResponse(), JustDocUserResponse.class);
        String userId = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getId()) ? diagnosticLoginResult.getId() : "";
        DiagnosticApplyCouponRequest request = new DiagnosticApplyCouponRequest();
        DiagnosticCouponDetails couponDetails = new DiagnosticCouponDetails();
        if (getAvailableLab() != null && getAvailableLab().getLabDescription() != null && getAvailableLab().getPriceDescription() != null
                && DiagnosticHelper.getSelectedList() != null && DiagnosticHelper.getSelectedList().size() > 0 && !TextUtils.isEmpty(userId)) {
            request.setLabDescription(getAvailableLab().getLabDescription());
            request.setPriceDescription(getAvailableLab().getPriceDescription());
            request.setTests(DiagnosticHelper.getSelectedList());
            couponDetails.setDiagnosticApplyCouponRequest(request);
            couponDetails.setCoupon(diagnosticOrderReviewBinding.editTextCoupon.getText().toString());
            couponDetails.setUserId(Integer.parseInt(userId));

        } else {
            listener.vmDismissProgress();
            failedTransactionId = DiagnosticServiceManager.DIAGNOSIS_APPLY_COUPON;
            return;
        }
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            listener.vmShowProgress();
            DiagnosticServiceManager.getInstance().applyCoupon(this, BasePreference.getInstance(mContext), couponDetails);
        } else {
            listener.vmDismissProgress();
            failedTransactionId = DiagnosticServiceManager.DIAGNOSIS_APPLY_COUPON;
        }
    }

    public void couponBox() {
        initCartAdapter();
        boolean isConnected = NetworkUtils.isConnected(mContext);
        showNoNetworkView(isConnected);
        if (isConnected) {
            DiagnosticServiceManager.getInstance().showCoupon(this, BasePreference.getInstance(mContext));
        } else {
            failedTransactionId = DiagnosticServiceManager.DIAGNOSIS_SHOW_COUPON;
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        listener.vmDismissProgress();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                BasePreference.getInstance(mContext).setPreviousCartShippingAddressId(PaymentHelper.getCustomerAddress() == null ? 0 : PaymentHelper.getCustomerAddress().getId());
                BasePreference.getInstance(mContext).setPreviousCartBillingAddressId(PaymentHelper.getCustomerAddress() == null ? 0 : PaymentHelper.getCustomerAddress().getId());
                BasePreference.getInstance(mContext).setDiagDefaultAddressId(PaymentHelper.getCustomerAddress().getId());
                BasePreference.getInstance(mContext).setChangeDiagnosticHome(true);
                break;
            case DiagnosticServiceManager.DIAGNOSIS_APPLY_COUPON: {
                if (!TextUtils.isEmpty(data)) {
                    testDetailResponse = new Gson().fromJson(data, TestDetailResponse.class);
                    if (isTestResultNotEmpty() && testDetailResponse.getServiceStatus() != null && testDetailResponse.getServiceStatus().getStatusCode() == DiagnosticConstant.STATUS_CODE_400 &&
                            testDetailResponse.getServiceStatus().getStatus().equalsIgnoreCase(mContext.getResources().getString(R.string.value_false))) {
                        SnackBarHelper.snackBarCallBack(diagnosticOrderReviewBinding.lytViewContent, mContext, !TextUtils.isEmpty(testDetailResponse.getResult().getMessage()) ? testDetailResponse.getResult().getMessage() : mContext.getResources().getString(R.string.coupon_invalid_msg));
                        return;
                    }
                    setDetails();
                }
                break;
            }
            case DiagnosticServiceManager.DIAGNOSIS_SHOW_COUPON: {
                if (!TextUtils.isEmpty(data)) {
                    showCouponResponse = new Gson().fromJson(data, ShowCouponResponse.class);
                    showCouponBox();
                }
                break;
            }
            case DiagnosticServiceManager.OFFER_BANNER: {
                OfferBannerResponse offerBannerResponse = new Gson().fromJson(data, OfferBannerResponse.class);
                labDetailsMutableLiveData.setValue(offerBannerResponse);
                break;
            }
        }
    }

    private void showCouponBox() {
        if (showCouponResponse != null && showCouponResponse.getResult() != null && showCouponResponse.getResult().isAnd()) {
            diagnosticOrderReviewBinding.cardViewOfferApplied.setVisibility(View.VISIBLE);
        } else {
            diagnosticOrderReviewBinding.cardViewOfferApplied.setVisibility(View.GONE);
        }
    }

    private void setDetails() {
        if (isCouponAvailable() && isOriginalPriceAvailable()) {
            diagnosticOrderReviewBinding.lLayoutApplyNow.setVisibility(View.GONE);
            diagnosticOrderReviewBinding.lLayoutApplied.setVisibility(View.VISIBLE);
            diagnosticOrderReviewBinding.lytDiscount.setVisibility(View.VISIBLE);
            diagnosticOrderReviewBinding.textPromoCode.setText(couponName());
            diagnosticOrderReviewBinding.txtViewOfferApplied.setText(appliedOfferText());
            diagnosticOrderReviewBinding.textAdditionalDiscount.setText(discountAmount());
            totalSaving();
            String tempTotalValue = totalAmount();
            if (originalAmount == 0) {
                diagnosticOrderReviewBinding.btnProceed.setText(mContext.getResources().getString(R.string.txt_place_order));
            } else {
                diagnosticOrderReviewBinding.btnProceed.setText(mContext.getResources().getString(R.string.text_pay));
            }
            diagnosticOrderReviewBinding.textTotalAmount.setText(tempTotalValue);
            diagnosticOrderReviewBinding.totalAmount.setText(tempTotalValue);
        } else {
            diagnosticOrderReviewBinding.lLayoutApplyNow.setVisibility(View.VISIBLE);
            diagnosticOrderReviewBinding.lLayoutApplied.setVisibility(View.GONE);
            diagnosticOrderReviewBinding.lytDiscount.setVisibility(View.GONE);
        }
    }

    public void changeLab(){
        BasePreference.getInstance(listener.getContext()).setPreviousActivitiesCleared(true);
        listener.finishMe(true);
    }

    public void changeTimeSlot(){
        listener.finishMe(true);
    }

    public MutableLiveData<OfferBannerResponse> getLabDetailsMutableLiveData() {
        return labDetailsMutableLiveData;
    }

    public void setLabDetails(OfferBannerResponse offerBannerResponse) {
        if (offerBannerResponse!=null && offerBannerResponse.getOfferBannerResult()!=null && offerBannerResponse.getOfferBannerResult().getLab_details() != null) {
            diagnosticOrderReviewBinding.tvCertification.setText(CommonUtils.validateString(offerBannerResponse.getOfferBannerResult().getLab_details().getLab_certification()));
            diagnosticOrderReviewBinding.tvReport.setText(CommonUtils.validateString(offerBannerResponse.getOfferBannerResult().getLab_details().getOthers()));
            diagnosticOrderReviewBinding.tvDiscount.setText(CommonUtils.validateString(offerBannerResponse.getOfferBannerResult().getLab_details().getDiscount()));
        }
    }

    /**
     * network connected or not
     */
    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == DiagnosticServiceManager.DIAGNOSIS_APPLY_COUPON) {
            applyCoupon();
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.vmDismissProgress();
        switch (transactionId) {
            case DiagnosticServiceManager.DIAGNOSIS_APPLY_COUPON: {
                vmOnError(transactionId);
                break;
            }
            case DiagnosticServiceManager.DIAGNOSIS_SHOW_COUPON: {
                diagnosticOrderReviewBinding.cardViewOfferApplied.setVisibility(View.GONE);
                break;
            }
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        diagnosticOrderReviewBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        diagnosticOrderReviewBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        diagnosticOrderReviewBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        diagnosticOrderReviewBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void setBillingAndShippingAddress(MStarAddressModel customerAddress) {
        initiateAPICall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS, customerAddress);
        initiateAPICall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS, customerAddress);
        initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS, customerAddress);
        initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS, customerAddress);
    }

    private void initiateAPICall(int transactionId, MStarAddressModel customerAddress) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, BasePreference.getInstance(mContext).getMstarBasicHeaderMap(), customerAddress.getId(), APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                    break;
                case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, BasePreference.getInstance(mContext).getMstarBasicHeaderMap(), customerAddress.getId(), APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId,
                            0);
                    break;
                case DiagnosticServiceManager.OFFER_BANNER:
                    DiagnosticServiceManager.getInstance().getDiagnosticOfferBanner(this, BasePreference.getInstance(listener.getContext()));
                    break;
            }
        }
    }
}
