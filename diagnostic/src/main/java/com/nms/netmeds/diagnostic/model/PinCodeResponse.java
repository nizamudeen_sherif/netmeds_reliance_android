package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class PinCodeResponse extends BaseResponse {
    @SerializedName("result")
    private PinCodeResult result;
    @SerializedName("updatedOn")
    private String updatedOn;

    public PinCodeResult getResult() {
        return result;
    }

    public void setResult(PinCodeResult result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
