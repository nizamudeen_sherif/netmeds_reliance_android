package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.diagnostic.adapter.TimeSlotAdapter;
import com.nms.netmeds.diagnostic.databinding.FragmentDiagnosticTimeSlotBinding;
import com.nms.netmeds.diagnostic.model.AvailableSlot;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.List;

public class DiagnosticTimeSlotFragmentViewModel extends AppViewModel {
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private FragmentDiagnosticTimeSlotBinding fragmentDiagnosticTimeSlotBinding;
    private DiagnosticTimeSlotFragmentViewModel timeSlotFragmentViewModel;
    private AvailableSlot availableSlot;

    public DiagnosticTimeSlotFragmentViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, FragmentDiagnosticTimeSlotBinding fragmentDiagnosticTimeSlotBinding, DiagnosticTimeSlotFragmentViewModel timeSlotFragmentViewModel, AvailableSlot availableSlot) {
        this.mContext = context;
        this.fragmentDiagnosticTimeSlotBinding = fragmentDiagnosticTimeSlotBinding;
        this.timeSlotFragmentViewModel = timeSlotFragmentViewModel;
        this.availableSlot = availableSlot;
        initTimeSlotAdapter();
    }

    private void initTimeSlotAdapter() {
        if (availableSlot != null && availableSlot.getSlotTimeList() != null && availableSlot.getSlotTimeList().size() > 0) {
            List<SlotTime> slotTimeList = new ArrayList<>();
            if (availableSlot.getSlotDay().equals(DiagnosticHelper.getSelectedSlotDay())) {
                slotTimeList.addAll(availableSlot.getSlotTimeList());
            } else {
                for (SlotTime slotTime : availableSlot.getSlotTimeList()) {
                    slotTime.setChecked(false);
                    slotTimeList.add(slotTime);
                }
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            fragmentDiagnosticTimeSlotBinding.timeSlotList.setLayoutManager(linearLayoutManager);
            fragmentDiagnosticTimeSlotBinding.timeSlotList.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
            TimeSlotAdapter timeSlotAdapter = new TimeSlotAdapter(mContext, slotTimeList);
            fragmentDiagnosticTimeSlotBinding.timeSlotList.setAdapter(timeSlotAdapter);

            fragmentDiagnosticTimeSlotBinding.cardViewTimeSlot.setVisibility(View.VISIBLE);
            fragmentDiagnosticTimeSlotBinding.textTimeSlotEmpty.setVisibility(View.GONE);
        } else {
            fragmentDiagnosticTimeSlotBinding.cardViewTimeSlot.setVisibility(View.GONE);
            fragmentDiagnosticTimeSlotBinding.textTimeSlotEmpty.setVisibility(View.VISIBLE);
        }
    }
}
