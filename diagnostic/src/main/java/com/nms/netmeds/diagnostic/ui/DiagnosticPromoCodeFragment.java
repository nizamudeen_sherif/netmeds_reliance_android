package com.nms.netmeds.diagnostic.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.DiagnosticPromoCodeDialogBinding;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPromoCodeViewModel;

@SuppressLint("ValidFragment")
public class DiagnosticPromoCodeFragment extends BaseDialogFragment {
    private Application application;

    public DiagnosticPromoCodeFragment(Application application) {
        this.application = application;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setWindowAnimations(com.nms.netmeds.base.R.style.BottomSheetDialogAnimation);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setCancelable(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DiagnosticPromoCodeDialogBinding promoCodeDialogBinding = DataBindingUtil.inflate(inflater, R.layout.diagnostic_promo_code_dialog, container, false);
        DiagnosticPromoCodeViewModel promoCodeViewModel = new DiagnosticPromoCodeViewModel(application);
        promoCodeViewModel.init(getActivity(),promoCodeDialogBinding);
        promoCodeDialogBinding.setViewModel(promoCodeViewModel);
        return promoCodeDialogBinding.getRoot();
    }

    public interface DiagnosticPromoCodeFragmentListener {

    }
}
