package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PriceDescription;

import java.util.List;

public class DiagnosticApplyCouponRequest {

    @SerializedName("tests")
    private List<Test> tests;
    @SerializedName("labDescription")
    private LabDescription labDescription;
    @SerializedName("priceDescription")
    private PriceDescription priceDescription;

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public LabDescription getLabDescription() {
        return labDescription;
    }

    public void setLabDescription(LabDescription labDescription) {
        this.labDescription = labDescription;
    }

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }
}
