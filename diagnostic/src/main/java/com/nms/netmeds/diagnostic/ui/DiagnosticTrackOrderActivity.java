package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticTrackOrderBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderTrackResponse;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTrackOrderViewModel;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;

public class DiagnosticTrackOrderActivity extends BaseViewModelActivity<DiagnosticTrackOrderViewModel> implements DiagnosticTrackOrderViewModel.DiagnosticTrackOrderListener {

    private DiagnosticTrackOrderViewModel trackOrderViewModel;
    private ActivityDiagnosticTrackOrderBinding trackOrderBinding;
    private String orderId;
    private boolean intentFrom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trackOrderBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_track_order);
        toolBarSetUp(trackOrderBinding.toolBarCustomLayout.toolbar);
        initToolBar(trackOrderBinding.toolBarCustomLayout.collapsingToolbar, getResources().getString(R.string.text_track_order));
        trackOrderBinding.setViewModel(onCreateViewModel());
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null)
            checkDeepLink(intent);
    }

    private void checkDeepLink(Intent intent) {
        if (intent.hasExtra(DiagnosticConstant.KEY_ORDER_ID)) {
            orderId = intent.getStringExtra(DiagnosticConstant.KEY_ORDER_ID);
            if (intent.hasExtra(DiagnosticConstant.KEY_INTENT_FROM_ORDER_SUCCESS))
                intentFrom = intent.getBooleanExtra(DiagnosticConstant.KEY_INTENT_FROM_ORDER_SUCCESS, false);
        } else
            onIntent(intent);
    }

    private void onIntent(Intent intent) {
        if (intent.hasExtra("extraPathParams")) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get("extraPathParams");
            orderId = intentParamList != null && intentParamList.size() > 0 ? intentParamList.get(0) : "";
        }
    }

    @Override
    protected DiagnosticTrackOrderViewModel onCreateViewModel() {
        trackOrderViewModel = ViewModelProviders.of(this).get(DiagnosticTrackOrderViewModel.class);
        trackOrderViewModel.getDiagnosticOrderTrackMutableLiveData().observe(this, new OrderTrackObserver());
        intentValue();
        trackOrderViewModel.init(this, trackOrderBinding, trackOrderViewModel, orderId, this);
        onRetry(trackOrderViewModel);
        return trackOrderViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            homeNavigation();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void vmShowProgress() {
        trackOrderBinding.toolBarCustomLayout.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        trackOrderBinding.toolBarCustomLayout.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void vmCallbackCancelOrder(String orderId) {

    }

    @Override
    public void vmCallbackNeedHelp() {
        DiagnosticNeedHelpFragment diagnosticNeedHelpFragment = new DiagnosticNeedHelpFragment(getApplication());
        getSupportFragmentManager().beginTransaction().add(diagnosticNeedHelpFragment, "Diagnostic_need_help_fragment").commitAllowingStateLoss();
    }

    @Override
    public void vmCallbackViewOrder(DiagnosticOrder diagnosticOrder) {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, diagnosticOrder);
        LaunchIntentManager.routeToActivity(DiagnosticTrackOrderActivity.this.getResources().getString(R.string.route_diagnostic_order_detail), intent, DiagnosticTrackOrderActivity.this);
    }

    @Override
    public Context getContext() {
        return this;
    }

    public class OrderTrackObserver implements Observer<DiagnosticOrderTrackResponse> {

        @Override
        public void onChanged(@Nullable DiagnosticOrderTrackResponse diagnosticOrderTrackResponse) {
            trackOrderViewModel.trackOrderDataAvailable(diagnosticOrderTrackResponse);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_TRACK_ORDER);
    }

    @Override
    public void onBackPressed() {
        homeNavigation();
    }

    private void homeNavigation() {
        if (intentFrom) {
            LaunchIntentManager.routeToActivity(getString(R.string.route_navigation_activity), this);
            DiagnosticTrackOrderActivity.this.finish();
        } else
            DiagnosticTrackOrderActivity.this.finish();
    }
}
