package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OriginalPrice implements Serializable {

    @SerializedName("test_count")
    private int test_count;
    @SerializedName("hc")
    private int hc;
    @SerializedName("offered_price")
    private int offered_price;
    @SerializedName("final_price")
    private int final_price;
    @SerializedName("discount_amount")
    private int discount_amount;
    @SerializedName("discount_percent")
    private int discount_percent;
    @SerializedName("discount_percent_info")
    private String discount_percent_info;
    @SerializedName("discount_amount_info")
    private String discount_amount_info;


    public int getTest_count() {
        return test_count;
    }

    public void setTest_count(int test_count) {
        this.test_count = test_count;
    }

    public int getHc() {
        return hc;
    }

    public void setHc(int hc) {
        this.hc = hc;
    }

    public int getOffered_price() {
        return offered_price;
    }

    public void setOffered_price(int offered_price) {
        this.offered_price = offered_price;
    }

    public int getFinal_price() {
        return final_price;
    }

    public void setFinal_price(int final_price) {
        this.final_price = final_price;
    }

    public int getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(int discount_amount) {
        this.discount_amount = discount_amount;
    }

    public int getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(int discount_percent) {
        this.discount_percent = discount_percent;
    }

    public String getDiscount_percent_info() {
        return discount_percent_info;
    }

    public void setDiscount_percent_info(String discount_percent_info) {
        this.discount_percent_info = discount_percent_info;
    }

    public String getDiscount_amount_info() {
        return discount_amount_info;
    }

    public void setDiscount_amount_info(String discount_amount_info) {
        this.discount_amount_info = discount_amount_info;
    }


}
