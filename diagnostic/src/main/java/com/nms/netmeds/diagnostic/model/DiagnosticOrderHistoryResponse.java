package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;

import java.util.List;

public class DiagnosticOrderHistoryResponse extends BaseResponse {
    @SerializedName("result")
    private List<DiagnosticOrder> orderResultList;
    @SerializedName("updatedOn")
    private String updatedOn;

    public List<DiagnosticOrder> getOrderResultList() {
        return orderResultList;
    }

    public void setOrderResultList(List<DiagnosticOrder> orderResultList) {
        this.orderResultList = orderResultList;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
