package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DiagnosticOrder implements Serializable {
    @SerializedName("hardCopy")
    private String hardCopy;
    @SerializedName("amount")
    private String amount;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("patientDetails")
    private PatientDetail patientDetails;
    @SerializedName("slot")
    private SlotTime slot;
    @SerializedName("type")
    private String type;
    @SerializedName("userId")
    private String userId;
    @SerializedName("userDetails")
    private DiagnosticUserDetail diagnosticUserDetail;
    @SerializedName("bookId")
    private String bookId;
    @SerializedName("orderDetails")
    private DiagnosticOrderDetail diagnosticOrderDetail;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("labId")
    private String labId;
    @SerializedName("labOrderStatus")
    private String labOrderStatus;
    @SerializedName("id")
    private String id;
    @SerializedName("status")
    private String status;
    @SerializedName("updatedAt")
    private String updatedAt;
    @SerializedName("paymentDetails")
    private PriceDescription priceDescription;
    @SerializedName("payments")
    private DiagnosticPaymentCreateResult payment;
    @SerializedName("trackDetails")
    private List<OrderTrackDetail> trackDetailList;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("message")
    private String message;
    @SerializedName("labDetails")
    private List<LabDetails> labDetailsList;
    @SerializedName("radio")
    private boolean radio;

    public boolean getRadio() {
        return radio;
    }

    public void setRadio(boolean radio) {
        this.radio = radio;
    }

    public String getHardCopy() {
        return hardCopy;
    }

    public void setHardCopy(String hardCopy) {
        this.hardCopy = hardCopy;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PatientDetail getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(PatientDetail patientDetails) {
        this.patientDetails = patientDetails;
    }

    public SlotTime getSlot() {
        return slot;
    }

    public void setSlot(SlotTime slot) {
        this.slot = slot;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DiagnosticUserDetail getDiagnosticUserDetail() {
        return diagnosticUserDetail;
    }

    public void setDiagnosticUserDetail(DiagnosticUserDetail diagnosticUserDetail) {
        this.diagnosticUserDetail = diagnosticUserDetail;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public DiagnosticOrderDetail getDiagnosticOrderDetail() {
        return diagnosticOrderDetail;
    }

    public void setDiagnosticOrderDetail(DiagnosticOrderDetail diagnosticOrderDetail) {
        this.diagnosticOrderDetail = diagnosticOrderDetail;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getLabOrderStatus() {
        return labOrderStatus;
    }

    public void setLabOrderStatus(String labOrderStatus) {
        this.labOrderStatus = labOrderStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }

    public DiagnosticPaymentCreateResult getPayment() {
        return payment;
    }

    public void setPayment(DiagnosticPaymentCreateResult payment) {
        this.payment = payment;
    }

    public List<OrderTrackDetail> getTrackDetailList() {
        return trackDetailList;
    }

    public void setTrackDetailList(List<OrderTrackDetail> trackDetailList) {
        this.trackDetailList = trackDetailList;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LabDetails> getLabDetailsList() {
        return labDetailsList;
    }

    public void setLabDetailsList(List<LabDetails> labDetailsList) {
        this.labDetailsList = labDetailsList;
    }
}
