package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterPopularHealthPackagesItemBinding;
import com.nms.netmeds.diagnostic.databinding.AdapterSearchPackageListItemBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class PackageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context mContext;
    private List<DiagnosticPackage> packageList, filterList;
    private PackageAdapterListener packageAdapterListener;
    private PackageFilter mPackageFilter;
    public static final int POPULAR_PACKAGE = 0;
    public static final int SEARCH_PACKAGE = 1;
    private int layoutType;

    public PackageAdapter(Context context, List<DiagnosticPackage> packageList, int layoutType, PackageAdapterListener packageAdapterListener) {
        this.mContext = context;
        this.packageList = packageList;
        this.filterList = packageList;
        this.layoutType = layoutType;
        this.packageAdapterListener = packageAdapterListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case SEARCH_PACKAGE:
                AdapterSearchPackageListItemBinding popularPackageItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_search_package_list_item, viewGroup, false);
                return new ViewAllPackageHolder(popularPackageItemBinding);
            case POPULAR_PACKAGE:
                AdapterPopularHealthPackagesItemBinding popularPackagesItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_popular_health_packages_item, viewGroup, false);
                return new PopularPackageViewHolder(popularPackagesItemBinding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final DiagnosticPackage diagnosticPackage = packageList.get(position);
        final Test test = diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 ? diagnosticPackage.getTestList().get(0) : new Test();
        final PriceDescription priceDescription = diagnosticPackage.getPriceDescription() != null ? diagnosticPackage.getPriceDescription() : new PriceDescription();
        final LabDescription labDescription = diagnosticPackage.getLabDescription() != null ? diagnosticPackage.getLabDescription() : new LabDescription();

        switch (getItemViewType(position)) {
            case POPULAR_PACKAGE:
                PopularPackageViewHolder popularPackageViewHolder = (PopularPackageViewHolder) holder;

                popularPackageViewHolder.healthPackagesItemBinding.textPackageName.setText(!TextUtils.isEmpty(test.getTestName()) ? test.getTestName() : "");
                popularPackageViewHolder.healthPackagesItemBinding.textLab.setText(!TextUtils.isEmpty(labDescription.getLabName()) ? String.format("%s %s", mContext.getResources().getString(R.string.text_by), labDescription.getLabName()) : "");
                popularPackageViewHolder.healthPackagesItemBinding.textDiscountPercentage.setText(!TextUtils.isEmpty(priceDescription.getDiscountPercentInfo()) ? priceDescription.getDiscountPercentInfo() : "");
                popularPackageViewHolder.healthPackagesItemBinding.textPrice.setText(!TextUtils.isEmpty(priceDescription.getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getFinalPrice()) : "");
                popularPackageViewHolder.healthPackagesItemBinding.textStrikePrice.setText(!TextUtils.isEmpty(priceDescription.getOfferedPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getOfferedPrice()) : "");
                popularPackageViewHolder.healthPackagesItemBinding.textStrikePrice.setPaintFlags(popularPackageViewHolder.healthPackagesItemBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                popularPackageViewHolder.healthPackagesItemBinding.textPackageName.setVisibility(TextUtils.isEmpty(test.getTestName()) ? View.GONE : View.VISIBLE);
                popularPackageViewHolder.healthPackagesItemBinding.textLab.setVisibility(TextUtils.isEmpty(labDescription.getLabName()) ? View.GONE : View.VISIBLE);
                popularPackageViewHolder.healthPackagesItemBinding.textDiscountPercentage.setVisibility(TextUtils.isEmpty(priceDescription.getDiscountPercentInfo()) ? View.GONE : View.VISIBLE);
                popularPackageViewHolder.healthPackagesItemBinding.textPrice.setVisibility(TextUtils.isEmpty(priceDescription.getFinalPrice()) ? View.GONE : View.VISIBLE);
                popularPackageViewHolder.healthPackagesItemBinding.textStrikePrice.setVisibility(TextUtils.isEmpty(priceDescription.getOfferedPrice()) ? View.GONE : View.VISIBLE);

                popularPackageViewHolder.healthPackagesItemBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNavigation(diagnosticPackage, labDescription, priceDescription);
                    }
                });
                popularPackageViewHolder.healthPackagesItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (DiagnosticHelper.isRadioTestExist())
                            DiagnosticHelper.clearSelectedList();
                        Intent intent = new Intent();
                        intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED, packageList.get(position));
                        intent.putExtra(DiagnosticConstant.INTENT_ALL_PACKAGE, (Serializable) packageList);
                        LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_package_detail), intent, mContext);
                    }
                });
                break;
            case SEARCH_PACKAGE:
                final ViewAllPackageHolder viewAllPackageHolder = (ViewAllPackageHolder) holder;

                viewAllPackageHolder.searchPackageListItemBinding.textPackageName.setText(!TextUtils.isEmpty(test.getTestName()) ? test.getTestName() : "");
                viewAllPackageHolder.searchPackageListItemBinding.textLabName.setText(!TextUtils.isEmpty(labDescription.getLabName()) ? String.format("%s %s", mContext.getResources().getString(R.string.text_by), labDescription.getLabName()) : "");
                viewAllPackageHolder.searchPackageListItemBinding.textOffer.setText(!TextUtils.isEmpty(priceDescription.getDiscountPercentInfo()) ? priceDescription.getDiscountPercentInfo() : "");
                viewAllPackageHolder.searchPackageListItemBinding.textPrice.setText(!TextUtils.isEmpty(priceDescription.getFinalPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getFinalPrice()) : "");
                viewAllPackageHolder.searchPackageListItemBinding.textStrikePrice.setText(!TextUtils.isEmpty(priceDescription.getOfferedPrice()) ? String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceDescription.getOfferedPrice()) : "");
                viewAllPackageHolder.searchPackageListItemBinding.textStrikePrice.setPaintFlags(viewAllPackageHolder.searchPackageListItemBinding.textStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                viewAllPackageHolder.searchPackageListItemBinding.textPackageName.setVisibility(TextUtils.isEmpty(test.getTestName()) ? View.GONE : View.VISIBLE);
                viewAllPackageHolder.searchPackageListItemBinding.textLabName.setVisibility(TextUtils.isEmpty(labDescription.getLabName()) ? View.GONE : View.VISIBLE);
                viewAllPackageHolder.searchPackageListItemBinding.textOffer.setVisibility(TextUtils.isEmpty(priceDescription.getDiscountPercent()) ? View.GONE : View.VISIBLE);
                viewAllPackageHolder.searchPackageListItemBinding.textPrice.setVisibility(TextUtils.isEmpty(priceDescription.getFinalPrice()) ? View.GONE : View.VISIBLE);
                viewAllPackageHolder.searchPackageListItemBinding.textStrikePrice.setVisibility(TextUtils.isEmpty(priceDescription.getOfferedPrice()) ? View.GONE : View.VISIBLE);

                viewAllPackageHolder.searchPackageListItemBinding.mainLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED, packageList.get(position));
                        intent.putExtra(DiagnosticConstant.INTENT_ALL_PACKAGE, (Serializable) packageList);
                        LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_package_detail), intent, mContext);
                    }
                });

                viewAllPackageHolder.searchPackageListItemBinding.btnBuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNavigation(diagnosticPackage, labDescription, priceDescription);
                    }
                });

                viewAllPackageHolder.searchPackageListItemBinding.viewHideDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewAllPackageHolder.searchPackageListItemBinding.packageListView.getVisibility() == View.VISIBLE) {
                            viewAllPackageHolder.searchPackageListItemBinding.packageListView.setVisibility(View.GONE);
                            viewAllPackageHolder.searchPackageListItemBinding.viewHideDetails.setText(mContext.getResources().getString(R.string.text_view_package_details));
                        } else {
                            viewAllPackageHolder.searchPackageListItemBinding.packageListView.setVisibility(View.VISIBLE);
                            viewAllPackageHolder.searchPackageListItemBinding.viewHideDetails.setText(mContext.getResources().getString(R.string.text_hide_package_details));
                        }
                    }
                });
                break;
            default:
                break;
        }
    }

    private void onNavigation(DiagnosticPackage diagnosticPackage, LabDescription labDescription, PriceDescription priceDescription) {
        /*Diagnostic WebEngage Package Added Event*/
        setWebEngagePackageAddedEvent(diagnosticPackage, labDescription, priceDescription);
        DiagnosticHelper.setTest(DiagnosticHelper.addATest(diagnosticPackage));
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
        LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_select_lab), intent, mContext);
    }

    private void setWebEngagePackageAddedEvent(DiagnosticPackage diagnosticPackage, LabDescription labDescription, PriceDescription priceDescription) {
        if (diagnosticPackage != null) {
            String jsonObject = new Gson().toJson(diagnosticPackage);
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            HashMap<String, Object> packageMap = new Gson().fromJson(jsonObject, type);
            String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
            double price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? Double.parseDouble(priceDescription.getFinalPrice()) : 0;

            WebEngageHelper.getInstance().packageAddedEvent(BasePreference.getInstance(mContext), DiagnosticHelper.getCity(), packageMap, labName, price, mContext);
        }
    }

    public void updatePackageAdapter(List<DiagnosticPackage> updateList, int type) {
        this.packageList = updateList;
        this.filterList = updateList;
        this.layoutType = type;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (layoutType == POPULAR_PACKAGE)
            return POPULAR_PACKAGE;
        else return SEARCH_PACKAGE;
    }

    @Override
    public Filter getFilter() {
        if (mPackageFilter == null)
            mPackageFilter = new PackageFilter(PackageAdapter.this);
        return mPackageFilter;
    }

    private class PackageFilter extends Filter {
        private PackageAdapter packageAdapter;

        PackageFilter(PackageAdapter packageAdapter) {
            super();
            this.packageAdapter = packageAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResult = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                filterResult.values = filterList;
                filterResult.count = filterList.size();
            } else {
                List<DiagnosticPackage> filteredList = new ArrayList<>();
                final String filterPattern = constraint.toString().toLowerCase();
                for (DiagnosticPackage diagnosticPackage : filterList) {
                    Test test = diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 ? diagnosticPackage.getTestList().get(0) : new Test();
                    if (test.getTestName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(diagnosticPackage);
                    }
                }
                filterResult.values = filteredList;
                filterResult.count = filteredList.size();
            }
            return filterResult;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            packageAdapter.packageList = (ArrayList<DiagnosticPackage>) results.values;
            packageAdapter.notifyDataSetChanged();
        }
    }

    class ViewAllPackageHolder extends RecyclerView.ViewHolder {
        AdapterSearchPackageListItemBinding searchPackageListItemBinding;

        ViewAllPackageHolder(@NonNull AdapterSearchPackageListItemBinding searchPackageListItemBinding) {
            super(searchPackageListItemBinding.getRoot());
            this.searchPackageListItemBinding = searchPackageListItemBinding;
        }
    }

    class PopularPackageViewHolder extends RecyclerView.ViewHolder {
        private AdapterPopularHealthPackagesItemBinding healthPackagesItemBinding;

        PopularPackageViewHolder(@NonNull AdapterPopularHealthPackagesItemBinding healthPackagesItemBinding) {
            super(healthPackagesItemBinding.getRoot());
            this.healthPackagesItemBinding = healthPackagesItemBinding;
        }
    }

    private void packageCheck(DiagnosticPackage diagnosticPackage) {
        if (diagnosticPackage.isChecked()) {
            diagnosticPackage.setChecked(false);
            packageAdapterListener.onPackageUnChecked();
        } else {
            packageUnCheck();
            diagnosticPackage.setChecked(true);
            packageAdapterListener.onPackageChecked(diagnosticPackage);
        }
        notifyDataSetChanged();
    }

    private void packageUnCheck() {
        for (DiagnosticPackage diagnosticPackage : packageList)
            diagnosticPackage.setChecked(false);
    }

    public interface PackageAdapterListener {
        void onPackageChecked(DiagnosticPackage diagnosticPackage);

        void onPackageUnChecked();
    }
}