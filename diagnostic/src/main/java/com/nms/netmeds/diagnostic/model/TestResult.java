package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.Test;

import java.io.Serializable;
import java.util.List;

public class TestResult implements Serializable {

    @SerializedName("message")
    private String message;
    @SerializedName("tests")
    private List<Test> tests;
    @SerializedName("labs")
    private List<LabResult> labs;
    @SerializedName("coupon")
    private DiagnosticCoupon coupon;
    @SerializedName("originalPrice")
    private OriginalPrice originalPrice;
    @SerializedName("priceDescription")
    private PriceDescription priceDescription;

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }

    public DiagnosticCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(DiagnosticCoupon coupon) {
        this.coupon = coupon;
    }

    public OriginalPrice getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(OriginalPrice originalPrice) {
        this.originalPrice = originalPrice;
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public List<LabResult> getLabs() {
        return labs;
    }

    public void setLabs(List<LabResult> labs) {
        this.labs = labs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
