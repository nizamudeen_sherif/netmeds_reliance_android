package com.nms.netmeds.diagnostic.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;

public class DiagnosticPromoCodeAdapterViewModel extends AppViewModel {
    public DiagnosticPromoCodeAdapterViewModel(@NonNull Application application) {
        super(application);
    }
}
