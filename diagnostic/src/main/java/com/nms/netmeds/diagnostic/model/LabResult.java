package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LabResult implements Serializable {

    @SerializedName("priceDescription")
    private PriceDescription priceDescription;
    @SerializedName("labDescription")
    private LabDescription labDescription;

    public PriceDescription getPriceDescription() {
        return priceDescription;
    }

    public void setPriceDescription(PriceDescription priceDescription) {
        this.priceDescription = priceDescription;
    }

    public LabDescription getLabDescription() {
        return labDescription;
    }

    public void setLabDescription(LabDescription labDescription) {
        this.labDescription = labDescription;
    }



}
