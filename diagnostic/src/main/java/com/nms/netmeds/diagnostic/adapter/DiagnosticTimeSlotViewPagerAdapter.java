package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nms.netmeds.diagnostic.ui.DiagnosticTimeSlotFragment;

import java.util.ArrayList;
import java.util.List;

public class DiagnosticTimeSlotViewPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public DiagnosticTimeSlotViewPagerAdapter(Context context, FragmentManager manager) {
        super(manager);
        this.context = context;

    }

    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    public void addFragmentTitleList(List<String> stringList) {
        mFragmentTitleList.addAll(stringList);
    }

    @Override
    public Fragment getItem(int i) {
        return new DiagnosticTimeSlotFragment();
    }

    @Override
    public int getCount() {
        return mFragmentTitleList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
