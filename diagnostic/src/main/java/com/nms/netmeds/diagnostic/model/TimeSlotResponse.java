package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class TimeSlotResponse extends BaseResponse {
    @SerializedName("result")
    private TimeSlotResult result;
    @SerializedName("updatedOn")
    private String updatedOn;

    public TimeSlotResult getResult() {
        return result;
    }

    public void setResult(TimeSlotResult result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
