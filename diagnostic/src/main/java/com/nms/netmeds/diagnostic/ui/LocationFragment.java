package com.nms.netmeds.diagnostic.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.diagnostic.LocationServiceCheckListener;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.DialogxLocationBinding;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticHomeViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPackageDescriptionViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPackageViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticSelectLabViewModel;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTestDescriptionViewModel;
import com.nms.netmeds.diagnostic.viewModel.LocationViewModel;
import com.webengage.sdk.android.WebEngage;

@SuppressLint("ValidFragment")
public class LocationFragment extends BaseBottomSheetFragment implements LocationViewModel.LocationModelListener {
    private LocationFragmentListener locationFragmentListener;
    private DialogxLocationBinding locationBinding;
    private boolean getCurrentLocation;

    public LocationFragment(boolean getCurrentLocation) {
        this.getCurrentLocation = getCurrentLocation;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            locationFragmentListener = (LocationFragmentListener) getActivity();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        locationBinding = DataBindingUtil.inflate(inflater, R.layout.dialogx_location, container, false);
        LocationViewModel locationViewModel = new LocationViewModel(getActivity().getApplication());
        locationViewModel.init(getActivity(), locationBinding, this);
        locationBinding.setViewModel(locationViewModel);
        if (!getCurrentLocation) {
            locationBinding.textSamplePickup.setVisibility(View.GONE);
            locationBinding.lytCurrentLocation.setVisibility(View.GONE);
            locationBinding.lLayoutOR.setVisibility(View.GONE);
        }
        return locationBinding.getRoot();
    }

    private void onPinSuccess(String currentPinCode) {
        locationBinding.progressBar.setVisibility(View.GONE);
        BasePreference basePreference = BasePreference.getInstance(getActivity());
        basePreference.setDiagnosticPinCode(currentPinCode);
        basePreference.setLabPinCode(currentPinCode);
        /*Diagnostic WebEngage PinCode Enter Event*/
        WebEngageHelper.getInstance().diagnosticEnterPinCodeEvent(basePreference, DiagnosticHelper.getCity(), getActivity());
        CommonUtils.hideKeyboard(getContext(), locationBinding.getRoot());
        LocationFragment.this.dismissAllowingStateLoss();
        BasePreference.getInstance(getContext()).setChangeDiagnosticHome(true);
        locationFragmentListener.onLocationServiceResponse();
    }

    private void onPinCodeFailure() {
        locationBinding.progressBar.setVisibility(View.GONE);
        SnackBarHelper.snackBarCallBack(locationBinding.lytMain, getActivity(), getString(R.string.text_pin_code_error));
    }

    private void onPinShowMessage(String message) {
        locationBinding.progressBar.setVisibility(View.GONE);
        SnackBarHelper.snackBarCallBack(locationBinding.lytMain, getActivity(), message);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DiagnosticSelectLabViewModel.setServiceCheckListener(locationServiceCheckListener);
        DiagnosticHomeViewModel.setServiceCheckListener(locationServiceCheckListener);
        DiagnosticPackageDescriptionViewModel.setServiceCheckListener(locationServiceCheckListener);
        DiagnosticTestDescriptionViewModel.setServiceCheckListener(locationServiceCheckListener);
    }


    // Location Check service Callbacks
    private LocationServiceCheckListener locationServiceCheckListener = new LocationServiceCheckListener() {
        @Override
        public void onPinCodeSuccess(String currentPinCode) {
            onPinSuccess(currentPinCode);
        }

        @Override
        public void onPinCodeFailed() {
            onPinCodeFailure();
        }

        @Override
        public void onShowMessage(String message) {
            onPinShowMessage(message);
        }

        @Override
        public void onDismissProgress() {
            dismissProgress();
        }

        @Override
        public void onPermissionDeniedMessage() {
            locationBinding.progressBar.setVisibility(View.GONE);
            if (getActivity() != null) {
                Snackbar snackbar = Snackbar.make(locationBinding.lytMain, getString(R.string.err_msg_permission_denied), Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction(getString(R.string.text_allow), new AllowLocationListener());
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorDarkBlueGrey));
                snackbar.setActionTextColor(ContextCompat.getColor(getActivity(), R.color.colorMediumPink));
                snackbar.show();
            }
        }
    };

    private class AllowLocationListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (getActivity() != null) {
                LocationFragment.this.dismissAllowingStateLoss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        }
    }

    @Override
    public void initLocationPermission() {
        locationBinding.progressBar.setVisibility(View.VISIBLE);
        locationFragmentListener.onLocationPermission();
    }

    @Override
    public void initPinCode(String code) {
        locationBinding.progressBar.setVisibility(View.VISIBLE);
        locationFragmentListener.onPinCodeCheck(code);
    }

    @Override
    public void close() {
        locationBinding.progressBar.setVisibility(View.GONE);
        LocationFragment.this.dismissAllowingStateLoss();
        locationFragmentListener.onLocationDialogClose();
    }

    @Override
    public void showLoader() {
        locationBinding.progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void dismissLoader() {
        locationBinding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void defaultAddressChanged(MStarAddressModel customerSelectedAddress) {
        DiagnosticHelper.setDefaultAddress(customerSelectedAddress, getContext());
        BasePreference.getInstance(getContext()).setDiagDefaultAddressId(customerSelectedAddress.getId());
        BasePreference.getInstance(getContext()).setPreviousCartBillingAddressId(customerSelectedAddress.getId());
        BasePreference.getInstance(getContext()).setPreviousCartShippingAddressId(customerSelectedAddress.getId());
        BasePreference.getInstance(getContext()).setLabPinCode(!TextUtils.isEmpty(customerSelectedAddress.getPin()) ? customerSelectedAddress.getPin() : "");
        BasePreference.getInstance(getContext()).setChangeDiagnosticHome(true);
        LocationFragment.this.dismissAllowingStateLoss();
        locationFragmentListener.onChangedDefaultAddress();
    }

    @Override
    public void addAddress() {
        if (getActivity() != null) {
            LocationFragment.this.dismissAllowingStateLoss();
            Intent intent = new Intent();
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAG_HOME, true);
            LaunchIntentManager.routeToActivity(getActivity().getResources().getString(R.string.route_address_activity), intent, getActivity());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocationFragment.this.dismissAllowingStateLoss();
        locationFragmentListener.onLocationDialogClose();
    }

    public interface LocationFragmentListener {
        void onPinCodeCheck(String pinCode);

        void onLocationPermission();

        void onLocationDialogClose();

        void onLocationServiceResponse();

        void onChangedDefaultAddress();
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_LOCATION);
    }
}
