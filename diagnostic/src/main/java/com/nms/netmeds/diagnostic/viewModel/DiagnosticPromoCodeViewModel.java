package com.nms.netmeds.diagnostic.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.diagnostic.adapter.DiagnosticPromoCodeListAdapter;
import com.nms.netmeds.diagnostic.databinding.DiagnosticPromoCodeDialogBinding;

public class DiagnosticPromoCodeViewModel extends BaseViewModel {
    private Context mContext;
    private DiagnosticPromoCodeDialogBinding promoCodeDialogBinding;

    public DiagnosticPromoCodeViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DiagnosticPromoCodeDialogBinding promoCodeDialogBinding) {
        this.mContext = context;
        this.promoCodeDialogBinding = promoCodeDialogBinding;
        initPromoCodeAdapter();
    }

    private void initPromoCodeAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        promoCodeDialogBinding.promoCodeList.setLayoutManager(linearLayoutManager);
        DiagnosticPromoCodeListAdapter promoCodeListAdapter = new DiagnosticPromoCodeListAdapter(mContext);
        promoCodeDialogBinding.promoCodeList.setAdapter(promoCodeListAdapter);
    }
}
