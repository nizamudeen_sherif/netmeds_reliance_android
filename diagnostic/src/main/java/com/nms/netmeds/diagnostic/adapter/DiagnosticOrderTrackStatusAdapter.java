package com.nms.netmeds.diagnostic.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.DiagnosticOrderStatusItemBinding;
import com.nms.netmeds.diagnostic.model.OrderTrackDetail;

import java.util.List;

public class DiagnosticOrderTrackStatusAdapter extends RecyclerView.Adapter<DiagnosticOrderTrackStatusAdapter.DiagnosticOrderStatusViewHolder> {
    private final Context mContext;
    private List<OrderTrackDetail> trackStatusList;
    private OrderTrackStatusAdapterListener orderTrackStatusAdapterListener;

    public DiagnosticOrderTrackStatusAdapter(Context context, List<OrderTrackDetail> trackStatusList, OrderTrackStatusAdapterListener orderTrackStatusAdapterListener) {
        this.mContext = context;
        this.trackStatusList = trackStatusList;
        this.orderTrackStatusAdapterListener = orderTrackStatusAdapterListener;
    }

    @NonNull
    @Override
    public DiagnosticOrderStatusViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        DiagnosticOrderStatusItemBinding diagnosticOrderStatusItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.diagnostic_order_status_item, viewGroup, false);
        return new DiagnosticOrderStatusViewHolder(diagnosticOrderStatusItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DiagnosticOrderStatusViewHolder holder, int position) {
        OrderTrackDetail trackStatusDetail = trackStatusList.get(position);
        String date = !TextUtils.isEmpty(trackStatusDetail.getDate()) ? trackStatusDetail.getDate() : "";
        holder.diagnosticOrderStatusItemBinding.trackingDate.setText(date);
        holder.diagnosticOrderStatusItemBinding.trackingDate.setVisibility(TextUtils.isEmpty(date) ? View.GONE : View.VISIBLE);
        holder.diagnosticOrderStatusItemBinding.imgBackground.setBackground(ContextCompat.getDrawable(mContext, trackStatusDetail.isActive() ? R.drawable.pale_blue_button_round_background : R.drawable.disabled_button_background));
        holder.diagnosticOrderStatusItemBinding.textStatus.setText(trackStatusDetail.getStatus());
        holder.diagnosticOrderStatusItemBinding.textStatus.setVisibility(TextUtils.isEmpty(trackStatusDetail.getStatus()) ? View.GONE : View.VISIBLE);
        holder.diagnosticOrderStatusItemBinding.textStatus.setTypeface(trackStatusDetail.isActive() ? CommonUtils.getTypeface(mContext, "font/Lato-Bold.ttf") : CommonUtils.getTypeface(mContext, "font/Lato-Regular.ttf"));
        holder.diagnosticOrderStatusItemBinding.textStatus.setTextColor(ContextCompat.getColor(mContext, trackStatusDetail.isActive() ? R.color.colorDarkBlueGrey : R.color.colorLightBlueGrey));
        holder.diagnosticOrderStatusItemBinding.verticalLine.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);

        if (!TextUtils.isEmpty(trackStatusDetail.getStatus())) {
            if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.ORDER_PLACED)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, trackStatusDetail.isActive() ? R.drawable.ic_shopping_cart_diagnostic_added_active : R.drawable.ic_shopping_cart_added_inactive));
            } else if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.ORDER_PENDING)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_order_pending_lab));
            } else if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.PICKUP_SCHEDULED)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, trackStatusDetail.isActive() ? R.drawable.ic_delivery_active : R.drawable.ic_delivery_diagnostic_inactive));
            } else if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.SAMPLE_COLLECTED)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, trackStatusDetail.isActive() ? R.drawable.ic_lab_test_active : R.drawable.ic_lab_test_inactive));
            } else if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.SAMPLE_SENT_TO_LAB)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, trackStatusDetail.isActive() ? R.drawable.ic_rocket_active : R.drawable.ic_rocket_inactive));
            } else if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.REPORTS_GENERATED)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, trackStatusDetail.isActive() ? R.drawable.ic_document_active : R.drawable.ic_document_diagnostic_inactive));
                if (trackStatusDetail.isActive())
                    orderTrackStatusAdapterListener.onReportGenerated();
            } else if (trackStatusDetail.getStatus().equalsIgnoreCase(DiagnosticConstant.ORDER_CANCELLED)) {
                holder.diagnosticOrderStatusItemBinding.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_cancel));
            }
        }
    }

    @Override
    public int getItemCount() {
        return trackStatusList.size();
    }

    class DiagnosticOrderStatusViewHolder extends RecyclerView.ViewHolder {
        private DiagnosticOrderStatusItemBinding diagnosticOrderStatusItemBinding;

        DiagnosticOrderStatusViewHolder(DiagnosticOrderStatusItemBinding diagnosticOrderStatusItemBinding) {
            super(diagnosticOrderStatusItemBinding.getRoot());
            this.diagnosticOrderStatusItemBinding = diagnosticOrderStatusItemBinding;
        }
    }

    public interface OrderTrackStatusAdapterListener {
        void onReportGenerated();
    }
}