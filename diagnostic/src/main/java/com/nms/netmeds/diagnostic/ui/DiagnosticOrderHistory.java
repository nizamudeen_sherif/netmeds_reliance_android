package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticHistoryBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderHistoryResponse;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticOrderHistoryViewModel;
import com.webengage.sdk.android.WebEngage;

public class DiagnosticOrderHistory extends BaseViewModelActivity<DiagnosticOrderHistoryViewModel> implements DiagnosticOrderHistoryViewModel.DiagnosticOrderHistoryListener {
    private ActivityDiagnosticHistoryBinding diagnosticHistoryBinding;
    private DiagnosticOrderHistoryViewModel diagnosticOrderHistoryViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        diagnosticHistoryBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_history);
        toolBarSetUp(diagnosticHistoryBinding.toolBarCustomLayout.toolbar);
        initToolBar(diagnosticHistoryBinding.toolBarCustomLayout.collapsingToolbar, getResources().getString(R.string.text_diagnostic_records));
        diagnosticOrderHistoryViewModel = onCreateViewModel();
    }

    @Override
    protected DiagnosticOrderHistoryViewModel onCreateViewModel() {
        diagnosticOrderHistoryViewModel = ViewModelProviders.of(this).get(DiagnosticOrderHistoryViewModel.class);
        diagnosticOrderHistoryViewModel.getOrderHistoryMutableLiveData().observe(this, new OrderHistoryObserver());
        diagnosticOrderHistoryViewModel.getLoginResponseMutableLiveData().observe(this, new LoginObserver());
        diagnosticOrderHistoryViewModel.getLoganMutableLiveData().observe(this, new LoganTokenObserver());
        diagnosticOrderHistoryViewModel.init(this, diagnosticHistoryBinding, diagnosticOrderHistoryViewModel, this);
        diagnosticHistoryBinding.setViewModel(diagnosticOrderHistoryViewModel);
        onRetry(diagnosticOrderHistoryViewModel);
        return diagnosticOrderHistoryViewModel;
    }

    @Override
    public void vmShowProgress() {
        diagnosticHistoryBinding.toolBarCustomLayout.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        diagnosticHistoryBinding.toolBarCustomLayout.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void forceLogout() {
        logoutUserForceFully();
        return;
    }

    public class OrderHistoryObserver implements Observer<DiagnosticOrderHistoryResponse> {

        @Override
        public void onChanged(@Nullable DiagnosticOrderHistoryResponse diagnosticOrderHistoryResponse) {
            diagnosticOrderHistoryViewModel.orderHistoryDataAvailable(diagnosticOrderHistoryResponse);
        }
    }

    private class LoginObserver implements Observer<LoginResponse> {

        @Override
        public void onChanged(@Nullable LoginResponse loginResponse) {
            diagnosticOrderHistoryViewModel.diagnosticLoginDataAvailable(loginResponse);
        }
    }

    private class LoganTokenObserver implements Observer<LoganResponse> {

        @Override
        public void onChanged(@Nullable LoganResponse loganResponse) {
            diagnosticOrderHistoryViewModel.onLoganTokenDataAvailable(loganResponse);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            DiagnosticOrderHistory.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_ORDER_HISTORY);
    }

    // logout user forcefully
    public void logoutUserForceFully() {
        pushUnRegisterFromConsultation();
        BasePreference.getInstance(this).clear();
        BasePreference.getInstance(this).setClearPreference(true);
        WebEngageHelper.getInstance().getUser().logout();
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
        navigateToSignIn();
    }

    private void pushUnRegisterFromConsultation() {
        if(TextUtils.isEmpty(BasePreference.getInstance(this).getJustDocUserResponse())) return;
        boolean isConnected = NetworkUtils.isConnected(this);
        if (isConnected) {
            MStarCustomerDetails mStarCustomerDetails = new Gson().fromJson(BasePreference.getInstance(this).getCustomerDetails(), MStarCustomerDetails.class);
            SendFcmTokenRequest sendFcmTokenRequest = new SendFcmTokenRequest();
            sendFcmTokenRequest.setEmail(mStarCustomerDetails != null ? !TextUtils.isEmpty(mStarCustomerDetails.getEmail()) ? mStarCustomerDetails.getEmail() : "" : "");
            sendFcmTokenRequest.setToken(BasePreference.getInstance(this).getFcmToken());
            sendFcmTokenRequest.setUserType(ConsultationConstant.ROLE_USER);
            if (!TextUtils.isEmpty(BasePreference.getInstance(this).getFcmToken()))
                ConsultationServiceManager.getInstance().pushUnRegisterFromConsultationServer(sendFcmTokenRequest, BasePreference.getInstance(this));
        }
    }

    private void navigateToSignIn() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LaunchIntentManager.routeToActivity(this.getString(com.nms.netmeds.consultation.R.string.route_sign_in_activity), intent, this);
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
