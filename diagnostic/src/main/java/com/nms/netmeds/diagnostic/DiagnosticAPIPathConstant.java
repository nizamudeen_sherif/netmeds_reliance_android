package com.nms.netmeds.diagnostic;

public class DiagnosticAPIPathConstant {
    public static final String ACTION_PIN_CODE_STATUS = "labs/pin/status";
    public static final String ACTION_BANNER = "api/v1/homePageBanner";
    public static final String ACTION_GET_LABS = "labs/data/getById";
    public static final String ACTION_SEARCH = "labs/search/test";
    public static final String ACTION_POPULAR_TEST = "/labs/products/poptest/get";
    public static final String ACTION_POPULAR_PACKAGE = "labs/products/poppack/get";
    public static final String ACTION_MOST_POPULAR_PACKAGE = "labs/products/poppack/get";
    public static final String ACTION_TIME_SLOT = "labs/slots/get";
    public static final String ACTION_ORDER_CREATE = "omega/users/create/orders";
    public static final String ACTION_ORDER_HISTORY = "omega/user/get/history";
    public static final String ACTION_ORDER_TRACK = "omega/labs/order/track";
    public static final String ACTION_PAYMENT_CREATION = "omega/users/create/payment";
    public static final String ACTION_PAYMENT_UPDATE = "omega/users/update/payment";
    public static final String ACTION_ORDER_UPDATE = "omega/users/update/orders";
    public static final String ACTION_TEST_DETAIL = "labtest";
    public static final String DIAGNOSIS_PAYMENT_GATEWAY_LIST = "omega/V1/user/payment/list";
    public static final String DIAGNOSIS_NMS_WALLET_BALANCE = "mStar/NmsWallet/get";
    public static final String DIAGNOSIS_APPLY_COUPON = "omega/apply/coupon/review";
    public static final String DIAGNOSIS_SHOW_COUPON = "api/v1/couponBox";
    public static final String CUSTOMER_ADDRESS = "address/get/all";
    public static final String DC_JUSPAY_ORDER_CREATE = "/api/v1/juspay/cdordercreate";

}
