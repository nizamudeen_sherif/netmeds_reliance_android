package com.nms.netmeds.diagnostic;

// Common Location Service Check Interface
public interface LocationServiceCheckListener {

    void onPinCodeSuccess(String currentPinCode);

    void onPinCodeFailed();

    void onShowMessage(String message);

    void onDismissProgress();

    void onPermissionDeniedMessage();
}
