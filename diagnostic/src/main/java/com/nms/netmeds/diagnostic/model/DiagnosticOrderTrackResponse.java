package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class DiagnosticOrderTrackResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("result")
    private DiagnosticOrder orderTrackResult;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public DiagnosticOrder getOrderTrackResult() {
        return orderTrackResult;
    }

    public void setOrderTrackResult(DiagnosticOrder orderTrackResult) {
        this.orderTrackResult = orderTrackResult;
    }
}
