package com.nms.netmeds.diagnostic.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.DiagnosticBottomViewBinding;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticBottomViewModel;

import java.util.List;

@SuppressLint("ValidFragment")
public class DiagnosticBottomFragment extends BaseDialogFragment implements DiagnosticBottomViewModel.BottomViewModelListener {
    private Application application;
    private List<Test> testList;
    private DiagnosticBottomFragmentListener bottomFragmentListener;
    private String type = "";
    private String page = "";

    public DiagnosticBottomFragment(Application application, List<Test> list, String type, String page, DiagnosticBottomFragmentListener bottomFragmentListener) {
        this.application = application;
        this.testList = list;
        this.bottomFragmentListener = bottomFragmentListener;
        this.type = type;
        this.page = page;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setWindowAnimations(com.nms.netmeds.base.R.style.BottomSheetDialogAnimation);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setCancelable(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DiagnosticBottomViewBinding bottomViewBinding = DataBindingUtil.inflate(inflater, R.layout.diagnostic_bottom_view, container, false);
        DiagnosticBottomViewModel bottomViewModel = new DiagnosticBottomViewModel(application);
        bottomViewModel.init(getActivity(), bottomViewBinding, testList, type, page, this);
        bottomViewBinding.setViewModel(bottomViewModel);
        return bottomViewBinding.getRoot();
    }

    @Override
    public void onCloseDialog() {
        DiagnosticBottomFragment.this.dismissAllowingStateLoss();
    }

    @Override
    public void onNext() {
        DiagnosticBottomFragment.this.dismissAllowingStateLoss();
        bottomFragmentListener.onNavigation();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {
        if (type.equalsIgnoreCase(DiagnosticConstant.TEST) || type.equalsIgnoreCase(DiagnosticConstant.TEST_TYPE_PROFILE) || type.equalsIgnoreCase(DiagnosticConstant.PACKAGE) || type.equalsIgnoreCase(DiagnosticConstant.PACKAGES)) {
            switch (page) {
                case DiagnosticConstant.HOME_PAGE:
                    bottomFragmentListener.onUpdatePopularTest(removeTest);
                    break;
                case DiagnosticConstant.SEARCH_TEST_PAGE:
                    bottomFragmentListener.onUpdateSearchList(removeTest);
                    break;
            }
        }
    }

    @Override
    public void onUpdateLabCallback() {
        DiagnosticBottomFragment.this.dismissAllowingStateLoss();
        bottomFragmentListener.onUpdateLabList();
    }


    public interface DiagnosticBottomFragmentListener {
        void onNavigation();

        void onUpdatePopularTest(Test removeTest);

        void onUpdateSearchList(Test removeTest);

        void onUpdateLabList();
    }
}
