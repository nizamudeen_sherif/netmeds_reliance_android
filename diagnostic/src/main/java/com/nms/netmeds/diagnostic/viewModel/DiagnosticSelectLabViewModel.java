package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.LocationServiceCheckListener;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.SelectLabAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivitySelectLabBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResult;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.model.Request.SelectLabRequest;
import com.nms.netmeds.diagnostic.model.SelectLabResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class DiagnosticSelectLabViewModel extends AppViewModel implements SelectLabAdapter.LabAdapterListener {

    private int failedTransactionId;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private ActivitySelectLabBinding selectLabBinding;
    private DiagnosticSelectLabViewModel labViewModel;
    private SelectLabViewModelListener selectLabViewModelListener;
    private SelectLabAdapter mSelectLabAdapter;
    private MutableLiveData<SelectLabResponse> selectLabMutableLiveData = new MutableLiveData<>();
    private List<AvailableLab> availableLabList;
    public List<Test> mSelectedTestList;
    private AvailableLab selectedLab;
    private boolean isMissingTest;
    private SelectLabResponse selectLabResponse;
    private int minimumAmountSelectedPosition;
    private static LocationServiceCheckListener locationServiceCheckListener;
    private MutableLiveData<PinCodeResponse> pinCodeResponseMutableLiveData = new MutableLiveData<>();
    private int viewExpandHeight =  0;

    public boolean isMissingTest() {
        return isMissingTest;
    }

    public void setMissingTest(boolean missingTest) {
        isMissingTest = missingTest;
    }

    public AvailableLab getSelectedLab() {
        return selectedLab;
    }

    public void setSelectedLab(AvailableLab selectedLab) {
        this.selectedLab = selectedLab;
    }

    public DiagnosticSelectLabViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivitySelectLabBinding selectLabBinding, DiagnosticSelectLabViewModel labViewModel, List<Test> selectedTestList, SelectLabViewModelListener selectLabViewModelListener) {
        this.mContext = context;
        this.selectLabBinding = selectLabBinding;
        this.labViewModel = labViewModel;
        this.mSelectedTestList = selectedTestList;
        this.selectLabViewModelListener = selectLabViewModelListener;
        availableLabList = new ArrayList<>();
        initSelectLabAdapter();
        initListener();
    }

    private void initListener() {
        selectLabBinding.testCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLabViewModelListener.vmPatientDetailCallback();
            }
        });

        selectLabBinding.testCount.lytSelectedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Test> testList = getSelectedLab() != null && getSelectedLab().getTestList() != null && getSelectedLab().getTestList().size() > 0 ? getSelectedLab().getTestList() : new ArrayList<Test>();
                selectLabViewModelListener.vmInitBottomView(testList);
            }
        });
    }

    public static void setServiceCheckListener(LocationServiceCheckListener successListener) {
        locationServiceCheckListener = successListener;
    }

    public MutableLiveData<SelectLabResponse> getSelectLabMutableLiveData() {
        return selectLabMutableLiveData;
    }

    public MutableLiveData<PinCodeResponse> getPinCodeResponseMutableLiveData() {
        return pinCodeResponseMutableLiveData;
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case DiagnosticServiceManager.GET_LABS:
                selectLabResponse = new Gson().fromJson(data, SelectLabResponse.class);
                displayMinPrice();
                selectLabMutableLiveData.setValue(selectLabResponse);
                setPrimeMemberDiscount(selectLabResponse);
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                if (data != null) {
                    PinCodeResponse pinCodeResponse = new Gson().fromJson(CommonUtils.isJSONValid(data) ? data : "", PinCodeResponse.class);
                    DefaultAddress defaultAddress = new Gson().fromJson(BasePreference.getInstance(mContext).getDefaultAddress(), DefaultAddress.class);
                    defaultAddress.getResult().getCustomer_details().getAddress().setStreet("");
                    defaultAddress.getResult().getCustomer_details().getAddress().setLandmark("");
                    defaultAddress.getResult().getCustomer_details().getAddress().setCity("");
                    defaultAddress.getResult().getCustomer_details().getAddress().setState("");
                    defaultAddress.getResult().getCustomer_details().getAddress().setPostcode("");
                    defaultAddress.getResult().getCustomer_details().getAddress().setMobileNo("");
                    BasePreference.getInstance(mContext).saveDefaultAddress(new Gson().toJson(defaultAddress));
                    pinCodeResponseMutableLiveData.setValue(pinCodeResponse);
                }
                break;
        }
    }

    private void displayMinPrice() {
        if (selectLabResponse == null || selectLabResponse.getSelectLabResult() == null || selectLabResponse.getSelectLabResult().getResultList() == null
                || selectLabResponse.getSelectLabResult().getResultList().size() == 0) return;
        int minAmount = Integer.MAX_VALUE;
        for (int i = 0; i < selectLabResponse.getSelectLabResult().getResultList().size(); i++) {
            if (selectLabResponse.getSelectLabResult().getResultList().get(i).getPriceDescription() != null && !TextUtils.isEmpty(selectLabResponse.getSelectLabResult().getResultList().get(i).getPriceDescription().getFinalPrice())
                    && Integer.parseInt(selectLabResponse.getSelectLabResult().getResultList().get(i).getPriceDescription().getFinalPrice()) < minAmount) {
                minAmount = Integer.parseInt(selectLabResponse.getSelectLabResult().getResultList().get(i).getPriceDescription().getFinalPrice());
                minimumAmountSelectedPosition = i;
            }
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        selectLabViewModelListener.vmDismissProgress();
        switch (transactionId) {
            case DiagnosticServiceManager.GET_LABS:
                showNoLabFoundView();
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                locationServiceCheckListener.onShowMessage(mContext.getResources().getString(R.string.text_webservice_error_title));
                break;
        }
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case DiagnosticServiceManager.GET_LABS:
                getLabsByTestId(mSelectedTestList);
                break;
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        selectLabBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        selectLabBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        selectLabBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        selectLabBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    @Override
    public void onLabChecked(AvailableLab availableLab) {
        setSelectedLab(availableLab);
        setBottomView();
    }

    @Override
    public void onLabUnChecked() {
        setSelectedLab(new AvailableLab());
        setBottomView();
    }

    @Override
    public void OnPriceBreakupClicked(List<Test> tests) {
        selectLabViewModelListener.onPriceBreakUpClicked(tests);

    }

    private void setBottomView() {
        if (!TextUtils.isEmpty(getTotalAmount())) {
            selectLabBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, getTotalAmount()));
            selectLabBinding.testCount.textLabel.setText(mContext.getResources().getString(R.string.text_total_amount));
            selectLabBinding.testCount.textYouSave.setText(!TextUtils.isEmpty(getDiscountAmount()) ? String.format(Locale.getDefault(), "%s %s%s", mContext.getResources().getString(R.string.text_you_save), DiagnosticConstant.INR, getDiscountAmount()) : "");
            selectLabBinding.testCount.textYouSave.setVisibility(TextUtils.isEmpty(getDiscountAmount()) ? View.GONE : View.VISIBLE);
            if(viewExpandHeight <= 0)
            viewExpandHeight = CommonUtils.getViewHeight(selectLabBinding.testCount.lytTestCount, selectLabViewModelListener.getContext())
                    - (CommonUtils.getViewHeight(selectLabBinding.testCount.textYouSave, selectLabViewModelListener.getContext()));
            CommonUtils.expand(selectLabBinding.testCount.lytTestCount, DiagnosticConstant.COLLAPSE_EXPAND_VIEW_TIMEOUT, viewExpandHeight);
        } else {
            CommonUtils.collapse(selectLabBinding.testCount.lytTestCount, DiagnosticConstant.COLLAPSE_EXPAND_VIEW_TIMEOUT, DiagnosticConstant.COLLAPSING_VIEW_HEIGHT);
        }
    }

    private String getTotalAmount() {
        return getSelectedLab() != null && getSelectedLab().getPriceDescription() != null && !TextUtils.isEmpty(getSelectedLab().getPriceDescription().getFinalPrice()) ? getSelectedLab().getPriceDescription().getFinalPrice() : "";
    }

    private String getDiscountAmount() {
        return getSelectedLab() != null && getSelectedLab().getPriceDescription() != null && !TextUtils.isEmpty(getSelectedLab().getPriceDescription().getDiscountAmount()) ? getSelectedLab().getPriceDescription().getDiscountAmount() : "";
    }

    public interface SelectLabViewModelListener {

        void vmShowProgress();

        void vmDismissProgress();

        void vmInitBottomView(List<Test> testList);

        void vmPatientDetailCallback();

        void onPriceBreakUpClicked(List<Test> tests);

        void vmChangeAddress();

        Context getContext();

    }

    private void initSelectLabAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        selectLabBinding.labListView.setLayoutManager(linearLayoutManager);
        mSelectLabAdapter = new SelectLabAdapter(mContext, availableLabList, mSelectedTestList, 0, this);
        selectLabBinding.labListView.setAdapter(mSelectLabAdapter);
    }

    public void onFilterQueryChanged(CharSequence s) {
        if (mSelectLabAdapter != null) {
            mSelectLabAdapter.getFilter().filter(s.toString());
        }
        selectLabBinding.imgSearch.setImageDrawable(ContextCompat.getDrawable(mContext, s.toString().isEmpty() ? R.drawable.ic_search : R.drawable.ic_clear));
        selectLabBinding.imgSearch.setTag(!TextUtils.isEmpty(s.toString()) ? DiagnosticConstant.TAG_CLEAR : DiagnosticConstant.TAG_SEARCH);
    }

    public void onClear(View v) {
        String tag = v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString()) ? v.getTag().toString() : "";
        if (tag.equals(DiagnosticConstant.TAG_CLEAR)) {
            selectLabBinding.edtFilter.setText("");
            selectLabBinding.imgSearch.setTag(DiagnosticConstant.TAG_SEARCH);
            CommonUtils.hideKeyboard(mContext, selectLabBinding.lytViewContent);
        }
    }

    public void getLabsByTestId(List<Test> selectedTestList) {
        BasePreference basePreference = BasePreference.getInstance(selectLabViewModelListener.getContext());
        if(selectedTestList == null || selectedTestList.size() == 0 || labViewModel == null || basePreference == null) return;
        boolean isConnected = NetworkUtils.isConnected(selectLabViewModelListener.getContext());
        showNoNetworkView(isConnected);
        if (isConnected) {
            selectLabViewModelListener.vmShowProgress();
            SelectLabRequest selectLabRequest = new SelectLabRequest();
            List<String> testIdList = new ArrayList<>();
            for (Test test : selectedTestList) {
                testIdList.add(!TextUtils.isEmpty(test.getTestId()) ? test.getTestId() : "");
            }
            selectLabRequest.setTestIdList(testIdList);
            selectLabRequest.setPinCode(basePreference.getLabPinCode());
            if (!TextUtils.isEmpty(basePreference.getDiagnosticLoginResponse())) {
                try {
                    JSONObject jsonObject = new JSONObject(basePreference.getDiagnosticLoginResponse());
                    selectLabRequest.setUserId(jsonObject.has(DiagnosticConstant.DIAGNOSTIC_USER_ID) ? jsonObject.getString(DiagnosticConstant.DIAGNOSTIC_USER_ID) : "");
                } catch (JSONException e) {
                    selectLabViewModelListener.vmDismissProgress();
                    failedTransactionId = DiagnosticServiceManager.GET_LABS;
                }
            }
            DiagnosticServiceManager.getInstance().getLabs(labViewModel, selectLabRequest, basePreference, getDiagnosticAuthToken(basePreference));
        } else
            failedTransactionId = DiagnosticServiceManager.GET_LABS;
    }

    private String getDiagnosticAuthToken(BasePreference basePreference) {
        if(basePreference == null) return "";
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(basePreference.getDiagnosticLoginResponse(), JustDocUserResponse.class);
        if(diagnosticLoginResult == null || TextUtils.isEmpty(diagnosticLoginResult.getToken())) return "";
        return diagnosticLoginResult.getToken();
    }


    public void selectLabDataAvailable(SelectLabResponse selectLabResponse) {
        selectLabViewModelListener.vmDismissProgress();
        if (selectLabResponse != null) {
            selectLabServiceStatus(selectLabResponse);
        } else
            vmOnError(DiagnosticServiceManager.GET_LABS);
    }

    private void selectLabServiceStatus(SelectLabResponse selectLabResponse) {
        if (selectLabResponse.getServiceStatus() != null && selectLabResponse.getServiceStatus().getStatusCode() != null && selectLabResponse.getServiceStatus().getStatusCode() == 200) {
            selectLabResult(selectLabResponse);
        } else
            setNoLabAvailableError(selectLabResponse);
    }

    private void setNoLabAvailableError(SelectLabResponse selectLabResponse) {
        if (selectLabResponse.getServiceStatus() != null && selectLabResponse.getServiceStatus().getStatusCode() != null && selectLabResponse.getServiceStatus().getStatusCode() == 400) {
            setLabErrorView(selectLabResponse);
        } else
            vmOnError(DiagnosticServiceManager.GET_LABS);
    }


    private void showNoLabFoundView(){
        selectLabBinding.labEmptyView.setVisibility(View.VISIBLE);
        selectLabBinding.lytViewContent.setVisibility(View.VISIBLE);
        selectLabBinding.topLayout.setVisibility(View.GONE);
        selectLabBinding.testCount.lytTestCount.setVisibility(View.GONE);
        selectLabBinding.cardViewPrimeMember.setVisibility(View.GONE);
    }

    private void setLabErrorView(SelectLabResponse selectLabResponse) {
        if (selectLabResponse.getSelectLabResult() != null && !TextUtils.isEmpty(selectLabResponse.getSelectLabResult().getMessage())) {
            showNoLabFoundView();
        } else {
            vmOnError(DiagnosticServiceManager.GET_LABS);
        }
    }

    private void selectLabResult(SelectLabResponse selectLabResponse) {
        if (selectLabResponse.getSelectLabResult() != null) {
            getLabList(selectLabResponse);
        } else
            vmOnError(DiagnosticServiceManager.GET_LABS);
    }

    private void getLabList(SelectLabResponse selectLabResponse) {
        if (selectLabResponse.getSelectLabResult().getResultList() != null && selectLabResponse.getSelectLabResult().getResultList().size() > 0) {
            selectLabBinding.labEmptyView.setVisibility(View.GONE);
            selectLabBinding.lytViewContent.setVisibility(View.VISIBLE);
            selectLabBinding.topLayout.setVisibility(View.VISIBLE);
            selectLabBinding.lytViewContent.setVisibility(View.VISIBLE);
            availableLabList.clear();
            selectLabResponse.getSelectLabResult().getResultList().get(minimumAmountSelectedPosition).setChecked(true);
            onLabChecked(selectLabResponse.getSelectLabResult().getResultList().get(minimumAmountSelectedPosition));
            setCheckedLabToTop(selectLabResponse.getSelectLabResult().getResultList());
            setMissingTest(selectLabResponse.getSelectLabResult().isMissingTest());
            if (selectLabResponse.getSelectLabResult().isMissingTest()) {
                selectLabBinding.cardViewUnAvailableTest.setVisibility(View.VISIBLE);
                mSelectLabAdapter.updateLabAdapter(availableLabList, 1);
                setMissingTest(selectLabResponse);
            } else {
                selectLabBinding.cardViewUnAvailableTest.setVisibility(View.GONE);
                mSelectLabAdapter.updateLabAdapter(availableLabList, 0);
            }
            displayMinPrice();
            setWebEngageCartEvent(selectLabResponse.getSelectLabResult().getResultList());

        }
    }

    // set checked lab having minimum price to top of list
    private void setCheckedLabToTop(List<AvailableLab> availableLabs){
        if(availableLabs ==  null || availableLabs.size() == 0) return;
        if(getSelectedLab()!=null){
            availableLabList.add(getSelectedLab());
            availableLabs.remove(getSelectedLab());
            availableLabList.addAll(availableLabs);
        }else{
            availableLabList.addAll(availableLabs);
        }
    }

    private void setPrimeMemberDiscount(SelectLabResponse selectLabResponse) {
        if (selectLabResponse == null || selectLabResponse.getSelectLabResult() == null || selectLabResponse.getSelectLabResult().getSubscription() == null
                || selectLabResponse.getSelectLabResult().getSubscription().getPrime() == null) {
            selectLabBinding.cardViewPrimeMember.setVisibility(View.GONE);
            return;
        }

        if (selectLabResponse.getSelectLabResult().getSubscription().getPrime().isStatus()) {
            if (!TextUtils.isEmpty(selectLabResponse.getSelectLabResult().getSubscription().getMessage())) {
                selectLabBinding.cardViewPrimeMember.setVisibility(View.VISIBLE);
                selectLabBinding.txtViewPrimeDiscount.setText(selectLabResponse.getSelectLabResult().getSubscription().getMessage());
            }
        }
    }

    private void setWebEngageCartEvent(List<AvailableLab> resultList) {
        ArrayList<HashMap<String, Object>> labArray = new ArrayList<>();
        if (resultList != null && resultList.size() > 0) {
            for (AvailableLab availableLab : resultList) {
                String jsonObject = new Gson().toJson(availableLab);
                Type testType = new TypeToken<HashMap<String, Object>>() {
                }.getType();
                HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                labArray.add(testDetail);
            }
        }
        /*Diagnostic WebEngage Cart Event*/
        WebEngageHelper.getInstance().diagnosticsCartEvent(BasePreference.getInstance(mContext), DiagnosticHelper.getCity(), DiagnosticHelper.getSelectedList(), labArray, selectLabViewModelListener.getContext());
    }

    private void setMissingTest(SelectLabResponse selectLabResponse) {
        if (!TextUtils.isEmpty(selectLabResponse.getSelectLabResult().getUnAvailableTest())) {
            selectLabBinding.unAvailableTestName.setText(selectLabResponse.getSelectLabResult().getUnAvailableTest());
            selectLabBinding.unAvailableTestName.setVisibility(View.VISIBLE);
        } else
            selectLabBinding.unAvailableTestName.setVisibility(View.GONE);
    }

    public void setAddressViewVisibility() {
        if (mSelectedTestList != null && mSelectedTestList.size() > 0) {
            if (!TextUtils.isEmpty(mSelectedTestList.get(0).getCategory()) && mSelectedTestList.get(0).getCategory().equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)) {
                selectLabBinding.txtViewSample.setText(mContext.getResources().getString(R.string.your_address));
            } else {
                selectLabBinding.txtViewSample.setText(mContext.getResources().getString(R.string.sample_pickup_txt));
            }
            StringBuilder addressBuilder = new StringBuilder();
            DefaultAddress defaultAddress = new Gson().fromJson(BasePreference.getInstance(mContext).getDefaultAddress(), DefaultAddress.class);
            addressBuilder.append(!TextUtils.isEmpty(defaultAddress.getResult().getCustomer_details().getAddress().getStreet())
                    ? defaultAddress.getResult().getCustomer_details().getAddress().getStreet() : "");
            addressBuilder.append(!TextUtils.isEmpty(defaultAddress.getResult().getCustomer_details().getAddress().getCity())
                    ? " " + defaultAddress.getResult().getCustomer_details().getAddress().getCity() : "");
            addressBuilder.append(" ").append(BasePreference.getInstance(mContext).getLabPinCode());
            selectLabBinding.txtViewAddress.setText(addressBuilder.toString());
        }
    }

    public void changeAddress() {
        selectLabViewModelListener.vmChangeAddress();
    }

    public void pinCodeServiceCheck(String code) {
        boolean isConnected = NetworkUtils.isConnected(mContext);
        if (isConnected) {
            PinCodeServiceCheckRequest serviceCheckRequest = new PinCodeServiceCheckRequest();
            serviceCheckRequest.setPinCode(code);
            DiagnosticServiceManager.getInstance().pinCodeServiceAvailabilityCheck(labViewModel, serviceCheckRequest, BasePreference.getInstance(mContext));
        } else
            locationServiceCheckListener.onShowMessage(mContext.getResources().getString(R.string.text_nonetwork_error_message));
    }

    public void pinCodeDataAvailable(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse != null) {
            checkPinCodeServiceStatus(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(mContext.getResources().getString(R.string.text_webservice_error_title));
    }

    private void checkPinCodeServiceStatus(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getServiceStatus() != null && pinCodeResponse.getServiceStatus().getStatusCode() != null && pinCodeResponse.getServiceStatus().getStatusCode() == 200) {
            getPinCodeResponseResult(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(pinCodeResponse.getResult() != null && !TextUtils.isEmpty(pinCodeResponse.getResult().getMessage()) ? pinCodeResponse.getResult().getMessage() : mContext.getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeResponseResult(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getResult() != null) {
            getPinCodeService(pinCodeResponse.getResult());
        } else
            locationServiceCheckListener.onShowMessage(mContext.getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeService(PinCodeResult pinCodeResult) {
        if (pinCodeResult.getService()) {
            String pinCode = !TextUtils.isEmpty(pinCodeResult.getPinCode()) ? pinCodeResult.getPinCode() : "";
            String city = !TextUtils.isEmpty(pinCodeResult.getCity()) ? pinCodeResult.getCity() : "";
            DiagnosticHelper.setCity(city);
            BasePreference.getInstance(mContext).setDiagnosticPinCode(pinCode);
            BasePreference.getInstance(mContext).setDiagnosticCity(city);
            BasePreference.getInstance(mContext).setLabPinCode(pinCode);
            getLabsByTestId(mSelectedTestList);
            locationServiceCheckListener.onPinCodeSuccess(pinCode);
        } else
            locationServiceCheckListener.onPinCodeFailed();
    }
}
