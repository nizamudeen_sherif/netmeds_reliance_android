package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiagnosticSearchResult implements Serializable {

    @SerializedName("category")
    private String category;
    @SerializedName(value = "S.no", alternate = {"Sno"})
    private String serialNo;
    @SerializedName(value = "Netmeds ID", alternate = {"itemId"})
    private String netmedsId;
    @SerializedName(value = "Name", alternate = {"itemName"})
    private String name;
    @SerializedName(value = "X price", alternate = {"minPrice"})
    private String price;
    @SerializedName(value = "Available at", alternate = {"availableAt"})
    private String availableAt;
    @SerializedName(value = "Fasting Required", alternate = {"fasting"})
    private String fastingRequired;
    @SerializedName(value = "Type", alternate = {"type"})
    private String type;
    @SerializedName("Keyword")
    private String keyword;
    @SerializedName("No. of tests")
    private String noOfTest;
    @SerializedName("Included Tests")
    private String includeTest;
    @SerializedName(value = "Url - Know more", alternate = {"url"})
    private String knownMoreUrl;
    @SerializedName("objectID")
    private String objectId;
    private boolean isChecked;
    @SerializedName("labName")
    private String labName;

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getNetmedsId() {
        return netmedsId;
    }

    public void setNetmedsId(String netmedsId) {
        this.netmedsId = netmedsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailableAt() {
        return availableAt;
    }

    public void setAvailableAt(String availableAt) {
        this.availableAt = availableAt;
    }

    public String getFastingRequired() {
        return fastingRequired;
    }

    public void setFastingRequired(String fastingRequired) {
        this.fastingRequired = fastingRequired;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getNoOfTest() {
        return noOfTest;
    }

    public void setNoOfTest(String noOfTest) {
        this.noOfTest = noOfTest;
    }

    public String getIncludeTest() {
        return includeTest;
    }

    public void setIncludeTest(String includeTest) {
        this.includeTest = includeTest;
    }

    public String getKnownMoreUrl() {
        return knownMoreUrl;
    }

    public void setKnownMoreUrl(String knownMoreUrl) {
        this.knownMoreUrl = knownMoreUrl;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
