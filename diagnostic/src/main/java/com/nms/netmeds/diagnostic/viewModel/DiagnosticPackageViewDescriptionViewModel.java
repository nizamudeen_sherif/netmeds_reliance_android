package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.font.LatoTextView;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.FragmentDiagnosticPackageDescriptionBinding;
import com.nms.netmeds.diagnostic.model.TestDetailResponse;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;


public class DiagnosticPackageViewDescriptionViewModel extends AppViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private FragmentDiagnosticPackageDescriptionBinding descriptionBinding;
    public TestDetailResponse testDetailResponse;


    public DiagnosticPackageViewDescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, FragmentDiagnosticPackageDescriptionBinding descriptionBinding) {
        this.context = context;
        this.descriptionBinding = descriptionBinding;
        setMeantFor();
        initTabDescriptions();
        setTestReq();
    }

    @SuppressLint("SetTextI18n")
    private void setMeantFor() {
        if (testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getTests() != null && !TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getWhenToGetTested())) {
            descriptionBinding.llPackageMeantFor.setVisibility(View.VISIBLE);
            descriptionBinding.tvPackageMeantForContent.setVisibility(View.VISIBLE);
            descriptionBinding.tvPackageMeantForContent.removeAllViews();
            String[] whenToGetTest = testDetailResponse.getResult().getTests().get(0).getWhenToGetTested().split(",");
            for (String string : whenToGetTest) {
                LayoutInflater li = LayoutInflater.from(context);
                View childView = li.inflate(R.layout.meant_for, null, false);
                LatoTextView textView = childView.findViewById(R.id.txtViewMeantFor);
                textView.setText(CommonUtils.fromHtml(string));
                descriptionBinding.tvPackageMeantForContent.addView(childView);
            }
        } else {
            descriptionBinding.llPackageMeantFor.setVisibility(View.GONE);
            descriptionBinding.tvPackageMeantForContent.setVisibility(View.GONE);
        }
    }

    private void setTestReq() {
        if (testDetailResponse == null || testDetailResponse.getResult() == null
                || testDetailResponse.getResult().getTests() == null || testDetailResponse.getResult().getTests().size() == 0
                || (TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestSample())
                && TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestPreparation()))) {
            descriptionBinding.cardTestReq.setVisibility(View.GONE);
            descriptionBinding.llTestRequirements.setVisibility(View.GONE);
            return;
        }

        descriptionBinding.cardTestReq.setVisibility(View.VISIBLE);
        descriptionBinding.llTestRequirements.setVisibility(View.VISIBLE);
        descriptionBinding.llTestRequirements.removeAllViews();

        if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestSample())) {
            LayoutInflater li = LayoutInflater.from(context);
            View childView = li.inflate(R.layout.test_sample_prepartion, null, false);
            ImageView imgTestSamplePrep = childView.findViewById(R.id.imgTestSamplePrep);
            LatoTextView txtTestSamplePrep = childView.findViewById(R.id.txtTestSamplePrep);
            LatoTextView txtTestSamplePrepValue = childView.findViewById(R.id.txtTestSamplePrepValue);
            imgTestSamplePrep.setImageResource(R.drawable.experience_bachelor);
            txtTestSamplePrep.setText(context.getResources().getString(R.string.diag_test_sample_string));
            txtTestSamplePrepValue.setText(CommonUtils.fromHtml(testDetailResponse.getResult().getTests().get(0).getTestSample()));
            descriptionBinding.llTestRequirements.addView(childView);
        }

        if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestPreparation())) {
            LayoutInflater li = LayoutInflater.from(context);
            View childView = li.inflate(R.layout.test_sample_prepartion, null, false);
            ImageView imgTestSamplePrep = childView.findViewById(R.id.imgTestSamplePrep);
            LatoTextView txtTestSamplePrep = childView.findViewById(R.id.txtTestSamplePrep);
            LatoTextView txtTestSamplePrepValue = childView.findViewById(R.id.txtTestSamplePrepValue);
            imgTestSamplePrep.setImageResource(R.drawable.ic_no_image);
            txtTestSamplePrep.setText(context.getResources().getString(R.string.diag_test_sample_test_prep));
            txtTestSamplePrepValue.setText(CommonUtils.fromHtml(testDetailResponse.getResult().getTests().get(0).getTestPreparation()));
            descriptionBinding.llTestRequirements.addView(childView);
        }
    }


    public void setTestDetailResponse(TestDetailResponse testDetailResponse) {
        this.testDetailResponse = testDetailResponse;
    }

    private void initTabDescriptions() {
        if (testDetailResponse != null && testDetailResponse.getResult() != null && testDetailResponse.getResult().getTests() != null
                && testDetailResponse.getResult().getTests().size() > 0) {
            descriptionBinding.tabDetails.setVisibility(View.VISIBLE);
            descriptionBinding.cardTestDecription.setVisibility(View.VISIBLE);
            LinearLayout parent = descriptionBinding.llFragmentNames;
            final LinkedHashMap<String, String> detailsMap = new LinkedHashMap<>();

            if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestDescription()))
                detailsMap.put(context.getResources().getString(R.string.test_details_txt_test_desc), testDetailResponse.getResult().getTests().get(0).getTestDescription());
            if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getWhenToGetTested()))
                detailsMap.put(context.getResources().getString(R.string.test_when_to_get_tested), testDetailResponse.getResult().getTests().get(0).getWhenToGetTested());
            if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getTestInterpretation()))
                detailsMap.put(context.getResources().getString(R.string.test_interpretation), testDetailResponse.getResult().getTests().get(0).getTestInterpretation());
            if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getCommonQuestions()))
                detailsMap.put(context.getResources().getString(R.string.test_common_questions), testDetailResponse.getResult().getTests().get(0).getCommonQuestions());
            if (!TextUtils.isEmpty(testDetailResponse.getResult().getTests().get(0).getExpertAdvice()))
                detailsMap.put(context.getResources().getString(R.string.test_expert_advice), testDetailResponse.getResult().getTests().get(0).getExpertAdvice());

            int indexCount = 0;
            for (Map.Entry<String, String> entry : detailsMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (!TextUtils.isEmpty(value)) {
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final LatoTextView child = (LatoTextView) inflater.inflate(R.layout.diagnostic_test_tab_item, null);
                    child.setText(key);
                    if (indexCount == 0) {
                        child.setTextColor(context.getResources().getColor(R.color.colorDeepAqua));
                        child.setBackground(context.getResources().getDrawable(R.drawable.white_rectangle_rounded_top));
                        descriptionBinding.webPackageDescription.loadDataWithBaseURL("", value, DiagnosticConstant.MIME_TYPE, DiagnosticConstant.WEB_ENCODING_FORMAT, "");
                    }
                    child.setTag(key);
                    child.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String key = (String) v.getTag();
                            TextView selectedView = descriptionBinding.getRoot().findViewWithTag(key);
                            selectedView.setTextColor(context.getResources().getColor(R.color.colorDeepAqua));
                            selectedView.setBackground(context.getResources().getDrawable(R.drawable.white_rectangle_rounded_top));
                            descriptionBinding.webPackageDescription.loadDataWithBaseURL("", detailsMap.get(key), DiagnosticConstant.MIME_TYPE, DiagnosticConstant.WEB_ENCODING_FORMAT, "");
                            for (Map.Entry<String, String> entry : detailsMap.entrySet()) {
                                String k = entry.getKey();
                                String value = entry.getValue();
                                if (!key.equals(k)) {
                                    TextView notSelectedTV = descriptionBinding.getRoot().findViewWithTag(k);
                                    notSelectedTV.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
                                    notSelectedTV.setBackground(context.getResources().getDrawable(R.drawable.grey_rectangle_rounded_top));
                                }
                            }
                        }
                    });
                    indexCount++;
                    parent.addView(child);
                }
            }
        }
    }
}