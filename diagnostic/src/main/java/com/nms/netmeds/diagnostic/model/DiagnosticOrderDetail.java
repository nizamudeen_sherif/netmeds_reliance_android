package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.base.model.Test;

import java.io.Serializable;
import java.util.List;

public class DiagnosticOrderDetail implements Serializable {
    @SerializedName("lab")
    private LabDescription labDescription;
    @SerializedName("tests")
    private List<Test> testList;

    public LabDescription getLabDescription() {
        return labDescription;
    }

    public void setLabDescription(LabDescription labDescription) {
        this.labDescription = labDescription;
    }

    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }
}
