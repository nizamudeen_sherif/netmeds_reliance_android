package com.nms.netmeds.diagnostic.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.FragmentDiagnosticPackageDescriptionBinding;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPackageViewDescriptionViewModel;


public class DiagnosticPackageDescriptionFragment extends Fragment {

    public DiagnosticPackageDescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentDiagnosticPackageDescriptionBinding descriptionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_diagnostic_package_description, container, false);
        DiagnosticPackageViewDescriptionViewModel viewModel = new DiagnosticPackageViewDescriptionViewModel(getActivity().getApplication());
        if (getActivity() != null && !getActivity().isFinishing() && ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel != null &&
                ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse() != null) {
            viewModel.setTestDetailResponse(((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse());
        }
        viewModel.init(getActivity(), descriptionBinding);
        descriptionBinding.setViewModel(viewModel);
        return descriptionBinding.getRoot();
    }
}
