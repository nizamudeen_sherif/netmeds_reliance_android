package com.nms.netmeds.diagnostic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivitySearchTestBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticAlgoliaSearchResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.DiagnosticSearchResult;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticSearchTestViewModel;
import com.webengage.sdk.android.WebEngage;

import java.io.Serializable;
import java.util.ArrayList;

public class DiagnosticSearchTestActivity extends BaseViewModelActivity<DiagnosticSearchTestViewModel> implements DiagnosticSearchTestViewModel.DiagnosticSearchTestListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener {
    private ActivitySearchTestBinding searchTestBinding;
    private DiagnosticSearchTestViewModel searchTestViewModel;
    private ArrayList<DiagnosticPackage> packages;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.INTENT_PACKAGE_LIST)) {
            packages = (ArrayList<DiagnosticPackage>) getIntent().getSerializableExtra(DiagnosticConstant.INTENT_PACKAGE_LIST);
        }

        searchTestBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_test);
        toolBarSetUp(searchTestBinding.toolbar);
        searchTestBinding.setViewModel(onCreateViewModel());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_FILTER_TYPE)) {
            searchTestViewModel.setFilterType(getIntent().getStringExtra(DiagnosticConstant.INTENT_KEY_FILTER_TYPE));
        }

        if (DiagnosticHelper.IsRemovedElementPackage() || getIntent() != null && getIntent().hasExtra(DiagnosticConstant.INTENT_FROM_PACKAGE_SEARCH) && getIntent().getBooleanExtra(DiagnosticConstant.INTENT_FROM_PACKAGE_SEARCH, false)) {
            searchTestBinding.txtViewToolBarTitle.setText(getResources().getString(R.string.text_select_package));
            searchTestBinding.edtFilter.setHint(getResources().getString(R.string.text_search_package_name));
            searchTestViewModel.setFromTestOrPackageSearch(true);
            searchTestViewModel.setFilter(getResources().getString(R.string.filter_packages));
            searchTestViewModel.searchApiCall(getResources().getString(R.string.filter_packages));
            DiagnosticHelper.setIsRemovedElementPackage(false);
            return;
        }
        DiagnosticHelper.setIsRemovedElementPackage(false);
        searchTestViewModel.setFromTestOrPackageSearch(false);
        searchTestBinding.txtViewToolBarTitle.setText(getResources().getString(R.string.txt_lab_tests));
        searchTestBinding.edtFilter.setHint(getResources().getString(R.string.text_diagnosis_search_test_names));
        searchTestViewModel.setFilter(searchTestViewModel.apiFilterString(false));
        searchTestViewModel.searchApiCall(searchTestViewModel.apiFilterString(false));


    }

    @Override
    protected DiagnosticSearchTestViewModel onCreateViewModel() {
        searchTestViewModel = ViewModelProviders.of(this).get(DiagnosticSearchTestViewModel.class);
        searchTestViewModel.getSearchDataMutableLiveData().observe(this, new SearchResponseObserver());
        searchTestViewModel.init(this, searchTestBinding, searchTestViewModel, this);
        onRetry(searchTestViewModel);
        return searchTestViewModel;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void vmShowProgress() {
        showProgress(DiagnosticSearchTestActivity.this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmNavigateToLab() {
        /*Diagnostic WebEngage Test Added Event*/
        WebEngageHelper.getInstance().diagnosticTestAddedEvent(BasePreference.getInstance(this), DiagnosticHelper.getCity(), DiagnosticHelper.getSelectedList(), this);
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
        LaunchIntentManager.routeToActivity(DiagnosticSearchTestActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), intent, DiagnosticSearchTestActivity.this);
    }

    @Override
    public void vmInitBottomView() {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), DiagnosticHelper.getSelectedList(), DiagnosticHelper.getTestType(), DiagnosticConstant.SEARCH_TEST_PAGE, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, "Bottom_fragment").commitAllowingStateLoss();
    }

    @Override
    public void vmNavigateToTestDetail(DiagnosticSearchResult searchResult) {
        if (searchResult == null) return;
        Intent intent = new Intent();
        Test test = new Test();
        test.setUrl(searchResult.getKnownMoreUrl());
        test.setTestName(searchResult.getName());
        test.setCategory(searchResult.getCategory());
        if (searchResult.getType().equalsIgnoreCase(getResources().getString(R.string.txt_test_type))) {
            intent.putExtra(DiagnosticConstant.INTENT_TEST_SELECTED, test);
            LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_diagnostic_test_detail), intent, this);
        } else if (searchResult.getType().equalsIgnoreCase(getResources().getString(R.string.txt_profile_type)) || searchResult.getType().equalsIgnoreCase(getResources().getString(R.string.txt_packages_type))
                || searchResult.getType().equalsIgnoreCase(getResources().getString(R.string.txt_package_type))) {
            final DiagnosticPackage diagnosticPackage = new DiagnosticPackage();
            ArrayList<Test> arrayList = new ArrayList<>();
            arrayList.add(test);
            diagnosticPackage.setTestList(arrayList);
            intent.putExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED, diagnosticPackage);
            if (packages != null && packages.size() > 0) {
                intent.putExtra(DiagnosticConstant.INTENT_ALL_PACKAGE, packages);
            }
            LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_package_detail), intent, this);
        }
    }

    @Override
    public void onNavigation() {
        vmNavigateToLab();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {

    }

    @Override
    public void onUpdateSearchList(Test removeTest) {
        searchTestViewModel.onUpdateSearchList(removeTest);
    }

    @Override
    public void onUpdateLabList() {

    }

    public class SearchResponseObserver implements Observer<DiagnosticAlgoliaSearchResponse> {

        @Override
        public void onChanged(@Nullable DiagnosticAlgoliaSearchResponse algoliaSearchResponse) {
            searchTestViewModel.searchResponseDataAvailable(algoliaSearchResponse);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        searchTestViewModel.setLabTestCount();
        searchTestViewModel.setSelectedFilter();
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_SEARCH_TEST);
    }
}