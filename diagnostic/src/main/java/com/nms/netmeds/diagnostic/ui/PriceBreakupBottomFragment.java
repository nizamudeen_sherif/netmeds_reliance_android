package com.nms.netmeds.diagnostic.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.PriceBreakupBottomViewBinding;
import com.nms.netmeds.diagnostic.viewModel.PriceBreakupViewModel;

import java.util.List;

@SuppressLint("ValidFragment")
public class PriceBreakupBottomFragment extends BaseDialogFragment implements PriceBreakupViewModel.PriceBreakupViewModelListener {
    private Application application;
    private List<Test> tests;

    public PriceBreakupBottomFragment(Application application, List<Test> tests) {
        this.application = application;
        this.tests = tests;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setWindowAnimations(com.nms.netmeds.base.R.style.BottomSheetDialogAnimation);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setCancelable(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        PriceBreakupBottomViewBinding bottomViewBinding = DataBindingUtil.inflate(inflater, R.layout.price_breakup_bottom_view, container, false);
        PriceBreakupViewModel viewModel = new PriceBreakupViewModel(application);
        viewModel.init(getActivity(), bottomViewBinding, tests, this);
        bottomViewBinding.setViewModel(viewModel);
        return bottomViewBinding.getRoot();
    }

    @Override
    public void onCloseDialog() {
        PriceBreakupBottomFragment.this.dismissAllowingStateLoss();
    }
}
