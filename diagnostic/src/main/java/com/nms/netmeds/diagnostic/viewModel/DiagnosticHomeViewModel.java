package com.nms.netmeds.diagnostic.viewModel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.consultation.model.request.LoginRequest;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.LocationServiceCheckListener;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.DiagnosticHomeBannerAdapter;
import com.nms.netmeds.diagnostic.adapter.MostPopularPackageAdapter;
import com.nms.netmeds.diagnostic.adapter.PackageAdapter;
import com.nms.netmeds.diagnostic.adapter.TestAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticHomeBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPackageResult;
import com.nms.netmeds.diagnostic.model.OfferBannerResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResult;
import com.nms.netmeds.diagnostic.model.PopularTestResponse;
import com.nms.netmeds.diagnostic.model.PopularTestResult;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.nms.netmeds.base.utils.DiagnosticConstant.CATEGORY_PATH;
import static com.nms.netmeds.base.utils.DiagnosticConstant.CATEGORY_RADIO;

public class DiagnosticHomeViewModel extends AppViewModel implements TestAdapter.TestAdapterListener, PackageAdapter.PackageAdapterListener {
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityDiagnosticHomeBinding homeBinding;
    private DiagnosticHomeViewModel homeViewModel;
    private DiagnosticHomeListener homeModelListener;
    private static LocationServiceCheckListener locationServiceCheckListener;
    private int failedTransactionId;
    private BasePreference basePreference;
    private MutableLiveData<PopularTestResponse> popularTestMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<PinCodeResponse> pinCodeResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<OfferBannerResponse> offerBannerMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<DiagnosticPackageResponse> popularPackageMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<DiagnosticPackageResponse> mostPopularPackageLiveData = new MutableLiveData<>();
    private final MutableLiveData<LoginResponse> diagnosticLoginResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<LoganResponse> loganMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<DefaultAddress> defaultAddressMutableLiveData = new MutableLiveData<>();
    private List<Test> popularTestList;
    private TestAdapter mTestAdapter;
    private String loganSession;
    private PackageAdapter mHealthPackageAdapter;
    private List<DiagnosticPackage> popularPackageList, mostPopularPackageList;
    private MostPopularPackageAdapter mostPopularPackageAdapter;
    private int retryCount = 0;


    public String getLoganSession() {
        return loganSession;
    }

    public void setLoganSession(String loganSession) {
        this.loganSession = loganSession;
    }

    public DiagnosticHomeViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityDiagnosticHomeBinding binding, DiagnosticHomeViewModel diagnosticHomeViewModel, DiagnosticHomeListener listener) {
        this.context = context;
        this.homeBinding = binding;
        this.homeViewModel = diagnosticHomeViewModel;
        this.homeModelListener = listener;
        basePreference = BasePreference.getInstance(context);
        popularTestList = new ArrayList<>();
        popularPackageList = new ArrayList<>();
        mostPopularPackageList = new ArrayList<>();
        initHomeApiCall();
        initListener();
        initPopularAdapter();
        initHealthPackageAdapter();
        initMostPopularPackageAdapter();
        showPinCode();
    }

    private void initListener() {
        homeBinding.testCount.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeModelListener.vmNavigateToLab();
            }
        });

        homeBinding.testCount.lytSelectedTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeModelListener.vmInitBottomView();
            }
        });
    }

    public void onViewAllHealthPackage() {
        homeModelListener.vmViewAllPackageCallback(popularPackageList);
    }

    public void onViewAllTest() {
        homeModelListener.vmNavigateToSearch(popularTestList, true);
    }


    @Override
    public void onTestChecked(Test selectedTest) {
        setLabTestCount();
    }

    @Override
    public void onTestUnChecked() {
        setLabTestCount();
    }

    @Override
    public void onTestClicked(Test test) {
        homeModelListener.vmNavigateToTestDetail(popularTestList, test);

    }

    @Override
    public void onPackageChecked(DiagnosticPackage diagnosticPackage) {

    }

    @Override
    public void onPackageUnChecked() {

    }

    public interface DiagnosticHomeListener {

        void navigateToSearch(String filter);

        void vmViewAllPackageCallback(List<DiagnosticPackage> popularPackageList);

        void vmLocationDialog();

        void vmShowProgress();

        void vmDismissProgress();

        void vmNavigateToSearch(List<Test> popularTestList, boolean flag);

        void vmNavigateToLab();

        void vmInitBottomView();

        void vmInitPinCodeView();

        void vmNavigateToTestDetail(List<Test> popularTestList, Test test);

        void forceLogout();

        Context getContext();

        void vmNavigateDiagGuestUserToLabPage();

        void finishMe();
    }

    public static void setServiceCheckListener(LocationServiceCheckListener successListener) {
        locationServiceCheckListener = successListener;
    }

    public void onCurrentLocationDataAvailable(String code) {
        if (TextUtils.isEmpty(code)) {
            locationServiceCheckListener.onPinCodeFailed();
        } else
            pinCodeServiceCheck(code);
    }

    public String setPinCode() {
        return DiagnosticHelper.getFormattedAddress(homeModelListener.getContext());
    }

    public void showPinCode() {
        if (TextUtils.isEmpty(BasePreference.getInstance(context).getDiagnosticPinCode())) {
            homeBinding.lytPinCode.setVisibility(View.GONE);
        } else {
            homeBinding.lytPinCode.setVisibility(View.VISIBLE);
        }
    }

    public void onChangeLocation() {
        homeModelListener.vmLocationDialog();
    }

    public MutableLiveData<PopularTestResponse> getPopularTestMutableLiveData() {
        return popularTestMutableLiveData;
    }

    public MutableLiveData<PinCodeResponse> getPinCodeResponseMutableLiveData() {
        return pinCodeResponseMutableLiveData;
    }

    public MutableLiveData<OfferBannerResponse> getOfferBannerMutableLiveData() {
        return offerBannerMutableLiveData;
    }

    public MutableLiveData<DiagnosticPackageResponse> getPopularPackageMutableLiveData() {
        return popularPackageMutableLiveData;
    }


    public MutableLiveData<DiagnosticPackageResponse> getMostPopularPackageLiveData() {
        return mostPopularPackageLiveData;
    }

    public void setMostPopularPackageLiveData(MutableLiveData<DiagnosticPackageResponse> mostPopularPackageLiveData) {
        this.mostPopularPackageLiveData = mostPopularPackageLiveData;
    }


    public MutableLiveData<LoginResponse> getLoginResponseMutableLiveData() {
        return diagnosticLoginResponseMutableLiveData;
    }

    public MutableLiveData<LoganResponse> getLoganMutableLiveData() {
        return loganMutableLiveData;
    }

    public MutableLiveData<DefaultAddress> getDefaultAddressMutableLiveData() {
        return defaultAddressMutableLiveData;
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case DiagnosticServiceManager.POPULAR_TEST:
                PopularTestResponse popularTestResponse = new Gson().fromJson(data, PopularTestResponse.class);
                popularTestMutableLiveData.setValue(popularTestResponse);
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                if (data != null) {
                    PinCodeResponse pinCodeResponse = new Gson().fromJson(CommonUtils.isJSONValid(data) ? data : "", PinCodeResponse.class);
                    DiagnosticHelper.setDefaultAddressToEmpty(homeModelListener.getContext());
                    pinCodeResponseMutableLiveData.setValue(pinCodeResponse);
                }
                break;
            case DiagnosticServiceManager.OFFER_BANNER:
                OfferBannerResponse offerBannerResponse = new Gson().fromJson(data, OfferBannerResponse.class);
                offerBannerMutableLiveData.setValue(offerBannerResponse);
                break;
            case DiagnosticServiceManager.POPULAR_PACKAGE:
                DiagnosticPackageResponse diagnosticPackageResponse = new Gson().fromJson(data, DiagnosticPackageResponse.class);
                popularPackageMutableLiveData.setValue(diagnosticPackageResponse);
                break;
            case DiagnosticServiceManager.MOST_POPULAR_PACKAGE: {
                DiagnosticPackageResponse popularPackageResponse = new Gson().fromJson(data, DiagnosticPackageResponse.class);
                mostPopularPackageLiveData.setValue(popularPackageResponse);
                break;
            }
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
                diagnosticLoginResponseMutableLiveData.setValue(loginResponse);
                getCustomerDeliveryAddress();
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                LoganResponse loganResponse = new Gson().fromJson(data, LoganResponse.class);
                loganMutableLiveData.setValue(loganResponse);
                break;
            case DiagnosticServiceManager.CUSTOMER_ME:
                if (!TextUtils.isEmpty(data)) {
                    MStarBasicResponseTemplateModel customerResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                    if (customerResponse != null && customerResponse.getResult() != null && customerResponse.getResult().getAddressList() != null
                            && customerResponse.getResult().getAddressList().size() > 0) {
                        for (MStarAddressModel customerAddress : customerResponse.getResult().getAddressList()) {
                            if (customerAddress.getId() == basePreference.getDiagDefaultAddressId()) {
                                DiagnosticHelper.setDefaultAddress(customerAddress, context);
                                defaultAddressMutableLiveData.setValue(DiagnosticHelper.setDefaultAddress(customerAddress, context));
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        homeModelListener.vmDismissProgress();
        stopShimmer(homeBinding.popularTestShimmer.popularTestShimmer, homeBinding.popularTestView);
        stopShimmer(homeBinding.offerBannerShimmer.bannerShimmer,homeBinding.diagnosticOfferBanner);
        stopShimmer(homeBinding.mostPopularShimmer.mostPopularShimmer, homeBinding.mostPopularPackageList);
        switch (transactionId) {
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                locationServiceCheckListener.onShowMessage(context.getResources().getString(R.string.text_webservice_error_title));
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                vmOnError(transactionId);
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                homeModelListener.forceLogout();
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                doDiagnosticLogin();
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                homeModelListener.vmShowProgress();
                getLogan();
                break;
             default:{
                 stopShimmer(homeBinding.popularTestShimmer.popularTestShimmer, homeBinding.popularTestView);
                 stopShimmer(homeBinding.offerBannerShimmer.bannerShimmer,homeBinding.diagnosticOfferBanner);
                 stopShimmer(homeBinding.mostPopularShimmer.mostPopularShimmer, homeBinding.mostPopularPackageList);
                 stopShimmer(homeBinding.healthPackageShimmer.healthPackageShimmer, homeBinding.healthPackageView);
                 initHomeApiCall();
                 break;
             }
        }
    }

    public void getPopularTest() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            startShimmer(homeBinding.popularTestShimmer.popularTestShimmer);
            DiagnosticServiceManager.getInstance().getPopularTest(homeViewModel, basePreference.getDiagnosticPinCode(), basePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.POPULAR_TEST;
    }

    private void showNoNetworkView(boolean isConnected) {
        if (isConnected && BasePreference.getInstance(homeModelListener.getContext()).isDiaFirstTimeLogin() && !BasePreference.getInstance(homeModelListener.getContext()).isGuestCart()
                && DiagnosticHelper.getSelectedList() != null && DiagnosticHelper.getSelectedList().size() > 0) {
            homeBinding.frameGuestUser.setVisibility(View.VISIBLE);
        }
        homeBinding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        homeBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        homeBinding.lytViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        homeBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
        homeModelListener.vmDismissProgress();
        homeBinding.frameGuestUser.setVisibility(View.GONE);
        BasePreference.getInstance(homeModelListener.getContext()).setDiaFirstTimeLogin(false);
    }

    public void onPopularTestDataAvailable(PopularTestResponse popularTestResponse) {
        if (popularTestResponse != null) {
            serviceStatusCheck(popularTestResponse);
        }
    }

    private void serviceStatusCheck(PopularTestResponse popularTestResponse) {
        if (popularTestResponse.getServiceStatus() != null && popularTestResponse.getServiceStatus().getStatusCode() != null && popularTestResponse.getServiceStatus().getStatusCode() == 200) {
            getPopularTestResult(popularTestResponse);
        }
    }

    private void getPopularTestResult(PopularTestResponse popularTestResponse) {
        if (popularTestResponse.getPopularTestResult() != null) {
            stopShimmer(homeBinding.popularTestShimmer.popularTestShimmer, homeBinding.popularTestView);
            PopularTestResult popularTestResult = popularTestResponse.getPopularTestResult();
            if (popularTestResult != null && popularTestResult.getPopularTestList() != null && popularTestResult.getPopularTestList().size() > 0) {
                popularTestList.clear();
                popularTestList.addAll(popularTestResult.getPopularTestList());
                mTestAdapter.updateTestAdapter(popularTestList);
                if (BasePreference.getInstance(homeModelListener.getContext()).isGuestCart() && BasePreference.getInstance(homeModelListener.getContext()).isDiaFirstTimeLogin()) {
                    updatePopularTestAdapter();
                }
            }
        }
    }

    private void initPopularAdapter() {
        homeBinding.popularTestView.setLayoutManager(new LinearLayoutManager(context));
        homeBinding.popularTestView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        homeBinding.popularTestView.setNestedScrollingEnabled(false);
        mTestAdapter = new TestAdapter(context, popularTestList, TestAdapter.POPULAR_TEST, this);
        homeBinding.popularTestView.setAdapter(mTestAdapter);
    }

    private void initHealthPackageAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        linearLayoutManager.setReverseLayout(false);
        homeBinding.healthPackageView.setLayoutManager(linearLayoutManager);
        homeBinding.healthPackageView.setNestedScrollingEnabled(false);
        mHealthPackageAdapter = new PackageAdapter(context, popularPackageList, PackageAdapter.POPULAR_PACKAGE, this);
        homeBinding.healthPackageView.setAdapter(mHealthPackageAdapter);
    }

    private void initMostPopularPackageAdapter() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        homeBinding.mostPopularPackageList.setLayoutManager(gridLayoutManager);
        homeBinding.mostPopularPackageList.setNestedScrollingEnabled(false);
        mostPopularPackageAdapter = new MostPopularPackageAdapter(mostPopularPackageList, context);
        homeBinding.mostPopularPackageList.setAdapter(mostPopularPackageAdapter);
    }

    public void pinCodeServiceCheck(String code) {
        boolean isConnected = NetworkUtils.isConnected(context);
        if (isConnected) {
            PinCodeServiceCheckRequest serviceCheckRequest = new PinCodeServiceCheckRequest();
            serviceCheckRequest.setPinCode(code);
            DiagnosticServiceManager.getInstance().pinCodeServiceAvailabilityCheck(homeViewModel, serviceCheckRequest, basePreference);
        } else
            locationServiceCheckListener.onShowMessage(context.getResources().getString(R.string.text_nonetwork_error_message));
    }

    public void pinCodeDataAvailable(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse != null) {
            checkPinCodeServiceStatus(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(context.getResources().getString(R.string.text_webservice_error_title));
    }

    private void checkPinCodeServiceStatus(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getServiceStatus() != null && pinCodeResponse.getServiceStatus().getStatusCode() != null && pinCodeResponse.getServiceStatus().getStatusCode() == 200) {
            getPinCodeResponseResult(pinCodeResponse);
        } else
            locationServiceCheckListener.onShowMessage(pinCodeResponse.getResult() != null && !TextUtils.isEmpty(pinCodeResponse.getResult().getMessage()) ? pinCodeResponse.getResult().getMessage() : context.getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeResponseResult(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getResult() != null) {
            getPinCodeService(pinCodeResponse.getResult());
        } else
            locationServiceCheckListener.onShowMessage(context.getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeService(PinCodeResult pinCodeResult) {
        if (pinCodeResult.getService()) {
            String pinCode = !TextUtils.isEmpty(pinCodeResult.getPinCode()) ? pinCodeResult.getPinCode() : "";
            String city = !TextUtils.isEmpty(pinCodeResult.getCity()) ? pinCodeResult.getCity() : "";
            DiagnosticHelper.setCity(city);
            basePreference.setDiagnosticPinCode(pinCode);
            basePreference.setDiagnosticCity(city);
            basePreference.setLabPinCode(pinCode);
            locationServiceCheckListener.onPinCodeSuccess(pinCode);
        } else
            locationServiceCheckListener.onPinCodeFailed();
    }

    public void setLabTestCount() {
        if (!showLabTestCount()) {
            homeBinding.testCount.textTestCount.setText(String.format(Locale.getDefault(), "%02d", selectedCount()));
            homeBinding.testCount.textLabel.setText(context.getResources().getQuantityString(R.plurals.text_selected_test_label, selectedCount()));
            CommonUtils.expand(homeBinding.testCount.lytTestCount, DiagnosticConstant.COLLAPSE_EXPAND_VIEW_TIMEOUT, CommonUtils.getViewHeight(homeBinding.testCount.lytTestCount, homeModelListener.getContext()));
        } else {
            CommonUtils.collapse(homeBinding.testCount.lytTestCount, DiagnosticConstant.COLLAPSE_EXPAND_VIEW_TIMEOUT, DiagnosticConstant.COLLAPSING_VIEW_HEIGHT);
        }
    }

    private boolean showLabTestCount() {
        return selectedCount() == 0;
    }

    private int selectedCount() {
        return DiagnosticHelper.getSelectedList().size();
    }


    private void getTestCategories() {
        startShimmer(homeBinding.categoryTestShimmer.categoryShimmer);
        stopShimmer(homeBinding.categoryTestShimmer.categoryShimmer, homeBinding.lLayoutTestCategory);
    }

    private void getOfferBanner() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            startShimmer(homeBinding.offerBannerShimmer.bannerShimmer);
            homeBinding.diagnosticOfferBanner.setVisibility(View.GONE);
            DiagnosticServiceManager.getInstance().getDiagnosticOfferBanner(homeViewModel, basePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.OFFER_BANNER;
    }

    public void offerBannerDataAvailable(OfferBannerResponse offerBannerResponse) {
        if (offerBannerResponse != null && offerBannerResponse.getServiceStatus() != null && offerBannerResponse.getServiceStatus().getStatusCode() != null && offerBannerResponse.getServiceStatus().getStatusCode() == 200) {
            showOfferBanner(offerBannerResponse);
        }
    }

    private void showOfferBanner(OfferBannerResponse offerBannerResponse) {
        if (offerBannerResponse != null && offerBannerResponse.getOfferBannerResult() != null) {
            stopShimmer(homeBinding.offerBannerShimmer.bannerShimmer,homeBinding.diagnosticOfferBanner);
            basePreference.setDiagnosticFirstOrder(offerBannerResponse.getOfferBannerResult().getFirstOrder());
            if (offerBannerResponse.getOfferBannerResult().getMstarBanner() != null && offerBannerResponse.getOfferBannerResult().getMstarBanner().size() > 0) {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                homeBinding.diagnosticOfferBanner.setLayoutManager(linearLayoutManager);
                DiagnosticHomeBannerAdapter diagnosticHomeBannerAdapter = new DiagnosticHomeBannerAdapter(context, offerBannerResponse.getOfferBannerResult().getMstarBanner());
                homeBinding.diagnosticOfferBanner.setAdapter(diagnosticHomeBannerAdapter);
                CommonUtils.snapHelper(homeBinding.diagnosticOfferBanner);
                refreshViewModel();
                setLabDetails(offerBannerResponse);
            }
        }
    }

    private void setLabDetails(OfferBannerResponse offerBannerResponse) {
        if (offerBannerResponse.getOfferBannerResult().getLab_details() != null) {
            if (!TextUtils.isEmpty(offerBannerResponse.getOfferBannerResult().getLab_details().getLab_certification())) {
                homeBinding.textDesc1.setText(offerBannerResponse.getOfferBannerResult().getLab_details().getLab_certification());
            }
            if (!TextUtils.isEmpty(offerBannerResponse.getOfferBannerResult().getLab_details().getOthers())) {
                homeBinding.tvDesc3.setText(offerBannerResponse.getOfferBannerResult().getLab_details().getOthers());
            }
            if (!TextUtils.isEmpty(offerBannerResponse.getOfferBannerResult().getLab_details().getDiscount())) {
                homeBinding.tvDesc2.setText(offerBannerResponse.getOfferBannerResult().getLab_details().getDiscount());
            }
        }
    }


    private void initHomeApiCall() {
        getPopularTest();
        getOfferBanner();
        getPopularPackage();
        getMostPopularPackage();
        getTestCategories();
    }

    private void refreshViewModel() {
        homeBinding.setViewModel(homeViewModel);
    }

    public void onNavigateToSearch() {
        homeModelListener.vmNavigateToSearch(popularTestList, false);
    }


    public void getPopularPackage() {
        if(popularPackageList != null && popularPackageList.size() > 0 ) return;
        boolean isConnected = NetworkUtils.isConnected(homeModelListener.getContext());
        if (isConnected) {
            startShimmer(homeBinding.healthPackageShimmer.healthPackageShimmer);
            DiagnosticServiceManager.getInstance().getPopularPackage(homeViewModel, basePreference.getDiagnosticPinCode(), basePreference);
        }
    }

    public void getMostPopularPackage() {
        if(mostPopularPackageList != null && mostPopularPackageList.size() >0) return;
        boolean isConnected = NetworkUtils.isConnected(homeModelListener.getContext());
        if(isConnected) {
            startShimmer(homeBinding.mostPopularShimmer.mostPopularShimmer);
            DiagnosticServiceManager.getInstance().getMostPopularPackage(homeViewModel, basePreference.getDiagnosticPinCode(), true, basePreference);
        }
    }


    public void onPopularPackageDataAvailable(DiagnosticPackageResponse popularPackageResponse) {
        if (popularPackageResponse != null) {
            popularPackageServiceStatusCheck(popularPackageResponse);
        }
    }

    public void onMostPopularPackageDataAvailable(DiagnosticPackageResponse popularPackageResponse) {
        if (popularPackageResponse != null
                && popularPackageResponse.getDiagnosticPackageResult() != null
                && popularPackageResponse.getDiagnosticPackageResult().getPackageList() != null && popularPackageResponse.getDiagnosticPackageResult().getPackageList().size() > 0) {
            stopShimmer(homeBinding.mostPopularShimmer.mostPopularShimmer, homeBinding.mostPopularPackageList);
            mostPopularPackageList.clear();
            mostPopularPackageList.addAll(popularPackageResponse.getDiagnosticPackageResult().getPackageList());
            mostPopularPackageAdapter.notifyDataSetChanged();
        } else {
            stopShimmer(homeBinding.mostPopularShimmer.mostPopularShimmer, homeBinding.mostPopularPackageList);
            homeBinding.lLayoutMostPopular.setVisibility(View.GONE);
        }
    }

    private void popularPackageServiceStatusCheck(DiagnosticPackageResponse popularPackageResponse) {
        if (popularPackageResponse.getServiceStatus() != null && popularPackageResponse.getServiceStatus().getStatusCode() != null && popularPackageResponse.getServiceStatus().getStatusCode() == 200) {
            getPopularPackageResult(popularPackageResponse);
        }
    }

    private void getPopularPackageResult(DiagnosticPackageResponse popularPackageResponse) {
        if (popularPackageResponse.getDiagnosticPackageResult() != null) {
            setPopularPackageAdapter(popularPackageResponse.getDiagnosticPackageResult());
        }
    }

    private void setPopularPackageAdapter(DiagnosticPackageResult diagnosticPackageResult) {
        if (diagnosticPackageResult.getPackageList() != null && diagnosticPackageResult.getPackageList().size() > 0) {
            stopShimmer(homeBinding.healthPackageShimmer.healthPackageShimmer, homeBinding.healthPackageView);
            popularPackageList.clear();
            popularPackageList.addAll(diagnosticPackageResult.getPackageList());
            mHealthPackageAdapter.updatePackageAdapter(popularPackageList, PackageAdapter.POPULAR_PACKAGE);
        }
    }

    public void onUpdatePopularList(Test removeTest) {
        for (Test test : popularTestList) {
            if (test.getTestId().equals(removeTest.getTestId())) {
                test.setChecked(false);
                break;
            }
        }
        mTestAdapter.updateTestAdapter(popularTestList);
        setLabTestCount();
    }

    public void doDiagnosticLogin() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            LoginRequest diagnosticLoginRequest = new LoginRequest();
            diagnosticLoginRequest.setSessionId(getLogonSessionId());
            diagnosticLoginRequest.setSource(DiagnosticConstant.SOURCE);
            diagnosticLoginRequest.setAppversion(CommonUtils.getAppVersionCode(context));
            diagnosticLoginRequest.setMedium(DiagnosticConstant.APP_SOURCE);
            diagnosticLoginRequest.setCustomer_id(!TextUtils.isEmpty(BasePreference.getInstance(context).getMstarCustomerId())
                    ? BasePreference.getInstance(context).getMstarCustomerId() : "");
            if(!BasePreference.getInstance(homeModelListener.getContext()).isGuestCart() && CommonUtils.isFirstTimeLoginToDiagnostic(homeModelListener.getContext()) && DiagnosticHelper.getSelectedList()!=null && DiagnosticHelper.getSelectedList().size() >0){
                homeBinding.frameGuestUser.setVisibility(View.VISIBLE);
            } else {
                homeModelListener.vmShowProgress();
            }
            ConsultationServiceManager.getInstance().consultationDoLogin(this, diagnosticLoginRequest, ConfigMap.getInstance().getProperty(ConfigConstant.DIAGNOSTIC_BASE_URL));
        } else
            failedTransactionId = ConsultationServiceManager.TRANSACTION_LOGIN;
    }

    private String getLogonSessionId() {
        return getLoganSession() != null ? getLoganSession() : "";
    }

    public void diagnosticLoginDataAvailable(LoginResponse loginResponse) {
        if (loginResponse != null) {
            loginServiceStatus(loginResponse);
        } else {
            homeModelListener.vmDismissProgress();
            homeBinding.frameGuestUser.setVisibility(View.GONE);
            BasePreference.getInstance(homeModelListener.getContext()).setDiaFirstTimeLogin(false);
            homeModelListener.forceLogout();
        }
    }

    private void loginServiceStatus(LoginResponse loginResponse) {
        if (loginResponse.getServiceStatus() != null && loginResponse.getServiceStatus().getStatusCode() != null && loginResponse.getServiceStatus().getStatusCode() == DiagnosticConstant.STATUS_CODE_200
                && loginResponse.getResult() != null && !TextUtils.isEmpty(loginResponse.getResult().getToken())) {
            loginResult(loginResponse);
            retryCount = 0;
        } else {
            homeBinding.lytViewContent.setVisibility(View.GONE);
            validateLoginError(loginResponse);
        }
    }

    private void loginResult(LoginResponse loginResponse) {
        if (loginResponse.getResult() != null) {
            basePreference.setDiagnosticLoinResponse(new Gson().toJson(loginResponse.getResult()));
            diagnosticToken(loginResponse.getResult());
            /*WebEngage diagnostic register event*/
            WebEngageHelper.getInstance().diagnosticsRegistrationEvent(basePreference, homeModelListener.getContext());
            homeModelListener.vmNavigateDiagGuestUserToLabPage();
        } else {
            homeModelListener.vmDismissProgress();
            homeBinding.lytViewContent.setVisibility(View.GONE);
        }
    }

    private void diagnosticToken(JustDocUserResponse result) {
        homeModelListener.vmDismissProgress();
        if (!TextUtils.isEmpty(result.getToken())) {
            homeBinding.lytViewContent.setVisibility(View.VISIBLE);
            homeModelListener.vmInitPinCodeView();
        } else {
            homeBinding.lytViewContent.setVisibility(View.GONE);
        }
    }

    private void validateLoginError(LoginResponse loginResponse) {
        if (loginResponse.getResult() != null && !TextUtils.isEmpty(loginResponse.getResult().getMessage())) {
            validateSessionToken(loginResponse.getResult());
        } else {
            homeModelListener.vmDismissProgress();
            homeBinding.frameGuestUser.setVisibility(View.GONE);
            BasePreference.getInstance(homeModelListener.getContext()).setDiaFirstTimeLogin(false);
            homeModelListener.forceLogout();
        }
    }

    private void validateSessionToken(JustDocUserResponse justDocUserResponse) {
        if (justDocUserResponse.getMessage().equals(ConsultationConstant.SESSION_ID_NOT_FOUND) || justDocUserResponse.getMessage().contains(ConsultationConstant.INVALID_SESSION_ID)) {
            retryCount++;
            getLogan();
        } else {
            homeModelListener.vmDismissProgress();
            SnackBarHelper.snackBarCallBack(homeBinding.lytViewContent, context, justDocUserResponse.getMessage());
        }
    }

    private void getLogan() {
        if (TextUtils.isEmpty(basePreference.getMstarCustomerId()) || TextUtils.isEmpty(basePreference.getMStarSessionId()) || retryCount >= 3) {
            homeModelListener.vmDismissProgress();
            homeModelListener.forceLogout();
            homeBinding.frameGuestUser.setVisibility(View.GONE);
            BasePreference.getInstance(homeModelListener.getContext()).setDiaFirstTimeLogin(false);
            return;
        }
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            Map<String, String> header = new HashMap<>();
            header.put(AppConstant.MSTAR_AUTH_TOKEN, basePreference.getMStarSessionId());
            header.put(AppConstant.MSTAR_API_HEADER_USER_ID, basePreference.getMstarCustomerId());
            ConsultationServiceManager.getInstance().getLoganToken(this, header);
        } else
            failedTransactionId = ConsultationServiceManager.LOGAN_TOKEN;
    }

    public void onLoganTokenDataAvailable(LoganResponse loganResponse) {
        if (loganResponse != null) {
            logonServiceStatus(loganResponse);
        } else {
            homeModelListener.vmDismissProgress();
            homeModelListener.forceLogout();
            homeBinding.frameGuestUser.setVisibility(View.GONE);
            BasePreference.getInstance(homeModelListener.getContext()).setDiaFirstTimeLogin(false);
        }
    }

    private void logonServiceStatus(LoganResponse loganResponse) {
        if (!TextUtils.isEmpty(loganResponse.getStatus()) && loganResponse.getStatus().equalsIgnoreCase(DiagnosticConstant.SUCCESS)) {
            validateSessionId(loganResponse);
        } else {
            homeModelListener.vmDismissProgress();
            homeModelListener.forceLogout();
            homeBinding.frameGuestUser.setVisibility(View.GONE);
            BasePreference.getInstance(homeModelListener.getContext()).setDiaFirstTimeLogin(false);
        }
    }

    private void validateSessionId(LoganResponse loganResponse) {
        homeModelListener.vmDismissProgress();
        if (loganResponse.getLoganResult() != null && !TextUtils.isEmpty(loganResponse.getLoganResult().getSessionToken())) {
            setLoganSession(loganResponse.getLoganResult().getSessionToken());
            BasePreference.getInstance(context).setLoganSession(loganResponse.getLoganResult().getSessionToken());
            doDiagnosticLogin();
        }
    }

    private void resetPopularTestAdapter() {
        for (Test test : popularTestList)
            test.setChecked(false);
        mTestAdapter.updateTestAdapter(popularTestList);
    }

    public void updatePopularTestAdapter() {
        resetPopularTestAdapter();
        for (Test test : popularTestList)
            checkTestExistOrNot(test);
        mTestAdapter.updateTestAdapter(popularTestList);
    }

    private void checkTestExistOrNot(Test test) {
        for (Test selectedTest : DiagnosticHelper.getSelectedList()) {
            if (test.getTestId().equals(selectedTest.getTestId()))
                test.setChecked(true);
        }
    }

    public void clickOnBackArrow() {
        homeModelListener.finishMe();
    }

    public void clickPathology() {
        WebEngageHelper.getInstance().digTypeSelected(basePreference, CommonUtils.getValidStringWithFirstLetterUpperCase(CATEGORY_PATH), homeModelListener.getContext());
        homeModelListener.navigateToSearch(context.getResources().getString(R.string.filter_type_pathology));
    }

    public void clickRadiology() {
        WebEngageHelper.getInstance().digTypeSelected(basePreference, CommonUtils.getValidStringWithFirstLetterUpperCase(CATEGORY_RADIO), homeModelListener.getContext());
        homeModelListener.navigateToSearch(context.getResources().getString(R.string.filter_type_radiology));
    }


    public void onPermissionDenied() {
        locationServiceCheckListener.onPermissionDeniedMessage();
    }

    private void getCustomerDeliveryAddress() {
        // Not require to call the address api, in case user is not logged-in
        if (BasePreference.getInstance(homeModelListener.getContext()).isGuestCart()) return;
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        if (isConnected) {
            DiagnosticServiceManager.getInstance().getAllAddress(this, basePreference);
        }
    }

    private void startShimmer(final ShimmerFrameLayout shimmerFrameLayout) {
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmerAnimation();
    }

    private void stopShimmer(final ShimmerFrameLayout shimmerFrameLayout, final View view) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                view.setVisibility(View.VISIBLE);
            }
        }, DiagnosticConstant.SHIMMER_TIMEOUT);
    }

}


