package com.nms.netmeds.diagnostic.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.FragmentDiagnosticTimeSlotBinding;
import com.nms.netmeds.diagnostic.model.AvailableSlot;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTimeSlotFragmentViewModel;

@SuppressLint("ValidFragment")
public class DiagnosticTimeSlotFragment extends BaseViewModelFragment {

    private FragmentDiagnosticTimeSlotBinding fragmentDiagnosticTimeSlotBinding;
    private DiagnosticTimeSlotFragmentViewModel timeSlotFragmentViewModel;

    public static DiagnosticTimeSlotFragment newInstance(AvailableSlot availableSlot) {
        DiagnosticTimeSlotFragment diagnosticTimeSlotFragment = new DiagnosticTimeSlotFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("AVAILABLE_SLOT", availableSlot);
        diagnosticTimeSlotFragment.setArguments(bundle);
        return diagnosticTimeSlotFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentDiagnosticTimeSlotBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_diagnostic_time_slot, container, false);
        timeSlotFragmentViewModel = onCreateViewModel();
        return fragmentDiagnosticTimeSlotBinding.getRoot();
    }


    @Override
    protected DiagnosticTimeSlotFragmentViewModel onCreateViewModel() {
        timeSlotFragmentViewModel = ViewModelProviders.of(this).get(DiagnosticTimeSlotFragmentViewModel.class);
        timeSlotFragmentViewModel.init(getActivity(), fragmentDiagnosticTimeSlotBinding, timeSlotFragmentViewModel, (AvailableSlot) getArguments().getSerializable("AVAILABLE_SLOT"));
        fragmentDiagnosticTimeSlotBinding.setViewModel(timeSlotFragmentViewModel);
        return timeSlotFragmentViewModel;
    }
}
