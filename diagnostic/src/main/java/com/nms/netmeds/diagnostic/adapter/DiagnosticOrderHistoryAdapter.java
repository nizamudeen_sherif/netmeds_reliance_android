package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterDiagnosticOrderItemBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;

import java.util.List;


public class DiagnosticOrderHistoryAdapter extends RecyclerView.Adapter<DiagnosticOrderHistoryAdapter.OrderHistoryHolder> {
    private Context mContext;
    private List<DiagnosticOrder> orderList;

    public DiagnosticOrderHistoryAdapter(Context context, List<DiagnosticOrder> orderList) {
        this.mContext = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderHistoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AdapterDiagnosticOrderItemBinding adapterDiagnosticOrderItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_diagnostic_order_item, viewGroup, false);
        return new OrderHistoryHolder(adapterDiagnosticOrderItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHistoryHolder holder, @SuppressLint("RecyclerView") final int position) {
        final DiagnosticOrder diagnosticOrder = orderList.get(position);
        PatientDetail patientDetail = diagnosticOrder.getPatientDetails();
        LabDescription labDescription = diagnosticOrder.getDiagnosticOrderDetail() != null && diagnosticOrder.getDiagnosticOrderDetail().getLabDescription() != null ? diagnosticOrder.getDiagnosticOrderDetail().getLabDescription() : new LabDescription();
        String labName = !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
        String createdDate = !TextUtils.isEmpty(diagnosticOrder.getCreatedAt()) ? DateTimeUtils.getInstance().utcToLocal(DateTimeUtils.yyyyMMddTHHmmssSSS, DateTimeUtils.MMMddyyyyhhmma, diagnosticOrder.getCreatedAt()) : "";
        holder.diagnosticOrderItemBinding.textName.setText(CommonUtils.convertToTitleCase(patientDetail.getName()));
        holder.diagnosticOrderItemBinding.textLabName.setText(String.format("%s %s %s %s", mContext.getResources().getString(R.string.text_by), labName, mContext.getResources().getString(R.string.text_on), createdDate));
        holder.diagnosticOrderItemBinding.textOrderStatus.setText("");
        holder.diagnosticOrderItemBinding.textName.setVisibility(TextUtils.isEmpty(patientDetail.getName()) ? View.GONE : View.VISIBLE);
        holder.diagnosticOrderItemBinding.textLabName.setVisibility(TextUtils.isEmpty(labName) && TextUtils.isEmpty(createdDate) ? View.GONE : View.VISIBLE);
        if (diagnosticOrder.getDiagnosticOrderDetail() != null && diagnosticOrder.getDiagnosticOrderDetail().getTestList() != null && diagnosticOrder.getDiagnosticOrderDetail().getTestList().size() > 0)
            setItemAdapter(holder.diagnosticOrderItemBinding, diagnosticOrder.getDiagnosticOrderDetail().getTestList());
        holder.diagnosticOrderItemBinding.btnViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, diagnosticOrder);
                LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_order_detail), intent, mContext);
            }
        });
        holder.diagnosticOrderItemBinding.btnTrackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(DiagnosticConstant.KEY_ORDER_ID, diagnosticOrder.getOrderId());
                LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_diagnostic_track_order), intent, mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    class OrderHistoryHolder extends RecyclerView.ViewHolder {
        private AdapterDiagnosticOrderItemBinding diagnosticOrderItemBinding;

        OrderHistoryHolder(@NonNull AdapterDiagnosticOrderItemBinding diagnosticOrderItemBinding) {
            super(diagnosticOrderItemBinding.getRoot());
            this.diagnosticOrderItemBinding = diagnosticOrderItemBinding;
        }
    }

    public void updateOrderHistoryAdapter(List<DiagnosticOrder> updateOrderList) {
        this.orderList = updateOrderList;
        notifyDataSetChanged();
    }

    private void setItemAdapter(AdapterDiagnosticOrderItemBinding diagnosticOrderItemBinding, List<Test> itemList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        diagnosticOrderItemBinding.testList.setLayoutManager(linearLayoutManager);
        diagnosticOrderItemBinding.testList.setNestedScrollingEnabled(false);
        DiagnosticOrderItemAdapter orderItemAdapter = new DiagnosticOrderItemAdapter(mContext, itemList, false);
        diagnosticOrderItemBinding.testList.setAdapter(orderItemAdapter);
    }
}