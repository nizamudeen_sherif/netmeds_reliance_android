package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class DiagnosticPackageResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("result")
    private DiagnosticPackageResult diagnosticPackageResult;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public DiagnosticPackageResult getDiagnosticPackageResult() {
        return diagnosticPackageResult;
    }

    public void setDiagnosticPackageResult(DiagnosticPackageResult diagnosticPackageResult) {
        this.diagnosticPackageResult = diagnosticPackageResult;
    }
}
