package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DiagnosticAlgoliaSearchResponse implements Serializable {
    @SerializedName("hits")
    private List<DiagnosticSearchResult> diagnosticSearchResultList = null;
    @SerializedName("nbHits")
    private Integer nbHits;
    @SerializedName("page")
    private Integer page;
    @SerializedName("nbPages")
    private Integer nbPages;
    @SerializedName("hitsPerPage")
    private Integer hitsPerPage;
    @SerializedName("processingTimeMS")
    private Integer processingTimeMS;
    @SerializedName("exhaustiveFacetsCount")
    private boolean exhaustiveFacetsCount;
    @SerializedName("exhaustiveNbHits")
    private boolean exhaustiveNbHits;
    @SerializedName("query")
    private String query;
    @SerializedName("params")
    private String params;
    @SerializedName("facets")
    private Map<String, Map<String, String>> facetList;

    public List<DiagnosticSearchResult> getDiagnosticSearchResultList() {
        return diagnosticSearchResultList;
    }

    public void setDiagnosticSearchResultList(List<DiagnosticSearchResult> diagnosticSearchResultList) {
        this.diagnosticSearchResultList = diagnosticSearchResultList;
    }

    public Integer getNbHits() {
        return nbHits;
    }

    public void setNbHits(Integer nbHits) {
        this.nbHits = nbHits;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getNbPages() {
        return nbPages;
    }

    public void setNbPages(Integer nbPages) {
        this.nbPages = nbPages;
    }

    public Integer getHitsPerPage() {
        return hitsPerPage;
    }

    public void setHitsPerPage(Integer hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public Integer getProcessingTimeMS() {
        return processingTimeMS;
    }

    public void setProcessingTimeMS(Integer processingTimeMS) {
        this.processingTimeMS = processingTimeMS;
    }

    public boolean getExhaustiveFacetsCount() {
        return exhaustiveFacetsCount;
    }

    public void setExhaustiveFacetsCount(boolean exhaustiveFacetsCount) {
        this.exhaustiveFacetsCount = exhaustiveFacetsCount;
    }

    public boolean getExhaustiveNbHits() {
        return exhaustiveNbHits;
    }

    public void setExhaustiveNbHits(boolean exhaustiveNbHits) {
        this.exhaustiveNbHits = exhaustiveNbHits;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Map<String, Map<String, String>> getFacetList() {
        return facetList;
    }

    public void setFacetList(Map<String, Map<String, String>> facetList) {
        this.facetList = facetList;
    }
}
