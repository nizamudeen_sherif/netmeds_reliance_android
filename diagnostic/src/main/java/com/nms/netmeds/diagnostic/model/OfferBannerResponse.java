package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class OfferBannerResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("result")
    private OfferBannerResult offerBannerResult;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public OfferBannerResult getOfferBannerResult() {
        return offerBannerResult;
    }

    public void setOfferBannerResult(OfferBannerResult offerBannerResult) {
        this.offerBannerResult = offerBannerResult;
    }
}
