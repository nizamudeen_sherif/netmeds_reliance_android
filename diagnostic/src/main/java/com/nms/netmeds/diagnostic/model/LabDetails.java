package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LabDetails implements Serializable {
    @SerializedName("short_Details")
    private String shortDetails;
    @SerializedName("rating")
    private String rating;
    @SerializedName("labName")
    private String labName;
    @SerializedName("logo")
    private String logo;
    @SerializedName("id")
    private String id;
    @SerializedName("lab_certification")
    private String lab_certification;
    @SerializedName("discount")
    private String discount;
    @SerializedName("others")
    private String others;


    public String getLab_certification() {
        return lab_certification;
    }

    public void setLab_certification(String lab_certification) {
        this.lab_certification = lab_certification;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getShortDetails() {
        return shortDetails;
    }

    public void setShortDetails(String shortDetails) {
        this.shortDetails = shortDetails;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
