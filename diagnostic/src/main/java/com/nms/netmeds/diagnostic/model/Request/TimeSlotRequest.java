package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TimeSlotRequest {
    @SerializedName("pinCode")
    private String pinCode;
    @SerializedName("labId")
    private int labId;
    @SerializedName("cId")
    private int cId;
    @SerializedName("category")
    private String category;
    @SerializedName("testIds")
    private ArrayList<String> testids;

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public ArrayList<String> getTestids() {
        return testids;
    }

    public void setTestids(ArrayList<String> testids) {
        this.testids = testids;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }
}
