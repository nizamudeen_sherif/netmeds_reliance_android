package com.nms.netmeds.diagnostic.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class DiagnosticOrderCreateResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updateOn;
    @SerializedName("result")
    private DiagnosticOrderCreateResult orderCreateResult;

    public String getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(String updateOn) {
        this.updateOn = updateOn;
    }

    public DiagnosticOrderCreateResult getOrderCreateResult() {
        return orderCreateResult;
    }

    public void setOrderCreateResult(DiagnosticOrderCreateResult orderCreateResult) {
        this.orderCreateResult = orderCreateResult;
    }
}
