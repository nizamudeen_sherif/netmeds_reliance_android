package com.nms.netmeds.diagnostic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticViewOrderDetailBinding;
import com.nms.netmeds.diagnostic.model.DiagnosticOrder;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticOrderDetailViewModel;

public class DiagnosticOrderDetailActivity extends BaseViewModelActivity<DiagnosticOrderDetailViewModel> implements DiagnosticOrderDetailViewModel.DiagnosticOrderDetailListener {
    private ActivityDiagnosticViewOrderDetailBinding viewOrderDetailBinding;
    private DiagnosticOrderDetailViewModel orderDetailViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewOrderDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_view_order_detail);
        toolBarSetUp(viewOrderDetailBinding.toolbar);
        initToolBar();
        viewOrderDetailBinding.setViewModel(onCreateViewModel());
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL))
                orderDetailViewModel.setDiagnosticViewOrderDetail((DiagnosticOrder) intent.getSerializableExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL));
        }
    }

    @Override
    protected DiagnosticOrderDetailViewModel onCreateViewModel() {
        orderDetailViewModel = ViewModelProviders.of(this).get(DiagnosticOrderDetailViewModel.class);
        intentValue();
        orderDetailViewModel.init(this, viewOrderDetailBinding, orderDetailViewModel, this);
        return orderDetailViewModel;
    }

    protected void initToolBar() {
        viewOrderDetailBinding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        viewOrderDetailBinding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        viewOrderDetailBinding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        viewOrderDetailBinding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            DiagnosticOrderDetailActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void vmShowProgress() {
        showProgress(DiagnosticOrderDetailActivity.this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmCallbackCancelOrder(String orderId) {

    }

    @Override
    public void vmCallbackNeedHelp() {
        DiagnosticNeedHelpFragment diagnosticNeedHelpFragment = new DiagnosticNeedHelpFragment(getApplication());
        getSupportFragmentManager().beginTransaction().add(diagnosticNeedHelpFragment, "Diagnostic_need_help_fragment").commitAllowingStateLoss();
    }

    @Override
    public void vmCallbackShowStatus(String orderId) {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_ORDER_ID, orderId);
        LaunchIntentManager.routeToActivity(DiagnosticOrderDetailActivity.this.getResources().getString(R.string.route_diagnostic_track_order), intent, DiagnosticOrderDetailActivity.this);
    }
}
