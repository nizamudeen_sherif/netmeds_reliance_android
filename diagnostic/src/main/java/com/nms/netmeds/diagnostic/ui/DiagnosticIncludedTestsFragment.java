package com.nms.netmeds.diagnostic.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.FragmentDiagnosticIncludedTestsBinding;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPackageIncludedTestsViewModel;

public class DiagnosticIncludedTestsFragment extends Fragment {

    public DiagnosticIncludedTestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentDiagnosticIncludedTestsBinding includedTestsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_diagnostic_included_tests, container, false);
        if (getActivity() != null && !getActivity().isFinishing() && getActivity().getApplication() != null) {
            DiagnosticPackageIncludedTestsViewModel viewModel = new DiagnosticPackageIncludedTestsViewModel(getActivity().getApplication());
            includedTestsBinding.setViewModel(viewModel);
            viewModel.init(getContext(), includedTestsBinding, ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel != null && ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse() != null && ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse().getResult() != null && ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse().getResult().getTests() != null && ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse().getResult().getTests().size() > 0 ? ((DiagnosticPackageActivity) getActivity()).diagnosticPackageDescriptionViewModel.getTestDetailResponse().getResult().getTests().get(0).getChild() : "");
        }
        return includedTestsBinding.getRoot();
    }
}
