package com.nms.netmeds.diagnostic.model.Request;

import com.google.gson.annotations.SerializedName;

public class DiagnosticCouponDetails {

    @SerializedName("details")
    private DiagnosticApplyCouponRequest diagnosticApplyCouponRequest;
    @SerializedName("coupon")
    private String coupon;
    @SerializedName("userId")
    private int userId;

    public DiagnosticApplyCouponRequest getDiagnosticApplyCouponRequest() {
        return diagnosticApplyCouponRequest;
    }

    public void setDiagnosticApplyCouponRequest(DiagnosticApplyCouponRequest diagnosticApplyCouponRequest) {
        this.diagnosticApplyCouponRequest = diagnosticApplyCouponRequest;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


}
