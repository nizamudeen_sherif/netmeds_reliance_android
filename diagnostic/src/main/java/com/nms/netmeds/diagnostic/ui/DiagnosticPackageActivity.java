package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityDiagnosticPackageBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPackageDescriptionViewModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Package Detail Activity
 */
public class DiagnosticPackageActivity extends BaseViewModelActivity<DiagnosticPackageDescriptionViewModel> implements DiagnosticPackageDescriptionViewModel.TestDetailListener, LocationFragment.LocationFragmentListener{

    private ActivityDiagnosticPackageBinding diagnosticPackageBinding;
    public DiagnosticPackageDescriptionViewModel diagnosticPackageDescriptionViewModel;
    private DiagnosticPackage diagnosticPackage;
    private View.OnClickListener backListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DiagnosticHelper.isRadioTestExist()) {
            DiagnosticHelper.clearSelectedList();
        }
        getIntentValue();
        diagnosticPackageBinding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_package);
        diagnosticPackageBinding.setViewModel(onCreateViewModel());
        setBackListener();
        diagnosticPackageBinding.arrowBack.setOnClickListener(backListener);
    }

    private void getIntentValue() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM)) {
                ArrayList<String> arrayList = getIntent().getStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM);
                if (arrayList != null && arrayList.size() > 0 && !TextUtils.isEmpty(arrayList.get(0))) {
                    diagnosticPackage = new DiagnosticPackage();
                    ArrayList<Test> testArrayList = new ArrayList<>();
                    Test selectedTest = new Test();
                    selectedTest.setTestName("");
                    selectedTest.setUrl(arrayList.get(0));
                    testArrayList.add(selectedTest);
                    diagnosticPackage.setTestList(testArrayList);
                } else {
                    finish();
                }
            }
            if (getIntent().hasExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED)) {
                diagnosticPackage = (DiagnosticPackage) getIntent().getSerializableExtra(DiagnosticConstant.INTENT_PACKAGE_SELECTED);
                if (diagnosticPackage != null && diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 &&
                        !TextUtils.isEmpty(diagnosticPackage.getTestList().get(0).getCategory())) {
                    if ((!diagnosticPackage.getTestList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_RADIO) || !DiagnosticHelper.isRadioTestExist()) &&
                            (!diagnosticPackage.getTestList().get(0).getCategory().equals(DiagnosticConstant.CATEGORY_PATH) || !DiagnosticHelper.isPathoTestExist())) {
                        DiagnosticHelper.clearSelectedList();
                    }
                }
            }
        }
    }

    protected DiagnosticPackageDescriptionViewModel onCreateViewModel() {
        diagnosticPackageDescriptionViewModel = ViewModelProviders.of(this).get(DiagnosticPackageDescriptionViewModel.class);
        diagnosticPackageDescriptionViewModel.init(this, diagnosticPackageBinding, this , getIntent()!=null && getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM));
        diagnosticPackageDescriptionViewModel.setActivity(DiagnosticPackageActivity.this);
        diagnosticPackageDescriptionViewModel.setDiagnosticPackage(diagnosticPackage);
        diagnosticPackageDescriptionViewModel.getPinCodeResponseMutableLiveData().observe(this, new PinCodeResponseObserver());
        diagnosticPackageDescriptionViewModel.getTestDetails();
        return diagnosticPackageDescriptionViewModel;
    }

    private void onNavigation(DiagnosticPackage diagnosticPackage) {
        DiagnosticHelper.setTest(DiagnosticHelper.addATest(diagnosticPackage));
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
        LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_select_lab), intent, this);
    }

    private void setBackListener() {
        backListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    @Override
    public void vmShowProgress() {
        diagnosticPackageBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        diagnosticPackageBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void vmFinishActivity() {
        finish();
    }

    @Override
    public void navigateToBookTest() {
        if (diagnosticPackageDescriptionViewModel.isLabAvailable() && diagnosticPackageDescriptionViewModel.isTestAvailable() && diagnosticPackageDescriptionViewModel.testDetailResponse.getResult() != null) {
            if (diagnosticPackageDescriptionViewModel.getTestType().equalsIgnoreCase(getResources().getString(R.string.txt_profile_type))) {
                Intent intent = new Intent();
                Test selectedTest = diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getTests().get(0);
                selectedTest.setPriceInfo(DiagnosticHelper.getPriceInfo(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(0).getPriceDescription()));
                DiagnosticHelper.setTest(selectedTest);
                intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
                LaunchIntentManager.routeToActivity(DiagnosticPackageActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), intent, this);
            } else {
                DiagnosticPackage diagnosticPackage = new DiagnosticPackage();
                for (Test test : diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getTests()) {
                    test.setPriceInfo(DiagnosticHelper.getPriceInfo(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(0).getPriceDescription()));
                }
                diagnosticPackage.setTestList(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getTests());
                diagnosticPackage.setLabDescription(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(0).getLabDescription());
                diagnosticPackage.setPriceDescription(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(0).getPriceDescription());
                onNavigation(diagnosticPackage);
            }
        }
    }

    @Override
    public void vmSearchPackage() {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.INTENT_FROM_PACKAGE_SEARCH, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        LaunchIntentManager.routeToActivity(DiagnosticPackageActivity.this.getResources().getString(R.string.route_diagnostic_search_test), intent, this);
    }

    @Override
    public void vmNavigateToPatientDetails(int position) {
        if (diagnosticPackageDescriptionViewModel.isLabAvailable() && diagnosticPackageDescriptionViewModel.isTestAvailable() && diagnosticPackageDescriptionViewModel.testDetailResponse.getResult() != null) {
            if (diagnosticPackageDescriptionViewModel.getTestType().equalsIgnoreCase(getResources().getString(R.string.txt_profile_type))) {
                Intent intent = new Intent();
                Test selectedTest = diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getTests().get(0);
                selectedTest.setPriceInfo(DiagnosticHelper.getPriceInfo(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(position).getPriceDescription()));
                DiagnosticHelper.setTest(selectedTest);
                intent.putExtra(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) DiagnosticHelper.getSelectedList());
                LaunchIntentManager.routeToActivity(DiagnosticPackageActivity.this.getResources().getString(R.string.route_diagnostic_select_lab), intent, this);
                return;
            }
            AvailableLab availableLab = new AvailableLab();
            Test selectedTest = diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getTests().get(0);
            if (diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(position).getPriceDescription() != null) {
                selectedTest.setPriceInfo(DiagnosticHelper.getPriceInfo(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(position).getPriceDescription()));
            }
            ArrayList<Test> arrayList = new ArrayList<>();
            arrayList.add(selectedTest);
            availableLab.setTestList(arrayList);
            availableLab.setLabDescription(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(position).getLabDescription());
            availableLab.setPriceDescription(diagnosticPackageDescriptionViewModel.testDetailResponse.getResult().getLabs().get(position).getPriceDescription());
            Intent intent = new Intent();
            intent.putExtra(DiagnosticConstant.KEY_SELECTED_LAB, availableLab);
            intent.putExtra(DiagnosticConstant.KEY_INTENT_FROM, DiagnosticConstant.LAB_PAGE);
            LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_diagnostic_patient_detail), intent, this);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void vmChangeAddress() {
        LocationFragment locationFragment = new LocationFragment(false);
        getSupportFragmentManager().beginTransaction().add(locationFragment, "Location_fragment").commitNowAllowingStateLoss();
    }

    @Override
    public void onPinCodeCheck(String pinCode) {
        if(isDiagPackageDescViewModelNotNull())
        diagnosticPackageDescriptionViewModel.pinCodeServiceCheck(pinCode);
    }

    @Override
    public void onLocationPermission() {

    }

    @Override
    public void onLocationDialogClose() {

    }

    @Override
    public void onLocationServiceResponse() {
        if(isDiagPackageDescViewModelNotNull()) {
            diagnosticPackageDescriptionViewModel.getTestDetails();
            diagnosticPackageDescriptionViewModel.setPinCode();
        }
    }

    @Override
    public void onChangedDefaultAddress() {
        changeSelectedPinCode();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (BasePreference.getInstance(this).getChangeDiagnosticHome()) {
            changeSelectedPinCode();
        }
    }

    // Pin Code Response Observer
    public class PinCodeResponseObserver implements Observer<PinCodeResponse> {
        @Override
        public void onChanged(@Nullable PinCodeResponse pinCodeResponse) {
            diagnosticPackageDescriptionViewModel.pinCodeDataAvailable(pinCodeResponse);
        }
    }

    // check if viewModel is null
    private boolean isDiagPackageDescViewModelNotNull() {
        return diagnosticPackageDescriptionViewModel!=null;
    }


    // change selected pin code (either by entering manual or by selecting the address)
    private void changeSelectedPinCode() {
        BasePreference basePreference = BasePreference.getInstance(this);
        DefaultAddress address = new Gson().fromJson(basePreference.getDefaultAddress(), DefaultAddress.class);
        basePreference.setDiagnosticPinCode(TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())
                ? TextUtils.isEmpty(basePreference.getDiagnosticPinCode()) ? DiagnosticConstant.DEFAULT_PIN_CODE : basePreference.getDiagnosticPinCode() : address.getResult().getCustomer_details().getAddress().getPostcode());
        basePreference.setLabPinCode(TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())
                ? TextUtils.isEmpty(basePreference.getDiagnosticPinCode()) ? DiagnosticConstant.DEFAULT_PIN_CODE : basePreference.getDiagnosticPinCode() : address.getResult().getCustomer_details().getAddress().getPostcode());
        onLocationServiceResponse();
    }
}