package com.nms.netmeds.diagnostic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.adapter.TimeSlotAdapter;
import com.nms.netmeds.diagnostic.adapter.TimeSlotViewPagerAdapter;
import com.nms.netmeds.diagnostic.databinding.ActivitySelectTimeSlotBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.AvailableSlot;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.model.TimeSlotResponse;
import com.nms.netmeds.diagnostic.model.TimeSlotResult;
import com.nms.netmeds.diagnostic.model.WeekTabView;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticTimeSlotViewModel;
import com.webengage.sdk.android.WebEngage;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DiagnosticSelectTimeSlotActivity extends BaseViewModelActivity<DiagnosticTimeSlotViewModel> implements DiagnosticTimeSlotViewModel.DiagnosticTimeSlotListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener {

    private ActivitySelectTimeSlotBinding activitySelectTimeSlotBinding;
    private DiagnosticTimeSlotViewModel timeSlotViewModel;
    private SlotTime slotTime;
    private TimeSlotViewPagerAdapter mTimeSlotViewPagerAdapter;
    private int viewExpandHeight =  0;

    public SlotTime getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(SlotTime slotTime) {
        this.slotTime = slotTime;
    }

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySelectTimeSlotBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_time_slot);
        toolBarSetUp(activitySelectTimeSlotBinding.toolBarCustomLayout.toolbar);
        initToolBar(activitySelectTimeSlotBinding.toolBarCustomLayout.collapsingToolbar, getResources().getString(R.string.text_select_time_slot));
        activitySelectTimeSlotBinding.setViewModel(onCreateViewModel());
        initTimeSlotAdapterListener();
    }

    private void intentValue() {
        Intent intent = getIntent();
        timeSlotViewModel.setSkipTimeSlotApi(false);
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL))
                timeSlotViewModel.setDiagnosticOrderCreationDetail(intent.getBundleExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL));
            if (intent.hasExtra(DiagnosticConstant.KEY_SELECTED_TYPE))
                timeSlotViewModel.setDiagnosticSelectedType(intent.getStringExtra(DiagnosticConstant.KEY_SELECTED_TYPE));
            if(intent.hasExtra(DiagnosticConstant.INTENT_KEY_DIA_SELECTED_SLOTS)){
                timeSlotViewModel.getTimeSlotMutableLiveData().postValue((TimeSlotResponse) intent.getSerializableExtra(DiagnosticConstant.INTENT_KEY_DIA_SELECTED_SLOTS));
                timeSlotViewModel.setSkipTimeSlotApi(true);
            }
        }
    }

    private void initTimeSlotAdapterListener() {
        TimeSlotAdapter.setTimeSlotAdapterListener(new TimeSlotAdapter.TimeSlotAdapterListener() {
            @Override
            public void onSelectedSlot(SlotTime slotTime) {
                setSlotTime(slotTime);
                DiagnosticHelper.setSelectedSlotDay(slotTime.getSlotDay());
                if(viewExpandHeight <= 0)
                    viewExpandHeight = CommonUtils.getViewHeight(activitySelectTimeSlotBinding.testCount.lytTestCount, DiagnosticSelectTimeSlotActivity.this) - getResources().getDimensionPixelSize(R.dimen.density_size_5);
                CommonUtils.expand(activitySelectTimeSlotBinding.testCount.lytTestCount, DiagnosticConstant.COLLAPSE_EXPAND_VIEW_TIMEOUT, viewExpandHeight);
            }

            @Override
            public void onUnSelectedSlot() {
                setSlotTime(new SlotTime());
                DiagnosticHelper.setSelectedSlotDay("");
                CommonUtils.collapse(activitySelectTimeSlotBinding.testCount.lytTestCount, DiagnosticConstant.COLLAPSE_EXPAND_VIEW_TIMEOUT, DiagnosticConstant.COLLAPSING_VIEW_HEIGHT);
            }
        });
    }

    @Override
    protected DiagnosticTimeSlotViewModel onCreateViewModel() {
        timeSlotViewModel = ViewModelProviders.of(this).get(DiagnosticTimeSlotViewModel.class);
        timeSlotViewModel.getTimeSlotMutableLiveData().observe(this, new TimeSlotObserver());
        intentValue();
        timeSlotViewModel.init(this, activitySelectTimeSlotBinding, timeSlotViewModel, this);
        onRetry(timeSlotViewModel);
        return timeSlotViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void vmShowProgress() {
        activitySelectTimeSlotBinding.toolBarCustomLayout.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        activitySelectTimeSlotBinding.toolBarCustomLayout.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void vmInitViewPager(TimeSlotResult slotResult) {
        initViewPager(slotResult);
    }

    @Override
    public void vmInitPaymentNavigation() {
        Intent intent = new Intent();
//        /*Diagnostic WebEngage slot Selected Event*/
        setWebEngageSlotEvent();
        JustDocUserResponse diagnosticLoginResult = new Gson().fromJson(BasePreference.getInstance(this).getDiagnosticLoginResponse(), JustDocUserResponse.class);
        String userId = diagnosticLoginResult != null && !TextUtils.isEmpty(diagnosticLoginResult.getId()) ? diagnosticLoginResult.getId() : "";
        Bundle bundle = new Bundle();
        bundle.putAll(timeSlotViewModel.getDiagnosticOrderCreationDetail());

        if (timeSlotViewModel.getDiagnosticOrderCreationDetail() != null) {
            if (timeSlotViewModel.getDiagnosticSelectedType().equals(DiagnosticConstant.PACKAGE) && timeSlotViewModel.getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_PACKAGE)) {
                DiagnosticPackage diagnosticPackage = (DiagnosticPackage) timeSlotViewModel.getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE);
                AvailableLab availableLab = new AvailableLab();
                availableLab.setTestList(diagnosticPackage != null && diagnosticPackage.getTestList() != null ? diagnosticPackage.getTestList() : new ArrayList<Test>());
                availableLab.setLabDescription(diagnosticPackage != null && diagnosticPackage.getLabDescription() != null ? diagnosticPackage.getLabDescription() : new LabDescription());
                availableLab.setPriceDescription(diagnosticPackage != null && diagnosticPackage.getPriceDescription() != null ? diagnosticPackage.getPriceDescription() : new PriceDescription());
                bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_LAB, availableLab);
            } else {
                bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_LAB, timeSlotViewModel.getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB));
            }
        }
        bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_TIME_SLOT, getSlotTime());
        bundle.putString(DiagnosticConstant.KEY_DIAGNOSTIC, DiagnosticConstant.KEY_DIAGNOSTIC);
        bundle.putString(DiagnosticConstant.KEY_DIAGNOSTIC_USER_ID, userId);
        bundle.putDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT, timeSlotViewModel.getTotalAmount());
        bundle.putBoolean(ConsultationConstant.IS_FROM_PAYMENT, false);
        intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, bundle);
        LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_order_review), intent, this);
    }

    private void setWebEngageSlotEvent() {
        PatientDetail patientDetail = (PatientDetail) timeSlotViewModel.getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_PATIENT_DETAIL);
        String patientName = patientDetail != null && !TextUtils.isEmpty(patientDetail.getName()) ? patientDetail.getName() : "";
        String age = patientDetail != null && !TextUtils.isEmpty(patientDetail.getAge()) ? patientDetail.getAge() : "";
        String gender = patientDetail != null && !TextUtils.isEmpty(patientDetail.getGender()) ? patientDetail.getGender() : "";
        HashMap<String, Object> addressMap = null;
        MStarAddressModel collectionAddress = (MStarAddressModel) timeSlotViewModel.getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS);
        if (collectionAddress != null) {
            PaymentHelper.setCustomerAddress(collectionAddress);
            String jsonObject = new Gson().toJson(collectionAddress);
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            addressMap = new Gson().fromJson(jsonObject, type);
        }
        SlotTime slotTime = getSlotTime() != null ? getSlotTime() : new SlotTime();
        String selectedDay = slotTime != null && !TextUtils.isEmpty(slotTime.getSlotDay()) ? slotTime.getSlotDay() : "";
        String selectedTime = slotTime != null && !TextUtils.isEmpty(slotTime.getTimeRange()) ? slotTime.getTimeRange() : "";
        if (timeSlotViewModel.getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST)) {
            AvailableLab availableLab = (AvailableLab) timeSlotViewModel.getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB);
            LabDescription labDescription = availableLab != null && availableLab.getLabDescription() != null ? availableLab.getLabDescription() : new LabDescription();
            String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
            PriceDescription priceDescription = availableLab != null && availableLab.getPriceDescription() != null ? availableLab.getPriceDescription() : new PriceDescription();
            double price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? Double.parseDouble(priceDescription.getFinalPrice()) : 0;
            List<Test> testList = availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? availableLab.getTestList() : new ArrayList<Test>();
            WebEngageHelper.getInstance().slotSelectionEvent(BasePreference.getInstance(this), DiagnosticHelper.getCity(), age, gender, patientName, true, testList, null, labName, price, addressMap, selectedDay, selectedTime, this);
        } else {
            DiagnosticPackage diagnosticPackage = (DiagnosticPackage) timeSlotViewModel.getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE);
            String jsonObject = new Gson().toJson(diagnosticPackage);
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            HashMap<String, Object> packageMap = new Gson().fromJson(jsonObject, type);
            String labName = diagnosticPackage != null && diagnosticPackage.getLabDescription() != null && !TextUtils.isEmpty(diagnosticPackage.getLabDescription().getLabName()) ? diagnosticPackage.getLabDescription().getLabName() : "";
            double price = diagnosticPackage != null && diagnosticPackage.getPriceDescription() != null && !TextUtils.isEmpty(diagnosticPackage.getPriceDescription().getFinalPrice()) ? Double.parseDouble(diagnosticPackage.getPriceDescription().getFinalPrice()) : 0;
            WebEngageHelper.getInstance().slotSelectionEvent(BasePreference.getInstance(this), DiagnosticHelper.getCity(), age, gender, patientName, false, null, packageMap, labName, price, addressMap, selectedDay, selectedTime, this);
        }
    }

    @Override
    public void vmInitBottomView(List<Test> testList) {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), testList, timeSlotViewModel.getDiagnosticSelectedType(), DiagnosticConstant.TIME_SLOT, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, "Bottom_fragment").commitAllowingStateLoss();
    }

    @Override
    public void onNavigation() {
        vmInitPaymentNavigation();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {

    }

    @Override
    public void onUpdateSearchList(Test removeTest) {

    }

    @Override
    public void onUpdateLabList() {

    }

    public class TimeSlotObserver implements Observer<TimeSlotResponse> {

        @Override
        public void onChanged(@Nullable TimeSlotResponse timeSlotResponse) {
            timeSlotViewModel.timeSlotDataAvailable(timeSlotResponse);
            activitySelectTimeSlotBinding.setViewModel(timeSlotViewModel);
        }
    }

    private void initViewPager(TimeSlotResult slotResult) {
        if (slotResult != null && slotResult.getAvailableSlotList() != null && slotResult.getAvailableSlotList().size() > 0) {
            mTimeSlotViewPagerAdapter = new TimeSlotViewPagerAdapter(getSupportFragmentManager());
            for (AvailableSlot availableSlot : slotResult.getAvailableSlotList()) {
                WeekTabView weekTabView = new WeekTabView(availableSlot.getSlotDayDate(), DiagnosticTimeSlotFragment.newInstance(availableSlot));
                mTimeSlotViewPagerAdapter.addFragment(weekTabView);
            }
            activitySelectTimeSlotBinding.viewPager.setAdapter(mTimeSlotViewPagerAdapter);
            activitySelectTimeSlotBinding.weekTab.setupWithViewPager(activitySelectTimeSlotBinding.viewPager);
            activitySelectTimeSlotBinding.weekTab.setTabTextColors(getResources().getColor(R.color.colorLightBlueGrey), getResources().getColor(R.color.colorDarkBlueGrey));
        }

        activitySelectTimeSlotBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_TIME_SLOT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // finish activity if user coming from order review page for change lab
        if(BasePreference.getInstance(this).isPreviousActivitiesCleared()){
            finish();
        }
    }
}
