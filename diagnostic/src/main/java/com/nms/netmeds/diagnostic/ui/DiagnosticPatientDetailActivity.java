package com.nms.netmeds.diagnostic.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.ActivityPatientDetailsBinding;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.TimeSlotResponse;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.diagnostic.viewModel.DiagnosticPatientDetailsViewModel;
import com.webengage.sdk.android.WebEngage;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DiagnosticPatientDetailActivity extends BaseViewModelActivity<DiagnosticPatientDetailsViewModel> implements DiagnosticPatientDetailsViewModel.DiagnosticPatientDetailsListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener {
    private ActivityPatientDetailsBinding patientDetailsBinding;
    private DiagnosticPatientDetailsViewModel patientDetailsViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        patientDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_patient_details);
        toolBarSetUp(patientDetailsBinding.toolbar);
        initToolBar();
        patientDetailsBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected DiagnosticPatientDetailsViewModel onCreateViewModel() {
        patientDetailsViewModel = ViewModelProviders.of(this).get(DiagnosticPatientDetailsViewModel.class);
        patientDetailsViewModel.getTimeSlotMutableLiveData().observe(this, new TimeSlotObserver());
        intentValue();
        patientDetailsViewModel.init(this, patientDetailsBinding, this);
        return patientDetailsViewModel;
    }

    private void initToolBar() {
        patientDetailsBinding.collapsingToolbar.setTitle(getString(R.string.text_patient_details));
        patientDetailsBinding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        patientDetailsBinding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        patientDetailsBinding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        patientDetailsBinding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public void vmInitBottomView(List<Test> list) {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), list, patientDetailsViewModel.getDiagnosticType(), DiagnosticConstant.PATIENT_DETAIL, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, "Bottom_fragment").commitAllowingStateLoss();
    }

    @Override
    public void vmInitAddressNavigation() {
        /*Diagnostic WebEngage Patient Details Event*/
        setWebEngagePatientDetailsEvent(patientDetailsViewModel.getPatientDetail());
        Bundle bundle = new Bundle();
        bundle.putSerializable(DiagnosticConstant.KEY_PATIENT_DETAIL, patientDetailsViewModel.getPatientDetail());
        bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_LAB, patientDetailsViewModel.getAvailableLab());
        bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE, patientDetailsViewModel.getSelectedPackage());
        if(areSlotsAvailable()) {
            bundle.putSerializable(DiagnosticConstant.INTENT_KEY_DIA_SELECTED_SLOTS, patientDetailsViewModel.getTimeSlotResponse());
        }
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, bundle);
        intent.putExtra(DiagnosticConstant.KEY_DIAGNOSTIC, DiagnosticConstant.KEY_DIAGNOSTIC);
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TYPE, patientDetailsViewModel.getDiagnosticType());
        intent.putExtra(DiagnosticConstant.KEY_FROM_PATIENT_DETAILS, true);
        if (DiagnosticHelper.getSelectedList() != null && DiagnosticHelper.getSelectedList().size() > 0
                && !TextUtils.isEmpty(DiagnosticHelper.getSelectedList().get(0).getCategory()) && DiagnosticHelper.getSelectedList().get(0).getCategory().equalsIgnoreCase(DiagnosticConstant.CATEGORY_RADIO)) {
            intent.putExtra(DiagnosticConstant.INTENT_IS_RADIO, true);
        } else {
            intent.putExtra(DiagnosticConstant.INTENT_IS_RADIO, false);
        }
        LaunchIntentManager.routeToActivity(DiagnosticPatientDetailActivity.this.getResources().getString(R.string.route_address_activity), intent, DiagnosticPatientDetailActivity.this);
    }

    private void setWebEngagePatientDetailsEvent(PatientDetail patientDetail) {
        String patientName = patientDetail != null && !TextUtils.isEmpty(patientDetail.getName()) ? patientDetail.getName() : "";
        String age = patientDetail != null && !TextUtils.isEmpty(patientDetail.getAge()) ? patientDetail.getAge() : "";
        String gender = patientDetail != null && !TextUtils.isEmpty(patientDetail.getGender()) ? patientDetail.getGender() : "";
        if (patientDetailsViewModel.getDiagnosticType().equalsIgnoreCase(DiagnosticConstant.TEST)) {
            AvailableLab availableLab = patientDetailsViewModel.getAvailableLab();
            LabDescription labDescription = availableLab.getLabDescription() != null ? availableLab.getLabDescription() : new LabDescription();
            String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
            PriceDescription priceDescription = availableLab.getPriceDescription() != null ? availableLab.getPriceDescription() : new PriceDescription();
            double price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? Double.parseDouble(priceDescription.getFinalPrice()) : 0;
            List<Test> testList = availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? availableLab.getTestList() : new ArrayList<Test>();
            WebEngageHelper.getInstance().patientDetailsEvent(BasePreference.getInstance(this),
                    !TextUtils.isEmpty(BasePreference.getInstance(this).getDiagnosticCity()) ? BasePreference.getInstance(this).getDiagnosticCity() : DiagnosticHelper.getCity(), age, gender, patientName, true, testList, null, labName, price, this);
        } else {
            DiagnosticPackage diagnosticPackage = patientDetailsViewModel.getSelectedPackage();
            String jsonObject = new Gson().toJson(diagnosticPackage);
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            HashMap<String, Object> packageMap = new Gson().fromJson(jsonObject, type);
            String labName = diagnosticPackage != null && diagnosticPackage.getLabDescription() != null && !TextUtils.isEmpty(diagnosticPackage.getLabDescription().getLabName()) ? diagnosticPackage.getLabDescription().getLabName() : "";
            double price = diagnosticPackage != null && diagnosticPackage.getPriceDescription() != null && !TextUtils.isEmpty(diagnosticPackage.getPriceDescription().getFinalPrice()) ? Double.parseDouble(diagnosticPackage.getPriceDescription().getFinalPrice()) : 0;
            WebEngageHelper.getInstance().patientDetailsEvent(BasePreference.getInstance(this), !TextUtils.isEmpty(BasePreference.getInstance(this).getDiagnosticCity()) ? BasePreference.getInstance(this).getDiagnosticCity() : DiagnosticHelper.getCity(), age, gender, patientName, false, null, packageMap, labName, price, this);
        }
    }

    @Override
    public void onNavigation() {
        patientDetailsViewModel.onBottomNavigation();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {

    }

    @Override
    public void onUpdateSearchList(Test removeTest) {

    }

    @Override
    public void onUpdateLabList() {

    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            /*if (intent.hasExtra(DiagnosticConstant.KEY_INTENT_FROM))
                patientDetailsViewModel.setIntentFrom(intent.getStringExtra(DiagnosticConstant.KEY_INTENT_FROM));*/
            if (intent.hasExtra(DiagnosticConstant.KEY_SELECTED_LAB)) {
                patientDetailsViewModel.setAvailableLab((AvailableLab) intent.getSerializableExtra(DiagnosticConstant.KEY_SELECTED_LAB));
                patientDetailsViewModel.setDiagnosticType(DiagnosticConstant.TEST);
            }
            if (intent.hasExtra(DiagnosticConstant.KEY_SELECTED_PACKAGE)) {
                patientDetailsViewModel.setSelectedPackage((DiagnosticPackage) intent.getSerializableExtra(DiagnosticConstant.KEY_SELECTED_PACKAGE));
                patientDetailsViewModel.setDiagnosticType(DiagnosticConstant.PACKAGE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_PATIENT_DETAILS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // finish activity if user coming from order review page for change lab
        if(BasePreference.getInstance(this).isPreviousActivitiesCleared()){
            finish();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    private boolean areSlotsAvailable(){
        return patientDetailsViewModel.getTimeSlotResponse()!=null && patientDetailsViewModel.getTimeSlotResponse().getResult()!=null && patientDetailsViewModel.getTimeSlotResponse().getResult().getAvailableSlotList()
                !=null && patientDetailsViewModel.getTimeSlotResponse().getResult().getAvailableSlotList().size() > 0;
    }

    // Observer for letting time slot available for selected lab
    public class TimeSlotObserver implements Observer<TimeSlotResponse> {
        @Override
        public void onChanged(@Nullable TimeSlotResponse timeSlotResponse) {
            patientDetailsViewModel.setTimeSlotResponse(timeSlotResponse);
        }
    }
}
