package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.model.PriceInfo;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterBottomItemViewBinding;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.List;
import java.util.Locale;

public class DiagnosticBottomViewAdapter extends RecyclerView.Adapter<DiagnosticBottomViewAdapter.BottomViewHolder> {
    private Context context;
    private List<Test> testList;
    private CountUpdateListener countUpdateListener;
    private String type;
    private String page;

    public DiagnosticBottomViewAdapter(Context context, List<Test> testList, String page, CountUpdateListener countUpdateListener) {
        this.context = context;
        this.testList = testList;
        this.page = page;
        this.countUpdateListener = countUpdateListener;
    }

    @NonNull
    @Override
    public BottomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterBottomItemViewBinding bottomItemViewBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_bottom_item_view, viewGroup, false);
        return new BottomViewHolder(bottomItemViewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BottomViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Test test = testList.get(position);
        PriceInfo priceInfo = test.getPriceInfo() != null ? test.getPriceInfo() : new PriceInfo();
        holder.bottomItemViewBinding.selectedTestName.setText(test.getTestName());
        holder.bottomItemViewBinding.selectedTestName.setVisibility(TextUtils.isEmpty(test.getTestName()) ? View.GONE : View.VISIBLE);
        holder.bottomItemViewBinding.textPrice.setText(String.format(Locale.getDefault(), "%s%s", DiagnosticConstant.INR, priceInfo.getFinalPrice()));
        holder.bottomItemViewBinding.textPrice.setVisibility(page.equals(DiagnosticConstant.HOME_PAGE) || page.equals(DiagnosticConstant.SEARCH_TEST_PAGE) || TextUtils.isEmpty(priceInfo.getFinalPrice()) ? View.GONE : View.VISIBLE);
        if (test.getType() != null && (test.getType().equalsIgnoreCase(DiagnosticConstant.PACKAGE) ||
                test.getType().equalsIgnoreCase(DiagnosticConstant.PACKAGES)
                || test.getType().equalsIgnoreCase(DiagnosticConstant.TEST)
                || test.getType().equalsIgnoreCase(DiagnosticConstant.TEST_TYPE_PROFILE)) && (page.equals(DiagnosticConstant.HOME_PAGE) || page.equals(DiagnosticConstant.SEARCH_TEST_PAGE) || page.equals(DiagnosticConstant.LAB_PAGE))) {
            holder.bottomItemViewBinding.imgClearTest.setVisibility(View.VISIBLE);
            holder.bottomItemViewBinding.imgClearTest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testList.remove(position);
                    DiagnosticHelper.removeSelectedTest(test);
                    notifyDataSetChanged();
                    if (page.equals(DiagnosticConstant.LAB_PAGE))
                        countUpdateListener.onUpdateLab();
                    else
                        countUpdateListener.onUpdateCount(testList.size(), test);
                }
            });
        } else {
            holder.bottomItemViewBinding.imgClearTest.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    class BottomViewHolder extends RecyclerView.ViewHolder {
        private AdapterBottomItemViewBinding bottomItemViewBinding;

        BottomViewHolder(@NonNull AdapterBottomItemViewBinding bottomItemViewBinding) {
            super(bottomItemViewBinding.getRoot());
            this.bottomItemViewBinding = bottomItemViewBinding;
        }
    }

    public interface CountUpdateListener {
        void onUpdateCount(int size, Test removeTest);

        void onUpdateLab();
    }
}
