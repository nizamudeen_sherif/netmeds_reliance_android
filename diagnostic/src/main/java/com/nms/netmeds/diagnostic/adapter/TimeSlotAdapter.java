package com.nms.netmeds.diagnostic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.diagnostic.R;
import com.nms.netmeds.diagnostic.databinding.AdapterTimeSlotItemBinding;
import com.nms.netmeds.diagnostic.model.SlotTime;

import java.util.List;


public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.TimeSlotHolder> {
    private Context mContext;
    private List<SlotTime> timeSlotList;
    private static TimeSlotAdapterListener timeSlotAdapterListener;

    public TimeSlotAdapter(Context context, List<SlotTime> timeSlotList) {
        this.mContext = context;
        this.timeSlotList = timeSlotList;
    }

    @NonNull
    @Override
    public TimeSlotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AdapterTimeSlotItemBinding timeSlotItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_time_slot_item, viewGroup, false);
        return new TimeSlotHolder(timeSlotItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeSlotHolder holder, @SuppressLint("RecyclerView") final int position) {
        final SlotTime slotTime = timeSlotList.get(position);
        holder.adapterTimeSlotItemBinding.textTimeSlot.setText(slotTime.getTimeRange());
        if (slotTime.isChecked()) {
            holder.adapterTimeSlotItemBinding.imgSlotCheck.setImageResource(R.drawable.ic_radio_checked);
            holder.adapterTimeSlotItemBinding.textTimeSlot.setTypeface(CommonUtils.getTypeface(mContext, "font/Lato-Bold.ttf"));
        } else {
            holder.adapterTimeSlotItemBinding.imgSlotCheck.setImageResource(R.drawable.ic_radio_unchecked);
            holder.adapterTimeSlotItemBinding.textTimeSlot.setTypeface(CommonUtils.getTypeface(mContext, "font/Lato-Regular.ttf"));
        }
        holder.adapterTimeSlotItemBinding.imgSlotCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeSlotCheck(slotTime);
            }
        });
        holder.adapterTimeSlotItemBinding.lytSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeSlotCheck(slotTime);
            }
        });
    }

    private void timeSlotCheck(SlotTime slotTime) {
        if (slotTime.isChecked()) {
            slotTime.setChecked(false);
            timeSlotAdapterListener.onUnSelectedSlot();
        } else {
            slotUnCheck();
            slotTime.setChecked(true);
            timeSlotAdapterListener.onSelectedSlot(slotTime);
        }
        notifyDataSetChanged();
    }

    private void slotUnCheck() {
        for (SlotTime slotTime : timeSlotList)
            slotTime.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return timeSlotList.size();
    }

    class TimeSlotHolder extends RecyclerView.ViewHolder {
        private AdapterTimeSlotItemBinding adapterTimeSlotItemBinding;

        TimeSlotHolder(@NonNull AdapterTimeSlotItemBinding adapterTimeSlotItemBinding) {
            super(adapterTimeSlotItemBinding.getRoot());
            this.adapterTimeSlotItemBinding = adapterTimeSlotItemBinding;
        }
    }

    public static void setTimeSlotAdapterListener(TimeSlotAdapterListener slotAdapterListener) {
        timeSlotAdapterListener = slotAdapterListener;
    }

    public interface TimeSlotAdapterListener {
        void onSelectedSlot(SlotTime slotTime);

        void onUnSelectedSlot();
    }
}