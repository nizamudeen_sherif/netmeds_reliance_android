package com.nmp.android.netmeds.navigation;

/**
 * The type Route node not found.
 */
public class RouteNodeNotFound extends Exception {

    private final String TAG = RouteNodeNotFound.class.getSimpleName();

    /**
     * Constructs a new {@code Exception} that includes the current stack trace.
     */
    public RouteNodeNotFound() {
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage the detail message for this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public RouteNodeNotFound(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage the detail message for this com.wba.horizon.android.framework.core.component.persistence.exception.
     * @param throwable     the cause of this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public RouteNodeNotFound(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified cause.
     *
     * @param throwable the cause of this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public RouteNodeNotFound(Throwable throwable) {
        super(throwable);
    }
}