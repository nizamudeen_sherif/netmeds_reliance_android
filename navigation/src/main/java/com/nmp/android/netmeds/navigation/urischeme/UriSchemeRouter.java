package com.nmp.android.netmeds.navigation.urischeme;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.nmp.android.netmeds.navigation.RouteController;
import com.nmp.android.netmeds.navigation.config.NavConfigInitException;
import com.nmp.android.netmeds.navigation.config.NavConfigManager;

import org.json.JSONException;

/**
 * This Class is to open deep links via the payload of a notification, a link from a Rich App Page or the JavaScript Interface
 * The default deep link action assumes that the application is set up to handle deep links through Android implicit intents.
 * Implicit intents are intents used to launch components by specifying an action, category, and data instead of providing the component’s class.
 * This allows any application to invoke another application’s component to perform an action.
 */
public class UriSchemeRouter {

    /**
     * Constructor for UriSchemeRouter
     */
    private UriSchemeRouter() {

    }

    /**
     * This method is to route the uri link based on the deeplink option.
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param link           This string is the uri link
     * @param context        This is the context of the application
     * @param isFromDeeplink Indicates navigation is from Deep Link
     *                       </br><b>true</b> navigate the uri with deep link option
     *                       </br><b>false</b> navigate the uri without deep link option
     * @throws JSONException Exception that includes the current stack trace
     */
    public static void route(String link, Context context, boolean isFromDeeplink) throws JSONException {
        route(Uri.parse(link), context, isFromDeeplink);
    }

    /**
     * This method is to route the uri link based on the deeplink option.
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param link           This is the parsed uri link
     * @param context        This is the context of the application
     * @param isFromDeeplink Indicates navigation is from Deep Link
     *                       </br><b>true</b> navigate the uri with deep link option
     *                       </br><b>false</b> navigate the uri without deep link option
     * @throws JSONException Exception that includes the current stack trace
     */
    public static void route(Uri link, Context context, boolean isFromDeeplink) throws JSONException {
        route(link, context, true, isFromDeeplink);
    }

    /**
     * This method is to Launch activity based on the uri link.
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param link           This is the parsed uri link
     * @param context        This is the context of the application
     * @param onNewTask      Indicates activity launch as a NewTask
     *                       </br><b>true</b> Launch activity as a new Task
     *                       </br><b>false</b> Launch activity in a existing task
     * @param isFromDeeplink Indicates navigation is from Deep Link
     *                       </br><b>true</b> navigate the uri with deep link option
     *                       </br><b>false</b> navigate the uri without deep link option
     */
    public static void route(Uri link, Context context, boolean onNewTask, boolean isFromDeeplink) {
        // base case
        if (TextUtils.isEmpty(link.getHost()) && (TextUtils.isEmpty(link.getPath()))) {
            return;
        }

        try {
            UriSchemeMapper.UriScheme uriScheme = UriSchemeMapper.matchScheme(link);

            if (uriScheme.getNodeList().size() > 0) {
                RouteController.route(uriScheme.getNodeList(), uriScheme.getParamList(), context, onNewTask, isFromDeeplink);
            } else {
                RouteController.route(NavConfigManager.getDefaultRoute(), context, isFromDeeplink);
            }
        } catch (NavConfigInitException | InvalidUriScheme e) {
        }
    }
}
