package com.nmp.android.netmeds.navigation;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import com.nmp.android.netmeds.navigation.config.BaseRouteNode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * The Intent Factory class is used to launch the activity depending on route node, params
 * and intent flag states. The intent object for page navigation and to get metadata value from manifest file
 * is built in this class. Depending on the key we get the route node value and then we
 * navigate the page
 */
public final class IntentFactory {

    /**
     * Constructor Class for IntentFactory
     */
    private IntentFactory() {

    }


    /**
     * The buildIntent method is used to open the details activity. The BaseRouteNode object, routeParamList as Map, environment data context
     * and task, as a boolean value, are passed.
     * It return the intent object
     *
     * @param route          information about the route node value
     * @param routeParamList pass the route parameters
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     * @return a Intent that will start the given activity.
     */
    public static Intent buildIntent(BaseRouteNode route
            , Map<String, Object> routeParamList
            , Context context, boolean onNewTask, boolean isFromDeeplink) {
        return buildIntent(route, routeParamList, context, true, onNewTask, isFromDeeplink);
    }

    /**
     * The buildNavigationIntent method is used to open the details activity. The BaseRouteNode,
     * intent object, environment data context and task, as a boolean value, are passed.
     * It will return the intent object
     *
     * @param route          information about the route node value
     * @param routeIntent    Intent that will start the given activity
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     * @return a Intent that will start the given activity.
     */
    public static Intent buildNavigationIntent(BaseRouteNode route
            , Intent routeIntent
            , Context context, boolean onNewTask, boolean isFromDeeplink) {
        return buildIntent(route, routeIntent, context, true, onNewTask, isFromDeeplink);
    }

    /**
     * The buildIntent method is used to open the details activity
     * This method returns the intent object along with intent flags.
     * The boolean value for all clear top activities are passed if you set true
     * it clear the all top activities
     *
     * @param route              information about the route node value
     * @param routeParamList     pass the route parameters
     * @param context            Context represents environment data
     * @param clearTopActivities This one clear the top activities depending on boolean value
     * @param onNewTask          information about the activity task
     * @param isFromDeeplink     this boolean value provide the app link with in app or out side app
     * @return a Intent that will start the given activity.
     */
    public static Intent buildIntent(BaseRouteNode route
            , Map<String, Object> routeParamList
            , Context context
            , Boolean clearTopActivities, boolean onNewTask, boolean isFromDeeplink) {
        Intent intent = null;
        String activityName = route.getType();

        try {
            intent = new Intent();
            ComponentName componentName = new ComponentName(context.getPackageName(),
                    activityName);
            intent.setComponent(componentName);

            if (clearTopActivities) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (onNewTask) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            }
            if (isFromDeeplink) {
                intent.putExtra(NavConstants.IS_FROM_DEEP_LINK, isFromDeeplink);
            }
            updateIntentObject(routeParamList, intent);
        } catch (Exception e) {
        }

        return intent;
    }

    /**
     * The buildIntent method is used to open the details activity
     * This method returns the intent object along with intent flags.
     *
     * @param routeParamList pass the route parameters
     * @param intent         Intent object
     * @return intent object with params
     */
    private static void updateIntentObject(Map<String, Object> routeParamList, Intent intent) {
        if (routeParamList != null) {
            for (Map.Entry<String, Object> param : routeParamList.entrySet()) {
                if (param.getValue() instanceof ArrayList) {
                    intent.putExtra(param.getKey(), (ArrayList) param.getValue());
                } else if (param.getValue() instanceof Boolean) {
                    intent.putExtra(param.getKey(), (Boolean) param.getValue());
                } else if (param.getValue() instanceof Parcelable) {
                    intent.putExtra(param.getKey(), (Parcelable) param.getValue());
                } else if (param.getValue() instanceof Float) {
                    intent.putExtra(param.getKey(), (Float) param.getValue());
                } else if (param.getValue() instanceof Integer) {
                    intent.putExtra(param.getKey(), (Integer) param.getValue());
                } else if (param.getValue() instanceof Serializable) {
                    intent.putExtra(param.getKey(), (Serializable) param.getValue());
                } else {
                    intent.putExtra(param.getKey(), param.getValue().toString());
                }
            }
        }
    }

    /**
     * This buildIntent method is used to open the details activity.
     * The intent object is passed here. This method returns the intent object along with intent flags
     * The boolean value for all clear top activities are passed here. if you set true
     * it clear the all top activities
     *
     * @param route              information about the route node value
     * @param pRouteIntent       Intent that will start the given activity
     * @param context            Context represents environment data
     * @param clearTopActivities This one clear the top activities depending on boolean value
     * @param onNewTask          information about the activity task
     * @param isFromDeeplink     this boolean value provide the app link with in app or out side app
     * @return a Intent that will start the given activity.
     */
    public static Intent buildIntent(BaseRouteNode route
            , Intent pRouteIntent
            , Context context
            , Boolean clearTopActivities, boolean onNewTask, boolean isFromDeeplink) {

        String activityName = route.getType();
        Intent routeIntent = pRouteIntent;
        try {
            if (routeIntent == null)
                routeIntent = new Intent();
            ComponentName componentName = new ComponentName(context.getPackageName(),
                    activityName);
            routeIntent.setComponent(componentName);

            if (clearTopActivities) {
                routeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (onNewTask) {
                    routeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
            } else {
                routeIntent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            }
            if (isFromDeeplink) {
                routeIntent.putExtra(NavConstants.IS_FROM_DEEP_LINK, isFromDeeplink);
            }

        } catch (Exception e) {
        }

        return routeIntent;
    }

    /**
     * The buildAndFireIntent method is used to start the activity by passing intent object
     * to startActivity method. The BaseRouteNode, routeParameters and new task, as boolean value, are passed here.
     *
     * @param routeNode       information about the route node value
     * @param routeParameters pass the route parameters
     * @param context         Context represents environment data
     * @param onNewTask       information about the activity task
     * @param isFromDeeplink  this boolean value provide the app link with in app or out side app
     */
    public static void buildAndFireIntent(BaseRouteNode routeNode
            , Map<String, Object> routeParameters
            , Context context, boolean onNewTask, boolean isFromDeeplink) {
        Intent intent = buildIntent(routeNode, routeParameters, context, onNewTask, isFromDeeplink);
        context.startActivity(intent);
    }
}