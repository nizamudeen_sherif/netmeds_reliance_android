package com.nmp.android.netmeds.navigation;

import android.content.Context;
import android.content.Intent;

import com.nmp.android.netmeds.navigation.config.NavConfigInitException;


/**
 * The LaunchIntentManager class is used to launch the activity
 * depending on route node value.
 * RouteController class is accessed and the routeToNode method is called.
 * Route node value and context are passed
 */
public class LaunchIntentManager {

    /**
     * private constructor
     */
    private LaunchIntentManager() {

    }

    /**
     * The routeToActivity method navigates to RouteController class
     * The activity to be opened in RouteController class is determined by the route node.
     *
     * @param routeNode Provide the route node value
     * @param mContext  Context represents environment data
     */
    public static void routeToActivity(String routeNode, Context mContext) {
        try {
            RouteController rntCntl = RouteController.getRouteController();
            rntCntl.routeToNode(routeNode, mContext.getApplicationContext(), false);
        } catch (NavConfigInitException e) {
        }
    }

    /**
     * Route to activity.
     *
     * @param routeNode      the route node
     * @param routeParamList the route param list
     * @param mContext       the m context
     */
    public static void routeToActivity(String routeNode, Intent routeParamList, Context mContext) {
        try {
            RouteController rntCntl = RouteController.getRouteController();
            rntCntl.routeToNode(routeNode, routeParamList, mContext.getApplicationContext(), false);
        } catch (NavConfigInitException e) {
        }
    }

    /**
     * Route to activity.
     *
     * @param routeNode      the route node
     * @param routeParamList the route param list
     * @param mContext       the m context
     */
    public static void routeToActivity(String routeNode, Intent routeParamList, Context mContext, boolean deepLink) {
        try {
            RouteController rntCntl = RouteController.getRouteController();
            rntCntl.routeToNode(routeNode, routeParamList, mContext.getApplicationContext(), deepLink);
        } catch (NavConfigInitException e) {
        }
    }
}