package com.nmp.android.netmeds.navigation.config;

import com.google.gson.annotations.SerializedName;

/**
 * The BaseRoute class traverses through these nodes looking for one that match
 * parts a url. If it can match all nodes then it looks at the final node for
 * a matching verb. If that verb matches then the RouteNode will return a
 * RouteLink that points to a controller
 */
public class BaseRouteNode {

    @SerializedName("id")
    private String routeId = null;

    @SerializedName("class")
    private String routeType = null;

    /**
     * Create a helper object to assign id and type
     *
     * @param id   ID of the node
     * @param type Type of node
     */
    public BaseRouteNode(String id, String type) {
        this.routeId = id;
        this.routeType = type;
    }

    /**
     * Get BaseRouteNode id
     *
     * @return it will return baseroutenote id
     */
    public String getId() {
        return this.routeId;
    }

    /**
     * Set BaseRouteNode id
     *
     * @param id the id of node
     */
    protected void setId(String id) {
        this.routeId = id;
    }

    /**
     * Get BaseRouteNode type
     *
     * @return it will return baseroutenote type
     */
    public String getType() {
        return this.routeType;
    }

    /**
     * Get BaseRouteNode type
     *
     * @param type the type
     * @return it will return baseroutenote type
     */
    protected void setType(String type) {
        this.routeType = type;
    }
}
