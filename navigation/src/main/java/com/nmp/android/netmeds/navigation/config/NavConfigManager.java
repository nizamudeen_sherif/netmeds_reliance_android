package com.nmp.android.netmeds.navigation.config;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.nmp.android.netmeds.navigation.NavConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

/**
 * NavConfigManager Class
 */
public class NavConfigManager {

    private static NavConfig navConfig = null;
    private static String navInitError = "RouterController not initialized.";
    private final String TAG = NavConfigManager.class.getSimpleName();

    private NavConfigManager() {
    }

    /**
     * @return
     * @throws NavConfigInitException
     */
    public static BaseRouteNode getDefaultRoute() throws NavConfigInitException {
        if (navConfig == null) {
            throw new NavConfigInitException(navInitError);
        }

        return navConfig.getDefaultRoute();
    }

    /**
     * @return
     * @throws NavConfigInitException
     */
    public static Map<String, String> getRouteNodeHandlers() throws NavConfigInitException {
        if (navConfig == null) {

            throw new NavConfigInitException(navInitError);
        }

        return navConfig.getRouteNodeHandlers();
    }

    /**
     * @return
     * @throws NavConfigInitException
     */
    public static Map<String, RouteNode> getRouteNodes() throws NavConfigInitException {
        if (navConfig == null) {
            throw new NavConfigInitException(navInitError);
        }

        return navConfig.getRouteNodes();
    }

    /**
     * @return
     * @throws NavConfigInitException
     */
    public static List<String> getRoutes() throws NavConfigInitException {
        if (navConfig == null) {
            throw new NavConfigInitException(navInitError);
        }

        return navConfig.getRoutes();
    }

    /**
     * @param nodeName
     * @return
     * @throws NavConfigInitException
     */
    public static RouteNode getRouteNode(String nodeName) throws NavConfigInitException {
        return getRouteNodes().get(nodeName);
    }

    /**
     * @param context
     * @throws NavConfigInitException
     */
    public static void init(Context context) throws NavConfigInitException {
        JsonReader jReader;

        try {
            if (navConfig == null) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(RouteParamValue.class, new RouteParameterDeserializer());
                Gson gSon = gsonBuilder.create();

                jReader = getSchemeConfigReader(context);

                navConfig = gSon.fromJson(jReader, NavConfig.class);
                navConfig.getDefaultRoute();

                //Close the file reader.
                jReader.close();

                //Update RouteNode with Activity info from manifest.
                loadRouteNodeInfo(context);
            }

        } catch (Exception e) {
            throw new NavConfigInitException(e);
        }
    }

    /**
     * @param context
     * @return
     * @throws IOException
     */
    private static JsonReader getSchemeConfigReader(Context context) throws IOException {
        Resources appResources = context.getResources();
        AssetManager wagAssetManager = appResources.getAssets();

        InputStream inputStream = wagAssetManager.open("RouteConfig.json");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

        return new JsonReader(reader);
    }

    /**
     * @param context
     * @throws PackageManager.NameNotFoundException
     */
    private static void loadRouteNodeInfo(Context context) throws PackageManager.NameNotFoundException {
        PackageManager pm = context.getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(context.getPackageName());
        String mainActivityClassName = intent.getComponent().getClassName();

        PackageInfo info = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES | PackageManager.GET_META_DATA);

        ActivityInfo[] list = info.activities;

        String nodeName;
        RouteNode rtN;

        for (int i = 0; i < list.length; i++) {
            if (list[i].metaData != null && list[i].metaData.containsKey(NavConstants.NAV_MANIFEST_ROUTE_NODE_KEY)) {
                nodeName = list[i].metaData.getString(NavConstants.NAV_MANIFEST_ROUTE_NODE_KEY);
                rtN = navConfig.getRouteNodes().get(nodeName);

                if (rtN != null) {
                    rtN.setId(nodeName);
                    rtN.setType(list[i].name);
                } else {
                    rtN = new RouteNode(nodeName, list[i].name, null, null);
                    navConfig.getRouteNodes().put(nodeName, rtN);
                }

                //If default node name is not set in config, set launch activity as default.
                if ((navConfig.getDefaultRouteName() != null
                        || navConfig.getDefaultRouteName().isEmpty())
                        && list[i].name.equals(mainActivityClassName)) {
                    navConfig.setDefaultRouteName(nodeName);
                }
            }
        }
    }
}