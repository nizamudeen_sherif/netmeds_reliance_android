package com.nmp.android.netmeds.navigation.config;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * The type Route parameter deserializer.
 */
public class RouteParameterDeserializer implements JsonDeserializer<RouteParamValue> {

    @Override
    public RouteParamValue deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return this.getObject(json);
    }

    /**
     * Gets object.
     *
     * @param json the json
     * @return object
     */
    public RouteParamValue getObject(JsonElement json) {
        RouteParamValue rtnObj = null;

        if (json.isJsonArray()) {
            JsonArray jArr = json.getAsJsonArray();
            Iterator<JsonElement> itr = jArr.iterator();

            while (itr.hasNext()) {
                rtnObj = this.getObject(itr.next());
            }

        } else if (json.isJsonPrimitive()) {
            rtnObj = new RouteParamValue(null, json.getAsString());
        } else {
            JsonObject obj = json.getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> tempSet = obj.entrySet();

            if (tempSet.size() > 1) {
                Iterator<Map.Entry<String, JsonElement>> itr = tempSet.iterator();
                HashMap<String, RouteParamValue> tempMap = new HashMap<>();

                while (itr.hasNext()) {
                    Map.Entry<String, JsonElement> etr = itr.next();
                    updateRouteParamMap(etr, tempMap);
                }

                rtnObj = new RouteParamValue(null, tempMap);
            } else {
                Iterator<Map.Entry<String, JsonElement>> itr = tempSet.iterator();

                if (itr.hasNext()) {
                    Map.Entry<String, JsonElement> etr = itr.next();
                    rtnObj = getRouteParamValue(etr);
                }
            }
        }

        return rtnObj;
    }

    /**
     * This method to create temp map from etr
     *
     * @param etr     Map
     * @param tempMap
     * @return HashMap from etr and tempMap
     */
    private void updateRouteParamMap(Map.Entry<String, JsonElement> etr, HashMap<String, RouteParamValue> tempMap) {
        if (etr.getValue().isJsonPrimitive()) {
            tempMap.put(etr.getKey(), new RouteParamValue(etr.getKey(), etr.getValue().getAsString()));
        } else {
            tempMap.put(etr.getKey(), this.getObject(etr.getValue()));
        }
    }

    /**
     * This method to create temp map from etr and RouteParamValue
     *
     * @param etr Map
     * @return RouteParamValue object
     */
    private RouteParamValue getRouteParamValue(Map.Entry<String, JsonElement> etr) {
        RouteParamValue rtnObj;
        if (etr.getValue().isJsonPrimitive()) {
            rtnObj = new RouteParamValue(etr.getKey(), etr.getValue().getAsString());
        } else {
            rtnObj = new RouteParamValue(etr.getKey(), this.getObject(etr.getValue()));
        }
        return rtnObj;
    }
}
