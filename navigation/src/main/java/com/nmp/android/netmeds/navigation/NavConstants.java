package com.nmp.android.netmeds.navigation;

/**
 * Declare the constants for navigation ui
 */
public class NavConstants {

    /**
     * This constant used for check the key is equal to RouteParamValue
     */
    public static final String REGEX_JSON_NAME = "regex";
    /**
     * This constant used for compare the meta data value and get the route node value.
     */
    public static final String NAV_MANIFEST_ROUTE_NODE_KEY = "routeNodeName";
    /**
     * This constant used for store the deep link status. it's a boolean value.
     */
    public static final String IS_FROM_DEEP_LINK = "isFromDeepLink";
    /**
     * This constant used for store the route params in TreeMap
     */
    public static final String EXTRA_PATH_PARAMS = "extraPathParams";
    /**
     * This constant used for start the activity like NewTask or normal
     */
    public static final String ON_NEW_TASK = "ON_NEW_TASK";
    /**
     * This constant used for replace the empty with slash in path building
     */
    public static final String CHARACTER_SLASH = "/";
    /**
     * This constant used for add the empty before the ampersand in query value
     */
    public static final String CHARACTER_EMPTY = "";
    /**
     * This constant used for spilt the query value
     */
    public static final String CHARACTER_AMPERSAND = "&";
    /**
     * This constant used for spilt the query value
     */
    public static final String CHARACTER_EQUALS = "=";

    /**
     * Constructor for NavConstants
     */
    private NavConstants() {

    }
}