package com.nmp.android.netmeds.navigation.config;

/**
 * The type Nav config init exception.
 */
public class NavConfigInitException extends Exception {

    private final String TAG = NavConfigInitException.class.getName();

    /**
     * Constructs a new {@code Exception} that includes the current stack trace.
     */
    public NavConfigInitException() {
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage the detail message for this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public NavConfigInitException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage the detail message for this com.wba.horizon.android.framework.core.component.persistence.exception.
     * @param throwable     the cause of this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public NavConfigInitException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified cause.
     *
     * @param throwable the cause of this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public NavConfigInitException(Throwable throwable) {
        super(throwable);
    }
}