package com.nmp.android.netmeds.navigation;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nmp.android.netmeds.navigation.config.BaseRouteNode;
import com.nmp.android.netmeds.navigation.config.NavConfigInitException;
import com.nmp.android.netmeds.navigation.config.NavConfigManager;
import com.nmp.android.netmeds.navigation.config.RouteNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The RouteController class navigates activity depending on route node value.
 * These can be invoked directly from the routes file
 * Metadata from manifest file is read, route values are stored in map, base route node values are stored in array list.
 * Depending on the  metadata we navigate the page.
 */
public class RouteController {

    private static RouteController routeController = null;
    private final String TAG = RouteController.class.getName();
    private Map<String, RouteHandler> routeHandlerMap = null;

    /**
     * The RouteController Initialized
     *
     * @return RouteController bject
     */
    public static RouteController getRouteController() {
        if (routeController == null) {
            routeController = new RouteController();
        }

        return routeController;
    }

    /**
     * RouteController is initialized it returns the routeController object
     *
     * @param context Context represents environment data
     * @return RouteController object
     */
    public static RouteController initRouteController(Context context) {
        RouteController rntCntl = RouteController.getRouteController();
        rntCntl.init(context);

        return rntCntl;
    }

    /**
     * The route method is used to Dispatch the Activity.
     * The BaseRouteNode,Context is passed here.
     *
     * @param rtNode         provide the route node value
     * @param context        Context represents environment data
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    public static void route(BaseRouteNode rtNode, Context context, boolean isFromDeeplink) {
        ArrayList<BaseRouteNode> list = new ArrayList<>();
        list.add(rtNode);
        route(list, context, false, isFromDeeplink);
    }

    /**
     * The route method is used to dispatcher the route. That means we can go back to activity
     * The list of route nodes and environment context and task as a boolean value as passed here.
     *
     * @param routeNodes     provide the route node value
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    public static void route(List<BaseRouteNode> routeNodes, Context context, boolean onNewTask, boolean isFromDeeplink) {
        RouteDispatcher.dispatch(routeNodes, new TreeMap<String, Object>(), routeController.routeHandlerMap, context, onNewTask, isFromDeeplink);
    }

    /**
     * The route method is used to dispatcher the route. That means we can ro back to activity
     * The list of route nodes, environment context and task, as a boolean value
     * and routeParamList as Map value are passed here
     *
     * @param routeNodes     provide the route node value
     * @param routeParamList pass the route parameters
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    public static void route(List<BaseRouteNode> routeNodes
            , Map<String, Object> routeParamList
            , Context context
            , boolean onNewTask, boolean isFromDeeplink) {
        RouteDispatcher.dispatch(routeNodes, routeParamList, routeController.routeHandlerMap, context, onNewTask, isFromDeeplink);
    }

    /**
     * RouteController is initialized here.
     * NavConfigManager is initialized here.
     *
     * @param context Context represents environment data
     */
    public void init(Context context) {
        // Initialize configuration.
        try {
            NavConfigManager.init(context);
            this.initRouteNodeHandlers();
        } catch (NavConfigInitException e) {
        }

    }

    /**
     * The routeToNode method is used to handle the route depending on the RouteNode Name
     * In handleRoute, The list of BaseRouteNode, task, as boolean value passed here.
     * The NavConfigInitException handle here
     *
     * @param rtNodeName     provide the route node value
     * @param context        Context represents environment data
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     * @throws NavConfigInitException
     */
    public void routeToNode(String rtNodeName, Context context, boolean isFromDeeplink) throws NavConfigInitException {
        RouteNode rtN = NavConfigManager.getRouteNode(rtNodeName);

        if (rtN != null) {
            ArrayList<BaseRouteNode> list = new ArrayList<>();
            list.add(rtN);

            this.handleRoute(list, context, false, isFromDeeplink);
        } else {
            this.routeToDefault(context, isFromDeeplink);
        }
    }

    /**
     * The routeToNode method handle the route depending on the RouteNode Name and routeParamList
     * In handleRoute, The list of BaseRouteNode, task, as boolean value passed here
     * The NavConfigInitException handle here
     *
     * @param rtNodeName     provide the route node value
     * @param routeParamList pass the route parameters
     * @param context        Context represents environment data
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     * @throws NavConfigInitException
     */
    public void routeToNode(String rtNodeName, Map<String, Object> routeParamList, Context context, boolean isFromDeeplink) throws NavConfigInitException {
        RouteNode rtN = NavConfigManager.getRouteNode(rtNodeName);

        if (rtN != null) {
            ArrayList<BaseRouteNode> list = new ArrayList<>();
            list.add(rtN);

            this.handleRoute(list, routeParamList, context, false, isFromDeeplink);
        } else {
            this.routeToDefault(context, isFromDeeplink);
        }
    }

    /**
     * The routeToNode method is used to handle the route intent depending on the RouteNode Name and routeParamList
     * In handleRoute,The list of BaseRouteNode, task as boolean value are passed here
     * The NavConfigInitException are handle here
     *
     * @param rtNodeName     provide the route node value
     * @param routeIntent    Intent that will start the given activity
     * @param context        Context represents environment data
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     * @throws NavConfigInitException
     */
    public void routeToNode(String rtNodeName, Intent routeIntent, Context context, boolean isFromDeeplink) throws NavConfigInitException {
        RouteNode rtN = NavConfigManager.getRouteNode(rtNodeName);

        if (rtN != null) {
            ArrayList<BaseRouteNode> list = new ArrayList<>();
            list.add(rtN);

            this.handleRouteWithIntent(list, routeIntent, context, false, isFromDeeplink);
        } else {
            this.routeToDefault(context, isFromDeeplink);
        }
    }

    /**
     * private method
     * RouteNodeHandler are initialized
     * The NavConfigInitException are handle here
     *
     * @throws NavConfigInitException
     */
    private void initRouteNodeHandlers() throws NavConfigInitException {
        this.routeHandlerMap = new HashMap<>();
        Map<String, String> handlerConfig = NavConfigManager.getRouteNodeHandlers();

        Iterator<Map.Entry<String, String>> it = handlerConfig.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();

            try {
                this.routeHandlerMap.put(entry.getKey()
                        , (RouteHandler) Class.forName(entry.getValue()).getConstructor().newInstance());
            } catch (ClassCastException e) {
                Log.d("RouteConfigInitXception"
                        , String.format("%s class is not of type %s!", entry.getValue(), RouteHandler.class.toString()));
            } catch (Exception e) {
                //ClassNotFoundException, NoSuchMethodException,
                //IllegalAccessException, InstantiationException, InvocationTargetException
                Log.d("RouteConfigInitXception"
                        , String.format("%s class not found or invocation error!", entry.getValue()));
            }

        }
    }

    /**
     * The routeToDefault method is used to handle the Route.
     * The default route value, task, as a boolean value are passed here
     * The NavConfigInitException are handle here
     *
     * @param context        Context represents environment data
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     * @throws NavConfigInitException
     */
    private void routeToDefault(Context context, boolean isFromDeeplink) throws NavConfigInitException {
        Log.d("RouteController", "Routing to Default Route.");
        handleRoute(NavConfigManager.getDefaultRoute(), context, false, isFromDeeplink);
    }

    /**
     * The handleRoute method is used to dispatch the activity by using the RouteNode.
     * The BaseRouteNode,application context passed here
     *
     * @param routeNode      provide the route node value
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    private void handleRoute(BaseRouteNode routeNode, Context context, boolean onNewTask, boolean isFromDeeplink) {
        RouteDispatcher.dispatch(routeNode, this.routeHandlerMap, context, onNewTask, isFromDeeplink);
    }

    /**
     * The handleRoute method is used to dispatch the activity by using the RouteNode.
     * The list of routenode,application context,task as boolean are passed here
     *
     * @param routeNodes     provide the route node value
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    private void handleRoute(List<BaseRouteNode> routeNodes, Context context, boolean onNewTask, boolean isFromDeeplink) {
        RouteDispatcher.dispatch(routeNodes, null, this.routeHandlerMap, context, onNewTask, isFromDeeplink);
    }

    /**
     * The handleRoute method is used to dispatch the activity by using the RouteNode and routeParamList
     * The list of routenode, routeparamlist, application context,task as boolean are passed here
     *
     * @param routeNodes     provide the route node value
     * @param routeParamList pass the route parameters
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    private void handleRoute(List<BaseRouteNode> routeNodes, Map<String, Object> routeParamList, Context context, boolean onNewTask, boolean isFromDeeplink) {
        RouteDispatcher.dispatch(routeNodes, routeParamList, this.routeHandlerMap, context, onNewTask, isFromDeeplink);
    }

    /**
     * The handleRouteWithIntent method is used to dispatch the activity by using the RouteNode and routeIntent object
     * The list of routeNodes, routeIntent,application context,task as boolean value are passed here
     *
     * @param routeNodes     provide the route node value
     * @param routeIntent    Intent that will start the given activity
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the app link with in app or out side app
     */
    private void handleRouteWithIntent(List<BaseRouteNode> routeNodes, Intent routeIntent, Context context, boolean onNewTask, boolean isFromDeeplink) {
        RouteDispatcher.dispatchWithIntent(routeNodes, routeIntent, this.routeHandlerMap, context, onNewTask, isFromDeeplink);
    }
}