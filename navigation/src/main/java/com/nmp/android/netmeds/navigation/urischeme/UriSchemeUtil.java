package com.nmp.android.netmeds.navigation.urischeme;

import android.net.Uri;
import android.webkit.URLUtil;

import com.nmp.android.netmeds.navigation.NavConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class is utility class for UriScheme which all the utility methods needed in the UriScheme like getting the uri path.
 * This class has the utility to trim the uri.
 * The default deep link action assumes that the application is set up to handle deep links through Android implicit intents.
 * Implicit intents are intents used to launch components by specifying an action, category, and data instead of providing the component’s class.
 * This allows any application to invoke another application’s component to perform an action.
 */
public class UriSchemeUtil {

    private UriSchemeUtil() {

    }

    /**
     * This method is to get the path from the uri scheme.
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param uriScheme This is the uri for the match
     * @return The resulted uri path in string format
     */
    public static String getPath(Uri uriScheme) {
        return String.format("/%s%s", uriScheme.getHost(), uriScheme.getPath());
    }

    /**
     * This method is to trim the uri by getting the path segment from the uri scheme.
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param uriScheme This is the uri for the match
     * @return The resulted uri path segment
     */
    public static Uri trimUriScheme(Uri uriScheme) {
        List<String> pathSegments = new LinkedList<>(uriScheme.getPathSegments());

        for (int i = pathSegments.size() - 1; i >= 0; i--) {
            // remove trailing slashes
            if (pathSegments.get(i).equals(NavConstants.CHARACTER_SLASH)) {
                pathSegments.remove(i);
            }
        }

        StringBuilder pathStringBuilder = new StringBuilder();
        pathStringBuilder.append(NavConstants.CHARACTER_EMPTY);

        for (int i = 0; i < pathSegments.size(); i++) {
            pathStringBuilder.append(NavConstants.CHARACTER_SLASH);
            pathStringBuilder.append(pathSegments.get(i));
        }

        String pathString = pathStringBuilder.toString();
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(uriScheme.getScheme());
        builder.authority(uriScheme.getAuthority());
        builder.path(pathString);
        builder.query(uriScheme.getQuery());

        return builder.build();
    }

    /**
     * This method is to build uri using uri builder
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param scheme    This is the string uri for the match
     * @param authority This is the uri authority for the uri builder authority
     * @param path      This is the uri path segment for the uri builder path
     * @return The build uri.
     */
    public static Uri buildUri(String scheme, String authority, String path) {
        Uri.Builder bld = new Uri.Builder();

        bld.scheme(scheme);
        bld.authority(authority);
        bld.path(path);

        return bld.build();
    }

    /**
     * This method is to get the list of uri parameter using uri scheme and the route node string
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param uriScheme This is the uri for the match
     * @param route     This is the route node string
     * @return The resulted list of uri parameter from the uri scheme and route node string
     */
    public static List<String> getUriParam(Uri uriScheme, String route) {
        String param = (NavConstants.CHARACTER_SLASH + uriScheme.getAuthority() + uriScheme.getPath()).replace(route, NavConstants.CHARACTER_EMPTY);
        String urlParam = (NavConstants.CHARACTER_SLASH + uriScheme.getAuthority() + uriScheme.getPath()).replace(route + NavConstants.CHARACTER_SLASH, NavConstants.CHARACTER_EMPTY);

        ArrayList<String> list = new ArrayList<>();

        if (URLUtil.isValidUrl(urlParam)) {
            list.add(urlParam);
            return list;
        }

        String[] pathSegments = param.split(NavConstants.CHARACTER_SLASH, -1);

        for (int i = 0; i < pathSegments.length; i++) {
            if (pathSegments[i] != null && !pathSegments[i].equals(NavConstants.CHARACTER_EMPTY)) {
                list.add(pathSegments[i]);
            }
        }

        return list;
    }

    /**
     * This method is to get the list of uri query parameter using uri query value for the match uri
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     *
     * @param qValue This is the uri query value for the match uri
     * @return The resulted list of query parameter
     */
    public static Map<String, String> getQueryParams(String qValue) {
        TreeMap<String, String> results = new TreeMap<>();

        String tempQuery = NavConstants.CHARACTER_EMPTY;

        if (qValue != null) {
            String[] queryPairs = qValue.split(NavConstants.CHARACTER_AMPERSAND);

            for (String query : queryPairs) {
                if (!query.contains(NavConstants.CHARACTER_EQUALS)) {
                    query = tempQuery + NavConstants.CHARACTER_AMPERSAND + query;
                }
                List<String> nameAndValue = Arrays.asList(query.split(NavConstants.CHARACTER_EQUALS));

                if (nameAndValue.size() == 2) {
                    results.put(Uri.decode(nameAndValue.get(0)), Uri.decode(nameAndValue.get(1)));
                }
                tempQuery = query;
            }
        }

        return results;
    }
}
