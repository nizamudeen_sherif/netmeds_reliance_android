package com.nmp.android.netmeds.navigation;

import android.content.Intent;

import java.util.Map;

/**
 * Interface for RouteHandler
 */
public interface RouteHandler {
    /**
     * Method for execute Map
     *
     * @param routeParameters
     * @return
     */
    Map<String, Object> execute(Map<String, Object> routeParameters);

    /**
     * Method for RouteIntent
     *
     * @param routeIntent
     * @return
     */
    Intent execute(Intent routeIntent);
}