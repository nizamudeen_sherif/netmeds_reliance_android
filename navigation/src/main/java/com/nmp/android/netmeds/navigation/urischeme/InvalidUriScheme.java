package com.nmp.android.netmeds.navigation.urischeme;

/**
 * InvalidUriScheme Class
 */
public class InvalidUriScheme extends Exception {

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage the detail message for this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public InvalidUriScheme(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage the detail message for this com.wba.horizon.android.framework.core.component.persistence.exception.
     * @param throwable     the cause of this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public InvalidUriScheme(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified cause.
     *
     * @param throwable the cause of this com.wba.horizon.android.framework.core.component.persistence.exception.
     */
    public InvalidUriScheme(Throwable throwable) {
        super(throwable);
    }

}

