package com.nmp.android.netmeds.navigation.urischeme;

import android.net.Uri;
import android.text.TextUtils;

import com.nmp.android.netmeds.navigation.NavConstants;
import com.nmp.android.netmeds.navigation.RouteMapper;
import com.nmp.android.netmeds.navigation.RouteNodeNotFound;
import com.nmp.android.netmeds.navigation.config.BaseRouteNode;
import com.nmp.android.netmeds.navigation.config.NavConfigInitException;
import com.nmp.android.netmeds.navigation.config.NavConfigManager;
import com.nmp.android.netmeds.navigation.config.RouteNode;
import com.nmp.android.netmeds.navigation.config.RouteParamValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class is to map the uriScheme to open when the URI scheme is triggered and register an intent filter for it.
 * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
 * Open app via URI scheme if installed
 * Fall back to Play Store page if not installed
 * Deep Linking provides a mechanism to open an application to a specific resource.
 */
public class UriSchemeMapper {

    /**
     * Constructor class for UriSchemeMapper
     */
    private UriSchemeMapper() {

    }

    /**
     * This method is to match the uri scheme
     * Match query parameters against route node in descending order and validate the same.
     *
     * @param uriScheme This is the uri for the match
     * @return The matching uri Scheme
     * @throws InvalidUriScheme Exception that includes the current stack trace
     */
    public static UriScheme matchScheme(Uri uriScheme) throws InvalidUriScheme {

        //Get relatively mapped white listed scheme.
        UriScheme mappedUri = getRelativeMappedScheme(uriScheme);

        if (mappedUri == null)
            throw new InvalidUriScheme(String.format("Invalid Uri scheme '%s'!", uriScheme.toString()));

        return mappedUri;
    }

    /**
     * This method match the path parameter values for the destination Node
     * This will create a path based on the base route node to the destination node
     *
     * @param route      This is the base route node
     * @param pathParams This is the list of path
     * @return The resulting List of the matched path parameter string
     */
    static Map<String, String> matchPathParameters(BaseRouteNode route, List<String> pathParams) {
        Map<String, String> result = new TreeMap<>();
        Map<String, RouteParamValue> routeParamList = null;

        if (route instanceof RouteNode) {
            routeParamList = ((RouteNode) route).getRouteParameters();
        }

        if (routeParamList != null && !routeParamList.isEmpty() && !pathParams.isEmpty()) {
            Iterator<Map.Entry<String, RouteParamValue>> it = routeParamList.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, RouteParamValue> entry = it.next();

                if (!pathParams.isEmpty() && RouteMapper.validateRouteParam(entry.getKey(), pathParams.get(0), entry.getValue())) {
                    result.put(entry.getKey(), pathParams.get(0));

                    //Remove the pathParamValue
                    pathParams.remove(0);
                }
            }
        }

        return result;
    }

    /**
     * This method is to get relatively mapped white listed scheme
     * This will create a path based on the base route node to the destination node
     *
     * @param uriScheme This is the uri for the match
     * @return The related mapped white listed scheme
     * @throws InvalidUriScheme Exception that includes the current stack trace
     */
    private static UriScheme getRelativeMappedScheme(Uri uriScheme) throws InvalidUriScheme {
        UriScheme mappedUri = null;

        try {
            String mappedRoute = "";
            ArrayList<String> routeComponents = new ArrayList<>(uriScheme.getPathSegments());
            List<String> routes = NavConfigManager.getRoutes();

            routeComponents.add(0, uriScheme.getAuthority());

            //Find the relative match from white listed scheme
            while (!routeComponents.isEmpty()) {
                String temp = "/" + TextUtils.join("/", routeComponents.toArray());

                //Considering route starts with slash
                if (routes.contains(temp)) {
                    mappedRoute = temp;
                    break;
                } else {
                    routeComponents.remove(routeComponents.size() - 1);
                }
            }

            ArrayList<BaseRouteNode> nodeList = new ArrayList<>();
            Map<String, Object> routeParams = new TreeMap<>();
            Uri uri;

            if (!"".equals(mappedRoute)) {
                uri = UriSchemeUtil.buildUri(uriScheme.getScheme(), null, mappedRoute);

                List<String> pathParam = UriSchemeUtil.getUriParam(uriScheme, mappedRoute);

                //Process the path parameter values for the destination Node.
                BaseRouteNode rtNode = NavConfigManager.getRouteNodes().get(routeComponents.get(routeComponents.size() - 1));
                Map<String, String> qParams = matchPathParameters(rtNode, pathParam);

                //Merge the query parameters with path params. Note this will override
                //the param value from path parameters if any match.
                qParams.putAll(UriSchemeUtil.getQueryParams(uriScheme.getQuery()));

                createRouteParams(routeComponents, nodeList, qParams, routeParams);

                if (!pathParam.isEmpty()) {
                    routeParams.put(NavConstants.EXTRA_PATH_PARAMS, pathParam);
                }
            } else {
                uri = UriSchemeUtil.buildUri(uriScheme.getScheme(), NavConfigManager.getDefaultRoute().getId(), "");
                nodeList.add(NavConfigManager.getDefaultRoute());
            }

            mappedUri = new UriScheme(uri, nodeList, routeParams);

        } catch (NavConfigInitException e) {
            //Not initialized, hence return value is null.
        }

        return mappedUri;
    }

    /**
     * This method create route node and route params
     *
     * @param routeComponents Arraylist of route
     * @param nodeList        node list
     * @param qParams         qParams Map
     * @param routeParams     routeParams Map
     * @throws InvalidUriScheme
     */
    private static void createRouteParams(ArrayList<String> routeComponents, ArrayList<BaseRouteNode> nodeList, Map<String, String> qParams, Map<String, Object> routeParams) throws InvalidUriScheme {
        //Match query parameters against route node in descending order and validate the same.
        BaseRouteNode baseRouteNode;
        for (int i = routeComponents.size() - 1; i >= 0; i--) {
            try {
                baseRouteNode = RouteMapper.match(routeComponents.get(i));
            } catch (RouteNodeNotFound e) {

                break;
            }

            nodeList.add(0, baseRouteNode);
            if (!qParams.isEmpty()) {
                routeParams.putAll(RouteMapper.getParamValueList(qParams, baseRouteNode));
            }
        }
    }


    /**
     * This class is triggered and register an intent filter for it.
     * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
     * Open app via URI scheme if installed
     * Fall back to Play Store page if not installed
     * Deep Linking provides a mechanism to open an application to a specific resource.
     */
    public static final class UriScheme {
        private Uri uri;
        private ArrayList<BaseRouteNode> nodeList;
        private Map<String, Object> paramList;

        /**
         * This class is triggered and register an intent filter for it.
         * To handle the deep link in the app, you simply need to grab the intent data string in the Activity
         *
         * @param uri      This is the uri for navigation
         * @param nodeList This is the list of node from the base route node
         * @param params   This is the parameter list
         */
        UriScheme(Uri uri, ArrayList<BaseRouteNode> nodeList, Map<String, Object> params) {
            this.uri = uri;
            this.nodeList = nodeList;
            this.paramList = params;
        }

        /**
         * This method is to get the uri
         * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
         *
         * @return The resulted uri
         */
        public Uri getUri() {
            return this.uri;
        }

        /**
         * This method is to get the parameter
         * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
         *
         * @param key This is the key string for the parameter
         * @return Object The resulted parameter string
         */
        public Object getParameter(String key) {
            return this.paramList.get(key);
        }

        /**
         * This method is to get the parameter list
         * Deep Linking provides a mechanism to open an application to a specific resource.
         *
         * @return The list of resulted parameter
         */
        public Map<String, Object> getParamList() {
            return this.paramList;
        }

        /**
         * This method is to get the node list from the base route node
         * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
         *
         * @return The list of resulted node list from the base route node
         */
        public List<BaseRouteNode> getNodeList() {
            return this.nodeList;
        }
    }
}