package com.nmp.android.netmeds.navigation.config;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Nav config.
 */
public class NavConfig {

    @SerializedName("defaultRouteNode")
    private String defaultRouteNode = null;

    @SerializedName("routeNodeHandlers")
    private Map<String, String> handlers = new HashMap<>();

    @SerializedName("routeNodes")
    private Map<String, RouteNode> routeNodes = new HashMap<>();

    @SerializedName("routes")
    private List<String> routes = new ArrayList<>();

    /**
     * Instantiates a new Nav config.
     *
     * @param defaultRtNode the default rt node
     * @param pHandlers     the handlers
     * @param rtNodes       the rt nodes
     * @param routes        the routes
     */
    public NavConfig(String defaultRtNode, Map<String, String> pHandlers, Map<String, RouteNode> rtNodes, List<String> routes) {
        this.defaultRouteNode = defaultRtNode;
        this.handlers = pHandlers;
        this.routeNodes = rtNodes;
        this.routes = routes;
    }

    /**
     * Gets default route name.
     *
     * @return the default route name
     */
    public String getDefaultRouteName() {
        return this.defaultRouteNode;
    }

    /**
     * Sets default route name.
     *
     * @param nodeName the node name
     */
    protected void setDefaultRouteName(String nodeName) {
        this.defaultRouteNode = nodeName;
    }

    /**
     * Gets route node handlers.
     *
     * @return the route node handlers
     */
    public Map<String, String> getRouteNodeHandlers() {
        return this.handlers;
    }

    /**
     * Gets default route.
     *
     * @return the default route
     */
    public BaseRouteNode getDefaultRoute() {
        return this.routeNodes.get(this.defaultRouteNode);
    }

    /**
     * To Gets route nodes.
     *
     * @return the route nodes
     */
    public Map<String, RouteNode> getRouteNodes() {
        return this.routeNodes;
    }

    /**
     * To Get the list of routes.
     *
     * @return the list of routes
     */
    public List<String> getRoutes() {
        return this.routes;
    }
}