package com.nmp.android.netmeds.navigation.urischeme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.nmp.android.netmeds.navigation.NavConstants;

/**
 * This activity to open when the URI scheme is triggered and register an intent filter for it.
 * To handle the deep link in the app, you simply need to grab the intent data string in the Activity that was opened via the click
 * Open app via URI scheme if installed
 * Fall back to Play Store page if not installed
 * Deep Linking provides a mechanism to open an application to a specific resource.
 * This supports the opening of deep links via the payload of a notification, a link from a Rich App Page or the JavaScript Interface
 * The default deep link action assumes that the application is set up to handle deep links through Android implicit intents.
 * Implicit intents are intents used to launch components by specifying an action, category, and data instead of providing the component’s class.
 * This allows any application to invoke another application’s component to perform an action.
 */
public abstract class UriSchemeHandler extends AppCompatActivity {

    /**
     * This method is called when the activity is first created.
     * This is where you should do all of your normal static set up: create views, bind data to lists, etc.
     * This method also provides you with a Bundle containing the activity's previously frozen state, if there was one.
     *
     * @param savedInstanceState The savedInstanceState is a reference to a Bundle object that is passed into the onCreate method of every Android Activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean onNewTask = true;
        boolean isFromDeepLink = true;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(NavConstants.ON_NEW_TASK)) {
                onNewTask = savedInstanceState.getBoolean(NavConstants.ON_NEW_TASK);
            }
            // As Child class of UriSchemeHandler used for both URl push  ,Deeplink
            // Maintaining separate flag in savedInstanceState to Stop Navigation after
            // super.onCreate(savedInstanceState) call in UriSchemeHandler.
            if (savedInstanceState.containsKey(NavConstants.IS_FROM_DEEP_LINK)) {
                isFromDeepLink = savedInstanceState
                        .getBoolean(NavConstants.IS_FROM_DEEP_LINK);
            }
        }
        super.onCreate(savedInstanceState);

        navigateToLink(onNewTask, isFromDeepLink);
    }

    /**
     * This method launch the activity linked with the uri
     * As Child class of UriSchemeHandler used for both URl push, Deeplink Maintaining separate flag in savedInstanceState to Stop Navigation
     * Read JSON file and then route to the appropriate place
     *
     * @param onNewTask      indicates activity launch as a NewTask
     *                       </br> <b>true</b> Launch the activity as a new task
     *                       </br> <b>false</b> Launch the activity in the existing task
     * @param isFromDeepLink indicates navigation is from Deep Link
     *                       </br><b>true</b> route the navigation after trim the uri link and launch the activity based on the new task
     *                       </br><b>false</b> do nothing
     */
    protected void navigateToLink(boolean onNewTask, boolean isFromDeepLink) {
        if (isFromDeepLink) {
            navigateToPages(this.getIntent().getData());
        } else {
            handleIntent(getIntent());
        }
    }

    protected abstract void handleIntent(Intent intent);

    private void navigateToPages(Uri routeNodeUri) {
        // Read JSON file and then route to the appropriate place.
        try {
            UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(routeNodeUri), getApplicationContext(), false, true);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}