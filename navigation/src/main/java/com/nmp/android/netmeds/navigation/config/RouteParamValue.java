package com.nmp.android.netmeds.navigation.config;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * RouteParamValue Class
 */
public class RouteParamValue {

    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private Object value;

    @SerializedName("required")
    private boolean isRequired;

    private Map<String, RouteParamValue> params = new HashMap<>();


    /**
     * @param key
     * @param value
     */
    public RouteParamValue(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    /**
     * @param key
     * @param params
     */
    public RouteParamValue(String key, Map<String, RouteParamValue> params) {
        this.params = params;
        this.key = key;
    }

    public Map<String, RouteParamValue> getParamValues() {
        return this.params;
    }

    public Object getValue() {
        return this.value;
    }

    public String getKey() {
        return this.key;
    }

    public boolean isRequired() {
        return this.isRequired;
    }

    public boolean isPrimitiveType() {
        return this.params == null && this.value != null;
    }
}
