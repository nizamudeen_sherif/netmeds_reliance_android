package com.nmp.android.netmeds.navigation;

import android.text.TextUtils;
import android.util.Log;

import com.nmp.android.netmeds.navigation.config.BaseRouteNode;
import com.nmp.android.netmeds.navigation.config.NavConfigInitException;
import com.nmp.android.netmeds.navigation.config.NavConfigManager;
import com.nmp.android.netmeds.navigation.config.RouteNode;
import com.nmp.android.netmeds.navigation.config.RouteParamValue;
import com.nmp.android.netmeds.navigation.urischeme.InvalidUriScheme;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The RouteMapper class is used to  map the routes for all the classes. Availability of route node is checked.
 * Route node is mentioned on metadata in manifest file. Depending on route node the activity is launched.
 * Availability of Routecontroller is checked.
 * Route params are validated by passing the route Name
 * Availability of route parameter is checked.
 */
public class RouteMapper {

    /**
     * Constructor class for RouteMapper
     */
    private RouteMapper() {

    }

    /**
     * Availability of BaseRouteNode is checked.
     * It returns the BaseRouteNode value.
     * NavConfigInitException and RouteNodeNotFound Exception are handled in this class.
     *
     * @param routeName provide the route node value
     * @return BaseRouteNode provide the base route node value
     * @throws RouteNodeNotFound it throws RouteNodeNotFound Exception
     */
    public static BaseRouteNode match(String routeName) throws RouteNodeNotFound {
        BaseRouteNode route = null;
        try {
            route = NavConfigManager.getRouteNodes().get(routeName);
        } catch (NavConfigInitException e) {

        }

        if (route == null)
            throw new RouteNodeNotFound(String.format("Route Node '%s' not found.", routeName));

        return route;
    }

    /**
     * In this returns the TreeMap values. Using BaseRouteNode we can get the routeParamList
     * In this method we have pass the queryParamList and BaseRouteNode value
     * Here we handle the InvalidUriScheme Exception
     *
     * @param queryParamList pass the queryParamlist
     * @param routeOption    provide the base route node value
     * @return it returns the TreeMap
     * @throws InvalidUriScheme it throws the InvalidUriScheme Exception
     */
    public static Map<String, String> getParamValueList(Map<String, String> queryParamList
            , BaseRouteNode routeOption) throws InvalidUriScheme {
        Map<String, String> result = new TreeMap<>();
        Map<String, RouteParamValue> routeParamList = null;

        if (routeOption instanceof RouteNode) {
            routeParamList = ((RouteNode) routeOption).getRouteParameters();
        }
        updateResultFromRouteParamValue(queryParamList, routeParamList, result);
        return result;
    }

    /**
     * This method returns the result from param value
     *
     * @param queryParamList pass the queryParamlist
     * @param routeParamList Map for route param list
     * @param result         Map for result
     * @return Result map
     * @throws InvalidUriScheme it throws the InvalidUriScheme Exception
     */
    public static void updateResultFromRouteParamValue(Map<String, String> queryParamList, Map<String, RouteParamValue> routeParamList, Map<String, String> result) throws InvalidUriScheme {
        if (routeParamList != null && routeParamList.size() > 0 && queryParamList.size() > 0) {
            Iterator<Map.Entry<String, RouteParamValue>> it = routeParamList.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, RouteParamValue> entry = it.next();

                if (queryParamList.containsKey(entry.getKey()) &&
                        RouteMapper.validateRouteParam(entry.getKey(), queryParamList.get(entry.getKey()), entry.getValue())) {
                    result.put(entry.getKey(), queryParamList.get(entry.getKey()));

                    //Remove the pathParamValue
                    queryParamList.remove(entry.getKey());
                } else if (entry.getValue() != null && entry.getValue().isRequired()) { //checkForRequiredRouteParameters
                    throw new InvalidUriScheme("Invalid Uri scheme request, mandatory parameters are missing!");
                }
            }
        }
    }

    /**
     * RouteParam are validated by using the routeParamName,paramvalue and RouteNode.
     * It returns the the boolean value
     *
     * @param routeParamName provide the route node value
     * @param paramValue     provide the string value
     * @param routeOptions   Pass the RouteNode Value
     * @return it returns the boolean value
     */
    public static boolean validateRouteParam(String routeParamName
            , String paramValue
            , RouteNode routeOptions) {
        Map<String, RouteParamValue> routeParameters = routeOptions.getRouteParameters();
        RouteParamValue rtParam = routeParameters.get(routeParamName);

        return validateRouteParam(routeParamName, paramValue, rtParam);
    }

    /**
     * RouteParam are validated by using the routeParamName,paramvalue and RouteParamValue.
     * It returns the the boolean value
     *
     * @param routeParamName provide the route node value
     * @param pParamValue    provide the string value
     * @param rtParam        pass the route param value
     * @return it returns the boolean value
     */
    public static boolean validateRouteParam(String routeParamName, String pParamValue, RouteParamValue rtParam) {
        String regexString = getRegExValue(rtParam);
        String paramValue = pParamValue;
        Log.i("", routeParamName);
        if (paramValue == null)
            paramValue = "";

        if (rtParam == null) {
            return Boolean.TRUE;
        } else if (!TextUtils.isEmpty(regexString)) {
            return paramValue.matches(regexString);
        } else if (rtParam.isRequired() && "".equals(paramValue)) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    /**
     * The getRegExValue method return the regex value,
     * if paramValue is empty, it returns the null value
     *
     * @param paramValue pass the routeParam Value
     * @return it returns the regex value
     */
    private static String getRegExValue(RouteParamValue paramValue) {
        if (paramValue != null
                && paramValue.isPrimitiveType()
                && paramValue.getKey().equals(NavConstants.REGEX_JSON_NAME)) {
            return (String) paramValue.getValue();
        } else if (paramValue != null) {
            Iterator<Map.Entry<String, RouteParamValue>> itr = paramValue.getParamValues().entrySet().iterator();
            while (itr.hasNext()) {
                Map.Entry<String, RouteParamValue> item = itr.next();

                return getRegExValue(item.getValue());
            }
        }

        return null;
    }

    /**
     * validate the Route node parameters. It returns the boolean value
     * The routeNode and map values are passed here
     *
     * @param routeOptions pass the RouteNode Value
     * @param results      pass the map object
     * @return it returns boolean value
     */
    public static Boolean checkForRequiredRouteParameters(RouteNode routeOptions, Map<String, String> results) {
        List<String> requiredRouteParameterValues = getRequiredRouteParameterValues(routeOptions);
        for (String requiredValue : requiredRouteParameterValues) {
            if (results.get(requiredValue) == null) {
                Log.e("UriScheme", "Required route node parameter is missing.");
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    /**
     * The getRequiredRouteParameterValues method is used to get required Route Parameter values
     *
     * @param routeOptions pass the Route Node Value
     * @return it returns the list of values
     */
    static List<String> getRequiredRouteParameterValues(RouteNode routeOptions) {
        List<String> requiredRouteParameters = new ArrayList<>();

        if (routeOptions.getRouteParameters() != null && routeOptions.getRouteParameters().size() > 0) {
            Map<String, RouteParamValue> routeParameters = routeOptions.getRouteParameters();
            Iterator<Map.Entry<String, RouteParamValue>> keys = routeParameters.entrySet().iterator();

            while (keys.hasNext()) {
                Map.Entry<String, RouteParamValue> item = keys.next();
                RouteParamValue rParamVal = item.getValue();

                if (rParamVal.isRequired()) {
                    requiredRouteParameters.add(rParamVal.getKey());
                }
            }
        }

        return requiredRouteParameters;
    }
}