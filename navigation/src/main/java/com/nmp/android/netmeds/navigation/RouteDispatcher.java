package com.nmp.android.netmeds.navigation;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.TaskStackBuilder;

import com.nmp.android.netmeds.navigation.config.BaseRouteNode;
import com.nmp.android.netmeds.navigation.config.RouteNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * RouteDispatcher is the common core of storeOps route dispatching.
 * The RouteDispatcher class is used to dispatch the activity by passing the
 * route node value. The meta data is read from manifest file,
 * route values are stored in map and base route node values are stored in arraylist.
 * Page navigation is based on the metadata.
 */
public class RouteDispatcher {
    private final String TAG = RouteDispatcher.class.getName();

    /**
     * Constructor for RouteDispatcher class
     */
    private RouteDispatcher() {
    }


    /**
     * The dispatch method is used to previous activity is dispatched using route node.
     * The context environment data and RouteHandler are passed here
     *
     * @param routeNode      provide the route node value
     * @param context        Context represents environment data
     * @param handlers       identifying the callback thread
     * @param isFromDeeplink this boolean value provide the open app link with in app or out side app
     * @param <T>            this generic extends the BaseRouteNode
     */
    public static <T extends BaseRouteNode> void dispatch(T routeNode, Context context, Map<String, RouteHandler> handlers, boolean isFromDeeplink) {
        RouteDispatcher.dispatch(routeNode, null, handlers, context, false, isFromDeeplink);
    }

    /**
     * The dispatch method is used to previous activity is dispatched using route node.
     * Context environment data and RouteHandler are passed
     *
     * @param routeNode      provide the route node value
     * @param handlers       identifying the callback thread
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the open app link with in app or out side app
     * @param <T>            this generic extends the BaseRouteNode
     */
    public static <T extends BaseRouteNode> void dispatch(T routeNode, Map<String, RouteHandler> handlers, Context context, boolean onNewTask, boolean isFromDeeplink) {
        Log.i(RouteDispatcher.class.getName(), String.valueOf(onNewTask));
        RouteDispatcher.dispatch(routeNode, null, handlers, context, false, isFromDeeplink);
    }

    /**
     * The dispatch method is used to previous activity is dispatched using route node and routeParameters.
     * The context environment data and RouteHandler are passed here
     *
     * @param routeNode        provide the route node value
     * @param pRouteParameters pass the route parameters
     * @param handlers         identifying the callback thread
     * @param context          Context represents environment data
     * @param onNewTask        information about the activity task
     * @param isFromDeeplink   this boolean value provide the open app link with in app or out side app
     * @param <T>              this generic extends the BaseRouteNode
     */
    public static <T extends BaseRouteNode> void dispatch(T routeNode, Map<String, Object> pRouteParameters, Map<String, RouteHandler> handlers, Context context, boolean onNewTask, boolean isFromDeeplink) {

        Map<String, Object> routeParameters = pRouteParameters;

        if (routeParameters == null)
            routeParameters = new TreeMap<>();

        RouteDispatcher.executeHandlers(routeNode, routeParameters, handlers);

        ArrayList<Intent> list = new ArrayList<>();
        list.add(IntentFactory.buildIntent(routeNode, routeParameters, context, onNewTask, isFromDeeplink));

        startActivities(list, context, onNewTask);

    }

    /**
     * The dispatch method is used to previous activity is dispatched using route node and routeParameterList
     * The context environment data and RouteHandler are passed here
     *
     * @param routeNodes     provide the route node value
     * @param routeParamList pass the route parameters
     * @param handlers       identifying the callback thread
     * @param context        Context represents environment data
     * @param isFromDeeplink this boolean value provide the open app link with in app or out side app
     * @param <T>            this generic extends the BaseRouteNode
     */
    public static <T extends BaseRouteNode> void dispatch(List<T> routeNodes, Map<String, Object> routeParamList, Map<String, RouteHandler> handlers, Context context, boolean isFromDeeplink) {
        dispatch(routeNodes, routeParamList, handlers, context, false, isFromDeeplink);
    }

    /**
     * The dispatch method is used to build the intent and start the activities
     * depending on the list of routeNodes and routeParaList
     *
     * @param routeNodes      provide the route node value
     * @param pRouteParamList pass the route parameters
     * @param handlers        identifying the callback thread
     * @param context         Context represents environment data
     * @param onNewTask       information about the activity task
     * @param isFromDeeplink  this boolean value provide the open app link with in app or out side app
     * @param <T>             this generic extends the BaseRouteNode
     */

    public static <T extends BaseRouteNode> void dispatch(List<T> routeNodes, Map<String, Object> pRouteParamList, Map<String, RouteHandler> handlers, Context context, boolean onNewTask, boolean isFromDeeplink) {

        Iterator<T> it = routeNodes.listIterator();
        Map<String, Object> routeParamList = pRouteParamList;
        ArrayList<Intent> list = new ArrayList<>();
        RouteNode rtNode;
        Intent intent;

        if (routeParamList == null)
            routeParamList = new TreeMap<>();

        while (it.hasNext()) {
            rtNode = (RouteNode) it.next();
            if (rtNode instanceof RouteNode) {
                //Execute routeNode handler
                RouteDispatcher.executeHandlers(rtNode, routeParamList, handlers);

                intent = IntentFactory.buildIntent(rtNode, routeParamList, context, onNewTask, isFromDeeplink);

            } else {
                intent = IntentFactory.buildIntent(rtNode, null, context, onNewTask, isFromDeeplink);
            }

            list.add(intent);
        }

        startActivities(list, context, onNewTask);

    }

    /**
     * The dispatchWithIntent method is used to previous activity is dispatched by passing the intent.
     * Navigate to activities depending on the list of routeNodes.
     *
     * @param routeNodes     provide the route node value
     * @param pRouteIntent   Intent that will start the given activity
     * @param handlers       identifying the callback thread
     * @param context        Context represents environment data
     * @param onNewTask      information about the activity task
     * @param isFromDeeplink this boolean value provide the open app link with in app or out side app
     * @param <T>            this generic extends the BaseRouteNode
     */
    public static <T extends BaseRouteNode> void dispatchWithIntent(List<T> routeNodes, Intent pRouteIntent, Map<String, RouteHandler> handlers, Context context, boolean onNewTask, boolean isFromDeeplink) {

        Iterator<T> it = routeNodes.listIterator();
        Intent routeIntent = pRouteIntent;
        ArrayList<Intent> list = new ArrayList<>();
        RouteNode rtNode;
        Intent intent;

        if (routeIntent == null)
            routeIntent = new Intent();

        while (it.hasNext()) {
            rtNode = (RouteNode) it.next();
            if (rtNode instanceof RouteNode) {
                //Execute routeNode handler
                RouteDispatcher.executeHandlers(rtNode, routeIntent, handlers);

                intent = IntentFactory.buildNavigationIntent(rtNode, routeIntent, context, onNewTask, isFromDeeplink);

            } else {
                intent = IntentFactory.buildNavigationIntent(rtNode, null, context, onNewTask, isFromDeeplink);
            }

            list.add(intent);
        }

        startActivities(list, context, onNewTask);

    }

    /**
     * The executeHandlers method return routeParameters.
     * The routeNode, routeParameters,handlers and context are passed here
     *
     * @param routeNode        provide the route node value
     * @param pRouteParameters pass the route parameters
     * @param handlers         identifying the callback thread
     * @param <T>              this generic extends the BaseRouteNode
     * @return return the routeParameters
     */
    public static <T extends BaseRouteNode> Map<String, Object> executeHandlers(T routeNode, Map<String, Object> pRouteParameters, Map<String, RouteHandler> handlers) {
        Map<String, Object> routeParameters = pRouteParameters;
        if (routeNode instanceof RouteNode) {
            RouteNode rtNode = (RouteNode) routeNode;
            if (rtNode.getHandlers() != null && rtNode.getHandlers().size() > 0) {
                checkRouteHandler(routeNode, routeParameters, handlers);

            }
        }

        return routeParameters;
    }

    /**
     * The executeHandlers method return routeParameters.
     * The routeNode, routeParameters,handlers and context are passed here
     *
     * @param routeNode
     * @param pRouteParameters
     * @param handlers
     * @param <T>
     * @return
     */
    static <T extends BaseRouteNode> Map<String, Object> checkRouteHandler(T routeNode, Map<String, Object> pRouteParameters, Map<String, RouteHandler> handlers) {
        List nodeHandlers = ((RouteNode) routeNode).getHandlers();
        Map<String, Object> routeParameters = new HashMap<>();
        for (int i = 0; i < nodeHandlers.size(); i++) {
            RouteHandler handler = handlers.get(nodeHandlers.get(i));

            if (handler != null) {
                routeParameters = handler.execute(pRouteParameters);
            } else {
                Log.e("UriScheme", "Handler " + nodeHandlers.get(i) + " has not been registered with the UriScheme library.");
            }
        }
        return routeParameters;
    }

    /**
     * The executeHandlers method return intent object.
     * The routeNode, routeIntent object,handlers and context are passed here.
     *
     * @param routeNode   provide the route node value
     * @param routeIntent Intent that will start the given activity
     * @param handlers    identifying the callback thread
     * @param <T>         this generic extends the BaseRouteNode
     * @return the intent value
     */
    public static <T extends BaseRouteNode> Intent executeHandlers(T routeNode, Intent routeIntent, Map<String, RouteHandler> handlers) {
        Intent routeToIntent = routeIntent;
        if (routeNode instanceof RouteNode) {
            RouteNode rtNode = (RouteNode) routeNode;

            if (rtNode.getHandlers() != null && rtNode.getHandlers().size() > 0) {
                List nodeHandlers = ((RouteNode) routeNode).getHandlers();
                routeToIntent = executeHandler(nodeHandlers, handlers, routeToIntent);
            }
        }

        return routeToIntent;
    }

    /**
     * The executeHandler method return intent object.
     *
     * @param nodeHandlers      List of node handler
     * @param handlers          Map
     * @param routeIntentObject Intent that will start the given activity
     * @return the intent value
     */
    public static Intent executeHandler(List nodeHandlers, Map<String, RouteHandler> handlers, Intent routeIntentObject) {
        Intent routeToIntent = routeIntentObject;
        for (int i = 0; i < nodeHandlers.size(); i++) {
            RouteHandler handler = handlers.get(nodeHandlers.get(i));

            if (handler != null) {
                routeToIntent = handler.execute(routeToIntent);
            } else {
                Log.e("UriScheme", "Handler " + nodeHandlers.get(i) + " has not been registered with the UriScheme library.");
            }
        }
        return routeToIntent;
    }

    /**
     * The startActivities method is used to start the Activities by passing the list of intent
     * and task, as a boolean value.
     *
     * @param list      pass the list of intent objects
     * @param context   Context represents environment data
     * @param onNewTask information about the activity task
     */
    private static void startActivities(List<Intent> list, Context context, boolean onNewTask) {

        try {
            if (!onNewTask) {
                //Launch activity on the existing task.
                Intent[] int1 = list.toArray(new Intent[list.size()]);
                context.startActivities(int1);
            } else {
                TaskStackBuilder tStkBuilder = TaskStackBuilder.create(context);

                Iterator<Intent> itr = list.listIterator();

                while (itr.hasNext()) {
                    tStkBuilder.addNextIntent(itr.next());
                }
                //Launch activity on the new task.
                tStkBuilder.startActivities();
            }
        } catch (RuntimeException e) {
        }

    }
}