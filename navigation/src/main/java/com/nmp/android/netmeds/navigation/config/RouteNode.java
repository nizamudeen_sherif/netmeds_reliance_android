package com.nmp.android.netmeds.navigation.config;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The type Route node.
 */
public class RouteNode extends BaseRouteNode {

    @SerializedName("handlers")
    private List<String> handlers = new ArrayList<>();

    @SerializedName("routeParameters")
    private Map<String, RouteParamValue> params = new TreeMap<>();

    /**
     * Instantiates a new Route node.
     *
     * @param id       the id
     * @param type     the type
     * @param handlers the handlers
     * @param params   the params
     */
    public RouteNode(String id, String type, List<String> handlers, Map<String, RouteParamValue> params) {
        super(id, type);

        this.handlers = handlers;
        this.params = params;
    }

    /**
     * Gets handlers.
     *
     * @return the handlers
     */
    public List<String> getHandlers() {
        return this.handlers;
    }

    /**
     * Gets route parameters.
     *
     * @return the route parameters
     */
    public Map<String, RouteParamValue> getRouteParameters() {
        return this.params;
    }
}
