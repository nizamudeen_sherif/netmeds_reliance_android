/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaypalPaymentRequest {
    @SerializedName("intent")
    private String intent;
    @SerializedName("payer")
    private PaypalPaymentRequestPayer payer;
    @SerializedName("application_context")
    private PaypalPaymentRequestContext context;
    @SerializedName("transactions")
    private List<PaypalPaymentRequestTransaction> transactions = null;
    @SerializedName("note_to_payer")
    private String noteToPayer;
    @SerializedName("redirect_urls")
    private PaypalPaymentRequestRedirectUrls redirectUrls;

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public PaypalPaymentRequestPayer getPayer() {
        return payer;
    }

    public void setPayer(PaypalPaymentRequestPayer payer) {
        this.payer = payer;
    }

    public List<PaypalPaymentRequestTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<PaypalPaymentRequestTransaction> transactions) {
        this.transactions = transactions;
    }

    public String getNoteToPayer() {
        return noteToPayer;
    }

    public void setNoteToPayer(String noteToPayer) {
        this.noteToPayer = noteToPayer;
    }

    public PaypalPaymentRequestRedirectUrls getRedirectUrls() {
        return redirectUrls;
    }

    public void setRedirectUrls(PaypalPaymentRequestRedirectUrls redirectUrls) {
        this.redirectUrls = redirectUrls;
    }

    public PaypalPaymentRequestContext getContext() {
        return context;
    }

    public void setContext(PaypalPaymentRequestContext context) {
        this.context = context;
    }
}