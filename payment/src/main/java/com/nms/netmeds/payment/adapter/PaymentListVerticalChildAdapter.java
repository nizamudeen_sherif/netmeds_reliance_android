package com.nms.netmeds.payment.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.PaymentGatewayList;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.PaymentListVerticalChildRowBinding;
import com.nms.netmeds.payment.ui.PaymentConstants;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;

import java.util.List;

public class PaymentListVerticalChildAdapter extends RecyclerView.Adapter<PaymentListVerticalChildAdapter.PaymentListVerticalChildAdapterViewHolder> {
    private Context context;
    private PaymentListVerticalChildAdapterCallback callback;

    private List<PaymentGatewaySubCategory> paymentList;

    private int parenAdapterPosition;

    private boolean isPayTmUPIActive;
    private boolean isGooglePayUPIActive;

    public PaymentListVerticalChildAdapter(Context context, PaymentListVerticalChildAdapterCallback callback, PaymentGatewayList paymentGatewayParentCategory,
                                           boolean isPayTmUPIActive, boolean isGooglePayUPIActive, int parenAdapterPosition) {
        this.context = context;
        this.callback = callback;
        this.paymentList = paymentGatewayParentCategory.getSubList();
        this.parenAdapterPosition = parenAdapterPosition;
        this.isGooglePayUPIActive = isGooglePayUPIActive;
        this.isPayTmUPIActive = isPayTmUPIActive;
    }

    @NonNull
    @Override
    public PaymentListVerticalChildAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PaymentListVerticalChildRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.payment_list_vertical_child_row, parent, false);
        return new PaymentListVerticalChildAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListVerticalChildAdapterViewHolder holder, int position) {
        holder.bindPaymentGatewayData();
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListVerticalChildAdapterViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        holder.bindPaymentGatewayData();
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class PaymentListVerticalChildAdapterViewHolder extends RecyclerView.ViewHolder {

        private PaymentListVerticalChildRowBinding binding;
        private PaymentListVerticalChildAdapterViewHolder childAdapterViewHolder;
        private boolean isWallet;

        public PaymentListVerticalChildAdapterViewHolder(PaymentListVerticalChildRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.childAdapterViewHolder = this;
        }

        public void bindPaymentGatewayData() {
            PaymentGatewaySubCategory paymentGatewayDetail = paymentList.get(getAdapterPosition());
            isWallet = PaymentConstants.WALLET_LIST == paymentGatewayDetail.getParentId();
            /**
             * display name is not coming for Saved cards details.
             * so we re-assign the title name as display name in model class.
             */
            if (PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayDetail.getParentId()) {
                paymentGatewayDetail.setDisplayName(paymentGatewayDetail.getTitle());
            }
            binding.setGateway(paymentGatewayDetail);
            binding.setViewHolder(childAdapterViewHolder);
            binding.chkPaymentGateway.setChecked(paymentGatewayDetail.isGatewayChecked());

            binding.llPaymentParentView.setOnClickListener(gatewayClickListener(paymentGatewayDetail));
            binding.tvPay.setOnClickListener(initiatePlaceOrderClickListener(paymentGatewayDetail));
            binding.edtCvv.addTextChangedListener(isEnablePayButton(paymentGatewayDetail));
            setPaymentEnableDisableCondition(paymentGatewayDetail);
            initiateViews(paymentGatewayDetail);
        }

        private void setPaymentEnableDisableCondition(PaymentGatewaySubCategory paymentGatewayDetail) {
            boolean isGateWayEnable = PaymentConstants.PAY_TM_UPI_ID.equalsIgnoreCase(paymentGatewayDetail.getId()) ? isPayTmUPIActive : !PaymentConstants.GOOGLE_PAY_ID.equalsIgnoreCase(paymentGatewayDetail.getId()) || isGooglePayUPIActive;
            binding.llPaymentParentView.setEnabled(isGateWayEnable);
            binding.llPaymentParentView.setClickable(isGateWayEnable);
            binding.llPaymentParentView.setFocusable(isGateWayEnable);
            binding.llPaymentParentView.setAlpha(isGateWayEnable ? 1 : 0.4f);
        }

        private void initiateViews(PaymentGatewaySubCategory paymentGatewayDetail) {
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(PaymentConstants.NET_BANKING_LIST == paymentGatewayDetail.getParentId() ? R.drawable.ic_net_banking : R.drawable.ic_no_image)
                    .error(PaymentConstants.NET_BANKING_LIST == paymentGatewayDetail.getParentId() ? R.drawable.ic_net_banking : R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context).load(paymentGatewayDetail.getImage())
                    .apply(options)
                    .into(binding.imgGateway);
            setViewVisibilities(paymentGatewayDetail);
        }

        private void setViewVisibilities(PaymentGatewaySubCategory paymentGatewayDetail) {
            binding.trInitPay.setVisibility(isPaymentSelected(paymentGatewayDetail) ? View.VISIBLE : View.GONE);
            binding.tvWalletBalance.setVisibility(isBalanceEnable(paymentGatewayDetail) ? View.VISIBLE : View.GONE);
            binding.chkPaymentGateway.setVisibility(isRadioButtonVisible(paymentGatewayDetail) ? View.VISIBLE : View.GONE);
            binding.tvWalletLink.setVisibility(isWallet && !paymentGatewayDetail.getLinked() ? View.VISIBLE : View.GONE);
            binding.tvGatewayOffer.setVisibility(paymentGatewayDetail.isShowOffer() && !TextUtils.isEmpty(paymentGatewayDetail.getOffer()) ? View.VISIBLE : View.GONE);
            binding.llCvv.setVisibility(isCVVViewEnable(paymentGatewayDetail) ? View.VISIBLE : View.GONE);
            binding.viewDivider.setVisibility(getAdapterPosition() + 1 != paymentList.size() ? View.VISIBLE : View.GONE);
            if (PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayDetail.getParentId()) {
                checkPayButtonProperties();
                if (paymentGatewayDetail.isGatewayChecked()) {
                    binding.edtCvv.requestFocus();
                }
            }
        }

        private TextWatcher isEnablePayButton(PaymentGatewaySubCategory paymentGatewayDetail) {
            return new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    checkPayButtonProperties();
                }
            };
        }

        private void checkPayButtonProperties() {
            boolean isEnablePayButton = PaymentUtils.cvvValidation(binding.edtCvv.getText().toString());
            binding.tvPay.setEnabled(isEnablePayButton);
            binding.tvPay.setClickable(isEnablePayButton);
            binding.tvPay.setAlpha(isEnablePayButton ? 1f : 0.4f);
        }

        private boolean isPaymentSelected(PaymentGatewaySubCategory paymentGatewayDetail) {
            return isWallet ? paymentGatewayDetail.getLinked() && paymentGatewayDetail.isGatewayChecked() : paymentGatewayDetail.isGatewayChecked();
        }

        private View.OnClickListener gatewayClickListener(final PaymentGatewaySubCategory paymentGatewayDetail) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!paymentGatewayDetail.isGatewayChecked()) {
                        callback.updatePaymentSelection(paymentGatewayDetail, getAdapterPosition(), parenAdapterPosition);
                    }
                }
            };
        }

        private View.OnClickListener initiatePlaceOrderClickListener(final PaymentGatewaySubCategory paymentGatewayDetail) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.placeOrder(paymentGatewayDetail, binding.edtCvv.getText().toString());
                }
            };
        }

        public String getWalletBalance(PaymentGatewaySubCategory paymentGatewayDetail) {
            return CommonUtils.getPriceInFormat(paymentGatewayDetail.getCurrentBalance());
        }

        public boolean isRadioButtonVisible(PaymentGatewaySubCategory paymentGatewayDetail) {
            return !isWallet || paymentGatewayDetail.getLinked();
        }

        public boolean isBalanceEnable(PaymentGatewaySubCategory paymentGatewayDetail) {
            return isWallet && paymentGatewayDetail.getLinked() && PaymentConstants.PAYTM != paymentGatewayDetail.getSubId() && PaymentConstants.OLA != paymentGatewayDetail.getSubId() && paymentGatewayDetail.getCurrentBalance() > 0;
        }

        public boolean isCVVViewEnable(PaymentGatewaySubCategory paymentGatewayDetail) {
            return paymentGatewayDetail.isGatewayChecked() && PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayDetail.getParentId();
        }

        public String getPayButtonText() {
            return String.format(context.getString(R.string.text_pay_rs), CommonUtils.getPriceInFormat(callback.getTotalAmount()));
        }
    }

    public interface PaymentListVerticalChildAdapterCallback {

        void updatePaymentSelection(PaymentGatewaySubCategory selectedPaymentDetails, int childAdapterPosition, int parenAdapterPosition);

        void placeOrder(PaymentGatewaySubCategory selectedPaymentDetails, String cvv);

        void navigateToLinkWallet(PaymentGatewaySubCategory paymentGatewayDetail);

        double getTotalAmount();
    }
}
