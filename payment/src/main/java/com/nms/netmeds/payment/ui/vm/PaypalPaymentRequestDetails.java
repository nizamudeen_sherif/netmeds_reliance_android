package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;


public class PaypalPaymentRequestDetails {
    @SerializedName("subtotal")
    private String subtotal;
    @SerializedName("tax")
    private String tax;
    @SerializedName("shipping")
    private String shipping;
    @SerializedName("handling_fee")
    private String handlingFee;
    @SerializedName("shipping_discount")
    private String shippingDiscount;
    @SerializedName("insurance")
    private String insurance;

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getHandlingFee() {
        return handlingFee;
    }

    public void setHandlingFee(String handlingFee) {
        this.handlingFee = handlingFee;
    }

    public String getShippingDiscount() {
        return shippingDiscount;
    }

    public void setShippingDiscount(String shippingDiscount) {
        this.shippingDiscount = shippingDiscount;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }
}