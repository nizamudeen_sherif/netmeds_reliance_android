package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.FragmentCashOnDeliveryBinding;

@SuppressLint("ValidFragment")
public class CashOnDeliveryFragment extends BaseDialogFragment {

    private final CashOnDeliveryListener mListener;

    public CashOnDeliveryFragment(CashOnDeliveryListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentCashOnDeliveryBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cash_on_delivery, container, false);
        if (getActivity() != null && getActivity().getResources() != null) {
            binding.title.setText(getActivity().getResources().getString(R.string.text_to_COD));
            binding.description.setText(getActivity().getResources().getString(R.string.text_COD_alert));
            binding.positiveButton.setText(getActivity().getResources().getString(R.string.text_confirm));
        }
        binding.closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        binding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.initCODPayment();
                dismiss();
            }
        });
        binding.cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return binding.getRoot();
    }

    public interface CashOnDeliveryListener {
        void initCODPayment();
    }
}
