package com.nms.netmeds.payment.ui.vm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.FragmentPaymentDeductedBinding;

public class PaymentDetectedFragment extends BaseDialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentPaymentDeductedBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_deducted, container, false);
        binding.closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return binding.getRoot();
    }
}
