package com.nms.netmeds.payment.ui;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseServiceManager;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.BaseApiClient;
import com.nms.netmeds.payment.model.PayTmTransactionStatusResponse;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentKeyResponse;
import com.nms.netmeds.payment.ui.vm.JusPayCreateOrderAndOrderStatusResponse;
import com.nms.netmeds.payment.ui.vm.JusPayCreateUpdateResponse;
import com.nms.netmeds.payment.ui.vm.NetBankingListResponse;
import com.nms.netmeds.payment.ui.vm.PayPalTokenResponse;
import com.nms.netmeds.payment.ui.vm.PayTmChecksumResponse;
import com.nms.netmeds.payment.ui.vm.PayTmWithdrawResponse;
import com.nms.netmeds.payment.ui.vm.PaypalExecutePaymentRequest;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequest;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentResponse;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmValidateOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateTokenResponse;
import com.nms.netmeds.payment.ui.vm.PaytmWithdrawRequest;
import com.nms.netmeds.payment.ui.vm.TransactionLogRequest;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentServiceManager extends BaseServiceManager {

    public static final int DELETE_COUPON = 241;
    public static final int REMOVE_NMS_WALLET = 206;

    /**
     * PAYPAL
     */
    public static final int PAY_PAL_TOKEN = 218;
    public static final int INIT_PAY_PAL_PAYMENT = 219;
    public static final int PAY_PAL_PAYMENT_EXECUTE = 220;

    /**
     * PAYTM
     */
    public static final int PAY_TM_SEND_OTP = 221;
    public static final int PAY_TM_VALIDATE_OTP = 222;
    public static final int PAY_TM_VALIDATE_TOKEN = 223;
    public static final int PAY_TM_BALANCE_CHECKSUM = 224;
    public static final int PAY_TM_WITHDRAW_DETAILS = 225;
    public static final int PAY_TM_ADD_MONEY_CHECKSUM = 226;
    public static final int PAY_TM_TRANSACTION_CHECKSUM = 227;
    public static final int PAY_TM_TRANSACTION_CHECKSUM_STATUS = 228;
    public static final int PAY_TM_WITHDRAW_FROM_WALLET = 229;

    public static final int C_MSTAR_PAYMENT_LIST = 50200;
    public static final int C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL = 50201;
    public static final int C_MSTAR_COMPLETE_ORDER = 50203;
    public static final int C_MSTAR_PAYMENT_LOG = 50204;

    //JUSPAY
    public static final int CREATE_JUS_PAY_CUSTOMER = 238;
    public static final int CREATE_JUS_PAY_ORDER = 212;
    public static final int JUS_PAY_ORDER_STATUS = 213;
    public static final int JUS_PAY_PAYMENT_METHODS = 217;

    public static final int MSTAR_ORDER_COMPLETE_WITH_NOP = 51010;


    private static PaymentServiceManager paymentServiceManager;

    public static PaymentServiceManager getInstance() {
        if (paymentServiceManager == null)
            paymentServiceManager = new PaymentServiceManager();
        return paymentServiceManager;
    }

    void getPaymentList(final PaymentViewModel viewModel, Map<String, String> mStarBasicHeaderMap, String appVersion, String grandTotal, String cartId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getPaymentList(mStarBasicHeaderMap, grandTotal, AppConstant.ANDROID, appVersion, AppConstant.FALSE, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null)
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAYMENT_LIST);
                else
                    viewModel.onSyncData(errorHandling(response.errorBody()), C_MSTAR_PAYMENT_LIST);
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PAYMENT_LIST, null);
            }
        });
    }

    public <T extends AppViewModel> void getPaymentGatewayCredentialDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, final int transactionId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<PaymentKeyResponse> call = apiService.getPaymentGatewayCredentialDetails(mStarBasicHeaderMap);
        call.enqueue(new Callback<PaymentKeyResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentKeyResponse> call, @NonNull Response<PaymentKeyResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else
                    viewModel.onSyncData(errorHandling(response.errorBody()), transactionId);
            }

            @Override
            public void onFailure(@NonNull Call<PaymentKeyResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void createAndUpdateJusPayCustomer(final T viewModel, Map<String, String> mStarBasicHeaderMap, final int transactionId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<JusPayCreateUpdateResponse> call = apiService.createAndUpdateJusPayCustomer(mStarBasicHeaderMap);
        call.enqueue(new Callback<JusPayCreateUpdateResponse>() {
            @Override
            public void onResponse(@NonNull Call<JusPayCreateUpdateResponse> call, @NonNull Response<JusPayCreateUpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null)
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                else
                    viewModel.onFailed(transactionId, null);
            }

            @Override
            public void onFailure(@NonNull Call<JusPayCreateUpdateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void callJusPayAPIForOrderDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody body, String url, final int transactionId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().dynamicClient().create(RetrofitPaymentInterface.class);
        Call<JusPayCreateOrderAndOrderStatusResponse> call = apiService.callJusPayAPIForOrderDetails(mStarBasicHeaderMap, body, ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL).concat(url));
        call.enqueue(new Callback<JusPayCreateOrderAndOrderStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<JusPayCreateOrderAndOrderStatusResponse> call, @NonNull Response<JusPayCreateOrderAndOrderStatusResponse> response) {
                if (response.isSuccessful() && response.body() != null)
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                else
                    viewModel.onSyncData(errorHandling(response.errorBody()), transactionId);
            }

            @Override
            public void onFailure(@NonNull Call<JusPayCreateOrderAndOrderStatusResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }


    public <T extends AppViewModel> void completeOrderDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, String cartId, String paymentMethod, String requestRX, String prescription, String orderType, String paytmCheckSum, String paypalPayId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.completeOrderDetails(mStarBasicHeaderMap, cartId, paymentMethod, requestRX, prescription, orderType, paytmCheckSum, paypalPayId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_COMPLETE_ORDER);
                } else {
                    viewModel.onFailed(C_MSTAR_COMPLETE_ORDER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_COMPLETE_ORDER, null);
            }
        });
    }


    public <T extends AppViewModel> void mStarOrderCompleteWithNOP(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cartId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitPaymentInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarNOPPayment(mStarBasicHeaderMap, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_ORDER_COMPLETE_WITH_NOP);
                } else {
                    viewModel.onFailed(MSTAR_ORDER_COMPLETE_WITH_NOP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_ORDER_COMPLETE_WITH_NOP, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarPaymentLog(final T viewModel, Map<String, String> mStarBasicHeaderMap, TransactionLogRequest transactionLog) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.paymentLog(mStarBasicHeaderMap, transactionLog.getOrderId(), transactionLog.getPaymentMethod(), transactionLog.getRequest(), transactionLog.getResponse(), transactionLog.getAppVersion(), transactionLog.getSourceName());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAYMENT_LOG);
                } else {
                    if (response.errorBody() != null) {
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class)), C_MSTAR_PAYMENT_LOG);
                    } else {
                        viewModel.onFailed(C_MSTAR_PAYMENT_LOG, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PAYMENT_LOG, null);
            }
        });
    }

    <T extends AppViewModel> void netBankingList(final T viewModel, String url) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().dynamicClient().create(RetrofitPaymentInterface.class);
        Call<NetBankingListResponse> call = apiService.netBankingList(url);
        call.enqueue(new Callback<NetBankingListResponse>() {
            @Override
            public void onResponse(@NonNull Call<NetBankingListResponse> call, @NonNull Response<NetBankingListResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), JUS_PAY_PAYMENT_METHODS);
                } else {
                    viewModel.onFailed(JUS_PAY_PAYMENT_METHODS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<NetBankingListResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(JUS_PAY_PAYMENT_METHODS, null);
            }
        });
    }

    public <T extends AppViewModel> void getPayPalToken(final T viewModel, String headers, String url) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().dynamicService(headers).create(RetrofitPaymentInterface.class);
        Call<PayPalTokenResponse> call = apiService.getPayPalToken(PaymentConstants.CLIENT_CREDENTIALS, url);
        call.enqueue(new Callback<PayPalTokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayPalTokenResponse> call, @NonNull Response<PayPalTokenResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_PAL_TOKEN);
                } else {
                    viewModel.onFailed(PAY_PAL_TOKEN, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayPalTokenResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_PAL_TOKEN, null);
            }
        });
    }

    public <T extends AppViewModel> void initPayPalPayment(final T viewModel, String headers, PaypalPaymentRequest response, String url) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().dynamicService(headers).create(RetrofitPaymentInterface.class);
        Call<PaypalPaymentResponse> call = apiService.initPayPalPayment(response, url);
        call.enqueue(new Callback<PaypalPaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaypalPaymentResponse> call, @NonNull Response<PaypalPaymentResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), INIT_PAY_PAL_PAYMENT);
                } else {
                    viewModel.onFailed(INIT_PAY_PAL_PAYMENT, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaypalPaymentResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(INIT_PAY_PAL_PAYMENT, null);
            }
        });
    }

    public <T extends AppViewModel> void payPalPaymentExecute(final T viewModel, String headers, String url, PaypalExecutePaymentRequest request) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().dynamicService(headers).create(RetrofitPaymentInterface.class);
        Call<PaypalPaymentResponse> call = apiService.payPalPaymentExecute(url, request);
        call.enqueue(new Callback<PaypalPaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaypalPaymentResponse> call, @NonNull Response<PaypalPaymentResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_PAL_PAYMENT_EXECUTE);
                } else {
                    viewModel.onFailed(PAY_PAL_PAYMENT_EXECUTE, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaypalPaymentResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_PAL_PAYMENT_EXECUTE, null);
            }
        });
    }

    public <T extends AppViewModel> void sentPayTmOtp(final T viewModel, PaytmLinkWalletOtpRequest request) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.PAY_TM_ACCOUNT_BASE_URL)).create(RetrofitPaymentInterface.class);
        Call<PaytmLinkWalletOtpResponse> call = apiService.sendPayTmOtp(request);
        call.enqueue(new Callback<PaytmLinkWalletOtpResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaytmLinkWalletOtpResponse> call, @NonNull Response<PaytmLinkWalletOtpResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_SEND_OTP);
                } else {
                    viewModel.onFailed(PAY_TM_SEND_OTP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaytmLinkWalletOtpResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_SEND_OTP, null);
            }
        });
    }

    public <T extends AppViewModel> void verifyPayTmOtp(final T viewModel, PaytmValidateOtpRequest request, String header) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.PAY_TM_ACCOUNT_BASE_URL), header).create(RetrofitPaymentInterface.class);
        Call<PaytmValidateOtpResponse> call = apiService.validatePayTmOtp(request);
        call.enqueue(new Callback<PaytmValidateOtpResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaytmValidateOtpResponse> call, @NonNull Response<PaytmValidateOtpResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_VALIDATE_OTP);
                } else {
                    viewModel.onFailed(PAY_TM_VALIDATE_OTP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaytmValidateOtpResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_VALIDATE_OTP, null);
            }
        });
    }

    public <T extends AppViewModel> void validatePayTmToken(final T viewModel, String token) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.PAY_TM_ACCOUNT_BASE_URL)).create(RetrofitPaymentInterface.class);
        Call<PaytmValidateTokenResponse> call = apiService.validatePayTmToken(token);
        call.enqueue(new Callback<PaytmValidateTokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaytmValidateTokenResponse> call, @NonNull Response<PaytmValidateTokenResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_VALIDATE_TOKEN);
                } else {
                    viewModel.onFailed(PAY_TM_VALIDATE_TOKEN, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaytmValidateTokenResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_VALIDATE_TOKEN, null);
            }
        });
    }

    public <T extends AppViewModel> void payTmBalanceChecksum(final T viewModel, Map<String, String> mStarBasicHeaderMap, String accessToken, String totalAmount) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<PayTmChecksumResponse> call = apiService.payTmBalanceChecksum(mStarBasicHeaderMap, accessToken, totalAmount);
        call.enqueue(new Callback<PayTmChecksumResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayTmChecksumResponse> call, @NonNull Response<PayTmChecksumResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_BALANCE_CHECKSUM);
                } else {
                    viewModel.onFailed(PAY_TM_BALANCE_CHECKSUM, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayTmChecksumResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_BALANCE_CHECKSUM, null);
            }
        });
    }

    public <T extends AppViewModel> void payTmWithdrawFromWallet(final T viewModel, Map<String, String> mStarBasicHeaderMap, Map<String, String> param, boolean p_flag, String orderId, String txnAmount) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<PayTmChecksumResponse> call = apiService.payTmWithdrawFromWallet(mStarBasicHeaderMap, param.get(PaymentConstants.API_APPIP), param.get(PaymentConstants.API_CUSTOMERID),
                param.get(AppConstant.CARTID), param.get(PaymentConstants.API_SSOTOKEN), p_flag, orderId, txnAmount);
        call.enqueue(new Callback<PayTmChecksumResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayTmChecksumResponse> call, @NonNull Response<PayTmChecksumResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_WITHDRAW_FROM_WALLET);
                } else {
                    viewModel.onFailed(PAY_TM_WITHDRAW_FROM_WALLET, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayTmChecksumResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_WITHDRAW_FROM_WALLET, null);
            }
        });
    }

    public <T extends AppViewModel> void payTmWithdrawDetails(final T viewModel, PaytmWithdrawRequest request) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.PAY_TM_BASE_URL)).create(RetrofitPaymentInterface.class);
        Call<PayTmWithdrawResponse> call = apiService.payTmWithdrawDetails(request);
        call.enqueue(new Callback<PayTmWithdrawResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayTmWithdrawResponse> call, @NonNull Response<PayTmWithdrawResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_WITHDRAW_DETAILS);
                } else {
                    viewModel.onFailed(PAY_TM_WITHDRAW_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayTmWithdrawResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_WITHDRAW_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void payTmAddMoneyChecksum(final T viewModel, Map<String, String> mStarBasicHeaderMap, String cartId, String sosToken,
                                                               String cusId, String addingAmount, boolean p_flag, String orderId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<PayTmChecksumResponse> call = apiService.payTmAddMoneyChecksum(mStarBasicHeaderMap, cartId, sosToken, cusId, addingAmount, p_flag, orderId);
        call.enqueue(new Callback<PayTmChecksumResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayTmChecksumResponse> call, @NonNull Response<PayTmChecksumResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_ADD_MONEY_CHECKSUM);
                } else {
                    viewModel.onFailed(PAY_TM_ADD_MONEY_CHECKSUM, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayTmChecksumResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_ADD_MONEY_CHECKSUM, null);
            }
        });
    }

    public <T extends AppViewModel> void payTmTransactionChecksumStatus(final T viewModel, String query) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.PAY_TM_BASE_URL)).create(RetrofitPaymentInterface.class);
        Call<PayTmTransactionStatusResponse> call = apiService.payTmTransactionCheckSumStatus(PaymentApiConstant.PAY_TM_TRANSACTION_STATUS + "?JsonData=" + query);
        call.enqueue(new Callback<PayTmTransactionStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayTmTransactionStatusResponse> call, @NonNull Response<PayTmTransactionStatusResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_TRANSACTION_CHECKSUM_STATUS);
                } else {
                    viewModel.onFailed(PAY_TM_TRANSACTION_CHECKSUM_STATUS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayTmTransactionStatusResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_TRANSACTION_CHECKSUM_STATUS, null);
            }
        });
    }

    public <T extends AppViewModel> void payTmTransactionChecksum(final T viewModel, Map<String, String> mStarBasicHeaderMap, String cartId, boolean p_flag, String orderId) {
        RetrofitPaymentInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitPaymentInterface.class);
        Call<PayTmChecksumResponse> call = apiService.payTmTransactionChecksum(mStarBasicHeaderMap, cartId, p_flag, orderId);
        call.enqueue(new Callback<PayTmChecksumResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayTmChecksumResponse> call, @NonNull Response<PayTmChecksumResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAY_TM_TRANSACTION_CHECKSUM);
                } else {
                    viewModel.onFailed(PAY_TM_TRANSACTION_CHECKSUM, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PayTmChecksumResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_TRANSACTION_CHECKSUM, null);
            }
        });
    }
}