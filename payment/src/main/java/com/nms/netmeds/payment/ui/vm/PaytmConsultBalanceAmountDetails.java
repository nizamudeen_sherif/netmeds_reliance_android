package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaytmConsultBalanceAmountDetails {
    @SerializedName("walletAmount")
    private Double walletAmount;

    public Double getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(Double walletAmount) {
        this.walletAmount = walletAmount;
    }

}