/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaypalPaymentRequestTransaction {
    @SerializedName("amount")
    private PaypalPaymentRequestAmount amount;
    @SerializedName("description")
    private String description;
    @SerializedName("custom")
    private String custom;
    @SerializedName("invoice_number")
    private String invoiceNumber;
    @SerializedName("payment_options")
    private PaypalPaymentRequestPaymentOptions paymentOptions;
    @SerializedName("soft_descriptor")
    private String softDescriptor;
    @SerializedName("item_list")
    private PaypalPaymentRequestItemList itemList;

    public PaypalPaymentRequestAmount getAmount() {
        return amount;
    }

    public void setAmount(PaypalPaymentRequestAmount amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public PaypalPaymentRequestPaymentOptions getPaymentOptions() {
        return paymentOptions;
    }

    public void setPaymentOptions(PaypalPaymentRequestPaymentOptions paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    public String getSoftDescriptor() {
        return softDescriptor;
    }

    public void setSoftDescriptor(String softDescriptor) {
        this.softDescriptor = softDescriptor;
    }

    public PaypalPaymentRequestItemList getItemList() {
        return itemList;
    }

    public void setItemList(PaypalPaymentRequestItemList itemList) {
        this.itemList = itemList;
    }

}