package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayCreateUpdateResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private JusPayCreateUpdateResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JusPayCreateUpdateResult getResult() {
        return result;
    }

    public void setResult(JusPayCreateUpdateResult result) {
        this.result = result;
    }
}
