package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BillingAddress {
    @SerializedName("region")
    private String region;
    @SerializedName("region_id")
    private String regionId;
    @SerializedName("region_code")
    private String regionCode;
    @SerializedName("country_id")
    private String countryId;
    @SerializedName("street")
    private ArrayList<String> streetList = null;
    @SerializedName("telephone")
    private String telephone;
    @SerializedName("postcode")
    private String postCode;
    @SerializedName("city")
    private String city;
    @SerializedName("firstname")
    private String firstName;
    @SerializedName("lastname")
    private String lastName;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("email")
    private String email;
    @SerializedName("same_as_billing")
    private String sameAsBilling;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public ArrayList<String> getStreetList() {
        return streetList;
    }

    public void setStreetList(ArrayList<String> streetList) {
        this.streetList = streetList;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSameAsBilling() {
        return sameAsBilling;
    }

    public void setSameAsBilling(String sameAsBilling) {
        this.sameAsBilling = sameAsBilling;
    }
}
