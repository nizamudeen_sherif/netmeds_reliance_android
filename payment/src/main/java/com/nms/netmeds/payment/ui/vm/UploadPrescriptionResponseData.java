package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class UploadPrescriptionResponseData {

    @SerializedName("status")
    private String status;
    @SerializedName("object_id")
    private String objectId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
