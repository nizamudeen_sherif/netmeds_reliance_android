package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class PaytmConsultBalanceResponseBody {
    @SerializedName("resultInfo")
    private PaytmConsultBalanceResultInfo resultInfo;
    @SerializedName("fundsSufficient")
    private boolean fundsSufficient;
    @SerializedName("addMoneyAllowed")
    private Boolean addMoneyAllowed;
    @SerializedName("deficitAmount")
    private Double deficitAmount;
    @SerializedName("amountDetails")
    private PaytmConsultBalanceAmountDetails amountDetails;

    public PaytmConsultBalanceResultInfo getResultInfo() {
        return resultInfo;
    }

    public void setResultInfo(PaytmConsultBalanceResultInfo resultInfo) {
        this.resultInfo = resultInfo;
    }

    public boolean getFundsSufficient() {
        return fundsSufficient;
    }

    public void setFundsSufficient(boolean fundsSufficient) {
        this.fundsSufficient = fundsSufficient;
    }

    public Boolean getAddMoneyAllowed() {
        return addMoneyAllowed;
    }

    public void setAddMoneyAllowed(Boolean addMoneyAllowed) {
        this.addMoneyAllowed = addMoneyAllowed;
    }

    public PaytmConsultBalanceAmountDetails getAmountDetails() {
        return amountDetails;
    }

    public void setAmountDetails(PaytmConsultBalanceAmountDetails amountDetails) {
        this.amountDetails = amountDetails;
    }

    public Double getDeficitAmount() {
        return deficitAmount;
    }

    public void setDeficitAmount(Double deficitAmount) {
        this.deficitAmount = deficitAmount;
    }
}
