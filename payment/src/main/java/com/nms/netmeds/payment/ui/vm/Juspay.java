package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class Juspay {

    @SerializedName("client_auth_token")
    private String clientAuthToken;
    @SerializedName("client_auth_token_expiry")
    private String clientAuthTokenExpiry;

    public String getClientAuthToken() {
        return clientAuthToken;
    }

    public void setClientAuthToken(String clientAuthToken) {
        this.clientAuthToken = clientAuthToken;
    }

    public String getClientAuthTokenExpiry() {
        return clientAuthTokenExpiry;
    }

    public void setClientAuthTokenExpiry(String clientAuthTokenExpiry) {
        this.clientAuthTokenExpiry = clientAuthTokenExpiry;
    }
}
