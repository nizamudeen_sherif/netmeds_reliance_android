package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.AppliedVoucher;
import com.nms.netmeds.base.model.ConsultationEvent;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.PaymentGatewayList;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.ApplyCoupon;
import com.nms.netmeds.consultation.model.ApplyCouponResponse;
import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.model.ConsultationUpdateResponse;
import com.nms.netmeds.consultation.model.GetAllCouponResponse;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.model.PaymentCreationResponse;
import com.nms.netmeds.consultation.model.ReoccurUpdateResponse;
import com.nms.netmeds.consultation.model.Speciality;
import com.nms.netmeds.consultation.model.request.ApplyCouponRequest;
import com.nms.netmeds.consultation.model.request.ConsultationUpdatePayload;
import com.nms.netmeds.consultation.model.request.ConsultationUpdateRequest;
import com.nms.netmeds.consultation.model.request.Payment;
import com.nms.netmeds.consultation.model.request.PaymentCreationRequest;
import com.nms.netmeds.consultation.model.request.ReoccurPayload;
import com.nms.netmeds.consultation.model.request.ReoccurUpdateRequest;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderCreateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticOrderDetail;
import com.nms.netmeds.diagnostic.model.DiagnosticPaymentCreateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticPaymentGatewayResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticUpdateResponse;
import com.nms.netmeds.diagnostic.model.DiagnosticUpdateResult;
import com.nms.netmeds.diagnostic.model.DiagnosticUserDetail;
import com.nms.netmeds.diagnostic.model.LabAddress;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticOrderCreateRequest;
import com.nms.netmeds.diagnostic.model.Request.DiagnosticPaymentCreateRequest;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.payment.NetmedsPaymentProcess;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityPaymentBinding;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentKeyResponse;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.nms.netmeds.payment.ui.vm.JusPayCreateOrderAndOrderStatusResponse;
import com.nms.netmeds.payment.ui.vm.JusPayCreateUpdateResponse;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;
import com.tune.TuneEventItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;

import static com.nms.netmeds.payment.ui.PaymentServiceManager.C_MSTAR_PAYMENT_LIST;


public class PaymentViewModel extends AppViewModel implements MessageAdapterListener, NetmedsPaymentProcess.NetmedsPaymentProcessCallback {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    public MStarCartDetails cartDetails;
    private WebEngageModel webEngageModel;
    private BasePreference mBasePreference;
    private ActivityPaymentBinding mBinding;
    private PaymentInterface mPaymentInterface;
    public NetmedsPaymentProcess netmedsPaymentProcess;
    private MStarBasicResponseTemplateModel paymentGatewayResponse;

    private List<PaymentGatewayList> sortedPaymentList;
    private List<MStarProductDetails> webEngageProductDetails;

    private String cartId = "";
    private double amazonBalance = 0.0;
    private double nmsCash = 0.0;
    private double applingNMSCash = 0.0;
    private double nmsSuperCash = 0.0;
    private boolean amazonLinkStatus = false;
    private boolean fromPaymentFailure = false;
    private int failedTransactionId = 0;
    private final int PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID = 900001;
    private final int SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS = 900002;
    private final int CALL_WALLET_BALANCE_AND_CARD_DETAILS = 900004;
    private final int GET_CART_DETAILS_AFTER_CALL_PAYMENT_LIST = 900005;
    private final int PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY = 900006;

    /**
     * JusPay Credentials
     */
    private String juspayCreateCustomerId = "";

    private final List<String> staticBankList = new ArrayList<>();

    private boolean isFromPayment = false;
    private final MutableLiveData<MStarBasicResponseTemplateModel> paymentGatewayResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ApplyCouponResponse> applyCouponMutableLiveData = new MutableLiveData<>();


    private List<AppliedVoucher> appliedVoucherResponse = new ArrayList<>();
    private boolean formVoucherRemoval = false;

    //Consultation params
    private List<ConsultationCoupon> consultationCouponList = new ArrayList<>();
    private DateTimeUtils dateTimeUtils;
    private String consultationSpecialization = "";
    private String consultationRetryGatewayName = "";
    private String consultationRetryPaymentStatus = "";


    /*Prime*/
    private boolean isPrimeProduct = false;
    private boolean isNonPrimeProduct = false;

    private boolean isPayTmUpiActive = false;
    private boolean isGooglePayActive = false;
    private boolean isPhonePeActive = false;

    private PaymentGatewaySubCategory cODPaymentDetails;

    public void setWebEngageModel(WebEngageModel webEngageModel) {
        this.webEngageModel = webEngageModel;
    }

    /*Diagnostic related types*/
    private AvailableLab selectedLab;
    private PatientDetail patientDetail;
    private SlotTime slotTime;
    private MStarAddressModel diagnosticAddress;
    private String diagnosticUserId;

    private Bundle diagnosticOrderCreationDetail;
    private String diagnosticPaymentId;
    private String diagnosticUpdatePaymentType = "";
    private String diagnosticOrderCreateDate = "";

    public String getDiagnosticOrderCreateDate() {
        return diagnosticOrderCreateDate;
    }

    public void setDiagnosticOrderCreateDate(String diagnosticOrderCreateDate) {
        this.diagnosticOrderCreateDate = diagnosticOrderCreateDate;
    }

    public PaymentViewModel(@NonNull Application application) {
        super(application);
        this.netmedsPaymentProcess = new NetmedsPaymentProcess(application);
    }

    MutableLiveData<MStarBasicResponseTemplateModel> getPaymentGatewayResponseMutableLiveData() {
        return paymentGatewayResponseMutableLiveData;
    }

    private MStarBasicResponseTemplateModel getPaymentGatewayResponse() {
        return paymentGatewayResponse;
    }

    private void setPaymentGatewayResponse(MStarBasicResponseTemplateModel paymentGatewayResponse) {
        this.paymentGatewayResponse = paymentGatewayResponse;
    }

    private List<AppliedVoucher> getAppliedVoucherResponse() {
        return appliedVoucherResponse;
    }

    private void setAppliedVoucherResponse(List<AppliedVoucher> appliedVoucherResponse) {
        this.appliedVoucherResponse = appliedVoucherResponse;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getNmsWalletBalanceForDiagnosticsMutableLiveData() {
        return nmsWalletBalanceForDiagnosticsMutableLiveData;
    }

    private final MutableLiveData<MStarBasicResponseTemplateModel> nmsWalletBalanceForDiagnosticsMutableLiveData = new MutableLiveData<>();

    public void initViewModel(Context context, ActivityPaymentBinding binding, BasePreference basePreference,
                              PaymentInterface paymentInterface, boolean isFromPayment, boolean fromPaymentFailure, boolean isM2Order) {

        this.context = context;
        this.mBinding = binding;
        this.mBasePreference = basePreference;
        this.mPaymentInterface = paymentInterface;

        netmedsPaymentProcess.initiateProperties(this, this, context, basePreference, paymentInterface, isM2Order, isFromPayment);
        getInstalledUpiApps(context);
        setFromPayment(isFromPayment);
        setFromPaymentFailure(fromPaymentFailure);
        netmedsPaymentProcess.setFromConsultation(!isFromPayment() && !netmedsPaymentProcess.isFromDiagnostic());
        init();
        PaymentHelper.setReloadTotalInformation(false);
        checkPrimeProduct();
    }

    private void init() {
        dateTimeUtils = DateTimeUtils.getInstance();
        initiateFirstCall();
        if (isFromPayment()) {
            setFormVoucherRemoval(false);
        }
        if (netmedsPaymentProcess.isFromDiagnostic()) {
            initNmsWalletForDiagnosis();
        }
    }

    private void initiateFirstCall() {
        mPaymentInterface.vmShowProgress();
        mPaymentInterface.setAmazonPayBalanceLinkStatus();
    }

    public void initiateCalls() {
        if (PaymentHelper.isIsJusPayCustomerCreated()) {
            if (netmedsPaymentProcess.isFromConsultation()) {
                callPaymentListAPIAndUpdateTotalAmount();
            } else {
                initiateAPICall(PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID);
            }
        } else {
            if (!TextUtils.isEmpty(mBasePreference.getPaymentCredentials())) {
                initiateAPICall(PaymentServiceManager.CREATE_JUS_PAY_CUSTOMER);
            } else {
                initiateAPICall(PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY);
            }
        }
    }

    public void checkValidationAndInitiatePlaceOrder(PaymentGatewaySubCategory selectedPaymentDetails, String cvv) {
        if (selectedPaymentDetails != null && PaymentConstants.CREDIT_DEBIT_LIST == selectedPaymentDetails.getParentId()) {
            if (PaymentUtils.cvvValidation(cvv)) {
                selectedPaymentDetails.setCardCvv(cvv);
                netmedsPaymentProcess.setPaymentGatewaySubCategory(selectedPaymentDetails);
                placeOrder();
            } else {
                mPaymentInterface.setError(context.getString(R.string.invalid_cvv));
            }
        } else {
            placeOrder();
        }
    }

    public void placeOrder() {
        proceedPlaceOrderProcess();
    }

    public void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case PaymentServiceManager.CREATE_JUS_PAY_CUSTOMER:
                    PaymentServiceManager.getInstance().createAndUpdateJusPayCustomer(this, mBasePreference.getMstarBasicHeaderMap(), transactionId);
                    break;
                case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                    APIServiceManager.getInstance().mstarUpdateCustomer(this, mBasePreference.getMstarBasicHeaderMap(), getUpdateTokenInCustomerRequest());
                    break;
                case PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID:
                case SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS:
                case GET_CART_DETAILS_AFTER_CALL_PAYMENT_LIST:
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    APIServiceManager.getInstance().mStarGetCartDetails(this, mBasePreference.getMstarBasicHeaderMap(), netmedsPaymentProcess.getCurrentCartIdForAPICall(), transactionId);
                    break;
                case C_MSTAR_PAYMENT_LIST:
                    mPaymentInterface.vmShowProgress();
                    if (netmedsPaymentProcess.isFromDiagnostic())
                        DiagnosticServiceManager.getInstance().getDiagnosisPaymentList(this, mBasePreference.getMStarSessionId(),
                                getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO), labPayMode(), getcId(), getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO) ? getPid() : getLabId(), mBasePreference,
                                CommonUtils.getAmountWithTwoDecimalFormat(netmedsPaymentProcess.getDiagnosticTotalAmount()), BasePreference.getInstance(context).getMstarCustomerId(), CommonUtils.getAppVersionCode(context), getTestIdList());
                    else if (netmedsPaymentProcess.isFromConsultation())
                        ConsultationServiceManager.getInstance().getConsultationPaymentList(this, mBasePreference.getMStarSessionId(), mBasePreference,
                                CommonUtils.getAmountWithTwoDecimalFormat(netmedsPaymentProcess.getConsultationTotalAmount()), BasePreference.getInstance(context).getMstarCustomerId(), CommonUtils.getAppVersionCode(context));
                    else
                        PaymentServiceManager.getInstance().getPaymentList(this, mBasePreference.getMstarBasicHeaderMap(), CommonUtils.getAppVersionCode(context), CommonUtils.getAmountWithTwoDecimalFormat(netmedsPaymentProcess.getGrandTotal()), getCartId());
                    break;
                case PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL:
                case PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY:
                    mPaymentInterface.vmShowLoader();
                    PaymentServiceManager.getInstance().getPaymentGatewayCredentialDetails(this, mBasePreference.getMstarBasicHeaderMap(), transactionId);
                    break;
                case APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE:
                case CALL_WALLET_BALANCE_AND_CARD_DETAILS:
                    APIServiceManager.getInstance().getMStarPaymentWalletBalance(this, mBasePreference.getMstarBasicHeaderMap(), netmedsPaymentProcess.getCurrentCartIdForAPICall(), transactionId);
                    break;
                case APIServiceManager.MSTAR_APPLY_WALLET:
                case APIServiceManager.MSTAR_UN_APPLY_WALLET:
                    mPaymentInterface.vmShowProgress();
                    APIServiceManager.getInstance().mStarApplyAndUnApplyWalletAndSuperCash(this, mBasePreference.getMstarBasicHeaderMap(),
                            APIServiceManager.MSTAR_APPLY_WALLET == transactionId ? AppConstant.YES : AppConstant.NO, null, netmedsPaymentProcess.getCurrentCartIdForAPICall(), transactionId);
                    break;
                case APIServiceManager.MSTAR_UN_APPLY_PROMO_CODE:
                    APIServiceManager.getInstance().mStarUnApplyPromoCode(this, mBasePreference.getMstarBasicHeaderMap(), "", netmedsPaymentProcess.getCurrentCartIdForAPICall());
                    break;
                case DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION:
                    mPaymentInterface.vmShowLoader();
                    DiagnosticServiceManager.getInstance().diagnosticOrderCreation(this, getDiagnosticOrderCreate(true), mBasePreference);
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                initiateAPICall(!isFromPayment() ? C_MSTAR_PAYMENT_LIST : PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID);
                break;
            case PaymentServiceManager.CREATE_JUS_PAY_CUSTOMER:
                createJusPayCustomerResponse(data);
                break;
            case PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID:
            case SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS:
            case GET_CART_DETAILS_AFTER_CALL_PAYMENT_LIST:
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                cartDetailsResponse(data, transactionId);
                break;
            case C_MSTAR_PAYMENT_LIST:
                paymentListResponse(data);
                break;
            case PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL:
            case PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY:
                paymentGatewayResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_APPLY_WALLET:
            case APIServiceManager.MSTAR_UN_APPLY_WALLET:
                applyAndUnApplyWalletResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_UN_APPLY_PROMO_CODE:
                unApplyPromoCodeResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE:
            case CALL_WALLET_BALANCE_AND_CARD_DETAILS:
                walletBalanceResponse(data, transactionId);
                break;


            case DiagnosticServiceManager.NMS_WALLET_BALANCE:
                MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                if (response != null && response.getResult() != null && response.getResult().getHideNMS() && netmedsPaymentProcess.isFromDiagnostic())
                    return;
                nmsWalletBalanceForDiagnosticsMutableLiveData.setValue(response);
                if (response != null && response.getResult() != null && response.getResult().getUseableBalanceBreakupList() != null
                        && response.getResult().getUseableBalanceBreakupList().size() > 0) {
                    PaymentHelper.setCbBreakUp(new Gson().toJson(response.getResult().getUseableBalanceBreakupList()));
                }
                break;
            case ConsultationServiceManager.GET_ALL_COUPON:
                consultationCouponResponse(data);
                break;
            case ConsultationServiceManager.PAYMENT_CREATION:
                consultationPaymentCreationResponse(data);
                break;
            case ConsultationServiceManager.UPDATE_CONSULTATION:
                updateConsultationResponse(data);
                break;
            case ConsultationServiceManager.REOCCUR_UPDATE:
                updateReoccurResponse(data);
                break;
            case ConsultationServiceManager.APPLY_COUPON:
                ApplyCouponResponse applyCouponResponse = new Gson().fromJson(data, ApplyCouponResponse.class);
                applyCouponMutableLiveData.setValue(applyCouponResponse);
                break;
            case APIServiceManager.M3_SUBSCRIPTION_LOG:
                vmDismissLoader();
                mPaymentInterface.vmOrderSuccessActivity(netmedsPaymentProcess.getCompleteOrderResponse());
                break;

            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION:
                diagnosticOrderCreationResponse(data);
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION:
                diagnosticPaymentCreationResponse(data);
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE:
                diagnosticUpdateResponse(data);
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE:
                diagnosticOrderUpdateResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        mPaymentInterface.vmDismissProgress();
        failedTransactionId = transactionId;
        switch (transactionId) {
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                initiateAPICall(PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID);
                break;
            case PaymentServiceManager.CREATE_JUS_PAY_CUSTOMER:
            case PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID:
            case SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS:
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
            case PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL:
            case APIServiceManager.MSTAR_APPLY_WALLET:
            case PaymentServiceManager.C_MSTAR_PAYMENT_LIST:
            case APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE:
            case APIServiceManager.MSTAR_UN_APPLY_WALLET:
            case APIServiceManager.MSTAR_INITIATE_CHECKOUT:


            case ConsultationServiceManager.GET_ALL_COUPON:
            case ConsultationServiceManager.PAYMENT_CREATION:
            case ConsultationServiceManager.PAYMENT_UPDATE:
            case ConsultationServiceManager.REOCCUR_UPDATE:
            case ConsultationServiceManager.UPDATE_COUPON:

            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION:
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION:
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE:
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_FAILURE_UPDATE:
            case PaymentServiceManager.DELETE_COUPON:
                showApiError(transactionId);
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE: {
                showApiError(transactionId);
                sendDiagnosticPurchaseEvent();
                break;
            }

            case ConsultationServiceManager.APPLY_COUPON:
                mPaymentInterface.setError(getApplication().getResources().getString(R.string.text_promo_code_failure));
                netmedsPaymentProcess.setConsultationCouponListOpen(false);
                break;
        }
    }

    void nmsWalletBalanceForDiagnosticsResponse(MStarBasicResponseTemplateModel nmsWalletResponse) {
        mBinding.nmsCashCheckBox.setChecked(false);
        mBinding.cvWalletView.setVisibility(View.GONE);
        if (nmsWalletResponse != null && nmsWalletResponse.getResult() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(nmsWalletResponse.getStatus()) &&
                nmsWalletResponse.getResult().getWalletBalance().compareTo(BigDecimal.ZERO) > 0) {
            mBinding.cvWalletView.setVisibility(View.VISIBLE);
            if (nmsWalletResponse.getResult().getUseableBalanceBreakupList() != null &&
                    nmsWalletResponse.getResult().getUseableBalanceBreakupList().size() > 0)
                PaymentHelper.setCbBreakUp(new Gson().toJson(nmsWalletResponse.getResult().getUseableBalanceBreakupList()));
            mBinding.nmsCashAmount.setText(String.format("%s ₹%s", getApplication().getResources().getString(R.string.balance), String.valueOf(nmsWalletResponse.getResult().getWalletBalance())));
            mBinding.redeemedAmount.setVisibility(View.GONE);
            setNmsCash(nmsWalletResponse.getResult().getWalletBalance().doubleValue());
            setNmsSuperCash(nmsWalletResponse.getResult().getCashbackUseableBalance().doubleValue());
        }
        mPaymentInterface.vmDismissProgress();
        if (isFromPaymentFailure() && (PaymentHelper.isNMSCashApplied() || PaymentHelper.isNMSSuperCashApplied())) {
            walletTransaction();
        } else if (netmedsPaymentProcess.isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied()) {
            mBinding.nmsCashCheckBox.setChecked(true);
            setNMSCashViewDiagnosis(DiagnosticHelper.getAppliedNMSCash());
        } else if (PaymentHelper.isNMSCashApplied()) {
            mBinding.nmsCashCheckBox.setChecked(true);
            setNMSCashView(PaymentHelper.getAppliedNMSCash());
        }
    }


    /********************API Request**********************/


    private MstarUpdateCustomerRequest getUpdateTokenInCustomerRequest() {
        MStarCustomerDetails customerDetails = new Gson().fromJson(mBasePreference.getCustomerDetails(), MStarCustomerDetails.class);
        MstarUpdateCustomerRequest mstarUpdateCustomerRequest = new MstarUpdateCustomerRequest();
        mstarUpdateCustomerRequest.setFirstName(customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "");
        mstarUpdateCustomerRequest.setLastName(customerDetails != null && !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "");
        mstarUpdateCustomerRequest.setEmail(customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "");
        mstarUpdateCustomerRequest.setJuspayId(juspayCreateCustomerId);
        return mstarUpdateCustomerRequest;
    }


    /********************API Response**********************/
    private void createJusPayCustomerResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            JusPayCreateUpdateResponse jusPayCreateUpdateResponse = new Gson().fromJson(data, JusPayCreateUpdateResponse.class);
            if (jusPayCreateUpdateResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(jusPayCreateUpdateResponse.getStatus())) {
                if (!TextUtils.isEmpty(jusPayCreateUpdateResponse.getResult().getId())) {
                    juspayCreateCustomerId = jusPayCreateUpdateResponse.getResult().getId();
                    PaymentHelper.setIsJusPayCustomerCreated(true);
                    initiateAPICall(APIServiceManager.MSTAR_UPDATE_CUSTOMER);
                } else {
                    PaymentHelper.setIsJusPayCustomerCreated(true);
                    nextStepAfterCreateJusPayCustomerCall();
                }
            } else {
                nextStepAfterCreateJusPayCustomerCall();
            }
        } else {
            nextStepAfterCreateJusPayCustomerCall();
        }
    }

    private void nextStepAfterCreateJusPayCustomerCall() {
        if (netmedsPaymentProcess.isFromDiagnostic() || netmedsPaymentProcess.isFromConsultation()) {
            callPaymentListAPIAndUpdateTotalAmount();
        } else {
            initiateAPICall(PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID);
        }
    }

    private void callPaymentListAPIAndUpdateTotalAmount() {
        if (netmedsPaymentProcess.isFromDiagnostic()) {
            netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getDiagnosticTotalAmount());
        } else if (netmedsPaymentProcess.isFromConsultation()) {
            netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getConsultationTotalAmount());
        }
        initiateAPICall(C_MSTAR_PAYMENT_LIST);
        mPaymentInterface.setNetPayableAmount(netmedsPaymentProcess.getGrandTotal());
    }

    private void cartDetailsResponse(String data, int transactionId) {
        mBinding.setViewModel(this);
        if (netmedsPaymentProcess.isFromDiagnostic()) {
            netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getDiagnosticTotalAmount());
        } else if (netmedsPaymentProcess.isFromConsultation()) {
            netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getConsultationTotalAmount());
        }
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel cartDetailsModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (cartDetailsModel != null && cartDetailsModel.getResult() != null && cartDetailsModel.getResult().getCartDetails() != null) {
                cartDetails = cartDetailsModel.getResult().getCartDetails();
                PaymentHelper.setCartLineItems(cartDetails.getLines());
                checkPrimeProduct();
                setCartId(String.valueOf(cartDetails.getId()));
                netmedsPaymentProcess.setCartDetails(cartDetails);
                /**
                 * "SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS" is using only for showing payment details while clicking view all button in amount section.
                 * Thats y here after response we just showing the hidden views in payment section
                 *
                 * "PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID" is using for before calling payment list API we need to add total amount in that request. For this purpose
                 * after CART DETAILS API call immediately i call payment List API.
                 */
                if (SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS != transactionId) {
                    PaymentHelper.setTotalAmount(cartDetails.getNetPayableAmount().doubleValue());
                    netmedsPaymentProcess.setGrandTotal(isFromPayment() ? cartDetails.getNetPayableAmount().doubleValue() : netmedsPaymentProcess.isFromConsultation() ? netmedsPaymentProcess.getConsultationTotalAmount() : netmedsPaymentProcess.getDiagnosticTotalAmount());
                    disEnablePaymentList(!(netmedsPaymentProcess.getGrandTotal() <= 0), mBinding.paymentList);
                    if (PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID == transactionId || GET_CART_DETAILS_AFTER_CALL_PAYMENT_LIST == transactionId) {
                        initiateAPICall(C_MSTAR_PAYMENT_LIST);
                    } else {
                        if (isFormVoucherRemoval())
                            setNmsCashValueOnVoucherRemoval();
                        initiateAppliedVoucherView();
                        initiateAppliedWalletView(transactionId);
                        mPaymentInterface.vmDismissProgress();
                    }
                } else {
                    initiatePaymentDetails(cartDetails);
                }
            }
        }
        mPaymentInterface.setNetPayableAmount(netmedsPaymentProcess.getGrandTotal());
    }

    private void initiateAppliedWalletView(int transactionId) {
        PaymentHelper.setIsNMSCashApplied((cartDetails != null && cartDetails.getUsedWalletAmount() != null && cartDetails.getUsedWalletAmount().getWalletCash().compareTo(BigDecimal.ZERO) > 0));
        if (PaymentHelper.isNMSCashApplied()) {
            PaymentHelper.setAppliedNMSCash(cartDetails.getUsedWalletAmount().getWalletCash().doubleValue());
            if (PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID != transactionId) {
                mBinding.nmsCashCheckBox.setChecked(true);
                setNMSCashView(PaymentHelper.getAppliedNMSCash());
            }
        }
        boolean isVoucherAppliedAndWalletRemovedAndNoRemainingAmount = (cartDetails != null && cartDetails.getUsedVoucherAmount().compareTo(BigDecimal.ZERO) > 0 && cartDetails.getUsedWalletAmount().getWalletCash().doubleValue() <= 0 && netmedsPaymentProcess.getGrandTotal() <= 0);
        mBinding.cvWalletView.setAlpha(isVoucherAppliedAndWalletRemovedAndNoRemainingAmount ? 0.4f : 1f);
        if (isVoucherAppliedAndWalletRemovedAndNoRemainingAmount) {
            if (mBinding.nmsCashCheckBox.isChecked()) {
                mBinding.nmsCashCheckBox.setChecked(false);
            }
        }
        if (PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID == transactionId && !netmedsPaymentProcess.isFromConsultation() && !netmedsPaymentProcess.isFromDiagnostic()) {
            initiateAPICall(APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE);
        }
    }

    public void initiateAppliedVoucherView() {
        if (cartDetails != null && cartDetails.getUsedVoucherAmount().compareTo(BigDecimal.ZERO) > 0) {
            PaymentHelper.setIsVoucherApplied(true);
            List<AppliedVoucher> appliedVoucherList = new ArrayList<>();
            if (!TextUtils.isEmpty(cartDetails.getAppliedGeneralVouchers())) {
                AppliedVoucher appliedVoucher = new AppliedVoucher();
                appliedVoucher.setCode(cartDetails.getAppliedGeneralVouchers());
                appliedVoucher.setDiscount(String.valueOf(cartDetails.getGeneralVoucherAmount()));
                appliedVoucher.setGiftVoucherType("GEN");
                appliedVoucherList.add(appliedVoucher);
            }
            if (!TextUtils.isEmpty(cartDetails.getAppliedIhoVouchers())) {
                String ihoVoucher = cartDetails.getAppliedIhoVouchers();
                String[] splittedVoucherArray = ihoVoucher.split("\\|");
                for (String voucher : splittedVoucherArray) {
                    AppliedVoucher appliedVoucher = new AppliedVoucher();
                    appliedVoucher.setCode(voucher);
                    appliedVoucher.setDiscount(String.valueOf(cartDetails.getIhoVoucherAmount()));
                    appliedVoucher.setGiftVoucherType("IHO");
                    appliedVoucherList.add(appliedVoucher);
                }
            }
            setAppliedVoucherResponse(appliedVoucherList);
            setAppliedVoucherView(appliedVoucherList, false, false);
        } else {
            setVoucherView(new ArrayList<AppliedVoucher>());
            PaymentHelper.setIsVoucherApplied(false);
        }
    }

    void setAppliedVoucherView(List<AppliedVoucher> appliedVoucher, boolean callTotalInformation, boolean isFromVoucherRemoval) {
        setVoucherView(appliedVoucher);
        setFormVoucherRemoval(isFromVoucherRemoval);
        if (callTotalInformation) {
            showPaymentSplit(View.GONE);
            if (!netmedsPaymentProcess.isFromConsultation()) {
                initiateAPICall(CALL_WALLET_BALANCE_AND_CARD_DETAILS);
            }
        } else
            mPaymentInterface.vmDismissProgress();
    }

    public void setVoucherView(List<AppliedVoucher> appliedVoucher) {
        mBinding.paymentDetailLayout.cvVoucherView.setVisibility(voucherVisibility() ? View.GONE : View.VISIBLE);
        String voucher = "";
        for (AppliedVoucher voucherList : appliedVoucher) {
            voucher = TextUtils.isEmpty(voucher) ? voucherList.getCode() : voucher + "," + voucherList.getCode();
        }
        mBinding.paymentDetailLayout.voucherText.setText(TextUtils.isEmpty(voucher) ? getApplication().getResources().getString(R.string.voucher_note) : voucher);
        mBinding.paymentDetailLayout.voucherText.setTextColor(TextUtils.isEmpty(voucher) ? getApplication().getResources().getColor(R.color.colorLightBlueGrey) :
                getApplication().getResources().getColor(R.color.colorDarkBlueGrey));
        mBinding.paymentDetailLayout.voucherText.setTypeface(TextUtils.isEmpty(voucher) ? CommonUtils.getTypeface(context, "font/Lato-Regular.ttf") :
                CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
    }

    private void initiatePaymentDetails(MStarCartDetails cartDetails) {
        BigDecimal productDiscount = BigDecimal.ZERO;
        BigDecimal couponDiscount = BigDecimal.ZERO;
        if (cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
            for (MStarProductDetails item : cartDetails.getLines()) {
                productDiscount = productDiscount.add(item.getLineProductDiscount());
                couponDiscount = couponDiscount.add(item.getLineCouponDiscount());
            }
            mBinding.paymentDetailLayout.mrpTotal.setText(CommonUtils.getPriceInFormat(cartDetails.getSubTotalAmount()));
            mBinding.paymentDetailLayout.additionalDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(productDiscount));
            mBinding.paymentDetailLayout.additionalDiscountLayout.setVisibility(productDiscount.compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
            mBinding.paymentDetailLayout.totalSaving.setText(CommonUtils.getPriceInFormat(cartDetails.getTotalSavings()));
            mBinding.paymentDetailLayout.tvNmsWalletAmount.setText(netmedsCash());
            showPaymentSplit(View.VISIBLE);
            mBinding.paymentDetailLayout.totalSavingLayout.setVisibility(cartDetails.getTotalSavings().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
            mPaymentInterface.vmDismissProgress();
        }
    }

    private void showPaymentSplit(int visibility) {
        mBinding.paymentDetailLayout.paymentSplit.setVisibility(visibility);
        mBinding.paymentDetailLayout.totalSavingLayout.setVisibility(visibility);
        mBinding.paymentDetailLayout.paymentSplitViewAll.setVisibility(visibility == View.VISIBLE ? View.GONE : View.VISIBLE);
        if (!isFromPayment()) {
            mBinding.paymentDetailLayout.totalSavingLayout.setVisibility(showConsultationDiscount() && showDiagnosticDiscountAmount() ? View.GONE : View.VISIBLE);
            mBinding.paymentDetailLayout.llNetmedsDiscount.setVisibility(showConsultationDiscount() && showDiagnosticDiscountAmount() ? View.GONE : View.VISIBLE);
        }
    }

    private void walletBalanceResponse(String data, int transactionId) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel walletResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (walletResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(walletResponse.getStatus()) &&
                    walletResponse.getResult().getWalletBalance().compareTo(BigDecimal.ZERO) > 0) {
                PaymentHelper.setCbBreakUp(new Gson().toJson(walletResponse.getResult().getUseableBalanceBreakupList()));
                mBinding.cvWalletView.setVisibility(View.VISIBLE);
                mBinding.nmsCashAmount.setText(String.format("%s ₹%s", getApplication().getResources().getString(R.string.balance), String.valueOf(walletResponse.getResult().getWalletBalance())));
                mBinding.redeemedAmount.setVisibility(View.GONE);
                setNmsCash(walletResponse.getResult().getWalletBalance().doubleValue());
                setNmsSuperCash(walletResponse.getResult().getCashbackUseableBalance().doubleValue());
            } else {
                mBinding.nmsCashCheckBox.setChecked(false);
                mBinding.cvWalletView.setVisibility(View.GONE);
            }
            if (isFromPaymentFailure() && (PaymentHelper.isNMSCashApplied() || PaymentHelper.isNMSSuperCashApplied())) {
                walletTransaction();
            } else if (netmedsPaymentProcess.isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied()) {
                mBinding.nmsCashCheckBox.setChecked(true);
                setNMSCashViewDiagnosis(DiagnosticHelper.getAppliedNMSCash());
            } else if (PaymentHelper.isNMSCashApplied() && (CALL_WALLET_BALANCE_AND_CARD_DETAILS != transactionId)) {
                mBinding.nmsCashCheckBox.setChecked(true);
                setNMSCashView(PaymentHelper.getAppliedNMSCash());
            }
        }
        if (CALL_WALLET_BALANCE_AND_CARD_DETAILS == transactionId) {
            initiateAPICall(getPaymentGatewayResponse() != null ? APIServiceManager.MSTAR_GET_CART_DETAILS : GET_CART_DETAILS_AFTER_CALL_PAYMENT_LIST);
        } else {
            mPaymentInterface.vmDismissProgress();
        }
    }

    private void paymentListResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel basicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (basicResponseTemplateModel != null && !TextUtils.isEmpty(basicResponseTemplateModel.getStatus()) &&
                    AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(basicResponseTemplateModel.getStatus())) {
                paymentGatewayResponseMutableLiveData.setValue(basicResponseTemplateModel);
            } else {
                mPaymentInterface.vmDismissProgress();
            }
        } else {
            mPaymentInterface.vmDismissProgress();
        }
    }

    void onPaymentGatewayDataAvailable(MStarBasicResponseTemplateModel paymentGatewayResponse) {
        setPaymentGatewayResponse(paymentGatewayResponse);
        this.paymentGatewayResponse = paymentGatewayResponse;
        setPaymentGatewayList(paymentGatewayResponse.getResult().getPaymentGatewayLists());
    }

    private void proceedPlaceOrderProcess() {
        mPaymentInterface.disableOrEnableOtherViewClicks(false);
        if (!TextUtils.isEmpty(mBasePreference.getPaymentCredentials())) {
            initPayment();
        } else {
            initiateAPICall(PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL);
        }
    }

    private void setPaymentGatewayList(List<PaymentGatewayList> paymentGatewayList) {
        if ((netmedsPaymentProcess.isFromDiagnostic() || isFromPayment()) && !paymentGatewayList.isEmpty()) {
            ListIterator<PaymentGatewayList> iterator = paymentGatewayList.listIterator();
            while (iterator.hasNext()) {
                if (iterator.next().getId() == 0 || (netmedsPaymentProcess.isFromDiagnostic() && disableNewCardForLabAdviser() && iterator.next().getId() == PaymentConstants.CREDIT_DEBIT_LIST)) {
                    iterator.remove();
                }
            }
        }
        if (!paymentGatewayList.isEmpty()) {
            mPaymentInterface.setPaymentListAdapter(sortPaymentList(paymentGatewayList), (isPrimeProductOnly() || isMixedProduct()), isPayTmUpiActive(), isGooglePayActive());
            setStaticBankList(paymentGatewayList);
        }
        mBinding.paymentDetailLayout.mainLayout.setVisibility(View.VISIBLE);
        if (isFormVoucherRemoval())
            setNmsCashValueOnVoucherRemoval();
        initiateAppliedVoucherView();
        if (netmedsPaymentProcess.isFromDiagnostic()) {
            nmsWalletBalanceForDiagnosticsResponse(getNmsWalletBalanceForDiagnosticsMutableLiveData().getValue());
        }
        initiateAppliedWalletView(PAYMENT_LIST_WITH_CART_DETAILS_API_CALL_TRANSACTION_ID);
        mPaymentInterface.vmDismissProgress();
    }


    private boolean disableNewCardForLabAdviser() {
        if (paymentGatewayResponse == null || paymentGatewayResponse.getResult() == null)
            return false;
        return paymentGatewayResponse.getResult().getHideCard();
    }

    private void setStaticBankList(List<PaymentGatewayList> paymentGatewayList) {
        if (paymentGatewayList.size() > 0) {
            for (PaymentGatewayList list : paymentGatewayList) {
                if (list != null && list.getId() == (PaymentConstants.NET_BANKING_LIST)) {
                    if (list.getSubList() != null && list.getSubList().size() > 0) {
                        for (PaymentGatewaySubCategory subList : list.getSubList())
                            staticBankList.add(subList.getId());
                    }
                }
            }
        }
    }

    private List<PaymentGatewayList> sortPaymentList(List<PaymentGatewayList> list) {
        Collections.sort(list, new Comparator<PaymentGatewayList>() {
            @Override
            public int compare(PaymentGatewayList paymentGatewayList, PaymentGatewayList t1) {
                return Integer.compare(paymentGatewayList.getSequence(), t1.getSequence());
            }
        });
        for (PaymentGatewayList paymentGatewayList : list) {
            Collections.sort(paymentGatewayList.getSubList(), new Comparator<PaymentGatewaySubCategory>() {
                @Override
                public int compare(PaymentGatewaySubCategory paymentGatewayList, PaymentGatewaySubCategory t1) {
                    if (CommonUtils.isBuildVariantProdRelease() && paymentGatewayList.getParentId() == PaymentConstants.WALLET_LIST && paymentGatewayList.getSubId() == PaymentConstants.AMAZON) {
                        paymentGatewayList.setLinked(getAmazonPayLinkStatus());
                        paymentGatewayList.setCurrentBalance(getAmazonPayBalance());
                    }
                    return Integer.compare(paymentGatewayList.getSequence(), t1.getSequence());
                }
            });
        }
        setSortedPaymentList(list);
        return list;
    }

    private void paymentGatewayResponse(String data, int transactionId) {
        if (!TextUtils.isEmpty(data)) {
            PaymentKeyResponse paymentKeyResponse = new Gson().fromJson(data, PaymentKeyResponse.class);
            if (paymentKeyResponse != null && !TextUtils.isEmpty(paymentKeyResponse.getStatus()) && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(paymentKeyResponse.getStatus())) {
                mBasePreference.setPaymentCredentials(data);
                if (PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY == transactionId) {
                    initiateCalls();
                } else {
                    initPayment();
                }
            } else {
                if (PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY == transactionId) {
                    initiateCalls();
                } else {
                    mPaymentInterface.vmDismissProgress();
                    showApiError(PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL);
                }
            }
        } else {
            if (PAYMENT_GATEWAY_CREDIENTIAL_AND_CUSTOMER_CREATION_IN_JUSPAY == transactionId) {
                initiateCalls();
            } else {
                mPaymentInterface.vmDismissProgress();
                showApiError(PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL);
            }
        }
    }

    private void applyAndUnApplyWalletResponse(String data, int transactionId) {
        showPaymentSplit(View.GONE);
        PaymentHelper.setReloadTotalInformation(true);
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                boolean isWalletApplied = APIServiceManager.MSTAR_APPLY_WALLET == transactionId;
                PaymentHelper.setIsNMSCashApplied(isWalletApplied);
                //setNMSCashView(Math.abs(isWalletApplied ? applingNMSCash : 0));
                PaymentHelper.setAppliedNMSCash(Math.abs(isWalletApplied ? applingNMSCash : 0));
                PaymentHelper.setNmsWalletAmount(getNmsCash() + getNmsSuperCash());
                setFormVoucherRemoval(false);
                if (!netmedsPaymentProcess.isFromConsultation()) {
                    initiateAPICall(CALL_WALLET_BALANCE_AND_CARD_DETAILS);
                }
            } else {
                mPaymentInterface.vmDismissProgress();
            }
        } else {
            mPaymentInterface.vmDismissProgress();
        }
    }

    private void setNMSCashView(double appliedNmsCash) {
        mBinding.redeemedAmount.setText(String.format(Locale.getDefault(), "₹%.2f", appliedNmsCash));
        mBinding.nmsCashAmount.setText(String.format("%s ₹%s", getApplication().getResources().getString(R.string.balance), String.format(Locale.getDefault(), "%.2f", getNmsCash() - appliedNmsCash)));
        mBinding.redeemedAmount.setVisibility(appliedNmsCash > 0 ? View.VISIBLE : View.GONE);
    }

    private void unApplyPromoCodeResponse(String data) {
        mPaymentInterface.vmDismissProgress();
        if (data.equalsIgnoreCase(PaymentConstants.TRUE))
            initiateAPICall(APIServiceManager.MSTAR_GET_CART_DETAILS);
    }

    MutableLiveData<ApplyCouponResponse> getApplyCouponMutableLiveData() {
        return applyCouponMutableLiveData;
    }

    void onApplyCouponDataAvailable(ApplyCouponResponse applyCouponResponse) {
        mPaymentInterface.vmDismissProgress();
        netmedsPaymentProcess.setConsultationCouponListOpen(false);
        if (applyCouponResponse != null && applyCouponResponse.getServiceStatus() != null && applyCouponResponse.getServiceStatus().getStatusCode() != null && applyCouponResponse.getServiceStatus().getStatusCode() == 200) {
            if (applyCouponResponse.getApplyCoupon() != null) {
                netmedsPaymentProcess.setConsultationApplyCoupon(applyCouponResponse.getApplyCoupon());
                netmedsPaymentProcess.setConsultationTotalAmount(!TextUtils.isEmpty(applyCouponResponse.getApplyCoupon().getGrandTotal()) ? Double.parseDouble(applyCouponResponse.getApplyCoupon().getGrandTotal()) : 0);
                showConsultationPaymentSplit();
                updateCoupon(applyCouponResponse.getApplyCoupon());
            }
        }
    }

    private void updateCoupon(ApplyCoupon applyCoupon) {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            if (applyCoupon.getCode() != null) {
                ConsultationUpdateRequest updateCouponRequest = new ConsultationUpdateRequest();
                updateCouponRequest.setType(ConsultationConstant.UPDATE_TYPE_COUPON);
                updateCouponRequest.setUserId(netmedsPaymentProcess.getConsultationUserId());
                ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
                updateConsultingPayload.setId(netmedsPaymentProcess.getConsultationId());
                updateConsultingPayload.setCouponCode(applyCoupon.getCode());
                updateCouponRequest.setConsultingPayload(updateConsultingPayload);
                mPaymentInterface.vmShowProgress();
                ConsultationServiceManager.getInstance().updateConsultation(PaymentViewModel.this, mBasePreference, updateCouponRequest, "", ConsultationConstant.UPDATE_TYPE_COUPON, ConsultationServiceManager.UPDATE_COUPON);
            }
        } else
            failedTransactionId = ConsultationServiceManager.UPDATE_COUPON;
    }

    private void diagnosticOrderUpdateResponse(String data) {
        vmDismissLoader();
        DiagnosticUpdateResponse diagnosticUpdateResponse = new Gson().fromJson(data, DiagnosticUpdateResponse.class);
        if (diagnosticUpdateResponse != null) {
            diagnosticOrderUpdateStatus(diagnosticUpdateResponse);
        } else {
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE);
        }
    }

    private void diagnosticOrderUpdateStatus(DiagnosticUpdateResponse diagnosticUpdateResponse) {
        if (diagnosticUpdateResponse.getServiceStatus() != null && diagnosticUpdateResponse.getServiceStatus().getStatusCode() != null && diagnosticUpdateResponse.getServiceStatus().getStatusCode() == 200) {
            diagnosticOrderUpdateResult(diagnosticUpdateResponse.getPaymentUpdateResult());
        } else {
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE);
        }
    }

    private void diagnosticOrderUpdateResult(DiagnosticUpdateResult updateResult) {
        if (updateResult != null) {
            diagnosticOrderUpdateResultStatus(updateResult);
        } else {
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE);
        }
    }

    private void diagnosticOrderUpdateResultStatus(DiagnosticUpdateResult updateResult) {
        if (updateResult.isStatus()) {
            mPaymentInterface.navigateToSuccessPage("");
        }
    }

    private void diagnosticUpdateResponse(String data) {
        DiagnosticUpdateResponse diagnosticUpdateResponse = new Gson().fromJson(data, DiagnosticUpdateResponse.class);
        setDiagnosticUpdatePaymentType(diagnosticUpdateResponse != null && diagnosticUpdateResponse.getPaymentUpdateType() != null ? diagnosticUpdateResponse.getPaymentUpdateType() : "");
        if (diagnosticUpdateResponse != null) {
            diagnosticUpdateStatus(diagnosticUpdateResponse);
        } else {
            vmDismissLoader();
            showApiError(getDiagnosticUpdatePaymentType().equals(DiagnosticConstant.SUCCESS) ? DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE : DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_FAILURE_UPDATE);
        }
    }

    private void diagnosticUpdateStatus(DiagnosticUpdateResponse diagnosticUpdateResponse) {
        if (diagnosticUpdateResponse.getServiceStatus() != null && diagnosticUpdateResponse.getServiceStatus().getStatusCode() != null && diagnosticUpdateResponse.getServiceStatus().getStatusCode() == 200) {
            diagnosticUpdateResult(diagnosticUpdateResponse.getPaymentUpdateResult());
        } else {
            vmDismissLoader();
            showApiError(getDiagnosticUpdatePaymentType().equals(DiagnosticConstant.SUCCESS) ? DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE : DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_FAILURE_UPDATE);
        }
    }

    private void diagnosticUpdateResult(DiagnosticUpdateResult updateResult) {
        if (updateResult != null) {
            diagnosticUpdateResultStatus(updateResult);
        } else {
            vmDismissLoader();
            showApiError(getDiagnosticUpdatePaymentType().equals(DiagnosticConstant.SUCCESS) ? DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE : DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_FAILURE_UPDATE);
        }
    }

    private void diagnosticUpdateResultStatus(DiagnosticUpdateResult updateResult) {
        if (updateResult.isStatus()) {
            switch (getDiagnosticUpdatePaymentType()) {
                case DiagnosticConstant.SUCCESS:
                    diagnosticOrderUpdate();
                    sendDiagnosticPurchaseEvent();
                    if (DiagnosticHelper.isNMSCashApplied() && netmedsPaymentProcess.isFromDiagnostic()) {
                        DiagnosticHelper.setIsNMSCashApplied(false);
                    }
                    break;
                case DiagnosticConstant.FAILURE:
                    vmDismissLoader();
                    DiagnosticHelper.clearSelectedList();
                    mPaymentInterface.navigateToPaymentFailurePage(netmedsPaymentProcess.getJusPayOrderStatus(), getCODPaymentDetails(), (isPrimeProductOnly() || isMixedProduct()));
                    break;
            }
        }
    }

    private boolean isPrimeProductOnlyAvailableInCart() {
        MstarPrimeProductResult primeProductResult = new Gson().fromJson(mBasePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
        boolean isPrimeProductAvailable = false;
        if (cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
            for (MStarProductDetails productDetails : cartDetails.getLines()) {
                if (!isPrimeProductAvailable) {
                    isPrimeProductAvailable = primeProductResult != null && !primeProductResult.getPrimeProductCodesList().isEmpty() &&
                            primeProductResult.getPrimeProductCodesList().contains(String.valueOf(productDetails.getProductCode()));
                }
            }
        }
        return cartDetails != null && cartDetails.getLines().size() == 1 && isPrimeProductAvailable;
    }

    private void diagnosticPaymentCreationResponse(String data) {
        vmDismissLoader();
        DiagnosticPaymentCreateResponse diagnosticPaymentCreateResponse = new Gson().fromJson(data, DiagnosticPaymentCreateResponse.class);
        if (diagnosticPaymentCreateResponse != null) {
            diagnosticPaymentCreationStatus(diagnosticPaymentCreateResponse);
        } else
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION);
    }

    private void diagnosticPaymentCreationStatus(DiagnosticPaymentCreateResponse diagnosticPaymentCreateResponse) {
        if (diagnosticPaymentCreateResponse.getServiceStatus() != null && diagnosticPaymentCreateResponse.getServiceStatus().getStatusCode() != null && diagnosticPaymentCreateResponse.getServiceStatus().getStatusCode() == 200) {
            diagnosticPaymentCreationResult(diagnosticPaymentCreateResponse);
        } else
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION);
    }

    private void diagnosticPaymentCreationResult(DiagnosticPaymentCreateResponse diagnosticPaymentCreateResponse) {
        if (diagnosticPaymentCreateResponse.getResult() != null && !TextUtils.isEmpty(diagnosticPaymentCreateResponse.getResult().getPaymentId())) {
            setDiagnosticPaymentId(diagnosticPaymentCreateResponse.getResult().getPaymentId());
            setDiagnosticOrderCreateDate(!TextUtils.isEmpty(diagnosticPaymentCreateResponse.getResult().getCreatedAt()) ? diagnosticPaymentCreateResponse.getResult().getCreatedAt() : "");
            netmedsPaymentProcess.paymentGateWayReDirection();
        } else
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION);
    }

    /*Diagnostic Order Creation*/
    private void diagnosticOrderCreationResponse(String data) {
        DiagnosticOrderCreateResponse diagnosticOrderCreateResponse = new Gson().fromJson(data, DiagnosticOrderCreateResponse.class);
        if (diagnosticOrderCreateResponse != null) {
            diagnosticCreateOrderServiceStatus(diagnosticOrderCreateResponse);
        } else {
            vmDismissLoader();
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION);
        }
    }

    private void diagnosticCreateOrderServiceStatus(DiagnosticOrderCreateResponse diagnosticOrderCreateResponse) {
        if (diagnosticOrderCreateResponse.getServiceStatus() != null && diagnosticOrderCreateResponse.getServiceStatus().getStatusCode() != null && diagnosticOrderCreateResponse.getServiceStatus().getStatusCode() == 200) {
            diagnosticOrderCreationResult(diagnosticOrderCreateResponse);
        } else {
            vmDismissLoader();
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION);
        }
    }

    private void diagnosticOrderCreationResult(DiagnosticOrderCreateResponse diagnosticOrderCreateResponse) {
        if (diagnosticOrderCreateResponse.getOrderCreateResult() != null && !TextUtils.isEmpty(diagnosticOrderCreateResponse.getOrderCreateResult().getOrderId())) {
            netmedsPaymentProcess.setDiagnosticOrderId(diagnosticOrderCreateResponse.getOrderCreateResult().getOrderId());
            diagnosticPaymentCreation();
        } else {
            vmDismissLoader();
            showApiError(DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION);
        }
    }


    private void consultationCouponResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            GetAllCouponResponse getCouponListResponse = new Gson().fromJson(data, GetAllCouponResponse.class);
            if (getCouponListResponse != null && getCouponListResponse.getServiceStatus() != null && getCouponListResponse.getServiceStatus().getStatusCode() != null && getCouponListResponse.getServiceStatus().getStatusCode() == 200) {
                if (getCouponListResponse.getCouponList() != null && getCouponListResponse.getCouponList().size() > 0) {
                    consultationCouponList = new ArrayList<>();
                    for (ConsultationCoupon consultationCoupon : getCouponListResponse.getCouponList()) {
                        if (netmedsPaymentProcess.getConsultationApplyCoupon() != null && netmedsPaymentProcess.getConsultationApplyCoupon().getCode().equalsIgnoreCase(consultationCoupon.getCode())) {
                            consultationCoupon.setApplied(true);
                        } else {
                            consultationCoupon.setApplied(false);
                        }
                        consultationCouponList.add(consultationCoupon);
                    }
                }
            }
            mPaymentInterface.showCouponListView(consultationCouponList, this);
        } else
            mPaymentInterface.vmDismissProgress();
    }

    private void setNMSCashViewDiagnosis(double appliedNmsCash) {
        if (appliedNmsCash > getNmsCash()) {
            mBinding.redeemedAmount.setText(String.format(Locale.getDefault(), "₹%.2f", getNmsCash()));
            mBinding.nmsCashAmount.setText(String.format("%s ₹%s", getApplication().getResources().getString(R.string.balance), String.format(Locale.getDefault(), "%.2f", 0.00)));
            mBinding.redeemedAmount.setVisibility(appliedNmsCash > 0 ? View.VISIBLE : View.GONE);
        } else {
            mBinding.redeemedAmount.setText(String.format(Locale.getDefault(), "₹%.2f", appliedNmsCash));
            mBinding.nmsCashAmount.setText(String.format("%s ₹%s", getApplication().getResources().getString(R.string.balance), String.format(Locale.getDefault(), "%.2f", getNmsCash() - appliedNmsCash)));
            mBinding.redeemedAmount.setVisibility(appliedNmsCash > 0 ? View.VISIBLE : View.GONE);
        }
    }

    String getJusPayCredentials(int id) {
        return PaymentUtils.jusPayCredential(id, mBasePreference.getPaymentCredentials());
    }

    void setAmazonPayBalance(double balance) {
        this.amazonBalance = balance;
    }

    private double getAmazonPayBalance() {
        return amazonBalance;
    }

    void setAmazonPayLinkStatus(boolean linkStatus) {
        this.amazonLinkStatus = linkStatus;
    }

    private boolean getAmazonPayLinkStatus() {
        return amazonLinkStatus;
    }

    private String getGatewayKey() {
        return getJusPayCredentials(PaymentConstants.JUS_PAY_GATEWAY_KEY);
    }

    String getJusPayMerchantId() {
        return getJusPayCredentials(PaymentConstants.JUS_PAY_MERCHENT_ID);
    }

    String getJusPayClientId() {
        return getJusPayCredentials(PaymentConstants.JUS_PAY_CLIENT_ID);
    }

    private void setNmsCashValueOnVoucherRemoval() {
        double walletDiscount = netmedsPaymentProcess.getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_WALLET);
        if (walletDiscount > 0 && PaymentHelper.isNMSCashApplied()) {
            double nmsCash = Math.abs(walletDiscount);
            PaymentHelper.setAppliedNMSCash(nmsCash);
            setNMSCashView(nmsCash);
        }
    }

    private void initNmsWalletForDiagnosis() {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        showNoNetworkView(isConnected);
        if (isConnected) {
            DiagnosticServiceManager.getInstance().getNMSBalanceForDiagnosis(this, mBasePreference.getMStarSessionId(), getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO)
                    , labPayMode(), getcId(),
                    getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO)
                            ? getPid() : getLabId(), mBasePreference, CommonUtils.getAmountWithTwoDecimalFormat(netmedsPaymentProcess.getDiagnosticTotalAmount()), BasePreference.getInstance(context).getMstarCustomerId(), CommonUtils.getAppVersionCode(context)
                    , getTestIdList());
        } else failedTransactionId = DiagnosticServiceManager.NMS_WALLET_BALANCE;
    }

    public void initPayment() {
        netmedsPaymentProcess.initiatePaymentProcess();
    }

    public void fireWebEngageForPaymentMode() {
        if (webEngageModel == null) return;
        switch (netmedsPaymentProcess != null && netmedsPaymentProcess.getPaymentGatewaySubCategory() != null ? netmedsPaymentProcess.getPaymentGatewaySubCategory().getParentId() : 0) {
            case PaymentConstants.NET_BANKING_LIST: {
                webEngageModel.setPaymentMode(context.getResources().getString(R.string.txt_web_engage_consultation_payment_mode_net_banking));
                break;
            }
            case PaymentConstants.CREDIT_DEBIT_LIST: {
                webEngageModel.setPaymentMode(context.getResources().getString(R.string.txt_web_engage_consultation_payment_mode_card));
                break;
            }
            case PaymentConstants.WALLET_LIST: {
                webEngageModel.setPaymentMode(context.getResources().getString(R.string.txt_web_engage_consultation_payment_mode_wallet));
                break;
            }
            case PaymentConstants.COD: {
                webEngageModel.setPaymentMode(context.getResources().getString(R.string.txt_web_engage_consultation_payment_mode_cod));
                break;
            }
            case PaymentConstants.OTHER_PAYMENT_LIST: {
                webEngageModel.setPaymentMode(context.getResources().getString(R.string.txt_web_engage_consultation_payment_mode_other));
                break;
            }
            default:
                webEngageModel.setPaymentMode(PaymentConstants.FREE);
                break;
        }
        WebEngageHelper.getInstance().paymentModeSelectedEvent(webEngageModel, context);
    }


    public List<PaymentGatewayList> classifiedListWithoutEmpty(List<PaymentGatewayList> list, boolean isPrime) {
        List<PaymentGatewayList> classifiedPaymentList = new ArrayList<>();
        for (PaymentGatewayList gatewayList : list) {
            if (PaymentConstants.OTHER_PAYMENT_LIST == gatewayList.getId() || PaymentConstants.UPI_PAYMENT_LIST == gatewayList.getId()) {
                List<PaymentGatewaySubCategory> gatewaySubCategories = new ArrayList<>();
                for (PaymentGatewaySubCategory paymentGatewaySubCategory : gatewayList.getSubList()) {
                    if (paymentGatewaySubCategory.isDisplay()) {
                        gatewaySubCategories.add(paymentGatewaySubCategory);
                    }
                }
                if (!gatewaySubCategories.isEmpty()) {
                    gatewayList.setSubList(gatewaySubCategories);
                    classifiedPaymentList.add(gatewayList);
                }
            } else if (PaymentConstants.WALLET_LIST == gatewayList.getId()) {
                List<PaymentGatewaySubCategory> gatewaySubCategories = new ArrayList<>();
                for (PaymentGatewaySubCategory paymentGatewaySubCategory : gatewayList.getSubList()) {
                    if (paymentGatewaySubCategory.isDisplay()) {
                        gatewaySubCategories.add(paymentGatewaySubCategory);
                    }
                }
                if (!gatewaySubCategories.isEmpty()) {
                    gatewayList.setSubList(gatewaySubCategories);
                    classifiedPaymentList.add(gatewayList);
                }
            } else if (!gatewayList.getSubList().isEmpty()) {
                if (PaymentConstants.COD == gatewayList.getId()) {
                    setCODPaymentDetails(gatewayList.getSubList().get(0));
                    if (!isPrime) {
                        classifiedPaymentList.add(gatewayList);
                    }
                } else {
                    classifiedPaymentList.add(gatewayList);
                }
            } else if (PaymentConstants.CREDIT_DEBIT_LIST == gatewayList.getId()) {
                classifiedPaymentList.add(gatewayList);
            }
        }
        return classifiedPaymentList;
    }


    public String getPaymentMethod() {
        String paymentMethod = "";
        PaymentGatewaySubCategory subCategory = netmedsPaymentProcess.getPaymentGatewaySubCategory();
        if (netmedsPaymentProcess.isNewCard()) {
            return setJusPayPaymentMethod();
        } else if (subCategory != null) {
            switch (subCategory.getParentId()) {
                case PaymentConstants.WALLET_LIST:
                case PaymentConstants.UPI_PAYMENT_LIST:
                    paymentMethod = setWalletListPaymentMethod(subCategory);
                    break;
                case PaymentConstants.OTHER_PAYMENT_LIST:
                    paymentMethod = setOtherPaymentMethod(subCategory);
                    break;
                case PaymentConstants.NET_BANKING_LIST:
                case PaymentConstants.CREDIT_DEBIT_LIST:
                    paymentMethod = setJusPayPaymentMethod();
                    break;
                case PaymentConstants.COD:
                    paymentMethod = subCategory.getKey();
                    break;
            }
        } else return PaymentConstants.FREE;
        return paymentMethod;
    }

    private String setWalletListPaymentMethod(PaymentGatewaySubCategory category) {
        return category.getSubId() == PaymentConstants.PAYTM ? PaymentConstants.PAY_TM_PAYMENT_METHOD : getGatewayKey();
    }

    private String setOtherPaymentMethod(PaymentGatewaySubCategory subCategory) {
        return subCategory.getSubId() == PaymentConstants.PAYPAL ? PaymentConstants.PAY_PAL_PAYMENT_METHOD : getGatewayKey();
    }

    private String setJusPayPaymentMethod() {
        return getGatewayKey();
    }


    @Override
    public void imagePreview(String url) {

    }

    @Override
    public void editSelectedOption(int position, Message message) {

    }

    @Override
    public void selectedSpecialization(Speciality speciality) {

    }

    @Override
    public void closeSpecialization() {

    }

    @Override
    public void closeDoctorInfo() {

    }

    @Override
    public void viewDoctorInfo(int doctorId) {

    }

    @Override
    public void showAllCoupons() {

    }

    @Override
    public void paymentPlan(double fee, String selectedPaymentPlan) {

    }

    @Override
    public void selectedCouponCode(ConsultationCoupon coupon) {
        if (coupon != null && !TextUtils.isEmpty(coupon.getCode()) && !dateTimeUtils.isCouponExpired(coupon.getExpiry(), DateTimeUtils.yyyyMMddTHHmmss)) {
            mPaymentInterface.vmShowProgress();
            applyConsultationCoupon(coupon);
        }
    }

    private void applyConsultationCoupon(ConsultationCoupon couponCode) {
        ApplyCouponRequest applyCouponRequest = new ApplyCouponRequest();
        applyCouponRequest.setCouponCode(couponCode.getCode());
        applyCouponRequest.setType(netmedsPaymentProcess.getConsultationCouponType());
        applyCouponRequest.setSpecs(getConsultationSpecialization());
        ConsultationServiceManager.getInstance().applyCoupon(this, applyCouponRequest);
    }

    @Override
    public void closeCouponList() {
        netmedsPaymentProcess.setConsultationCouponListOpen(false);
    }

    @Override
    public void selectedProblem(Options selectedProblem) {

    }

    @Override
    public void selectedOption(List<Options> options) {

    }

    @Override
    public void otherOption() {

    }

    @Override
    public void loaderView(boolean isShowing) {

    }

    @Override
    public void downloadAttachment(Message message, int position) {

    }

    @Override
    public void orderMedicine(MedicineInfoResponse medicineInfoResponse) {

    }

    @Override
    public void onSubmitRating(Float rating) {

    }

    @Override
    public void bookLabTest(MedicineInfoResponse medicineInfoResponse) {

    }

    String getConsultationAppliedCouponCode() {
        return getConsultationCoupon() != null && !TextUtils.isEmpty(getConsultationCoupon().getCode()) ? getConsultationCoupon().getCode() : "";
    }

    public void consultationPaymentCreation(String gateWayName, String paymentStatus) {
        consultationRetryGatewayName = gateWayName;
        consultationRetryPaymentStatus = paymentStatus;
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            mPaymentInterface.vmShowLoader();
            ConsultationServiceManager.getInstance().paymentCreation(this, getPaymentCreationRequest(gateWayName, paymentStatus), mBasePreference, paymentStatus);
        } else
            failedTransactionId = ConsultationServiceManager.PAYMENT_CREATION;
    }

    @Override
    public void triggerEventForConsultationFailure() {
        if (webEngageModel != null) {
            webEngageModel.setPaymentStatus(context.getResources().getString(com.nms.netmeds.consultation.R.string.txt_web_engage_consultation_payment_failure));
            WebEngageHelper.getInstance().paymentModeSelectedEvent(webEngageModel, context);
        }
    }

    private void consultationPaymentCreationResponse(String data) {
        final PaymentCreationResponse paymentCreationResponse = new Gson().fromJson(CommonUtils.isJSONValid(data) ? data : "", PaymentCreationResponse.class);
        if (paymentCreationResponse != null && paymentCreationResponse.getServiceStatus() != null && paymentCreationResponse.getServiceStatus().getStatusCode() != null && paymentCreationResponse.getServiceStatus().getStatusCode() == 200) {
            checkConsultationPaymentCreationStatus(paymentCreationResponse);
        } else {
            vmDismissLoader();
            showApiError(ConsultationServiceManager.PAYMENT_CREATION);
        }
    }

    private void checkConsultationPaymentCreationStatus(PaymentCreationResponse paymentCreationResponse) {
        if (paymentCreationResponse.getResult() != null && !TextUtils.isEmpty(paymentCreationResponse.getResult().getMessage()) && paymentCreationResponse.getResult().getMessage().equalsIgnoreCase(PaymentConstants.SUCCESS)) {
            updateConsultationPayment();
        } else {
            vmDismissLoader();
            showApiError(ConsultationServiceManager.PAYMENT_CREATION);
        }
    }

    private void updateConsultationPayment() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            ConsultationServiceManager.getInstance().updateConsultation(this, mBasePreference, consultationUpdateRequest(), netmedsPaymentProcess.isPaymentTransactionSuccess() ? ConsultationConstant.SUCCESS : ConsultationConstant.FAILURE, ConsultationConstant.UPDATE_TYPE_PAYMENT, ConsultationServiceManager.PAYMENT_UPDATE);
        } else
            failedTransactionId = ConsultationServiceManager.PAYMENT_UPDATE;
    }

    private ConsultationUpdateRequest consultationUpdateRequest() {
        ConsultationUpdateRequest updatingConsultationRequest = new ConsultationUpdateRequest();
        updatingConsultationRequest.setType(ConsultationConstant.UPDATE_TYPE_PAYMENT);
        updatingConsultationRequest.setUserId(netmedsPaymentProcess.getConsultationUserId());
        ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
        updateConsultingPayload.setId(netmedsPaymentProcess.getConsultationId());
        Payment payment = new Payment();
        if (netmedsPaymentProcess.isPaymentTransactionSuccess())
            payment.setId(netmedsPaymentProcess.getConsultationOrderId());
        payment.setStatus(netmedsPaymentProcess.isPaymentTransactionSuccess() ? ConsultationConstant.SUCCESS : ConsultationConstant.FAILURE);
        updateConsultingPayload.setPayment(payment);
        updatingConsultationRequest.setConsultingPayload(updateConsultingPayload);
        return updatingConsultationRequest;
    }

    private void updateConsultationResponse(String data) {
        ConsultationUpdateResponse updateConsultingResponse = new Gson().fromJson(data, ConsultationUpdateResponse.class);
        if (updateConsultingResponse != null && updateConsultingResponse.getServiceStatus() != null && updateConsultingResponse.getServiceStatus().getStatusCode() != null && updateConsultingResponse.getServiceStatus().getStatusCode() == 200) {
            switch (updateConsultingResponse.getUpdateType()) {
                case ConsultationConstant.UPDATE_TYPE_PAYMENT:
                    if (updateConsultingResponse.getPaymentStatus().equalsIgnoreCase(ConsultationConstant.SUCCESS))
                        reoccurUpdate();
                    else vmDismissLoader();
                    break;
                case ConsultationConstant.UPDATE_TYPE_COUPON:
                    if (netmedsPaymentProcess.getGrandTotal() == 0.00) {
                        netmedsPaymentProcess.setGrandTotal(0.00);
                        consultationPaymentCreation("NA", ConsultationConstant.SUCCESS);
                    } else mPaymentInterface.vmDismissProgress();
                    break;
            }
        } else {
            mPaymentInterface.vmDismissProgress();
            showUpdateConsultationApiError(updateConsultingResponse);
        }
    }

    private void showUpdateConsultationApiError(ConsultationUpdateResponse updateConsultingResponse) {
        if (updateConsultingResponse != null && updateConsultingResponse.getUpdateType() != null) {
            int failedId = updateConsultingResponse.getUpdateType().equals(ConsultationConstant.UPDATE_TYPE_PAYMENT) ? ConsultationServiceManager.PAYMENT_UPDATE : updateConsultingResponse.getUpdateType().equals(ConsultationConstant.UPDATE_TYPE_COUPON) ? ConsultationServiceManager.UPDATE_COUPON : 0;
            showApiError(failedId);
        }
    }

    private void reoccurUpdate() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            ConsultationServiceManager.getInstance().reoccurUpdate(this, getReoccurUpdateRequest(), mBasePreference);
        } else
            failedTransactionId = ConsultationServiceManager.REOCCUR_UPDATE;
    }

    private ReoccurUpdateRequest getReoccurUpdateRequest() {
        ReoccurUpdateRequest reoccurUpdateRequest = new ReoccurUpdateRequest();
        reoccurUpdateRequest.setType(ConsultationConstant.UPDATE_TYPE_REOCCUR);
        ReoccurPayload reoccurPayload = new ReoccurPayload();
        reoccurPayload.setId(netmedsPaymentProcess.getConsultationUserId());
        reoccurPayload.setSpeciality(getConsultationSpecialization());
        reoccurPayload.setReoccurStatus(netmedsPaymentProcess.isConsultationSelectedPlan());
        reoccurUpdateRequest.setReoccurPayload(reoccurPayload);
        return reoccurUpdateRequest;
    }

    private void updateReoccurResponse(String data) {
        vmDismissLoader();
        ReoccurUpdateResponse reoccurUpdateResponse = new Gson().fromJson(data, ReoccurUpdateResponse.class);
        if (reoccurUpdateResponse != null && !TextUtils.isEmpty(reoccurUpdateResponse.getMessage()) && reoccurUpdateResponse.getMessage().equals(ConsultationConstant.SUCCESS)) {
            ConsultationEvent consultationEvent = new ConsultationEvent();
            consultationEvent.setStatus(true);
            consultationEvent.setPaymentMode(webEngageModel.getPaymentMode());
            consultationEvent.setOrderId(netmedsPaymentProcess.getOrderId());
            consultationEvent.setCoupon(new Gson().toJson(netmedsPaymentProcess.getConsultationApplyCoupon()));
            mPaymentInterface.consultationRedirection(consultationEvent);
        } else
            showApiError(ConsultationServiceManager.REOCCUR_UPDATE);
    }

    private PaymentCreationRequest getPaymentCreationRequest(String gatewayName, String paymentStatus) {
        String dateTime = DateTimeUtils.getInstance().convertMillisecondToDateTimeFormat(netmedsPaymentProcess.getConsultationOrderIdCreationTime(), DateTimeUtils.yyyyMMddTHHmmss);
        PaymentCreationRequest paymentCreationRequest = new PaymentCreationRequest();
        paymentCreationRequest.setAmount(String.valueOf(netmedsPaymentProcess.getGrandTotal()));
        paymentCreationRequest.setId(netmedsPaymentProcess.getConsultationOrderId());
        paymentCreationRequest.setPaymentGateway(gatewayName);
        paymentCreationRequest.setTime(dateTime);
        if (webEngageModel != null && !TextUtils.isEmpty(webEngageModel.getType())) {
            paymentCreationRequest.setConsulation_payment_type(webEngageModel.getType());
        }
        paymentCreationRequest.setStatus(paymentStatus);
        return paymentCreationRequest;
    }

    List<String> getStaticBankList() {
        return staticBankList;
    }

    public List<PaymentGatewayList> getSortedPaymentList() {
        return sortedPaymentList;
    }

    private void setSortedPaymentList(List<PaymentGatewayList> sortedPaymentList) {
        this.sortedPaymentList = sortedPaymentList;
    }

    //Clicking Checkbox in Wallet section.
    public void applyNmsCash() {
        if (netmedsPaymentProcess.isFromDiagnostic()) {
            applyNmsCashForDiagnosis();
            return;
        }
        setFromPaymentFailure(false);
        walletTransaction();
    }

    private void applyNmsCashForDiagnosis() {
        if (getNmsCash() > 0) {
            if (mBinding.nmsCashCheckBox.isChecked()) {
                enableDisableNmsCash(false);
                DiagnosticHelper.setIsNMSCashApplied(false);
                setNMSCashViewDiagnosis(Math.abs(getNmsCash()));
                netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getDiagnosticTotalAmount());
                DiagnosticHelper.setAppliedNMSCash(Math.abs(getNmsCash()));
                DiagnosticHelper.setNmsWalletAmount(getNmsCash());
                DiagnosticHelper.setremainingPaidAmount(netmedsPaymentProcess.getDiagnosticTotalAmount());
                mBinding.setViewModel(this);
            } else {
                enableDisableNmsCash(true);
                DiagnosticHelper.setIsNMSCashApplied(true);
                setNMSCashViewDiagnosis(Math.abs(netmedsPaymentProcess.getDiagnosticTotalAmount()));
                DiagnosticHelper.setAppliedNMSCash(Math.abs(netmedsPaymentProcess.getDiagnosticTotalAmount()));
                DiagnosticHelper.setNmsWalletAmount(getNmsCash());
                double balance = DiagnosticHelper.getAppliedNMSCash() - getNmsCash();
                DiagnosticHelper.setremainingPaidAmount(balance > 0 ? balance : 0.0);
                mBinding.setViewModel(this);
            }

            resetPaymentList();
            if (cartDetails != null)
                cartDetails.setNetPayableAmount(BigDecimal.valueOf(netmedsPaymentProcess.getGrandTotal()));

            if (isFormVoucherRemoval())
                setNmsCashValueOnVoucherRemoval();
            initiateAppliedVoucherView();

            if (new BigDecimal(String.valueOf(DiagnosticHelper.getremainingPaidAmount())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() <= 0)
                disEnablePaymentList(false, mBinding.paymentList);
            else disEnablePaymentList(true, mBinding.paymentList);

        }
    }

    private void walletTransaction() {
        resetPaymentList();
        if (getNmsCash() > 0) {
            if (mBinding.nmsCashCheckBox.isChecked()) {
                enableDisableNmsCash(false);
                mBinding.trPlaceOrderButton.setVisibility(View.GONE);
                initiateAPICall(APIServiceManager.MSTAR_UN_APPLY_WALLET);
            } else if ((isFromPaymentFailure() && PaymentHelper.isNMSCashApplied()) || netmedsPaymentProcess.getGrandTotal() > 0) {
                applyNmsWallet();
            }
        }
    }

    void resetPaymentList() {
        if (netmedsPaymentProcess.getPaymentGatewaySubCategory() != null) {
            mPaymentInterface.resetPaymentSelection();
        }
    }

    private void applyNmsWallet() {
        applingNMSCash = isFromPaymentFailure() && PaymentHelper.isNMSCashApplied() ? getNmsCash() : !isFromPaymentFailure() ? getNmsCash() : 0;
        initiateAPICall(APIServiceManager.MSTAR_APPLY_WALLET);
        enableDisableNmsCash(true);
    }

    private void enableDisableNmsCash(boolean enable) {
        mBinding.nmsCashCheckBox.setChecked(enable);
    }

    private double getNmsCash() {
        return nmsCash;
    }

    private double getNmsSuperCash() {
        return nmsSuperCash;
    }

    private void setNmsCash(double nmsCash) {
        PaymentHelper.setAvailableNMSCash(nmsCash);
        this.nmsCash = nmsCash;
    }

    private void setNmsSuperCash(double nmsSuperCash) {
        this.nmsSuperCash = nmsSuperCash;
    }

    public void enableVoucherView() {
        mPaymentInterface.vmShowProgress();
        resetPaymentList();
        mPaymentInterface.voucherView(getAppliedVoucherResponse() != null && getAppliedVoucherResponse().size() > 0 ?
                getAppliedVoucherResponse() : new ArrayList<AppliedVoucher>());
    }

    @Override
    public void onRetryClickListener() {
        onRetry(failedTransactionId);
        showWebserviceErrorView(false);
    }

    private void showApiError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.MSTAR_APPLY_WALLET:
                initiateAPICall(APIServiceManager.MSTAR_APPLY_WALLET);
                break;
            case PaymentServiceManager.REMOVE_NMS_WALLET:
                initiateAPICall(APIServiceManager.MSTAR_UN_APPLY_WALLET);
                break;
            case PaymentServiceManager.CREATE_JUS_PAY_ORDER:
            case PaymentServiceManager.CREATE_JUS_PAY_CUSTOMER:
                initiateAPICall(failedTransactionId);
                break;
            case ConsultationServiceManager.PAYMENT_CREATION:
                consultationPaymentCreation(consultationRetryGatewayName, consultationRetryPaymentStatus);
                break;
            case ConsultationServiceManager.PAYMENT_UPDATE:
                updateConsultationPayment();
                break;
            case ConsultationServiceManager.REOCCUR_UPDATE:
                reoccurUpdate();
                break;
            case ConsultationServiceManager.UPDATE_COUPON:
                updateCoupon(netmedsPaymentProcess.getConsultationApplyCoupon());
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION:
                diagnosticOrderCreation();
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION:
                diagnosticPaymentCreation();
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE:
                diagnosticPaymentUpdate();
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE:
                diagnosticOrderUpdate();
                break;
            case DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_FAILURE_UPDATE:
                diagnosticPaymentFailureUpdate();
                break;
        }
    }

    private void showWebserviceErrorView(boolean enable) {
        mBinding.parentLayout.setVisibility(enable ? View.GONE : View.VISIBLE);
        mBinding.apiErrorView.setVisibility(enable ? View.VISIBLE : View.GONE);
    }

    public void showNoNetworkView(boolean isConnected) {
        mBinding.parentLayout.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        mBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void navigateToSuccessFailurePage() {
        vmDismissLoader();
        String orderId = netmedsPaymentProcess.getOrderId();
        String apFirstOrder = netmedsPaymentProcess.getAppFirstOrder();
        if (netmedsPaymentProcess.isPaymentTransactionSuccess()) {
            sendToEcommerceTracker(apFirstOrder);
            /*WebEngage Checkout Completed Event*/
            WebEngageHelper.getInstance().checkoutCompletedEvent(context, orderId, PaymentHelper.getCustomerAddress(), apFirstOrder, cartDetails, MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList(),
                    PaymentHelper.isPrescriptionOrder(), getWebEngagePaymentMethod(), PaymentHelper.getCustomerBillingAddress(), getWebEngageProductDetails());
            /*Facebook Purchase Event*/
            FacebookPixelHelper.getInstance().logPurchaseEvent(context, orderId, cartDetails, apFirstOrder, getPaymentMethod(), PaymentHelper.isIsIncompleteOrder(), cartDetails.getLines());
            /*FireBase Purchase Event*/
            FireBaseAnalyticsHelper.getInstance().fireBasePurchaseEvent(context, orderId, apFirstOrder, netmedsPaymentProcess.getGrandTotal(), netmedsPaymentProcess.getShippingAmount(), cartDetails, cartDetails.getUsedWalletAmount().getNmsSuperCash().doubleValue(), cartDetails.getUsedWalletAmount().getWalletCash().doubleValue(), getVoucher(), PaymentHelper.getCustomerBillingAddress().getCity(), cartDetails.getSubTotalAmount().doubleValue(), cartDetails.getNetPayableAmount().doubleValue(), cartDetails.getCoupon_discount_total().doubleValue(), cartDetails.getProduct_discount_total().doubleValue());
            /* FireBase BuyAgain Event */
            if (PaymentHelper.getmStarProductDetails() != null && PaymentHelper.getmStarProductDetails().size() > 0)
                FireBaseAnalyticsHelper.getInstance().buyAndAgain(context, orderId, cartDetails);
            /*Alternate cart fire base event*/
            if (PaymentHelper.isCartSwitched())
                FireBaseAnalyticsHelper.getInstance().alternateCartFireBaseEvent(context, cartDetails);

            /*Google Tag Manager + FireBase Ecommerce Purchase Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseEcommercePurchaseEvent(context, orderId, cartDetails, getWebEngageProductDetails(), netmedsPaymentProcess.getShippingAmount(), getVoucher(), PaymentHelper.getCustomerBillingAddress().getCity());
            /*Google Tag Manager + FireBase Checkout Progress Step5 Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseCheckoutProgressEvent(context, cartDetails.getLines(), getWebEngageProductDetails(), FireBaseAnalyticsHelper.CHECKOUT_STEP_5, FireBaseAnalyticsHelper.EVENT_PARAM_PAYMENT_DETAILS);

            FireBaseAnalyticsHelper.getInstance().genericProductFireBaseEvent(context, orderId, cartDetails, mBasePreference);
            mBasePreference.setGenericProductMap(new HashMap<String, MStarProductDetails>());
            mBasePreference.setGenericBrandProductMap(new HashMap<String, MStarProductDetails>());
            mPaymentInterface.navigateToSuccessPage(netmedsPaymentProcess.getCompleteOrderResponse());
        } else {
            mPaymentInterface.navigateToPaymentFailurePage(netmedsPaymentProcess.getJusPayOrderStatus(), getCODPaymentDetails(), (isPrimeProductOnly() || isMixedProduct()));
        }
    }

    boolean isFromPayment() {
        return isFromPayment;
    }

    public boolean isConsultationCouponAvailable() {
        if (isFromPayment())
            return true;
        else return netmedsPaymentProcess.getConsultationApplyCoupon() == null;
    }

    private void setFromPayment(boolean fromPayment) {
        isFromPayment = fromPayment;
    }

    void disEnablePaymentList(final boolean enable, View view) {
        view.setEnabled(enable);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                disEnablePaymentList(enable, child);
            }
        }
        mPaymentInterface.enableAndDisablePlaceOrderButtonInWalletSection(enable);
    }


    //Assign value in payment amount details view
    public boolean getStrikeDeliveryChargeVisibility() {
        return cartDetails != null && cartDetails.getShippingChargesFinal().compareTo(BigDecimal.ZERO) == 0 && cartDetails.getShippingChargesOriginal().compareTo(BigDecimal.ZERO) != 0;
    }

    public String setDeliveryAmount() {
        mBinding.paymentDetailLayout.tvStrikeDeliveryCharges.setText(CommonUtils.getPriceInFormat(cartDetails != null ? cartDetails.getShippingChargesOriginal().doubleValue() : 0));
        mBinding.paymentDetailLayout.tvStrikeDeliveryCharges.setPaintFlags(mBinding.paymentDetailLayout.tvStrikeDeliveryCharges.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        return cartDetails != null ? CommonUtils.getPriceInFormat(cartDetails.getShippingChargesFinal().doubleValue()) : "";
    }

    public String setDiscountLabel() {
        return netmedsPaymentProcess.getIntentFromDiagnostic() != null && netmedsPaymentProcess.getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC) ?
                context.getResources().getString(R.string.text_diagnostic_discount) : context.getResources().getString(R.string.text_netmeds_discount);
    }

    public String netmedsDiscount() {
        if (isFromPayment()) {
            double netmedsDiscount = netmedsPaymentProcess.getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_COUPON);
            mBinding.paymentDetailLayout.llNetmedsDiscount.setVisibility(netmedsDiscount != 0 ? View.VISIBLE : View.GONE);
            return CommonUtils.getPriceInFormatWithHyphen(netmedsDiscount);
        } else if (netmedsPaymentProcess.getIntentFromDiagnostic() != null && netmedsPaymentProcess.getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC)) {
            return String.format(Locale.getDefault(), "₹-%.2f", getDiagnosticDiscountAmount());
        } else {
            return String.format(Locale.getDefault(), "₹-%.2f", consultationDiscount());
        }
    }

    public String netmedsCash() {
        if (netmedsPaymentProcess.isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied()) {
            if (DiagnosticHelper.getNmsWalletAmount() > netmedsPaymentProcess.getDiagnosticTotalAmount()) {
                mBinding.paymentDetailLayout.walletLayout.setVisibility((DiagnosticHelper.getNmsWalletAmount() != 0) ? View.VISIBLE : View.GONE);
                mBinding.paymentDetailLayout.tvNmsWalletAmount.setText(CommonUtils.getPriceInFormatWithHyphen(netmedsPaymentProcess.getDiagnosticTotalAmount()));
                return CommonUtils.getPriceInFormat(netmedsPaymentProcess.getDiagnosticTotalAmount());
            }
            mBinding.paymentDetailLayout.walletLayout.setVisibility((DiagnosticHelper.getNmsWalletAmount() != 0) ? View.VISIBLE : View.GONE);
            return CommonUtils.getPriceInFormatWithHyphen(DiagnosticHelper.getNmsWalletAmount());
        }
        double usedWalletAmount = netmedsPaymentProcess.getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_WALLET);
        PaymentHelper.setIsWalletApplied(usedWalletAmount != 0);
        mBinding.paymentDetailLayout.walletLayout.setVisibility(usedWalletAmount != 0 ? View.VISIBLE : View.GONE);
        return CommonUtils.getPriceInFormatWithHyphen(usedWalletAmount);
    }

    public String voucherDiscount() {
        double usedVoucherDiscount = netmedsPaymentProcess.getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_VOUCHER);
        PaymentHelper.setIsVoucherApplied(usedVoucherDiscount != 0);
        mBinding.paymentDetailLayout.voucherLayout.setVisibility(usedVoucherDiscount != 0 ? View.VISIBLE : View.GONE);
        return CommonUtils.getPriceInFormatWithHyphen(usedVoucherDiscount);
    }

    public boolean showGrandTotal() {
        return true;
    }

    public String setGrandTotalAmount() {
        if (isFromPayment()) {
            netmedsPaymentProcess.setGrandTotal(cartDetails != null ? cartDetails.getNetPayableAmount().doubleValue() : 0d);
            return cartDetails != null ? CommonUtils.getPriceInFormat(cartDetails.getNetPayableAmount()) : "";
        } else if (!TextUtils.isEmpty(netmedsPaymentProcess.getIntentFromDiagnostic()) && netmedsPaymentProcess.getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC)) {
            if (DiagnosticHelper.isNMSCashApplied()) {
                return String.format(Locale.getDefault(), "₹%.2f", DiagnosticHelper.getremainingPaidAmount());
            } else {
                netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getDiagnosticTotalAmount());
                return String.format(Locale.getDefault(), "₹%.2f", netmedsPaymentProcess.getDiagnosticTotalAmount());
            }
        } else {
            netmedsPaymentProcess.setGrandTotal(netmedsPaymentProcess.getConsultationTotalAmount());
            return String.format(Locale.getDefault(), "₹%.2f", netmedsPaymentProcess.getConsultationTotalAmount());
        }
    }

    public void setPaymentSplit() {
        if (isFromPayment()) {
            mPaymentInterface.vmShowProgress();
            initiateAPICall(SHOW_SPLIT_PAYMENT_DETAILS_AFTER_GET_CART_DETAILS);
        } else {
            consultationOrDiagnosticPaymentDetails();
        }
    }

    private void consultationOrDiagnosticPaymentDetails() {
        if (netmedsPaymentProcess.getIntentFromDiagnostic() != null && netmedsPaymentProcess.getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC))
            showDiagnosticPaymentSplit();
        else
            showConsultationPaymentSplit();
    }

    private void showConsultationPaymentSplit() {
        mBinding.paymentDetailLayout.mrpTotal.setText(String.format(Locale.getDefault(), "₹%.2f", consultationMrp()));
        mBinding.paymentDetailLayout.netmedsDiscount.setText(String.format(Locale.getDefault(), "₹-%.2f", consultationDiscount()));
        mBinding.paymentDetailLayout.totalSaving.setText(String.format(Locale.getDefault(), "₹%.2f", consultationDiscount()));
        mBinding.paymentDetailLayout.deliveryChargesLayout.setVisibility(View.GONE);
        mBinding.paymentDetailLayout.additionalDiscountLayout.setVisibility(View.GONE);
        mBinding.paymentDetailLayout.walletLayout.setVisibility(View.GONE);
        mBinding.paymentDetailLayout.voucherLayout.setVisibility(View.GONE);
        showPaymentSplit(View.VISIBLE);
    }

    private double consultationMrp() {
        double mrp;
        if (viewAllOffer()) {
            mrp = getConsultationUnlimitedActualAmount();
        } else {
            if (netmedsPaymentProcess.getConsultationApplyCoupon() != null && !TextUtils.isEmpty(netmedsPaymentProcess.getConsultationApplyCoupon().getActualPrice()))
                mrp = Double.parseDouble(netmedsPaymentProcess.getConsultationApplyCoupon().getActualPrice());
            else
                mrp = netmedsPaymentProcess.getGrandTotal();
        }
        return mrp;
    }

    private double getConsultationUnlimitedActualAmount() {
        double unlimitedActualAmount = 0.0;
        if (netmedsPaymentProcess.getGrandTotal() > 0) {
            unlimitedActualAmount = netmedsPaymentProcess.getGrandTotal() / 0.6;
        }
        return unlimitedActualAmount;
    }

    private double consultationDiscount() {
        double discount = 0.0;
        if (viewAllOffer()) {
            discount = getConsultationUnlimitedDiscountAmount();
        } else if (netmedsPaymentProcess.getConsultationApplyCoupon() != null && !TextUtils.isEmpty(netmedsPaymentProcess.getConsultationApplyCoupon().getDiscount())) {
            discount = Double.parseDouble(netmedsPaymentProcess.getConsultationApplyCoupon().getDiscount());
        }
        return discount;
    }

    private double getConsultationUnlimitedDiscountAmount() {
        double unlimitedDiscount = 0.0;
        if (netmedsPaymentProcess.getGrandTotal() > 0) {
            unlimitedDiscount = getConsultationUnlimitedActualAmount() - netmedsPaymentProcess.getGrandTotal();
        }
        return unlimitedDiscount;
    }

    private ApplyCoupon getConsultationCoupon() {
        return netmedsPaymentProcess.getConsultationApplyCoupon() == null ? new ApplyCoupon() : netmedsPaymentProcess.getConsultationApplyCoupon();
    }

    public String setConsultationCouponCode() {
        setConsultationCouponSelection();
        return getConsultationCoupon() != null && !TextUtils.isEmpty(getConsultationCoupon().getCode()) ? getConsultationCoupon().getCode() : "";
    }

    private void setConsultationCouponSelection() {
        boolean isCouponApplies = getConsultationCoupon() != null && !TextUtils.isEmpty(getConsultationCoupon().getCode());
        mBinding.consultationCouponView.couponCheckBox.setChecked(isCouponApplies);
        mBinding.consultationCouponView.couponCheckBox.setEnabled(!isCouponApplies);
    }

    public String setConsultationCouponOfferMsg() {
        return getConsultationCoupon() != null && !TextUtils.isEmpty(getConsultationCoupon().getDescription()) ? getConsultationCoupon().getDescription() : "";
    }

    public boolean setConsultationOfferMsgVisibility() {
        return !TextUtils.isEmpty(getConsultationCoupon().getDescription());
    }

    public String setConsultationDiscount() {
        return String.format(Locale.getDefault(), "%s ₹%.2f", context.getResources().getString(R.string.text_you_save), consultationDiscount());
    }

    public boolean showConsultationDiscount() {
        return consultationDiscount() == 0.0;
    }

    public boolean voucherVisibility() {
        if (netmedsPaymentProcess.isFromConsultation() || netmedsPaymentProcess.isFromDiagnostic())
            return true;
        else if (SubscriptionHelper.getInstance().isSubscriptionFlag()) return true;
        return isPrimeProductOnlyAvailableInCart();
    }

    private void vmDismissLoader() {
        mPaymentInterface.vmDismissProgress();
    }

    public void getCouponList() {
        if (!netmedsPaymentProcess.isConsultationCouponListOpen()) {
            netmedsPaymentProcess.setConsultationCouponListOpen(true);
            Map<String, String> header = new HashMap<>();
            header.put(ConsultationConstant.KEY_SPECS, getConsultationSpecialization());
            mPaymentInterface.vmShowProgress();
            ConsultationServiceManager.getInstance().getAllCoupon(this, header);
        }
    }

    private String getVoucher() {
        return cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedGeneralVouchers()) ? cartDetails.getAppliedGeneralVouchers() : !TextUtils.isEmpty(cartDetails != null ? cartDetails.getAppliedIhoVouchers() : null) ? cartDetails.getAppliedIhoVouchers() : "";
    }

    public boolean viewAllOffer() {
        return netmedsPaymentProcess.isConsultationSelectedPlan();
    }


    public void setCODPaymentDetails(PaymentGatewaySubCategory codPaymentDetails) {
        this.cODPaymentDetails = codPaymentDetails;
    }

    public PaymentGatewaySubCategory getCODPaymentDetails() {
        return cODPaymentDetails;
    }

    private void sendDiagnosticPurchaseEvent() {
        ProductAction productAction = new ProductAction(mBasePreference.isDiagnosticFirstOrder() ? context.getResources().getString(R.string.txt_purchase_first_event_diagnostic) : context.getResources().getString(R.string.txt_purchase_event_diagnostic))
                .setTransactionId(netmedsPaymentProcess.getOrderId())
                .setTransactionAffiliation(GoogleAnalyticsHelper.TRANSACTION_AFFILIATION)
                .setTransactionRevenue(netmedsPaymentProcess.getGrandTotal())
                .setTransactionTax(0)
                .setTransactionShipping(netmedsPaymentProcess.getShippingAmount());

        List<TuneEventItem> matItemList = new ArrayList<>();

        if (getOrderTestList() != null && getOrderTestList().size() > 0) {
            for (Test test : getOrderTestList()) {
                Product product = new Product()
                        .setId(test.getTestId())
                        .setName(test.getTestName())
                        .setCategory(test.getType())
                        .setPrice(Double.parseDouble(test.getPriceInfo().getFinalPrice()))
                        .setQuantity(1);

                GoogleAnalyticsHelper.getInstance().postTransactionEvent(context, new HitBuilders.ScreenViewBuilder()
                        .addProduct(product)
                        .setProductAction(productAction));

                matItemList.add(new TuneEventItem(test.getTestName()) // Required
                        .withQuantity(1) // Rest are optional
                        .withUnitPrice(Double.parseDouble(test.getPriceInfo().getOfferedPrice()))
                        .withRevenue(Double.parseDouble(test.getPriceInfo().getFinalPrice()))
                        .withAttribute1(test.getTestId()));
            }
        }
        MATHelper.getInstance().purchaseMATEvent(mBasePreference, matItemList, netmedsPaymentProcess.getGrandTotal(), netmedsPaymentProcess.getOrderId(),
                mBasePreference.isDiagnosticFirstOrder() ?
                        context.getResources().getString(R.string.txt_first_order_yes) :
                        context.getResources().getString(R.string.txt_first_order_no)
                , MATHelper.MIXED_ORDER, context.getResources().getString(R.string.txt_from_diagnostic));
    }


    public void sendToEcommerceTracker(String isAppOrderFirst) {
        boolean non_prescription = false;
        boolean prescription = false;

        ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                .setTransactionId(netmedsPaymentProcess.getOrderId())
                .setTransactionAffiliation(GoogleAnalyticsHelper.TRANSACTION_AFFILIATION)
                .setTransactionRevenue(netmedsPaymentProcess.getGrandTotal())
                .setTransactionTax(0)
                .setTransactionShipping(netmedsPaymentProcess.getShippingAmount());

        if (cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
            List<TuneEventItem> matItemList = new ArrayList<>();
            for (MStarProductDetails itemResult : cartDetails.getLines()) {
                Product product = new Product()
                        .setId(!TextUtils.isEmpty(String.valueOf(itemResult.getProductCode())) ? String.valueOf(itemResult.getProductCode()) : "")
                        .setName(!TextUtils.isEmpty(itemResult.getDisplayName()) ? itemResult.getDisplayName() : "")
                        .setCategory(!TextUtils.isEmpty(itemResult.getProductType()) ? itemResult.getProductType() : "")
                        .setPrice(itemResult.getLineValue().doubleValue())
                        .setQuantity(itemResult.getCartQuantity());

                GoogleAnalyticsHelper.getInstance().postTransactionEvent(context, new HitBuilders.ScreenViewBuilder()
                        .addProduct(product)
                        .setProductAction(productAction));

                matItemList.add(new TuneEventItem(!TextUtils.isEmpty(itemResult.getDisplayName()) ? itemResult.getDisplayName() : "") // Required
                        .withQuantity(itemResult.getCartQuantity()) // Rest are optional
                        .withUnitPrice(itemResult.getLineMrp().doubleValue())
                        .withRevenue(itemResult.getLineValue().doubleValue())
                        .withAttribute1(!TextUtils.isEmpty(String.valueOf(itemResult.getProductCode())) ? String.valueOf(itemResult.getProductCode()) : ""));

                if (!TextUtils.isEmpty(itemResult.getProductType())) {
                    switch (itemResult.getProductType()) {
                        case AppConstant.PRESCRIPTION_STATUS_PENDING:
                            prescription = true;
                            break;
                        case AppConstant.OTC_ORDER:
                            non_prescription = true;
                            break;
                    }
                }
            }
            String orderType = (prescription && non_prescription) ? MATHelper.MIXED_ORDER : (prescription ? MATHelper.PRESCRIPTION : (non_prescription ? MATHelper.NON_PRESCRIPTION : ""));
            //MAT Purchase Event
            MATHelper.getInstance().purchaseMATEvent(mBasePreference, matItemList, netmedsPaymentProcess.getGrandTotal(), netmedsPaymentProcess.getOrderId(), isAppOrderFirst, orderType, "");
        }
    }

    private String getWebEngagePaymentMethod() {
        StringBuilder paymentMethodBuilder = new StringBuilder();
        paymentMethodBuilder.append(getPaymentMethod());

        String webEngagePaymentMethod = "";
        if (PaymentHelper.isVoucherApplied() && PaymentHelper.isNMSCashApplied()) {
            webEngagePaymentMethod = WebEngageHelper.E_VOUCHER + " " + WebEngageHelper.AND + " " + WebEngageHelper.NMS_WALLET;
        } else if (PaymentHelper.isVoucherApplied()) {
            webEngagePaymentMethod = WebEngageHelper.E_VOUCHER;
        } else if (PaymentHelper.isNMSCashApplied() || PaymentHelper.isNMSSuperCashApplied()) {
            webEngagePaymentMethod = WebEngageHelper.NMS_WALLET;
        }

        if (!TextUtils.isEmpty(webEngagePaymentMethod) && webEngagePaymentMethod.length() > 0)
            paymentMethodBuilder.append(" ").append(WebEngageHelper.AND).append(" ").append(webEngagePaymentMethod);
        else
            paymentMethodBuilder.append(webEngagePaymentMethod);

        return paymentMethodBuilder.toString();
    }

    public Bundle getDiagnosticOrderCreationDetail() {
        return diagnosticOrderCreationDetail;
    }

    public void setDiagnosticOrderCreationDetail(Bundle diagnosticOrderCreationDetail) {
        this.diagnosticOrderCreationDetail = diagnosticOrderCreationDetail;
        setDiagnosticOrderDetails();
    }

    private void setDiagnosticOrderDetails() {
        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_LAB))
            setSelectedLab((AvailableLab) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB));

        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_PATIENT_DETAIL))
            setPatientDetail((PatientDetail) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_PATIENT_DETAIL));

        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_CUSTOMER_ADDRESS))
            setDiagnosticAddress((MStarAddressModel) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS));

        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_TIME_SLOT))
            setSlotTime((SlotTime) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_TIME_SLOT));
    }

    public void diagnosticOrderCreation() {
        initiateAPICall(DiagnosticServiceManager.DIAGNOSTIC_ORDER_CREATION);
    }

    private DiagnosticOrderCreateRequest getDiagnosticOrderCreate(boolean isOrderCreate) {
        DiagnosticOrderCreateRequest orderCreateRequest = new DiagnosticOrderCreateRequest();
        orderCreateRequest.setUserId(getDiagnosticUserId() != null ? getDiagnosticUserId() : "");
        DiagnosticOrderDetail diagnosticOrderDetail = new DiagnosticOrderDetail();
        diagnosticOrderDetail.setLabDescription(getLabDetails());
        diagnosticOrderDetail.setTestList(getOrderTestList());
        orderCreateRequest.setOrderDetail(diagnosticOrderDetail);
        if (getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO)) {
            orderCreateRequest.setLabId(getLabDescription().getPid());
        } else {
            orderCreateRequest.setLabId(getLabId());
        }
        orderCreateRequest.setType(getTestType());
        orderCreateRequest.setAmount(netmedsPaymentProcess.getDiagnosticTotalAmount());
        orderCreateRequest.setHardCopy("N");
        orderCreateRequest.setUserDetails(getDiagnosticUserDetail());
        orderCreateRequest.setPatientDetails(getDiagnosticPatientDetail());
        orderCreateRequest.setSlot(getSlotTime() != null ? getSlotTime() : new SlotTime());
        orderCreateRequest.setPaymentId(isOrderCreate ? "" : getDiagnosticPaymentId() != null ? getDiagnosticPaymentId() : "");
        orderCreateRequest.setOrderId(isOrderCreate ? "" : netmedsPaymentProcess.getDiagnosticOrderId() != null ? netmedsPaymentProcess.getDiagnosticOrderId() : "");
        orderCreateRequest.setTestIdList(getTestIdList());
        orderCreateRequest.setPriceDescription(getPriceDescription());
        orderCreateRequest.setSourceType(DiagnosticConstant.SOURCE_TYPE);
        if (netmedsPaymentProcess.isFromDiagnostic() && netmedsPaymentProcess.getDiagnosticCoupon() != null) {
            orderCreateRequest.setCoupon(netmedsPaymentProcess.getDiagnosticCoupon());
        }

        return orderCreateRequest;
    }

    public List<Test> getTestList() {
        return getSelectedLab() != null && getSelectedLab().getTestList() != null && getSelectedLab().getTestList().size() > 0 ? getSelectedLab().getTestList() : new ArrayList<Test>();
    }

    private String getTestType() {
        return getTestList().size() > 0 ? getTestList().get(0).getType() : "";
    }

    private String getTestCategory() {
        return getTestList().size() > 0 ? getTestList().get(0).getCategory() : "";
    }

    private String labPayMode() {
        if (!getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO)
                || getSelectedLab() == null || getSelectedLab().getLabDescription() == null || TextUtils.isEmpty(getSelectedLab().getLabDescription().getPayMode()))
            return "";
        return getSelectedLab().getLabDescription().getPayMode();
    }

    private int getcId() {
        if (!getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO) || getSelectedLab() == null || getSelectedLab().getLabDescription() == null)
            return 0;
        return getSelectedLab().getLabDescription().getcId();
    }

    private LabDescription getLabDescription() {
        return getSelectedLab() != null && getSelectedLab().getLabDescription() != null ? getSelectedLab().getLabDescription() : new LabDescription();
    }

    public PriceDescription getPriceDescription() {
        return getSelectedLab() != null && getSelectedLab().getPriceDescription() != null ? getSelectedLab().getPriceDescription() : new PriceDescription();
    }

    private int getLabId() {
        return getLabDescription().getId();
    }

    private int getPid() {
        return getLabDescription().getPid();
    }

    private LabDescription getLabDetails() {
        String labName = !TextUtils.isEmpty(getLabDescription().getLabName()) ? getLabDescription().getLabName() : "";
        String labLogo = !TextUtils.isEmpty(getLabDescription().getLogo()) ? getLabDescription().getLogo() : "";
        String shortDetail = !TextUtils.isEmpty(getLabDescription().getShortDetails()) ? getLabDescription().getShortDetails() : "";
        String rating = !TextUtils.isEmpty(getLabDescription().getRating()) ? getLabDescription().getRating() : "";
        LabDescription labDetails = new LabDescription();
        if (getTestCategory().equals(DiagnosticConstant.CATEGORY_RADIO) && getLabDescription() != null
                && getLabDescription().getLabAddressList() != null && getLabDescription().getLabAddressList().size() > 0) {
            LabAddress labAddress = new LabAddress();
            labAddress.setFullAddress(getLabDescription().getLabAddressList().get(0).getFullAddress());
            ArrayList<LabAddress> labAddresses = new ArrayList<>();
            labAddresses.add(labAddress);
            labDetails.setLabAddressList(labAddresses);
        }
        labDetails.setId(getLabDescription().getId());
        labDetails.setId(getLabDescription().getId());
        labDetails.setLabName(labName);
        return labDetails;
    }

    private DiagnosticUserDetail getDiagnosticUserDetail() {
        DiagnosticUserDetail userDetail = new DiagnosticUserDetail();
        userDetail.setAddress(getDiagnosticAddress() != null ? getDiagnosticAddress() : new MStarAddressModel());
        return userDetail;
    }

    private PatientDetail getDiagnosticPatientDetail() {
        PatientDetail patientDetails;
        patientDetails = getPatientDetail() != null ? getPatientDetail() : new PatientDetail();
        patientDetails.setAddress(getDiagnosticAddress() != null ? getDiagnosticAddress() : new MStarAddressModel());
        return patientDetails;
    }

    private List<Test> getOrderTestList() {
        List<Test> diagnosticTestList = new ArrayList<>();
        for (Test test : getTestList()) {
            Test testItem = new Test();
            testItem.setTestId(test.getTestId());
            testItem.setTestName(test.getTestName());
            testItem.setPriceInfo(test.getPriceInfo());
            diagnosticTestList.add(testItem);
        }
        return diagnosticTestList;
    }

    private List<String> getTestIdList() {
        List<String> testIdList = new ArrayList<>();
        for (Test test : getTestList())
            testIdList.add(test.getTestId());
        return testIdList;
    }

    private void diagnosticPaymentCreation() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            DiagnosticServiceManager.getInstance().diagnosticPaymentCreation(this, getDiagnosticPaymentCreateRequest(true), mBasePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_CREATION;
    }

    private DiagnosticPaymentCreateRequest getDiagnosticPaymentCreateRequest(boolean isPaymentCreation) {
        DiagnosticPaymentCreateRequest paymentCreateRequest = new DiagnosticPaymentCreateRequest();
        paymentCreateRequest.setPaymentId(isPaymentCreation ? "" : getDiagnosticPaymentId());
        paymentCreateRequest.setOrderId(netmedsPaymentProcess.getDiagnosticOrderId());
        if (netmedsPaymentProcess.isFromDiagnostic() && netmedsPaymentProcess.getDiagnosticCoupon() != null && netmedsPaymentProcess.getDiagnosticTotalAmount() == 0) {
            paymentCreateRequest.setPg(context.getResources().getString(R.string.payment_mode_applied_coupon));
        } else {
            paymentCreateRequest.setPg(isPaymentCreation ? "" : netmedsPaymentProcess.getPaymentGatewaySubCategory() != null ? getPaymentMethod() : context.getResources().getString(R.string.payment_id_card));
        }
        paymentCreateRequest.setStatus(isPaymentCreation ? DiagnosticConstant.PENDING : DiagnosticConstant.SUCCESS);
        paymentCreateRequest.setAmount(netmedsPaymentProcess.getDiagnosticTotalAmount());
        DiagnosticPaymentGatewayResponse diagnosticPaymentGatewayResponse = new DiagnosticPaymentGatewayResponse();
        diagnosticPaymentGatewayResponse.setStatus(isPaymentCreation ? "" : new Gson().toJson(netmedsPaymentProcess.getTransactionLogPaymentResponse()));
        paymentCreateRequest.setPaymentGatewayResponse(diagnosticPaymentGatewayResponse);
        paymentCreateRequest.setUserId(getDiagnosticUserId() != null ? getDiagnosticUserId() : "");
        if (netmedsPaymentProcess.isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied()) {
            paymentCreateRequest.setPaymentMode(context.getResources().getString(R.string.payment_mode_prepaid));
            paymentCreateRequest.setNmscash(true);
            paymentCreateRequest.setNmscash_amount(DiagnosticHelper.getNmsWalletAmount() > netmedsPaymentProcess.getDiagnosticTotalAmount() ? netmedsPaymentProcess.getDiagnosticTotalAmount() : DiagnosticHelper.getNmsWalletAmount());
            paymentCreateRequest.setPaid_amount(DiagnosticHelper.getremainingPaidAmount());
        } else if (netmedsPaymentProcess.isFromDiagnostic() && netmedsPaymentProcess.getPaymentGatewaySubCategory() != null && netmedsPaymentProcess.getPaymentGatewaySubCategory().getId().equalsIgnoreCase(PaymentConstants.CASH_ON_DELIVERY)) {
            paymentCreateRequest.setPaymentMode(context.getResources().getString(R.string.payment_mode_postPaid));
        } else {
            paymentCreateRequest.setPaymentMode(context.getResources().getString(R.string.payment_mode_prepaid));
        }
        return paymentCreateRequest;
    }

    public void diagnosticPaymentUpdate() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            mPaymentInterface.vmShowLoader();
            DiagnosticServiceManager.getInstance().diagnosticPaymentUpdate(this, getDiagnosticPaymentCreateRequest(false), DiagnosticConstant.SUCCESS, mBasePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_UPDATE;
    }

    private void diagnosticOrderUpdate() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            DiagnosticServiceManager.getInstance().diagnosticOrderUpdate(this, getDiagnosticOrderCreate(false), mBasePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_ORDER_UPDATE;
    }

    public void diagnosticPaymentFailureUpdate() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            mPaymentInterface.vmShowLoader();
            DiagnosticServiceManager.getInstance().diagnosticPaymentUpdate(this, getDiagnosticPaymentFailureUpdateRequest(), DiagnosticConstant.FAILURE, mBasePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.DIAGNOSTIC_PAYMENT_FAILURE_UPDATE;
    }

    private DiagnosticPaymentCreateRequest getDiagnosticPaymentFailureUpdateRequest() {
        DiagnosticPaymentCreateRequest paymentUpdateFailureRequest = new DiagnosticPaymentCreateRequest();
        paymentUpdateFailureRequest.setPaymentId(getDiagnosticPaymentId());
        paymentUpdateFailureRequest.setOrderId(netmedsPaymentProcess.getDiagnosticOrderId());
        paymentUpdateFailureRequest.setPg(netmedsPaymentProcess.getPaymentGatewaySubCategory() != null ? getPaymentMethod() : "Card");
        paymentUpdateFailureRequest.setStatus(DiagnosticConstant.FAILURE);
        paymentUpdateFailureRequest.setAmount(netmedsPaymentProcess.getDiagnosticTotalAmount());
        DiagnosticPaymentGatewayResponse diagnosticPaymentGatewayResponse = new DiagnosticPaymentGatewayResponse();
        diagnosticPaymentGatewayResponse.setStatus(new Gson().toJson(netmedsPaymentProcess.getTransactionLogPaymentResponse()));
        paymentUpdateFailureRequest.setPaymentGatewayResponse(diagnosticPaymentGatewayResponse);
        paymentUpdateFailureRequest.setUserId(getDiagnosticUserId() != null ? getDiagnosticUserId() : "");

        return paymentUpdateFailureRequest;
    }

    private void showDiagnosticPaymentSplit() {
        mBinding.paymentDetailLayout.mrpTotal.setText(String.format(Locale.getDefault(), "₹%.2f", getOfferedPrice()));
        mBinding.paymentDetailLayout.netmedsDiscount.setText(String.format(Locale.getDefault(), "₹-%.2f", getDiagnosticDiscountAmount()));
        mBinding.paymentDetailLayout.totalSaving.setText(String.format(Locale.getDefault(), "₹%.2f", getDiagnosticDiscountAmount()));
        mBinding.paymentDetailLayout.deliveryChargesLayout.setVisibility(View.GONE);
        mBinding.paymentDetailLayout.additionalDiscountLayout.setVisibility(View.GONE);
        if (netmedsPaymentProcess.isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied()) {
            mBinding.paymentDetailLayout.walletLayout.setVisibility(View.VISIBLE);
            netmedsCash();
        } else {
            mBinding.paymentDetailLayout.walletLayout.setVisibility(View.GONE);
        }
        mBinding.paymentDetailLayout.voucherLayout.setVisibility(View.GONE);
        showPaymentSplit(View.VISIBLE);
    }

    public AvailableLab getSelectedLab() {
        return selectedLab;
    }

    public void setSelectedLab(AvailableLab selectedLab) {
        this.selectedLab = selectedLab;
    }

    public PatientDetail getPatientDetail() {
        return patientDetail;
    }

    public void setPatientDetail(PatientDetail patientDetail) {
        this.patientDetail = patientDetail;
    }

    public SlotTime getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(SlotTime slotTime) {
        this.slotTime = slotTime;
    }

    public MStarAddressModel getDiagnosticAddress() {
        return diagnosticAddress;
    }

    public void setDiagnosticAddress(MStarAddressModel diagnosticAddress) {
        this.diagnosticAddress = diagnosticAddress;
    }

    public String getDiagnosticUserId() {
        return diagnosticUserId;
    }

    public void setDiagnosticUserId(String diagnosticUserId) {
        this.diagnosticUserId = diagnosticUserId;
    }

    private String getDiagnosticPaymentId() {
        return diagnosticPaymentId;
    }

    private void setDiagnosticPaymentId(String diagnosticPaymentId) {
        this.diagnosticPaymentId = diagnosticPaymentId;
    }

    private String getDiagnosticUpdatePaymentType() {
        return diagnosticUpdatePaymentType;
    }

    private void setDiagnosticUpdatePaymentType(String diagnosticUpdatePaymentType) {
        this.diagnosticUpdatePaymentType = diagnosticUpdatePaymentType;
    }

    private double getDiagnosticDiscountAmount() {
        return getPriceDescription() != null && !TextUtils.isEmpty(getPriceDescription().getDiscountAmount()) ? Double.parseDouble(getPriceDescription().getDiscountAmount()) : 0.0;
    }

    private boolean showDiagnosticDiscountAmount() {
        return getDiagnosticDiscountAmount() == 0.0;
    }

    private double getOfferedPrice() {
        return getPriceDescription() != null && !TextUtils.isEmpty(getPriceDescription().getOfferedPrice()) ? Double.parseDouble(getPriceDescription().getOfferedPrice()) : 0.0;
    }

    private void checkPrimeProduct() {
        MstarPrimeProductResult primeProductResult = new Gson().fromJson(mBasePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
        if (cartDetails != null) {
            if (cartDetails.getLines() == null || cartDetails.getLines().size() == 0)
                return;
            for (MStarProductDetails itemResult : cartDetails.getLines()) {
                if (primeProductResult != null && primeProductResult.getPrimeProductCodesList() != null && primeProductResult.getPrimeProductCodesList().contains(String.valueOf(itemResult.getProductCode())))
                    isPrimeProduct = true;
                else
                    isNonPrimeProduct = true;
            }
        }
    }

    private void getInstalledUpiApps(Context context) {
        Intent intent = new Intent();
        intent.setData(Uri.parse("upi://pay"));
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> resolveInfo = packageManager.queryIntentActivities(intent, 0);
        for (ResolveInfo info : resolveInfo) {
            if (info.activityInfo != null && info.activityInfo.applicationInfo != null && info.activityInfo.applicationInfo.packageName != null && info.activityInfo.applicationInfo.packageName.equals(PaymentConstants.PAY_TM_UPI_PACKAGE))
                setPayTmUpiActive();
            if (info.activityInfo != null && info.activityInfo.applicationInfo != null && info.activityInfo.applicationInfo.packageName != null && info.activityInfo.applicationInfo.packageName.equals(PaymentConstants.GOOGLE_PAY_PACKAGE))
                setGooglePayActive();
            if (info.activityInfo != null && info.activityInfo.applicationInfo != null && info.activityInfo.applicationInfo.packageName != null && info.activityInfo.applicationInfo.packageName.equals(PaymentConstants.PHONE_PE_PACKAGE))
                netmedsPaymentProcess.setPhonePeActive(true);
        }
    }

    private String productType() {
        return (isPrimeProduct && isNonPrimeProduct) ? PaymentConstants.MIXED_PRODUCT : (isPrimeProduct ? PaymentConstants.PRIME_PRODUCT : (isNonPrimeProduct ? PaymentConstants.NON_PRIME_PRODUCT : ""));
    }

    public boolean isPrimeProductOnly() {
        return productType().equals(PaymentConstants.PRIME_PRODUCT);
    }

    public boolean isMixedProduct() {
        return productType().equals(PaymentConstants.MIXED_PRODUCT);
    }

    public boolean isNonPrimeProductOnly() {
        return productType().equals(PaymentConstants.NON_PRIME_PRODUCT);
    }

    private List<MStarProductDetails> getWebEngageProductDetails() {
        return webEngageProductDetails;
    }

    public void setWebEngageProductDetails(List<MStarProductDetails> webEngageProductDetails) {
        this.webEngageProductDetails = webEngageProductDetails;
    }

    private String getConsultationSpecialization() {
        return consultationSpecialization;
    }

    void setConsultationSpecialization(String consultationSpecialization) {
        this.consultationSpecialization = consultationSpecialization;
    }

    private boolean isFromPaymentFailure() {
        return fromPaymentFailure;
    }

    private void setFromPaymentFailure(boolean fromPaymentFailure) {
        this.fromPaymentFailure = fromPaymentFailure;
    }

    private boolean isFormVoucherRemoval() {
        return formVoucherRemoval;
    }

    private void setFormVoucherRemoval(boolean formVoucherRemoval) {
        this.formVoucherRemoval = formVoucherRemoval;
    }

    public String getCartId() {
        return netmedsPaymentProcess.isFromDiagnostic() ? String.valueOf(mBasePreference.getMstarDiagnosticCartId()) : cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public boolean isPayTmUpiActive() {
        return isPayTmUpiActive;
    }

    private void setPayTmUpiActive() {
        isPayTmUpiActive = true;
    }

    public boolean isGooglePayActive() {
        return isGooglePayActive;
    }

    private void setGooglePayActive() {
        isGooglePayActive = true;
    }

    private boolean isFullNMSCashApplied() {
        return mBinding.nmsCashCheckBox.isChecked() && cartDetails.getNetPayableAmount().compareTo(BigDecimal.ZERO) == 0 && mBinding.paymentDetailLayout.voucherText.getText().toString().equalsIgnoreCase(getApplication().getResources().getString(R.string.voucher_note));
    }

    public interface PaymentInterface {
        void setPaymentListAdapter(List<PaymentGatewayList> list, boolean isPrime, boolean isPayTmUpiActive, boolean isGooglePayActive);

        void initiateJusPayTransaction(JusPayCreateOrderAndOrderStatusResponse response);

        void navigateToPaymentFailurePage(int jusPayOrderStatus, PaymentGatewaySubCategory primeProductOnly, boolean productOnly);

        void vmShowProgress();

        void vmShowLoader();

        void vmDismissProgress();

        void initOtpView(JusPayCreateWalletResponse createWalletResponse, PaymentGatewaySubCategory paymentGatewaySubCategory, Bundle jusPayCreateWalletRequest, String cartId);

        Bundle getNewCardDetails();

        void setError(String errorMessage);

        void redirectToPayPalWebView(String orderId, String redirectUrl);

        void initPayTmOtpView(String response, PaymentGatewaySubCategory paymentGatewaySubCategory, PayTmAddMoneyToWalletParams payTmOtpViewParam, String cartId);

        void navigateToSuccessPage(String completeOrderResponse);

        void voucherView(List<AppliedVoucher> voucherList);

        void showCouponListView(List<ConsultationCoupon> consultationCouponList, MessageAdapterListener listener);

        void payTmAddMoney(PayTmAddMoneyToWalletParams intentParam, String cartId);

        void consultationRedirection(ConsultationEvent consultationRedirection);

        void setAmazonPayBalanceLinkStatus();

        void clearPaymentsFragment();

        void vmOrderSuccessActivity(String completeOrderResponse);

        String getSFlagForJusPayCreateOrder();

        void backToOrderReviewPage();

        void executeBindingForShowPaymentDetails();

        Context getContext();

        void enableAndDisablePlaceOrderButtonInWalletSection(boolean enable);

        void resetPaymentSelection();

        void setNetPayableAmount(double grandTotal);

        void disableOrEnableOtherViewClicks(boolean isEnableOtherViewClicks);
    }
}