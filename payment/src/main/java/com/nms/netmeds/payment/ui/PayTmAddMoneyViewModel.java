package com.nms.netmeds.payment.ui;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityPaytmAddMoneyBinding;
import com.nms.netmeds.payment.model.PayTmTransactionStatusResponse;
import com.nms.netmeds.payment.model.request.PayTmTransactionStatusRequest;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;
import com.nms.netmeds.payment.ui.vm.PayTmChecksumResponse;
import com.nms.netmeds.payment.ui.vm.PayTmWithdrawResponse;
import com.nms.netmeds.payment.ui.vm.PaytmWithdrawRequest;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PayTmAddMoneyViewModel extends AppViewModel {

    private PayTmAddMoneyToWalletParams intentValue;
    private ActivityPaytmAddMoneyBinding mBinding;
    private AddMoneyListener mListener;
    private final ConfigMap getConfig;
    private String paymentRequest = "";
    private String paymentResponse = "";
    private BasePreference mBasePreference;
    private String payTmCheckSum = "";
    private String cartId;
    private String payTmCheckSumHash = "";
    private String payTmTransactionStatusRequestQuery;
    private boolean isFromConsultationOrDiagnostic;
    private String diagnosticOrConsultationOrderId = "";
    private double dcTransactionAmount = 0;

    public PayTmAddMoneyViewModel(@NonNull Application application) {
        super(application);
        getConfig = ConfigMap.getInstance();
    }

    public String imageUrl() {
        return getIntentValue() != null && !TextUtils.isEmpty(getIntentValue().getImageUrl()) ? getIntentValue().getImageUrl() : "";
    }

    private static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public void init(PayTmAddMoneyToWalletParams intentValue, String cartId, ActivityPaytmAddMoneyBinding binding, AddMoneyListener listener, BasePreference basePreference) {
        setIntentValue(intentValue);
        this.mBinding = binding;
        this.mListener = listener;
        this.cartId = cartId;
        this.mBasePreference = basePreference;
    }

    private void initiateAPICall(int transactionId) {
        switch (transactionId) {
            case PaymentServiceManager.PAY_TM_ADD_MONEY_CHECKSUM:
                mListener.vmShowProgress();
                PaymentServiceManager.getInstance().payTmAddMoneyChecksum(this,
                        mBasePreference.getMstarBasicHeaderMap(),
                        isFromConsultationOrDiagnostic() ? "" : cartId,
                        getIntentValue().getPaytmUserToken(),
                        getIntentValue().getPaymentUserId(),
                        mBinding.edtTxtAmt.getText().toString(),
                        isFromConsultationOrDiagnostic(),
                        isFromConsultationOrDiagnostic() ? getDiagnosticOrConsultationOrderId() : ""
                );
                break;
            case PaymentServiceManager.PAY_TM_TRANSACTION_CHECKSUM:
                PaymentServiceManager.getInstance().payTmTransactionChecksum(this, mBasePreference.getMstarBasicHeaderMap(), isFromConsultationOrDiagnostic() ? "" : cartId,
                        isFromConsultationOrDiagnostic(),
                        isFromConsultationOrDiagnostic() ? getDiagnosticOrConsultationOrderId() : "");
                break;
            case PaymentServiceManager.PAY_TM_TRANSACTION_CHECKSUM_STATUS:
                PaymentServiceManager.getInstance().payTmTransactionChecksumStatus(this, payTmTransactionStatusRequestQuery);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
                PaymentServiceManager.getInstance().payTmWithdrawFromWallet(this, mBasePreference.getMstarBasicHeaderMap(), getWithdrawFromWalletRequest()
                        , isFromConsultationOrDiagnostic(),
                        isFromConsultationOrDiagnostic() ? getDiagnosticOrConsultationOrderId() : "",
                        isFromConsultationOrDiagnostic() ? "" + getDcTransactionAmount() : "");
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:
                PaymentServiceManager.getInstance().payTmWithdrawDetails(this, getPayTmWithdrawRequest());
                break;
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        setPaymentResponse(data);
        switch (transactionId) {
            case PaymentServiceManager.PAY_TM_ADD_MONEY_CHECKSUM:
                patTmAddMoneyChecksumResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_TRANSACTION_CHECKSUM:
                payTmTransactionCheckSumResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_TRANSACTION_CHECKSUM_STATUS:
                payTmTransactionChecksumStatusResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
                payTmWithdrawFromWalletResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:
                paymentWithdrawDetailsResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        setPaymentResponse(data);
        super.onFailed(transactionId, data);
        mListener.vmDismissProgress();
        mListener.setPaymentResult(false);
    }

    private Map<String, String> getTransactionChecksumRequest() {
        Map<String, String> param = new HashMap<>();
        param.put(PaymentConstants.PAYTM_MID, getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID));
        param.put(PaymentConstants.PAYTM_ORDERID, getIntentValue().getOrderId());
        setPaymentRequest(String.valueOf(param));
        return param;
    }

    private Map<String, String> getWithdrawFromWalletRequest() {
        Map<String, String> param = new HashMap<>();
        param.put(PaymentConstants.API_APPIP, getConfig.getProperty(ConfigConstant.PAYTM_LINK_APPIP));
        param.put(AppConstant.CARTID, isFromConsultationOrDiagnostic() ? "" : cartId);
        param.put(PaymentConstants.API_CUSTOMERID, getIntentValue().getPaymentUserId());
        param.put(PaymentConstants.API_SSOTOKEN, getIntentValue().getPaytmUserToken());
        setPaymentRequest(String.valueOf(param));
        return param;
    }

    private PaytmWithdrawRequest getPayTmWithdrawRequest() {
        PaytmWithdrawRequest request = new PaytmWithdrawRequest();
        request.setAppIP(getConfig.getProperty(ConfigConstant.PAYTM_LINK_APPIP));
        request.setMID(getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID));
        request.setAuthMode(getConfig.getProperty(ConfigConstant.PAYTM_LINK_AUTHMODE));
        request.setTxnAmount(getIntentValue() != null ? isFromConsultationOrDiagnostic() ? String.valueOf(getIntentValue().getOrderAmt()) : BigDecimal.valueOf(getIntentValue().getOrderAmt()).stripTrailingZeros().toPlainString() : "0");
        request.setOrderId(getIntentValue().getOrderId());
        request.setDeviceId(getCustomerDetails().getMobileNo());
        request.setSSOToken(getIntentValue().getPaytmUserToken());
        request.setPaymentMode(getConfig.getProperty(ConfigConstant.PAYTM_LINK_PAYMENTMODE));
        request.setCustId(getIntentValue().getPaymentUserId());
        request.setIndustryType(getPayTmCredentials(PaymentConstants.PAYTM_LINK_INDUSTRY_KEY));
        request.setChannel(getPayTmCredentials(PaymentConstants.PAYTM_LINK_CHANNEL));
        request.setReqType(getConfig.getProperty(ConfigConstant.PAYTM_LINK_WITHDRAW_REQUESTTYPE));
        request.setCurrency(getConfig.getProperty(ConfigConstant.PAYTM_LINK_CURRENCY));
        request.setCheckSum(payTmCheckSumHash);
        setPaymentRequest(new Gson().toJson(request));
        return request;
    }


    private void patTmAddMoneyChecksumResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            PayTmChecksumResponse paytmChecksumResponse = new Gson().fromJson(data, PayTmChecksumResponse.class);
            mListener.initTransaction(paytmChecksumResponse.getChecksumhash());
        } else {
            mListener.setPaymentResult(false);
        }
    }

    private void payTmTransactionCheckSumResponse(String data) {
        PayTmChecksumResponse paytmChecksumResponse = new Gson().fromJson(data, PayTmChecksumResponse.class);
        if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(paytmChecksumResponse.getStatus())) {
            PayTmTransactionStatusRequest request = new PayTmTransactionStatusRequest();
            request.setmId(getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID));
            request.setOrderId(getIntentValue().getOrderId());
            request.setChecksumHash(paytmChecksumResponse.getChecksumhash());
            try {
                payTmTransactionStatusRequestQuery = URLEncoder.encode(new Gson().toJson(request), "utf-8");
                initiateAPICall(PaymentServiceManager.PAY_TM_TRANSACTION_CHECKSUM_STATUS);
            } catch (UnsupportedEncodingException e) {
                initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET);
            }
        } else {
            mListener.setPaymentResult(false);
        }
    }

    private void payTmTransactionChecksumStatusResponse(String data) {
        PayTmTransactionStatusResponse response = new Gson().fromJson(data, PayTmTransactionStatusResponse.class);
        if (response.getTXNID() != null && response.getRESPCODE().equals(PaymentConstants.PAYTM_TRANSACTION_CHECKSUM_STATUS_SUCCESS_CODE))
            initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET);
        else {
            mListener.setPaymentResult(false);
        }
    }

    private void payTmWithdrawFromWalletResponse(String data) {
        final PayTmChecksumResponse paytmChecksumBalanceResponse = new Gson().fromJson(data, PayTmChecksumResponse.class);
        if (paytmChecksumBalanceResponse != null && !TextUtils.isEmpty(paytmChecksumBalanceResponse.getChecksumhash())) {
            this.payTmCheckSumHash = paytmChecksumBalanceResponse.getChecksumhash();
            initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS);
        } else {
            mListener.setPaymentResult(false);
        }
    }


    private void paymentWithdrawDetailsResponse(String data) {
        PayTmWithdrawResponse paytmWithdrawResponse = new Gson().fromJson(data, PayTmWithdrawResponse.class);
        if (paytmWithdrawResponse != null && paytmWithdrawResponse.getTxnId() != null && paytmWithdrawResponse.getResponseCode().equals("01")) {
            if (!TextUtils.isEmpty(paytmWithdrawResponse.getCheckSum()))
                setPayTmCheckSum(paytmWithdrawResponse.getCheckSum());
            mListener.setPaymentResult(true);
        } else {
            mListener.setPaymentResult(false);
        }
    }

    void transactionChecksum() {
        initiateAPICall(PaymentServiceManager.PAY_TM_TRANSACTION_CHECKSUM);
    }

    String getPayTmCredentials(int id) {
        return PaymentUtils.payTmCredentials(id, mBasePreference.getPaymentCredentials());
    }

    private MStarCustomerDetails getCustomerDetails() {
        return new Gson().fromJson(mBasePreference.getCustomerDetails(), MStarCustomerDetails.class);
    }

    public String walletAmount() {
        Double walletAmount = Math.abs(getIntentValue().getOrderAmt() - getIntentValue().getTransactionAmt());
        return walletAmount > 0 ? String.format(Locale.getDefault(), "%.2f", walletAmount) : "0";
    }

    public String orderAmount() {
        return String.format(Locale.getDefault(), "%.2f", getIntentValue().getOrderAmt());
    }

    public String transactionAmount() {
        return String.format(Locale.getDefault(), "%.2f", getIntentValue().getTransactionAmt());
    }

    public void initPayment() {
        String enteredAmount = mBinding.edtTxtAmt.getText() != null ? mBinding.edtTxtAmt.getText().toString() : "";
        if (isNumeric(enteredAmount) && Double.parseDouble(enteredAmount) >= getIntentValue().getTransactionAmt())
            initiateAPICall(PaymentServiceManager.PAY_TM_ADD_MONEY_CHECKSUM);
        else
            mListener.setError(String.format(getApplication().getResources().getString(R.string.text_recharge_amount), String.valueOf(getIntentValue().getTransactionAmt())));
    }

    private PayTmAddMoneyToWalletParams getIntentValue() {
        return intentValue;
    }

    private void setIntentValue(PayTmAddMoneyToWalletParams intentValue) {
        this.intentValue = intentValue;
    }

    String getPaymentRequest() {
        return paymentRequest;
    }

    void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    String getPaymentResponse() {
        return paymentResponse;
    }

    private void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    String getPayTmCheckSum() {
        return payTmCheckSum;
    }

    private void setPayTmCheckSum(String payTmCheckSum) {
        this.payTmCheckSum = payTmCheckSum;
    }

    public boolean isFromConsultationOrDiagnostic() {
        return isFromConsultationOrDiagnostic;
    }

    public void setFromConsultationOrDiagnostic(boolean fromConsultationOrDiagnostic) {
        isFromConsultationOrDiagnostic = fromConsultationOrDiagnostic;
    }

    public String getDiagnosticOrConsultationOrderId() {
        return diagnosticOrConsultationOrderId;
    }

    public void setDiagnosticOrConsultationOrderId(String diagnosticOrConsultationOrderId) {
        this.diagnosticOrConsultationOrderId = diagnosticOrConsultationOrderId;
    }

    public double getDcTransactionAmount() {
        return dcTransactionAmount;
    }

    public void setDcTransactionAmount(double dcTransactionAmount) {
        this.dcTransactionAmount = dcTransactionAmount;
    }


    public interface AddMoneyListener {
        void setError(String error);

        void vmShowProgress();

        void vmDismissProgress();

        void initTransaction(String checksumHash);

        void setPaymentResult(boolean paymentStatus);
    }
}
