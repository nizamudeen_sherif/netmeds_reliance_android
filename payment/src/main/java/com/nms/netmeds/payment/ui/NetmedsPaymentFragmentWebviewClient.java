package com.nms.netmeds.payment.ui;

import in.juspay.godel.ui.JuspayWebView;
import in.juspay.godel.ui.JuspayWebViewClient;
import in.juspay.godel.ui.PaymentFragment;

public class NetmedsPaymentFragmentWebviewClient extends JuspayWebViewClient {


    public NetmedsPaymentFragmentWebviewClient(JuspayWebView juspayWebView, PaymentFragment paymentFragment) {
        super(juspayWebView, paymentFragment);
    }
}
