package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.minkasu.android.twofa.model.Address;
import com.minkasu.android.twofa.model.Config;
import com.minkasu.android.twofa.model.CustomerInfo;
import com.minkasu.android.twofa.model.OrderInfo;
import com.minkasu.android.twofa.sdk.Minkasu2faSDK;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;

import in.juspay.godel.ui.PaymentFragment;

@SuppressLint("ValidFragment")
public class MinKasuFragment extends PaymentFragment {

    private final BasePreference preference;
    private final String orderId;

    public MinKasuFragment(BasePreference preference, String orderId) {
        this.preference = preference;
        this.orderId = orderId;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initMinkasu();
    }

    private void initMinkasu() {
        try {
            MStarCustomerDetails customerDetails = new Gson().fromJson(preference.getCustomerDetails(), MStarCustomerDetails.class);
            CustomerInfo customer = new CustomerInfo();
            customer.setFirstName(customerDetails.getFirstName());
            customer.setLastName(customerDetails.getLastName());
            customer.setEmail(customerDetails.getEmail());
            customer.setPhone("+91" + customerDetails.getMobileNo());
            customer.setAddress(getCustomerAddress());
            Config config = Config.getInstance(PaymentUtils.minKasuCredential(PaymentConstants.MINKASU_MERCHANT_ID, preference.getPaymentCredentials()),
                    PaymentUtils.minKasuCredential(PaymentConstants.MINKASU_ACCESS_TOKEN, preference.getPaymentCredentials()), String.valueOf(customerDetails.getId()), customer);
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setOrderId(orderId);
            config.setOrderInfo(orderInfo);
            Minkasu2faSDK.init(getActivity(), config, getWebView());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Address getCustomerAddress() {
        Address address = new Address();
        MStarAddressModel mStarAddressModel = PaymentHelper.getCustomerAddress();
        if (mStarAddressModel != null) {
            address.setAddressLine1(!TextUtils.isEmpty(mStarAddressModel.getStreet()) ? mStarAddressModel.getStreet() : "");
            address.setAddressLine2(!TextUtils.isEmpty(mStarAddressModel.getLandmark()) ? mStarAddressModel.getLandmark() : "");
            address.setCity(!TextUtils.isEmpty(mStarAddressModel.getCity()) ? mStarAddressModel.getCity() : "");
            address.setState(!TextUtils.isEmpty(mStarAddressModel.getState()) ? mStarAddressModel.getState() : "");
            address.setCountry("" + mStarAddressModel.getCustomerId());
            address.setZipCode(!TextUtils.isEmpty(mStarAddressModel.getPin()) ? mStarAddressModel.getPin() : "");
        }
        return address;
    }
}
