package com.nms.netmeds.payment.ui;

import android.app.Application;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.nms.netmeds.payment.ui.vm.ModifiedNetBankingList;
import com.nms.netmeds.payment.ui.vm.ModifiedPaymentMethod;
import com.nms.netmeds.payment.ui.vm.NetBankingListResponse;
import com.nms.netmeds.payment.ui.vm.NetBankingPaymentMethods;

import java.util.ArrayList;
import java.util.List;

public class NetBankingViewModel extends AppViewModel {

    private NetBankingListener netBankingListener;
    private Context mContext;
    private ArrayList<NetBankingPaymentMethods> jusPayPaymentMethod = new ArrayList<>();

    public NetBankingViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(NetBankingListener netBankingListener, Context context) {
        this.netBankingListener = netBankingListener;
        this.mContext = context;
        getNetBakingList();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case PaymentServiceManager.JUS_PAY_PAYMENT_METHODS:
                jusPayPaymentMethodResponse(data);
                break;
        }
    }

    private void jusPayPaymentMethodResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            NetBankingListResponse listResponse = new Gson().fromJson(data, NetBankingListResponse.class);
            if (listResponse != null)
                getAlphabeticallySortedList(listResponse);
        }
        netBankingListener.vmDismissProgress();
    }

    private void getAlphabeticallySortedList(NetBankingListResponse listResponse) {

        ArrayList<NetBankingPaymentMethods> methodsArrayList = new ArrayList<>();
        for (NetBankingPaymentMethods paymentMethods : listResponse.getPaymentMethods()) {
            if (paymentMethods.getPaymentMethodType().equalsIgnoreCase("NB")) {
                methodsArrayList.add(paymentMethods);
            }
        }
        setJusPayPaymentMethod(methodsArrayList);
        setNetBankingList(methodsArrayList);
    }

    private void setNetBankingList(ArrayList<NetBankingPaymentMethods> methodsArrayList) {
        netBankingListener.setListAdapter(getModifiedNetBankingList(methodsArrayList));
    }

    private ModifiedNetBankingList getModifiedNetBankingList(ArrayList<NetBankingPaymentMethods> methodsArrayList) {

        ModifiedPaymentMethod modifiesPaymentMethod;

        ModifiedNetBankingList netBankingList = new ModifiedNetBankingList();

        List<ModifiedPaymentMethod> nmsModifiedNetBankingList = new ArrayList<>();
        String alphaList[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        for (String alpha : alphaList) {
            modifiesPaymentMethod = getSortedList(methodsArrayList, alpha);
            if (modifiesPaymentMethod.getArray().size() > 0) {
                nmsModifiedNetBankingList.add(modifiesPaymentMethod);
            }
        }
        netBankingList.setPaymentMethods((nmsModifiedNetBankingList));
        return netBankingList;
    }

    private ModifiedPaymentMethod getSortedList(ArrayList<NetBankingPaymentMethods> methodsArrayList, String string) {
        ModifiedPaymentMethod nmsModifiesPaymentMethod = new ModifiedPaymentMethod();
        nmsModifiesPaymentMethod.setTitle(string);
        List<NetBankingPaymentMethods> list = new ArrayList<>();

        for (int i = 0; i < methodsArrayList.size(); i++) {
            NetBankingPaymentMethods consultationPaymentMethods = methodsArrayList.get(i);
            if (consultationPaymentMethods.getDescription().substring(0, 1).equalsIgnoreCase(string)) {
                list.add(setMethod(consultationPaymentMethods));
            }
        }
        nmsModifiesPaymentMethod.setArray(list);
        return nmsModifiesPaymentMethod;
    }

    private NetBankingPaymentMethods setMethod(NetBankingPaymentMethods consultationPaymentMethods) {
        NetBankingPaymentMethods modified = new NetBankingPaymentMethods();
        modified.setPaymentMethodType(consultationPaymentMethods.getPaymentMethodType());
        modified.setPaymentMethod(consultationPaymentMethods.getPaymentMethod());
        modified.setDescription(consultationPaymentMethods.getDescription());
        return modified;
    }

    private void getNetBakingList() {
        PaymentServiceManager.getInstance().netBankingList(this, getJusPayCredentials());
    }

    private String getJusPayCredentials() {
        return PaymentUtils.jusPayCredential(PaymentConstants.JUS_PAY_PAYMENT_METHODS, BasePreference.getInstance(mContext).getPaymentCredentials());
    }

    public TextWatcher getFilteredBankList() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString()) && s.toString().length() > 0) {
                    getFilteredBankList(s.toString());
                } else {
                    netBankingListener.setListAdapter(getModifiedNetBankingList(getJusPayPaymentMethod()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private void setFilteredBankList(ModifiedNetBankingList list) {
        netBankingListener.setListAdapter(list);
    }

    void getFilteredBankList(String filterString) {

        ArrayList<NetBankingPaymentMethods> filteredList = new ArrayList<>(getJusPayPaymentMethod());

        ArrayList<NetBankingPaymentMethods> modifiedList = new ArrayList<>();

        if (!TextUtils.isEmpty(filterString)) {
            for (int i = 0; i < filteredList.size(); i++) {
                if (filteredList.get(i).getDescription().toLowerCase().contains(filterString.toLowerCase())) {
                    modifiedList.add(filteredList.get(i));
                }
            }
        }

        setFilteredBankList(getModifiedNetBankingList(modifiedList));
    }

    public ArrayList<NetBankingPaymentMethods> getJusPayPaymentMethod() {
        return jusPayPaymentMethod;
    }

    public void setJusPayPaymentMethod(ArrayList<NetBankingPaymentMethods> jusPayPaymentMethod) {
        this.jusPayPaymentMethod = jusPayPaymentMethod;
    }


    public interface NetBankingListener {

        void showProgress();

        void vmDismissProgress();

        void setListAdapter(ModifiedNetBankingList netBankingList);
    }

}
