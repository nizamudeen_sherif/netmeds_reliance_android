package com.nms.netmeds.payment.model;

import com.google.gson.annotations.SerializedName;

public class PayTmTransactionStatusResponse {
    @SerializedName("TXNID")
    private String tXNID;
    @SerializedName("BANKTXNID")
    private String bANKTXNID;
    @SerializedName("ORDERID")
    private String oRDERID;
    @SerializedName("TXNAMOUNT")
    private String tXNAMOUNT;
    @SerializedName("STATUS")
    private String sTATUS;
    @SerializedName("TXNTYPE")
    private String tXNTYPE;
    @SerializedName("GATEWAYNAME")
    private String gATEWAYNAME;
    @SerializedName("RESPCODE")
    private String rESPCODE;
    @SerializedName("RESPMSG")
    private String rESPMSG;
    @SerializedName("BANKNAME")
    private String bANKNAME;
    @SerializedName("MID")
    private String mID;
    @SerializedName("PAYMENTMODE")
    private String pAYMENTMODE;
    @SerializedName("REFUNDAMT")
    private String rEFUNDAMT;
    @SerializedName("TXNDATE")
    private String tXNDATE;

    public String getTXNID() {
        return tXNID;
    }

    public void setTXNID(String tXNID) {
        this.tXNID = tXNID;
    }

    public String getBANKTXNID() {
        return bANKTXNID;
    }

    public void setBANKTXNID(String bANKTXNID) {
        this.bANKTXNID = bANKTXNID;
    }

    public String getORDERID() {
        return oRDERID;
    }

    public void setORDERID(String oRDERID) {
        this.oRDERID = oRDERID;
    }

    public String getTXNAMOUNT() {
        return tXNAMOUNT;
    }

    public void setTXNAMOUNT(String tXNAMOUNT) {
        this.tXNAMOUNT = tXNAMOUNT;
    }

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public String getTXNTYPE() {
        return tXNTYPE;
    }

    public void setTXNTYPE(String tXNTYPE) {
        this.tXNTYPE = tXNTYPE;
    }

    public String getGATEWAYNAME() {
        return gATEWAYNAME;
    }

    public void setGATEWAYNAME(String gATEWAYNAME) {
        this.gATEWAYNAME = gATEWAYNAME;
    }

    public String getRESPCODE() {
        return rESPCODE;
    }

    public void setRESPCODE(String rESPCODE) {
        this.rESPCODE = rESPCODE;
    }

    public String getRESPMSG() {
        return rESPMSG;
    }

    public void setRESPMSG(String rESPMSG) {
        this.rESPMSG = rESPMSG;
    }

    public String getBANKNAME() {
        return bANKNAME;
    }

    public void setBANKNAME(String bANKNAME) {
        this.bANKNAME = bANKNAME;
    }

    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getPAYMENTMODE() {
        return pAYMENTMODE;
    }

    public void setPAYMENTMODE(String pAYMENTMODE) {
        this.pAYMENTMODE = pAYMENTMODE;
    }

    public String getREFUNDAMT() {
        return rEFUNDAMT;
    }

    public void setREFUNDAMT(String rEFUNDAMT) {
        this.rEFUNDAMT = rEFUNDAMT;
    }

    public String getTXNDATE() {
        return tXNDATE;
    }

    public void setTXNDATE(String tXNDATE) {
        this.tXNDATE = tXNDATE;
    }
}
