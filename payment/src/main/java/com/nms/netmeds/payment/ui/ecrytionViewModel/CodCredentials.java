package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;

public class CodCredentials {

    @SerializedName("key1")
    private String payementMethod;
    @SerializedName("key3")
    private String minimumAmount;
    @SerializedName("key2")
    private String maximumAmount;

    public String getPayementMethod() {
        return payementMethod;
    }

    public void setPayementMethod(String payementMethod) {
        this.payementMethod = payementMethod;
    }

    public String getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(String minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public String getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(String maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

}
