package com.nms.netmeds.payment.ui;

import android.app.Application;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.AppliedVoucher;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.FragmentVoucherBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VoucherViewModel extends AppViewModel {

    private FragmentVoucherBinding mBinding;
    private BasePreference mBasePreference;
    private List<AppliedVoucher> appliedVouchers;
    private VoucherInterface mListener;
    private double grandTotal;
    private boolean isM2Order = false;
    private MStarCartDetails cartDetails;
    private boolean isVoucher = false;
    public boolean isRemoveVoucher = false;
    private boolean isAppliedVoucher = false;
    public String voucherCode;

    VoucherViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(FragmentVoucherBinding binding, BasePreference instance, VoucherInterface listener, List<AppliedVoucher> mVoucherList, double grandTotal, boolean m2Order, MStarCartDetails cartDetails) {
        this.mBinding = binding;
        this.mBasePreference = instance;
        this.mListener = listener;
        this.grandTotal = grandTotal;
        this.isM2Order = m2Order;
        this.cartDetails = cartDetails;
        if (cartDetails != null && (!TextUtils.isEmpty(cartDetails.getAppliedIhoVouchers()) || !TextUtils.isEmpty(cartDetails.getAppliedGeneralVouchers()))) {
            setAppliedVouchers(mVoucherList);
            setVoucherView();
        } else {
            setAppliedVouchers(new ArrayList<AppliedVoucher>());
            setVoucherView();
        }

    }

    void initApiCall(int id, Map<String, String> mstarBasicHeaderMap, Integer currentCartId) {
        mListener.vmShowProgress();
        switch (id) {
            case APIServiceManager.MSTAR_APPLY_VOUCHER:
                APIServiceManager.getInstance().mStarApplyVoucher(this, mBasePreference.getMstarBasicHeaderMap(), mBinding.promoCode.getText().toString(), currentCartId);
                break;
            case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                APIServiceManager.getInstance().mStarUnapplyVoucher(this, mBasePreference.getMstarBasicHeaderMap(), voucherCode, getCurrentCartId());
                break;
        }

    }

    @Override
    public void onSyncData(String data, int transactionId) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_APPLY_VOUCHER:
                applyVoucherResponse(data);
                break;
            case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                removeVoucherResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_APPLY_VOUCHER:
                PaymentHelper.setIsVoucherApplied(false);
                PaymentHelper.setVoucherCode("");
                break;
            case APIServiceManager.MSTAR_INITIATE_CHECKOUT:
                initApiCall(APIServiceManager.MSTAR_APPLY_VOUCHER, mBasePreference.getMstarBasicHeaderMap(), getCurrentCartId());
                break;
            case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                mListener.setError(getApplication().getResources().getString(R.string.text_voucher_error));
                break;
        }
    }

    private void setVoucherView() {
        if (getAppliedVouchers() != null && getAppliedVouchers().size() > 0) {
            setVoucherList();
            if (getAppliedVouchers().size() >= 2 || grandTotal <= 0) {
                mBinding.applyVoucherView.setVisibility(View.GONE);
            } else {
                for (AppliedVoucher voucher : getAppliedVouchers()) {
                    mBinding.applyVoucherView.setVisibility(voucher.getGiftVoucherType().equalsIgnoreCase(PaymentConstants.IHO) ? View.VISIBLE : View.GONE);
                }
            }
        }
    }

    private void setVoucherList() {
        if (getAppliedVouchers() != null && getAppliedVouchers().size() > 0)
            mListener.setVoucherListView(getAppliedVouchers());
    }

    private void removeVoucherResponse(String data) {
        PaymentHelper.setIsVoucherApplied(false);
        PaymentHelper.setVoucherCode("");
        PaymentHelper.setReloadTotalInformation(true);
        mListener.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && !TextUtils.isEmpty(response.getStatus()) && response.getStatus().equalsIgnoreCase(PaymentConstants.SUCCESS)) {
                isAppliedVoucher = false;
                isVoucher = false;
                isRemoveVoucher = false;
                redirectToPayment();
            } else
                mListener.setError(getApplication().getResources().getString(R.string.text_voucher_invalid));
        }
    }

    public void closeView() {
        mListener.dismissDialog();
    }

    public void applyVoucher() {
        if (mBinding.promoCode.getText() != null && !TextUtils.isEmpty(mBinding.promoCode.getText().toString())) {
            isVoucher = true;
            initApiCall(APIServiceManager.MSTAR_APPLY_VOUCHER, mBasePreference.getMstarBasicHeaderMap(), getCurrentCartId());
        } else
            mListener.setError(getApplication().getResources().getString(R.string.text_empty_voucher));
    }

    private void applyVoucherResponse(String data) {
        mListener.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && !TextUtils.isEmpty(response.getStatus()) && response.getStatus().equalsIgnoreCase(PaymentConstants.SUCCESS)) {
                isAppliedVoucher = true;
                isRemoveVoucher = false;
                isVoucher = false;
                PaymentHelper.setIsVoucherApplied(true);
                PaymentHelper.setVoucherCode(mBinding.promoCode.getText() != null && !TextUtils.isEmpty(mBinding.promoCode.getText().toString()) ? mBinding.promoCode.getText().toString() : "");
                redirectToPayment();
            } else if (response != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng())) {
                mListener.setError(response.getReason().getReason_eng());
            } else
                mListener.setError(getApplication().getResources().getString(R.string.text_voucher_invalid));
        }
    }


    private void redirectToPayment() {
        mListener.appliedVoucher(getAppliedVouchers());
        mListener.vmDismissProgress();
        mListener.dismissDialog();
    }

    public boolean isM2Order() {
        return isM2Order;
    }

    private List<AppliedVoucher> getAppliedVouchers() {
        return appliedVouchers;
    }

    private void setAppliedVouchers(List<AppliedVoucher> appliedVouchers) {
        PaymentHelper.setAppliedVoucherList(appliedVouchers);
        this.appliedVouchers = appliedVouchers;
    }

    Integer getCurrentCartId() {
        if (isM2Order) return mBasePreference.getMstarMethod2CartId();
        if (SubscriptionHelper.getInstance().isSubscriptionFlag())
            return SubscriptionHelper.getInstance().getSubscriptionCartId();
        else return PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : null;
    }

    public interface VoucherInterface {
        void dismissDialog();

        void appliedVoucher(List<AppliedVoucher> appliedVoucherList);

        void vmShowProgress();

        void vmDismissProgress();

        void setError(String errorMsg);

        void setVoucherListView(List<AppliedVoucher> appliedVouchers);
    }
}
