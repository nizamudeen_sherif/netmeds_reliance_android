package com.nms.netmeds.payment.ui;

import com.google.gson.annotations.SerializedName;

public class JusPayOrderStatusRequest {
    @SerializedName("order_id")
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}