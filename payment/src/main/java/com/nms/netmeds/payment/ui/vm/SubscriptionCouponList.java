package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionCouponList {
    @SerializedName("couponCode")
    @Expose
    private String couponCode;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("discount")
    @Expose
    private Object discount;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getDiscount() {
        return discount;
    }

    public void setDiscount(Object discount) {
        this.discount = discount;
    }

}