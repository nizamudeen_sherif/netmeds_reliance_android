package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;

public class PayPalGatewayKey {
    @SerializedName("key1")
    private String clientid;
    @SerializedName("key2")
    private String secret;
    @SerializedName("key3")
    private String key3;
    @SerializedName("key4")
    private String returnUrlAndroid;
    @SerializedName("key5")
    private String authTokenUrl;
    @SerializedName("key6")
    private String currency;
    @SerializedName("key7")
    private String paymentUrl;

    public String getClientId() {
        return clientid;
    }

    public void setClientId(String clientid) {
        this.clientid = clientid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getKey3() {
        return key3;
    }

    public void setKey3(String key3) {
        this.key3 = key3;
    }

    public String getReturnUrlAndroid() {
        return returnUrlAndroid;
    }

    public void setReturnUrlAndroid(String returnUrlAndroid) {
        this.returnUrlAndroid = returnUrlAndroid;
    }

    public String getAuthTokenUrl() {
        return authTokenUrl;
    }

    public void setAuthTokenUrl(String authTokenUrl) {
        this.authTokenUrl = authTokenUrl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

}
