package com.nms.netmeds.payment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityLinkWalletBinding;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;

public class LinkWalletActivity extends BaseViewModelActivity<LinkWalletViewModel> implements LinkWalletViewModel.LinkWalletListener {

    private ActivityLinkWalletBinding mBinding;
    private LinkWalletViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_link_wallet);
        toolBarSetUp(mBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        onCreateViewModel();
        mBinding.setViewModel(viewModel);
    }

    @Override
    protected LinkWalletViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(LinkWalletViewModel.class);
        setView();
        setPaymentGateway();
        onRetry(viewModel);
        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION))
            viewModel.setFromConsultationOrDiagnostic(getIntent().getBooleanExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION, false));
        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.KEY_ORDER_ID) && !TextUtils.isEmpty(getIntent().getStringExtra(DiagnosticConstant.KEY_ORDER_ID)))
            viewModel.setDcOrderId(getIntent().getStringExtra(DiagnosticConstant.KEY_ORDER_ID));
        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.KEY_TRANSACTION_AMOUNT))
            viewModel.setDcTransactionAmount(getIntent().getDoubleExtra(DiagnosticConstant.KEY_TRANSACTION_AMOUNT, 0.0));
        return viewModel;
    }

    private void setPaymentGateway() {
        if (isJusPay()) {
            viewModel.initJusPayOtpValidation(mBinding, BasePreference.getInstance(this), getCreateWalletResponse(), getPaymentSubCategory(), isJusPay(), this);
        } else
            viewModel.initPayTmOtpValidation(mBinding, BasePreference.getInstance(this), isJusPay(), this, getCreateWalletResponse(), getPaymentSubCategory(), getPayTmOtpParam(), getCartId());
    }

    private String getCartId() {
        return getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.CART_ID) && !TextUtils.isEmpty(getIntent().getStringExtra(PaymentIntentConstant.CART_ID))
                ? getIntent().getStringExtra(PaymentIntentConstant.CART_ID) : "";
    }

    private boolean isJusPay() {
        return (getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.IS_JUSPAY)) && getIntent().getBooleanExtra(PaymentIntentConstant.IS_JUSPAY, false);
    }

    private String getCreateWalletResponse() {
        return getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.CREATE_WALLET_RESPONSE) && !TextUtils.isEmpty(getIntent().getStringExtra(PaymentIntentConstant.CREATE_WALLET_RESPONSE))
                ? getIntent().getStringExtra(PaymentIntentConstant.CREATE_WALLET_RESPONSE) : "";
    }

    private String getPaymentSubCategory() {
        return getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.PAYMENT_SUB_CATEGORY) && !TextUtils.isEmpty(getIntent().getStringExtra(PaymentIntentConstant.PAYMENT_SUB_CATEGORY))
                ? getIntent().getStringExtra(PaymentIntentConstant.PAYMENT_SUB_CATEGORY) : "";
    }

    private Bundle getJuPayCreateWalletRequest() {
        return getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.CREATE_WALLET_REQUEST)
                ? getIntent().getBundleExtra(PaymentIntentConstant.CREATE_WALLET_REQUEST) : new Bundle();
    }

    private String getPayTmOtpParam() {
        return getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.PAY_TM_OTP_PARAM) && !TextUtils.isEmpty(getIntent().getStringExtra(PaymentIntentConstant.PAY_TM_OTP_PARAM))
                ? getIntent().getStringExtra(PaymentIntentConstant.PAY_TM_OTP_PARAM) : "";
    }


    @Override
    public void initJusPay(String payLoad, int callBackId) {
        in.juspay.godel.PaymentActivity.preFetch(this, getValue(in.juspay.godel.core.PaymentConstants.CLIENT_ID));
        Bundle args = new Bundle();
        args.putString(in.juspay.godel.core.PaymentConstants.MERCHANT_ID, getValue(in.juspay.godel.core.PaymentConstants.MERCHANT_ID));
        args.putString(in.juspay.godel.core.PaymentConstants.CLIENT_ID, getValue(in.juspay.godel.core.PaymentConstants.CLIENT_ID));
        args.putString(in.juspay.godel.core.PaymentConstants.ORDER_ID, getValue(in.juspay.godel.core.PaymentConstants.ORDER_ID));
        args.putString(in.juspay.godel.core.PaymentConstants.AMOUNT, getValue(in.juspay.godel.core.PaymentConstants.AMOUNT));
        args.putString(in.juspay.godel.core.PaymentConstants.TRANSACTION_ID, getValue(in.juspay.godel.core.PaymentConstants.TRANSACTION_ID));
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_ID, getValue(in.juspay.godel.core.PaymentConstants.CUSTOMER_ID));
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_MOBILE, getValue(in.juspay.godel.core.PaymentConstants.CUSTOMER_MOBILE));
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_EMAIL, getValue(in.juspay.godel.core.PaymentConstants.CUSTOMER_EMAIL));
        args.putString(in.juspay.godel.core.PaymentConstants.ENV, in.juspay.godel.core.PaymentConstants.ENVIRONMENT.PRODUCTION);
        args.putString(in.juspay.godel.core.PaymentConstants.SERVICE, PaymentConstants.JUSPAY_SERVICE);
        args.putStringArrayList(in.juspay.godel.core.PaymentConstants.END_URLS, getJuPayCreateWalletRequest().getStringArrayList(in.juspay.godel.core.PaymentConstants.END_URLS));
        args.putString(in.juspay.godel.core.PaymentConstants.CLIENT_AUTH_TOKEN, getValue(in.juspay.godel.core.PaymentConstants.CLIENT_AUTH_TOKEN));
        args.putString(in.juspay.godel.core.PaymentConstants.PAYLOAD, payLoad);

        Intent intent = new Intent(this, in.juspay.godel.PaymentActivity.class);
        intent.putExtras(args);
        startActivityForResult(intent, callBackId);
        dismissProgress();
    }

    private String getValue(String key) {
        Bundle jusPayCreateWalletRequest = getJuPayCreateWalletRequest();
        return !TextUtils.isEmpty(jusPayCreateWalletRequest.getString(key)) ? jusPayCreateWalletRequest.getString(key) : "";
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void setError(String error) {
        SnackBarHelper.snackBarCallBack(mBinding.mainLayout, this, error);
    }

    @Override
    public void setPaymentResult(boolean paymentResult) {
        Intent intent = new Intent();
        intent.putExtra(PaymentIntentConstant.TRANSACTION_STATUS, paymentResult);
        intent.putExtra(PaymentConstants.PAYMENT_REQUEST, viewModel.getPaymentRequest());
        intent.putExtra(PaymentConstants.PAYMENT_RESPONSE, viewModel.getPaymentResponse());
        intent.putExtra(PaymentConstants.PAYTM_CHECK_SUM, viewModel.getPayTmCheckSum());
        setResult(RESULT_OK, intent);
        this.finish();
    }

    @Override
    public void payTmAddMoney(PayTmAddMoneyToWalletParams intentParam) {
        Intent intent = new Intent(this, PayTmAddMoneyActivity.class);
        intent.putExtra(PaymentIntentConstant.PAY_TM_ADD_MONEY_PARAM, new Gson().toJson(intentParam));
        intent.putExtra(PaymentIntentConstant.CART_ID, getCartId());
        if (viewModel.isFromConsultationOrDiagnostic()) {
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION, true);
        }
        startActivityForResult(intent, PaymentIntentConstant.PAYTM_ADD_MONEY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case PaymentConstants.JUSPAY_VERIFY_OTP:
                viewModel.verifyJusPayOtpResponse(resultCode, data);
                break;
            case PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT:
                navigateToParent(resultCode, data);
                break;
            case PaymentConstants.JUSPAY_RESEND_OTP:
                viewModel.resendOtpResponse(resultCode);
                break;
            case PaymentIntentConstant.PAYTM_ADD_MONEY:
                navigateToParentViaPayTM(data);
                break;
        }
    }

    private void navigateToParentViaPayTM(Intent data) {
        setResult(RESULT_OK, data);
        this.finish();
    }

    private void navigateToParent(int resultCode, Intent data) {
        Intent intent = new Intent();
        if (resultCode == RESULT_OK && data != null && data.hasExtra(PaymentConstants.JUSPAY_PAY_LOAD)) {
            intent.putExtra(PaymentIntentConstant.TRANSACTION_STATUS, true);
        } else if (resultCode == RESULT_CANCELED)
            intent.putExtra(PaymentIntentConstant.TRANSACTION_STATUS, false);
        intent.putExtra(PaymentConstants.PAYMENT_REQUEST, viewModel.getPaymentRequest());
        intent.putExtra(PaymentConstants.PAYMENT_RESPONSE, viewModel.getPaymentResponse());
        setResult(RESULT_OK, intent);
        this.finish();
    }

    private void setView() {
        mBinding.otpSix.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (TextUtils.isEmpty(mBinding.otpSix.getText())) {
                        mBinding.otpFive.requestFocus();
                    }
                }
                return false;
            }
        });

        mBinding.otpFive.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (TextUtils.isEmpty(mBinding.otpFive.getText()))
                        mBinding.otpFour.requestFocus();
                }
                return false;
            }
        });

        mBinding.otpFour.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (TextUtils.isEmpty(mBinding.otpFour.getText()))
                        mBinding.otpThree.requestFocus();
                }
                return false;
            }
        });

        mBinding.otpThree.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (TextUtils.isEmpty(mBinding.otpThree.getText()))
                        mBinding.otpTwo.requestFocus();
                }
                return false;
            }
        });

        mBinding.otpTwo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (TextUtils.isEmpty(mBinding.otpTwo.getText()))
                        mBinding.otpOne.requestFocus();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(PaymentConstants.PAYMENT_REQUEST, TextUtils.isEmpty(viewModel.getPaymentRequest()) ? PaymentConstants.CANCELLED_BY_USER : viewModel.getPaymentRequest());
        intent.putExtra(PaymentConstants.PAYMENT_RESPONSE, TextUtils.isEmpty(viewModel.getPaymentResponse()) ? PaymentConstants.CANCELLED_BY_USER : viewModel.getPaymentResponse());
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }
}