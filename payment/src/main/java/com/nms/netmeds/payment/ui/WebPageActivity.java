package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.databinding.ActivityWebPageBinding;
import com.nms.netmeds.payment.R;

import static android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;

public class WebPageActivity extends BaseActivity implements NetmedsJavaScriptInterface.PaymentCallBackInterface {

    private ActivityWebPageBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_page);
        toolBarSetUp(binding.toolbar);
        initToolBar();
        setWebView();
        if (getIntent() != null)
            onNewIntent(getIntent());
    }

    private void setWebView() {
        initWebViewSetting(binding.webview);
        binding.webview.setWebViewClient(new NetmedsWebViewClient());
        binding.webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 50) {
                    dismissProgress();
                }
            }
        });
        binding.webview.loadUrl(getIntent().getStringExtra("WEB_PAGE_URL"));
    }

    private void initToolBar() {
        binding.collapsingToolbar.setTitle(getIntent().getStringExtra("PAGE_TITLE_KEY"));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public void onSuccess(String result) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public void onFailure(String result) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    WebView getWebView() {
        return binding.webview;
    }

    void loadURL(String url) {
        binding.webview.loadUrl(url);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebViewSetting(WebView webView) {
        WebSettings webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(false);
        webSetting.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSetting.setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSetting.setMixedContentMode(MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (binding.webview.canGoBack()) {
            binding.webview.goBack();
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("JavascriptInterface")
    void initJavaScriptInterface(WebView webView) {
        NetmedsJavaScriptInterface javaScriptInterface = new NetmedsJavaScriptInterface(this);
        webView.addJavascriptInterface(javaScriptInterface, "PayU");
    }

    class NetmedsWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dismissProgress();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgress(WebPageActivity.this);
        }
    }
}
