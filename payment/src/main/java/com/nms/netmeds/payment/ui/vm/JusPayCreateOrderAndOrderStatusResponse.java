package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayCreateOrderAndOrderStatusResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private JusPayOrderResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JusPayOrderResult getResult() {
        return result;
    }

    public void setResult(JusPayOrderResult result) {
        this.result = result;
    }
}
