package com.nms.netmeds.payment.ui.vm;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.payment.databinding.FragmentCashOnDeliveryBinding;

@SuppressLint("ValidFragment")
public class PaymentFailureCancelFragment extends BaseDialogFragment {

    private final PaymentFailureCancelListener mListener;

    public PaymentFailureCancelFragment(PaymentFailureCancelListener listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentCashOnDeliveryBinding binding = DataBindingUtil.inflate(inflater, com.nms.netmeds.payment.R.layout.fragment_cash_on_delivery, container, false);
        if (getActivity() != null && getActivity().getResources() != null) {
            binding.title.setVisibility(View.GONE);
            binding.description.setText(getActivity().getResources().getString(com.nms.netmeds.payment.R.string.text_failure_cancel));
            binding.positiveButton.setText(getActivity().getResources().getString(com.nms.netmeds.payment.R.string.text_retry));
        }
        binding.closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        binding.cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListener.retainCart();
            }
        });
        binding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListener.paymentRetry();
            }
        });
        return binding.getRoot();
    }

    public interface PaymentFailureCancelListener {
        void paymentRetry();

        void retainCart();
    }
}
