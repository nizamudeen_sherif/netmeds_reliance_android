/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaypalPaymentRequestContext {
    @SerializedName("shipping_preference")
    private String shippingPreference;
    @SerializedName("user_action")
    private String userAction;
    @SerializedName("locale")
    private String locale;
    @SerializedName("landing_page")
    private String landingPage;

    public String getShippingPreference() {
        return shippingPreference;
    }

    public void setShippingPreference(String shippingPreference) {
        this.shippingPreference = shippingPreference;
    }

    public String getUserAction() {
        return userAction;
    }

    public void setUserAction(String userAction) {
        this.userAction = userAction;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }
}