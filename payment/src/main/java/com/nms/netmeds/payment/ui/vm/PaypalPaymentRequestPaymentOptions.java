package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaypalPaymentRequestPaymentOptions {
    @SerializedName("allowed_payment_method")
    private String allowedPaymentMethod;

    public String getAllowedPaymentMethod() {
        return allowedPaymentMethod;
    }

    public void setAllowedPaymentMethod(String allowedPaymentMethod) {
        this.allowedPaymentMethod = allowedPaymentMethod;
    }

}