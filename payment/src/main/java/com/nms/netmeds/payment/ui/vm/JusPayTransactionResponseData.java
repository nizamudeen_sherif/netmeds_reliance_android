package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayTransactionResponseData {
    @SerializedName("status")
    private String status;
    @SerializedName("orderId")
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
