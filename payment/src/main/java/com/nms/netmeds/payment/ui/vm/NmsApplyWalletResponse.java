package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class NmsApplyWalletResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private NmsApplyWalletResponseData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NmsApplyWalletResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(NmsApplyWalletResponseData responseData) {
        this.responseData = responseData;
    }
}
