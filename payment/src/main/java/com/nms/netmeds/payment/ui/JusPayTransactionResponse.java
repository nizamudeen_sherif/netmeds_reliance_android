package com.nms.netmeds.payment.ui;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.payment.ui.vm.JusPayTransactionResponseData;

public class JusPayTransactionResponse {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private JusPayTransactionResponseData response;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JusPayTransactionResponseData getResponse() {
        return response;
    }

    public void setResponse(JusPayTransactionResponseData response) {
        this.response = response;
    }
}