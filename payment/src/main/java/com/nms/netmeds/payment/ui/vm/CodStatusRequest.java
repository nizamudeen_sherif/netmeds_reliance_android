package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class CodStatusRequest {
    @SerializedName("pincode")
    private String pinCode;
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("couponCode")
    private String couponCode;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
