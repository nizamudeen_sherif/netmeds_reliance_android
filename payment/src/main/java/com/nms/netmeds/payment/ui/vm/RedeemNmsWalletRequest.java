package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class RedeemNmsWalletRequest {
    @SerializedName("cart_id")
    private String pvCardId;
    @SerializedName("nmscash")
    private String nmsCash;
    @SerializedName("supercash")
    private String pvSuperCash;


    public String getNmscash() {
        return nmsCash;
    }

    public void setNmscash(String nmsCash) {
        this.nmsCash = nmsCash;
    }

    public String getPvCardId() {
        return pvCardId;
    }

    public void setPvCardId(String pvCardId) {
        this.pvCardId = pvCardId;
    }

    public String getPvSuperCash() {
        return pvSuperCash;
    }

    public void setPvSuperCash(String pvSuperCash) {
        this.pvSuperCash = pvSuperCash;
    }
}
