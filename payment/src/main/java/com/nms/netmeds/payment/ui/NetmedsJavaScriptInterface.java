package com.nms.netmeds.payment.ui;


import android.webkit.JavascriptInterface;

public class NetmedsJavaScriptInterface {

    private final PaymentCallBackInterface callBackInterface;

    NetmedsJavaScriptInterface(PaymentCallBackInterface callBackInterface) {
        this.callBackInterface = callBackInterface;
    }

    @JavascriptInterface
    public void onSuccess(final String result) {
        callBackInterface.onSuccess(result);
    }

    @JavascriptInterface
    public void onFailure(final String result) {
        callBackInterface.onFailure(result);

    }

    public interface PaymentCallBackInterface {
        void onSuccess(String result);

        void onFailure(String result);
    }
}
