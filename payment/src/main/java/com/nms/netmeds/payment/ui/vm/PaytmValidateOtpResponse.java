package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class PaytmValidateOtpResponse {
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("expires")
    private String expires;
    @SerializedName("scope")
    private String scope;
    @SerializedName("resourceOwnerId")
    private String resourceOwnerId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getResourceOwnerId() {
        return resourceOwnerId;
    }

    public void setResourceOwnerId(String resourceOwnerId) {
        this.resourceOwnerId = resourceOwnerId;
    }
}
