package com.nms.netmeds.payment.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.payment.NetmedsPaymentProcess;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.DialogNewCardDetailsBinding;
import com.nms.netmeds.payment.ui.vm.NewPaymentCardDetailsDialogViewModel;

import static com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils.cardNameValidation;
import static com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils.cvvValidation;
import static com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils.expiryValidation;
import static com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils.luhnAlgorithim;

public class NewPaymentCardDetailsDialog extends BaseBottomSheetFragment implements NewPaymentCardDetailsDialogViewModel.NewPaymentCardDetailsDialogViewModelCallback {

    private NewPaymentCardDetailsDialogCallback callback;
    private DialogNewCardDetailsBinding binding;
    private static final char space = ' ';
    private boolean isSaveCard = false;
    private double grandTotal;
    private Bundle cardDetails = new Bundle();

    public NewPaymentCardDetailsDialog(NewPaymentCardDetailsDialogCallback callback, double grandTotal) {
        this.callback = callback;
        this.grandTotal = grandTotal;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_new_card_details, container, false);
        final NewPaymentCardDetailsDialogViewModel viewModel = ViewModelProviders.of(this).get(NewPaymentCardDetailsDialogViewModel.class);
        initializeBehaviour();
        initPayButton();
        viewModel.initialize(this);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ResponsiveDialogStyle);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    onDismissNewCardDialog();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    private void initPayButton() {
        binding.tvPay.setText(String.format(getString(R.string.text_pay_rs), CommonUtils.getPriceInFormat(grandTotal)));

        binding.chkSaveCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isSaveCard = isChecked;
                if (isChecked) {
                    binding.tvPay.setText(getString(R.string.text_add_and_pay));
                } else {
                    binding.tvPay.setText(String.format(getString(R.string.text_pay_rs), CommonUtils.getPriceInFormat(grandTotal)));
                }
            }
        });
    }

    private void initializeBehaviour() {
        binding.cardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int before, int count, int i2) {
                binding.tilCardNumber.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(editable.toString()) && editable.length() > 0 && (editable.length() % 5) == 0) {
                    char c = editable.charAt(editable.length() - 1);
                    if (space == c) {
                        editable.delete(editable.length() - 1, editable.length());
                    } else if (Character.isDigit(c) && TextUtils.split(editable.toString(), String.valueOf(space)).length <= 3) {
                        editable.insert(editable.length() - 1, String.valueOf(space));
                    }
                }
                cardDetails.putString(PaymentConstants.CARD_NUMBER, editable.toString());
            }
        });


        binding.cardValidity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                binding.tilCardValidity.setError(null);
                String working = charSequence.toString();
                boolean isValid = true;
                if (!TextUtils.isEmpty(working) && working.length() == 1 && before == 0) {
                    if (Integer.parseInt(working) < 10 && Integer.parseInt(working) != 1 && Integer.parseInt(working) != 0) {
                        working = "0" + working;
                        binding.cardValidity.setText(working);
                    }
                    isValid = true;
                }
                if (!TextUtils.isEmpty(working) && working.length() == 2 && before == 0) {
                    if (Integer.parseInt(working) < 1 || Integer.parseInt(working) > 12) {
                        isValid = false;
                    } else {
                        working += "/";
                        binding.cardValidity.setText(working);
                        binding.cardValidity.setSelection(working.length());
                    }
                }

                if (!isValid) {
                    binding.cardValidity.setError(getString(R.string.invalid_expiry_date));
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                cardDetails.putString(PaymentConstants.CARD_VALIDITY, editable.toString());
            }
        });

        binding.cardCvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.tilCardCvv.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                cardDetails.putString(PaymentConstants.CARD_CVV, s.toString());
            }
        });


        binding.cardHolderName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.tilCardHolderName.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                cardDetails.putString(PaymentConstants.NAME_ON_CARD, s.toString());
            }
        });
    }

    @Override
    public void onDismissNewCardDialog() {
        dismiss();
    }

    @Override
    public void onProceedPaymentWithNewCardDetails() {
        if (validateCard(cardDetails)) {
            onDismissNewCardDialog();
            cardDetails.putBoolean(PaymentConstants.SAVE_TO_LOCKER, isSaveCard);
            callback.onProceedPaymentWithNewCardDetails(cardDetails);
        }
    }

    private boolean validateCard(Bundle bundle) {
        if (bundle != null) {
            if (!luhnAlgorithim(bundle.getString(PaymentConstants.CARD_NUMBER) != null ? bundle.getString(PaymentConstants.CARD_NUMBER) : "")) {
                binding.tilCardNumber.setError(getString(R.string.invalid_card_number));
                return false;
            } else if (!expiryValidation(bundle.getString(PaymentConstants.CARD_VALIDITY) != null ? bundle.getString(PaymentConstants.CARD_VALIDITY) : "", callback.getPaymentProcessReference())) {
                binding.tilCardValidity.setError(getString(R.string.invalid_expiry_date));
                return false;
            } else if (!cvvValidation(bundle.getString(PaymentConstants.CARD_CVV) != null ? bundle.getString(PaymentConstants.CARD_CVV) : "")) {
                binding.tilCardCvv.setError(getString(R.string.invalid_cvv));
                return false;
            } else if (!cardNameValidation(bundle.getString(PaymentConstants.NAME_ON_CARD) != null ? bundle.getString(PaymentConstants.NAME_ON_CARD) : "")) {
                binding.tilCardHolderName.setError(getString(R.string.invalid_card_name));
                return false;
            } else return true;
        } else return false;
    }


    public interface NewPaymentCardDetailsDialogCallback {

        void onProceedPaymentWithNewCardDetails(Bundle cardDetails);

        NetmedsPaymentProcess getPaymentProcessReference();
    }
}

