package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class CodStatusResult {
    @SerializedName("message")
    private String message;
    @SerializedName("codEnable")
    private Boolean codEnable;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getCodEnable() {
        return codEnable;
    }

    public void setCodEnable(Boolean codEnable) {
        this.codEnable = codEnable;
    }
}
