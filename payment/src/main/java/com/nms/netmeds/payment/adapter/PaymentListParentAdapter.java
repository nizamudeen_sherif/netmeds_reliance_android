package com.nms.netmeds.payment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.font.LatoTextView;
import com.nms.netmeds.base.model.PaymentGatewayList;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.PaymentListParentRowBinding;
import com.nms.netmeds.payment.ui.PaymentConstants;

import java.util.List;

public class PaymentListParentAdapter extends RecyclerView.Adapter<PaymentListParentAdapter.PaymentListParentAdapterViewHolder>
        implements PaymentListVerticalChildAdapter.PaymentListVerticalChildAdapterCallback {
    private Context context;
    private PaymentListParentAdapterCallback parentPaymentListCallback;
    private PaymentListVerticalChildAdapter.PaymentListVerticalChildAdapterCallback childPaymentListCallback;

    private List<PaymentGatewayList> paymentList;

    private boolean isPayTmUPIActive;
    private boolean isGooglePayUPIActive;

    public PaymentListParentAdapter(Object contextAndListener, List<PaymentGatewayList> paymentList, boolean isPayTmUPIActive, boolean isGooglePayUPIActive) {
        PaymentHelper.setChildPosition(-1);
        this.context = (Context) contextAndListener;
        this.parentPaymentListCallback = (PaymentListParentAdapterCallback) contextAndListener;
        this.childPaymentListCallback = this;
        this.paymentList = paymentList;
        this.isGooglePayUPIActive = isGooglePayUPIActive;
        this.isPayTmUPIActive = isPayTmUPIActive;
    }

    @NonNull
    @Override
    public PaymentListParentAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PaymentListParentRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.payment_list_parent_row, parent, false);
        return new PaymentListParentAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListParentAdapterViewHolder holder, int position) {
        holder.bindPaymentList();
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListParentAdapterViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        holder.bindPaymentList();
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void updatePaymentSelection(PaymentGatewaySubCategory selectedPaymentDetails, int selectedPaymentChildPosition, int selectedPaymentParentPosition) {
        int parentIndex = 0;
        int childIndex = 0;
        int previousParentPosition = -1;
        int previousChildPosition = -1;
        for (PaymentGatewayList parentGatewayCategory : paymentList) {
            parentIndex = paymentList.indexOf(parentGatewayCategory);
            for (PaymentGatewaySubCategory paymentGateway : parentGatewayCategory.getSubList()) {
                childIndex = parentGatewayCategory.getSubList().indexOf(paymentGateway);
                if (previousParentPosition == -1) {
                    previousParentPosition = paymentGateway.isGatewayChecked() ? parentIndex : -1;
                    previousChildPosition = paymentGateway.isGatewayChecked() ? childIndex : -1;
                }
                paymentGateway.setGatewayChecked(selectedPaymentDetails != null && parentIndex == selectedPaymentParentPosition && childIndex == selectedPaymentChildPosition &&
                        selectedPaymentDetails.getParentId() == paymentGateway.getParentId() && selectedPaymentDetails.getSubId() == paymentGateway.getSubId());
            }
        }
        /**
         * first "If Condition" while work for reset particular position of the previous selection of Payment gateway.
         */
        if (previousParentPosition != -1) {
            PaymentHelper.setChildPosition(previousChildPosition);
            notifyItemChanged(previousParentPosition);
        }
        PaymentHelper.setChildPosition(selectedPaymentChildPosition);
        notifyItemChanged(selectedPaymentParentPosition);
        parentPaymentListCallback.updateSelectedPaymentGateway(selectedPaymentDetails);
        if (selectedPaymentDetails != null && PaymentConstants.WALLET_LIST == selectedPaymentDetails.getParentId() && !selectedPaymentDetails.getLinked()) {
            /**
             * Remove previous selection and initiate and navigate to wallet link process.
             */
            navigateToLinkWallet(selectedPaymentDetails);
        }
    }

    @Override
    public void placeOrder(PaymentGatewaySubCategory selectedPaymentDetails, String cvv) {
        parentPaymentListCallback.placeOrder(selectedPaymentDetails, cvv);
    }

    @Override
    public void navigateToLinkWallet(PaymentGatewaySubCategory paymentGatewayDetail) {
        /**
         * if wallet is not linked then the place order method will navigate to link wallet page with checking some condition inside that method.
         */
        placeOrder(paymentGatewayDetail, "");
    }

    @Override
    public double getTotalAmount() {
        return parentPaymentListCallback.getTotalAmount();
    }

    public class PaymentListParentAdapterViewHolder extends RecyclerView.ViewHolder {

        private PaymentListParentRowBinding binding;
        private PaymentListParentAdapterViewHolder viewHolder;

        public PaymentListParentAdapterViewHolder(PaymentListParentRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.viewHolder = this;
        }

        public void bindPaymentList() {
            PaymentGatewayList paymentCategory = paymentList.get(getAdapterPosition());
            binding.setParentCategory(paymentCategory);
            binding.setViewHolder(viewHolder);
            binding.trMoreSection.setOnClickListener(initiateMoreSectionClickListener());
            setPaymentChildListAdapter(binding, paymentCategory);
            continuePlaceOrderBaseOnSelectionOfNetbanking(PaymentConstants.ORIENTATION_HORIZONTAL.equalsIgnoreCase(paymentCategory.getOrientation()), paymentCategory.getSubList());
        }

        private void continuePlaceOrderBaseOnSelectionOfNetbanking(boolean horizontalOrientation, List<PaymentGatewaySubCategory> paymentGatewayList) {
            if (horizontalOrientation) {
                for (PaymentGatewaySubCategory gatewaySubCategory : paymentGatewayList) {
                    if (gatewaySubCategory.isGatewayChecked()) {
                        parentPaymentListCallback.placeOrder(gatewaySubCategory, "");
                        break;
                    }
                }
            }
        }

        private View.OnClickListener initiateMoreSectionClickListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PaymentGatewayList paymentGatewayParentCategory = paymentList.get(getAdapterPosition());
                    if (PaymentConstants.NET_BANKING_LIST == paymentGatewayParentCategory.getId()) {
                        parentPaymentListCallback.navigateToBankList();
                    } else if (PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayParentCategory.getId()) {
                        parentPaymentListCallback.navigateToAddNewCardDetails();
                    } else if (PaymentConstants.UPI_PAYMENT_LIST == paymentGatewayParentCategory.getId()) {

                    }
                }
            };
        }

        private void setPaymentChildListAdapter(PaymentListParentRowBinding binding, PaymentGatewayList paymentCategory) {
            if (PaymentConstants.ORIENTATION_HORIZONTAL.equalsIgnoreCase(paymentCategory.getOrientation())) {
                binding.rvPaymentChild.setVisibility(View.GONE);
                binding.incHorizontalPaymentGateway.clParentNetBanking.setVisibility(View.VISIBLE);
                binding.incHorizontalPaymentGateway.setViewHolder(this);
                loadHorizontalPaymentViews(paymentCategory, binding);
            } else {
                binding.rvPaymentChild.setVisibility(View.VISIBLE);
                binding.incHorizontalPaymentGateway.clParentNetBanking.setVisibility(View.GONE);
                binding.rvPaymentChild.setItemAnimator(null);
                PaymentListVerticalChildAdapter paymentListVerticalChildAdapter = new PaymentListVerticalChildAdapter(context, childPaymentListCallback, paymentCategory, isPayTmUPIActive, isGooglePayUPIActive, getAdapterPosition());
                binding.rvPaymentChild.setLayoutManager(new LinearLayoutManager(context));
                if (PaymentHelper.getChildPosition() != -1) {
                    paymentListVerticalChildAdapter.notifyItemChanged(PaymentHelper.getChildPosition());
                } else {
                    binding.rvPaymentChild.setAdapter(paymentListVerticalChildAdapter);
                }
            }
        }

        private void loadHorizontalPaymentViews(PaymentGatewayList paymentCategory, PaymentListParentRowBinding binding) {
            for (PaymentGatewaySubCategory paymentGatewaySubCategory : paymentCategory.getSubList()) {
                loadImage(paymentGatewaySubCategory.getImage(), getImageViewNameAndSetTitle(binding, paymentCategory.getSubList().indexOf(paymentGatewaySubCategory), paymentGatewaySubCategory));
            }
        }

        public void horizontalPaymentGatewayClickListener(int index) {
            updatePaymentSelection(paymentList.get(getAdapterPosition()).getSubList().get(index), index, getAdapterPosition());
        }

        private ImageView getImageViewNameAndSetTitle(PaymentListParentRowBinding binding, int index, PaymentGatewaySubCategory paymentGatewaySubCategory) {
            switch (index) {
                case 0:
                    setUIProperties(binding.incHorizontalPaymentGateway.tvGatewayTitle1, binding.incHorizontalPaymentGateway.imgGateway1, paymentGatewaySubCategory);
                    return binding.incHorizontalPaymentGateway.imgGateway1;
                case 1:
                    setUIProperties(binding.incHorizontalPaymentGateway.tvGatewayTitle2, binding.incHorizontalPaymentGateway.imgGateway2, paymentGatewaySubCategory);
                    return binding.incHorizontalPaymentGateway.imgGateway2;
                case 2:
                    setUIProperties(binding.incHorizontalPaymentGateway.tvGatewayTitle3, binding.incHorizontalPaymentGateway.imgGateway3, paymentGatewaySubCategory);
                    return binding.incHorizontalPaymentGateway.imgGateway3;
                case 3:
                    setUIProperties(binding.incHorizontalPaymentGateway.tvGatewayTitle4, binding.incHorizontalPaymentGateway.imgGateway4, paymentGatewaySubCategory);
                    return binding.incHorizontalPaymentGateway.imgGateway4;
                case 4:
                    setUIProperties(binding.incHorizontalPaymentGateway.tvGatewayTitle5, binding.incHorizontalPaymentGateway.imgGateway5, paymentGatewaySubCategory);
                    return binding.incHorizontalPaymentGateway.imgGateway5;
            }
            return null;
        }

        private void setUIProperties(LatoTextView gatewayTitle, ImageView gateImg, PaymentGatewaySubCategory gatewaySubCategory) {
            gatewayTitle.setText(gatewaySubCategory.getDisplayName());
            if (PaymentConstants.GOOGLE_PAY_ID.equalsIgnoreCase(gatewaySubCategory.getId())) {
                gatewayTitle.setEnabled(isGooglePayUPIActive);
                gatewayTitle.setClickable(isGooglePayUPIActive);
                gatewayTitle.setFocusable(isGooglePayUPIActive);
                gatewayTitle.setAlpha(isGooglePayUPIActive ? 1 : 0.4f);

                gateImg.setEnabled(isGooglePayUPIActive);
                gateImg.setClickable(isGooglePayUPIActive);
                gateImg.setFocusable(isGooglePayUPIActive);
                gateImg.setAlpha(isGooglePayUPIActive ? 1 : 0.4f);
            }
        }

        private void loadImage(String imagePath, ImageView imageView) {
            if (imageView != null) {
                RequestOptions options = new RequestOptions()
                        .fitCenter()
                        .placeholder(R.drawable.ic_net_banking)
                        .error(R.drawable.ic_net_banking)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);

                Glide.with(context).load(imagePath)
                        .apply(options)
                        .into(imageView);
            }
        }

        public boolean isAddOtherSectionEnable(PaymentGatewayList paymentGatewayList) {
            return (PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayList.getId() ||
                    PaymentConstants.NET_BANKING_LIST == paymentGatewayList.getId() ||
                    (PaymentConstants.UPI_PAYMENT_LIST == paymentGatewayList.getId() && paymentGatewayList.getSubList().size() > 5));
        }

        public boolean isAddOtherSectionDividerEnable(PaymentGatewayList paymentGatewayList) {
            return ((PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayList.getId() && paymentGatewayList.getSubList().size() > 0) ||
                    PaymentConstants.NET_BANKING_LIST == paymentGatewayList.getId() ||
                    (PaymentConstants.UPI_PAYMENT_LIST == paymentGatewayList.getId() && paymentGatewayList.getSubList().size() > 5));
        }

        public boolean isAddButtonEnable(PaymentGatewayList paymentGatewayList) {
            return PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayList.getId();
        }

        public String getAddOtherSectionText(PaymentGatewayList paymentGatewayList) {
            if (PaymentConstants.CREDIT_DEBIT_LIST == paymentGatewayList.getId()) {
                return context.getString(R.string.text_add_new_card);
            } else if (PaymentConstants.NET_BANKING_LIST == paymentGatewayList.getId()) {
                return context.getString(R.string.text_more_banks);
            } else if (PaymentConstants.UPI_PAYMENT_LIST == paymentGatewayList.getId() && paymentGatewayList.getSubList().size() > 5) {
                return context.getString(R.string.text_more_upi);
            } else {
                return "";
            }
        }
    }

    public interface PaymentListParentAdapterCallback {

        void placeOrder(PaymentGatewaySubCategory selectedPaymentDetails, String cvv);

        void updateSelectedPaymentGateway(PaymentGatewaySubCategory selectedPaymentDetails);

        void navigateToBankList();

        void navigateToAddNewCardDetails();

        double getTotalAmount();
    }
}
