package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayPaymentLinks {
    @SerializedName("web")
    private String web;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("iframe")
    private String iframe;

    public String getWeb() {
        return this.web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIframe() {
        return this.iframe;
    }

    public void setIframe(String iframe) {
        this.iframe = iframe;
    }
}
