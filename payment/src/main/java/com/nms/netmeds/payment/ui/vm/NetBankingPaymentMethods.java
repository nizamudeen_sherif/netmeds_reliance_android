package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class NetBankingPaymentMethods {
    @SerializedName("payment_method_type")
    private String paymentMethodType;
    @SerializedName("payment_method")
    private String paymentMethod;
    @SerializedName("description")
    private String description;

    public String getPaymentMethodType() {
        return this.paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }


    public String getPaymentMethod() {
        return this.paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
