package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.OrderConfirmedPending;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.payment.R;

import java.util.Locale;


public class PaymentFailurePopupViewModel extends AppViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private PaymentFailureInterface mListener;
    private PaymentGatewaySubCategory codCategory;
    private ConfigurationResponse configurationResponse;
    private BasePreference basePreference;

    private String cartId;
    private double amount = 0;
    private int jusPayOrderStatus;
    private boolean isFromDiagnostics = false;
    private boolean isPrimeAvailableInCart = false;

    public PaymentFailurePopupViewModel(@NonNull Application application) {
        super(application);
    }

    public void initViewModel(Context context, PaymentFailureInterface listener, Bundle bundle, BasePreference basePreference) {
        this.context = context;
        this.mListener = listener;
        this.basePreference = basePreference;
        configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (bundle != null) {
            if (bundle.containsKey(PaymentIntentConstant.AMOUNT))
                setAmount(bundle.getDouble(PaymentIntentConstant.AMOUNT));

            if (bundle.containsKey(PaymentIntentConstant.JUSPAY_ORDER_STATUS))
                jusPayOrderStatus = bundle.getInt(PaymentIntentConstant.JUSPAY_ORDER_STATUS);

            if (bundle.containsKey(PaymentIntentConstant.PAYMENT_COD_SUB_CATEGORY))
                codCategory = new Gson().fromJson(bundle.getString(PaymentIntentConstant.PAYMENT_COD_SUB_CATEGORY), PaymentGatewaySubCategory.class);

            if (bundle.containsKey(PaymentIntentConstant.IS_PRIME_PRODUCT_AVAILABLE_IN_CART))
                isPrimeAvailableInCart = bundle.getBoolean(PaymentIntentConstant.IS_PRIME_PRODUCT_AVAILABLE_IN_CART, false);

            if (bundle.containsKey(PaymentIntentConstant.FROM_DIAGNOSTICS)) {
                String fromDiagnostic = bundle.getString(PaymentIntentConstant.FROM_DIAGNOSTICS);
                assert fromDiagnostic != null;
                setFromDiagnostics(fromDiagnostic.equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC));
            }
        }
        getCODStatus();
    }

    private void getCODStatus() {
        if (isCODEligible()) {
            initiateAPICall(APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK);
        } else {
            codEligibleCheck(null);
        }
    }

    private void initiateAPICall(int transactionId) {
        if (APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK == transactionId) {
            APIServiceManager.getInstance().checkCODEligibleForCreateNewOrder(this, basePreference.getMstarBasicHeaderMap());
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        codEligibleCheck(data);
    }

    private void codEligibleCheck(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel codEligibleStatus = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            mListener.codEnable((codEligibleStatus != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(codEligibleStatus.getStatus()) &&
                    codEligibleStatus.getResult() != null && codEligibleStatus.getResult().isCodEnable()));
        } else {
            mListener.codEnable(false);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        codEligibleCheck(null);
    }

    public void onFailedPopupClose() {
        mListener.vmOnPopupClose();
    }

    public void onPayCash() {
        /*Google Analytics Event*/
        basePreference.setCartId(getCartId());
        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_CHANGE_TO_COD, GoogleAnalyticsHelper.EVENT_LABEL_FAILED_PAYMENT_PAGE);
        mListener.onClickOnCOD();
    }

    public void onRetry() {
        /*Google Analytics Event*/
        basePreference.setCartId(getCartId());
        PaymentHelper.setReloadCart(true);
        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_RETRY_PAYMENT, GoogleAnalyticsHelper.EVENT_LABEL_FAILED_PAYMENT_PAGE);
        mListener.vmOnPaymentRetry();
    }

    public String setTotalAmount() {
        return String.format(Locale.getDefault(), "₹%.2f", getAmount());
    }

    public boolean isCODEligible() {
        return getAmount() > 0 && codCategory != null && getAmount() > Double.valueOf(codCategory.getMinimumAmount())
                && getAmount() < Double.valueOf(codCategory.getMaximumAmount()) && !PaymentHelper.isNMSCashApplied()
                && !PaymentHelper.isNMSSuperCashApplied() && !PaymentHelper.isVoucherApplied() && !PaymentHelper.isIsWalletApplied() && PaymentHelper.getPinCodeStatus() && !isPrimeAvailableInCart;
    }

    public String setFailureDescription() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getTransactionFailure()) ? configurationResponse.getResult().getConfigDetails().getTransactionFailure() : "";
    }

    private OrderConfirmedPending getOrderConfirmedPending() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getOrderConfirmedPending() != null ? configurationResponse.getResult().getConfigDetails().getOrderConfirmedPending() : new OrderConfirmedPending();
    }

    private String setOrderPendingDescription() {
        return getOrderConfirmedPending() != null && !TextUtils.isEmpty(getOrderConfirmedPending().getDescription()) ? getOrderConfirmedPending().getDescription() : "";
    }

    public String setDescription() {
        return !isFromDiagnostics() && (jusPayOrderStatus == PaymentConstants.PAYMENT_STATUS_PENDING_VBV || jusPayOrderStatus == PaymentConstants.PAYMENT_STATUS_AUTHORIZING) ? setOrderPendingDescription() : setFailureDescription();
    }

    public String setTransactionFailedTitle() {
        return context.getResources().getString(R.string.text_transaction_failed);
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public double getAmount() {
        return amount;
    }

    public boolean isFromDiagnostics() {
        return isFromDiagnostics;
    }

    private void setFromDiagnostics(boolean fromDiagnostics) {
        isFromDiagnostics = fromDiagnostics;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public interface PaymentFailureInterface {

        void vmShowProgress();

        void vmDismissProgress();

        void vmOnPaymentRetry();

        void onClickOnCOD();

        void vmOnPopupClose();

        void codEnable(boolean isCODEligible);
    }
}