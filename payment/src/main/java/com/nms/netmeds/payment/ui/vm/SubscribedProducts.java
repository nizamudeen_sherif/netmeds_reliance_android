package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class SubscribedProducts {
    @SerializedName("product_code")
    private int productCode;
    @SerializedName("qty")
    private int qty;
    @SerializedName("price")
    private String price;

    public int getProductCode() {
        return this.productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public int getQty() {
        return this.qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
