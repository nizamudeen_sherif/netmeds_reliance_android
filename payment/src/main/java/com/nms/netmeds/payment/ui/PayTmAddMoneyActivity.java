package com.nms.netmeds.payment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityPaytmAddMoneyBinding;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.util.HashMap;
import java.util.Map;

public class PayTmAddMoneyActivity extends BaseViewModelActivity<PayTmAddMoneyViewModel> implements PayTmAddMoneyViewModel.AddMoneyListener {

    private ActivityPaytmAddMoneyBinding mBinding;
    private PayTmAddMoneyViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_paytm_add_money);
        toolBarSetUp(mBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        mBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected PayTmAddMoneyViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(PayTmAddMoneyViewModel.class);
        onRetry(viewModel);
        setView();
        return viewModel;
    }

    private void setView() {
        viewModel.init(getIntentValue(), getCartId(), mBinding, this, BasePreference.getInstance(this));
    }


    private String getCartId() {
        return getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.CART_ID) && !TextUtils.isEmpty(getIntent().getStringExtra(PaymentIntentConstant.CART_ID))
                ? getIntent().getStringExtra(PaymentIntentConstant.CART_ID) : "";
    }

    private PayTmAddMoneyToWalletParams getIntentValue() {
        PayTmAddMoneyToWalletParams params = new PayTmAddMoneyToWalletParams();
        if (getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.PAY_TM_ADD_MONEY_PARAM) && !TextUtils.isEmpty(getIntent().getStringExtra(PaymentIntentConstant.PAY_TM_ADD_MONEY_PARAM))) {
            params = new Gson().fromJson(getIntent().getStringExtra(PaymentIntentConstant.PAY_TM_ADD_MONEY_PARAM), PayTmAddMoneyToWalletParams.class);
            viewModel.setDiagnosticOrConsultationOrderId(params.getOrderId());
        }
        if (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION)) {
            viewModel.setFromConsultationOrDiagnostic(getIntent().getBooleanExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION, false));
            viewModel.setDcTransactionAmount(params.getOrderAmt());
        }
        return params;
    }

    @Override
    public void setError(String error) {
        SnackBarHelper.snackBarCallBack(mBinding.parentLayout, this, error);
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void initTransaction(String checksumHash) {
        PaytmPGService Service = PaytmPGService.getProductionService();
        Map<String, String> paramMap = new HashMap<>();
        ConfigMap getConfig = ConfigMap.getInstance();
        paramMap.put(PaymentConstants.CALLBACK_URL, viewModel.getPayTmCredentials(PaymentConstants.PAYTM_LINK_CALLBACK) + "?ORDER_ID=" + getIntentValue().getOrderId());
        paramMap.put(PaymentConstants.CHANNEL_ID, viewModel.getPayTmCredentials(PaymentConstants.PAYTM_LINK_CHANNEL));
        paramMap.put(PaymentConstants.CHECKSUMHASH, checksumHash);
        paramMap.put(PaymentConstants.CUST_ID, getIntentValue().getPaymentUserId());
        paramMap.put(PaymentConstants.INDUSTRY_TYPE_ID, viewModel.getPayTmCredentials(PaymentConstants.PAYTM_LINK_INDUSTRY_KEY));
        paramMap.put(PaymentConstants.PAYTM_MID, viewModel.getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID));
        paramMap.put(PaymentConstants.PAY_TM_ORDER_ID, getIntentValue().getOrderId());
        paramMap.put(PaymentConstants.TXN_AMOUNT, mBinding.edtTxtAmt.getText() != null ? mBinding.edtTxtAmt.getText().toString() : String.valueOf(getIntentValue().getTransactionAmt()));
        paramMap.put(PaymentConstants.WEBSITE, viewModel.getPayTmCredentials(PaymentConstants.PAYTM_LINK_WEBSITE));
        paramMap.put(PaymentConstants.REQUEST_TYPE, getConfig.getProperty(ConfigConstant.PAYTM_LINK_ADDMONEY_REQUESTTYPE));
        paramMap.put(PaymentConstants.SSO_TOKEN, getIntentValue().getPaytmUserToken());

        viewModel.setPaymentRequest(paramMap.toString());

        PaytmOrder Order = new PaytmOrder(paramMap);

        Service.initialize(Order, null);
        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        payTmCallBack();
                    }


                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        payTmCallBack();
                    }

                    @Override
                    public void networkNotAvailable() {
                        payTmCallBack();
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        payTmCallBack();
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {
                        payTmCallBack();
                    }

                    @Override
                    public void onBackPressedCancelTransaction() {
                        payTmCallBack();
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        payTmCallBack();
                    }

                });
    }

    @Override
    public void setPaymentResult(boolean paymentStatus) {
        Intent intent = new Intent();
        intent.putExtra(PaymentIntentConstant.TRANSACTION_STATUS, paymentStatus);
        intent.putExtra(PaymentConstants.PAYMENT_REQUEST, viewModel.getPaymentRequest());
        intent.putExtra(PaymentConstants.PAYMENT_RESPONSE, viewModel.getPaymentResponse());
        intent.putExtra(PaymentConstants.PAYTM_CHECK_SUM, viewModel.getPayTmCheckSum());
        setResult(RESULT_OK, intent);
        this.finish();
    }

    private void payTmCallBack() {
        showProgress(this);
        viewModel.transactionChecksum();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(PaymentConstants.PAYMENT_REQUEST, TextUtils.isEmpty(viewModel.getPaymentRequest()) ? PaymentConstants.CANCELLED_BY_USER : viewModel.getPaymentRequest());
        intent.putExtra(PaymentConstants.PAYMENT_RESPONSE, TextUtils.isEmpty(viewModel.getPaymentResponse()) ? PaymentConstants.CANCELLED_BY_USER : viewModel.getPaymentResponse());
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}