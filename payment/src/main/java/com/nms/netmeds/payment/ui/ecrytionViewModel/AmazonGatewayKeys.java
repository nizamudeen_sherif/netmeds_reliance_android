package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;

class AmazonGatewayKeys {
    @SerializedName("key1")
    private String merchantId;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

}