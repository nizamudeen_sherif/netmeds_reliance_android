package com.nms.netmeds.payment.ui;

class PaymentApiConstant {

    static final String DE_LINK = "oauth2/accessToken/{linkToken}";

    static final String C_MSTAR_PAYMENT_GATEWAY_LIST = "api/v1/gateway/paymentlist";
    static final String C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL_DETAILS = "api/v1/gateway/get";
    static final String C_MSTAR_COMPLETE_ORDER = "api/v1/completeorder/yourreceipt";
    static final String C_MSTAR_PAYMENT_LOG = "api/v1/log/paymentlog";
    static final String C_MSTAR_CREATE_JUS_PAY_CUSTOMER = "api/v1/juspay/createcustomer";
    static final String MSTAR_NOP_PAYMENT = "cart/nop_payment_processed";

    /**
     * PAYTM
     */
    static final String C_PAYTM_INITIATE_WITHRAW_AMOUNT = "api/v1/paytm/withdraw";
    static final String C_PAY_TM_ADD_MONEY = "api/v1/paytm/addmoney";
    static final String C_PAYTM_BALANCE_CHECK = "api/v1/paytm/balancecheck";
    static final String C_PAY_TM_TRANSACTION_CHECK = "api/v1/paytm/transactioncheck";
    static final String PAYTM_SEND_OTP = "signin/otp";
    static final String VALIDATE_PAY_TM_OTP = "signin/validate/otp";
    static final String VALIDATE_PAY_TM_TOKEN = "user/details";
    static final String PAY_TM_WITHDRAW_DETAILS = "paymentservices/HANDLER_FF/withdrawScw";
    static final String PAY_TM_TRANSACTION_STATUS = "merchant-status/getTxnStatus";

}
