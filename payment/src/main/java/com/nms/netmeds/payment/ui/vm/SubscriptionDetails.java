package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SubscriptionDetails {

    @SerializedName("days_between_issues")
    private int daysBetweenIssues;
    @SerializedName("no_of_issues")
    private int noOfIssues;
    @SerializedName("payment_method")
    private String paymentMethod;
    @SerializedName("payment_details")
    private String subscriptionPaymentDetails;
    @SerializedName("lines")
    private ArrayList<SubscribedProducts> subscribedProducts;
    @SerializedName("subcribeamount")
    private double subcribedAmount;
    @SerializedName("subcribe_item_mrp")
    private double subcribedItemMrp;

    public int getDaysBetweenIssues() {
        return this.daysBetweenIssues;
    }

    public void setDaysBetweenIssues(int daysBetweenIssues) {
        this.daysBetweenIssues = daysBetweenIssues;
    }


    public int getNoOfIssues() {
        return this.noOfIssues;
    }

    public void setNoOfIssues(int noOfIssues) {
        this.noOfIssues = noOfIssues;
    }


    public String getPaymentMethod() {
        return this.paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    public String getPaymentDetails() {
        return this.subscriptionPaymentDetails;
    }

    public void setPaymentDetails(String subscriptionPaymentDetails) {
        this.subscriptionPaymentDetails = subscriptionPaymentDetails;
    }


    public ArrayList<SubscribedProducts> getLines() {
        return this.subscribedProducts;
    }

    public void setLines(ArrayList<SubscribedProducts> subscribedProducts) {
        this.subscribedProducts = subscribedProducts;
    }


    public double getSubcribeamount() {
        return this.subcribedAmount;
    }

    public void setSubcribeamount(double subcribedAmount) {
        this.subcribedAmount = subcribedAmount;
    }


    public double getSubcribeItemMrp() {
        return this.subcribedItemMrp;
    }

    public void setSubcribeItemMrp(double subcribedItemMrp) {
        this.subcribedItemMrp = subcribedItemMrp;
    }
}
