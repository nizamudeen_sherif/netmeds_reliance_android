package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class PaypalRedirectResponse {
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("token")
    private String token;
    @SerializedName("PayerID")
    private String payerID;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPayerID() {
        return payerID;
    }

    public void setPayerID(String payerID) {
        this.payerID = payerID;
    }
}