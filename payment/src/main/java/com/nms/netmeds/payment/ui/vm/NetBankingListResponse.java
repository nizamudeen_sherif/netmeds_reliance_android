package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NetBankingListResponse {
    @SerializedName("payment_methods")
    private ArrayList<NetBankingPaymentMethods> paymentMethods;

    public ArrayList<NetBankingPaymentMethods> getPaymentMethods() {
        return this.paymentMethods;
    }

    public void setPaymentMethods(ArrayList<NetBankingPaymentMethods> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
