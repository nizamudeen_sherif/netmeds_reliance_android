package com.nms.netmeds.payment.ui;

import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.payment.model.PayTmTransactionStatusResponse;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentKeyResponse;
import com.nms.netmeds.payment.ui.vm.JusPayCreateOrderAndOrderStatusResponse;
import com.nms.netmeds.payment.ui.vm.JusPayCreateUpdateResponse;
import com.nms.netmeds.payment.ui.vm.NetBankingListResponse;
import com.nms.netmeds.payment.ui.vm.PayPalTokenResponse;
import com.nms.netmeds.payment.ui.vm.PayTmChecksumResponse;
import com.nms.netmeds.payment.ui.vm.PayTmWithdrawResponse;
import com.nms.netmeds.payment.ui.vm.PaypalExecutePaymentRequest;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequest;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentResponse;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmValidateOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateTokenResponse;
import com.nms.netmeds.payment.ui.vm.PaytmWithdrawRequest;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RetrofitPaymentInterface {


    @FormUrlEncoded
    @POST(PaymentApiConstant.C_MSTAR_PAYMENT_GATEWAY_LIST)
    Call<MStarBasicResponseTemplateModel> getPaymentList(@HeaderMap Map<String, String> header,
                                                         @Field(AppConstant.ORDER_VALUE) String grandTotal,
                                                         @Field(AppConstant.APP_SOURCE) String source,
                                                         @Field(AppConstant.APP_VERSION) String version,
                                                         @Field(AppConstant.AMAZON_FLAG) String amazonFlag,
                                                         @Field(AppConstant.CARTID) String cartId);

    @GET(PaymentApiConstant.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL_DETAILS)
    Call<PaymentKeyResponse> getPaymentGatewayCredentialDetails(@HeaderMap Map<String, String> header);

    @GET(PaymentApiConstant.C_MSTAR_CREATE_JUS_PAY_CUSTOMER)
    Call<JusPayCreateUpdateResponse> createAndUpdateJusPayCustomer(@HeaderMap Map<String, String> header);

    @POST()
    Call<JusPayCreateOrderAndOrderStatusResponse> callJusPayAPIForOrderDetails(@HeaderMap Map<String, String> header, @Body MultipartBody body, @Url String url);

    @GET(PaymentApiConstant.MSTAR_NOP_PAYMENT)
    Call<MStarBasicResponseTemplateModel> mStarNOPPayment(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cartId);

    @FormUrlEncoded
    @POST(PaymentApiConstant.C_MSTAR_COMPLETE_ORDER)
    Call<MStarBasicResponseTemplateModel> completeOrderDetails(@HeaderMap Map<String, String> header,
                                                               @Field(AppConstant.CARTID) String cartId,
                                                               @Field(PaymentConstants.PAYMENT_METHOD) String paymentMethod,
                                                               @Field(AppConstant.REQUESTRX) String requestRX,
                                                               @Field(AppConstant.PRESCRIPTION) String prescription,
                                                               @Field(PaymentConstants.ORDERTYPE) String orderType,
                                                               @Field(PaymentConstants.FORM_DATA_PAYTM_CHECK_SUM) String paytmCheckSum,
                                                               @Field(PaymentConstants.FORM_DATA_PAY_PAL_PAY_ID) String paypalPayId);


    /**
     * PAYMENTS____________________________________________________________________________________________
     */
    @GET()
    Call<NetBankingListResponse> netBankingList(@Url String url);


    /**
     * PAYPAL________________
     */
    @FormUrlEncoded
    @POST()
    Call<PayPalTokenResponse> getPayPalToken(@Field(PaymentConstants.GRANT_TYPE) String clientCredentials, @Url String url);

    @POST()
    Call<PaypalPaymentResponse> initPayPalPayment(@Body PaypalPaymentRequest request, @Url String url);

    @POST
    Call<PaypalPaymentResponse> payPalPaymentExecute(@Url String utl, @Body PaypalExecutePaymentRequest request);


    /**
     * PAYTM____________
     */
    @POST(PaymentApiConstant.PAYTM_SEND_OTP)
    Call<PaytmLinkWalletOtpResponse> sendPayTmOtp(@Body PaytmLinkWalletOtpRequest request);

    @POST(PaymentApiConstant.VALIDATE_PAY_TM_OTP)
    Call<PaytmValidateOtpResponse> validatePayTmOtp(@Body PaytmValidateOtpRequest request);

    @GET(PaymentApiConstant.VALIDATE_PAY_TM_TOKEN)
    Call<PaytmValidateTokenResponse> validatePayTmToken(@Header(PaymentConstants.SESSION_TOKEN) String token);

    @FormUrlEncoded
    @POST(PaymentApiConstant.C_PAYTM_BALANCE_CHECK)
    Call<PayTmChecksumResponse> payTmBalanceChecksum(@HeaderMap Map<String, String> header, @Field(PaymentConstants.USER_TOKEN) String accessToken, @Field(PaymentConstants.TOTAL_AMOUNT) String totalAmount);

    @FormUrlEncoded
    @POST(PaymentApiConstant.C_PAYTM_INITIATE_WITHRAW_AMOUNT)
    Call<PayTmChecksumResponse> payTmWithdrawFromWallet(@HeaderMap Map<String, String> header, @Field(PaymentConstants.API_APPIP) String appIp,
                                                        @Field(PaymentConstants.API_CUSTOMERID) String customerId, @Field(AppConstant.CARTID) String cartId, @Field(PaymentConstants.API_SSOTOKEN) String ssoToken
            , @Field(PaymentConstants.P_FLAG) boolean pFlag, @Field(PaymentConstants.DC_ORDER_ID) String orderId, @Field(PaymentConstants.API_TRANSACTION_AMOUNT) String txnAmount);

    @POST(PaymentApiConstant.PAY_TM_WITHDRAW_DETAILS)
    Call<PayTmWithdrawResponse> payTmWithdrawDetails(@Body PaytmWithdrawRequest request);

    @FormUrlEncoded
    @POST(PaymentApiConstant.C_PAY_TM_ADD_MONEY)
    Call<PayTmChecksumResponse> payTmAddMoneyChecksum(@HeaderMap Map<String, String> header, @Field(AppConstant.CARTID) String cartId, @Field(PaymentConstants.API_SSOTOKEN) String ssoToken,
                                                      @Field(PaymentConstants.API_CUSTOMERID) String custId, @Field(PaymentConstants.API_TRNXAMOUNT) String txnAmount, @Field(PaymentConstants.P_FLAG) boolean pFlag
            , @Field(PaymentConstants.DC_ORDER_ID) String orderId);

    @POST()
    Call<PayTmTransactionStatusResponse> payTmTransactionCheckSumStatus(@Url String query);

    @FormUrlEncoded
    @POST(PaymentApiConstant.C_PAY_TM_TRANSACTION_CHECK)
    Call<PayTmChecksumResponse> payTmTransactionChecksum(@HeaderMap Map<String, String> header, @Field(AppConstant.CARTID) String cartId,
                                                         @Field(PaymentConstants.P_FLAG) boolean pFlag, @Field(PaymentConstants.DC_ORDER_ID) String orderId);

    @DELETE(PaymentApiConstant.DE_LINK)
    Call<Object> delinkPaytm(@Path("linkToken") String linkToken, @Header("Authorization") String auth,
                             @Header("Content-Type") String contentType, @Header("Content-Length") String contentLength, @Header("Host") String host);

    @FormUrlEncoded
    @POST(PaymentApiConstant.C_MSTAR_PAYMENT_LOG)
    Call<MStarBasicResponseTemplateModel> paymentLog(@HeaderMap Map<String, String> header,
                                                     @Field(AppConstant.ORDER_ID) String orderId,
                                                     @Field(AppConstant.PAYMENT_METHOD) String paymentMethod,
                                                     @Field(AppConstant.REQUEST) String request,
                                                     @Field(AppConstant.RESPONSE) String response,
                                                     @Field(AppConstant.APP_VERSION) String appVersion,
                                                     @Field(AppConstant.APP_SOURCE_NAME) String sourceName);
}