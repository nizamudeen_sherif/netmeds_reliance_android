package com.nms.netmeds.payment.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.view.SingleClickSubscriptionDialog;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.ui.OnboardingSubscriptionActivity;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityOrderPlaceSuccessfulBinding;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;

public class OrderPlacedSuccessfullyActivity extends BaseViewModelActivity<OrderPlacedSuccessfullyViewModel> implements OrderPlacedSuccessfullyViewModel.OrderPlacedSuccessfulListener,
        BannerAdapter.BannerAdapterListener, SingleClickSubscriptionDialog.SingleClickSubscriptionDilaogListener {

    private OrderPlacedSuccessfullyViewModel viewModel;
    private ActivityOrderPlaceSuccessfulBinding successfulBinding;
    private String orderId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        successfulBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_place_successful);
        successfulBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(successfulBinding.toolbar);
        BasePreference.getInstance(this).setCartCount(0);
        BasePreference.getInstance(this).setCartId("");
        BasePreference.getInstance(this).setAlternateAndOriginalCartProductDetails("");
        BasePreference.getInstance(this).setCartSubstituted(false);
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(new ArrayList<MStarUploadPrescription>());
        MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(new ArrayList<String>());
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected OrderPlacedSuccessfullyViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(OrderPlacedSuccessfullyViewModel.class);
        String address = getIntent().getStringExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS);
        if (!TextUtils.isEmpty(address)) {
            viewModel.setAddress(address);
        }
        if (getIntent().hasExtra(SubscriptionIntentConstant.REFILL_SUBSCRIPTION_RESPONSE)) {
            viewModel.setRefillResponse(getIntent().getExtras().getString(SubscriptionIntentConstant.REFILL_SUBSCRIPTION_RESPONSE));
        }
        if (getIntent().hasExtra(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE)) {
            viewModel.completeOrderRespose(getIntent().getExtras().getString(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE));
        }
        if (getIntent().hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL)) {
            viewModel.setDiagnosticOrderCreationDetail(getIntent().getBundleExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL));
        }
        if (getIntent().hasExtra(SubscriptionIntentConstant.ORDER_ID)) {
            viewModel.setDiagnosticOrderId(getIntent().getStringExtra(SubscriptionIntentConstant.ORDER_ID));
        }
        if (getIntent().hasExtra(DiagnosticConstant.KEY_ORDER_DATE)) {
            viewModel.setDiagnosticOrderDate(getIntent().getStringExtra(DiagnosticConstant.KEY_ORDER_DATE));
        }
        viewModel.getReferAndEarnMutableLiveData().observe(this, new ReferralObserver());
        viewModel.init(this, successfulBinding, this, getIntent(), this);
        return viewModel;
    }

    @Override
    public void checkIntentValue() {
        String intentValue = getIntent().getStringExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG);
        viewModel.renderPageBasedOnIntentValue(intentValue);
    }

    public void smoothScrollTop() {
        successfulBinding.svParent.postDelayed(new Runnable() {
            @Override
            public void run() {
                successfulBinding.svParent.fullScroll(View.FOCUS_UP);
            }
        }, 100);
    }


    @Override
    public void initiateSubscription() {
        orderId = getIntent().hasExtra(SubscriptionIntentConstant.ORDER_ID) ? getIntent().getExtras().getString(SubscriptionIntentConstant.ORDER_ID) : "";
        if (!SubscriptionHelper.getInstance().isSubscriptionFlag() && !getIntent().getBooleanExtra(PaymentIntentConstant.M2_FLAG, false) && !viewModel.isFromDiagnostic()) {
            openSubscriptionInitialDialog();
        }
    }

    //todo
    private class ReferralObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel referralCodeResponse) {
            viewModel.onReferralAvailable(referralCodeResponse);
            successfulBinding.setViewModel(viewModel);
        }
    }


    @Override
    public void vmSubscriptionOnboardingActivity() {
        startActivity(new Intent(this, OnboardingSubscriptionActivity.class));
    }

    @Override
    public void shareIntent(String title, String mailContent, String message, String url) {
        PackageManager pm = this.getPackageManager();
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        List<ResolveInfo> resInfo = pm.queryIntentActivities(sharingIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<>();
        for (int i = 0; i < resInfo.size(); i++) {
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("whatsapp")) {
                switch (packageName) {
                    case "com.twitter.android":
                        intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + url);
                        break;
                    case "com.facebook.katana":
                        intent.putExtra(Intent.EXTRA_TEXT, url);
                        break;
                    case "com.google.android.gm":
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, title != null && !TextUtils.isEmpty(title) ? title : "");
                        intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(mailContent != null && !TextUtils.isEmpty(mailContent) ? mailContent : ""));
                        intent.setType("message/rfc822");
                        break;
                    case "com.whatsapp":
                        intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + url);
                        intent.setPackage(packageName);
                        break;
                    case "com.android.mms":
                        intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + url);
                        intent.setPackage(packageName);
                        break;
                }
            } else {
                intent.putExtra(Intent.EXTRA_TEXT, title + "\n" + url);
            }
            intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
        }
        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);
        Intent openInChooser = Intent.createChooser(sharingIntent, getString(R.string.text_invite_friends));
        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }

    public void openSubscriptionInitialDialog() {
        if (PaymentHelper.getDeliveryEstimateWithYear() != null && !TextUtils.isEmpty(getIntent().getStringExtra(SubscriptionIntentConstant.ORDER_ID))) {
            SingleClickSubscriptionDialog singleClickSubscriptionDialog = new SingleClickSubscriptionDialog(this, PaymentHelper.getDeliveryEstimateWithYear(), getIntent().getStringExtra(SubscriptionIntentConstant.ORDER_ID));
            getSupportFragmentManager().beginTransaction().add(singleClickSubscriptionDialog, IntentConstant.SINGLE_CLICK_SUBSCRIPTION_DIALOG_FLAG).commitNowAllowingStateLoss();
        }
    }

    @Override
    public void navigateToSubscription() {
        Snackbar snackbar = Snackbar.make(successfulBinding.toolbar, getString(com.nms.netmeds.base.R.string.text_subscription_enabled), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(com.nms.netmeds.base.R.string.text_view), subscriptionViewClickListener);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, com.nms.netmeds.base.R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, com.nms.netmeds.base.R.color.colorMediumPink));
        snackbar.show();
        SubscriptionHelper.getInstance().setRefillSubscription(true);
    }

    @Override
    public void snackBarView(String message) {
        SnackBarHelper.snackBarCallBack(successfulBinding.svParent, this, message);
    }

    View.OnClickListener subscriptionViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Navigate to Subscription page after Subscription has enabled.
            viewModel.setIsFromOrder();
            startHome(true);
        }
    };

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void initDiagnosticOrderTack(String orderId, String mobileNo) {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_ORDER_ID, orderId);
        intent.putExtra(DiagnosticConstant.KEY_MOBILE_NO, mobileNo);
        intent.putExtra(DiagnosticConstant.KEY_INTENT_FROM_ORDER_SUCCESS, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        LaunchIntentManager.routeToActivity(getString(R.string.route_diagnostic_track_order), intent, this);
        OrderPlacedSuccessfullyActivity.this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startHome(false);
    }

    @Override
    public void startHome(boolean value) {
        Intent intent = new Intent();
        if (SubscriptionHelper.getInstance().isThisIssueOrder()) {
            intent.putExtra(IntentConstant.HOME_FLAG_FROM_SUBSCRIPTION_ORDER_PLACED_SUCCESSFUL, true);
        } else if (viewModel.isFromOrder()) {
            intent.putExtra(PaymentIntentConstant.HOME_FLAG_FROM_ORDER_PLACED_SUCCESSFUL, value);
        } else if (viewModel.isFromDiagnostic()) {
            intent.putExtra(PaymentIntentConstant.HOME_FLAG_FROM_DIAGNOSTIC_PLACED_SUCCESSFUL, value);
        } else {
            intent.putExtra(PaymentIntentConstant.HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL, value);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        LaunchIntentManager.routeToActivity(getString(R.string.route_navigation_activity), intent, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_ORDER_CONFIRMED);
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        if (viewModel.isFromDiagnostic())
            WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_PLACE_ORDER);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void updateRatingView(final int ratingCount) {
        successfulBinding.imgRating1.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 1 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive_2));
        successfulBinding.imgRating2.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 2 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive_2));
        successfulBinding.imgRating3.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 3 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive_2));
        successfulBinding.imgRating4.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 4 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive_2));
        successfulBinding.imgRating5.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 5 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive_2));

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showSnackbarMessage(getString(R.string.string_thanks_note));
                successfulBinding.cvReviewOrder.setVisibility(View.GONE);
            }
        }, 500);
    }

    @Override
    public void showSnackbarMessage(String message) {
        SnackBarHelper.snackBarCallBack(successfulBinding.svParent, this, message);
    }

    @Override
    public boolean isM2Order() {
        return getIntent().hasExtra(PaymentIntentConstant.M2_FLAG) && getIntent().getBooleanExtra(PaymentIntentConstant.M2_FLAG, false);
    }

    @Override
    public void disableReview(boolean disable) {
        successfulBinding.imgRating1.setClickable(!disable);
        successfulBinding.imgRating2.setClickable(!disable);
        successfulBinding.imgRating3.setClickable(!disable);
        successfulBinding.imgRating4.setClickable(!disable);
        successfulBinding.imgRating5.setClickable(!disable);
    }
}
