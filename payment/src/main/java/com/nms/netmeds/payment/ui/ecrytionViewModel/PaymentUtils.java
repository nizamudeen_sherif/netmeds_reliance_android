package com.nms.netmeds.payment.ui.ecrytionViewModel;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.EncryptionUtils;
import com.nms.netmeds.payment.NetmedsPaymentProcess;
import com.nms.netmeds.payment.ui.PaymentConstants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PaymentUtils {

    public static String payTmCredentials(int key, String paymentCredentials) {
        String value = "";
        if (!TextUtils.isEmpty(paymentCredentials)) {
            PaymentKeyResponse response = new Gson().fromJson(paymentCredentials, PaymentKeyResponse.class);
            if (response != null && response.getResult() != null && response.getResult().getPayTmGatewayKeys() != null) {
                PayTmGatewayKeys payTmGatewayKeys = response.getResult().getPayTmGatewayKeys();
                switch (key) {
                    case PaymentConstants.PAYTM_LINK_CLIENT_ID:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getClientId());
                        break;
                    case PaymentConstants.PAYTM_LINK_SECRET_KEY:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getSecretKey());
                        break;
                    case PaymentConstants.PAYTM_LINK_MID:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getMid());
                        break;
                    case PaymentConstants.PAYTM_LINK_CHANNEL:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getChannel());
                        break;
                    case PaymentConstants.PAYTM_LINK_INDUSTRY_KEY:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getIndustryKey());
                        break;
                    case PaymentConstants.PAYTM_LINK_WEBSITE:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getWebSite());
                        break;
                    case PaymentConstants.PAYTM_LINK_CALLBACK:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getCallBack());
                        break;
                    case PaymentConstants.PAYTM_ADD_MONEY:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getAddMoney());
                        break;
                    case PaymentConstants.PAYTM_BALANCE_CHECK:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getBalanceCheck());
                        break;
                    case PaymentConstants.PAYTM_WITHDRAW:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getWithDraw());
                        break;
                    case PaymentConstants.PAYTM_TRANSACTION:
                        value = PaymentUtils.ValidateString(payTmGatewayKeys.getTransaction());
                        break;
                }
            }
        }
        return PaymentUtils.getDeCodedString(value);
    }

    public static String amazonPayCredential(String credentials) {
        String value = "";
        if (!TextUtils.isEmpty(credentials)) {
            PaymentKeyResponse response = new Gson().fromJson(credentials, PaymentKeyResponse.class);
            if (response != null && response.getResult() != null && response.getResult().getAmazonGatewayKeys() != null) {
                AmazonGatewayKeys gatewayKeys = response.getResult().getAmazonGatewayKeys();
                if (!TextUtils.isEmpty(gatewayKeys.getMerchantId())) {
                    value = PaymentUtils.ValidateString(gatewayKeys.getMerchantId());
                }
            }
        }
        return PaymentUtils.getDeCodedString(value);
    }

    public static String jusPayCredential(int id, String credentials) {
        String value = "";
        if (!TextUtils.isEmpty(credentials)) {
            PaymentKeyResponse response = new Gson().fromJson(credentials, PaymentKeyResponse.class);
            if (response != null && response.getResult() != null && response.getResult().getJusPayGatewayKey() != null) {
                JusPayGatewayKey gatewayKeys = response.getResult().getJusPayGatewayKey();
                switch (id) {
                    case PaymentConstants.JUS_PAY_RETURN_URL:
                        value = PaymentUtils.ValidateString(gatewayKeys.getReturnUrl());
                        break;
                    case PaymentConstants.JUS_PAY_CREATE_ORDER:
                        value = PaymentUtils.ValidateString(gatewayKeys.getCreateOrder());
                        break;
                    case PaymentConstants.JUS_PAY_UPDATE_CUSTOMER:
                        value = PaymentUtils.ValidateString(gatewayKeys.getUpdateCustomer());
                        break;
                    case PaymentConstants.JUS_PAY_ORDER_STATUS:
                        value = PaymentUtils.ValidateString(gatewayKeys.getOrderStatus());
                        break;
                    case PaymentConstants.JUS_PAY_CREATE_CUSTOMER:
                        value = PaymentUtils.ValidateString(gatewayKeys.getCreateCustomer());
                        break;
                    case PaymentConstants.JUS_PAY_PAYMENT_METHODS:
                        value = PaymentUtils.ValidateString(gatewayKeys.getPaymentMethods());
                        break;
                    case PaymentConstants.JUS_PAY_END_URL:
                        value = PaymentUtils.ValidateString(gatewayKeys.getEndUrl());
                        break;
                    case PaymentConstants.JUS_PAY_GATEWAY_KEY:
                        value = PaymentUtils.ValidateString(gatewayKeys.getJuspayGatewayKey());
                        break;
                    case PaymentConstants.JUS_PAY_CLIENT_ID:
                        value = PaymentUtils.ValidateString(gatewayKeys.getJuspayClientKey());
                        break;
                    case PaymentConstants.JUS_PAY_MERCHENT_ID:
                        value = PaymentUtils.ValidateString(gatewayKeys.getJuspayMerchantId());
                        break;
                }
            }
        }
        return PaymentUtils.getDeCodedString(value);
    }

    public static String minKasuCredential(int id, String credentials) {
        String value = "";
        if (!TextUtils.isEmpty(credentials)) {
            PaymentKeyResponse response = new Gson().fromJson(credentials, PaymentKeyResponse.class);
            if (response != null && response.getResult() != null && response.getResult().getMinKasuCredentials() != null) {
                MinKasuCredentials gatewayKeys = response.getResult().getMinKasuCredentials();
                switch (id) {
                    case PaymentConstants.MINKASU_MERCHANT_ID:
                        value = PaymentUtils.ValidateString(gatewayKeys.getMerchantId());
                        break;
                    case PaymentConstants.MINKASU_ACCESS_TOKEN:
                        value = PaymentUtils.ValidateString(gatewayKeys.getAccessToken());
                        break;
                }
            }
        }
        return PaymentUtils.getDeCodedString(value);
    }

    public static String codCredential(int id, String credentials) {
        String value = "";
        if (!TextUtils.isEmpty(credentials)) {
            PaymentKeyResponse response = new Gson().fromJson(credentials, PaymentKeyResponse.class);
            if (response != null && response.getResult() != null && response.getResult().getCodCreditials() != null) {
                CodCredentials gatewayKeys = response.getResult().getCodCreditials();
                switch (id) {
                    case PaymentConstants.COD_PAYMENT_METHOD:
                        value = PaymentUtils.ValidateString(gatewayKeys.getPayementMethod());
                        break;
                    case PaymentConstants.COD_MINIMUM_AMOUNT:
                        value = PaymentUtils.ValidateString(gatewayKeys.getMinimumAmount());
                        break;
                    case PaymentConstants.COD_MAXIMUM_AMOUNT:
                        value = PaymentUtils.ValidateString(gatewayKeys.getMaximumAmount());
                        break;
                }
            }
        }
        return PaymentUtils.getDeCodedString(value);
    }

    public static String getDeCodedString(String value) {
        return EncryptionUtils.decryptAESCBCEncryption(value);
    }

    public static String ValidateString(String value) {
        return !TextUtils.isEmpty(value) ? value : "";
    }

    public static boolean luhnAlgorithim(String card) {
        int sum = 0;
        if (!TextUtils.isEmpty(card)) {
            if (!(card.trim().length() > 0))
                return false;
            String cardNumber = card.replaceAll(" ", "");
            boolean alternate = false;
            for (int i = cardNumber.length() - 1; i >= 0; i--) {
                int n = Integer.parseInt(cardNumber.substring(i, i + 1));
                if (alternate) {
                    n *= 2;
                    if (n > 9) {
                        n = (n % 10) + 1;
                    }
                }
                sum += n;
                alternate = !alternate;
            }
            return (sum % 10 == 0);
        }
        return false;
    }

    public static boolean expiryValidation(String string, NetmedsPaymentProcess netmedsPaymentProcess) {
        if (TextUtils.isEmpty(string))
            return false;
        else {
            separateDateMonth(string, netmedsPaymentProcess);
            return validateDate(netmedsPaymentProcess);
        }
    }

    private static boolean validateDate(NetmedsPaymentProcess netmedsPaymentProcess) {
        DateFormat dateFormat = new SimpleDateFormat("yy", Locale.getDefault());
        int currentYear = Integer.parseInt(dateFormat.format(Calendar.getInstance().getTime()));
        return !TextUtils.isEmpty(netmedsPaymentProcess.getValidationYear()) && Integer.parseInt(netmedsPaymentProcess.getValidationYear()) >= currentYear;
    }

    private static void separateDateMonth(String expiryDate, NetmedsPaymentProcess netmedsPaymentProcess) {
        String[] expiryList = expiryDate.split("/");
        if (expiryList.length > 0 && expiryList[0] != null)
            netmedsPaymentProcess.setValidationMonth(expiryList[0]);
        if (expiryList.length > 1 && expiryList[1] != null)
            netmedsPaymentProcess.setValidationYear(expiryList[1]);
    }

    public static boolean cvvValidation(String string) {
        if (string != null && string.length() > 0) {
            String cvv = string.trim();
            return cvv.length() > 2 && cvv.length() < 5;
        } else return false;
    }

    public static boolean cardNameValidation(String cardName) {
        if (!TextUtils.isEmpty(cardName))
            return cardName.trim().matches("[a-zA-Z ]+");
        else return false;
    }


    public static String getPayPalCredentials(String id, BasePreference basePreference) {
        String keyValue = "";
        if (!TextUtils.isEmpty(basePreference.getPaymentCredentials())) {
            PaymentKeyResponse response = new Gson().fromJson(basePreference.getPaymentCredentials(), PaymentKeyResponse.class);
            if (response != null && response.getResult() != null && response.getResult().getPayPalGatewayKey() != null) {
                PayPalGatewayKey payPalGatewayKey = response.getResult().getPayPalGatewayKey();
                switch (id) {
                    case PaymentConstants.PAY_PAL_CLIENT_ID:
                        keyValue = PaymentUtils.ValidateString(payPalGatewayKey.getClientId());
                        break;
                    case PaymentConstants.PAY_PAL_SECRET:
                        keyValue = PaymentUtils.ValidateString(payPalGatewayKey.getSecret());
                        break;
                    case PaymentConstants.PAY_PAL_RETURN_URL:
                        keyValue = PaymentUtils.ValidateString(payPalGatewayKey.getReturnUrlAndroid());
                        break;
                    case PaymentConstants.PAY_PAL_AUTH_URL:
                        keyValue = PaymentUtils.ValidateString(payPalGatewayKey.getAuthTokenUrl());
                        break;
                    case PaymentConstants.PAY_PAL_CURRENCY:
                        keyValue = PaymentUtils.ValidateString(payPalGatewayKey.getCurrency());
                        break;
                    case PaymentConstants.PAY_PAL_PAYMENT_URL:
                        keyValue = PaymentUtils.ValidateString(payPalGatewayKey.getPaymentUrl());
                        break;
                }
            }
        }
        return PaymentUtils.getDeCodedString(keyValue);
    }


    @SuppressLint("StaticFieldLeak")
    public static class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private final String param;
        private final NetmedsPaymentProcess netmedsPaymentProcess;

        public AsyncTaskRunner(String param, NetmedsPaymentProcess netmedsPaymentProcess) {
            this.param = param;
            this.netmedsPaymentProcess = netmedsPaymentProcess;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL("https://securegw.paytm.in/paymentservices/pay/consult").openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setUseCaches(false);
                connection.setDoOutput(true);

                DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
                requestWriter.writeBytes(param);
                requestWriter.close();
                String responseData;
                InputStream is = connection.getInputStream();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                responseData = responseReader.readLine();
                responseReader.close();
                return responseData;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            netmedsPaymentProcess.payTmConsultBalance(result);
        }

    }
}