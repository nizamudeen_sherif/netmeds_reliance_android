/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaypalPaymentResponse {
    @SerializedName("id")
    private String id;
    @SerializedName("create_time")
    private String createTime;
    @SerializedName("update_time")
    private String updateTime;
    @SerializedName("state")
    private String state;
    @SerializedName("cart")
    private String cart;
    @SerializedName("intent")
    private String intent;
    @SerializedName("payer")
    private PaypalPaymentRequestPayer payer;
    @SerializedName("transactions")
    private List<PaypalPaymentRequestTransaction> transactions = null;
    @SerializedName("links")
    private List<PaypalPaymentLink> links = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public PaypalPaymentRequestPayer getPayer() {
        return payer;
    }

    public void setPayer(PaypalPaymentRequestPayer payer) {
        this.payer = payer;
    }

    public List<PaypalPaymentRequestTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<PaypalPaymentRequestTransaction> transactions) {
        this.transactions = transactions;
    }

    public List<PaypalPaymentLink> getLinks() {
        return links;
    }

    public void setLinks(List<PaypalPaymentLink> links) {
        this.links = links;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }
}
