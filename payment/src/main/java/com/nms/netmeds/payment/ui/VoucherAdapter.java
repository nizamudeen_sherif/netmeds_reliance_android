package com.nms.netmeds.payment.ui;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.model.AppliedVoucher;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.AdapterVoucherBinding;

import java.util.List;

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.VoucherHolder> {
    private final List<AppliedVoucher> mAppliedVoucher;
    private final VoucherListener mVoucherListener;

    VoucherAdapter(List<AppliedVoucher> appliedVouchers, VoucherListener voucherListener) {
        this.mAppliedVoucher = appliedVouchers;
        this.mVoucherListener = voucherListener;
    }

    @NonNull
    @Override
    public VoucherHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterVoucherBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_voucher, viewGroup, false);
        return new VoucherHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VoucherHolder voucherHolder, int i) {
        final AppliedVoucher voucher = mAppliedVoucher.get(i);
        voucherHolder.voucherBinding.voucherCode.setText(TextUtils.isEmpty(voucher.getCode()) ? "" : voucher.getCode());
        voucherHolder.voucherBinding.removeVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVoucherListener.removeVoucher(voucher.getCode());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAppliedVoucher.size();
    }

    class VoucherHolder extends RecyclerView.ViewHolder {
        private final AdapterVoucherBinding voucherBinding;

        VoucherHolder(AdapterVoucherBinding voucherBinding) {
            super(voucherBinding.getRoot());
            this.voucherBinding = voucherBinding;
        }
    }

    public interface VoucherListener {
        void removeVoucher(String code);
    }
}