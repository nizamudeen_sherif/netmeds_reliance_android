/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaypalPaymentRequestPayer {
    @SerializedName("payment_method")
    private String paymentMethod;
    @SerializedName("payer_info")
    private PaypalPaymentRequestPayerInfo payerInfo;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaypalPaymentRequestPayerInfo getPayerInfo() {
        return payerInfo;
    }

    public void setPayerInfo(PaypalPaymentRequestPayerInfo payerInfo) {
        this.payerInfo = payerInfo;
    }

}