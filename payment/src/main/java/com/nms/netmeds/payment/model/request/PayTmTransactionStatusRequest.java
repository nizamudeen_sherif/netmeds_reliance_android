package com.nms.netmeds.payment.model.request;

import com.google.gson.annotations.SerializedName;

public class PayTmTransactionStatusRequest {
    @SerializedName("MID")
    private String mId;
    @SerializedName("ORDERID")
    private String orderId;
    @SerializedName("CHECKSUMHASH")
    private String checksumHash;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getChecksumHash() {
        return checksumHash;
    }

    public void setChecksumHash(String checksumHash) {
        this.checksumHash = checksumHash;
    }
}
