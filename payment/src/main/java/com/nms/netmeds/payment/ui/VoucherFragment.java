package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.model.AppliedVoucher;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.FragmentVoucherBinding;

import java.util.List;

@SuppressLint("ValidFragment")
public class VoucherFragment extends BaseBottomSheetFragment implements VoucherViewModel.VoucherInterface, VoucherAdapter.VoucherListener {
    private final VoucherListener mListener;
    private final List<AppliedVoucher> mVoucherList;
    private FragmentVoucherBinding mBinding;
    private VoucherViewModel viewModel;
    private double grandTotal;
    private boolean m2Order = false;
    private MStarCartDetails cartDetails;

    @SuppressLint("ValidFragment")
    public VoucherFragment(VoucherListener listener, List<AppliedVoucher> voucherList, double grandTotal, boolean m2Order, MStarCartDetails cartDetails) {
        this.mListener = listener;
        this.mVoucherList = voucherList;
        this.grandTotal = grandTotal;
        this.m2Order = m2Order;
        this.cartDetails = cartDetails;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_voucher, container, false);
        viewModel = new VoucherViewModel(getActivity().getApplication());
        viewModel.init(mBinding, BasePreference.getInstance(getActivity()), this, mVoucherList, grandTotal, m2Order, cartDetails);
        mBinding.setViewModel(viewModel);
        return mBinding.getRoot();
    }

    @Override
    public void dismissDialog() {
        this.dismissAllowingStateLoss();
    }

    @Override
    public void appliedVoucher(List<AppliedVoucher> appliedVoucherList) {
        mListener.setAppliedVoucher(appliedVoucherList);
        this.dismissAllowingStateLoss();
    }

    @Override
    public void vmShowProgress() {
        showProgress(getActivity());
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void setError(String errorMsg) {
        this.dismissAllowingStateLoss();
        dismissProgress();
        mListener.showAlert(errorMsg);
    }

    @Override
    public void setVoucherListView(List<AppliedVoucher> appliedVouchers) {
        VoucherAdapter adapter = new VoucherAdapter(appliedVouchers, this);
        mBinding.voucherList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.voucherList.setAdapter(adapter);
    }

    @Override
    public void removeVoucher(String code) {
        showProgress(getActivity());
        viewModel.isRemoveVoucher = true;
        viewModel.voucherCode = code;
        viewModel.initApiCall(APIServiceManager.MSTAR_UNAPPLY_VOUCHER, BasePreference.getInstance(getActivity()).getMstarBasicHeaderMap(), viewModel.getCurrentCartId());
    }

    public interface VoucherListener {
        void setAppliedVoucher(List<AppliedVoucher> appliedVoucher);

        void showAlert(String message);
    }

}
