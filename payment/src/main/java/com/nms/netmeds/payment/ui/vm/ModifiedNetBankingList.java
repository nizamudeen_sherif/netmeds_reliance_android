package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModifiedNetBankingList {
    @SerializedName("payment_methods")
    private List<ModifiedPaymentMethod> paymentMethods = null;

    public List<ModifiedPaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<ModifiedPaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
