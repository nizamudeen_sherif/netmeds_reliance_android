package com.nms.netmeds.payment.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.AdapterNetbankingSubListBinding;
import com.nms.netmeds.payment.ui.vm.NetBankingPaymentMethods;

import java.util.List;

public class NetBankingListAdapter extends RecyclerView.Adapter<NetBankingListAdapter.NetBankingListHolder> {

    private final List<NetBankingPaymentMethods> list;
    private final NetBankingListener listener;

    NetBankingListAdapter(List<NetBankingPaymentMethods> list, NetBankingListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public NetBankingListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterNetbankingSubListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_netbanking_sub_list, parent, false);
        return new NetBankingListAdapter.NetBankingListHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NetBankingListHolder holder, int position) {
        final NetBankingPaymentMethods consultationPaymentMethods = list.get(position);

        if (consultationPaymentMethods != null) {

            if (!TextUtils.isEmpty(consultationPaymentMethods.getDescription()) && !TextUtils.isEmpty(consultationPaymentMethods.getPaymentMethod())) {
                holder.mBinding.bankName.setText(consultationPaymentMethods.getDescription());
            }

            holder.mBinding.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setBank(consultationPaymentMethods.getDescription(), consultationPaymentMethods.getPaymentMethod());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setBank(String description, String paymentMethod) {
        Intent intent = new Intent();
        intent.putExtra(PaymentConstants.BANK_NAME, description);
        intent.putExtra(PaymentConstants.BANK_ID, paymentMethod);
        listener.setBankSelection(intent);
    }

    public interface NetBankingListener {
        void setBankSelection(Intent intent);
    }

    class NetBankingListHolder extends RecyclerView.ViewHolder {
        final AdapterNetbankingSubListBinding mBinding;

        NetBankingListHolder(AdapterNetbankingSubListBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;

        }
    }
}
