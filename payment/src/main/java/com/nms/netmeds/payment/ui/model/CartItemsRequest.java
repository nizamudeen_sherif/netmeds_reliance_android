package com.nms.netmeds.payment.ui.model;

import com.google.gson.annotations.SerializedName;

public class CartItemsRequest {
    @SerializedName("token")
    private String token;
    @SerializedName("guestToken")
    private String guestToken;
    @SerializedName("guestFlag")
    private Boolean guestFlag;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGuestToken() {
        return guestToken;
    }

    public void setGuestToken(String guestToken) {
        this.guestToken = guestToken;
    }

    public Boolean getGuestFlag() {
        return guestFlag;
    }

    public void setGuestFlag(Boolean guestFlag) {
        this.guestFlag = guestFlag;
    }


}
