package com.nms.netmeds.payment.ui;

import com.google.gson.annotations.SerializedName;

public class JusPayCreateWalletResponse {
    @SerializedName("walletId")
    private String walletId;
    @SerializedName("wallet")
    private String wallet;
    @SerializedName("token")
    private String token;
    @SerializedName("object")
    private String object;
    @SerializedName("linked")
    private Boolean linked;
    @SerializedName("lastRefreshed")
    private String lastRefreshed;
    @SerializedName("currentBalance")
    private String currentBalance;

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Boolean getLinked() {
        return linked;
    }

    public void setLinked(Boolean linked) {
        this.linked = linked;
    }

    public String getLastRefreshed() {
        return lastRefreshed;
    }

    public void setLastRefreshed(String lastRefreshed) {
        this.lastRefreshed = lastRefreshed;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }


}
