package com.nms.netmeds.payment.ui;

public class PaymentIntentConstant {
    public static final String HOME_FLAG_FROM_ORDER_PLACED_SUCCESSFUL = "HOME_FLAG_FROM_ORDER_PLACED_SUCCESSFUL";
    public static final String HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL = "HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL";
    public static final String HOME_FLAG_FROM_DIAGNOSTIC_PLACED_SUCCESSFUL = "HOME_FLAG_FROM_DIAGNOSTIC_PLACED_SUCCESSFUL";
    public static final String FIRST_ORDER_DELIVERY_DATE = "FIRST_ORDER_DELIVERY_DATE";
    public static final String COMPLETE_ORDER_RESPONSE = "COMPLETE_ORDER_RESPONSE";
    public static final boolean OPEN_ORDER_SCREEN = true;
    /*DeLink Wallet intent ID*/
    public static final int DE_LINK_WALLET_JUST_PAY = 503;
    /*Order Success*/
    public static final String PLACED_SUCCESSFUL_FROM_FLAG = "PLACED_SUCCESSFUL_FROM_FLAG";
    public static final String PLACED_SUCCESSFUL_FROM_M1 = "PLACED_SUCCESSFUL_FROM_M1";
    public static final String PLACED_SUCCESSFUL_FROM_M2 = "PLACED_SUCCESSFUL_FROM_M2";
    public static final String PLACED_SUCCESSFUL_FROM_DIAGNOSTIC = "PLACED_SUCCESSFUL_FROM_DIAGNOSTIC";
    public static final String DELIVERY_ADDRESS = "DELIVERY_ADDRESS";
    public static final String IS_FROM_PAYMENT = "IS_FROM_PAYMENT";
    public static final String TIME_DURATION = "TIME_DURATION";
    public static final String AMOUNT = "totalAmount";
    /*Link Wallet intent ID*/
    public static final int LINK_WALLET_JUSPAY = 500;
    public static final int LINK_WALLET_PAYTM = 501;
    public static final int PAYTM_ADD_MONEY = 502;
    /*Banking List*/
    public static final String STATIC_BANK_LIST = "STATIC_BANK_LIST";
    /*Payment Failure Constant*/
    public static final String JUSPAY_ORDER_STATUS = "juspay_order_status";
    /*Link Wallet Constant*/
    public static final String PAYMENT_SUB_CATEGORY = "payment_sub_category";
    public static final String PAYMENT_COD_SUB_CATEGORY = "payment_cod_sub_category";
    public static final String CREATE_WALLET_RESPONSE = "create_wallet_response";
    public static final String CREATE_WALLET_REQUEST = "create_wallet_request";
    public static final String IS_JUSPAY = "is_juspay";
    public static final String TRANSACTION_STATUS = "transaction_status";
    public static final String PAY_TM_ADD_MONEY_PARAM = "paytm_add_money_param";
    public static final String PAY_TM_OTP_PARAM = "paytm_otp_param";
    public static final String FROM_PAYMENT_FAILURE = "FROM_PAYMENT_FAILURE";
    public static final String M2_ORDER_DESCRIPTION = "M2_ORDER_DESCRIPTION";
    public static final String EXISTING_ENTITY_ID = "EXISTING_ENTITY_ID";
    public static final String CART_ID = "CART_ID";
    public static final String FROM_DIAGNOSTICS = "FROM_DIAGNOSTICS";
    public static final String DIAGNOSTICS_BUNDLE = "DIAGNOSTICS_BUNDLE";
    public static final String ORDER_ID = "ORDER_ID";
    public static final String M2_FLAG = "M2_FLAG";

    public static String IS_PRIME_PRODUCT_AVAILABLE_IN_CART = "IS_PRIME_PRODUCT_AVAILABLE_IN_CART";
}