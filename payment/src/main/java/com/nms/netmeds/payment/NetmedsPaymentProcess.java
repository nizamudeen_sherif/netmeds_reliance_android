package com.nms.netmeds.payment;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.GenerateOrderIdRequest;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.PaymentGatewayList;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.model.ApplyCoupon;
import com.nms.netmeds.diagnostic.DiagnosticAPIPathConstant;
import com.nms.netmeds.diagnostic.model.DiagnosticCoupon;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.payment.ui.JusPayCreateWalletResponse;
import com.nms.netmeds.payment.ui.JusPayTransactionResponse;
import com.nms.netmeds.payment.ui.PaymentConstants;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;
import com.nms.netmeds.payment.ui.PaymentServiceManager;
import com.nms.netmeds.payment.ui.PaymentViewModel;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.nms.netmeds.payment.ui.vm.JusPayCreateOrderAndOrderStatusResponse;
import com.nms.netmeds.payment.ui.vm.PayPalTokenResponse;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;
import com.nms.netmeds.payment.ui.vm.PayTmChecksumResponse;
import com.nms.netmeds.payment.ui.vm.PayTmWithdrawResponse;
import com.nms.netmeds.payment.ui.vm.PaypalExecutePaymentRequest;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentLink;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequest;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestAddress;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestAmount;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestContext;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestDetails;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestItem;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestItemList;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestPayer;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestPayerInfo;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestPaymentOptions;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestRedirectUrls;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentRequestTransaction;
import com.nms.netmeds.payment.ui.vm.PaypalPaymentResponse;
import com.nms.netmeds.payment.ui.vm.PaypalRedirectResponse;
import com.nms.netmeds.payment.ui.vm.PaytmConsultBalanceResponse;
import com.nms.netmeds.payment.ui.vm.PaytmConsultBalanceResponseBody;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateTokenResponse;
import com.nms.netmeds.payment.ui.vm.PaytmWithdrawRequest;
import com.nms.netmeds.payment.ui.vm.TransactionLogRequest;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MultipartBody;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.nms.netmeds.payment.ui.PaymentServiceManager.C_MSTAR_PAYMENT_LIST;

public class NetmedsPaymentProcess extends AppViewModel {

    private Context context;
    private MStarCartDetails cartDetails;
    private BasePreference basePreference;
    private PaymentViewModel paymentViewModel;
    private MStarCustomerDetails customerDetails;
    private NetmedsPaymentProcessCallback callback;
    private Bundle addNewCardBundle = new Bundle();
    private PaymentGatewaySubCategory paymentGatewaySubCategory;
    private PaymentViewModel.PaymentInterface paymentViewModelCallBack;

    private Map<Integer, Integer> productCodeAndSelectedQtyMap;

    private String orderId = "";
    private String productIdList;
    private String validationMonth = "";
    private String validationYear = "";
    private String transactionLogPaymentRequest = "";
    private String transactionLogPaymentResponse = "";

    private boolean isM2Order = false;
    private boolean isNewCard = false;
    private boolean isFromPayment = false;
    private double grandTotal;
    private int jusPayOrderStatus = 0;

    //Consultation
    private ApplyCoupon consultationApplyCoupon;
    private String consultationUserId = "";
    private String consultationCouponType = "";
    private String consultationId = "";
    private String consultationOrderId = ""; //Consultation
    private boolean consultationSelectedPlan = false;
    private boolean isConsultationCouponListOpen = false;
    private boolean isFromConsultation = false;
    private double consultationTotalAmount = 0;
    private long consultationOrderIdCreationTime = 0;

    // Diagnostic
    private DiagnosticCoupon diagnosticCoupon;
    private String intentFromDiagnostic = "";
    private String diagnosticOrderId;
    private double diagnosticTotalAmount;
    private boolean isFromDiagnostic = false;

    //PayPal
    private String payPalId = "";
    private String paypalPayerId;
    private String payPalAccessToken = "";
    private String payPalPaymentInitResponse = "";
    //Paytm
    private PaytmValidateTokenResponse paytmValidateTokenResponse;
    private String payTmCheckSum = "";
    private String payTmCheckSumHash = "";
    //JUSPAY
    private JusPayCreateOrderAndOrderStatusResponse jusPayCreateOrderAndOrderStatusResponse;
    private Bundle jusPayLinkWalletRequest = new Bundle();
    private int jusPayCallBackRequestCode = 0;
    private String orderIdFromJusPayOrderCreationResponse = "";

    //Transaction
    private boolean isPaymentTransactionSuccess = false;
    private String completeOrderPaymentMethod = "";
    private String completeOrderPaymentType = "";
    private String appFirstOrder;
    private String completeOrderResponse;
    private boolean isPhonePeActive;


    public NetmedsPaymentProcess(@NonNull Application application) {
        super(application);
    }

    public void setCartDetails(MStarCartDetails cartDetails) {
        this.cartDetails = cartDetails;
    }

    private String getCartId() {
        return String.valueOf(isFromDiagnostic() ? String.valueOf(basePreference.getMstarDiagnosticCartId()) : cartDetails != null ? cartDetails.getId() : "");
    }

    public void initiateProperties(NetmedsPaymentProcessCallback callback, PaymentViewModel paymentViewModel, Context context, BasePreference basePreference,
                                   PaymentViewModel.PaymentInterface paymentViewModelCallBack, boolean isM2Order, boolean isFromPayment) {
        this.context = context;
        this.callback = callback;
        this.isM2Order = isM2Order;
        this.isFromPayment = isFromPayment;
        this.basePreference = basePreference;
        this.paymentViewModel = paymentViewModel;
        this.paymentViewModelCallBack = paymentViewModelCallBack;
        this.customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        callback.showNoNetworkView(isConnected);
        paymentViewModelCallBack.vmShowLoader();
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                APIServiceManager.getInstance().mStarProductDetailsForIdList(this, productIdList, transactionId);
                break;
            case APIServiceManager.MSTAR_GENERATE_ORDER_ID:
                APIServiceManager.getInstance().generateOrderId(this, basePreference.getMstarBasicHeaderMap(), getCurrentCartIdForAPICall(),
                        PaymentHelper.isExternalDoctor() ? AppConstant.YES : AppConstant.NO, getGenerateOrderIdRequest(), !PaymentHelper.isIsPayAndEditM2Order(), subscriptionThisIssue());
                break;

            /**
             * PAYPAL
             */
            case PaymentServiceManager.PAY_PAL_TOKEN:
                PaymentServiceManager.getInstance().getPayPalToken(this, getPayPalHeader(), PaymentUtils.getPayPalCredentials(PaymentConstants.PAY_PAL_AUTH_URL, basePreference));
                break;
            case PaymentServiceManager.INIT_PAY_PAL_PAYMENT:
                PaymentServiceManager.getInstance().initPayPalPayment(this, PaymentConstants.BEARER + getPayPalAccessToken(), getPayPalRequestParam(), PaymentUtils.getPayPalCredentials(PaymentConstants.PAY_PAL_PAYMENT_URL, basePreference));
                break;
            case PaymentServiceManager.PAY_PAL_PAYMENT_EXECUTE:
                PaymentServiceManager.getInstance().payPalPaymentExecute(this, PaymentConstants.BEARER + getPayPalAccessToken(), getExecuteUrl(), getPayPalPaymentExecuteRequest(getPaypalPayerId()));
                break;

            /**
             * Paytm
             */
            case PaymentServiceManager.PAY_TM_SEND_OTP:
                PaymentServiceManager.getInstance().sentPayTmOtp(this, payTmOtpRequest());
                break;
            case PaymentServiceManager.PAY_TM_VALIDATE_TOKEN:
                PaymentServiceManager.getInstance().validatePayTmToken(this, getPaymentGatewaySubCategory().getLinkToken());
                break;
            case PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM:
                PaymentServiceManager.getInstance().payTmBalanceChecksum(this, basePreference.getMstarBasicHeaderMap(), getPaymentGatewaySubCategory().getLinkToken(), String.valueOf(getGrandTotal()));
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
                PaymentServiceManager.getInstance().payTmWithdrawFromWallet(this, basePreference.getMstarBasicHeaderMap(),
                        getWithDrawFromWalletRequest(), !isFromPayment, !isFromPayment ? getOrderId() : "", !isFromPayment ? "" + getGrandTotal() : "");
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:
                PaymentServiceManager.getInstance().payTmWithdrawDetails(this, getPayTmWithdrawRequest());
                break;

            /**
             * JUSPAY
             */
            case PaymentServiceManager.CREATE_JUS_PAY_ORDER:
            case PaymentServiceManager.JUS_PAY_ORDER_STATUS:
                PaymentServiceManager.getInstance().callJusPayAPIForOrderDetails(this, basePreference.getMstarBasicHeaderMap(), getCreateOrderMultipartFormData(transactionId),
                        (!isFromPayment && (transactionId == PaymentServiceManager.CREATE_JUS_PAY_ORDER)) ? DiagnosticAPIPathConstant.DC_JUSPAY_ORDER_CREATE :
                                PaymentUtils.jusPayCredential(PaymentServiceManager.JUS_PAY_ORDER_STATUS == transactionId ? PaymentConstants.JUS_PAY_ORDER_STATUS :
                                        PaymentConstants.JUS_PAY_CREATE_ORDER, basePreference.getPaymentCredentials()), transactionId);
                break;

            /**
             * COD__AND__NOP
             */
            case APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK:
                APIServiceManager.getInstance().checkCODEligibleForCreateNewOrder(this, basePreference.getMstarBasicHeaderMap());
                break;
            case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
                PaymentHelper.setIsCartSwitched(false);
                PaymentHelper.setIsInitiateCheckoutCalled(false);
                APIServiceManager.getInstance().mStarOrderCompleteWithCOD(this, CommonUtils.getFingerPrintHeaderMap(context), getCurrentCartIdForAPICall());
                break;
            case PaymentServiceManager.MSTAR_ORDER_COMPLETE_WITH_NOP:
                PaymentHelper.setIsCartSwitched(false);
                PaymentHelper.setIsInitiateCheckoutCalled(false);
                PaymentServiceManager.getInstance().mStarOrderCompleteWithNOP(this, CommonUtils.getFingerPrintHeaderMap(context), getCurrentCartIdForAPICall());
                break;
            case PaymentServiceManager.C_MSTAR_COMPLETE_ORDER:
                PaymentServiceManager.getInstance().completeOrderDetails(this, CommonUtils.getFingerPrintHeaderMap(context),
                        getCartId(), callback.getPaymentMethod(), PaymentHelper.isExternalDoctor() ? AppConstant.YES : AppConstant.NO, PaymentHelper.isExternalDoctor() ? AppConstant.NO : AppConstant.YES,
                        getOrderType(), PaymentConstants.PAY_TM_PAYMENT_METHOD.equalsIgnoreCase(callback.getPaymentMethod()) ? getPayTmCheckSum() : "", PaymentConstants.PAY_PAL_PAYMENT_METHOD.equalsIgnoreCase(callback.getPaymentMethod()) ? getPayPalId() : "");
                break;
            case PaymentServiceManager.C_MSTAR_PAYMENT_LOG:
                PaymentServiceManager.getInstance().mStarPaymentLog(this, basePreference.getMstarBasicHeaderMap(), getPaymentLogRequest());
                break;

        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                checkProductStockResponse(data);
                break;
            case APIServiceManager.MSTAR_GENERATE_ORDER_ID:
                getOrderIdResponse(data);
                break;

            /**
             * PAYPAL
             */
            case PaymentServiceManager.PAY_PAL_TOKEN:
                setTransactionLogPaymentResponse(data);
                getPayPalResponse(data);
                break;
            case PaymentServiceManager.INIT_PAY_PAL_PAYMENT:
                setTransactionLogPaymentResponse(data);
                initPayPalPaymentResponse(data);
                break;
            case PaymentServiceManager.PAY_PAL_PAYMENT_EXECUTE:
                setTransactionLogPaymentResponse(data);
                payPalPaymentExecuteResponse(data);
                break;


            /**
             * PAYTM
             */
            case PaymentServiceManager.PAY_TM_SEND_OTP:
                payTmSendOtpResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_VALIDATE_TOKEN:
                payTmValidateTokenResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM:
                payTmBalanceChecksumResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
                patTmWithdrawFromWalletResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:
                payTmWithdrawResponse(data);
                break;

            /**
             * JUSPAY
             */
            case PaymentServiceManager.CREATE_JUS_PAY_ORDER:
                createJusPayOrderResponse(data);
                break;
            case PaymentServiceManager.JUS_PAY_ORDER_STATUS:
                jusPayOrderStatusResponse(data);
                break;

            /**
             * COD_AND_NOP
             */
            case APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK:
                codEligibleCheckResponse(data);
                break;
            case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
            case PaymentServiceManager.MSTAR_ORDER_COMPLETE_WITH_NOP:
                orderCompleteWithCODRAndNOPesponse(data);
                break;
            case PaymentServiceManager.C_MSTAR_COMPLETE_ORDER:
                setCompleteOrderResponse(data);
                completeOrderResponse(data);
                break;

            case PaymentServiceManager.C_MSTAR_PAYMENT_LOG:
                paymentLogResponse();
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK:
            case APIServiceManager.MSTAR_GENERATE_ORDER_ID:
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                /**
                 * PAYPAL
                 */
            case PaymentServiceManager.PAY_PAL_TOKEN:
            case PaymentServiceManager.INIT_PAY_PAL_PAYMENT:
            case PaymentServiceManager.PAY_PAL_PAYMENT_EXECUTE:
                /**
                 * PAYTM
                 */
            case PaymentServiceManager.PAY_TM_SEND_OTP:
            case PaymentServiceManager.PAY_TM_VALIDATE_TOKEN:
            case PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM:
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:

                /**
                 * JUSPAY
                 */
            case PaymentServiceManager.CREATE_JUS_PAY_ORDER:
            case PaymentServiceManager.JUS_PAY_ORDER_STATUS:

            case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
            case PaymentServiceManager.MSTAR_ORDER_COMPLETE_WITH_NOP:
                if (!TextUtils.isEmpty(data) && !AppConstant.NULL.equalsIgnoreCase(data)) {
                    setTransactionLogPaymentResponse(data);
                }
                initPaymentFailure();
                break;
            case PaymentServiceManager.C_MSTAR_COMPLETE_ORDER:
                completeOrderFailureResponse();
                break;
            case PaymentServiceManager.C_MSTAR_PAYMENT_LOG:
                paymentLogResponse();
                break;
        }
    }


    public Integer getCurrentCartIdForAPICall() {
        if (isM2Order) return basePreference.getMstarMethod2CartId();
        if (isFromSubscription()) {
            return SubscriptionHelper.getInstance().getSubscriptionCartId();
        } else if (isFromDiagnostic()) {
            return basePreference.getMstarDiagnosticCartId();
        } else return PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : null;
    }

    private boolean isFromSubscription() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag() || SubscriptionHelper.getInstance().isPayNowSubscription() || SubscriptionHelper.getInstance().isSubscriptionEdit();
    }

    private GenerateOrderIdRequest getGenerateOrderIdRequest() {
        GenerateOrderIdRequest request = new GenerateOrderIdRequest();
        request.setSplitInfo(!TextUtils.isEmpty(PaymentHelper.getDeliveryEstimateResponse()) ? new Gson().fromJson(PaymentHelper.getDeliveryEstimateResponse(), DeliveryEstimateResponse.class) : new DeliveryEstimateResponse());
        return request;
    }

    private String subscriptionThisIssue() {
        return SubscriptionHelper.getInstance().isSubscriptionEdit() || SubscriptionHelper.getInstance().isPayNowSubscription() && !TextUtils.isEmpty(SubscriptionHelper.getInstance().getSubscriptionThisIssue()) ? SubscriptionHelper.getInstance().getSubscriptionThisIssue() : null;
    }

    /**
     * PAYPAL_REQUEST__________************************************************************************_START_______________
     ****/
    private String getPayPalHeader() {
        String credential = String.format("%s:%s", PaymentUtils.getPayPalCredentials(PaymentConstants.PAY_PAL_CLIENT_ID, basePreference), PaymentUtils.getPayPalCredentials(PaymentConstants.PAY_PAL_SECRET, basePreference));
        return "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
    }

    private PaypalPaymentRequest getPayPalRequestParam() {
        PaypalPaymentRequest paypalPaymentRequest = new PaypalPaymentRequest();
        paypalPaymentRequest.setIntent(PaymentConstants.SALE);

        PaypalPaymentRequestContext paypalPaymentRequestContext = new PaypalPaymentRequestContext();
        paypalPaymentRequestContext.setShippingPreference(PaymentConstants.SET_PROVIDED_ADDRESS);
        paypalPaymentRequestContext.setUserAction(PaymentConstants.COMMIT);
        paypalPaymentRequestContext.setLocale(PaymentConstants.LOCALE);
        paypalPaymentRequestContext.setLandingPage(PaymentConstants.BILLING);
        paypalPaymentRequest.setContext(paypalPaymentRequestContext);

        PaypalPaymentRequestPayer paypalPaymentRequestPayer = new PaypalPaymentRequestPayer();
        paypalPaymentRequestPayer.setPaymentMethod(PaymentConstants.PAY_PAL_PAYMENT_METHOD);
        PaypalPaymentRequestPayerInfo paypalPaymentRequestPayerInfo = new PaypalPaymentRequestPayerInfo();
        paypalPaymentRequestPayerInfo.setEmail(customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "");
        paypalPaymentRequestPayerInfo.setFirstName(customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "");
        paypalPaymentRequestPayerInfo.setLastName(customerDetails != null && !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "");
        paypalPaymentRequestPayerInfo.setBillingAddress(getPayPalRequestAddress(false));
        paypalPaymentRequestPayer.setPayerInfo(paypalPaymentRequestPayerInfo);
        paypalPaymentRequest.setPayer(paypalPaymentRequestPayer);

        List<PaypalPaymentRequestTransaction> payPalPaymentRequestTransactionLst = new ArrayList<>();
        PaypalPaymentRequestTransaction paypalPaymentRequestTransaction = new PaypalPaymentRequestTransaction();

        PaypalPaymentRequestAmount paypalPaymentRequestAmount = new PaypalPaymentRequestAmount();
        paypalPaymentRequestAmount.setTotal("" + getGrandTotal());
        paypalPaymentRequestAmount.setCurrency(PaymentConstants.INR);
        PaypalPaymentRequestDetails paypalPaymentRequestDetails = new PaypalPaymentRequestDetails();
        paypalPaymentRequestDetails.setSubtotal(String.format(Locale.getDefault(), "%.2f", (getGrandTotal() - getShippingAmount())));
        paypalPaymentRequestDetails.setShipping(String.valueOf(getShippingAmount()));
        paypalPaymentRequestAmount.setDetails(paypalPaymentRequestDetails);
        paypalPaymentRequestTransaction.setAmount(paypalPaymentRequestAmount);
        paypalPaymentRequestTransaction.setCustom(customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "");
        paypalPaymentRequestTransaction.setInvoiceNumber(getOrderId());

        PaypalPaymentRequestPaymentOptions paypalPaymentRequestPaymentOptions = new PaypalPaymentRequestPaymentOptions();
        paypalPaymentRequestPaymentOptions.setAllowedPaymentMethod(PaymentConstants.INSTANT_FUNDING_SOURCE);
        paypalPaymentRequestTransaction.setPaymentOptions(paypalPaymentRequestPaymentOptions);

        PaypalPaymentRequestItemList paypalPaymentRequestItemList = new PaypalPaymentRequestItemList();
        paypalPaymentRequestItemList.setItems(getPayPalRequestItemList());
        paypalPaymentRequestItemList.setShippingAddress(getPayPalRequestAddress(true));
        paypalPaymentRequestItemList.setShippingPhoneNumber("91" + (customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : ""));
        paypalPaymentRequestTransaction.setItemList(paypalPaymentRequestItemList);
        payPalPaymentRequestTransactionLst.add(paypalPaymentRequestTransaction);

        PaypalPaymentRequestRedirectUrls paypalPaymentRequestRedirectUrls = new PaypalPaymentRequestRedirectUrls();
        paypalPaymentRequestRedirectUrls.setReturnUrl(PaymentUtils.getPayPalCredentials(PaymentConstants.PAY_PAL_RETURN_URL, basePreference));
        paypalPaymentRequestRedirectUrls.setCancelUrl(paypalPaymentRequestRedirectUrls.getReturnUrl());
        paypalPaymentRequest.setTransactions(payPalPaymentRequestTransactionLst);
        paypalPaymentRequest.setRedirectUrls(paypalPaymentRequestRedirectUrls);
        setTransactionLogPaymentRequest(new Gson().toJson(paypalPaymentRequest));
        return paypalPaymentRequest;
    }

    private PaypalPaymentRequestAddress getPayPalRequestAddress(boolean isShipping) {
        PaypalPaymentRequestAddress paypalPaymentRequestAddress = new PaypalPaymentRequestAddress();
        if (PaymentHelper.getCustomerAddress() != null) {
            MStarAddressModel address = isFromPayment ? PaymentHelper.getCustomerAddress() : getPayPayStaticAddress();
            if (isShipping) {
                paypalPaymentRequestAddress.setRecipientName(PaymentHelper.getCustomerAddress() != null && !TextUtils.isEmpty(PaymentHelper.getCustomerAddress().getName()) ? PaymentHelper.getCustomerAddress().getName() : "");
            }
            paypalPaymentRequestAddress.setLine1(address.getStreet());
            if (!TextUtils.isEmpty(address.getLandmark())) {
                paypalPaymentRequestAddress.setLine2(address.getLandmark());
            }
            paypalPaymentRequestAddress.setCity(!TextUtils.isEmpty(address.getCity()) ? address.getCity() : "");
            paypalPaymentRequestAddress.setCountryCode(AppConstant.IN);
            paypalPaymentRequestAddress.setPostalCode(!TextUtils.isEmpty(address.getPin()) ? address.getPin() : "");
            paypalPaymentRequestAddress.setPhone("91" + (!TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : ""));
            paypalPaymentRequestAddress.setState(!TextUtils.isEmpty(address.getState()) ? address.getState() : "");
        }
        return paypalPaymentRequestAddress;
    }

    private MStarAddressModel getPayPayStaticAddress() {
        MStarAddressModel address = new MStarAddressModel();
        address.setCity(PaymentConstants.ADDRESS_CITY);
        address.setState(PaymentConstants.TAMIL_NADU);
        address.setStreet(PaymentConstants.ADDRESS_LINE_ONE);
        address.setLandmark(PaymentConstants.ADDRESS_LINE_TWO);
        address.setPin(PaymentConstants.ADDRESS_PINCODE);
        return address;
    }

    private List<PaypalPaymentRequestItem> getPayPalRequestItemList() {
        List<PaypalPaymentRequestItem> payPalPaymentRequestItemLst = new ArrayList<>();
        if (isFromPayment && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
            List<MStarProductDetails> lineItemsList = cartDetails.getLines();
            for (MStarProductDetails productDetails : lineItemsList) {
                PaypalPaymentRequestItem paypalPaymentRequestItem = new PaypalPaymentRequestItem();
                if (productDetails != null) {
                    paypalPaymentRequestItem.setCurrency(PaymentConstants.INR);
                    paypalPaymentRequestItem.setDescription(productDetails.getDisplayName());
                    paypalPaymentRequestItem.setName(productDetails.getDisplayName());
                    paypalPaymentRequestItem.setPrice(String.valueOf(productDetails.getSellingPrice()));
                    paypalPaymentRequestItem.setQuantity(String.valueOf(productDetails.getCartQuantity()));
                    paypalPaymentRequestItem.setSku(String.valueOf(productDetails.getProductCode()));
                    payPalPaymentRequestItemLst.add(paypalPaymentRequestItem);
                }
            }
            double couponDiscount = getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_COUPON);
            if (Math.abs(couponDiscount) > 0) {
                payPalPaymentRequestItemLst.add(getPayPayLineItems(PaymentConstants.COUPON, String.valueOf(couponDiscount)));
            }
            double walletDiscount = getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_WALLET);
            if (Math.abs(walletDiscount) > 0) {
                payPalPaymentRequestItemLst.add(getPayPayLineItems(PaymentConstants.WALLET, String.valueOf(walletDiscount)));
            }
            double voucherDiscount = getIndividualDiscount(PaymentConstants.DISCOUNT_FROM_VOUCHER);
            if (Math.abs(voucherDiscount) > 0) {
                payPalPaymentRequestItemLst.add(getPayPayLineItems(PaymentConstants.VOUCHER, String.valueOf(voucherDiscount)));
            }
        } else {
            PaypalPaymentRequestItem paypalPaymentRequestItem = new PaypalPaymentRequestItem();
            paypalPaymentRequestItem.setCurrency(PaymentConstants.INR);
            paypalPaymentRequestItem.setDescription("");
            paypalPaymentRequestItem.setName(!TextUtils.isEmpty(getIntentFromDiagnostic()) && getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC) ?
                    getDiagnosticOrderId() : getConsultationOrderId());
            paypalPaymentRequestItem.setPrice(String.valueOf(getGrandTotal()));
            paypalPaymentRequestItem.setQuantity("1");
            paypalPaymentRequestItem.setSku(!TextUtils.isEmpty(getIntentFromDiagnostic()) && getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC) ?
                    getDiagnosticOrderId() : getConsultationOrderId());
            payPalPaymentRequestItemLst.add(paypalPaymentRequestItem);
        }
        return payPalPaymentRequestItemLst;
    }

    private PaypalPaymentRequestItem getPayPayLineItems(String key, String value) {
        PaypalPaymentRequestItem paypalPaymentRequestItem = new PaypalPaymentRequestItem();
        paypalPaymentRequestItem.setName(key);
        paypalPaymentRequestItem.setQuantity("1");
        paypalPaymentRequestItem.setPrice("-" + Math.abs(Double.valueOf(value)));
        paypalPaymentRequestItem.setCurrency(PaymentConstants.INR);
        return paypalPaymentRequestItem;
    }

    private String getExecuteUrl() {
        return getPayPalRedirectionUrl(getPayPalPaymentInitResponse(), PaymentConstants.EXECUTE);
    }

    private PaypalExecutePaymentRequest getPayPalPaymentExecuteRequest(String payerID) {
        PaypalExecutePaymentRequest payPalExecuteRequest = new PaypalExecutePaymentRequest();
        payPalExecuteRequest.setPayerId(payerID);
        if (!TextUtils.isEmpty(payerID)) {
            setTransactionLogPaymentRequest(new Gson().toJson(payPalExecuteRequest));
        }
        return payPalExecuteRequest;
    }

    /**
     * PAYPAL_REQUEST__________************************************************************************_END_______________
     ****/

    /**
     * PAYTM_REQUEST__________************************************************************************_START_______________
     */
    private PaytmLinkWalletOtpRequest payTmOtpRequest() {
        ConfigMap getConfig = ConfigMap.getInstance();
        PaytmLinkWalletOtpRequest paytmLinkWalletOtpRequest = new PaytmLinkWalletOtpRequest();
        paytmLinkWalletOtpRequest.setEmail(customerDetails.getEmail());
        paytmLinkWalletOtpRequest.setPhone(customerDetails.getMobileNo());
        paytmLinkWalletOtpRequest.setClientId(getPayTmCredentials(PaymentConstants.PAYTM_LINK_CLIENT_ID));
        paytmLinkWalletOtpRequest.setResponseType(getConfig.getProperty(ConfigConstant.PAYTM_LINK_OTP_RESPONSETYPE));
        paytmLinkWalletOtpRequest.setScope(getConfig.getProperty(ConfigConstant.PAYTM_LINK_OTP_SCOPE));
        return paytmLinkWalletOtpRequest;
    }

    private Map<String, String> getWithDrawFromWalletRequest() {
        Map<String, String> param = new HashMap<>();
        param.put(PaymentConstants.API_APPIP, ConfigMap.getInstance().getProperty(ConfigConstant.PAYTM_LINK_APPIP));
        param.put(AppConstant.CARTID, String.valueOf(!isFromPayment ? "" : cartDetails.getId()));
        param.put(PaymentConstants.API_CUSTOMERID, getPayTmValidateTokenResponse() != null && !TextUtils.isEmpty(getPayTmValidateTokenResponse().getId()) ? getPayTmValidateTokenResponse().getId() : "");
        param.put(PaymentConstants.API_SSOTOKEN, getPayTmValidateTokenResponse() != null && !TextUtils.isEmpty(getPaymentGatewaySubCategory().getLinkToken()) ? getPaymentGatewaySubCategory().getLinkToken() : "");
        return param;
    }

    private PaytmWithdrawRequest getPayTmWithdrawRequest() {
        PaytmWithdrawRequest request = new PaytmWithdrawRequest();
        ConfigMap getConfig = ConfigMap.getInstance();
        request.setAppIP(getConfig.getProperty(ConfigConstant.PAYTM_LINK_APPIP));
        request.setMID(getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID));
        request.setAuthMode(getConfig.getProperty(ConfigConstant.PAYTM_LINK_AUTHMODE));
        request.setTxnAmount(String.valueOf(!isFromPayment ? getGrandTotal() : BigDecimal.valueOf(getGrandTotal()).stripTrailingZeros().toPlainString()));
        request.setOrderId(getOrderId());
        request.setDeviceId(customerDetails.getMobileNo());
        request.setSSOToken(getPayTmValidateTokenResponse() != null && !TextUtils.isEmpty(getPaymentGatewaySubCategory().getLinkToken()) ? getPaymentGatewaySubCategory().getLinkToken() : "");
        request.setPaymentMode(getConfig.getProperty(ConfigConstant.PAYTM_LINK_PAYMENTMODE));
        request.setCustId(getPayTmValidateTokenResponse() != null && !TextUtils.isEmpty(getPayTmValidateTokenResponse().getId()) ? getPayTmValidateTokenResponse().getId() : "");
        request.setIndustryType(getPayTmCredentials(PaymentConstants.PAYTM_LINK_INDUSTRY_KEY));
        request.setChannel(getPayTmCredentials(PaymentConstants.PAYTM_LINK_CHANNEL));
        request.setReqType(getConfig.getProperty(ConfigConstant.PAYTM_LINK_WITHDRAW_REQUESTTYPE));
        request.setCurrency(getConfig.getProperty(ConfigConstant.PAYTM_LINK_CURRENCY));
        request.setCheckSum(!TextUtils.isEmpty(getPayTmCheckSumHash()) ? getPayTmCheckSumHash() : "");
        setTransactionLogPaymentRequest(new Gson().toJson(request));
        return request;
    }

    /**
     * PAYTM_REQUEST__________************************************************************************_END_______________
     ****/


    private MultipartBody getCreateOrderMultipartFormData(int transactionId) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if (PaymentServiceManager.CREATE_JUS_PAY_ORDER == transactionId && !isFromPayment) {
            MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
            builder.addFormDataPart(AppConstant.ORDER_ID, getOrderId());
            if (isFromConsultation()) {
                builder.addFormDataPart(AppConstant.NET_PAYABLE, "" + getConsultationTotalAmount());
            } else {
                builder.addFormDataPart(AppConstant.NET_PAYABLE, "" + getGrandTotal());
            }
            builder.addFormDataPart(AppConstant.CUSTOMER_ID, "" + customerDetails.getId());
            builder.addFormDataPart(AppConstant.MOBILE, customerDetails.getMobileNo());
        } else if (PaymentServiceManager.CREATE_JUS_PAY_ORDER == transactionId) {
            builder.addFormDataPart(AppConstant.CARTID, getCartId());
        } else if (PaymentServiceManager.JUS_PAY_ORDER_STATUS == transactionId) {
            builder.addFormDataPart(AppConstant.ORDER_ID, orderIdFromJusPayOrderCreationResponse);
        }
        return builder.build();
    }

    private String getOrderType() {
        if (cartDetails != null) {
            for (MStarProductDetails productDetails : cartDetails.getLines()) {
                if (productDetails.isRxRequired()) {
                    return AppConstant.DRUG_TYPE_P;
                }
            }
        }
        return AppConstant.OTC_ORDER;
    }

    /**
     * PAYMENT LOG REQUEST
     */
    private TransactionLogRequest getPaymentLogRequest() {
        TransactionLogRequest transactionalLogRequest = new TransactionLogRequest();
        transactionalLogRequest.setOrderId(!TextUtils.isEmpty(getOrderId()) ? getOrderId() : "");
        String paymentMethod = !TextUtils.isEmpty(callback.getPaymentMethod()) ? callback.getPaymentMethod() : "";
        if (PaymentConstants.FREE.equalsIgnoreCase(paymentMethod)) {
            if (PaymentHelper.isNMSCashApplied() && PaymentHelper.isVoucherApplied()) {
                transactionalLogRequest.setPaymentMethod(PaymentConstants.E_WALLET_AND_E_VOUCHER);
            } else if (PaymentHelper.isNMSCashApplied()) {
                transactionalLogRequest.setPaymentMethod(PaymentConstants.E_WALLET);
            } else if (PaymentHelper.isVoucherApplied()) {
                transactionalLogRequest.setPaymentMethod(PaymentConstants.E_VOUCHER);
            } else {
                transactionalLogRequest.setPaymentMethod(PaymentConstants.EWALLET);
            }
        } else {
            transactionalLogRequest.setPaymentMethod(paymentMethod);
        }
        String eWallet = PaymentHelper.isNMSCashApplied() ? (", " + PaymentConstants.EWALLET + PaymentHelper.getAppliedNMSCash()) : "";
        String eVoucher = PaymentHelper.isVoucherApplied() && !TextUtils.isEmpty(PaymentHelper.getVoucherCode()) ? (", " + PaymentConstants.VOUCHER + PaymentHelper.getVoucherCode()) : "";
        if (!TextUtils.isEmpty(callback.getPaymentMethod()) && (callback.getPaymentMethod().equalsIgnoreCase(PaymentConstants.FREE) || callback.getPaymentMethod().equalsIgnoreCase(PaymentConstants.CASH_ON_DELIVERY))) {
            String data;
            if (callback.getPaymentMethod().equalsIgnoreCase(PaymentConstants.FREE)) {
                data = PaymentConstants.ORDERID + getOrderId() + ", " + AppConstant.CARTID + ":" + getCartId() + eWallet + eVoucher;
            } else {
                data = PaymentConstants.ORDERID + getOrderId() + ", " + AppConstant.CARTID + ":" + getCartId() + ", " + PaymentConstants.CASH_ON_DELIVERY + ":" + getGrandTotal();
            }
            setTransactionLogPaymentRequest(data);
            setTransactionLogPaymentResponse(data);
        } else {

            setTransactionLogPaymentRequest(getTransactionLogPaymentRequest() + eWallet + eVoucher);
        }
        transactionalLogRequest.setRequest(getTransactionLogPaymentRequest());
        transactionalLogRequest.setResponse(!TextUtils.isEmpty(getTransactionLogPaymentResponse()) ? getTransactionLogPaymentResponse() : PaymentConstants.CANCELLED_BY_USER);
        transactionalLogRequest.setAppVersion(CommonUtils.getAppVersion(context));
        transactionalLogRequest.setSourceName(AppConstant.SOURCE_NAME);
        return transactionalLogRequest;
    }


    private void checkProductStockResponse(String data) {
        boolean isOutOfStockProductAvailableCart = false;
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel basicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (basicResponseTemplateModel.getResult() != null && !basicResponseTemplateModel.getResult().getProductDetailsList().isEmpty()) {
                for (MStarProductDetails productDetails : basicResponseTemplateModel.getResult().getProductDetailsList()) {
                    callback.setWebEngageProductDetails(basicResponseTemplateModel.getResult().getProductDetailsList());
                    if (!isOutOfStockProductAvailableCart) {
                        if (CommonUtils.isProductAvailableWithStock(productDetails)) {
                            int productMaxQty = productDetails.getStockQty() < productDetails.getMaxQtyInOrder() ? productDetails.getStockQty() : productDetails.getMaxQtyInOrder();
                            int productCartQty = productCodeAndSelectedQtyMap.get(productDetails.getProductCode());
                            isOutOfStockProductAvailableCart = !(productCartQty <= productMaxQty);
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        if (isOutOfStockProductAvailableCart) {
            paymentViewModelCallBack.vmDismissProgress();
            paymentViewModelCallBack.backToOrderReviewPage();
        } else {
            /**
             * STEP 2:
             */
            initiateAPICall(APIServiceManager.MSTAR_GENERATE_ORDER_ID);
        }
    }

    private void getOrderIdResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel orderIdData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (orderIdData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(orderIdData.getStatus()) && orderIdData.getResult() != null && !(TextUtils.isEmpty(orderIdData.getResult().getOrderId()))) {
                setOrderId(orderIdData.getResult().getOrderId());
                paymentGateWayReDirection();
            } else {
                paymentViewModelCallBack.vmDismissProgress();
                paymentViewModelCallBack.disableOrEnableOtherViewClicks(true);
            }
        } else {
            paymentViewModelCallBack.vmDismissProgress();
            paymentViewModelCallBack.disableOrEnableOtherViewClicks(true);
        }
    }

    /**
     * PAYPAL_RESPONSE__________************************************************************************_START_______________
     */
    private void getPayPalResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            PayPalTokenResponse tokenResponse = new Gson().fromJson(response, PayPalTokenResponse.class);
            String accessToken = tokenResponse != null && !TextUtils.isEmpty(tokenResponse.getAccessToken()) ? tokenResponse.getAccessToken() : "";
            setPayPalAccessToken(accessToken);
            initiateAPICall(PaymentServiceManager.INIT_PAY_PAL_PAYMENT);
        } else initPaymentFailure();
    }

    private void initPayPalPaymentResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            setPayPalPaymentInitResponse(response);
            paymentViewModelCallBack.redirectToPayPalWebView(getOrderId(), getPayPalRedirectionUrl(response, PaymentConstants.APPROVAL_URL));
        } else initPaymentFailure();
    }

    private String getPayPalRedirectionUrl(String response, String urlId) {
        PaypalPaymentResponse paypalPaymentResponse = new Gson().fromJson(response, PaypalPaymentResponse.class);
        List<PaypalPaymentLink> getLinks = paypalPaymentResponse.getLinks();
        String url = "";
        for (PaypalPaymentLink paypalPaymentLink : getLinks) {
            if (paypalPaymentLink.getRel().equalsIgnoreCase(urlId)) {
                url = paypalPaymentLink.getHref();
                break;
            }
        }
        return url;
    }

    private void payPalPaymentExecuteResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            PaypalPaymentResponse payPalExecuteResponse = new Gson().fromJson(response, PaypalPaymentResponse.class);
            if (payPalExecuteResponse != null && payPalExecuteResponse.getState() != null && payPalExecuteResponse.getState().equalsIgnoreCase(PaymentConstants.APPROVED)) {
                if (!TextUtils.isEmpty(payPalExecuteResponse.getId()))
                    setPayPalId(payPalExecuteResponse.getId());
                initPaymentSuccess(PaymentConstants.PAY_PAL_PAYMENT_METHOD, PaymentConstants.PAY_PAL_PAYMENT_METHOD);
            } else {
                initPaymentFailure();
            }
        }
    }
    /**
     * PAYPAL_RESPONSE__________************************************************************************_END_______________
     */


    /**
     * PAYTM_RESPONSE__________************************************************************************_START_______________
     */
    private void payTmSendOtpResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            PaytmLinkWalletOtpResponse paytmLinkWalletOtpResponse = new Gson().fromJson(response, PaytmLinkWalletOtpResponse.class);
            if (paytmLinkWalletOtpResponse.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(paytmLinkWalletOtpResponse.getStatus())) {
                paymentViewModelCallBack.initPayTmOtpView(response, getPaymentGatewaySubCategory(), getPayTmOtpViewParam(), getCartId());
            } else {
                paymentViewModelCallBack.vmDismissProgress();
                initPaymentFailure();
            }
        } else {
            paymentViewModelCallBack.vmDismissProgress();
            initPaymentFailure();
        }
    }

    private void payTmValidateTokenResponse(String data) {
        if (data != null) {
            PaytmValidateTokenResponse response = new Gson().fromJson(data, PaytmValidateTokenResponse.class);
            setPayTmValidateTokenResponse(response);
            if (response != null && response.getMobile() != null && !TextUtils.isEmpty(response.getMobile()))
                initiateAPICall(PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM);
            else {
                paymentViewModelCallBack.vmDismissProgress();
                initiateAPICall(PaymentServiceManager.PAY_TM_SEND_OTP);
            }
        } else {
            paymentViewModelCallBack.vmDismissProgress();
            initPaymentFailure();
        }
    }

    private void payTmBalanceChecksumResponse(String response) {
        String checkSum = "";
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        if (!TextUtils.isEmpty(response)) {
            final PayTmChecksumResponse paytmChecksumBalanceResponse = gson.fromJson(response, PayTmChecksumResponse.class);
            checkSum = paytmChecksumBalanceResponse.getChecksumhash();
        }
        String input = "{\"head\":{\"clientId\":" + "\"" + getPayTmCredentials(PaymentConstants.PAYTM_LINK_CLIENT_ID) + "\"" +
                ",\"version\":\"v1\",\"requestTimestamp\":" + "\"" + Calendar.getInstance().getTimeInMillis() +
                "\"" + ",\"channelId\":" + "\"" + getPayTmCredentials(PaymentConstants.PAYTM_LINK_CHANNEL) +
                "\"" + ",\"signature\":" + "\"" + checkSum + "\"" +
                "},\"body\":{\"userToken\":" + "\"" + getPaymentGatewaySubCategory().getLinkToken() + "\"" +
                ",\"totalAmount\":" + "\"" + getGrandTotal() + "\"" + ",\"mid\":" + "\"" + getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID) + "\"}}";
        new PaymentUtils.AsyncTaskRunner(input, this).execute();
    }

    public void payTmConsultBalance(String response) {
        if (!TextUtils.isEmpty(response)) {
            PaytmConsultBalanceResponse paytmConsultBalanceResponse = new Gson().fromJson(response, PaytmConsultBalanceResponse.class);
            if (paytmConsultBalanceResponse != null && paytmConsultBalanceResponse.getBody() != null && paytmConsultBalanceResponse.getBody().getFundsSufficient()) {
                initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET);
            } else if (paytmConsultBalanceResponse != null && paytmConsultBalanceResponse.getBody() != null) {
                PaytmConsultBalanceResponseBody paytmConsultBalanceResponseBody = paytmConsultBalanceResponse.getBody();
                PayTmAddMoneyToWalletParams intentParam = new PayTmAddMoneyToWalletParams();
                intentParam.setPaymentUserId(getPayTmValidateTokenResponse().getId());
                intentParam.setPaytmUserToken(getPaymentGatewaySubCategory().getLinkToken());
                intentParam.setTransactionAmt(paytmConsultBalanceResponseBody.getDeficitAmount() != null ? paytmConsultBalanceResponseBody.getDeficitAmount() : Double.valueOf(getGrandTotal()));
                intentParam.setImageUrl(getPaymentGatewaySubCategory().getImage());
                intentParam.setOrderId(getOrderId());
                intentParam.setOrderAmt(getGrandTotal());
                paymentViewModelCallBack.payTmAddMoney(intentParam, getCartId());
            }
        } else {
            paymentViewModelCallBack.vmDismissProgress();
            initPaymentFailure();
        }
    }

    private void patTmWithdrawFromWalletResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            final PayTmChecksumResponse paytmChecksumBalanceResponse = gson.fromJson(data, PayTmChecksumResponse.class);
            setPayTmCheckSumHash(paytmChecksumBalanceResponse.getChecksumhash());
            initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS);
        } else {
            paymentViewModelCallBack.vmDismissProgress();
            initPaymentFailure();
        }
    }

    private void payTmWithdrawResponse(String data) {
        paymentViewModelCallBack.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            setTransactionLogPaymentResponse(data);
            PayTmWithdrawResponse paytmWithdrawResponse = new Gson().fromJson(data, PayTmWithdrawResponse.class);
            if (paytmWithdrawResponse != null && !TextUtils.isEmpty(paytmWithdrawResponse.getTxnId()) && !TextUtils.isEmpty(paytmWithdrawResponse.getResponseCode())) {
                if (paytmWithdrawResponse.getTxnId() != null && paytmWithdrawResponse.getResponseCode().equals("01")) {
                    if (!TextUtils.isEmpty(paytmWithdrawResponse.getCheckSum()))
                        setPayTmCheckSum(paytmWithdrawResponse.getCheckSum());
                    initPaymentSuccess(PaymentConstants.PAY_TM_PAYMENT_METHOD, PaymentConstants.PAY_TM_PAYMENT_METHOD);
                } else initPaymentFailure();
            } else {
                initPaymentFailure();
            }
        }
    }
    /**
     * PAYTM_RESPONSE__________************************************************************************_END_______________
     */

    /**
     * JUSPAY_RESPONSE
     */
    private void createJusPayOrderResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            JusPayCreateOrderAndOrderStatusResponse response = new Gson().fromJson(data, JusPayCreateOrderAndOrderStatusResponse.class);
            setJusPayCreateOrderAndOrderStatusResponse(response);
            if (response != null) {
                identifyAndInitializeJusPay(response);
            } else {
                paymentViewModelCallBack.vmDismissProgress();
            }
        } else {
            paymentViewModelCallBack.vmDismissProgress();
        }
    }

    private void jusPayOrderStatusResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            setTransactionLogPaymentResponse(response);
            JusPayCreateOrderAndOrderStatusResponse orderStatusResponse = new Gson().fromJson(response, JusPayCreateOrderAndOrderStatusResponse.class);
            if (orderStatusResponse != null && orderStatusResponse.getResult() != null) {
                setJusPayOrderStatus(orderStatusResponse.getResult().getStatusId());
                if (PaymentConstants.PAYMENT_STATUS_SUCESS == orderStatusResponse.getResult().getStatusId()) {
                    initPaymentSuccess(!TextUtils.isEmpty(orderStatusResponse.getResult().getPaymentMethod()) ? orderStatusResponse.getResult().getPaymentMethod() : "",
                            !TextUtils.isEmpty(orderStatusResponse.getResult().getPayment_method_type()) ? orderStatusResponse.getResult().getPayment_method_type() : "");
                } else
                    initPaymentFailure();
            } else {
                paymentViewModelCallBack.vmDismissProgress();
            }
        } else {
            paymentViewModelCallBack.vmDismissProgress();
        }
    }


    private void codEligibleCheckResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel codEligibleStatus = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (codEligibleStatus != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(codEligibleStatus.getStatus()) && codEligibleStatus.getResult() != null && codEligibleStatus.getResult().isCodEnable()) {
                initiateOrderCreationProcess();
            } else {
                notEligibleForCOD();
            }
        } else {
            notEligibleForCOD();
        }
    }

    private void notEligibleForCOD() {
        paymentViewModelCallBack.vmDismissProgress();
        paymentViewModelCallBack.setError(getApplication().getResources().getString(R.string.text_cod_error));
    }

    private void orderCompleteWithCODRAndNOPesponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            setCompleteOrderResponse(data);
            setTransactionLogPaymentResponse(data);
            MStarBasicResponseTemplateModel codDetailsResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (codDetailsResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(codDetailsResponse.getStatus())) {
                resetCartPreference();
                setPaymentTransactionSuccess(true);
                setAppFirstOrder(codDetailsResponse.getResult() != null && !TextUtils.isEmpty(codDetailsResponse.getResult().getIsAppFirstOrder()) ? codDetailsResponse.getResult().getIsAppFirstOrder() : "");
            }
        }
        initPaymentLog();
        callback.navigateToSuccessFailurePage();
    }

    private void resetCartPreference() {
        if (isM2Order) {
            basePreference.setMStarM2CartIsOpen(true);
            basePreference.setMstarMethod2CartId(0);
        } else {
            basePreference.setMStarM1CartIsOpen(true);
            basePreference.setMStarCartId(0);
        }
    }

    public void completeOrderFailureResponse() {
        MStarBasicResponseTemplateModel model = new MStarBasicResponseTemplateModel();
        MstarBasicResponseResultTemplateModel result = new MstarBasicResponseResultTemplateModel();
        result.setOrderId(getOrderId());
        result.setOrderStatus(AppConstant.CONFIRMATION_PENDING_DESC);
        result.setDescription("");
        completeOrderResponse(new Gson().toJson(model));
    }

    private void completeOrderResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel completeOrderResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (completeOrderResponse != null && !TextUtils.isEmpty(completeOrderResponse.getStatus()) && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(completeOrderResponse.getStatus())) {
                resetCartPreference();
                setAppFirstOrder(completeOrderResponse.getResult().getOrderStatusAfterPayment() != null ? completeOrderResponse.getResult().getOrderStatusAfterPayment().isFirstOrder() ? AppConstant.YES : AppConstant.NO : "");
            }
        }
        initPaymentLog();
        callback.navigateToSuccessFailurePage();
    }

    /***
     * ****************************************************************************************************************************************************************
     */

    private void paymentLogResponse() {
        if (isPaymentTransactionSuccess() && !TextUtils.isEmpty(getIntentFromDiagnostic()) && getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC)) {
            callback.diagnosticPaymentUpdate();
        } else if (isFromConsultation()) {
            initiateAPICall(C_MSTAR_PAYMENT_LIST);
            paymentViewModelCallBack.clearPaymentsFragment();
            if (!isPaymentTransactionSuccess()) {
                paymentViewModelCallBack.setError(getApplication().getResources().getString(R.string.text_failure_consultation));
                // Web Engage - Consultation Payment Status - Failure
                callback.triggerEventForConsultationFailure();
            }
            callback.consultationPaymentCreation(getPaymentGatewaySubCategory() != null ? callback.getPaymentMethod() : "Card", isPaymentTransactionSuccess() ? ConsultationConstant.SUCCESS : ConsultationConstant.FAILURE);
        }
        paymentViewModelCallBack.disableOrEnableOtherViewClicks(true);
        paymentViewModelCallBack.vmDismissProgress();
    }

    public void initiatePaymentProcess() {
        //Google Analytics Click Event
        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_PLACE_ORDER, GoogleAnalyticsHelper.EVENT_LABEL_PAYMENT_DETAILS_PAGE);
        basePreference.setCartCount(0);
        setJusPayOrderStatus(0);
        if (!isFromPayment)
            setConsultationOrderId(consultationOrderId());
        if (isNewCard()) {
            setAddNewCardBundle(paymentViewModelCallBack.getNewCardDetails());
            initiateOrderCreationProcess();
        } else if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getParentId() == PaymentConstants.CREDIT_DEBIT_LIST) {
            initiateOrderCreationProcess();
        } else if (getPaymentGatewaySubCategory() == null && isFromDiagnostic() && getDiagnosticCoupon() != null && getDiagnosticTotalAmount() == 0) {
            checkCODEligibilityAndProceed();
        } else if (getPaymentGatewaySubCategory() == null && isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied()) {
            checkCODEligibilityAndProceed();
        } else if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getId().equalsIgnoreCase(PaymentConstants.CASH_ON_DELIVERY)) {
            checkCODEligibilityAndProceed();
        } else initiateOrderCreationProcess();
    }

    private void checkCODEligibilityAndProceed() {
        if (isFromDiagnostic()) {
            paymentViewModelCallBack.vmShowLoader();
            initiateOrderCreationProcess();
            return;
        }
        if (isEligibleForCod()) {
            initiateAPICall(APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK);
        } else notEligibleForCOD();
    }

    private boolean isEligibleForCod() {
        PaymentHelper.setIsNMSCashApplied(cartDetails != null && cartDetails.getUsedWalletAmount() != null && cartDetails.getUsedWalletAmount().getTotalWallet().doubleValue() > 0);
        PaymentHelper.setIsNMSSuperCashApplied(cartDetails != null && cartDetails.getUsedWalletAmount() != null && cartDetails.getUsedWalletAmount().getTotalWallet().doubleValue() > 0);
        PaymentHelper.setIsWalletApplied(cartDetails != null && cartDetails.getUsedWalletAmount() != null && cartDetails.getUsedWalletAmount().getTotalWallet().doubleValue() > 0);
        PaymentHelper.setIsVoucherApplied(cartDetails != null && cartDetails.getUsedVoucherAmount() != null && cartDetails.getUsedVoucherAmount().doubleValue() > 0);

        return getGrandTotal() > 0 && getGrandTotal() >= Double.valueOf(getPaymentGatewaySubCategory().getMinimumAmount())
                && getGrandTotal() <= Double.valueOf(getPaymentGatewaySubCategory().getMaximumAmount()) && !PaymentHelper.isNMSCashApplied()
                && !PaymentHelper.isNMSSuperCashApplied() && !PaymentHelper.isVoucherApplied() && !PaymentHelper.isIsWalletApplied() && PaymentHelper.getPinCodeStatus();
    }

    private void initiateOrderCreationProcess() {
        if (isFromPayment) {
            /**
             *steps to initiate order:
             * step 1: check all the product are in stock."call Product details in list API to check status".
             * step 2: after step 1, need to get Order Id for this porocessing order. "call get order details API to generate order Id for the current cart".
             * step 3: after getting order Id will navigate to Payment page based on our payment selection in payment list. call "paymentGateWayReDirection()" method.
             */
            /**
             *STEP 1:
             */
            productCodeAndSelectedQtyMap = new HashMap<>();
            if (cartDetails != null && cartDetails.getLines() != null) {
                for (MStarProductDetails productDetails : cartDetails.getLines()) {
                    productCodeAndSelectedQtyMap.put(productDetails.getProductCode(), productDetails.getCartQuantity());
                    int position = cartDetails.getLines().indexOf(productDetails);
                    String productCode = String.valueOf(productDetails.getProductCode());
                    if (position == 0) {
                        this.productIdList = productCode;
                    } else {
                        this.productIdList = productIdList.concat(", " + productCode);
                    }
                }
                initiateAPICall(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
            } else {
                initiateAPICall(APIServiceManager.MSTAR_GENERATE_ORDER_ID);
            }
        } else if (!TextUtils.isEmpty(getIntentFromDiagnostic()) && getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC))
            callback.diagnosticOrderCreation();
        else paymentGateWayReDirection();
    }

    /**
     * STEP 3:
     */
    public void paymentGateWayReDirection() {
        if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getParentId() == PaymentConstants.OTHER_PAYMENT_LIST
                && getPaymentGatewaySubCategory().getSubId() == PaymentConstants.PAYPAL) {
            initPayPal();
        } else if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getParentId() == PaymentConstants.WALLET_LIST
                && getPaymentGatewaySubCategory().getSubId() == PaymentConstants.PAYTM) {
            initPayTm();
        } else if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getParentId() == PaymentConstants.COD) {
            initPaymentSuccess(PaymentConstants.CASH_ON_DELIVERY, PaymentConstants.CASH_ON_DELIVERY);
        } else if (getGrandTotal() == 0 || (getPaymentGatewaySubCategory() == null && isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied())) {
            initPaymentSuccess(PaymentConstants.FREE, PaymentConstants.FREE);
        } else if (isFromDiagnostic() && getDiagnosticCoupon() != null && getDiagnosticTotalAmount() == 0) {
            initPaymentSuccess(PaymentConstants.FREE, PaymentConstants.FREE);
        } else {
            initiateAPICall(PaymentServiceManager.CREATE_JUS_PAY_ORDER);
        }
    }

    private void initPayPal() {
        paymentViewModelCallBack.vmShowLoader();
        PaymentHelper.setIsCartSwitched(false);
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        initiateAPICall(PaymentServiceManager.PAY_PAL_TOKEN);
    }

    /**
     * PAYTM--------start*********************************---------------------------------------------
     * for PAYTM PAYMENT need to follow the steps:
     * step 1:  need to check the paytm wallet is linked to that mobile no
     * if "Yes", the need to validate user token and
     */
    private void initPayTm() {
        paymentViewModelCallBack.vmShowLoader();
        PaymentHelper.setIsCartSwitched(false);
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getSubId() == PaymentConstants.PAYTM) {
            if (!getPaymentGatewaySubCategory().getLinked())
                initiateAPICall(PaymentServiceManager.PAY_TM_SEND_OTP);
            else
                initiateAPICall(PaymentServiceManager.PAY_TM_VALIDATE_TOKEN);
        }
    }

    private PayTmAddMoneyToWalletParams getPayTmOtpViewParam() {
        PayTmAddMoneyToWalletParams params = new PayTmAddMoneyToWalletParams();
        params.setOrderAmt(getGrandTotal());
        params.setOrderId(getOrderId());
        return params;
    }

    private String getPayTmCredentials(int id) {
        return PaymentUtils.payTmCredentials(id, basePreference.getPaymentCredentials());
    }
    /**
     * PAYTM--------END__________*********************************---------------------------------------------
     */

    /***************************************************NAVIGATE TO PAYMENT SELECTED IN PAYMENT LIST EXCEPT____NEW CARD_____START_____________*************************/
    private void identifyAndInitializeJusPay(JusPayCreateOrderAndOrderStatusResponse response) {
        if (isNewCard())
            initNewCardPayment();
        else {
            PaymentGatewaySubCategory subCategory = getPaymentGatewaySubCategory();
            if (subCategory != null) {
                switch (subCategory.getParentId()) {
                    case PaymentConstants.OTHER_PAYMENT_LIST:
                        otherPayment(subCategory, response);
                        break;
                    case PaymentConstants.UPI_PAYMENT_LIST:
                        upiPayment(subCategory, response);
                        break;
                    case PaymentConstants.WALLET_LIST:
                        walletPayment(subCategory, response);
                        break;
                    case PaymentConstants.NET_BANKING_LIST:
                        netBankingPayment(subCategory, response);
                        break;
                    case PaymentConstants.CREDIT_DEBIT_LIST:
                        savedCardPayment(subCategory);
                        break;
                }
            }
        }
    }

    /***************************************************____NEW CARD_____START_____________***********************************************/
    private void initNewCardPayment() {
        if (getAddNewCardBundle() != null) {
            setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_CARD_TRANSACTION);
            JSONObject newCardPayLoad = new JSONObject();
            try {
                newCardPayLoad.put(PaymentConstants.OP_NAME, PaymentConstants.CARD_TXN);
                newCardPayLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.CARD);
                newCardPayLoad.put(PaymentConstants.CARD_NUMBER, !TextUtils.isEmpty(getAddNewCardBundle().getString(PaymentConstants.CARD_NUMBER)) ? getAddNewCardBundle().getString(PaymentConstants.CARD_NUMBER) : "");
                newCardPayLoad.put(PaymentConstants.CARD_EXP_MONTH, getValidationMonth());
                newCardPayLoad.put(PaymentConstants.CARD_EXP_YEAR, getValidationYear());
                newCardPayLoad.put(PaymentConstants.NAME_ON_CARD, !TextUtils.isEmpty(getAddNewCardBundle().getString(PaymentConstants.NAME_ON_CARD)) ? getAddNewCardBundle().getString(PaymentConstants.NAME_ON_CARD) : "");
                newCardPayLoad.put(PaymentConstants.CARD_SECURITY_CODE, !TextUtils.isEmpty(getAddNewCardBundle().getString(PaymentConstants.CARD_CVV)) ? getAddNewCardBundle().getString(PaymentConstants.CARD_CVV) : "");
                newCardPayLoad.put(PaymentConstants.SAVE_TO_LOCKER, getAddNewCardBundle() != null && getAddNewCardBundle().getBoolean(PaymentConstants.SAVE_TO_LOCKER) ? getApplication().getString(R.string.text_true) : "");
                newCardPayLoad.put(PaymentConstants.REDIRECT_AFTER_PAYMENT, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JusPayCreateOrderAndOrderStatusResponse jusPayCreateOrderAndOrderStatusResponse = getJusPayCreateOrderAndOrderStatusResponse();
            jusPayCreateOrderAndOrderStatusResponse.getResult().setPayLoad(newCardPayLoad.toString());
            paymentViewModelCallBack.initiateJusPayTransaction(jusPayCreateOrderAndOrderStatusResponse);
        }
    }
    /***************************************************____NEW CARD_____END_____________**************************************************/

    /********OTHER PAYMENT PAYLOAD____________---- start----------****OLA, PHONEPE and PAYTM_UPI***______*************/
    private void otherPayment(PaymentGatewaySubCategory subCategory, JusPayCreateOrderAndOrderStatusResponse response) {
        switch (subCategory.getSubId()) {
            case PaymentConstants.PHONEPE:
            case PaymentConstants.OLA:
                response.getResult().setPayLoad(createOtherPaymentPayLoad(subCategory.getKey(), false, PaymentConstants.PHONEPE == subCategory.getSubId()));
                setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT);
                break;
            case PaymentConstants.PAYTM_UPI:
                response.getResult().setPayLoad(upiPaymentPayLoad());
                setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT);
                break;
        }
        paymentViewModelCallBack.initiateJusPayTransaction(response);
    }

    private String createOtherPaymentPayLoad(String title, boolean isAmazon, boolean isPhonePe) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.WALLET_TXN);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.WALLET);
            payLoad.put(PaymentConstants.PAYMENT_METHOD, title.equalsIgnoreCase(PaymentConstants.PHONE_PE_ID) ? title.toUpperCase() : title);
            payLoad.put(PaymentConstants.REDIRECT_AFTER_PAYMENT, true);
            payLoad.put(PaymentConstants.DIRECT_WALLET_TOKEN, "");
            payLoad.put(PaymentConstants.SDK_PRESENT, isAmazon ? PaymentConstants.ANDROID_AMAZONPAY_TOKENIZED : isPhonePe ? PaymentConstants.ANDROID_PHONEPE_SDK : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    private String upiPaymentPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.UPI_OP_NAME);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.UPI_METHOD_NAME);
            payLoad.put(PaymentConstants.UPI_SDK_PRESENT, true);
            payLoad.put(PaymentConstants.PAY_WITH_APP, PaymentConstants.PAY_TM_UPI_PACKAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
    /********OTHER PAYMENT PAYLOAD____________---- end----------****OLA, PHONEPE and PAYTM_UPI***______*************/

    /*******UPI PAYMENT PAYLOAD ______ GOOGLEPAY____________---- start----------______*************/
    private void upiPayment(PaymentGatewaySubCategory subCategory, JusPayCreateOrderAndOrderStatusResponse response) {
        switch (subCategory.getSubId()) {
            case PaymentConstants.GOOGLE_PAY_UPI:
                response.getResult().setPayLoad(upiGooglePayPayload(subCategory));
                setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT);
                break;
        }
        paymentViewModelCallBack.initiateJusPayTransaction(response);
    }

    private String upiGooglePayPayload(PaymentGatewaySubCategory subCategory) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.WALLET_TXN);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.WALLET);
            payLoad.put(PaymentConstants.PAYMENT_METHOD, subCategory.getId());
            payLoad.put(PaymentConstants.GPAY_MOBILE_NO, customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "");
            payLoad.put(PaymentConstants.REDIRECT_AFTER_PAYMENT, true);
            payLoad.put(PaymentConstants.DIRECT_WALLET_TOKEN, "");
            payLoad.put(PaymentConstants.SDK_PRESENT, PaymentConstants.ANDROID_GOOGLEPAY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
    /********UPI PAYMENT PAYLOAD ______ GOOGLEPAY____________---- end----------______*************/

    /*******WALLET PAYLOAD____________---- start----------______*************/
    private void walletPayment(PaymentGatewaySubCategory subCategory, JusPayCreateOrderAndOrderStatusResponse response) {
        switch (subCategory.getSubId()) {
            case PaymentConstants.MOBIKWIK:
            case PaymentConstants.FREE_CHARGE:
            case PaymentConstants.AMAZON:
                walletTransaction(subCategory, response);
                break;
            case PaymentConstants.OLA:
                otherPayment(subCategory, response);
                break;
        }
    }

    private void walletTransaction(PaymentGatewaySubCategory subCategory, JusPayCreateOrderAndOrderStatusResponse response) {
        if (!subCategory.getLinked()) {
            createWalletTransaction(subCategory, response);
            setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_LINK_WALLET);
            paymentViewModelCallBack.initiateJusPayTransaction(response);
        } else {
            setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_REFRESH_WALLET);
            refreshWallet(subCategory, response);
        }
    }

    private void createWalletTransaction(PaymentGatewaySubCategory subCategory, JusPayCreateOrderAndOrderStatusResponse response) {
        response.getResult().setPayLoad(jusPayWalletCreationPayLoad(subCategory));
    }

    private String jusPayWalletCreationPayLoad(PaymentGatewaySubCategory subCategory) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.CREATE_WALLET);
            payLoad.put(PaymentConstants.WALLET_NAME, subCategory.getKey());
            payLoad.put(PaymentConstants.OTP_PAGE_ENABLED, false);
            if (PaymentConstants.AMAZON == subCategory.getSubId()) {
                payLoad.put(PaymentConstants.SDK_WALLET_IDENTIFIER, PaymentUtils.amazonPayCredential(basePreference.getPaymentCredentials()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    private void refreshWallet(PaymentGatewaySubCategory subCategory, JusPayCreateOrderAndOrderStatusResponse response) {
        response.getResult().setPayLoad(jusPayRefreshWalletPayLoad(subCategory));
        paymentViewModelCallBack.initiateJusPayTransaction(response);
    }

    private String jusPayRefreshWalletPayLoad(PaymentGatewaySubCategory subCategory) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.REFRESH_WALLET);
            payLoad.put(PaymentConstants.WALLET_ID, subCategory != null ? subCategory.getId() : "");
            payLoad.put(PaymentConstants.WALLET_NAME, subCategory != null ? subCategory.getKey() : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
    /*******WALLET PAYLOAD____________---- end----------______*************/

    /*******NET BANKING PAYLOAD____________---- start----------______*************/
    private void netBankingPayment(@NotNull PaymentGatewaySubCategory subCategory, @NotNull JusPayCreateOrderAndOrderStatusResponse response) {
        response.getResult().setPayLoad(netBankingPaymentPayLoad(subCategory.getId()));
        setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_NETBANKING);
        paymentViewModelCallBack.initiateJusPayTransaction(response);
    }

    @NotNull
    private String netBankingPaymentPayLoad(String bankId) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.NB_TXN);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.NB);
            payLoad.put(PaymentConstants.PAYMENT_METHOD, bankId);
            payLoad.put(PaymentConstants.REDIRECT_AFTER_PAYMENT, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }
    /*******NET BANKING PAYLOAD____________---- end----------______*************/

    /********SAVED CARDS PAYMENT PAYLOAD____________---- start----------______*************/
    private void savedCardPayment(PaymentGatewaySubCategory subCategory) {
        setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_CARD_TRANSACTION);
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.CARD_TXN);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.CARD);
            payLoad.put(PaymentConstants.CARD_TOKEN, subCategory.getToken());
            payLoad.put(PaymentConstants.CARD_SECURITY_CODE, subCategory.getCardCvv());
            payLoad.put(PaymentConstants.REDIRECT_AFTER_PAYMENT, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initSaveCardJusPayPayment(payLoad.toString());
    }

    private void initSaveCardJusPayPayment(String payLoad) {
        JusPayCreateOrderAndOrderStatusResponse response = getJusPayCreateOrderAndOrderStatusResponse();
        response.getResult().setPayLoad(payLoad);
        paymentViewModelCallBack.initiateJusPayTransaction(response);
    }
    /********SAVED CARDS PAYMENT PAYLOAD____________---- end----------______*************/


    /**
     *
     * After callback from jusPay payment process from JUSPAY SDK and PAYTM and PAYPAL
     */
    /**
     * PAYPAL EXECUTION AFTER COMPLETE PROCESS
     */
    public void executePayPalPayment(Intent data) {
        if (data != null && data.hasExtra(PaymentConstants.RESULT) && !TextUtils.isEmpty(data.getStringExtra(PaymentConstants.RESULT))) {
            PaypalRedirectResponse paypalRedirectResponse = new Gson().fromJson(data.getStringExtra(PaymentConstants.RESULT), PaypalRedirectResponse.class);
            setPaypalPayerId(paypalRedirectResponse.getPayerID());
            initiateAPICall(PaymentServiceManager.PAY_PAL_PAYMENT_EXECUTE);
        } else {
            setTransactionLogPaymentResponse(PaymentConstants.CANCELLED_BY_USER);
            initPaymentFailure();
        }
    }

    /**
     * PAYTM EXECUTION AFTER COMPLETE PROCESS
     */
    public void paytmTransactionResultCallback(Intent data, int resultCode) {
        setTransactionLogRequestAndResponse(data);
        if (resultCode == RESULT_OK && data != null && data.hasExtra(PaymentIntentConstant.TRANSACTION_STATUS) && data.getBooleanExtra(PaymentIntentConstant.TRANSACTION_STATUS, false)) {
            if (data.hasExtra(PaymentConstants.PAYTM_CHECK_SUM) && !TextUtils.isEmpty(data.getStringExtra(PaymentConstants.PAYTM_CHECK_SUM)))
                setPayTmCheckSum(data.getStringExtra(PaymentConstants.PAYTM_CHECK_SUM));
            initPaymentSuccess(PaymentConstants.PAY_TM_PAYMENT_METHOD, PaymentConstants.PAY_TM_PAYMENT_METHOD);
        } else {
            setTransactionLogPaymentResponse(PaymentConstants.CANCELLED_BY_USER);
            initPaymentFailure();
        }
    }

    /*******************************CALL BACK FROM JUSPAY SDK PAYMENT************************------start---------******************************************/
    public void jusPayDirectTransactionCallBack(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null && data.hasExtra(PaymentConstants.JUSPAY_PAY_LOAD)) {
            JusPayTransactionResponse transactionResponse = new Gson().fromJson(data.getStringExtra(PaymentConstants.JUSPAY_PAY_LOAD), JusPayTransactionResponse.class);
            initJusPayOrderStatus(transactionResponse.getOrderId());
        } else {
            initJusPayOrderStatus(getOrderId());
        }
    }

    public void jusPayLinkWallet201CallBack(Intent data) {
        if (data != null && data.hasExtra(PaymentConstants.JUSPAY_PAY_LOAD) && !TextUtils.isEmpty(data.getStringExtra(PaymentConstants.JUSPAY_PAY_LOAD))) {
            JusPayCreateWalletResponse createWalletResponse = new Gson().fromJson(data.getStringExtra(PaymentConstants.JUSPAY_PAY_LOAD), JusPayCreateWalletResponse.class);
            if (createWalletResponse != null && getJusPayCreateOrderAndOrderStatusResponse() != null) {
                if (getPaymentGatewaySubCategory() != null && getPaymentGatewaySubCategory().getParentId() == PaymentConstants.WALLET_LIST && getPaymentGatewaySubCategory().getSubId() == PaymentConstants.AMAZON) {
                    initAmazonDirectDebit();
                } else
                    paymentViewModelCallBack.initOtpView(createWalletResponse, getPaymentGatewaySubCategory(), getJusPayLinkWalletRequest(), getCartId());
            }
        }
    }

    private void initAmazonDirectDebit() {
        getJusPayCreateOrderAndOrderStatusResponse().getResult().setPayLoad(createOtherPaymentPayLoad(getPaymentGatewaySubCategory().getKey(), true, false));
        setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT);
        paymentViewModelCallBack.initiateJusPayTransaction(getJusPayCreateOrderAndOrderStatusResponse());
    }

    public void jusPayLinkWallet500CallBack(Intent data) {
        setTransactionLogRequestAndResponse(data);
        paymentViewModelCallBack.vmShowLoader();
        initJusPayOrderStatus(getOrderId());
    }

    public void netBankingCallBack(Intent data) {
        List<PaymentGatewayList> gatewayList = new ArrayList<>(paymentViewModel.getSortedPaymentList());
        if (data != null) {
            if (data.hasExtra(PaymentConstants.BANK_NAME) && !TextUtils.isEmpty(data.getStringExtra(PaymentConstants.BANK_NAME)) && data.hasExtra(PaymentConstants.BANK_ID) && !TextUtils.isEmpty(data.getStringExtra(PaymentConstants.BANK_ID))) {
                for (PaymentGatewayList consultationPaymentGatewayList : gatewayList) {
                    if (consultationPaymentGatewayList != null && consultationPaymentGatewayList.getId() == PaymentConstants.NET_BANKING_LIST) {

                        PaymentGatewaySubCategory consultationPaymentGatewaySubCategory = new PaymentGatewaySubCategory();
                        consultationPaymentGatewaySubCategory.setKey(data.getStringExtra(PaymentConstants.BANK_NAME));
                        consultationPaymentGatewaySubCategory.setDisplayName(data.getStringExtra(PaymentConstants.BANK_NAME));
                        consultationPaymentGatewaySubCategory.setId(data.getStringExtra(PaymentConstants.BANK_ID));
                        consultationPaymentGatewaySubCategory.setShowChangeButton(true);
                        consultationPaymentGatewaySubCategory.setGatewayChecked(true);
                        consultationPaymentGatewaySubCategory.setParentId(PaymentConstants.NET_BANKING_LIST);
                        paymentViewModelCallBack.resetPaymentSelection();
                        setPaymentGatewaySubCategory(consultationPaymentGatewaySubCategory);

                        List<PaymentGatewaySubCategory> list = new ArrayList<>();
                        list.add(consultationPaymentGatewaySubCategory);
                        consultationPaymentGatewayList.setSubList(list);
                    } else {
                        if (consultationPaymentGatewayList != null && consultationPaymentGatewayList.getSubList() != null) {
                            for (PaymentGatewaySubCategory subCategory : consultationPaymentGatewayList.getSubList()) {
                                subCategory.setGatewayChecked(false);
                            }

                            consultationPaymentGatewayList.setNewCardView(false);
                        }
                    }
                }
                paymentViewModelCallBack.setPaymentListAdapter((gatewayList), (paymentViewModel.isPrimeProductOnly() || paymentViewModel.isMixedProduct()), paymentViewModel.isPayTmUpiActive(), paymentViewModel.isGooglePayActive());
            }
        }
        paymentViewModelCallBack.vmDismissProgress();
    }

    public void initJusPayRefreshWalletCallBack(int resultCode, Intent data) {
        JusPayCreateWalletResponse response = new Gson().fromJson(data.getStringExtra(PaymentConstants.JUSPAY_PAY_LOAD), JusPayCreateWalletResponse.class);
        if (resultCode == RESULT_OK) {
            setJusPayCallBackRequestCode(PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT);
            directWalletTransaction(response);
        } else if (resultCode == RESULT_CANCELED)
            initJusPayOrderStatus(getOrderId());
    }

    private void directWalletTransaction(JusPayCreateWalletResponse response) {
        getJusPayCreateOrderAndOrderStatusResponse().getResult().setPayLoad(jusPayWalletPayLoad(response));
        paymentViewModelCallBack.initiateJusPayTransaction(getJusPayCreateOrderAndOrderStatusResponse());
    }

    private String jusPayWalletPayLoad(JusPayCreateWalletResponse response) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.WALLET_TXN);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.WALLET);
            payLoad.put(PaymentConstants.PAYMENT_METHOD, response.getWallet());
            payLoad.put(PaymentConstants.DIRECT_WALLET_TOKEN, response.getToken());
            payLoad.put(PaymentConstants.OTP_PAGE_ENABLED, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    /*******************************CALL BACK FROM JUSPAY SDK PAYMENT************************------end-----------******************************************/

    private void initJusPayOrderStatus(String orderId) {
        this.orderIdFromJusPayOrderCreationResponse = orderId;
        initiateAPICall(PaymentServiceManager.JUS_PAY_ORDER_STATUS);
    }

    private void setTransactionLogRequestAndResponse(Intent data) {
        if (data != null) {
            setTransactionLogPaymentRequest(!TextUtils.isEmpty(data.getStringExtra(PaymentConstants.PAYMENT_REQUEST)) ? data.getStringExtra(PaymentConstants.PAYMENT_REQUEST) : "");
            setTransactionLogPaymentResponse(!TextUtils.isEmpty(data.getStringExtra(PaymentConstants.PAYMENT_RESPONSE)) ? data.getStringExtra(PaymentConstants.PAYMENT_RESPONSE) : "");
        }
    }


    private void initPaymentSuccess(String paymentMethod, String paymentMethodType) {
        setPaymentTransactionSuccess(true);
        setCompleteOrderPaymentMethod(paymentMethod);
        setCompleteOrderPaymentType(paymentMethodType);
        if (isFromPayment) {
            paymentViewModelCallBack.vmShowLoader();
            initiateAPICall(PaymentConstants.CASH_ON_DELIVERY.equalsIgnoreCase(paymentMethod) ? APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD :
                    PaymentConstants.FREE.equalsIgnoreCase(paymentMethod) ? PaymentServiceManager.MSTAR_ORDER_COMPLETE_WITH_NOP : PaymentServiceManager.C_MSTAR_COMPLETE_ORDER);
        } else {
            initPaymentLog();
        }
    }


    private void initPaymentFailure() {
        setPaymentTransactionSuccess(false);
        if (isFromPayment) {
            initiateAPICall(PaymentServiceManager.C_MSTAR_COMPLETE_ORDER);
            //WebEngage Payment Failure Event
            WebEngageHelper.getInstance().paymentFailureEvent(context, getOrderId(), WebEngageHelper.TRANSACTION_FAILED_STATUS, getGrandTotal());
        } else if (getIntentFromDiagnostic() != null && getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC)) {
            paymentViewModel.diagnosticPaymentFailureUpdate();
        }
        initPaymentLog();
    }

    private void initPaymentLog() {
        initiateAPICall(PaymentServiceManager.C_MSTAR_PAYMENT_LOG);
    }


    //Utils
    public double getIndividualDiscount(String discountCode) {
        double discount = 0;
        if (cartDetails != null) {
            if (PaymentConstants.DISCOUNT_FROM_WALLET.equalsIgnoreCase(discountCode)) {
                discount = cartDetails.getUsedWalletAmount().getTotalWallet().doubleValue();
            } else if (PaymentConstants.DISCOUNT_FROM_COUPON.equalsIgnoreCase(discountCode)) {
                for (MStarProductDetails productDetails : cartDetails.getLines()) {
                    discount = discount + productDetails.getLineCouponDiscount().doubleValue();
                }
            } else {
                discount = cartDetails.getUsedVoucherAmount().doubleValue();
            }
        }
        return discount;
    }


    //GETTER and  SETTER
    public PaymentGatewaySubCategory getPaymentGatewaySubCategory() {
        return paymentGatewaySubCategory;
    }

    public void setPaymentGatewaySubCategory(PaymentGatewaySubCategory subCategory) {
        this.paymentGatewaySubCategory = subCategory;
    }

    public boolean isNewCard() {
        return isNewCard;
    }

    public void setNewCard(boolean newCard) {
        isNewCard = newCard;
    }

    private String getValidationMonth() {
        return validationMonth;
    }

    public void setValidationMonth(String validationMonth) {
        this.validationMonth = validationMonth;
    }

    public String getValidationYear() {
        return validationYear;
    }

    public void setValidationYear(String validationYear) {
        this.validationYear = validationYear;
    }

    private Bundle getAddNewCardBundle() {
        return addNewCardBundle;
    }

    private void setAddNewCardBundle(Bundle addNewCardBundle) {
        this.addNewCardBundle = addNewCardBundle;
    }

    public int getJusPayOrderStatus() {
        return this.jusPayOrderStatus;
    }

    private void setJusPayOrderStatus(int orderStatus) {
        this.jusPayOrderStatus = orderStatus;
    }

    private void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        if (isFromPayment)
            return orderId;
        else if (getIntentFromDiagnostic().equalsIgnoreCase(DiagnosticConstant.KEY_DIAGNOSTIC))
            return getDiagnosticOrderId();
        else
            return getConsultationOrderId();
    }

    public double getGrandTotal() {
        if (isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied())
            return new BigDecimal(String.valueOf(DiagnosticHelper.getremainingPaidAmount())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        BigDecimal totalAmount = new BigDecimal(String.valueOf(grandTotal)).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.grandTotal = totalAmount.doubleValue();
    }

    public double getShippingAmount() {
        return cartDetails != null ? cartDetails.getShippingChargesFinal().doubleValue() : 0;
    }


    //Consultation
    public boolean isConsultationSelectedPlan() {
        return consultationSelectedPlan;
    }

    public void setConsultationSelectedPlan(boolean consultationSelectedPlan) {
        this.consultationSelectedPlan = consultationSelectedPlan;
    }

    public boolean isConsultationCouponListOpen() {
        return isConsultationCouponListOpen;
    }

    public void setConsultationCouponListOpen(boolean consultationCouponListOpen) {
        isConsultationCouponListOpen = consultationCouponListOpen;
    }

    public ApplyCoupon getConsultationApplyCoupon() {
        return consultationApplyCoupon;
    }

    public void setConsultationApplyCoupon(ApplyCoupon consultationApplyCoupon) {
        this.consultationApplyCoupon = consultationApplyCoupon;
    }

    public String getConsultationUserId() {
        return consultationUserId;
    }

    public void setConsultationUserId(String consultationUserId) {
        this.consultationUserId = consultationUserId;
        setConsultationOrderId(consultationOrderId());
    }

    private String consultationOrderId() {
        setConsultationOrderIdCreationTime(System.currentTimeMillis());
        return ConsultationConstant.ORDER_ID_PREFIX + getConsultationUserId() + getConsultationOrderIdCreationTime();
    }

    public double getConsultationTotalAmount() {
        return consultationTotalAmount;
    }

    public void setConsultationTotalAmount(double consultationTotalAmount) {
        this.consultationTotalAmount = consultationTotalAmount;
    }

    public String getConsultationCouponType() {
        return consultationCouponType;
    }

    public void setConsultationCouponType(String consultationCouponType) {
        this.consultationCouponType = consultationCouponType;
    }

    public String getConsultationId() {
        return consultationId;
    }

    public void setConsultationId(String consultationId) {
        this.consultationId = consultationId;
    }

    public long getConsultationOrderIdCreationTime() {
        return consultationOrderIdCreationTime;
    }

    private void setConsultationOrderIdCreationTime(long consultationOrderIdCreationTime) {
        this.consultationOrderIdCreationTime = consultationOrderIdCreationTime;
    }

    private void setConsultationOrderId(String orderId) {
        this.consultationOrderId = orderId;
    }

    public String getConsultationOrderId() {
        return this.consultationOrderId;
    }

    public boolean isFromConsultation() {
        return isFromConsultation;
    }

    public void setFromConsultation(boolean isFromConsultation) {
        this.isFromConsultation = isFromConsultation;
    }


    //Diagnostic
    public String getDiagnosticOrderId() {
        return diagnosticOrderId;
    }

    public void setDiagnosticOrderId(String diagnosticOrderId) {
        this.diagnosticOrderId = diagnosticOrderId;
    }

    public DiagnosticCoupon getDiagnosticCoupon() {
        return diagnosticCoupon;
    }

    public void setDiagnosticCoupon(DiagnosticCoupon diagnosticCoupon) {
        this.diagnosticCoupon = diagnosticCoupon;
    }

    public boolean isFromDiagnostic() {
        return isFromDiagnostic;
    }

    public void setFromDiagnostic(boolean fromDiagnostic) {
        isFromDiagnostic = fromDiagnostic;
    }

    public double getDiagnosticTotalAmount() {
        return diagnosticTotalAmount;
    }

    public void setDiagnosticTotalAmount(double diagnosticTotalAmount) {
        this.diagnosticTotalAmount = diagnosticTotalAmount;
    }

    public String getIntentFromDiagnostic() {
        return intentFromDiagnostic;
    }

    public void setIntentFromDiagnostic(String intentFromDiagnostic) {
        this.intentFromDiagnostic = intentFromDiagnostic;
    }


    //PayPal
    private String getPayPalAccessToken() {
        return payPalAccessToken;
    }

    private void setPayPalAccessToken(String payPalAccessToken) {
        this.payPalAccessToken = payPalAccessToken;
    }

    private String getPayPalPaymentInitResponse() {
        return payPalPaymentInitResponse;
    }

    private void setPayPalPaymentInitResponse(String payPalPaymentInitResponse) {
        this.payPalPaymentInitResponse = payPalPaymentInitResponse;
    }

    public String getPaypalPayerId() {
        return paypalPayerId;
    }

    public void setPaypalPayerId(String paypalPayerId) {
        this.paypalPayerId = paypalPayerId;
    }

    private String getPayPalId() {
        return payPalId;
    }

    private void setPayPalId(String payPalId) {
        this.payPalId = payPalId;
    }


    //Paytm
    private PaytmValidateTokenResponse getPayTmValidateTokenResponse() {
        return paytmValidateTokenResponse;
    }

    private void setPayTmValidateTokenResponse(PaytmValidateTokenResponse paytmValidateTokenResponse) {
        this.paytmValidateTokenResponse = paytmValidateTokenResponse;
    }

    public String getPayTmCheckSum() {
        return payTmCheckSum;
    }

    public void setPayTmCheckSum(String payTmCheckSum) {
        this.payTmCheckSum = payTmCheckSum;
    }

    public String getPayTmCheckSumHash() {
        return payTmCheckSumHash;
    }

    public void setPayTmCheckSumHash(String payTmCheckSumHash) {
        this.payTmCheckSumHash = payTmCheckSumHash;
    }


    //JUSPAY
    private JusPayCreateOrderAndOrderStatusResponse getJusPayCreateOrderAndOrderStatusResponse() {
        return jusPayCreateOrderAndOrderStatusResponse;
    }

    private void setJusPayCreateOrderAndOrderStatusResponse(JusPayCreateOrderAndOrderStatusResponse jusPayCreateOrderAndOrderStatusResponse) {
        this.jusPayCreateOrderAndOrderStatusResponse = jusPayCreateOrderAndOrderStatusResponse;
    }

    public int getJusPayCallBackRequestCode() {
        return jusPayCallBackRequestCode;
    }

    public void setJusPayCallBackRequestCode(int jusPayCallBackRequestCode) {
        this.jusPayCallBackRequestCode = jusPayCallBackRequestCode;
    }

    private Bundle getJusPayLinkWalletRequest() {
        return jusPayLinkWalletRequest;
    }

    public void setJusPayLinkWalletRequest(Bundle args) {
        this.jusPayLinkWalletRequest = args;
    }


    public boolean isPaymentTransactionSuccess() {
        return isPaymentTransactionSuccess;
    }

    private void setPaymentTransactionSuccess(boolean paymentTransactionSuccess) {
        isPaymentTransactionSuccess = paymentTransactionSuccess;
    }

    private String getCompleteOrderPaymentMethod() {
        return completeOrderPaymentMethod;
    }

    private void setCompleteOrderPaymentMethod(String completeOrderPaymentMethod) {
        this.completeOrderPaymentMethod = completeOrderPaymentMethod;
    }

    private String getCompleteOrderPaymentType() {
        return completeOrderPaymentType;
    }

    private void setCompleteOrderPaymentType(String completeOrderPaymentType) {
        this.completeOrderPaymentType = completeOrderPaymentType;
    }

    public String getAppFirstOrder() {
        return appFirstOrder;
    }

    private void setAppFirstOrder(String appFirstOrder) {
        this.appFirstOrder = appFirstOrder;
    }

    public String getCompleteOrderResponse() {
        return completeOrderResponse;
    }

    public void setCompleteOrderResponse(String completeOrderResponse) {
        this.completeOrderResponse = completeOrderResponse;
    }

    //Log
    private String getTransactionLogPaymentRequest() {
        return transactionLogPaymentRequest;
    }

    public void setTransactionLogPaymentRequest(String transactionLogPaymentRequest) {
        this.transactionLogPaymentRequest = transactionLogPaymentRequest;
    }

    public String getTransactionLogPaymentResponse() {
        return transactionLogPaymentResponse;
    }

    private void setTransactionLogPaymentResponse(String transactionLogPaymentResponse) {
        this.transactionLogPaymentResponse = transactionLogPaymentResponse;
    }

    public void setPhonePeActive(boolean isPhonePeActive) {
        this.isPhonePeActive = isPhonePeActive;
    }

    public boolean isPhonePeActive() {
        return isPhonePeActive;
    }

    public interface NetmedsPaymentProcessCallback {

        void diagnosticOrderCreation();

        void showNoNetworkView(boolean isConnected);

        void navigateToSuccessFailurePage();

        String getPaymentMethod();

        void diagnosticPaymentUpdate();

        void consultationPaymentCreation(String s, String s1);

        void triggerEventForConsultationFailure();

        void setWebEngageProductDetails(List<MStarProductDetails> productDetailsList);
    }
}
