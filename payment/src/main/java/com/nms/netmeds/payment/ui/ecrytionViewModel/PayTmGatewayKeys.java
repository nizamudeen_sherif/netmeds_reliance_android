package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;

public class PayTmGatewayKeys {
    @SerializedName("key1")
    private String addMoney;
    @SerializedName("key2")
    private String balanceCheck;
    @SerializedName("key3")
    private String withDraw;
    @SerializedName("key4")
    private String transaction;
    @SerializedName("key5")
    private String mid;
    @SerializedName("key6")
    private String webSite;
    @SerializedName("key7")
    private String merchantKey;
    @SerializedName("key8")
    private String channel;
    @SerializedName("key9")
    private String industryKey;
    @SerializedName("key10")
    private String callBack;
    @SerializedName("key11")
    private String clientId;
    @SerializedName("key12")
    private String secretKey;

    public String getAddMoney() {
        return addMoney;
    }

    public void setAddMoney(String addMoney) {
        this.addMoney = addMoney;
    }

    public String getBalanceCheck() {
        return balanceCheck;
    }

    public void setBalanceCheck(String balanceCheck) {
        this.balanceCheck = balanceCheck;
    }

    public String getWithDraw() {
        return withDraw;
    }

    public void setWithDraw(String withDraw) {
        this.withDraw = withDraw;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transactio) {
        this.transaction = transactio;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getIndustryKey() {
        return industryKey;
    }

    public void setIndustryKey(String industryKey) {
        this.industryKey = industryKey;
    }

    public String getCallBack() {
        return callBack;
    }

    public void setCallBack(String callBack) {
        this.callBack = callBack;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

}
