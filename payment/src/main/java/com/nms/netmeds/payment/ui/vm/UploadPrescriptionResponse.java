package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UploadPrescriptionResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private List<UploadPrescriptionResponseData> responseData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<UploadPrescriptionResponseData> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<UploadPrescriptionResponseData> responseData) {
        this.responseData = responseData;
    }
}
