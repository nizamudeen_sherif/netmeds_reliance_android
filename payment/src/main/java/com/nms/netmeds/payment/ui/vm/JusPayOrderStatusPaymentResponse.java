package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayOrderStatusPaymentResponse {
    @SerializedName("created")
    private String created;
    @SerializedName("txn_id")
    private String txnId;
    @SerializedName("rrn")
    private String rrn;
    @SerializedName("epg_txn_id")
    private String epgTxnId;
    @SerializedName("auth_id_code")
    private String authIdCode;
    @SerializedName("resp_code")
    private String respCode;
    @SerializedName("resp_message")
    private String respMessage;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEpgTxnId() {
        return epgTxnId;
    }

    public void setEpgTxnId(String epgTxnId) {
        this.epgTxnId = epgTxnId;
    }

    public String getAuthIdCode() {
        return authIdCode;
    }

    public void setAuthIdCode(String authIdCode) {
        this.authIdCode = authIdCode;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
