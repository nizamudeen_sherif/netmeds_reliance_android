package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class PayTmChecksumResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("CHECKSUMHASH")
    private String checksumhash;
    @SerializedName("payt_STATUS")
    private String paytSTATUS;
    @SerializedName("ORDER_ID")
    private String orderId;

    public String getChecksumhash() {
        return checksumhash;
    }

    public void setChecksumhash(String checksumhash) {
        this.checksumhash = checksumhash;
    }

    public String getPaytSTATUS() {
        return paytSTATUS;
    }

    public void setPaytSTATUS(String paytSTATUS) {
        this.paytSTATUS = paytSTATUS;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
