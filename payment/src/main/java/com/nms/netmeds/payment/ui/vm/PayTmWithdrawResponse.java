package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PayTmWithdrawResponse {
    @SerializedName("TxnId")
    private String txnId;
    @SerializedName("MID")
    private String mID;
    @SerializedName("OrderId")
    private String orderId;
    @SerializedName("TxnAmount")
    private String txnAmount;
    @SerializedName("BankTxnId")
    private String bankTxnId;
    @SerializedName("ResponseCode")
    private String responseCode;
    @SerializedName("ResponseMessage")
    private String responseMessage;
    @SerializedName("Status")
    private String status;
    @SerializedName("PaymentMode")
    private String paymentMode;
    @SerializedName("BankName")
    private String bankName;
    @SerializedName("CustId")
    private String custId;
    @SerializedName("MBID")
    private String mBID;
    @SerializedName("CheckSum")
    private String checkSum;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getBankTxnId() {
        return bankTxnId;
    }

    public void setBankTxnId(String bankTxnId) {
        this.bankTxnId = bankTxnId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getMBID() {
        return mBID;
    }

    public void setMBID(String mBID) {
        this.mBID = mBID;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

}