package com.nms.netmeds.payment.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuItemCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.BuildConfig;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.AppliedVoucher;
import com.nms.netmeds.base.model.ConsultationEvent;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.PaymentGatewayList;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.ApplyCoupon;
import com.nms.netmeds.consultation.model.ApplyCouponResponse;
import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.view.CouponListBottomSheetDialog;
import com.nms.netmeds.diagnostic.model.DiagnosticCoupon;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.payment.NetmedsPaymentProcess;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.adapter.PaymentListParentAdapter;
import com.nms.netmeds.payment.databinding.ActivityPaymentBinding;
import com.nms.netmeds.payment.model.PaymentWrapperModel;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.nms.netmeds.payment.ui.vm.JusPayCreateOrderAndOrderStatusResponse;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import amazonpay.silentpay.APayCallback;
import amazonpay.silentpay.APayError;
import amazonpay.silentpay.AmazonPay;
import amazonpay.silentpay.GetBalanceRequest;
import amazonpay.silentpay.GetBalanceResponse;
import in.juspay.godel.ui.PaymentFragment;
import in.juspay.hypersdk.core.JuspayCallback;

import static com.nms.netmeds.base.utils.DiagnosticConstant.INTENT_KEY_COUPON_OBJECT;
import static in.juspay.godel.PaymentActivity.preFetch;


public class PaymentActivity extends BaseViewModelActivity<PaymentViewModel> implements PaymentViewModel.PaymentInterface,
        PaymentListParentAdapter.PaymentListParentAdapterCallback, NewPaymentCardDetailsDialog.NewPaymentCardDetailsDialogCallback,
        VoucherFragment.VoucherListener, PaymentFailureFragment.PaymentFailureFragmentListener {

    private Bundle newCardDetails;
    private PaymentViewModel viewModel;
    private MinKasuFragment minkasuFragment;
    private ActivityPaymentBinding mBinding;
    private PaymentFragment netmedsPaymentFragment;
    private PaymentWrapperModel paymentWrapperModel;
    private PaymentListParentAdapter paymentListParentAdapter;
    private TextView tvCartNetAmount;
    private TextView tvCartNetAmountLabel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        mBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(mBinding.toolbar);
        getSupportActionBar().setTitle(getString(R.string.text_payment_details));

        //To Enable WebViewDebugging in Debug Mode
        if (BuildConfig.DEBUG) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    @Override
    protected PaymentViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        init();
        viewModel.getPaymentGatewayResponseMutableLiveData().observe(this, new PaymentGatewayObserver());
        viewModel.getApplyCouponMutableLiveData().observe(this, new ApplyCouponObserver());
        viewModel.getNmsWalletBalanceForDiagnosticsMutableLiveData().observe(this, new NMmsWalletBalanceForDiagnosticsObserver());
        onRetry(viewModel);
        return viewModel;
    }

    private void init() {
        if (idFromDiagnostics() && getIntent() != null && getIntent().hasExtra(INTENT_KEY_COUPON_OBJECT)) {
            viewModel.netmedsPaymentProcess.setDiagnosticCoupon((DiagnosticCoupon) getIntent().getSerializableExtra(INTENT_KEY_COUPON_OBJECT));
        }
        if (idFromDiagnostics()) {
            viewModel.netmedsPaymentProcess.setFromDiagnostic(true);
        }

        if (!getIntentData() && !idFromDiagnostics())
            setConsultationPaymentValue();
        else setDiagnosticPaymentValue();

        viewModel.initViewModel(this, mBinding, BasePreference.getInstance(this), this, getIntentData(), isFromPaymentFailure(), isM2Order());

        if (viewModel.netmedsPaymentProcess.isFromDiagnostic() && viewModel.netmedsPaymentProcess.getDiagnosticCoupon() != null && viewModel.netmedsPaymentProcess.getDiagnosticTotalAmount() == 0) {
            mBinding.mainLayout.setVisibility(View.GONE);
            viewModel.placeOrder();
        }
    }

    private void setDiagnosticPaymentValue() {
        Bundle bundle = getIntent().getBundleExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL);
        viewModel.setDiagnosticOrderCreationDetail(bundle);
        viewModel.netmedsPaymentProcess.setIntentFromDiagnostic(bundle != null && !TextUtils.isEmpty(bundle.getString(DiagnosticConstant.KEY_DIAGNOSTIC)) ? bundle.getString(DiagnosticConstant.KEY_DIAGNOSTIC) : "");
        viewModel.setDiagnosticUserId(bundle != null && !TextUtils.isEmpty(bundle.getString(DiagnosticConstant.KEY_DIAGNOSTIC_USER_ID)) ? bundle.getString(DiagnosticConstant.KEY_DIAGNOSTIC_USER_ID) : "");
        viewModel.netmedsPaymentProcess.setDiagnosticTotalAmount(bundle != null ? bundle.getDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) : 0);
    }

    private boolean idFromDiagnostics() {
        return (getIntent() != null && getIntent().hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL));
    }

    private Bundle getDiagnosticsBundle() {
        return getIntent().getBundleExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL);
    }

    private void setConsultationPaymentValue() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(ConsultationConstant.KEY_USER_ID) && !TextUtils.isEmpty(getIntent().getStringExtra(ConsultationConstant.KEY_USER_ID))) {
                viewModel.netmedsPaymentProcess.setConsultationUserId(getIntent().getStringExtra(ConsultationConstant.KEY_USER_ID));
            }
            if (getIntent().hasExtra(ConsultationConstant.KEY_TOTAL_AMOUNT)) {
                viewModel.netmedsPaymentProcess.setConsultationTotalAmount(Double.parseDouble(new DecimalFormat("##.##").format(getIntent().getDoubleExtra(ConsultationConstant.KEY_TOTAL_AMOUNT, 0))));
            }
            if (getIntent().hasExtra(ConsultationConstant.KEY_APPLIED_COUPON) && !TextUtils.isEmpty(getIntent().getStringExtra(ConsultationConstant.KEY_APPLIED_COUPON))) {
                viewModel.netmedsPaymentProcess.setConsultationApplyCoupon(new Gson().fromJson(getIntent().getStringExtra(ConsultationConstant.KEY_APPLIED_COUPON), ApplyCoupon.class));
            }
            if (getIntent().hasExtra(ConsultationConstant.KEY_COUPON_TYPE)) {
                viewModel.netmedsPaymentProcess.setConsultationCouponType(getIntent().getStringExtra(ConsultationConstant.KEY_COUPON_TYPE));
            }
            if (getIntent().hasExtra(ConsultationConstant.KEY_SELECTED_PLAN)) {
                viewModel.netmedsPaymentProcess.setConsultationSelectedPlan(getIntent().getBooleanExtra(ConsultationConstant.KEY_SELECTED_PLAN, false));
            }
            if (getIntent().hasExtra(ConsultationConstant.KEY_CONSULTATION_ID) && !TextUtils.isEmpty(getIntent().getStringExtra(ConsultationConstant.KEY_CONSULTATION_ID))) {
                viewModel.netmedsPaymentProcess.setConsultationId(getIntent().getStringExtra(ConsultationConstant.KEY_CONSULTATION_ID));
            }
            if (getIntent().hasExtra(ConsultationConstant.KEY_SPECIALIZATION) && !TextUtils.isEmpty(getIntent().getStringExtra(ConsultationConstant.KEY_SPECIALIZATION))) {
                viewModel.setConsultationSpecialization(getIntent().getStringExtra(ConsultationConstant.KEY_SPECIALIZATION));
            }
            // get web engageModel
            if (getIntent() != null && getIntent().hasExtra(ConsultationConstant.CONSULTATION_WEB_ENGAGE_MODEL)) {
                WebEngageModel webEngageModel = (WebEngageModel) getIntent().getSerializableExtra(ConsultationConstant.CONSULTATION_WEB_ENGAGE_MODEL);
                if (webEngageModel == null) return;
                viewModel.setWebEngageModel(webEngageModel);
            }
        }
    }

    private boolean getIntentData() {
        if (getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.IS_FROM_PAYMENT))
            return getIntent().getBooleanExtra(PaymentIntentConstant.IS_FROM_PAYMENT, false);
        else return false;
    }

    private boolean isFromPaymentFailure() {
        if (getIntent() != null && getIntent().hasExtra(PaymentIntentConstant.FROM_PAYMENT_FAILURE))
            return getIntent().getBooleanExtra(PaymentIntentConstant.FROM_PAYMENT_FAILURE, false);
        else return false;
    }

    private boolean isM2Order() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.IS_M2_ORDER))
            return getIntent().getBooleanExtra(IntentConstant.IS_M2_ORDER, false);
        else return false;
    }


    @Override
    public void setPaymentListAdapter(List<PaymentGatewayList> list, boolean isPrime, boolean isPayTmUpiActive, boolean isGooglePayActive) {
        paymentListParentAdapter = new PaymentListParentAdapter(this, viewModel.classifiedListWithoutEmpty(list, isPrime), isPayTmUpiActive, isGooglePayActive);
        mBinding.paymentList.setItemAnimator(null);
        mBinding.paymentList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.paymentList.setAdapter(paymentListParentAdapter);
        mBinding.paymentList.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mBinding.paymentList.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        if (viewModel.netmedsPaymentProcess.getGrandTotal() == 0 || (viewModel.netmedsPaymentProcess.isFromDiagnostic() && DiagnosticHelper.isNMSCashApplied() && DiagnosticHelper.getremainingPaidAmount() == 0))
                            viewModel.disEnablePaymentList(false, mBinding.paymentList);
                    }
                });
        dismissProgressBarWithDelay(false);
    }

    @Override
    public String getSFlagForJusPayCreateOrder() {
        return idFromDiagnostics() ? AppConstant.S_FLAG_DIAGNOSTICS : getIntentData() ? AppConstant.S_FLAG_MEDICINE : AppConstant.S_FLAG_CONSULTATION;
    }

    @Override
    public void backToOrderReviewPage() {
        onBackPressed();
    }

    @Override
    public void executeBindingForShowPaymentDetails() {
        mBinding.setViewModel(viewModel);
    }

    @Override
    public void initiateJusPayTransaction(JusPayCreateOrderAndOrderStatusResponse response) {
        PaymentHelper.setIsCartSwitched(false);
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        preFetch(this, viewModel.getJusPayClientId());
        Bundle args = new Bundle();
        ArrayList<String> endUrl = new ArrayList<>();
        endUrl.add(response.getResult().getReturnUrl());
        args.putString(in.juspay.godel.core.PaymentConstants.MERCHANT_ID, viewModel.getJusPayMerchantId());
        args.putString(in.juspay.godel.core.PaymentConstants.CLIENT_ID, viewModel.getJusPayClientId());
        args.putString(in.juspay.godel.core.PaymentConstants.ORDER_ID, viewModel.netmedsPaymentProcess.getOrderId());
        args.putString(in.juspay.godel.core.PaymentConstants.AMOUNT, String.valueOf(viewModel.netmedsPaymentProcess.getGrandTotal()));
        args.putString(in.juspay.godel.core.PaymentConstants.TRANSACTION_ID, response.getResult().getId());
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_ID, response.getResult().getCustomerId());
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_MOBILE, response.getResult().getCustomerPhone());
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_EMAIL, response.getResult().getCustomerEmail());
        args.putString(in.juspay.godel.core.PaymentConstants.ENV, in.juspay.godel.core.PaymentConstants.ENVIRONMENT.PRODUCTION);
        args.putString(in.juspay.godel.core.PaymentConstants.SERVICE, PaymentConstants.JUSPAY_SERVICE);
        args.putStringArrayList(in.juspay.godel.core.PaymentConstants.END_URLS, endUrl);
        args.putString(in.juspay.godel.core.PaymentConstants.CLIENT_AUTH_TOKEN, response.getResult().getJuspay().getClientAuthToken());
        args.putString(in.juspay.godel.core.PaymentConstants.PAYLOAD, response.getResult().getPayLoad());
        args.putInt(PaymentConstants.REQUEST_CODE, viewModel.netmedsPaymentProcess.getJusPayCallBackRequestCode());
        viewModel.netmedsPaymentProcess.setTransactionLogPaymentRequest(args.toString());

        if (PaymentConstants.JUSPAY_LINK_WALLET == viewModel.netmedsPaymentProcess.getJusPayCallBackRequestCode()) {
            viewModel.netmedsPaymentProcess.setJusPayLinkWalletRequest(args);
        }
        if (viewModel.netmedsPaymentProcess.getJusPayCallBackRequestCode() == PaymentConstants.JUSPAY_NETBANKING) {
            minkasuFragment = new MinKasuFragment(BasePreference.getInstance(this), viewModel.netmedsPaymentProcess.getOrderId());
            minkasuFragment.setArguments(args);
            minkasuFragment.setJuspayCallback(juspayCallback());
            getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, minkasuFragment, minkasuFragment.getClass().getSimpleName()).addToBackStack(null).commit();
        } else {
            if (viewModel.netmedsPaymentProcess.getPaymentGatewaySubCategory() != null && PaymentConstants.OTHER_PAYMENT_LIST == viewModel.netmedsPaymentProcess.getPaymentGatewaySubCategory().getParentId() &&
                    PaymentConstants.PHONEPE == viewModel.netmedsPaymentProcess.getPaymentGatewaySubCategory().getSubId() && viewModel.netmedsPaymentProcess.isPhonePeActive()) {
                netmedsPaymentFragment = new PaymentFragment();
                netmedsPaymentFragment.setArguments(args);
                netmedsPaymentFragment.setJuspayCallback(juspayCallback());
                netmedsPaymentFragment.setWebViewClient(new NetmedsPaymentFragmentWebviewClient(netmedsPaymentFragment.getWebView(), netmedsPaymentFragment));
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, netmedsPaymentFragment, netmedsPaymentFragment.getClass().getSimpleName()).addToBackStack(null).commit();
            } else {
                Intent intent = new Intent(this, in.juspay.godel.PaymentActivity.class);
                intent.putExtras(args);
                startActivityForResult(intent, viewModel.netmedsPaymentProcess.getJusPayCallBackRequestCode());
            }
        }
        dismissProgressBarWithDelay(true);
    }

    private void dismissProgressBarWithDelay(final boolean isAnimationBar) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isAnimationBar) {
                    dismissProgress();
                } else {
                    vmDismissProgress();
                }
            }
        }, 1000 * 2);
    }

    @Override
    public void setNetPayableAmount(final double grandTotal) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (tvCartNetAmount != null && tvCartNetAmountLabel != null) {
                    tvCartNetAmountLabel.setVisibility(View.VISIBLE);
                    tvCartNetAmount.setVisibility(View.VISIBLE);
                    tvCartNetAmount.setText(CommonUtils.getPriceInFormat(grandTotal));
                }
            }
        });
    }

    @Override
    public void initOtpView(JusPayCreateWalletResponse createWalletResponse, PaymentGatewaySubCategory paymentGatewaySubCategory, Bundle jusPayCreateWalletRequest, String cartId) {
        Intent intent = new Intent(this, LinkWalletActivity.class);
        intent.putExtra(PaymentIntentConstant.PAYMENT_SUB_CATEGORY, new Gson().toJson(paymentGatewaySubCategory));
        intent.putExtra(PaymentIntentConstant.CREATE_WALLET_RESPONSE, new Gson().toJson(createWalletResponse));
        intent.putExtra(PaymentIntentConstant.CREATE_WALLET_REQUEST, jusPayCreateWalletRequest);
        intent.putExtra(PaymentIntentConstant.CART_ID, cartId);
        intent.putExtra(PaymentIntentConstant.IS_JUSPAY, true);
        if (viewModel.netmedsPaymentProcess.isFromDiagnostic() || viewModel.netmedsPaymentProcess.isFromConsultation()) {
            intent.putExtra(DiagnosticConstant.KEY_ORDER_ID, viewModel.netmedsPaymentProcess.getOrderId());
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION, true);
            intent.putExtra(DiagnosticConstant.KEY_TRANSACTION_AMOUNT, viewModel.netmedsPaymentProcess.getGrandTotal());
        }
        startActivityForResult(intent, PaymentIntentConstant.LINK_WALLET_JUSPAY);
    }

    @Override
    public void redirectToPayPalWebView(String orderId, String redirectUrl) {
        Intent intent = new Intent(this, PaypalActivity.class);
        intent.putExtra(PaymentConstants.ORDERID, orderId);
        intent.putExtra(PaymentConstants.REDIRECTION_URL, redirectUrl);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, PaymentConstants.PAYPAL_CALLBACK);
        dismissProgress();
    }

    @Override
    public void initPayTmOtpView(String response, PaymentGatewaySubCategory paymentGatewaySubCategory, PayTmAddMoneyToWalletParams payTmOtpViewParam, String cartId) {
        Intent intent = new Intent(this, LinkWalletActivity.class);
        intent.putExtra(PaymentIntentConstant.PAYMENT_SUB_CATEGORY, new Gson().toJson(paymentGatewaySubCategory));
        intent.putExtra(PaymentIntentConstant.CREATE_WALLET_RESPONSE, response);
        intent.putExtra(PaymentIntentConstant.CART_ID, cartId);
        intent.putExtra(PaymentIntentConstant.PAY_TM_OTP_PARAM, new Gson().toJson(payTmOtpViewParam));
        intent.putExtra(PaymentIntentConstant.IS_JUSPAY, false);
        if (viewModel.netmedsPaymentProcess.isFromDiagnostic() || viewModel.netmedsPaymentProcess.isFromConsultation()) {
            intent.putExtra(DiagnosticConstant.KEY_ORDER_ID, viewModel.netmedsPaymentProcess.getOrderId());
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION, true);
            intent.putExtra(DiagnosticConstant.KEY_TRANSACTION_AMOUNT, viewModel.netmedsPaymentProcess.getGrandTotal());
        }
        startActivityForResult(intent, PaymentIntentConstant.LINK_WALLET_PAYTM);
    }

    @Override
    public void navigateToPaymentFailurePage(int jusPayOrderStatus, PaymentGatewaySubCategory codGatewayDetails, boolean isPrimeProductAvailableInCart) {
        dismissProgress();
        clearPaymentsFragment();
        viewModel.resetPaymentList();
        onNavigateToFailure(jusPayOrderStatus, codGatewayDetails, isPrimeProductAvailableInCart);
    }

    private void onNavigateToFailure(int jusPayOrderStatus, PaymentGatewaySubCategory codGatewayDetails, boolean isPrimeProductAvailableInCart) {
        Bundle args = new Bundle();
        args.putInt(PaymentIntentConstant.JUSPAY_ORDER_STATUS, jusPayOrderStatus);
        args.putDouble(PaymentIntentConstant.AMOUNT, viewModel.netmedsPaymentProcess.getGrandTotal());
        args.putBoolean(PaymentIntentConstant.IS_PRIME_PRODUCT_AVAILABLE_IN_CART, isPrimeProductAvailableInCart);
        args.putString(PaymentIntentConstant.PAYMENT_COD_SUB_CATEGORY, new Gson().toJson(codGatewayDetails));
        args.putString(PaymentIntentConstant.FROM_DIAGNOSTICS, !TextUtils.isEmpty(viewModel.netmedsPaymentProcess.getIntentFromDiagnostic()) ? viewModel.netmedsPaymentProcess.getIntentFromDiagnostic() : "");
        args.putBundle(PaymentIntentConstant.DIAGNOSTICS_BUNDLE, getDiagnosticsBundle());
        PaymentFailureFragment paymentFailureFragment = new PaymentFailureFragment();
        paymentFailureFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(paymentFailureFragment, "Payment_failure_fragment").commitNowAllowingStateLoss();
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmShowLoader() {
        showTransactionProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    private JuspayCallback juspayCallback() {
        return new JuspayCallback() {
            @Override
            public void onResult(int requestCode, int resultCode, @NonNull Intent data) {
                jusPayPaymentCallBack(resultCode, data);
            }
        };
    }

    private void jusPayPaymentCallBack(int resultCode, Intent data) {
        vmShowLoader();
        viewModel.netmedsPaymentProcess.jusPayDirectTransactionCallBack(resultCode, data);
    }

    public void newCardPayLoad(boolean isNewCard) {
        viewModel.netmedsPaymentProcess.setNewCard(isNewCard);
        if (isNewCard) {
            viewModel.netmedsPaymentProcess.setPaymentGatewaySubCategory(null);
        }
    }

    @Override
    public void navigateToBankList() {
        Intent intent = new Intent(this, NetBankingActivity.class);
        intent.putStringArrayListExtra(PaymentIntentConstant.STATIC_BANK_LIST, (ArrayList<String>) viewModel.getStaticBankList());
        startActivityForResult(intent, PaymentConstants.NETBANKING_CALLBACK);
    }

    @Override
    public void navigateToAddNewCardDetails() {
        NewPaymentCardDetailsDialog cardDetailsDialog = new NewPaymentCardDetailsDialog(this, viewModel.netmedsPaymentProcess.getGrandTotal());
        getSupportFragmentManager().beginTransaction().add(cardDetailsDialog, IntentConstant.DELETE_CARD_CONFIRM_DILAOG).commitAllowingStateLoss();
    }

    @Override
    public double getTotalAmount() {
        return viewModel.netmedsPaymentProcess.getGrandTotal();
    }

    @Override
    public Bundle getNewCardDetails() {
        return newCardDetails;
    }

    @Override
    public void setError(String error) {
        dismissProgress();
        disableOrEnableOtherViewClicks(true);
        SnackBarHelper.snackBarCallBack(mBinding.parentLayout, this, error);
    }

    @Override
    public void navigateToSuccessPage(String completeOrderResponse) {
        Intent intent = new Intent(this, OrderPlacedSuccessfullyActivity.class);
        if (!idFromDiagnostics()) {
            intent.putExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG, SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER : PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M1);
            intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, new Gson().toJson(PaymentHelper.getCustomerAddress()));
        } else {
            intent.putExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG, PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_DIAGNOSTIC);
            intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, getDiagnosticsBundle());
            intent.putExtra(DiagnosticConstant.KEY_ORDER_DATE, viewModel.getDiagnosticOrderCreateDate());
        }
        intent.putExtra(SubscriptionIntentConstant.ORDER_ID, viewModel.netmedsPaymentProcess.getOrderId());
        intent.putExtra(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE, completeOrderResponse);
        intent.putExtra(PaymentIntentConstant.M2_FLAG, isM2Order());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void voucherView(List<AppliedVoucher> voucherList) {
        dismissProgress();
        if (getSupportFragmentManager().findFragmentByTag("VoucherFragment") == null) {
            VoucherFragment doctorConsultationFragment = new VoucherFragment(this, voucherList, viewModel.netmedsPaymentProcess.getGrandTotal(), isM2Order(), viewModel.cartDetails);
            getSupportFragmentManager().beginTransaction().add(doctorConsultationFragment, "VoucherFragment").commitNowAllowingStateLoss();
        }
    }

    @Override
    public void showCouponListView(List<ConsultationCoupon> consultationCouponList, MessageAdapterListener listener) {
        CouponListBottomSheetDialog couponListBottomSheetDialog = new CouponListBottomSheetDialog(consultationCouponList, viewModel.getConsultationAppliedCouponCode(), listener);
        getSupportFragmentManager().beginTransaction().add(couponListBottomSheetDialog, "CouponListBottomSheetDialog").commitNowAllowingStateLoss();
        dismissProgress();
    }

    @Override
    public void payTmAddMoney(PayTmAddMoneyToWalletParams intentParam, String cartId) {
        Intent intent = new Intent(this, PayTmAddMoneyActivity.class);
        intent.putExtra(PaymentIntentConstant.PAY_TM_ADD_MONEY_PARAM, new Gson().toJson(intentParam));
        intent.putExtra(PaymentIntentConstant.CART_ID, cartId);
        if (viewModel.netmedsPaymentProcess.isFromDiagnostic() || viewModel.netmedsPaymentProcess.isFromConsultation()) {
            intent.putExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAGNOSTIC_CONSULTATION, true);
        }
        startActivityForResult(intent, PaymentIntentConstant.PAYTM_ADD_MONEY);
    }

    @Override
    public void consultationRedirection(ConsultationEvent intent) {
        EventBus.getDefault().post(intent);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT:
            case PaymentConstants.JUSPAY_CARD_TRANSACTION:
            case PaymentConstants.JUSPAY_NETBANKING:
            case PaymentConstants.JUSPAY_AMAZONOPAY_WALLET:
                viewModel.netmedsPaymentProcess.jusPayDirectTransactionCallBack(resultCode, data);
                break;
            case PaymentConstants.JUSPAY_LINK_WALLET:
                viewModel.netmedsPaymentProcess.jusPayLinkWallet201CallBack(data);
                break;
            case PaymentConstants.NETBANKING_CALLBACK:
                viewModel.netmedsPaymentProcess.netBankingCallBack(data);
                break;
            case PaymentConstants.PAYPAL_CALLBACK:
                viewModel.netmedsPaymentProcess.executePayPalPayment(data);
                break;
            case PaymentConstants.JUSPAY_REFRESH_WALLET:
                viewModel.netmedsPaymentProcess.initJusPayRefreshWalletCallBack(resultCode, data);
                break;
            case PaymentIntentConstant.LINK_WALLET_PAYTM:
            case PaymentIntentConstant.PAYTM_ADD_MONEY:
                viewModel.netmedsPaymentProcess.paytmTransactionResultCallback(data, resultCode);
                break;
            case PaymentIntentConstant.LINK_WALLET_JUSPAY:
                viewModel.netmedsPaymentProcess.jusPayLinkWallet500CallBack(data);
                break;
        }
    }

    @Override
    public void setAppliedVoucher(List<AppliedVoucher> appliedVoucher) {
        showProgress(this);
        viewModel.setAppliedVoucherView(appliedVoucher, true, true);
    }

    @Override
    public void showAlert(String message) {
        dismissProgress();
        setError(message);
    }

    @Override
    public void onBackPressed() {
        if (minkasuFragment != null && minkasuFragment.isAdded()) {
            minkasuFragment.backPressHandler(true);
        } else if (netmedsPaymentFragment != null && netmedsPaymentFragment.isAdded()) {
            netmedsPaymentFragment.backPressHandler(true);
        } else {
            if (!viewModel.isFromPayment()) {
                ConsultationEvent consultationEvent = new ConsultationEvent();
                // keep it false else duplicate payment will be make - crucial
                consultationEvent.setStatus(false);
                consultationEvent.setCoupon(viewModel.netmedsPaymentProcess.isConsultationSelectedPlan() ? "" : viewModel.netmedsPaymentProcess.getConsultationApplyCoupon() != null ? new Gson().toJson(viewModel.netmedsPaymentProcess.getConsultationApplyCoupon()) : "");
                EventBus.getDefault().post(consultationEvent);
                this.finish();
            } else
                PaymentActivity.this.finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void setAmazonPayBalanceLinkStatus() {
        if (CommonUtils.isBuildVariantProdRelease()) {
            String paymentCredentials = BasePreference.getInstance(this).getPaymentCredentials();
            if (!TextUtils.isEmpty(paymentCredentials) && !TextUtils.isEmpty(PaymentUtils.amazonPayCredential(paymentCredentials))) {
                GetBalanceRequest request = new GetBalanceRequest(PaymentUtils.amazonPayCredential(paymentCredentials), false);
                AmazonPay.getBalance(this, request, new APayCallback() {
                    @Override
                    public void onSuccess(final Bundle bundle) {
                        final GetBalanceResponse response = GetBalanceResponse.fromBundle(bundle);
                        viewModel.setAmazonPayBalance(Double.parseDouble(response.getBalance()));
                        viewModel.setAmazonPayLinkStatus(true);
                        viewModel.initiateCalls();
                    }

                    @Override
                    public void onError(final APayError aPayError) {
                        viewModel.setAmazonPayLinkStatus(false);
                        viewModel.setAmazonPayBalance(0);
                        viewModel.initiateCalls();
                    }
                });
            } else {
                viewModel.setAmazonPayLinkStatus(false);
                viewModel.setAmazonPayBalance(0);
                viewModel.initiateCalls();
            }
        } else {
            viewModel.initiateCalls();
        }
    }

    @Override
    public void clearPaymentsFragment() {
        minkasuFragment = null;
        netmedsPaymentFragment = null;
    }

    @Override
    public void vmOrderSuccessActivity(String completeOrderResponse) {
        Intent intent = new Intent(this, OrderPlacedSuccessfullyActivity.class);
        intent.putExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG, SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER);
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, new Gson().toJson(PaymentHelper.getCustomerAddress()));
        intent.putExtra(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE, completeOrderResponse);
        intent.putExtra(PaymentIntentConstant.M2_FLAG, isM2Order());
        startActivity(intent);
    }

    @Override
    public void onPaymentRetry() {
        onPaymentFailureDismissDialog();
    }

    @Override
    public void onClickOnCOD() {
        PaymentHelper.setIsPayAndEditM2Order(false);
        PaymentHelper.setIsTempCart(true);
        PaymentHelper.setTempCartId(viewModel.netmedsPaymentProcess.isFromDiagnostic() ? BasePreference.getInstance(this).getMstarDiagnosticCartId() : Integer.valueOf(viewModel.getCartId()));
        viewModel.netmedsPaymentProcess.setPaymentGatewaySubCategory(viewModel.getCODPaymentDetails());
        viewModel.placeOrder();
    }

    @Override
    public void onPaymentFailureDismissDialog() {
        PaymentHelper.setIsPayAndEditM2Order(false);
        PaymentHelper.setIsFromRetryPopup(true);
        PaymentHelper.setIsTempCart(true);
        if (!TextUtils.isEmpty(viewModel.getCartId()))
            PaymentHelper.setTempCartId(viewModel.netmedsPaymentProcess.isFromDiagnostic() ? BasePreference.getInstance(this).getMstarDiagnosticCartId() : Integer.valueOf(viewModel.getCartId()));
        viewModel.initiateAPICall(PaymentServiceManager.C_MSTAR_PAYMENT_LIST);
    }

    @Override
    public void placeOrder(PaymentGatewaySubCategory selectedPaymentDetails, String cvv) {
        viewModel.checkValidationAndInitiatePlaceOrder(selectedPaymentDetails, cvv);
    }

    public void disableOrEnableOtherViewClicks(boolean isEnableOtherViewClicks) {
        mBinding.disableView.setVisibility(isEnableOtherViewClicks ? View.GONE : View.VISIBLE);
    }

    @Override
    public void updateSelectedPaymentGateway(PaymentGatewaySubCategory selectedPaymentDetails) {
        newCardPayLoad(false);
        viewModel.netmedsPaymentProcess.setPaymentGatewaySubCategory(selectedPaymentDetails);
        viewModel.fireWebEngageForPaymentMode();
    }

    @Override
    public void onProceedPaymentWithNewCardDetails(Bundle cardDetails) {
        newCardPayLoad(true);
        newCardDetails = cardDetails;
        /**
         * Proceed order using new card details.
         */
        placeOrder(null, "");
    }

    @Override
    public NetmedsPaymentProcess getPaymentProcessReference() {
        return viewModel.netmedsPaymentProcess;
    }

    private class PaymentGatewayObserver implements androidx.lifecycle.Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel paymentGatewayRespons) {
            viewModel.onPaymentGatewayDataAvailable(paymentGatewayRespons);
            mBinding.setViewModel(viewModel);
        }
    }

    private class ApplyCouponObserver implements Observer<ApplyCouponResponse> {

        @Override
        public void onChanged(@Nullable ApplyCouponResponse applyCouponResponse) {
            viewModel.onApplyCouponDataAvailable(applyCouponResponse);
            mBinding.setViewModel(viewModel);
        }
    }

    private class NMmsWalletBalanceForDiagnosticsObserver implements androidx.lifecycle.Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel nmsWalletResponse) {
            mBinding.setViewModel(viewModel);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Facebook Pixel Checkout Event*/
        FacebookPixelHelper.getInstance().logCheckoutEvent(this, FacebookPixelHelper.EVENT_PARAM_CHECKOUT_REVIEW, "");
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_PAYMENT_PAGE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void enableAndDisablePlaceOrderButtonInWalletSection(boolean enable) {
        /**
         * it means payment option is in enable or not.
         * enable = false only when there no remaining amount to pay through any gateway.(means amount used from wallet or voucher)
         */
        mBinding.paymentList.setEnabled(enable);
        mBinding.paymentList.setClickable(enable);
        if (!enable) {
            mBinding.trPlaceOrderButton.setVisibility(View.VISIBLE);
            mBinding.paymentList.setAlpha(0.4f);
        } else {
            mBinding.paymentList.setAlpha(1f);
            mBinding.trPlaceOrderButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void resetPaymentSelection() {
        disableOrEnableOtherViewClicks(true);
        paymentListParentAdapter.updatePaymentSelection(null, -1, -1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.payment_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.payment_total);
        View actionView = MenuItemCompat.getActionView(menuItem);
        tvCartNetAmount = actionView.findViewById(R.id.tv_net_amount);
        tvCartNetAmountLabel = actionView.findViewById(R.id.tv_label_pay);
        tvCartNetAmount.setVisibility(View.GONE);
        tvCartNetAmountLabel.setVisibility(View.GONE);
        if (viewModel != null && viewModel.netmedsPaymentProcess != null && viewModel.netmedsPaymentProcess.getGrandTotal() > 0) {
            tvCartNetAmountLabel.setVisibility(View.VISIBLE);
            tvCartNetAmount.setVisibility(View.VISIBLE);
            tvCartNetAmount.setText(CommonUtils.getPriceInFormat(viewModel.netmedsPaymentProcess.getGrandTotal()));
        }
        return true;
    }
}