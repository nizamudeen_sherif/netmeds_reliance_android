package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayOrderResult {
    @SerializedName("merchant_id")
    private String merchantId;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("id")
    private String id;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_email")
    private String customerEmail;
    @SerializedName("customer_phone")
    private String customerPhone;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("status")
    private String status;
    @SerializedName("status_id")
    private Integer statusId;
    @SerializedName("amount")
    private float amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("refunded")
    private boolean refunded;
    @SerializedName("amount_refunded")
    private float amountRefunded;
    @SerializedName("date_created")
    private String dateCreated;
    @SerializedName("return_url")
    private String returnUrl;
    @SerializedName("juspay")
    private Juspay juspay;
    @SerializedName("udf1")
    private String udf1;
    @SerializedName("udf2")
    private String udf2;
    @SerializedName("udf3")
    private String udf3;
    @SerializedName("udf4")
    private String udf4;
    @SerializedName("udf5")
    private String udf5;
    @SerializedName("udf6")
    private String udf6;
    @SerializedName("udf7")
    private String udf7;
    @SerializedName("udf8")
    private String udf8;
    @SerializedName("udf9")
    private String udf9;
    @SerializedName("udf10")
    private String udf10;
    @SerializedName("payment_links")
    private JusPayPaymentLinks payment_links;
    @SerializedName("txn_id")
    private String txn_id;
    @SerializedName("txn_uuid")
    private String txn_uuid;
    @SerializedName("gateway_id")
    private int gateway_id;
    @SerializedName("bank_error_code")
    private String bank_error_code;
    @SerializedName("bank_error_message")
    private String bank_error_message;
    @SerializedName("payment_method_type")
    private String payment_method_type;
    @SerializedName("payment_method")
    private String paymentMethod;
    @SerializedName("auth_type")
    private String authType;
    @SerializedName("payment_gateway_response")
    private JusPayOrderStatusPaymentResponse paymentGatewayResponse;

    private String payLoad;

    public Juspay getJuspay() {
        return juspay;
    }

    public void setJuspay(Juspay juspay) {
        this.juspay = juspay;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getRefunded() {
        return refunded;
    }

    public void setRefunded(Boolean refunded) {
        this.refunded = refunded;
    }

    public float getAmountRefunded() {
        return amountRefunded;
    }

    public void setAmountRefunded(Integer amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(String payLoad) {
        this.payLoad = payLoad;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public boolean isRefunded() {
        return refunded;
    }

    public void setRefunded(boolean refunded) {
        this.refunded = refunded;
    }

    public void setAmountRefunded(float amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getUdf2() {
        return udf2;
    }

    public void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public String getUdf6() {
        return udf6;
    }

    public void setUdf6(String udf6) {
        this.udf6 = udf6;
    }

    public String getUdf7() {
        return udf7;
    }

    public void setUdf7(String udf7) {
        this.udf7 = udf7;
    }

    public String getUdf8() {
        return udf8;
    }

    public void setUdf8(String udf8) {
        this.udf8 = udf8;
    }

    public String getUdf9() {
        return udf9;
    }

    public void setUdf9(String udf9) {
        this.udf9 = udf9;
    }

    public String getUdf10() {
        return udf10;
    }

    public void setUdf10(String udf10) {
        this.udf10 = udf10;
    }

    public JusPayPaymentLinks getPayment_links() {
        return payment_links;
    }

    public void setPayment_links(JusPayPaymentLinks payment_links) {
        this.payment_links = payment_links;
    }

    public String getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }

    public String getTxn_uuid() {
        return txn_uuid;
    }

    public void setTxn_uuid(String txn_uuid) {
        this.txn_uuid = txn_uuid;
    }

    public int getGateway_id() {
        return gateway_id;
    }

    public void setGateway_id(int gateway_id) {
        this.gateway_id = gateway_id;
    }

    public String getBank_error_code() {
        return bank_error_code;
    }

    public void setBank_error_code(String bank_error_code) {
        this.bank_error_code = bank_error_code;
    }

    public String getBank_error_message() {
        return bank_error_message;
    }

    public void setBank_error_message(String bank_error_message) {
        this.bank_error_message = bank_error_message;
    }

    public String getPayment_method_type() {
        return payment_method_type;
    }

    public void setPayment_method_type(String payment_method_type) {
        this.payment_method_type = payment_method_type;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public JusPayOrderStatusPaymentResponse getPaymentGatewayResponse() {
        return paymentGatewayResponse;
    }

    public void setPaymentGatewayResponse(JusPayOrderStatusPaymentResponse paymentGatewayResponse) {
        this.paymentGatewayResponse = paymentGatewayResponse;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
