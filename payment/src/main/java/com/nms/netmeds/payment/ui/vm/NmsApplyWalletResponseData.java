package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class NmsApplyWalletResponseData {

    @SerializedName("code")
    private String code;
    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private double value;
    @SerializedName("nmscash")
    private double nmsCash;
    @SerializedName("nmssupercash")
    private double nmsSuperCash;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getNmsCash() {
        return nmsCash;
    }

    public void setNmsCash(double nmsCash) {
        this.nmsCash = nmsCash;
    }

    public double getNmsSuperCash() {
        return nmsSuperCash;
    }

    public void setNmsSuperCash(double nmsSuperCash) {
        this.nmsSuperCash = nmsSuperCash;
    }
}
