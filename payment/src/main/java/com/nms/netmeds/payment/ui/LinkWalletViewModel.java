package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityLinkWalletBinding;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.nms.netmeds.payment.ui.vm.PayTmAddMoneyToWalletParams;
import com.nms.netmeds.payment.ui.vm.PayTmChecksumResponse;
import com.nms.netmeds.payment.ui.vm.PayTmWithdrawResponse;
import com.nms.netmeds.payment.ui.vm.PaytmConsultBalanceResponse;
import com.nms.netmeds.payment.ui.vm.PaytmConsultBalanceResponseBody;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmLinkWalletOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateOtpRequest;
import com.nms.netmeds.payment.ui.vm.PaytmValidateOtpResponse;
import com.nms.netmeds.payment.ui.vm.PaytmValidateTokenResponse;
import com.nms.netmeds.payment.ui.vm.PaytmWithdrawRequest;
import com.nms.netmeds.payment.ui.vm.VerifJusPayOtpResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class LinkWalletViewModel extends AppViewModel {

    private ActivityLinkWalletBinding mBinding;
    private JusPayCreateWalletResponse mJusPayCreateWalletResponse;
    private PaymentGatewaySubCategory mPaymentGatewaySubCategory;
    private LinkWalletListener mLinkWalletListener;
    private BasePreference mPreference;
    private ConfigMap getConfig;
    private PaytmLinkWalletOtpResponse paytmLinkWalletOtpResponse;
    private PaytmValidateTokenResponse paytmValidateToken;
    private PayTmAddMoneyToWalletParams addMoneyToWalletParams;
    private MStarCustomerDetails customerDetails;

    private String payTmValidateOtpResponseAccessToken = "";
    private String paymentRequest = "";
    private String paymentResponse = "";
    private String payTmCheckSum = "";
    private String payTmWithDrawCheckSum = "";
    private String cartId = "";
    private double totalAmount = 0.0;
    private boolean isJusPay = false;
    private boolean isFromConsultationOrDiagnostic;
    private String dcOrderId = "";
    private double dcTransactionAmount = 0.0;


    public LinkWalletViewModel(@NonNull Application application) {
        super(application);
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .into(view);
    }

    public String imageUrl() {
        return getPaymentGatewaySubCategory() != null && !TextUtils.isEmpty(getPaymentGatewaySubCategory().getImage()) ? getPaymentGatewaySubCategory().getImage() : "";
    }

    public String setPaymentGatewayName() {
        return getPaymentGatewaySubCategory() != null && !TextUtils.isEmpty(getPaymentGatewaySubCategory().getKey()) ? getPaymentGatewaySubCategory().getKey() : "";
    }

    public String setUserPhoneNo() {
        return customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "";
    }

    void initJusPayOtpValidation(ActivityLinkWalletBinding mBinding, BasePreference instance, String jusPayCreateWalletResponse, String paymentSubCategory, boolean jusPay, LinkWalletListener linkWalletListener) {
        this.mBinding = mBinding;
        this.mPreference = instance;
        this.mLinkWalletListener = linkWalletListener;
        customerDetails = new Gson().fromJson(mPreference.getCustomerDetails(), MStarCustomerDetails.class);
        setJusPayCreateWalletResponse(new Gson().fromJson(jusPayCreateWalletResponse, JusPayCreateWalletResponse.class));
        setPaymentGatewaySubCategory(new Gson().fromJson(paymentSubCategory, PaymentGatewaySubCategory.class));
        setJusPay(jusPay);
        startTimer();
        setOtpViewCount();
        getConfig = ConfigMap.getInstance();
        setOtpViewBehavior();
    }

    void initPayTmOtpValidation(ActivityLinkWalletBinding mBinding, BasePreference instance, boolean jusPay, LinkWalletListener linkWalletListener,
                                String createWalletResponse, String paymentSubCategory, String payTmOtpParam, String cartId) {
        this.mBinding = mBinding;
        this.mPreference = instance;
        this.mLinkWalletListener = linkWalletListener;
        this.cartId = cartId;
        setJusPay(jusPay);
        startTimer();
        customerDetails = new Gson().fromJson(mPreference.getCustomerDetails(), MStarCustomerDetails.class);
        setPaytmLinkWalletOtpResponse(new Gson().fromJson(createWalletResponse, PaytmLinkWalletOtpResponse.class));
        setPaymentGatewaySubCategory(new Gson().fromJson(paymentSubCategory, PaymentGatewaySubCategory.class));
        setAddMoneyToWalletParams(new Gson().fromJson(payTmOtpParam, PayTmAddMoneyToWalletParams.class));
        getConfig = ConfigMap.getInstance();
        setOtpViewBehavior();
    }

    private void initiateAPICall(int transactionId) {
        switch (transactionId) {
            case PaymentServiceManager.PAY_TM_VALIDATE_OTP:
                mLinkWalletListener.vmShowProgress();
                PaymentServiceManager.getInstance().verifyPayTmOtp(this, payTmValidateOtpRequest(), getPayTmHeader());
                break;
            case PaymentServiceManager.PAY_TM_VALIDATE_TOKEN:
                mLinkWalletListener.vmShowProgress();
                PaymentServiceManager.getInstance().validatePayTmToken(this, getPayTmValidateOtpResponseAccessToken());
                break;
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                APIServiceManager.getInstance().mstarUpdateCustomer(this, mPreference.getMstarBasicHeaderMap(), getUpdateTokenInCustomerRequest());
                break;
            case PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM:
                setTotalAmount(getAddMoneyToWalletParams().getOrderAmt());
                PaymentServiceManager.getInstance().payTmBalanceChecksum(this, mPreference.getMstarBasicHeaderMap(), getPayTmValidateOtpResponseAccessToken(), String.valueOf(getAddMoneyToWalletParams().getOrderAmt()));
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
                PaymentServiceManager.getInstance().payTmWithdrawFromWallet(this, mPreference.getMstarBasicHeaderMap(), getPayTmWithDrawFromWalletRequest()
                        , isFromConsultationOrDiagnostic(),
                        isFromConsultationOrDiagnostic() ? getDcOrderId() : "",
                        isFromConsultationOrDiagnostic() ? getDcTransactionAmount() + "" : "");
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:
                PaymentServiceManager.getInstance().payTmWithdrawDetails(this, getPayTmWithdrawTransactionDetailsRequest());
                break;
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        setPaymentResponse(data);
        switch (transactionId) {
            case PaymentServiceManager.PAY_TM_VALIDATE_OTP:
                payTmValidateOtpResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_VALIDATE_TOKEN:
                payTmValidateTokenResponse(data);
                break;
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                initiateAPICall(PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM);
                break;
            case PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM:
                payTmBalanceChecksumResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET:
                payTmWithdrawFromWalletResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS:
                payTmWithdrawDetailsResponse(data);
                break;
            case PaymentServiceManager.PAY_TM_SEND_OTP:
                payTmSendOtpResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        mLinkWalletListener.vmDismissProgress();
        setPaymentResponse(data);
        switch (transactionId) {
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                initiateAPICall(PaymentServiceManager.PAY_TM_BALANCE_CHECKSUM);
                break;
            case PaymentServiceManager.PAY_TM_VALIDATE_OTP:
                mLinkWalletListener.setError(getApplication().getResources().getString(R.string.text_enter_otp));
                break;
        }
    }

    /*****
     *
     * REQUEST**************************************
     */

    private PaytmValidateOtpRequest payTmValidateOtpRequest() {
        PaytmValidateOtpRequest request = new PaytmValidateOtpRequest();
        request.setOtp(getOtpEnteredByUser());
        request.setState(getPaytmLinkWalletOtpResponse().getState());
        setPaymentRequest(new Gson().toJson(request));
        return request;
    }

    private MstarUpdateCustomerRequest getUpdateTokenInCustomerRequest() {
        MStarCustomerDetails customerDetails = new Gson().fromJson(mPreference.getCustomerDetails(), MStarCustomerDetails.class);
        MstarUpdateCustomerRequest mstarUpdateCustomerRequest = new MstarUpdateCustomerRequest();
        mstarUpdateCustomerRequest.setFirstName(customerDetails.getFirstName());
        mstarUpdateCustomerRequest.setLastName(customerDetails.getLastName());
        mstarUpdateCustomerRequest.setEmail(customerDetails.getEmail());
        mstarUpdateCustomerRequest.setPaytmCustomerToken(getPayTmValidateOtpResponseAccessToken());
        return mstarUpdateCustomerRequest;
    }

    private Map<String, String> getPayTmWithDrawFromWalletRequest() {
        Map<String, String> param = new HashMap<>();
        param.put(PaymentConstants.API_APPIP, getConfig.getProperty(ConfigConstant.PAYTM_LINK_APPIP));
        param.put(AppConstant.CARTID, cartId);
        param.put(PaymentConstants.API_CUSTOMERID, getPaytmValidateToken().getId());
        param.put(PaymentConstants.API_SSOTOKEN, getPayTmValidateOtpResponseAccessToken());
        setPaymentRequest(new Gson().toJson(param));
        return param;
    }

    private PaytmWithdrawRequest getPayTmWithdrawTransactionDetailsRequest() {
        PaytmWithdrawRequest request = new PaytmWithdrawRequest();
        request.setAppIP(getConfig.getProperty(ConfigConstant.PAYTM_LINK_APPIP));
        request.setMID(getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID));
        request.setAuthMode(getConfig.getProperty(ConfigConstant.PAYTM_LINK_AUTHMODE));
        request.setTxnAmount(getAddMoneyToWalletParams() != null ?
                isFromConsultationOrDiagnostic() ? String.valueOf(getAddMoneyToWalletParams().getOrderAmt()) : BigDecimal.valueOf(getAddMoneyToWalletParams().getOrderAmt()).stripTrailingZeros().toPlainString() :
                "0");
        request.setOrderId(getAddMoneyToWalletParams().getOrderId());
        request.setDeviceId(setUserPhoneNo());
        request.setSSOToken(getPayTmValidateOtpResponseAccessToken());
        request.setPaymentMode(getConfig.getProperty(ConfigConstant.PAYTM_LINK_PAYMENTMODE));
        request.setCustId(getPaytmValidateToken().getId());
        request.setIndustryType(getPayTmCredentials(PaymentConstants.PAYTM_LINK_INDUSTRY_KEY));
        request.setChannel(getPayTmCredentials(PaymentConstants.PAYTM_LINK_CHANNEL));
        request.setReqType(getConfig.getProperty(ConfigConstant.PAYTM_LINK_WITHDRAW_REQUESTTYPE));
        request.setCurrency(getConfig.getProperty(ConfigConstant.PAYTM_LINK_CURRENCY));
        request.setCheckSum(payTmWithDrawCheckSum);
        setPaymentRequest(new Gson().toJson(request));
        return request;
    }


    /*****
     *
     * RESPSONE**************************************
     */
    private void payTmValidateOtpResponse(String data) {
        PaytmValidateOtpResponse paytmValidateOtpResponse = new Gson().fromJson(data, PaytmValidateOtpResponse.class);
        setPayTmValidateOtpResponseAccessToken(!TextUtils.isEmpty(paytmValidateOtpResponse.getAccessToken()) ? paytmValidateOtpResponse.getAccessToken() : "");
        initiateAPICall(PaymentServiceManager.PAY_TM_VALIDATE_TOKEN);
    }

    private void payTmSendOtpResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            PaytmLinkWalletOtpResponse paytmLinkWalletOtpResponse = new Gson().fromJson(response, PaytmLinkWalletOtpResponse.class);
            if (paytmLinkWalletOtpResponse.getStatus() != null && paytmLinkWalletOtpResponse.getStatus().equalsIgnoreCase("success")) {
                startTimer();
                setResendView(false);
            } else {
                mLinkWalletListener.vmDismissProgress();
                mLinkWalletListener.setError(paytmLinkWalletOtpResponse.getMessage());
            }
        }
    }

    private void payTmValidateTokenResponse(String data) {
        PaytmValidateTokenResponse response = new Gson().fromJson(data, PaytmValidateTokenResponse.class);
        if (response.getMobile() != null && !TextUtils.isEmpty(response.getMobile())) {
            setPaytmValidateToken(response);
            initiateAPICall(APIServiceManager.MSTAR_UPDATE_CUSTOMER);
        } else {
            mLinkWalletListener.vmDismissProgress();
            mLinkWalletListener.setError(getApplication().getResources().getString(R.string.verify_mobile_no));
        }
    }

    private void payTmBalanceChecksumResponse(String response) {
        String checkSum = "";
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        if (!TextUtils.isEmpty(response)) {
            final PayTmChecksumResponse paytmChecksumBalanceResponse = gson.fromJson(response, PayTmChecksumResponse.class);
            checkSum = paytmChecksumBalanceResponse.getChecksumhash();
        }
        String input = "{\"head\":{\"clientId\":" + "\"" + getPayTmCredentials(PaymentConstants.PAYTM_LINK_CLIENT_ID) + "\"" +
                ",\"version\":\"v1\",\"requestTimestamp\":" + "\"" + Calendar.getInstance().getTimeInMillis() +
                "\"" + ",\"channelId\":" + "\"" + getPayTmCredentials(PaymentConstants.PAYTM_LINK_CHANNEL) +
                "\"" + ",\"signature\":" + "\"" + checkSum + "\"" +
                "},\"body\":{\"userToken\":" + "\"" + getPayTmValidateOtpResponseAccessToken() + "\"" +
                ",\"totalAmount\":" + "\"" + getTotalAmount() + "\"" + ",\"mid\":" + "\"" + getPayTmCredentials(PaymentConstants.PAYTM_LINK_MID) + "\"}}";
        new AsyncTaskRunner(input).execute();
    }

    private void payTmConsultBalanceResponse(String response) {
        double deficitAmount = 0;
        PaytmConsultBalanceResponse paytmConsultBalanceResponse = new Gson().fromJson(response, PaytmConsultBalanceResponse.class);
        if (paytmConsultBalanceResponse != null && paytmConsultBalanceResponse.getBody() != null && paytmConsultBalanceResponse.getBody().getFundsSufficient()) {
            initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_FROM_WALLET);
        } else {
            if (paytmConsultBalanceResponse != null && paytmConsultBalanceResponse.getBody() != null) {
                PaytmConsultBalanceResponseBody paytmConsultBalanceResponseBody = paytmConsultBalanceResponse.getBody();
                deficitAmount = paytmConsultBalanceResponseBody.getDeficitAmount() != null ? paytmConsultBalanceResponseBody.getDeficitAmount() : 0;
            }
            mLinkWalletListener.vmDismissProgress();
            PayTmAddMoneyToWalletParams intentParam = new PayTmAddMoneyToWalletParams();
            intentParam.setPaymentUserId(getPaytmValidateToken().getId());
            intentParam.setPaytmUserToken(getPayTmValidateOtpResponseAccessToken());
            intentParam.setTransactionAmt(deficitAmount != 0 ? deficitAmount : getAddMoneyToWalletParams().getOrderAmt());
            intentParam.setImageUrl(getPaymentGatewaySubCategory().getImage());
            intentParam.setOrderId(getAddMoneyToWalletParams().getOrderId());
            intentParam.setOrderAmt(getTotalAmount());
            mLinkWalletListener.payTmAddMoney(intentParam);
        }
    }

    private void payTmWithdrawFromWalletResponse(String data) {
        final PayTmChecksumResponse paytmChecksumBalanceResponse = new Gson().fromJson(data, PayTmChecksumResponse.class);
        payTmWithDrawCheckSum = paytmChecksumBalanceResponse.getChecksumhash();
        initiateAPICall(PaymentServiceManager.PAY_TM_WITHDRAW_DETAILS);
    }

    private void payTmWithdrawDetailsResponse(String data) {
        PayTmWithdrawResponse response = new Gson().fromJson(data, PayTmWithdrawResponse.class);
        if (response != null && response.getTxnId() != null && response.getResponseCode().equals("01")) {
            if (!TextUtils.isEmpty(response.getCheckSum()))
                setPayTmCheckSum(response.getCheckSum());
            mLinkWalletListener.setPaymentResult(true);
        } else {
            mLinkWalletListener.setPaymentResult(false);
        }
    }

    private JusPayCreateWalletResponse getJusPayCreateWalletResponse() {
        return mJusPayCreateWalletResponse;
    }

    private void setJusPayCreateWalletResponse(JusPayCreateWalletResponse mJusPayCreateWalletResponse) {
        this.mJusPayCreateWalletResponse = mJusPayCreateWalletResponse;
    }

    private PaymentGatewaySubCategory getPaymentGatewaySubCategory() {
        return mPaymentGatewaySubCategory;
    }

    private void setPaymentGatewaySubCategory(PaymentGatewaySubCategory mPaymentGatewaySubCategory) {
        this.mPaymentGatewaySubCategory = mPaymentGatewaySubCategory;
    }

    public String setOtpSentText() {
        return getOtpSentText();
    }

    private String getOtpSentText() {
        String otpSentText = "";
        if (getPaymentGatewaySubCategory() != null) {
            switch (getPaymentGatewaySubCategory().getSubId()) {
                case PaymentConstants.MOBIKWIK:
                case PaymentConstants.PAYTM:
                    otpSentText = String.format(getApplication().getResources().getString(R.string.text_otp_sent), 6);
                    break;
                case PaymentConstants.FREE_CHARGE:
                    otpSentText = String.format(getApplication().getResources().getString(R.string.text_otp_sent), 4);
                    break;
            }
        }
        return otpSentText;
    }

    private void startTimer() {
        mBinding.otpTimer.setVisibility(View.VISIBLE);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mBinding.otpTimer.setText(String.format(getApplication().getResources().getString(R.string.text_waiting_for_otp), String.valueOf(millisUntilFinished / 1000)));
            }

            public void onFinish() {
                mBinding.otpTimer.setVisibility(View.INVISIBLE);
                setResendView(true);
            }
        }.start();
    }

    private void setResendView(boolean enabled) {
        mBinding.reSendOtp.setEnabled(enabled);
        mBinding.reSendOtp.setTextColor(enabled ? getApplication().getResources().getColor(R.color.colorMediumPink) : getApplication().getResources().getColor(R.color.colorLightPink));
    }

    public void verifyOtp() {
        if (!TextUtils.isEmpty(getOtpEnteredByUser())) {
            if (isJusPay())
                validateJusPayOtp();
            else validatePayTmOtp();
        } else
            mLinkWalletListener.setError(getApplication().getResources().getString(R.string.text_enter_otp));
    }

    public void reSendOtp() {
        startTimer();
        if (isJusPay())
            mLinkWalletListener.initJusPay(reSendOtpPayLoad(), PaymentConstants.JUSPAY_RESEND_OTP);
        else resendPayTmOtp();
    }

    private void resendPayTmOtp() {
        PaymentServiceManager.getInstance().sentPayTmOtp(this, payTmOtpRequest());
    }

    private PaytmLinkWalletOtpRequest payTmOtpRequest() {
        ConfigMap getConfig = ConfigMap.getInstance();
        PaytmLinkWalletOtpRequest paytmLinkWalletOtpRequest = new PaytmLinkWalletOtpRequest();
        paytmLinkWalletOtpRequest.setEmail(customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "");
        paytmLinkWalletOtpRequest.setPhone(customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "");
        paytmLinkWalletOtpRequest.setClientId(getPayTmCredentials(PaymentConstants.PAYTM_LINK_CLIENT_ID));
        paytmLinkWalletOtpRequest.setResponseType(getConfig.getProperty(ConfigConstant.PAYTM_LINK_OTP_RESPONSETYPE));
        paytmLinkWalletOtpRequest.setScope(getConfig.getProperty(ConfigConstant.PAYTM_LINK_OTP_SCOPE));
        setPaymentRequest(new Gson().toJson(paytmLinkWalletOtpRequest));
        return paytmLinkWalletOtpRequest;
    }


    private String getOtpEnteredByUser() {
        String otp = mBinding.otpOne.getText().toString() + mBinding.otpTwo.getText().toString() + mBinding.otpThree.getText().toString()
                + mBinding.otpFour.getText().toString() + mBinding.otpFive.getText().toString() + mBinding.otpSix.getText().toString();
        return otp.trim();
    }

    private boolean isJusPay() {
        return isJusPay;
    }

    private void setJusPay(boolean jusPay) {
        isJusPay = jusPay;
    }

    private void validateJusPayOtp() {
        mLinkWalletListener.initJusPay(validateOtpPayLoad(), PaymentConstants.JUSPAY_VERIFY_OTP);
    }

    private String validateOtpPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.LINK_WALLET);
            payLoad.put(PaymentConstants.WALLET_ID, getJusPayCreateWalletResponse() != null && !TextUtils.isEmpty(getJusPayCreateWalletResponse().getWalletId()) ? getJusPayCreateWalletResponse().getWalletId() : "");
            payLoad.put(PaymentConstants.OTP, getOtpEnteredByUser());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    private String reSendOtpPayLoad() {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.CREATE_WALLET);
            payLoad.put(PaymentConstants.WALLET_NAME, getJusPayCreateWalletResponse().getWallet());
            payLoad.put(PaymentConstants.OTP_PAGE_ENABLED, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    private void validatePayTmOtp() {
        initiateAPICall(PaymentServiceManager.PAY_TM_VALIDATE_OTP);
    }

    private String getPayTmHeader() {
        String credential = String.format("%s:%s", getPayTmCredentials(PaymentConstants.PAYTM_LINK_CLIENT_ID), getPayTmCredentials(PaymentConstants.PAYTM_LINK_SECRET_KEY));
        return PaymentConstants.BASIC + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
    }

    void verifyJusPayOtpResponse(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null && data.hasExtra(PaymentConstants.JUSPAY_PAY_LOAD) && !TextUtils.isEmpty(data.getStringExtra(PaymentConstants.JUSPAY_PAY_LOAD))) {
            VerifJusPayOtpResponse otpResponse = new Gson().fromJson(data.getStringExtra(PaymentConstants.JUSPAY_PAY_LOAD), VerifJusPayOtpResponse.class);
            initJusPayDirectWalletTransaction(otpResponse);
        } else {
            mLinkWalletListener.setError(getApplication().getResources().getString(R.string.text_valid_otp));
        }

    }

    private void initJusPayDirectWalletTransaction(VerifJusPayOtpResponse otpResponse) {
        if (!TextUtils.isEmpty(otpResponse.getWallet()) && !TextUtils.isEmpty(otpResponse.getToken()))
            mLinkWalletListener.initJusPay(jusPayWalletDirectDebitPayLoad(otpResponse.getWallet(), otpResponse.getToken()), PaymentConstants.JUSPAY_WALLET_DIRECT_DEBIT);
    }

    private String jusPayWalletDirectDebitPayLoad(String title, String token) {
        JSONObject payLoad = new JSONObject();
        try {
            payLoad.put(PaymentConstants.OP_NAME, PaymentConstants.WALLET_TXN);
            payLoad.put(PaymentConstants.PAYMENT_METHOD_TYPE, PaymentConstants.WALLET);
            payLoad.put(PaymentConstants.PAYMENT_METHOD, title);
            payLoad.put(PaymentConstants.DIRECT_WALLET_TOKEN, token);
            payLoad.put(PaymentConstants.REDIRECT_AFTER_PAYMENT, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payLoad.toString();
    }

    private PaytmLinkWalletOtpResponse getPaytmLinkWalletOtpResponse() {
        return paytmLinkWalletOtpResponse;
    }

    private void setPaytmLinkWalletOtpResponse(PaytmLinkWalletOtpResponse paytmLinkWalletOtpResponse) {
        this.paytmLinkWalletOtpResponse = paytmLinkWalletOtpResponse;
    }

    private String getPayTmValidateOtpResponseAccessToken() {
        return payTmValidateOtpResponseAccessToken;
    }

    private void setPayTmValidateOtpResponseAccessToken(String payTmValidateOtpResponseAccessToken) {
        this.payTmValidateOtpResponseAccessToken = payTmValidateOtpResponseAccessToken;
    }

    private double getTotalAmount() {
        return totalAmount;
    }

    private void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    private PaytmValidateTokenResponse getPaytmValidateToken() {
        return paytmValidateToken;
    }

    private void setPaytmValidateToken(PaytmValidateTokenResponse paytmValidateToken) {
        this.paytmValidateToken = paytmValidateToken;
    }

    String getPaymentRequest() {
        return paymentRequest;
    }

    private void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    String getPaymentResponse() {
        return paymentResponse;
    }

    private void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    private void setOtpViewBehavior() {
        setOPTView(mBinding.otpOne, mBinding.otpTwo, mBinding.otpOne);
        setOPTView(mBinding.otpTwo, mBinding.otpThree, mBinding.otpOne);
        setOPTView(mBinding.otpThree, mBinding.otpFour, mBinding.otpTwo);
        setOPTView(mBinding.otpFour, mBinding.otpFive, mBinding.otpThree);
        setOPTView(mBinding.otpFive, mBinding.otpSix, mBinding.otpFour);
        setOtpSixViewBehaviour();
    }

    private void setOtpSixViewBehaviour() {
        mBinding.otpSix.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0)
                    mBinding.otpFive.requestFocus();
            }
        });
    }

    private void setOPTView(final EditText editTextFrom, final EditText editTextTo, final EditText focusEditText) {
        editTextFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    editTextTo.requestFocus();
                } else if (s.length() == 0)
                    focusEditText.requestFocus();
            }
        });
    }

    private void setOtpViewCount() {
        if (getJusPayCreateWalletResponse() != null && !TextUtils.isEmpty(getJusPayCreateWalletResponse().getWallet())) {
            if (getJusPayCreateWalletResponse().getWallet().equalsIgnoreCase(PaymentConstants.FREE_CHARGE_WALLET)) {
                mBinding.otpSix.setVisibility(View.GONE);
                mBinding.otpFive.setVisibility(View.GONE);
            } else {
                mBinding.otpSix.setVisibility(View.VISIBLE);
                mBinding.otpFive.setVisibility(View.VISIBLE);
            }
        }
    }

    private PayTmAddMoneyToWalletParams getAddMoneyToWalletParams() {
        return addMoneyToWalletParams;
    }

    private void setAddMoneyToWalletParams(PayTmAddMoneyToWalletParams addMoneyToWalletParams) {
        this.addMoneyToWalletParams = addMoneyToWalletParams;
    }

    void resendOtpResponse(int resultCode) {
        if (resultCode == RESULT_OK) {
            startTimer();
            setResendView(false);
        }
    }

    public String getPayTmCheckSum() {
        return payTmCheckSum;
    }

    private void setPayTmCheckSum(String payTmCheckSum) {
        this.payTmCheckSum = payTmCheckSum;
    }


    public interface LinkWalletListener {
        void initJusPay(String payLoad, int callBackId);

        void vmShowProgress();

        void vmDismissProgress();

        void setError(String error);

        void setPaymentResult(boolean paymentResult);

        void payTmAddMoney(PayTmAddMoneyToWalletParams intentParam);
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private final String param;

        AsyncTaskRunner(String param) {
            this.param = param;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL("https://securegw.paytm.in/paymentservices/pay/consult").openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setUseCaches(false);
                connection.setDoOutput(true);

                DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
                requestWriter.writeBytes(param);
                requestWriter.close();
                String responseData;
                InputStream is = connection.getInputStream();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                responseData = responseReader.readLine();
                responseReader.close();
                return responseData;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            payTmConsultBalanceResponse(result);
        }
    }

    private String getPayTmCredentials(int key) {
        return PaymentUtils.payTmCredentials(key, mPreference.getPaymentCredentials());
    }

    public boolean isFromConsultationOrDiagnostic() {
        return isFromConsultationOrDiagnostic;
    }

    public void setFromConsultationOrDiagnostic(boolean fromConsultationOrDiagnostic) {
        isFromConsultationOrDiagnostic = fromConsultationOrDiagnostic;
    }

    public String getDcOrderId() {
        return dcOrderId;
    }

    public void setDcOrderId(String dcOrderId) {
        this.dcOrderId = dcOrderId;
    }

    public double getDcTransactionAmount() {
        return dcTransactionAmount;
    }

    public void setDcTransactionAmount(double dcTransactionAmount) {
        this.dcTransactionAmount = dcTransactionAmount;
    }

}