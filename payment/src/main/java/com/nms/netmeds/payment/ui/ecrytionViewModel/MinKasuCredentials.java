package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;

public class MinKasuCredentials {
    @SerializedName("key1")
    private String merchantId;
    @SerializedName("key2")
    private String accessToken;

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantId() {
        return this.merchantId;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return this.accessToken;
    }
}
