package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.Banner;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.CreateSubscriptionResponse;
import com.nms.netmeds.base.model.M3Flow2SubscriptionResponse;
import com.nms.netmeds.base.model.M3SubscriptionLogRequest;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCompleteOrderStatusAfterPayment;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.MstarReferAndEarn;
import com.nms.netmeds.base.model.OrderSuccess;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.adapter.DiagnosticOrderItemAdapter;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.SlotTime;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityOrderPlaceSuccessfulBinding;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;

public class OrderPlacedSuccessfullyViewModel extends AppViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Intent getIntent;
    private ActivityOrderPlaceSuccessfulBinding successfulBinding;
    private OrderPlacedSuccessfulListener listener;
    private M3Flow2SubscriptionResponse refillResponse = null;
    private MStarBasicResponseTemplateModel completeOrderResponse;
    private CreateSubscriptionResponse editOrderResponse;
    private OrderSuccess referEarnResponse;
    private ConfigurationResponse configurationResponse;
    private BannerAdapter.BannerAdapterListener bannerAdapterListener;
    private MstarReferAndEarn referAndEarnResponse;
    private M3SubscriptionLogRequest subscriptionLogRequest;
    private PatientDetail patientDetail;
    private Bundle diagnosticOrderCreationDetail;
    private AvailableLab selectedLab;
    private SlotTime slotTime;
    private MutableLiveData<MStarBasicResponseTemplateModel> referAndEarnMutableLiveData = new MutableLiveData<>();

    //FOR MSTAR
    private MStarAddressModel customerAddress;
    private String orderId;


    private String placedSuccessfulTitle = "";
    private String greetingsDescription = "";
    private String intentValue;
    private String currentDate = "";
    private String title;
    private String mail_content;
    private String mail_message;
    private String url;
    private String diagnosticOrderId;
    private String diagnosticOrderDate = "";
    private boolean isFromOrder = false;
    private boolean isM1Order = false;
    private boolean isFlow1SubscriptionOrder = false;
    private boolean refillFlag = false;

    /*Prime*/
    private boolean isPrimeProduct = false;
    private boolean isNonPrimeProduct = false;
    private int ratingCount = 0;

    public String getDiagnosticOrderDate() {
        return diagnosticOrderDate;
    }

    public void setDiagnosticOrderDate(String diagnosticOrderDate) {
        this.diagnosticOrderDate = diagnosticOrderDate;
    }

    public Bundle getDiagnosticOrderCreationDetail() {
        return diagnosticOrderCreationDetail;
    }

    public void setDiagnosticOrderCreationDetail(Bundle diagnosticOrderCreationDetail) {
        this.diagnosticOrderCreationDetail = diagnosticOrderCreationDetail;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getReferAndEarnMutableLiveData() {
        return referAndEarnMutableLiveData;
    }

    public OrderPlacedSuccessfullyViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityOrderPlaceSuccessfulBinding successfulBinding, OrderPlacedSuccessfulListener listener, Intent intent, BannerAdapter.BannerAdapterListener bannerAdapterListener) {
        this.context = context;
        this.successfulBinding = successfulBinding;
        this.listener = listener;
        this.getIntent = intent;
        this.bannerAdapterListener = bannerAdapterListener;
        this.configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        checkPrimeProduct();
        initiateProperties();
    }

    private void initiateProperties() {
        listener.checkIntentValue();
        calculateCurrentDate();
        editOrderResponse();
        referEarnResponse();
        PaymentHelper.setIsNMSCashApplied(false);
        PaymentHelper.setIsVoucherApplied(false);
        PaymentHelper.setIsNMSSuperCashApplied(false);
        if (isFromDiagnostic()) {
            setDiagnosticOrderDetail();
            DiagnosticHelper.clearSelectedList();
        }
        resetAllFlags();
        listener.smoothScrollTop();
    }

    private void calculateCurrentDate() {
        BasePreference.getInstance(context).setDeliveryIntervalPreference(0);
        Calendar calendar = Calendar.getInstance();
        currentDate = calendar.get(Calendar.DATE) + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR);
    }

    private void resetAllFlags() {
        PaymentHelper.setItemInsideOrderMap(null);
        PaymentHelper.setIsIncompleteOrder(false);
        PaymentHelper.setInCompleteOrderId("");
        PaymentHelper.setM2ChangeToM1(false);
        PaymentHelper.setIsTempCart(false);
        PaymentHelper.setTempCartId(0);
        PaymentHelper.setIsEditOrder(false);
        RefillHelper.resetMapValues();
        PaymentHelper.setIsPayAndEditM2Order(false);
        SubscriptionHelper.getInstance().setSubscriptionCartId(0);
        SubscriptionHelper.getInstance().setEditorderquantityflag(false);
    }

    public void setAddress(String address) {
        this.customerAddress = new Gson().fromJson(address, MStarAddressModel.class);
    }

    public int getExpectedDeliveryVisibility() {
        return (SubscriptionHelper.getInstance().isThisIssueOrder() || refillFlag || TextUtils.isEmpty(PaymentHelper.getDeliveryEstimate())) ? View.INVISIBLE : (isFlow1SubscriptionOrder || isM1Order) ? View.VISIBLE : View.INVISIBLE;
    }

    public String getSubscriptionHeader() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getRefillSectionValues() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getRefillSectionValues().getHeader()) ? configurationResponse.getResult().getConfigDetails().getRefillSectionValues().getHeader() : "";
    }

    public String getSubscriptionSubHeader() {
        ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getRefillSectionValues() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getRefillSectionValues().getSubHeader()) ? response.getResult().getConfigDetails().getRefillSectionValues().getSubHeader() : "";
    }

    public String getOrderedPersonName() {
        if (isFromOrder) {
            if (customerAddress != null) {
                return CommonUtils.getValidStringWithFirstLetterUpperCase(customerAddress.getFirstname() + " " + customerAddress.getLastname());
            } else {
                return "";
            }
        } else {
            return refillResponse != null && refillResponse.getResult() != null ? refillResponse.getResult().getCustomerName() : "";
        }
    }

    public String getOrderStatus() {
        if (!TextUtils.isEmpty(SubscriptionHelper.getInstance().getEditOrderResponse())) {
            if (editOrderResponse != null && editOrderResponse.getResult() != null) {
                successfulBinding.orderStatus.setTextColor(ContextCompat.getColor(context, !TextUtils.isEmpty(editOrderResponse.getResult().getSubscriptionStatusText())
                        ? editOrderResponse.getResult().getSubscriptionStatusText().equalsIgnoreCase(AppConstant.SUBSCRIPTION_CONFIRMED) ? R.color.colorGreen : R.color.colorSquash : R.color.colorSquash));
            }
            return editOrderResponse != null && editOrderResponse.getResult() != null && !TextUtils.isEmpty(editOrderResponse.getResult().getSubscriptionStatusText()) ? editOrderResponse.getResult().getSubscriptionStatusText() : "";
        } else {
            return isFromCod() ? getCodOrderStatus() : completeOrderResponse != null && !TextUtils.isEmpty(completeOrderResponse.getResult().getDisplayStatus()) ? completeOrderResponse.getResult().getDisplayStatus() : PaymentHelper.getIsRetryClick() ? context.getString(R.string.retry_order_text) : "";
        }
    }

    public boolean isFromOrder() {
        return isFromOrder || isFlow1SubscriptionOrder;
    }

    public void setIsFromOrder() {
        isFromOrder = false;
        isFlow1SubscriptionOrder = false;
    }

    public String orderID() {
        return isFromCod() ? getApplication().getString(R.string.text_order_id) + getCodOrderId() : completeOrderResponse != null && completeOrderResponse.getResult() != null &&
                !TextUtils.isEmpty(completeOrderResponse.getResult().getOrderId()) ? getApplication().getString(R.string.text_order_id) + completeOrderResponse.getResult().getOrderId() : "";
    }

    public String getOrderId() {
        return isFromCod() ? getCodOrderId() : completeOrderResponse != null && completeOrderResponse.getResult() != null &&
                !TextUtils.isEmpty(completeOrderResponse.getResult().getOrderId()) ? completeOrderResponse.getResult().getOrderId() : "";

    }

    public String OrderButton() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag() ? getApplication().getString(R.string.text_my_subscription) : isFromOrder || isFlow1SubscriptionOrder ? getApplication().getString(R.string.text_track_order) : getApplication().getString(R.string.text_manage);
    }

    public String getDeliveryDate() {
        return isFromOrder ? PaymentHelper.getDeliveryEstimate() : refillResponse != null && refillResponse.getResult() != null ? refillResponse.getResult().getNextDeliveryDate() : "";

    }

    public String getPlacedSuccessfulTitle() {
        return SubscriptionHelper.getInstance().isCreateNewFillFlag() ? getApplication().getString(R.string.text_title_refill_placed_successful) : placedSuccessfulTitle;
    }

    public String getGreetingsDescription() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag() ? getApplication().getString(R.string.text_refill_greeting_thanks_and_process) : greetingsDescription;
    }

    public String getOrderDiscription() {
        if (!TextUtils.isEmpty(SubscriptionHelper.getInstance().getEditOrderResponse())) {
            return editOrderResponse != null && editOrderResponse.getResult() != null && !TextUtils.isEmpty(editOrderResponse.getResult().getSubscriptionInfoText()) ? editOrderResponse.getResult().getSubscriptionInfoText() : "";
        } else {
            return isFromCod() ? getCodOrderDescription() : completeOrderResponse != null && !TextUtils.isEmpty(completeOrderResponse.getResult().getDescription()) ? completeOrderResponse.getResult().getDescription() :
                    PaymentHelper.getIsRetryClick() ? context.getString(R.string.retry_order_description_text) : "";
        }
    }

    public String referHeater() {
        return !TextUtils.isEmpty(referEarnResponse.getHeader()) ? referEarnResponse.getHeader() : "";
    }

    public String referSubTitle() {
        return !TextUtils.isEmpty(referEarnResponse.getSubTitle()) ? referEarnResponse.getSubTitle() : "";
    }

    public String referDescription() {
        return referEarnResponse != null && !TextUtils.isEmpty(referEarnResponse.getDescription()) ? referEarnResponse.getDescription() : "";
    }

    public void onclickShare() {
        listener.shareIntent(title, mail_content, mail_message, url);
    }

    public String buttonDescription() {
        return !TextUtils.isEmpty(referEarnResponse.getButtonDescription()) ? referEarnResponse.getButtonDescription() : "";
    }

    public String referCoupon() {
        return referAndEarnResponse != null && !TextUtils.isEmpty(referAndEarnResponse.getReferralCode()) ? referAndEarnResponse.getReferralCode() : "";
    }

    public String bannerExtra() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners().getHeader()) ? configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners().getHeader() : "";
    }

    public String bannerBenefits() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners().getSubHeader()) ? configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners().getSubHeader() : "";
    }

    public boolean referEarnVisibility() {
        return !isFromDiagnostic() && (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getReferEarnEnableFlag()) && !isPrimeProductOnly();

    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_REFER_AND_EARN:
                onReferAndEarnResponseAvailable(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                checkPrimeUserResponse(data);
                break;
            case APIServiceManager.C_MSTAR_RATE_ORDERS:
                onRatedResponse(data);
                break;
        }
    }

    private void onRatedResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null && response.getResult().getMessage() != null && AppConstant.RATING_SUCCESS_MESSAGE.equalsIgnoreCase(response.getResult().getMessage())) {
                listener.updateRatingView(ratingCount);
            } else if (response != null && response.getStatus() != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng())) {
                listener.showSnackbarMessage(response.getReason().getReason_eng());
            }
        }
    }

    private void checkPrimeUserResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getCustomerDetails() != null && !TextUtils.isEmpty(response.getResult().getCustomerDetails().getPrimeValidTillTime()) && !TextUtils.isEmpty(response.getResult().getCustomerDetails().getPrimePackage())) {
                successfulBinding.primeMemberCardView.setVisibility(View.VISIBLE);
                successfulBinding.primeExpiryDate.setText(DateTimeUtils.getInstance().stringDate(response.getResult().getCustomerDetails().getPrimeValidTillTime(), DateTimeUtils.yyyyMMddHHmmss, DateTimeUtils.ddMMMyyyy));
                successfulBinding.planPackageText.setText(response.getResult().getCustomerDetails().getPrimePackage());
            } else {
                successfulBinding.primeMemberCardView.setVisibility(View.GONE);
            }
        } else
            successfulBinding.primeMemberCardView.setVisibility(View.GONE);
    }

    private void onReferAndEarnResponseAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            referAndEarnMutableLiveData.setValue(response);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.vmDismissProgress();
        super.onFailed(transactionId, data);
    }

    public void renderPageBasedOnIntentValue(String intentValue) {
        this.intentValue = intentValue;
        if (!TextUtils.isEmpty(intentValue)) {
            ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
            boolean isRefillEnable = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().isRefillBlockEnableFlag();
            isFromOrder = PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M1.equals(intentValue) || PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M2.equals(intentValue) || SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER.equals(intentValue);
            isM1Order = PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M1.equals(intentValue) || PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_DIAGNOSTIC.equals(intentValue);
            isFlow1SubscriptionOrder = SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER.equals(intentValue);
            successfulBinding.cvRefillView.setVisibility(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M1.equals(intentValue) && isRefillEnable && !isPrimeProductOnly() ? View.VISIBLE : View.GONE);
            placedSuccessfulTitle = context.getString(isFromOrder || isFlow1SubscriptionOrder || isFromDiagnostic() ? R.string.text_title_order_placed_successful : R.string.text_title_refill_placed_successful);
            greetingsDescription = context.getString(isFromDiagnostic() ? R.string.text_diagnostic_order_success_message : isFromOrder || isFlow1SubscriptionOrder ? R.string.text_greeting_thanks_and_process : R.string.text_refill_greeting_thanks_and_process);
            successfulBinding.cvUnwellView.setVisibility(isFromDiagnostic() || isPrimeProductOnly() ? View.VISIBLE : View.GONE);
            successfulBinding.tvM2Desc.setVisibility(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M2.equals(intentValue) ? View.VISIBLE : View.GONE);
        }
    }

    public void trackOrder() {
        /*Google Analytics Click Event*/
        if (PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M2.equals(intentValue))
            GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_TRACK_ORDER, GoogleAnalyticsHelper.EVENT_LABEL_M2_ORDER_SUCCESSFUL);
        else
            GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_TRACK_ORDER, GoogleAnalyticsHelper.EVENT_LABEL_ORDER_SUCCESSFUL);
        listener.startHome(PaymentIntentConstant.OPEN_ORDER_SCREEN);
    }

    public void onRefillNow() {
        listener.initiateSubscription();
    }

    public void seeHowItWorks() {
        listener.vmSubscriptionOnboardingActivity();
    }

    public void setRefillResponse(String refillResponse) {
        if (!TextUtils.isEmpty(refillResponse)) {
            if (!SubscriptionHelper.getInstance().isCreateNewFillFlag()) {
                refillFlag = true;
            }
            this.refillResponse = new Gson().fromJson(refillResponse, M3Flow2SubscriptionResponse.class);
        }
    }

    private void editOrderResponse() {
        if (!TextUtils.isEmpty(SubscriptionHelper.getInstance().getEditOrderResponse())) {
            this.editOrderResponse = new Gson().fromJson(SubscriptionHelper.getInstance().getEditOrderResponse(), CreateSubscriptionResponse.class);
        }
    }

    public void completeOrderRespose(String response) {
        if (!TextUtils.isEmpty(response)) {
            completeOrderResponse = new Gson().fromJson(response, MStarBasicResponseTemplateModel.class);
        }
    }

    public void onReferralAvailable(MStarBasicResponseTemplateModel response) {
        if (response != null && response.getStatus().equals("Success") && response.getResult() != null && response.getResult().getReferAndEarn() != null) {
            referAndEarnResponse = response.getResult().getReferAndEarn();
            title = referAndEarnResponse.getReferralTitleContent() != null && !TextUtils.isEmpty(referAndEarnResponse.getReferralTitleContent()) ? referAndEarnResponse.getReferralTitleContent() : "";
            mail_content = referAndEarnResponse.getReferralMailContent() != null && !TextUtils.isEmpty(referAndEarnResponse.getReferralMailContent()) ? referAndEarnResponse.getReferralMailContent() : "";
            mail_message = referAndEarnResponse.getReferralMessageContent() != null && !TextUtils.isEmpty(referAndEarnResponse.getReferralMessageContent()) ? referAndEarnResponse.getReferralMessageContent() : "";
            url = referAndEarnResponse.getReferralUrl() != null && !TextUtils.isEmpty(referAndEarnResponse.getReferralUrl()) ? referAndEarnResponse.getReferralUrl() : "";
        }
    }


    private void referEarnResponse() {
        configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getReferEarn() != null && configurationResponse.getResult().getConfigDetails().getReferEarn().getOrderSuccess() != null) {
            referEarnResponse = configurationResponse.getResult().getConfigDetails().getReferEarn().getOrderSuccess();
            float density = context.getResources().getDisplayMetrics().density;
            Glide.with(context).load(referEarnResponse.getReferImg()).apply(new RequestOptions().override(Math.round(200 * density), Math.round(190 * density)))
                    .into(successfulBinding.referalImage);
            Glide.with(context).load(referEarnResponse.getReferAmountImg()).apply(new RequestOptions().override(Math.round(70 * density), Math.round(50 * density)))
                    .into(successfulBinding.amountImage);
            //Banner Visiblity
            if (!isFromDiagnostic() && (configurationResponse.getResult().getConfigDetails().getOrderSuccessBannerEnableFlag()) && !isPrimeProductOnly()) {
                bannerAdapter(configurationResponse.getResult().getConfigDetails().getOrderSuccessBlockBanners().getBanners());
            }
            if (configurationResponse.getResult().getConfigDetails().getReferEarnEnableFlag()) {
                referEarn();
            }
        }
        scrollToTop();
    }

    private void bannerAdapter(List<Banner> orderSuccessBlockBanners) {
        List<Banner> offer = new ArrayList<>();
        for (Banner value : orderSuccessBlockBanners) {
            if (!isM1Order && !AppConstant.SUBSCRIPTION.equalsIgnoreCase(value.getUrl())) {
                offer.add(value);
            }
        }
        if (!SubscriptionHelper.getInstance().isSubscriptionFlag() && !getIntent.getBooleanExtra(PaymentIntentConstant.M2_FLAG, false) && !isFromDiagnostic() && !isPrimeProductOnly()) {
            openSubscriptionDialog();
        }
        if (orderSuccessBlockBanners.size() > 0) {
            successfulBinding.offerView.setLayoutManager(setLinearLayoutManager());
            BannerAdapter offerAdapter = new BannerAdapter(context, isM1Order ? orderSuccessBlockBanners : offer, bannerAdapterListener);
            successfulBinding.offerView.setAdapter(offerAdapter);
            successfulBinding.cvOfferView.setVisibility(View.VISIBLE);
        } else successfulBinding.cvOfferView.setVisibility(View.GONE);
    }

    private void openSubscriptionDialog() {
        listener.openSubscriptionInitialDialog();
    }

    private LinearLayoutManager setLinearLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    private void referEarn() {
        APIServiceManager.getInstance().MstarReferAndEarn(this, BasePreference.getInstance(context).getMstarBasicHeaderMap());
    }

    private void scrollToTop() {
        successfulBinding.svParent.post(new Runnable() {
            @Override
            public void run() {
                successfulBinding.svParent.fullScroll(successfulBinding.svParent.FOCUS_UP);
            }
        });
    }

    public void orderReviewClick(View view) {
        listener.disableReview(true);
        int id = view.getId();
        if (id == R.id.img_rating_1) {
            this.ratingCount = 1;
        } else if (id == R.id.img_rating_2) {
            this.ratingCount = 2;
        } else if (id == R.id.img_rating_3) {
            this.ratingCount = 3;
        } else if (id == R.id.img_rating_4) {
            this.ratingCount = 4;
        } else if (id == R.id.img_rating_5) {
            this.ratingCount = 5;
        }
        if (!TextUtils.isEmpty(isFromDiagnostic() ? getDiagnosticOrderId() : getOrderId())) {
            listener.vmShowProgress();
            APIServiceManager.getInstance().MstarRateOrders(this, BasePreference.getInstance(context).getMstarBasicHeaderMap(), getRatingBody());
        } else {
            listener.disableReview(false);
        }
    }

    public void onDiagnosticOrderTrack() {
        listener.initDiagnosticOrderTack(getDiagnosticOrderId(), getPatientMobileNo());
    }

    private void setDiagnosticOrderDetail() {
        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_LAB))
            setSelectedLab((AvailableLab) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB));

        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_SELECTED_TIME_SLOT))
            setSlotTime((SlotTime) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_TIME_SLOT));

        if (getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_PATIENT_DETAIL))
            setPatientDetail((PatientDetail) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_PATIENT_DETAIL));

        if (getTestList() != null && getTestList().size() > 0)
            setTestListAdapter();

        /*Diagnostic WebEngage Order Placed Event*/
        setWebEngageOrderPlacedEvent();
    }

    private void setWebEngageOrderPlacedEvent() {
        PatientDetail patientDetail = (PatientDetail) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_PATIENT_DETAIL);
        String patientName = patientDetail != null && !TextUtils.isEmpty(patientDetail.getName()) ? patientDetail.getName() : "";
        String age = patientDetail != null && !TextUtils.isEmpty(patientDetail.getAge()) ? patientDetail.getAge() : "";
        String gender = patientDetail != null && !TextUtils.isEmpty(patientDetail.getGender()) ? patientDetail.getGender() : "";
        HashMap<String, Object> addressMap = null;
        MStarAddressModel collectionAddress = (MStarAddressModel) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS);
        if (collectionAddress != null) {
            String jsonObject = new Gson().toJson(collectionAddress);
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            addressMap = new Gson().fromJson(jsonObject, type);
        }
        String selectedDay = getSlotTime() != null && !TextUtils.isEmpty(getSlotTime().getSlotDay()) ? getSlotTime().getSlotDay() : "";
        String selectedTime = getSlotTime() != null && !TextUtils.isEmpty(getSlotTime().getTimeRange()) ? getSlotTime().getTimeRange() : "";

        AvailableLab availableLab = (AvailableLab) getDiagnosticOrderCreationDetail().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB);
        LabDescription labDescription = availableLab != null && availableLab.getLabDescription() != null ? availableLab.getLabDescription() : new LabDescription();
        String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
        PriceDescription priceDescription = availableLab != null && availableLab.getPriceDescription() != null ? availableLab.getPriceDescription() : new PriceDescription();
        double price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? Double.parseDouble(priceDescription.getFinalPrice()) : 0;
        List<Test> testList = availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? availableLab.getTestList() : new ArrayList<Test>();
        double diagnosticTotalAmount = getDiagnosticOrderCreationDetail() != null && getDiagnosticOrderCreationDetail().containsKey(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) ? getDiagnosticOrderCreationDetail().getDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) : 0;

        Bundle bundle = new Bundle();
        bundle.putString(DiagnosticConstant.KEY_PATIENT_NAME, patientName);
        bundle.putString(DiagnosticConstant.KEY_AGE, age);
        bundle.putString(DiagnosticConstant.KEY_GENDER, gender);
        bundle.putSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS, addressMap);
        bundle.putString(DiagnosticConstant.KEY_SELECTED_DAY, selectedDay);
        bundle.putString(DiagnosticConstant.KEY_SELECTED_TIME, selectedTime);
        bundle.putString(DiagnosticConstant.KEY_LAB_NAME, labName);
        bundle.putDouble(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE, price);
        bundle.putSerializable(DiagnosticConstant.KEY_SELECTED_TEST_LIST, (Serializable) testList);
        bundle.putString(DiagnosticConstant.KEY_ORDER_ID, getDiagnosticOrderId());
        bundle.putDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT, diagnosticTotalAmount);
        bundle.putString(DiagnosticConstant.KEY_ORDER_SUCCESS, DiagnosticConstant.YES);
        bundle.putString(DiagnosticConstant.KEY_ORDER_DATE, DateTimeUtils.getInstance().utcToLocal(DateTimeUtils.yyyyMMddTHHmmssSSS, DateTimeUtils.yyyyMMddHHmmss, getDiagnosticOrderDate()));

        WebEngageHelper.getInstance().orderPlacedEvent(BasePreference.getInstance(context), DiagnosticHelper.getCity(), bundle, listener.getContext());
    }

    private void setTestListAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        successfulBinding.testListView.setLayoutManager(linearLayoutManager);
        successfulBinding.testListView.setNestedScrollingEnabled(false);
        DiagnosticOrderItemAdapter orderItemAdapter = new DiagnosticOrderItemAdapter(context, getTestList(), false);
        successfulBinding.testListView.setAdapter(orderItemAdapter);
    }

    public boolean isFromDiagnostic() {
        return intentValue != null && intentValue.equals(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_DIAGNOSTIC);
    }

    private AvailableLab getSelectedLab() {
        return selectedLab;
    }

    private void setSelectedLab(AvailableLab selectedLab) {
        this.selectedLab = selectedLab;
    }

    private SlotTime getSlotTime() {
        return slotTime;
    }

    private void setSlotTime(SlotTime slotTime) {
        this.slotTime = slotTime;
    }

    private List<Test> getTestList() {
        return getSelectedLab() != null && getSelectedLab().getTestList() != null && getSelectedLab().getTestList().size() > 0 ? getSelectedLab().getTestList() : new ArrayList<Test>();
    }

    private LabDescription getLabDescription() {
        return getSelectedLab() != null && getSelectedLab().getLabDescription() != null ? getSelectedLab().getLabDescription() : new LabDescription();
    }

    public String getLabName() {
        return !TextUtils.isEmpty(getLabDescription().getLabName()) ? String.format("%s %s", context.getResources().getString(R.string.text_by), CommonUtils.convertToTitleCase(getLabDescription().getLabName())) : "";
    }

    public String getLabNameWithoutPrefix() {
        return !TextUtils.isEmpty(getLabDescription().getLabName()) ? String.format("%s", CommonUtils.convertToTitleCase(getLabDescription().getLabName())) : "";
    }

    public String slotDateTime() {
        String slot = "";
        String slotDate = getSlotTime() != null && getSlotTime().getSlotDate() != null && !TextUtils.isEmpty(getSlotTime().getSlotDate()) ? getSlotTime().getSlotDate() : "";
        String slotTime = getSlotTime() != null && getSlotTime().getStartTime() != null && !TextUtils.isEmpty(getSlotTime().getStartTime()) ? getSlotTime().getStartTime() : "";
        slot = DateTimeUtils.getInstance().stringDate(slotDate, DateTimeUtils.yyyyMMdd, DateTimeUtils.dd_MMM);
        return slot + "," + slotTime;
    }

    private PatientDetail getPatientDetail() {
        return patientDetail;
    }

    private void setPatientDetail(PatientDetail patientDetail) {
        this.patientDetail = patientDetail;
    }

    public String getPatientName() {
        return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getName()) ? CommonUtils.convertToTitleCase(patientDetail.getName()) : "";
    }

    public String getPatientMobileNo() {
        return getPatientDetail() != null && !TextUtils.isEmpty(getPatientDetail().getTelephone()) ? patientDetail.getTelephone() : "";
    }

    public String getDiagnosticOrderId() {
        return diagnosticOrderId;
    }

    public void setDiagnosticOrderId(String diagnosticOrderId) {
        this.diagnosticOrderId = diagnosticOrderId;
    }

    public void setOrderID(String orderID) {
        this.orderId = orderID;
    }

    private void checkPrimeProduct() {
        if (PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M2.equals(getIntent.getStringExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG)) ||
                getIntent.getStringExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG).equals(SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_REFILL)) {
            isNonPrimeProduct = true;
        } else {
            if (PaymentHelper.getCartLineItems() != null && PaymentHelper.getCartLineItems().size() > 0) {
                MstarPrimeProductResult primeProductResult = new Gson().fromJson(BasePreference.getInstance(context).getMstarPrimeMembership(), MstarPrimeProductResult.class);
                for (MStarProductDetails itemResult : PaymentHelper.getCartLineItems()) {
                    if (primeProductResult.getPrimeProductCodesList().contains(String.valueOf(itemResult.getProductCode()))) {
                        isPrimeProduct = true;
                    } else
                        isNonPrimeProduct = true;
                }
            } else {
                isNonPrimeProduct = true;
            }
            if (isPrimeProduct)
                checkPrimeUser();
        }
    }

    private String productType() {
        return (isPrimeProduct && isNonPrimeProduct) ? PaymentConstants.MIXED_PRODUCT : (isPrimeProduct ? PaymentConstants.PRIME_PRODUCT : (isNonPrimeProduct ? PaymentConstants.NON_PRIME_PRODUCT : ""));
    }

    public boolean isPrimeProductOnly() {
        return productType().equals(PaymentConstants.PRIME_PRODUCT);
    }

    public boolean isMixedProduct() {
        return productType().equals(PaymentConstants.MIXED_PRODUCT);
    }

    public boolean isNonPrimeProductOnly() {
        return productType().equals(PaymentConstants.NON_PRIME_PRODUCT);
    }

    public boolean isShowDiagnostic() {
        return isFromDiagnostic() && !isPrimeProductOnly();
    }

    public boolean isShowTrackOrder() {
        return isFromDiagnostic() || isPrimeProductOnly();
    }

    private PrimeConfig primeConfig() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null ? configurationResponse.getResult().getConfigDetails().getPrimeConfig() : new PrimeConfig();
    }

    public String primeMemberHeaderText() {
        return primeConfig() != null && !TextUtils.isEmpty(primeConfig().getPrimeMemberText()) ? primeConfig().getPrimeMemberText() : "";
    }

    //todo
    private void checkPrimeUser() {
        boolean isConnected = NetworkUtils.isConnected(context);
        if (isConnected) {
            APIServiceManager.getInstance().mStarCustomerDetails(this, BasePreference.getInstance(context).getMstarBasicHeaderMap());
        }
    }


    private MStarCompleteOrderStatusAfterPayment getCompleteOrderStatusAfterPaymentStatus() {
        return completeOrderResponse != null && completeOrderResponse.getResult() != null && completeOrderResponse.getResult().getOrderStatusAfterPayment() != null ? completeOrderResponse.getResult().getOrderStatusAfterPayment() : new MStarCompleteOrderStatusAfterPayment();
    }

    private String getCodOrderStatus() {
        return getCompleteOrderStatusAfterPaymentStatus() != null && !TextUtils.isEmpty(getCompleteOrderStatusAfterPaymentStatus().getDisplayStaus()) ? getCompleteOrderStatusAfterPaymentStatus().getDisplayStaus() : "";
    }

    private String getCodOrderDescription() {
        return getCompleteOrderStatusAfterPaymentStatus() != null && !TextUtils.isEmpty(getCompleteOrderStatusAfterPaymentStatus().getMessage()) ? getCompleteOrderStatusAfterPaymentStatus().getMessage() : "";
    }

    private MStarCartDetails getCartDetails() {
        return completeOrderResponse != null && completeOrderResponse.getResult() != null && completeOrderResponse.getResult().getCartDetails() != null ? completeOrderResponse.getResult().getCartDetails() : new MStarCartDetails();
    }

    private String getCodOrderId() {
        return getCartDetails() != null && !TextUtils.isEmpty(getCartDetails().getOrder_id()) ? getCartDetails().getOrder_id() : "";
    }

    private boolean isFromCod() {
        return getCartDetails() != null && !TextUtils.isEmpty(getCartDetails().getPaymentAggregator()) && (getCartDetails().getPaymentAggregator().equalsIgnoreCase("cod") || getCartDetails().getPaymentAggregator().equalsIgnoreCase("nop"));
    }

    private MultipartBody getRatingBody() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.ORDER_ID, isFromDiagnostic() ? getDiagnosticOrderId() : getOrderId()).addFormDataPart(AppConstant.RATING_COUNT, String.valueOf(ratingCount))
                .addFormDataPart(AppConstant.CHANNEL, AppConstant.MSTAR_CHANNEL_NAME).addFormDataPart(AppConstant.RATING_EVENT, AppConstant.FROM_ORDER_SUCCESS).build();
    }


    public boolean isRatingEnabled() {
        return !listener.isM2Order() && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isRatingSuccessPageEnabled();
    }

    public interface OrderPlacedSuccessfulListener {

        void checkIntentValue();

        void startHome(boolean value);

        void initiateSubscription();

        void vmSubscriptionOnboardingActivity();

        void shareIntent(String title, String mailContent, String message, String url);

        void vmShowProgress();

        void vmDismissProgress();

        void openSubscriptionInitialDialog();

        void initDiagnosticOrderTack(String orderId, String mobileNo);

        void smoothScrollTop();

        Context getContext();

        void updateRatingView(int ratingCount);

        void showSnackbarMessage(String message);

        boolean isM2Order();

        void disableReview(boolean disable);
    }

}
