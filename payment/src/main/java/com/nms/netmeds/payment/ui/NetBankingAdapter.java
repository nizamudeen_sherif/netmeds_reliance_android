package com.nms.netmeds.payment.ui;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.AdapterNetbankingEmptyViewBinding;
import com.nms.netmeds.payment.databinding.AdapterNetbankingLayoutBinding;
import com.nms.netmeds.payment.ui.vm.ModifiedNetBankingList;
import com.nms.netmeds.payment.ui.vm.ModifiedPaymentMethod;
import com.nms.netmeds.payment.ui.vm.NetBankingPaymentMethods;

import java.util.List;

public class NetBankingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 10;
    private final ModifiedNetBankingList netBankingList;
    private final Context mContext;
    private final NetBankingListAdapter.NetBankingListener listener;


    NetBankingAdapter(ModifiedNetBankingList netBankingList, Context context, NetBankingListAdapter.NetBankingListener listener) {
        this.netBankingList = netBankingList;
        this.mContext = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            AdapterNetbankingEmptyViewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_netbanking_empty_view, parent, false);
            return new NetBankingAdapter.EmptyViewViewHolder(binding);
        } else {
            AdapterNetbankingLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_netbanking_layout, parent, false);
            return new NetBankingAdapter.NetBankingHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {

        if (vh instanceof NetBankingHolder) {

            NetBankingHolder holder = (NetBankingHolder) vh;

            ModifiedPaymentMethod nmsModifiesPaymentMethod = netBankingList.getPaymentMethods().get(position);

            if (nmsModifiesPaymentMethod != null) {
                if (!TextUtils.isEmpty(nmsModifiesPaymentMethod.getTitle()))
                    holder.mBinding.title.setText(nmsModifiesPaymentMethod.getTitle());

                if (nmsModifiesPaymentMethod.getArray() != null && nmsModifiesPaymentMethod.getArray().size() > 0) {
                    setNetBankingListView(holder, nmsModifiesPaymentMethod.getArray());
                }
            }
        }
    }

    private void setNetBankingListView(NetBankingHolder holder, List<NetBankingPaymentMethods> array) {
        NetBankingListAdapter nmsNetBankingListAdapter = new NetBankingListAdapter(array, listener);
        holder.mBinding.netBankingSubList.setLayoutManager(new LinearLayoutManager(mContext));
        holder.mBinding.netBankingSubList.setAdapter(nmsNetBankingListAdapter);
    }

    @Override
    public int getItemCount() {
        return netBankingList.getPaymentMethods().size() > 0 ? netBankingList.getPaymentMethods().size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (netBankingList.getPaymentMethods().size() == 0) {
            return VIEW_TYPE_EMPTY;
        } else {
            return super.getItemViewType(position);
        }
    }

    public class NetBankingHolder extends RecyclerView.ViewHolder {
        private final AdapterNetbankingLayoutBinding mBinding;

        NetBankingHolder(AdapterNetbankingLayoutBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }
    }

    private class EmptyViewViewHolder extends RecyclerView.ViewHolder {

        private EmptyViewViewHolder(AdapterNetbankingEmptyViewBinding binding) {
            super(binding.getRoot());
        }
    }
}
