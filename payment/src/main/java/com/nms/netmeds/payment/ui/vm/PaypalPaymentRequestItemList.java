/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PaypalPaymentRequestItemList {
    @SerializedName("items")
    private List<PaypalPaymentRequestItem> items = null;
    @SerializedName("shipping_phone_number")
    private String shippingPhoneNumber;
    @SerializedName("shipping_address")
    private PaypalPaymentRequestAddress shippingAddress;

    public List<PaypalPaymentRequestItem> getItems() {
        return items;
    }

    public void setItems(List<PaypalPaymentRequestItem> items) {
        this.items = items;
    }

    public PaypalPaymentRequestAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(PaypalPaymentRequestAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingPhoneNumber() {
        return shippingPhoneNumber;
    }

    public void setShippingPhoneNumber(String shippingPhoneNumber) {
        this.shippingPhoneNumber = shippingPhoneNumber;
    }
}