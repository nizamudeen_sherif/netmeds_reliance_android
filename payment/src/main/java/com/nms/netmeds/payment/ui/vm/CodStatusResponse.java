package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

public class CodStatusResponse {
    @SerializedName("updatedOn")
    private Integer updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private CodStatusResult result;
    @SerializedName("fromRetry")
    private boolean fromRetry;
    private String selectedGateway;


    public boolean isFromRetry() {
        return fromRetry;
    }

    public void setFromRetry(boolean fromRetry) {
        this.fromRetry = fromRetry;
    }

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Integer updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public CodStatusResult getResult() {
        return result;
    }

    public void setResult(CodStatusResult result) {
        this.result = result;
    }

    public String getSelectedGateway() {
        return selectedGateway;
    }

    public void setSelectedGateway(String selectedGateway) {
        this.selectedGateway = selectedGateway;
    }
}
