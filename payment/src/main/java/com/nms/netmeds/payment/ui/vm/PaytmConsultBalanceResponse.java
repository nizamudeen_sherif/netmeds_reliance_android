package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class PaytmConsultBalanceResponse {
    @SerializedName("head")
    private PaytmConsultBalanceHead head;
    @SerializedName("body")
    private PaytmConsultBalanceResponseBody body;

    public PaytmConsultBalanceHead getHead() {
        return head;
    }

    public void setHead(PaytmConsultBalanceHead head) {
        this.head = head;
    }

    public PaytmConsultBalanceResponseBody getBody() {
        return body;
    }

    public void setBody(PaytmConsultBalanceResponseBody body) {
        this.body = body;
    }

}