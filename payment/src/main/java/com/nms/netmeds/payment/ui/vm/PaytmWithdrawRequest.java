package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaytmWithdrawRequest {
    @SerializedName("MID")
    private String mID;
    @SerializedName("ReqType")
    private String reqType;
    @SerializedName("TxnAmount")
    private String txnAmount;
    @SerializedName("AppIP")
    private String appIP;
    @SerializedName("OrderId")
    private String orderId;
    @SerializedName("Currency")
    private String currency;
    @SerializedName("DeviceId")
    private String deviceId;
    @SerializedName("SSOToken")
    private String sSOToken;
    @SerializedName("PaymentMode")
    private String paymentMode;
    @SerializedName("CustId")
    private String custId;
    @SerializedName("IndustryType")
    private String industryType;
    @SerializedName("Channel")
    private String channel;
    @SerializedName("AuthMode")
    private String authMode;
    @SerializedName("CheckSum")
    private String checkSum;

    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getAppIP() {
        return appIP;
    }

    public void setAppIP(String appIP) {
        this.appIP = appIP;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSSOToken() {
        return sSOToken;
    }

    public void setSSOToken(String sSOToken) {
        this.sSOToken = sSOToken;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAuthMode() {
        return authMode;
    }

    public void setAuthMode(String authMode) {
        this.authMode = authMode;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }


}