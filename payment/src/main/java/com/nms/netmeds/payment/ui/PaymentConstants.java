package com.nms.netmeds.payment.ui;

public class PaymentConstants {
    public static final String IN = "IN";
    /**
     * Total Information API Constants
     */

    public static final String IHO = "IHO";
    public static final String SHIPPING_AMOUNT = "shipping";
    public static final String WALLET_AMOUNT_DEDECTED = "fee";
    public static final String GIFT_VOUCHER = "giftvoucheraftertax";
    public static final String GRAND_TOTAL = "grand_total";
    public static final String DISCOUNT = "discount";
    public static final String ORDER_SOURCE = "app-android";
    public static final int NETBANKING_CALLBACK = 211;
    public static final String REQUEST_CODE = "requestCode";
    public static final String CANCELLED_BY_USER = "Cancelled By User";
    public static final String PAYMENT_REQUEST = "PAYMENT_REQUEST";
    public static final String PAYMENT_RESPONSE = "PAYMENT_RESPONSE";
    public static final String PAYTM_CHECK_SUM = "PAYTM_CHECK_SUM";
    public static final String API_TRNXAMOUNT = "txnAmount";
    public static final String API_TRANSACTION_AMOUNT = "TxnAmount";
    public static final String P_FLAG = "pFlag";
    public static final String DC_ORDER_ID = "orderId";


    public static final String DISCOUNT_FROM_WALLET = "DISCOUNT_FROM_WALLET";
    public static final String DISCOUNT_FROM_VOUCHER = "DISCOUNT_FROM_VOUCHER";
    public static final String DISCOUNT_FROM_COUPON = "DISCOUNT_FROM_COUPON";
    /**
     * JusPay Service Call Constants
     */
    public static final String JUSPAY_SERVICE = "in.juspay.ec";
    public static final String IS_DEVICE_READY = "isDeviceReady";
    public static final String GOOGLEPAY = "GOOGLEPAY";

    public static final String E_WALLET = "E_Wallet";
    public static final String E_VOUCHER = "E_Voucher";
    public static final String E_WALLET_AND_E_VOUCHER = "E_Wallet + E_Voucher";

    public static final int JUS_PAY = 6;
    public static final String SUCCESS = "success";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String FREE = "free";
    public static final String PENDING = "Pending";
    public static final String FAILURE = "Failure";
    public static final String EWALLET = "eWallet : ";
    public static final String VOUCHER = "Voucher : ";
    public static final String ORDERID = "orderId :";
    public static final String PENDING_VBV = "PENDING_VBV";
    public static final String AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
    public static final String AUTHORIZATION_FAILED = "AUTHORIZATION_FAILED";
    public static final String JUSPAY_DECLINED = "JUSPAY_DECLINED";
    public static final String AUTHORIZING = "AUTHORIZING";
    /**
     * JusPay Payment Gateway Parent Id
     */

    public static final int OTHER_PAYMENT_LIST = 1;
    public static final int UPI_PAYMENT_LIST = 6;
    public static final int WALLET_LIST = 2;
    public static final int CREDIT_DEBIT_LIST = 3;
    public static final int NET_BANKING_LIST = 4;
    public static final int COD = 5;
    /**
     * JusPay Payment Gateway Child Id
     */

    public static final int PAYPAL = 0;
    public static final int PHONEPE = 1;
    public static final int MOBIKWIK = 1;
    public static final int FREE_CHARGE = 2;
    public static final int PAYTM_UPI = 2;
    public static final int GOOGLE_PAY_UPI = 0;
    public static final int PAYTM = 3;
    public static final int AMAZON = 4;
    public static final int OLA = 5;
    /**
     * Payment Activity Result ID
     */
    public static final int PAYPAL_CALLBACK = 200;
    public static final int JUSPAY_LINK_WALLET = 201;
    public static final int JUSPAY_CARD_TRANSACTION = 202;
    public static final int JUSPAY_WALLET_DIRECT_DEBIT = 203;
    public static final int JUSPAY_NETBANKING = 204;
    public static final int OTHER_PAYMENTS = 205;
    public static final int JUSPAY_AMAZONOPAY_WALLET = 206;
    public static final int JUSPAY_RESEND_OTP = 207;
    public static final int JUSPAY_VERIFY_OTP = 208;
    public static final int JUSPAY_REFRESH_WALLET = 210;
    /**
     * JusPay Operation Constant
     */
    public static final String OP_NAME = "opName";
    public static final String PAYMENT_METHOD_TYPE = "paymentMethodType";
    public static final String PAYMENT_METHOD = "paymentMethod";
    public static final String SDK_PRESENT = "sdkPresent";
    public static final String REDIRECT_AFTER_PAYMENT = "redirectAfterPayment";
    public static final String GPAY_MOBILE_NO = "mobileNumber";
    public static final String WALLET_TXN = "walletTxn";
    public static final String WALLET = "Wallet";
    public static final String ORDER_ID = "order_id";
    public static final String AMOUNT = "amount";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String API_CUSTOMERID = "custId";
    public static final String CUSTOMER_EMAIL = "customer_email";
    public static final String CUSTOMER_PHONE = "customer_phone";
    public static final String REGX_URL = "regxurl";
    public static final String S_FLAG = "sFlag";
    public static final String NB_TXN = "nbTxn";
    public static final String NB = "NB";
    public static final String CARD_NUMBER = "cardNumber";
    public static final String CARD_EXP_MONTH = "cardExpMonth";
    public static final String CARD_EXP_YEAR = "cardExpYear";
    public static final String NAME_ON_CARD = "nameOnCard";
    public static final String CARD_SECURITY_CODE = "cardSecurityCode";
    public static final String SAVE_TO_LOCKER = "saveToLocker";
    public static final String CARD_TXN = "cardTxn";
    public static final String CARD = "Card";
    public static final String DIRECT_WALLET_TOKEN = "directWalletToken";
    public static final String CARD_TOKEN = "cardToken";
    public static final String OTP_PAGE_ENABLED = "otpPageEnabled";
    public static final String SDK_WALLET_IDENTIFIER = "sdkWalletIdentifier";
    public static final String WALLET_NAME = "walletName";
    public static final String CREATE_WALLET = "createWallet";
    public static final String DELINK_WALLET = "delinkWallet";
    public static final String WALLET_ID = "walletId";
    public static final String REFRESH_WALLET = "refreshWallet";
    public static final String LINK_WALLET = "linkWallet";
    public static final String OTP = "otp";
    public static final String JUSPAY_PAY_LOAD = "payload";
    public static final String ANDROID_AMAZONPAY_TOKENIZED = "ANDROID_AMAZONPAY_TOKENIZED";
    public static final String ANDROID_PHONEPE_SDK = "ANDROID_PHONEPE";
    public static final String ANDROID_GOOGLEPAY = "ANDROID_GOOGLEPAY";
    public static final String JUSPAY_CUSTOMERS_ID = "Customer_id";
    public static final String CARD_VALIDITY = "CARD_VALIDITY";
    public static final String CARD_CVV = "CARD_CVV";
    public static final String BANK_NAME = "BANK_NAME";
    public static final String BANK_ID = "BANK_ID";
    public static final String FREE_CHARGE_WALLET = "FREECHARGE";
    public static final String CASH_ON_DELIVERY = "cashondelivery";
    public static final String MOBILENO = "mobileno";
    public static final String EMAIL = "email";
    public static final String UPI_OP_NAME = "upiTxn";
    public static final String UPI_METHOD_NAME = "UPI";
    public static final String UPI_SDK_PRESENT = "upiSdkPresent";
    public static final String PAY_WITH_APP = "payWithApp";
    /**
     * Generate Quote Request Constants
     */

    public static final String PRESCRIPTION = "P";
    public static final String OTC = "O";
    public static final String YES = "Y";
    public static final String NO = "N";
    /**
     * JusPay Order Status
     */
    public static final int PAYMENT_STATUS_SUCESS = 21;
    public static final int PAYMENT_STATUS_PENDING = 28;
    public static final int PAYMENT_STATUS_PENDING_VBV = 23;
    public static final int PAYMENT_STATUS_AUTHORIZING = 28;
    public static final int PAYMENT_STATUS_AUTHENTICATION_FAILED = 26;
    public static final int PAYMENT_STATUS_AUTHORIZATION_FAILED = 27;
    public static final int PAYMENT_STATUS_JUS_PAY_DECLINED = 22;
    /**
     * PayPal Constants
     */
    public static final String GRANT_TYPE = "grant_type";
    public static final String CLIENT_CREDENTIALS = "client_credentials";
    public static final String BEARER = "Bearer ";
    public static final String REDIRECTION_URL = "redirectUrl";
    public static final String RESULT = "result";
    public static final String APPROVAL_URL = "approval_url";
    public static final String EXECUTE = "execute";
    public static final String PAY_PAL_PAYMENT_METHOD = "paypal";
    public static final String APPROVED = "approved";
    public static final String SALE = "sale";
    public static final String SET_PROVIDED_ADDRESS = "SET_PROVIDED_ADDRESS";
    public static final String COMMIT = "commit";
    public static final String LOCALE = "en_IN";
    public static final String BILLING = "Billing";
    public static final String INR = "INR";
    public static final String INSTANT_FUNDING_SOURCE = "INSTANT_FUNDING_SOURCE";
    public static final String COUPON = "COUPON";
    public static final String PAY_PAL_VOUCHER = "VOUCHER";

    /**
     * PayTm Constants
     */
    public static final String BASIC = "Basic ";
    public static final String SESSION_TOKEN = "Session_token";
    public static final String USER_TOKEN = "userToken";
    public static final String TOTAL_AMOUNT = "totalAmount";
    public static final String MID = "mid";
    public static final String CUST_ID = "CUST_ID";
    public static final String PAY_TM_ORDER_ID = "ORDER_ID";
    public static final String TXN_AMOUNT = "TXN_AMOUNT";
    public static final String APP_IP = "APP_IP";
    public static final String API_APPIP = "appIp";
    public static final String MOBILE_NO = "MOBILE_NO";
    public static final String SSO_TOKEN = "SSO_TOKEN";
    public static final String API_SSOTOKEN = "ssoToken";
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String INDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID";
    public static final String WEBSITE = "WEBSITE";
    public static final String CALLBACK_URL = "CALLBACK_URL";
    public static final String REQUEST_TYPE = "REQUEST_TYPE";
    public static final String CHECKSUMHASH = "CHECKSUMHASH";
    public static final String PAYTM_ORDERID = "ORDERID";
    public static final String PAYTM_MID = "MID";
    public static final String PAY_TM_PAYMENT_METHOD = "paytm";
    public static final String JUS_PAY_PAYMENT_METHOD = "nmsPay";

    /**
     * Payment Key Token
     */
    public static final String PAY_PAL_AUTH_URL = "PAY_PAL_AUTH_URL";
    public static final String PAY_PAL_PAYMENT_URL = "PAY_PAL_PAYMENT_URL";
    public static final String PAY_PAL_RETURN_URL = "PAY_PAL_RETURN_URL";
    public static final String PAY_PAL_CLIENT_ID = "PAY_PAL_CLIENT_ID";
    public static final String PAY_PAL_SECRET = "PAY_PAL_SECRET";
    public static final String PAY_PAL_CURRENCY = "PAY_PAL_CURRENCY";

    public static final String PAYTM_TRANSACTION_CHECKSUM_STATUS_SUCCESS_CODE = "01";
    //PayTM Key Token
    public static final int PAYTM_LINK_CLIENT_ID = 1;
    public static final int PAYTM_LINK_SECRET_KEY = 2;
    public static final int PAYTM_LINK_MID = 3;
    public static final int PAYTM_LINK_CHANNEL = 4;
    public static final int PAYTM_LINK_INDUSTRY_KEY = 5;
    public static final int PAYTM_LINK_WEBSITE = 6;
    public static final int PAYTM_LINK_CALLBACK = 7;
    public static final int PAYTM_ADD_MONEY = 8;
    public static final int PAYTM_BALANCE_CHECK = 9;
    public static final int PAYTM_WITHDRAW = 10;
    public static final int PAYTM_TRANSACTION = 11;

    //JusPay Key Token
    public static final int JUS_PAY_RETURN_URL = 1;
    public static final int JUS_PAY_CREATE_ORDER = 2;
    public static final int JUS_PAY_UPDATE_CUSTOMER = 3;
    public static final int JUS_PAY_ORDER_STATUS = 4;
    public static final int JUS_PAY_CREATE_CUSTOMER = 5;
    public static final int JUS_PAY_PAYMENT_METHODS = 6;
    public static final int JUS_PAY_END_URL = 7;
    public static final int JUS_PAY_CLIENT_ID = 8;
    public static final int JUS_PAY_MERCHENT_ID = 9;
    public static final int JUS_PAY_GATEWAY_KEY = 10;

    //Update JusPay Customer
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";

    //Consultaion PayPal Constant
    public static final String ADDRESS_CITY = "Address City";
    public static final String TAMIL_NADU = "Tamil Nadu";
    public static final String ADDRESS_LINE_ONE = "Street address line one";
    public static final String ADDRESS_LINE_TWO = "Street address line two";
    public static final String ADDRESS_PINCODE = "000000";

    //Minkasu Credentials
    public static final int MINKASU_MERCHANT_ID = 1;
    public static final int MINKASU_ACCESS_TOKEN = 2;

    //COD Credentials
    public static final int COD_PAYMENT_METHOD = 1;
    public static final int COD_MINIMUM_AMOUNT = 2;
    public static final int COD_MAXIMUM_AMOUNT = 3;

    public static final String MIXED_PRODUCT = "MIXED_PRODUCT";
    public static final String PRIME_PRODUCT = "PRIME_PRODUCT";
    public static final String NON_PRIME_PRODUCT = "NON_PRIME_PRODUCT";

    public static final String PAY_TM_UPI_PACKAGE = "net.one97.paytm";
    public static final String GOOGLE_PAY_PACKAGE = "com.google.android.apps.nbu.paisa.user";
    public static final String PHONE_PE_PACKAGE = "com.phonepe.app";
    public static final String PAY_TM_UPI_ID = "upipaytm";
    public static final String GOOGLE_PAY_ID = "GOOGLEPAY";
    public static final String ORDERTYPE = "orderType";
    public static final String FORM_DATA_PAYTM_CHECK_SUM = "paytmCheckSum";
    public static final String FORM_DATA_PAY_PAL_PAY_ID = "paypalPayId";


    public static final String ORIENTATION_HORIZONTAL = "horizontal";
    public static final String PHONE_PE_ID = "PhonePe";
}