package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class PaytmValidateOtpRequest {
    @SerializedName("otp")
    private String otp;
    @SerializedName("state")
    private String state;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
