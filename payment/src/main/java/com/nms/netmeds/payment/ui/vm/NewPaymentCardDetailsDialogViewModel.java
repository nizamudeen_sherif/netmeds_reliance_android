package com.nms.netmeds.payment.ui.vm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.BaseViewModel;

public class NewPaymentCardDetailsDialogViewModel extends BaseViewModel {

    private NewPaymentCardDetailsDialogViewModelCallback callback;

    public NewPaymentCardDetailsDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void initialize(NewPaymentCardDetailsDialogViewModelCallback callback) {
        this.callback = callback;
    }

    public void closeView() {
        callback.onDismissNewCardDialog();
    }

    public void onClickAddAndPay() {
        callback.onProceedPaymentWithNewCardDetails();
    }

    public interface NewPaymentCardDetailsDialogViewModelCallback {
        void onDismissNewCardDialog();

        void onProceedPaymentWithNewCardDetails();
    }
}
