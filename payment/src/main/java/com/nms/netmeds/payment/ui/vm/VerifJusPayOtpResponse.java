package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class VerifJusPayOtpResponse {
    @SerializedName("wallet")
    private String wallet;
    @SerializedName("token")
    private String token;
    @SerializedName("code ")
    private int code;
    @SerializedName("status ")
    private String status;

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
