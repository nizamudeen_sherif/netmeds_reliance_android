package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;

public class JusPayGatewayKey {
    @SerializedName("key1")
    private String returnUrl;
    @SerializedName("key2")
    private String key2;
    @SerializedName("key3")
    private String deLink;
    @SerializedName("key4")
    private String key4;
    @SerializedName("key5")
    private String order;
    @SerializedName("key6")
    private String updateCustomer;
    @SerializedName("key7")
    private String orderStatus;
    @SerializedName("key8")
    private String key8;
    @SerializedName("key9")
    private String createCustomer;
    @SerializedName("key10")
    private String key10;
    @SerializedName("key11")
    private String paymentMethods;
    @SerializedName("key12")
    private String endUrl;
    @SerializedName("key14")
    private String createOrder;
    @SerializedName("key15")
    private String juspayClientKey;
    @SerializedName("key16")
    private String juspayGatewayKey;
    @SerializedName("key17")
    private String juspayMerchantId;
    @SerializedName("key18")
    private String juspayAPIKey;
    @SerializedName("key19")
    private String juspayAPI;

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getDeLink() {
        return deLink;
    }

    public void setDeLink(String deLink) {
        this.deLink = deLink;
    }

    public String getKey4() {
        return key4;
    }

    public void setKey4(String key4) {
        this.key4 = key4;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getUpdateCustomer() {
        return updateCustomer;
    }

    public void setUpdateCustomer(String updateCustomer) {
        this.updateCustomer = updateCustomer;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getKey8() {
        return key8;
    }

    public void setKey8(String key8) {
        this.key8 = key8;
    }

    public String getCreateCustomer() {
        return createCustomer;
    }

    public void setCreateCustomer(String createCustomer) {
        this.createCustomer = createCustomer;
    }

    public String getKey10() {
        return key10;
    }

    public void setKey10(String key10) {
        this.key10 = key10;
    }

    public String getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public void setEndUrl(String endUrl) {
        this.endUrl = endUrl;
    }

    public String getEndUrl() {
        return endUrl;
    }

    public String getCreateOrder() {
        return createOrder;
    }

    public void setCreateOrder(String createOrder) {
        this.createOrder = createOrder;
    }

    public String getJuspayClientKey() {
        return juspayClientKey;
    }

    public void setJuspayClientKey(String juspayClientKey) {
        this.juspayClientKey = juspayClientKey;
    }

    public String getJuspayGatewayKey() {
        return juspayGatewayKey;
    }

    public void setJuspayGatewayKey(String juspayGatewayKey) {
        this.juspayGatewayKey = juspayGatewayKey;
    }

    public String getJuspayMerchantId() {
        return juspayMerchantId;
    }

    public void setJuspayMerchantId(String juspayMerchantId) {
        this.juspayMerchantId = juspayMerchantId;
    }

    public String getJuspayAPIKey() {
        return juspayAPIKey;
    }

    public void setJuspayAPIKey(String juspayAPIKey) {
        this.juspayAPIKey = juspayAPIKey;
    }

    public String getJuspayAPI() {
        return juspayAPI;
    }

    public void setJuspayAPI(String juspayAPI) {
        this.juspayAPI = juspayAPI;
    }
}
