package com.nms.netmeds.payment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.ActivityNetBankingBinding;
import com.nms.netmeds.payment.ui.vm.ModifiedNetBankingList;

public class NetBankingActivity extends BaseViewModelActivity<NetBankingViewModel> implements NetBankingViewModel.NetBankingListener, NetBankingListAdapter.NetBankingListener {

    private ActivityNetBankingBinding mBinding;
    private ModifiedNetBankingList netBankingList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_net_banking);
        toolBarSetUp(mBinding.toolbar);
        intiToolBar();
        mBinding.setViewModel(onCreateViewModel());
    }

    protected void intiToolBar() {
        mBinding.collapsingToolbar.setTitle(NetBankingActivity.this.getResources().getString(R.string.select_bank_text));
        mBinding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        mBinding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        mBinding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        mBinding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    protected NetBankingViewModel onCreateViewModel() {
        NetBankingViewModel nmsNetBankingViewModel = ViewModelProviders.of(this).get(NetBankingViewModel.class);
        mBinding.setViewModel(nmsNetBankingViewModel);
        nmsNetBankingViewModel.init(this, this);
        return nmsNetBankingViewModel;
    }

    @Override
    public void showProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void setListAdapter(ModifiedNetBankingList netBankingList) {
        this.netBankingList = netBankingList;
        setAdapter();
    }

    private void setAdapter() {
        NetBankingAdapter netBankingAdapter = new NetBankingAdapter(netBankingList, this, this);
        mBinding.bankList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.bankList.setAdapter(netBankingAdapter);
        mBinding.bankList.setVisibility(View.VISIBLE);
    }

    @Override
    public void setBankSelection(Intent intent) {
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }
}
