package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModifiedPaymentMethod {

    @SerializedName("title")
    private String title;
    @SerializedName("array")
    private List<NetBankingPaymentMethods> array = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<NetBankingPaymentMethods> getArray() {
        return array;
    }

    public void setArray(List<NetBankingPaymentMethods> array) {
        this.array = array;
    }
}
