package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.R;
import com.nms.netmeds.payment.databinding.FragmentPaymentFailureBinding;

@SuppressLint("ValidFragment")
public class PaymentFailureFragment extends BaseBottomSheetFragment implements PaymentFailurePopupViewModel.PaymentFailureInterface {

    private FragmentPaymentFailureBinding binding;
    private PaymentFailurePopupViewModel paymentFailurePopupViewModel;
    private PaymentFailureFragmentListener paymentFailureFragmentListener;

    public PaymentFailureFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            paymentFailureFragmentListener = (PaymentFailureFragmentListener) getActivity();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(true);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_failure, container, false);
        paymentFailurePopupViewModel = new PaymentFailurePopupViewModel(getActivity().getApplication());
        Bundle bundle = getArguments();
        paymentFailurePopupViewModel.initViewModel(getActivity(), this, bundle, BasePreference.getInstance(getActivity()));
        binding.setViewModel(paymentFailurePopupViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void vmShowProgress() {
        showProgress(getActivity());
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmOnPaymentRetry() {
        dismissProgress();
        dismissAllowingStateLoss();
        paymentFailureFragmentListener.onPaymentRetry();
    }

    @Override
    public void onClickOnCOD() {
        dismissProgress();
        dismissAllowingStateLoss();
        paymentFailureFragmentListener.onClickOnCOD();
    }

    @Override
    public void vmOnPopupClose() {
        dismissProgress();
        dismissAllowingStateLoss();
        paymentFailureFragmentListener.onPaymentFailureDismissDialog();

    }

    @Override
    public void codEnable(boolean isCODEligible) {
        binding.payWithCash.setVisibility(isCODEligible ? View.VISIBLE : View.GONE);
    }

    public interface PaymentFailureFragmentListener {
        void onPaymentRetry();

        void onClickOnCOD();

        void onPaymentFailureDismissDialog();
    }
}
