/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui.vm;


import com.google.gson.annotations.SerializedName;

public class PaypalPaymentRequestAmount {
    @SerializedName("total")
    private String total;
    @SerializedName("currency")
    private String currency;
    @SerializedName("details")
    private PaypalPaymentRequestDetails details;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public PaypalPaymentRequestDetails getDetails() {
        return details;
    }

    public void setDetails(PaypalPaymentRequestDetails details) {
        this.details = details;
    }

}