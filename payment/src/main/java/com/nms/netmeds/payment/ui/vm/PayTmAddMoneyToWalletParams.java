package com.nms.netmeds.payment.ui.vm;


public class PayTmAddMoneyToWalletParams {
    private String modeOfPayment;
    private double transactionAmt;
    private double orderAmt;
    private String paymentUserId;
    private String imageUrl;
    private String paytmUserToken;
    private String orderId;

    public PayTmAddMoneyToWalletParams() {
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public double getTransactionAmt() {
        return transactionAmt;
    }

    public void setTransactionAmt(double transactionAmt) {
        this.transactionAmt = transactionAmt;
    }

    public double getOrderAmt() {
        return orderAmt;
    }

    public void setOrderAmt(double orderAmt) {
        this.orderAmt = orderAmt;
    }

    public String getPaymentUserId() {
        return paymentUserId;
    }

    public void setPaymentUserId(String paymentUserId) {
        this.paymentUserId = paymentUserId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPaytmUserToken() {
        return paytmUserToken;
    }

    public void setPaytmUserToken(String paytmUserToken) {
        this.paytmUserToken = paytmUserToken;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}