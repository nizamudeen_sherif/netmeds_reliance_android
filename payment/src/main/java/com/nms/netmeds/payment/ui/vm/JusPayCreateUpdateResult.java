package com.nms.netmeds.payment.ui.vm;

import com.google.gson.annotations.SerializedName;

public class JusPayCreateUpdateResult {

    @SerializedName("id")
    private String id;
    @SerializedName("object")
    private String object;
    @SerializedName("object_reference_id")
    private String objectReferenceId;
    @SerializedName("mobile_country_code")
    private String mobileCountryCode;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("email_address")
    private String emailAddress;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("date_created")
    private String dateCreated;
    @SerializedName("last_updated")
    private String lastUpdated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getObjectReferenceId() {
        return objectReferenceId;
    }

    public void setObjectReferenceId(String objectReferenceId) {
        this.objectReferenceId = objectReferenceId;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(String mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
