package com.nms.netmeds.payment.ui.ecrytionViewModel;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

public class PaymentKeyResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private PaymentKeyResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PaymentKeyResult getResult() {
        return result;
    }

    public void setResult(PaymentKeyResult result) {
        this.result = result;
    }


}
