package com.nms.netmeds.payment.ui;

import android.os.Bundle;

public class PaypalActivity extends WebPageActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            initJavaScriptInterface(getWebView());
            loadURL(getIntent().getStringExtra("redirectUrl"));
        }
    }
}


