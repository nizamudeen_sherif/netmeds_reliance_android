/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.payment.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.Banner;
import com.nms.netmeds.base.utils.DeepLinkConstant;
import com.nms.netmeds.payment.R;

import java.util.List;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.BannerHolder> {
    private final List<Banner> offerList;
    private final Context mContext;
    private BannerAdapterListener listener;
    private static final String SLASH_SYMBOL = "/";


    BannerAdapter(Context mContext, List<Banner> offerList, BannerAdapterListener listener) {
        this.offerList = offerList;
        this.mContext = mContext;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BannerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_offer, parent, false);
        return new BannerHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final BannerHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Banner banner = offerList.get(position);
        if (mContext.getApplicationContext() != null) {
            Glide.with(mContext.getApplicationContext()).load(banner != null && banner.getImage() != null && !TextUtils.isEmpty(banner.getImage()) ? banner.getImage() : "").into(holder.imgOffer);
        }
        holder.tittle.setText(banner != null && banner.getTitle() != null && !TextUtils.isEmpty(banner.getTitle()) ? banner.getTitle() : "");
        holder.description.setText(banner != null && banner.getDescription() != null && !TextUtils.isEmpty(banner.getDescription()) ? banner.getDescription() : "");
        holder.subscribeButton.setText(banner != null && banner.getButtonTitle() != null && !TextUtils.isEmpty(banner.getButtonTitle()) ? banner.getButtonTitle() : "");
        holder.subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentNavigation(!TextUtils.isEmpty(banner.getUrl()) ? banner.getUrl() : "");
            }
        });

        holder.onBoardingImage.setVisibility(banner != null && banner.getUrl() != null && AppConstant.SUBSCRIPTION.equalsIgnoreCase(banner.getUrl()) ? View.VISIBLE : View.GONE);
        holder.onBoardingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.vmSubscriptionOnboardingActivity();
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    class BannerHolder extends RecyclerView.ViewHolder {
        private final ImageView imgOffer;
        private TextView tittle;
        private TextView description;
        private TextView subscribeButton;
        private FrameLayout onBoardingImage;

        BannerHolder(View itemView) {
            super(itemView);
            imgOffer = itemView.findViewById(R.id.img_offer);
            tittle = itemView.findViewById(R.id.tv_title);
            description = itemView.findViewById(R.id.tv_description);
            subscribeButton = itemView.findViewById(R.id.tv_subscripe);
            onBoardingImage = itemView.findViewById(R.id.on_boarding_icon);
        }
    }

    private void intentNavigation(String banner) {
        switch (banner) {
            case AppConstant.SUBSCRIPTION:
                if (!SubscriptionHelper.getInstance().isRefillSubscription()) {
                    SubscriptionHelper.getInstance().setSubscriptionFlag(false);
                    listener.initiateSubscription();
                }
                break;
            case AppConstant.CONSULTATION: {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(DeepLinkConstant.REDIRECT_TO_HISTORY, true);
                LaunchIntentManager.routeToActivity(mContext.getString(com.nms.netmeds.base.R.string.route_navigation_activity), intent, mContext);
                break;
            }
        }
    }

    public interface BannerAdapterListener {
        void initiateSubscription();

        void vmSubscriptionOnboardingActivity();
    }

}
