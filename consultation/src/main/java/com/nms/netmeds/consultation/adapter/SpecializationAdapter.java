/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.consultation.adapter;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.SpecialityListItemBinding;
import com.nms.netmeds.consultation.model.Speciality;
import com.nms.netmeds.consultation.vm.SpecializationAdapterViewModel;

import java.util.List;

public class SpecializationAdapter extends RecyclerView.Adapter<SpecializationAdapter.SpecialityHolder> implements SpecializationAdapterViewModel.SpecializationAdapterViewModelListener {
    private final List<Speciality> specialityList;
    private final Context context;
    private final SpecializationAdapterListener specializationAdapterListener;
    private final Application application;

    public SpecializationAdapter(Context context, Application application, List<Speciality> specialityList, SpecializationAdapterListener specializationAdapterListener) {
        this.context = context;
        this.application = application;
        this.specialityList = specialityList;
        this.specializationAdapterListener = specializationAdapterListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public SpecialityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SpecialityListItemBinding specialityListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.speciality_list_item, parent, false);
        return new SpecialityHolder(specialityListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull SpecialityHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Speciality speciality = specialityList.get(position);
        SpecializationAdapterViewModel specializationAdapterViewModel = new SpecializationAdapterViewModel(application);
        specializationAdapterViewModel.onDataAvailable(context, speciality, holder.specialityListItemBinding, this);
        holder.specialityListItemBinding.setViewModel(specializationAdapterViewModel);
    }

    @Override
    public int getItemCount() {
        return specialityList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void specializationClick(Speciality speciality) {
        for (Speciality specialization : specialityList) {
            if (specialization.getCode().equals(speciality.getCode()))
                specialization.setChecked(true);
            else
                specialization.setChecked(false);
        }
        notifyDataSetChanged();
        specializationAdapterListener.specializationSelection(speciality);
    }

    class SpecialityHolder extends RecyclerView.ViewHolder {
        private final SpecialityListItemBinding specialityListItemBinding;

        SpecialityHolder(SpecialityListItemBinding specialityListItemBinding) {
            super(specialityListItemBinding.getRoot());
            this.specialityListItemBinding = specialityListItemBinding;
        }
    }

    public interface SpecializationAdapterListener {
        void specializationSelection(Speciality speciality);
    }
}
