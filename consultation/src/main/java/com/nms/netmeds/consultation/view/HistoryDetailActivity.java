package com.nms.netmeds.consultation.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationUtil;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ActivityHistoryDetailBinding;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.LabTest;
import com.nms.netmeds.consultation.vm.HistoryDetailViewModel;

import java.lang.reflect.Type;
import java.util.List;

public class HistoryDetailActivity extends BaseUploadPrescription<HistoryDetailViewModel> implements HistoryDetailViewModel.HistoryDetailListener {
    private ActivityHistoryDetailBinding activityHistoryDetailBinding;
    private HistoryDetailViewModel historyDetailViewModel;
    private HistoryResult historyResult;
    private BasePreference basePreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        basePreference = BasePreference.getInstance(this);
        activityHistoryDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_history_detail);
        toolBarSetUp(activityHistoryDetailBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        intentValue();
        activityHistoryDetailBinding.setViewModel(onCreateViewModel());
        if (historyResult != null && !TextUtils.isEmpty(historyResult.getDoctorId()))
            historyDetailViewModel.getDoctorInfo(Integer.parseInt(historyResult.getDoctorId()));
    }

    @Override
    protected HistoryDetailViewModel onCreateViewModel() {
        historyDetailViewModel = ViewModelProviders.of(this).get(HistoryDetailViewModel.class);
        historyDetailViewModel.getDoctorInfoMutableLiveData().observe(this, new DoctorInfoObserver());
        historyDetailViewModel.init(this, historyDetailViewModel, historyResult, activityHistoryDetailBinding, this);
        onRetry(historyDetailViewModel);
        return historyDetailViewModel;
    }

    private void intentValue() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.CONSULTATION_HISTORY)
                && !TextUtils.isEmpty(getIntent().getStringExtra(IntentConstant.CONSULTATION_HISTORY))) {
            historyResult = new Gson().fromJson(getIntent().getStringExtra(IntentConstant.CONSULTATION_HISTORY), HistoryResult.class);
            if (historyResult != null && historyResult.isConsultationExpiry()) {
                // Web Engage - Chat consultation is completed
                WebEngageModel webEngageModel = new WebEngageModel();
                MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
                webEngageModel.setMStarUserDetails(customerDetails);
                webEngageModel.setCommonProperties();
                webEngageModel.setPatientIdentified("");
                if (historyResult.getSpeciality() != null && !TextUtils.isEmpty(historyResult.getSpeciality().getName())) {
                    webEngageModel.setSpeciality(historyResult.getSpeciality().getName());
                }
                if (!TextUtils.isEmpty(historyResult.getSymptoms()))
                    webEngageModel.setSymptoms(historyResult.getSymptoms());
                webEngageModel.setType("");
                if (!TextUtils.isEmpty(historyResult.getMode()))
                    webEngageModel.setMode(historyResult.getMode());
                if (!TextUtils.isEmpty(historyResult.getPrescriptionStatus()))
                    webEngageModel.setPrescriptionStatus(historyResult.getPrescriptionStatus());
                try {
                    if (historyResult.getTestJson() != null && !historyResult.getTestJson().isJsonNull()) {
                        Type type = new TypeToken<List<LabTest>>() {
                        }.getType();
                        List<LabTest> labTests = new Gson().fromJson(historyResult.getTestJson().getAsJsonObject().getAsJsonArray(this.getString(R.string.lab_tests)).toString(), type);
                        webEngageModel.setLabTestPrescribed(this.getResources().getString(R.string.txt_web_engage_consultation_yes));
                        webEngageModel.setLabTestName(ConsultationUtil.getLabTestName(labTests));
                    } else {
                        webEngageModel.setLabTestPrescribed(this.getResources().getString(R.string.txt_web_engage_consultation_No));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                webEngageModel.setFollowUpStatus(getResources().getString(R.string.txt_web_engage_consultation_No));
                WebEngageHelper.getInstance().chatConsultationIsCompletedEvent(webEngageModel, this);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            HistoryDetailActivity.this.finish();
            return true;
        }
        return false;
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmCallBackOnChat(Intent intent) {
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_chat_activity), intent, this);
    }

    @Override
    public void vmDownloadPrescription() {
        if (checkGalleryPermission())
            historyDetailViewModel.downloadPrescription();
        else
            requestPermission(PermissionConstants.DOWNLOAD_PERMISSION);
    }

    @Override
    public void vmCallBackOnHelp() {
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_need_help_activity), this);
    }

    private class DoctorInfoObserver implements Observer<DoctorListResponse> {

        @Override
        public void onChanged(@Nullable DoctorListResponse doctorInfoResponse) {
            historyDetailViewModel.onDoctorInfoDataAvailable(doctorInfoResponse);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (checkPermissionResponse(grantResults) && requestCode == PermissionConstants.DOWNLOAD_PERMISSION)
            historyDetailViewModel.downloadPrescription();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
