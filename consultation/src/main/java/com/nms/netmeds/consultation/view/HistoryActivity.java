package com.nms.netmeds.consultation.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ActivityHistoryBinding;
import com.nms.netmeds.consultation.model.HistoryResponse;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.consultation.vm.HistoryViewModel;
import com.webengage.sdk.android.WebEngage;

public class HistoryActivity extends BaseViewModelActivity<HistoryViewModel> implements HistoryViewModel.HistoryListener {

    private ActivityHistoryBinding historyBinding;
    private HistoryViewModel historyViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BasePreference.getInstance(this).isGuestCart()) {
            navigateToSignIn();
            return;
        }
        historyBinding = DataBindingUtil.setContentView(this, R.layout.activity_history);
        toolBarSetUp(historyBinding.toolBarCustomLayout.toolbar);
        initToolBar(historyBinding.toolBarCustomLayout.collapsingToolbar, getResources().getString(R.string.text_online_consultation));
        historyBinding.setViewModel(onCreateViewModel());
        /*Fcm token send to the server*/
        historyViewModel.tokenSendToServer();
    }


    // logout user forcefully
    public void logoutUserForceFully() {
        pushUnRegisterFromConsultation();
        BasePreference.getInstance(this).clear();
        BasePreference.getInstance(this).setClearPreference(true);
        WebEngageHelper.getInstance().getUser().logout();
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
        navigateToSignIn();
    }

    private void pushUnRegisterFromConsultation() {
        if(TextUtils.isEmpty(BasePreference.getInstance(this).getJustDocUserResponse())) return;
        boolean isConnected = NetworkUtils.isConnected(this);
        if (isConnected) {
            MStarCustomerDetails mStarCustomerDetails = new Gson().fromJson(BasePreference.getInstance(this).getCustomerDetails(), MStarCustomerDetails.class);
            SendFcmTokenRequest sendFcmTokenRequest = new SendFcmTokenRequest();
            sendFcmTokenRequest.setEmail(mStarCustomerDetails != null ? !TextUtils.isEmpty(mStarCustomerDetails.getEmail()) ? mStarCustomerDetails.getEmail() : "" : "");
            sendFcmTokenRequest.setToken(BasePreference.getInstance(this).getFcmToken());
            sendFcmTokenRequest.setUserType(ConsultationConstant.ROLE_USER);
            if (!TextUtils.isEmpty(BasePreference.getInstance(this).getFcmToken()))
                ConsultationServiceManager.getInstance().pushUnRegisterFromConsultationServer(sendFcmTokenRequest, BasePreference.getInstance(this));
        }
    }

    @Override
    protected HistoryViewModel onCreateViewModel() {
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        historyViewModel.getHistoryResponseMutableLiveData().observe(this, new HistoryObserver());
        historyViewModel.getLoginResponseMutableLiveData().observe(this, new LoginObserver());
        historyViewModel.getLoganMutableLiveData().observe(this, new LoganTokenObserver());
        historyViewModel.init(this, this, historyBinding);
        onRetry(historyViewModel);
        return historyViewModel;
    }


    private void init() {
        BasePreference basePreference = BasePreference.getInstance(this);
        JustDocUserResponse consultationLoginResult = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
        String jusDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
        if (!TextUtils.isEmpty(jusDocToken)) {
            // Web Engage - Login for consultation
            WebEngageModel webEngageModel = new WebEngageModel();
            MStarCustomerDetails mStarCustomerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
            webEngageModel.setMStarUserDetails(mStarCustomerDetails);
            webEngageModel.setCommonProperties();
            WebEngageHelper.getInstance().loginForConsultationEvent(webEngageModel, this);
            historyViewModel.getHistory(consultationLoginResult, 1);
        } else {
            historyViewModel.doLoginInJustDoc(basePreference.getLoganSession());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    public void vmCallbackOnChatDoctor() {
        Intent intent = new Intent();
        if (historyViewModel != null)
            intent.putExtra(IntentConstant.INTENT_FIRST_CONSULTATION, historyViewModel.isConsultationHistoryExist());
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_chat_activity), this);
    }

    @Override
    public void vmCallbackOnHistoryDetail(HistoryResult historyResult) {
        String historyResultString = new Gson().toJson(historyResult);
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.CONSULTATION_HISTORY, historyResultString);
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_history_detail_activity), intent, this);
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,historyBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,historyBinding.toolBarCustomLayout.progressBar);
    }


    private class HistoryObserver implements Observer<HistoryResponse> {

        @Override
        public void onChanged(@Nullable HistoryResponse consultationHistoryResponse) {
            historyViewModel.onHistoryDataAvailable(consultationHistoryResponse);
        }
    }

    private class LoginObserver implements Observer<LoginResponse> {

        @Override
        public void onChanged(@Nullable LoginResponse loginResponse) {
            historyViewModel.onUserDataAvailable(loginResponse);
        }
    }

    private class LoganTokenObserver implements Observer<LoganResponse> {

        @Override
        public void onChanged(@Nullable LoganResponse loganResponse) {
            historyViewModel.onLoganTokenDataAvailable(loganResponse);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_consultation, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.download_prescription).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            HistoryActivity.this.finish();
        } else if (item.getItemId() == R.id.help) {
            LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_need_help_activity), this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void navigateToSignIn() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LaunchIntentManager.routeToActivity(this.getString(R.string.route_sign_in_activity), intent, this);
        HistoryActivity.this.finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_CONSULTATION_PAGE);
    }

    @Override
    public void forceLogout() {
        logoutUserForceFully();
        return;
    }

    @Override
    public Context getContext() {
        return this;
    }
}
