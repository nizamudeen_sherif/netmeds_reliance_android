package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {
    @SerializedName("sessionId")
    private String sessionId;
    @SerializedName("source")
    private String source;
    @SerializedName("medium")
    private String medium;
    @SerializedName("appversion")
    private String appversion;
    @SerializedName("customer_id")
    private String customer_id;

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getAppversion() {
        return appversion;
    }

    public void setAppversion(String appversion) {
        this.appversion = appversion;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
