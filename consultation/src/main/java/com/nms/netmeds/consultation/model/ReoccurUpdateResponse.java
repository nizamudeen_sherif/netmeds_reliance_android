package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class ReoccurUpdateResponse {
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
