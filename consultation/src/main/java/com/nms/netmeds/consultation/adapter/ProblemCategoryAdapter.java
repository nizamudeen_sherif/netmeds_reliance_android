/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.consultation.adapter;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ProblemCategoryListItemBinding;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.vm.ProblemCategoryListItemViewModel;

import java.util.List;

public class ProblemCategoryAdapter extends RecyclerView.Adapter<ProblemCategoryAdapter.ProblemCategoryHolder> implements ProblemCategoryListItemViewModel.ProblemCategoryListItemListener {
    private final List<Options> problemCategoryList;
    private final Application application;
    private final Context context;
    private final ProblemCategoryAdapterListener problemCategoryAdapterListener;

    public ProblemCategoryAdapter(Context context, List<Options> problemCategoryList, Application application, ProblemCategoryAdapterListener problemCategoryAdapterListener) {
        this.context = context;
        this.problemCategoryList = problemCategoryList;
        this.application = application;
        this.problemCategoryAdapterListener = problemCategoryAdapterListener;
    }

    @NonNull
    @Override
    public ProblemCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProblemCategoryListItemBinding problemCategoryListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.problem_category_list_item, parent, false);
        return new ProblemCategoryHolder(problemCategoryListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProblemCategoryHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Options problemCategory = problemCategoryList.get(position);
        ProblemCategoryListItemViewModel problemCategoryListItemViewModel = new ProblemCategoryListItemViewModel(application);
        problemCategoryListItemViewModel.onDataAvailable(context, problemCategory, holder.problemCategoryListItemBinding, this);
        holder.problemCategoryListItemBinding.setViewModel(problemCategoryListItemViewModel);
        holder.problemCategoryListItemBinding.horizontalDivider.setVisibility(position == problemCategoryList.size() - 1 ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return problemCategoryList.size();
    }

    @Override
    public void selectedProblem(Options problemCategory) {
        problemCategoryAdapterListener.paSelectedProblem(problemCategory);
    }

    class ProblemCategoryHolder extends RecyclerView.ViewHolder {
        private final ProblemCategoryListItemBinding problemCategoryListItemBinding;

        ProblemCategoryHolder(ProblemCategoryListItemBinding problemCategoryListItemBinding) {
            super(problemCategoryListItemBinding.getRoot());
            this.problemCategoryListItemBinding = problemCategoryListItemBinding;
        }
    }

    public interface ProblemCategoryAdapterListener {
        void paSelectedProblem(Options problemCategory);
    }
}
