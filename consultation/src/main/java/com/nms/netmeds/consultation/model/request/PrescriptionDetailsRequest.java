package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * @author vikas.grover
 * View Prescription Request Model
 */
public class PrescriptionDetailsRequest {

    @SerializedName("userId")
    private String userId;
    @SerializedName("id")
    private String id;
    @SerializedName("image")
    private boolean image;


    public PrescriptionDetailsRequest(String id, String userId, boolean image) {
        this.id = id;
        this.userId = userId;
        this.image = image;
    }
}

