package com.nms.netmeds.consultation.adapter;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.AdapterHistoryBinding;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.vm.HistoryAdapterViewModel;
import com.nms.netmeds.consultation.vm.HistoryViewModel;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {
    private List<HistoryResult> consultationHistoryList;
    private final Context context;
    private final HistoryViewModel.HistoryListener historyListener;
    private final Application application;

    public HistoryAdapter(Context context, HistoryViewModel.HistoryListener historyListener, List<HistoryResult> consultationHistoryList, Application application) {
        this.context = context;
        this.historyListener = historyListener;
        this.consultationHistoryList = consultationHistoryList;
        this.application = application;
    }

    @NonNull
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterHistoryBinding historyBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_history, parent, false);
        return new HistoryHolder(historyBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryHolder holder, int position) {
        HistoryResult consultationHistory = consultationHistoryList.get(position);
        HistoryAdapterViewModel historyAdapterViewModel = new HistoryAdapterViewModel(application);
        historyAdapterViewModel.onDataAvailable(context, historyListener, consultationHistory, holder.historyBinding);
        holder.historyBinding.setViewModel(historyAdapterViewModel);
        if (position % 2 == 0) {
            holder.historyBinding.lytHistoryItem.setBackgroundColor(context.getResources().getColor(R.color.colorOpacityBlueGrey));
        } else {
            holder.historyBinding.lytHistoryItem.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return consultationHistoryList.size();
    }

    class HistoryHolder extends RecyclerView.ViewHolder {
        private final AdapterHistoryBinding historyBinding;

        HistoryHolder(AdapterHistoryBinding historyBinding) {
            super(historyBinding.getRoot());
            this.historyBinding = historyBinding;
        }
    }

    public void updateHistoryAdapter(List<HistoryResult> updateList) {
        this.consultationHistoryList = updateList;
        notifyDataSetChanged();
    }
}
