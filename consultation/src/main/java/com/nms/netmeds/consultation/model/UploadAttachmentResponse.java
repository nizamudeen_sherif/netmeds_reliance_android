package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class UploadAttachmentResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("object")
    private UploadAttachmentObject uploadAttachmentObject;
    private String uploadUrl;
    private String fileName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UploadAttachmentObject getUploadAttachmentObject() {
        return uploadAttachmentObject;
    }

    public void setUploadAttachmentObject(UploadAttachmentObject uploadAttachmentObject) {
        this.uploadAttachmentObject = uploadAttachmentObject;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
