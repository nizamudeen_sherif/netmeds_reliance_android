package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class ChatHistoryResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("result")
    private ChatHistoryResult chatHistoryResult;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ChatHistoryResult getChatHistoryResult() {
        return chatHistoryResult;
    }

    public void setChatHistoryResult(ChatHistoryResult chatHistoryResult) {
        this.chatHistoryResult = chatHistoryResult;
    }
}
