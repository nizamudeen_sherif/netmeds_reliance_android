package com.nms.netmeds.consultation.model;

import java.io.Serializable;

public class Medicine implements Serializable {

    private String id;
    private String dosage;
    private int doseId;
    private String edPill;
    private int duration;
    private String medicine;
    private boolean showMeds;
    private boolean isChronic;
    private String medicineId;
    private boolean showDosage;
    private String genericName;
    private int maxDuration;
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public int getDoseId() {
        return doseId;
    }

    public void setDoseId(int doseId) {
        this.doseId = doseId;
    }

    public String getEdPill() {
        return edPill;
    }

    public void setEdPill(String edPill) {
        this.edPill = edPill;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public boolean isShowMeds() {
        return showMeds;
    }

    public void setShowMeds(boolean showMeds) {
        this.showMeds = showMeds;
    }

    public boolean isChronic() {
        return isChronic;
    }

    public void setChronic(boolean chronic) {
        isChronic = chronic;
    }

    public String getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(String medicineId) {
        this.medicineId = medicineId;
    }

    public boolean isShowDosage() {
        return showDosage;
    }

    public void setShowDosage(boolean showDosage) {
        this.showDosage = showDosage;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

}
