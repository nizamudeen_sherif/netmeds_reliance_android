package com.nms.netmeds.consultation.model;

import java.io.Serializable;
import java.util.ArrayList;

public class MedicineInfoResponse implements Serializable {

    private ArrayList<Medicine> medicines;
    private String diagnosis;
    private String notes;
    private String symptoms;
    private String allergies;
    private String doctorId;
    private String id;
    private String userId;
    private String status;
    private String followUpDate;
    private String creationDate;
    private String speciality;
    private String doctorName;
    private String docSpec;
    private String assessment;
    private ArrayList<LabTest> labTests;
    private DoctorDegree doctorDegrees;
    private String prescImageBase64;


    public String getPrescImageBase64() {
        return prescImageBase64;
    }

    public void setPrescImageBase64(String prescImageBase64) {
        this.prescImageBase64 = prescImageBase64;
    }


    public DoctorDegree getDoctorDegrees() {
        return doctorDegrees;
    }

    public void setDoctorDegrees(DoctorDegree doctorDegrees) {
        this.doctorDegrees = doctorDegrees;
    }

    public CustomerData getCustomer_data() {
        return customer_data;
    }

    public void setCustomer_data(CustomerData customer_data) {
        this.customer_data = customer_data;
    }

    private CustomerData customer_data;


    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDocSpec() {
        return docSpec;
    }

    public void setDocSpec(String docSpec) {
        this.docSpec = docSpec;
    }


    public ArrayList<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(ArrayList<Medicine> medicines) {
        this.medicines = medicines;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(String followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getAssessment() {
        return assessment;
    }

    public void setAssessment(String assessment) {
        this.assessment = assessment;
    }

    public ArrayList<LabTest> getLabTests() {
        return labTests;
    }

    public void setLabTests(ArrayList<LabTest> labTests) {
        this.labTests = labTests;
    }
}
