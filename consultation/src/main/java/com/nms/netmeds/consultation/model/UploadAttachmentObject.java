package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class UploadAttachmentObject {
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
