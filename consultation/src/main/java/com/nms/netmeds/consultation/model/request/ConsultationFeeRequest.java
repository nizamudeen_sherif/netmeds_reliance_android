package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class ConsultationFeeRequest {
    @SerializedName("speciality")
    private String speciality;
    @SerializedName("userId")
    private String userId;

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
