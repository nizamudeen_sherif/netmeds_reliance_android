package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class Payment {
    @SerializedName("status")
    private String status;
    @SerializedName("id")
    private String id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
