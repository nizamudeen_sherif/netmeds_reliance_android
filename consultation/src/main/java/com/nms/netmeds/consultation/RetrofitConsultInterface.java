package com.nms.netmeds.consultation;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.SendFcmTokenResponse;
import com.nms.netmeds.consultation.model.ApplyCouponResponse;
import com.nms.netmeds.consultation.model.ChatHistoryResponse;
import com.nms.netmeds.consultation.model.ConsultationFeeResponse;
import com.nms.netmeds.consultation.model.ConsultationUpdateResponse;
import com.nms.netmeds.consultation.model.CreateChatResponse;
import com.nms.netmeds.consultation.model.CreateConsultationResponse;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.GetAllCouponResponse;
import com.nms.netmeds.consultation.model.HistoryRequest;
import com.nms.netmeds.consultation.model.HistoryResponse;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.consultation.model.PaymentCreationResponse;
import com.nms.netmeds.consultation.model.ReoccurUpdateResponse;
import com.nms.netmeds.consultation.model.SpecialityResponse;
import com.nms.netmeds.consultation.model.UploadAttachmentResponse;
import com.nms.netmeds.consultation.model.request.ApplyCouponRequest;
import com.nms.netmeds.consultation.model.request.ChatHistoryRequest;
import com.nms.netmeds.consultation.model.request.ConsultationFeeRequest;
import com.nms.netmeds.consultation.model.request.ConsultationUpdateRequest;
import com.nms.netmeds.consultation.model.request.CreateChatRequest;
import com.nms.netmeds.consultation.model.request.CreateConsultationRequest;
import com.nms.netmeds.consultation.model.request.DoctorInfoRequest;
import com.nms.netmeds.consultation.model.request.DoctorListRequest;
import com.nms.netmeds.consultation.model.request.LoginRequest;
import com.nms.netmeds.consultation.model.request.PaymentCreationRequest;
import com.nms.netmeds.consultation.model.request.PrescriptionDetailsRequest;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;
import com.nms.netmeds.consultation.model.request.ReoccurUpdateRequest;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitConsultInterface {

    @POST(ConsultAPIPathConstant.HISTORY)
    Call<HistoryResponse> verifyUser(@HeaderMap Map<String, String> header, @Body HistoryRequest verifyUserRequest, @Query("page") int pageIndex, @Query("limit") int pageLimit);

    @POST(ConsultAPIPathConstant.DO_LOGIN)
    Call<LoginResponse> doLogin(@Body LoginRequest consultationLoginRequest);

    @POST(ConsultAPIPathConstant.DOCTOR_INFO)
    Call<DoctorListResponse> doctorInfo(@Body DoctorInfoRequest doctorInfoRequest);

    @POST(ConsultAPIPathConstant.CREATE_CHAT)
    Call<CreateChatResponse> createChat(@Body CreateChatRequest createChatRequest);

    @POST(ConsultAPIPathConstant.UPDATE_CONSULTATION)
    Call<ConsultationUpdateResponse> updateConsultation(@HeaderMap Map<String, String> header, @Body ConsultationUpdateRequest consultationUpdateRequest);

    @GET(ConsultAPIPathConstant.GET_ALL_SPECIALITY)
    Call<SpecialityResponse> getAllSpeciality();

    @POST(ConsultAPIPathConstant.DOCTOR_INFO)
    Call<DoctorListResponse> doctorList(@Body DoctorListRequest doctorListRequest);

    @POST(ConsultAPIPathConstant.CREATE_CONSULTATION)
    Call<CreateConsultationResponse> createConsultation(@HeaderMap Map<String, String> header, @Body CreateConsultationRequest createConsultationRequest);

    @POST(ConsultAPIPathConstant.CONSULTATION_FEE)
    Call<ConsultationFeeResponse> consultationFee(@Body ConsultationFeeRequest consultationFeeRequest);

    @GET(ConsultAPIPathConstant.GET_ALL_COUPON)
    Call<GetAllCouponResponse> allCoupon(@HeaderMap Map<String, String> header);

    @POST(ConsultAPIPathConstant.APPLY_COUPON)
    Call<ApplyCouponResponse> applyCoupon(@Body ApplyCouponRequest applyCouponRequest);

    @POST(ConsultAPIPathConstant.UPLOAD_ATTACHMENT)
    Call<UploadAttachmentResponse> attachmentUpload(@HeaderMap Map<String, String> header, @Body RequestBody requestBody);

    @POST(ConsultAPIPathConstant.PAYMENT_CREATION)
    Call<PaymentCreationResponse> paymentCreation(@HeaderMap Map<String, String> header, @Body PaymentCreationRequest paymentCreationRequest);

    @POST(ConsultAPIPathConstant.REOCCUR_UPDATE)
    Call<ReoccurUpdateResponse> reoccurUpdate(@HeaderMap Map<String, String> header, @Body ReoccurUpdateRequest reoccurUpdateRequest);

    @POST(ConsultAPIPathConstant.CHAT_HISTORY)
    Call<ChatHistoryResponse> chatHistory(@HeaderMap Map<String, String> header, @Body ChatHistoryRequest chatHistoryRequest);

    @GET(ConsultAPIPathConstant.C_MSTAR_LOGAN_TOKEN)
    Call<LoganResponse> loganToken(@HeaderMap Map<String, String> header);

    @POST(ConsultAPIPathConstant.PUSH_REGISTER)
    Call<SendFcmTokenResponse> sendFcmToken(@HeaderMap Map<String, String> header, @Body SendFcmTokenRequest request);

    @POST(ConsultAPIPathConstant.PRESCRIPTION_DETAILS)
    Call<JsonArray> getPrescriptionDetails(@HeaderMap Map<String, String> header, @Body PrescriptionDetailsRequest request, @Query(ConsultationConstant.FOLLOWUP) String followup);

    @POST(ConsultAPIPathConstant.CONSULTATION_RATING)
    Call<JsonObject> submitRatingToService(@HeaderMap Map<String, String> header, @Body RatingSubmitRequest request);

    @GET(ConsultAPIPathConstant.PATIENT_DETAILS)
    Call<CustomerResponse> patientDetails();

    @GET(ConsultAPIPathConstant.CONSULTATION_BANNER)
    Call<JsonObject> getBanner();

    @POST(ConsultAPIPathConstant.ORDER_FROM_PRESCRIPTION)
    Call<JsonObject> orderFromPrescription(@Body JsonObject request);

    @POST(ConsultAPIPathConstant.CONSULTATION_PAYMENT_GATEWAY_LIST)
    Call<MStarBasicResponseTemplateModel> getConsultationPaymentList(@Body JsonObject request, @HeaderMap Map<String, String> header);

    @POST(ConsultAPIPathConstant.PUSH_UN_REGISTER)
    Call<SendFcmTokenResponse> pushUnRegisterFromConsultation(@HeaderMap Map<String, String> header, @Body SendFcmTokenRequest request);
}
