package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatHistoryResult {
    @SerializedName("message")
    private String message;
    @SerializedName("object")
    private List<Message> chatHistoryList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Message> getChatHistoryList() {
        return chatHistoryList;
    }

    public void setChatHistoryList(List<Message> chatHistoryList) {
        this.chatHistoryList = chatHistoryList;
    }
}
