package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class ConsultationUpdateResult {
    @SerializedName("id")
    private String consultationId;
    @SerializedName("speciality")
    private String speciality;
    @SerializedName("mode")
    private String mode;
    @SerializedName("reoccur")
    private boolean reoccur;
    @SerializedName("message")
    private String message;

    public String getConsultationId() {
        return consultationId;
    }

    public void setConsultationId(String consultationId) {
        this.consultationId = consultationId;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isReoccur() {
        return reoccur;
    }

    public void setReoccur(boolean reoccur) {
        this.reoccur = reoccur;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
