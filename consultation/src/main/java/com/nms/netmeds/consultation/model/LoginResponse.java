package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;
import com.nms.netmeds.base.model.JustDocUserResponse;

public class LoginResponse extends BaseResponse {

    @SerializedName("result")
    private JustDocUserResponse result;

    public JustDocUserResponse getResult() {
        return result;
    }

    public void setResult(JustDocUserResponse result) {
        this.result = result;
    }
}
