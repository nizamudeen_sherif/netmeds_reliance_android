package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class ReoccurPayload {
    @SerializedName("id")
    private String id;
    @SerializedName("reoccurStatus")
    private boolean reoccurStatus;
    @SerializedName("speciality")
    private String speciality;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isReoccurStatus() {
        return reoccurStatus;
    }

    public void setReoccurStatus(boolean reoccurStatus) {
        this.reoccurStatus = reoccurStatus;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
}
