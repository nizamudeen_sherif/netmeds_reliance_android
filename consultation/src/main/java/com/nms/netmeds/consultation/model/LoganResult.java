package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class LoganResult {

    @SerializedName(value = "sessionToken", alternate = "logan_session_id")
    private String sessionToken;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

}
