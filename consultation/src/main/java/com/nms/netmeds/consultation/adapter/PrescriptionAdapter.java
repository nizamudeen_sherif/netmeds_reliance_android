package com.nms.netmeds.consultation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.consultation.ConsultationUtil;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.PrescriptionItemLayoutBinding;
import com.nms.netmeds.consultation.model.Medicine;

import java.util.ArrayList;

public class PrescriptionAdapter extends ArrayAdapter<Medicine> {


    private ArrayList<Medicine> medicineArrayList;
    private Context context;


    public PrescriptionAdapter(Context context, ArrayList<Medicine> medicines) {
        super(context, 0, medicines);
        this.medicineArrayList = medicines;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        PrescriptionItemLayoutBinding binding = DataBindingUtil.getBinding(convertView);
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.prescription_item_layout, parent, false);

        }
        Medicine medicine = medicineArrayList.get(position);
        binding.setMedicine(medicine);
        boolean isEveryDay =  ConsultationUtil.isEveryday(medicine);
        binding.textDosage.setVisibility(isEveryDay  ? View.GONE : View.VISIBLE);
        binding.tablets1.setVisibility(isEveryDay ? View.VISIBLE : View.GONE);
        binding.tablets2.setVisibility(isEveryDay ? View.VISIBLE : View.GONE);
        binding.tablets3.setVisibility(isEveryDay ? View.VISIBLE : View.GONE);
        binding.executePendingBindings();
        return binding.getRoot();
    }

    @Override
    public int getCount() {
        return medicineArrayList.size();
    }

    @Override
    public Medicine getItem(int position) {
        return medicineArrayList.get(position);
    }
}
