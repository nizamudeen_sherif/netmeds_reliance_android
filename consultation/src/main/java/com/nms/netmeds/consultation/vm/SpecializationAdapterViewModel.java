package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.SpecialityListItemBinding;
import com.nms.netmeds.consultation.model.Speciality;

public class SpecializationAdapterViewModel extends AppViewModel {
    private Speciality speciality;
    private SpecializationAdapterViewModelListener specializationAdapterViewModelListener;

    public SpecializationAdapterViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, Speciality speciality, SpecialityListItemBinding specialityListItemBinding, SpecializationAdapterViewModelListener adapterViewModelListener) {
        this.speciality = speciality;
        this.specializationAdapterViewModelListener = adapterViewModelListener;
        if (speciality.isChecked()) {
            specialityListItemBinding.specialityCheck.setImageResource(R.drawable.ic_done_active);
            specialityListItemBinding.textSpeciality.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
        } else {
            specialityListItemBinding.specialityCheck.setImageResource(android.R.color.transparent);
            specialityListItemBinding.textSpeciality.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Regular.ttf"));
        }
    }

    public String specialityName() {
        return speciality != null && !TextUtils.isEmpty(speciality.getName()) ? speciality.getName() : "";
    }

    public void onSpecializationClick() {
        specializationAdapterViewModelListener.specializationClick(speciality);
    }

    public interface SpecializationAdapterViewModelListener {
        void specializationClick(Speciality speciality);
    }
}
