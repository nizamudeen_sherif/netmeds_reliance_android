package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DoctorInformation implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private String id;
    @SerializedName("degrees")
    private DoctorDegree doctorDegree;
    @SerializedName("experience")
    private String experience;
    @SerializedName("about")
    private String about;
    @SerializedName("gender")
    private String gender;
    @SerializedName("rating")
    private String rating;
    @SerializedName("image")
    private String image;
    @SerializedName("specialityTitle")
    private String specialityTitle;
    @SerializedName("feedback")
    private String feedBack;
    @SerializedName("language")
    private Language language;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DoctorDegree getDoctorDegree() {
        return doctorDegree;
    }

    public void setDoctorDegree(DoctorDegree doctorDegree) {
        this.doctorDegree = doctorDegree;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSpecialityTitle() {
        return specialityTitle;
    }

    public void setSpecialityTitle(String specialityTitle) {
        this.specialityTitle = specialityTitle;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getFeedBack() {
        return feedBack;
    }

    public void setFeedBack(String feedBack) {
        this.feedBack = feedBack;
    }
}
