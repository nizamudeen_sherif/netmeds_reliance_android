/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */


package com.nms.netmeds.consultation.adapter;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.FollowUpInChatBinding;
import com.nms.netmeds.consultation.databinding.InflateOrderMedicineBinding;
import com.nms.netmeds.consultation.databinding.InflateRatingViewBinding;
import com.nms.netmeds.consultation.databinding.InflatorDoctorListBinding;
import com.nms.netmeds.consultation.databinding.InflatorLeftChatItemBinding;
import com.nms.netmeds.consultation.databinding.InflatorNotificationBinding;
import com.nms.netmeds.consultation.databinding.InflatorPaymentPlanBinding;
import com.nms.netmeds.consultation.databinding.InflatorRightChatItemBinding;
import com.nms.netmeds.consultation.databinding.InflatorTypingViewBinding;
import com.nms.netmeds.consultation.databinding.LayoutHeaderBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.vm.ChatLeftViewModel;
import com.nms.netmeds.consultation.vm.ChatRightViewModel;
import com.nms.netmeds.consultation.vm.DoctorListViewModel;
import com.nms.netmeds.consultation.vm.FollowUpInChatViewModel;
import com.nms.netmeds.consultation.vm.LoaderViewModel;
import com.nms.netmeds.consultation.vm.MedicineLabViewModel;
import com.nms.netmeds.consultation.vm.NotificationViewModel;
import com.nms.netmeds.consultation.vm.PaymentPlanViewModel;
import com.nms.netmeds.consultation.vm.RatingViewModel;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Message> messageList;
    private final Context context;
    private final MessageAdapterListener messageAdapterListener;
    private final Application application;
    private MedicineInfoResponse medicineInfoResponse;
    private String intentFrom;


    public MessageAdapter(Context context, List<Message> messageList, MessageAdapterListener messageAdapterListener, Application application) {
        this.messageList = messageList;
        this.context = context;
        this.messageAdapterListener = messageAdapterListener;
        this.application = application;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ConsultationConstant.HEADER:
                LayoutHeaderBinding headerBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_header, parent, false);
                return new ChatHeaderHolder(headerBinding);
            case ConsultationConstant.DOCTOR:
                InflatorLeftChatItemBinding leftChatItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_left_chat_item, parent, false);
                return new ChatLeftHolder(leftChatItemBinding);
            case ConsultationConstant.USER:
                InflatorRightChatItemBinding rightChatItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_right_chat_item, parent, false);
                return new ChatRightHolder(rightChatItemBinding);
            case ConsultationConstant.LIST:
                InflatorDoctorListBinding doctorListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_doctor_list, parent, false);
                return new DoctorHolder(doctorListBinding);
            case ConsultationConstant.PAYMENT_PLAN:
                InflatorPaymentPlanBinding paymentPlanBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_payment_plan, parent, false);
                return new PaymentPlanHolder(paymentPlanBinding);
            case ConsultationConstant.NOTIFICATION:
                InflatorNotificationBinding notificationBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_notification, parent, false);
                return new NotificationHolder(notificationBinding);
            case ConsultationConstant.TYPING_VIEW:
                InflatorTypingViewBinding typingViewBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_typing_view, parent, false);
                return new TypingViewHolder(typingViewBinding);
            case ConsultationConstant.MEDICINE_VIEW:
            case ConsultationConstant.LAB_TEST_VIEW:
                InflateOrderMedicineBinding orderMedicineBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflate_order_medicine, parent, false);
                return new MedicineOrderViewHolder(orderMedicineBinding);
            case ConsultationConstant.RATING_VIEW:
                InflateRatingViewBinding ratingViewBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflate_rating_view, parent, false);
                return new RatingViewHolder(ratingViewBinding);
            case ConsultationConstant.FOLLOWUP_VIEW:
                FollowUpInChatBinding followUpInChatBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.follow_up_in_chat, parent, false);
                return new FollowUpInChatHolder(followUpInChatBinding);
            default:
                InflatorLeftChatItemBinding chatItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.inflator_left_chat_item, parent, false);
                return new ChatLeftHolder(chatItemBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message message = messageList.get(position);
        switch (getItemViewType(position)) {
            case ConsultationConstant.DOCTOR:
                ChatLeftHolder chatLeftHolder = (ChatLeftHolder) holder;
                ChatLeftViewModel chatLeftViewModel = new ChatLeftViewModel(application);
                chatLeftViewModel.onDataAvailable(context, chatLeftHolder.leftChatItemBinding, message, messageAdapterListener, position);
                chatLeftHolder.leftChatItemBinding.setViewModel(chatLeftViewModel);
                break;
            case ConsultationConstant.USER:
                ChatRightHolder chatRightHolder = (ChatRightHolder) holder;
                ChatRightViewModel chatRightViewModel = new ChatRightViewModel(application);
                chatRightViewModel.onDataAvailable(context, chatRightHolder.rightChatItemBinding, message, messageAdapterListener, position);
                chatRightHolder.rightChatItemBinding.setViewModel(chatRightViewModel);
                break;
            case ConsultationConstant.LIST:
                DoctorHolder doctorHolder = (DoctorHolder) holder;
                DoctorListViewModel doctorListViewModel = new DoctorListViewModel(application);
                doctorListViewModel.onDataAvailable(context, doctorHolder.doctorListBinding, message, messageAdapterListener);
                doctorHolder.doctorListBinding.setViewModel(doctorListViewModel);
                break;
            case ConsultationConstant.PAYMENT_PLAN:
                PaymentPlanHolder paymentPlanHolder = (PaymentPlanHolder) holder;
                PaymentPlanViewModel paymentPlanViewModel = new PaymentPlanViewModel(application);
                paymentPlanViewModel.onDataAvailable(context, paymentPlanHolder.paymentPlanBinding, message, messageAdapterListener);
                paymentPlanHolder.paymentPlanBinding.setViewModel(paymentPlanViewModel);
                break;
            case ConsultationConstant.TYPING_VIEW:
                TypingViewHolder typingHolder = (TypingViewHolder) holder;
                LoaderViewModel loaderViewModel = new LoaderViewModel(application);
                loaderViewModel.onDataAvailable(messageAdapterListener);
                typingHolder.typingViewBinding.setViewModel(loaderViewModel);
                break;
            case ConsultationConstant.NOTIFICATION:
                NotificationHolder notificationHolder = (NotificationHolder) holder;
                NotificationViewModel notificationViewModel = new NotificationViewModel(application);
                notificationViewModel.onDataAvailable(message, notificationHolder.notificationBinding, messageAdapterListener);
                notificationHolder.notificationBinding.setViewModel(notificationViewModel);
                break;
            case ConsultationConstant.MEDICINE_VIEW:
                MedicineOrderViewHolder medicineOrderViewHolder = (MedicineOrderViewHolder) holder;
                MedicineLabViewModel medicineInfoViewModel = new MedicineLabViewModel(application, medicineInfoResponse, true);
                medicineInfoViewModel.onDoctorInfoDataAvailable(context, message, medicineOrderViewHolder.inflateOrderMedicineBinding, messageAdapterListener);
                medicineOrderViewHolder.inflateOrderMedicineBinding.setViewModel(medicineInfoViewModel);
                medicineInfoViewModel.setIntentFrom(getIntentFrom());
                break;
            case ConsultationConstant.LAB_TEST_VIEW:
                MedicineOrderViewHolder labTestViewHolder = (MedicineOrderViewHolder) holder;
                MedicineLabViewModel labTestInfoViewModel = new MedicineLabViewModel(application, medicineInfoResponse, false);
                labTestInfoViewModel.onDoctorInfoDataAvailable(context, message, labTestViewHolder.inflateOrderMedicineBinding, messageAdapterListener);
                labTestViewHolder.inflateOrderMedicineBinding.setViewModel(labTestInfoViewModel);
                labTestInfoViewModel.setIntentFrom(getIntentFrom());
                break;
            case ConsultationConstant.RATING_VIEW:
                RatingViewHolder ratingViewHolder = (RatingViewHolder) holder;
                RatingViewModel ratingViewModel = new RatingViewModel(application, messageAdapterListener);
                ratingViewModel.onRatingInfoDataAvailable(context, message, ratingViewHolder.inflateRatingViewBinding);
                ratingViewHolder.inflateRatingViewBinding.setViewModel(ratingViewModel);
                break;
            case ConsultationConstant.FOLLOWUP_VIEW:
                FollowUpInChatHolder followUpInChatHolder = (FollowUpInChatHolder) holder;
                FollowUpInChatViewModel followUpInChatViewModel = new FollowUpInChatViewModel(application);
                followUpInChatViewModel.onDataAvailable(context, message, followUpInChatHolder.followUpInChatBinding);
                followUpInChatHolder.followUpInChatBinding.setViewModel(followUpInChatViewModel);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        Message message = messageList.get(position);
        switch (message.getFromRole()) {
            case ConsultationConstant.ROLE_HEADER:
                return ConsultationConstant.HEADER;
            case ConsultationConstant.ROLE_DOCTOR:
            case ConsultationConstant.ROLE_BOT:
                return ConsultationConstant.DOCTOR;
            case ConsultationConstant.ROLE_USER:
                return ConsultationConstant.USER;
            case ConsultationConstant.ROLE_LIST:
                return ConsultationConstant.LIST;
            case ConsultationConstant.ROLE_PAYMENT_PLAN:
                return ConsultationConstant.PAYMENT_PLAN;
            case ConsultationConstant.ROLE_NOTIFICATION:
                return ConsultationConstant.NOTIFICATION;
            case ConsultationConstant.ROLE_TYPING:
                return ConsultationConstant.TYPING_VIEW;
            case ConsultationConstant.ROLE_MEDICINE:
                return ConsultationConstant.MEDICINE_VIEW;
            case ConsultationConstant.ROLE_LAB_TEST:
                return ConsultationConstant.LAB_TEST_VIEW;
            case ConsultationConstant.ROLE_RATING:
                return ConsultationConstant.RATING_VIEW;
            case ConsultationConstant.ROLE_FOLLOWUP:
                return ConsultationConstant.FOLLOWUP_VIEW;
            default:
                return ConsultationConstant.DOCTOR;
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public void updateAdapter(List<Message> updateList) {
        this.messageList = updateList;
        notifyDataSetChanged();
    }

    public void removeItem(int index) {
        messageList.remove(index);
        notifyItemRemoved(index);
    }

    class ChatHeaderHolder extends RecyclerView.ViewHolder {

        ChatHeaderHolder(LayoutHeaderBinding headerBinding) {
            super(headerBinding.getRoot());
        }
    }

    class ChatLeftHolder extends RecyclerView.ViewHolder {
        private final InflatorLeftChatItemBinding leftChatItemBinding;

        ChatLeftHolder(InflatorLeftChatItemBinding leftChatItemBinding) {
            super(leftChatItemBinding.getRoot());
            this.leftChatItemBinding = leftChatItemBinding;
        }
    }

    class ChatRightHolder extends RecyclerView.ViewHolder {
        private final InflatorRightChatItemBinding rightChatItemBinding;

        ChatRightHolder(InflatorRightChatItemBinding rightChatItemBinding) {
            super(rightChatItemBinding.getRoot());
            this.rightChatItemBinding = rightChatItemBinding;
        }
    }

    class DoctorHolder extends RecyclerView.ViewHolder {
        private final InflatorDoctorListBinding doctorListBinding;

        DoctorHolder(InflatorDoctorListBinding doctorListBinding) {
            super(doctorListBinding.getRoot());
            this.doctorListBinding = doctorListBinding;
        }
    }

    class PaymentPlanHolder extends RecyclerView.ViewHolder {
        private final InflatorPaymentPlanBinding paymentPlanBinding;

        PaymentPlanHolder(InflatorPaymentPlanBinding paymentPlanBinding) {
            super(paymentPlanBinding.getRoot());
            this.paymentPlanBinding = paymentPlanBinding;
        }
    }

    class NotificationHolder extends RecyclerView.ViewHolder {
        private final InflatorNotificationBinding notificationBinding;

        NotificationHolder(InflatorNotificationBinding notificationBinding) {
            super(notificationBinding.getRoot());
            this.notificationBinding = notificationBinding;
        }
    }

    class TypingViewHolder extends RecyclerView.ViewHolder {
        private final InflatorTypingViewBinding typingViewBinding;

        TypingViewHolder(InflatorTypingViewBinding typingViewBinding) {
            super(typingViewBinding.getRoot());
            this.typingViewBinding = typingViewBinding;
        }
    }


    class MedicineOrderViewHolder extends RecyclerView.ViewHolder {
        private final InflateOrderMedicineBinding inflateOrderMedicineBinding;

        MedicineOrderViewHolder(InflateOrderMedicineBinding inflateOrderMedicineBinding) {
            super(inflateOrderMedicineBinding.getRoot());
            this.inflateOrderMedicineBinding = inflateOrderMedicineBinding;
        }
    }

    class RatingViewHolder extends RecyclerView.ViewHolder {
        private final InflateRatingViewBinding inflateRatingViewBinding;

        RatingViewHolder(InflateRatingViewBinding inflateRatingViewBinding) {
            super(inflateRatingViewBinding.getRoot());
            this.inflateRatingViewBinding = inflateRatingViewBinding;
        }
    }

    class FollowUpInChatHolder extends RecyclerView.ViewHolder {
        private final FollowUpInChatBinding followUpInChatBinding;

        FollowUpInChatHolder(FollowUpInChatBinding followUpInChatBinding) {
            super(followUpInChatBinding.getRoot());
            this.followUpInChatBinding = followUpInChatBinding;
        }
    }


    public void setMedicineInfoResponse(MedicineInfoResponse medicineInfoResponse) {
        this.medicineInfoResponse = medicineInfoResponse;
    }

    public String getIntentFrom() {
        return intentFrom;
    }

    public void setIntentFrom(String intentFrom) {
        this.intentFrom = intentFrom;
    }

}
