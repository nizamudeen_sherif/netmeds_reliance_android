/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.consultation.adapter;

import android.app.Application;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DoctorDegreeItemBinding;
import com.nms.netmeds.consultation.model.Others;
import com.nms.netmeds.consultation.vm.DegreeViewModel;

import java.util.List;

public class DegreeAdapter extends RecyclerView.Adapter<DegreeAdapter.DegreeHolder> {
    private final List<Others> degreeList;
    private final Application application;

    public DegreeAdapter(Application application, List<Others> degreeList) {
        this.degreeList = degreeList;
        this.application = application;
    }

    @NonNull
    @Override
    public DegreeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DoctorDegreeItemBinding doctorDegreeItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.doctor_degree_item, parent, false);
        return new DegreeHolder(doctorDegreeItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DegreeHolder holder, int position) {
        DegreeViewModel degreeViewModel = new DegreeViewModel(application);
        degreeViewModel.onDataAvailable(degreeList.get(position), holder.doctorDegreeItemBinding);
        holder.doctorDegreeItemBinding.setViewModel(degreeViewModel);
    }

    @Override
    public int getItemCount() {
        return degreeList.size();
    }

    class DegreeHolder extends RecyclerView.ViewHolder {
        private final DoctorDegreeItemBinding doctorDegreeItemBinding;

        DegreeHolder(DoctorDegreeItemBinding doctorDegreeItemBinding) {
            super(doctorDegreeItemBinding.getRoot());
            this.doctorDegreeItemBinding = doctorDegreeItemBinding;
        }
    }
}
