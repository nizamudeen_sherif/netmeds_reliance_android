package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.adapter.DegreeAdapter;
import com.nms.netmeds.consultation.databinding.DoctorInfoBinding;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.model.Main;
import com.nms.netmeds.consultation.model.Others;

import java.util.ArrayList;
import java.util.List;

public class DoctorInfoViewModel extends AppViewModel {
    private DoctorInformation doctorInformation;
    private DoctorInfoBinding doctorInfoBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private DoctorInfoViewModelListener doctorInfoViewModelListener;
    private final Application application;

    public DoctorInfoViewModel(Application application) {
        super(application);
        this.application = application;
    }

    public void onDoctorInfoDataAvailable(Context context, DoctorInformation doctorInformation, DoctorInfoBinding doctorInfoBinding, DoctorInfoViewModelListener doctorInfoViewModelListener) {
        this.context = context;
        this.doctorInformation = doctorInformation;
        this.doctorInfoBinding = doctorInfoBinding;
        this.doctorInfoViewModelListener = doctorInfoViewModelListener;
        doctorInfoBinding.scrollView.smoothScrollTo(0, 0);
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_circular)
                .error(gender() == 0 ? R.drawable.ic_circular : gender())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(!TextUtils.isEmpty(imageUrl()) ? imageUrl() : gender() == 0 ? R.drawable.ic_circular : gender()).apply(options).into(doctorInfoBinding.imgDoctor);
        showDegree();
        showDoctorName();
        showSpecialization();
        showRating();
        showAbout();
        showDetails();
        showExperience();
        showFeedback();
        showLanguage();
        showFirstLanguage();
        showSecondLanguage();
    }

    public void onClose() {
        doctorInfoViewModelListener.doctorInfoClose();
    }

    public String doctorName() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getName()) ? doctorInformation.getName() : "";
    }

    private String doctorExperience() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getExperience()) ? String.format("%s %s %s", "|", doctorInformation.getExperience(), context.getString(R.string.text_doctor_experience)) : "";
    }

    private String doctorSpeciality() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getSpecialityTitle()) ? doctorInformation.getSpecialityTitle() : "";
    }

    public String rating() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getRating()) ? String.format("%s %s%s%s", context.getString(R.string.text_rated), doctorInformation.getRating(), "/", ConsultationConstant.RATING_VALUE) : "";
    }

    public float ratingValue() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getRating()) ? Float.parseFloat(doctorInformation.getRating()) : 0;
    }

    public String aboutDoctor() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getAbout()) ? doctorInformation.getAbout() : "";
    }

    public String specialityAndExperience() {
        return String.format("%s %s", doctorSpeciality(), doctorExperience());
    }

    public String experience() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getExperience()) ? String.format("%s %s", doctorInformation.getExperience(), context.getString(R.string.text_years)) : "";
    }

    public String feedBack() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getFeedBack()) ? doctorInformation.getFeedBack() : "";
    }

    public String firstLanguage() {
        return doctorInformation != null && doctorInformation.getLanguage() != null && !TextUtils.isEmpty(doctorInformation.getLanguage().getFirstLanguage()) ? doctorInformation.getLanguage().getFirstLanguage() : "";
    }

    public String secondLanguage() {
        return doctorInformation != null && doctorInformation.getLanguage() != null && !TextUtils.isEmpty(doctorInformation.getLanguage().getSecondLanguage()) ? doctorInformation.getLanguage().getSecondLanguage() : "";
    }

    private String imageUrl() {
        String url = "";
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getImage())) {
            url = ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_IMAGE_URL) + doctorInformation.getImage();
        }
        return url;
    }

    private int gender() {
        int gender = 0;
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getGender())) {
            if (doctorInformation.getGender().equals("male")) {
                gender = R.drawable.ic_default_doctor_male;
            } else {
                gender = R.drawable.ic_default_doctor_female;
            }
        }
        return gender;
    }

    private void showDegree() {
        if (doctorInformation != null && doctorInformation.getDoctorDegree() != null) {
            if (doctorInformation.getDoctorDegree().getOthersList() != null || doctorInformation.getDoctorDegree().getMain() != null) {
                List<Others> degreeList = new ArrayList<>();
                if (doctorInformation.getDoctorDegree().getOthersList() != null && doctorInformation.getDoctorDegree().getOthersList().size() > 0) {
                    for (Others others : doctorInformation.getDoctorDegree().getOthersList()) {
                        if (!TextUtils.isEmpty(others.getCollege()) || !TextUtils.isEmpty(others.getDomain()))
                            degreeList.add(others);
                    }
                } else {
                    Main main = doctorInformation.getDoctorDegree().getMain();
                    Others others = new Others();
                    String degree = !TextUtils.isEmpty(main.getTitle()) ? main.getTitle() : "";
                    String college = !TextUtils.isEmpty(main.getCollege()) ? main.getCollege() : "";
                    others.setDomain(degree);
                    others.setCollege(college);
                    degreeList.add(others);
                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                doctorInfoBinding.degreeList.setLayoutManager(linearLayoutManager);
                DegreeAdapter degreeAdapter = new DegreeAdapter(application, degreeList);
                doctorInfoBinding.degreeList.setAdapter(degreeAdapter);
                doctorInfoBinding.lytDegree.setVisibility(View.VISIBLE);
            } else
                doctorInfoBinding.lytDegree.setVisibility(View.GONE);
        } else {
            doctorInfoBinding.lytDegree.setVisibility(View.GONE);
        }
    }

    private void showDoctorName() {
        doctorInfoBinding.textDoctorName.setVisibility(TextUtils.isEmpty(doctorName()) ? View.GONE : View.VISIBLE);
    }

    private void showSpecialization() {
        doctorInfoBinding.textSpecialization.setVisibility((TextUtils.isEmpty(doctorSpeciality()) && TextUtils.isEmpty(doctorExperience())) ? View.GONE : View.VISIBLE);
    }

    private void showExperience() {
        doctorInfoBinding.lLayoutExperience.setVisibility(TextUtils.isEmpty(doctorExperience()) ? View.GONE : View.VISIBLE);
    }

    private void showRating() {
        doctorInfoBinding.doctorRating.setVisibility(TextUtils.isEmpty(rating()) ? View.GONE:View.VISIBLE);
        doctorInfoBinding.textRatingValue.setVisibility(TextUtils.isEmpty(rating()) ? View.GONE:View.VISIBLE);
    }

    private void showAbout() {
        doctorInfoBinding.lLDoctorAbout.setVisibility(TextUtils.isEmpty(aboutDoctor()) ? View.GONE : View.VISIBLE);
    }

    private void showFeedback() {
        doctorInfoBinding.lLayoutFeedBack.setVisibility(TextUtils.isEmpty(feedBack()) ? View.GONE : View.VISIBLE);
    }

    private void showFirstLanguage() {
        doctorInfoBinding.lLFirstLanguage.setVisibility(TextUtils.isEmpty(firstLanguage()) ? View.GONE : View.VISIBLE);
    }

    private void showSecondLanguage() {
        doctorInfoBinding.lLayoutSecondLanguage.setVisibility(TextUtils.isEmpty(secondLanguage()) ? View.GONE : View.VISIBLE);
    }

    private void showLanguage() {
        doctorInfoBinding.lLayoutLanguage.setVisibility((TextUtils.isEmpty(firstLanguage()) && TextUtils.isEmpty(secondLanguage())) ?View.GONE : View.VISIBLE);
    }

    public void showDetails() {
        doctorInfoBinding.lLayoutShowDetails.setVisibility((TextUtils.isEmpty(experience()) && TextUtils.isEmpty(feedBack())) ?  View.GONE : View.VISIBLE);
    }

    public interface DoctorInfoViewModelListener {
        void doctorInfoClose();
    }
}
