package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DoctorListItemBinding;
import com.nms.netmeds.consultation.model.DoctorInformation;

public class DoctorListItemViewModel extends AppViewModel {
    private DoctorInformation doctorInformation;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private DoctorListItemViewModelListener doctorListItemViewModelListener;
    private DoctorListItemBinding doctorListItemBinding;

    public DoctorListItemViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, DoctorInformation doctorInformation, DoctorListItemBinding doctorListItemBinding, DoctorListItemViewModelListener doctorListItemViewModelListener) {
        this.context = context;
        this.doctorInformation = doctorInformation;
        this.doctorListItemViewModelListener = doctorListItemViewModelListener;
        this.doctorListItemBinding =  doctorListItemBinding;
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_circular)
                .error(gender() == 0 ? R.drawable.ic_circular : gender())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(!TextUtils.isEmpty(imageUrl()) ? imageUrl() : gender() == 0 ? R.drawable.ic_circular : gender()).apply(options).into(doctorListItemBinding.imgDoctor);
        showDoctorName();
        showSpecialization();
        showExperience();
        showRating();
    }

    public String doctorName() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getName()) ? doctorInformation.getName() : "";
    }

    public String doctorExperience() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getExperience()) ? String.format("%s %s", doctorInformation.getExperience(), context.getString(R.string.text_doctor_experience)) : "";
    }

    public String doctorSpeciality() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getSpecialityTitle()) ? doctorInformation.getSpecialityTitle() : "";
    }

    public String rating() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getRating()) ? String.format("%s %s%s%s", context.getString(R.string.text_rated), doctorInformation.getRating(), "/", ConsultationConstant.RATING_VALUE) : "";
    }

    public float ratingValue() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getRating()) ? Float.parseFloat(doctorInformation.getRating()) : 0;
    }

    public void onViewProfile() {
        doctorListItemViewModelListener.viewProfile(doctorInformation.getId());
    }

    private String imageUrl() {
        String url = "";
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getImage())) {
            url = ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_IMAGE_URL) + doctorInformation.getImage();
        }
        return url;
    }

    private int gender() {
        int gender = 0;
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getGender())) {
            if (doctorInformation.getGender().equals("male")) {
                gender = R.drawable.ic_default_doctor_male;
            } else {
                gender = R.drawable.ic_default_doctor_female;
            }
        }
        return gender;
    }

    private void showDoctorName() {
        doctorListItemBinding.textDoctorName.setVisibility(TextUtils.isEmpty(doctorName()) ? View.GONE : View.VISIBLE);
    }

    private void showSpecialization() {
        doctorListItemBinding.textSpecialization.setVisibility(TextUtils.isEmpty(doctorSpeciality()) ? View.GONE : View.VISIBLE);
    }

    private void showExperience() {
        doctorListItemBinding.textExperience.setVisibility(TextUtils.isEmpty(doctorExperience()) ? View.GONE : View.VISIBLE);
    }

    private void showRating() {
        doctorListItemBinding.lLayoutRating.setVisibility(TextUtils.isEmpty(rating()) ? View.GONE : View.VISIBLE);
    }

    public interface DoctorListItemViewModelListener {
        void viewProfile(String doctorId);
    }
}
