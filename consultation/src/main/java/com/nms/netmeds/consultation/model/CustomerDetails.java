package com.nms.netmeds.consultation.model;

import java.io.Serializable;

public class CustomerDetails implements Serializable {
    private String customer_name;
    private int customer_id;
    private String customer_token;
    private String customer_email;
    private String customer_mobile;
    private int is_prime_customer;
    private int prime_expire;
    private String dob;
    private Address address = new Address();

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_token() {
        return customer_token;
    }

    public void setCustomer_token(String customer_token) {
        this.customer_token = customer_token;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public int getIs_prime_customer() {
        return is_prime_customer;
    }

    public void setIs_prime_customer(int is_prime_customer) {
        this.is_prime_customer = is_prime_customer;
    }

    public int getPrime_expire() {
        return prime_expire;
    }

    public void setPrime_expire(int prime_expire) {
        this.prime_expire = prime_expire;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getGender_id() {
        return gender_id;
    }

    public void setGender_id(int gender_id) {
        this.gender_id = gender_id;
    }

    private int gender_id;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


}
