package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.databinding.InflatorNotificationBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Message;

public class NotificationViewModel extends AppViewModel {
    private Message message;

    public NotificationViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Message message, InflatorNotificationBinding notificationBinding, MessageAdapterListener messageAdapterListener) {
        this.message = message;
        messageAdapterListener.loaderView(false);
        notificationBinding.textNotification.setBackgroundResource(message.getNotificationBackground());
        notificationBinding.textNotification.setTextColor(message.getNotificationTextColor());
    }

    public String notification() {
        return message != null && !TextUtils.isEmpty(message.getMessage()) ? message.getMessage() : "";
    }
}
