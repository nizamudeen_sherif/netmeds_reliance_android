package com.nms.netmeds.consultation.adapter;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.MultiChoiceItemBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.vm.MultiChoiceItemViewModel;

import java.util.List;

public class MultiChoiceAdapter extends RecyclerView.Adapter<MultiChoiceAdapter.MultiChoiceHolder> implements MultiChoiceItemViewModel.MultiChoiceItemListener {
    private List<Options> optionList;
    private final Application application;
    private final Context context;
    private final MessageAdapterListener messageAdapterListener;

    public MultiChoiceAdapter(Context context, Application application, List<Options> optionList, MessageAdapterListener messageAdapterListener) {
        this.context = context;
        this.application = application;
        this.optionList = optionList;
        this.messageAdapterListener = messageAdapterListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public MultiChoiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MultiChoiceItemBinding multiChoiceItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.multi_choice_item, parent, false);
        return new MultiChoiceHolder(multiChoiceItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiChoiceHolder holder, int position) {
        Options options = optionList.get(position);
        MultiChoiceItemViewModel multiChoiceViewModel = new MultiChoiceItemViewModel(application);
        multiChoiceViewModel.onDataAvailable(context, options, holder.multiChoiceItemBinding, position, this);
        holder.multiChoiceItemBinding.setViewModel(multiChoiceViewModel);
    }

    @Override
    public int getItemCount() {
        return optionList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onOptionSelected(int selectedPosition) {
        if (optionList.get(selectedPosition).isChecked())
            optionList.get(selectedPosition).setChecked(false);
        else
            optionList.get(selectedPosition).setChecked(true);
        notifyDataSetChanged();
        messageAdapterListener.selectedOption(optionList);
    }

    @Override
    public void onOtherOption() {
        messageAdapterListener.otherOption();
    }

    class MultiChoiceHolder extends RecyclerView.ViewHolder {
        private final MultiChoiceItemBinding multiChoiceItemBinding;

        MultiChoiceHolder(MultiChoiceItemBinding multiChoiceItemBinding) {
            super(multiChoiceItemBinding.getRoot());
            this.multiChoiceItemBinding = multiChoiceItemBinding;
        }
    }
}
