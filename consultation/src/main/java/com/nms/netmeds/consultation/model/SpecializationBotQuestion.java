package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SpecializationBotQuestion {
    @SerializedName("gyna")
    private List<BotSymptomQuestion> gynecologyQuestionList;
    @SerializedName("der")
    private List<BotSymptomQuestion> dermatologyQuestionList;
    @SerializedName("radio")
    private List<BotSymptomQuestion> radiologyQuestionList;
    @SerializedName("onco")
    private List<BotSymptomQuestion> oncologyQuestionList;
    @SerializedName("cardio")
    private List<BotSymptomQuestion> cardiologyQuestionList;
    @SerializedName("sexo")
    private List<BotSymptomQuestion> sexologyQuestionList;
    @SerializedName("pedia")
    private List<BotSymptomQuestion> pediatricsQuestionList;
    @SerializedName("psych")
    private List<BotSymptomQuestion> psychologyQuestionList;
    @SerializedName("ortho")
    private List<BotSymptomQuestion> orthopedicsQuestionList;
    @SerializedName("nutri")
    private List<BotSymptomQuestion> nutritionistQuestionList;
    @SerializedName("gas")
    private List<BotSymptomQuestion> gastroenterologyQuestionList;
    @SerializedName("ent")
    private List<BotSymptomQuestion> entQuestionList;
    @SerializedName("pulmo")
    private List<BotSymptomQuestion> pulmonologyQuestionList;
    @SerializedName("ophtha")
    private List<BotSymptomQuestion> ophthalmologyQuestionList;
    @SerializedName("endo")
    private List<BotSymptomQuestion> endocrinologyQuestionList;
    @SerializedName("gen")
    private List<BotSymptomQuestion> generalPhysicianQuestionList;

    public List<BotSymptomQuestion> getGynecologyQuestionList() {
        return gynecologyQuestionList;
    }

    public void setGynecologyQuestionList(List<BotSymptomQuestion> gynecologyQuestionList) {
        this.gynecologyQuestionList = gynecologyQuestionList;
    }

    public List<BotSymptomQuestion> getDermatologyQuestionList() {
        return dermatologyQuestionList;
    }

    public void setDermatologyQuestionList(List<BotSymptomQuestion> dermatologyQuestionList) {
        this.dermatologyQuestionList = dermatologyQuestionList;
    }

    public List<BotSymptomQuestion> getRadiologyQuestionList() {
        return radiologyQuestionList;
    }

    public void setRadiologyQuestionList(List<BotSymptomQuestion> radiologyQuestionList) {
        this.radiologyQuestionList = radiologyQuestionList;
    }

    public List<BotSymptomQuestion> getOncologyQuestionList() {
        return oncologyQuestionList;
    }

    public void setOncologyQuestionList(List<BotSymptomQuestion> oncologyQuestionList) {
        this.oncologyQuestionList = oncologyQuestionList;
    }

    public List<BotSymptomQuestion> getCardiologyQuestionList() {
        return cardiologyQuestionList;
    }

    public void setCardiologyQuestionList(List<BotSymptomQuestion> cardiologyQuestionList) {
        this.cardiologyQuestionList = cardiologyQuestionList;
    }

    public List<BotSymptomQuestion> getSexologyQuestionList() {
        return sexologyQuestionList;
    }

    public void setSexologyQuestionList(List<BotSymptomQuestion> sexologyQuestionList) {
        this.sexologyQuestionList = sexologyQuestionList;
    }

    public List<BotSymptomQuestion> getPediatricsQuestionList() {
        return pediatricsQuestionList;
    }

    public void setPediatricsQuestionList(List<BotSymptomQuestion> pediatricsQuestionList) {
        this.pediatricsQuestionList = pediatricsQuestionList;
    }

    public List<BotSymptomQuestion> getPsychologyQuestionList() {
        return psychologyQuestionList;
    }

    public void setPsychologyQuestionList(List<BotSymptomQuestion> psychologyQuestionList) {
        this.psychologyQuestionList = psychologyQuestionList;
    }

    public List<BotSymptomQuestion> getOrthopedicsQuestionList() {
        return orthopedicsQuestionList;
    }

    public void setOrthopedicsQuestionList(List<BotSymptomQuestion> orthopedicsQuestionList) {
        this.orthopedicsQuestionList = orthopedicsQuestionList;
    }

    public List<BotSymptomQuestion> getNutritionistQuestionList() {
        return nutritionistQuestionList;
    }

    public void setNutritionistQuestionList(List<BotSymptomQuestion> nutritionistQuestionList) {
        this.nutritionistQuestionList = nutritionistQuestionList;
    }

    public List<BotSymptomQuestion> getGastroenterologyQuestionList() {
        return gastroenterologyQuestionList;
    }

    public void setGastroenterologyQuestionList(List<BotSymptomQuestion> gastroenterologyQuestionList) {
        this.gastroenterologyQuestionList = gastroenterologyQuestionList;
    }

    public List<BotSymptomQuestion> getEntQuestionList() {
        return entQuestionList;
    }

    public void setEntQuestionList(List<BotSymptomQuestion> entQuestionList) {
        this.entQuestionList = entQuestionList;
    }

    public List<BotSymptomQuestion> getPulmonologyQuestionList() {
        return pulmonologyQuestionList;
    }

    public void setPulmonologyQuestionList(List<BotSymptomQuestion> pulmonologyQuestionList) {
        this.pulmonologyQuestionList = pulmonologyQuestionList;
    }

    public List<BotSymptomQuestion> getOphthalmologyQuestionList() {
        return ophthalmologyQuestionList;
    }

    public void setOphthalmologyQuestionList(List<BotSymptomQuestion> ophthalmologyQuestionList) {
        this.ophthalmologyQuestionList = ophthalmologyQuestionList;
    }

    public List<BotSymptomQuestion> getEndocrinologyQuestionList() {
        return endocrinologyQuestionList;
    }

    public void setEndocrinologyQuestionList(List<BotSymptomQuestion> endocrinologyQuestionList) {
        this.endocrinologyQuestionList = endocrinologyQuestionList;
    }

    public List<BotSymptomQuestion> getGeneralPhysicianQuestionList() {
        return generalPhysicianQuestionList;
    }

    public void setGeneralPhysicianQuestionList(List<BotSymptomQuestion> generalPhysicianQuestionList) {
        this.generalPhysicianQuestionList = generalPhysicianQuestionList;
    }
}
