package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.InflatorPaymentPlanBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.ConsultationFeeDetail;
import com.nms.netmeds.consultation.model.Message;

import java.util.Locale;

public class PaymentPlanViewModel extends AppViewModel {
    private ConsultationFeeDetail consultationFee;
    private InflatorPaymentPlanBinding paymentPlanBinding;
    private boolean isUnlimitedConsult;
    private boolean isOneTimeConsult;
    private String specialization;
    private String consultationMode;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private MessageAdapterListener messageAdapterListener;

    public PaymentPlanViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, InflatorPaymentPlanBinding paymentPlanBinding, Message message, MessageAdapterListener messageAdapterListener) {
        this.context = context;
        this.paymentPlanBinding = paymentPlanBinding;
        this.consultationFee = message.getFeeDetail();
        this.specialization = message.getSpecialization();
        this.consultationMode = message.getChatType();
        this.messageAdapterListener = messageAdapterListener;
        messageAdapterListener.loaderView(false);

        paymentPlanBinding.lytUnlimitedConsult.setEnabled(message.isEditable());
        paymentPlanBinding.lytUnlimitedConsult.setFocusable(message.isEditable());
        paymentPlanBinding.lytUnlimitedConsult.setClickable(message.isEditable());

        paymentPlanBinding.lytOneTimeConsult.setEnabled(message.isEditable());
        paymentPlanBinding.lytOneTimeConsult.setFocusable(message.isEditable());
        paymentPlanBinding.lytOneTimeConsult.setClickable(message.isEditable());

        onTimeOfferButtonEnableAndDisable(message.isEditable());
        unlimitedOfferButtonEnableAndDisable(message.isEditable());
        if (message.isEditable()) {
            paymentPlanBinding.lytUnlimitedConsult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isUnlimitedConsult) {
                        oneTimePaymentEnableAndDisable(false);
                        unLimitedPaymentEnableAndDisable(true);
                        isOneTimeConsult = true;
                        unLimitedFee();
                    }
                }
            });
            paymentPlanBinding.lytOneTimeConsult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isOneTimeConsult) {
                        oneTimePaymentEnableAndDisable(true);
                        unLimitedPaymentEnableAndDisable(false);
                        isUnlimitedConsult = true;
                        oneTimeFee();
                    }
                }
            });

            if (consultationFee.isCouponUpdate()) {
                isUnlimitedConsult = true;
                isOneTimeConsult = false;
                oneTimeFee();
                oneTimePaymentEnableAndDisable(true);
                unLimitedPaymentEnableAndDisable(false);
            } else {
                isOneTimeConsult = true;
                isUnlimitedConsult = false;
                unLimitedFee();
                oneTimePaymentEnableAndDisable(false);
                unLimitedPaymentEnableAndDisable(true);
            }

            if (!TextUtils.isEmpty(oneTimeStrikeOutPrice())) {
                double oneTimeFee = Double.parseDouble(oneTimeStrikeOutPrice());
                paymentPlanBinding.textOneTimeDiscountAmount.setText(String.format(Locale.getDefault(), "%.2f", oneTimeFee));
            }
            if (!TextUtils.isEmpty(oneTimeActualPrice())) {
                double oneTimeActualFee = Double.parseDouble(oneTimeActualPrice());
                paymentPlanBinding.textOneTimeNormalAmount.setText(CommonUtils.getPriceInFormat(oneTimeActualFee));
                paymentPlanBinding.textOneTimeNormalAmount.setPaintFlags(paymentPlanBinding.textOneTimeNormalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
        showUnlimitedStrikeOutPrice();
        showConsultationMode();
        showSpecialization();
        showOneTimeStrikeOutPrice();
        showOneTimeActualPrice();
        showOneTimeSavePercentage();
        showOfferApplied();
    }

    private void oneTimeFee() {
        if (!TextUtils.isEmpty(oneTimeStrikeOutPrice())) {
            double oneTimeFee = Double.parseDouble(oneTimeStrikeOutPrice());
            paymentPlanBinding.textOneTimeDiscountAmount.setText(String.format(Locale.getDefault(), "%.2f", oneTimeFee));
            messageAdapterListener.paymentPlan(oneTimeFee, ConsultationConstant.ONETIME_PLAN);
        }
        if (!TextUtils.isEmpty(oneTimeActualPrice())) {
            double oneTimeActualFee = Double.parseDouble(oneTimeActualPrice());
            paymentPlanBinding.textOneTimeNormalAmount.setText(CommonUtils.getPriceInFormat(oneTimeActualFee));
            paymentPlanBinding.textOneTimeNormalAmount.setPaintFlags(paymentPlanBinding.textOneTimeNormalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    private void unLimitedFee() {
        if (!TextUtils.isEmpty(unlimitedStrikeOutPrice())) {
            double unlimitedFee = ((Double.parseDouble(unlimitedStrikeOutPrice()) * 3 * 0.6) / 3);
            double unlimitedActualFee = Double.parseDouble(unlimitedStrikeOutPrice());
            paymentPlanBinding.textUnlimitedDiscountAmount.setText(String.format(Locale.getDefault(), "%.2f", unlimitedFee));
            paymentPlanBinding.textUnlimitedNormalAmount.setText(CommonUtils.getPriceInFormat(unlimitedActualFee));
            paymentPlanBinding.textUnlimitedNormalAmount.setPaintFlags(paymentPlanBinding.textUnlimitedNormalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            messageAdapterListener.paymentPlan((unlimitedFee * 3), ConsultationConstant.UNLIMITED_PLAN);
        }
    }

    private void oneTimePaymentEnableAndDisable(boolean checked) {
        onTimeOfferButtonEnableAndDisable(checked);

        paymentPlanBinding.lytOneTimeConsult.setBackgroundDrawable(checked ? context.getResources().getDrawable(R.drawable.primary_curved_rectangle) : context.getResources().getDrawable(R.drawable.list_selection_inactive));
        paymentPlanBinding.btnOfferAppliedOneTimeConsult.setBackgroundDrawable(checked ? context.getResources().getDrawable(R.drawable.pale_blue_grey_curved_rectangle) : context.getResources().getDrawable(R.drawable.offer_applied_disable));
        paymentPlanBinding.btnOfferAppliedOneTimeConsult.setTextColor(checked ? context.getResources().getColor(R.color.colorDeepAqua) : context.getResources().getColor(R.color.colorLightPaleBlueGrey));
        paymentPlanBinding.textOneTimeConsult.setTextColor(checked ? context.getResources().getColor(R.color.colorDarkBlueGrey) : context.getResources().getColor(R.color.colorLightBlueGrey));
        paymentPlanBinding.textOneTimeDiscountAmount.setTextColor(checked ? context.getResources().getColor(R.color.colorDarkBlueGrey) : context.getResources().getColor(R.color.colorLightBlueGrey));
        paymentPlanBinding.textOneTimeNormalAmount.setTextColor(checked ? context.getResources().getColor(R.color.colorLightBlueGrey) : context.getResources().getColor(R.color.colorLightBlueGrey));
        paymentPlanBinding.imgOneTimeRupee.setImageResource(checked ? R.drawable.ic_rupee_active_dark_blue : R.drawable.ic_rupee_inactive);
        paymentPlanBinding.imgOneTimeConsult.setBackgroundDrawable(checked ? context.getResources().getDrawable(R.drawable.ic_radio_checked) : context.getResources().getDrawable(R.drawable.ic_radio_unchecked));

        if (checked)
            paymentPlanBinding.btnOfferAppliedOneTimeConsult.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right_deep_aqua, 0);
        else
            paymentPlanBinding.btnOfferAppliedOneTimeConsult.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_right_disabled, 0);

    }

    private void unLimitedPaymentEnableAndDisable(boolean checked) {
        unlimitedOfferButtonEnableAndDisable(checked);

        paymentPlanBinding.lytUnlimitedConsult.setBackgroundDrawable(checked ? context.getResources().getDrawable(R.drawable.primary_curved_rectangle) : context.getResources().getDrawable(R.drawable.list_selection_inactive));
        paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setBackgroundDrawable(checked ? context.getResources().getDrawable(R.drawable.pale_blue_grey_curved_rectangle) : context.getResources().getDrawable(R.drawable.offer_applied_disable));
        paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setTextColor(checked ? context.getResources().getColor(R.color.colorDeepAqua) : context.getResources().getColor(R.color.colorLightPaleBlueGrey));
        paymentPlanBinding.textUnlimitedConsult.setTextColor(checked ? context.getResources().getColor(R.color.colorDarkBlueGrey) : context.getResources().getColor(R.color.colorLightBlueGrey));
        paymentPlanBinding.textUnlimitedDiscountAmount.setTextColor(checked ? context.getResources().getColor(R.color.colorDarkBlueGrey) : context.getResources().getColor(R.color.colorLightBlueGrey));
        paymentPlanBinding.textUnlimitedNormalAmount.setTextColor(checked ? context.getResources().getColor(R.color.colorLightBlueGrey) : context.getResources().getColor(R.color.colorLightBlueGrey));
        paymentPlanBinding.imgUnlimitedRupee.setImageResource(checked ? R.drawable.ic_rupee_active_dark_blue : R.drawable.ic_rupee_inactive);
        paymentPlanBinding.imgUnlimitedConsult.setBackgroundDrawable(checked ? context.getResources().getDrawable(R.drawable.ic_radio_checked) : context.getResources().getDrawable(R.drawable.ic_radio_unchecked));

        if (checked)
            paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right_deep_aqua, 0);
        else
            paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_right_disabled, 0);
    }

    private void onTimeOfferButtonEnableAndDisable(boolean checked) {
        paymentPlanBinding.btnOfferAppliedOneTimeConsult.setEnabled(checked);
        paymentPlanBinding.btnOfferAppliedOneTimeConsult.setFocusable(checked);
        paymentPlanBinding.btnOfferAppliedOneTimeConsult.setClickable(checked);
    }

    private void unlimitedOfferButtonEnableAndDisable(boolean checked) {
        paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setEnabled(checked);
        paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setFocusable(checked);
        paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setClickable(checked);
    }

    private String oneTimeActualPrice() {
        return consultationFee != null && !TextUtils.isEmpty(consultationFee.getOneTimeActualPrice()) ? consultationFee.getOneTimeActualPrice() : "";
    }

    private String oneTimeStrikeOutPrice() {
        return consultationFee != null && !TextUtils.isEmpty(consultationFee.getOneTimeStrikeOutPrice()) ? consultationFee.getOneTimeStrikeOutPrice() : "";
    }

    private String unlimitedStrikeOutPrice() {
        return consultationFee != null && !TextUtils.isEmpty(consultationFee.getUnlimitedStrikeOutPrice()) ? consultationFee.getUnlimitedStrikeOutPrice() : "";
    }

    public String oneTimeSavePercentage() {
        return consultationFee != null && !TextUtils.isEmpty(consultationFee.getOneTimePercentageValue()) ? String.format("%s %s%s", ConsultationConstant.TEXT_SAVE, consultationFee.getOneTimePercentageValue(), ConsultationConstant.PERCENTAGE_SYMBOL) : "";
    }

    public String unlimitedSavePercentage() {
        return consultationFee != null && !TextUtils.isEmpty(consultationFee.getUnlimitedPercentageValue()) ? String.format("%s %s%s", ConsultationConstant.TEXT_SAVE, consultationFee.getUnlimitedPercentageValue(), ConsultationConstant.PERCENTAGE_SYMBOL) : "";
    }

    private void showOneTimeActualPrice() {
        paymentPlanBinding.textOneTimeNormalAmount.setVisibility(TextUtils.isEmpty(oneTimeActualPrice()) ? View.GONE : View.VISIBLE);
    }

    private void showOneTimeStrikeOutPrice() {
        paymentPlanBinding.textOneTimeDiscountAmount.setVisibility(TextUtils.isEmpty(oneTimeStrikeOutPrice()) ? View.GONE : View.VISIBLE);
    }

    private void showUnlimitedStrikeOutPrice() {
        paymentPlanBinding.textUnlimitedDiscountAmount.setVisibility(TextUtils.isEmpty(unlimitedStrikeOutPrice()) ? View.GONE : View.VISIBLE);
        paymentPlanBinding.textUnlimitedNormalAmount.setVisibility(TextUtils.isEmpty(unlimitedStrikeOutPrice()) ? View.GONE : View.VISIBLE);
        paymentPlanBinding.textSavingUnlimitedConsult.setVisibility(TextUtils.isEmpty(unlimitedStrikeOutPrice()) ? View.GONE : View.VISIBLE);
        paymentPlanBinding.btnOfferAppliedUnlimitedConsult.setVisibility(TextUtils.isEmpty(unlimitedStrikeOutPrice()) ? View.GONE : View.VISIBLE);

    }

    private void showOneTimeSavePercentage() {
        paymentPlanBinding.textSavingOneTimeConsult.setVisibility(TextUtils.isEmpty(oneTimeSavePercentage()) ? View.GONE : View.VISIBLE);
    }

    private void showOfferApplied() {
        paymentPlanBinding.btnOfferAppliedOneTimeConsult.setVisibility((consultationFee != null && !TextUtils.isEmpty(consultationFee.getOneTimePercentageValue()))
        ? View.VISIBLE : View.GONE);
    }

    public void onOfferApplied() {
        messageAdapterListener.showAllCoupons();
    }

    public String specialization() {
        return !TextUtils.isEmpty(specialization) ? specialization : "";
    }

    private void showSpecialization() {
        paymentPlanBinding.lLayoutSpecialization.setVisibility(TextUtils.isEmpty(specialization) ? View.GONE : View.VISIBLE);
    }

    public String consultationMode() {
        return !TextUtils.isEmpty(consultationMode) ? CommonUtils.convertToTitleCase(consultationMode) : "";
    }

    private void showConsultationMode() {
        paymentPlanBinding.lLayoutConsultMode.setVisibility(TextUtils.isEmpty(consultationMode) ? View.GONE : View.VISIBLE);
    }
}
