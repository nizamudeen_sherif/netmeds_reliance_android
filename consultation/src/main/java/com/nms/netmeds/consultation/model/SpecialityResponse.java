package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

import java.util.List;

public class SpecialityResponse extends BaseResponse {
    @SerializedName("result")
    private List<Speciality> specialityList;
    @SerializedName("updatedOn")
    private String updatedOn;

    public List<Speciality> getSpecialityList() {
        return specialityList;
    }

    public void setSpecialityList(List<Speciality> specialityList) {
        this.specialityList = specialityList;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
