package com.nms.netmeds.consultation.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.AttachmentBinding;
import com.nms.netmeds.consultation.vm.AttachmentViewMode;

public class AttachmentFragment extends BaseDialogFragment implements AttachmentViewMode.AttachmentViewModelListener {
    private AttachmentListener attachmentListener;

    //Default Constructor
    public AttachmentFragment() {
    }

    @SuppressLint("ValidFragment")
    public AttachmentFragment(AttachmentListener attachmentListener) {
        this.attachmentListener = attachmentListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AttachmentBinding attachmentBinding = DataBindingUtil.inflate(inflater, R.layout.attachment, container, false);
        AttachmentViewMode attachmentViewMode = ViewModelProviders.of(this).get(AttachmentViewMode.class);
        attachmentViewMode.init(this);
        attachmentBinding.setViewModel(attachmentViewMode);
        return attachmentBinding.getRoot();
    }

    @Override
    public void closeDialog() {
        dismissDialog();
        attachmentListener.initCloseView();
    }

    @Override
    public void documentView() {
        dismissDialog();
        attachmentListener.initDocumentView();
    }

    @Override
    public void cameraView() {
        dismissDialog();
        attachmentListener.initCameraView();
    }

    @Override
    public void galleryView() {
        dismissDialog();
        attachmentListener.initGalleryView();
    }

    private void dismissDialog() {
        dismiss();
    }

    interface AttachmentListener {
        void initDocumentView();

        void initCameraView();

        void initGalleryView();

        void initCloseView();
    }
}
