package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.InflatorRightChatItemBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Message;

public class ChatRightViewModel extends AppViewModel {
    private Message message;
    private String attachmentUrl = "";
    private InflatorRightChatItemBinding rightChatItemBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private MessageAdapterListener messageAdapterListener;
    private int position;

    public ChatRightViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, InflatorRightChatItemBinding rightChatItemBinding, final Message message, final MessageAdapterListener messageAdapterListener, final int position) {
        this.context = context;
        this.message = message;
        this.rightChatItemBinding = rightChatItemBinding;
        this.messageAdapterListener = messageAdapterListener;
        this.position = position;
        messageAdapterListener.loaderView(false);

        if (message.getType() != null)
            checkMessageType();

        if (message.getTime() != null) {
            String date = message.getTime();
            rightChatItemBinding.textChatTime.setText(DateTimeUtils.getInstance().stringDate(date, DateTimeUtils.yyyyMMddTHHmmss, DateTimeUtils.HHmma));
        }
        rightChatItemBinding.imgChatSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(attachmentUrl))
                    messageAdapterListener.imagePreview(attachmentUrl);
            }
        });

        rightChatItemBinding.lytDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(attachmentUrl))
                    messageAdapterListener.imagePreview(attachmentUrl);
            }
        });

        if (message.isEditable()) {
            rightChatItemBinding.textChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0);
            rightChatItemBinding.textChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messageAdapterListener.editSelectedOption(position, message);
                }
            });
        } else {
            rightChatItemBinding.textChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            rightChatItemBinding.textChat.setOnClickListener(null);
        }

        showTime();
    }

    private void checkMessageType() {
        if (message.getType().equals(ConsultationConstant.MESSAGE_TYPE_TEXT)) {
            rightChatItemBinding.textChat.setVisibility(View.VISIBLE);
            rightChatItemBinding.imgChatSelf.setVisibility(View.GONE);
            rightChatItemBinding.lytDocument.setVisibility(View.GONE);
        } else
            showAttachmentImage();
    }

    private void showAttachmentImage() {
        rightChatItemBinding.textChat.setVisibility(View.GONE);
        attachmentUrl = message.getImageUri() != null && !TextUtils.isEmpty(message.getImageUri()) ? message.getImageUri() : "";
        checkFileFormat();
    }

    private void checkFileFormat() {
        if (attachmentUrl.contains(ConsultationConstant.JPG) || attachmentUrl.contains(ConsultationConstant.JPEG) || attachmentUrl.contains(ConsultationConstant.PNG)) {
            rightChatItemBinding.lytDocument.setVisibility(View.GONE);
            rightChatItemBinding.imgChatSelf.setVisibility(View.VISIBLE);
            Glide.with(context).load(attachmentUrl).apply(getRequestOption()).into(rightChatItemBinding.imgChatSelf);
        } else if (attachmentUrl.contains(ConsultationConstant.DOC) || attachmentUrl.contains(ConsultationConstant.DOCX) || attachmentUrl.contains(ConsultationConstant.PDF)) {
            rightChatItemBinding.imgChatSelf.setVisibility(View.GONE);
            rightChatItemBinding.lytDocument.setVisibility(View.VISIBLE);
            rightChatItemBinding.textDocument.setText(message.getMessage());
        } else {
            rightChatItemBinding.lytDocument.setVisibility(View.GONE);
            rightChatItemBinding.imgChatSelf.setVisibility(View.VISIBLE);
            Glide.with(context).load(attachmentUrl).apply(getRequestOption()).into(rightChatItemBinding.imgChatSelf);
            messageAdapterListener.downloadAttachment(message, position);
        }
    }

    public String message() {
        return message != null && !TextUtils.isEmpty(message.getMessage()) ? message.getMessage() : "";
    }

    private void showTime() {
        rightChatItemBinding.textChatTime.setVisibility(message.getTime()!=null ? View.VISIBLE : View.GONE);
    }

    private RequestOptions getRequestOption() {
        return new RequestOptions()
                .placeholder(R.drawable.ic_circular)
                .error(R.drawable.ic_circular)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
    }
}
