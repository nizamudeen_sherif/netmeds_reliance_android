package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.InflatorLeftChatItemBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Message;

public class ChatLeftViewModel extends AppViewModel {
    private Message message;
    private String attachmentUrl = "";
    private InflatorLeftChatItemBinding leftChatItemBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private MessageAdapterListener messageAdapterListener;
    private int position;

    public ChatLeftViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, InflatorLeftChatItemBinding leftChatItemBinding, Message message, final MessageAdapterListener messageAdapterListener, int position) {
        this.context = context;
        this.message = message;
        this.leftChatItemBinding = leftChatItemBinding;
        this.messageAdapterListener = messageAdapterListener;
        this.position = position;

        messageAdapterListener.loaderView(false);

        if (message.getType() != null)
            checkMessageType();

        if (message.getTime() != null) {
            String date = message.getTime();
            leftChatItemBinding.textChatTime.setText(DateTimeUtils.getInstance().stringDate(date, DateTimeUtils.yyyyMMddTHHmmss, DateTimeUtils.HHmma));
        }

        leftChatItemBinding.imgChatDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(attachmentUrl))
                    messageAdapterListener.imagePreview(attachmentUrl);
            }
        });

        leftChatItemBinding.lytDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(attachmentUrl))
                    messageAdapterListener.imagePreview(attachmentUrl);
            }
        });
        showTime();
    }

    private void checkMessageType() {
        if (message.getType().equals(ConsultationConstant.MESSAGE_TYPE_TEXT)) {
            leftChatItemBinding.textChat.setVisibility(View.VISIBLE);
            leftChatItemBinding.imgChatDoctor.setVisibility(View.GONE);
            leftChatItemBinding.lytDocument.setVisibility(View.GONE);
        } else
            showAttachmentImage();
    }

    private void showAttachmentImage() {
        leftChatItemBinding.textChat.setVisibility(View.GONE);
        attachmentUrl = message.getImageUri() != null && !TextUtils.isEmpty(message.getImageUri()) ? message.getImageUri() : "";
        checkFileFormat();
    }

    private void checkFileFormat() {
        if (attachmentUrl.contains(ConsultationConstant.JPG) || attachmentUrl.contains(ConsultationConstant.JPEG) || attachmentUrl.contains(ConsultationConstant.PNG)) {
            leftChatItemBinding.lytDocument.setVisibility(View.GONE);
            leftChatItemBinding.imgChatDoctor.setVisibility(View.VISIBLE);
            Glide.with(context).load(attachmentUrl).apply(getRequestOption()).into(leftChatItemBinding.imgChatDoctor);
        } else if (attachmentUrl.contains(ConsultationConstant.DOC) || attachmentUrl.contains(ConsultationConstant.DOCX) || attachmentUrl.contains(ConsultationConstant.PDF)) {
            leftChatItemBinding.imgChatDoctor.setVisibility(View.GONE);
            leftChatItemBinding.lytDocument.setVisibility(View.VISIBLE);
            leftChatItemBinding.textDocument.setText(message.getMessage());
        } else {
            leftChatItemBinding.lytDocument.setVisibility(View.GONE);
            leftChatItemBinding.imgChatDoctor.setVisibility(View.VISIBLE);
            Glide.with(context).load(attachmentUrl).apply(getRequestOption()).into(leftChatItemBinding.imgChatDoctor);
            messageAdapterListener.downloadAttachment(message, position);
        }
    }

    public String message() {
        return message != null && !TextUtils.isEmpty(message.getMessage()) ? message.getMessage() : "";
    }

    private void showTime() {
        leftChatItemBinding.textChatTime.setVisibility((message!=null && message.isShowTime()) ? View.GONE : View.VISIBLE);
    }

    private RequestOptions getRequestOption() {
        return new RequestOptions()
                .placeholder(R.drawable.ic_circular)
                .error(R.drawable.ic_circular)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
    }
}
