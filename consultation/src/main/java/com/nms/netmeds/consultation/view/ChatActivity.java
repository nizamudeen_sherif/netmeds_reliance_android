package com.nms.netmeds.consultation.view;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConsultationEvent;
import com.nms.netmeds.base.model.ConsultationNotification;
import com.nms.netmeds.base.model.MessageFrom;
import com.nms.netmeds.base.model.MessageTo;
import com.nms.netmeds.base.model.NotificationBody;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationDatePickerFragment;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ActivityChatBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.ApplyCouponResponse;
import com.nms.netmeds.consultation.model.ChatHistoryResponse;
import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.model.ConsultationFeeDetail;
import com.nms.netmeds.consultation.model.ConsultationFeeResponse;
import com.nms.netmeds.consultation.model.ConsultationUpdateResponse;
import com.nms.netmeds.consultation.model.CreateChatResponse;
import com.nms.netmeds.consultation.model.CreateConsultationResponse;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.GetAllCouponResponse;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.model.PaymentCreationResponse;
import com.nms.netmeds.consultation.model.ReoccurUpdateResponse;
import com.nms.netmeds.consultation.model.Speciality;
import com.nms.netmeds.consultation.model.UploadAttachmentResponse;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;
import com.nms.netmeds.consultation.vm.ChatViewModel;
import com.webengage.sdk.android.WebEngage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChatActivity extends BaseUploadPrescription<ChatViewModel> implements ChatViewModel.ChatListener {
    private ActivityChatBinding chatBinding;
    private ChatViewModel chatViewModel;
    private String prescriptionId = "";
    private boolean isPrescriptionSubmitted = false;
    private String intentFrom = "";
    private Bundle bundle;
    private static SocketListener socketListener;
    private DoctorInformation doctorInformation;
    private RatingSubmitRequest availableRating;
    private ConsultationFeeDetail followupFeeDetail;
    private HistoryResult historyResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        toolBarSetUp(chatBinding.toolbar);
        initToolbar();
        intentValue();
        EventBus.getDefault().register(this);
        chatBinding.setViewModel(onCreateViewModel());
    }

    private void initToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(ChatActivity.this.getResources().getString(R.string.text_medi_bot));
            if (chatBinding.toolbar.getChildCount() > 0 && chatBinding.toolbar.getChildAt(2) instanceof TextView)
                ((TextView) chatBinding.toolbar.getChildAt(2)).setTypeface(CommonUtils.getTypeface(this, "font/Lato-Regular.ttf"));
        }
    }

    @Override
    protected ChatViewModel onCreateViewModel() {
        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        chatViewModel.getCreateChatMutableLiveData().observe(this, new CreateChatObserver());
        chatViewModel.getConsultationUpdateMutableLiveData().observe(this, new ConsultationUpdateObserver());
        chatViewModel.getDoctorListMutableLiveData().observe(this, new DoctorListObserver());
        chatViewModel.getDoctorInfoMutableLiveData().observe(this, new DoctorInfoObserver());
        chatViewModel.getCreateConsultationMutableLiveData().observe(this, new CreateConsultationObserver());
        chatViewModel.getFeeMutableLiveData().observe(this, new ConsultationFeeObserver());
        chatViewModel.getApplyCouponMutableLiveData().observe(this, new ApplyCouponObserver());
        chatViewModel.getAllCouponMutableLiveData().observe(this, new GetAllCouponObserver());
        chatViewModel.getUploadAttachmentMutableLiveData().observe(this, new UploadAttachmentObserver());
        chatViewModel.getPaymentCreationMutableLiveData().observe(this, new PaymentCreationObserver());
        chatViewModel.getReoccurUpdateMutableLiveData().observe(this, new ReoccurObserver());
        chatViewModel.getChatHistoryMutableLiveData().observe(this, new ChatHistoryObserver());
        chatViewModel.getMedicineInfoResponseMutableLiveData().observe(this, new MedicineObserver());
        if (intentFrom.equals(ConsultationConstant.FOLLOW_UP) || intentFrom.equals(ConsultationConstant.CHAT_HISTORY))
            chatViewModel.init(this, chatBinding, chatViewModel, this, bundle, doctorInformation, availableRating, followupFeeDetail);
        else
            chatViewModel.init(this, chatBinding, chatViewModel, this, intentFrom);
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.INTENT_FIRST_CONSULTATION)) {
            chatViewModel.setConsultationHistoryExist(getIntent().hasExtra(IntentConstant.INTENT_FIRST_CONSULTATION));
        }
        onRetry(chatViewModel);
        chatViewModel.setHistoryResult(historyResult);
        chatViewModel.fetchPatientDetails();
        return chatViewModel;
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            checkDeepLink(intent);
        }
    }

    private void checkDeepLink(Intent intent) {
        if (intent.hasExtra("extraPathParams")) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get("extraPathParams");
            validatePathParam(intentParamList);
        } else {
            checkIntentFromKey(intent);
        }
    }

    private void checkIntentFromKey(Intent intent) {
        bundle = intent.getExtras();
        if (intent.hasExtra(ConsultationConstant.KEY_INTENT_FROM)) {
            intentFrom = intent.getStringExtra(ConsultationConstant.KEY_INTENT_FROM);
        }
        if (intent.hasExtra(ConsultationConstant.KEY_DOC_INFO)) {
            doctorInformation = (DoctorInformation) getIntent().getSerializableExtra(ConsultationConstant.KEY_DOC_INFO);
        }
        if (intent.hasExtra(ConsultationConstant.KEY_CONSULTATION_FEE_DETAIL)) {
            followupFeeDetail = (ConsultationFeeDetail) getIntent().getSerializableExtra(ConsultationConstant.KEY_CONSULTATION_FEE_DETAIL);
        }
        if (intent.hasExtra(IntentConstant.RATING)) {
            availableRating = (RatingSubmitRequest) getIntent().getSerializableExtra(IntentConstant.RATING);
        }

        if (intent.hasExtra(IntentConstant.CONSULTATION_HISTORY)) {
            historyResult = new Gson().fromJson(getIntent().getStringExtra(IntentConstant.CONSULTATION_HISTORY), HistoryResult.class);
        }
    }

    private void validatePathParam(ArrayList<String> intentParamList) {
        if (intentParamList != null && intentParamList.size() > 0) {
            ConsultationNotification consultationNotification = new Gson().fromJson(intentParamList.get(0), ConsultationNotification.class);
            NotificationBody notificationBody = consultationNotification != null && consultationNotification.getBody() != null ? consultationNotification.getBody() : null;
            MessageTo messageTo = notificationBody != null && notificationBody.getMessageTo() != null ? notificationBody.getMessageTo() : null;
            String userId = messageTo != null && !TextUtils.isEmpty(messageTo.getId()) ? messageTo.getId() : "";
            MessageFrom messageFrom = notificationBody != null && notificationBody.getMessageFrom() != null ? notificationBody.getMessageFrom() : null;
            String doctorId = messageFrom != null && !TextUtils.isEmpty(messageFrom.getId()) ? messageFrom.getId() : "";
            String chatId = notificationBody != null && !TextUtils.isEmpty(notificationBody.getChatId()) ? notificationBody.getChatId() : "";

            bundle = new Bundle();
            bundle.putString(ConsultationConstant.KEY_CHAT_ID, chatId);
            bundle.putString(ConsultationConstant.KEY_DOCTOR_ID, doctorId);
            bundle.putString(ConsultationConstant.KEY_USER_ID, userId);
            bundle.putString(ConsultationConstant.KEY_CONSULTATION_ID, chatId);
            bundle.putString(ConsultationConstant.KEY_INTENT_FROM, ConsultationConstant.CHAT_HISTORY);
            intentFrom = ConsultationConstant.CHAT_HISTORY;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        socketListener.socketConnection();
    }

    @Override
    public void onPause() {
        super.onPause();
        socketListener.socketDisconnection();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatViewModel.socketOff();
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmReceiveMessage(final Object... args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onReceiveMessage(args);
            }
        });
    }

    @Override
    public void vmDoctorJoinedChat(final Object... args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onDoctorJoined(args);
            }
        });
    }

    @Override
    public void vmPrescriptionSubmitted(final Object... args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (args != null && args.length > 0) {
                    prescriptionId = (String) args[0];
                    isPrescriptionSubmitted = true;
                    invalidateOptionsMenu();
                    chatViewModel.onPrescriptionSubmitted(prescriptionId);
                }
            }
        });
    }

    @Override
    public void vmOnConnect() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onConnect();
            }
        });
    }

    @Override
    public void vmOnDisConnect() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onDisConnect();
            }
        });
    }

    @Override
    public void vmOnConnectError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onConnectError();
            }
        });
    }

    @Override
    public void vmOnError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onError();
            }
        });
    }

    @Override
    public void vmShowSpecialization(List<Speciality> specialityList, boolean editSpecialization, MessageAdapterListener messageAdapterListener) {
        SpecializationBottomSheetDialog specializationBottomSheetDialog = new SpecializationBottomSheetDialog(specialityList, editSpecialization, messageAdapterListener);
        getSupportFragmentManager().beginTransaction().add(specializationBottomSheetDialog, "SpecializationBottomSheetDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void vmShowDoctorInfo(DoctorInformation doctorInformation, MessageAdapterListener messageAdapterListener) {
        DoctorInfoBottomSheetDialog doctorInfoBottomSheetDialog = new DoctorInfoBottomSheetDialog(doctorInformation, messageAdapterListener);
        getSupportFragmentManager().beginTransaction().add(doctorInfoBottomSheetDialog, "DoctorInfoBottonSheetDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void vmShowPromoCodeList(List<ConsultationCoupon> promoCodeList, String appliedPromoCode, MessageAdapterListener messageAdapterListener) {
        CouponListBottomSheetDialog couponListBottomSheetDialog = new CouponListBottomSheetDialog(promoCodeList, appliedPromoCode, messageAdapterListener);
        getSupportFragmentManager().beginTransaction().add(couponListBottomSheetDialog, "CouponListBottomSheetDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void vmShowProblemCategory(List<Options> problemCategoryList, MessageAdapterListener messageAdapterListener) {
        ProblemCategoryBottomSheetDialog problemCategoryBottomSheetDialog = new ProblemCategoryBottomSheetDialog(problemCategoryList, messageAdapterListener);
        getSupportFragmentManager().beginTransaction().add(problemCategoryBottomSheetDialog, "ProblemCategoryBottomSheetDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void vmShowDatePicker() {
        showPicker();
    }

    @Override
    public void vmInitCameraIntent() {
        launchCameraIntent(getContentUri());
    }

    @Override
    public void vmInitGalleryIntent() {
        launchGalleryIntent();
    }

    @Override
    public void vmInitDocumentIntent() {
        launchDocumentIntent();
    }

    @Override
    public void vmShowImagePreview(String url) {
        ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(url);
        getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void vmOpenDocument(Intent intent) {
        ChatActivity.this.startActivity(intent);
    }

    @Override
    public void vmCallBackPayment(Bundle bundle) {
        Intent intent = new Intent();
        intent.putExtras(bundle);
        LaunchIntentManager.routeToActivity(ChatActivity.this.getResources().getString(R.string.route_payment_activity), intent, this);
    }

    @Override
    public void vmInitDocumentView() {
        if (checkGalleryPermission()) {
            vmInitDocumentIntent();
        } else
            requestPermission(PermissionConstants.DOCUMENT_PERMISSION);
    }

    @Override
    public void vmInitCameraView() {
        if (checkCameraPermission()) {
            vmInitCameraIntent();
        } else requestPermission(PermissionConstants.CAMERA_PERMISSION);
    }

    @Override
    public void vmInitGalleryView() {
        if (checkGalleryPermission()) {
            vmInitGalleryIntent();
        } else
            requestPermission(PermissionConstants.GALLERY_PERMISSION);
    }

    @Override
    public void vmOnFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatViewModel.onFailedData();
            }
        });
    }

    @Override
    public void vmInitDownloadAttachment() {
        if (checkGalleryPermission()) {
            chatViewModel.vmDownloadAttachment();
        } else
            requestPermission(PermissionConstants.DOWNLOAD_ATTACHMENT);
    }

    @Override
    public void navigateToBookLabTest(MedicineInfoResponse medicineInfoResponse) {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_LAB_TEST_FROM_CONSULTATION, medicineInfoResponse.getLabTests());
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_diagnostic_home), intent, this);
    }

    private Uri getContentUri() {
        File photo = new File(FileUtils.getTempDirectoryPath(this), FileUtils.TEMP_DIRECTORY_PATH);
        final String authority = CommonUtils.getPackageInfo(this) + FileUtils.PROVIDER;
        return FileProvider.getUriForFile(this, authority, photo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_consultation, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.download_prescription).setVisible(isPrescriptionSubmitted);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ChatActivity.this.finish();
        } else if (item.getItemId() == R.id.help) {
            LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_need_help_activity), this);
        } else if (item.getItemId() == R.id.download_prescription) {
            initDownload();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDownload() {
        if (checkGalleryPermission())
            chatViewModel.downloadPrescription(prescriptionId);
        else
            requestPermission(PermissionConstants.DOWNLOAD_PERMISSION);
    }

    public static void setListener(SocketListener listener) {
        socketListener = listener;
    }

    public interface SocketListener {
        void socketConnection();

        void socketDisconnection();
    }

    private class CreateChatObserver implements Observer<CreateChatResponse> {

        @Override
        public void onChanged(@Nullable CreateChatResponse createChatResponse) {
            chatViewModel.onCreateChatDataAvailable(createChatResponse);
        }
    }

    private class ConsultationUpdateObserver implements Observer<ConsultationUpdateResponse> {

        @Override
        public void onChanged(@Nullable ConsultationUpdateResponse consultationUpdateResponse) {
            chatViewModel.onConsultationUpdateDataAvailable(consultationUpdateResponse);
        }
    }

    private class DoctorListObserver implements Observer<DoctorListResponse> {

        @Override
        public void onChanged(@Nullable DoctorListResponse doctorListResponse) {
            chatViewModel.onDoctorListDataAvailable(doctorListResponse);
        }
    }

    private class DoctorInfoObserver implements Observer<DoctorListResponse> {

        @Override
        public void onChanged(@Nullable DoctorListResponse doctorInfoResponse) {
            chatViewModel.onDoctorInfoDataAvailable(doctorInfoResponse);
        }
    }

    private class CreateConsultationObserver implements Observer<CreateConsultationResponse> {

        @Override
        public void onChanged(@Nullable CreateConsultationResponse createConsultationResponse) {
            chatViewModel.createConsultationOnDataAvailable(createConsultationResponse);
        }
    }

    private class ConsultationFeeObserver implements Observer<ConsultationFeeResponse> {

        @Override
        public void onChanged(@Nullable ConsultationFeeResponse consultationFeeResponse) {
            chatViewModel.consultationFeeDataAvailable(consultationFeeResponse);
        }
    }

    private class ApplyCouponObserver implements Observer<ApplyCouponResponse> {

        @Override
        public void onChanged(@Nullable ApplyCouponResponse applyCouponResponse) {
            chatViewModel.onApplyCouponDataAvailable(applyCouponResponse);
        }
    }

    private class GetAllCouponObserver implements Observer<GetAllCouponResponse> {

        @Override
        public void onChanged(@Nullable GetAllCouponResponse getAllCouponResponse) {
            chatViewModel.onGetAllCouponDataAvailable(getAllCouponResponse);
        }
    }

    private class UploadAttachmentObserver implements Observer<UploadAttachmentResponse> {

        @Override
        public void onChanged(@Nullable UploadAttachmentResponse uploadAttachmentResponse) {
            chatViewModel.onUploadAttachmentDataAvailable(uploadAttachmentResponse);
        }
    }

    private class PaymentCreationObserver implements Observer<PaymentCreationResponse> {

        @Override
        public void onChanged(@Nullable PaymentCreationResponse paymentCreationResponse) {
            chatViewModel.paymentCreationDataAvailable(paymentCreationResponse);
        }
    }

    private class ReoccurObserver implements Observer<ReoccurUpdateResponse> {

        @Override
        public void onChanged(@Nullable ReoccurUpdateResponse reoccurUpdateResponse) {
            chatViewModel.onReoccurDataAvailable(reoccurUpdateResponse);
        }
    }

    private class ChatHistoryObserver implements Observer<ChatHistoryResponse> {

        @Override
        public void onChanged(@Nullable ChatHistoryResponse chatHistoryResponse) {
            chatViewModel.onChatHistoryDataAvailable(chatHistoryResponse);
        }
    }


    private class MedicineObserver implements Observer<MedicineInfoResponse> {

        @Override
        public void onChanged(@Nullable MedicineInfoResponse medicineInfoResponse) {
            chatViewModel.onMedicineDataAvailable(medicineInfoResponse);
        }
    }

    private void showPicker() {
        ConsultationDatePickerFragment date = new ConsultationDatePickerFragment();
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        date.setCallBack(onDate);
        date.show(getSupportFragmentManager(), "Date_Picker");
    }

    private final DatePickerDialog.OnDateSetListener onDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            view.setMaxDate(System.currentTimeMillis());
            chatViewModel.onDateAvailable(year, month, day);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (checkPermissionResponse(grantResults)) {
            switch (requestCode) {
                case PermissionConstants.CAMERA_PERMISSION:
                    launchCameraIntent(getContentUri());
                    break;
                case PermissionConstants.GALLERY_PERMISSION:
                    launchGalleryIntent();
                    break;
                case PermissionConstants.DOCUMENT_PERMISSION:
                    launchDocumentIntent();
                    break;
                case PermissionConstants.DOWNLOAD_PERMISSION:
                    chatViewModel.downloadPrescription(prescriptionId);
                    break;
                case PermissionConstants.DOWNLOAD_ATTACHMENT:
                    chatViewModel.vmDownloadAttachment();
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PermissionConstants.ACTION_IMAGE_CAPTURE:
                chatViewModel.setCapturedImage(resultCode);
                break;
            case PermissionConstants.ACTION_GALLERY_REQUEST:
                chatViewModel.setGalleryImage(resultCode, data);
                break;
            case PermissionConstants.ACTION_DOCUMENT_REQUEST:
                chatViewModel.setDocument(data);
                break;
        }
    }

    @Subscribe
    public void onEvent(ConsultationEvent consultationEvent) {
        chatViewModel.setPaymentResponse(consultationEvent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_CHAT_PAGE);

    }

    @Override
    public Context getContext() {
        return this;
    }
}
