package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class ConsultationUpdateResponse extends BaseResponse {
    @SerializedName("result")
    private ConsultationUpdateResult result;
    @SerializedName("updatedOn")
    private String updatedOn;
    private String updateType;
    private String paymentStatus;

    public ConsultationUpdateResult getResult() {
        return result;
    }

    public void setResult(ConsultationUpdateResult result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
