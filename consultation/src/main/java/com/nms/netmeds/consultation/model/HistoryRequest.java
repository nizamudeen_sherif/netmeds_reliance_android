package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class HistoryRequest {
    @SerializedName("orderBy")
    private OrderBy orderBy;
    @SerializedName("payment")
    private String payment;
    @SerializedName("userId")
    private String userId;

    public OrderBy getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderBy orderBy) {
        this.orderBy = orderBy;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
