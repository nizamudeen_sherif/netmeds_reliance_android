package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class OrderBy {
    @SerializedName("field")
    private String field;
    @SerializedName("order")
    private String order;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
