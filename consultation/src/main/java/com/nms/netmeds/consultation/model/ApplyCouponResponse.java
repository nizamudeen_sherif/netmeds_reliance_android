package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class ApplyCouponResponse extends BaseResponse {
    @SerializedName("result")
    private ApplyCoupon applyCoupon;
    @SerializedName("updatedOn")
    private String updatedOn;

    public ApplyCoupon getApplyCoupon() {
        return applyCoupon;
    }

    public void setApplyCoupon(ApplyCoupon applyCoupon) {
        this.applyCoupon = applyCoupon;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
