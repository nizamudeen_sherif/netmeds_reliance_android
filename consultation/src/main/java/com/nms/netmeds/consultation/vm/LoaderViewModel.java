package com.nms.netmeds.consultation.vm;

import android.app.Application;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;

public class LoaderViewModel extends AppViewModel {

    public LoaderViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(MessageAdapterListener messageListener) {
        messageListener.loaderView(true);
    }
}
