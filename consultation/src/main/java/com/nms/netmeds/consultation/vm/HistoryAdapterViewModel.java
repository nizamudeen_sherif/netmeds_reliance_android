package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.AdapterHistoryBinding;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.Speciality;

import org.w3c.dom.Text;

public class HistoryAdapterViewModel extends AppViewModel {
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private HistoryResult historyResult;
    private HistoryViewModel.HistoryListener historyListener;
    private DoctorInformation doctorInformation;
    private DateTimeUtils dateTimeUtils;
    private Speciality specialization;
    private AdapterHistoryBinding historyBinding;

    public HistoryAdapterViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(final Context context, HistoryViewModel.HistoryListener historyListener, HistoryResult historyResult, final AdapterHistoryBinding historyBinding) {
        this.context = context;
        this.historyListener = historyListener;
        this.historyResult = historyResult;
        dateTimeUtils = DateTimeUtils.getInstance();
        this.historyBinding = historyBinding;
        if (historyResult.getDoctorInformation() != null)
            doctorInformation = historyResult.getDoctorInformation();
        if (historyResult.getSpeciality() != null)
            specialization = historyResult.getSpeciality();
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_circular)
                .error(gender() == 0 ? R.drawable.ic_circular : gender())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(this.context).load(!TextUtils.isEmpty(imageUrl()) ? imageUrl() : gender() == 0 ? R.drawable.ic_circular : gender()).apply(options).into(historyBinding.imgDoctor);
        isShowUnreadCount();
        isShowLastMessage();
        isShowDateTime();
        isShowStatus();
    }

    public String doctorName() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getName()) ? doctorInformation.getName() : context.getResources().getString(R.string.text_medi_bot);
    }

    public String status() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getStatus()) ? historyResult.getStatus() : "";
    }

    public String dateTime() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getCreationDate()) ? dateTimeUtils.dateTime(dateTimeUtils.utcToLocal(DateTimeUtils.yyyyMMddTHHmmssSSS, DateTimeUtils.yyyyMMddTHHmmssSSS, historyResult.getCreationDate()), DateTimeUtils.yyyyMMddTHHmmssSSS) : "";
    }

    public String unReadCount() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getUnreadCount()) ? historyResult.getUnreadCount() : "";
    }

    public String lastMessage() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getLastMessage()) ? historyResult.getLastMessage() : "";
    }

    public String specialization() {
        return specialization != null && !TextUtils.isEmpty(specialization.getName()) ? String.format("%s %s", ",", specialization.getName()) : "";
    }

    private String imageUrl() {
        String url = "";
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getImage())) {
            url = ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_IMAGE_URL) + doctorInformation.getImage();
        }
        return url;
    }

    private int gender() {
        int gender = 0;
        if (historyResult != null && !TextUtils.isEmpty(historyResult.getGender())) {
            if (historyResult.getGender().equalsIgnoreCase(ConsultationConstant.MALE)) {
                gender = R.drawable.ic_default_doctor_male;
            } else {
                gender = R.drawable.ic_default_doctor_female;
            }
        }
        return gender;
    }


    private void isShowUnreadCount() {
        historyBinding.txtUnreadCount.setVisibility((TextUtils.isEmpty(unReadCount()) || Integer.parseInt(unReadCount()) == 0) ? View.GONE: View.VISIBLE);
    }

    private void isShowLastMessage() {
        historyBinding.txtShowLastMsg.setVisibility(!TextUtils.isEmpty(lastMessage()) ? View.VISIBLE : View.GONE);
    }

    private void isShowDateTime() {
        historyBinding.imgShowDateTime.setVisibility(TextUtils.isEmpty(dateTime()) ? View.GONE:View.VISIBLE);
        historyBinding.txtDateAndTime.setVisibility(TextUtils.isEmpty(dateTime()) ? View.GONE:View.VISIBLE);
    }

    private void isShowStatus() {
        historyBinding.viewShowStatus.setVisibility(TextUtils.isEmpty(status()) ?  View.GONE: View.VISIBLE);
        historyBinding.txtShowStatus.setVisibility(TextUtils.isEmpty(status()) ?  View.GONE: View.VISIBLE);
    }

    public void onConsultation() {
        historyListener.vmCallbackOnHistoryDetail(historyResult);
    }
}
