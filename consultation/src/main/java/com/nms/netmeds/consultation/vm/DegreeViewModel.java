package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.text.TextUtils;
import android.view.View;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.databinding.DoctorDegreeItemBinding;
import com.nms.netmeds.consultation.model.Others;

public class DegreeViewModel extends AppViewModel {
    private Others others;
    private DoctorDegreeItemBinding doctorDegreeItemBinding;

    public DegreeViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(Others others,DoctorDegreeItemBinding doctorDegreeItemBinding) {
        this.others = others;
        this.doctorDegreeItemBinding = doctorDegreeItemBinding;
        hideDegreeView();
        showCollege();
        showDomain();
    }

    public String domain() {
        return others != null && !TextUtils.isEmpty(others.getDomain()) ? others.getDomain() : "";
    }

    public String college() {
        return others != null && !TextUtils.isEmpty(others.getCollege()) ? others.getCollege() : "";
    }

    private void showDomain() {
        doctorDegreeItemBinding.txtShowDomain.setVisibility(TextUtils.isEmpty(domain()) ? View.GONE: View.VISIBLE);
    }

    private void showCollege() {
        doctorDegreeItemBinding.txtShowCollege.setVisibility(TextUtils.isEmpty(college()) ? View.GONE: View.VISIBLE);
    }

    private void hideDegreeView() {
        doctorDegreeItemBinding.lLayoutDocDegree.setVisibility((TextUtils.isEmpty(domain()) && TextUtils.isEmpty(college())) ? View.GONE : View.VISIBLE);
    }
}
