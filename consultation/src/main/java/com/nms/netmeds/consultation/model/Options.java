package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class Options {
    @SerializedName("option")
    private String option;
    @SerializedName("checked")
    private boolean checked;

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
