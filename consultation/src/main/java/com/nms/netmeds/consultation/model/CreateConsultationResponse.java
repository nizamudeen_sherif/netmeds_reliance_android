package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class CreateConsultationResponse extends BaseResponse {
    @SerializedName("result")
    private CreateConsultationResult result;
    @SerializedName("updatedOn")
    private String updatedOn;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public CreateConsultationResult getResult() {
        return result;
    }

    public void setResult(CreateConsultationResult result) {
        this.result = result;
    }
}
