/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.consultation.adapter;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.CouponListItemBinding;
import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.vm.CouponListItemViewModel;

import java.util.List;

public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.CouponListHolder> implements CouponListItemViewModel.CouponListItemListener {
    private final Context context;
    private final List<ConsultationCoupon> couponList;
    private final String promoCode;
    private final Application application;
    private final CouponAdapterListener couponAdapterListener;

    public CouponListAdapter(Context context, List<ConsultationCoupon> couponList, String promoCode, Application application,CouponAdapterListener couponAdapterListener) {
        this.context = context;
        this.couponList = couponList;
        this.application = application;
        this.promoCode = promoCode;
        this.couponAdapterListener=couponAdapterListener;
    }

    @NonNull
    @Override
    public CouponListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CouponListItemBinding couponListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.coupon_list_item, parent, false);
        return new CouponListHolder(couponListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponListHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ConsultationCoupon consultationCoupon = couponList.get(position);
        CouponListItemViewModel couponListItemViewModel = new CouponListItemViewModel(application);
        couponListItemViewModel.onDataAvailable(context, consultationCoupon, promoCode, holder.couponListItemBinding, this);
        holder.couponListItemBinding.setViewModel(couponListItemViewModel);
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    @Override
    public void selectedCoupon(ConsultationCoupon coupon) {
        couponAdapterListener.caSelectedCoupon(coupon);
    }

    class CouponListHolder extends RecyclerView.ViewHolder {
        private final CouponListItemBinding couponListItemBinding;

        CouponListHolder(CouponListItemBinding couponListItemBinding) {
            super(couponListItemBinding.getRoot());
            this.couponListItemBinding = couponListItemBinding;
        }
    }

    public interface CouponAdapterListener {
        void caSelectedCoupon(ConsultationCoupon coupon);
    }
}
