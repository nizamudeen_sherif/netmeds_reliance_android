package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class ConsultationCoupon {
    @SerializedName("id")
    private String id;
    @SerializedName("description")
    private String description;
    @SerializedName("value")
    private String value;
    @SerializedName("code")
    private String code;
    @SerializedName("specs")
    private String[] specs;
    @SerializedName("expiry")
    private String expiry;
    private boolean isApplied;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String[] getSpecs() {
        return specs;
    }

    public void setSpecs(String[] specs) {
        this.specs = specs;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public boolean isApplied() {
        return isApplied;
    }

    public void setApplied(boolean applied) {
        isApplied = applied;
    }
}
