package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.base.model.MessageFrom;
import com.nms.netmeds.base.model.MessageTo;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;

import java.util.ArrayList;
import java.util.List;

public class Message {
    @SerializedName("id")
    private String id;
    @SerializedName("from")
    private MessageFrom messageFrom;
    @SerializedName("to")
    private MessageTo messageTo;
    @SerializedName("type")
    private String type;
    @SerializedName("time")
    private String time;
    @SerializedName("chatId")
    private String chatId;
    @SerializedName("message")
    private String message;
    @SerializedName("fromRole")
    private String fromRole;
    @SerializedName("toRole")
    private String toRole;
    @SerializedName("hashId")
    private String hashId;
    private List<DoctorInformation> doctorList;
    private ConsultationFeeDetail feeDetail;
    private boolean isEditable;
    private boolean isShowTime;
    private String specialization;
    private String chatType;
    private int notificationBackground;
    private int notificationTextColor;
    private String problemCategoryId;
    private String multipleOptionId;
    private String multipleOptionWithTextId;
    private String imageUri;
    private String editFrom;
    private ArrayList<Medicine> medicines;
    private String doctorName;
    private RatingSubmitRequest ratingSubmitRequest;
    private RatingSubmitRequest availableRating;
    private boolean isRatingAvailable;
    private DoctorInformation followUpInChatDoctorInfo;
    private CustomerResponse customerResponse;

    public CustomerResponse getCustomerResponse() {
        return customerResponse;
    }

    public void setCustomerResponse(CustomerResponse customerResponse) {
        this.customerResponse = customerResponse;
    }

    public DoctorInformation getFollowUpInChatDoctorInfo() {
        return followUpInChatDoctorInfo;
    }

    public void setFollowUpInChatDoctorInfo(DoctorInformation followUpInChatDoctorInfo) {
        this.followUpInChatDoctorInfo = followUpInChatDoctorInfo;
    }

    public RatingSubmitRequest getAvailableRating() {
        return availableRating;
    }

    public void setAvailableRating(RatingSubmitRequest availableRating) {
        this.availableRating = availableRating;
    }

    public boolean isRatingAvailable() {
        return isRatingAvailable;
    }

    public void setRatingAvailable(boolean ratingAvailable) {
        isRatingAvailable = ratingAvailable;
    }

    public RatingSubmitRequest getRatingSubmitRequest() {
        return ratingSubmitRequest;
    }

    public void setRatingSubmitRequest(RatingSubmitRequest ratingSubmitRequest) {
        this.ratingSubmitRequest = ratingSubmitRequest;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public ArrayList<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(ArrayList<Medicine> medicines) {
        this.medicines = medicines;
    }

    public String getFromRole() {
        return fromRole;
    }

    public void setFromRole(String fromRole) {
        this.fromRole = fromRole;
    }

    public String getToRole() {
        return toRole;
    }

    public void setToRole(String toRole) {
        this.toRole = toRole;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DoctorInformation> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<DoctorInformation> doctorList) {
        this.doctorList = doctorList;
    }

    public ConsultationFeeDetail getFeeDetail() {
        return feeDetail;
    }

    public void setFeeDetail(ConsultationFeeDetail feeDetail) {
        this.feeDetail = feeDetail;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MessageFrom getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(MessageFrom messageFrom) {
        this.messageFrom = messageFrom;
    }

    public MessageTo getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(MessageTo messageTo) {
        this.messageTo = messageTo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isShowTime() {
        return isShowTime;
    }

    public void setShowTime(boolean showTime) {
        isShowTime = showTime;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public int getNotificationBackground() {
        return notificationBackground;
    }

    public void setNotificationBackground(int notificationBackground) {
        this.notificationBackground = notificationBackground;
    }

    public int getNotificationTextColor() {
        return notificationTextColor;
    }

    public void setNotificationTextColor(int notificationTextColor) {
        this.notificationTextColor = notificationTextColor;
    }

    public String getProblemCategoryId() {
        return problemCategoryId;
    }

    public void setProblemCategoryId(String problemCategoryId) {
        this.problemCategoryId = problemCategoryId;
    }

    public String getMultipleOptionId() {
        return multipleOptionId;
    }

    public void setMultipleOptionId(String multipleOptionId) {
        this.multipleOptionId = multipleOptionId;
    }

    public String getMultipleOptionWithTextId() {
        return multipleOptionWithTextId;
    }

    public void setMultipleOptionWithTextId(String multipleOptionWithTextId) {
        this.multipleOptionWithTextId = multipleOptionWithTextId;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getEditFrom() {
        return editFrom;
    }

    public void setEditFrom(String editFrom) {
        this.editFrom = editFrom;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }
}
