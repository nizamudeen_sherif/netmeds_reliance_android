package com.nms.netmeds.consultation;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseServiceManager;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.SendFcmTokenResponse;
import com.nms.netmeds.base.retrofit.BaseApiClient;
import com.nms.netmeds.base.retrofit.RetrofitAPIInterface;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.consultation.model.ApplyCouponResponse;
import com.nms.netmeds.consultation.model.ChatHistoryResponse;
import com.nms.netmeds.consultation.model.ConsultationFeeResponse;
import com.nms.netmeds.consultation.model.ConsultationUpdateResponse;
import com.nms.netmeds.consultation.model.CreateChatResponse;
import com.nms.netmeds.consultation.model.CreateConsultationResponse;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.GetAllCouponResponse;
import com.nms.netmeds.consultation.model.HistoryRequest;
import com.nms.netmeds.consultation.model.HistoryResponse;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.consultation.model.PaymentCreationResponse;
import com.nms.netmeds.consultation.model.ReoccurUpdateResponse;
import com.nms.netmeds.consultation.model.SpecialityResponse;
import com.nms.netmeds.consultation.model.UploadAttachmentResponse;
import com.nms.netmeds.consultation.model.request.ApplyCouponRequest;
import com.nms.netmeds.consultation.model.request.ChatHistoryRequest;
import com.nms.netmeds.consultation.model.request.ConsultationFeeRequest;
import com.nms.netmeds.consultation.model.request.ConsultationUpdateRequest;
import com.nms.netmeds.consultation.model.request.CreateChatRequest;
import com.nms.netmeds.consultation.model.request.CreateConsultationRequest;
import com.nms.netmeds.consultation.model.request.DoctorInfoRequest;
import com.nms.netmeds.consultation.model.request.DoctorListRequest;
import com.nms.netmeds.consultation.model.request.LoginRequest;
import com.nms.netmeds.consultation.model.request.PaymentCreationRequest;
import com.nms.netmeds.consultation.model.request.PrescriptionDetailsRequest;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;
import com.nms.netmeds.consultation.model.request.ReoccurUpdateRequest;
import com.nms.netmeds.consultation.vm.ChatViewModel;
import com.nms.netmeds.consultation.vm.HistoryViewModel;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConsultationServiceManager extends BaseServiceManager {

    private static ConsultationServiceManager consultationServiceManager;
    public final static int TRANSACTION_LOGIN = 301;
    public final static int TRANSACTION_HISTORY = 302;
    public final static int DOCTOR_INFO = 303;
    public final static int DOWNLOAD_PRESCRIPTION = 304;
    public final static int CREATE_CHAT = 305;
    public final static int UPDATE_CONSULTATION = 306;
    public final static int GET_ALL_SPECIALITY = 307;
    public final static int SPECIALIZATION_UPDATE = 308;
    public final static int FOLLOW_UP = 309;
    public final static int DOCTOR_LIST = 310;
    public final static int CREATE_CONSULTATION = 311;
    public final static int CONSULTATION_MODE_UPDATE = 312;
    public final static int CONSULTATION_FEE = 313;
    public final static int GET_ALL_COUPON = 314;
    public final static int APPLY_COUPON = 315;
    public final static int UPLOAD_ATTACHMENT = 316;
    public final static int UPDATE_COUPON = 317;
    public final static int PAYMENT_CREATION = 318;
    public final static int PAYMENT_UPDATE = 319;
    public final static int REOCCUR_UPDATE = 320;
    public final static int CHAT_HISTORY = 321;
    public final static int LOGAN_TOKEN = 322;
    public final static int MEDICINE = 323;
    public final static int ORDER_MEDICINE = 324;
    public static final int RE_ORDER_STOCK_INFO = 153;
    public static final int PATIENT_DETAILS = 154;
    public static final int BANNER_DETAILS = 325;
    public final static int SYMPTOM_UPDATE = 308;

    public static final int PAYMENT_LIST = 207;
    public final static int ORDER_FROM_PRESCRIPTION = 564;
    public static final int C_MSTAR_PAYMENT_LIST = 50200;

    public static ConsultationServiceManager getInstance() {
        if (consultationServiceManager == null)
            consultationServiceManager = new ConsultationServiceManager();
        return consultationServiceManager;
    }

    public <T extends AppViewModel> void consultationDoLogin(final T viewModel, LoginRequest consultationLoginRequest, String baseUrl) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(baseUrl).create(RetrofitConsultInterface.class);
        Call<LoginResponse> call = apiService.doLogin(consultationLoginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), TRANSACTION_LOGIN);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(TRANSACTION_LOGIN, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), TRANSACTION_LOGIN);
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(TRANSACTION_LOGIN, null);
            }
        });
    }

    public void consultationHistory(final HistoryViewModel historyViewModel, HistoryRequest consultationHistoryRequest, BasePreference basePreference, int pageIndex, int pageLimit) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<HistoryResponse> call = apiService.verifyUser(getHeader(basePreference), consultationHistoryRequest, pageIndex, pageLimit);
        call.enqueue(new Callback<HistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<HistoryResponse> call, @NonNull Response<HistoryResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    historyViewModel.onSyncData(new Gson().toJson(response.body()), TRANSACTION_HISTORY);
                } else if (checkErrorCode(response.code())) {
                    historyViewModel.onFailed(TRANSACTION_HISTORY, null);
                } else {
                    historyViewModel.onSyncData(errorHandling(response.errorBody()), TRANSACTION_HISTORY);
                }
            }

            @Override
            public void onFailure(@NonNull Call<HistoryResponse> call, @NonNull Throwable t) {
                historyViewModel.onFailed(TRANSACTION_HISTORY, null);
            }
        });
    }

    private Map<String, String> getHeader(BasePreference basePreference) {
        JustDocUserResponse consultationLoginResult = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
        String justDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
        String sessionId = consultationLoginResult != null && consultationLoginResult.getSession() != null && consultationLoginResult.getSession().getId() != null ? consultationLoginResult.getSession().getId() : "";
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Bearer " + justDocToken);
        header.put("session", sessionId);
        return header;
    }

    public <T extends AppViewModel> void getDoctorInfo(final T viewModel, final DoctorInfoRequest doctorInfoRequest) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<DoctorListResponse> call = apiService.doctorInfo(doctorInfoRequest);
        call.enqueue(new Callback<DoctorListResponse>() {
            @Override
            public void onResponse(@NonNull Call<DoctorListResponse> call, @NonNull Response<DoctorListResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DOCTOR_INFO);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(DOCTOR_INFO, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), DOCTOR_INFO);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DoctorListResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DOCTOR_INFO, null);
            }
        });
    }

    public void createChat(final ChatViewModel chatViewModel, CreateChatRequest createChatRequest) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<CreateChatResponse> call = apiService.createChat(createChatRequest);
        call.enqueue(new Callback<CreateChatResponse>() {
            @Override
            public void onResponse(@NonNull Call<CreateChatResponse> call, @NonNull Response<CreateChatResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), CREATE_CHAT);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(CREATE_CHAT, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), CREATE_CHAT);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CreateChatResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(CREATE_CHAT, null);
            }
        });
    }

    public <T extends AppViewModel> void updateConsultation(final T viewModel, BasePreference basePreference, ConsultationUpdateRequest consultationUpdateRequest, final String paymentStatus, final String updateType, final int apiTransactionId) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<ConsultationUpdateResponse> call = apiService.updateConsultation(getHeader(basePreference), consultationUpdateRequest);
        call.enqueue(new Callback<ConsultationUpdateResponse>() {
            @Override
            public void onResponse(@NonNull Call<ConsultationUpdateResponse> call, @NonNull Response<ConsultationUpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    response.body().setPaymentStatus(paymentStatus);
                    response.body().setUpdateType(updateType);
                    viewModel.onSyncData(new Gson().toJson(response.body()), UPDATE_CONSULTATION);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(apiTransactionId, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), UPDATE_CONSULTATION);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ConsultationUpdateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(apiTransactionId, null);
            }
        });
    }

    public void getAllSpecialization(final ChatViewModel chatViewModel) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<SpecialityResponse> call = apiService.getAllSpeciality();
        call.enqueue(new Callback<SpecialityResponse>() {
            @Override
            public void onResponse(@NonNull Call<SpecialityResponse> call, @NonNull Response<SpecialityResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), GET_ALL_SPECIALITY);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(GET_ALL_SPECIALITY, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), GET_ALL_SPECIALITY);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SpecialityResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(GET_ALL_SPECIALITY, null);
            }
        });
    }

    public void getDoctorList(final ChatViewModel chatViewModel, DoctorListRequest doctorListRequest) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<DoctorListResponse> call = apiService.doctorList(doctorListRequest);
        call.enqueue(new Callback<DoctorListResponse>() {
            @Override
            public void onResponse(@NonNull Call<DoctorListResponse> call, @NonNull Response<DoctorListResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), DOCTOR_LIST);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(DOCTOR_LIST, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), DOCTOR_LIST);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DoctorListResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(DOCTOR_LIST, null);
            }
        });
    }


    public void getPrescriptionDetails(final ChatViewModel chatViewModel, BasePreference basePreference, PrescriptionDetailsRequest prescriptionDetailsRequest, String followUp) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<JsonArray> call = apiService.getPrescriptionDetails(getHeader(basePreference), prescriptionDetailsRequest, followUp);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(@NonNull Call<JsonArray> call, @NonNull Response<JsonArray> response) {
                try {
                    if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {
                        chatViewModel.onSyncData(response.body().toString(), MEDICINE);
                    } else if (checkErrorCode(response.code())) {
                        chatViewModel.onFailed(MEDICINE, null);
                    } else {
                        chatViewModel.onSyncData(errorHandling(response.errorBody()), MEDICINE);
                    }
                } catch (Exception e) {
                    chatViewModel.onFailed(MEDICINE, null);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonArray> call, @NonNull Throwable t) {
                chatViewModel.onFailed(MEDICINE, null);
            }
        });
    }

    public void getBanner(final HistoryViewModel historyViewModel) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<JsonObject> call = apiService.getBanner();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {
                        historyViewModel.onSyncData(response.body().toString(), BANNER_DETAILS);
                    } else if (checkErrorCode(response.code())) {
                        historyViewModel.onFailed(BANNER_DETAILS, null);
                    } else {
                        historyViewModel.onSyncData(errorHandling(response.errorBody()), BANNER_DETAILS);
                    }
                } catch (Exception e) {
                    historyViewModel.onFailed(BANNER_DETAILS, null);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                historyViewModel.onFailed(BANNER_DETAILS, null);

            }
        });
    }

    public void createConsultation(final ChatViewModel chatViewModel, CreateConsultationRequest createConsultationRequest, BasePreference basePreference) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<CreateConsultationResponse> call = apiService.createConsultation(getHeader(basePreference), createConsultationRequest);
        call.enqueue(new Callback<CreateConsultationResponse>() {
            @Override
            public void onResponse(@NonNull Call<CreateConsultationResponse> call, @NonNull Response<CreateConsultationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), CREATE_CONSULTATION);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(CREATE_CONSULTATION, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), CREATE_CONSULTATION);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CreateConsultationResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(CREATE_CONSULTATION, null);
            }
        });
    }

    public void consultationFee(final ChatViewModel chatViewModel, ConsultationFeeRequest consultationFeeRequest) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<ConsultationFeeResponse> call = apiService.consultationFee(consultationFeeRequest);
        call.enqueue(new Callback<ConsultationFeeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ConsultationFeeResponse> call, @NonNull Response<ConsultationFeeResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (chatViewModel != null)
                        chatViewModel.onSyncData(new Gson().toJson(response.body()), CONSULTATION_FEE);

                } else if (checkErrorCode(response.code())) {
                    if (chatViewModel != null)
                        chatViewModel.onFailed(CONSULTATION_FEE, null);

                } else {
                    if (chatViewModel != null)
                        chatViewModel.onSyncData(errorHandling(response.errorBody()), CONSULTATION_FEE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ConsultationFeeResponse> call, @NonNull Throwable t) {
                if (chatViewModel != null)
                    chatViewModel.onFailed(CONSULTATION_FEE, null);

            }
        });
    }

    public <T extends AppViewModel> void getAllCoupon(final T chatViewModel, Map<String, String> header) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<GetAllCouponResponse> call = apiService.allCoupon(header);
        call.enqueue(new Callback<GetAllCouponResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAllCouponResponse> call, @NonNull Response<GetAllCouponResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), GET_ALL_COUPON);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(GET_ALL_COUPON, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), GET_ALL_COUPON);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAllCouponResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(GET_ALL_COUPON, null);
            }
        });
    }

    public <T extends AppViewModel> void applyCoupon(final T chatViewModel, ApplyCouponRequest applyCouponRequest) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<ApplyCouponResponse> call = apiService.applyCoupon(applyCouponRequest);
        call.enqueue(new Callback<ApplyCouponResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApplyCouponResponse> call, @NonNull Response<ApplyCouponResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), APPLY_COUPON);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(APPLY_COUPON, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), APPLY_COUPON);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApplyCouponResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(APPLY_COUPON, null);
            }
        });
    }

    public void uploadAttachment(final ChatViewModel chatViewModel, RequestBody requestBody, final String path, BasePreference basePreference) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<UploadAttachmentResponse> call = apiService.attachmentUpload(getHeader(basePreference), requestBody);
        call.enqueue(new Callback<UploadAttachmentResponse>() {
            @Override
            public void onResponse(@NonNull Call<UploadAttachmentResponse> call, @NonNull Response<UploadAttachmentResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    response.body().setUploadUrl(path);
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), UPLOAD_ATTACHMENT);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(UPLOAD_ATTACHMENT, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), UPLOAD_ATTACHMENT);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UploadAttachmentResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(UPLOAD_ATTACHMENT, null);
            }
        });
    }

    public <T extends AppViewModel> void paymentCreation(final T viewModel, PaymentCreationRequest paymentCreationRequest, BasePreference basePreference, final String paymentStatus) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<PaymentCreationResponse> call = apiService.paymentCreation(getHeader(basePreference), paymentCreationRequest);
        call.enqueue(new Callback<PaymentCreationResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentCreationResponse> call, @NonNull Response<PaymentCreationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    response.body().setPaymentStatus(paymentStatus);
                    viewModel.onSyncData(new Gson().toJson(response.body()), PAYMENT_CREATION);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(PAYMENT_CREATION, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), PAYMENT_CREATION);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentCreationResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(PAYMENT_CREATION, null);
            }
        });
    }

    public <T extends AppViewModel> void reoccurUpdate(final T viewModel, ReoccurUpdateRequest reoccurUpdateRequest, BasePreference basePreference) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<ReoccurUpdateResponse> call = apiService.reoccurUpdate(getHeader(basePreference), reoccurUpdateRequest);
        call.enqueue(new Callback<ReoccurUpdateResponse>() {
            @Override
            public void onResponse(@NonNull Call<ReoccurUpdateResponse> call, @NonNull Response<ReoccurUpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), REOCCUR_UPDATE);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(REOCCUR_UPDATE, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), REOCCUR_UPDATE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReoccurUpdateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(REOCCUR_UPDATE, null);
            }
        });
    }

    public void chatHistory(final ChatViewModel chatViewModel, ChatHistoryRequest chatHistoryRequest, BasePreference basePreference) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<ChatHistoryResponse> call = apiService.chatHistory(getHeader(basePreference), chatHistoryRequest);
        call.enqueue(new Callback<ChatHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ChatHistoryResponse> call, @NonNull Response<ChatHistoryResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    chatViewModel.onSyncData(new Gson().toJson(response.body()), CHAT_HISTORY);
                } else if (checkErrorCode(response.code())) {
                    chatViewModel.onFailed(CHAT_HISTORY, null);
                } else {
                    chatViewModel.onSyncData(errorHandling(response.errorBody()), CHAT_HISTORY);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChatHistoryResponse> call, @NonNull Throwable t) {
                chatViewModel.onFailed(CHAT_HISTORY, null);
            }
        });
    }

    public <T extends AppViewModel> void getLoganToken(final T viewModel, Map<String, String> header) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitConsultInterface.class);
        Call<LoganResponse> call = apiService.loganToken(header);
        call.enqueue(new Callback<LoganResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoganResponse> call, @NonNull Response<LoganResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), LOGAN_TOKEN);
                } else if (checkErrorCode(response.code())) {
                    viewModel.onFailed(LOGAN_TOKEN, null);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), LOGAN_TOKEN);
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoganResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(LOGAN_TOKEN, null);
            }
        });
    }

    public void sendFcmTokenToServer(SendFcmTokenRequest sendFcmTokenRequest, final BasePreference basePreference) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<SendFcmTokenResponse> call = apiService.sendFcmToken(getHeader(basePreference), sendFcmTokenRequest);
        call.enqueue(new Callback<SendFcmTokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<SendFcmTokenResponse> call, @NonNull Response<SendFcmTokenResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    basePreference.setFcmTokenRefresh(false);
                    Log.d("SendToken", "success");
                }
            }

            @Override
            public void onFailure(@NonNull Call<SendFcmTokenResponse> call, @NonNull Throwable t) {
                Log.e("SendToken", "error");
            }
        });
    }

    public void submitRating(RatingSubmitRequest request, BasePreference basePreference) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<JsonObject> call = apiService.submitRatingToService(getHeader(basePreference), request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
            }
        });
    }

    public <T extends AppViewModel> void getPatientDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarCustomerDetails(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), PATIENT_DETAILS);
                } else {
                    viewModel.onFailed(PATIENT_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(PATIENT_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void trackOrderMedicine(String consultationId, String authToken) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(ConsultationConstant.CONSULTATION_ID, consultationId);
        jsonObject.addProperty(ConsultationConstant.MOBILE, true);
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL), authToken).create(RetrofitConsultInterface.class);
        Call<JsonObject> call = apiService.orderFromPrescription(jsonObject);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
            }
        });
    }


    private boolean checkErrorCode(int errorCode) {
        return errorCode == 502 || errorCode == 500;
    }


    public <T extends AppViewModel> void getConsultationPaymentList(final T paymentViewModel, String authToken, BasePreference basePreference, String grandTotal, String userId, String appVersion) {
        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty(AppConstant.ORDER_VALUE, grandTotal);
            jsonObject.addProperty(AppConstant.BOOMRANG_AUTH_TOKEN, authToken);
            jsonObject.addProperty(AppConstant.APP_SOURCE, AppConstant.ANDROID);
            jsonObject.addProperty(AppConstant.APP_VERSION, appVersion);
            jsonObject.addProperty(AppConstant.AMAZON_FLAG, AppConstant.FALSE);
            jsonObject.addProperty(AppConstant.MSTAR_API_HEADER_USER_ID, userId);
        } catch (JsonIOException e) {
            e.printStackTrace();
        }
        Call<MStarBasicResponseTemplateModel> call = apiService.getConsultationPaymentList(jsonObject, getHeader(basePreference));
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null)
                    paymentViewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAYMENT_LIST);
                else
                    paymentViewModel.onSyncData(errorHandling(response.errorBody()), C_MSTAR_PAYMENT_LIST);
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                paymentViewModel.onFailed(C_MSTAR_PAYMENT_LIST, null);
            }
        });
    }


    public void pushUnRegisterFromConsultationServer(SendFcmTokenRequest sendFcmTokenRequest, final BasePreference basePreference) {
        JustDocUserResponse consultationLoginResult = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
        String justDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
        String sessionId = consultationLoginResult != null && consultationLoginResult.getSession() != null && consultationLoginResult.getSession().getId() != null ? consultationLoginResult.getSession().getId() : "";
        Map<String, String> header = new HashMap<>();
        header.put(AppConstant.KEY_AUTHORIZATION, AppConstant.KEY_BEARER + justDocToken);
        header.put(AppConstant.KEY_SESSION, sessionId);

        RetrofitConsultInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitConsultInterface.class);
        Call<SendFcmTokenResponse> call = apiService.pushUnRegisterFromConsultation(header, sendFcmTokenRequest);
        call.enqueue(new Callback<SendFcmTokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<SendFcmTokenResponse> call, @NonNull Response<SendFcmTokenResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    basePreference.setFcmTokenRefresh(true);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SendFcmTokenResponse> call, @NonNull Throwable t) {
            }
        });
    }

}
