package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FollowUp implements Serializable {
    @SerializedName("symptoms")
    private String symptoms;

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }
}
