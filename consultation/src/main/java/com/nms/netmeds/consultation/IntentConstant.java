package com.nms.netmeds.consultation;

public class IntentConstant {
    public static final String CONSULTATION_HISTORY = "CONSULTATION_HISTORY";
    public static final String VIEW_PRESCRIPTION = "VIEW_PRESCRIPTION";
    public static final String RATING = "RATING";
    public static final String CUSTOMER_DETAIL = "CUSTOMER_DETAIL";
    public static final String PRESCRIPTION_BASE_URL = "PRESCRIPTION_BASE_URL";
    public static final String INTENT_FIRST_CONSULTATION = "INTENT_FIRST_CONSULTATION";
    public static String IS_FROM_CONSULTATION_TO_CART = "IS_FROM_CONSULTATION_TO_CART";
}
