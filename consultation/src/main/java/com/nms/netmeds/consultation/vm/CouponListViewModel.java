package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.adapter.CouponListAdapter;
import com.nms.netmeds.consultation.databinding.DialogCouponListBinding;
import com.nms.netmeds.consultation.model.ConsultationCoupon;

import java.util.List;

public class CouponListViewModel extends AppViewModel implements CouponListAdapter.CouponAdapterListener {
    private final Application application;
    private CouponListItemListener couponListItemListener;


    public CouponListViewModel(Application application) {
        super(application);
        this.application = application;
    }

    public void onDataAvailable(Context context, List<ConsultationCoupon> promoCodeList, String appliedPromoCode, DialogCouponListBinding dialogCouponListBinding, CouponListItemListener couponListItemListener) {
        this.couponListItemListener = couponListItemListener;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        dialogCouponListBinding.couponList.setLayoutManager(linearLayoutManager);
        CouponListAdapter couponListAdapter = new CouponListAdapter(context, promoCodeList, appliedPromoCode, application,this);
        dialogCouponListBinding.couponList.setAdapter(couponListAdapter);
    }

    public void onCouponListClose() {
        couponListItemListener.couponListClose();
    }

    @Override
    public void caSelectedCoupon(ConsultationCoupon coupon) {
        couponListItemListener.selectedCouponCode(coupon);
    }

    public interface CouponListItemListener {
        void couponListClose();

        void selectedCouponCode(ConsultationCoupon coupon);
    }
}
