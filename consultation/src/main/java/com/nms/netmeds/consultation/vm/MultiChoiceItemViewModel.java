package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.MultiChoiceItemBinding;
import com.nms.netmeds.consultation.model.Options;

public class MultiChoiceItemViewModel extends AppViewModel {
    private Options options;
    private int selectedPosition;
    private MultiChoiceItemListener multiChoiceItemListener;

    public MultiChoiceItemViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, Options choice, MultiChoiceItemBinding multiChoiceItemBinding, int position, MultiChoiceItemListener multiChoiceItemListener) {
        this.options = choice;
        this.selectedPosition = position;
        this.multiChoiceItemListener = multiChoiceItemListener;
        if (options.isChecked()) {
            multiChoiceItemBinding.lytMultiChoiceItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pale_blue_grey_curved_rectangle));
            multiChoiceItemBinding.textMultiChoice.setTextColor(context.getResources().getColor(R.color.colorDeepAqua));
        } else {
            multiChoiceItemBinding.lytMultiChoiceItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.primary_curved_rectangle));
            multiChoiceItemBinding.textMultiChoice.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
        }
    }

    public String choice() {
        return options != null && !TextUtils.isEmpty(options.getOption()) ? options.getOption() : "";
    }

    public void onOptionSelection() {
        if (options != null && options.getOption() != null && options.getOption().equalsIgnoreCase(ConsultationConstant.OTHER))
            multiChoiceItemListener.onOtherOption();
        else
            multiChoiceItemListener.onOptionSelected(selectedPosition);
    }

    public interface MultiChoiceItemListener {
        void onOptionSelected(int selectedPosition);

        void onOtherOption();
    }
}
