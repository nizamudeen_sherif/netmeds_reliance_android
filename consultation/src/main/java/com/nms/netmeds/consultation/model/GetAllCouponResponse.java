package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

import java.util.List;

public class GetAllCouponResponse extends BaseResponse {
    @SerializedName("result")
    private List<ConsultationCoupon> couponList;
    @SerializedName("updatedOn")
    private String updatedOn;

    public List<ConsultationCoupon> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<ConsultationCoupon> couponList) {
        this.couponList = couponList;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
