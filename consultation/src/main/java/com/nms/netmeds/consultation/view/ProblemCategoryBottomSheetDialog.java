package com.nms.netmeds.consultation.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DialogProblemCategoryBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.vm.ProblemCategoryViewModel;

import java.util.List;

@SuppressLint("ValidFragment")
public class ProblemCategoryBottomSheetDialog extends BaseDialogFragment implements ProblemCategoryViewModel.ProblemCategoryViewModelListener {
    private final List<Options> problemCategoryList;
    private final MessageAdapterListener messageAdapterListener;

    @SuppressLint("ValidFragment")
    public ProblemCategoryBottomSheetDialog(List<Options> problemCategoryList, MessageAdapterListener messageAdapterListener) {
        this.problemCategoryList = problemCategoryList;
        this.messageAdapterListener = messageAdapterListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogProblemCategoryBinding dialogProblemCategoryBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_problem_category, container, false);
        ProblemCategoryViewModel problemCategoryViewModel = new ProblemCategoryViewModel(getActivity().getApplication());
        problemCategoryViewModel.onDataAvailable(getActivity(), problemCategoryList, dialogProblemCategoryBinding, this);
        dialogProblemCategoryBinding.setViewModel(problemCategoryViewModel);
        return dialogProblemCategoryBinding.getRoot();
    }

    @Override
    public void pvmSelectedProblem(Options problemCategory) {
        messageAdapterListener.selectedProblem(problemCategory);
        ProblemCategoryBottomSheetDialog.this.dismissAllowingStateLoss();
    }

    @Override
    public void pvmProblemCategoryClose() {
        ProblemCategoryBottomSheetDialog.this.dismissAllowingStateLoss();
    }
}
