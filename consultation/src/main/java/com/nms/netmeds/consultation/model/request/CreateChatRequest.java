package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class CreateChatRequest {
    @SerializedName("id")
    private String id;
    @SerializedName("userId")
    private String userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
