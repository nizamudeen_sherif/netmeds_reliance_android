package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class ConsultationUpdateRequest {
    @SerializedName("type")
    private String type;
    @SerializedName("userId")
    private String userId;
    @SerializedName("payload")
    private ConsultationUpdatePayload consultingPayload;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ConsultationUpdatePayload getConsultingPayload() {
        return consultingPayload;
    }

    public void setConsultingPayload(ConsultationUpdatePayload consultingPayload) {
        this.consultingPayload = consultingPayload;
    }
}
