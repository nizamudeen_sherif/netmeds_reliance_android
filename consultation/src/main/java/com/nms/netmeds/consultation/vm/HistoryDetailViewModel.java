package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultAPIPathConstant;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.ConsultationUtil;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ActivityHistoryDetailBinding;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.LabTest;
import com.nms.netmeds.consultation.model.request.DoctorInfoRequest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class HistoryDetailViewModel extends AppViewModel {
    private HistoryResult historyResult;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityHistoryDetailBinding historyDetailBinding;
    private int failedTransactionId;
    private int doctorId;
    private final MutableLiveData<DoctorListResponse> doctorInfoMutableLiveData = new MutableLiveData<>();
    private DoctorInformation doctorInformation;
    private HistoryDetailListener historyDetailListener;
    private ConfigMap configMap;
    private String consultationId = "";
    private Intent intent;
    private HistoryDetailViewModel historyDetailViewModel;
    private BasePreference basePreference;

    public HistoryDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(final Context context, HistoryDetailViewModel historyDetailViewModel, HistoryResult historyResult, ActivityHistoryDetailBinding activityHistoryDetailBinding, HistoryDetailListener historyDetailListener) {
        this.historyResult = historyResult;
        this.context = context;
        this.historyDetailListener = historyDetailListener;
        this.historyDetailBinding = activityHistoryDetailBinding;
        this.historyDetailViewModel = historyDetailViewModel;
        basePreference = BasePreference.getInstance(context);
        configMap = ConfigMap.getInstance();
        consultationId = historyResult != null && historyResult.getId() != null && !TextUtils.isEmpty(historyResult.getId()) ? historyResult.getId() : "";
        if (historyResult != null && TextUtils.isEmpty(historyResult.getDoctorId()))
            setDoctorImage();
        showDoctorName();
        showSpecialization();
        showRating();
        isShowConsultationType();
        isShowStatus();
        isShowDateTime();
        showDownloadPrescription();
    }

    public MutableLiveData<DoctorListResponse> getDoctorInfoMutableLiveData() {
        return doctorInfoMutableLiveData;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == ConsultationServiceManager.DOCTOR_INFO) {
            DoctorListResponse doctorInfoResponse = new Gson().fromJson(data, DoctorListResponse.class);
            doctorInfoMutableLiveData.setValue(doctorInfoResponse);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        historyDetailListener.vmDismissProgress();
        if (transactionId == ConsultationServiceManager.DOCTOR_INFO)
            vmOnError(transactionId);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case ConsultationServiceManager.DOCTOR_INFO:
                getDoctorInfo(doctorId);
                break;
            case ConsultationServiceManager.DOWNLOAD_PRESCRIPTION:
                onDownloadPrescription();
                break;
        }
    }

    public String status() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getStatus()) ? CommonUtils.convertToTitleCase(historyResult.getStatus()) : "";
    }

    public String dateTime() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getCreationDate()) ? DateTimeUtils.getInstance().utcToLocal(DateTimeUtils.yyyyMMddTHHmmssSSS, DateTimeUtils.ddMMMhhmma, historyResult.getCreationDate()) : "";
    }

    public String consultationType() {
        return historyResult != null && !TextUtils.isEmpty(historyResult.getMode()) ? CommonUtils.convertToTitleCase(historyResult.getMode()) : "";
    }

    public String doctorName() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getName()) ? doctorInformation.getName() : context.getResources().getString(R.string.text_medi_bot);
    }

    private String doctorExperience() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getExperience()) ? String.format("%s %s %s", "|", doctorInformation.getExperience(), context.getString(R.string.text_doctor_experience)) : "";
    }

    private String doctorSpeciality() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getSpecialityTitle()) ? doctorInformation.getSpecialityTitle() : "";
    }

    public String specialityAndExperience() {
        return String.format("%s %s", doctorSpeciality(), doctorExperience());
    }

    public String rating() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getRating()) ? String.format("%s %s%s%s", context.getString(R.string.text_rated), doctorInformation.getRating(), "/", ConsultationConstant.RATING_VALUE) : "";
    }

    public float ratingValue() {
        return doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getRating()) ? Float.parseFloat(doctorInformation.getRating()) : 0;
    }

    private String imageUrl() {
        String url = "";
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getImage())) {
            url = configMap.getProperty(ConfigConstant.CONSULTATION_IMAGE_URL) + doctorInformation.getImage();
        }
        return url;
    }

    private int gender() {
        int gender = 0;
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getGender())) {
            if (doctorInformation.getGender().equals(ConsultationConstant.MALE)) {
                gender = R.drawable.ic_default_doctor_male;
            } else {
                gender = R.drawable.ic_default_doctor_female;
            }
        }
        return gender;
    }

    public String setFollowUpOrConsultAgainText() {
        return historyResult != null && !historyResult.isConsultationExpiry() && historyResult.getMode().equalsIgnoreCase(ConsultationConstant.PARAM_AUDIO) && !TextUtils.isEmpty(historyResult.getEndTime()) && historyResult.getFollowUp() == null ? context.getResources().getString(R.string.text_follow_up) : context.getResources().getString(R.string.text_consult_again);
    }

    public void showDoctorName() {
        if(TextUtils.isEmpty(doctorName())){
            historyDetailBinding.txtDoctorName.setVisibility(View.GONE);
        }else{
            historyDetailBinding.txtDoctorName.setVisibility(View.VISIBLE);
        }
    }

    public void showSpecialization() {
        if(TextUtils.isEmpty(doctorSpeciality())){
            historyDetailBinding.txtSpecialisation.setVisibility(View.GONE);
        }else{
            historyDetailBinding.txtSpecialisation.setVisibility(View.VISIBLE);
        }
    }

    public void showRating() {
        if(TextUtils.isEmpty(rating())){
            historyDetailBinding.doctorRating.setVisibility(View.GONE);
            historyDetailBinding.txtRatingValue.setVisibility(View.GONE);
        }else{
            historyDetailBinding.doctorRating.setVisibility(View.VISIBLE);
            historyDetailBinding.txtRatingValue.setVisibility(View.VISIBLE);
        }
    }

    public void isShowConsultationType() {
        if(TextUtils.isEmpty(consultationType())){
            historyDetailBinding.lLConsultationType.setVisibility(View.GONE);
        }else{
            historyDetailBinding.lLConsultationType.setVisibility(View.VISIBLE);
        }
    }

    public void isShowDateTime() {
        if(TextUtils.isEmpty(dateTime())){
            historyDetailBinding.lLayoutShowTime.setVisibility(View.GONE);
        }else{
            historyDetailBinding.lLayoutShowTime.setVisibility(View.VISIBLE);
        }
    }

    public void isShowStatus() {
        if(TextUtils.isEmpty(status())){
            historyDetailBinding.lLayoutStatus.setVisibility(View.GONE);
        }else{
            historyDetailBinding.lLayoutStatus.setVisibility(View.VISIBLE);
        }
    }

    public void showDownloadPrescription() {
        if(historyResult != null && !TextUtils.isEmpty(historyResult.getPrescriptionStatus()) && historyResult.getPrescriptionStatus().equals(ConsultationConstant.PRESCRIPTION_STATUS)){
            historyDetailBinding.lytDownloadPrescription.setVisibility(View.VISIBLE);
            historyDetailBinding.view.setVisibility(View.VISIBLE);
        }else{
            historyDetailBinding.lytDownloadPrescription.setVisibility(View.GONE);
            historyDetailBinding.view.setVisibility(View.GONE);
        }
    }

    public void getDoctorInfo(int doctorId) {
        this.doctorId = doctorId;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            DoctorInfoRequest doctorInfoRequest = new DoctorInfoRequest();
            List<Integer> doctorIdList = new ArrayList<>();
            doctorIdList.add(doctorId);
            doctorInfoRequest.setDoctorId(doctorIdList);
            historyDetailListener.vmShowProgress();
            ConsultationServiceManager.getInstance().getDoctorInfo(this, doctorInfoRequest);
        } else {
            failedTransactionId = ConsultationServiceManager.DOCTOR_INFO;
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        historyDetailBinding.lytHistoryDetailsViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        historyDetailBinding.historyDetailsNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        historyDetailBinding.lytHistoryDetailsViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        historyDetailBinding.historyDetailsApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    public void onDoctorInfoDataAvailable(DoctorListResponse doctorInfoResponse) {
        historyDetailListener.vmDismissProgress();
        if (doctorInfoResponse != null && doctorInfoResponse.getServiceStatus() != null && doctorInfoResponse.getServiceStatus().getStatusCode() != null && doctorInfoResponse.getServiceStatus().getStatusCode() == 200) {
            if (doctorInfoResponse.getDoctorList() != null && doctorInfoResponse.getDoctorList().size() > 0) {
                doctorInformation = doctorInfoResponse.getDoctorList().get(0);
                setDoctorImage();
                historyDetailBinding.setViewModel(historyDetailBinding.getViewModel());
                showDoctorName();
                showSpecialization();
                showRating();
                isShowConsultationType();
                isShowStatus();
                isShowDateTime();
                showDownloadPrescription();
            }
        } else {
            if (doctorInfoResponse != null && doctorInfoResponse.getServiceStatus() != null && !TextUtils.isEmpty(doctorInfoResponse.getServiceStatus().getMessage()))
                SnackBarHelper.snackBarCallBack(historyDetailBinding.lytHistoryDetailsViewContent, context, doctorInfoResponse.getServiceStatus().getMessage());
        }
    }

    public interface HistoryDetailListener {

        void vmShowProgress();

        void vmDismissProgress();

        void vmCallBackOnChat(Intent intent);

        void vmDownloadPrescription();

        void vmCallBackOnHelp();

        Context getContext();
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    public void onConsultAgain() {
        Intent intent = new Intent();
        if (!historyResult.isConsultationExpiry() && historyResult.getMode().equalsIgnoreCase(ConsultationConstant.PARAM_AUDIO) && !TextUtils.isEmpty(historyResult.getEndTime()) && historyResult.getFollowUp() == null) {
            String specialization = historyResult != null && historyResult.getSpeciality() != null && historyResult.getSpeciality().getCode() != null ? historyResult.getSpeciality().getCode() : "";
            Bundle bundle = new Bundle();
            bundle.putString(ConsultationConstant.KEY_CHAT_ID, getChatId());
            bundle.putString(ConsultationConstant.KEY_DOCTOR_ID, historyResult.getDoctorId());
            bundle.putString(ConsultationConstant.KEY_USER_ID, historyResult.getUserId());
            bundle.putString(ConsultationConstant.KEY_SPECIALIZATION, specialization);
            bundle.putString(ConsultationConstant.KEY_SYMPTOM, historyResult.getSymptoms());
            bundle.putString(ConsultationConstant.KEY_CONSULTATION_ID, consultationId);
            bundle.putString(ConsultationConstant.KEY_INTENT_FROM, ConsultationConstant.FOLLOW_UP);
            intent.putExtras(bundle);
        } else {
            intent.putExtra(ConsultationConstant.KEY_INTENT_FROM, "");
        }

        WebEngageModel webEngageModel = new WebEngageModel();
        MStarCustomerDetails customerDetails = new Gson().fromJson(BasePreference.getInstance(context).getCustomerDetails(), MStarCustomerDetails.class);
        webEngageModel.setMStarUserDetails(customerDetails);
        webEngageModel.setCommonProperties();
        // "patient identified" not available from api here
        if (historyResult != null) {
            if (historyResult.getSpeciality() != null && historyResult.getSpeciality().getName() != null
                    && !TextUtils.isEmpty(historyResult.getSpeciality().getName()))
                webEngageModel.setSpeciality(historyResult.getSpeciality().getName());
            if (!TextUtils.isEmpty(historyResult.getSymptoms()))
                webEngageModel.setSymptoms(historyResult.getSymptoms());
            // "mode" not available from api here
            if (!TextUtils.isEmpty(historyResult.getMode()))
                webEngageModel.setMode(historyResult.getMode());
            if (!TextUtils.isEmpty(historyResult.getPrescriptionStatus()))
                webEngageModel.setPrescriptionStatus(historyResult.getPrescriptionStatus());
            try {
                if (historyResult.getTestJson() != null && !historyResult.getTestJson().isJsonNull()) {
                    Type type = new TypeToken<List<LabTest>>() {
                    }.getType();
                    List<LabTest> labTests = new Gson().fromJson(historyResult.getTestJson().getAsJsonObject().getAsJsonArray(context.getString(R.string.lab_tests)).toString(), type);
                    webEngageModel.setLabTestPrescribed(context.getResources().getString(R.string.txt_web_engage_consultation_yes));
                    webEngageModel.setLabTestName(ConsultationUtil.getLabTestName(labTests));
                } else {
                    webEngageModel.setLabTestPrescribed(context.getResources().getString(R.string.txt_web_engage_consultation_No));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(setFollowUpOrConsultAgainText()) && setFollowUpOrConsultAgainText().equalsIgnoreCase(context.getResources().getString(R.string.txt_web_engage_followup))) {
                webEngageModel.setFollowUpStatus(context.getResources().getString(R.string.txt_web_engage_consultation_yes));
            } else {
                webEngageModel.setFollowUpStatus(context.getResources().getString(R.string.txt_web_engage_consultation_No));
            }
            WebEngageHelper.getInstance().followUpEvent(webEngageModel, historyDetailListener.getContext());

        }
        historyDetailListener.vmCallBackOnChat(intent);
    }

    public void onGoToChat() {
        intent = new Intent();
        Bundle bundle = new Bundle();
        String specialization = historyResult != null && historyResult.getSpeciality() != null && historyResult.getSpeciality().getCode() != null ? historyResult.getSpeciality().getCode() : "";
        bundle.putString(ConsultationConstant.KEY_CHAT_ID, getChatId());
        bundle.putString(ConsultationConstant.KEY_DOCTOR_ID, historyResult.getDoctorId());
        bundle.putString(ConsultationConstant.KEY_USER_ID, historyResult.getUserId());
        bundle.putBoolean(ConsultationConstant.KEY_CONSULTATION_EXPIRY, historyResult.isConsultationExpiry());
        bundle.putString(ConsultationConstant.KEY_CONSULTATION_ID, consultationId);
        bundle.putString(ConsultationConstant.KEY_INTENT_FROM, ConsultationConstant.CHAT_HISTORY);
        bundle.putString(ConsultationConstant.KEY_DOCTOR_NAME, doctorName());
        bundle.putString(ConsultationConstant.KEY_SPECIALIZATION, specialization);
        bundle.putString(ConsultationConstant.KEY_SYMPTOM, historyResult.getSymptoms());

        if (historyResult.getPid() != null && (!TextUtils.isEmpty(historyResult.getPid())))
            bundle.putString(ConsultationConstant.KEY_PRESCRIPTION_ID, historyResult.getPid());
        intent.putExtra(ConsultationConstant.KEY_DOC_INFO, doctorInformation);
        intent.putExtra(IntentConstant.RATING, historyResult.getRating());
        intent.putExtras(bundle);
        intent.putExtra(IntentConstant.CONSULTATION_HISTORY, new Gson().toJson(historyResult));
        historyDetailListener.vmCallBackOnChat(intent);
    }

    public void onDownloadPrescription() {
        historyDetailListener.vmDownloadPrescription();
    }

    public void downloadPrescription() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            if (historyResult != null && !TextUtils.isEmpty(historyResult.getPid())) {
                String url = configMap.getProperty(ConfigConstant.CONSULTATION_BASE_URL) + ConsultAPIPathConstant.DOWNLOAD_PRESCRIPTION + "?pid=" + getPrescriptionId();
                CommonUtils.downloadPrescription(context, url, context.getResources().getString(R.string.text_download_prescription_description), historyResult.getPid(), ConsultationConstant.DOWNLOAD_FORMAT);
            }
        } else
            failedTransactionId = ConsultationServiceManager.DOWNLOAD_PRESCRIPTION;
    }

    private String getPrescriptionId() {
        return historyResult.getFollowUp() != null ? historyResult.getPid() + "&followUp=isFollowUp" : historyResult.getPid();
    }

    public void onHelp() {
        historyDetailListener.vmCallBackOnHelp();
    }

    private String getChatId() {
        return historyResult != null && historyResult.getId() != null && !TextUtils.isEmpty(historyResult.getId()) ? historyResult.getId() : "";
    }

    private void setDoctorImage() {
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_circular)
                .error(gender() == 0 ? R.drawable.ic_circular : gender())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(!TextUtils.isEmpty(imageUrl()) ? imageUrl() : gender() == 0 ? R.drawable.ic_circular : gender()).apply(options).into(historyDetailBinding.imgDoctor);
    }
}
