package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class DoctorListRequest {
    @SerializedName("active")
    private boolean active;
    @SerializedName("speciality")
    private String specialization;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String isSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
