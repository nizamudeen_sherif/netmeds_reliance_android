package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("logan_session_id")
    private String logan_session_id;
    @SerializedName("customer_details")
    private CustomerDetails customer_details = new CustomerDetails();
    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogan_session_id() {
        return logan_session_id;
    }

    public void setLogan_session_id(String logan_session_id) {
        this.logan_session_id = logan_session_id;
    }

    public CustomerDetails getCustomer_details() {
        return customer_details;
    }

    public void setCustomer_details(CustomerDetails customer_details) {
        this.customer_details = customer_details;
    }
}
