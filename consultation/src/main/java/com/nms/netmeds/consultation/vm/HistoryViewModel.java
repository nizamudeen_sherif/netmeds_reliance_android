package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.Banner;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.adapter.HistoryAdapter;
import com.nms.netmeds.consultation.databinding.ActivityHistoryBinding;
import com.nms.netmeds.consultation.model.HistoryRequest;
import com.nms.netmeds.consultation.model.HistoryResponse;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.LoganResponse;
import com.nms.netmeds.consultation.model.LoginResponse;
import com.nms.netmeds.consultation.model.OrderBy;
import com.nms.netmeds.consultation.model.request.LoginRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryViewModel extends AppViewModel {

    private final MutableLiveData<HistoryResponse> historyResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<LoginResponse> loginResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<LoganResponse> loganMutableLiveData = new MutableLiveData<>();
    private HistoryListener historyListener;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityHistoryBinding historyBinding;
    private int failedTransactionId;
    private JustDocUserResponse consultationLoginResult;
    private String loganSession;
    private int currentPage;
    private int noOfPages = 0;
    private boolean loadMore = false;
    private LinearLayoutManager linearLayoutManager;
    private HistoryAdapter historyAdapter;
    private List<HistoryResult> historyList;
    private BasePreference basePreference;
    private final Application application;
    private boolean consultationHistoryExist;

    public HistoryViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

    }

    public void init(Context context, HistoryListener historyListener, ActivityHistoryBinding historyBinding) {
        this.context = context;
        this.historyListener = historyListener;
        this.historyBinding = historyBinding;
        historyList = new ArrayList<>();
        basePreference = BasePreference.getInstance(context);
        getBanner();
        initHistoryAdapter();
        initScrollListener();
    }

    public void onChatWithDoctor() {
        historyListener.vmCallbackOnChatDoctor();
    }

    private void initHistoryAdapter() {
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        historyBinding.consultationHistoryList.setLayoutManager(linearLayoutManager);
        historyAdapter = new HistoryAdapter(context, historyListener, historyList, application);
        historyBinding.consultationHistoryList.setAdapter(historyAdapter);

    }

    private void initScrollListener() {
        historyBinding.consultationHistoryList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (!loadMore && currentPage < noOfPages) {
                        currentPage += 1;
                        loadMore = true;
                        getHistory(consultationLoginResult, currentPage);
                    }
                }
            }
        });
    }

    public void onHistoryDataAvailable(HistoryResponse historyResponse) {
        historyListener.vmDismissProgress();
        if (historyResponse != null && historyResponse.getServiceStatus() != null && historyResponse.getServiceStatus().getStatusCode() != null) {
            if (historyResponse.getServiceStatus().getStatusCode() == 200) {
                noOfPages = historyResponse.getNoOfPages();
                if (historyResponse.getConfig() != null)
                    basePreference.setJustDocConfig(new Gson().toJson(historyResponse.getConfig()));

                if (!loadMore)
                    historyList.clear();

                if (historyResponse.getHistoryResults() != null && historyResponse.getHistoryResults().size() > 0) {
                    setConsultationHistoryExist(true);
                    historyList.addAll(historyResponse.getHistoryResults());
                    historyAdapter.updateHistoryAdapter(historyList);
                    historyBinding.consultationHistoryList.setVisibility(View.VISIBLE);
                    historyBinding.lytHistoryEmpty.setVisibility(View.GONE);
                } else {
                    setConsultationHistoryExist(false);
                    historyBinding.consultationHistoryList.setVisibility(View.GONE);
                    historyBinding.lytHistoryEmpty.setVisibility(View.VISIBLE);
                }
                loadMore = false;
            } else {
                historyBinding.consultationHistoryList.setVisibility(View.GONE);
                historyBinding.lytHistoryEmpty.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(historyResponse.getServiceStatus().getMessage()))
                    SnackBarHelper.snackBarCallBack(historyBinding.lytHistoryViewContent, context, historyResponse.getServiceStatus().getMessage());
            }
        }
    }

    public MutableLiveData<HistoryResponse> getHistoryResponseMutableLiveData() {
        return historyResponseMutableLiveData;
    }

    public MutableLiveData<LoginResponse> getLoginResponseMutableLiveData() {
        return loginResponseMutableLiveData;
    }

    public MutableLiveData<LoganResponse> getLoganMutableLiveData() {
        return loganMutableLiveData;
    }

    public void getHistory(JustDocUserResponse consultationLoginResult, int currentPage) {
        this.consultationLoginResult = consultationLoginResult;
        this.currentPage = currentPage;
        showChatWithDoctor();
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            String userId = consultationLoginResult != null && consultationLoginResult.getId() != null ? consultationLoginResult.getId() : "";
            HistoryRequest consultationHistoryRequest = new HistoryRequest();
            OrderBy orderBy = new OrderBy();
            orderBy.setField(ConsultationConstant.PARAM_CREATE_DATE);
            orderBy.setOrder(ConsultationConstant.PARAM_ORDER);
            consultationHistoryRequest.setOrderBy(orderBy);
            consultationHistoryRequest.setPayment(ConsultationConstant.SUCCESS);
            consultationHistoryRequest.setUserId(userId);
            historyListener.vmShowProgress();
            ConsultationServiceManager.getInstance().consultationHistory(this, consultationHistoryRequest, basePreference, currentPage, ConsultationConstant.HISTORY_PAGE_LIMIT);
        } else {
            failedTransactionId = ConsultationServiceManager.TRANSACTION_HISTORY;
        }
    }

    public void doLoginInJustDoc(String loganSession) {
        this.loganSession = loganSession;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            LoginRequest consultationLoginRequest = new LoginRequest();
            consultationLoginRequest.setSessionId(loganSession);
            consultationLoginRequest.setAppversion(CommonUtils.getAppVersionCode(context));
            consultationLoginRequest.setMedium(DiagnosticConstant.APP_SOURCE);
            consultationLoginRequest.setCustomer_id(!TextUtils.isEmpty(BasePreference.getInstance(context).getMstarCustomerId())
                    ?BasePreference.getInstance(context).getMstarCustomerId():"");
            historyListener.vmShowProgress();
            ConsultationServiceManager.getInstance().consultationDoLogin(this, consultationLoginRequest, ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL));
        } else {
            failedTransactionId = ConsultationServiceManager.TRANSACTION_LOGIN;
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case ConsultationServiceManager.TRANSACTION_HISTORY:
                HistoryResponse historyResponse = new Gson().fromJson(data, HistoryResponse.class);
                historyResponseMutableLiveData.setValue(historyResponse);
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
                loginResponseMutableLiveData.setValue(loginResponse);
                // Web Engage - Registered for consultation
                WebEngageModel webEngageModel = new WebEngageModel();
                MStarCustomerDetails userDetailsResult = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
                webEngageModel.setMStarUserDetails(userDetailsResult);
                webEngageModel.setCommonProperties();
                WebEngageHelper.getInstance().registeredForConsultation(webEngageModel, historyListener.getContext());
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                LoganResponse loganResponse = new Gson().fromJson(data, LoganResponse.class);
                loganMutableLiveData.setValue(loganResponse);
                break;
            case ConsultationServiceManager.BANNER_DETAILS:
                try {
                    if (TextUtils.isEmpty(data)) return;
                    JSONObject jsonObject = new JSONObject(data);
                    if (TextUtils.isEmpty(jsonObject.toString())) return;
                    Type type = new TypeToken<List<Banner>>() {
                    }.getType();
                    List<Banner> banners = new Gson().fromJson(jsonObject.getJSONArray(context.getString(R.string.result)).toString(), type);
                    if (banners == null || banners.size() == 0) return;
                    loadBanner(banners);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }

    private void loadBanner(List<Banner> banners) {
        if (!TextUtils.isEmpty(banners.get(0).getUrl()) && banners.get(0).getPositionId().equals(context.getResources().getString(R.string.banner_position_hometop))) {
            historyBinding.imgViewBannerTop.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);
            Glide.with(context).load(banners.get(0).getUrl()).apply(options).into(historyBinding.imgViewBannerTop);
        }

    }

    @Override
    public void onFailed(int transactionId, String data) {
        historyListener.vmDismissProgress();
        switch (transactionId) {
            case ConsultationServiceManager.TRANSACTION_HISTORY:
                vmOnError(transactionId);
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                vmOnError(transactionId);
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                historyListener.forceLogout();
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case ConsultationServiceManager.TRANSACTION_HISTORY:
                getHistory(consultationLoginResult, currentPage);
                break;
            case ConsultationServiceManager.TRANSACTION_LOGIN:
                doLoginInJustDoc(loganSession);
                break;
            case ConsultationServiceManager.LOGAN_TOKEN:
                getLogan();
                break;
        }
    }

    public void onUserDataAvailable(LoginResponse loginResponse) {
        if (loginResponse != null && loginResponse.getServiceStatus() != null && loginResponse.getServiceStatus().getStatusCode() != null) {
            if (loginResponse.getServiceStatus().getStatusCode() == 200) {
                if (loginResponse.getResult() != null) {
                    basePreference.setJustDocUserResponse(new Gson().toJson(loginResponse.getResult()));
                    if (!TextUtils.isEmpty(loginResponse.getResult().getToken())) {
                        historyBinding.btnChatWithDoctor.setVisibility(View.VISIBLE);
                        /*Fcm token send to the server*/
                        tokenSendToServer();
                    }
                    getHistory(loginResponse.getResult(), 1);
                } else {
                    historyBinding.consultationHistoryList.setVisibility(View.GONE);
                    historyBinding.lytHistoryEmpty.setVisibility(View.VISIBLE);
                }
            } else {
                historyListener.vmDismissProgress();
                historyBinding.consultationHistoryList.setVisibility(View.GONE);
                historyBinding.lytHistoryEmpty.setVisibility(View.GONE);
                validateError(loginResponse);
            }
        }
    }

    private void validateError(LoginResponse loginResponse) {
        if (loginResponse.getResult() != null && !TextUtils.isEmpty(loginResponse.getResult().getMessage()))
            validateSessionToken(loginResponse.getResult());
    }

    private void validateSessionToken(JustDocUserResponse justDocUserResponse) {
        if (justDocUserResponse.getMessage().equals(ConsultationConstant.SESSION_ID_NOT_FOUND) || justDocUserResponse.getMessage().contains(ConsultationConstant.INVALID_SESSION_ID))
            getLogan();
        else
            SnackBarHelper.snackBarCallBack(historyBinding.lytHistoryViewContent, context, justDocUserResponse.getMessage());
    }

    public interface HistoryListener {
        void vmCallbackOnChatDoctor();

        void vmCallbackOnHistoryDetail(HistoryResult historyResult);

        void vmShowProgress();

        void vmDismissProgress();

        void forceLogout();

        Context getContext();
    }

    private void showNoNetworkView(boolean isConnected) {
        historyBinding.lytHistoryViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        historyBinding.historyNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        historyBinding.lytHistoryViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        historyBinding.historyApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }


    private void getLogan() {
        if (TextUtils.isEmpty(basePreference.getMstarCustomerId()) || TextUtils.isEmpty(basePreference.getMStarSessionId())) {
            historyListener.vmDismissProgress();
            historyListener.forceLogout();
            return;
        }
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            Map<String, String> header = new HashMap<>();
            header.put(AppConstant.MSTAR_AUTH_TOKEN, basePreference.getMStarSessionId());
            header.put(AppConstant.MSTAR_API_HEADER_USER_ID, basePreference.getMstarCustomerId());
            ConsultationServiceManager.getInstance().getLoganToken(this, header);
        } else
            failedTransactionId = ConsultationServiceManager.LOGAN_TOKEN;
    }

    public void onLoganTokenDataAvailable(LoganResponse loganResponse) {
        historyListener.vmDismissProgress();
        if (loganResponse!=null && !TextUtils.isEmpty(loganResponse.getStatus()) && loganResponse.getStatus().equalsIgnoreCase(DiagnosticConstant.SUCCESS)){
            validateSessionId(loganResponse);
        } else {
            historyListener.vmDismissProgress();
            historyListener.forceLogout();
        }
    }

    private void validateSessionId(LoganResponse loganResponse) {
        if (loganResponse.getLoganResult() != null && !TextUtils.isEmpty(loganResponse.getLoganResult().getSessionToken())) {
            doLoginInJustDoc(loganResponse.getLoganResult().getSessionToken());
            BasePreference.getInstance(context).setLoganSession(loganResponse.getLoganResult().getSessionToken());
        }else{
            historyListener.vmDismissProgress();
            historyListener.forceLogout();
        }
    }

    public void showChatWithDoctor() {
        String jusDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
        if(TextUtils.isEmpty(jusDocToken)){
            historyBinding.btnChatWithDoctor.setVisibility(View.GONE);
        }else{
            historyBinding.btnChatWithDoctor.setVisibility(View.VISIBLE);
        }
    }

    public void tokenSendToServer() {
        BasePreference basePreference = BasePreference.getInstance(context);
        MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        String email = customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "";
        JustDocUserResponse consultationLoginResult = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
        String jusDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";

        if (basePreference.isFcmTokenRefreshed() && !TextUtils.isEmpty(basePreference.getFcmToken()) && !TextUtils.isEmpty(jusDocToken)) {
            SendFcmTokenRequest sendFcmTokenRequest = new SendFcmTokenRequest();
            sendFcmTokenRequest.setEmail(email);
            sendFcmTokenRequest.setToken(basePreference.getFcmToken());
            sendFcmTokenRequest.setUserType("user");
            ConsultationServiceManager.getInstance().sendFcmTokenToServer(sendFcmTokenRequest, basePreference);
        }
    }

    public void getBanner() {
        ConsultationServiceManager.getInstance().getBanner(this);
    }

    public boolean isConsultationHistoryExist() {
        return consultationHistoryExist;
    }

    public void setConsultationHistoryExist(boolean consultationHistoryExist) {
        this.consultationHistoryExist = consultationHistoryExist;
    }
}
