package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class CreateChatResult {
    @SerializedName("message")
    private String message;
    @SerializedName("object")
    private CreateChatObject object;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CreateChatObject getObject() {
        return object;
    }

    public void setObject(CreateChatObject object) {
        this.object = object;
    }
}
