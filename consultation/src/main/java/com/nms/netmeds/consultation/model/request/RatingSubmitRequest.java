package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RatingSubmitRequest implements Serializable {

    @SerializedName("userId")
    private String userId;
    @SerializedName("doctorId")
    private String doctorId;
    @SerializedName("rating")
    private Float rating;
    @SerializedName("ratedOn")
    private String ratedOn;
    @SerializedName("comment")
    private String comment;
    @SerializedName("consultationId")
    private String consultationId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getRatedOn() {
        return ratedOn;
    }

    public void setRatedOn(String ratedOn) {
        this.ratedOn = ratedOn;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getConsultationId() {
        return consultationId;
    }

    public void setConsultationId(String consultationId) {
        this.consultationId = consultationId;
    }

    public RatingSubmitRequest(String userId, String doctorId, Float rating, String ratedOn, String comment, String consultationId) {
        this.userId = userId;
        this.doctorId = doctorId;
        this.rating = rating;
        this.ratedOn = ratedOn;
        this.comment = comment;
        this.consultationId = consultationId;
    }
}
