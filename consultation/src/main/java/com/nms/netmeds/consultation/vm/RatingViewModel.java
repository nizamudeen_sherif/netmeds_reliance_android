package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.InflateRatingViewBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RatingViewModel extends AppViewModel {

    private final Application application;
    protected Message Message;
    private InflateRatingViewBinding inflateRatingViewBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private boolean isChanged;
    private RatingSubmitRequest ratingSubmitRequest;
    private RatingSubmitRequest availableRating;
    private float rating;
    private BasePreference basePreference;
    private MessageAdapterListener messageAdapterListener;
    private static final float LAYOUT_WEIGHT = 1.0f;

    public RatingViewModel(Application application, MessageAdapterListener messageAdapterListener) {
        super(application);
        this.application = application;
        this.messageAdapterListener = messageAdapterListener;

    }

    public void onRatingInfoDataAvailable(final Context context, Message Message, final InflateRatingViewBinding inflateRatingViewBinding) {
        this.context = context;
        this.Message = Message;
        this.inflateRatingViewBinding = inflateRatingViewBinding;
        basePreference = BasePreference.getInstance(context);
        if (Message.isRatingAvailable()) {
            ratingExists(true);
        } else {
            if (Message.getRatingSubmitRequest() == null || Message.getRatingSubmitRequest().getUserId().equals("") || Message.getRatingSubmitRequest().getConsultationId().equals("") || Message.getRatingSubmitRequest().getDoctorId().equals("")) {
                inflateRatingViewBinding.ratingLl.setVisibility(View.GONE);
            }
            if (basePreference.getRating(Message.getRatingSubmitRequest().getConsultationId()).equals("null")) {
                isChanged = false;
                inflateRatingViewBinding.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float actualRating, boolean fromUser) {
                        if (inflateRatingViewBinding.ratingBar.getRating() > 0) {
                            rating = actualRating;
                            isChanged = true;
                            inflateRatingViewBinding.submit.setTextColor(context.getResources().getColor(R.color.colorMediumPink));
                        } else if (inflateRatingViewBinding.ratingBar.getRating() == 0) {
                            isChanged = false;
                            inflateRatingViewBinding.submit.setTextColor(context.getResources().getColor(R.color.colorLightPaleBlueGrey));
                        }

                    }
                });
                inflateRatingViewBinding.submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isChanged)
                            submitRating();
                        else {
                            Snackbar snackbar = Snackbar.make(inflateRatingViewBinding.getRoot(), application.getString(R.string.provide_a_rating_to_submit), Snackbar.LENGTH_SHORT);
                            snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorDarkBlueGrey));
                            snackbar.show();
                        }
                    }
                });
            } else
                ratingExists(false);
        }

    }

    private void ratingExists(boolean isApi) {
        this.availableRating = Message.getAvailableRating();
        if (isApi) rating = availableRating.getRating();
        else
            rating = Float.parseFloat(basePreference.getRating(Message.getRatingSubmitRequest().getConsultationId()));
        inflateRatingViewBinding.ratingBar.setRating(rating);
        inflateRatingViewBinding.submit.setText(context.getString(R.string.text_thanks_for_feedback));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = LAYOUT_WEIGHT;
        params.setMargins(context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_12), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0));
        params.gravity = Gravity.CENTER;
        inflateRatingViewBinding.submit.setLayoutParams(params);
        inflateRatingViewBinding.ratingBar.setIsIndicator(true);
        inflateRatingViewBinding.submit.setTextColor(context.getResources().getColor(R.color.colorMediumPink));
        inflateRatingViewBinding.submit.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void submitRating() {
        inflateRatingViewBinding.submit.setText(context.getString(R.string.text_thanks_for_feedback));
        inflateRatingViewBinding.ratingBar.setIsIndicator(true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = LAYOUT_WEIGHT;
        params.setMargins(context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_12), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0));
        params.gravity = Gravity.CENTER;
        inflateRatingViewBinding.submit.setLayoutParams(params);
        inflateRatingViewBinding.submit.setGravity(Gravity.CENTER_HORIZONTAL);
        this.ratingSubmitRequest = Message.getRatingSubmitRequest();
        ratingSubmitRequest.setRating(rating);
        basePreference.setRatingMap(Message.getRatingSubmitRequest().getConsultationId(), String.valueOf(rating));
        String formatDate = "";
        Date d = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateTimeUtils.yyyyMMddThhmmss);
        formatDate = simpleDateFormat.format(d);
        ratingSubmitRequest.setRatedOn(formatDate);
        callRatingService();
        messageAdapterListener.onSubmitRating(rating);
    }

    private void callRatingService() {
        ConsultationServiceManager.getInstance().submitRating(ratingSubmitRequest, BasePreference.getInstance(context));
    }

    public String getDoctorName() {
        if (Message == null || Message.getDoctorName() == null || Message.getDoctorName().equals(""))
            return context.getString(R.string.rate_your_experience);
        return MessageFormat.format("{0} {1}", context.getResources().getString(R.string.rate), Message.getDoctorName());
    }


}