package com.nms.netmeds.consultation.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DoctorInfoBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.vm.DoctorInfoViewModel;

@SuppressLint("ValidFragment")
public class DoctorInfoBottomSheetDialog extends BaseDialogFragment implements DoctorInfoViewModel.DoctorInfoViewModelListener {
    private final MessageAdapterListener messageAdapterListener;
    private final DoctorInformation doctorInformation;

    @SuppressLint("ValidFragment")
    public DoctorInfoBottomSheetDialog(DoctorInformation doctorInformation, MessageAdapterListener messageAdapterListener) {
        this.doctorInformation = doctorInformation;
        this.messageAdapterListener = messageAdapterListener;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setWindowAnimations(com.nms.netmeds.base.R.style.BottomSheetDialogAnimation);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setCancelable(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DoctorInfoBinding doctorInfoBinding = DataBindingUtil.inflate(inflater, R.layout.doctor_info, container, false);
        DoctorInfoViewModel doctorInfoViewModel = new DoctorInfoViewModel(getActivity().getApplication());
        doctorInfoViewModel.onDoctorInfoDataAvailable(getActivity(), doctorInformation, doctorInfoBinding, this);
        doctorInfoBinding.setViewModel(doctorInfoViewModel);
        return doctorInfoBinding.getRoot();
    }

    @Override
    public void doctorInfoClose() {
        this.dismissAllowingStateLoss();
        messageAdapterListener.closeDoctorInfo();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        messageAdapterListener.closeDoctorInfo();
    }
}
