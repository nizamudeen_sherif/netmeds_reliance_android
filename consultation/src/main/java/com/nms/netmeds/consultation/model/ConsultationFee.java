package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.Subscription;

public class ConsultationFee {
    @SerializedName("id")
    private String id;
    @SerializedName("speciality")
    private String speciality;
    @SerializedName("international")
    private String international;
    @SerializedName("chat")
    private String chat;
    @SerializedName("interChat")
    private String interChat;
    @SerializedName("general")
    private String general;
    @SerializedName("reoccur")
    private boolean reoccur;
    @SerializedName("message")
    private String message;
    @SerializedName("subscription")
    private Subscription subscription ;


    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    public String getInterChat() {
        return interChat;
    }

    public void setInterChat(String interChat) {
        this.interChat = interChat;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getReoccur() {
        return reoccur;
    }

    public void setReoccur(boolean reoccur) {
        this.reoccur = reoccur;
    }
}
