package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.zxing.client.result.VINParsedResult;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.adapter.SpecializationAdapter;
import com.nms.netmeds.consultation.databinding.DialogSpecializationBinding;
import com.nms.netmeds.consultation.model.Speciality;

import java.util.List;

public class SpecializationBottomSheetDialogViewModel extends AppViewModel implements SpecializationAdapter.SpecializationAdapterListener {
    private SpecializationBottomSheetDialogListener specializationBottomSheetDialogListener;
    private boolean isEditSpecialization;
    private Speciality selectedSpeciality;
    private DialogSpecializationBinding specializationBinding;

    public SpecializationBottomSheetDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, Application application, List<Speciality> specialityList, boolean isEditSpecialization, DialogSpecializationBinding specializationBinding, SpecializationBottomSheetDialogListener SpecializationBottomSheetDialogListener) {
        this.specializationBottomSheetDialogListener = SpecializationBottomSheetDialogListener;
        this.isEditSpecialization = isEditSpecialization;
        this.specializationBinding = specializationBinding;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        specializationBinding.specializationList.setLayoutManager(linearLayoutManager);
        SpecializationAdapter specialityAdapter = new SpecializationAdapter(context, application, specialityList, this);
        specializationBinding.specializationList.setAdapter(specialityAdapter);
        showEditSpecializationDescription();
    }

    public void onSpecializationClose() {
        specializationBottomSheetDialogListener.vmBottomSheetDismiss();
    }

    public void onSpecializationDone() {
        specializationBottomSheetDialogListener.vmDone(selectedSpeciality);
    }

    private void showEditSpecializationDescription() {
        specializationBinding.textEditSpecializationTitle.setVisibility(isEditSpecialization ? View.VISIBLE :View.GONE);
    }

    @Override
    public void specializationSelection(Speciality speciality) {
        selectedSpeciality = speciality;
    }

    public interface SpecializationBottomSheetDialogListener {
        void vmBottomSheetDismiss();

        void vmDone(Speciality speciality);
    }
}
