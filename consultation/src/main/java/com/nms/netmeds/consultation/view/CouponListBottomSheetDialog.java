package com.nms.netmeds.consultation.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DialogCouponListBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.vm.CouponListViewModel;

import java.util.List;

@SuppressLint("ValidFragment")
public class CouponListBottomSheetDialog extends BaseDialogFragment implements CouponListViewModel.CouponListItemListener {
    private final List<ConsultationCoupon> promoCodeList;
    private final String appliedPromoCode;
    private final MessageAdapterListener messageAdapterListener;

    @SuppressLint("ValidFragment")
    public CouponListBottomSheetDialog(List<ConsultationCoupon> promoCodeList, String appliedPromoCode, MessageAdapterListener messageAdapterListener) {
        this.promoCodeList = promoCodeList;
        this.appliedPromoCode = appliedPromoCode;
        this.messageAdapterListener = messageAdapterListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogCouponListBinding dialogCouponListBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_coupon_list, container, false);
        CouponListViewModel couponListViewModel = new CouponListViewModel(getActivity().getApplication());
        couponListViewModel.onDataAvailable(getActivity(), promoCodeList, appliedPromoCode, dialogCouponListBinding, this);
        dialogCouponListBinding.setViewModel(couponListViewModel);
        return dialogCouponListBinding.getRoot();
    }

    @Override
    public void couponListClose() {
        CouponListBottomSheetDialog.this.dismissAllowingStateLoss();
        messageAdapterListener.closeCouponList();
    }

    @Override
    public void selectedCouponCode(ConsultationCoupon coupon) {
        CouponListBottomSheetDialog.this.dismissAllowingStateLoss();
        messageAdapterListener.selectedCouponCode(coupon);
    }
}
