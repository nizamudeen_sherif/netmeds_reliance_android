/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.consultation.adapter;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DoctorListItemBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.vm.DoctorListItemViewModel;

import java.util.List;


public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.DoctorListHolder> implements DoctorListItemViewModel.DoctorListItemViewModelListener {
    private final List<DoctorInformation> doctorList;
    private final Application application;
    private final Context context;
    private final MessageAdapterListener messageAdapterListener;

    public DoctorListAdapter(Context context, List<DoctorInformation> doctorList, Application application, MessageAdapterListener messageAdapterListener) {
        this.doctorList = doctorList;
        this.application = application;
        this.context = context;
        this.messageAdapterListener=messageAdapterListener;
    }

    @NonNull
    @Override
    public DoctorListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DoctorListItemBinding doctorListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.doctor_list_item, parent, false);
        return new DoctorListHolder(doctorListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorListHolder holder, int position) {
        DoctorListItemViewModel doctorListItemViewModel = new DoctorListItemViewModel(application);
        doctorListItemViewModel.onDataAvailable(context, doctorList.get(position), holder.doctorListItemBinding, this);
        holder.doctorListItemBinding.setViewModel(doctorListItemViewModel);
    }

    @Override
    public int getItemCount() {
        return doctorList.size();
    }

    @Override
    public void viewProfile(String doctorId) {
        messageAdapterListener.viewDoctorInfo(Integer.parseInt(doctorId));
    }

    class DoctorListHolder extends RecyclerView.ViewHolder {
        private final DoctorListItemBinding doctorListItemBinding;

        DoctorListHolder(DoctorListItemBinding doctorListItemBinding) {
            super(doctorListItemBinding.getRoot());
            this.doctorListItemBinding = doctorListItemBinding;
        }
    }
}
