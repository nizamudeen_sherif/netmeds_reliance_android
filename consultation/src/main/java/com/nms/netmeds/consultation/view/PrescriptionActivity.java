package com.nms.netmeds.consultation.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.adapter.PrescriptionAdapter;
import com.nms.netmeds.consultation.databinding.ActivityViewPrescriptionBinding;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.vm.ViewPrescriptionViewModel;


public class PrescriptionActivity extends BaseUploadPrescription<ViewPrescriptionViewModel> implements ViewPrescriptionViewModel.PrescriptionListener {

    private ActivityViewPrescriptionBinding viewPrescriptionBinding;
    private ViewPrescriptionViewModel viewPrescriptionViewModel;
    private MedicineInfoResponse medicineInfoResponse;
    private String intentFrom = "";
    private CustomerResponse customerResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntentData();
        viewPrescriptionBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_prescription);
        toolBarSetUp(viewPrescriptionBinding.toolbar);
        initToolBar();
        viewPrescriptionBinding.setViewModel(onCreateViewModel());
        onRetry(viewPrescriptionViewModel);
        viewPrescriptionViewModel.setPrescriptionResponse(medicineInfoResponse);
        CharSequence diagnosis = viewPrescriptionViewModel.setUpDiagnosis();
        if (!diagnosis.equals(""))
            viewPrescriptionBinding.diagnosis.setText(diagnosis);

        CharSequence investigations = viewPrescriptionViewModel.setUpInvestigations();
        if (!investigations.equals(""))
            viewPrescriptionBinding.investigations.setText(investigations);

        CharSequence doctorsAdvice = viewPrescriptionViewModel.setUpDocAdvice();
        if (!doctorsAdvice.equals(""))
            viewPrescriptionBinding.advice.setText(doctorsAdvice);

        setUpOrderAdapter();
    }

    private void getIntentData() {
        if (getIntent() == null || !getIntent().hasExtra(IntentConstant.VIEW_PRESCRIPTION)) return;
        medicineInfoResponse = (MedicineInfoResponse) getIntent().getSerializableExtra(IntentConstant.VIEW_PRESCRIPTION);
        if (getIntent().hasExtra(ConsultationConstant.KEY_INTENT_FROM))
            intentFrom = getIntent().getStringExtra(ConsultationConstant.KEY_INTENT_FROM);
        if (getIntent() == null || !getIntent().hasExtra(IntentConstant.CUSTOMER_DETAIL)) return;
        customerResponse = (CustomerResponse) getIntent().getSerializableExtra(IntentConstant.CUSTOMER_DETAIL);
    }


    private void initToolBar() {
        viewPrescriptionBinding.collapsingToolbar.setTitle(getString(R.string.text_prescription));
        viewPrescriptionBinding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        viewPrescriptionBinding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        viewPrescriptionBinding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        viewPrescriptionBinding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }


    private void setUpOrderAdapter() {
        if (medicineInfoResponse == null || medicineInfoResponse.getMedicines() == null || medicineInfoResponse.getMedicines().get(0).getMedicineId() == null)
            return;
        PrescriptionAdapter prescriptionAdapter = new PrescriptionAdapter(this, medicineInfoResponse.getMedicines());
        viewPrescriptionBinding.ordersList.setAdapter(prescriptionAdapter);
    }

    protected ViewPrescriptionViewModel onCreateViewModel() {
        viewPrescriptionViewModel = ViewModelProviders.of(this).get(ViewPrescriptionViewModel.class);
        viewPrescriptionViewModel.init(this, intentFrom, viewPrescriptionBinding, this, customerResponse);
        return viewPrescriptionViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.help) {
            LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_need_help_activity), this);
        } else if (item.getItemId() == R.id.download_prescription) {
            initDownload();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDownload() {
        if (checkGalleryPermission())
            viewPrescriptionViewModel.downloadPrescription(medicineInfoResponse.getId());
        else
            requestPermission(PermissionConstants.DOWNLOAD_PERMISSION);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_consultation, menu);
        return true;
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);

    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void navigateToBookLabTest(MedicineInfoResponse medicineInfoResponse) {
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_LAB_TEST_FROM_CONSULTATION, medicineInfoResponse.getLabTests());
        LaunchIntentManager.routeToActivity(this.getResources().getString(R.string.route_diagnostic_home), intent, this);
    }
}