package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class ConsultationFeeResponse extends BaseResponse {
    @SerializedName("result")
    private ConsultationFee consultationFee;
    @SerializedName("updatedOn")
    private String updatedOn;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ConsultationFee getConsultationFee() {
        return consultationFee;
    }

    public void setConsultationFee(ConsultationFee consultationFee) {
        this.consultationFee = consultationFee;
    }
}
