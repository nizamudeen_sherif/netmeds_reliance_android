package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.view.LinePagerIndicatorDecoration;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.adapter.DoctorListAdapter;
import com.nms.netmeds.consultation.databinding.InflatorDoctorListBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Message;

public class DoctorListViewModel extends AppViewModel {
    private final Application application;

    public DoctorListViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public void onDataAvailable(Context context, InflatorDoctorListBinding doctorListBinding, Message message, MessageAdapterListener messageAdapterListener) {
        messageAdapterListener.loaderView(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        doctorListBinding.doctorList.setLayoutManager(linearLayoutManager);
        if (doctorListBinding.doctorList.getItemDecorationCount() > 0) {
            doctorListBinding.doctorList.removeItemDecorationAt(0);
        }
        doctorListBinding.doctorList.addItemDecoration(new LinePagerIndicatorDecoration(context.getResources().getColor(R.color.colorAccent), context.getResources().getColor(R.color.colorSecondary)), 0);
        DoctorListAdapter doctorListAdapter = new DoctorListAdapter(context, message.getDoctorList(), application, messageAdapterListener);
        doctorListBinding.doctorList.setAdapter(doctorListAdapter);
        CommonUtils.snapHelper(doctorListBinding.doctorList);

    }
}
