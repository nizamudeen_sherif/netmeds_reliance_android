package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.CouponListItemBinding;
import com.nms.netmeds.consultation.model.ConsultationCoupon;

public class CouponListItemViewModel extends AppViewModel {
    private ConsultationCoupon consultationCoupon;
    private CouponListItemListener couponListItemListener;
    private String promoCode;
    private CouponListItemBinding couponListItemBinding;

    public CouponListItemViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, ConsultationCoupon consultationCoupon, String promoCode, CouponListItemBinding couponListItemBinding, CouponListItemListener couponListItemListener) {
        this.consultationCoupon = consultationCoupon;
        this.couponListItemListener = couponListItemListener;
        this.promoCode = promoCode;
        this.couponListItemBinding = couponListItemBinding;
        if (consultationCoupon.getCode().equalsIgnoreCase(promoCode)) {
            couponListItemBinding.btnCouponApplied.setVisibility(View.VISIBLE);
            couponListItemBinding.imgChecked.setImageResource(R.drawable.ic_radio_checked);
            couponListItemBinding.lytCouponItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selection_active));
            couponListItemBinding.textCouponCode.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
        } else {
            couponListItemBinding.btnCouponApplied.setVisibility(View.GONE);
            couponListItemBinding.imgChecked.setImageResource(R.drawable.ic_radio_unchecked);
            couponListItemBinding.lytCouponItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selection_inactive));
            couponListItemBinding.textCouponCode.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Regular.ttf"));
        }
        showCouponCode();
        showCouponDescription();
    }

    public String couponCode() {
        return consultationCoupon != null && !TextUtils.isEmpty(consultationCoupon.getCode()) ? consultationCoupon.getCode() : "";
    }

    public String couponMessage() {
        return consultationCoupon != null && !TextUtils.isEmpty(consultationCoupon.getDescription()) ? consultationCoupon.getDescription() : "";
    }

    public void showCouponDescription() {
        couponListItemBinding.textCouponMessage.setVisibility(TextUtils.isEmpty(couponMessage()) ?  View.GONE : View.VISIBLE);
    }

    public void showCouponCode() {
        couponListItemBinding.textCouponCode.setVisibility(TextUtils.isEmpty(couponCode()) ? View.GONE:View.VISIBLE);
    }

    public void onCouponSelection() {
        if (!promoCode.equalsIgnoreCase(couponCode()))
            couponListItemListener.selectedCoupon(consultationCoupon);
    }

    public interface CouponListItemListener {
        void selectedCoupon(ConsultationCoupon coupon);
    }
}
