package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

public class Config {
    @SerializedName("s3AttachmentURL")
    private String attachmentUrl;
    @SerializedName("s3BucketURL")
    private String bucketURL;

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }

    public String getBucketURL() {
        return bucketURL;
    }

    public void setBucketURL(String bucketURL) {
        this.bucketURL = bucketURL;
    }
}
