package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.consultation.adapter.ProblemCategoryAdapter;
import com.nms.netmeds.consultation.databinding.DialogProblemCategoryBinding;
import com.nms.netmeds.consultation.model.Options;

import java.util.List;

public class ProblemCategoryViewModel extends AppViewModel implements ProblemCategoryAdapter.ProblemCategoryAdapterListener {
    private final Application application;
    private ProblemCategoryViewModelListener problemCategoryViewModelListener;

    public ProblemCategoryViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public void onDataAvailable(Context context, List<Options> problemCategoryList, DialogProblemCategoryBinding dialogProblemCategoryBinding, ProblemCategoryViewModelListener problemCategoryViewModelListener) {
        this.problemCategoryViewModelListener = problemCategoryViewModelListener;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dialogProblemCategoryBinding.problemCategoryList.setLayoutManager(linearLayoutManager);
        ProblemCategoryAdapter problemCategoryAdapter = new ProblemCategoryAdapter(context, problemCategoryList, application, this);
        dialogProblemCategoryBinding.problemCategoryList.setAdapter(problemCategoryAdapter);
    }

    public void onProblemCategoryClose() {
        problemCategoryViewModelListener.pvmProblemCategoryClose();
    }

    @Override
    public void paSelectedProblem(Options problemCategory) {
        problemCategoryViewModelListener.pvmSelectedProblem(problemCategory);
    }

    public interface ProblemCategoryViewModelListener {
        void pvmSelectedProblem(Options problemCategory);

        void pvmProblemCategoryClose();
    }
}
