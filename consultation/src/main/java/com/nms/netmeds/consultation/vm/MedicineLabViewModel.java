package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.InflateOrderMedicineBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.LabTest;
import com.nms.netmeds.consultation.model.Medicine;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.view.PrescriptionActivity;

public class MedicineLabViewModel extends AppViewModel implements View.OnClickListener {

    private final Application application;
    protected Message Message;
    private InflateOrderMedicineBinding inflateOrderMedicineBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private MessageAdapterListener messageAdapterListener;
    private MedicineInfoResponse medicineInfoResponse;
    private String intentFrom;
    private boolean isMedicine;
    private CustomerResponse customerResponse;

    public MedicineLabViewModel(Application application, MedicineInfoResponse medicineInfoResponse, boolean isMedicine) {
        super(application);
        this.application = application;
        this.medicineInfoResponse = medicineInfoResponse;
        this.isMedicine = isMedicine;
    }

    public void onDoctorInfoDataAvailable(Context context, Message Message, InflateOrderMedicineBinding inflateOrderMedicineBinding, MessageAdapterListener messageAdapterListener) {
        this.context = context;
        this.Message = Message;
        this.inflateOrderMedicineBinding = inflateOrderMedicineBinding;
        this.messageAdapterListener = messageAdapterListener;
        this.inflateOrderMedicineBinding.txtViewPrescription.setOnClickListener(this);
        this.inflateOrderMedicineBinding.txtViewOrderMedicine.setOnClickListener(this);
        customerResponse = Message.getCustomerResponse();
        if (isMedicine)
            showMedicines();
        else
            showLabTests();
    }

    private void showLabTests() {
        if (medicineInfoResponse == null || medicineInfoResponse.getLabTests() == null || medicineInfoResponse.getLabTests().size() == 0) {
            return;
        }

        inflateOrderMedicineBinding.txtViewOrderMedicine.setVisibility(View.VISIBLE);

        inflateOrderMedicineBinding.LlMedicines.removeAllViews();
        inflateOrderMedicineBinding.txtRx.setText("");
        inflateOrderMedicineBinding.txtRx.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lab_test_active, 0);
        inflateOrderMedicineBinding.txtRx.setPadding(context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_14), context.getResources().getDimensionPixelSize(R.dimen.density_size_0));

        for (LabTest labTest : medicineInfoResponse.getLabTests()) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View medicineView = inflater.inflate(R.layout.medicine_row, null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_10));
            TextView textView = medicineView.findViewById(R.id.txtView_medicine_name);
            textView.setText(labTest.getTestName());
            inflateOrderMedicineBinding.LlMedicines.addView(medicineView, params);
        }
    }

    private void showMedicines() {

        if (Message == null || Message.getMedicines() == null || Message.getMedicines().size() == 0)
            return;

        inflateOrderMedicineBinding.txtViewOrderMedicine.setVisibility(View.VISIBLE);

        inflateOrderMedicineBinding.LlMedicines.removeAllViews();

        inflateOrderMedicineBinding.txtRx.setText(context.getResources().getString(R.string.text_rx));
        inflateOrderMedicineBinding.txtRx.setPadding(context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0));
        inflateOrderMedicineBinding.txtRx.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


        for (Medicine medicine : Message.getMedicines()) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View medicineView = inflater.inflate(R.layout.medicine_row, null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_0), context.getResources().getDimensionPixelSize(R.dimen.density_size_8));
            TextView textView = medicineView.findViewById(R.id.txtView_medicine_name);
            textView.setText(medicine.getMedicine());
            inflateOrderMedicineBinding.LlMedicines.addView(medicineView, params);
        }
    }

    public String prescribedBy() {
        if (isMedicine) {
            if (!TextUtils.isEmpty(Message.getDoctorName())) {
                return " " + Message.getDoctorName();
            }
            return "";
        } else {
            if (!TextUtils.isEmpty(medicineInfoResponse.getDoctorName())) {
                return " " + medicineInfoResponse.getDoctorName();
            }
            return "";
        }
    }


    public String prescribedTitle() {
        if (isMedicine)
            return context.getResources().getString(R.string.text_medicine_prescribed);
        return context.getString(R.string.lab_test_prescribed);
    }

    public String setTextForLabOrMedicine() {
        if (isMedicine)
            return context.getResources().getString(R.string.text_order_medicine);
        return context.getResources().getString(R.string.text_book_tests);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.txtViewPrescription) {
            if (medicineInfoResponse == null) return;
            Intent intent = new Intent(context, PrescriptionActivity.class);
            intent.putExtra(ConsultationConstant.KEY_INTENT_FROM, intentFrom);
            intent.putExtra(IntentConstant.VIEW_PRESCRIPTION, medicineInfoResponse);
            intent.putExtra(IntentConstant.CUSTOMER_DETAIL, customerResponse);
            LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_view_prescription_activity), intent, context);
            return;
        }
        if (view.getId() == R.id.txtViewOrderMedicine) {
            if (isMedicine) {
                messageAdapterListener.orderMedicine(medicineInfoResponse);
            } else {
                messageAdapterListener.bookLabTest(medicineInfoResponse);
            }
        }
    }

    public void setIntentFrom(String intentFrom) {
        this.intentFrom = intentFrom;
    }
}