package com.nms.netmeds.consultation.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.consultation.model.request.FollowUp;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;

import java.io.Serializable;

public class HistoryResult implements Serializable {
    @SerializedName("doctorImage")
    private String doctorImage;
    @SerializedName("status")
    private String status;
    @SerializedName("partnerId")
    private String partnerId;
    @SerializedName("type")
    private String type;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("endTime")
    private String endTime;
    @SerializedName("id")
    private String id;
    @SerializedName("creationDate")
    private String creationDate;
    @SerializedName("doctorId")
    private String doctorId;
    @SerializedName("userId")
    private String userId;
    @SerializedName("doctorName")
    private String doctorName;
    @SerializedName("tatTime")
    private Time tatTime;
    @SerializedName("specialityName")
    private String specialityName;
    @SerializedName("callType")
    private String callType;
    @SerializedName("lastMessage")
    private String lastMessage;
    @SerializedName("lastMessageTime")
    private String lastMessageTime;
    @SerializedName("lastMessageType")
    private String lastMessageType;
    @SerializedName("unreadCount")
    private String unreadCount;
    @SerializedName("gender")
    private String gender;
    @SerializedName("pid")
    private String pid;
    @SerializedName("prescriptionStatus")
    private String prescriptionStatus;
    @SerializedName("audioOnly")
    private boolean isAudio;
    @SerializedName("isChat")
    private boolean isChat;
    @SerializedName("symptoms")
    private String symptoms;
    @SerializedName("isExpiry")
    private boolean isConsultationExpiry;
    @SerializedName("mode")
    private String mode;
    @SerializedName("doctor")
    private DoctorInformation doctorInformation;
    @SerializedName("specs")
    private Speciality speciality;
    @SerializedName("chat")
    private Chat chat;
    @SerializedName("followUp")
    private FollowUp followUp;
    @SerializedName("rating")
    private RatingSubmitRequest rating;
    @SerializedName("test")
    private JsonElement testJson;

    public JsonElement getTestJson() {
        return testJson;
    }

    public void setTestJson(JsonElement testJson) {
        this.testJson = testJson;
    }


    public RatingSubmitRequest getRating() {
        return rating;
    }

    public void setRating(RatingSubmitRequest rating) {
        this.rating = rating;
    }

    public String getDoctorImage() {
        return doctorImage;
    }

    public void setDoctorImage(String doctorImage) {
        this.doctorImage = doctorImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Time getTatTime() {
        return tatTime;
    }

    public void setTatTime(Time tatTime) {
        this.tatTime = tatTime;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getLastMessageType() {
        return lastMessageType;
    }

    public void setLastMessageType(String lastMessageType) {
        this.lastMessageType = lastMessageType;
    }

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPrescriptionStatus() {
        return prescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
        this.prescriptionStatus = prescriptionStatus;
    }

    public boolean isAudio() {
        return isAudio;
    }

    public void setAudio(boolean audio) {
        isAudio = audio;
    }

    public boolean isChat() {
        return isChat;
    }

    public void setChat(boolean chat) {
        isChat = chat;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public boolean isConsultationExpiry() {
        return isConsultationExpiry;
    }

    public void setConsultationExpiry(boolean consultationExpiry) {
        isConsultationExpiry = consultationExpiry;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public DoctorInformation getDoctorInformation() {
        return doctorInformation;
    }

    public void setDoctorInformation(DoctorInformation doctorInformation) {
        this.doctorInformation = doctorInformation;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public FollowUp getFollowUp() {
        return followUp;
    }

    public void setFollowUp(FollowUp followUp) {
        this.followUp = followUp;
    }


}
