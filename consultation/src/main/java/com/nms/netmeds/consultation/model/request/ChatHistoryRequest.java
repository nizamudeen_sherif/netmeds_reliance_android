package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.consultation.model.OrderBy;

public class ChatHistoryRequest {
    @SerializedName("chatId")
    private String chatId;
    @SerializedName("orderBy")
    private OrderBy orderBy;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public OrderBy getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderBy orderBy) {
        this.orderBy = orderBy;
    }
}
