package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class PaymentCreationResponse extends BaseResponse {
    @SerializedName("result")
    private PaymentCreationResult result;
    @SerializedName("updatedOn")
    private String updatedOn;
    private String paymentStatus;

    public PaymentCreationResult getResult() {
        return result;
    }

    public void setResult(PaymentCreationResult result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}

