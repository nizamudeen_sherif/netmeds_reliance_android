package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConsultationEvent;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarAddMultipleProductsResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.WebEngageModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.JsonHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultAPIPathConstant;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.ConsultationUtil;
import com.nms.netmeds.consultation.IntentConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.adapter.MessageAdapter;
import com.nms.netmeds.consultation.adapter.MultiChoiceAdapter;
import com.nms.netmeds.consultation.databinding.ActivityChatBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.ApplyCoupon;
import com.nms.netmeds.consultation.model.ApplyCouponResponse;
import com.nms.netmeds.consultation.model.BotQuestion;
import com.nms.netmeds.consultation.model.BotSymptomQuestion;
import com.nms.netmeds.consultation.model.ChatHistoryResponse;
import com.nms.netmeds.consultation.model.ChatHistoryResult;
import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.model.ConsultationFeeDetail;
import com.nms.netmeds.consultation.model.ConsultationFeeResponse;
import com.nms.netmeds.consultation.model.ConsultationUpdateResponse;
import com.nms.netmeds.consultation.model.CreateChatObject;
import com.nms.netmeds.consultation.model.CreateChatResponse;
import com.nms.netmeds.consultation.model.CreateConsultationResponse;
import com.nms.netmeds.consultation.model.CreateConsultationResult;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.GetAllCouponResponse;
import com.nms.netmeds.consultation.model.HistoryResult;
import com.nms.netmeds.consultation.model.LabTest;
import com.nms.netmeds.consultation.model.Medicine;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.model.OrderBy;
import com.nms.netmeds.consultation.model.PaymentCreationResponse;
import com.nms.netmeds.consultation.model.Question;
import com.nms.netmeds.consultation.model.ReoccurUpdateResponse;
import com.nms.netmeds.consultation.model.Speciality;
import com.nms.netmeds.consultation.model.SpecialityResponse;
import com.nms.netmeds.consultation.model.SpecializationBotQuestion;
import com.nms.netmeds.consultation.model.UploadAttachmentResponse;
import com.nms.netmeds.consultation.model.request.ApplyCouponRequest;
import com.nms.netmeds.consultation.model.request.ChatHistoryRequest;
import com.nms.netmeds.consultation.model.request.ConsultationFeeRequest;
import com.nms.netmeds.consultation.model.request.ConsultationUpdatePayload;
import com.nms.netmeds.consultation.model.request.ConsultationUpdateRequest;
import com.nms.netmeds.consultation.model.request.CreateChatRequest;
import com.nms.netmeds.consultation.model.request.CreateConsultationRequest;
import com.nms.netmeds.consultation.model.request.DoctorInfoRequest;
import com.nms.netmeds.consultation.model.request.DoctorListRequest;
import com.nms.netmeds.consultation.model.request.FollowUp;
import com.nms.netmeds.consultation.model.request.Payment;
import com.nms.netmeds.consultation.model.request.PaymentCreationRequest;
import com.nms.netmeds.consultation.model.request.PrescriptionDetailsRequest;
import com.nms.netmeds.consultation.model.request.RatingSubmitRequest;
import com.nms.netmeds.consultation.model.request.ReoccurPayload;
import com.nms.netmeds.consultation.model.request.ReoccurUpdateRequest;
import com.nms.netmeds.consultation.view.ChatActivity;
import com.tune.TuneEventItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.nms.netmeds.base.retrofit.APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS;
import static com.nms.netmeds.base.retrofit.APIServiceManager.MSTAR_CREATE_CART;
import static com.nms.netmeds.base.utils.GoogleAnalyticsHelper.EVENT_ACTION_CONSULTATION_ORDER_MEDICINE;
import static com.nms.netmeds.base.utils.GoogleAnalyticsHelper.EVENT_LABLE_ORDER_MEDICINE_FROM_CONSULTATION;
import static com.nms.netmeds.consultation.ConsultationServiceManager.ORDER_MEDICINE;
import static com.nms.netmeds.consultation.IntentConstant.PRESCRIPTION_BASE_URL;

public class ChatViewModel extends AppViewModel implements MessageAdapterListener {
    private ActivityChatBinding chatBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ChatListener chatListener;
    private ChatViewModel chatViewModel;
    private String intentFrom = "";
    private int failedTransactionId;
    private ConfigMap configMap;
    private Socket mSocket;
    private String userId = "";
    private String chatId = "";
    private String doctorId = "";
    private String consultationId = "";
    private JustDocUserResponse justDocUserInformation;
    private SpecializationBotQuestion specializationBotQuestion;
    private List<BotSymptomQuestion> symptomQuestionList;
    private BotSymptomQuestion botSymptomQuestion;
    private boolean isDoctorJoined = false;
    private List<Message> messageList;
    private boolean isAnswered = true;
    private int queryLength = 0;
    private int query = 0;
    private String followUpSpecialization;
    private BasePreference basePreference;
    private String followUpSymptom;
    private MessageAdapter messageAdapter;
    private LinearLayoutManager layoutManager;
    private boolean isFromChat = true;
    private final MutableLiveData<CreateChatResponse> createChatMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ConsultationUpdateResponse> consultationUpdateMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<DoctorListResponse> doctorListMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<DoctorListResponse> doctorInfoMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<CreateConsultationResponse> createConsultationMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ConsultationFeeResponse> feeMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ApplyCouponResponse> applyCouponMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<GetAllCouponResponse> allCouponMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<UploadAttachmentResponse> uploadAttachmentMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<PaymentCreationResponse> paymentCreationMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ReoccurUpdateResponse> reoccurUpdateMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ChatHistoryResponse> chatHistoryMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MedicineInfoResponse> medicineInfoResponseMutableLiveData = new MutableLiveData<>();
    private DoctorInformation doctorInformation;
    private List<Question> botQuestionList;
    private int botQuestionLength = 0;
    private int botQuestionIndex = 0;
    private boolean isLoading = true;
    private Animation viewShow;
    private List<Speciality> speciality;
    private Speciality selectedSpeciality;
    private boolean isEditSpecialisation = false;
    private int editPosition;
    private boolean isViewAllSpecializationOpen = false;
    private boolean isSpecializationQuestionEnd = false;
    private boolean isPaymentSuccess = false;
    private int viewDoctorInfoId;
    private boolean isAudioMode = false;
    private boolean isChatMode = false;
    private ConsultationFeeDetail consultationFeeDetail;
    private List<ConsultationCoupon> consultationCouponList;
    private ConsultationCoupon retryCouponCode;
    private boolean isCouponChanged = false;
    private ApplyCoupon oneTimeAppliedCoupon;
    private String selectedPaymentPlan;
    private DateTimeUtils dateTimeUtils;
    private MultiChoiceAdapter multiChoiceAdapter;
    private final Application application;
    private MessageAdapterListener messageAdapterListener;
    private String sendMessageFrom = "";
    private String path;
    private String selectedRadioOption = "";
    private boolean isChatInitialized = true;
    private DoctorInformation chatJoinedDoctorInformation;
    private double totalAmount = 0;
    private ApplyCoupon appliedCoupon;
    private long orderCreatedTime;
    private String orderId = "";
    private boolean isReoccur = false;
    private boolean isConsultationExpiry;
    private String selectedMultiOption = "";
    private boolean isLoaderView = false;
    private boolean isDoctorInfoOpen = false;
    private boolean isPromoCodeOpen = false;
    private final String TAG_SOCKET_CONNECTION = "SOCKET_CONNECTION";
    private String hashId;
    private String contentType;
    private String fileName = "";
    private String downloadAttachmentUrl;
    private String downloadAttachmentFilePath;
    private int downloadFilePosition;
    private String prscriptionId;
    private String historyPrescriptionId = "";
    private MedicineInfoResponse medicineInfoResponse;
    private String doctorName = "";
    private RatingSubmitRequest availableRating;
    private ConsultationFeeDetail followUpFeeDetail;
    private WebEngageModel webEngageModel;
    private CustomerResponse customerResponse;
    private ConsultationFeeResponse consultationFeeResponse;
    private boolean consultationHistoryExist;

    public HistoryResult getHistoryResult() {
        return historyResult;
    }

    public void setHistoryResult(HistoryResult historyResult) {
        this.historyResult = historyResult;
    }

    private HistoryResult historyResult;

    public ChatViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    private ApplyCoupon getAppliedCoupon() {
        return appliedCoupon;
    }

    private void setAppliedCoupon(ApplyCoupon appliedCoupon) {
        this.appliedCoupon = appliedCoupon;
    }

    private List<Speciality> getSpeciality() {
        return speciality;
    }

    private void setSpeciality(List<Speciality> speciality) {
        this.speciality = speciality;
    }

    private Speciality getSelectedSpeciality() {
        return selectedSpeciality;
    }

    private void setSelectedSpeciality(Speciality selectedSpeciality) {
        this.selectedSpeciality = selectedSpeciality;
    }

    private ApplyCoupon getOneTimeAppliedCoupon() {
        return oneTimeAppliedCoupon;
    }

    private void setOneTimeAppliedCoupon(ApplyCoupon oneTimeAppliedCoupon) {
        this.oneTimeAppliedCoupon = oneTimeAppliedCoupon;
    }

    public MutableLiveData<CreateChatResponse> getCreateChatMutableLiveData() {
        return createChatMutableLiveData;
    }

    public MutableLiveData<ConsultationUpdateResponse> getConsultationUpdateMutableLiveData() {
        return consultationUpdateMutableLiveData;
    }

    public MutableLiveData<DoctorListResponse> getDoctorListMutableLiveData() {
        return doctorListMutableLiveData;
    }

    public MutableLiveData<DoctorListResponse> getDoctorInfoMutableLiveData() {
        return doctorInfoMutableLiveData;
    }

    public MutableLiveData<CreateConsultationResponse> getCreateConsultationMutableLiveData() {
        return createConsultationMutableLiveData;
    }

    public MutableLiveData<ConsultationFeeResponse> getFeeMutableLiveData() {
        return feeMutableLiveData;
    }

    public MutableLiveData<ApplyCouponResponse> getApplyCouponMutableLiveData() {
        return applyCouponMutableLiveData;
    }

    public MutableLiveData<GetAllCouponResponse> getAllCouponMutableLiveData() {
        return allCouponMutableLiveData;
    }

    public MutableLiveData<UploadAttachmentResponse> getUploadAttachmentMutableLiveData() {
        return uploadAttachmentMutableLiveData;
    }

    public MutableLiveData<PaymentCreationResponse> getPaymentCreationMutableLiveData() {
        return paymentCreationMutableLiveData;
    }

    public MutableLiveData<ReoccurUpdateResponse> getReoccurUpdateMutableLiveData() {
        return reoccurUpdateMutableLiveData;
    }

    public MutableLiveData<ChatHistoryResponse> getChatHistoryMutableLiveData() {
        return chatHistoryMutableLiveData;
    }

    public MutableLiveData<MedicineInfoResponse> getMedicineInfoResponseMutableLiveData() {
        return medicineInfoResponseMutableLiveData;
    }

    public void init(Context context, ActivityChatBinding chatBinding, ChatViewModel chatViewModel, ChatListener chatListener, String intentFrom) {
        this.context = context;
        this.chatBinding = chatBinding;
        this.chatListener = chatListener;
        this.chatViewModel = chatViewModel;
        this.intentFrom = intentFrom;
        messageAdapterListener = this;
        messageList = new ArrayList<>();
        configMap = ConfigMap.getInstance();
        basePreference = BasePreference.getInstance(context);
        dateTimeUtils = DateTimeUtils.getInstance();
        justDocUserInformation = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
        webEngageModel = new WebEngageModel();
        MStarCustomerDetails mStarCustomerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        webEngageModel.setMStarUserDetails(mStarCustomerDetails);
        webEngageModel.setCommonProperties();
        //Web Engage  - consultation initiated
        WebEngageHelper.getInstance().consultationInitiatedEvent();
        this.userId = getUserId();
        initSocket();
        initSocketListener();
        initRadioButtonListener();
        initAnimation();
        initMessageAdapter();
    }

    public void init(Context context, ActivityChatBinding chatBinding, ChatViewModel chatViewModel, ChatListener chatListener, Bundle bundle, DoctorInformation doctorInformation, RatingSubmitRequest availableString, ConsultationFeeDetail followUpFeeDetail) {
        this.context = context;
        this.chatBinding = chatBinding;
        this.chatListener = chatListener;
        this.chatViewModel = chatViewModel;
        this.doctorInformation = doctorInformation;
        this.availableRating = availableString;
        this.followUpFeeDetail = followUpFeeDetail;

        if (bundle != null) {
            if (bundle.containsKey(ConsultationConstant.KEY_INTENT_FROM))
                this.intentFrom = bundle.getString(ConsultationConstant.KEY_INTENT_FROM);
            if (bundle.containsKey(ConsultationConstant.KEY_CHAT_ID))
                this.chatId = bundle.getString(ConsultationConstant.KEY_CHAT_ID);
            if (bundle.containsKey(ConsultationConstant.KEY_DOCTOR_ID))
                this.doctorId = bundle.getString(ConsultationConstant.KEY_DOCTOR_ID);
            if (bundle.containsKey(ConsultationConstant.KEY_USER_ID))
                this.userId = bundle.getString(ConsultationConstant.KEY_USER_ID);
            if (bundle.containsKey(ConsultationConstant.KEY_SPECIALIZATION))
                this.followUpSpecialization = bundle.getString(ConsultationConstant.KEY_SPECIALIZATION);
            if (bundle.containsKey(ConsultationConstant.KEY_SYMPTOM))
                this.followUpSymptom = bundle.getString(ConsultationConstant.KEY_SYMPTOM);
            if (bundle.containsKey(ConsultationConstant.KEY_CONSULTATION_EXPIRY))
                this.isConsultationExpiry = bundle.getBoolean(ConsultationConstant.KEY_CONSULTATION_EXPIRY);
            if (bundle.containsKey(ConsultationConstant.KEY_CONSULTATION_ID))
                this.consultationId = bundle.getString(ConsultationConstant.KEY_CONSULTATION_ID);
            if (bundle.containsKey(ConsultationConstant.KEY_PRESCRIPTION_ID))
                this.historyPrescriptionId = bundle.getString(ConsultationConstant.KEY_PRESCRIPTION_ID);
            if (bundle.containsKey(ConsultationConstant.KEY_DOCTOR_NAME))
                doctorName = bundle.getString(ConsultationConstant.KEY_DOCTOR_NAME);

        }
        messageAdapterListener = this;
        messageList = new ArrayList<>();
        basePreference = BasePreference.getInstance(context);
        configMap = ConfigMap.getInstance();
        dateTimeUtils = DateTimeUtils.getInstance();
        initSocket();
        initSocketListener();
        initRadioButtonListener();
        initAnimation();
        initMessageAdapter();
    }

    private void initChat() {
        switch (intentFrom) {
            case ConsultationConstant.FOLLOW_UP:
                followUp();
                break;
            case ConsultationConstant.CHAT_HISTORY:
                chatHistory();
                break;
            default:
                createChat();
                speciality();
                break;
        }
    }

    private void initAnimation() {
        viewShow = AnimationUtils.loadAnimation(context, R.anim.slide_up);
    }

    private void initSocket() {
        String socketUrl = configMap.getProperty(ConfigConstant.CONSULTATION_BASE_URL);
        try {
            IO.Options options = new IO.Options();
            options.transports = new String[]{WebSocket.NAME};
            if (mSocket == null)
                mSocket = IO.socket(socketUrl, options);

            mSocket.on(ConsultationConstant.SOCKET_EVENT_DOCTOR_JOINED_CHAT, doctor);
            mSocket.on(ConsultationConstant.SOCKET_EVENT_RECEIVE_MESSAGE, receiveMessage);
            mSocket.on(ConsultationConstant.SOCKET_EVENT_PRESCRIPTION_SUBMITTED, prescriptionSubmitted);
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.on(Socket.EVENT_ERROR, onError);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void initSocketListener() {
        ChatActivity.setListener(new ChatActivity.SocketListener() {
            @Override
            public void socketConnection() {
                connectSocket();
            }

            @Override
            public void socketDisconnection() {
                if (mSocket != null) {
                    mSocket.disconnect();
                }
            }
        });
    }

    private void connectSocket() {
        if (mSocket != null && !mSocket.connected()) {
            mSocket.connect();
        }
    }

    public void socketOff() {
        if (mSocket != null) {
            mSocket.disconnect();
            mSocket.off();
            mSocket.off(ConsultationConstant.SOCKET_EVENT_DOCTOR_JOINED_CHAT, doctor);
            mSocket.off(ConsultationConstant.SOCKET_EVENT_RECEIVE_MESSAGE, receiveMessage);
            mSocket.off(ConsultationConstant.SOCKET_EVENT_PRESCRIPTION_SUBMITTED, prescriptionSubmitted);
            mSocket.off(Socket.EVENT_CONNECT, onConnect);
            mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.off(Socket.EVENT_ERROR, onError);
        }
    }

    private void initRadioButtonListener() {
        chatBinding.radioBtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedRadioOption = ConsultationConstant.YES;
                messageList.add(createMessage(selectedRadioOption, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
                addNewItem(messageList);
                sendMessageToSocket(selectedRadioOption, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                isAnswered = true;
                showPostPaymentBotQuestion();
            }
        });

        chatBinding.radioBtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedRadioOption = ConsultationConstant.NO;
                messageList.add(createMessage(selectedRadioOption, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
                addNewItem(messageList);
                sendMessageToSocket(selectedRadioOption, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                isAnswered = true;
                query = queryLength;
                showPostPaymentBotQuestion();
            }
        });
    }

    private final Emitter.Listener receiveMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            chatListener.vmReceiveMessage(args);
        }
    };

    private final Emitter.Listener doctor = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            chatListener.vmDoctorJoinedChat(args);
        }
    };

    private final Emitter.Listener prescriptionSubmitted = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            chatListener.vmPrescriptionSubmitted(args);
        }
    };

    private final Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            chatListener.vmOnConnect();
        }
    };

    private final Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            chatListener.vmOnDisConnect();
        }
    };

    private final Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            chatListener.vmOnConnectError();
        }
    };

    private final Emitter.Listener onError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            chatListener.vmOnError();
        }
    };

    public void onConnect() {
        mSocket.emit(ConsultationConstant.SOCKET_EVENT_JOIN_ROOM, ConsultationConstant.PARAM_USER + userId);
        if (isChatInitialized) {
            isChatInitialized = false;
            initChat();
        }
        Log.d(TAG_SOCKET_CONNECTION, "Connected...");
    }

    public void onDisConnect() {
        Log.d(TAG_SOCKET_CONNECTION, "Disconnected....");
    }

    public void onConnectError() {
        connectSocket();
        Log.d(TAG_SOCKET_CONNECTION, "Failed to connect...");
    }

    public void onError() {
        connectSocket();
        Log.d(TAG_SOCKET_CONNECTION, "Error...");
    }

    public void onReceiveMessage(Object... args) {
        JSONObject messageObj = (JSONObject) args[0];
        if (messageObj != null)
            newReceivedMessage(messageObj);
    }

    private void newReceivedMessage(JSONObject messageObj) {
        Message receivedMessage = new Gson().fromJson(messageObj.toString(), Message.class);
        if (receivedMessage.getChatId().equals(chatId)) {
            checkLayoutVisibility();
            receivedMessage.setTime(dateTimeUtils.stringDate(receivedMessage.getTime(), DateTimeUtils.yyyyMMddHHmmss, DateTimeUtils.yyyyMMddTHHmmss));
            receivedMessage.setFromRole(ConsultationConstant.ROLE_DOCTOR);
            receivedMessage.setShowTime(false);
            messageList.add(receivedMessage);
            addNewItem(messageList);
        }
    }

    public void onDoctorJoined(Object... args) {
        JSONObject jsonObject = (JSONObject) args[1];
        chatJoinedDoctorInformation = new Gson().fromJson(jsonObject.toString(), DoctorInformation.class);
        doctorId = chatJoinedDoctorInformation != null && !TextUtils.isEmpty(chatJoinedDoctorInformation.getId()) ? chatJoinedDoctorInformation.getId() : "";
        isDoctorJoined = true;
        isSpecializationQuestionEnd = true;
        chatBinding.closeAttachment.setVisibility(View.GONE);
        disableEditOption();
        hideView();
        if (!isLoading)
            doctorJoinedNotification();

        //Web Engage Event - Doctor has replied
        if (webEngageModel != null) {
            WebEngageHelper.getInstance().doctorHasRepliedEvent(webEngageModel, chatListener.getContext());
        }
    }

    private void doctorJoinedNotification() {
        String doctorName = chatJoinedDoctorInformation != null && !TextUtils.isEmpty(chatJoinedDoctorInformation.getName()) ? chatJoinedDoctorInformation.getName() : "";
        showNotification(doctorName + " " + ConsultationConstant.DOCTOR_JOINED_NOTIFICATION, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
        addNewItem(messageList);
        showWriteMessage();
    }

    public void onPrescriptionSubmitted(String prescriptionId) {
        checkLayoutVisibility();
        showNotification(ConsultationConstant.PREPARE_PRESCRIPTION_NOTIFICATION, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
        addNewItem(messageList);
        this.prscriptionId = prescriptionId;

        // show the patient all the medicines prescribed and avail the user to order medicine directly.
        showOrderMedicineView(this.prscriptionId);
        // Web Engage Event - Consultation Complete/Prescription is ready
        if (webEngageModel == null) return;
        webEngageModel.setPrescriptionStatus(context.getResources().getString(R.string.txt_web_engage_consultation_complete));
        WebEngageHelper.getInstance().consultationCompletePrescriptionReadyEvent(webEngageModel, chatListener.getContext());
    }

    private void showOrderMedicineView(String prescriptionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            PrescriptionDetailsRequest prescriptionDetailsRequest = new PrescriptionDetailsRequest(prescriptionId, userId, true);
            ConsultationServiceManager.getInstance().getPrescriptionDetails(chatViewModel, basePreference, prescriptionDetailsRequest,
                    (intentFrom != null && intentFrom.equalsIgnoreCase(ConsultationConstant.FOLLOW_UP)) ? context.getResources().getString(R.string.follow_parameter) : "");
        } else {
            failedTransactionId = ConsultationServiceManager.MEDICINE;
        }
    }


    @Override
    public void imagePreview(String url) {
        if (url.contains(ConsultationConstant.JPEG) || url.contains(ConsultationConstant.JPG) || url.contains(ConsultationConstant.PNG))
            chatListener.vmShowImagePreview(url);
        else
            documentFormat(url);
    }

    private void documentFormat(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.contains(ConsultationConstant.HTTP))
            intent.setData(Uri.parse(url));
        else
            checkPdfOrDocFileFormat(url, intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            chatListener.vmOpenDocument(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void checkPdfOrDocFileFormat(String url, Intent intent) {
        File file = new File(url);
        final String authority = CommonUtils.getPackageInfo(chatListener.getContext()) + FileUtils.PROVIDER;
        Uri uri = FileProvider.getUriForFile(context, authority, file);
        if (url.contains(ConsultationConstant.DOC) || url.contains(ConsultationConstant.DOCX))
            intent.setDataAndType(uri, FileUtils.MIME_TYPE_DOC);
        else if (url.contains(ConsultationConstant.PDF))
            intent.setDataAndType(uri, FileUtils.MIME_TYPE_PDF);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }

    @Override
    public void editSelectedOption(int position, Message message) {
        if (!isLoading && message != null) {
            editPosition = position;
            setAppliedCoupon(new ApplyCoupon());
            setOneTimeAppliedCoupon(new ApplyCoupon());
            isCouponChanged = false;
            switch (message.getEditFrom()) {
                case ConsultationConstant.UPDATE_TYPE_SPECIALIZATION:
                    if (!isViewAllSpecializationOpen)
                        editSpecialization(message);
                    break;
                case ConsultationConstant.UPDATE_TYPE_CONSULTATION_MODE:
                    editConsultationMode();
                    break;
            }
        }
    }

    // Web Engage Event - Selected speciality
    private void fireSpecialitySelected(Speciality speciality) {
        if (speciality != null && !TextUtils.isEmpty(speciality.getName())) {
            if (speciality != null && !TextUtils.isEmpty(speciality.getName())) {
                webEngageModel.setSpeciality(speciality.getName());
            } else {
                webEngageModel.setSpeciality("");
            }
            WebEngageHelper.getInstance().specialitySelectedEvent(webEngageModel, chatListener.getContext());
        }
    }

    @Override
    public void selectedSpecialization(Speciality speciality) {
        fireSpecialitySelected(speciality);
        isViewAllSpecializationOpen = false;
        if (!TextUtils.isEmpty(getSelectedSpecialization())) {
            boolean isSpecializationChanged = false;

            if (speciality != null && speciality.isChecked() && !speciality.getCode().equals(getSelectedSpecialization())) {
                setSelectedSpeciality(speciality);
                isSpecializationChanged = true;
            }

            if (isSpecializationChanged) {
                queryLength = 0;
                query = 0;
                botQuestionIndex = 3;
                botQuestionLength = botQuestionList.size();
                removeExistingView();
                startAnimation(chatBinding.lytSpecialization);

                showSelectedSpecialization(true);
            }

        } else {
            setSelectedSpeciality(speciality);
            showSelectedSpecialization(false);
        }
    }

    @Override
    public void closeSpecialization() {
        isViewAllSpecializationOpen = false;
        if (!isEditSpecialisation) {
            for (Speciality speciality : getSpeciality())
                speciality.setChecked(false);
        }
    }

    @Override
    public void closeDoctorInfo() {
        isDoctorInfoOpen = false;
    }

    @Override
    public void viewDoctorInfo(int doctorId) {
        if (!isDoctorInfoOpen) {
            isDoctorInfoOpen = true;
            getDoctorInfo(doctorId);
        }
    }

    @Override
    public void showAllCoupons() {
        if (!isPromoCodeOpen) {
            isPromoCodeOpen = true;
            switch (selectedPaymentPlan) {
                case ConsultationConstant.UNLIMITED_PLAN:
                    List<ConsultationCoupon> unlimitedPromoCodeList = new ArrayList<>();
                    ConsultationCoupon consultationCoupon = new ConsultationCoupon();
                    consultationCoupon.setCode(ConsultationConstant.UNLIMITED_COUPON);
                    consultationCoupon.setDescription(ConsultationConstant.UNLIMITED_COUPON_DESCRIPTION);
                    unlimitedPromoCodeList.add(consultationCoupon);
                    chatListener.vmShowPromoCodeList(unlimitedPromoCodeList, ConsultationConstant.UNLIMITED_COUPON, this);
                    break;
                case ConsultationConstant.ONETIME_PLAN:
                    if (consultationCouponList != null && consultationCouponList.size() > 0) {
                        String appliedCouponCode = getOneTimeAppliedCoupon() != null && getOneTimeAppliedCoupon().getCode() != null ? getOneTimeAppliedCoupon().getCode() : "";
                        chatListener.vmShowPromoCodeList(consultationCouponList, appliedCouponCode, this);
                    }
                    break;
            }
        }
    }

    @Override
    public void paymentPlan(double fee, String selectedPaymentPlan) {
        totalAmount = fee;
        this.selectedPaymentPlan = selectedPaymentPlan;
        switch (selectedPaymentPlan) {
            case ConsultationConstant.UNLIMITED_PLAN:
                ApplyCoupon unlimitedAppliedCoupon = new ApplyCoupon();
                unlimitedAppliedCoupon.setCode(ConsultationConstant.UNLIMITED_COUPON);
                unlimitedAppliedCoupon.setDescription(ConsultationConstant.UNLIMITED_COUPON_DESCRIPTION);
                setAppliedCoupon(unlimitedAppliedCoupon);
                break;
            case ConsultationConstant.ONETIME_PLAN:
                setAppliedCoupon(getOneTimeAppliedCoupon());
                break;
        }

        if (getAppliedCoupon() != null && getAppliedCoupon().getCode() != null)
            updateCoupon();

        if (totalAmount == 0.00) {
            chatBinding.btnMakePayment.setVisibility(View.GONE);
            sendMessageToSocket(selectedPaymentPlan.equals(ConsultationConstant.UNLIMITED_PLAN) ? ConsultationConstant.UNLIMITED : ConsultationConstant.ONE_TIME, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
            orderId = getOrderId();
            paymentCreation();
        }
    }

    private String getOrderId() {
        orderCreatedTime = System.currentTimeMillis();
        return ConsultationConstant.ORDER_ID_PREFIX + userId + orderCreatedTime;
    }

    @Override
    public void selectedCouponCode(ConsultationCoupon coupon) {
        isPromoCodeOpen = false;
        if (coupon != null && !TextUtils.isEmpty(coupon.getCode()) && !dateTimeUtils.isCouponExpired(coupon.getExpiry(), DateTimeUtils.yyyyMMddTHHmmss)) {
            isCouponChanged = true;
            applyCoupon(coupon);
        }
    }

    @Override
    public void closeCouponList() {
        isPromoCodeOpen = false;
    }

    @Override
    public void selectedProblem(Options selectedProblem) {
        if (selectedProblem != null && !TextUtils.isEmpty(selectedProblem.getOption())) {
            chatBinding.lytProblemCategory.setVisibility(View.GONE);
            Message sentMessage = new Message();
            sentMessage.setMessage(selectedProblem.getOption());
            sentMessage.setType(ConsultationConstant.MESSAGE_TYPE_TEXT);
            sentMessage.setFromRole(ConsultationConstant.ROLE_USER);
            sentMessage.setTime(dateTimeUtils.currentTime(new Date(), DateTimeUtils.yyyyMMddTHHmmss));
            sentMessage.setShowTime(true);
            sentMessage.setEditable(false);
            messageList.add(sentMessage);
            addNewItem(messageList);
            sendMessageToSocket(selectedProblem.getOption(), ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
            isAnswered = true;
            showPostPaymentBotQuestion();
        }
    }

    @Override
    public void selectedOption(List<Options> options) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Options option : options) {
            if (option.isChecked()) {
                if (stringBuilder.length() > 0)
                    stringBuilder.append(" , ").append(option.getOption());
                else
                    stringBuilder.append(option.getOption());
            }
        }
        chatBinding.btnMultiChoiceDone.setTag(stringBuilder.toString());
        selectedMultiOption = stringBuilder.toString();
        if (stringBuilder.length() > 0)
            chatBinding.btnMultiChoiceDone.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));
        else
            chatBinding.btnMultiChoiceDone.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorLightPaleBlueGrey)));
    }

    @Override
    public void otherOption() {
        chatBinding.imgAttachment.setVisibility(View.GONE);
        chatBinding.lytMultiChoice.setVisibility(View.GONE);
        startAnimation(chatBinding.lytWriteMessage);
        sendMessageFrom = ConsultationConstant.OTHER;
        chatBinding.editTextMessage.requestFocus();
        showForcedKeyboard();
    }

    @Override
    public void loaderView(boolean isShowing) {
        isLoaderView = isShowing;
    }

    @Override
    public void downloadAttachment(Message message, int position) {
        this.downloadFilePosition = position;
        String id = !TextUtils.isEmpty(message.getChatId()) ? message.getChatId() : "";
        String hashId = !TextUtils.isEmpty(message.getHashId()) ? message.getHashId() : "";
        downloadAttachmentUrl = ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL) + ConsultAPIPathConstant.DOWNLOAD_ATTACHMENT + id + "/" + hashId;
        chatListener.vmInitDownloadAttachment();
    }

    @Override
    public void orderMedicine(MedicineInfoResponse medicineInfoResponse) {
        this.medicineInfoResponse = medicineInfoResponse;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            addToCart();
            // Track Order Medicine From Consultation
            if (!TextUtils.isEmpty(consultationId) && !TextUtils.isEmpty(basePreference.getLoganSession()))
                ConsultationServiceManager.getInstance().trackOrderMedicine(consultationId, basePreference.getLoganSession());
            chatListener.vmShowProgress();
        } else {
            failedTransactionId = ORDER_MEDICINE;

        }
        // GA for order Medicine from consultation
        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_CONSULTATION_SECTION, EVENT_ACTION_CONSULTATION_ORDER_MEDICINE,
                EVENT_LABLE_ORDER_MEDICINE_FROM_CONSULTATION);
    }

    private void addToCart() {
        if (basePreference.getMStarCartId() > 0) {
            checkCartStatusToAddToCart();
        } else {
            initiateAPICall(MSTAR_CREATE_CART);
        }
    }

    private void initiateAPICall(int transactionId) {
        switch (transactionId) {
            case MSTAR_CREATE_CART:
                APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                break;
            case MSTAR_ADD_MULTIPLE_PRODUCTS:
                APIServiceManager.getInstance().mstarAddMutlipleProducts(this, basePreference.getMstarBasicHeaderMap(), getProductList());
                break;
        }
    }

    private HashMap<String, Integer> getProductList() {
        HashMap<String, Integer> productList = new HashMap<>();
        if (medicineInfoResponse == null || medicineInfoResponse.getMedicines() == null || medicineInfoResponse.getMedicines().size() == 0)
            return productList;
        for (Medicine medicine : medicineInfoResponse.getMedicines()) {
            productList.put(medicine.getMedicineId(), 1);
        }
        return productList;
    }


    @Override
    public void bookLabTest(MedicineInfoResponse medicineInfoResponse) {
        if (medicineInfoResponse == null || medicineInfoResponse.getLabTests() == null || medicineInfoResponse.getLabTests().size() == 0)
            return;
        this.medicineInfoResponse = medicineInfoResponse;
        chatListener.navigateToBookLabTest(medicineInfoResponse);
    }

    @Override
    public void onSubmitRating(Float rating) {
        showRating(true, rating);
    }

    private void showForcedKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void onMultiChoice(View v) {
        if (v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString())) {
            chatBinding.lytMultiChoice.setVisibility(View.GONE);
            messageList.add(createMessage(v.getTag().toString(), ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
            addNewItem(messageList);
            sendMessageToSocket(v.getTag().toString(), ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
            chatBinding.btnMultiChoiceDone.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorLightPaleBlueGrey)));
            selectedMultiOption = "";
            chatBinding.btnMultiChoiceDone.setTag("");
            isAnswered = true;
            showPostPaymentBotQuestion();
        }
    }

    private void editSpecialization(Message message) {
        isViewAllSpecializationOpen = true;
        isEditSpecialisation = true;
        for (Speciality editSpeciality : getSpeciality()) {
            if (editSpeciality.getCode().equals(message.getSpecialization()))
                editSpeciality.setChecked(true);
            else
                editSpeciality.setChecked(false);
        }
        if (getSpeciality() != null && getSpeciality().size() > 0)
            chatListener.vmShowSpecialization(getSpeciality(), isEditSpecialisation, this);
    }

    private void editConsultationMode() {
        queryLength = 0;
        query = 0;
        botQuestionIndex = 6;
        botQuestionLength = botQuestionList.size();
        removeExistingView();
        startAnimation(chatBinding.lytConsultationMode);
    }

    private void removeExistingView() {
        hideView();
        int chatListSize = messageList.size();
        int removeElement = chatListSize - editPosition;
        if (chatListSize > editPosition) {
            for (int i = 0; i < removeElement; i++) {
                removeLoader();
            }
        }
    }

    private void hideView() {
        chatBinding.lytMyself.setVisibility(View.GONE);
        chatBinding.lytSpecialization.setVisibility(View.GONE);
        chatBinding.lytSymptom.setVisibility(View.GONE);
        chatBinding.lytConsultationMode.setVisibility(View.GONE);
        chatBinding.btnMakePayment.setVisibility(View.GONE);
        chatBinding.lytProblemCategory.setVisibility(View.GONE);
        chatBinding.lytMultiChoice.setVisibility(View.GONE);
        chatBinding.lytWriteMessage.setVisibility(View.GONE);
        chatBinding.lytDate.setVisibility(View.GONE);
        chatBinding.lytAttachment.setVisibility(View.GONE);
        chatBinding.lytRadioGroup.setVisibility(View.GONE);
        chatBinding.radioBtnYes.setChecked(false);
        chatBinding.radioBtnNo.setChecked(false);
    }

    private void showSelectedSpecialization(boolean isUpdateSpecialization) {
        if (getSelectedSpeciality() != null && !TextUtils.isEmpty(getSelectedSpeciality().getName())) {
            messageList.add(specializationMessage(getSelectedSpeciality().getName(), getSelectedSpeciality().getCode()));
            addNewItem(messageList);
            sendMessageToSocket(getSelectedSpeciality().getName(), ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
            chatBinding.lytSpecialization.setVisibility(View.GONE);
            isAnswered = true;
            if (isUpdateSpecialization)
                checkConsultationCreation();
            else {
                messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
                addNewItem(messageList);
                removeTypingView();
            }
        }
    }

    private void checkConsultationCreation() {
        if (TextUtils.isEmpty(consultationId))
            createConsultation();
        else
            updateSpecialization();
    }

    private void checkEditSpecialization() {
        if (isEditSpecialisation) {
            queryLength = 0;
            query = 0;
            botQuestionIndex = 3;
            botQuestionLength = botQuestionList.size();
            chatBinding.radioBtnYes.setChecked(false);
            chatBinding.radioBtnNo.setChecked(false);
            chatBinding.lytSymptom.setVisibility(View.GONE);
            chatBinding.lytConsultationMode.setVisibility(View.GONE);
            chatBinding.btnMakePayment.setVisibility(View.GONE);
            chatBinding.lytRadioGroup.setVisibility(View.GONE);
            chatBinding.lytWriteMessage.setVisibility(View.GONE);
            chatBinding.lytMultiChoice.setVisibility(View.GONE);
            chatBinding.lytProblemCategory.setVisibility(View.GONE);
            chatBinding.lytDate.setVisibility(View.GONE);

            int chatListSize = messageList.size();
            int removeElement = chatListSize - editPosition;
            if (chatListSize > editPosition) {
                for (int i = 0; i < removeElement; i++) {
                    removeLoader();
                }
            }
        }
    }

    public interface ChatListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmReceiveMessage(Object... args);

        void vmDoctorJoinedChat(Object... args);

        void vmPrescriptionSubmitted(Object... args);

        void vmOnConnect();

        void vmOnDisConnect();

        void vmOnConnectError();

        void vmOnError();

        void vmShowSpecialization(List<Speciality> specialityList, boolean editSpecialization, MessageAdapterListener messageAdapterListener);

        void vmShowDoctorInfo(DoctorInformation doctorInformation, MessageAdapterListener messageAdapterListener);

        void vmShowPromoCodeList(List<ConsultationCoupon> promoCodeList, String appliedPromoCode, MessageAdapterListener messageAdapterListener);

        void vmShowProblemCategory(List<Options> problemCategoryList, MessageAdapterListener messageAdapterListener);

        void vmShowDatePicker();

        void vmInitCameraIntent();

        void vmInitGalleryIntent();

        void vmInitDocumentIntent();

        Uri getUriFromCameraImage(int resultCode);

        Uri getUriFromGalleryImage(int resultCode, Intent data);

        boolean validateUploadedImage(String path, Application application, ViewGroup mainLayout, Long uploadSizeLimit, String maxFileSizeError);

        boolean validateUploadedDocument(String path, Application application, ViewGroup mainLayout, Long uploadSizeLimit);

        void vmShowImagePreview(String url);

        void vmOpenDocument(Intent intent);

        void vmCallBackPayment(Bundle intent);

        void vmInitDocumentView();

        void vmInitCameraView();

        void vmInitGalleryView();

        void vmOnFailed();

        void vmInitDownloadAttachment();

        void navigateToBookLabTest(MedicineInfoResponse medicineInfoResponse);

        Context getContext();

    }

    private void showNoNetworkView(boolean isConnected) {
        chatBinding.lytChatViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        chatBinding.chatNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        chatBinding.lytChatViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        chatBinding.chatApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }


    private StringBuilder medicines, dosages;


    private void getMedicineAndDosage(ArrayList<Medicine> medicineList) {
        medicines = new StringBuilder();
        dosages = new StringBuilder();

        for (Medicine test : medicineList) {
            if (!TextUtils.isEmpty(test.getMedicine())) {
                medicines.append(test.getMedicine());
                medicines.append(",");
            }
            if (!TextUtils.isEmpty(test.getDosage())) {
                dosages.append(test.getDosage());
                dosages.append(",");
            }
        }
    }

    private boolean isLabTestPrescribed, isMedicinesPrescribed;

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case ConsultationServiceManager.CREATE_CHAT:
                CreateChatResponse createChatResponse = new Gson().fromJson(data, CreateChatResponse.class);
                createChatMutableLiveData.setValue(createChatResponse);
                break;
            case ConsultationServiceManager.UPDATE_CONSULTATION:
                ConsultationUpdateResponse consultationUpdateResponse = new Gson().fromJson(data, ConsultationUpdateResponse.class);
                consultationUpdateMutableLiveData.setValue(consultationUpdateResponse);
                break;
            case ConsultationServiceManager.GET_ALL_SPECIALITY:
                getAllSpecialityResponse(data);
                break;
            case ConsultationServiceManager.DOCTOR_LIST:
                DoctorListResponse doctorListResponse = new Gson().fromJson(data, DoctorListResponse.class);
                doctorListMutableLiveData.setValue(doctorListResponse);
                break;
            case ConsultationServiceManager.DOCTOR_INFO:
                DoctorListResponse doctorInfoResponse = new Gson().fromJson(data, DoctorListResponse.class);
                doctorInfoMutableLiveData.setValue(doctorInfoResponse);
                break;
            case ConsultationServiceManager.CREATE_CONSULTATION:
                CreateConsultationResponse createConsultationResponse = new Gson().fromJson(data, CreateConsultationResponse.class);
                createConsultationMutableLiveData.setValue(createConsultationResponse);
                break;
            case ConsultationServiceManager.CONSULTATION_FEE:
                consultationFeeResponse = new Gson().fromJson(data, ConsultationFeeResponse.class);
                feeMutableLiveData.setValue(consultationFeeResponse);
                break;
            case ConsultationServiceManager.APPLY_COUPON:
                ApplyCouponResponse applyCouponResponse = new Gson().fromJson(data, ApplyCouponResponse.class);
                applyCouponMutableLiveData.setValue(applyCouponResponse);
                break;
            case ConsultationServiceManager.GET_ALL_COUPON:
                GetAllCouponResponse allCouponResponse = new Gson().fromJson(data, GetAllCouponResponse.class);
                allCouponMutableLiveData.setValue(allCouponResponse);
                break;
            case ConsultationServiceManager.UPLOAD_ATTACHMENT:
                UploadAttachmentResponse uploadAttachmentResponse = new Gson().fromJson(data, UploadAttachmentResponse.class);
                uploadAttachmentMutableLiveData.setValue(uploadAttachmentResponse);
                break;
            case ConsultationServiceManager.PAYMENT_CREATION:
                PaymentCreationResponse paymentCreationResponse = new Gson().fromJson(data, PaymentCreationResponse.class);
                paymentCreationMutableLiveData.setValue(paymentCreationResponse);
                break;
            case ConsultationServiceManager.REOCCUR_UPDATE:
                ReoccurUpdateResponse reoccurUpdateResponse = new Gson().fromJson(data, ReoccurUpdateResponse.class);
                reoccurUpdateMutableLiveData.setValue(reoccurUpdateResponse);
                break;
            case ConsultationServiceManager.CHAT_HISTORY:
                ChatHistoryResponse chatHistoryResponse = new Gson().fromJson(data, ChatHistoryResponse.class);
                chatHistoryMutableLiveData.setValue(chatHistoryResponse);
                break;
            case ConsultationServiceManager.MEDICINE: {
                try {
                    JSONArray medicineJsonArray = new JSONArray(data);
                    JSONObject medicineJsonObject = medicineJsonArray.getJSONObject(0);
                    MedicineInfoResponse medicineInfoResponse = new Gson().fromJson(medicineJsonObject.toString(), MedicineInfoResponse.class);
                    medicineInfoResponseMutableLiveData.setValue(medicineInfoResponse);

                    if (medicineInfoResponse != null) {
                        messageAdapter.setMedicineInfoResponse(medicineInfoResponse);

                        // Web Engage Event - if Medicine Or Lab Test is prescribed, If LabTest is prescribed
                        if (medicineInfoResponse.getMedicines() != null && medicineInfoResponse.getMedicines().size() > 0 && !TextUtils.isEmpty(medicineInfoResponse.getMedicines().get(0).getMedicineId())) {
                            isMedicinesPrescribed = true;
                            getMedicineAndDosage(medicineInfoResponse.getMedicines());
                            if (webEngageModel != null) {
                                webEngageModel.setPrescribedMedicines(medicines.toString());
                                webEngageModel.setDosagePrescribed(dosages.toString());
                            }
                        }
                        if (medicineInfoResponse.getLabTests() != null && medicineInfoResponse.getLabTests().size() > 0 && webEngageModel != null) {
                            isLabTestPrescribed = true;
                            webEngageModel.setLabTestPrescribed(context.getResources().getString(R.string.txt_web_engage_consultation_yes));
                            webEngageModel.setLabTestName(ConsultationUtil.getLabTestName(medicineInfoResponse.getLabTests()));
                        }
                        if (webEngageModel != null) {
                            if (isLabTestPrescribed || isMedicinesPrescribed) {
                                WebEngageHelper.getInstance().ifLabTestOrMedicineIsPrescribed(webEngageModel, chatListener.getContext());
                            }
                            if (isLabTestPrescribed) {
                                WebEngageHelper.getInstance().ifLabTestPrescribedEvent(webEngageModel, chatListener.getContext());
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (isConsultationExpiry) {
                    showFollowupInChat();
                    showRating(false, null);
                }
                chatListener.vmDismissProgress();
                break;
            }
            case APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS:
                onAddedProductResponse(data);
                break;
            case MSTAR_CREATE_CART:
                createMStarCartResponse(data);
                break;
            case ConsultationServiceManager.PATIENT_DETAILS:
                MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                customerResponse = CommonUtils.convertMStarObjectToCustomer(mStarBasicResponseTemplateModel);
                break;
        }
    }

    private void createMStarCartResponse(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            basePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            checkCartStatusToAddToCart();
        }
    }

    private void checkCartStatusToAddToCart() {
        boolean isCartOpen = basePreference.isMStarM1CartOpen();
            initiateAPICall(MSTAR_ADD_MULTIPLE_PRODUCTS);
    }

    private void onAddedProductResponse(String data) {
        chatListener.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarAddMultipleProductsResponse response = new Gson().fromJson(data, MStarAddMultipleProductsResponse.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getAddedProductsResultMap() != null && !response.getAddedProductsResultMap().isEmpty()) {
                Intent intent = new Intent();
                if (medicineInfoResponse != null && !TextUtils.isEmpty(medicineInfoResponse.getPrescImageBase64())) {
                    intent.putExtra(PRESCRIPTION_BASE_URL, medicineInfoResponse.getPrescImageBase64());
                    intent.putExtra(IntentConstant.IS_FROM_CONSULTATION_TO_CART, true);
                }
                LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_netmeds_mstar_cart), intent, context);
            } else {
                // show message - problem to order medicines
                SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, context.getResources().getString(R.string.txt_error_placing_order));
            }
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        chatListener.vmDismissProgress();
        chatListener.vmOnFailed();
        isLoading = false;
        isCouponChanged = false;
        if (transactionId == MSTAR_CREATE_CART || transactionId == MSTAR_ADD_MULTIPLE_PRODUCTS)
            transactionId = ORDER_MEDICINE;
        vmOnError(transactionId);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case ConsultationServiceManager.CREATE_CHAT:
                createChat();
                break;
            case ConsultationServiceManager.FOLLOW_UP:
                followUp();
                break;
            case ConsultationServiceManager.SPECIALIZATION_UPDATE:
                updateSpecialization();
                break;
            case ConsultationServiceManager.GET_ALL_SPECIALITY:
                speciality();
                break;
            case ConsultationServiceManager.DOCTOR_LIST:
                doctorList();
                break;
            case ConsultationServiceManager.DOCTOR_INFO:
                getDoctorInfo(viewDoctorInfoId);
                break;
            case ConsultationServiceManager.CREATE_CONSULTATION:
                createConsultation();
                break;
            case ConsultationServiceManager.CONSULTATION_MODE_UPDATE:
                consultationModeUpdate();
                break;
            case ConsultationServiceManager.CONSULTATION_FEE:
                consultationFee();
                break;
            case ConsultationServiceManager.APPLY_COUPON:
                applyCoupon(retryCouponCode);
                break;
            case ConsultationServiceManager.GET_ALL_COUPON:
                allCoupon();
                break;
            case ConsultationServiceManager.UPLOAD_ATTACHMENT:
                setMultipartBuilder();
                break;
            case ConsultationServiceManager.UPDATE_COUPON:
                updateCoupon();
                break;
            case ConsultationServiceManager.PAYMENT_CREATION:
                paymentCreation();
                break;
            case ConsultationServiceManager.PAYMENT_UPDATE:
                paymentUpdate();
                break;
            case ConsultationServiceManager.REOCCUR_UPDATE:
                reoccurUpdate();
                break;
            case ConsultationServiceManager.CHAT_HISTORY:
                chatHistory();
                break;
            case ConsultationServiceManager.MEDICINE: {
                showOrderMedicineView(prscriptionId);
                break;
            }
            case ORDER_MEDICINE: {
                orderMedicine(getMedicineInfoResponseMutableLiveData().getValue());
                break;
            }
        }
    }

    private void createChat() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            long time = new Date().getTime();
            String id = justDocUserInformation != null && justDocUserInformation.getSession() != null && justDocUserInformation.getSession().getId() != null ? justDocUserInformation.getSession().getId() + ":" + time : "";
            CreateChatRequest createChatRequest = new CreateChatRequest();
            createChatRequest.setId(id);
            createChatRequest.setUserId(getUserId());
            chatListener.vmShowProgress();
            ConsultationServiceManager.getInstance().createChat(chatViewModel, createChatRequest);
        } else {
            failedTransactionId = ConsultationServiceManager.CREATE_CHAT;
        }
    }

    private String getUserId() {
        return justDocUserInformation != null && justDocUserInformation.getId() != null ? justDocUserInformation.getId() : "";
    }

    public void onCreateChatDataAvailable(CreateChatResponse createChatResponse) {
        chatListener.vmDismissProgress();
        if (createChatResponse != null && createChatResponse.getServiceStatus() != null && createChatResponse.getServiceStatus().getStatusCode() != null)
            checkCreateChatStatus(createChatResponse);
        else
            vmOnError(ConsultationServiceManager.CREATE_CHAT);
    }

    private void checkCreateChatStatus(CreateChatResponse createChatResponse) {
        if (createChatResponse.getServiceStatus().getStatusCode() == 200)
            checkCreateChat(createChatResponse);
        else
            showCreateChatFailureMessage(createChatResponse);
    }

    private void checkCreateChat(CreateChatResponse createChatResponse) {
        if (createChatResponse.getResult() != null && createChatResponse.getResult().getObject() != null) {
            CreateChatObject createChatObject = createChatResponse.getResult().getObject();
            chatId = createChatObject.getId() != null ? createChatObject.getId() : "";
            //init bot question
            initBotQuestion();
        } else
            vmOnError(ConsultationServiceManager.CREATE_CHAT);
    }

    private void showCreateChatFailureMessage(CreateChatResponse createChatResponse) {
        if (createChatResponse.getResult() != null && !TextUtils.isEmpty(createChatResponse.getResult().getMessage()))
            SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, createChatResponse.getResult().getMessage());
        else
            vmOnError(ConsultationServiceManager.CREATE_CHAT);
    }

    private void parseSpecializationBotQuestion() {
        try {
            JsonReader jsonReader = JsonHelper.readJSONFromAsset(context, "specializationQuestion.json");
            specializationBotQuestion = new Gson().fromJson(jsonReader, SpecializationBotQuestion.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getSpecializationBasedBotQuestion(String specialization) {
        if (!TextUtils.isEmpty(specialization)) {
            symptomQuestionList = new ArrayList<>();
            switch (specialization) {
                case ConsultationConstant.GYNECOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getGynecologyQuestionList());
                    break;
                case ConsultationConstant.DERMATOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getDermatologyQuestionList());
                    break;
                case ConsultationConstant.RADIOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getRadiologyQuestionList());
                    break;
                case ConsultationConstant.ONCOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getOncologyQuestionList());
                    break;
                case ConsultationConstant.CARDIOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getCardiologyQuestionList());
                    break;
                case ConsultationConstant.SEXOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getSexologyQuestionList());
                    break;
                case ConsultationConstant.PEDIATRICS_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getPediatricsQuestionList());
                    break;
                case ConsultationConstant.PSYCHOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getPsychologyQuestionList());
                    break;
                case ConsultationConstant.ORTHOPEDICS_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getOrthopedicsQuestionList());
                    break;
                case ConsultationConstant.NUTRITIONIST_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getNutritionistQuestionList());
                    break;
                case ConsultationConstant.GASTROENTEROLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getGastroenterologyQuestionList());
                    break;
                case ConsultationConstant.ENT_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getEntQuestionList());
                    break;
                case ConsultationConstant.PULMONOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getPulmonologyQuestionList());
                    break;
                case ConsultationConstant.OPHTHALMOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getOphthalmologyQuestionList());
                    break;
                case ConsultationConstant.ENDOCRINOLOGY_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getEndocrinologyQuestionList());
                    break;
                case ConsultationConstant.GENERAL_PHYSICIAN_CODE:
                    symptomQuestionList.addAll(specializationBotQuestion.getGeneralPhysicianQuestionList());
                    break;
            }
        }
    }

    private void setUpSpecialisationBotQuestion() {
        if (!isDoctorJoined) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    removeLoader();
                    if (isAnswered) {
                        botSymptomQuestion = symptomQuestionList.get(query);
                        messageList.add(createMessage(botSymptomQuestion.getField(), botSymptomQuestion.getRole(), false, botSymptomQuestion.getType()));
                        addNewItem(messageList);
                        sendMessageToSocket(botSymptomQuestion.getField(), ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_BOT);
                        query = botSymptomQuestion.getNextIndex() != 0 ? botSymptomQuestion.getNextIndex() : queryLength;
                        CommonUtils.hideKeyboard(context, chatBinding.lytWriteMessage);
                        switch (botSymptomQuestion.getFieldType()) {
                            case ConsultationConstant.POST_PAYMENT_QUESTION_DROP_DOWN_TYPE:
                                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                                chatBinding.lytWriteMessage.setVisibility(View.GONE);
                                chatBinding.lytMultiChoice.setVisibility(View.GONE);
                                chatBinding.lytDate.setVisibility(View.GONE);
                                chatBinding.lytAttachment.setVisibility(View.GONE);
                                startAnimation(chatBinding.lytProblemCategory);
                                break;
                            case ConsultationConstant.POST_PAYMENT_QUESTION__MULTI_CHOICE_TYPE:
                            case ConsultationConstant.POST_PAYMENT_QUESTION__MULTI_CHOICE_WITH_OTHER_OPTION_TYPE:
                                chatBinding.lytProblemCategory.setVisibility(View.GONE);
                                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                                chatBinding.lytWriteMessage.setVisibility(View.GONE);
                                chatBinding.lytDate.setVisibility(View.GONE);
                                chatBinding.lytAttachment.setVisibility(View.GONE);
                                GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
                                chatBinding.multiChoiceList.setLayoutManager(gridLayoutManager);
                                multiChoiceAdapter = new MultiChoiceAdapter(context, application, botSymptomQuestion.getOptionsList(), messageAdapterListener);
                                chatBinding.multiChoiceList.setAdapter(multiChoiceAdapter);
                                startAnimation(chatBinding.lytMultiChoice);
                                break;
                            case ConsultationConstant.POST_PAYMENT_QUESTION__DATE_TYPE:
                                chatBinding.lytProblemCategory.setVisibility(View.GONE);
                                chatBinding.lytMultiChoice.setVisibility(View.GONE);
                                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                                chatBinding.lytWriteMessage.setVisibility(View.GONE);
                                chatBinding.lytAttachment.setVisibility(View.GONE);
                                startAnimation(chatBinding.lytDate);
                                break;
                            case ConsultationConstant.POST_PAYMENT_QUESTION__ATTACHMENT_TYPE:
                                chatBinding.lytProblemCategory.setVisibility(View.GONE);
                                chatBinding.lytMultiChoice.setVisibility(View.GONE);
                                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                                chatBinding.lytDate.setVisibility(View.GONE);
                                chatBinding.lytWriteMessage.setVisibility(View.GONE);
                                chatBinding.imgAttachment.setVisibility(View.GONE);
                                startAnimation(chatBinding.lytAttachment);
                                break;
                            case ConsultationConstant.POST_PAYMENT_QUESTION__RADIO_BUTTON_TYPE:
                                chatBinding.lytProblemCategory.setVisibility(View.GONE);
                                chatBinding.lytMultiChoice.setVisibility(View.GONE);
                                chatBinding.lytWriteMessage.setVisibility(View.GONE);
                                chatBinding.lytDate.setVisibility(View.GONE);
                                chatBinding.lytAttachment.setVisibility(View.GONE);
                                startAnimation(chatBinding.lytRadioGroup);
                                break;
                            case ConsultationConstant.POST_PAYMENT_QUESTION__TEXT_TYPE:
                                chatBinding.lytProblemCategory.setVisibility(View.GONE);
                                chatBinding.lytMultiChoice.setVisibility(View.GONE);
                                chatBinding.lytRadioGroup.setVisibility(View.GONE);
                                chatBinding.lytDate.setVisibility(View.GONE);
                                chatBinding.imgAttachment.setVisibility(View.GONE);
                                chatBinding.lytAttachment.setVisibility(View.GONE);
                                startAnimation(chatBinding.lytWriteMessage);
                                sendMessageFrom = ConsultationConstant.POST_PAYMENT_QUESTION__TEXT_TYPE;
                                break;
                        }
                        isLoading = false;
                    }

                }
            }, ConsultationConstant.BOT_QUESTION_DELAY_TIME);
        } else {
            removeLoader();
            isLoading = false;
            isAnswered = false;
            doctorJoinedNotification();
        }
    }

    private void followUp() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            ConsultationUpdateRequest followUpRequest = new ConsultationUpdateRequest();
            followUpRequest.setType(ConsultationConstant.UPDATE_TYPE_FOLLOW_UP);
            followUpRequest.setUserId(userId);
            ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
            updateConsultingPayload.setId(chatId);
            FollowUp followUp = new FollowUp();
            followUp.setSymptoms(followUpSymptom);
            updateConsultingPayload.setFollowUp(followUp);
            followUpRequest.setConsultingPayload(updateConsultingPayload);
            ConsultationServiceManager.getInstance().updateConsultation(chatViewModel, basePreference, followUpRequest, "", ConsultationConstant.UPDATE_TYPE_FOLLOW_UP, ConsultationServiceManager.FOLLOW_UP);
        } else {
            failedTransactionId = ConsultationServiceManager.FOLLOW_UP;
        }
    }

    public void onConsultationUpdateDataAvailable(ConsultationUpdateResponse consultationUpdateResponse) {
        isLoading = false;
        if (consultationUpdateResponse.getServiceStatus() != null && consultationUpdateResponse.getServiceStatus().getStatusCode() != null)
            updateConsultationStatus(consultationUpdateResponse);
        else
            consultationUpdateApiError(consultationUpdateResponse);
    }

    private void consultationUpdateApiError(ConsultationUpdateResponse consultationUpdateResponse) {
        removeLoader();
        if (consultationUpdateResponse.getUpdateType() != null) {
            switch (consultationUpdateResponse.getUpdateType()) {
                case ConsultationConstant.UPDATE_TYPE_FOLLOW_UP:
                    vmOnError(ConsultationServiceManager.FOLLOW_UP);
                    break;
                case ConsultationConstant.UPDATE_TYPE_SPECIALIZATION:
                    vmOnError(ConsultationServiceManager.SPECIALIZATION_UPDATE);
                    break;
                case ConsultationConstant.UPDATE_TYPE_CONSULTATION_MODE:
                    vmOnError(ConsultationServiceManager.CONSULTATION_MODE_UPDATE);
                    break;
                case ConsultationConstant.UPDATE_TYPE_COUPON:
                    vmOnError(ConsultationServiceManager.UPDATE_COUPON);
                    break;
                case ConsultationConstant.UPDATE_TYPE_PAYMENT:
                    vmOnError(ConsultationServiceManager.PAYMENT_UPDATE);
                    break;
            }
        }
    }

    private void updateConsultationStatus(ConsultationUpdateResponse consultationUpdateResponse) {
        if (consultationUpdateResponse.getServiceStatus().getStatusCode() == 200)
            consultationUpdateResult(consultationUpdateResponse);
        else
            consultationUpdateFailureMessage(consultationUpdateResponse);
    }

    private void consultationUpdateResult(ConsultationUpdateResponse consultationUpdateResponse) {
        if (consultationUpdateResponse.getUpdateType() != null) {
            switch (consultationUpdateResponse.getUpdateType()) {
                case ConsultationConstant.UPDATE_TYPE_FOLLOW_UP:
                    parseSpecializationBotQuestion();
                    getSpecializationBasedBotQuestion(followUpSpecialization);
                    query = 0;
                    queryLength = symptomQuestionList.size();
                    showPostPaymentBotQuestion();
                    sendConsultationRequestToSocket();
                    break;
                case ConsultationConstant.UPDATE_TYPE_SPECIALIZATION:
                    isEditSpecialisation = false;
                    isAnswered = true;
                    removeTypingView();
                    break;
                case ConsultationConstant.UPDATE_TYPE_CONSULTATION_MODE:
                    consultationFee();
                    break;
                case ConsultationConstant.UPDATE_TYPE_PAYMENT:
                    if (isPrime) {
                        updateConsultationPayment();
                    } else if (!isReoccur) {
                        reoccurUpdate();
                    } else {
                        updateConsultationPayment();
                    }
                    break;
            }
        }
    }


    private void updateConsultationPayment() {
        isPaymentSuccess = true;
        isLoading = false;
        botQuestionIndex += 1;
        isAnswered = true;
        removeLoader();
        sendConsultationRequestToSocket();
        disableEditOption();
        showNotification(ConsultationConstant.PAYMENT_SUCCESS_MESSAGE, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
        addNewItem(messageList);
        showTypingView();
        removeTypingView();
    }


    private void consultationUpdateFailureMessage(ConsultationUpdateResponse consultationUpdateResponse) {
        if (consultationUpdateResponse.getResult() != null && !TextUtils.isEmpty(consultationUpdateResponse.getResult().getMessage())) {
            removeLoader();
            SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, consultationUpdateResponse.getResult().getMessage());
        } else
            consultationUpdateApiError(consultationUpdateResponse);
    }

    private Message createMessage(String message, String role, boolean isShowTime, String type) {
        Message sentMessage = new Message();
        sentMessage.setMessage(message);
        sentMessage.setType(type);
        sentMessage.setFromRole(role);
        sentMessage.setTime(dateTimeUtils.currentTime(new Date(), DateTimeUtils.yyyyMMddTHHmmss));
        sentMessage.setShowTime(isShowTime);
        return sentMessage;
    }

    private void initMessageAdapter() {
        messageList.add(createMessage("", ConsultationConstant.ROLE_HEADER, false, ConsultationConstant.MESSAGE_TYPE_TEXT));
        int notificationTextColor = context.getResources().getColor(R.color.colorBrown);
        showNotification(ConsultationConstant.HEADER_NOTIFICATION, R.drawable.peach_curved_rectangle, notificationTextColor);
        layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatBinding.messageList.setLayoutManager(layoutManager);
        layoutManager.setStackFromEnd(true);
        messageAdapter = new MessageAdapter(context, messageList, this, application);
        chatBinding.messageList.setAdapter(messageAdapter);
        if (TextUtils.isEmpty(intentFrom)) return;
        messageAdapter.setIntentFrom(intentFrom);
    }


    private void getPrimeSubscription() {
        if (consultationFeeResponse != null && consultationFeeResponse.getConsultationFee() != null) {
            isReoccurConsultation = consultationFeeResponse.getConsultationFee().getReoccur();
            if (consultationFeeResponse.getConsultationFee().getSubscription() != null && consultationFeeResponse.getConsultationFee().getSubscription().getPrime() != null
                    && consultationFeeResponse.getConsultationFee().getSubscription().getPrime().isStatus()
                    && consultationFeeResponse.getConsultationFee().getSubscription().getPrime().getDiscount() != null) {
                isPrime = consultationFeeResponse.getConsultationFee().getSubscription().getPrime().isStatus();
                if (webEngageModel != null) {
                    if (webEngageModel.getMode().equals(context.getResources().getString(R.string.text_audio))) {
                        discount = consultationFeeResponse.getConsultationFee().getSubscription().getPrime().getDiscount().getGeneral_offer_value();
                    } else {
                        discount = consultationFeeResponse.getConsultationFee().getSubscription().getPrime().getDiscount().getChat_offer_value();
                    }
                }
            }
        }
    }

    private boolean isReoccurConsultation, isPrime;
    private int discount;

    private void showNotification(String message, int notificationBg, int textColor) {
        String msg;
        if (message.equals(ConsultationConstant.PAYMENT_SUCCESS_MESSAGE)) {
            getPrimeSubscription();
            if (isReoccurConsultation && isPrime) {
                if (discount == 100)
                    msg = ConsultationConstant.PRIME_FIRST_ACTIVATED;
                else
                    msg = ConsultationConstant.REACCURE_UNLIMITED_PACKAGE_ACTIVATED;
            } else if (isReoccurConsultation) {
                msg = ConsultationConstant.REACCURE_UNLIMITED_PACKAGE_ACTIVATED;
            } else if (isPrime) {
                if (discount == 100)
                    msg = ConsultationConstant.PRIME_FIRST_ACTIVATED;
                else
                    msg = ConsultationConstant.PAYMENT_SUCCESS_MESSAGE;
            } else {
                msg = ConsultationConstant.PAYMENT_SUCCESS_MESSAGE;
            }
            Message notification = createMessage(msg, ConsultationConstant.ROLE_NOTIFICATION, false, ConsultationConstant.MESSAGE_TYPE_TEXT);
            notification.setNotificationBackground(notificationBg);
            notification.setNotificationTextColor(textColor);
            messageList.add(notification);
            return;
        }
        Message notification = createMessage(message, ConsultationConstant.ROLE_NOTIFICATION, false, ConsultationConstant.MESSAGE_TYPE_TEXT);
        notification.setNotificationBackground(notificationBg);
        notification.setNotificationTextColor(textColor);
        messageList.add(notification);
    }

    private void showTypingView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
                addNewItem(messageList);
            }
        }, ConsultationConstant.TYPING_MESSAGE_DELAY_TIME);
    }

    private void addNewItem(List<Message> messageList) {
        layoutManager.setStackFromEnd(true);
        messageAdapter.notifyItemInserted(messageList.size() - 1);
        if (messageAdapter.getItemCount() > 1 && chatBinding.messageList.getLayoutManager() != null)
            chatBinding.messageList.getLayoutManager().smoothScrollToPosition(chatBinding.messageList, null, messageAdapter.getItemCount() - 1);
    }

    private void initBotQuestion() {
        parsePrePaymentBotQuestion();
        parseSpecializationBotQuestion();
        showTypingView();
        removeTypingView();
    }

    private void parsePrePaymentBotQuestion() {
        try {
            JsonReader jsonReader = JsonHelper.readJSONFromAsset(context, "botQuestion.json");
            BotQuestion question = new Gson().fromJson(jsonReader, BotQuestion.class);
            botQuestionList = new ArrayList<>();
            botQuestionList.addAll(question.getBotQuestion());
            botQuestionLength = botQuestionList.size();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeTypingView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                removeLoader();
                if (!isDoctorJoined) {
                    if (isAnswered)
                        setUpPrePaymentBotQuestion();
                } else {
                    hideView();
                    showWriteMessage();
                }
            }
        }, ConsultationConstant.BOT_QUESTION_DELAY_TIME);
    }

    private void removeLoader() {
        messageAdapter.removeItem(messageList.size() - 1);
    }

    private void setUpPrePaymentBotQuestion() {
        if (botQuestionIndex < botQuestionLength) {
            Question botQuery = botQuestionList.get(botQuestionIndex);
            messageList.add(createMessage(botQuery.getField(), botQuery.getRole(), botQuery.isContinue(), botQuery.getType()));
            addNewItem(messageList);
            if (Integer.parseInt(botQuery.getId()) != 1 && Integer.parseInt(botQuery.getId()) != 5 && Integer.parseInt(botQuery.getId()) != 8)
                sendMessageToSocket(botQuery.getField(), ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_BOT);
            botQuestionIndex = botQuery.getNextIndex() != 0 ? botQuery.getNextIndex() : botQuestionLength;
            immediateQuestion(botQuery);
        } else {
            botQuestionIndex = 0;
            botQuestionLength = 0;
            getSpecializationBasedBotQuestion(getSelectedSpecialization());
            queryLength = symptomQuestionList.size();
            if (isPaymentSuccess)
                showPostPaymentBotQuestion();
        }
    }

    private void immediateQuestion(Question botQuery) {
        if (botQuery.isContinue()) {
            showTypingView();
            removeTypingView();
        } else
            showLayout(botQuery.getId());
    }

    private void showLayout(String showLayoutId) {
        switch (Integer.parseInt(showLayoutId)) {
            case ConsultationConstant.MYSELF_WIDGET:
                postLayoutDelay(chatBinding.lytMyself);
                break;
            case ConsultationConstant.SPECIALIZATION_WIDGET:
                postLayoutDelay(chatBinding.lytSpecialization);
                break;
            case ConsultationConstant.SYMPTOMS_WIDGET:
                postLayoutDelay(chatBinding.lytSymptom);
                break;
            case ConsultationConstant.PAYMENT_SELECTION_WIDGET:
                showNotification(context.getResources().getString(R.string.payment_mode_note), R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
                addNewItem(messageList);
                postLayoutDelay(chatBinding.lytConsultationMode);
                break;
        }
        isAnswered = false;
        isLoading = false;
    }

    private void postLayoutDelay(final LinearLayout linearLayout) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startAnimation(linearLayout);
            }
        }, ConsultationConstant.TYPING_MESSAGE_DELAY_TIME);
    }

    private void startAnimation(ViewGroup view) {
        view.setVisibility(View.VISIBLE);
        view.startAnimation(viewShow);
    }

    private void sendMessageToSocket(String message, String messageType, String role) {
        if (!TextUtils.isEmpty(message)) {
            JSONObject messagePayload = null;
            try {
                messagePayload = new JSONObject();
                messagePayload.put(ConsultationConstant.PARAM_MESSAGE, message);
                messagePayload.put(ConsultationConstant.PARAM_TYPE, messageType);
                messagePayload.put(ConsultationConstant.PARAM_FROM, new JSONObject("{'" + ConsultationConstant.PARAM_ID + "' : '" + userId + "', '" + ConsultationConstant.PARAM_ROLE + "':'" + role + "'}"));
                messagePayload.put(ConsultationConstant.PARAM_TO, new JSONObject("{'" + ConsultationConstant.PARAM_ID + "' :'" + doctorId + "', '" + ConsultationConstant.PARAM_ROLE + "':'" + ConsultationConstant.ROLE_DOCTOR + "'}"));
                messagePayload.put(ConsultationConstant.PARAM_CHAT_ID, chatId);
                if (!TextUtils.isEmpty(getHashId()))
                    messagePayload.put(ConsultationConstant.PARAM_HASH_ID, getHashId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String doctorSocketId = ConsultationConstant.PARAM_DOCTOR + doctorId;
            mSocket.emit(ConsultationConstant.SOCKET_EVENT_SEND_MESSAGE, doctorSocketId, messagePayload);
        }
    }

    public void onMySelfClick() {
        messageList.add(createMessage(ConsultationConstant.MYSELF, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.MYSELF, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytMyself.setVisibility(View.GONE);
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        // webEngage Event - Patient Identified
        if (webEngageModel != null) {
            webEngageModel.setPatientIdentified(ConsultationConstant.MYSELF);
            WebEngageHelper.getInstance().patientIdentifiedEvent(webEngageModel, chatListener.getContext());
        }
    }

    public void onSomeOneClick() {
        messageList.add(createMessage(ConsultationConstant.SOMEONE_ELSE, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.SOMEONE_ELSE, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytMyself.setVisibility(View.GONE);
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        // webEngage Event - Patient Identified
        if (webEngageModel != null) {
            webEngageModel.setPatientIdentified(ConsultationConstant.SOMEONE_ELSE);
            WebEngageHelper.getInstance().patientIdentifiedEvent(webEngageModel, chatListener.getContext());
        }
    }

    private void speciality() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected)
            ConsultationServiceManager.getInstance().getAllSpecialization(chatViewModel);
        else failedTransactionId = ConsultationServiceManager.GET_ALL_SPECIALITY;
    }

    private void getAllSpecialityResponse(String data) {
        SpecialityResponse specialityResponse = new Gson().fromJson(data, SpecialityResponse.class);
        if (specialityResponse != null && specialityResponse.getServiceStatus() != null && specialityResponse.getServiceStatus().getStatusCode() != null && specialityResponse.getServiceStatus().getStatusCode() == 200)
            saveSpecialityList(specialityResponse);
        else
            vmOnError(ConsultationServiceManager.GET_ALL_SPECIALITY);
    }

    private void saveSpecialityList(SpecialityResponse specialityResponse) {
        if (specialityResponse.getSpecialityList() != null && specialityResponse.getSpecialityList().size() > 0)
            setSpeciality(specialityResponse.getSpecialityList());
    }

    public void onGeneralPhysicianClick() {
        messageList.add(specializationMessage(ConsultationConstant.GENERAL_PHYSICIAN, ConsultationConstant.GENERAL_PHYSICIAN_CODE));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.GENERAL_PHYSICIAN, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytSpecialization.setVisibility(View.GONE);
        setSelectedSpeciality(createSpecializationObject(ConsultationConstant.GENERAL_PHYSICIAN_ID, ConsultationConstant.GENERAL_PHYSICIAN_CODE, ConsultationConstant.GENERAL_PHYSICIAN));
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        // webEngage Event - Specialty Selected, GENERAL_PHYSICIAN
        if (webEngageModel != null) {
            webEngageModel.setSpeciality(ConsultationConstant.GENERAL_PHYSICIAN);
            WebEngageHelper.getInstance().specialitySelectedEvent(webEngageModel, chatListener.getContext());
        }
    }

    public void onGynecologistClick() {
        messageList.add(specializationMessage(ConsultationConstant.GYNECOLOGIST, ConsultationConstant.GYNECOLOGY_CODE));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.GYNECOLOGIST, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytSpecialization.setVisibility(View.GONE);
        setSelectedSpeciality(createSpecializationObject(ConsultationConstant.GYNECOLOGY_ID, ConsultationConstant.GYNECOLOGY_CODE, ConsultationConstant.GYNECOLOGIST));
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        // webEngage Event - Specialty Selected, GYNECOLOGIST
        if (webEngageModel != null) {
            webEngageModel.setSpeciality(ConsultationConstant.GYNECOLOGIST);
            WebEngageHelper.getInstance().specialitySelectedEvent(webEngageModel, chatListener.getContext());
        }
    }

    public void onDermatologistClick() {
        messageList.add(specializationMessage(ConsultationConstant.DERMATOLOGIST, ConsultationConstant.DERMATOLOGY_CODE));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.DERMATOLOGIST, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytSpecialization.setVisibility(View.GONE);
        setSelectedSpeciality(createSpecializationObject(ConsultationConstant.DERMATOLOGY_ID, ConsultationConstant.DERMATOLOGY_CODE, ConsultationConstant.DERMATOLOGIST));
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        // webEngage Event - Specialty Selected , DERMATOLOGIST
        if (webEngageModel != null) {
            webEngageModel.setSpeciality(ConsultationConstant.DERMATOLOGIST);
            WebEngageHelper.getInstance().specialitySelectedEvent(webEngageModel, chatListener.getContext());
        }
    }

    public void onSexologistClick() {
        messageList.add(specializationMessage(ConsultationConstant.SEXOLOGIST, ConsultationConstant.SEXOLOGY_CODE));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.SEXOLOGIST, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytSpecialization.setVisibility(View.GONE);
        setSelectedSpeciality(createSpecializationObject(ConsultationConstant.SEXOLOGY_ID, ConsultationConstant.SEXOLOGY_CODE, ConsultationConstant.SEXOLOGIST));
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        // webEngage Event - Specialty Selected,SEXOLOGIST
        if (webEngageModel != null) {
            webEngageModel.setSpeciality(ConsultationConstant.SEXOLOGIST);
            WebEngageHelper.getInstance().specialitySelectedEvent(webEngageModel, chatListener.getContext());
        }

    }

    public void onViewAllSpecializationClick() {
        if (getSpeciality() != null && getSpeciality().size() > 0)
            chatListener.vmShowSpecialization(getSpeciality(), isEditSpecialisation, this);
    }

    private Message specializationMessage(String message, String specialization) {
        Message specializationMessage = new Message();
        specializationMessage.setMessage(message);
        specializationMessage.setFromRole(ConsultationConstant.ROLE_USER);
        specializationMessage.setTime(dateTimeUtils.currentTime(new Date(), DateTimeUtils.yyyyMMddTHHmmss));
        specializationMessage.setEditable(true);
        specializationMessage.setEditFrom(ConsultationConstant.UPDATE_TYPE_SPECIALIZATION);
        specializationMessage.setSpecialization(specialization);
        return specializationMessage;
    }

    private Speciality createSpecializationObject(String id, String code, String name) {
        Speciality speciality = new Speciality();
        speciality.setId(id);
        speciality.setCode(code);
        speciality.setName(name);
        speciality.setChecked(true);
        return speciality;
    }

    private String getSelectedSpecialization() {
        return getSelectedSpeciality() != null && !TextUtils.isEmpty(getSelectedSpeciality().getCode()) ? getSelectedSpeciality().getCode() : "";
    }

    private void updateSpecialization() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
            addNewItem(messageList);
            isLoading = true;
            ConsultationUpdateRequest updateSpecializationRequest = new ConsultationUpdateRequest();
            updateSpecializationRequest.setUserId(userId);
            updateSpecializationRequest.setType(ConsultationConstant.UPDATE_TYPE_SPECIALIZATION);
            ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
            updateConsultingPayload.setId(consultationId);
            updateConsultingPayload.setSpeciality(getSelectedSpecialization());
            updateSpecializationRequest.setConsultingPayload(updateConsultingPayload);
            ConsultationServiceManager.getInstance().updateConsultation(chatViewModel, basePreference, updateSpecializationRequest, "", ConsultationConstant.UPDATE_TYPE_SPECIALIZATION, ConsultationServiceManager.SPECIALIZATION_UPDATE);
        } else
            failedTransactionId = ConsultationServiceManager.SPECIALIZATION_UPDATE;
    }

    public void onYes() {
        chatBinding.lytSymptom.setVisibility(View.GONE);
        chatBinding.imgAttachment.setVisibility(View.GONE);
        sendMessageFrom = ConsultationConstant.SYMPTOM;
        startAnimation(chatBinding.lytWriteMessage);
        chatBinding.lytWriteMessage.requestFocus();
        showForcedKeyboard();
    }

    public void onNo() {
        messageList.add(createMessage(ConsultationConstant.SKIP, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.SKIP, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytSymptom.setVisibility(View.GONE);
        isAnswered = true;
        isLoading = true;
        showTypingView();
        removeTypingView();
        doctorList();
    }

    public void onSendMessageClick() {
        if (chatBinding.editTextMessage.getText() != null && !TextUtils.isEmpty(chatBinding.editTextMessage.getText().toString())) {
            String message = chatBinding.editTextMessage.getText().toString();
            switch (sendMessageFrom) {
                case ConsultationConstant.OTHER:
                    sendMessage(selectedMultiOption + "," + message);
                    CommonUtils.hideKeyboard(context, chatBinding.lytWriteMessage);
                    selectedMultiOption = "";
                    sendMessageFrom = "";
                    checkSpecializationQuestionShowOrNot();
                    break;
                case ConsultationConstant.SYMPTOM:
                    sendMessage(message);
                    updateConsultationSymptom(message);
                    CommonUtils.hideKeyboard(context, chatBinding.lytWriteMessage);
                    chatBinding.lytWriteMessage.setVisibility(View.GONE);
                    sendMessageFrom = "";
                    isAnswered = true;
                    isLoading = true;
                    showTypingView();
                    removeTypingView();
                    doctorList();
                    // WebEngage Event - Symptom Entered
                    if (!TextUtils.isEmpty(message) && webEngageModel != null) {
                        webEngageModel.setSymptoms(message);
                        WebEngageHelper.getInstance().SymptomEnteredEvent(webEngageModel, chatListener.getContext());
                    }
                    break;
                case ConsultationConstant.POST_PAYMENT_QUESTION__TEXT_TYPE:
                    sendMessage(message);
                    CommonUtils.hideKeyboard(context, chatBinding.lytWriteMessage);
                    sendMessageFrom = "";
                    chatBinding.lytWriteMessage.setVisibility(View.GONE);
                    checkSpecializationQuestionShowOrNot();
                    break;
                default:
                    sendMessage(message);
            }
        }
    }

    private void updateConsultationSymptom(String message) {
        ConsultationUpdateRequest updateSymptomRequest = new ConsultationUpdateRequest();
        updateSymptomRequest.setUserId(userId);
        ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
        updateConsultingPayload.setId(chatId);
        updateConsultingPayload.setSymptoms(message);
        updateSymptomRequest.setConsultingPayload(updateConsultingPayload);
        ConsultationServiceManager.getInstance().updateConsultation(chatViewModel, basePreference, updateSymptomRequest, "", ConsultationConstant.UPDATE_TYPE_SYMPTOMS, ConsultationServiceManager.SYMPTOM_UPDATE);
    }

    private void checkSpecializationQuestionShowOrNot() {
        if (!isSpecializationQuestionEnd) {
            isAnswered = true;
            showPostPaymentBotQuestion();
        }
    }

    private void sendMessage(String message) {
        //Message send to socket
        sendMessageToSocket(message, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);

        Message sentMessage = new Message();
        sentMessage.setMessage(message);
        sentMessage.setType(ConsultationConstant.MESSAGE_TYPE_TEXT);
        sentMessage.setFromRole(ConsultationConstant.ROLE_USER);
        sentMessage.setTime(dateTimeUtils.currentTime(new Date(), DateTimeUtils.yyyyMMddTHHmmss));
        messageList.add(sentMessage);
        addNewItem(messageList);
        chatBinding.editTextMessage.setText("");
    }

    private void showWriteMessage() {
        chatBinding.imgAttachment.setImageResource(R.drawable.ic_plus_accent);
        chatBinding.imgAttachment.setVisibility(View.VISIBLE);
        startAnimation(chatBinding.lytWriteMessage);
        sendMessageFrom = "";
    }

    private void showPostPaymentBotQuestion() {
        if (query < queryLength) {
            isLoading = true;
            showTypingView();
            setUpSpecialisationBotQuestion();
        } else {
            showNotification(ConsultationConstant.SENT_CASE_NOTIFICATION, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
            showNotification(ConsultationConstant.GET_WELL_SOON_NOTIFICATION, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
            addNewItem(messageList);
            isSpecializationQuestionEnd = true;
            query = 0;
            queryLength = 0;
            chatBinding.closeAttachment.setVisibility(View.GONE);
            showWriteMessage();
        }
    }

    private void doctorList() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
                    addNewItem(messageList);
                    isLoading = true;
                    DoctorListRequest doctorListRequest = new DoctorListRequest();
                    doctorListRequest.setSpecialization(getSelectedSpecialization());
                    doctorListRequest.setActive(true);
                    ConsultationServiceManager.getInstance().getDoctorList(chatViewModel, doctorListRequest);
                }
            }, ConsultationConstant.BOT_QUESTION_DELAY_TIME);
        } else
            failedTransactionId = ConsultationServiceManager.DOCTOR_LIST;
    }

    public void onDoctorListDataAvailable(DoctorListResponse doctorListResponse) {
        removeLoader();
        if (doctorListResponse != null)
            doctorListStatusCheck(doctorListResponse);
        else
            vmOnError(ConsultationServiceManager.DOCTOR_LIST);
    }

    private void doctorListStatusCheck(DoctorListResponse doctorListResponse) {
        if (doctorListResponse.getServiceStatus() != null && doctorListResponse.getServiceStatus().getStatusCode() != null && doctorListResponse.getServiceStatus().getStatusCode() == 200)
            showDoctorList(doctorListResponse);
        else
            vmOnError(ConsultationServiceManager.DOCTOR_LIST);
    }

    private void showDoctorList(DoctorListResponse doctorListResponse) {
        if (doctorListResponse.getDoctorList() != null && doctorListResponse.getDoctorList().size() > 0) {
            List<DoctorInformation> doctorList = new ArrayList<>();
            for (DoctorInformation doctorInformation : doctorListResponse.getDoctorList()) {
                if (doctorList.size() != 3)
                    doctorList.add(doctorInformation);
            }
            if (doctorList.size() > 0) {
                Message sentMessage = new Message();
                sentMessage.setMessage("");
                sentMessage.setFromRole(ConsultationConstant.ROLE_LIST);
                sentMessage.setDoctorList(doctorList);
                messageList.add(sentMessage);
                messageAdapter.notifyItemInserted(messageList.size() - 1);
            }
        }

        isAnswered = true;
        showTypingView();
        removeTypingView();
    }

    private void getDoctorInfo(int doctorId) {
        viewDoctorInfoId = doctorId;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            DoctorInfoRequest doctorInfoRequest = new DoctorInfoRequest();
            List<Integer> doctorIdList = new ArrayList<>();
            doctorIdList.add(viewDoctorInfoId);
            doctorInfoRequest.setDoctorId(doctorIdList);
            chatListener.vmShowProgress();
            ConsultationServiceManager.getInstance().getDoctorInfo(chatViewModel, doctorInfoRequest);
        } else {
            failedTransactionId = ConsultationServiceManager.DOCTOR_INFO;
        }
    }

    public void onDoctorInfoDataAvailable(DoctorListResponse doctorInfoResponse) {
        chatListener.vmDismissProgress();
        if (doctorInfoResponse != null && doctorInfoResponse.getServiceStatus() != null && doctorInfoResponse.getServiceStatus().getStatusCode() != null && doctorInfoResponse.getServiceStatus().getStatusCode() == 200) {
            if (doctorInfoResponse.getDoctorList() != null && doctorInfoResponse.getDoctorList().size() > 0) {
                chatListener.vmShowDoctorInfo(doctorInfoResponse.getDoctorList().get(0), this);
            }
        } else {
            if (doctorInfoResponse != null && doctorInfoResponse.getServiceStatus() != null && !TextUtils.isEmpty(doctorInfoResponse.getServiceStatus().getMessage()))
                SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, doctorInfoResponse.getServiceStatus().getMessage());
        }
    }

    public void onChatModeClick() {
        isAudioMode = false;
        isChatMode = true;
        Message message = createMessage(ConsultationConstant.CHAT_CONSULTATION, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT);
        message.setEditable(false);
        message.setEditFrom(ConsultationConstant.UPDATE_TYPE_CONSULTATION_MODE);
        messageList.add(message);
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.CHAT_CONSULTATION, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytConsultationMode.setVisibility(View.GONE);
        isLoading = true;
        if (TextUtils.isEmpty(consultationId))
            createConsultation();
        else
            consultationModeUpdate();
        //WebEngage Event - Consultation Mode - Chat
        if (webEngageModel != null) {
            webEngageModel.setMode(ConsultationConstant.CHAT_CONSULTATION);
            WebEngageHelper.getInstance().modeSelectedEvent(webEngageModel, chatListener.getContext());
        }
    }

    public void onAudioModeClick() {
        isAudioMode = true;
        isChatMode = false;
        Message message = createMessage(ConsultationConstant.AUDIO_CALL, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT);
        message.setEditable(false);
        message.setEditFrom(ConsultationConstant.UPDATE_TYPE_CONSULTATION_MODE);
        messageList.add(message);
        addNewItem(messageList);
        sendMessageToSocket(ConsultationConstant.AUDIO_CALL, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        chatBinding.lytConsultationMode.setVisibility(View.GONE);
        isLoading = true;
        if (TextUtils.isEmpty(consultationId))
            createConsultation();
        else
            consultationModeUpdate();
        //WebEngage Event - Consultation Mode - Audio
        if (webEngageModel != null) {
            webEngageModel.setMode(ConsultationConstant.AUDIO_CALL);
            WebEngageHelper.getInstance().modeSelectedEvent(webEngageModel, chatListener.getContext());
        }
    }

    private void createConsultation() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
                    addNewItem(messageList);
                    ConsultationServiceManager.getInstance().createConsultation(chatViewModel, createConsultationRequest(), basePreference);
                }
            }, ConsultationConstant.BOT_QUESTION_DELAY_TIME);
        } else
            failedTransactionId = ConsultationServiceManager.CREATE_CONSULTATION;
    }

    private CreateConsultationRequest createConsultationRequest() {
        CreateConsultationRequest createConsultationRequest = new CreateConsultationRequest();
        createConsultationRequest.setUserId(Integer.valueOf(userId));
        createConsultationRequest.setId(chatId);
        createConsultationRequest.setSpeciality(getSelectedSpecialization());
        createConsultationRequest.setType(ConsultationConstant.PARAM__CONSULTATION_CREATE_TYPE);
        Payment payment = new Payment();
        payment.setStatus(ConsultationConstant.PARAM_PENDING);
        createConsultationRequest.setPayment(payment);
        createConsultationRequest.setStatus(ConsultationConstant.PARAM_PENDING);
        createConsultationRequest.setReferrer("");
        String symptom = "";
        createConsultationRequest.setSymptoms(symptom);
        createConsultationRequest.setChosenSymptoms("");
        createConsultationRequest.setInterPatient(false);
        createConsultationRequest.setReoccur(false);
        createConsultationRequest.setMode(getConsultationMode());
        return createConsultationRequest;
    }

    public void createConsultationOnDataAvailable(CreateConsultationResponse createConsultationResponse) {
        if (createConsultationResponse != null && createConsultationResponse.getServiceStatus() != null && createConsultationResponse.getServiceStatus().getStatusCode() != null && createConsultationResponse.getServiceStatus().getStatusCode() == 200)
            createConsultationResult(createConsultationResponse);
        else
            createConsultationFailure(createConsultationResponse);
    }

    private void createConsultationResult(CreateConsultationResponse createConsultationResponse) {
        if (createConsultationResponse.getResult() != null) {
            CreateConsultationResult consultationResult = createConsultationResponse.getResult();
            checkConsultationId(consultationResult);
        } else
            vmOnError(ConsultationServiceManager.CREATE_CONSULTATION);
    }

    private void checkConsultationId(CreateConsultationResult consultationResult) {
        if (consultationResult.getConsultationId() != null && !TextUtils.isEmpty(consultationResult.getConsultationId())) {
            consultationId = consultationResult.getConsultationId();
            if (isEditSpecialisation) {
                isEditSpecialisation = false;
                isAnswered = true;
                removeTypingView();
            } else
                consultationFee();
        } else
            vmOnError(ConsultationServiceManager.CREATE_CONSULTATION);
    }

    private void createConsultationFailure(CreateConsultationResponse createConsultationResponse) {
        removeLoader();
        isLoading = false;
        if (createConsultationResponse != null && createConsultationResponse.getResult() != null && !TextUtils.isEmpty(createConsultationResponse.getResult().getMessage()))
            SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, createConsultationResponse.getResult().getMessage());
        else
            vmOnError(ConsultationServiceManager.CREATE_CONSULTATION);
    }

    private void consultationModeUpdate() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
            addNewItem(messageList);
            ConsultationUpdateRequest consultationModeUpdateRequest = new ConsultationUpdateRequest();
            consultationModeUpdateRequest.setUserId(userId);
            ConsultationUpdatePayload consultationModeUpdatePayload = new ConsultationUpdatePayload();
            consultationModeUpdatePayload.setId(consultationId);
            consultationModeUpdatePayload.setSpeciality(getSelectedSpecialization());
            consultationModeUpdatePayload.setMode(getConsultationMode());
            consultationModeUpdateRequest.setConsultingPayload(consultationModeUpdatePayload);
            ConsultationServiceManager.getInstance().updateConsultation(chatViewModel, basePreference, consultationModeUpdateRequest, "", ConsultationConstant.UPDATE_TYPE_CONSULTATION_MODE, ConsultationServiceManager.CONSULTATION_MODE_UPDATE);
        } else
            failedTransactionId = ConsultationServiceManager.CONSULTATION_MODE_UPDATE;
    }

    private String getConsultationMode() {
        return isChatMode ? ConsultationConstant.PARAM_CHAT : isAudioMode ? ConsultationConstant.PARAM_AUDIO : "";
    }

    private void consultationFee() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            isLoading = true;
            ConsultationFeeRequest consultationFeeRequest = new ConsultationFeeRequest();
            consultationFeeRequest.setSpeciality(getSelectedSpecialization());
            consultationFeeRequest.setUserId(userId);
            ConsultationServiceManager.getInstance().consultationFee(chatViewModel, consultationFeeRequest);
        } else
            failedTransactionId = ConsultationServiceManager.CONSULTATION_FEE;

    }

    public void consultationFeeDataAvailable(ConsultationFeeResponse consultationFeeResponse) {
        if (consultationFeeResponse != null && consultationFeeResponse.getServiceStatus() != null && consultationFeeResponse.getServiceStatus().getStatusCode() != null && consultationFeeResponse.getServiceStatus().getStatusCode() == 200)
            consultationFeeResult(consultationFeeResponse);
        else
            consultationFeeFailure(consultationFeeResponse);
    }

    private void consultationFeeResult(ConsultationFeeResponse consultationFeeResponse) {
        if (consultationFeeResponse.getConsultationFee() != null) {
            consultationReoccurCheck(consultationFeeResponse);
        } else {
            removeLoader();
            vmOnError(ConsultationServiceManager.CONSULTATION_FEE);
        }
    }

    private void consultationReoccurCheck(ConsultationFeeResponse consultationFeeResponse) {
        getPrimeSubscription();
        isReoccur = consultationFeeResponse.getConsultationFee().getReoccur();
        if (isPrime || isReoccur) {
            orderId = getOrderId();
            removeLoader();
            paymentCreation();
        } else {
            isPaymentSuccess = false;
            consultationFeeDetail = new ConsultationFeeDetail();
            consultationFeeDetail.setCouponUpdate(false);
            if (isAudioMode) {
                consultationFeeDetail.setUnlimitedStrikeOutPrice(consultationFeeResponse.getConsultationFee().getGeneral() != null ? consultationFeeResponse.getConsultationFee().getGeneral() : "");
                consultationFeeDetail.setOneTimeStrikeOutPrice(consultationFeeResponse.getConsultationFee().getGeneral() != null ? consultationFeeResponse.getConsultationFee().getGeneral() : "");
            } else {
                consultationFeeDetail.setUnlimitedStrikeOutPrice(consultationFeeResponse.getConsultationFee().getChat() != null ? consultationFeeResponse.getConsultationFee().getChat() : "");
                consultationFeeDetail.setOneTimeStrikeOutPrice(consultationFeeResponse.getConsultationFee().getChat() != null ? consultationFeeResponse.getConsultationFee().getChat() : "");
            }
            consultationFeeDetail.setUnlimitedPercentageValue(ConsultationConstant.UNLIMITED_PERCENTAGE);
            consultationFeeDetail.setOneTimePercentageValue("");
            isAnswered = true;
            removeTypingView();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    allCoupon();
                }
            }, ConsultationConstant.BOT_QUESTION_DELAY_TIME);
        }
    }

    private void consultationFeeFailure(ConsultationFeeResponse consultationFeeResponse) {
        removeLoader();
        isLoading = false;
        if (consultationFeeResponse != null && consultationFeeResponse.getConsultationFee() != null && !TextUtils.isEmpty(consultationFeeResponse.getConsultationFee().getMessage()))
            SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, consultationFeeResponse.getConsultationFee().getMessage());
        else
            vmOnError(ConsultationServiceManager.CONSULTATION_FEE);
    }

    private void sendConsultationRequestToSocket() {
        Object[] consultationRequestObject = new Object[2];
        consultationRequestObject[0] = consultationId;
        consultationRequestObject[1] = doctorId;
        mSocket.emit(ConsultationConstant.SOCKET_EVENT_CONSULTATION_REQUEST, consultationRequestObject);
    }

    private void allCoupon() {
        messageList.add(createMessage("", ConsultationConstant.ROLE_TYPING, false, ConsultationConstant.MESSAGE_TYPE_TYPING));
        addNewItem(messageList);
        Map<String, String> header = new HashMap<>();
        header.put(ConsultationConstant.KEY_SPECS, getSelectedSpecialization());
        ConsultationServiceManager.getInstance().getAllCoupon(chatViewModel, header);
    }

    public void onGetAllCouponDataAvailable(GetAllCouponResponse allCouponResponse) {
        if (allCouponResponse != null && allCouponResponse.getServiceStatus() != null && allCouponResponse.getServiceStatus().getStatusCode() != null && allCouponResponse.getServiceStatus().getStatusCode() == 200) {
            allCouponList(allCouponResponse);
        } else {
            removeLoader();
            isLoading = false;
            showPaymentOption();
        }
    }

    private void allCouponList(GetAllCouponResponse allCouponResponse) {
        consultationCouponList = new ArrayList<>();
        if (allCouponResponse.getCouponList() != null && allCouponResponse.getCouponList().size() > 0) {
            for (ConsultationCoupon consultationCoupon : allCouponResponse.getCouponList()) {
                consultationCoupon.setApplied(false);
                consultationCouponList.add(consultationCoupon);
            }
            ConsultationCoupon coupon = consultationCouponList.size() > 1 ? getMaximumDiscountCoupon(consultationCouponList) : consultationCouponList.get(0);
            checkCouponExpiry(coupon);
        } else {
            removeLoader();
            showPaymentOption();
        }
    }

    private void checkCouponExpiry(ConsultationCoupon coupon) {
        if (coupon != null && !TextUtils.isEmpty(coupon.getCode()) && !DateTimeUtils.getInstance().isCouponExpired(coupon.getExpiry(), DateTimeUtils.yyyyMMddTHHmmss))
            applyCoupon(coupon);
        else {
            removeLoader();
            showPaymentOption();
        }
    }

    private ConsultationCoupon getMaximumDiscountCoupon(List<ConsultationCoupon> couponList) {
        ConsultationCoupon maxCoupon = couponList.get(0);
        for (ConsultationCoupon currentIndex : couponList) {
            if (Integer.parseInt(currentIndex.getValue()) > Integer.parseInt(maxCoupon.getValue()) && !DateTimeUtils.getInstance().isCouponExpired(currentIndex.getExpiry(), DateTimeUtils.yyyyMMddTHHmmss))
                maxCoupon = currentIndex;
        }
        return maxCoupon;
    }

    private void showPaymentOption() {
        isLoading = false;
        Message message = new Message();
        message.setMessage("");
        message.setFromRole(ConsultationConstant.ROLE_PAYMENT_PLAN);
        message.setFeeDetail(consultationFeeDetail);
        message.setSpecialization(getSelectedSpeciality() != null && !TextUtils.isEmpty(getSelectedSpeciality().getName()) ? getSelectedSpeciality().getName() : "");
        message.setChatType(getConsultationMode());
        message.setEditable(true);
        messageList.add(message);
        addNewItem(messageList);
        isAnswered = true;
        chatBinding.cartViewChatBottom.setBackgroundColor(Color.TRANSPARENT);
        chatBinding.btnMakePayment.setVisibility(View.VISIBLE);

        messageList.get(messageList.size() - 3).setEditable(true);
        messageAdapter.notifyDataSetChanged();
    }

    private void applyCoupon(final ConsultationCoupon couponCode) {
        retryCouponCode = couponCode;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            String type = isChatMode ? ConsultationConstant.PARAM_CHAT : isAudioMode ? ConsultationConstant.PARAM_GENERAL : "";
            ApplyCouponRequest applyCouponRequest = new ApplyCouponRequest();
            applyCouponRequest.setCouponCode(couponCode.getCode());
            applyCouponRequest.setType(type);
            applyCouponRequest.setSpecs(getSelectedSpecialization());
            ConsultationServiceManager.getInstance().applyCoupon(chatViewModel, applyCouponRequest);
        } else
            failedTransactionId = ConsultationServiceManager.APPLY_COUPON;
    }

    public void onApplyCouponDataAvailable(ApplyCouponResponse applyCouponResponse) {
        if (applyCouponResponse != null && applyCouponResponse.getServiceStatus() != null && applyCouponResponse.getServiceStatus().getStatusCode() != null && applyCouponResponse.getServiceStatus().getStatusCode() == 200)
            applyCouponResult(applyCouponResponse);
        else
            applyCouponFailure();
    }

    private void applyCouponResult(ApplyCouponResponse applyCouponResponse) {
        if (applyCouponResponse.getApplyCoupon() != null) {
            setOneTimeAppliedCoupon(applyCouponResponse.getApplyCoupon());
            checkChangedCoupon();
        } else {
            removeLoader();
            showPaymentOption();
        }
    }

    private void checkChangedCoupon() {
        if (isCouponChanged) {
            updatePaymentPlan();
            isCouponChanged = false;
        } else {
            setAppliedCouponDetails();
            removeLoader();
            showPaymentOption();
        }
    }

    private void setAppliedCouponDetails() {
        if (getOneTimeAppliedCoupon() != null && !TextUtils.isEmpty(getOneTimeAppliedCoupon().getCode())) {
            consultationFeeDetail.setOneTimeActualPrice(getOneTimeAppliedCoupon().getActualPrice());
            consultationFeeDetail.setOneTimeStrikeOutPrice(getOneTimeAppliedCoupon().getStrikeOutPrice());
            consultationFeeDetail.setOneTimePercentageValue(getOneTimeAppliedCoupon().getValue());
        }
    }

    private void applyCouponFailure() {
        removeLoader();
        isLoading = false;
        isCouponChanged = false;
        showPaymentOption();
    }

    private void updatePaymentPlan() {
        if (getOneTimeAppliedCoupon() != null && !TextUtils.isEmpty(getOneTimeAppliedCoupon().getCode())) {
            Message couponUpdate = messageList.get(messageList.size() - 1);
            ConsultationFeeDetail consultationFeeDetail = couponUpdate.getFeeDetail();
            consultationFeeDetail.setOneTimeActualPrice(getOneTimeAppliedCoupon().getActualPrice());
            consultationFeeDetail.setOneTimeStrikeOutPrice(getOneTimeAppliedCoupon().getStrikeOutPrice());
            consultationFeeDetail.setOneTimePercentageValue(getOneTimeAppliedCoupon().getValue());
            consultationFeeDetail.setCouponUpdate(true);
            couponUpdate.setFeeDetail(consultationFeeDetail);
            messageAdapter.notifyDataSetChanged();
        }
    }


    // Web Engage Event - - Consultation Type Selected
    private void fireConsultationTypeEvent() {
        if (webEngageModel == null || TextUtils.isEmpty(selectedPaymentPlan)) return;
        switch (selectedPaymentPlan) {
            case ConsultationConstant.UNLIMITED_PLAN: {
                webEngageModel.setType(context.getResources().getString(R.string.txt_web_engage_consultation_type_unlimited));
                WebEngageHelper.getInstance().consultationTypeSelectedEvent(webEngageModel, chatListener.getContext());
                break;
            }
            case ConsultationConstant.ONETIME_PLAN: {
                webEngageModel.setType(context.getResources().getString(R.string.txt_web_engage_consultation_type_one_time));
                WebEngageHelper.getInstance().consultationTypeSelectedEvent(webEngageModel, chatListener.getContext());
                break;
            }
        }
    }

    public void onMakePayment() {
        fireConsultationTypeEvent();
        chatBinding.btnMakePayment.setVisibility(View.GONE);
        Bundle bundle = new Bundle();
        bundle.putDouble(ConsultationConstant.KEY_TOTAL_AMOUNT, totalAmount);
        bundle.putString(ConsultationConstant.KEY_CONSULTATION_ID, consultationId);
        bundle.putString(ConsultationConstant.KEY_SPECIALIZATION, getSelectedSpecialization());
        bundle.putBoolean(ConsultationConstant.KEY_SELECTED_PLAN, selectedPaymentPlan.equals(ConsultationConstant.UNLIMITED_PLAN));
        bundle.putString(ConsultationConstant.KEY_USER_ID, userId);
        if (getAppliedCoupon() != null && !TextUtils.isEmpty(getAppliedCoupon().getCode()))
            bundle.putString(ConsultationConstant.KEY_APPLIED_COUPON, new Gson().toJson(getAppliedCoupon()));
        bundle.putString(ConsultationConstant.KEY_COUPON_TYPE, isChatMode ? ConsultationConstant.PARAM_CHAT : isAudioMode ? ConsultationConstant.PARAM_GENERAL : "");
        bundle.putBoolean(ConsultationConstant.IS_FROM_PAYMENT, false);
        if (webEngageModel != null) {
            bundle.putSerializable(ConsultationConstant.CONSULTATION_WEB_ENGAGE_MODEL, webEngageModel);
        }
        chatListener.vmCallBackPayment(bundle);
    }

    private void disableEditOption() {
        for (Message message : messageList)
            message.setEditable(false);
        messageAdapter.notifyDataSetChanged();
    }

    public void onProblemCategory() {
        chatListener.vmShowProblemCategory(botSymptomQuestion.getOptionsList(), this);
    }

    public void onDatePicker() {
        chatListener.vmShowDatePicker();
    }

    public void onDateAvailable(int year, int month, int day) {
        chatBinding.lytDate.setVisibility(View.GONE);
        messageList.add(createMessage(String.valueOf(day) + "/" + (month + 1) + "/" + year, ConsultationConstant.ROLE_USER, true, ConsultationConstant.MESSAGE_TYPE_TEXT));
        addNewItem(messageList);
        sendMessageToSocket(String.valueOf(day) + "/" + (month + 1) + "/" + year, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
        isAnswered = true;
        showPostPaymentBotQuestion();
    }

    private void onAttachmentClose() {
        if (!isSpecializationQuestionEnd) {
            chatBinding.lytAttachment.setVisibility(View.GONE);
            isAnswered = true;
            showPostPaymentBotQuestion();
        }
    }

    public void setCapturedImage(int resultCode) {
        validateImage(chatListener.getUriFromCameraImage(resultCode));
    }

    public void setGalleryImage(int resultCode, Intent data) {
        validateImage(chatListener.getUriFromGalleryImage(resultCode, data));
    }

    public void setDocument(Intent data) {
        Uri uri;
        if (data != null) {
            uri = data.getData();
            validateDocument(uri);
        }
    }

    private void validateImage(Uri uri) {
        if (uri != null) {
            path = FileUtils.getPath(getApplication(), uri);
            imageValidation();
        }
    }

    private void imageValidation() {
        if (chatListener.validateUploadedImage(path, getApplication(), chatBinding.lytChatViewContent, FileUtils.MAX_DOCUMENT_SIZE_IN_BYTES, context.getResources().getString(com.nms.netmeds.base.R.string.text_max_file_size))) {
            validatePath();
            showUploadAttachment(true);
        }
    }

    private void validatePath() {
        setMultipartBuilder();
    }

    private void setMultipartBuilder() {
        if (!TextUtils.isEmpty(consultationId) && !TextUtils.isEmpty(path)) {
            MultipartBody requestBody = multipartBuilder(getFileFromPath(path)).build();
            uploadFile(requestBody, path);
        }
    }

    private void validateDocument(Uri uri) {
        if (uri != null) {
            path = FileUtils.getPath(context, uri);
            documentValidation();
        }
    }

    private void documentValidation() {
        if (chatListener.validateUploadedDocument(path, getApplication(), chatBinding.lytChatViewContent, FileUtils.MAX_DOCUMENT_SIZE_IN_BYTES)) {
            validatePath();
            showUploadAttachment(false);
        }
    }

    private void showUploadAttachment(boolean isFromImage) {
        Message message = new Message();
        message.setType(ConsultationConstant.MESSAGE_TYPE_FILE);
        message.setFromRole(ConsultationConstant.ROLE_USER);
        message.setTime(dateTimeUtils.currentTime(new Date(), DateTimeUtils.yyyyMMddTHHmmss));
        message.setMessage(isFromImage ? "" : getFileFromPath(path).getName());
        message.setImageUri(path);
        messageList.add(message);
        addNewItem(messageList);
        isAnswered = true;
        if (!intentFrom.equalsIgnoreCase(ConsultationConstant.CHAT_HISTORY) && !isSpecializationQuestionEnd) {
            chatBinding.lytAttachment.setVisibility(View.GONE);
            showPostPaymentBotQuestion();
        }
    }

    private void uploadFile(RequestBody requestBody, String path) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            ConsultationServiceManager.getInstance().uploadAttachment(chatViewModel, requestBody, path, basePreference);
        } else
            failedTransactionId = ConsultationServiceManager.UPLOAD_ATTACHMENT;
    }

    private MultipartBody.Builder multipartBuilder(File file) {
        setHashId(FileUtils.fileToMD5(!TextUtils.isEmpty(file.getPath()) ? file.getPath() : ""));
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(BaseUploadPrescription.USER_ID, userId)
                .addFormDataPart(BaseUploadPrescription.MD5SUM, getHashId())
                .addFormDataPart(BaseUploadPrescription.FILE, file.getName(), RequestBody.create(MediaType.parse(FileUtils.MULTI_PART_TYPE), file))
                .addFormDataPart(BaseUploadPrescription.CID, consultationId);
    }

    public void onUploadAttachmentDataAvailable(UploadAttachmentResponse uploadAttachmentResponse) {
        if (uploadAttachmentResponse != null && !TextUtils.isEmpty(uploadAttachmentResponse.getMessage()) && uploadAttachmentResponse.getMessage().equals(ConsultationConstant.SUCCESS))
            uploadAttachmentSendToSocket(uploadAttachmentResponse);
        else {
            setMultipartBuilder();
        }
    }

    private void uploadAttachmentSendToSocket(UploadAttachmentResponse uploadAttachmentResponse) {
        String path = !TextUtils.isEmpty(uploadAttachmentResponse.getUploadUrl()) ? uploadAttachmentResponse.getUploadUrl() : "";
        if (!TextUtils.isEmpty(path))
            sendMessageToSocket(getFileFromPath(path).getName(), ConsultationConstant.MESSAGE_TYPE_FILE, ConsultationConstant.ROLE_USER);

        setHashId("");
    }

    private File getFileFromPath(String path) {
        return new File(path);
    }

    private void checkLayoutVisibility() {
        if (chatBinding.lytMyself.getVisibility() == View.VISIBLE
                || chatBinding.lytRadioGroup.getVisibility() == View.VISIBLE
                || chatBinding.lytSpecialization.getVisibility() == View.VISIBLE
                || chatBinding.lytConsultationMode.getVisibility() == View.VISIBLE
                || chatBinding.lytSymptom.getVisibility() == View.VISIBLE
                || chatBinding.btnMakePayment.getVisibility() == View.VISIBLE
                || chatBinding.lytMultiChoice.getVisibility() == View.VISIBLE
                || chatBinding.lytProblemCategory.getVisibility() == View.VISIBLE
                || chatBinding.lytDate.getVisibility() == View.VISIBLE
                || chatBinding.lytAttachment.getVisibility() == View.VISIBLE) {
            hideView();
            sendMessageFrom = "";
            startAnimation(chatBinding.lytWriteMessage);
        }
    }

    private void updateCoupon() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            if (getAppliedCoupon() != null && getAppliedCoupon().getCode() != null) {
                ConsultationUpdateRequest updateCouponRequest = new ConsultationUpdateRequest();
                updateCouponRequest.setType(ConsultationConstant.UPDATE_TYPE_COUPON);
                updateCouponRequest.setUserId(userId);
                ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
                updateConsultingPayload.setId(consultationId);
                updateConsultingPayload.setCouponCode(getAppliedCoupon().getCode());
                updateCouponRequest.setConsultingPayload(updateConsultingPayload);
                ConsultationServiceManager.getInstance().updateConsultation(chatViewModel, basePreference, updateCouponRequest, "", ConsultationConstant.UPDATE_TYPE_COUPON, ConsultationServiceManager.UPDATE_COUPON);
            }
        } else
            failedTransactionId = ConsultationServiceManager.UPDATE_COUPON;
    }

    private void paymentCreation() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            showTypingView();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ConsultationServiceManager.getInstance().paymentCreation(chatViewModel, getPaymentCreationRequest(), basePreference, "");
                }
            }, ConsultationConstant.TYPING_MESSAGE_DELAY_TIME);
        } else
            failedTransactionId = ConsultationServiceManager.PAYMENT_CREATION;
    }

    private PaymentCreationRequest getPaymentCreationRequest() {
        String dateTime = DateTimeUtils.getInstance().convertMillisecondToDateTimeFormat(orderCreatedTime, DateTimeUtils.yyyyMMddTHHmmss);
        PaymentCreationRequest paymentCreationRequest = new PaymentCreationRequest();
        paymentCreationRequest.setAmount("0");
        paymentCreationRequest.setId(orderId);
        paymentCreationRequest.setPaymentGateway("NA");
        paymentCreationRequest.setTime(dateTime);
        paymentCreationRequest.setStatus(ConsultationConstant.SUCCESS);
        if (webEngageModel != null && !TextUtils.isEmpty(webEngageModel.getType())) {
            paymentCreationRequest.setConsulation_payment_type(webEngageModel.getType());
        }
        if (!TextUtils.isEmpty(consultationId))
            paymentCreationRequest.setConsulation_id(consultationId);
        return paymentCreationRequest;
    }

    public void paymentCreationDataAvailable(PaymentCreationResponse paymentCreationResponse) {
        if (paymentCreationResponse != null && paymentCreationResponse.getServiceStatus() != null && paymentCreationResponse.getServiceStatus().getStatusCode() != null && paymentCreationResponse.getServiceStatus().getStatusCode() == 200) {
            checkPaymentCreationResult(paymentCreationResponse);
        } else {
            removeLoader();
            vmOnError(ConsultationServiceManager.PAYMENT_CREATION);
        }
    }

    private void checkPaymentCreationResult(PaymentCreationResponse paymentCreationResponse) {
        if (paymentCreationResponse.getResult() != null && !TextUtils.isEmpty(paymentCreationResponse.getResult().getMessage()) && paymentCreationResponse.getResult().getMessage().equals(ConsultationConstant.SUCCESS)) {
            paymentUpdate();
        } else
            removeLoader();
    }

    private void paymentUpdate() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            ConsultationServiceManager.getInstance().updateConsultation(chatViewModel, basePreference, paymentUpdateRequest(), "", ConsultationConstant.UPDATE_TYPE_PAYMENT, ConsultationServiceManager.PAYMENT_UPDATE);
        } else
            failedTransactionId = ConsultationServiceManager.PAYMENT_UPDATE;
    }

    private ConsultationUpdateRequest paymentUpdateRequest() {
        ConsultationUpdateRequest updatingConsultationRequest = new ConsultationUpdateRequest();
        updatingConsultationRequest.setType(ConsultationConstant.UPDATE_TYPE_PAYMENT);
        updatingConsultationRequest.setUserId(userId);

        ConsultationUpdatePayload updateConsultingPayload = new ConsultationUpdatePayload();
        updateConsultingPayload.setId(consultationId);

        Payment payment = new Payment();
        payment.setId(orderId);
        payment.setStatus(ConsultationConstant.SUCCESS);

        updateConsultingPayload.setPayment(payment);

        updatingConsultationRequest.setConsultingPayload(updateConsultingPayload);

        return updatingConsultationRequest;
    }

    private void reoccurUpdate() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            ReoccurUpdateRequest reoccurUpdateRequest = new ReoccurUpdateRequest();
            reoccurUpdateRequest.setType(ConsultationConstant.UPDATE_TYPE_REOCCUR);
            ReoccurPayload reoccurPayload = new ReoccurPayload();
            reoccurPayload.setId(userId);
            reoccurPayload.setSpeciality(getSelectedSpecialization());
            reoccurPayload.setReoccurStatus(selectedPaymentPlan.equals(ConsultationConstant.UNLIMITED_PLAN));
            reoccurUpdateRequest.setReoccurPayload(reoccurPayload);
            ConsultationServiceManager.getInstance().reoccurUpdate(chatViewModel, reoccurUpdateRequest, basePreference);
        } else
            failedTransactionId = ConsultationServiceManager.REOCCUR_UPDATE;
    }

    public void onReoccurDataAvailable(ReoccurUpdateResponse reoccurUpdateResponse) {
        removeLoader();
        if (reoccurUpdateResponse != null && !TextUtils.isEmpty(reoccurUpdateResponse.getMessage()) && reoccurUpdateResponse.getMessage().equalsIgnoreCase(ConsultationConstant.SUCCESS)) {
            disableEditOption();
            sendConsultationRequestToSocket();
            showNotification(ConsultationConstant.PAYMENT_SUCCESS_MESSAGE, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
            addNewItem(messageList);
            isPaymentSuccess = true;
            isLoading = true;
            showTypingView();
            removeTypingView();
        } else
            vmOnError(ConsultationServiceManager.REOCCUR_UPDATE);
    }

    private void chatHistory() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            ChatHistoryRequest chatHistoryRequest = new ChatHistoryRequest();
            chatHistoryRequest.setChatId(chatId);
            OrderBy orderBy = new OrderBy();
            orderBy.setField(ConsultationConstant.PARAM_TIME);
            orderBy.setOrder(ConsultationConstant.PARAM_ASC);
            chatHistoryRequest.setOrderBy(orderBy);
            chatListener.vmShowProgress();
            ConsultationServiceManager.getInstance().chatHistory(chatViewModel, chatHistoryRequest, basePreference);
        } else
            failedTransactionId = ConsultationServiceManager.CHAT_HISTORY;
    }


    public void onMedicineDataAvailable(MedicineInfoResponse medicineInfoResponse) {
        if (medicineInfoResponse != null && medicineInfoResponse.getMedicines() != null && medicineInfoResponse.getMedicines().get(0).getMedicineId() != null) {
            Message message = new Message();
            message.setMessage("");
            message.setFromRole(ConsultationConstant.ROLE_MEDICINE);
            message.setMedicines(medicineInfoResponse.getMedicines());
            message.setDoctorName(medicineInfoResponse.getDoctorName());
            message.setCustomerResponse(customerResponse);
            messageList.add(message);
            addNewItem(messageList);

        }

        // as of now we have commented the lab test, as dependency on api for ordering lab test.
        if (medicineInfoResponse != null && medicineInfoResponse.getLabTests() != null && medicineInfoResponse.getLabTests().size() > 0) {
            Message lab_message = new Message();
            lab_message.setMessage("");
            lab_message.setFromRole(ConsultationConstant.ROLE_LAB_TEST);
            messageList.add(lab_message);
            addNewItem(messageList);
        }
        isAnswered = true;

    }

    public void onChatHistoryDataAvailable(ChatHistoryResponse chatHistoryResponse) {
        chatListener.vmDismissProgress();
        isLoading = false;
        if (chatHistoryResponse != null && chatHistoryResponse.getServiceStatus() != null && chatHistoryResponse.getServiceStatus().getStatusCode() != null && chatHistoryResponse.getServiceStatus().getStatusCode() == 200)
            chatHistoryResult(chatHistoryResponse);
        else
            vmOnError(ConsultationServiceManager.CHAT_HISTORY);
    }

    private void chatHistoryResult(ChatHistoryResponse chatHistoryResponse) {
        if (chatHistoryResponse.getChatHistoryResult() != null) {
            ChatHistoryResult chatHistoryResult = chatHistoryResponse.getChatHistoryResult();
            showChatHistory(chatHistoryResult);
        } else
            vmOnError(ConsultationServiceManager.CHAT_HISTORY);
    }

    private void showChatHistory(ChatHistoryResult chatHistoryResult) {
        if (chatHistoryResult.getChatHistoryList() != null && chatHistoryResult.getChatHistoryList().size() > 0) {
            messageList.addAll(chatHistoryResult.getChatHistoryList());
            messageAdapter.updateAdapter(messageList);
            if (historyPrescriptionId != null && (!TextUtils.isEmpty(historyPrescriptionId))) {
                chatListener.vmShowProgress();
                isFromChat = false;
                showOrderMedicineView(historyPrescriptionId);
            }
            checkConsultationExpiry();
        } else
            chatHistoryError(chatHistoryResult);
    }

    private void chatHistoryError(ChatHistoryResult chatHistoryResult) {
        if (chatHistoryResult.getMessage() != null && !TextUtils.isEmpty(chatHistoryResult.getMessage()))
            SnackBarHelper.snackBarCallBack(chatBinding.lytChatViewContent, context, chatHistoryResult.getMessage());
    }

    private void checkConsultationExpiry() {
        if (!isConsultationExpiry) {
            chatBinding.closeAttachment.setVisibility(View.GONE);
            if (chatBinding.messageList.getLayoutManager() != null)
                chatBinding.messageList.getLayoutManager().smoothScrollToPosition(chatBinding.messageList, null, messageAdapter.getItemCount() - 1);
            startAnimation(chatBinding.lytWriteMessage);
        } else {
            showNotification(ConsultationConstant.CONSULTATION_COMPLETED_NOTIFICATION, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
            addNewItem(messageList);
            if (isFromChat) {
                showFollowupInChat();
                showRating(false, null);
            }
        }
    }

    public void showRating(Boolean flag, Float rating) {

        if (!flag) {
            Message message = new Message();
            message.setMessage("");
            message.setFromRole(ConsultationConstant.ROLE_RATING);
            message.setDoctorName(doctorName);
            if (availableRating == null || availableRating.getRating() == 0) {
                message.setRatingSubmitRequest(new RatingSubmitRequest(userId, doctorId, (float) 0, "", "", consultationId));
                message.setRatingAvailable(false);
            } else {
                message.setRatingAvailable(true);
                message.setAvailableRating(availableRating);
            }
            messageList.add(message);
            addNewItem(messageList);
            return;
        }

        if (getHistoryResult() == null) return;
        WebEngageModel webEngageModel = new WebEngageModel();
        MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        webEngageModel.setMStarUserDetails(customerDetails);
        webEngageModel.setCommonProperties();
        if (messageList != null && messageList.size() > 0 && messageList.get(3) != null) {
            Message tempMsg = messageList.get(3);
            if (!TextUtils.isEmpty(tempMsg.getMessage())) {
                webEngageModel.setPatientIdentified(tempMsg.getMessage());
            }
        }
        if (historyResult.getSpeciality() != null && !TextUtils.isEmpty(historyResult.getSpeciality().getName()))
            webEngageModel.setSpeciality(historyResult.getSpeciality().getName());
        if (!TextUtils.isEmpty(historyResult.getSymptoms()))
            webEngageModel.setSymptoms(historyResult.getSymptoms());
        if (!TextUtils.isEmpty(historyResult.getMode()))
            webEngageModel.setType(historyResult.getMode());
        if (!TextUtils.isEmpty(historyResult.getPrescriptionStatus()))
            webEngageModel.setPrescriptionStatus(historyResult.getPrescriptionStatus());

        try {
            if (historyResult.getTestJson() != null && !historyResult.getTestJson().isJsonNull()) {
                Type type = new TypeToken<List<LabTest>>() {
                }.getType();
                List<LabTest> labTests = new Gson().fromJson(historyResult.getTestJson().getAsJsonObject().getAsJsonArray(context.getString(R.string.lab_tests)).toString(), type);
                webEngageModel.setLabTestPrescribed(context.getResources().getString(R.string.txt_web_engage_consultation_yes));
                webEngageModel.setLabTestName(ConsultationUtil.getLabTestName(labTests));
            } else {
                webEngageModel.setLabTestPrescribed(context.getResources().getString(R.string.txt_web_engage_consultation_No));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        webEngageModel.setFollowUpStatus(context.getResources().getString(R.string.txt_web_engage_consultation_No));

        if (getMedicineInfoResponseMutableLiveData() != null && getMedicineInfoResponseMutableLiveData().getValue() != null
                && getMedicineInfoResponseMutableLiveData().getValue().getMedicines() != null
                && getMedicineInfoResponseMutableLiveData().getValue().getMedicines().size() > 0) {
            getMedicineAndDosage(getMedicineInfoResponseMutableLiveData().getValue().getMedicines());
            webEngageModel.setPrescribedMedicines(medicines.toString());
            webEngageModel.setDosagePrescribed(dosages.toString());
        }
        webEngageModel.setRating(rating);
        WebEngageHelper.getInstance().ratingEvent(webEngageModel, chatListener.getContext());
    }

    private void showFollowupInChat() {
        Message message = new Message();
        message.setMessage("");
        message.setFromRole(ConsultationConstant.ROLE_FOLLOWUP);
        message.setDoctorName(doctorName);
        message.setFeeDetail(followUpFeeDetail);
        message.setFollowUpInChatDoctorInfo(doctorInformation);
        messageList.add(message);
        addNewItem(messageList);
    }


    public void downloadPrescription(String prescriptionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            if (!TextUtils.isEmpty(prescriptionId)) {
                String url = configMap.getProperty(ConfigConstant.CONSULTATION_BASE_URL) + ConsultAPIPathConstant.DOWNLOAD_PRESCRIPTION + "?pid=" + getPrescriptionId(prescriptionId);
                CommonUtils.downloadPrescription(context, url, context.getResources().getString(R.string.text_download_prescription_description), prescriptionId, ConsultationConstant.DOWNLOAD_FORMAT);
            }
        } else
            failedTransactionId = ConsultationServiceManager.DOWNLOAD_PRESCRIPTION;
    }

    private String getPrescriptionId(String prescriptionId) {
        return intentFrom != null && intentFrom.equalsIgnoreCase(ConsultationConstant.FOLLOW_UP) ? prescriptionId + "&followUp=isFollowUp" : prescriptionId;
    }

    public void setPaymentResponse(ConsultationEvent consultationEvent) {
        if (webEngageModel != null && consultationEvent != null && !TextUtils.isEmpty(consultationEvent.getPaymentMode())) {
            webEngageModel.setPaymentMode(consultationEvent.getPaymentMode());
        }

        if (consultationEvent != null) {
            checkCouponAppliedOrNotInPaymentPage(consultationEvent.getCoupon());
            checkPaymentStatus(consultationEvent.isStatus(), !TextUtils.isEmpty(consultationEvent.getOrderId()) ? consultationEvent.getOrderId() : "");
        } else
            chatBinding.btnMakePayment.setVisibility(View.VISIBLE);
    }

    private void checkPaymentStatus(boolean paymentStatus, String purchaseEventOrderId) {
        if (paymentStatus) {
            // Web Engage - Consultation Payment Status - Success
            if (webEngageModel != null) {
                webEngageModel.setPaymentStatus(context.getResources().getString(R.string.txt_web_engage_consultation_payment_success));
                WebEngageHelper.getInstance().paymentSuccessOrderPlacedEvent(webEngageModel, chatListener.getContext());
                // consultation purchase event
                sendConsultationPurchaseEvent(purchaseEventOrderId);
            }
            sendMessageToSocket(selectedPaymentPlan.equals(ConsultationConstant.UNLIMITED_PLAN) ? ConsultationConstant.UNLIMITED : ConsultationConstant.ONE_TIME, ConsultationConstant.MESSAGE_TYPE_TEXT, ConsultationConstant.ROLE_USER);
            sendConsultationRequestToSocket();
            isPaymentSuccess = true;
            disableEditOption();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showNotification(ConsultationConstant.PAYMENT_SUCCESS_MESSAGE, R.drawable.low_green_curved_rectangle, context.getResources().getColor(R.color.colorGreen));
                    addNewItem(messageList);
                    isLoading = true;
                    getSpecializationBasedBotQuestion(getSelectedSpecialization());
                    if (symptomQuestionList.size() == 0)
                        botQuestionIndex += 1;
                    showTypingView();
                    removeTypingView();
                }
            }, ConsultationConstant.TYPING_MESSAGE_DELAY_TIME);
        } else {
            chatBinding.btnMakePayment.setVisibility(View.VISIBLE);
        }
    }


    private void sendConsultationPurchaseEvent(String purchaseEventOrderId) {
        ProductAction productAction = new ProductAction(isConsultationHistoryExist() ? context.getResources().getString(R.string.txt_purchase_consultation_event)
                : context.getResources().getString(R.string.txt_purchase_event_first_consultation))
                .setTransactionId(purchaseEventOrderId)
                .setTransactionAffiliation(GoogleAnalyticsHelper.TRANSACTION_AFFILIATION)
                .setTransactionRevenue(totalAmount)
                .setTransactionTax(0)
                .setTransactionShipping(totalAmount);

        Product product = new Product()
                .setId(purchaseEventOrderId)
                .setName(context.getResources().getString(R.string.txt_purchase_event_consultation))
                .setCategory(webEngageModel.getMode())
                .setPrice(totalAmount)
                .setQuantity(1);

        List<TuneEventItem> matItemList = new ArrayList<>();
        GoogleAnalyticsHelper.getInstance().postTransactionEvent(context, new HitBuilders.ScreenViewBuilder()
                .addProduct(product)
                .setProductAction(productAction));


        matItemList.add(new TuneEventItem(context.getResources().getString(R.string.txt_purchase_event_consultation)) // Required
                .withQuantity(1) // Rest are optional
                .withUnitPrice(totalAmount)
                .withRevenue(totalAmount)
                .withAttribute1(purchaseEventOrderId));

        MATHelper.getInstance().purchaseMATEvent(basePreference, matItemList, totalAmount, purchaseEventOrderId,
                isConsultationHistoryExist() ?
                        context.getResources().getString(R.string.txt_first_order_yes) :
                        context.getResources().getString(R.string.txt_first_order_no), "", context.getResources()
                        .getString(R.string.txt_from_consultation));
    }


    private void checkCouponAppliedOrNotInPaymentPage(String data) {
        if (!TextUtils.isEmpty(data)) {
            ApplyCoupon applyCoupon = new Gson().fromJson(data, ApplyCoupon.class);
            if (applyCoupon != null && !TextUtils.isEmpty(applyCoupon.getCode())) {
                setOneTimeAppliedCoupon(applyCoupon);
                updatePaymentPlan();
            }
        }
    }

    public void onCloseView() {
        onAttachmentClose();
    }

    public void onDocument() {
        chatListener.vmInitDocumentView();
    }

    public void onCameraView() {
        chatListener.vmInitCameraView();
    }

    public void onGalleryView() {
        chatListener.vmInitGalleryView();
    }

    public void onAttachment() {
        if (chatBinding.lytAttachment.getVisibility() == View.VISIBLE) {
            chatBinding.lytAttachment.setVisibility(View.GONE);
            chatBinding.imgAttachment.setImageResource(R.drawable.ic_plus_accent);
        } else {
            chatBinding.lytAttachment.setVisibility(View.VISIBLE);
            chatBinding.imgAttachment.setImageResource(R.drawable.ic_close_bottom_dialog);
        }
    }

    public void onFailedData() {
        if (isLoaderView)
            removeLoader();
    }

    private String getHashId() {
        return hashId;
    }

    private void setHashId(String hashId) {
        this.hashId = hashId;
    }

    @SuppressLint("StaticFieldLeak")
    public class DownloadTask extends AsyncTask<String, String, String> {
        private String url;
        private String justDocToken;
        private String sessionId;
        private int position;

        DownloadTask(String url, int pos) {
            this.url = url;
            this.position = pos;
            JustDocUserResponse consultationLoginResult = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
            justDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
            sessionId = consultationLoginResult != null && consultationLoginResult.getSession() != null && consultationLoginResult.getSession().getId() != null ? consultationLoginResult.getSession().getId() : "";
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                InputStream inputStream = downloadFile(url, justDocToken, sessionId);
                if (inputStream != null)
                    return inputStream.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {
                Message message = messageList.get(position);
                message.setImageUri(getDownloadAttachmentFilePath());
                messageAdapter.notifyDataSetChanged();
            }
        }
    }

    private InputStream downloadFile(String fileUrl, String justDocToken, String sessionId) throws IOException {
        InputStream inputStream = null;

        URL url = new URL(fileUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization", "Bearer " + justDocToken);
        connection.setRequestProperty("session", sessionId);
        connection.connect();

        int responseCode = connection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {
            String disposition = connection.getHeaderField("Content-Disposition");
            contentType = connection.getContentType();

            if (disposition != null) {
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }
            } else {
                fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1, fileUrl.length());
            }
            inputStream = connection.getInputStream();
            if (inputStream != null) {
                convertStreamToFile(inputStream);
            }
        } else {
            Log.d("download file error", "" + responseCode);
        }
        connection.disconnect();
        return inputStream;
    }

    private void convertStreamToFile(InputStream inputStream) {
        String fileExtension = getFileExtension(this.contentType);
        int fileFormatIndex = fileName.lastIndexOf('.');
        String fileFormat = fileFormatIndex < 0 ? "" : fileName.substring(fileFormatIndex);
        if (TextUtils.isEmpty(fileFormat) && (TextUtils.isEmpty(fileExtension) || !fileName.endsWith(fileExtension))) {
            fileName += "." + fileExtension;
        }
        String downloadFilePath = "/Consultation/";
        File folder = new File(Environment.getExternalStorageDirectory() + downloadFilePath);
        folder.mkdirs();
        renameFileExist(inputStream, Environment.getExternalStorageDirectory() + downloadFilePath);
    }

    private boolean checkContentType() {
        return TextUtils.isEmpty(contentType) || ("*/*").equals(contentType);
    }

    private String getFileExtension(String mimeType) {
        if (!TextUtils.isEmpty(mimeType)) {
            String splitContentType[] = mimeType.split("/");
            return splitContentType.length > 0 ? splitContentType[1] : "";
        }
        return "";
    }

    private void renameFileExist(InputStream inputStream, String filePath) {
        File file = new File(filePath + fileName);
        copyInputStreamToFile(inputStream, file);
        if (!TextUtils.isEmpty(file.getPath()))
            setDownloadAttachmentFilePath(file.getPath());
    }

    private void copyInputStreamToFile(InputStream inputStream, File file) {
        try {
            OutputStream outputStream = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            long total = 0;
            while ((len = inputStream.read(buffer)) > 0) {
                total += len;
                outputStream.write(buffer, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDownloadAttachmentFilePath() {
        return downloadAttachmentFilePath;
    }

    private void setDownloadAttachmentFilePath(String downloadAttachmentFilePath) {
        this.downloadAttachmentFilePath = downloadAttachmentFilePath;
    }

    public void vmDownloadAttachment() {
        new DownloadTask(downloadAttachmentUrl, downloadFilePosition).execute();
    }

    // Fetch Patient Details
    public void fetchPatientDetails() {
        boolean isConnected = NetworkUtils.isConnected(context);
        if (isConnected) {
            ConsultationServiceManager.getInstance().getPatientDetails(this, basePreference.getMstarBasicHeaderMap());
        } else {
            failedTransactionId = ConsultationServiceManager.PATIENT_DETAILS;
        }
    }

    public boolean isConsultationHistoryExist() {
        return consultationHistoryExist;
    }

    public void setConsultationHistoryExist(boolean consultationHistoryExist) {
        this.consultationHistoryExist = consultationHistoryExist;
    }
}


