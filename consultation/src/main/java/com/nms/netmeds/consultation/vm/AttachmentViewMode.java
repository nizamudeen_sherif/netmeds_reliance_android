package com.nms.netmeds.consultation.vm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;

public class AttachmentViewMode extends AppViewModel {
    private AttachmentViewModelListener attachmentViewModelListener;

    public AttachmentViewMode(@NonNull Application application) {
        super(application);
    }

    public void init(AttachmentViewModelListener attachmentViewModelListener) {
        this.attachmentViewModelListener = attachmentViewModelListener;
    }

    public void closeView() {
        attachmentViewModelListener.closeDialog();
    }

    public void onDocument() {
        attachmentViewModelListener.documentView();
    }

    public void cameraView() {
        attachmentViewModelListener.cameraView();
    }

    public void galleryView() {
        attachmentViewModelListener.galleryView();
    }

    public interface AttachmentViewModelListener {

        void closeDialog();

        void documentView();

        void cameraView();

        void galleryView();
    }
}
