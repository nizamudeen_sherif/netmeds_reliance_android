package com.nms.netmeds.consultation.model.request;

import com.nms.netmeds.consultation.model.CustomerData;
import com.nms.netmeds.consultation.model.Medicine;

import java.util.ArrayList;

public class OrderMedicineRequest {


    private String authkey;
    private String doctor_consultant_raise;
    private CustomerData customer_data;
    private ArrayList<Medicine> cart_data;


    public String getAuthkey() {
        return authkey;
    }

    public void setAuthkey(String authkey) {
        this.authkey = authkey;
    }

    public String getDoctor_consultant_raise() {
        return doctor_consultant_raise;
    }

    public void setDoctor_consultant_raise(String doctor_consultant_raise) {
        this.doctor_consultant_raise = doctor_consultant_raise;
    }

    public CustomerData getCustomer_data() {
        return customer_data;
    }

    public void setCustomer_data(CustomerData customer_data) {
        this.customer_data = customer_data;
    }

    public ArrayList<Medicine> getCart_data() {
        return cart_data;
    }

    public void setCart_data(ArrayList<Medicine> cart_data) {
        this.cart_data = cart_data;
    }


}
