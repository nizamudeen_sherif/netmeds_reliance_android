package com.nms.netmeds.consultation;

import android.text.TextUtils;

import com.nms.netmeds.consultation.model.LabTest;
import com.nms.netmeds.consultation.model.Medicine;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Class for utils methods related to consultation
 */
public class ConsultationUtil {

    public static final String REGEX = "[0-9]-[0-9]-[0-9]";

    // get lab test name from test list
    public static String getLabTestName(List<LabTest> arrayList) {

        if (arrayList == null || arrayList.size() == 0) return "";

        StringBuilder stringBuilder = new StringBuilder();

        for (LabTest test : arrayList) {
            if (!TextUtils.isEmpty(test.getTestName())) {
                stringBuilder.append(test.getTestName());
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }

    // check if medicine prescribed to take every day
    public static boolean isEveryday(Medicine medicine) {
        Pattern pattern = Pattern.compile(REGEX);
        return pattern.matcher(medicine.getDosage()).matches();
    }

}
