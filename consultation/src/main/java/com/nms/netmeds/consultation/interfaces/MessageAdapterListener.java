package com.nms.netmeds.consultation.interfaces;

import com.nms.netmeds.consultation.model.ConsultationCoupon;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.model.Options;
import com.nms.netmeds.consultation.model.Speciality;
import com.nms.netmeds.consultation.model.request.OrderMedicineRequest;

import java.util.List;

public interface MessageAdapterListener {
    void imagePreview(String url);

    void editSelectedOption(int position, Message message);

    void selectedSpecialization(Speciality speciality);

    void closeSpecialization();

    void closeDoctorInfo();

    void viewDoctorInfo(int doctorId);

    void showAllCoupons();

    void paymentPlan(double fee, String selectedPaymentPlan);

    void selectedCouponCode(ConsultationCoupon coupon);

    void closeCouponList();

    void selectedProblem(Options selectedProblem);

    void selectedOption(List<Options> options);

    void otherOption();

    void loaderView(boolean isShowing);

    void downloadAttachment(Message message, int position);

    void orderMedicine(MedicineInfoResponse medicineInfoResponse);

    void onSubmitRating(Float rating);

    void bookLabTest(MedicineInfoResponse medicineInfoResponse);
}
