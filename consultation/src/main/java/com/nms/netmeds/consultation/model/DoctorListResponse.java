package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

import java.util.List;

public class DoctorListResponse extends BaseResponse {
    @SerializedName("result")
    private List<DoctorInformation> doctorList;
    @SerializedName("updatedOn")
    private String updatedOn;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<DoctorInformation> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<DoctorInformation> doctorList) {
        this.doctorList = doctorList;
    }
}
