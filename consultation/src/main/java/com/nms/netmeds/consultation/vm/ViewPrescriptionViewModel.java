package com.nms.netmeds.consultation.vm;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BulletSpan;
import android.view.View;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.base.model.MStarAddMultipleProductsResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.ConsultAPIPathConstant;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.ConsultationServiceManager;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ActivityViewPrescriptionBinding;
import com.nms.netmeds.consultation.model.DoctorListResponse;
import com.nms.netmeds.consultation.model.LabTest;
import com.nms.netmeds.consultation.model.Medicine;
import com.nms.netmeds.consultation.model.MedicineInfoResponse;
import com.nms.netmeds.consultation.model.Others;
import com.nms.netmeds.consultation.model.request.DoctorInfoRequest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static com.nms.netmeds.base.retrofit.APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS;
import static com.nms.netmeds.base.retrofit.APIServiceManager.MSTAR_CREATE_CART;
import static com.nms.netmeds.consultation.ConsultationConstant.LEADING_MARGIN;
import static com.nms.netmeds.consultation.ConsultationServiceManager.ORDER_MEDICINE;
import static com.nms.netmeds.consultation.ConsultationUtil.isEveryday;


public class ViewPrescriptionViewModel extends AppViewModel {
    private ActivityViewPrescriptionBinding viewPrescriptionBinding;
    private MedicineInfoResponse medicineInfoResponse;
    private DateTimeUtils dateTimeUtils;
    private String intentFrom = "";
    private ViewPrescriptionViewModel viewModel;
    private int failedTransactionId;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ConfigMap configMap;
    private PrescriptionListener prescriptionListener;
    private BasePreference basePreference;
    private String prescriptionId;
    private CustomerResponse customerResponse;
    private static final int PRESCRIPTION_ARRAY_LENGTH = 2;


    public void init(Context context, String intentFrom, ActivityViewPrescriptionBinding viewPrescriptionBinding, PrescriptionListener prescriptionListener, CustomerResponse customerResponse) {
        this.context = context;
        configMap = ConfigMap.getInstance();
        basePreference = BasePreference.getInstance(context);
        this.intentFrom = intentFrom;
        this.viewPrescriptionBinding = viewPrescriptionBinding;
        this.prescriptionListener = prescriptionListener;
        this.customerResponse = customerResponse;

    }

    public ViewPrescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public MedicineInfoResponse getPrescriptionResponse() {
        return medicineInfoResponse;
    }

    public void setPrescriptionResponse(MedicineInfoResponse prescriptionResponse) {
        this.medicineInfoResponse = prescriptionResponse;
        getDoctorInfo();
        isDoctorSpecAvailable();
        isPatientNameAvailable();
        isAgeAvailable();
        isSymptomsAvailable();
        isDiagnosisAvailable();
        isOrderAvailable();
        isInvestigationsAvailable();
        isAdviceAvailable();
        isCreationDateAvailable();
    }

    public CharSequence setUpDiagnosis() {
        if (medicineInfoResponse == null || medicineInfoResponse.getDiagnosis() == null)
            return "";

        return medicineInfoResponse.getDiagnosis();
    }

    public CharSequence setUpDocAdvice() {
        if (medicineInfoResponse == null || medicineInfoResponse.getNotes() == null)
            return "";

        return makeBulletList(LEADING_MARGIN, medicineInfoResponse.getNotes());
    }

    public CharSequence setUpInvestigations() {
        viewPrescriptionBinding.orderMedicineLongBtn.setText(getBottomButtonName());
        if (medicineInfoResponse == null || medicineInfoResponse.getLabTests() == null || medicineInfoResponse.getLabTests().size() == 0)
            return "";
        ArrayList<String> TestNamesList = new ArrayList<>();
        for (LabTest labTest : medicineInfoResponse.getLabTests()) {
            if (!TextUtils.isEmpty(labTest.getTestName())) {
                TestNamesList.add(labTest.getTestName());
            }

        }
        return makeBulletList(LEADING_MARGIN, TestNamesList.toArray(new CharSequence[TestNamesList.size()]));
    }

    public String getAgeString() {
        if (customerResponse == null || TextUtils.isEmpty(customerResponse.getDob())) return "";
        StringBuilder ageGender = new StringBuilder();
        Calendar calendar = DateTimeUtils.getInstance().getSeparatedCalendar(customerResponse.getDob());
        ageGender.append(DateTimeUtils.getInstance().calculateAgeFromDOB(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        ageGender.append(" ");
        ageGender.append(context.getResources().getString(R.string.years));
        ageGender.append(" ");

        switch (customerResponse.getGender()) {
            case 0: {
                return ageGender.toString();
            }
            case 1: {
                return ageGender.append(context.getResources().getString(R.string.txt_view_prescription_male)).toString();
            }
            case 2: {
                return ageGender.append(context.getResources().getString(R.string.txt_view_prescription_female)).toString();
            }
            case 3: {
                return ageGender.append(context.getResources().getString(R.string.txt_view_prescription_other)).toString();
            }
        }
        return ageGender.toString();
    }

    private void isAgeAvailable() {
        if (customerResponse == null || TextUtils.isEmpty(customerResponse.getDob())) {
            viewPrescriptionBinding.txtAgeAvailable.setVisibility(View.GONE);
        } else {
            viewPrescriptionBinding.txtAgeAvailable.setVisibility(View.VISIBLE);
        }
    }

    private void isOrderAvailable() {
        viewPrescriptionBinding.cardOrderAvailable.setVisibility((getPrescriptionResponse() == null
                || getPrescriptionResponse().getMedicines() == null || getPrescriptionResponse().getMedicines().size() == 0
                || TextUtils.isEmpty(getPrescriptionResponse().getMedicines().get(0).getMedicineId())) ? View.GONE : View.VISIBLE);
    }

    private void isInvestigationsAvailable() {
        viewPrescriptionBinding.cardInvestigationAvailable.setVisibility(
                (getPrescriptionResponse() == null || getPrescriptionResponse().getLabTests()
                        == null || getPrescriptionResponse().getLabTests().size() == 0) ? View.GONE : View.VISIBLE);
    }

    private void isAdviceAvailable() {
        viewPrescriptionBinding.txtAdviceAvailable.setVisibility(
                (getPrescriptionResponse() == null || TextUtils.isEmpty(getPrescriptionResponse().getNotes())) ? View.GONE : View.VISIBLE
        );
        viewPrescriptionBinding.advice.setVisibility((getPrescriptionResponse() == null || TextUtils.isEmpty(getPrescriptionResponse().getNotes())) ? View.GONE : View.VISIBLE);
    }

    private boolean isFollowUpAvailable(ViewPrescriptionViewModel viewModel) {
        return viewModel.getPrescriptionResponse() != null && viewModel.getPrescriptionResponse().getFollowUpDate() != null;
    }

    private void isCreationDateAvailable() {
        viewPrescriptionBinding.creationDateTextview.setVisibility(
                (getPrescriptionResponse() == null || TextUtils.isEmpty(getPrescriptionResponse().getCreationDate()))
                        ? View.INVISIBLE : View.VISIBLE
        );
        viewPrescriptionBinding.creationDate.setVisibility((getPrescriptionResponse() == null || TextUtils.isEmpty(getPrescriptionResponse().getCreationDate()))
                ? View.GONE : View.VISIBLE);
    }

    public String getFollowUpDate(ViewPrescriptionViewModel viewModel) {
        dateTimeUtils = DateTimeUtils.getInstance();
        if (!isFollowUpAvailable(viewModel)) return context.getString(R.string.dashes);
        return dateTimeUtils.utcToLocal(DateTimeUtils.yyyyMMddTHHmmss, DateTimeUtils.ddMMMyyyy, viewModel.getPrescriptionResponse().getFollowUpDate());

    }

    public String getCreationDate(ViewPrescriptionViewModel viewModel) {
        dateTimeUtils = DateTimeUtils.getInstance();
        if (getPrescriptionResponse() == null || TextUtils.isEmpty(getPrescriptionResponse().getCreationDate()))
            return context.getString(R.string.dashes);
        return dateTimeUtils.utcToLocal(DateTimeUtils.yyyyMMddTHHmmssSSS, DateTimeUtils.ddMMMyyyy, viewModel.getPrescriptionResponse().getCreationDate());
    }

    public String getSymptoms(ViewPrescriptionViewModel viewModel) {
        if (medicineInfoResponse == null || TextUtils.isEmpty(medicineInfoResponse.getSymptoms()))
            return "";
        return String.format("%s %s", context.getString(R.string.chief_symptoms), viewModel.medicineInfoResponse.getSymptoms());
    }

    private void isSymptomsAvailable() {
        viewPrescriptionBinding.txtSymptomAvailable.setVisibility((medicineInfoResponse == null || TextUtils.isEmpty(medicineInfoResponse.getSymptoms())) ? View.GONE : View.VISIBLE);
    }

    public static String getDuration(Medicine medicine) {
        if (!(medicine.getDuration() > 0)) return "";
        return String.format("%s Days", String.valueOf(medicine.getDuration()));
    }

    public static boolean isTablet(Medicine medicine, int position) {
        if (!isEveryday(medicine)) return false;
        if (medicine.getDosage().length() == 5) {
            String dosage = medicine.getDosage();
            String[] dosageArray = dosage.split("-");
            return !dosageArray[position].equals("0");
        } else return false;

    }

    public String getDoctorName(ViewPrescriptionViewModel viewModel) {
        if (TextUtils.isEmpty(viewModel.getPrescriptionResponse().getDoctorName())) return "";
        return viewModel.getPrescriptionResponse().getDoctorName();
    }

    public String getPatientName() {
        if (customerResponse == null || TextUtils.isEmpty(customerResponse.getFirstname()))
            return "";
        return String.format((TextUtils.isEmpty(customerResponse.getLastname()) ? "%s" : "%s %s"),
                (TextUtils.isEmpty(customerResponse.getFirstname()) ? "" : customerResponse.getFirstname().substring(0, 1).toUpperCase() + customerResponse.getFirstname().substring(1)),
                (TextUtils.isEmpty(customerResponse.getLastname()) ? "" : customerResponse.getLastname().substring(0, 1).toUpperCase() + customerResponse.getLastname().substring(1)));
    }


    public void onOrderMedicineClicked(ViewPrescriptionViewModel viewModel) {
        if (viewPrescriptionBinding.orderMedicineLongBtn.getText().equals(context.getResources().getString(R.string.book_now))) {
            bookLabTest();
            return;
        }
        this.viewModel = viewModel;
        orderMedicine(viewModel);
    }


    // book lab Test
    public void bookLabTest() {
        if (medicineInfoResponse == null || medicineInfoResponse.getLabTests() == null || medicineInfoResponse.getLabTests().size() == 0)
            return;
        prescriptionListener.navigateToBookLabTest(medicineInfoResponse);

        if (!TextUtils.isEmpty(medicineInfoResponse.getId())) {
            String[] prescriptionArray = medicineInfoResponse.getId().split(":");
            if (prescriptionArray.length > PRESCRIPTION_ARRAY_LENGTH && !TextUtils.isEmpty(basePreference.getLoganSession())) {
                String consultationId = String.format("%s:%s:%s", prescriptionArray[0], prescriptionArray[1], prescriptionArray[PRESCRIPTION_ARRAY_LENGTH]);
                ConsultationServiceManager.getInstance().trackOrderMedicine(consultationId, basePreference.getLoganSession());
            }
        }
    }

    private String getBottomButtonName() {
        if (medicineInfoResponse != null && medicineInfoResponse.getMedicines() != null && medicineInfoResponse.getMedicines().size() > 0
                && medicineInfoResponse.getMedicines().get(0).getMedicineId() != null && medicineInfoResponse.getLabTests() != null && medicineInfoResponse.getLabTests().size() > 0) {
            return context.getResources().getString(R.string.text_order_medicine);
        } else if (medicineInfoResponse != null && medicineInfoResponse.getMedicines() != null && medicineInfoResponse.getMedicines().size() > 0 && medicineInfoResponse.getMedicines().get(0).getMedicineId() != null) {
            return context.getResources().getString(R.string.text_order_medicine);
        } else if (medicineInfoResponse != null && medicineInfoResponse.getLabTests() != null && medicineInfoResponse.getLabTests().size() > 0) {
            return context.getResources().getString(R.string.book_now);
        } else {
            return context.getResources().getString(R.string.text_order_medicine);
        }
    }


    private void orderMedicine(ViewPrescriptionViewModel viewModel) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            prescriptionListener.vmShowProgress();
            if (!TextUtils.isEmpty(medicineInfoResponse.getId()) && !TextUtils.isEmpty(basePreference.getLoganSession())) {
                String[] prescriptionArray = medicineInfoResponse.getId().split(":");
                if (prescriptionArray.length > 2) {
                    String consultationId = String.format("%s:%s:%s", prescriptionArray[0], prescriptionArray[1], prescriptionArray[2]);
                    ConsultationServiceManager.getInstance().trackOrderMedicine(consultationId, basePreference.getLoganSession());
                }
            }
            addToCart();
        } else {
            failedTransactionId = ConsultationServiceManager.ORDER_MEDICINE;
        }
    }

    private void addToCart() {
        if (basePreference.getMStarCartId() > 0) {
            checkCartStatusToAddToCart();
        } else {
            initiateAPICall(MSTAR_CREATE_CART);
        }
    }

    private void checkCartStatusToAddToCart() {
        initiateAPICall(MSTAR_ADD_MULTIPLE_PRODUCTS);
    }

    private void onAddedProductResponse(String data) {
        prescriptionListener.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarAddMultipleProductsResponse response = new Gson().fromJson(data, MStarAddMultipleProductsResponse.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getAddedProductsResultMap() != null && !response.getAddedProductsResultMap().isEmpty()) {
                LaunchIntentManager.routeToActivity(context.getString(R.string.route_netmeds_mstar_cart), context);
            } else {
                // show message - problem to order medicines
                SnackBarHelper.snackBarCallBack(viewPrescriptionBinding.lytViewPrescriptionViewContent, context, context.getResources().getString(R.string.txt_error_placing_order));
            }
        }
    }

    private HashMap<String, Integer> getProductList() {
        HashMap<String, Integer> productList = new HashMap<>();
        if (medicineInfoResponse == null || medicineInfoResponse.getMedicines() == null || medicineInfoResponse.getMedicines().size() == 0)
            return productList;
        for (Medicine medicine : medicineInfoResponse.getMedicines()) {
            productList.put(medicine.getMedicineId(), 1);
        }
        return productList;
    }


    private void initiateAPICall(int transactionId) {
        switch (transactionId) {
            case MSTAR_CREATE_CART:
                APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                break;
            case MSTAR_ADD_MULTIPLE_PRODUCTS:
                APIServiceManager.getInstance().mstarAddMutlipleProducts(this, basePreference.getMstarBasicHeaderMap(), getProductList());
                break;
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        viewPrescriptionBinding.lytViewPrescriptionViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        viewPrescriptionBinding.prescriptionNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        prescriptionListener.vmDismissProgress();
        switch (transactionId) {
            case ConsultationServiceManager.DOCTOR_INFO: {
                if (TextUtils.isEmpty(data)) return;
                DoctorListResponse doctorInfoResponse = new Gson().fromJson(data, DoctorListResponse.class);
                if (doctorInfoResponse != null && doctorInfoResponse.getDoctorList() != null && doctorInfoResponse.getDoctorList().size() > 0) {
                    RequestOptions options = new RequestOptions()
                            .circleCrop()
                            .placeholder(R.drawable.ic_medi_bot_logo)
                            .error(R.drawable.ic_medi_bot_logo)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);
                    Glide.with(context).load(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_IMAGE_URL) + doctorInfoResponse.getDoctorList().get(0).getImage()).apply(options).into(viewPrescriptionBinding.imgViewDoctor);
                }
            }
            break;
            case APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS:
                onAddedProductResponse(data);
                break;
            case MSTAR_CREATE_CART:
                createMStarCartResponse(data);
                break;
        }
    }

    private void createMStarCartResponse(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            basePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            checkCartStatusToAddToCart();
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        prescriptionListener.vmDismissProgress();
        failedTransactionId = transactionId;
        if (transactionId == MSTAR_CREATE_CART || transactionId == MSTAR_ADD_MULTIPLE_PRODUCTS)
            failedTransactionId = ORDER_MEDICINE;
        showWebserviceErrorView(true);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        prescriptionListener.vmDismissProgress();
        viewPrescriptionBinding.lytViewPrescriptionViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        viewPrescriptionBinding.prescriptionApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onRetry(failedTransactionId);
    }

    private void onRetry(int failedTransactionId) {
        if (failedTransactionId == ConsultationServiceManager.ORDER_MEDICINE) {
            orderMedicine(viewModel);
        }
        if (failedTransactionId == ConsultationServiceManager.DOWNLOAD_PRESCRIPTION) {
            downloadPrescription(prescriptionId);
        }
    }

    private void isPatientNameAvailable() {
        if (customerResponse == null || TextUtils.isEmpty(customerResponse.getFirstname())) {
            viewPrescriptionBinding.txtPatientAvailable.setVisibility(View.GONE);
        } else {
            viewPrescriptionBinding.txtPatientAvailable.setVisibility(View.VISIBLE);
        }
    }

    private void isDiagnosisAvailable() {
        viewPrescriptionBinding.txtDiagAvailable.setVisibility((medicineInfoResponse == null || TextUtils.isEmpty(medicineInfoResponse.getDiagnosis()))
                ? View.GONE : View.VISIBLE);
        viewPrescriptionBinding.lLayoutDiagnostic.setVisibility((medicineInfoResponse == null || TextUtils.isEmpty(medicineInfoResponse.getDiagnosis()))
                ? View.GONE : View.VISIBLE);
    }

    public String getDoctorSpec(ViewPrescriptionViewModel viewModel) {
        StringBuilder stringBuilder = new StringBuilder();
        if (medicineInfoResponse == null || medicineInfoResponse.getDoctorDegrees() == null
                || medicineInfoResponse.getDoctorDegrees().getMain() == null
                || TextUtils.isEmpty(medicineInfoResponse.getDoctorDegrees().getMain().getTitle()))
            return stringBuilder.toString();

        stringBuilder.append(viewModel.medicineInfoResponse.getDoctorDegrees().getMain().getTitle());

        if (viewModel.medicineInfoResponse.getDoctorDegrees().getOthersList() == null
                || viewModel.medicineInfoResponse.getDoctorDegrees().getOthersList().size() == 0)
            return stringBuilder.toString();

        for (Others others : viewModel.medicineInfoResponse.getDoctorDegrees().getOthersList()) {
            stringBuilder.append(", ");
            stringBuilder.append(others.getDomain());
        }
        return stringBuilder.toString();
    }

    private void isDoctorSpecAvailable() {
        if (medicineInfoResponse != null && medicineInfoResponse.getDoctorDegrees() != null
                && medicineInfoResponse.getDoctorDegrees().getMain() != null
                && !TextUtils.isEmpty(medicineInfoResponse.getDoctorDegrees().getMain().getTitle())) {
            viewPrescriptionBinding.txtDoctorSpec.setVisibility(View.VISIBLE);
        } else {
            viewPrescriptionBinding.txtDoctorSpec.setVisibility(View.GONE);
        }
    }

    public String getName(String name) {
        return String.format("With %s", name);
    }

    public void downloadPrescription(String prescriptionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        this.prescriptionId = prescriptionId;
        showNoNetworkView(isConnected);
        if (isConnected) {
            if (!TextUtils.isEmpty(prescriptionId)) {
                String url = configMap.getProperty(ConfigConstant.CONSULTATION_BASE_URL) + ConsultAPIPathConstant.DOWNLOAD_PRESCRIPTION + "?pid=" + getPrescriptionId(prescriptionId);
                CommonUtils.downloadPrescription(context, url, context.getResources().getString(R.string.text_download_prescription_description), prescriptionId, ConsultationConstant.DOWNLOAD_FORMAT);
            }
        } else {
            failedTransactionId = ConsultationServiceManager.DOWNLOAD_PRESCRIPTION;
        }
    }

    private String getPrescriptionId(String prescriptionId) {
        return intentFrom != null && intentFrom.equalsIgnoreCase(ConsultationConstant.FOLLOW_UP) ? prescriptionId + "&followUp=isFollowUp" : prescriptionId;
    }


    private CharSequence makeBulletList(int leadingMargin, CharSequence... lines) {

        SpannableStringBuilder sb = new SpannableStringBuilder();
        for (int i = 0; i < lines.length; i++) {
            CharSequence line = lines[i] + (i < lines.length - 1 ? "\n" : "");
            Spannable spannable = new SpannableString(line);
            spannable.setSpan(new BulletSpan(leadingMargin), 0, spannable.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            sb.append(spannable);
        }
        return sb;
    }

    public interface PrescriptionListener {
        void vmShowProgress();

        void vmDismissProgress();

        void navigateToBookLabTest(MedicineInfoResponse medicineInfoResponse);
    }

    private void getDoctorInfo() {
        boolean isConnected = NetworkUtils.isConnected(context);
        if (!isConnected || medicineInfoResponse == null || TextUtils.isEmpty(medicineInfoResponse.getDoctorId()))
            return;
        DoctorInfoRequest doctorInfoRequest = new DoctorInfoRequest();
        List<Integer> doctorIdList = new ArrayList<>();
        doctorIdList.add(Integer.parseInt(medicineInfoResponse.getDoctorId()));
        doctorInfoRequest.setDoctorId(doctorIdList);
        ConsultationServiceManager.getInstance().getDoctorInfo(this, doctorInfoRequest);
    }
}