package com.nms.netmeds.consultation;

public class ConsultAPIPathConstant {
    static final String DO_LOGIN = "external/logan";
    static final String HISTORY = "newApi/consult/history";
    static final String DOCTOR_INFO = "doctor/info/get";
    public static final String DOWNLOAD_PRESCRIPTION = "prescription/getPdf";
    static final String CREATE_CHAT = "chat/create";
    static final String UPDATE_CONSULTATION = "newApi/user/consultation/update";
    static final String GET_ALL_SPECIALITY = "API/speciality/get/all";
    static final String CREATE_CONSULTATION = "newApi/user/consultation/create";
    static final String CONSULTATION_FEE = "user/fee/get";
    static final String GET_ALL_COUPON = "coupons/get/all";
    static final String APPLY_COUPON = "API/coupon";
    static final String UPLOAD_ATTACHMENT = "newApi/user/file/upload";
    static final String PAYMENT_CREATION = "newApi/user/payment/create";
    static final String REOCCUR_UPDATE = "user/update";
    static final String CHAT_HISTORY = "message/get";
    static final String C_MSTAR_LOGAN_TOKEN = "api/v1/customer/logantoken";
    public static final String DOWNLOAD_ATTACHMENT = "newApi/attachment/";
    static final String PUSH_REGISTER = "newApi/push/register";
    static final String PRESCRIPTION_DETAILS = "newapi/prescription/get";
    static final String ORDER_MEDICINE = "newApi/user/order/prescription";
    static final String CONSULTATION_RATING = "newApi/user/rating/";
    static final String PATIENT_DETAILS = "customers/me";
    static final String CONSULTATION_BANNER = "banners/get/all";
    static final String ORDER_FROM_PRESCRIPTION = "user/placeOrderThroughPrescription";
    static final String CONSULTATION_PAYMENT_GATEWAY_LIST = "/newApi/user/payment/list";
    public static final String PUSH_UN_REGISTER = "newApi/push/unregister";

}
