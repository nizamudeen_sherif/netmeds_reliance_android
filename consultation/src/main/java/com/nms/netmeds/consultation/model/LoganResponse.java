package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class LoganResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private LoganResult loganResult;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public LoganResult getLoganResult() {
        return loganResult;
    }

    public void setLoganResult(LoganResult loganResult) {
        this.loganResult = loganResult;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
