package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.FollowUpInChatBinding;
import com.nms.netmeds.consultation.model.ConsultationFee;
import com.nms.netmeds.consultation.model.DoctorInformation;
import com.nms.netmeds.consultation.model.Message;
import com.nms.netmeds.consultation.view.ChatActivity;

public class FollowUpInChatViewModel extends AppViewModel {
    private final Application application;
    private Message message;
    private ConsultationFee consultationFee;
    protected FollowUpInChatBinding followUpInChatBinding;
    protected Context context;
    private DoctorInformation doctorInformation;

    public FollowUpInChatViewModel(Application application) {
        super(application);
        this.application = application;
    }

    public void onDataAvailable(Context context, Message message, FollowUpInChatBinding followUpInChatBinding) {
        this.context = context;
        this.message = message;
        this.followUpInChatBinding = followUpInChatBinding;
        this.doctorInformation = message.getFollowUpInChatDoctorInfo();
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_circular)
                .error(gender() == 0 ? R.drawable.ic_circular : gender())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(!TextUtils.isEmpty(imageUrl()) ? imageUrl() : gender() == 0 ? R.drawable.ic_circular : gender()).apply(options).into(followUpInChatBinding.imgDoctor);
        showDoctorName();
        showSpecialization();
        showExperience();
    }

    private String imageUrl() {
        String url = "";
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getImage())) {
            url = ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_IMAGE_URL) + doctorInformation.getImage();
        }
        return url;
    }

    public String doctorName() {
        if (doctorInformation == null || TextUtils.isEmpty(doctorInformation.getName())) return "";
        return doctorInformation.getName();
    }

    private void showDoctorName() {
        followUpInChatBinding.textDoctorName.setVisibility((doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getName())) ? View.VISIBLE : View.GONE);
    }

    public String doctorSpeciality() {
        if (doctorInformation == null || TextUtils.isEmpty(doctorInformation.getSpecialityTitle())) return "";
        return doctorInformation.getSpecialityTitle();
    }

    private void showSpecialization() {
        followUpInChatBinding.textSpecialization.setVisibility((doctorInformation!=null && !TextUtils.isEmpty(doctorInformation.getSpecialityTitle())) ? View.VISIBLE : View.GONE);
    }

    public String doctorExperience() {
        if (doctorInformation ==  null || TextUtils.isEmpty(doctorInformation.getExperience())) return "";
        return doctorInformation.getExperience() + " " + context.getString(R.string.years_of_experience);
    }

    private void showExperience() {
        followUpInChatBinding.textExperience.setVisibility((doctorInformation!=null && !TextUtils.isEmpty(doctorInformation.getExperience())) ? View.VISIBLE : View.GONE);
    }

    public void onFollowup() {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(ConsultationConstant.KEY_INTENT_FROM, "");

        LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_chat_activity), intent, context);

    }


    private int gender() {
        int gender = 0;
        if (doctorInformation != null && !TextUtils.isEmpty(doctorInformation.getGender())) {
            if (doctorInformation.getGender().equals("male")) {
                gender = R.drawable.ic_default_doctor_male;
            } else {
                gender = R.drawable.ic_default_doctor_female;
            }
        }
        return gender;
    }

}
