package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class ConsultationUpdatePayload {
    @SerializedName("id")
    private String id;
    @SerializedName("payment")
    private Payment payment;
    @SerializedName("speciality")
    private String speciality;
    @SerializedName("mode")
    private String mode;
    @SerializedName("couponCode")
    private String couponCode;
    @SerializedName("followUp")
    private FollowUp followUp;
    @SerializedName("symptoms")
    private String symptoms;

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public FollowUp getFollowUp() {
        return followUp;
    }

    public void setFollowUp(FollowUp followUp) {
        this.followUp = followUp;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
