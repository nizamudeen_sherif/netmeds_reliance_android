package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BotSymptomQuestion {
    @SerializedName("field")
    private String field;
    @SerializedName("id")
    private String id;
    @SerializedName("nextIndex")
    private int nextIndex;
    @SerializedName("fieldType")
    private String fieldType;
    @SerializedName("role")
    private String role;
    @SerializedName("type")
    private String type;
    @SerializedName("options")
    private List<Options> optionsList;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNextIndex() {
        return nextIndex;
    }

    public void setNextIndex(int nextIndex) {
        this.nextIndex = nextIndex;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Options> getOptionsList() {
        return optionsList;
    }

    public void setOptionsList(List<Options> optionsList) {
        this.optionsList = optionsList;
    }
}
