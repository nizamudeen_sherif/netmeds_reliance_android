package com.nms.netmeds.consultation.model;

import java.io.Serializable;

public class ConsultationFeeDetail implements Serializable {
    private String unlimitedActualPrice;
    private String unlimitedStrikeOutPrice;
    private String unlimitedPercentageValue;
    private String oneTimeActualPrice;
    private String oneTimeStrikeOutPrice;
    private String oneTimePercentageValue;
    private boolean isCouponUpdate;


    public String getOneTimeActualPrice() {
        return oneTimeActualPrice;
    }

    public void setOneTimeActualPrice(String oneTimeActualPrice) {
        this.oneTimeActualPrice = oneTimeActualPrice;
    }

    public String getOneTimeStrikeOutPrice() {
        return oneTimeStrikeOutPrice;
    }

    public void setOneTimeStrikeOutPrice(String oneTimeStrikeOutPrice) {
        this.oneTimeStrikeOutPrice = oneTimeStrikeOutPrice;
    }

    public String getOneTimePercentageValue() {
        return oneTimePercentageValue;
    }

    public void setOneTimePercentageValue(String oneTimePercentageValue) {
        this.oneTimePercentageValue = oneTimePercentageValue;
    }

    public String getUnlimitedActualPrice() {
        return unlimitedActualPrice;
    }

    public void setUnlimitedActualPrice(String unlimitedActualPrice) {
        this.unlimitedActualPrice = unlimitedActualPrice;
    }

    public String getUnlimitedStrikeOutPrice() {
        return unlimitedStrikeOutPrice;
    }

    public void setUnlimitedStrikeOutPrice(String unlimitedStrikeOutPrice) {
        this.unlimitedStrikeOutPrice = unlimitedStrikeOutPrice;
    }

    public String getUnlimitedPercentageValue() {
        return unlimitedPercentageValue;
    }

    public void setUnlimitedPercentageValue(String unlimitedPercentageValue) {
        this.unlimitedPercentageValue = unlimitedPercentageValue;
    }

    public boolean isCouponUpdate() {
        return isCouponUpdate;
    }

    public void setCouponUpdate(boolean couponUpdate) {
        isCouponUpdate = couponUpdate;
    }

}
