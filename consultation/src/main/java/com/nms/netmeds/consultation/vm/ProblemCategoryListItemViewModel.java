package com.nms.netmeds.consultation.vm;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.ProblemCategoryListItemBinding;
import com.nms.netmeds.consultation.model.Options;

public class ProblemCategoryListItemViewModel extends AppViewModel {
    private Options problemCategory;
    private ProblemCategoryListItemListener problemCategoryListItemListener;

    public ProblemCategoryListItemViewModel(@NonNull Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, Options problemCategory, ProblemCategoryListItemBinding problemCategoryListItemBinding, ProblemCategoryListItemListener problemCategoryListItemListener) {
        this.problemCategory = problemCategory;
        this.problemCategoryListItemListener = problemCategoryListItemListener;
        if (problemCategory.isChecked()) {
            problemCategoryListItemBinding.imgCategoryCheck.setImageResource(R.drawable.ic_done_active);
            problemCategoryListItemBinding.textProblemCategory.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
        } else {
            problemCategoryListItemBinding.imgCategoryCheck.setImageResource(android.R.color.transparent);
            problemCategoryListItemBinding.textProblemCategory.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Regular.ttf"));
        }
    }

    public String problemCategory() {
        return problemCategory != null && !TextUtils.isEmpty(problemCategory.getOption()) ? problemCategory.getOption() : "";
    }

    public void onProblemCategorySelection() {
        problemCategoryListItemListener.selectedProblem(problemCategory);
    }

    public interface ProblemCategoryListItemListener {
        void selectedProblem(Options problemCategory);
    }
}
