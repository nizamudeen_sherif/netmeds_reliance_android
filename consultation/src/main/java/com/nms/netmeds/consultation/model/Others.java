package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Others implements Serializable {
    @SerializedName("college")
    private String college;
    @SerializedName("domain")
    private String domain;
    @SerializedName("year")
    private String year;

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
