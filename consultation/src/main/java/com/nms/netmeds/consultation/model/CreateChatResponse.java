package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class CreateChatResponse extends BaseResponse {
    @SerializedName("result")
    private CreateChatResult result;
    @SerializedName("updatedOn")
    private String updatedOn;

    public CreateChatResult getResult() {
        return result;
    }

    public void setResult(CreateChatResult result) {
        this.result = result;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
