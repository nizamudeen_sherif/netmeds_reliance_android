package com.nms.netmeds.consultation.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.consultation.R;
import com.nms.netmeds.consultation.databinding.DialogSpecializationBinding;
import com.nms.netmeds.consultation.interfaces.MessageAdapterListener;
import com.nms.netmeds.consultation.model.Speciality;
import com.nms.netmeds.consultation.vm.SpecializationBottomSheetDialogViewModel;

import java.util.List;

@SuppressLint("ValidFragment")
public class SpecializationBottomSheetDialog extends BaseDialogFragment implements SpecializationBottomSheetDialogViewModel.SpecializationBottomSheetDialogListener {
    private final List<Speciality> specialityList;
    private boolean isEditSpecialization;
    private final MessageAdapterListener messageAdapterListener;

    @SuppressLint("ValidFragment")
    public SpecializationBottomSheetDialog(List<Speciality> specialityList, boolean isEditSpecialization, MessageAdapterListener messageAdapterListener) {
        this.specialityList = specialityList;
        this.isEditSpecialization = isEditSpecialization;
        this.messageAdapterListener = messageAdapterListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogSpecializationBinding specializationBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_specialization, container, false);
        SpecializationBottomSheetDialogViewModel bottomSheetDialogViewModel = new SpecializationBottomSheetDialogViewModel(getActivity().getApplication());
        bottomSheetDialogViewModel.onDataAvailable(getActivity(), getActivity().getApplication(), specialityList, isEditSpecialization, specializationBinding, this);
        specializationBinding.setViewModel(bottomSheetDialogViewModel);
        return specializationBinding.getRoot();
    }

    @Override
    public void vmBottomSheetDismiss() {
        this.dismiss();
        messageAdapterListener.closeSpecialization();
    }

    @Override
    public void vmDone(Speciality speciality) {
        this.dismiss();
        messageAdapterListener.selectedSpecialization(speciality);
    }
}
