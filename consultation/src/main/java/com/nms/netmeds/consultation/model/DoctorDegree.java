package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DoctorDegree implements Serializable {
    @SerializedName("main")
    private Main main;
    @SerializedName("others")
    private List<Others> othersList;

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public List<Others> getOthersList() {
        return othersList;
    }

    public void setOthersList(List<Others> othersList) {
        this.othersList = othersList;
    }
}
