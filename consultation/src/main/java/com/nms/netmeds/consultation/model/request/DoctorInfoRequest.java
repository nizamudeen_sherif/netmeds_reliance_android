package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorInfoRequest {
    @SerializedName("ids")
    private List<Integer> doctorId;

    public List<Integer> getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(List<Integer> doctorId) {
        this.doctorId = doctorId;
    }
}
