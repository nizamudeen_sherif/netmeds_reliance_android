package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class ApplyCouponRequest {
    @SerializedName("couponCode")
    private String couponCode;
    @SerializedName("type")
    private String type;
    @SerializedName("specs")
    private String specs;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }
}
