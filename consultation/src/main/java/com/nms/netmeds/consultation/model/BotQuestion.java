package com.nms.netmeds.consultation.model;

import java.util.List;

public class BotQuestion {
    private List<Question> botQuestion;

    public List<Question> getBotQuestion() {
        return botQuestion;
    }

    public void setBotQuestion(List<Question> botQuestion) {
        this.botQuestion = botQuestion;
    }
}
