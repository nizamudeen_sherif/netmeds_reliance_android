package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class ReoccurUpdateRequest{
    @SerializedName("type")
    private String type;
    @SerializedName("payload")
    private ReoccurPayload reoccurPayload;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ReoccurPayload getReoccurPayload() {
        return reoccurPayload;
    }

    public void setReoccurPayload(ReoccurPayload reoccurPayload) {
        this.reoccurPayload = reoccurPayload;
    }
}
