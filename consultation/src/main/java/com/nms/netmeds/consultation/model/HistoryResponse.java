package com.nms.netmeds.consultation.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

import java.util.List;

public class HistoryResponse extends BaseResponse {
    @SerializedName("result")
    private List<HistoryResult> historyResults;
    @SerializedName("count")
    private int count;
    @SerializedName("pages")
    private int noOfPages;
    @SerializedName("currentPage")
    private int currentPage;
    @SerializedName("configs")
    private Config config;

    public List<HistoryResult> getHistoryResults() {
        return historyResults;
    }

    public void setHistoryResults(List<HistoryResult> historyResults) {
        this.historyResults = historyResults;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(int noOfPages) {
        this.noOfPages = noOfPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
