package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class CreateConsultationRequest {
    @SerializedName("userId")
    private Integer userId;
    @SerializedName("id")
    private String id;
    @SerializedName("speciality")
    private String speciality;
    @SerializedName("type")
    private Integer type;
    @SerializedName("status")
    private String status;
    @SerializedName("partnerId")
    private String partnerId;
    @SerializedName("referrer")
    private String referrer;
    @SerializedName("symptoms")
    private String symptoms;
    @SerializedName("chosenSymptoms")
    private String chosenSymptoms;
    @SerializedName("interPatient")
    private boolean interPatient;
    @SerializedName("reoccur")
    private boolean reoccur;
    @SerializedName("payment")
    private Payment payment;
    @SerializedName("mode")
    private String mode;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getChosenSymptoms() {
        return chosenSymptoms;
    }

    public void setChosenSymptoms(String chosenSymptoms) {
        this.chosenSymptoms = chosenSymptoms;
    }

    public boolean isInterPatient() {
        return interPatient;
    }

    public void setInterPatient(boolean interPatient) {
        this.interPatient = interPatient;
    }

    public boolean isReoccur() {
        return reoccur;
    }

    public void setReoccur(boolean reoccur) {
        this.reoccur = reoccur;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
