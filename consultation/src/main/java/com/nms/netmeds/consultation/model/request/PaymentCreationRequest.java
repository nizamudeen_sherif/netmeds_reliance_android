package com.nms.netmeds.consultation.model.request;

import com.google.gson.annotations.SerializedName;

public class PaymentCreationRequest {
    @SerializedName("id")
    private String id;
    @SerializedName("pg")
    private String paymentGateway;
    @SerializedName("time")
    private String time;
    @SerializedName("status")
    private String status;
    @SerializedName("amount")
    private String amount;


    @SerializedName("consulation_payment_type")
    private String consulation_payment_type;
    @SerializedName("consultation_id")
    private String consulation_id;

    public String getConsulation_payment_type() {
        return consulation_payment_type;
    }

    public void setConsulation_payment_type(String consulation_payment_type) {
        this.consulation_payment_type = consulation_payment_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getConsulation_id() {
        return consulation_id;
    }

    public void setConsulation_id(String consulation_id) {
        this.consulation_id = consulation_id;
    }

}
