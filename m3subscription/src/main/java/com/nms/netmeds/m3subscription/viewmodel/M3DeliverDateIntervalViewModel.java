package com.nms.netmeds.m3subscription.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimatePinCodeResult;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.databinding.ActivityM3DeliveryDateIntervalBinding;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class M3DeliverDateIntervalViewModel extends AppViewModel {
    private M3DeliverDateIntervalListener listener;
    private DeliveryEstimateRequest estimateRequest;
    private BasePreference basePreference;
    private boolean isRefillInitiateFromBanner;
    private int rescheduleTimeDuration = 0;
    private int failedTransactionId;
    private String nextDeliveryIntervalListBuilder = "";
    private String estimateDeliveryDate;

    public M3DeliverDateIntervalViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(M3DeliverDateIntervalListener m3DeliverDateIntervalListener) {
        this.listener = m3DeliverDateIntervalListener;
        this.basePreference = BasePreference.getInstance(listener.getContext());
        initiateProperties();
    }

    private void initiateProperties() {
        isRefillInitiateFromBanner = listener.getIntentFromActivity().getBooleanExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, false);
        if (!isRefillInitiateFromBanner) {
            fetchDeliveryEstimateTime();
            listener.binding().cvFirstDelivery.setVisibility(View.GONE);
        } else {
            listener.binding().cvFirstDelivery.setVisibility(View.VISIBLE);
            estimateDeliveryDate = getEstimateDeliveryDate();
            setDefaultOrPreSelectedDeliveryInterval();
        }
    }

    private void fetchDeliveryEstimateTime() {
        String estimatedDateRequest = listener.getIntentFromActivity().getStringExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST);
        if (!TextUtils.isEmpty(estimatedDateRequest)) {
            estimateRequest = new Gson().fromJson(estimatedDateRequest, DeliveryEstimateRequest.class);
            MStarAddressModel customerAddress = new Gson().fromJson(PaymentHelper.getSingle_address(), MStarAddressModel.class);
            estimateRequest.setPincode(customerAddress.getPin());
            initiateAPICall(APIServiceManager.DELIVERY_DATE_ESTIMATE);
        }
    }

    public String getEstimateDeliveryDate() {
        Calendar calendar = Calendar.getInstance();
        String currentDate = calendar.get(Calendar.DATE) + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR);
        estimateDeliveryDate = isRefillInitiateFromBanner && listener.getIntentFromActivity().hasExtra(SubscriptionIntentConstant.EXPECTED_DELIVERY_DATE) ? listener.getIntentFromActivity().getExtras().getString(SubscriptionIntentConstant.EXPECTED_DELIVERY_DATE) : TextUtils.isEmpty(estimateDeliveryDate) ? currentDate : estimateDeliveryDate;
        SubscriptionHelper.getInstance().setCurrentDeliveryDate(estimateDeliveryDate);
        return estimateDeliveryDate;
    }

    public String getNextDeliveryEstimateInterval() {
        return String.format(listener.getContext().getString(R.string.text_next_delivery_interval), nextDeliveryIntervalListBuilder);
    }

    public void onClick30Days() {
        listener.clickCheckBox(true, false, false);
        rescheduleTimeDuration = 30;
        basePreference.setDeliveryIntervalPreference(rescheduleTimeDuration);
        calculateNextDeliveryTimeInterval();
    }

    public void onClick45Days() {
        listener.clickCheckBox(false, true, false);
        rescheduleTimeDuration = 45;
        basePreference.setDeliveryIntervalPreference(rescheduleTimeDuration);
        calculateNextDeliveryTimeInterval();
    }

    public void onClick60Days() {

        listener.clickCheckBox(false, false, true);
        rescheduleTimeDuration = 60;
        basePreference.setDeliveryIntervalPreference(rescheduleTimeDuration);
        calculateNextDeliveryTimeInterval();
    }

    public void onClickProceed() {
        listener.vmCallbackSubscriptionDeliverDateActivity(rescheduleTimeDuration);
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(listener.getContext());
        listener.showNoNetworkView(isConnected);
        if (isConnected) {
            listener.showLoader();
            if (transactionId == APIServiceManager.DELIVERY_DATE_ESTIMATE) {
                APIServiceManager.getInstance().deliveryEstimation(this, estimateRequest, getDeliveryDateApi());
            }
        }
    }

    private String getDeliveryDateApi() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDeliveryDateApi())) {
            return configurationResponse.getResult().getConfigDetails().getDeliveryDateApi() + "/";
        } else {
            return "";
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        listener.dismissLoader();
        if (transactionId == APIServiceManager.DELIVERY_DATE_ESTIMATE) {
            deliveryEstimateResponse(data);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        failedTransactionId = transactionId;
        listener.dismissLoader();
        if (transactionId == APIServiceManager.DELIVERY_DATE_ESTIMATE) {
            listener.binding().cvFirstDelivery.setVisibility(View.VISIBLE);
            listener.binding().invalidateAll();
            onClick30Days();
        } else {
            listener.dismissLoader();
            showApiError(failedTransactionId);
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        listener.showWebserviceErrorView(false);
    }

    private void deliveryEstimateResponse(String data) {
        DeliveryEstimateResponse response = new Gson().fromJson(data, DeliveryEstimateResponse.class);
        if (response.getStatus() != null && response.getStatus() && response.getResult() != null && response.getResult().getPinCodeResultList() != null && response.getResult().getPinCodeResultList().size() > 0) {
            List<String> estimation = new ArrayList<>();
            for (DeliveryEstimatePinCodeResult pinCodeResult : response.getResult().getPinCodeResultList()) {
                estimation.add(DateTimeUtils.getInstance().stringDate(pinCodeResult.getDeliveryEstimate().getInDates().getOnOrBefore(), DateTimeUtils.yyyyMMdd, DateTimeUtils.ddMMMyyyy));
            }
            estimateDeliveryDate = Collections.max(estimation);
            listener.binding().cvFirstDelivery.setVisibility(View.VISIBLE);
            listener.binding().invalidateAll();
            setDefaultOrPreSelectedDeliveryInterval();
        }
    }

    private void calculateNextDeliveryTimeInterval() {
        List<String> nextDeliveryTimeIntervalList = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getEstimateCalenderTime());
            calendar.add(Calendar.DATE, rescheduleTimeDuration * i);
            if (i == 1) {
                DecimalFormat mFormat = new DecimalFormat("00");
                String date = mFormat.format(Double.valueOf(calendar.get(Calendar.DATE))) + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).substring(0, 3) + " " + calendar.get(Calendar.YEAR);
                SubscriptionHelper.getInstance().setNextDeliveryIntervalDate(date);
            }
            nextDeliveryTimeIntervalList.add(calendar.get(Calendar.DATE) + "" + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).substring(0, 3));
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : nextDeliveryTimeIntervalList) {
            stringBuilder.append(str + ", ");
        }
        nextDeliveryIntervalListBuilder = stringBuilder.toString();
        listener.binding().invalidateAll();
    }

    private Date getEstimateCalenderTime() {
        Date date = new Date();
        try {
            date = new SimpleDateFormat(DateTimeUtils.ddMMMyyyy).parse(estimateDeliveryDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    private void setDefaultOrPreSelectedDeliveryInterval() {
        if (basePreference.getDeliveryIntervalPreference() > 0) {
            if (basePreference.getDeliveryIntervalPreference() == 30) {
                onClick30Days();
            } else if (basePreference.getDeliveryIntervalPreference() == 45) {
                onClick45Days();
            } else if (basePreference.getDeliveryIntervalPreference() == 60) {
                onClick60Days();
            }
        } else {
            onClick30Days();
        }
    }

    private void showApiError(int transactionId) {
        failedTransactionId = transactionId;
        listener.showWebserviceErrorView(true);
    }


    public interface M3DeliverDateIntervalListener {

        void showLoader();

        void dismissLoader();

        void vmCallbackSubscriptionDeliverDateActivity(int rescheduleTimeDuration);

        Intent getIntentFromActivity();

        ActivityM3DeliveryDateIntervalBinding binding();

        void clickCheckBox(boolean isFirstCheckBox, boolean isSecondCheckBox, boolean isThirdCheckBox);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        Context getContext();
    }
}
