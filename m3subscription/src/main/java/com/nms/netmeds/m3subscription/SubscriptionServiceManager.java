package com.nms.netmeds.m3subscription;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseServiceManager;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.retrofit.BaseApiClient;
import com.nms.netmeds.m3subscription.model.RescheduleResponse;
import com.nms.netmeds.m3subscription.model.SkipOrderResponse;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaderResponse;
import com.nms.netmeds.m3subscription.model.UnSubscripeResponse;
import com.nms.netmeds.m3subscription.viewmodel.SubscriptionManageViewModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionServiceManager extends BaseServiceManager {

    private static SubscriptionServiceManager subscriptionServiceManager;

    public final static int C_MSTAR_SUBSCRIPTION_HEATER_DETAILS = 80000;
    public static final int C_MSTAR_M3_SKIP_ORDER = 80002;
    public static final int C_MSTAR_M3_UN_SUBSCRIBE_ORDER = 80003;
    public static final int C_MSTAR_M3_RESCHEDULE_ORDER = 80004;


    public static SubscriptionServiceManager getInstance() {
        if (subscriptionServiceManager == null)
            subscriptionServiceManager = new SubscriptionServiceManager();
        return subscriptionServiceManager;
    }

    public <T extends AppViewModel> void subscriptionDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitSubscriptionInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitSubscriptionInterface.class);
        Call<SubscriptionHeaderResponse> call = apiService.subscriptionHeaterDetails(mStarBasicHeaderMap);
        call.enqueue(new Callback<SubscriptionHeaderResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubscriptionHeaderResponse> call, @NonNull Response<SubscriptionHeaderResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_SUBSCRIPTION_HEATER_DETAILS);
                } else {
                    SubscriptionHeaderResponse subscriptionHeaderResponse = null;
                    if (response.errorBody() != null) {
                        subscriptionHeaderResponse = new Gson().fromJson(response.errorBody().charStream(), SubscriptionHeaderResponse.class);
                    }
                    if (subscriptionHeaderResponse != null && subscriptionHeaderResponse.getReason() != null && !TextUtils.isEmpty(subscriptionHeaderResponse.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(subscriptionHeaderResponse), C_MSTAR_SUBSCRIPTION_HEATER_DETAILS);
                    } else {
                        viewModel.onFailed(C_MSTAR_SUBSCRIPTION_HEATER_DETAILS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubscriptionHeaderResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_SUBSCRIPTION_HEATER_DETAILS, null);
            }
        });
    }


    public void getM3SkipOrder(final SubscriptionManageViewModel viewModel, Map<String, String> mStarBasicHeaderMap, String issueId, String primaryOrderId) {
        RetrofitSubscriptionInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitSubscriptionInterface.class);
        Call<SkipOrderResponse> call = apiService.skipOrder(mStarBasicHeaderMap, issueId, primaryOrderId);
        call.enqueue(new Callback<SkipOrderResponse>() {
            @Override
            public void onResponse(@NonNull Call<SkipOrderResponse> call, @NonNull Response<SkipOrderResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_M3_SKIP_ORDER);
                } else {
                    viewModel.onFailed(C_MSTAR_M3_SKIP_ORDER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<SkipOrderResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_M3_SKIP_ORDER, null);
            }
        });
    }

    public void getM3UnSubscribeOrder(final SubscriptionManageViewModel viewModel, Map<String, String> mStarBasicHeaderMap, String primaryOderId, String reason) {
        RetrofitSubscriptionInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitSubscriptionInterface.class);
        Call<UnSubscripeResponse> call = apiService.unSubscribeOrder(mStarBasicHeaderMap, primaryOderId, reason);
        call.enqueue(new Callback<UnSubscripeResponse>() {
            @Override
            public void onResponse(@NonNull Call<UnSubscripeResponse> call, @NonNull Response<UnSubscripeResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_M3_UN_SUBSCRIBE_ORDER);
                } else {
                    viewModel.onFailed(C_MSTAR_M3_UN_SUBSCRIBE_ORDER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<UnSubscripeResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_M3_UN_SUBSCRIBE_ORDER, null);
            }
        });
    }

    public void getM3Reschedule(final SubscriptionManageViewModel viewModel, Map<String, String> mStarBasicHeaderMap, String issueId, String primaryOrderId, int rescheduledDays, boolean isForAllOrder) {
        RetrofitSubscriptionInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitSubscriptionInterface.class);
        Call<RescheduleResponse> call = apiService.rescheduleOrder(mStarBasicHeaderMap, issueId, primaryOrderId, rescheduledDays, isForAllOrder);
        call.enqueue(new Callback<RescheduleResponse>() {
            @Override
            public void onResponse(@NonNull Call<RescheduleResponse> call, @NonNull Response<RescheduleResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_M3_RESCHEDULE_ORDER);
                } else {
                    viewModel.onFailed(C_MSTAR_M3_RESCHEDULE_ORDER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<RescheduleResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_M3_RESCHEDULE_ORDER, null);
            }
        });


    }
}
