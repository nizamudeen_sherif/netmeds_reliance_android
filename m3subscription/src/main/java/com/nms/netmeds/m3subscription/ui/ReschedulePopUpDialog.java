package com.nms.netmeds.m3subscription.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.databinding.ReschedulePopUpBinding;
import com.nms.netmeds.m3subscription.viewmodel.ReschedulePopUpViewModel;

@SuppressLint("ValidFragment")
public class ReschedulePopUpDialog extends BaseDialogFragment implements ReschedulePopUpViewModel.M3ReschedulePopUpListener {


    private ReschedulePopUpDialogListener listener;
    private ReschedulePopUpBinding binding;
    private int rescheduleTimeDuration;

    public ReschedulePopUpDialog(ReschedulePopUpDialogListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.reschedule_pop_up, container, false);
        binding.setViewModel(onCreateViewModel());
        return binding.getRoot();
    }

    private ReschedulePopUpViewModel onCreateViewModel() {
        if (getActivity() != null) {
            ReschedulePopUpViewModel reschedulePopUpViewModel = new ReschedulePopUpViewModel(getActivity().getApplication());
            reschedulePopUpViewModel.init(this);
            return reschedulePopUpViewModel;
        }
        return null;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    vmDismiss();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void vmDismiss() {
        SubscriptionHelper.getInstance().setOrderSuccessFalg(false);
        this.dismissAllowingStateLoss();
    }

    @Override
    public void onClickReschedule(boolean isSelect) {
        listener.onClickReschedule(rescheduleTimeDuration, isSelect);
    }


    private void setCheckBoxCondition() {
        if (getActivity() != null) {
            binding.fourText.setTypeface(CommonUtils.getTypeface(getActivity(), binding.firstCheckbox.isChecked() ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));
            binding.tenText.setTypeface(CommonUtils.getTypeface(getActivity(), binding.secondCheckbox.isChecked() ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));
            binding.fifteenText.setTypeface(CommonUtils.getTypeface(getActivity(), binding.thirdCheckbox.isChecked() ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));
        }
    }

    @Override
    public void enableContinueButton(boolean enable) {
        binding.reschedule.setBackground(enable ? getResources().getDrawable(R.drawable.accent_button) : getResources().getDrawable(R.drawable.secondary_button));
        binding.reschedule.setEnabled(enable);
    }

    @Override
    public void onClickFourDays() {
        binding.firstCheckbox.setChecked(true);
        binding.secondCheckbox.setChecked(false);
        binding.thirdCheckbox.setChecked(false);
        rescheduleTimeDuration = 4;
        setCheckBoxCondition();
        enableContinueButton(true);
        binding.executePendingBindings();
    }

    @Override
    public void onClickTenDays() {
        binding.secondCheckbox.setChecked(true);
        binding.firstCheckbox.setChecked(false);
        binding.thirdCheckbox.setChecked(false);
        rescheduleTimeDuration = 10;
        setCheckBoxCondition();
        enableContinueButton(true);
        binding.executePendingBindings();
    }

    @Override
    public void onClickFifteenDays() {
        binding.thirdCheckbox.setChecked(true);
        binding.secondCheckbox.setChecked(false);
        binding.firstCheckbox.setChecked(false);
        rescheduleTimeDuration = 15;
        enableContinueButton(true);
        setCheckBoxCondition();
        binding.executePendingBindings();
    }

    @Override
    public void onTypeCheckBox(boolean isCheck) {
        binding.checkBox.setChecked(isCheck);
    }

    public interface ReschedulePopUpDialogListener {
        void onClickReschedule(int rescheduleTime, boolean isSelect);
    }
}
