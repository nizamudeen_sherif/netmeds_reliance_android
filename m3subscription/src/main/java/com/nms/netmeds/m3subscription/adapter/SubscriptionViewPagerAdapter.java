package com.nms.netmeds.m3subscription.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nms.netmeds.m3subscription.model.TabDetails;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<TabDetails> tabs = new ArrayList<>();

    public SubscriptionViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    public void addFragment(TabDetails tab) {
        tabs.add(tab);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).getTabName();
    }
}

