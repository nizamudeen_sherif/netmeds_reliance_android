package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class M3OrderDetailsResponseModel implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private SubscriptionItemDetails result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SubscriptionItemDetails getResult() {
        return result;
    }

    public void setResult(SubscriptionItemDetails result) {
        this.result = result;
    }
}