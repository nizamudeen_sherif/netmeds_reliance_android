package com.nms.netmeds.m3subscription.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.adapter.SubscriptionHeaterAdapter;
import com.nms.netmeds.m3subscription.databinding.ActivitySubscriptionHeaterBinding;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaterDetail;
import com.nms.netmeds.m3subscription.ui.SubscriptionManageActivity;
import com.nms.netmeds.m3subscription.viewmodel.SubscriptionManageViewModel;
import com.nms.netmeds.m3subscription.viewmodel.SubscriptionTabFragmentViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubscriptionTabFragment extends BaseViewModelFragment implements SubscriptionTabFragmentViewModel.SubscriptionTabFragmentViewModelListener, SubscriptionHeaterAdapter.AdapterListerner {

    private static final String MONTH = "month";
    private ActivitySubscriptionHeaterBinding binding;
    private SubscriptionTabFragmentViewModel viewModel;

    public static SubscriptionTabFragment newInstance(String month) {
        SubscriptionTabFragment fragment = new SubscriptionTabFragment();
        Bundle args = new Bundle();
        args.putString(MONTH, month);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_subscription_heater, container, false);
        binding.setViewModel(onCreateViewModel());
        return binding.getRoot();
    }

    @Override
    protected SubscriptionTabFragmentViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(SubscriptionTabFragmentViewModel.class);
        subscriptionToastMessage();
        viewModel.initiate(getArguments().getString(MONTH), this, getBasePreference());
        return viewModel;
    }

    @Override
    public void showLoader() {
        showProgress(getActivity());
    }

    @Override
    public void dismissLoader() {
        dismissProgress();
    }

    @Override
    public void onResume() {
        super.onResume();
        SubscriptionHelper.getInstance().setCreateNewFillFlag(false);
    }


    @Override
    public void showErrorMessage(String errorMessage) {
        SnackBarHelper.snackBarCallBack(binding.recyclerview, getActivity(), errorMessage);
    }

    @Override
    public void showNetworkErrorView(boolean isShowNetWokErrorView) {
        binding.recyclerview.setVisibility(isShowNetWokErrorView ? View.VISIBLE : View.GONE);
        binding.networkErrorView.setVisibility(isShowNetWokErrorView ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isShowWebserviceErrorView) {
        binding.recyclerview.setVisibility(isShowWebserviceErrorView ? View.GONE : View.VISIBLE);
        binding.apiErrorView.setVisibility(isShowWebserviceErrorView ? View.VISIBLE : View.GONE);
    }

    @Override
    public void loadSubscriptionListAdapter(List<SubscriptionHeaterDetail> monthlySubscriptionList) {
        SubscriptionHeaterAdapter subscriptionHeaterAdapter = new SubscriptionHeaterAdapter(monthlySubscriptionList, getActivity(), this);
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerview.setAdapter(subscriptionHeaterAdapter);
        if (SubscriptionHelper.getInstance().getOrderSuccessFalg()) {
            binding.recyclerview.scrollToPosition(SubscriptionHelper.getInstance().getRecyclerviewPostion());
        }
    }

    @Override
    public void recyclerViewVisibilty(boolean isRecyclerView, boolean isCreateNewFill) {
        binding.recyclerview.setVisibility(isRecyclerView ? View.VISIBLE : View.GONE);
        binding.lvCreateNewFill.setVisibility(isCreateNewFill ? View.VISIBLE : View.GONE);
    }

    @Override
    public void clickCreateNewFill() {
        Intent intent = new Intent();
        BasePreference.getInstance(getActivity()).setDeliveryIntervalPreference(0);
        LaunchIntentManager.routeToActivity(getString(R.string.route_search), intent, getActivity());
        SubscriptionHelper.getInstance().setCreateNewFillFlag(true);
    }

    @Override
    public void vmCallbackManageActivity(String issueId, String primaryOrderId, Integer nmsCustomerId) {
        SubscriptionHelper.getInstance().setIssueId(issueId);
        SubscriptionHelper.getInstance().setOrderId(primaryOrderId);
        Intent intent = new Intent(getActivity(), SubscriptionManageActivity.class);
        intent.putExtra(IntentConstant.ISSUE_ID, issueId);
        intent.putExtra(IntentConstant.PRIMARY_ORDER_ID, primaryOrderId);
        intent.putExtra(IntentConstant.CUSTOMER_ID, nmsCustomerId);
        startActivity(intent);
    }

    @Override
    public void vmTrackOrder(String primaryOrderId) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.ORDER_ID, primaryOrderId);
        intent.putExtra(IntentConstant.FROM_SUBSCRIPTION, primaryOrderId);
        intent.putExtra(IntentConstant.TRACK_ORDER_POSITION, 0);
        LaunchIntentManager.routeToActivity(getString(R.string.route_track_order_detail), intent, getActivity());
    }

    private BasePreference getBasePreference() {
        return BasePreference.getInstance(getActivity());
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void navigateToAddAddress() {
        Intent intent = new Intent();
        LaunchIntentManager.routeToActivity(getString(R.string.route_address_activity), intent, getActivity());

    }

    @Override
    public void navigateCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, boolean isFromEditOrder, String orderId) {
        if (getActivity() != null) {
            Intent intent = new Intent();
            if (notAddedProducts != null && !notAddedProducts.isEmpty()) {
                RefillHelper.getOutOfStockProductsMap().putAll(notAddedProducts);
            }
            intent.putExtra(IntentConstant.ORDER_ID, orderId);
            LaunchIntentManager.routeToActivity(getString(R.string.route_netmeds_mstar_cart), intent, getActivity());
        }
    }

    @Override
    public void navigatePaymentActivity() {
        if (getActivity() != null) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstant.IS_FROM_PAYMENT, true);
            intent.putExtra(IntentConstant.FROM_PAYMENT_FAILURE, false);
            LaunchIntentManager.routeToActivity(getString(R.string.route_order_confirmation), intent, getActivity());
        }
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putStringArrayListExtra(IntentConstant.INTER_CITY_PRODUCT_LIST, interCityProductList);
        intent.putExtra(IntentConstant.IS_OUT_OF_STOCK, false);
        LaunchIntentManager.routeToActivity(getString(R.string.route_netmeds_mstar_cart), intent, getActivity());
    }

    @Override
    public void onLaunchPaynow(String issueId, String orderId, Integer customerId) {
        viewModel.onLaunchPaynow(issueId, orderId, customerId);
    }

    private void subscriptionToastMessage() {
        SubscriptionManageViewModel.setSubscriptionManageToast(new SubscriptionManageViewModel.subscriptionManageToast() {
            @Override
            public void toastMessage(String message) {
                SnackBarHelper.snackBarCallBack(binding.subscriptionHeaterViewContent, getActivity(), message);
            }
        });
    }
}