package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UnSubscripeResponse implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private UnSubscripeResult result;

    public UnSubscripeResult getResult() {
        return result;
    }

    public void setResult(UnSubscripeResult result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
