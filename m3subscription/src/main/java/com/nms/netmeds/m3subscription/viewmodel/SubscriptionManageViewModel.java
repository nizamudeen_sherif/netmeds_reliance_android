package com.nms.netmeds.m3subscription.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.OrderListenerTypeEnum;
import com.nms.netmeds.base.OrderPayNowRetryReorderHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.adapter.PastPrescriptionAdapter;
import com.nms.netmeds.base.model.DrugDetail;
import com.nms.netmeds.base.model.M3OrderDetailsResponseResultModel;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.OrderShippingInfo;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.SubscriptionServiceManager;
import com.nms.netmeds.m3subscription.model.RescheduleResponse;
import com.nms.netmeds.m3subscription.model.SkipOrderResponse;
import com.nms.netmeds.m3subscription.model.UnSubscripeResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionManageViewModel extends AppViewModel implements PastPrescriptionAdapter.UploadedPrescriptionListener, OrderPayNowRetryReorderHelper.OrderHelperCallback {

    private M3SubscriptionManageListener listener;
    private BasePreference basePreference;
    private M3OrderDetailsResponseResultModel subOrderDetailsResponse;
    private Intent getIntent;
    private final MutableLiveData<MStarBasicResponseTemplateModel> m3OrderDetailsMutableLiveData = new MutableLiveData<>();
    private String issueId;
    private String primaryOrderId;
    private int reschedule;
    private boolean selectRescheduleFlag;
    private int failedTransactionId;
    private Application application;
    private Map<String, MStarProductDetails> productCodeWithPriceMap = new HashMap<>();
    private List<DrugDetail> drugDetails;
    private boolean isProduct = false;
    private static subscriptionManageToast subscriptionManageToast;
    private String payNow = "";

    public SubscriptionManageViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public void init(M3SubscriptionManageListener listener, Intent intent, BasePreference basePreference) {
        this.listener = listener;
        this.getIntent = intent;
        this.basePreference = basePreference;
        getIntentValue();
        if (!TextUtils.isEmpty(issueId) && !TextUtils.isEmpty(primaryOrderId))
            init();
    }

    public static void setSubscriptionManageToast(SubscriptionManageViewModel.subscriptionManageToast subscriptionManageToast) {
        SubscriptionManageViewModel.subscriptionManageToast = subscriptionManageToast;
    }

    private void getIntentValue() {
        if (getIntent != null && getIntent.getExtras() != null) {
            if (getIntent.hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
                ArrayList<String> intentParamList = (ArrayList<String>) getIntent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
                primaryOrderId = intentParamList != null && !intentParamList.isEmpty() ? intentParamList.get(0) : "";
                issueId = intentParamList != null && intentParamList.size() > 1 ? intentParamList.get(1) : "";
                payNow = intentParamList != null && intentParamList.size() > 2 ? intentParamList.get(2) : "";
            } else {
                issueId = getIntent.getStringExtra(IntentConstant.ISSUE_ID);
                primaryOrderId = getIntent.getStringExtra(IntentConstant.PRIMARY_ORDER_ID);
            }
            SubscriptionHelper.getInstance().setIssueId(issueId);
            SubscriptionHelper.getInstance().setOrderId(primaryOrderId);
            if (payNow.equalsIgnoreCase(AppConstant.M3_PAYNOW_ORDER_STATUS)) {
                listener.buttonDisable();
                onClickPayNow();
            }
        }
    }

    private void init() {
        initiateAPICall(APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS);
    }

    public boolean isUnSubscribe() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getSubscriptionStatus() != null && !IntentConstant.M3_UNSUBSCRIBE_ORDER_STATUS.equalsIgnoreCase(subOrderDetailsResponse.getSubscriptionStatus());
    }

    public boolean isSkip() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getIsSkipped() != null && subOrderDetailsResponse.getIsSkipped();
    }

    public boolean isEditOrder() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getIsEditable() != null && subOrderDetailsResponse.getIsEditable();
    }

    public boolean isReschedule() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getIsReschedule() != null && subOrderDetailsResponse.getIsReschedule();
    }

    public String getDeliveredBy() {
        return (subOrderDetailsResponse != null && subOrderDetailsResponse.getIssueDate() != null && !TextUtils.isEmpty(subOrderDetailsResponse.getIssueDate())) ? DateTimeUtils.getInstance().stringDate(subOrderDetailsResponse.getIssueDate(), DateTimeUtils.yyyyMMdd, DateTimeUtils.EMMMdd) : "";
    }

    public String paymentStatus() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getPaymentButtonStatus() != null && !TextUtils.isEmpty(subOrderDetailsResponse.getPaymentButtonStatus()) ? subOrderDetailsResponse.getPaymentButtonStatus() : "";
    }

    public String patientName() {
        return (subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getPatientName())) ? getValidString(subOrderDetailsResponse.getPatientName()) : "";
    }

    public String getOrderDateLabel() {
        String issOrderId = (subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getIssueOrderId())) ? subOrderDetailsResponse.getIssueOrderId() : "";
        return !TextUtils.isEmpty(issOrderId) ? listener.getContext().getString(R.string.text_order_placed) : listener.getContext().getString(R.string.text_issue_date);
    }

    public String getSubOrderLabelName() {
        String issOrderId = (subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getIssueOrderId())) ? subOrderDetailsResponse.getIssueOrderId() : "";
        return !TextUtils.isEmpty(issOrderId) ? listener.getContext().getString(R.string.text_order_id) : listener.getContext().getString(R.string.text_subscription_issue_id);
    }

    public String getSubscriptionWithIssueId() {
        String subscriptionId = (subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getSubscriptionId())) ? subOrderDetailsResponse.getSubscriptionId() : "";
        String issOrderId = (subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getIssueOrderId())) ? subOrderDetailsResponse.getIssueOrderId() : "";
        return !TextUtils.isEmpty(issOrderId) ? issOrderId : !TextUtils.isEmpty(subscriptionId) && !TextUtils.isEmpty(issueId) ? String.format(listener.getContext().getString(R.string.text_sub_with_issue_id_value), subscriptionId, issueId) : !TextUtils.isEmpty(subscriptionId) && TextUtils.isEmpty(issueId) ? subscriptionId : issueId;
    }

    public String getOrderPlacedDateAndTime() {
        SubscriptionHelper.getInstance().setIssueOrderDate(subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getOrderPlacedDate()) ? DateTimeUtils.getInstance().stringDate(subOrderDetailsResponse.getOrderPlacedDate(), DateTimeUtils.yyyyMMddThhmmss, DateTimeUtils.MMMddyyyyhhmma) : "");
        return (subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getOrderPlacedDate())) ? DateTimeUtils.getInstance().stringDate(subOrderDetailsResponse.getOrderPlacedDate(), DateTimeUtils.yyyyMMddThhmmss, DateTimeUtils.MMMddyyyyhhmma) : "";
    }

    public String getCouponDiscount() {
        return (subOrderDetailsResponse != null && subOrderDetailsResponse.getDiscount() != null && Double.valueOf(subOrderDetailsResponse.getDiscount().replace(",", "")) > 0) ? CommonUtils.getPriceInFormat(CommonUtils.getIndianCurrencyFormat(subOrderDetailsResponse.getDiscount())) : "";
    }

    public String getPaymentMode() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getPaymentMode() != null && !TextUtils.isEmpty(subOrderDetailsResponse.getPaymentMode()) ? subOrderDetailsResponse.getPaymentMode() : "";
    }

    public String getDeliveryAddress() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getShippingAddress() != null ? getFormatDeliveryAddress(subOrderDetailsResponse.getShippingAddress()) : "";
    }

    public void unSubscribeOrder() {
        initiateAPICall(SubscriptionServiceManager.C_MSTAR_M3_UN_SUBSCRIBE_ORDER);
    }

    public int getPrescriptionVisibility() {
        return subOrderDetailsResponse != null && subOrderDetailsResponse.getRxId() != null && subOrderDetailsResponse.getRxId().size() != 0 ? View.VISIBLE : View.GONE;
    }

    public String getContactPersonName() {
        return String.format("%s %s", getValidString(String.valueOf(subOrderDetailsResponse != null && subOrderDetailsResponse.getShippingAddress() != null && !TextUtils.isEmpty(subOrderDetailsResponse.getShippingAddress().getShippingFirstName()) ? subOrderDetailsResponse.getShippingAddress().getShippingFirstName() : "")), getValidString(String.valueOf(subOrderDetailsResponse != null && subOrderDetailsResponse.getShippingAddress() != null && !TextUtils.isEmpty(subOrderDetailsResponse.getShippingAddress().getShippingLastName()) ? subOrderDetailsResponse.getShippingAddress().getShippingLastName() : "")));
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getM3OrderDetailsMutableData() {
        return m3OrderDetailsMutableLiveData;
    }

    public void skipOrder() {
        initiateAPICall(SubscriptionServiceManager.C_MSTAR_M3_SKIP_ORDER);
    }

    public void rescheduleOrder(int rescheduleTime, boolean isSelect) {
        reschedule = rescheduleTime;
        selectRescheduleFlag = isSelect;
        initiateAPICall(SubscriptionServiceManager.C_MSTAR_M3_RESCHEDULE_ORDER);
    }

    public void onRescheduleClick() {
        listener.onRescheduleClick();
    }

    public void onCancelOrderClick() {
        listener.onCancelOrderClick();
    }

    public void onClickEditOrder() {
        SubscriptionHelper.getInstance().setSubscriptionEdit(true);
        SubscriptionHelper.getInstance().setPayNowSubscription(false);
        initiateAPICall(APIServiceManager.MSTAR_SUBSCRIPTION_PAYNOW);
    }

    public void onClickTrackOrder() {
        listener.navigateTrackOrder(getSubscriptionWithIssueId());
    }

    public void onClickPayNow() {
        SubscriptionHelper.getInstance().setIssueId(issueId);
        SubscriptionHelper.getInstance().setOrderId(primaryOrderId);
        SubscriptionHelper.getInstance().setPayNowSubscription(true);
        SubscriptionHelper.getInstance().setSubscriptionEdit(false);
        new OrderPayNowRetryReorderHelper(application).initiate(this, "", OrderListenerTypeEnum.SUBSCRIPTION_PAY_NOW, basePreference);
    }

    public String getOrderId() {
        return primaryOrderId;
    }

    public void onSkipOrderClick() {
        listener.onSkipOrderClick();
    }

    private void initiateAPICall(int apiCallId) {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        listener.showNoNetworkView(isConnected);
        if (isConnected) {
            listener.showProgressLoader();
            switch (apiCallId) {
                case APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS:
                    listener.showAndHideContentView(false);
                    APIServiceManager.getInstance().getM3OrderDetails(this, basePreference.getMstarBasicHeaderMap(), issueId, getOrderId());
                    break;
                case SubscriptionServiceManager.C_MSTAR_M3_SKIP_ORDER:
                    SubscriptionServiceManager.getInstance().getM3SkipOrder(this, basePreference.getMstarBasicHeaderMap(), issueId, getOrderId());
                    break;
                case SubscriptionServiceManager.C_MSTAR_M3_UN_SUBSCRIBE_ORDER:
                    SubscriptionServiceManager.getInstance().getM3UnSubscribeOrder(this, basePreference.getMstarBasicHeaderMap(), getIntent.getBooleanExtra(IntentConstant.SUBSCRIPTION_CANCEL_FLAG, false) ? subOrderDetailsResponse != null && !TextUtils.isEmpty(subOrderDetailsResponse.getPrimaryOrderId()) ? subOrderDetailsResponse.getPrimaryOrderId() : "" : getOrderId(), listener.getContext().getString(R.string.text_unsub_reason));
                    break;
                case SubscriptionServiceManager.C_MSTAR_M3_RESCHEDULE_ORDER:
                    SubscriptionServiceManager.getInstance().getM3Reschedule(this, basePreference.getMstarBasicHeaderMap(), issueId, getOrderId(), reschedule, selectRescheduleFlag);
                    break;
                case APIServiceManager.MSTAR_SUBSCRIPTION_PAYNOW:
                    APIServiceManager.getInstance().subscriptionPayNow(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarCustomerId(), SubscriptionHelper.getInstance().getOrderId(), SubscriptionHelper.getInstance().getIssueId());
                    break;
                case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                    APIServiceManager.getInstance().mStarProductDetailsForIdList(this, getProductList(), APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.dismissProgressLoader();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS:
                m3OrderDetailsResponse(data);
                break;
            case SubscriptionServiceManager.C_MSTAR_M3_SKIP_ORDER:
                skipOrderAfterResponse(data);
                break;
            case SubscriptionServiceManager.C_MSTAR_M3_UN_SUBSCRIBE_ORDER:
                unSubscribeOrderResponse(data);
                break;
            case SubscriptionServiceManager.C_MSTAR_M3_RESCHEDULE_ORDER:
                rescheduleOrderResponse(data);
                break;
            case APIServiceManager.MSTAR_SUBSCRIPTION_PAYNOW:
                onSubscriptionPayNowResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                onNotAddedProductDetailsAvailable(data);
                break;

        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.dismissProgressLoader();
        if (transactionId == APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS) {
            listener.navigationActivity();
        } else listener.showWebserviceErrorView(true);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        listener.showWebserviceErrorView(false);
        initiateAPICall(failedTransactionId);
    }

    private void m3OrderDetailsResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getStatus().equalsIgnoreCase(IntentConstant.SUCCESS) && response.getResult() != null && response.getResult().getSubscriptinItemDetails() != null) {
            listener.showAndHideContentView(true);
            m3OrderDetailsMutableLiveData.setValue(response);
            if (response.getResult().getSubscriptinItemDetails().getRxId() != null && response.getResult().getSubscriptinItemDetails().getRxId().size() > 0)
                listener.uploadPrescriptionAdapter(response.getResult().getSubscriptinItemDetails().getRxId());
            if (response.getResult().getSubscriptinItemDetails().getRxId() != null && response.getResult().getSubscriptinItemDetails().getLineItems().size() > 0)
                drugDetails = response.getResult().getSubscriptinItemDetails().getLineItems();
            initiateAPICall(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
        } else {
            listener.onBackPressCallBack();
        }
    }

    private void skipOrderAfterResponse(String data) {
        SkipOrderResponse response = new Gson().fromJson(data, SkipOrderResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(IntentConstant.SUCCESS) && response.getResult() != null) {
            SubscriptionHelper.getInstance().setOrderSuccessFalg(true);
            listener.onSkipOrderFromResponse();
            if (listener != null && listener.getContext() != null)
                subscriptionManageToast.toastMessage(listener.getContext().getString(R.string.text_skip_subscription));
        }
    }

    private void unSubscribeOrderResponse(String data) {
        UnSubscripeResponse response = new Gson().fromJson(data, UnSubscripeResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(IntentConstant.SUCCESS) && response.getResult() != null && response.getResult().getAstatus() != null) {
            SubscriptionHelper.getInstance().setOrderSuccessFalg(true);
            listener.onUnSubscribeOrderFromResponse();
            if (listener != null && listener.getContext() != null)
                subscriptionManageToast.toastMessage(listener.getContext().getString(R.string.text_un_subscription));
        }
    }

    private void rescheduleOrderResponse(String data) {
        RescheduleResponse response = new Gson().fromJson(data, RescheduleResponse.class);
        if (response != null && response.getStatus() != null && response.getStatus().equalsIgnoreCase(IntentConstant.SUCCESS) && response.getResult() != null) {
            SubscriptionHelper.getInstance().setOrderSuccessFalg(true);
            listener.onRescheduleOrderFromResponse();
            if (listener != null && listener.getContext() != null)
                subscriptionManageToast.toastMessage(listener.getContext().getString(R.string.text_rescheduled_subscription));

        }

    }

    private void onSubscriptionPayNowResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && response.getResult().getAddedProductsResultMap() != null && !response.getResult().getAddedProductsResultMap().isEmpty() && response.getResult().getCartDetails() != null) {
                SubscriptionHelper.getInstance().setSubscriptionCartId(response.getResult().getCartDetails().getId());
                if (isProduct) {
                    isProduct = false;
                    listener.navigateProductDetailActivity();
                } else
                    listener.navigateCartActivity();
            }
        }
    }

    private void onNotAddedProductDetailsAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && !response.getResult().getProductDetailsList().isEmpty() && response.getResult().getProductDetailsList().size() > 0) {
                setOrderItemListAdapter();
                BigDecimal totalAmount = BigDecimal.valueOf(0.0);
                if (drugDetails != null && !drugDetails.isEmpty()) {
                    for (int i = 0; i < response.getResult().getProductDetailsList().size(); i++) {
                        MStarProductDetails productDetails = response.getResult().getProductDetailsList().get(i);
                        productCodeWithPriceMap.put(String.valueOf(productDetails.getProductCode()), productDetails);
                    }
                    for (DrugDetail drugDetail : drugDetails) {
                        if (productCodeWithPriceMap.containsKey(String.valueOf(drugDetail.getDrugCode())))
                            totalAmount = totalAmount.add(productCodeWithPriceMap.get(String.valueOf(drugDetail.getDrugCode())).getSellingPrice().multiply(BigDecimal.valueOf(drugDetail.getPurchaseQuantity())));
                    }
                }
                listener.totalAmount(totalAmount);
            }
        }
    }

    public void initiatePageUsingOrderDetails(MStarBasicResponseTemplateModel responseModel) {
        subOrderDetailsResponse = responseModel.getResult().getSubscriptinItemDetails();
    }


    private void setOrderItemListAdapter() {
        if (subOrderDetailsResponse != null && subOrderDetailsResponse.getLineItems().size() > 0)
            listener.orderListAdapter(subOrderDetailsResponse.getLineItems(), productCodeWithPriceMap);

    }

    private String getValidString(String str) {
        return !TextUtils.isEmpty(str) ? str.substring(0, 1).toUpperCase() + str.substring(1) : "";
    }

    private String getFormatDeliveryAddress(OrderShippingInfo orderShippingInfo) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(orderShippingInfo.getShippingAddress1()).append(",");
        if (!TextUtils.isEmpty(orderShippingInfo.getShippingAddress2())) {
            stringBuilder.append("\n");
            stringBuilder.append(orderShippingInfo.getShippingAddress2()).append(",");
        }
        stringBuilder.append("\n");
        stringBuilder.append(orderShippingInfo.getShippingCity()).append(" - ").append(orderShippingInfo.getShippingZipCode()).append(", ").append(orderShippingInfo.getShippingState()).append(".");
        stringBuilder.append("\n");
        stringBuilder.append(String.format(listener.getContext().getString(R.string.text_state_code), orderShippingInfo.getShippingPhoneNumber()));
        return stringBuilder.toString();
    }

    private String getProductList() {
        String productCodeListString = "";
        if (drugDetails != null && drugDetails.size() > 0) {
            StringBuilder productCodeList = new StringBuilder();
            for (DrugDetail productCode : drugDetails) {
                productCodeList.append(productCode.getDrugCode()).append(",");
            }
            productCodeListString = productCodeList.toString();
        }
        return productCodeListString.substring(0, productCodeListString.length() - 1);
    }

    public void initPayNowApiCall(boolean isProduct) {
        this.isProduct = isProduct;
        onClickEditOrder();
    }

    @Override
    public void prescriptionSelected(String Id) {
        listener.previewPrescription(Id);
    }

    @Override
    public void removePrescription(String id) {

    }

    @Override
    public void launchPreviewPrescription(String url) {
        listener.previewPrescription(url);
    }

    @Override
    public void setAlert() {

    }

    @Override
    public int getPrescriptionCount() {
        return 0;
    }

    @Override
    public void navigateToAddAddress() {
        listener.navigateAddressActivity();
    }

    @Override
    public void redirectToPayment() {
        listener.navigateConfirmationActivity();
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> productsResultMap, List<String> maxLimitBreachProducts, boolean isCart, String orderId) {
        listener.navigateToCartActivity(productsResultMap, isCart, orderId);
    }

    @Override
    public void hideLoader() {
        listener.dismissProgressLoader();
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(listener.getContext());
    }

    @Override
    public void showNoNetworkError(boolean isConnected) {


    }

    @Override
    public void showLoader() {
        listener.showProgressLoader();
    }

    @Override
    public void showCancelOrderError(String s) {

    }

    @Override
    public void onFailedFromHelper(int transactionId, String data, boolean isFromHelper) {

    }

    @Override
    public void cancelOrderSuccessful(String message) {

    }

    @Override
    public void navigateToSuccessPage(String data) {

    }

    @Override
    public void navigateToCodFailure(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct) {

    }

    @Override
    public void showMessage(String message) {
        listener.showErrorMessage(message);
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        listener.navigateToCartForColdStorage(interCityProductList);
    }

    @Override
    public Context getContext() {
        return listener.getContext();
    }


    public interface M3SubscriptionManageListener {
        void showProgressLoader();

        void dismissProgressLoader();

        void onBackPressCallBack();

        void showErrorMessage(String errorMessage);

        void onRescheduleClick();

        void onCancelOrderClick();

        void onSkipOrderClick();

        void onRescheduleOrderFromResponse();

        void onUnSubscribeOrderFromResponse();

        void onSkipOrderFromResponse();

        void previewPrescription(String str);

        void snackBarMessage(String message);

        Context getContext();

        void orderListAdapter(List<DrugDetail> drugDetails, Map<String, MStarProductDetails> productCodeWithPriceMap);

        void showWebserviceErrorView(boolean isShowWebserviceErrorView);

        void showNoNetworkView(boolean isShowNoNetworkView);

        void showAndHideContentView(boolean isShowAndHideContentView);

        void uploadPrescriptionAdapter(List<String> rxId);

        void navigateCartActivity();

        void navigateAddressActivity();

        void totalAmount(BigDecimal totalAmount);

        void navigateToCartActivity(Map<String, MStarAddedProductsResult> productsResultMap, boolean isCart, String orderId);

        void navigateProductDetailActivity();

        void navigateConfirmationActivity();

        void navigateToCartForColdStorage(ArrayList<String> interCityProductList);

        void navigateTrackOrder(String primaryOrderId);

        void navigationActivity();

        void buttonDisable();
    }

    public interface subscriptionManageToast {
        void toastMessage(String string);

    }
}
