package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

public class UnSubscripeResult {
    @SerializedName("astatus")
    private String astatus;
    @SerializedName("subscribedDate")
    private String subscribedDate;

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }

    public String getSubscribedDate() {
        return subscribedDate;
    }

    public void setSubscribedDate(String subscribedDate) {
        this.subscribedDate = subscribedDate;
    }
}
