package com.nms.netmeds.m3subscription.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.databinding.ActivityM3DeliveryDateIntervalBinding;
import com.nms.netmeds.m3subscription.viewmodel.M3DeliverDateIntervalViewModel;


public class M3DeliveryDateIntervalActivity extends BaseViewModelActivity<M3DeliverDateIntervalViewModel> implements M3DeliverDateIntervalViewModel.M3DeliverDateIntervalListener, CancelAndSkipPopUpDialog.CancelAndSkipPopUpDialogListener {

    private ActivityM3DeliveryDateIntervalBinding binding;
    private boolean isRefillFromBanner = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_m3_delivery_date_interval);
        binding.setViewModel(onCreateViewModel());
        toolBarSetUp(binding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        initToolBar();
    }

    @Override
    protected M3DeliverDateIntervalViewModel onCreateViewModel() {
        M3DeliverDateIntervalViewModel viewModel = ViewModelProviders.of(this).get(M3DeliverDateIntervalViewModel.class);
        viewModel.init(this);
        isRefillFromBanner = getIntent().getBooleanExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, false);
        return viewModel;
    }

    private void initToolBar() {
        toolBarSetUp(binding.toolbar);
        binding.collapsingToolbar.setTitle(getString(R.string.text_schedule_delivery));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public void showLoader() {
        showHorizontalProgressBar(false, binding.deliveryIntervalProgressBar); // while loading the view should be non clickable
    }

    @Override
    public void dismissLoader() {
        showHorizontalProgressBar(true, binding.deliveryIntervalProgressBar);// after loading the view should be clickable
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void vmCallbackSubscriptionDeliverDateActivity(int rescheduleTimeDuration) {
        Intent intent = new Intent();
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, PaymentHelper.getSingle_address());
        intent.putExtra(SubscriptionIntentConstant.TIME_DURATION, rescheduleTimeDuration);
        intent.putExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, isRefillFromBanner);
        LaunchIntentManager.routeToActivity(getString(R.string.route_order_confirmation), intent, this);
    }

    @Override
    public void onBackPressed() {
        customBackPressAction();
    }

    private void openAlertForExitSubscription() {
        CancelAndSkipPopUpDialog cancelAndSkipPopUpDialog = new CancelAndSkipPopUpDialog(this, true, true);
        getSupportFragmentManager().beginTransaction().add(cancelAndSkipPopUpDialog, "cancelPopUpDialog").commitAllowingStateLoss();
    }

    @Override
    public Intent getIntentFromActivity() {
        return getIntent();
    }

    @Override
    public ActivityM3DeliveryDateIntervalBinding binding() {
        return binding;
    }

    @Override
    public void clickCheckBox(boolean isFirstCheckBox, boolean isSecondCheckBox, boolean isThirdCheckBox) {
        binding.firstCheckbox.setChecked(isFirstCheckBox);
        binding.secondCheckbox.setChecked(isSecondCheckBox);
        binding.thirdCheckbox.setChecked(isThirdCheckBox);
        binding.executePendingBindings();

        binding.thirtyFiveText.setTypeface(CommonUtils.getTypeface(getApplication().getApplicationContext(), binding.firstCheckbox.isChecked() ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));
        binding.fortyFiveText.setTypeface(CommonUtils.getTypeface(getApplication().getApplicationContext(), binding.secondCheckbox.isChecked() ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));
        binding.sixtyFiveText.setTypeface(CommonUtils.getTypeface(getApplication().getApplicationContext(), binding.thirdCheckbox.isChecked() ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        binding.deliveryDateViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.tvButton.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.deliveryDateViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.tvButton.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onYesClick(boolean isCancelOrder) {
        M3DeliveryDateIntervalActivity.this.finish();
    }

    private void customBackPressAction() {
        if (isRefillFromBanner) {
            openAlertForExitSubscription();
        } else {
            M3DeliveryDateIntervalActivity.this.finish();
        }
    }
}
