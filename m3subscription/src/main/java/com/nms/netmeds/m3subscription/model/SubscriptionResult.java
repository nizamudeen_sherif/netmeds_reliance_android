package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionResult {
    @SerializedName("monthName")
    private String monthName;
    @SerializedName("subscriptionDetails")
    private List<SubscriptionHeaterDetail> subscriptionDetails = null;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public List<SubscriptionHeaterDetail> getSubscriptionDetails() {
        return subscriptionDetails;
    }

    public void setSubscriptionDetails(List<SubscriptionHeaterDetail> subscriptionDetails) {
        this.subscriptionDetails = subscriptionDetails;
    }


}
