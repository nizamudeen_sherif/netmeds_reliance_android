package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

public class EditRefillSubscriptionRequest {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("issueId")
    private String issueId;
    @SerializedName("refillDays")
    private String refillDays;
    @SerializedName("editOrderFlag")
    private String editOrderFlag;
    @SerializedName("updatePrimaryOrder")
    private String updatePrimaryOrder;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getRefillDays() {
        return refillDays;
    }

    public void setRefillDays(String refillDays) {
        this.refillDays = refillDays;
    }

    public String getEditOrderFlag() {
        return editOrderFlag;
    }

    public void setEditOrderFlag(String editOrderFlag) {
        this.editOrderFlag = editOrderFlag;
    }

    public String getUpdatePrimaryOrder() {
        return updatePrimaryOrder;
    }

    public void setUpdatePrimaryOrder(String updatePrimaryOrder) {
        this.updatePrimaryOrder = updatePrimaryOrder;
    }
}
