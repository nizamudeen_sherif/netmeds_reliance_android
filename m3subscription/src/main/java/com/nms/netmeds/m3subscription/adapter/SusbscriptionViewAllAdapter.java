package com.nms.netmeds.m3subscription.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.databinding.SubscriptionViewAllItemBinding;

import java.util.List;

public class SusbscriptionViewAllAdapter extends RecyclerView.Adapter<SusbscriptionViewAllAdapter.SubscriptionViewHolder> {

    private Context context;
    private List<String> item;
    private List<String> displayItem;

    public SusbscriptionViewAllAdapter(Context context, List<String> item) {
        this.context = context;
        this.item = item;
        this.displayItem = item;
        updateDisplayItem(false);
    }

    @NonNull
    @Override
    public SubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        SubscriptionViewAllItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.subscription_view_all_item, parent, false);
        return new SubscriptionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionViewHolder holder, int position) {
        final String text = getVIewAllLessText();
        holder.itemBinding.productTitle.setText(item.get(position));
        holder.itemBinding.tvViewLess.setText(text);
        holder.itemBinding.tvViewLess.setVisibility(getItemCount() == position + 1 && !TextUtils.isEmpty(text) ? View.VISIBLE : View.GONE);
        holder.itemBinding.tvViewLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDisplayItem(context.getString(R.string.text_view_all).equals(text));
            }
        });
    }

    private void updateDisplayItem(boolean isViewMore) {
        displayItem = isViewMore ? item : item.subList(0, item.size() > 2 ? 2 : item.size());
        notifyDataSetChanged();
    }

    private String getVIewAllLessText() {
        return item.size() <= 2 ? "" : context.getString(item.size() > displayItem.size() ? R.string.text_view_all : R.string.text_view_less);
    }

    @Override
    public int getItemCount() {
        return displayItem.size();
    }


    class SubscriptionViewHolder extends RecyclerView.ViewHolder {
        private final SubscriptionViewAllItemBinding itemBinding;

        SubscriptionViewHolder(final SubscriptionViewAllItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }

}
