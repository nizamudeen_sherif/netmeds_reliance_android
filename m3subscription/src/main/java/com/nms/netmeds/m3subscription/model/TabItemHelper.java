package com.nms.netmeds.m3subscription.model;

public class TabItemHelper {
    private static TabItemHelper item;
    private SubscriptionHeaderResponse subscriptionHeaderResponse;

    public static TabItemHelper getInstance() {
        if (item == null)
            item = new TabItemHelper();
        return item;
    }

    public SubscriptionHeaderResponse getSubscriptionHeaderResponse() {
        return subscriptionHeaderResponse;
    }

    public void setSubscriptionHeaderResponse(SubscriptionHeaderResponse subscriptionHeaderResponse) {
        this.subscriptionHeaderResponse = subscriptionHeaderResponse;
    }
}
