package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UnSubscripeRequest implements Serializable {

    @SerializedName("primaryOrderId")
    @Expose
    private String primaryOrderId;
    @SerializedName("reason")
    @Expose
    private String reason;

    public String getPrimaryOrderId() {
        return primaryOrderId;
    }

    public void setPrimaryOrderId(String primaryOrderId) {
        this.primaryOrderId = primaryOrderId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
