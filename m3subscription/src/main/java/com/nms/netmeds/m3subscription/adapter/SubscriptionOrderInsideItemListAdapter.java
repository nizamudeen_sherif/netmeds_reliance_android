package com.nms.netmeds.m3subscription.adapter;


import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.databinding.AdapterOrderItemListBinding;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DrugDetail;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.BasePreference;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubscriptionOrderInsideItemListAdapter extends RecyclerView.Adapter<SubscriptionOrderInsideItemListAdapter.ItemListViewHolder> {

    private final Context context;
    private final List<DrugDetail> detailsList;
    private final boolean isM2Order;
    private final boolean isOrderDelivered;
    private Map<String, MStarProductDetails> productCodeWithPriceMap;
    private SubscriptionOrderInsideItemListAdapterCallback listAdapterCallback;

    public SubscriptionOrderInsideItemListAdapter(Context context, List<DrugDetail> detailsList, boolean isM2Order, boolean isOrderDelivered,
                                                  Map<String, MStarProductDetails> productCodeWithPriceMap, SubscriptionOrderInsideItemListAdapterCallback listAdapterCallback) {
        this.context = context;
        this.detailsList = detailsList;
        this.isM2Order = isM2Order;
        this.isOrderDelivered = isOrderDelivered;
        this.productCodeWithPriceMap = productCodeWithPriceMap;
        this.listAdapterCallback = listAdapterCallback;
    }

    @NonNull
    @Override
    public SubscriptionOrderInsideItemListAdapter.ItemListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AdapterOrderItemListBinding listBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_order_item_list, viewGroup, false);
        return new SubscriptionOrderInsideItemListAdapter.ItemListViewHolder(listBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionOrderInsideItemListAdapter.ItemListViewHolder holder, int position) {
        final DrugDetail drugDetail = detailsList.get(position);
        MStarProductDetails mStarProductDetails = productCodeWithPriceMap.get(String.valueOf(drugDetail.getDrugCode()));
        holder.listBinding.tvDrugName.setText(drugDetail.getBrandName());
        holder.listBinding.tvAlgoliaPrice.setText(CommonUtils.getPriceInFormat(mStarProductDetails.getSellingPrice().multiply(BigDecimal.valueOf(drugDetail.getPurchaseQuantity()))));
        holder.listBinding.tvStrikePrice.setText(CommonUtils.getPriceInFormat(mStarProductDetails.getMrp().multiply(BigDecimal.valueOf(drugDetail.getPurchaseQuantity()))));
        holder.listBinding.tvStrikePrice.setPaintFlags(holder.listBinding.tvStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.listBinding.tlMrp.setVisibility(isDiscountAvailable(mStarProductDetails.getDiscount()) ? View.VISIBLE : View.GONE);

        holder.listBinding.trQuantity.setVisibility(isPrimeProduct(drugDetail.getDrugCode()) ? View.GONE : View.VISIBLE);
        holder.listBinding.imgProductImage.setImageDrawable(ContextCompat.getDrawable(context, isPrimeProduct(drugDetail.getDrugCode()) ? isOrderDelivered ? R.drawable.ic_super_member : R.drawable.ic_crown_circle : R.drawable.ic_no_image));
        holder.listBinding.tvDrugQuantity.setText(getBoldFormatQuantity(drugDetail.getPurchaseQuantity()));
        holder.listBinding.imgRxImage.setVisibility(mStarProductDetails.isRxRequired() ? View.VISIBLE : View.GONE);
        holder.listBinding.divider.setVisibility((isM2Order && (getItemCount() - 1 == position)) ? View.GONE : View.VISIBLE);
        holder.listBinding.lvParentList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAdapterCallback.navigateProductDetail(drugDetail.getDrugCode(), true);
            }
        });
    }

    private boolean isPrimeProduct(long drugCode) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        List<String> primeCodeList = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode().size() > 0 ? configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode() : new ArrayList<String>();
        return primeCodeList.contains(String.valueOf(drugCode));
    }

    private boolean isDiscountAvailable(BigDecimal discount) {
        return discount.compareTo(BigDecimal.ZERO) > 0;
    }

    @Override
    public int getItemCount() {
        return detailsList.size();
    }

    private SpannableString getBoldFormatQuantity(int productQuantity) {
        String quantity = String.valueOf(productQuantity);
        String totalString = String.format(context.getString(R.string.text_quantity_with_count), quantity);
        SpannableString resultString = new SpannableString(totalString);
        resultString.setSpan(new StyleSpan(Typeface.BOLD), (totalString.length() - quantity.length()), totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return resultString;
    }

    class ItemListViewHolder extends RecyclerView.ViewHolder {

        private final AdapterOrderItemListBinding listBinding;

        ItemListViewHolder(@NonNull AdapterOrderItemListBinding listBinding) {
            super(listBinding.getRoot());
            this.listBinding = listBinding;
        }
    }

    public interface SubscriptionOrderInsideItemListAdapterCallback {
        void navigateProductDetail(int drugCode, boolean isProduct);
    }
}
