package com.nms.netmeds.m3subscription.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.SubscriptionBannersList;
import com.nms.netmeds.m3subscription.R;

import java.util.List;

public class OnBoardingAdapterSubscription extends RecyclerView.Adapter<OnBoardingAdapterSubscription.MyViewHolder> {
    private Context context;
    private OnboardingAdapterListener onboardingAdapterListener;
    private List<SubscriptionBannersList> onBoarding;

    public OnBoardingAdapterSubscription(Context context, List<SubscriptionBannersList> subscriptionBannersList, OnboardingAdapterListener onboardingAdapterListener) {
        this.onBoarding = subscriptionBannersList;
        this.context = context;
        this.onboardingAdapterListener = onboardingAdapterListener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView close;
        private TextView description;
        private TextView title;
        private ImageView imageView;

        MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            close = view.findViewById(R.id.close_icon);
            imageView = view.findViewById(R.id.image);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_onboard_item_subscription, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        SubscriptionBannersList subscriptionBannersList = onBoarding.get(position);
        holder.title.setText(subscriptionBannersList.getTitle());
        holder.description.setText(subscriptionBannersList.getDescription());
        holder.title.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
        float density = context.getResources().getDisplayMetrics().density;
        Glide.with(context).load(subscriptionBannersList.getImage()).apply(new RequestOptions().override(Math.round(330 * density), Math.round(290 * density)))
                .into(holder.imageView);
        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onboardingAdapterListener.vmCallbackFinish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return onBoarding.size();
    }

    public interface OnboardingAdapterListener {

        void vmCallbackFinish();
    }
}
