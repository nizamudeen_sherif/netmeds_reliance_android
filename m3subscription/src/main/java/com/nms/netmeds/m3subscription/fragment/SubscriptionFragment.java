package com.nms.netmeds.m3subscription.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.SubscriptionServiceManager;
import com.nms.netmeds.m3subscription.adapter.SubscriptionViewPagerAdapter;
import com.nms.netmeds.m3subscription.databinding.FragmentSubscriptionBinding;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaderResponse;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaterDetail;
import com.nms.netmeds.m3subscription.model.SubscriptionResult;
import com.nms.netmeds.m3subscription.model.TabDetails;
import com.nms.netmeds.m3subscription.viewmodel.SubscriptionFragmentViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SubscriptionFragment extends BaseViewModelFragment<SubscriptionFragmentViewModel> implements SubscriptionFragmentViewModel.Listener {

    FragmentSubscriptionBinding binding;
    private SubscriptionFragmentViewModel subscriptionFragmentViewModel;
    private int position;
    private List<SubscriptionHeaterDetail> subscriptionDetails;
    private List<SubscriptionResult> results;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_subscription, container, false);
        binding.setViewModel(onCreateViewModel());
        SubscriptionHelper.getInstance().setOrderSuccessFalg(false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected SubscriptionFragmentViewModel onCreateViewModel() {
        subscriptionFragmentViewModel = ViewModelProviders.of(this).get(SubscriptionFragmentViewModel.class);
        subscriptionFragmentViewModel.init(this, basePreference());
        onRetry(subscriptionFragmentViewModel, binding.getRoot());
        SubscriptionHelper.getInstance().setSubscriptionFlag(true);
        viewPagerOnchangeListener();
        subscriptionFragmentViewModel.getSubscriptionDetailResponseMutableLiveData().observe(this, new SubscriptionDetailObserver());
        return subscriptionFragmentViewModel;
    }

    @Override
    public void loadViewpagerWithTab(SubscriptionHeaderResponse subscriptionHeaderResponse) {
        if (getActivity() != null && subscriptionHeaderResponse != null && subscriptionHeaderResponse.getResult() != null && subscriptionHeaderResponse.getResult().size() > 0) {
            List<String> tab = new ArrayList<>();
            SubscriptionViewPagerAdapter SubscriptionViewPagerAdapter = new SubscriptionViewPagerAdapter(getChildFragmentManager());
            for (SubscriptionResult result : subscriptionHeaderResponse.getResult()) {
                String name = result.getMonthName().substring(0, 3).toUpperCase();
                if (!tab.contains(name)) {
                    tab.add(name);
                    TabDetails tabDetails = new TabDetails(name, SubscriptionTabFragment.newInstance(name));
                    SubscriptionViewPagerAdapter.addFragment(tabDetails);
                }
            }
            binding.viewpager.setAdapter(SubscriptionViewPagerAdapter);
            Calendar calendar = Calendar.getInstance();
            boolean monthflag = false;
            binding.tabs.setupWithViewPager(binding.viewpager);
            binding.tabs.setTabTextColors(getResources().getColor(R.color.colorLightBlueGrey), getResources().getColor(R.color.colorDarkBlueGrey));
            if (SubscriptionHelper.getInstance().getOrderSuccessFalg()) {
                binding.viewpager.setCurrentItem(SubscriptionHelper.getInstance().getTabPosition());
            } else {
                String calendarMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                for (int i = 0; i < subscriptionHeaderResponse.getResult().size(); i++) {
                    String currentString = subscriptionHeaderResponse.getResult().get(i).getMonthName();
                    String[] splitString = currentString.split("-");
                    if (calendarMonth.equals(splitString[0])) {
                        if (!monthflag) {
                            monthflag = true;
                            results = subscriptionHeaderResponse.getResult();
                            position = i;
                            binding.viewpager.setCurrentItem(i);
                            subscriptionDetails = subscriptionHeaderResponse.getResult().get(i).getSubscriptionDetails();
                            if (subscriptionDetails.size() == 0) {
                                binding.tvCreateSubscription.setVisibility(View.GONE);
                                SubscriptionHelper.getInstance().setThisIsIssueOrder(true);
                            } else {
                                binding.tvCreateSubscription.setVisibility(View.VISIBLE);
                                SubscriptionHelper.getInstance().setThisIsIssueOrder(false);
                            }
                        }
                    }
                }
            }
        }
    }

    private void viewPagerOnchangeListener() {
        binding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                SubscriptionHelper.getInstance().setTabPosition(i);
                if (subscriptionDetails != null && subscriptionDetails.size() == 0) {
                    binding.tvCreateSubscription.setVisibility(View.GONE);
                    SubscriptionHelper.getInstance().setThisIsIssueOrder(true);
                } else if (position == i) {
                    binding.tvCreateSubscription.setVisibility(View.VISIBLE);
                    SubscriptionHelper.getInstance().setThisIsIssueOrder(false);
                } else {
                    binding.tvCreateSubscription.setVisibility(View.GONE);
                    SubscriptionHelper.getInstance().setThisIsIssueOrder(true);
                }
                if (results != null && results.size() > 0) {
                    if (i == results.size() - 1) {
                        SubscriptionHelper.getInstance().setLastMonthIssueOrderFlag(true);
                    } else {
                        SubscriptionHelper.getInstance().setLastMonthIssueOrderFlag(false);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (SubscriptionHelper.getInstance().getOrderSuccessFalg()) {
            subscriptionFragmentViewModel.initApiCall(SubscriptionServiceManager.C_MSTAR_SUBSCRIPTION_HEATER_DETAILS);

        }
    }

    @Override
    public void vmShowProgress() {
        showProgress(getActivity());
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void showWebserviceErrorView(boolean isShowWebserviceErrorView) {
        binding.fragmentSubscriptionViewContent.setVisibility(isShowWebserviceErrorView ? View.GONE : View.VISIBLE);
        binding.apiErrorView.setVisibility(isShowWebserviceErrorView ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNoNetworkView(boolean isShowNoNetworkView) {
        binding.fragmentSubscriptionViewContent.setVisibility(isShowNoNetworkView ? View.VISIBLE : View.GONE);
        binding.networkErrorView.setVisibility(isShowNoNetworkView ? View.GONE : View.VISIBLE);
    }

    @Override
    public void hideCreateNewFill(List<SubscriptionResult> result) {
        binding.lvCreateNewFill.setVisibility(result != null && !result.isEmpty() ? View.GONE : View.VISIBLE);
        binding.viewpager.setVisibility(result != null && !result.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void visibleCreateNewFill() {
        binding.rlViewpager.setVisibility(View.GONE);
        binding.lvCreateNewFill.setVisibility(View.VISIBLE);
    }


    private class SubscriptionDetailObserver implements Observer<SubscriptionHeaderResponse> {
        @Override
        public void onChanged(@Nullable SubscriptionHeaderResponse subscriptionHeaderResponse) {
            subscriptionFragmentViewModel.SubscriptionDetailAvailable(subscriptionHeaderResponse);
            binding.setViewModel(subscriptionFragmentViewModel);
        }
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void navigateCartActivity() {
        Intent intent = new Intent();
        if (getActivity() != null)
            LaunchIntentManager.routeToActivity(getString(R.string.route_search), intent, getActivity());
    }

    private BasePreference basePreference() {
        return BasePreference.getInstance(getActivity());
    }
}


