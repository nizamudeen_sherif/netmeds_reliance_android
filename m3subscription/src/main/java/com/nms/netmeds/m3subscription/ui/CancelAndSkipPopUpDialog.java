package com.nms.netmeds.m3subscription.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.databinding.CancelSkipPopUpBinding;
import com.nms.netmeds.m3subscription.viewmodel.CancelAndSkipPopUpDialogViewModel;

@SuppressLint("ValidFragment")
public class CancelAndSkipPopUpDialog extends BaseDialogFragment implements CancelAndSkipPopUpDialogViewModel.CancelAndSkipPopUpDialogViewModelListener {

    private CancelAndSkipPopUpDialogListener listener;
    private boolean isFromCancelOrder;
    private boolean isFromExitSubscription;

    public CancelAndSkipPopUpDialog(CancelAndSkipPopUpDialogListener listener, boolean isFromCancelOrder, boolean isFromExitSubscription) {
        this.listener = listener;
        this.isFromCancelOrder = isFromCancelOrder;
        this.isFromExitSubscription = isFromExitSubscription;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        CancelSkipPopUpBinding binding = DataBindingUtil.inflate(inflater, R.layout.cancel_skip_pop_up, container, false);
        binding.setViewModel(onCreateViewModel());
        return binding.getRoot();
    }

    private CancelAndSkipPopUpDialogViewModel onCreateViewModel() {
        CancelAndSkipPopUpDialogViewModel viewModel = ViewModelProviders.of(this).get(CancelAndSkipPopUpDialogViewModel.class);
        viewModel.init(this, isFromCancelOrder, isFromExitSubscription);
        return viewModel;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    onNoClick();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void onNoClick() {
        SubscriptionHelper.getInstance().setOrderSuccessFalg(false);
        this.dismissAllowingStateLoss();
    }

    @Override
    public void onYesClick(boolean isFromCancelOrder) {
        this.dismissAllowingStateLoss();
        listener.onYesClick(isFromCancelOrder);
    }

    interface CancelAndSkipPopUpDialogListener {
        void onYesClick(boolean isCancelOrder);
    }
}
