package com.nms.netmeds.m3subscription;

import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.m3subscription.model.RescheduleResponse;
import com.nms.netmeds.m3subscription.model.SkipOrderResponse;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaderResponse;
import com.nms.netmeds.m3subscription.model.UnSubscripeResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface RetrofitSubscriptionInterface {

    @GET(SubscriptionAPIPathConstant.C_MSTAR_SUBSCRIPTION_HEATER_DETAILS)
    Call<SubscriptionHeaderResponse> subscriptionHeaterDetails(@HeaderMap Map<String, String> header);

    @FormUrlEncoded
    @POST(SubscriptionAPIPathConstant.C_MSTAR_SKIP_ORDER)
    Call<SkipOrderResponse> skipOrder(@HeaderMap Map<String, String> header, @Field(AppConstant.SUBSCRIPTION_ISSUE_ID) String issueId, @Field(AppConstant.SUBSCRIPTION_PRIMARY_ORDER_ID) String primaryOderId);

    @FormUrlEncoded
    @POST(SubscriptionAPIPathConstant.C_MSTAR_UN_SUBSCRIBE_ORDER)
    Call<UnSubscripeResponse> unSubscribeOrder(@HeaderMap Map<String, String> header, @Field(AppConstant.SUBSCRIPTION_PRIMARY_ORDER_ID) String primaryOrderId, @Field(AppConstant.REASON) String reason);

    @FormUrlEncoded
    @POST(SubscriptionAPIPathConstant.C_MSTAR_RESCHEDULE_ORDER)
    Call<RescheduleResponse> rescheduleOrder(@HeaderMap Map<String, String> header, @Field(AppConstant.SUBSCRIPTION_ISSUE_ID) String issueId, @Field(AppConstant.SUBSCRIPTION_PRIMARY_ORDER_ID) String primaryOderId, @Field(AppConstant.SUBSCRIPTION_RESCHEDULED_DAYS) int rescheduledDays, @Field(AppConstant.SUBSCRIPTION_IS_FOR_ALL_ORDER) boolean isForAllOrder);
}