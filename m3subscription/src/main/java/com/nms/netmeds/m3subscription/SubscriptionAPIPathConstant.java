package com.nms.netmeds.m3subscription;

public class SubscriptionAPIPathConstant {
    static final String C_MSTAR_SUBSCRIPTION_HEATER_DETAILS = "api/v1/subscription/getsubscriptionmainorders";
    static final String C_MSTAR_SKIP_ORDER = "api/v1/subscription/subscriptionskiporder";
    static final String C_MSTAR_UN_SUBSCRIBE_ORDER = "api/v1/subscription/unsubscribeorders";
    static final String C_MSTAR_RESCHEDULE_ORDER = "api/v1/subscription/subscriptionrescheduled";
}
