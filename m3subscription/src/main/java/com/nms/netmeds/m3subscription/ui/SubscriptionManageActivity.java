package com.nms.netmeds.m3subscription.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.DrugDetail;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.adapter.PrescriptionAdpater;
import com.nms.netmeds.m3subscription.adapter.SubscriptionOrderInsideItemListAdapter;
import com.nms.netmeds.m3subscription.databinding.ActivityManageSubscriptionBinding;
import com.nms.netmeds.m3subscription.viewmodel.SubscriptionManageViewModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubscriptionManageActivity extends BaseViewModelActivity<SubscriptionManageViewModel> implements SubscriptionManageViewModel.M3SubscriptionManageListener,
        CancelAndSkipPopUpDialog.CancelAndSkipPopUpDialogListener, ReschedulePopUpDialog.ReschedulePopUpDialogListener, PrescriptionAdpater.PrescriptionAdapterCallback, SubscriptionOrderInsideItemListAdapter.SubscriptionOrderInsideItemListAdapterCallback {

    private ActivityManageSubscriptionBinding binding;
    private SubscriptionManageViewModel viewModel;
    private int productCode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_manage_subscription);
        SubscriptionHelper.getInstance().setPayNowSubscription(false);
        onCreateViewModel();
        initToolBar();


    }

    @Override
    protected SubscriptionManageViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(SubscriptionManageViewModel.class);
        viewModel.init(this, getIntent(), basePreference());
        viewModel.getM3OrderDetailsMutableData().observe(this, new M3OrderDetailsObserver());
        onRetry(viewModel);
        return viewModel;
    }

    private void initToolBar() {
        toolBarSetUp(binding.toolbar);
        binding.collapsingToolbar.setTitle(getString(R.string.text_manage_refill));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }


    @Override
    public void showProgressLoader() {
        showHorizontalProgressBar(false, binding.subscriptionManageProgressBar); // while loading the view should be non clickable
    }

    @Override
    public void dismissProgressLoader() {
        showHorizontalProgressBar(true, binding.subscriptionManageProgressBar);// after loading the view should be clickable
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        SnackBarHelper.snackBarCallBack(binding.bodyView, this, errorMessage);
    }

    @Override
    public void onBackPressCallBack() {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.HOME_FLAG_FROM_SUBSCRIPTION_ORDER_PLACED_SUCCESSFUL, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        LaunchIntentManager.routeToActivity(getString(R.string.route_navigation_activity), intent, this);
    }

    @Override
    public void onRescheduleClick() {
        ReschedulePopUpDialog reschedulePopUpDialog = new ReschedulePopUpDialog(this);
        getSupportFragmentManager().beginTransaction().add(reschedulePopUpDialog, "ReschedulePopUpDialog").commitAllowingStateLoss();
    }

    @Override
    public void onCancelOrderClick() {
        CancelAndSkipPopUpDialog cancelAndSkipPopUpDialog = new CancelAndSkipPopUpDialog(this, true, false);
        getSupportFragmentManager().beginTransaction().add(cancelAndSkipPopUpDialog, "cancelPopUpDialog").commitAllowingStateLoss();
    }

    @Override
    public void onSkipOrderClick() {
        CancelAndSkipPopUpDialog cancelAndSkipPopUpDialog = new CancelAndSkipPopUpDialog(this, false, false);
        getSupportFragmentManager().beginTransaction().add(cancelAndSkipPopUpDialog, "skipPopUpDialog").commitAllowingStateLoss();
    }

    @Override
    public void onYesClick(boolean isCancelOrder) {
        if (isCancelOrder) {
            viewModel.unSubscribeOrder();
        } else {
            viewModel.skipOrder();
        }
    }

    @Override
    public void onClickReschedule(int rescheduleTime, boolean isSelect) {
        viewModel.rescheduleOrder(rescheduleTime, isSelect);
    }

    private BasePreference basePreference() {
        return BasePreference.getInstance(this);
    }

    @Override
    public void onRescheduleOrderFromResponse() {
        finish();
    }

    @Override
    public void onUnSubscribeOrderFromResponse() {
        finish();
    }

    @Override
    public void onSkipOrderFromResponse() {
        finish();
    }

    @Override
    public void snackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(binding.subscriptionManageViewContent, getApplication(), message);

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void orderListAdapter(List<DrugDetail> drugDetails, Map<String, MStarProductDetails> productCodeWithPriceMap) {
        binding.rvOrderItemList.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOrderItemList.setNestedScrollingEnabled(false);
        binding.rvOrderItemList.setAdapter(new SubscriptionOrderInsideItemListAdapter(this, drugDetails, false, false, productCodeWithPriceMap, this));

    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
        showAndHideContentView(!isWebserviceError);

    }

    @Override
    public void showNoNetworkView(boolean isShowNoNetworkView) {
        showAndHideContentView(isShowNoNetworkView);
        binding.networkErrorView.setVisibility(isShowNoNetworkView ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showAndHideContentView(boolean isShowAndHideContentView) {
        binding.bodyView.setVisibility(isShowAndHideContentView ? View.VISIBLE : View.GONE);
    }

    @Override
    public void uploadPrescriptionAdapter(List<String> rxId) {
        PrescriptionAdpater attachPrescriptionAdapter = new PrescriptionAdpater(rxId, this);
        binding.rvPrescription.setLayoutManager(setLayoutManager());
        binding.rvPrescription.setAdapter(attachPrescriptionAdapter);
        binding.rvPrescription.setLayoutManager(setLayoutManager());
        binding.rvPrescription.setAdapter(attachPrescriptionAdapter);
    }

    @Override
    public void navigateCartActivity() {
        Intent intent = new Intent();
        LaunchIntentManager.routeToActivity(getString(R.string.route_netmeds_mstar_cart), intent, this);
    }

    @Override
    public void navigateAddressActivity() {
        Intent intent = new Intent();
        LaunchIntentManager.routeToActivity(getString(R.string.route_address_activity), intent, this);
    }

    @Override
    public void totalAmount(BigDecimal totalAmount) {
        binding.totalAmount.setText(CommonUtils.getPriceInFormat(totalAmount));
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> productsResultMap, boolean isCart, String orderId) {
        Intent intent = new Intent();
        if (productsResultMap != null && !productsResultMap.isEmpty()) {
            RefillHelper.getOutOfStockProductsMap().putAll(productsResultMap);
        }
        intent.putExtra(IntentConstant.ORDER_ID, orderId);
        LaunchIntentManager.routeToActivity(getString(R.string.route_netmeds_mstar_cart), intent, this);
    }

    @Override
    public void navigateProductDetailActivity() {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
        LaunchIntentManager.routeToActivity(getString(R.string.route_product_detail), intent, this);
    }

    @Override
    public void navigateConfirmationActivity() {
        binding.btnSkip.setEnabled(true);
        binding.btnCancel.setEnabled(true);
        binding.payNow.setEnabled(true);
        binding.btnReschedule.setEnabled(true);
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.IS_FROM_PAYMENT, true);
        intent.putExtra(IntentConstant.FROM_PAYMENT_FAILURE, false);
        LaunchIntentManager.routeToActivity(getString(R.string.route_order_confirmation), intent, this);

    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putStringArrayListExtra(IntentConstant.INTER_CITY_PRODUCT_LIST, interCityProductList);
        intent.putExtra(IntentConstant.IS_OUT_OF_STOCK, false);
        LaunchIntentManager.routeToActivity(getString(R.string.route_netmeds_mstar_cart), intent, this);
    }

    @Override
    public void navigateTrackOrder(String primaryOrderId) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.ORDER_ID, primaryOrderId);
        intent.putExtra(IntentConstant.FROM_SUBSCRIPTION, primaryOrderId);
        intent.putExtra(IntentConstant.TRACK_ORDER_POSITION, 0);
        LaunchIntentManager.routeToActivity(getString(R.string.route_track_order_detail), intent, this);
        this.finish();
    }

    @Override
    public void navigationActivity() {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        LaunchIntentManager.routeToActivity(getString(R.string.route_navigation_activity), intent, this);
    }

    @Override
    public void buttonDisable() {
        binding.btnSkip.setEnabled(false);
        binding.btnCancel.setEnabled(false);
        binding.payNow.setEnabled(false);
        binding.btnReschedule.setEnabled(false);
    }

    private LinearLayoutManager setLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    @Override
    public void previewPrescription(String url) {
        if (getSupportFragmentManager().findFragmentByTag("ImagePreviewDialog") == null) {
            ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(url);
            getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitAllowingStateLoss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PaymentHelper.setItemInsideOrderMap(null);
        PaymentHelper.setIsIncompleteOrder(false);
        PaymentHelper.setInCompleteOrderId("");
        BasePreference.getInstance(this).setOutOfStockProductMessage("");
        SubscriptionHelper.getInstance().setEditorderquantityflag(false);
    }

    @Override
    public void navigateProductDetail(int drugCode, boolean isProduct) {
        viewModel.initPayNowApiCall(isProduct);
        this.productCode = drugCode;
    }

    private class M3OrderDetailsObserver implements Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel responseModel) {
            viewModel.initiatePageUsingOrderDetails(responseModel);
            binding.setViewModel(viewModel);
        }
    }


}
