package com.nms.netmeds.m3subscription.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.SubscriptionServiceManager;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaderResponse;
import com.nms.netmeds.m3subscription.model.SubscriptionResult;
import com.nms.netmeds.m3subscription.model.TabItemHelper;

import java.util.List;

public class SubscriptionFragmentViewModel extends AppViewModel {
    private Listener listener;
    private final MutableLiveData<SubscriptionHeaderResponse> SubscriptionDetailResponseMutableLiveData = new MutableLiveData<>();
    private BasePreference basePreference;

    public SubscriptionFragmentViewModel(@NonNull Application application) {
        super(application);

    }

    public MutableLiveData<SubscriptionHeaderResponse> getSubscriptionDetailResponseMutableLiveData() {
        return SubscriptionDetailResponseMutableLiveData;
    }

    public void init(Listener listener, BasePreference basePreference) {
        this.listener = listener;
        this.basePreference = basePreference;
        listener.vmShowProgress();
        initApiCall(SubscriptionServiceManager.C_MSTAR_SUBSCRIPTION_HEATER_DETAILS);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        listener.vmShowProgress();
        listener.showWebserviceErrorView(false);
        initApiCall(SubscriptionServiceManager.C_MSTAR_SUBSCRIPTION_HEATER_DETAILS);
    }

    public void onClickCreateNewFill() {
        listener.vmShowProgress();
        initApiCall(APIServiceManager.MSTAR_SUBSCRIPTION_CREATE_CART);
    }

    public void initApiCall(int transactionID) {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        listener.showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionID) {
                case APIServiceManager.MSTAR_SUBSCRIPTION_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateSubscriptionCart(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case SubscriptionServiceManager.C_MSTAR_SUBSCRIPTION_HEATER_DETAILS:
                    SubscriptionServiceManager.getInstance().subscriptionDetails(this, basePreference.getMstarBasicHeaderMap());
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        listener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_SUBSCRIPTION_CREATE_CART:
                createMStarCartResponse(data);
                break;
            case SubscriptionServiceManager.C_MSTAR_SUBSCRIPTION_HEATER_DETAILS:
                subscriptionResponseData(data);
                break;
        }
    }

    private void subscriptionResponseData(String data) {
        SubscriptionHeaderResponse response = new Gson().fromJson(data, SubscriptionHeaderResponse.class);
        if (response != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng())) {
            listener.visibleCreateNewFill();
        } else if (response != null && response.getStatus() != null && SubscriptionIntentConstant.API_SUCCESS.equalsIgnoreCase(response.getStatus())) {
            SubscriptionDetailResponseMutableLiveData.setValue(response);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.vmDismissProgress();
        listener.showWebserviceErrorView(true);
    }


    public void SubscriptionDetailAvailable(SubscriptionHeaderResponse subscriptionHeaderResponse) {
        if (subscriptionHeaderResponse != null) {
            TabItemHelper.getInstance().setSubscriptionHeaderResponse(subscriptionHeaderResponse);
            listener.loadViewpagerWithTab(subscriptionHeaderResponse);
            listener.hideCreateNewFill(subscriptionHeaderResponse.getResult());
        }
    }

    private void createMStarCartResponse(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            SubscriptionHelper.getInstance().setSubscriptionCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            basePreference.setDeliveryIntervalPreference(0);
            SubscriptionHelper.getInstance().setCreateNewFillFlag(true);
            SubscriptionHelper.getInstance().setPayNowSubscription(false);
            SubscriptionHelper.getInstance().setSubscriptionEdit(false);
            listener.navigateCartActivity();
        }
    }

    public interface Listener {

        void loadViewpagerWithTab(SubscriptionHeaderResponse subscriptionHeaderResponse);

        void vmShowProgress();

        void vmDismissProgress();

        void showWebserviceErrorView(boolean isShowWebserviceErrorView);

        void showNoNetworkView(boolean isShowNoNetworkView);

        void hideCreateNewFill(List<SubscriptionResult> result);

        void visibleCreateNewFill();

        Context getContext();

        void navigateCartActivity();
    }

}
