package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionHeaterDetail {
    @SerializedName("primaryOrderId")
    private String primaryOrderId;
    @SerializedName("issueOrderId")
    private String issueOrderId;
    @SerializedName("issueId")
    private String issueId;
    @SerializedName("name")
    private String name;
    @SerializedName("purchasedDate")
    private String purchasedDate;
    @SerializedName("orderAmount")
    private Float orderAmount;
    @SerializedName("subscriptionStatus")
    private String subscriptionStatus;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("statusDescription")
    private String statusDescription;
    @SerializedName("paymentStatus")
    private String paymentStatus;
    @SerializedName("issueDate")
    private String issueDate;
    @SerializedName("monthName")
    private String monthName;
    @SerializedName("statusColor")
    private String statusColor;
    @SerializedName("itemCount")
    private Integer itemCount;
    @SerializedName("nmsCustomerId")
    private Integer nmsCustomerId;
    @SerializedName("magentoCustomerId")
    private Integer magentoCustomerId;
    @SerializedName("item")
    private List<String> item = null;

    public String getPrimaryOrderId() {
        return primaryOrderId;
    }

    public void setPrimaryOrderId(String primaryOrderId) {
        this.primaryOrderId = primaryOrderId;
    }

    public String getIssueOrderId() {
        return issueOrderId;
    }

    public void setIssueOrderId(String issueOrderId) {
        this.issueOrderId = issueOrderId;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public Float getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Float orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public Integer getNmsCustomerId() {
        return nmsCustomerId;
    }

    public void setNmsCustomerId(Integer nmsCustomerId) {
        this.nmsCustomerId = nmsCustomerId;
    }

    public Integer getMagentoCustomerId() {
        return magentoCustomerId;
    }

    public void setMagentoCustomerId(Integer magentoCustomerId) {
        this.magentoCustomerId = magentoCustomerId;
    }

    public List<String> getItem() {
        return item;
    }

    public void setItem(List<String> item) {
        this.item = item;
    }

}
