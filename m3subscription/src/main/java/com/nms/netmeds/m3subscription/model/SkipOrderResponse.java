package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkipOrderResponse implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
