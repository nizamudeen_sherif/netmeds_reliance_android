package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.M3OrderDetailsResponseResultModel;

public class SubscriptionItemDetails {
    @SerializedName("itemDetails")
    private M3OrderDetailsResponseResultModel result;

    public M3OrderDetailsResponseResultModel getItemResult() {
        return result;
    }

    public void setResult(M3OrderDetailsResponseResultModel result) {
        this.result = result;
    }
}
