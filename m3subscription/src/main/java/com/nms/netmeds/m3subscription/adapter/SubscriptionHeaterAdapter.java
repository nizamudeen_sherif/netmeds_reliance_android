package com.nms.netmeds.m3subscription.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.databinding.FragmentSubscriptionHeaterItemBinding;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaterDetail;

import java.util.List;

public class SubscriptionHeaterAdapter extends RecyclerView.Adapter<SubscriptionHeaterAdapter.SubscriptionViewHolder> {

    private List<SubscriptionHeaterDetail> subscriptionHeaterDetailsList;
    private AdapterListerner adapterListerner;
    private Context context;

    public SubscriptionHeaterAdapter(List<SubscriptionHeaterDetail> item, Context context, AdapterListerner adapterListerner) {
        this.subscriptionHeaterDetailsList = item;
        this.adapterListerner = adapterListerner;
        this.context = context;
    }

    @NonNull
    @Override
    public SubscriptionHeaterAdapter.SubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FragmentSubscriptionHeaterItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_subscription_heater_item, parent, false);
        return new SubscriptionHeaterAdapter.SubscriptionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubscriptionHeaterAdapter.SubscriptionViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final SubscriptionHeaterDetail subscriptionDetails = subscriptionHeaterDetailsList.get(position);
        holder.binding.date.setText(subscriptionDetails != null && !TextUtils.isEmpty(subscriptionDetails.getIssueDate()) ? DateTimeUtils.getInstance().stringDate(subscriptionDetails.getIssueDate(), DateTimeUtils.yyyyMMddTHHmmss, "dd") : "");
        holder.binding.month.setText(getMonth(subscriptionDetails));
        holder.binding.name.setText(subscriptionDetails != null && !TextUtils.isEmpty(subscriptionDetails.getName()) ? subscriptionDetails.getName() : "");
        holder.binding.orderStatus.setTextColor(ContextCompat.getColor(context, getColorCode(subscriptionDetails)));
        holder.binding.orderStatus.setText(subscriptionDetails != null && !TextUtils.isEmpty(subscriptionDetails.getStatusDescription()) ? subscriptionDetails.getStatusDescription() : "");
        holder.binding.llNavigationButton.setVisibility(getNavigationButtonVisibility(subscriptionDetails));

        initiateItemList(holder.binding, subscriptionDetails);
        setNavigateButtonProperties(holder, subscriptionDetails);

        holder.binding.tvManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubscriptionHelper.getInstance().setOrderSuccessFalg(false);
                SubscriptionHelper.getInstance().setCreateNewFillFlag(false);
                SubscriptionHelper.getInstance().setRecyclerviewPostion(position);
                adapterListerner.vmCallbackManageActivity(subscriptionDetails.getIssueId(), subscriptionDetails.getPrimaryOrderId(), subscriptionDetails.getNmsCustomerId());
            }
        });

        holder.binding.tvButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubscriptionHelper.getInstance().setOrderSuccessFalg(false);
                SubscriptionHelper.getInstance().setCreateNewFillFlag(false);
                if (SubscriptionIntentConstant.PAYMENT_STATUS_PAYNOW.equalsIgnoreCase(subscriptionDetails.getPaymentStatus())) {
                    adapterListerner.onLaunchPaynow(subscriptionDetails.getIssueId(), subscriptionDetails.getPrimaryOrderId(), subscriptionDetails.getNmsCustomerId());
                } else {
                    SubscriptionHelper.getInstance().setIssueId(subscriptionDetails.getIssueId());
                    adapterListerner.vmTrackOrder(subscriptionDetails.getIssueOrderId());
                }
            }
        });
    }

    private void initiateItemList(FragmentSubscriptionHeaterItemBinding binding, SubscriptionHeaterDetail subscriptionDetails) {
        SusbscriptionViewAllAdapter adapter = new SusbscriptionViewAllAdapter(context, subscriptionDetails.getItem());
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerview.setNestedScrollingEnabled(false);
        binding.recyclerview.setAdapter(adapter);
    }

    private int getColorCode(SubscriptionHeaterDetail subscriptionHeaterDetail) {
        String status = subscriptionHeaterDetail != null && !TextUtils.isEmpty(subscriptionHeaterDetail.getStatusDescription()) ? subscriptionHeaterDetail.getStatusDescription() : "";
        int colorCode = R.color.colorSquash;
        if (!TextUtils.isEmpty(status)) {
            if (SubscriptionIntentConstant.CONFIRMATION_PENDING.equalsIgnoreCase(status) || SubscriptionIntentConstant.PRESCRIPTION_VERIFICATION_PENDING.equalsIgnoreCase(status)) {
                colorCode = R.color.colorSquash;
            } else if (SubscriptionIntentConstant.OUT_OF_DELIVERY.equalsIgnoreCase(status) || SubscriptionIntentConstant.DELIVERED.equalsIgnoreCase(status) || SubscriptionIntentConstant.PROCESSING.equalsIgnoreCase(status)) {
                colorCode = R.color.colorGreen;
            } else if (SubscriptionIntentConstant.PAYMENT_PENDING.equalsIgnoreCase(status)) {
                colorCode = R.color.colorMediumPink;
            } else if (SubscriptionIntentConstant.UNSUBSCRIBED.equalsIgnoreCase(status) || SubscriptionIntentConstant.PENDING_DELIVERY.equalsIgnoreCase(status) || SubscriptionIntentConstant.SKIPPED.equalsIgnoreCase(status)) {
                colorCode = R.color.colorDarkGrey;
            }
        }
        return colorCode;
    }

    private int getNavigationButtonVisibility(SubscriptionHeaterDetail subscriptionHeaterDetail) {
        return subscriptionHeaterDetail != null && !TextUtils.isEmpty(subscriptionHeaterDetail.getOrderStatus()) && !TextUtils.isEmpty(subscriptionHeaterDetail.getSubscriptionStatus()) ?
                (!(subscriptionHeaterDetail.getOrderStatus().equalsIgnoreCase(SubscriptionIntentConstant.ORDER_STATUS_X)) && subscriptionHeaterDetail.getSubscriptionStatus().equalsIgnoreCase(SubscriptionIntentConstant.SUBSCRIPTION_STATUS_ACTIVE)) ? View.VISIBLE : View.GONE
                : View.GONE;
    }

    private void setNavigateButtonProperties(SubscriptionViewHolder holder, SubscriptionHeaterDetail subscriptionHeaterDetail) {
        String paymentStatus = subscriptionHeaterDetail != null && !TextUtils.isEmpty(subscriptionHeaterDetail.getPaymentStatus()) ? subscriptionHeaterDetail.getPaymentStatus() : "";
        boolean isPaymentStatusPending = SubscriptionIntentConstant.PAYMENT_STATUS_PENDING.equalsIgnoreCase(paymentStatus);
        boolean isButton2Enable = SubscriptionIntentConstant.PAYMENT_STATUS_TRACK.equalsIgnoreCase(paymentStatus) || SubscriptionIntentConstant.PAYMENT_STATUS_PAYNOW.equalsIgnoreCase(paymentStatus);

        holder.binding.tvManage.setClickable(!isPaymentStatusPending);
        holder.binding.tvManage.setEnabled(!isPaymentStatusPending);
        holder.binding.tvButton2.setClickable(isButton2Enable);
        holder.binding.tvButton2.setEnabled(isButton2Enable);

        holder.binding.tvButton2.setText(context.getString(SubscriptionIntentConstant.PAYMENT_STATUS_PAYNOW.equalsIgnoreCase(paymentStatus) ? R.string.text_pay_now :
                SubscriptionIntentConstant.PAYMENT_STATUS_TRACK.equalsIgnoreCase(paymentStatus) || SubscriptionIntentConstant.PAYMENT_STATUS_SUBSCRIPTION_PENDING.equalsIgnoreCase(paymentStatus) ? R.string.text_track_order :
                        R.string.text_pay_now));

        holder.binding.tvButton2.setBackground(ContextCompat.getDrawable(context, isButton2Enable ? R.drawable.accent_button : R.drawable.grey_background_button));
        holder.binding.tvButton2.setTextColor(ContextCompat.getColor(context, isButton2Enable ? R.color.colorPrimary : R.color.colorLightPaleBlueGrey));
        holder.binding.tvManage.setBackground(ContextCompat.getDrawable(context, !isPaymentStatusPending ? R.drawable.grey_background_button : R.drawable.grey_background_button));
        holder.binding.tvManage.setTextColor(ContextCompat.getColor(context, !isPaymentStatusPending ? R.color.colorLightBlueGrey : R.color.colorLightPaleBlueGrey));
    }

    @Override
    public int getItemCount() {
        return subscriptionHeaterDetailsList.size();
    }

    private String getMonth(SubscriptionHeaterDetail subscriptionDetails) {
        String currentString = subscriptionDetails != null && !TextUtils.isEmpty(subscriptionDetails.getMonthName()) ? subscriptionDetails.getMonthName() : "JAN-";
        String[] separated = currentString.split("-");
        return separated[0].substring(0, 3).toUpperCase();
    }

    class SubscriptionViewHolder extends RecyclerView.ViewHolder {
        private final FragmentSubscriptionHeaterItemBinding binding;

        SubscriptionViewHolder(final FragmentSubscriptionHeaterItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public interface AdapterListerner {
        void vmCallbackManageActivity(String issueId, String primaryOrderId, Integer nmsCustomerId);

        void vmTrackOrder(String primaryOrderId);

        void onLaunchPaynow(String issueId, String orderId, Integer customerID);
    }

    public interface PayNowListerner {

    }
}
