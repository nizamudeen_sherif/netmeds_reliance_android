package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RescheduleRequest implements Serializable {

    @SerializedName("primaryOrderId")
    @Expose
    private String primaryOrderId;
    @SerializedName("issueId")
    @Expose
    private String issueId;
    @SerializedName("rescheduledDays")
    @Expose
    private Integer rescheduledDays;
    @SerializedName("isForAllOrder")
    @Expose
    private Boolean isForAllOrder;

    public String getPrimaryOrderId() {
        return primaryOrderId;
    }

    public void setPrimaryOrderId(String primaryOrderId) {
        this.primaryOrderId = primaryOrderId;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public Integer getRescheduledDays() {
        return rescheduledDays;
    }

    public void setRescheduledDays(Integer rescheduledDays) {
        this.rescheduledDays = rescheduledDays;
    }

    public Boolean getIsForAllOrder() {
        return isForAllOrder;
    }

    public void setIsForAllOrder(Boolean isForAllOrder) {
        this.isForAllOrder = isForAllOrder;
    }
}
