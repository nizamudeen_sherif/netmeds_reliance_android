package com.nms.netmeds.m3subscription.ui;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.nms.netmeds.base.BaseActivity;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.m3subscription.R;
import com.nms.netmeds.m3subscription.adapter.OnBoardingAdapterSubscription;
import com.nms.netmeds.m3subscription.databinding.ActivityOnboardingSubscriptionBinding;

public class OnboardingSubscriptionActivity extends BaseActivity implements OnBoardingAdapterSubscription.OnboardingAdapterListener {

    private ActivityOnboardingSubscriptionBinding activityOnboardingSubscriptionBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activityOnboardingSubscriptionBinding = DataBindingUtil.setContentView(this, R.layout.activity_onboarding_subscription);
        adapter();
        if (android.os.Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
    }

    private void adapter() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        activityOnboardingSubscriptionBinding.onboardingRecyclerview.setLayoutManager(linearLayoutManager);
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(this).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getSubscriptionBannersList() != null && configurationResponse.getResult().getConfigDetails().getSubscriptionBannersList().size() > 0) {
            OnBoardingAdapterSubscription adapter = new OnBoardingAdapterSubscription(this, configurationResponse.getResult().getConfigDetails().getSubscriptionBannersList(), this);
            activityOnboardingSubscriptionBinding.onboardingRecyclerview.setAdapter(adapter);
        }
    }

    @Override
    public void vmCallbackFinish() {
        OnboardingSubscriptionActivity.this.finish();
    }
}
