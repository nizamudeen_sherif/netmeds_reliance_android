package com.nms.netmeds.m3subscription.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.m3subscription.R;

public class CancelAndSkipPopUpDialogViewModel extends BaseViewModel {

    private CancelAndSkipPopUpDialogViewModelListener listener;
    private boolean isFromCancelOrder;
    private boolean isFromExitSubscription = false;


    public CancelAndSkipPopUpDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(CancelAndSkipPopUpDialogViewModelListener listener, boolean isFromCancelOrder, boolean isFromExitSubscription) {
        this.listener = listener;
        this.isFromCancelOrder = isFromCancelOrder;
        this.isFromExitSubscription = isFromExitSubscription;
    }

    public boolean isFromCancelOrder() {
        return isFromCancelOrder;
    }

    public String dialogTitle() {
        return isFromCancelOrder && !isFromExitSubscription ? getApplication().getString(R.string.text_cancel_subscription) : isFromExitSubscription ? getApplication().getString(R.string.text_exit_subscription_header) : getApplication().getString(R.string.text_skip_this_delivery);
    }

    public String isOrderDescription() {
        return isFromCancelOrder && !isFromExitSubscription? getApplication().getString(R.string.text_cancel_sub_description) : isFromExitSubscription ? getApplication().getString(R.string.text_exit_subscription) : String.format(getApplication().getString(R.string.text_skip_description), splitIssueDate());
    }

    private String splitIssueDate() {
        if (!TextUtils.isEmpty(SubscriptionHelper.getInstance().getIssueOrderDate())) {
            String[] splitString = SubscriptionHelper.getInstance().getIssueOrderDate().split(",");
            return splitString[0];
        } else
            return "";
    }

    public void OnclickDismiss() {
        onNoClick();
    }

    public void onNoClick() {
        listener.onNoClick();
    }

    public void onYesClick() {
        listener.onYesClick(isFromCancelOrder());
    }

    public interface CancelAndSkipPopUpDialogViewModelListener {
        void onNoClick();

        void onYesClick(boolean isFromCancelOrder);
    }
}
