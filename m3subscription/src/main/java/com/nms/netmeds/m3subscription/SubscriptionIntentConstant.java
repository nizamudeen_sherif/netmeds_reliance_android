package com.nms.netmeds.m3subscription;

public class SubscriptionIntentConstant {

    public static final String API_SUCCESS = "success";
    public static final String API_FAIL = "Fail";

    public static final String PLACED_SUCCESSFUL_FROM_FLAG = "PLACED_SUCCESSFUL_FROM_FLAG";
    public static final String PLACED_SUCCESSFUL_FROM_REFILL = "PLACED_SUCCESSFUL_FROM_REFILL";
    public static final String PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER = "PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER";
    public static final String REFILL_SUBSCRIPTION_RESPONSE = "REFILL_SUBSCRIPTION_RESPONSE";

    public static final String ORDER_ID = "ORDER_ID";
    public static final String PAYNOW = "PAYNOW";


    public static final String ISSUE_ID = "issueId";
    public static final String PRIMARY_ORDER_ID = "primaryOrderId";
    public static final String M3_UNSUBSCRIBE_ORDER_STATUS = "Active";
    public static final String M3_PAYNOW_ORDER_STATUS = "Paynow";
    public static final String M3_TRACK_ORDER_ORDER_STATUS = "TrackOrder";

    public static final String DELIVERY_ADDRESS = "DELIVERY_ADDRESS";
    public static final String TIME_DURATION = "TIME_DURATION";

    public static final String REFILL_INITIATE_FROM_BANNER = "REFILL_INITIATE_FROM_BANNER";
    public static final String EXPECTED_DELIVERY_DATE = "EXPECTED_DELIVERY_DATE";
    public static final String DELIVERY_ESTIMATE_REQUEST = "DELIVERY_ESTIMATE_REQUEST";
    public static final String NEXT_DELIVERY_INTERVAL_DATE = "NEXT_DELIVERY_INTERVAL_DATE";


    //ConstantStrings
    public static final String CONFIRMATION_PENDING = "Confirmation Pending";
    public static final String OUT_OF_DELIVERY = "Out for Delivery";
    public static final String PAYMENT_PENDING = "Payment Pending";
    public static final String DELIVERED = "Delivered";
    public static final String UNSUBSCRIBED = "UnSubscribed";
    public static final String SKIPPED = "Skipped";
    public static final String ORDER_STATUS_X = "X";
    public static final String SUBSCRIPTION_STATUS_ACTIVE = "Active";
    public static final String PAYMENT_STATUS_PENDING = "Pending";
    public static final String PAYMENT_STATUS_PAYNOW = "PayNow";
    public static final String PAYMENT_STATUS_TRACK = "Track";
    public static final String PAYMENT_STATUS_SUBSCRIPTION_PENDING = "Subscription Pending";
    public static final String PROCESSING = "Processing";
    public static final String PRO = "Processing";
    public static final String PENDING_DELIVERY = "Pending Delivery";
    public static final String PRESCRIPTION_VERIFICATION_PENDING = "Prescription verification pending";

    public static final String IS_FROM_PAYMENT = "IS_FROM_PAYMENT";
}
