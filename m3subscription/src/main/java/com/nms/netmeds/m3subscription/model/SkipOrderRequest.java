package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkipOrderRequest implements Serializable {
    @SerializedName("primaryOrderId")
    @Expose
    private String primaryOrderId;
    @SerializedName("issueId")
    @Expose
    private String issueId;

    public String getPrimaryOrderId() {
        return primaryOrderId;
    }

    public void setPrimaryOrderId(String primaryOrderId) {
        this.primaryOrderId = primaryOrderId;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

}
