package com.nms.netmeds.m3subscription.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.MstarBasicResponseFailureReasonTemplateModel;

import java.io.Serializable;
import java.util.List;

public class SubscriptionHeaderResponse implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("result")

    private List<SubscriptionResult> result = null;
    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel reason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SubscriptionResult> getResult() {
        return result;
    }

    public void setResult(List<SubscriptionResult> result) {
        this.result = result;
    }

    public MstarBasicResponseFailureReasonTemplateModel getReason() {
        return reason;
    }

    public void setReason(MstarBasicResponseFailureReasonTemplateModel reason) {
        this.reason = reason;
    }
}
