package com.nms.netmeds.m3subscription.model;

import com.nms.netmeds.m3subscription.fragment.SubscriptionTabFragment;

public class TabDetails {
    private String tabName;
    private SubscriptionTabFragment fragment;

    public TabDetails(String tabName, SubscriptionTabFragment fragment) {
        this.tabName = tabName;
        this.fragment = fragment;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public SubscriptionTabFragment getFragment() {
        return fragment;
    }

    public void setFragment(SubscriptionTabFragment fragment) {
        this.fragment = fragment;
    }

}
