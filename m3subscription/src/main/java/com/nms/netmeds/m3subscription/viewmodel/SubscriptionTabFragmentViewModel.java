package com.nms.netmeds.m3subscription.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.OrderListenerTypeEnum;
import com.nms.netmeds.base.OrderPayNowRetryReorderHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.m3subscription.model.SubscriptionHeaterDetail;
import com.nms.netmeds.m3subscription.model.SubscriptionResult;
import com.nms.netmeds.m3subscription.model.TabItemHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubscriptionTabFragmentViewModel extends AppViewModel implements OrderPayNowRetryReorderHelper.OrderHelperCallback {

    private SubscriptionTabFragmentViewModelListener listener;
    private BasePreference basePreference;

    private List<SubscriptionHeaterDetail> monthlySubscriptionList = new ArrayList<>();

    private String tabName;
    private Application application;

    public SubscriptionTabFragmentViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public void initiate(String tabName, SubscriptionTabFragmentViewModelListener listener, BasePreference basePreference) {
        this.listener = listener;
        this.tabName = tabName;
        this.basePreference = basePreference;
        loadSubscriptionDetails();
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(listener.getContext());
        listener.showNetworkErrorView(isConnected);
        if (isConnected) {
            if (transactionId == APIServiceManager.MSTAR_SUBSCRIPTION_CREATE_CART) {
                APIServiceManager.getInstance().mStarCreateSubscriptionCart(this, basePreference.getMstarBasicHeaderMap());
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        if (transactionId == APIServiceManager.MSTAR_SUBSCRIPTION_CREATE_CART) {
            createMStarCartResponse(data);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.dismissLoader();
        listener.showWebserviceErrorView(true);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        listener.showWebserviceErrorView(false);
    }

    private void createMStarCartResponse(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            SubscriptionHelper.getInstance().setSubscriptionCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            listener.clickCreateNewFill();

        }
    }

    private void loadSubscriptionDetails() {
        if (TabItemHelper.getInstance().getSubscriptionHeaderResponse() != null) {
            List<SubscriptionResult> subscriptionResults = TabItemHelper.getInstance().getSubscriptionHeaderResponse().getResult();
            if (subscriptionResults != null && subscriptionResults.size() > 0) {
                monthlySubscriptionList.clear();
                for (SubscriptionResult result : subscriptionResults) {
                    String month = result.getMonthName().substring(0, 3).toUpperCase();
                    if (tabName != null && tabName.equals(month)) {
                        monthlySubscriptionList.addAll(result.getSubscriptionDetails());
                        break;
                    }
                }
            }
        }
        if (monthlySubscriptionList != null && monthlySubscriptionList.size() == 0) {
            listener.recyclerViewVisibilty(false, true);
        } else {
            listener.recyclerViewVisibilty(true, false);

            loadSubscriptionListAdapter();
        }
    }

    public void onClickCreateNewFill() {
        initiateAPICall(APIServiceManager.MSTAR_SUBSCRIPTION_CREATE_CART);
    }

    private void loadSubscriptionListAdapter() {
        listener.loadSubscriptionListAdapter(monthlySubscriptionList);
    }


    public void onLaunchPaynow(String issueId, String orderId, Integer customerId) {
        SubscriptionHelper.getInstance().setIssueId(issueId);
        SubscriptionHelper.getInstance().setOrderId(orderId);
        SubscriptionHelper.getInstance().setPayNowSubscription(true);
        new OrderPayNowRetryReorderHelper(application).initiate(this, "", OrderListenerTypeEnum.SUBSCRIPTION_PAY_NOW, basePreference);

    }

    @Override
    public void navigateToAddAddress() {
        listener.navigateToAddAddress();
    }

    @Override
    public void redirectToPayment() {
        listener.navigatePaymentActivity();
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, List<String> maxLimitBreachProducts, boolean isFromEditOrder, String orderId) {
        listener.navigateCartActivity(notAddedProducts, isFromEditOrder, orderId);
    }

    @Override
    public void hideLoader() {
        listener.dismissLoader();
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(listener.getContext());
    }

    @Override
    public void showNoNetworkError(boolean isConnected) {
    }

    @Override
    public void showLoader() {
        listener.showLoader();

    }

    @Override
    public void showCancelOrderError(String s) {

    }

    @Override
    public void onFailedFromHelper(int transactionId, String data, boolean isFromHelper) {

    }

    @Override
    public void cancelOrderSuccessful(String message) {

    }

    @Override
    public void navigateToSuccessPage(String data) {

    }

    @Override
    public void navigateToCodFailure(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct) {

    }

    @Override
    public void showMessage(String message) {
        listener.showErrorMessage(message);
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        listener.navigateToCartForColdStorage(interCityProductList);
    }

    @Override
    public Context getContext() {
        return listener.getContext();
    }

    public interface SubscriptionTabFragmentViewModelListener {

        void showLoader();

        void dismissLoader();

        void showErrorMessage(String errorMessage);

        void showNetworkErrorView(boolean isShowNetWokErrorView);

        void showWebserviceErrorView(boolean isShowWebserviceErrorView);

        void loadSubscriptionListAdapter(List<SubscriptionHeaterDetail> monthlySubscriptionList);

        void recyclerViewVisibilty(boolean isRecyclerView, boolean isCreateNewFill);

        void clickCreateNewFill();

        Context getContext();

        void navigateToAddAddress();

        void navigateCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, boolean isFromEditOrder, String orderId);

        void navigatePaymentActivity();

        void navigateToCartForColdStorage(ArrayList<String> interCityProductList);
    }
}
