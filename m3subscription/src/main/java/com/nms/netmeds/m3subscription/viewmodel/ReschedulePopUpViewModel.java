package com.nms.netmeds.m3subscription.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.SubscriptionHelper;

public class ReschedulePopUpViewModel extends BaseViewModel {

    private M3ReschedulePopUpListener m3ReschedulePopUpListener;
    private boolean isselect = false;

    public ReschedulePopUpViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(M3ReschedulePopUpListener m3ReschedulePopUpListener) {
        this.m3ReschedulePopUpListener = m3ReschedulePopUpListener;
        m3ReschedulePopUpListener.enableContinueButton(false);
    }

    public void onClickDismiss() {
        m3ReschedulePopUpListener.vmDismiss();
    }

    public boolean onRescheduleVisibility() {
        return SubscriptionHelper.getInstance().getLastMonthIssueOrderFlag();
    }


    public void onTypeChecked(boolean checked, int i) {
        if (checked) {
            m3ReschedulePopUpListener.onTypeCheckBox(true);
            isselect = true;
        } else {
            m3ReschedulePopUpListener.onTypeCheckBox(false);
            isselect = false;
        }
    }


    public void onClickFourDays() {
        m3ReschedulePopUpListener.onClickFourDays();
    }

    public void onClickTenDays() {
        m3ReschedulePopUpListener.onClickTenDays();
    }

    public void onClickFifteenDays() {
        m3ReschedulePopUpListener.onClickFifteenDays();
    }


    public void onRescheduleClick() {
        onClickDismiss();
        m3ReschedulePopUpListener.onClickReschedule(isselect);
    }


    public interface M3ReschedulePopUpListener {

        void vmDismiss();

        void onClickReschedule(boolean isSelect);

        void onClickFourDays();

        void enableContinueButton(boolean isEnable);

        void onClickTenDays();

        void onClickFifteenDays();

        void onTypeCheckBox(boolean isCheck);

    }
}
