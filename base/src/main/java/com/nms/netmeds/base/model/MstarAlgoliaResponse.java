package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class MstarAlgoliaResponse implements Serializable {
    @SerializedName(value = "hits", alternate = "result")
    private List<MstarAlgoliaResult> algoliaResultList = null;
    @SerializedName("nbHits")
    private int nbHits;
    @SerializedName("page")
    private int page;
    @SerializedName("nbPages")
    private int nbPages;
    @SerializedName("hitsPerPage")
    private int hitsPerPage;
    @SerializedName("query")
    private String query;
    @SerializedName("params")
    private String params;
    @SerializedName("queryID")
    private String queryID;
    @SerializedName("facets")
    private Map<String, Map<String, String>> facetList;
    @SerializedName("status")
    private String status;

    public List<MstarAlgoliaResult> getAlgoliaResultList() {
        return algoliaResultList;
    }

    public void setAlgoliaResultList(List<MstarAlgoliaResult> algoliaResultList) {
        this.algoliaResultList = algoliaResultList;
    }

    public int getNbHits() {
        return nbHits;
    }

    public void setNbHits(int nbHits) {
        this.nbHits = nbHits;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getNbPages() {
        return nbPages;
    }

    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

    public int getHitsPerPage() {
        return hitsPerPage;
    }

    public void setHitsPerPage(int hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getQueryID() {
        return queryID;
    }

    public void setQueryID(String queryID) {
        this.queryID = queryID;
    }

    public Map<String, Map<String, String>> getFacetList() {
        return facetList;
    }

    public void setFacetList(Map<String, Map<String, String>> facetList) {
        this.facetList = facetList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
