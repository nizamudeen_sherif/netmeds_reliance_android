package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Variant {
    @SerializedName("facet_name")
    private String variantTitle;
    @SerializedName("facet_values")
    private List<VariantFacet> variantFacetList;

    public String getVariantTitle() {
        return variantTitle;
    }

    public void setVariantTitle(String variantTitle) {
        this.variantTitle = variantTitle;
    }

    public List<VariantFacet> getVariantFacetList() {
        return variantFacetList;
    }

    public void setVariantFacetList(List<VariantFacet> variantFacetList) {
        this.variantFacetList = variantFacetList;
    }
}
