package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarNetmedsOffer {

    @SerializedName("pageId")
    private String pageId;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("bannerImg")
    private String bannerImg;
    @SerializedName("btnText")
    private String btnText;
    @SerializedName("shopUrl")
    private String shopUrl;
    @SerializedName("combineId")
    private String combineId;
    @SerializedName("sectionType")
    private String sectionType;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("couponCode")
    private String couponCode;
    @SerializedName("couponDescription")
    private String couponDescription;
    @SerializedName("eligibility")
    private String eligibility;
    @SerializedName("howToGet")
    private String howToGet;
    @SerializedName("conditions")
    private String conditions;
    @SerializedName("offerType")
    private String offerType;
    @SerializedName("url")
    private String url;
    @SerializedName("content")
    private String content;
    @SerializedName("offerList")
    private List<MstarNetmedsOffer> offerList;


    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBannerImg() {
        return bannerImg;
    }

    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg;
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

    public String getCombineId() {
        return combineId;
    }

    public void setCombineId(String combineId) {
        this.combineId = combineId;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponDescription() {
        return couponDescription;
    }

    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public String getEligibility() {
        return eligibility;
    }

    public void setEligibility(String eligibility) {
        this.eligibility = eligibility;
    }

    public String getHowToGet() {
        return howToGet;
    }

    public void setHowToGet(String howToGet) {
        this.howToGet = howToGet;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<MstarNetmedsOffer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<MstarNetmedsOffer> offerList) {
        this.offerList = offerList;
    }
}

