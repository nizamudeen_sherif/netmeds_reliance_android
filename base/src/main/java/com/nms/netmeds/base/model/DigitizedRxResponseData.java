package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

public class DigitizedRxResponseData {
    @SerializedName("result")
    private DigitizedRxResult result;

    public DigitizedRxResult getResult() {
        return result;
    }

    public void setResult(DigitizedRxResult result) {
        this.result = result;
    }
}
