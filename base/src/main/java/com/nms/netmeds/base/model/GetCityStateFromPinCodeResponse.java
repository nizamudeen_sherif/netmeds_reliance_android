package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class GetCityStateFromPinCodeResponse {
    @SerializedName("result")
    private GetCityStateFromPinCodeResult result;
    @SerializedName("reason")
    private String reason;
    @SerializedName("status")
    private String status;

    public GetCityStateFromPinCodeResult getResult() {
        return result;
    }

    public void setResult(GetCityStateFromPinCodeResult result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
