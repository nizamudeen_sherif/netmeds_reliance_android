package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarBrainSinConfigDetails {

    @SerializedName("TemplateId")
    private String templateId;
    @SerializedName("EnableStatus")
    private boolean enableStatus;
    @SerializedName("Title")
    private String title;
    @SerializedName("SubTitle")
    private String subTitle;
    @SerializedName("Description")
    private String description;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public boolean isEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(boolean enableStatus) {
        this.enableStatus = enableStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
