package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class MStarUsedWalletAmountModel {

    @SerializedName("wallet_cash")
    private BigDecimal walletCash = BigDecimal.ZERO;

    @SerializedName("super_cash")
    private BigDecimal nmsSuperCash = BigDecimal.ZERO;

    @SerializedName("nms_cash")
    private BigDecimal nmsPrepaidCash = BigDecimal.ZERO;

    @SerializedName("total")
    private BigDecimal totalWallet = BigDecimal.ZERO;

    public BigDecimal getWalletCash() {
        return walletCash;
    }

    public void setWalletCash(BigDecimal nmsCash) {
        this.walletCash = nmsCash;
    }

    public BigDecimal getNmsSuperCash() {
        return nmsSuperCash;
    }

    public void setNmsSuperCash(BigDecimal nmsSuperCash) {
        this.nmsSuperCash = nmsSuperCash;
    }

    public BigDecimal getNmsPrepaidCash() {
        return nmsPrepaidCash;
    }

    public void setNmsPrepaidCash(BigDecimal nmsPrepaidCash) {
        this.nmsPrepaidCash = nmsPrepaidCash;
    }

    public BigDecimal getTotalWallet() {
        return totalWallet;
    }

    public void setTotalWallet(BigDecimal totalWallet) {
        this.totalWallet = totalWallet;
    }
}
