
package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.AppConstant;

import java.io.Serializable;
import java.math.BigDecimal;

public class Alternative implements Serializable {

    @SerializedName("cart_product_code")
    private int cartProductCode;
    @SerializedName("cart_product_name")
    private String cartProductName;
    @SerializedName("cart_product_qty")
    private int cartProductQuantity;
    @SerializedName("cart_product_mfgr_name")
    private String cartProductMfgName;
    @SerializedName("cart_product_mrp")
    private BigDecimal cartProductMrp = BigDecimal.ZERO;
    @SerializedName("cart_product_selling_price")
    private BigDecimal cartProductSellingPrice = BigDecimal.ZERO;
    @SerializedName("cart_product_line_value")
    private BigDecimal cartProductLineValue = BigDecimal.ZERO;
    @SerializedName("cart_product_image_path")
    private String cartProductImagePath;


    @SerializedName("alternate_product_code")
    private int alternateProductCode;
    @SerializedName("alternate_product_name")
    private String alternateProductName;
    @SerializedName("alternate_product_qty")
    private int alternateQuantity;
    @SerializedName("alternate_product_mfgr_name")
    private String alternateProductMfgrName;
    @SerializedName("alternate_product_generic")
    private String alternateProductGeneric;
    @SerializedName("alternate_product_mrp")
    private BigDecimal alternateProductMrp = BigDecimal.ZERO;
    @SerializedName("alternate_product_selling_price")
    private BigDecimal alternateProductSellingPrice = BigDecimal.ZERO;
    @SerializedName("alternate_product_line_value")
    private BigDecimal alternateProductLineValue = BigDecimal.ZERO;
    @SerializedName("alternate_product_image_path")
    private String alternateProductImagePath;

    @SerializedName("rx_required")
    private boolean isRxRequired;

    @SerializedName("savings")
    private BigDecimal savings = BigDecimal.ZERO;

    public int getCartProductCode() {
        return cartProductCode;
    }

    public void setCartProductCode(int cartProductCode) {
        this.cartProductCode = cartProductCode;
    }

    public String getCartProductName() {
        return cartProductName;
    }

    public void setCartProductName(String cartProductName) {
        this.cartProductName = cartProductName;
    }

    public int getCartProductQuantity() {
        return cartProductQuantity;
    }

    public void setCartProductQuantity(int cartProductQuantity) {
        this.cartProductQuantity = cartProductQuantity;
    }

    public String getCartProductMfgName() {
        return AppConstant.MFR+ cartProductMfgName;
    }

    public void setCartProductMfgName(String cartProductMfgName) {
        this.cartProductMfgName =  cartProductMfgName;
    }

    public BigDecimal getCartProductMrp() {
        return cartProductMrp;
    }

    public void setCartProductMrp(BigDecimal cartProductMrp) {
        this.cartProductMrp = cartProductMrp;
    }

    public BigDecimal getCartProductSellingPrice() {
        return cartProductSellingPrice;
    }

    public void setCartProductSellingPrice(BigDecimal cartProductSellingPrice) {
        this.cartProductSellingPrice = cartProductSellingPrice;
    }

    public BigDecimal getCartProductLineValue() {
        return cartProductLineValue;
    }

    public void setCartProductLineValue(BigDecimal cartProductLineValue) {
        this.cartProductLineValue = cartProductLineValue;
    }

    public String getCartProductImagePath() {
        return cartProductImagePath;
    }

    public void setCartProductImagePath(String cartProductImagePath) {
        this.cartProductImagePath = cartProductImagePath;
    }

    public int getAlternateProductCode() {
        return alternateProductCode;
    }

    public void setAlternateProductCode(int alternateProductCode) {
        this.alternateProductCode = alternateProductCode;
    }

    public String getAlternateProductName() {
        return alternateProductName;
    }

    public void setAlternateProductName(String alternateProductName) {
        this.alternateProductName = alternateProductName;
    }

    public int getAlternateQuantity() {
        return alternateQuantity;
    }

    public void setAlternateQuantity(int alternateQuantity) {
        this.alternateQuantity = alternateQuantity;
    }

    public String getAlternateProductMfgrName() {
        return AppConstant.MFR + alternateProductMfgrName;
    }

    public void setAlternateProductMfgrName(String alternateProductMfgrName) {
        this.alternateProductMfgrName =  alternateProductMfgrName;
    }

    public String getAlternateProductGeneric() {
        return alternateProductGeneric;
    }

    public void setAlternateProductGeneric(String alternateProductGeneric) {
        this.alternateProductGeneric = alternateProductGeneric;
    }

    public BigDecimal getAlternateProductMrp() {
        return alternateProductMrp;
    }

    public void setAlternateProductMrp(BigDecimal alternateProductMrp) {
        this.alternateProductMrp = alternateProductMrp;
    }

    public BigDecimal getAlternateProductSellingPrice() {
        return alternateProductSellingPrice;
    }

    public void setAlternateProductSellingPrice(BigDecimal alternateProductSellingPrice) {
        this.alternateProductSellingPrice = alternateProductSellingPrice;
    }

    public BigDecimal getAlternateProductLineValue() {
        return alternateProductLineValue;
    }

    public void setAlternateProductLineValue(BigDecimal alternateProductLineValue) {
        this.alternateProductLineValue = alternateProductLineValue;
    }

    public String getAlternateProductImagePath() {
        return alternateProductImagePath;
    }

    public void setAlternateProductImagePath(String alternateProductImagePath) {
        this.alternateProductImagePath = alternateProductImagePath;
    }

    public boolean isRxRequired() {
        return isRxRequired;
    }

    public void setRxRequired(boolean rxRequired) {
        isRxRequired = rxRequired;
    }

    public BigDecimal getSavings() {
        return savings;
    }

    public void setSavings(BigDecimal savings) {
        this.savings = savings;
    }
}
