package com.nms.netmeds.base.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.nms.netmeds.base.R;

public class SnackBarHelper {
    private static Snackbar snackbar;

    public static void snackBarCallBack(ViewGroup parentView, Context context, String error) {
        snackbar = Snackbar.make(parentView, error, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.colorMediumPink));
        snackbar.show();
    }

    public static void snackBarCallBackSetAction(ViewGroup parentView, Context context, String error, String actionText, final SnackBarHelperListener snackBarHelperListener) {
        snackbar = Snackbar.make(parentView, error, Snackbar.LENGTH_INDEFINITE);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.colorMediumPink));
        snackbar.setAction(actionText, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
                snackBarHelperListener.snackBarOnClick();
            }
        });
        snackbar.show();
    }

    public static void dismissSnackBar() {
        if (snackbar != null)
            snackbar.dismiss();
    }

    public interface SnackBarHelperListener {
        void snackBarOnClick();
    }
}
