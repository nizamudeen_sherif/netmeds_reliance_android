package com.nms.netmeds.base;

import android.content.Context;
import android.widget.ProgressBar;

public interface LoaderHelper {

    void showProgress(Context context);

    void dismissProgress();

    void showTransactionProgress(Context context);

    void showHorizontalProgressBar(boolean isClickable, ProgressBar progressBar);
}
