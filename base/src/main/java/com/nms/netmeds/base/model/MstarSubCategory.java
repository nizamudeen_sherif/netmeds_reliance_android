package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarSubCategory {
    @SerializedName("aggregate_product_count")
    private Integer aggregateProductCount;

    @SerializedName("product_count")
    private Integer productCount;

    @SerializedName("masthead_image_path")
    private String mastheadImagePath;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private Integer id;

    @SerializedName("thumbnail_image_path")
    private String thumbnailImagePath;

    @SerializedName("url_path")
    private String urlPath;

    @SerializedName("sub_categories")
    private List<MstarSubCategory> subCategories;

    public List<MstarSubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<MstarSubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public Integer getAggregateProductCount() {
        return aggregateProductCount;
    }

    public void setAggregateProductCount(Integer aggregateProductCount) {
        this.aggregateProductCount = aggregateProductCount;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public String getMastheadImagePath() {
        return mastheadImagePath;
    }

    public void setMastheadImagePath(String mastheadImagePath) {
        this.mastheadImagePath = mastheadImagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getThumbnailImagePath() {
        return thumbnailImagePath;
    }

    public void setThumbnailImagePath(String thumbnailImagePath) {
        this.thumbnailImagePath = thumbnailImagePath;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }
}
