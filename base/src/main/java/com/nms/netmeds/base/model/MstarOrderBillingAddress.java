package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarOrderBillingAddress {
    @SerializedName("billingFirstName")
    private String billingFirstName;
    @SerializedName("billingLastName")
    private String billingLastName;
    @SerializedName("billingAddress1")
    private String billingAddress1;
    @SerializedName("billingAddress2")
    private String billingAddress2;
    @SerializedName("billingCity")
    private String billingCity;
    @SerializedName("billingZipCode")
    private String billingZipCode;
    @SerializedName("billingState")
    private String billingState;
    @SerializedName("billingCountry")
    private String billingCountry;
    @SerializedName("phoneNumber")
    private String phoneNumber;

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingZipCode() {
        return billingZipCode;
    }

    public void setBillingZipCode(String billingZipCode) {
        this.billingZipCode = billingZipCode;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
