package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PrescriptionUploadConfig {
    @SerializedName("header1")
    private String header1;
    @SerializedName("header2")
    private String header2;
    @SerializedName("description")
    private String description;
    @SerializedName("orderText")
    private String orderText;
    @SerializedName("topPrescriptionIcon")
    private String topPrescriptionIcon;
    @SerializedName("orderIcon")
    private String orderIcon;
    @SerializedName("buttonText")
    private String buttonText;
    @SerializedName("buttonTextNew")
    private String m2ButtonText;

    public String getHeader1() {
        return header1;
    }

    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrderText() {
        return orderText;
    }

    public void setOrderText(String orderText) {
        this.orderText = orderText;
    }

    public String getTopPrescriptionIcon() {
        return topPrescriptionIcon;
    }

    public void setTopPrescriptionIcon(String topPrescriptionIcon) {
        this.topPrescriptionIcon = topPrescriptionIcon;
    }

    public String getOrderIcon() {
        return orderIcon;
    }

    public void setOrderIcon(String orderIcon) {
        this.orderIcon = orderIcon;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getM2ButtonText() {
        return m2ButtonText;
    }

}
