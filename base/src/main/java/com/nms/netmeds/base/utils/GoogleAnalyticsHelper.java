/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.utils;


import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;

public class GoogleAnalyticsHelper {
    public static final String TRANSACTION_AFFILIATION = "Netmeds Mobile";
    private static final String CURRENCY = "INR";
    private static final String CURRENCY_CODE = "&cu";
    private static final String POST_SCREEN_PARAM_TRANSACTION = "transaction";

    //Event Category
    public static final String EVENT_CATEGORY_NAME_NAVIGATION = "Navigation";
    public static final String EVENT_CATEGORY_NAME_BANNER = "Banner";
    public static final String EVENT_CATEGORY_NAME_CATEGORY = "Category";
    public static final String EVENT_CATEGORY_NAME_NAVIGATION_2 = "Navigation 2";
    public static final String EVENT_CATEGORY_NAME_WELLNESS_SECTION = "Wellness Section";
    public static final String EVENT_CATEGORY_NAME_CONSULTATION_SECTION = "Consultation Section";
    public static final String EVENT_CATEGORY_NAME_REFERRAL = "Referral";
    public static final String EVENT_CATEGORY_NAME_OTHER_ACTIONS = "Other Actions";
    public static final String EVENT_CATEGORY_NAME_OUT_OF_STOCK = "Out of Stock";
    public static final String EVENT_CATEGORY_NAME_HIGH_ORDER_VALUE = "High Order Value";
    public static final String EVENT_CATEGORY_NAME_PROMO_CODE = "Promo Code";
    public static final String EVENT_CATEGORY_NAME_NEW_ADDRESS = "New Address";
    public static final String EVENT_CATEGORY_NAME_LOGIN = "Login";
    public static final String EVENT_CATEGORY_NAME_NAVIGATION_3 = "Navigation 3";
    public static final String EVENT_CATEGORY_NAME_NETMEDS_FIRST = "Netmeds First";
    public static final String EVENT_CATEGORY_NAME_ALT_CART = "Alt Cart";

    //Event Label
    public static final String EVENT_LABEL_HOME_PAGE = "Home Page";
    public static final String EVENT_LABEL_SEARCH_PAGE = "Search Page";
    public static final String EVENT_LABEL_PRODUCT_DETAILS_PAGE = "Product Details Page";
    public static final String EVENT_LABEL_CART_PAGE = "Cart Page";
    public static final String EVENT_LABEL_M1_ATTACH_PRESCRIPTION_PAGE = "M1 - Attach Prescription Page";
    public static final String EVENT_LABEL_SELECT_ADDRESS_PAGE = "Select Address Page";
    public static final String EVENT_LABEL_ORDER_REVIEW_PAGE = "Order Review Page";
    public static final String EVENT_LABEL_PAYMENT_DETAILS_PAGE = "Payment Details Page";
    public static final String EVENT_LABEL_FAILED_PAYMENT_PAGE = "Failed Payment Page";
    public static final String EVENT_LABEL_ORDER_SUCCESSFUL = "Order Successful";
    public static final String EVENT_LABEL_M2_UPLOAD_CONTINUED = "M2 - Upload Continued";
    public static final String EVENT_LABEL_M2_ORDER_CONFIRMATION = "M2 - Order Confirmation";
    public static final String EVENT_LABEL_M2_CART = "M2 - Cart";
    public static final String EVENT_LABEL_M2_ADDRESS = "M2 - Address";
    public static final String EVENT_LABEL_M2_Order_Review = "M2 - Order Review";
    public static final String EVENT_LABEL_M2_ORDER_SUCCESSFUL = "M2 - Order Successful";
    public static final String EVENT_LABEL_LOGIN_REGISTRATION = "Login/Registration";
    public static final String EVENT_LABEL_LOGIN_WITH_PASSWORD = "Login with Password";
    public static final String EVENT_LABEL_LOGIN_WITH_OTP = "Login with OTP";
    public static final String EVENT_LABEL_SIGN_UP = "Sign Up";
    public static final String EVENT_LABEL_ON_BOARDING_SCREEN = "Onboarding Screen";
    public static final String EVENT_LABEL_TRUE_CALLER = "Truecaller";
    public static final String EVENT_LABEL_MY_ORDER = "My Order";
    public static final String EVENT_LABEL_MY_ORDER_DETAILS = "My Order Details";
    public static final String EVENT_LABEL_MY_ACCOUNT = "My Account";
    public static final String EVENT_LABEL_ELITE_Membership = "Elite Membership";
    public static final String EVENT_LABLE_ORDER_MEDICINE_FROM_CONSULTATION = "Order Medicine";

    //Event Action
    public static final String EVENT_ACTION_SEARCH = "Search";
    public static final String EVENT_ACTION_CART = "Cart";
    public static final String EVENT_ACTION_PRIMARY_BANNERS = "Primary Banners";
    public static final String EVENT_ACTION_MEDICINE = "Medicine";
    public static final String EVENT_ACTION_WELLNESS = "Wellness";
    public static final String EVENT_ACTION_CONSULTATION = "Consultation";
    public static final String EVENT_ACTION_UPLOAD_PRESCRIPTION = "Upload Prescription";
    public static final String EVENT_ACTION_SECONDARY_BANNER = "Secondary Banner";
    public static final String EVENT_ACTION_WINTER_SALE = "Winter Sale";
    public static final String EVENT_ACTION_WELLNESS_PRODUCTS = "Wellness Products";
    public static final String EVENT_ACTION_HEALTH_CONCERNS = "Health Concerns";
    public static final String EVENT_ACTION_ONLINE_CONSULTATION = "Online Consultation";
    public static final String EVENT_ACTION_POPULAR_CONCERNS_CONSULTATION = "Popular Concerns - Consultation";
    public static final String EVENT_ACTION_REFER_EARN = "Refer & Earn";
    public static final String EVENT_ACTION_OFFER_BANNER_PG = "Offer Banner PG";
    public static final String EVENT_ACTION_BLOGS = "Blogs";
    public static final String EVENT_ACTION_PRODUCT_SEARCHED = "Product Searched";
    public static final String EVENT_ACTION_UPLOAD_SEARCH = "Upload - Search";
    public static final String EVENT_ACTION_VIEW_CART_SEARCH_PAGE = "View Cart - Search Page";
    public static final String EVENT_ACTION_ADD_PRODUCT_SEARCH = "Add Product - Search";
    public static final String EVENT_ACTION_VIEW_CART_PRODUCT_DETAILS = "View Cart - Product Details";
    public static final String EVENT_ACTION_CART_ICON_PRODUCT_DETAILS = "Cart Icon - Product Details";
    public static final String EVENT_ACTION_SUBSTITUTION_CLICKED = "Substitution Clicked";
    public static final String EVENT_ACTION_HOV_EXTRA_DISCOUNT = "HOV Extra discount";
    public static final String EVENT_ACTION_APPLY_PROMOCODE = "Apply Promocode";
    public static final String EVENT_ACTION_COUPON_APPLIED = "Coupon Applied";
    public static final String EVENT_ACTION_NMS_SUPERCASH_APPLIED = "NMS Supercash Applied";
    public static final String EVENT_ACTION_CART_PROCEED = "Cart - Proceed";
    public static final String EVENT_ACTION_EXTERNAL_DOCTOR_CONSULT = "External Doctor Consult";
    public static final String EVENT_ACTION_PROCEED_ATTACH_PRESCRIPTION = "Proceed - Attach Prescription";
    public static final String EVENT_ACTION_ADD_NEW_ADDRESS = "Add New Address";
    public static final String EVENT_ACTION_PROCEED_ADDRESS = "Proceed - Address";
    public static final String EVENT_ACTION_TAKE_ACTION = "Take Action";
    public static final String EVENT_ACTION_REVIEW_PROCEED = "Review - Proceed";
    public static final String EVENT_ACTION_PLACE_ORDER = "Place Order";
    public static final String EVENT_ACTION_CHANGE_TO_COD = "Change to COD";
    public static final String EVENT_ACTION_RETRY_PAYMENT = "Retry Payment";
    public static final String EVENT_ACTION_TRACK_ORDER = "Track Order";
    public static final String EVENT_ACTION_UPLOAD_CONTINUED = "Upload - Continued";
    public static final String EVENT_ACTION_ORDER_CONFIRMATION_CONTINUED = "Order Confirmation - Continued";
    public static final String EVENT_ACTION_ADDRESS_PROCEED = "Address - Proceed";
    public static final String EVENT_ACTION_PLACE_ORDER_PROCEED = "Place Order - Proceed";
    public static final String EVENT_ACTION_MOBILE_NUMBER_ENTERED = "Mobile Number Entered";
    public static final String EVENT_ACTION_FACEBOOK_LOGIN = "Facebook Login";
    public static final String EVENT_ACTION_GOOGLE_LOGIN = "Google Login";
    public static final String EVENT_ACTION_PASSWORD_ENTERED_PROCEED = "Password Entered - Proceed";
    public static final String EVENT_ACTION_LOGIN_WITH_OTP_CLICK = "Login with OTP - Click";
    public static final String EVENT_ACTION_LOGIN_WITH_OTP_SUCCESSFUL = "Login with OTP - Successful";
    public static final String EVENT_ACTION_REGISTERED_SUCCESSFUL = "Registered Successful";
    public static final String EVENT_ACTION_GET_STARTED_CLICKED = "Get Started Clicked";
    public static final String EVENT_ACTION_TRUE_CALLER_LOGIN = "Truecaller Login";
    public static final String EVENT_ACTION_NEW_NUMBER = "New Number";
    public static final String EVENT_ACTION_TRUE_CALLER_SIGN_UP = "Truecaller Sign Up";
    public static final String EVENT_ACTION_REORDER = "Reorder";
    public static final String EVENT_ACTION_VIEW_DETAILS = "View Details";
    public static final String EVENT_ACTION_PAY_NOW = "Pay Now";
    public static final String EVENT_ACTION_EDIT_ORDER = "Edit Order";
    public static final String EVENT_ACTION_CANCEL_ORDER = "Cancel Order";
    public static final String EVENT_ACTION_NEED_HELP = "Need Help";
    public static final String EVENT_ACTION_EXPLORE_PLANS_BUTTON = "Explore Plans button";
    public static final String EVENT_ACTION_NETMEDS_FIRST_BUTTON = "Netmeds First button";
    public static final String EVENT_ACTION_ADD_TO_CART_BUTTON = " Add to cart button";
    public static final String EVENT_ACTION_CONSULTATION_ORDER_MEDICINE = "Order Medicine - Consultation";
    public static final String EVENT_ACTION_RETRY = "Retry";
    public static final String EVENT_ACTION_COD = "COD";
    public static final String EVENT_ACTION_ALTERNATE_CART_YOU_SAVE = "Click-You save button";
    public static final String EVENT_ACTION_ALTERNATE_CART_REVERT_CART = "Click-Revert to original button";
    public static final String EVENT_ACTION_ALTERNATE_CART_UPDATE_CART = "Click-Update Cart button";
    public static final String EVENT_ACTION_ALTERNATE_CART_TAP_TO_REVERT = "Click-Tap to Revert butto￼";
    public static final String EVENT_ACTION_LAB_TESTS = "Lab Tests";

    //Post Screen Name
    public static final String POST_SCREEN_ON_BOARDING = "Onboarding";
    public static final String POST_SCREEN_SIGN_IN_SIGN_UP = "Sign in / Sign up";
    public static final String POST_SCREEN_SIGN_IN_WITH_PASSWORD = "Sign In with Password";
    public static final String POST_SCREEN_SIGN_IN_WITH_OTP = "Sign In with OTP";
    public static final String POST_SCREEN_FORGOT_PASSWORD_VERIFY_OTP = "Forgot Password - Verify OTP";
    public static final String POST_SCREEN_CREATE_ACCOUNT = "Create Account";
    public static final String POST_SCREEN_SET_PASSWORD_NEW_USER = "Set Password - New User";
    public static final String POST_SCREEN_RESET_PASSWORD_FORGOT_USER = "Reset Password - Forgot User";
    public static final String POST_SCREEN_HOME_PAGE = "Home Page";
    public static final String POST_SCREEN_SEARCH_PAGE = "Search Page";
    public static final String POST_SCREEN_PRODUCT_DETAILS_PAGE = "Product Details";
    public static final String POST_SCREEN_CART = "Cart Page";
    public static final String POST_SCREEN_ATTACH_PRESCRIPTION_M1 = "Attach Prescription - M1";
    public static final String POST_SCREEN_SELECT_ADDRESS = "Select Address";
    public static final String POST_SCREEN_ORDER_REVIEW = "Order Review";
    public static final String POST_SCREEN_PAYMENT_PAGE = "Payment Page";
    public static final String POST_SCREEN_ORDER_CONFIRMED = "Order Confirmed";
    public static final String POST_SCREEN_ORDER_FAILED = "Order Failed";
    public static final String POST_SCREEN_ATTACH_PRESCRIPTION_M2 = "Attach Prescription - M2";
    public static final String POST_SCREEN_CART_M2 = "Cart - M2";
    public static final String POST_SCREEN_SELECT_ADDRESS_M2 = "Select Address - M2";
    public static final String POST_SCREEN_ORDER_REVIEW_M2 = "Order Review - M2";
    public static final String POST_SCREEN_ORDER_CONFIRMATION_M2 = "Order Confirmation - M2";
    public static final String POST_SCREEN_MY_ORDERS = "My Orders";
    public static final String POST_SCREEN_ORDER_DETAILS = "Order Details";
    public static final String POST_SCREEN_TRACK_ORDER = "Track Order";
    public static final String POST_SCREEN_MY_ACCOUNTS = "My Accounts";
    public static final String POST_SCREEN_WALLETS = "Wallets";
    public static final String POST_SCREEN_OFFERS = "Offers";
    public static final String POST_SCREEN_HELPS = "Helps";
    public static final String POST_WELLNES_HOME = "Wellness Home";
    public static final String POST_SCREEN_PAYMENT_METHODS = "Payment Methods";
    public static final String POST_SCREEN_CONSULTATION_PAGE = "Consultation Page";
    public static final String POST_SCREEN_REFER_AND_EARN_PAGE = "Refer and Earn Page";
    public static final String POST_SCREEN_WELLNESS_PAGE = "Wellness Page";
    public static final String POST_SCREEN_CATEGORY_LIST_PAGE = "Category List Page";
    public static final String POST_SCREEN_PRODUCT_LIST_PAGE = "Product List Page";
    public static final String POST_SCREEN_CHAT_PAGE = "Chat Page";
    public static final String POST_SCREEN_CART_PAGE = "Cart page";
    public static final String POST_SCREEN_ALTERNATE_CART = "Alternate Cart";
    public static final String POST_SCREEN_PRODUCTS_ORDERED = "Products Ordered";
    public static final String POST_SCREEN_GENERIC = "Generic-";
    public static final String POST_SCREEN_GENERIC_HOME = "Generic Home";
    public static final String POST_SCREEN_GENERIC_SEARCH = "Generic Search";


    /*Diagnostic screen */
    public static final String POST_SCREEN_DIAGNOSTIC_LOCATION = "Dia_locationscreen";
    public static final String POST_SCREEN_DIAGNOSTIC_HOME_PAGE = "Dia_homepage";
    public static final String POST_SCREEN_DIAGNOSTIC_SEARCH_TEST = "Dia_testsearch";
    public static final String POST_SCREEN_DIAGNOSTIC_SELECT_LAB = "Dia_labselect";
    public static final String POST_SCREEN_DIAGNOSTIC_PATIENT_DETAILS = "Dia_patientdetails";
    public static final String POST_SCREEN_DIAGNOSTIC_ADDRESS = "Dia_address";
    public static final String POST_SCREEN_DIAGNOSTIC_TIME_SLOT = "Dia_slotselect";
    public static final String POST_SCREEN_DIAGNOSTIC_PACKAGE_SELECTION = "Dia_packselect";
    public static final String POST_SCREEN_DIAGNOSTIC_PLACE_ORDER = "Dia_orderplaced";
    public static final String POST_SCREEN_DIAGNOSTIC_TRACK_ORDER = "Dia_trackorder";
    public static final String POST_SCREEN_DIAGNOSTIC_ORDER_HISTORY = "Dia_orderhistory";

    private static GoogleAnalyticsHelper googleAnalyticsHelper;
    private Tracker mTracker;

    public static GoogleAnalyticsHelper getInstance() {
        if (googleAnalyticsHelper == null) {
            googleAnalyticsHelper = new GoogleAnalyticsHelper();
        }
        return googleAnalyticsHelper;
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized private Tracker getDefaultTracker(Context context) {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            mTracker = analytics.newTracker(ConfigMap.getInstance().getProperty(ConfigConstant.GOOGLE_ANALYTICS_ID));
            mTracker.enableExceptionReporting(true);
        }
        return mTracker;
    }

    /**
     * Google Analytics Screen Tracking
     *
     * @param context    current state of the application/object
     * @param screenName name of the screen
     */
    public void postScreen(Context context, String screenName) {
        if (context != null && !TextUtils.isEmpty(screenName)) {
            if(CommonUtils.isBuildVariantProdRelease()) {
                Tracker tracker = getDefaultTracker(context);
                tracker.setScreenName(screenName);
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                /*Facebook Post Screen Event*/
                FacebookPixelHelper.getInstance().logScreenEvent(context, screenName);
            }
            /*FireBase Analytics Screen Name Tracking Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseAnalyticsScreenName(context, screenName);
        }
    }

    /**
     * Google Analytics Event Tracking
     *
     * @param context  current state of the application/object
     * @param category event category name
     * @param action   event action name
     * @param label    event label name
     */
    public void postEvent(Context context, String category, String action, String label) {
        if (CommonUtils.isBuildVariantProdRelease()) {
            getDefaultTracker(context).send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        }
    }

    /**
     * Google Analytics E-Commerce Tracking
     *
     * @param context current state of the application/object
     * @param builder transaction object
     */
    public void postTransactionEvent(Context context, HitBuilders.ScreenViewBuilder builder) {
        if (CommonUtils.isBuildVariantProdRelease()) {
            Tracker tracker = getDefaultTracker(context);
            tracker.setScreenName(POST_SCREEN_PARAM_TRANSACTION);
            tracker.set(CURRENCY_CODE, CURRENCY);
            tracker.send(builder.build());
        }
    }
}
