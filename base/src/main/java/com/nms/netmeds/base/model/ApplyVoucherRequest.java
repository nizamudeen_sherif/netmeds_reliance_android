package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ApplyVoucherRequest {
    @SerializedName("cartId")
    private String cartId;
    @SerializedName("added_codes")
    private List<AppliedVoucher> addedCodeList = null;
    @SerializedName("existed_code")
    private String existedCode;
    @SerializedName("new_code")
    private String newCode;
    @SerializedName("giftCode")
    private String giftCode;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public List<AppliedVoucher> getAddedCodeList() {
        return addedCodeList;
    }

    public void setAddedCodeList(List<AppliedVoucher> addedCodeList) {
        this.addedCodeList = addedCodeList;
    }

    public String getExistedCode() {
        return existedCode;
    }

    public void setExistedCode(String existedCode) {
        this.existedCode = existedCode;
    }

    public String getNewCode() {
        return newCode;
    }

    public void setNewCode(String newCode) {
        this.newCode = newCode;
    }

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }
}
