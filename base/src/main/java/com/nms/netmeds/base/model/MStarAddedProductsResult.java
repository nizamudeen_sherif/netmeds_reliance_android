package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarAddedProductsResult {

    @SerializedName("result")
    private String result;
    @SerializedName("result_reason_code")
    private String resultReasonCode;
    @SerializedName("result_reason")
    private String resultReason;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResultReasonCode() {
        return resultReasonCode;
    }

    public void setResultReasonCode(String resultReasonCode) {
        this.resultReasonCode = resultReasonCode;
    }

    public String getResultReason() {
        return resultReason;
    }

    public void setResultReason(String resultReason) {
        this.resultReason = resultReason;
    }
}
