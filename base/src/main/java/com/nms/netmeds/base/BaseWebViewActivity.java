package com.nms.netmeds.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.lang.ref.WeakReference;

import static android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;

public class BaseWebViewActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("SetJavaScriptEnabled")
    protected void initWebViewSetting(WebView webView) {
        WebSettings webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(false);
        webSetting.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSetting.setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSetting.setMixedContentMode(MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    protected void initToolBar(CollapsingToolbarLayout collapsingToolbar, String title) {
        collapsingToolbar.setTitle(title);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    protected void setWebView(WebView webView) {
        initWebViewSetting(webView);
        webView.setWebViewClient(new NetmedsWebViewClient(this));
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 50) {
                    dismissProgress();
                }
            }
        });
    }

    class NetmedsWebViewClient extends WebViewClient {
        private final WeakReference<Activity> mActivityRef;

        public NetmedsWebViewClient(Activity activity) {
            mActivityRef = new WeakReference<Activity>(activity);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("mailto:")) {
                final Activity activity = mActivityRef.get();
                if (activity != null) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(activity, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                    activity.startActivity(i);
                    view.reload();
                    return true;
                }
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dismissProgress();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgress(BaseWebViewActivity.this);
        }


        private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            intent.putExtra(Intent.EXTRA_TEXT, body);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_CC, cc);
            intent.setType("message/rfc822");
            return intent;
        }
    }
}
