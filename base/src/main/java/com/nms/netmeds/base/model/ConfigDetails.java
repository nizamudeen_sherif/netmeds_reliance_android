package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConfigDetails {
    @SerializedName("fetchCommonDetails")
    private boolean fetchCommonDetails;
    @SerializedName("fetchCountryList")
    private boolean fetchCountryList;
    @SerializedName("fetchMasterDataApi")
    private boolean fetchMasterDataApi;
    @SerializedName("fetchPaymentConfigApi")
    private boolean fetchPaymentConfigApi;
    @SerializedName("commonDetailsCacheTimeHrs")
    private Integer commonDetailsCacheTimeHrs;
    @SerializedName("countryListCacheTimeHrs")
    private Integer countryListCacheTimeHrs;
    @SerializedName("uploadPrescriptionLimit")
    private Integer uploadPrescriptionLimit;
    @SerializedName("androidForceUpdateVersion")
    private String androidForceUpdateVersion;
    @SerializedName("iosForceUpdateVersion")
    private String iosForceUpdateVersion;
    @SerializedName("appUpdateTitle")
    private String appUpdateTitle;
    @SerializedName("appUpdateMesssage")
    private String appUpdateMesssage;
    @SerializedName("algolia")
    private AlgoliaCredentialDetail algoliaCredentialDetail;
    @SerializedName("magentoImageUrl")
    private String magentoImageUrl;
    @SerializedName("algoliaMagentoImageUrl")
    private String algoliaMagentoImageUrl;
    @SerializedName("privacy")
    private String privacy;
    @SerializedName("terms")
    private String terms;
    @SerializedName("pinCodeUrl")
    private String pinCodeUrl;
    @SerializedName("stateCityUrl")
    private String stateCityUrl;
    @SerializedName("referralTerms")
    private String referralTerms;
    @SerializedName("shippingMethodCode")
    private String shippingMethodCode;
    @SerializedName("shippingCarrierCode")
    private String shippingCarrierCode;
    @SerializedName("deliveryDateApi")
    private String deliveryDateApi;
    @SerializedName("webEngageKey")
    private String webEngageKey;
    @SerializedName("emailApiUrl")
    private String emailApiUrl;
    @SerializedName("emailApiKey")
    private String emailApiKey;
    @SerializedName("smsApiUrl")
    private String smsApiUrl;
    @SerializedName("storeId")
    private String storeId;
    @SerializedName("winterSaleActiveFlag")
    private boolean winterSaleActiveFlag;
    @SerializedName("wellnessProductsActiveFlag")
    private boolean wellnessProductsActiveFlag;
    @SerializedName("gatewayOffersActiveFlag")
    private boolean gatewayOffersActiveFlag;
    @SerializedName("promotionBannerFlag")
    private boolean promotionBannerFlag;
    @SerializedName("featuredArticleFlag")
    private boolean featuredArticleFlag;
    @SerializedName("cancelOrderReason")
    private List<CancelOrderReason> cancelOrderReason = null;
    @SerializedName("brainsinStatusFlag")
    private boolean brainsinStatusFlag;
    @SerializedName("brainsinsKey")
    private String brainsinsKey;
    @SerializedName("brainSinConfigValues")
    private MstarBrainSinConfig brainSinConfigValues;
    @SerializedName("m2PrescriptionUploadEnableFlag")
    private boolean m2PrescriptionUploadEnableFlag;
    @SerializedName("m2Coupon")
    private M2Coupon m2Coupon;
    @SerializedName("defaultDeliveryCharges")
    private String defaultDeliveryCharges;
    @SerializedName("onBoarding")
    private List<OnBoardingResult> onBoarding = null;
    @SerializedName("enquiryMobileNo")
    private String enquiryMobileNo;
    @SerializedName("winterBackgroundColor")
    private String winterBackgroundColor;
    @SerializedName("referEarn")
    private ReferEarn referEarn;
    @SerializedName("subscriptionCouponList")
    private SubscriptionCouponList subscriptionCouponList;
    @SerializedName("subscriptionBannersList")
    private List<SubscriptionBannersList> subscriptionBannersList = null;
    @SerializedName("subscriptionStatus")
    private boolean subscriptionStatus;
    @SerializedName("tryAndBuyTemplateId")
    private String tryAndBuyTemplateId;
    @SerializedName("tryAndBuyEnableStatus")
    private boolean tryAndBuyEnableStatus;
    @SerializedName("tryAndBuyTitle")
    private String tryAndBuyTitle;
    @SerializedName("tryAndBuySubTitle")
    private String tryAndBuySubTitle;
    @SerializedName("tryAndBuyDescription")
    private String tryAndBuyDescription;
    @SerializedName("peopleViewTemplateId")
    private String peopleViewTemplateId;
    @SerializedName("refillSectionValues")
    private RefillSectionValues refillSectionValues;
    @SerializedName("refillBlockEnableFlag")
    private boolean refillBlockEnableFlag;
    @SerializedName("referEarnEnableFlag")
    private boolean referEarnEnableFlag;
    @SerializedName("orderSuccessBannerEnableFlag")
    private boolean orderSuccessBannerEnableFlag;
    @SerializedName("orderSuccessBlockBanners")
    private OrderSuccessBlockBanners orderSuccessBlockBanners;
    @SerializedName("welnessSectionFlag")
    private WelnessSectionFlag welnessSectionFlag;
    @SerializedName("diagnosisEnableFlag")
    private boolean diagnosisEnableFlag;
    @SerializedName("consultIconEnableFlag")
    private boolean consultIconEnableFlag;
    @SerializedName("diagnosisText")
    private String diagnosticSamplePickup;
    @SerializedName("m2DeliveryCharges")
    private M2DeliveryCharges m2DeliveryCharges;
    @SerializedName("m2PrescriptionUploadConfigs")
    private PrescriptionUploadConfig m2PrescriptionUploadConfigs;
    @SerializedName("productDetailsFlags")
    private ProductDetailsFlag productDetailsFlags;
    @SerializedName("primeConfig")
    private PrimeConfig primeConfig;
    @SerializedName("consultationUploadConfigs")
    private ConsultationUploadConfigs consultationUploadConfigs;
    @SerializedName("consultationUploadEnableFlag")
    private boolean consultationUploadEnableFlag;
    @SerializedName("deliveryContent")
    private DeliveryContent deliveryContent;
    @SerializedName("orderConfirm")
    private OrderConfirmedPending orderConfirmedPending;
    @SerializedName("transactionFailure")
    private String transactionFailure;
    @SerializedName("diagnosticsContentNew")
    private String diagnosticsNeedHelpContent;
    @SerializedName("algDetail1")
    private String algDetail1;
    @SerializedName("algDetail2")
    private String algDetail2;
    @SerializedName("algDiagdetail1")
    private String algDiagdetail1;
    @SerializedName("algDiagIndex")
    private String algDiagIndex;
    @SerializedName("orderAuthConfirm")
    private OrderAuthConfirm orderAuthConfirm;
    @SerializedName("consultationPopularConcernConfigs")
    private ConsultationPopularConcernConfigs concernConfigs;
    @SerializedName("consultationHomeSliderConfigs")
    private ConsultationHomeSliderConfigs sliderConfigs;
    @SerializedName("outofStockContent")
    private OutOfStockPopupContent outOfStockPopupContent;
    @SerializedName("diagnosticsHomeSliderConfigs")
    private ConsultationHomeSliderConfigs diagnosisSliderConfigs;
    @SerializedName("diagnosticsPopularConcernConfigs")
    private ConsultationPopularConcernConfigs diagnosisConfigs;
    @SerializedName("nmscashTermsConditions")
    private String nmscashTermsConditions;
    @SerializedName("nmssupercashTermsConditions")
    private String nmssupercashTermsConditions;
    @SerializedName("genericCornerAndroidEnableFlag")
    private boolean genericCornerAndroidEnableFlag;
    @SerializedName("buyAgainAndroidEnableDisable")
    private boolean buyAgainAndroidEnableDisable;
    /**
     * Cart Substitution Found alternate product for cart products
     */
    @SerializedName("altCartEnableFlag")
    private boolean altCartEnableFlag;
    /**
     * Alternate Product for Out Of Stock Product in cart screen.
     */

    @SerializedName("cartAlternateEnableFlag")
    private boolean cartAlternateEnableFlag;
    //todo After proper working iof kapchat remove old key(kapture_chat_android) and use this new key(kaptureChatAndroidNew)
    // @SerializedName("kaptureChatAndroidNew")
    @SerializedName("kapture_chat_android")
    private boolean kaptureChatEnable = false;

    @SerializedName("inAppForceUpdateEnableAndroidFlag")
    private boolean inAppForceUpdateEnableAndroidFlag;
    @SerializedName("inAppForceUpdateAndroidVersion")
    private String inAppForceUpdateAndroidVersion;
    @SerializedName("inAppSoftUpdateEnableAndroidFlag")
    private boolean inAppSoftUpdateEnableAndroidFlag;
    @SerializedName("inAppSoftUpdateAndroidVersion")
    private String inAppSoftUpdateAndroidVersion;
    @SerializedName("inAppSoftUpdateTitle")
    private String inAppSoftUpdateTitle;
    @SerializedName("inAppSoftUpdateDesc")
    private String inAppSoftUpdateDesc;
    @SerializedName("subscribtionTerms")
    private String subscriptionTerms;
    @SerializedName("returnProductText")
    private String returnProductText;
    @SerializedName("buyAgainLabelText")
    private String buyAgainLabelText;
    @SerializedName("genericLabelText")
    private String genericLabelText;

    @SerializedName("varientAndroidEnableFlag")
    private boolean varientAndroidEnableFlag;
    @SerializedName("orderReviewTermsCond")
    private String orderReviewTermsAndCondition;
    //For Generics
    @SerializedName("GenericMedicine")
    private MstarGenericConfiguration genericConfiguration;
    @SerializedName("orderDescripText")
    private String orderDescripText;

    // diagnostic order track boolean
    @SerializedName("diagTrackOrderEnableFlag")
    private boolean diagTrackOrderEnableFlag;
    @SerializedName("prodPackTwoText")
    private String prodPackTwoText;
    @SerializedName("prodPackFourText")
    private String prodPackFourText;

    //Rating
    @SerializedName("ratingSuccessPageAndroid")
    private boolean ratingSuccessPageEnabled;
    @SerializedName("ratingMyAccountAndroid")
    private boolean ratingAppMyAccountEnabled;
    @SerializedName("emergencyMsgHomeAndroid")
    private boolean emergencyMessageHomeEnabled;
    @SerializedName("emergencyMsgOrderReviewAndroid")
    private boolean emergencyMessageOrderRevEnabled;
    //Emergency message
    @SerializedName("emergencyHomeTitle")
    private String emergencyHomeTitle;
    @SerializedName("emergencyHomeContent")
    private String emergencyHomeContent;

    @SerializedName("emergencyOrderReviewTitle")
    private String emergencyOrderReviewTitle;
    @SerializedName("emergencyOrderReviewContent")
    private String emergencyOrderReviewContent;

    public boolean isDiagTrackOrderEnableFlag() {
        return diagTrackOrderEnableFlag;
    }

    public void setDiagTrackOrderEnableFlag(boolean diagTrackOrderEnableFlag) {
        this.diagTrackOrderEnableFlag = diagTrackOrderEnableFlag;
    }

    public boolean isConsultIconEnableFlag() {
        return consultIconEnableFlag;
    }

    public void setConsultIconEnableFlag(boolean consultIconEnableFlag) {
        this.consultIconEnableFlag = consultIconEnableFlag;
    }

    public String getAlgDetail1() {
        return algDetail1;
    }

    public void setAlgDetail1(String algDetail1) {
        this.algDetail1 = algDetail1;
    }

    public String getAlgDetail2() {
        return algDetail2;
    }

    public void setAlgDetail2(String algDetail2) {
        this.algDetail2 = algDetail2;
    }

    public String getAlgDiagdetail1() {
        return algDiagdetail1;
    }

    public void setAlgDiagdetail1(String algDiagdetail1) {
        this.algDiagdetail1 = algDiagdetail1;
    }

    public String getAlgDiagIndex() {
        return algDiagIndex;
    }

    public void setAlgDiagIndex(String algDiagIndex) {
        this.algDiagIndex = algDiagIndex;
    }

    public WelnessSectionFlag getWelnessSectionFlag() {
        return welnessSectionFlag;
    }

    public void setWelnessSectionFlag(WelnessSectionFlag welnessSectionFlag) {
        this.welnessSectionFlag = welnessSectionFlag;
    }

    public boolean getReferEarnEnableFlag() {
        return referEarnEnableFlag;
    }

    public boolean getOrderSuccessBannerEnableFlag() {
        return orderSuccessBannerEnableFlag;
    }

    public void setOrderSuccessBannerEnableFlag(boolean orderSuccessBannerEnableFlag) {
        this.orderSuccessBannerEnableFlag = orderSuccessBannerEnableFlag;
    }

    public OrderSuccessBlockBanners getOrderSuccessBlockBanners() {
        return orderSuccessBlockBanners;
    }

    public void setOrderSuccessBlockBanners(OrderSuccessBlockBanners orderSuccessBlockBanners) {
        this.orderSuccessBlockBanners = orderSuccessBlockBanners;
    }

    public boolean getFetchCommonDetails() {
        return fetchCommonDetails;
    }

    public void setFetchCommonDetails(boolean fetchCommonDetails) {
        this.fetchCommonDetails = fetchCommonDetails;
    }

    public boolean getFetchCountryList() {
        return fetchCountryList;
    }

    public void setFetchCountryList(boolean fetchCountryList) {
        this.fetchCountryList = fetchCountryList;
    }

    public boolean getFetchMasterDataApi() {
        return fetchMasterDataApi;
    }

    public void setFetchMasterDataApi(boolean fetchMasterDataApi) {
        this.fetchMasterDataApi = fetchMasterDataApi;
    }

    public boolean getFetchPaymentConfigApi() {
        return fetchPaymentConfigApi;
    }

    public void setFetchPaymentConfigApi(boolean fetchPaymentConfigApi) {
        this.fetchPaymentConfigApi = fetchPaymentConfigApi;
    }

    public Integer getCommonDetailsCacheTimeHrs() {
        return commonDetailsCacheTimeHrs;
    }

    public void setCommonDetailsCacheTimeHrs(Integer commonDetailsCacheTimeHrs) {
        this.commonDetailsCacheTimeHrs = commonDetailsCacheTimeHrs;
    }

    public Integer getCountryListCacheTimeHrs() {
        return countryListCacheTimeHrs;
    }

    public void setCountryListCacheTimeHrs(Integer countryListCacheTimeHrs) {
        this.countryListCacheTimeHrs = countryListCacheTimeHrs;
    }

    public Integer getUploadPrescriptionLimit() {
        return uploadPrescriptionLimit;
    }

    public void setUploadPrescriptionLimit(Integer uploadPrescriptionLimit) {
        this.uploadPrescriptionLimit = uploadPrescriptionLimit;
    }

    public String getAndroidForceUpdateVersion() {
        return androidForceUpdateVersion;
    }

    public void setAndroidForceUpdateVersion(String androidForceUpdateVersion) {
        this.androidForceUpdateVersion = androidForceUpdateVersion;
    }

    public String getIosForceUpdateVersion() {
        return iosForceUpdateVersion;
    }

    public void setIosForceUpdateVersion(String iosForceUpdateVersion) {
        this.iosForceUpdateVersion = iosForceUpdateVersion;
    }

    public String getAppUpdateTitle() {
        return appUpdateTitle;
    }

    public void setAppUpdateTitle(String appUpdateTitle) {
        this.appUpdateTitle = appUpdateTitle;
    }

    public String getAppUpdateMesssage() {
        return appUpdateMesssage;
    }

    public void setAppUpdateMesssage(String appUpdateMesssage) {
        this.appUpdateMesssage = appUpdateMesssage;
    }

    public String getMagentoImageUrl() {
        return magentoImageUrl;
    }

    public void setMagentoImageUrl(String magentoImageUrl) {
        this.magentoImageUrl = magentoImageUrl;
    }

    public String getAlgoliaMagentoImageUrl() {
        return algoliaMagentoImageUrl;
    }

    public void setAlgoliaMagentoImageUrl(String algoliaMagentoImageUrl) {
        this.algoliaMagentoImageUrl = algoliaMagentoImageUrl;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPinCodeUrl() {
        return pinCodeUrl;
    }

    public void setPinCodeUrl(String pinCodeUrl) {
        this.pinCodeUrl = pinCodeUrl;
    }

    public String getStateCityUrl() {
        return stateCityUrl;
    }

    public void setStateCityUrl(String stateCityUrl) {
        this.stateCityUrl = stateCityUrl;
    }

    public String getReferralTerms() {
        return referralTerms;
    }

    public void setReferralTerms(String referralTerms) {
        this.referralTerms = referralTerms;
    }

    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    public String getShippingCarrierCode() {
        return shippingCarrierCode;
    }

    public void setShippingCarrierCode(String shippingCarrierCode) {
        this.shippingCarrierCode = shippingCarrierCode;
    }

    public String getDeliveryDateApi() {
        return deliveryDateApi;
    }

    public void setDeliveryDateApi(String deliveryDateApi) {
        this.deliveryDateApi = deliveryDateApi;
    }

    public String getWebEngageKey() {
        return webEngageKey;
    }

    public void setWebEngageKey(String webEngageKey) {
        this.webEngageKey = webEngageKey;
    }

    public String getEmailApiUrl() {
        return emailApiUrl;
    }

    public void setEmailApiUrl(String emailApiUrl) {
        this.emailApiUrl = emailApiUrl;
    }

    public String getEmailApiKey() {
        return emailApiKey;
    }

    public void setEmailApiKey(String emailApiKey) {
        this.emailApiKey = emailApiKey;
    }

    public String getSmsApiUrl() {
        return smsApiUrl;
    }

    public void setSmsApiUrl(String smsApiUrl) {
        this.smsApiUrl = smsApiUrl;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public boolean getWinterSaleActiveFlag() {
        return winterSaleActiveFlag;
    }

    public void setWinterSaleActiveFlag(boolean winterSaleActiveFlag) {
        this.winterSaleActiveFlag = winterSaleActiveFlag;
    }

    public boolean getWellnessProductsActiveFlag() {
        return wellnessProductsActiveFlag;
    }

    public void setWellnessProductsActiveFlag(boolean wellnessProductsActiveFlag) {
        this.wellnessProductsActiveFlag = wellnessProductsActiveFlag;
    }

    public boolean getGatewayOffersActiveFlag() {
        return gatewayOffersActiveFlag;
    }

    public void setGatewayOffersActiveFlag(boolean gatewayOffersActiveFlag) {
        this.gatewayOffersActiveFlag = gatewayOffersActiveFlag;
    }

    public boolean getPromotionBannerFlag() {
        return promotionBannerFlag;
    }

    public void setPromotionBannerFlag(boolean promotionBannerFlag) {
        this.promotionBannerFlag = promotionBannerFlag;
    }

    public boolean getFeaturedArticleFlag() {
        return featuredArticleFlag;
    }

    public void setFeaturedArticleFlag(boolean featuredArticleFlag) {
        this.featuredArticleFlag = featuredArticleFlag;
    }

    public List<CancelOrderReason> getCancelOrderReason() {
        return cancelOrderReason;
    }

    public void setCancelOrderReason(List<CancelOrderReason> cancelOrderReason) {
        this.cancelOrderReason = cancelOrderReason;
    }

    public boolean getBrainsinStatusFlag() {
        return brainsinStatusFlag;
    }

    public void setBrainsinStatusFlag(boolean brainsinStatusFlag) {
        this.brainsinStatusFlag = brainsinStatusFlag;
    }

    public M2Coupon getM2Coupon() {
        return m2Coupon;
    }

    public void setM2Coupon(M2Coupon m2Coupon) {
        this.m2Coupon = m2Coupon;
    }

    public String getDefaultDeliveryCharges() {
        return defaultDeliveryCharges;
    }

    public void setDefaultDeliveryCharges(String defaultDeliveryCharges) {
        this.defaultDeliveryCharges = defaultDeliveryCharges;
    }

    public List<OnBoardingResult> getOnBoarding() {
        return onBoarding;
    }

    public void setOnBoarding(List<OnBoardingResult> onBoarding) {
        this.onBoarding = onBoarding;
    }

    public String getEnquiryMobileNo() {
        return enquiryMobileNo;
    }

    public void setEnquiryMobileNo(String enquiryMobileNo) {
        this.enquiryMobileNo = enquiryMobileNo;
    }

    public String getWinterBackgroundColor() {
        return winterBackgroundColor;
    }

    public void setWinterBackgroundColor(String winterBackgroundColor) {
        this.winterBackgroundColor = winterBackgroundColor;
    }

    public SubscriptionCouponList getSubscriptionCouponList() {
        return subscriptionCouponList;
    }

    public void setSubscriptionCouponList(SubscriptionCouponList subscriptionCouponList) {
        this.subscriptionCouponList = subscriptionCouponList;
    }

    public List<SubscriptionBannersList> getSubscriptionBannersList() {
        return subscriptionBannersList;
    }

    public void setSubscriptionBannersList(List<SubscriptionBannersList> subscriptionBannersList) {
        this.subscriptionBannersList = subscriptionBannersList;
    }

    public ReferEarn getReferEarn() {
        return referEarn;
    }

    public void setReferEarn(ReferEarn referEarn) {
        this.referEarn = referEarn;
    }

    public boolean getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(boolean subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getTryAndBuyTemplateId() {
        return tryAndBuyTemplateId;
    }

    public void setTryAndBuyTemplateId(String tryAndBuyTemplateId) {
        this.tryAndBuyTemplateId = tryAndBuyTemplateId;
    }

    public boolean getTryAndBuyEnableStatus() {
        return tryAndBuyEnableStatus;
    }

    public void setTryAndBuyEnableStatus(boolean tryAndBuyEnableStatus) {
        this.tryAndBuyEnableStatus = tryAndBuyEnableStatus;
    }

    public String getTryAndBuyTitle() {
        return tryAndBuyTitle;
    }

    public void setTryAndBuyTitle(String tryAndBuyTitle) {
        this.tryAndBuyTitle = tryAndBuyTitle;
    }

    public String getTryAndBuySubTitle() {
        return tryAndBuySubTitle;
    }

    public void setTryAndBuySubTitle(String tryAndBuySubTitle) {
        this.tryAndBuySubTitle = tryAndBuySubTitle;
    }

    public String getTryAndBuyDescription() {
        return tryAndBuyDescription;
    }

    public void setTryAndBuyDescription(String tryAndBuyDescription) {
        this.tryAndBuyDescription = tryAndBuyDescription;
    }

    public String getPeopleViewTemplateId() {
        return peopleViewTemplateId;
    }

    public void setPeopleViewTemplateId(String peopleViewTemplateId) {
        this.peopleViewTemplateId = peopleViewTemplateId;
    }

    public RefillSectionValues getRefillSectionValues() {
        return refillSectionValues;
    }

    public void setRefillSectionValues(RefillSectionValues refillSectionValues) {
        this.refillSectionValues = refillSectionValues;
    }

    public boolean isRefillBlockEnableFlag() {
        return refillBlockEnableFlag;
    }

    public void setRefillBlockEnableFlag(boolean refillBlockEnableFlag) {
        this.refillBlockEnableFlag = refillBlockEnableFlag;
    }

    public boolean isDiagnosisEnableFlag() {
        return diagnosisEnableFlag;
    }

    public void setDiagnosisEnableFlag(boolean diagnosisEnableFlag) {
        this.diagnosisEnableFlag = diagnosisEnableFlag;
    }

    public String getDiagnosticSamplePickup() {
        return diagnosticSamplePickup;
    }

    public void setDiagnosticSamplePickup(String diagnosticSamplePickup) {
        this.diagnosticSamplePickup = diagnosticSamplePickup;
    }

    public M2DeliveryCharges getM2DeliveryCharges() {
        return m2DeliveryCharges;
    }

    public void setM2DeliveryCharges(M2DeliveryCharges m2DeliveryCharges) {
        this.m2DeliveryCharges = m2DeliveryCharges;
    }

    public PrescriptionUploadConfig getM2PrescriptionUploadConfigs() {
        return m2PrescriptionUploadConfigs;
    }

    public void setM2PrescriptionUploadConfigs(PrescriptionUploadConfig m2PrescriptionUploadConfigs) {
        this.m2PrescriptionUploadConfigs = m2PrescriptionUploadConfigs;
    }

    public ProductDetailsFlag getProductDetailsFlags() {
        return productDetailsFlags;
    }

    public void setProductDetailsFlags(ProductDetailsFlag productDetailsFlags) {
        this.productDetailsFlags = productDetailsFlags;
    }

    public PrimeConfig getPrimeConfig() {
        return primeConfig;
    }

    public void setPrimeConfig(PrimeConfig primeConfig) {
        this.primeConfig = primeConfig;
    }

    public ConsultationUploadConfigs getConsultationUploadConfigs() {
        return consultationUploadConfigs;
    }

    public void setConsultationUploadConfigs(ConsultationUploadConfigs consultationUploadConfigs) {
        this.consultationUploadConfigs = consultationUploadConfigs;
    }

    public boolean getConsultationUploadEnableFlag() {
        return consultationUploadEnableFlag;
    }

    public void setConsultationUploadEnableFlag(boolean consultationUploadEnableFlag) {
        this.consultationUploadEnableFlag = consultationUploadEnableFlag;
    }

    public OrderConfirmedPending getOrderConfirmedPending() {
        return orderConfirmedPending;
    }

    public void setOrderConfirmedPending(OrderConfirmedPending orderConfirmedPending) {
        this.orderConfirmedPending = orderConfirmedPending;
    }

    public String getDiagnosticsNeedHelpContent() {
        return diagnosticsNeedHelpContent;
    }

    public void setDiagnosticsNeedHelpContent(String diagnosticsNeedHelpContent) {
        this.diagnosticsNeedHelpContent = diagnosticsNeedHelpContent;
    }

    public DeliveryContent getDeliveryContent() {
        return deliveryContent;
    }

    public void setDeliveryContent(DeliveryContent deliveryContent) {
        this.deliveryContent = deliveryContent;
    }

    public String getDiagnosticsContent() {
        return diagnosticsNeedHelpContent;
    }

    public void setDiagnosticsContent(String diagnosticsContent) {
        this.diagnosticsNeedHelpContent = diagnosticsContent;
    }

    public String getTransactionFailure() {
        return transactionFailure;
    }

    public void setTransactionFailure(String transactionFailure) {
        this.transactionFailure = transactionFailure;
    }

    public OrderAuthConfirm getOrderAuthConfirm() {
        return orderAuthConfirm;
    }

    public void setOrderAuthConfirm(OrderAuthConfirm orderAuthConfirm) {
        this.orderAuthConfirm = orderAuthConfirm;
    }

    public void setConcernConfigs(ConsultationPopularConcernConfigs concernConfigs) {
        this.concernConfigs = concernConfigs;
    }

    public ConsultationPopularConcernConfigs getConcernConfigs() {
        return concernConfigs;
    }

    public ConsultationHomeSliderConfigs getSliderConfigs() {
        return sliderConfigs;
    }

    public void setSliderConfigs(ConsultationHomeSliderConfigs sliderConfigs) {
        this.sliderConfigs = sliderConfigs;
    }

    public OutOfStockPopupContent getOutOfStockPopupContent() {
        return outOfStockPopupContent;
    }

    public void setOutOfStockPopupContent(OutOfStockPopupContent outOfStockPopupContent) {
        this.outOfStockPopupContent = outOfStockPopupContent;
    }

    public ConsultationPopularConcernConfigs getDiagnosisConfigs() {
        return diagnosisConfigs;
    }

    public void setDiagnosisConfigs(ConsultationPopularConcernConfigs diagnosisConfigs) {
        this.diagnosisConfigs = diagnosisConfigs;
    }

    public ConsultationHomeSliderConfigs getDiagnosisSliderConfigs() {
        return diagnosisSliderConfigs;
    }

    public void setDiagnosisSliderConfigs(ConsultationHomeSliderConfigs diagnosisSliderConfigs) {
        this.diagnosisSliderConfigs = diagnosisSliderConfigs;
    }

    public String getNmscashTermsConditions() {
        return nmscashTermsConditions;
    }

    public void setNmscashTermsConditions(String nmscashTermsConditions) {
        this.nmscashTermsConditions = nmscashTermsConditions;
    }

    public String getNmssupercashTermsConditions() {
        return nmssupercashTermsConditions;
    }

    public void setNmssupercashTermsConditions(String nmssupercashTermsConditions) {
        this.nmssupercashTermsConditions = nmssupercashTermsConditions;
    }

    public AlgoliaCredentialDetail getAlgoliaCredentialDetail() {
        return algoliaCredentialDetail;
    }

    public void setAlgoliaCredentialDetail(AlgoliaCredentialDetail algoliaCredentialDetail) {
        this.algoliaCredentialDetail = algoliaCredentialDetail;
    }

    public boolean isBrainsinStatusFlag() {
        return brainsinStatusFlag;
    }

    public String getBrainsinsKey() {
        return brainsinsKey;
    }

    public void setBrainsinsKey(String brainsinsKey) {
        this.brainsinsKey = brainsinsKey;
    }

    public MstarBrainSinConfig getBrainSinConfigValues() {
        return brainSinConfigValues;
    }

    public void setBrainSinConfigValues(MstarBrainSinConfig brainSinConfigValues) {
        this.brainSinConfigValues = brainSinConfigValues;
    }

    public boolean isM2PrescriptionUploadEnableFlag() {
        return m2PrescriptionUploadEnableFlag;
    }

    public void setM2PrescriptionUploadEnableFlag(boolean m2PrescriptionUploadEnableFlag) {
        this.m2PrescriptionUploadEnableFlag = m2PrescriptionUploadEnableFlag;
    }

    public boolean isAltCartEnableFlag() {
        return altCartEnableFlag;
    }

    public void setAltCartEnableFlag(boolean altCartEnableFlag) {
        this.altCartEnableFlag = altCartEnableFlag;
    }

    public boolean isCartAlternateEnableFlag() {
        return cartAlternateEnableFlag;
    }

    public void setCartAlternateEnableFlag(boolean cartAlternateEnableFlag) {
        this.cartAlternateEnableFlag = cartAlternateEnableFlag;
    }

    public boolean isKaptureChatEnable() {
        return kaptureChatEnable;
    }

    public void setKaptureChatEnable(boolean kaptureChatEnable) {
        this.kaptureChatEnable = kaptureChatEnable;
    }

    public boolean isInAppForceUpdateEnableAndroidFlag() {
        return inAppForceUpdateEnableAndroidFlag;
    }

    public void setInAppForceUpdateEnableAndroidFlag(boolean inAppForceUpdateEnableAndroidFlag) {
        this.inAppForceUpdateEnableAndroidFlag = inAppForceUpdateEnableAndroidFlag;
    }

    public boolean isInAppSoftUpdateEnableAndroidFlag() {
        return inAppSoftUpdateEnableAndroidFlag;
    }

    public void setInAppSoftUpdateEnableAndroidFlag(boolean inAppSoftUpdateEnableAndroidFlag) {
        this.inAppSoftUpdateEnableAndroidFlag = inAppSoftUpdateEnableAndroidFlag;
    }

    public String getInAppForceUpdateAndroidVersion() {
        return inAppForceUpdateAndroidVersion;
    }

    public void setInAppForceUpdateAndroidVersion(String inAppForceUpdateAndroidVersion) {
        this.inAppForceUpdateAndroidVersion = inAppForceUpdateAndroidVersion;
    }


    public String getInAppSoftUpdateAndroidVersion() {
        return inAppSoftUpdateAndroidVersion;
    }

    public void setInAppSoftUpdateAndroidVersion(String inAppSoftUpdateAndroidVersion) {
        this.inAppSoftUpdateAndroidVersion = inAppSoftUpdateAndroidVersion;
    }

    public String getInAppSoftUpdateTitle() {
        return inAppSoftUpdateTitle;
    }

    public void setInAppSoftUpdateTitle(String inAppSoftUpdateTitle) {
        this.inAppSoftUpdateTitle = inAppSoftUpdateTitle;
    }

    public String getInAppSoftUpdateDesc() {
        return inAppSoftUpdateDesc;
    }

    public void setInAppSoftUpdateDesc(String inAppSoftUpdateDesc) {
        this.inAppSoftUpdateDesc = inAppSoftUpdateDesc;
    }

    public String getSubscriptionTerms() {
        return subscriptionTerms;
    }

    public String getReturnProductText() {
        return returnProductText;
    }

    public boolean isGenericCornerAndroidEnableFlag() {
        return genericCornerAndroidEnableFlag;
    }

    public boolean isBuyAgainAndroidEnableDisable() {
        return buyAgainAndroidEnableDisable;
    }

    public String getBuyAgainLabelText() {
        return buyAgainLabelText;
    }

    public String getGenericLabelText() {
        return genericLabelText;
    }


    public boolean isVarientAndroidEnableFlag() {
        return varientAndroidEnableFlag;
    }

    public void setVarientAndroidEnableFlag(boolean varientAndroidEnableFlag) {
        this.varientAndroidEnableFlag = varientAndroidEnableFlag;
    }

    public MstarGenericConfiguration getGenericConfiguration() {
        return genericConfiguration;
    }

    public void setGenericConfiguration(MstarGenericConfiguration genericConfiguration) {
        this.genericConfiguration = genericConfiguration;
    }

    public String getProdPackTwoText() {
        return prodPackTwoText;
    }

    public String getProdPackFourText() {
        return prodPackFourText;
    }

    public String getOrderReviewTermsAndCondition() {
        return orderReviewTermsAndCondition;
    }

    public void setOrderReviewTermsAndCondition(String orderReviewTermsAndCondition) {
        this.orderReviewTermsAndCondition = orderReviewTermsAndCondition;
    }

    public String getorderDescripText() {
        return orderDescripText;
    }

    public boolean isRatingSuccessPageEnabled() {
        return ratingSuccessPageEnabled;
    }

    public void setRatingSuccessPageEnabled(boolean ratingSuccessPageEnabled) {
        this.ratingSuccessPageEnabled = ratingSuccessPageEnabled;
    }

    public boolean isRatingAppMyAccountEnabled() {
        return ratingAppMyAccountEnabled;
    }

    public void setRatingAppMyAccountEnabled(boolean ratingAppMyAccountEnabled) {
        this.ratingAppMyAccountEnabled = ratingAppMyAccountEnabled;
    }

    public boolean isEmergencyMessageHomeEnabled() {
        return emergencyMessageHomeEnabled;
    }

    public void setEmergencyMessageHomeEnabled(boolean emergencyMessageHomeEnabled) {
        this.emergencyMessageHomeEnabled = emergencyMessageHomeEnabled;
    }

    public boolean isEmergencyMessageOrderRevEnabled() {
        return emergencyMessageOrderRevEnabled;
    }

    public void setEmergencyMessageOrderRevEnabled(boolean emergencyMessageOrderRevEnabled) {
        this.emergencyMessageOrderRevEnabled = emergencyMessageOrderRevEnabled;
    }

    public String getEmergencyHomeTitle() {
        return emergencyHomeTitle;
    }

    public void setEmergencyHomeTitle(String emergencyHomeTitle) {
        this.emergencyHomeTitle = emergencyHomeTitle;
    }

    public String getEmergencyHomeContent() {
        return emergencyHomeContent;
    }

    public void setEmergencyHomeContent(String emergencyHomeContent) {
        this.emergencyHomeContent = emergencyHomeContent;
    }

    public String getEmergencyOrderReviewTitle() {
        return emergencyOrderReviewTitle;
    }

    public void setEmergencyOrderReviewTitle(String emergencyOrderReviewTitle) {
        this.emergencyOrderReviewTitle = emergencyOrderReviewTitle;
    }

    public String getEmergencyOrderReviewContent() {
        return emergencyOrderReviewContent;
    }

    public void setEmergencyOrderReviewContent(String emergencyOrderReviewContent) {
        this.emergencyOrderReviewContent = emergencyOrderReviewContent;
    }
}
