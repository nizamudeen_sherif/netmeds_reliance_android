package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ReviveOrderResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private ReviveOrderResultResponse result;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public ReviveOrderResultResponse getResult() {
        return result;
    }
}
