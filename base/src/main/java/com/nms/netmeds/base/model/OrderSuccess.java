package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class OrderSuccess {
    private String header;
    @SerializedName("subTitle")
    private String subTitle;
    @SerializedName("description")
    private String description;
    @SerializedName("buttonDescription")
    private String buttonDescription;
    @SerializedName("amount")
    private String amount;
    @SerializedName("referAmountImg")
    private String referAmountImg;
    @SerializedName("referImg")
    private String referImg;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getButtonDescription() {
        return buttonDescription;
    }

    public void setButtonDescription(String buttonDescription) {
        this.buttonDescription = buttonDescription;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReferAmountImg() {
        return referAmountImg;
    }

    public void setReferAmountImg(String referAmountImg) {
        this.referAmountImg = referAmountImg;
    }

    public String getReferImg() {
        return referImg;
    }

    public void setReferImg(String referImg) {
        this.referImg = referImg;
    }

}
