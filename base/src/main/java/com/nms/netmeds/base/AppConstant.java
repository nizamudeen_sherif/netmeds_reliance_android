package com.nms.netmeds.base;

public class AppConstant {

    public static final int ALTERNATE_CART_REQUEST_CODE = 0115;
    public static final int MSTAR_PRIME_PRODUCT_CODE_6_MON = 2001;
    public static final int MSTAR_PRIME_PRODUCT_CODE_12_MON = 2002;

    public static final String IN = "IN";
    public static final String NO = "N";
    public static final String YES = "Y";
    public static final String KP = "KP";
    public static final String OUT_OF_STACK = "Out of Stock";
    public static final String NOTIFY_ME = "Notify me";
    public static final String OUT_OF_STOCK_STATUS = "S";
    public static final String NOT_AVAILABLE_STATUS_C = "C";
    public static final String NOT_AVAILABLE_STATUS_T = "T";
    public static final String NOT_FOR_SALE = "R";
    public static final String RETRUN_ORDER = "ARS";
    public static final String PRIME_PRODUCT = "F";

    public static final String API_SUCCESS_STATUS = "Success";
    public static final String API_MESSAGE_STATUS = "User not exists";
    public static final String API_FAILURE_STATUS = "failure";
    public static final String API_FAILURE_REASON_CODE = "MAX_LIMIT_BREACH";
    public static final String API_MSTAR_FAILURE_STATUS = "fail";
    public static final String PLAY_STORE_LINK = "http://play.google.com/store/apps/details?id=%s&hl=en";
    public static final String SUCCESS_STATUS = "success";
    public static final String NO_RECORD_FOUND = "No Record Found";
    public static final String SOURCE_NAME = "app-android";


    public static final String ORDER_PLACE = "Z";
    public static final String AWAITING_PRESCRIPTION = "A";
    public static final String PRESCRIPTION_STATUS_PENDING = "P";
    public static final String OTC_ORDER = "O";
    public static final String ALL = "all";
    public static final String RECENT = "recent";
    public static final String PROCESSING = "S";
    public static final String SHIPPED = "I";
    public static final String DELIVERED = "D";
    public static final String DECLINED = "R";
    public static final String CANCELLED = "C";
    public static final String SUBSCRIPTION_DELIVERY_PENDING = "K";
    public static final String RETURN = "U";
    public static final String INTRANSIT = "I";
    public static final String RESET_OTP_TYPE = "R";
    public static final String GENRAL_OTP_TYPE = "G";

    public static final String M2_ORDER = "P";
    public static final String PRESCRIPTION_NEEDED_Y = "Y";
    public static final String NMS_SUPER_CASH_ID = "1";
    public static final int M2_DELIVERY_CHARGES = 25;
    public static final int DEFAULT_ALGOLIA_HITS = 15;

    public static final String CONSULTATION_SCHEDULED = "N";
    public static final String PAYMENT_PENDING = "N";
    public static final String PAYMENT_RECEIVED = "M";
    public static final String CONSULTATION_SCHEDULED_DESC = "Consultation Scheduled";
    public static final String CONFIRMATION_PENDING_DESC = "Confirmation Pending";
    public static final String PAYMENT_PENDING_DESC = "Payment Pending";
    public static final String PREPAID = "Prepaid";
    public static final String OPNAME = "opName";
    public static final String CARDTOKEN = "cardToken";
    public static final String WALLET_ID = "walletId";
    public static final String WALLET_NAME = "walletName";
    public static final String DE_LINK_WALLET = "delinkWallet";
    public static final String DELETE_CARD = "deleteCard";
    public static final String PAYTM_TITLE = "PAYTM";
    public static final String OFFER_COUPON = "CouponCode";
    public static final String AMAZONPAY_TITLE = "AMAZONPAY";
    public static final String SUFFIX_NETMEDS = "netmeds_";
    public static final String LINKED_WALLET = "LINKED_WALLET";
    public static final String SAVED_CARDS = "SAVED_CARDS";
    public static final String MSTAR_ORDER_ID = "order_id";
    public static final String LOGIN_NAME_QUERY = "&Login_Name=";
    public static final String M3_UNSUBSCRIBE_ORDER_STATUS = "Active";
    public static final String M3_PAYNOW_ORDER_STATUS = "Paynow";
    public static final String M3_TRACK_ORDER_ORDER_STATUS = "TrackOrder";

    public static final String PRODUCT_OUT_OF_STOCK = "S";
    public static final String PRODUCT_AVAILABLE_CODE = "A";
    public static final String PRODUCT_NOT_AVAILABLE_CODE_C = "C";
    public static final String PRODUCT_NOT_AVAILABLE_CODE_R = "R";
    public static final String PRODUCT_NOT_AVAILABLE_CODE_T = "T";
    public static final String MFR = "Mfr: ";
    public static final String CREDIT = "Credit";

    public static final String CC_USER_NAME = "username";
    public static final String CC_PASSWORD = "password";
    public static final String CC_GRAND_TYPE = "grant_type";
    public static final String NA = "NA";
    public static final String TEL = "tel:";

    public static final String DEFAULT_DOB = "01/01/2000";
    public static final int DEFAULT_GENDER = 3;
    public static final String SUBSCRIPTION_CONFIRMED = "SUBSCRIPTION CONFIRMED";

    //Config Constants
    public static final String ALGOLIA_HITS = "ALGOLIA_HITS";
    public static final String ALGOLIA_API_KEY = "ALGOLIA_API_KEY";
    public static final String ALGOLIA_INDEX = "ALGOLIA_INDEX";
    public static final String ALGOLIA_APP_ID = "ALGOLIA_APP_ID";
    public static final CharSequence SRF = "SRF";
    public static final String SUBSCRIPTION = "Subscription";
    public static final String CONSULTATION = "Consultation";

    public static final String YOUTUBE = "youtube";

    public static final String OTC_SCHEDULE_TYPE_F = "F";
    public static final String OTC_SCHEDULE_TYPE_M = "M";
    public static final String OTC_SCHEDULE_TYPE_O = "O";
    public static final String DRUG_TYPE_P = "P";
    public static final String DRUG_TYPE_O = "O";
    public static final String DRUG_TYPE_NP = "NP";

    public static final String BRAIN_SINS_PRODUCT_MAX_COUNT = "3";
    public static final String EXTRA_PATH_PARAMS = "extraPathParams";
    public static final int PRIME_REQUEST_CODE = 100;
    public static final String NETMEDS_FIRST_ORDER_ID_S = "S";

    public static final int NOT_FOUND_ERROR_CODE = 404;
    public static final String S_FLAG_MEDICINE = "M";
    public static final String S_FLAG_DIAGNOSTICS = "D";
    public static final String S_FLAG_CONSULTATION = "C";
    public static final String PAYMENT_CONFIRMATION_PENDING_DESC = "Payment Confirmation Pending";
    public static final String ORDER_RECEIVED_DESC = "Order Received";
    public static final String PAYMENT_FAILED_DESC = "Payment Failed";
    public static final String MEDICINE = "Medicine";
    public static final String DIAGNOSTICS = "Diagnostics";
    public static final String VOUCHER_NAME = "GEN";
    public static final String BOOMRANG_AUTH_TOKEN = "authtoken";
    public static final String GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    public static final String MID = "mId";
    public static final String FROM_RADIO = "from_radio";
    public static final String KEY_OUT_OF_STOCK_LIST = "OUT_OF_STOCK_LIST";
    public static final String KEY_AUTHORIZATION = "Authorization";
    public static final String KEY_SESSION = "session";
    public static final String KEY_BEARER = "Bearer ";
    public static final String KEY_IS_FROM_CART = "IS_FROM_CART";
    public static final String BANNER_TITLE = "BANNER_TITLE";
    public static final String SESSION_ID = "sessionId";
    public static final String CID = "cId";
    public static final String PAY_MODE = "payMode";
    public static final String TEST_IDS = "testIds";
    public static final String COD = "COD";


    public static final String CHEAPER_WITH_PERCENT = "% Cheaper";


    //API Path Keys
    public static final String VAL = "val";
    public static final String QUERY_PRODUCT_CODE = "product_code";
    public static final String QUERY_PRODUCT_QUANTITY = "qty";
    public static final String QUERY_COUPON_CODE = "coupon_code";
    public static final String QUERY_ADDRESS_ID = "address_id";
    public static final String QUERY_ADDR_ID = "addr_id";
    public static final String QUERY_AMT = "amt";
    public static final String MOBILE_NO = "mobile_no";
    public static final String RK = "rk";
    public static final String PASSWORD = "passwd";
    public static final String OTP = "otp";
    public static final String CART_ID = "cart_id";
    public static final String CARTID = "cartId";
    public static final String NEW_METHOD2_CART = "new_method2_cart";
    public static final String MSTAR_CHANNEL = "channel";
    public static final String MSTAR_CHANNEL_NAME = "app-android";
    public static final String PATIENT_NAME = "patientname";
    public static final String CUSTOMER_PHONE = "customerphone";
    public static final String PRODUCT_NAME = "productName";
    public static final String HIGH_PRODUCT_NAME = "productname";
    public static final String FULL_NAME = "fullName";
    public static final String ACCOUNT_EMAIL = "accountEmail";
    public static final String ADDRESS = "address";
    public static final String PHONE = "phone";
    public static final String OFFER_APPLIED = "F,M,O";
    public static final String PAYMENT_METHOD_TYPE = "paymentMethodType";
    public static final String MSTAR_CUSTOMER_ID = "customer_id";
    public static final String MSTAR_FINGERPRINT = "fingerprint";

    //API Request Body of type form-data
    public static final String PID = "pid";
    public static final String PIMG = "pimg";
    public static final String MERGE_SESSION = "merge_session";

    //Digitalized API Request
    public static final String PAGE_INDEX_RX = "1";
    public static final String PAGE_SIZE_RX = "12";
    public static final String MODE_RX = "";


    //API Header keys
    public static final String MSTAR_AUTH_TOKEN = "authtoken";
    public static final String MSTAR_API_HEADER_USER_ID = "userid";


    //Others
    public static final String CART_STATUS_OPEN = "open";
    public static final int MSTAR_ADDRESS_REQUEST_CODE = 9001;
    public static final String PRODUCT_FIELD_SET_NAME = "product_fieldset_name";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_ID_LIST = "id_list";

    public static final String MSTAR_CATEGORY_FIELDSET_NAME_MGF = "mfgr";
    public static final String MSTAR_CATEGORY_ID = "category_id";
    public static final String MSTAR_MANUFACTURER_ID = "mfgr_id";
    public static final String MSTAR_BRAND_ID = "brand_id";
    public static final String RETURN_PRODUCTS = "return_products";
    public static final String RETURN_FACETS = "return_facets";
    public static final String PRODUCT_SORT_ORDER = "products_sort_order";
    public static final String PAGE_NO = "page_no";
    public static final String PAGE_SIZE = "page_size";
    public static final String SUB_CATEGORY_DEPTH = "sub_category_depth";
    public static final String NAME_ASCENDING = "name-asc";
    public static final String NAME_DECENDING = "name-desc";
    public static final String NAME_DISC = "disc";
    public static final String NAME_POPULARITY = "popularity";
    public static final String MSTAR_IMAGE_SIZE_75 = "75x75";
    public static final String MSTAR_IMAGE_SIZE_120 = "120x120";
    public static final String MSTAR_IMAGE_SIZE_150 = "150x150";
    public static final String MSTAR_IMAGE_SIZE_240 = "240x240";
    public static final String MSTAR_IMAGE_SIZE_600 = "600x600";
    public static final String VOUCHER_CODE = "voucher_code";
    public static final String PRESCRIPTION_ATTACHED = "ATTACHED";
    public static final String PRESCRIPTION_UPLOADED = "UPLOADED";
    public static final String WALLET = "wallet";
    public static final String NMS_CASH = "nms_cash";
    public static final String NMS_SUPER_CASH = "super_cash";
    public static final String ORDER_VALUE = "orderValue";
    public static final String APP_SOURCE = "appSource";
    public static final String APP_VERSION = "appVersion";
    public static final String AMAZON_FLAG = "amazonFlag";
    public static final String ANDROID = "Android";
    public static final String FALSE = "false";
    public static final String DETACHED = "DETACHED";
    public static final String CHECKING_OUT = "checkingout";
    public static final String URL = "url";
    public static final String PRODUCT_FIELD_SET_NAME_FOR_CATEGORY = "product_fieldset_name_for_category";
    public static final String PRODUCT_FIELD_SET_NAME_FOR_MANUFACTURER = "product_fieldset_name_for_manufacturer";
    public static final String PRODUCT_FIELD_SET_NAME_FOR_PRODUCT = "product_fieldset_name_for_product";
    public static final String PRODUCTS_SELECTOR = "products_selector";
    public static final String NON_PRESC = "nonpresc";
    public static final String MFR_SMALL = "mfr";
    public static final String PRESC = "presc";


    /*Algolia filter constant*/
    public static final String ALGOLIA_FILTER_KEY_IN_STOCK = "in_stock";
    public static final String ALGOLIA_FILTER_KEY_BRAND = "brand";
    public static final String ALGOLIA_FILTER_KEY_MANUFACTURER_NAME = "manufacturer_name";
    public static final String ALGOLIA_FILTER_KEY_CATEGORIES = "categories";
    public static final String ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL = "category_tree.level";
    public static final String ALGOLIA_FILTER_KEY_SELLING_PRICE = "selling_price";
    public static final String ALGOLIA_FILTER_KEY_DISCOUNT_PCT = "discount_pct";
    public static final String EXCLUDE_OUT_OF_STOCK = "Exclude out of stock";
    public static final String ALGOLIA_FILTER_KEY_CATEGORY_IDS = "category_ids";
    public static final String ALGOLIA_FILTER_KEY_MANUFACTURER_ID = "manufacturer_id";
    public static final String ALGOLIA_FILTER_KEY_BRAND_ID = "brand_id";
    public static final String ALGOLIA_FILTER_KEY_GENERIC = "generic";
    public static final String ALGOLIA_FILTER_KEY_FORMULATION_TYPE = "formulation_type";
    public static final String ALGOLIA_FILTER_KEY_AVAILABILITY_STATUS = "availability_status";
    public static final String ALGOLIA_FILTER_KEY_GENERIC_WITH_DOSAGE_ID = "generic_with_dosage_id";
    public static final String ALGOLIA_FILTER_KEY_IS_GEN_SUBS_AVAILABLE = "is_gen_subs_available";

    public static final String CATEGORY = "CATEGORY";
    public static final String BRAND = "BRAND";
    public static final String MANUFACTURER = "MANUFACTURER";
    public static final String PAGE_INDEX = "pageIndex";
    public static final String ORDER_STATUS_TEXT = "orderStatusText";
    public static final String PAGESIZE = "pageSize";
    public static final String ORDER_ID = "orderId";
    public static final String NET_PAYABLE = "netPayable";
    public static final String CUSTOMER_ID = "customerId";
    public static final String MOBILE = "mobilno";
    public static final String REASON = "reason";
    public static final String CUSTOMER_MSG = "customermsg";
    public static final int TWO = 2;
    public static final String OFFER_PAGE = "offerpage";

    public static final String DRUGCODE = "drugCode";
    public static final String DRUG_TYPE = "drugType";
    public static final String CURRENT_PSWRD = "current_passwd";
    public static final String NEW_PASSWORD = "new_passwd";
    public static final String KEY_KIVI_ORDER = "IS_KIVI_ORDER";
    public static final String ALTERNATE_SALT = "ALTERNATE_SALT";
    public static final String PEOPLE_ALSO_VIEWED = "PEOPLE_ALSO_VIEWED";
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final int GUEST_CUSTOMER_ID = 0;
    public static final String REMOVE_STRING_FROM_BASE_URL = "api/v1/";
    public static final String REQUESTRX = "rxRequest";
    public static final String PRESCRIPTION = "prescriptions";
    public static final String AND = "AND";
    public static final String OR = "OR";
    public static final String TO = "TO";
    public static final int ALTERNATE_SALTS_HIT_COUNT = 1000;

    //MORNING STAR
    //API FAILURE RESPONSE CODE
    public static final String MSTAR_OUT_OF_STOCK = "OUT_OF_STOCK";
    public static final String MSTAR_MAX_LIMIT_BREACH = "MAX_LIMIT_BREACH";
    public static final String MSTAR_ADD_PRODUCT_FAILURE = "FAIL_BAD_REQUEST";
    public static final String CHECKOUT_ERROR = "OPERATION_PROHIBITED_WHILE_CHECKINGOUT";
    public static final String CART_NOT_FOUND = "CART_NOT_FOUND";

    //For Gender
    public static final String MALE = "male";
    public static final String FEMALE = "female";
    public static final String OTHERS = "others";

    //For Need Help
    public static final String TITLE_TEXT = "titleTxt";
    public static final String SUBJECT = "subject";
    public static final String FULL_NAME_SMALL = "fullname";
    public static final String DESCRIPTION = "description";
    public static final String EMAIL = "email";

    //For All Offers
    public static final String ID = "id";

    public static final String M1 = "M1";
    public static final String M2 = "M2";

    // Subscription
    public static final String PRIMARY_ORDER_ID = "primary_order_id";
    public static final String DAYS_BETWEEN_ISSUE = "days_between_issues";
    public static final String FROM_PAGE = "from_page";
    public static final String REFILL_NOW = "refill_flow";
    public static final String PAYMENT_SUCCESS = "payment_success";

    public static final String KEY_LINK_TOKEN = "linkToken";
    public static final String KEY_CONTENT_TYPE = "Content-Type";
    public static final String KEY_CONTENT_LENGTH = "Content-Length";
    public static final String KEY_HOST = "Host";
    public static final String PAY_TM_HOST = "accounts-uat.paytm.com";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String PAYMENT_METHOD = "paymentMethod";
    public static final String REQUEST = "request";
    public static final String RESPONSE = "response";
    public static final String APP_SOURCE_NAME = "sourceName";
    public static final String API_DR_CONSUL_NEEDED = "dr_cons_needed";
    public static final String FORCE_NEW = "force_new";
    public static final String ISSUE_ID = "issue_id";
    public static final String ORDER_ID_QUERY = "?order_id=";
    public static final String DOC_CONSULT_NEEDED = "dr_cons_needed";
    public static final String HTTPS = "https:";
    public static final String THIS_ISSUE_ONLY = "this_issue_only";
    public static final String THIS_FUTURE_ONLY = "all_future_issues";
    public static final String EDIT_SCOPE = "edit_scope";
    public static final String SUBSCRIPTION_ISSUE_ID = "issueId";
    public static final String SUBSCRIPTION_PRIMARY_ORDER_ID = "primaryOrderId";
    public static final String SUBSCRIPTION_RESCHEDULED_DAYS = "rescheduledDays";
    public static final String SUBSCRIPTION_IS_FOR_ALL_ORDER = "isForAllOrder";


    public static final String TYPE = "type";
    public static final String C_CATEGORY_ID = "catId";
    public static final String C_MANUFACTURE_ID = "manufId";
    public static final String C_BRAND_ID = "brandId";
    public static final String PINCODE = "pin";
    public static final String PRODUCTID = "productId";
    public static final String SIGNATURE = "signature";
    public static final String PAYLOAD = "payload";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String SOCIAL_FLAG = "socialFlag";
    public static final String SOURCE = "source";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String RANDOM_KEY = "randomkey";
    public static final String USER_NOT_EXISTS = "USERNOTEXISTS";
    public static final String EXISTS = "EXISTS";
    public static final String USER_EXISTS = "USEREXISTS";
    public static final String MANUFACTURE = "manufacture";
    public static final String LEVEL_0 = "level0";
    public static final String LEVEL_1 = "level1";
    public static final String LEVEL_2 = "level2";

    // FireBase Event
    public static final String OTC = "OTC";
    public static final String LOGIN_OTP = "OTP";
    public static final String PHARMA = "PHARMA";
    public static final String NON_PHARMA = "NON_PHARMA";
    public static final String APP_OPEN = "app_open";
    public static final String ADDRESS_SELECTED = "address_selected";
    public static final String NEW_ADDRESS_ADDED = "new_address_added";
    public static final String CHECKOUT_UPLOAD = "upload_doctor_consult_done";
    public static final String CHECKOUT_ORDER_REVIEW = "order_review_done";
    public static final String TRUE_CALLER = "Truecaller";
    public static final String FACEBOOK = "Facebook";
    public static final String GMAIL = "gmail";
    public static final String WHATSAPP = "whatsapp";
    public static final String SMS = "sms";
    public static final String TWITTER = "TWitter";
    public static final String GOOGLE = "Google";
    public static final String FIREBASE_PASSWORD = "Password";
    public static final String PASSWORD_WITH_OTP = "password_with_otp";
    public static final String REFERRAL_SHARE = "Referral share";
    public static final String USER_NAME = "UserName";
    public static final String USER_EMAIL = "Email";
    public static final String USER_PHONE = "Phone Number";
    public static final String USER_ID = "User id";
    public static final String PROD_RELEASE = "prodRelease";
    public static final String KEY_MANUFACTURER = "manufacturer";
    public static final String KEY_PRESCRIPTION = "prescription";
    public static final String KEY_OTC = "otc";

    public static final String NULL = "null";
    public static final String SUBSCRIPTION_PROGRAMME = "Subscription Programme";
    public static final String GRID = "grid";
    public static final String PRODUCTS = "Products";
    public static final String BANNER = "Banner";
    public static final String RX = "Rx";
    public static final String PRODUCT_CODE = "productCode";
    public static final String GENERIC = "G";
    public static final String IN_STOCK = "1";
    public static final String BRANDS_URL = "brands/";
    public static final int CATEGORY_LEVEL_1 = 1;
    public static final int CATEGORY_LEVEL_2 = 2;

    //rating
    public static final String RATING_COUNT = "rateCnt";
    public static final String FEEDBACK = "rateDesc";
    public static final String CHANNEL = "channel";
    public static final String RATING_EVENT = "ratingEvent";
    public static final String FROM_MY_ACCOUNT = "myaccount";
    public static final String FROM_ORDER_SUCCESS = "ordersuccess";
    public static final String RATING_SUCCESS_MESSAGE = "Rating Successfully Submitted";

}
