package com.nms.netmeds.base.retrofit;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nms.netmeds.base.BuildConfig;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApiClient {

    private static BaseApiClient mServiceManager;
    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

    public static BaseApiClient getInstance() {
        if (mServiceManager == null)
            mServiceManager = new BaseApiClient();
        return mServiceManager;
    }

    public Retrofit getClient(String url) {
        Retrofit.Builder builder = getBuilder(url);
        return builder.build();
    }

    private Retrofit.Builder getBuilder(String url) {
        Gson gson = new GsonBuilder().setLenient().disableHtmlEscaping().create();
        return new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create(gson));
    }

    public Retrofit createService(String url, final String authToken) {
        Retrofit retrofit = null;
        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(90, TimeUnit.SECONDS)
                .build();

        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                Retrofit.Builder builder = getBuilder(url);
                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit;
    }

    public Retrofit dynamicService(final String authToken) {
        Retrofit retrofit = null;
        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(90, TimeUnit.SECONDS)
                .build();

        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                Retrofit.Builder builder = getBuilder("http://localhost/");
                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit;
    }

    public Retrofit dynamicClient() {
        return getBuilder("http://localhost/").build();
    }

}
