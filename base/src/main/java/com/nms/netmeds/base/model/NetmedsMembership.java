package com.nms.netmeds.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NetmedsMembership {

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("header")
    @Expose
    public String header;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("memberShipIcons")
    @Expose
    public List<MemberShipIcon> memberShipIcons = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MemberShipIcon> getMemberShipIcons() {
        return memberShipIcons;
    }

    public void setMemberShipIcons(List<MemberShipIcon> memberShipIcons) {
        this.memberShipIcons = memberShipIcons;
    }
}
