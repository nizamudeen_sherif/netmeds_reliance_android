package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class OrderConfirm {

    @SerializedName("title")
    private String title;
    @SerializedName("descp")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
