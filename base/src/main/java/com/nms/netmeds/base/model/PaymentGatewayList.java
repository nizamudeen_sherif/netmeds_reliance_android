package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.PaymentGatewaySubCategory;

import java.util.List;

public class PaymentGatewayList {
    @SerializedName("enabled")
    private boolean enabled;
    @SerializedName("orientation")
    private String orientation;
    @SerializedName("title")
    private String title;
    @SerializedName("sequence")
    private int sequence;
    @SerializedName("image")
    private String image;
    @SerializedName("subList")
    private List<PaymentGatewaySubCategory> subList;
    @SerializedName("id")
    private int id;
    private boolean newCardView = false;

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<PaymentGatewaySubCategory> getSubList() {
        return subList;
    }

    public void setSubList(List<PaymentGatewaySubCategory> subList) {
        this.subList = subList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isNewCardView() {
        return newCardView;
    }

    public void setNewCardView(boolean newCardView) {
        this.newCardView = newCardView;
    }
}
