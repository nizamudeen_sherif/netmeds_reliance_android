package com.nms.netmeds.base;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class BaseUploadPrescription<VM extends BaseViewModel> extends BaseViewModelActivity<VM> {

    public static final String CUSTOMER_ID = "customer_id";
    public static final String CHANNEL = "channel";
    public static final String FILE = "file";
    public static final String SOURCE = "app-android";
    public static final String USER_ID = "userId";
    public static final String MD5SUM = "md5sum";
    public static final String PACKAGE = "package";
    public static final String CID = "cId";

    public void launchCameraIntent(Uri uri) {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getApplication().getPackageManager()) != null) {
            cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
            cameraIntent.setClipData(ClipData.newRawUri("", uri));
            cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(cameraIntent, PermissionConstants.ACTION_IMAGE_CAPTURE);
        }
    }

    public void launchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK);
        pickPhoto.setType(FileUtils.GALLERY_IMAGE_TYPE);
        pickPhoto.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        try {
            startActivityForResult(pickPhoto, PermissionConstants.ACTION_GALLERY_REQUEST);
        } catch (ActivityNotFoundException ignored) {
        }
    }

    public void launchDocumentIntent() {
        Intent documentIntent;
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
            documentIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        else
            documentIntent = new Intent(Intent.ACTION_PICK);

        documentIntent.addCategory(Intent.CATEGORY_OPENABLE);
        documentIntent.setType("*/*");
        documentIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(documentIntent, PermissionConstants.ACTION_DOCUMENT_REQUEST);
    }

    public boolean checkGalleryPermission() {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }


    public boolean checkCameraPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    public void requestPermission(int permissionCode) {
        if (permissionCode == PermissionConstants.GALLERY_PERMISSION || permissionCode == PermissionConstants.DOCUMENT_PERMISSION || permissionCode == PermissionConstants.DOWNLOAD_PERMISSION || permissionCode == PermissionConstants.DOWNLOAD_ATTACHMENT) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, permissionCode);
        } else
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, permissionCode);
    }

    public boolean checkPermissionResponse(int[] grantResults) {
        boolean permissionDenied = true;
        if (grantResults.length > 0) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED)
                    permissionDenied = false;
            }
        }
        return permissionDenied;
    }

    public Uri getUriFromCameraImage(int resultCode) {
        final Uri[] imageUri = {null};
        if (resultCode == Activity.RESULT_OK) {
            Runnable captureImage = new Runnable() {
                @Override
                public void run() {
                    try {
                        ContentValues values = new ContentValues();
                        values.put(android.provider.MediaStore.Images.Media.MIME_TYPE, FileUtils.IMAGE_JPEG);
                        Uri uri = ImageUtils.getImageUri(values, getApplication());
                        FileInputStream fis = new FileInputStream(String.format("%s%s%s", FileUtils.getTempDirectoryPath(getApplication()), "/", FileUtils.TEMP_DIRECTORY_PATH));
                        OutputStream os = null;
                        if (uri != null) {
                            os = getApplication().getContentResolver().openOutputStream(uri);
                        }
                        byte[] buffer = new byte[4096];
                        int len;
                        while ((len = fis.read(buffer)) != -1) {
                            if (os != null) {
                                os.write(buffer, 0, len);
                            }
                        }
                        if (os != null) {
                            os.flush();
                            os.close();
                        }
                        fis.close();
                        imageUri[0] = uri;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            captureImage.run();
        }
        return imageUri[0];
    }

    public Uri getUriFromGalleryImage(int resultCode, Intent data) {
        Uri imageUri = null;
        if (resultCode == Activity.RESULT_OK) {
            Uri photoUri = data.getData();
            if (photoUri != null) {
                try {
                    imageUri = photoUri;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return imageUri;
    }

    public boolean validateUploadedImage(String path, Application application, ViewGroup view, Long uploadSizeLimit, String maxFileSizeError) {
        if (!TextUtils.isEmpty(path) && application != null && view != null) {
            if (!FileUtils.isValidPictureFileExtension(path)) {
                SnackBarHelper.snackBarCallBack(view, application, application.getResources().getString(R.string.text_invalid_image));
                return false;
            } else if (!FileUtils.isValidSize(path, uploadSizeLimit)) {
                SnackBarHelper.snackBarCallBack(view, application, maxFileSizeError);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean validateUploadedDocument(String path, Application application, ViewGroup view, Long uploadSizeLimit) {
        if (!TextUtils.isEmpty(path) && application != null && view != null) {
            if (!FileUtils.isValidDocumentFileExtension(path)) {
                SnackBarHelper.snackBarCallBack(view, application, application.getResources().getString(R.string.text_invalid_document));
                return false;
            } else if (!FileUtils.isValidSize(path, uploadSizeLimit)) {
                SnackBarHelper.snackBarCallBack(view, application, application.getResources().getString(R.string.consultation_max_file_size));
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public void navigateToSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts(PACKAGE, getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }
}
