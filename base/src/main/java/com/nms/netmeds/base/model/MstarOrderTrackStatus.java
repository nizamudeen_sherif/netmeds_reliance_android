package com.nms.netmeds.base.model;

import android.view.View;

import com.google.gson.annotations.SerializedName;

public class MstarOrderTrackStatus {
    @SerializedName("status")
    private String status;
    @SerializedName("shortStatus")
    private String shortStatus;
    @SerializedName("date")
    private String date;

    private int trackIdVisibility = View.GONE;
    private String clickPostTrackId;
    private String trackId;
    private String trackUrl;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShortStatus() {
        return shortStatus;
    }

    public void setShortStatus(String shortStatus) {
        this.shortStatus = shortStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTrackIdVisibility() {
        return trackIdVisibility;
    }

    public void setTrackIdVisibility(int trackIdVisibility) {
        this.trackIdVisibility = trackIdVisibility;
    }

    public String getClickPostTrackId() {
        return clickPostTrackId;
    }

    public void setClickPostTrackId(String clickPostTrackId) {
        this.clickPostTrackId = clickPostTrackId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackUrl() {
        return trackUrl;
    }

    public void setTrackUrl(String trackUrl) {
        this.trackUrl = trackUrl;
    }
}
