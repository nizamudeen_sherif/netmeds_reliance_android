package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstargetAddress {
    @SerializedName("created_time")
    private String createdTime;
    @SerializedName("updated_time")
    private String updatedTime;
    @SerializedName("is_active")
    private Boolean isActive;
    @SerializedName("bo_address_id")
    private Object boAddressId;
    @SerializedName("city")
    private String city;
    @SerializedName("mobile_no")
    private String mobileNo;
    @SerializedName("pin")
    private String pin;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private Integer id;
    @SerializedName("state")
    private String state;
    @SerializedName("customer_id")
    private Integer customerId;
    @SerializedName("line3")
    private String line3;
    @SerializedName("line2")
    private String line2;
    @SerializedName("line1")
    private String line1;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("lastname")
    private String lastname;

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Object getBoAddressId() {
        return boAddressId;
    }

    public void setBoAddressId(Object boAddressId) {
        this.boAddressId = boAddressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
