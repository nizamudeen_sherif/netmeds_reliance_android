package com.nms.netmeds.base.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.nms.netmeds.base.R;

public class CustomNumberPicker extends LinearLayout implements NumberPicker.OnValueChangeListener {

    private int value;
    private int maxValue;
    private int minValue;
    private int stepSize;
    private boolean showLeadingZeros;

    private ImageView buttonMinus;
    private ImageView buttonPlus;
    private TextView textValue;

    private boolean autoIncrement;
    private boolean autoDecrement;

    private int updateInterval;

    private Handler updateIntervalHandler;

    private CustomNumberPickerListener listener;

    public CustomNumberPicker(Context context) {
        this(context, null);
    }

    public CustomNumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (isInEditMode()) {
            return;
        }

        LayoutInflater layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.number_picker, this);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.HorizontalNumberPicker, 0, 0);
        Resources res = getResources();

        initButtonMinus();
        initButtonPlus();

        minValue = typedArray.getInt(R.styleable.HorizontalNumberPicker_minValue,
                res.getInteger(R.integer.default_minValue));
        maxValue = typedArray.getInt(R.styleable.HorizontalNumberPicker_maxValue,
                res.getInteger(R.integer.default_maxValue));

        updateInterval = typedArray.getInt(R.styleable.HorizontalNumberPicker_repeatDelay,
                res.getInteger(R.integer.default_updateInterval));
        stepSize = typedArray.getInt(R.styleable.HorizontalNumberPicker_stepSize,
                res.getInteger(R.integer.default_stepSize));
        showLeadingZeros = typedArray.getBoolean(R.styleable.HorizontalNumberPicker_showLeadingZeros,
                res.getBoolean(R.bool.default_showLeadingZeros));

        initTextValue();
        value = typedArray.getInt(R.styleable.HorizontalNumberPicker_value,
                res.getInteger(R.integer.default_value));
        typedArray.recycle();

        this.setValue();

        autoIncrement = false;
        autoDecrement = false;

        updateIntervalHandler = new Handler();
    }

    private void initTextValue() {
        textValue = findViewById(R.id.text_value);
        textValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                numberPicker();
            }
        });
    }

    private void initButtonPlus() {
        buttonPlus = findViewById(R.id.increment);

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                increment();
            }
        });

        buttonPlus.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                autoIncrement = true;
                updateIntervalHandler.post(new repeat());
                return false;
            }
        });

        buttonPlus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP && autoIncrement) {
                    autoIncrement = false;
                }
                return false;
            }
        });
    }

    private void initButtonMinus() {
        buttonMinus = findViewById(R.id.decrement);

        buttonMinus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                decrement();
            }
        });

        buttonMinus.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                autoDecrement = true;
                updateIntervalHandler.post(new repeat());
                return false;
            }
        });

        buttonMinus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP && autoDecrement) {
                    autoDecrement = false;
                }
                return false;
            }
        });
    }

    public void setDecrementButtonIcon(int drawable) {
        buttonMinus.setImageResource(drawable);
    }

    public void increment() {
        if (value < maxValue) {
            this.setValue(value + stepSize);
        }
        if (listener != null) {
            listener.onHorizontalNumberPickerChanged(CustomNumberPicker.this, value);
        }
    }

    public void decrement() {
        if (value > minValue) {
            this.setValue(value - stepSize);
        }
        if (listener != null) {
            listener.onHorizontalNumberPickerChanged(CustomNumberPicker.this, value);
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        if (value > maxValue) {
            value = maxValue;
        }
        if (value < minValue) {
            value = minValue;
        }

        this.value = value;
        this.setValue();

        if (listener != null) {
            listener.onHorizontalNumberPickerChanged(CustomNumberPicker.this, value);
        }
    }

    private void setValue() {
        String formatter = "%0" + String.valueOf(maxValue).length() + "d";
        textValue.setText(showLeadingZeros ? String.format(formatter, value) : String.valueOf(value));
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        if (maxValue < value) {
            value = maxValue;
            this.setValue();
        }
        if (minValue == this.maxValue) {
            buttonMinus.setEnabled(false);
            buttonPlus.setEnabled(false);
        }
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
        if (minValue > value) {
            value = minValue;
            this.setValue();
        }
    }

    public int getStepSize() {
        return stepSize;
    }

    public void setStepSize(int stepSize) {
        this.stepSize = stepSize;
    }

    public void setShowLeadingZeros(boolean showLeadingZeros) {
        this.showLeadingZeros = showLeadingZeros;

        String formatter = "%0" + String.valueOf(maxValue).length() + "d";
        textValue.setText(showLeadingZeros ? String.format(formatter, value) : String.valueOf(value));
    }

    public void setListener(CustomNumberPickerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
    }

    private class repeat implements Runnable {
        public void run() {
            if (autoIncrement) {
                increment();
                updateIntervalHandler.postDelayed(new repeat(), updateInterval);
            } else if (autoDecrement) {
                decrement();
                updateIntervalHandler.postDelayed(new repeat(), updateInterval);
            }
        }
    }
}

