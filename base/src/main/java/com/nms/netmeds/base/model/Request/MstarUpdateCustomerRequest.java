package com.nms.netmeds.base.model.Request;

import com.google.gson.annotations.SerializedName;

public class MstarUpdateCustomerRequest {
    @SerializedName("firstname")
    private String firstName;
    @SerializedName("lastname")
    private String lastName;
    @SerializedName("email")
    private String email;
    @SerializedName("date_of_birth")
    private String dob;
    @SerializedName("gender")
    private String gender;
    @SerializedName("juspay_id")
    private String juspayId;
    @SerializedName("paytm_customer_token")
    private String paytmCustomerToken;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJuspayId() {
        return juspayId;
    }

    public void setJuspayId(String juspayId) {
        this.juspayId = juspayId;
    }

    public String getPaytmCustomerToken() {
        return paytmCustomerToken;
    }

    public void setPaytmCustomerToken(String paytmCustomerToken) {
        this.paytmCustomerToken = paytmCustomerToken;
    }
}
