package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class ProductPackRulesModel {
    @SerializedName("condition")
    private String condition;
    @SerializedName("qty")
    private int qty;
    @SerializedName("discount_pct")
    private String discountPct;
    @SerializedName("discount")
    private BigDecimal discount = BigDecimal.ZERO;
    @SerializedName("mrp")
    private BigDecimal mrp = BigDecimal.ZERO;
    @SerializedName("selling_price")
    private BigDecimal sellingPrice = BigDecimal.ZERO;

    private String productName;
    private int productCode;
    private String productImageUrl;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDiscountPct() {
        return discountPct +"% OFF";
    }

    public void setDiscountPct(String discountPct) {
        this.discountPct = discountPct;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }
}
