package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MstarBanner implements Serializable {
    @SerializedName(value = "banner", alternate = {"imageName"})
    private String imageName;
    @SerializedName(value = "imageUrl", alternate = {"image"})
    private String imageUrl;
    @SerializedName("id")
    private String id;
    @SerializedName("url")
    private String url;
    @SerializedName("linktype")
    private String linktype;
    @SerializedName("linkpage")
    private String linkpage;
    @SerializedName("newPageId")
    private String newPageId;
    @SerializedName("title")
    private String bannerTitle;
    @SerializedName("displayFrom")
    private String displayFrom;
    @SerializedName("displayTo")
    private String displayTo;
    @SerializedName("name")
    private String name;
    @SerializedName("imgUrl")
    private String imgUrl;
    @SerializedName("level")
    private int level;
    // test/package type
    @SerializedName("type")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //for Default banner
    private String defaultBannerUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageName() {
        return imageName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getLinkpage() {
        return linkpage;
    }

    public void setLinkpage(String linkpage) {
        this.linkpage = linkpage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNewPageId() {
        return newPageId;
    }

    public void setNewPageId(String newPageId) {
        this.newPageId = newPageId;
    }

    public String getBannerTitle() {
        return bannerTitle;
    }

    public void setBannerTitle(String bannerTitle) {
        this.bannerTitle = bannerTitle;
    }

    public String getDisplayFrom() {
        return displayFrom;
    }

    public void setDisplayFrom(String displayFrom) {
        this.displayFrom = displayFrom;
    }

    public String getDisplayTo() {
        return displayTo;
    }

    public void setDisplayTo(String displayTo) {
        this.displayTo = displayTo;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDefaultBannerUrl() {
        return defaultBannerUrl;
    }

    public void setDefaultBannerUrl(String defaultBannerUrl) {
        this.defaultBannerUrl = defaultBannerUrl;
    }
}
