package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.PaymentGatewayList;

import java.util.List;

public class PaymentGatewayResult {
    @SerializedName("list")
    private List<PaymentGatewayList> list;
    @SerializedName("displayName")
    private String displayName;
    @SerializedName("clientID")
    private String clientId;
    @SerializedName("MerchantID")
    private String merchantId;
    @SerializedName("gatewayKey")
    private String gatewayKey;
    @SerializedName("returnUrl")
    private String returnUrl;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getGatewayKey() {
        return gatewayKey;
    }

    public void setGatewayKey(String gatewayKey) {
        this.gatewayKey = gatewayKey;
    }

    public List<PaymentGatewayList> getList() {
        return list;
    }

    public void setList(List<PaymentGatewayList> list) {
        this.list = list;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
}
