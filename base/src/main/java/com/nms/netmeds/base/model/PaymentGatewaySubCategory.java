package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PaymentGatewaySubCategory {
    @SerializedName("id")
    private String id;
    @SerializedName("enabled")
    private boolean enabled;
    //only for card section
    @SerializedName("title")
    private String title;
    //end
    @SerializedName("key")
    private String key;
    @SerializedName("token")
    private String token;
    @SerializedName("displayName")
    private String displayName;
    @SerializedName("sequence")
    private int sequence;
    @SerializedName("image")
    private String image;
    @SerializedName("currentBalance")
    private double currentBalance;
    @SerializedName("offer")
    private String offer;
    @SerializedName("linked")
    private boolean linked;
    @SerializedName("cardType")
    private String cardType;
    @SerializedName("linkToken")
    private String linkToken;
    @SerializedName("showOffer")
    private boolean showOffer;
    @SerializedName("parentId")
    private int parentId;
    @SerializedName("subid")
    private int subId;
    @SerializedName("cardExpYear")
    private String cardExpYear;
    private String cardExpMonth;
    @SerializedName("nameOnCard")
    private String nameOnCard;
    private String walletErrorMsg;
    @SerializedName("maximumAmount")
    private String maximumAmount;
    @SerializedName("minimumAmount")
    private String minimumAmount;
    private boolean childView;
    @SerializedName("cardBrand")
    private String cardBrand;
    @SerializedName("display")
    private boolean display;


    private boolean codEnable;
    private String codMessage;
    private double superCashUsableAmount;
    private boolean isGatewayChecked = false;
    private boolean isShowMessage;
    private boolean showChangeButton;
    private String cardCvv;

    public String getWalletErrorMsg() {
        return walletErrorMsg;
    }

    public void setWalletErrorMsg(String walletErrorMsg) {
        this.walletErrorMsg = walletErrorMsg;
    }

    public boolean isShowOffer() {
        return showOffer;
    }

    public void setShowOffer(boolean showOffer) {
        this.showOffer = showOffer;
    }

    public String getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(String maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public String getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(String minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public boolean isChildView() {
        return childView;
    }

    public void setChildView(boolean childView) {
        this.childView = childView;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public boolean getLinked() {
        return linked;
    }

    public void setLinked(boolean linked) {
        this.linked = linked;
    }

    public boolean isCodEnable() {
        return codEnable;
    }

    public void setCodEnable(boolean codEnable) {
        this.codEnable = codEnable;
    }

    public String getCodMessage() {
        return codMessage;
    }

    public void setCodMessage(String codMessage) {
        this.codMessage = codMessage;
    }

    public double getSuperCashUsableAmount() {
        return superCashUsableAmount;
    }

    public void setSuperCashUsableAmount(double superCashUsableAmount) {
        this.superCashUsableAmount = superCashUsableAmount;
    }

    public boolean isGatewayChecked() {
        return isGatewayChecked;
    }

    public void setGatewayChecked(boolean gatewayChecked) {
        isGatewayChecked = gatewayChecked;
    }

    public boolean isShowMessage() {
        return isShowMessage;
    }

    public void setShowMessage(boolean showMessage) {
        isShowMessage = showMessage;
    }

    public String getLinkToken() {
        return linkToken;
    }

    public void setLinkToken(String linkToken) {
        this.linkToken = linkToken;
    }

    public boolean isShowChangeButton() {
        return showChangeButton;
    }

    public void setShowChangeButton(boolean showChangeButton) {
        this.showChangeButton = showChangeButton;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardExpYear() {
        return cardExpYear;
    }

    public void setCardExpYear(String cardExpYear) {
        this.cardExpYear = cardExpYear;
    }

    public String getCardExpMonth() {
        return cardExpMonth;
    }

    public void setCardExpMonth(String cardExpMonth) {
        this.cardExpMonth = cardExpMonth;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
