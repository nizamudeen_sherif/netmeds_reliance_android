package com.nms.netmeds.base;

public interface VisibleItemListener {
    void viewVisiblePosition(int firstVisiblePosition,int lastVisiblePosition);
}
