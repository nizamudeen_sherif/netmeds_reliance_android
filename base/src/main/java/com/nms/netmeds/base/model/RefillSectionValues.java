package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class RefillSectionValues {
    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }
}
