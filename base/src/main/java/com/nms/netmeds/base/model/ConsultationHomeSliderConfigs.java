package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConsultationHomeSliderConfigs {

    @SerializedName("enableFlag")
    private boolean enableFlag;
    @SerializedName("slide")
    private List<ConcernSlides> slidesList;

    public void setSlidesList(List<ConcernSlides> slidesList) {
        this.slidesList = slidesList;
    }

    public void setEnableFlag(boolean enableFlag) {
        this.enableFlag = enableFlag;
    }

    public boolean isEnableFlag() {
        return enableFlag;
    }

    public List<ConcernSlides> getSlidesList() {
        return slidesList;
    }
}
