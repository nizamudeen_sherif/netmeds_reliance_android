package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ConfigurationResponse extends BaseResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private ConfigurationResult result;

    public ConfigurationResult getResult() {
        return result;
    }

    public void setResult(ConfigurationResult result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
