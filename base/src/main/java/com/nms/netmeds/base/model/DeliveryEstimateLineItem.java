package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class DeliveryEstimateLineItem {
    @SerializedName("itemcode")
    private String itemcode;
    @SerializedName("Qty")
    private Integer qty;

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
