package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Subscription implements Serializable {

    @SerializedName("prime")
    private Prime prime;
    @SerializedName("message")
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public Prime getPrime() {
        return prime;
    }

    public void setPrime(Prime prime) {
        this.prime = prime;
    }


}
