package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class SortOptionLabel extends BaseResponse {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    transient private boolean isSelected = false;

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
