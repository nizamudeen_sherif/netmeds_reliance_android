package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MStarPackOfNRulesModel {
    @SerializedName("rules")
    private List<ProductPackRulesModel> rules = null;

    public List<ProductPackRulesModel> getRules() {
        return rules;
    }

    public void setRules(List<ProductPackRulesModel> rules) {
        this.rules = rules;
    }
}
