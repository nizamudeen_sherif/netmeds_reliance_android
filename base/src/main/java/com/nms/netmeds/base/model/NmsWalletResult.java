package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.NmsWalletBalance;

public class NmsWalletResult {
    @SerializedName(value = "balances", alternate = "walletBalance")
    private NmsWalletBalance balances;

    public NmsWalletBalance getBalances() {
        return balances;
    }

    public void setBalances(NmsWalletBalance balances) {
        this.balances = balances;
    }

}
