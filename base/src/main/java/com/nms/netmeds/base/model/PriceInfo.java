package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PriceInfo implements Serializable {
    @SerializedName("hc")
    private String hc;
    @SerializedName("offered_price")
    private String offeredPrice;
    @SerializedName("final_price")
    private String finalPrice;
    @SerializedName("discount_amount")
    private String discountAmount;
    @SerializedName("discount_percent_info")
    private String discountPercentInfo;
    @SerializedName("discount_amount_info")
    private String discountAmountInfo;
    @SerializedName("discount_percent")
    private String discountPercent;
    
    public String getHc() {
        return hc;
    }

    public void setHc(String hc) {
        this.hc = hc;
    }

    public String getOfferedPrice() {
        return offeredPrice;
    }

    public void setOfferedPrice(String offeredPrice) {
        this.offeredPrice = offeredPrice;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountPercentInfo() {
        return discountPercentInfo;
    }

    public void setDiscountPercentInfo(String discountPercentInfo) {
        this.discountPercentInfo = discountPercentInfo;
    }

    public String getDiscountAmountInfo() {
        return discountAmountInfo;
    }

    public void setDiscountAmountInfo(String discountAmountInfo) {
        this.discountAmountInfo = discountAmountInfo;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }
}
