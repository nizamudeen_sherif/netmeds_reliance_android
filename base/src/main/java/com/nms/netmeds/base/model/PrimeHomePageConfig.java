package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PrimeHomePageConfig {
    @SerializedName("header")
    private String headerText;
    @SerializedName("title")
    private String titleText;
    @SerializedName("description")
    private String description;
    @SerializedName("buttontext")
    private String buttonText;
    @SerializedName("enableFlag")
    private boolean enableFlag;

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public boolean isEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(boolean enableFlag) {
        this.enableFlag = enableFlag;
    }
}
