package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ColdStorageResponse {
    @SerializedName("result")
    private ColdStorageResult result;
    @SerializedName("status")
    private String status;

    public ColdStorageResult getResult() {
        return result;
    }

    public void setResult(ColdStorageResult result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
