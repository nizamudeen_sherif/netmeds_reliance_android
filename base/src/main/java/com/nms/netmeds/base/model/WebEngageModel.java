package com.nms.netmeds.base.model;

import android.text.TextUtils;

import java.io.Serializable;


/**
 * @author vikas.grover
 * This class will be used to capture all the user details
 * which requrie for web engage events.
 */
public class WebEngageModel implements Serializable {

    private String customerName;
    private String email;
    private String phone;
    private String patientIdentified;
    private String speciality;
    private String symptoms;
    private String mode;
    private String type;
    private String paymentMode;
    private String paymentStatus;
    private String dismiss;
    private String PrescriptionStatus;
    private String labTestPrescribed;
    private String labTestName;
    private String followUpStatus;
    private String prescribedMedicines;
    private String dosagePrescribed;
    private Float rating;
    private MStarCustomerDetails mStarCustomerDetails;


    public String getPrescribedMedicines() {
        return prescribedMedicines;
    }

    public void setPrescribedMedicines(String prescribedMedicines) {
        this.prescribedMedicines = prescribedMedicines;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }


    public String getDosagePrescribed() {
        return dosagePrescribed;
    }

    public void setDosagePrescribed(String dosagePrescribed) {
        this.dosagePrescribed = dosagePrescribed;
    }


    public String getFollowUpStatus() {
        return followUpStatus;
    }

    public void setFollowUpStatus(String followUpStatus) {
        this.followUpStatus = followUpStatus;
    }

    public String getLabTestName() {
        return labTestName;
    }

    public void setLabTestName(String labTestName) {
        this.labTestName = labTestName;
    }

    public String getLabTestPrescribed() {
        return labTestPrescribed;
    }

    public void setLabTestPrescribed(String labTestPrescribed) {
        this.labTestPrescribed = labTestPrescribed;
    }

    public String getDismiss() {
        return dismiss;
    }

    public void setDismiss(String dismiss) {
        this.dismiss = dismiss;
    }

    public String getPrescriptionStatus() {
        return PrescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
        PrescriptionStatus = prescriptionStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPatientIdentified() {
        return patientIdentified;
    }

    public void setPatientIdentified(String patientIdentified) {
        this.patientIdentified = patientIdentified;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public void setMStarUserDetails(MStarCustomerDetails mStarCustomerDetails) {
        this.mStarCustomerDetails = mStarCustomerDetails;
    }

    public void setCommonProperties() {
        String firstName = mStarCustomerDetails != null && !TextUtils.isEmpty(mStarCustomerDetails.getFirstName()) ? mStarCustomerDetails.getFirstName() : "";
        String lastName = mStarCustomerDetails != null && !TextUtils.isEmpty(mStarCustomerDetails.getLastName()) ? mStarCustomerDetails.getLastName() : "";
        email = mStarCustomerDetails != null && !TextUtils.isEmpty(mStarCustomerDetails.getEmail()) ? mStarCustomerDetails.getEmail() : "";
        phone = mStarCustomerDetails != null && !TextUtils.isEmpty(mStarCustomerDetails.getMobileNo()) ? mStarCustomerDetails.getMobileNo() : "";
        customerName = firstName + " " + lastName;
        setCustomerName(customerName);
        setEmail(email);
        setPhone(phone);
    }
}