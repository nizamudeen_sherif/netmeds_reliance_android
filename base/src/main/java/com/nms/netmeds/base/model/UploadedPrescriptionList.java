package com.nms.netmeds.base.model;

import android.graphics.Bitmap;

public class UploadedPrescriptionList {

    private String file;
    private boolean isDigitized = false;
    private Bitmap bitmap;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public boolean isDigitized() {
        return isDigitized;
    }

    public void setDigitized(boolean digitized) {
        isDigitized = digitized;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
