package com.nms.netmeds.base.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.databinding.DialogxSingleClickSubscriptionBinding;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.viewModel.SingleClickSubscriptionViewModel;

@SuppressLint("ValidFragment")
public class SingleClickSubscriptionDialog extends BaseBottomSheetFragment implements SingleClickSubscriptionViewModel.SingleClickSubscriptionViewModelListener {

    private final SingleClickSubscriptionDilaogListener listener;
    private String currentDeliveryDate;
    private String orderId;
    private DialogxSingleClickSubscriptionBinding binding;

    public SingleClickSubscriptionDialog(SingleClickSubscriptionDilaogListener listener, String currentDeliveryDate, String orderId) {
        this.listener = listener;
        this.currentDeliveryDate = currentDeliveryDate;
        this.orderId = orderId;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    onDismissDialog();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialogx_single_click_subscription, container, false);
        SingleClickSubscriptionViewModel viewModel = ViewModelProviders.of(this).get(SingleClickSubscriptionViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(binding, currentDeliveryDate, orderId, this, getBasePreference());
        return binding.getRoot();
    }

    @Override
    public void showProgress() {
        showProgress(getActivity());
    }

    @Override
    public void dismissProgress() {
        super.dismissProgress();
    }

    @Override
    public void onDismissDialog() {
        SingleClickSubscriptionDialog.this.dismissAllowingStateLoss();
    }

    @Override
    public void subscriptionF2InitiatedSuccessful() {
        onDismissDialog();
        listener.navigateToSubscription();
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public DialogxSingleClickSubscriptionBinding binding() {
        return binding;
    }

    @Override
    public void snackBarView(String message) {
        listener.snackBarView(message);
    }

    public interface SingleClickSubscriptionDilaogListener {

        void navigateToSubscription();

        void snackBarView(String message);
    }

    private BasePreference getBasePreference() {
        return BasePreference.getInstance(getActivity());
    }
}
