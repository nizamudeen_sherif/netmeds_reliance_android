package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ConfigurationResult {
    @SerializedName("configDetails")
    private ConfigDetails configDetails;

    public ConfigDetails getConfigDetails() {
        return configDetails;
    }

    public void setConfigDetails(ConfigDetails configDetails) {
        this.configDetails = configDetails;
    }
}
