package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CreationResult {


    @SerializedName("customerName")
    private String customerName;
    @SerializedName("subscriptionStatusText")
    private String subscriptionStatusText;
    @SerializedName("subscriptionInfoText")
    private String subscriptionInfoText;
    @SerializedName("currentOrderDate")
    private String currentOrderDate;
    @SerializedName("nextDeliveryDate")
    private String nextDeliveryDate;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSubscriptionStatusText() {
        return subscriptionStatusText;
    }

    public void setSubscriptionStatusText(String subscriptionStatusText) {
        this.subscriptionStatusText = subscriptionStatusText;
    }

    public String getSubscriptionInfoText() {
        return subscriptionInfoText;
    }

    public void setSubscriptionInfoText(String subscriptionInfoText) {
        this.subscriptionInfoText = subscriptionInfoText;
    }

    public String getCurrentOrderDate() {
        return currentOrderDate;
    }

    public void setCurrentOrderDate(String currentOrderDate) {
        this.currentOrderDate = currentOrderDate;
    }

    public String getNextDeliveryDate() {
        return nextDeliveryDate;
    }

    public void setNextDeliveryDate(String nextDeliveryDate) {
        this.nextDeliveryDate = nextDeliveryDate;
    }

}
