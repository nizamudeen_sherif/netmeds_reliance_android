package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductItem {
    @SerializedName("name")
    private String name;
    @SerializedName("categoryId")
    private String categoryId;
    @SerializedName("genericName")
    private String genericName;
    @SerializedName("categoryName")
    private String categoryName;
    @SerializedName("subCategoryName")
    private String subCategoryName;
    @SerializedName("rxRequired")
    private String rxRequired;
    @SerializedName("manufacturerName")
    private String manufacturerName;
    @SerializedName("strikePrice")
    private String strikePrice;
    @SerializedName("actualPrice")
    private String actualPrice;
    @SerializedName("packSizeTypeLabel")
    private String packSizeTypeLabel;
    @SerializedName("availableStatus")
    private String availableStatus;
    @SerializedName("returnableMessage")
    private String returnableMessage;
    @SerializedName("availableStatusCode")
    private String availableStatusCode;
    @SerializedName("imageURL")
    private List<String> imageURL;
    @SerializedName("skuId")
    private String skuId;
    @SerializedName("typeId")
    private String typeId;
    @SerializedName("groupId")
    private String groupId;
    @SerializedName("schedule")
    private String schedule;
    @SerializedName("drugType")
    private String drugType;
    @SerializedName("maximumSelectableQuantity")
    private int maximumSelectableQuantity;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("id")
    private String id;
    @SerializedName("minimumQuantity")
    private int minimumQuantity;
    @SerializedName("coldStorage")
    private boolean coldStorage;
    @SerializedName("discount")
    private String discount;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAvailableStatusCode() {
        return availableStatusCode;
    }

    public void setAvailableStatusCode(String availableStatusCode) {
        this.availableStatusCode = availableStatusCode;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public int getMaximumSelectableQuantity() {
        return maximumSelectableQuantity;
    }

    public void setMaximumSelectableQuantity(int maximumSelectableQuantity) {
        this.maximumSelectableQuantity = maximumSelectableQuantity;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public boolean isColdStorage() {
        return coldStorage;
    }

    public void setColdStorage(boolean coldStorage) {
        this.coldStorage = coldStorage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getRxRequired() {
        return rxRequired;
    }

    public void setRxRequired(String rxRequired) {
        this.rxRequired = rxRequired;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(String strikePrice) {
        this.strikePrice = strikePrice;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getPackSizeTypeLabel() {
        return packSizeTypeLabel;
    }

    public void setPackSizeTypeLabel(String packSizeTypeLabel) {
        this.packSizeTypeLabel = packSizeTypeLabel;
    }

    public String getReturnableMessage() {
        return returnableMessage;
    }

    public void setReturnableMessage(String returnableMessage) {
        this.returnableMessage = returnableMessage;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus;
    }

    public List<String> getImageURL() {
        return imageURL;
    }

    public void setImageURL(List<String> imageURL) {
        this.imageURL = imageURL;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(int minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
