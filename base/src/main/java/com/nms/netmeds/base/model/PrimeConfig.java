package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PrimeConfig {
    @SerializedName("primeEnableFlag")
    public boolean primeEnableFlag;
    @SerializedName("primeHeader")
    public String primeHeader;
    @SerializedName("shoppingCart")
    public ShoppingCart shoppingCart;
    @SerializedName("netmedsMembership")
    public NetmedsMembership netmedsMembership;
    @SerializedName("primeFaq")
    private String primeFag;
    @SerializedName("primeMemberText")
    private String primeMemberText;
    @SerializedName("orderheader")
    private PrimeOrderBannerConfig orderBannerConfig;
    @SerializedName("homePageConfig")
    private PrimeHomePageConfig homePageConfig;
    @SerializedName("primeProduceCode")
    private List<String> primeProduceCode;

    public boolean getPrimeEnableFlag() {
        return primeEnableFlag;
    }

    public void setPrimeEnableFlag(boolean primeEnableFlag) {
        this.primeEnableFlag = primeEnableFlag;
    }

    public String getPrimeHeader() {
        return primeHeader;
    }

    public void setPrimeHeader(String primeHeader) {
        this.primeHeader = primeHeader;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public NetmedsMembership getNetmedsMembership() {
        return netmedsMembership;
    }

    public void setNetmedsMembership(NetmedsMembership netmedsMembership) {
        this.netmedsMembership = netmedsMembership;
    }

    public String getPrimeFag() {
        return primeFag;
    }

    public void setPrimeFag(String primeFag) {
        this.primeFag = primeFag;
    }

    public String getPrimeMemberText() {
        return primeMemberText;
    }

    public void setPrimeMemberText(String primeMemberText) {
        this.primeMemberText = primeMemberText;
    }

    public PrimeOrderBannerConfig getOrderBannerConfig() {
        return orderBannerConfig;
    }

    public void setOrderBannerConfig(PrimeOrderBannerConfig orderBannerConfig) {
        this.orderBannerConfig = orderBannerConfig;
    }

    public PrimeHomePageConfig getHomePageConfig() {
        return homePageConfig;
    }

    public void setHomePageConfig(PrimeHomePageConfig homePageConfig) {
        this.homePageConfig = homePageConfig;
    }

    public List<String> getPrimeProduceCode() {
        List<String> primeProductCodeList = new ArrayList<>();
        primeProductCodeList.add("2001");
        primeProductCodeList.add("2002");
        return primeProduceCode != null && !primeProduceCode.isEmpty() ? primeProduceCode : primeProductCodeList;
    }

    public void setPrimeProduceCode(List<String> primeProduceCode) {
        this.primeProduceCode = primeProduceCode;
    }
}
