package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class DeliveryContent {

    @SerializedName("productCannotBeDelivered")
    private String productCannotBeDelivered;
    @SerializedName("productCannotBeDeliveredColdStorage")
    private String productCannotBeDeliveredColdStorage;
    @SerializedName("productCannotBeDeliveredToEstimatedDelivery")
    private String productCannotBeDeliveredToEstimatedDelivery;
    @SerializedName("pinBlockedErrorMessage")
    private String pinBlockedErrorMessage;

    public String getProductCannotBeDelivered() {
        return productCannotBeDelivered;
    }

    public void setProductCannotBeDelivered(String productCannotBeDelivered) {
        this.productCannotBeDelivered = productCannotBeDelivered;
    }

    public String getProductCannotBeDeliveredColdStorage() {
        return productCannotBeDeliveredColdStorage;
    }

    public void setProductCannotBeDeliveredColdStorage(String productCannotBeDeliveredColdStorage) {
        this.productCannotBeDeliveredColdStorage = productCannotBeDeliveredColdStorage;
    }

    public String getProductCannotBeDeliveredToEstimatedDelivery() {
        return productCannotBeDeliveredToEstimatedDelivery;
    }

    public void setProductCannotBeDeliveredToEstimatedDelivery(String productCannotBeDeliveredToEstimatedDelivery) {
        this.productCannotBeDeliveredToEstimatedDelivery = productCannotBeDeliveredToEstimatedDelivery;
    }

    public String getPinBlockedErrorMessage() {
        return pinBlockedErrorMessage;
    }

    public void setPinBlockedErrorMessage(String pinBlockedErrorMessage) {
        this.pinBlockedErrorMessage = pinBlockedErrorMessage;
    }
}
