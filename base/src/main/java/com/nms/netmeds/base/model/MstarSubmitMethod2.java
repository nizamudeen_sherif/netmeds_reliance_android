package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarSubmitMethod2 {
    @SerializedName("message")
    private String message;
    @SerializedName("display_status")
    private String display_status;
    @SerializedName("status")
    private String status;
    @SerializedName("order_id")
    private String order_id;


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDisplay_status() {
        return display_status;
    }

    public void setDisplay_status(String display_status) {
        this.display_status = display_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
