package com.nms.netmeds.base.model;

import android.graphics.Bitmap;

public class DigitizedPrescription {
    private Bitmap bitmap;
    private boolean isDigitized;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isDigitized() {
        return isDigitized;
    }

    public void setDigitized(boolean digitized) {
        isDigitized = digitized;
    }
}
