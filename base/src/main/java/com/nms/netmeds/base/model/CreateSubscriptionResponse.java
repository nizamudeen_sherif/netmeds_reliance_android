package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CreateSubscriptionResponse {

    @SerializedName("updatedOn")
    private long updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private CreationResult result;

    public long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public CreationResult getResult() {
        return result;
    }

    public void setResult(CreationResult result) {
        this.result = result;
    }
}
