package com.nms.netmeds.base;

import com.nms.netmeds.base.model.AppliedVoucher;
import com.nms.netmeds.base.model.DrugDetail;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentHelper {

    private static boolean isShowContinueUpload = false;
    private static boolean isJusPayCustomerCreated = false;
    private static boolean reloadTotalInformation = false;
    private static boolean reloadCart = false;
    private static boolean pinCodeStatus = false;
    private static boolean isWalletApplied = false;
    private static boolean retroWalletApplied = false;
    private static boolean PromoCodeDialoguePop = false;
    private static boolean ProductDetailReloadCart = false;
    private static boolean isOutOfStockPopUpShown = false;
    private static boolean isPrime = false;

    /**
     * Generate Quote Request Helper
     */
    private static boolean isPrescriptionOrder = false;
    private static boolean isIncompleteOrder = false;
    private static String inCompleteOrderId = "";
    private static boolean isExternalDoctor = false;
    private static String deliveryEstimate = "";
    private static String deliveryEstimateResponse = "";
    private static String cbBreakUp = "";
    private static String bundleSkuId;
    private static String boomDate = "";
    private static String sigup_method = "";
    private static String cancel_reason = "";
    private static String single_address = "";

    /**
     * Payment Information Request Helper
     */

    private static MStarAddressModel addressModel = new MStarAddressModel();
    private static MStarAddressModel customerBillingAddress = new MStarAddressModel();
    private static List<MStarProductDetails> mStarProductDetails = new ArrayList<>();

    /**
     * Complete Order Request Helper
     */
    private static boolean isNMSCashApplied = false;
    private static boolean isNMSSuperCashApplied = false;
    private static boolean isPrescriptionUploaded = false;
    private static boolean isVoucherApplied = false;
    public static boolean isMedicineIncreaseQuantity = true;
    private static String couponCode = "";
    private static String prescriptionItems = "";
    private static String voucherCode = "";
    private static double availableNMSCash = 0;
    private static double appliedNMSCash = 0;
    private static double appliedNMSSuperCash = 0;
    private static double totalAmount = 0;
    private static int bundleCount = 1;

    /**
     * Transaction Log Request Helper
     */
    private static double nmsWalletAmount = 0;

    /**
     * NMS Wallet Balance Request Helper
     */
    private static String deliveryPinCode = "";


    /**
     * if M2 changed to M1 then it is true
     */
    private static boolean m2ChangeToM1;

    /**
     * Refill Request Helper
     **/

    private static List<MStarProductDetails> cartLineItems;
    private static String deliveryEstimateWithYear;
    private static Map<String, DrugDetail> itemInsideOrderMap;
    private static boolean cartRetained = false;
    private static String primePlan;
    private static boolean isPaymentFromRetry = false;
    private static boolean isRetryClick = false;
    private static boolean isFromReOrder = false;
    private static boolean isFromRetryPopup = false;

    // For Reorder and Retry
    private static Integer tempCartId;
    private static boolean isTempCartOpen = false;
    private static boolean isTempCart = false;
    private static boolean isEditOrder = false;
    private static boolean isPayAndEditM2Order;

    // OrderDetailResponse
    private static MstarOrderDetailsResult mstarOrderDetailsResult;


    /**
     * Initiate checkout will only once when we get inside to the cart page, after that no need to call again.
     * And also alternate product api will call only before calling switch product API.
     */
    private static boolean isCartSwitched;
    private static boolean isInitiateCheckoutCalled;
    private static BigDecimal originalCartAmount;
    private static Map<String, MStarProductDetails> genericProductMap = new HashMap<>();
    private static Map<String, MStarProductDetails> genericBrandProductMap = new HashMap<>();
    private static List<MStarProductDetails> analyticalTrackingProductList;

    /**
     * For reset and change the selected payment gateway in child adapter
     */
    private static int childPosition = -1;

    public static boolean isIsFromRetryPopup() {
        return isFromRetryPopup;
    }

    public static void setIsFromRetryPopup(boolean isFromRetryPopup) {
        PaymentHelper.isFromRetryPopup = isFromRetryPopup;
    }

    private static List<AppliedVoucher> appliedVoucherList;

    public static boolean isIsFromReOrder() {
        return isFromReOrder;
    }

    public static void setIsFromReOrder(boolean isFromReOrder) {
        PaymentHelper.isFromReOrder = isFromReOrder;
    }

    public static String getPrimePlan() {
        return primePlan;
    }

    public static void setPrimePlan(String primePlan) {
        PaymentHelper.primePlan = primePlan;
    }

    public static boolean isPrescriptionOrder() {
        return isPrescriptionOrder;
    }

    public static void setIsPrescriptionOrder(boolean isPrescriptionOrder) {
        PaymentHelper.isPrescriptionOrder = isPrescriptionOrder;
    }

    public static boolean isIsIncompleteOrder() {
        return isIncompleteOrder;
    }

    public static void setIsIncompleteOrder(boolean isIncompleteOrder) {
        PaymentHelper.isIncompleteOrder = isIncompleteOrder;
    }

    public static String getInCompleteOrderId() {
        return inCompleteOrderId;
    }

    public static void setInCompleteOrderId(String inCompleteOrderId) {
        PaymentHelper.inCompleteOrderId = inCompleteOrderId;
    }

    public static boolean isExternalDoctor() {
        return isExternalDoctor;
    }

    public static void setIsExternalDoctor(boolean isExternalDoctor) {
        PaymentHelper.isExternalDoctor = isExternalDoctor;
    }

    public static String getDeliveryEstimate() {
        return deliveryEstimate;
    }

    public static void setDeliveryEstimate(String deliveryEstimate) {
        PaymentHelper.deliveryEstimate = deliveryEstimate;
    }

    public static void setDeliveryEstimateWithYear(String deliveryEstimateWithYear) {
        PaymentHelper.deliveryEstimateWithYear = deliveryEstimateWithYear;
    }

    public static String getDeliveryEstimateWithYear() {
        return deliveryEstimateWithYear;
    }

    public static String getCbBreakUp() {
        return cbBreakUp;
    }

    public static void setCbBreakUp(String cbBreakUp) {
        PaymentHelper.cbBreakUp = cbBreakUp;
    }

    public static MStarAddressModel getCustomerAddress() {
        return addressModel;
    }

    public static void setCustomerAddress(MStarAddressModel addressModel) {
        PaymentHelper.addressModel = addressModel;
    }

    public static boolean isNMSCashApplied() {
        return isNMSCashApplied;
    }

    public static void setIsNMSCashApplied(boolean isNMSCashApplied) {
        PaymentHelper.isNMSCashApplied = isNMSCashApplied;
    }

    public static boolean isNMSSuperCashApplied() {
        return isNMSSuperCashApplied;
    }

    public static void setIsNMSSuperCashApplied(boolean isNMSSuperCashApplied) {
        PaymentHelper.isNMSSuperCashApplied = isNMSSuperCashApplied;
    }

    public static boolean isPrescriptionUploaded() {
        return isPrescriptionUploaded;
    }

    public static void setIsPrescriptionUploaded(boolean isPrescriptionUploaded) {
        PaymentHelper.isPrescriptionUploaded = isPrescriptionUploaded;
    }

    public static String getCouponCode() {
        return couponCode;
    }

    public static void setCouponCode(String couponCode) {
        PaymentHelper.couponCode = couponCode;
    }

    public static boolean isVoucherApplied() {
        return isVoucherApplied;
    }

    public static void setIsVoucherApplied(boolean isVoucherApplied) {
        PaymentHelper.isVoucherApplied = isVoucherApplied;
    }

    public static String getPrescriptionItems() {
        return prescriptionItems;
    }

    public static void setPrescriptionItems(String prescriptionItems) {
        PaymentHelper.prescriptionItems = prescriptionItems;
    }

    public static String getVoucherCode() {
        return voucherCode;
    }

    public static void setVoucherCode(String voucherCode) {
        PaymentHelper.voucherCode = voucherCode;
    }

    public static double getNmsWalletAmount() {
        return nmsWalletAmount;
    }

    public static void setNmsWalletAmount(double nmsWalletAmount) {
        PaymentHelper.nmsWalletAmount = nmsWalletAmount;
    }

    public static String getDeliveryPinCode() {
        return deliveryPinCode;
    }

    public static void setDeliveryPinCode(String deliveryPinCode) {
        PaymentHelper.deliveryPinCode = deliveryPinCode;
    }

    public static List<MStarProductDetails> getCartLineItems() {
        return cartLineItems;
    }

    public static void setCartLineItems(List<MStarProductDetails> cartLineItems) {
        PaymentHelper.cartLineItems = cartLineItems;
    }

    public static boolean isIsShowContinueUpload() {
        return isShowContinueUpload;
    }

    public static void setIsShowContinueUpload(boolean isShowContinueUpload) {
        PaymentHelper.isShowContinueUpload = isShowContinueUpload;
    }

    public static boolean isIsJusPayCustomerCreated() {
        return isJusPayCustomerCreated;
    }

    public static void setIsJusPayCustomerCreated(boolean isJusPayCustomerCreated) {
        PaymentHelper.isJusPayCustomerCreated = isJusPayCustomerCreated;
    }

    public static String getDeliveryEstimateResponse() {
        return deliveryEstimateResponse;
    }

    public static void setDeliveryEstimateResponse(String deliveryEstimateResponse) {
        PaymentHelper.deliveryEstimateResponse = deliveryEstimateResponse;
    }

    public static double getAvailableNMSCash() {
        return availableNMSCash;
    }

    public static void setAvailableNMSCash(double availableNMSCash) {
        PaymentHelper.availableNMSCash = availableNMSCash;
    }

    public static double getAppliedNMSCash() {
        return appliedNMSCash;
    }

    public static void setAppliedNMSCash(double appliedNMSCash) {
        PaymentHelper.appliedNMSCash = appliedNMSCash;
    }

    public static boolean isReloadTotalInformation() {
        return reloadTotalInformation;
    }

    public static void setReloadTotalInformation(boolean reloadTotalInformation) {
        PaymentHelper.reloadTotalInformation = reloadTotalInformation;
    }

    public static double getAppliedNMSSuperCash() {
        return appliedNMSSuperCash;
    }

    public static void setAppliedNMSSuperCash(double appliedNMSSuperCash) {
        PaymentHelper.appliedNMSSuperCash = appliedNMSSuperCash;
    }

    public static double getTotalAmount() {
        return totalAmount;
    }

    public static void setTotalAmount(double totalAmount) {
        PaymentHelper.totalAmount = totalAmount;
    }

    public static MStarAddressModel getCustomerBillingAddress() {
        return customerBillingAddress;
    }

    public static void setCustomerBillingAddress(MStarAddressModel customerBillingAddress) {
        PaymentHelper.customerBillingAddress = customerBillingAddress;
    }

    public static boolean isReloadCart() {
        return reloadCart;
    }

    public static void setReloadCart(boolean reloadCart) {
        PaymentHelper.reloadCart = reloadCart;
    }

    public static boolean getPinCodeStatus() {
        return pinCodeStatus;
    }

    public static void setPinCodeStatus(boolean pinCodeStatus) {
        PaymentHelper.pinCodeStatus = pinCodeStatus;
    }

    public static void setItemInsideOrderMap(Map<String, DrugDetail> itemInsideOrderMap) {
        PaymentHelper.itemInsideOrderMap = itemInsideOrderMap;
    }

    public static Map<String, DrugDetail> getItemInsideOrderMap() {
        return itemInsideOrderMap;
    }

    public static boolean isIsWalletApplied() {
        return isWalletApplied;
    }

    public static void setIsWalletApplied(boolean isWalletApplied) {
        PaymentHelper.isWalletApplied = isWalletApplied;
    }

    public static boolean isRetroWalletApplied() {
        return retroWalletApplied;
    }

    public static void setRetroWalletApplied(boolean retroWalletApplied) {
        PaymentHelper.retroWalletApplied = retroWalletApplied;
    }


    public static boolean isM2ChangeToM1() {
        return m2ChangeToM1;
    }

    public static void setM2ChangeToM1(boolean m2ChangeToM1) {
        PaymentHelper.m2ChangeToM1 = m2ChangeToM1;
    }

    public static boolean getPromoCodeDialoguePop() {
        return PromoCodeDialoguePop;
    }

    public static void setPromoCodeDialoguePop(boolean promoCodeDialoguePop) {
        PromoCodeDialoguePop = promoCodeDialoguePop;
    }

    public static int getBundleCount() {
        return bundleCount;
    }

    public static void setBundleCount(int bundleCount) {
        PaymentHelper.bundleCount = bundleCount;
    }

    public static String getBundleSkuId() {
        return bundleSkuId;
    }

    public static void setBundleSkuId(String bundleSkuId) {
        PaymentHelper.bundleSkuId = bundleSkuId;
    }

    public static String getBoomDate() {
        return boomDate;
    }

    public static void setBoomDate(String boomDate) {
        PaymentHelper.boomDate = boomDate;
    }

    public static boolean getIsRetryClick() {
        return isRetryClick;
    }

    public static void setIsRetryClick(boolean isRetryClick) {
        PaymentHelper.isRetryClick = isRetryClick;
    }

    public static boolean isProductDetailReloadCart() {
        return ProductDetailReloadCart;
    }

    public static void setProductDetailReloadCart(boolean productDetailReloadCart) {
        ProductDetailReloadCart = productDetailReloadCart;
    }

    public static boolean isIsOutOfStockPopUpShown() {
        return isOutOfStockPopUpShown;
    }

    public static void setIsOutOfStockPopUpShown(boolean isOutOfStockPopUpShown) {
        PaymentHelper.isOutOfStockPopUpShown = isOutOfStockPopUpShown;
    }

    public static void setAppliedVoucherList(List<AppliedVoucher> appliedVoucherList) {
        PaymentHelper.appliedVoucherList = appliedVoucherList;
    }

    public static List<AppliedVoucher> getAppliedVoucherList() {
        return appliedVoucherList;
    }

    public static Integer getTempCartId() {
        return tempCartId;
    }

    public static void setTempCartId(Integer tempCartId) {
        PaymentHelper.tempCartId = tempCartId;
    }

    public static boolean isIsTempCart() {
        return isTempCart;
    }

    public static void setIsTempCart(boolean isTempCart) {
        PaymentHelper.isTempCart = isTempCart;
    }

    public static boolean isIsEditOrder() {
        return isEditOrder;
    }

    public static void setIsEditOrder(boolean isEditOrder) {
        PaymentHelper.isEditOrder = isEditOrder;
    }

    public static boolean isIsTempCartOpen() {
        return isTempCartOpen;
    }

    public static void setIsTempCartOpen(boolean isTempCartOpen) {
        PaymentHelper.isTempCartOpen = isTempCartOpen;
    }

    public static void setIsPayAndEditM2Order(boolean isPayAndEditM2Order) {
        PaymentHelper.isPayAndEditM2Order = isPayAndEditM2Order;
    }

    public static boolean isIsPayAndEditM2Order() {
        return isPayAndEditM2Order;
    }

    public static boolean getIsPayAndEditM2Order() {
        return isPayAndEditM2Order;
    }

    public static MstarOrderDetailsResult getMstarOrderDetailsResult() {
        return PaymentHelper.mstarOrderDetailsResult;
    }

    public static void setMstarOrderDetailsResult(MstarOrderDetailsResult mstarOrderDetailsResult) {
        PaymentHelper.mstarOrderDetailsResult = mstarOrderDetailsResult;
    }

    public static String getSigup_method() {
        return sigup_method;
    }

    public static void setSigup_method(String sigup_method) {
        PaymentHelper.sigup_method = sigup_method;
    }

    public static String getCancel_reason() {
        return cancel_reason;
    }

    public static void setCancel_reason(String cancel_reason) {
        PaymentHelper.cancel_reason = cancel_reason;
    }

    public static boolean isIsPrime() {
        return isPrime;
    }

    public static void setIsPrime(boolean isPrime) {
        PaymentHelper.isPrime = isPrime;
    }

    public static void setIsCartSwitched(boolean isCartSwitched) {
        PaymentHelper.isCartSwitched = isCartSwitched;
    }

    public static boolean isCartSwitched() {
        return isCartSwitched;
    }

    public static void setIsInitiateCheckoutCalled(boolean isInitiateCheckoutCalled) {
        PaymentHelper.isInitiateCheckoutCalled = isInitiateCheckoutCalled;
    }

    public static boolean isInitiateCheckoutCalled() {
        return isInitiateCheckoutCalled;
    }

    public static void setOriginalCartAmount(BigDecimal originalCartAmount) {
        PaymentHelper.originalCartAmount = originalCartAmount;
    }

    public static BigDecimal getOriginalCartAmount() {
        return originalCartAmount;
    }

    public static String getSingle_address() {
        return single_address;
    }

    public static void setSingle_address(String single_address) {
        PaymentHelper.single_address = single_address;
    }

    public static void setChildPosition(int childPosition) {
        PaymentHelper.childPosition = childPosition;
    }

    public static int getChildPosition() {
        return childPosition;
    }


    public static List<MStarProductDetails> getmStarProductDetails() {
        return mStarProductDetails;
    }

    public static void setmStarProductDetails(List<MStarProductDetails> mStarProductDetails) {
        PaymentHelper.mStarProductDetails = mStarProductDetails;
    }

    public static List<MStarProductDetails> getAnalyticalTrackingProductList() {
        return analyticalTrackingProductList;
    }

    public static void setAnalyticalTrackingProductList(List<MStarProductDetails> analyticalTrackingProductList) {
        PaymentHelper.analyticalTrackingProductList = analyticalTrackingProductList;
    }

    public static boolean isIsMedicineIncreaseQuantity() {
        return isMedicineIncreaseQuantity;
    }

    public static void setIsMedicineIncreaseQuantity(boolean isMedicineIncreaseQuantity) {
        PaymentHelper.isMedicineIncreaseQuantity = isMedicineIncreaseQuantity;
    }
}