package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarFacets {

    @SerializedName("brand")
    private List<MstarFacetItemDetails> brandList;
    @SerializedName("manufacturer")
    private List<MstarFacetItemDetails> manufacturerList;

    public List<MstarFacetItemDetails> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<MstarFacetItemDetails> brandList) {
        this.brandList = brandList;
    }

    public List<MstarFacetItemDetails> getManufacturerList() {
        return manufacturerList;
    }

    public void setManufacturerList(List<MstarFacetItemDetails> manufacturerList) {
        this.manufacturerList = manufacturerList;
    }
}
