package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class GenerateOrderIdRequest {

    @SerializedName("split_info")
    private DeliveryEstimateResponse splitInfo;

    public DeliveryEstimateResponse getSplitInfo() {
        return splitInfo;
    }

    public void setSplitInfo(DeliveryEstimateResponse splitInfo) {
        this.splitInfo = splitInfo;
    }
}
