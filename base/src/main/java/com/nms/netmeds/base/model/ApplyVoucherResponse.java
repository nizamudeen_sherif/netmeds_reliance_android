package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApplyVoucherResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("applied_voucher")
    private ArrayList<AppliedVoucher> appliedVoucherList = null;
    @SerializedName("message")
    private String message;
    @SerializedName("remove_coupon")
    private boolean removeCoupon;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AppliedVoucher> getAppliedVoucherList() {
        return appliedVoucherList;
    }

    public void setAppliedVoucherList(ArrayList<AppliedVoucher> appliedVoucherList) {
        this.appliedVoucherList = appliedVoucherList;
    }

    public boolean getRemoveCoupon() {
        return removeCoupon;
    }

    public void setRemoveCoupon(boolean removeCoupon) {
        this.removeCoupon = removeCoupon;
    }
}
