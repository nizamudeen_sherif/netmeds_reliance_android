package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarOrderDetail {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("shipLocation")
    private String shipLocation;
    @SerializedName("drugStatus")
    private String drugStatus;
    @SerializedName("drugStatusCode")
    private String drugStatusCode;
    @SerializedName("trackingNumber")
    private String trackingNumber;
    @SerializedName("trackingUrl")
    private String trackingUrl;
    @SerializedName("trackingCompany")
    private String trackingCompany;
    @SerializedName("clickPostId")
    private String clickPostId;
    @SerializedName("promisedDeliveryDateFrom")
    private String promisedDeliveryDateFrom;
    @SerializedName("promisedDeliveryDate")
    private String promisedDeliveryDate;
    @SerializedName("drugDetails")
    private List<MstarDrugDetail> drugDetails = null;
    @SerializedName("trackStatus")
    private List<MstarOrderTrackStatus> trackStatus = null;
    @SerializedName("trackShortStatus")
    private String trackShortStatus;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getShipLocation() {
        return shipLocation;
    }

    public void setShipLocation(String shipLocation) {
        this.shipLocation = shipLocation;
    }

    public String getDrugStatus() {
        return drugStatus;
    }

    public void setDrugStatus(String drugStatus) {
        this.drugStatus = drugStatus;
    }

    public String getDrugStatusCode() {
        return drugStatusCode;
    }

    public void setDrugStatusCode(String drugStatusCode) {
        this.drugStatusCode = drugStatusCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getTrackingCompany() {
        return trackingCompany;
    }

    public void setTrackingCompany(String trackingCompany) {
        this.trackingCompany = trackingCompany;
    }

    public String getClickPostId() {
        return clickPostId;
    }

    public void setClickPostId(String clickPostId) {
        this.clickPostId = clickPostId;
    }

    public String getPromisedDeliveryDateFrom() {
        return promisedDeliveryDateFrom;
    }

    public void setPromisedDeliveryDateFrom(String promisedDeliveryDateFrom) {
        this.promisedDeliveryDateFrom = promisedDeliveryDateFrom;
    }

    public String getPromisedDeliveryDate() {
        return promisedDeliveryDate;
    }

    public void setPromisedDeliveryDate(String promisedDeliveryDate) {
        this.promisedDeliveryDate = promisedDeliveryDate;
    }

    public List<MstarDrugDetail> getDrugDetails() {
        return drugDetails;
    }

    public void setDrugDetails(List<MstarDrugDetail> drugDetails) {
        this.drugDetails = drugDetails;
    }

    public List<MstarOrderTrackStatus> getTrackStatus() {
        return trackStatus;
    }

    public void setTrackStatus(List<MstarOrderTrackStatus> trackStatus) {
        this.trackStatus = trackStatus;
    }

    public String getTrackShortStatus() {
        return trackShortStatus;
    }

    public void setTrackShortStatus(String trackShortStatus) {
        this.trackShortStatus = trackShortStatus;
    }
}
