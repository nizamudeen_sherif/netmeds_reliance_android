package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class Flow2SubscriptionRequest {

    @SerializedName("orderId")
    private String orderId;
    @SerializedName("refillDays")
    private String refillDays;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRefillDays() {
        return refillDays;
    }

    public void setRefillDays(String refillDays) {
        this.refillDays = refillDays;
    }
}
