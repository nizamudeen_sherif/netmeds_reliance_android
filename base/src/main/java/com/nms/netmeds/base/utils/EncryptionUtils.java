package com.nms.netmeds.base.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtils {

    //TODO
    public static String decryptAESCBCEncryption(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec("3794118009232073".getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec("4u7x!A%D*F-JaNdR".getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //TODO
    public static String encryptAESCBCEncryption(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec("37941180092320736".getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec("4u7x!A%D*F-JaNdR".getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            return Base64.encodeToString(cipher.doFinal(value.getBytes("UTF-8")), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //TODO
    public static String base64Conversion(String value) {
        try {
            byte[] data = value.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
