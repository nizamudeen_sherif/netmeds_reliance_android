package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class SendFcmTokenRequest {
    @SerializedName("email")
    private String email;
    @SerializedName("gcmSubId")
    private String token;
    @SerializedName("userType")
    private String userType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
