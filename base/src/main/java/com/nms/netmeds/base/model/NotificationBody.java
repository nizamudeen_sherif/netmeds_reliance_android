package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationBody implements Serializable {
    @SerializedName("chatId")
    private String chatId;
    @SerializedName("type")
    private String type;
    @SerializedName("message")
    private String message;
    @SerializedName("from")
    private MessageFrom messageFrom;
    @SerializedName("to")
    private MessageTo messageTo;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageFrom getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(MessageFrom messageFrom) {
        this.messageFrom = messageFrom;
    }

    public MessageTo getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(MessageTo messageTo) {
        this.messageTo = messageTo;
    }
}
