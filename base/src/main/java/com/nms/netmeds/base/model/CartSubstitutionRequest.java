package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSubstitutionRequest {


    @SerializedName("drug_code")
    private String drug_code;
    @SerializedName("qty")
    private int qty;

    public String getDrug_code() {
        return drug_code;
    }

    public void setDrug_code(String drug_code) {
        this.drug_code = drug_code;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
