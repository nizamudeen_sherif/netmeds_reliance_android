package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class AlgoliaCredentialDetail {
    @SerializedName("algMedIndex")
    private String algoliaIndex;
    @SerializedName("algoliaAppId")
    private String algoliaAppId;
    @SerializedName("algMedDetail1")
    private String algoliaApiKey;
    @SerializedName("algoliaHits")
    private String algoliaHits;
    @SerializedName("algDiagIndex")
    private String algoliaDiagnosticIndex;
    @SerializedName("algDiagdetail1")
    private String algoliaDiagnosticApiKey;

    public String getAlgoliaIndex() {
        return algoliaIndex;
    }

    public void setAlgoliaIndex(String algoliaIndex) {
        this.algoliaIndex = algoliaIndex;
    }

    public String getAlgoliaAppId() {
        return algoliaAppId;
    }

    public void setAlgoliaAppId(String algoliaAppId) {
        this.algoliaAppId = algoliaAppId;
    }

    public String getAlgoliaApiKey() {
        return algoliaApiKey;
    }

    public void setAlgoliaApiKey(String algoliaApiKey) {
        this.algoliaApiKey = algoliaApiKey;
    }

    public String getAlgoliaHits() {
        return algoliaHits;
    }

    public void setAlgoliaHits(String algoliaHits) {
        this.algoliaHits = algoliaHits;
    }

    public String getAlgoliaDiagnosticIndex() {
        return algoliaDiagnosticIndex;
    }

    public void setAlgoliaDiagnosticIndex(String algoliaDiagnosticIndex) {
        this.algoliaDiagnosticIndex = algoliaDiagnosticIndex;
    }

    public String getAlgoliaDiagnosticApiKey() {
        return algoliaDiagnosticApiKey;
    }

    public void setAlgoliaDiagnosticApiKey(String algoliaDiagnosticApiKey) {
        this.algoliaDiagnosticApiKey = algoliaDiagnosticApiKey;
    }
}
