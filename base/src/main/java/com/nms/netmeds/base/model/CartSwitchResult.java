package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSwitchResult {
    @SerializedName("product_code")
    private int productCode;
    @SerializedName("status")
    private String status;
    @SerializedName("result_reason")
    private String resultReson;
    @SerializedName("result_reason_code")
    private String resultReasonCode;

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResultReson() {
        return resultReson;
    }

    public void setResultReson(String resultReson) {
        this.resultReson = resultReson;
    }

    public String getResultReasonCode() {
        return resultReasonCode;
    }

    public void setResultReasonCode(String resultReasonCode) {
        this.resultReasonCode = resultReasonCode;
    }
}
