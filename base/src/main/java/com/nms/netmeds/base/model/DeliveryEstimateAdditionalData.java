package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DeliveryEstimateAdditionalData {
    @SerializedName("cold_storage_supported")
    private boolean coldStorageSupported;
    @SerializedName("unsupported_pin")
    private boolean isUnSupportedPincode;
    @SerializedName("cod_supported")
    private boolean codSupported;
    @SerializedName("pin_blocked")
    private boolean pinBlocked;
    @SerializedName("special_message")
    private String specialMessage;
    @SerializedName("inter_city_products")
    private ArrayList<String> interCityProductList;


    public boolean getColdStorageSupported() {
        return coldStorageSupported;
    }

    public void setColdStorageSupported(boolean coldStorageSupported) {
        this.coldStorageSupported = coldStorageSupported;
    }

    public boolean getCodSupported() {
        return codSupported;
    }

    public void setCodSupported(boolean codSupported) {
        this.codSupported = codSupported;
    }

    public boolean getPinBlocked() {
        return pinBlocked;
    }

    public void setPinBlocked(boolean pinBlocked) {
        this.pinBlocked = pinBlocked;
    }

    public String getSpecialMessage() {
        return specialMessage;
    }

    public void setSpecialMessage(String specialMessage) {
        this.specialMessage = specialMessage;
    }

    public boolean isUnSupportedPincode() {
        return isUnSupportedPincode;
    }

    public void setUnSupportedPincode(boolean unSupportedPincode) {
        isUnSupportedPincode = unSupportedPincode;
    }

    public ArrayList<String> getInterCityProductList() {
        return interCityProductList;
    }

    public void setInterCityProductList(ArrayList<String> interCityProductList) {
        this.interCityProductList = interCityProductList;
    }
}