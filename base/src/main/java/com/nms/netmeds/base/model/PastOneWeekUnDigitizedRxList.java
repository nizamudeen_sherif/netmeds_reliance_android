package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PastOneWeekUnDigitizedRxList {
    @SerializedName("rxId")
    private List<String> rxId = null;

    public List<String> getRxId() {
        return rxId;
    }

    public void setRxId(List<String> rxId) {
        this.rxId = rxId;
    }
}
