package com.nms.netmeds.base;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.model.CancelOrderResponseModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateLineItem;
import com.nms.netmeds.base.model.DeliveryEstimatePinCodeResult;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.DeliveryEstimateResult;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarOrderBillingAddress;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;
import com.nms.netmeds.base.model.OrderShippingInfo;
import com.nms.netmeds.base.model.Request.MstarTransactionLogRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;

public class OrderPayNowRetryReorderHelper extends AppViewModel {
    private static OrderHelperCallback callback;

    public BigDecimal totalAmount = BigDecimal.ZERO;
    private Integer cartId;
    private Integer cancelMessageIndex;
    private String cancelMessage;
    private String orderId;
    private String completeOrderResponse = "";
    private int billingAddressID = 0;
    public static final String CASH_ON_DELIVERY = "cashondelivery";
    static final String ORDERID = "orderId :";
    private BasePreference basePreference;
    private MStarCartDetails cartDetails;
    private boolean isForPayNow = false;
    private boolean isForRetry = false;
    private boolean isFromHelper = true;
    private boolean isPayWithCash = false;
    private boolean isCartOpen = false;
    public boolean isEditOrder = false;
    private List<Date> minDateRageList = new ArrayList<>();
    private List<Date> maxDateRageList = new ArrayList<>();


    public OrderPayNowRetryReorderHelper(@NonNull Application application) {
        super(application);
    }

    public void initiate(OrderHelperCallback callback, String orderId, OrderListenerTypeEnum typeEnum, BasePreference basePreference) {
        OrderPayNowRetryReorderHelper.callback = callback;
        PaymentHelper.setIsPayAndEditM2Order(false);
        this.orderId = orderId;
        this.basePreference = basePreference;
        startOrder(typeEnum);
    }

    public void initApiCall(int transactionId) {
        initMstarApi(transactionId);
    }

    public void cancelOrder(OrderHelperCallback callback, String orderId, OrderListenerTypeEnum typeEnum, BasePreference basePreference, Integer cancelMessageIndex, String cancelMessage) {
        OrderPayNowRetryReorderHelper.callback = callback;
        this.orderId = orderId;
        this.basePreference = basePreference;
        this.cancelMessageIndex = cancelMessageIndex;
        this.cancelMessage = cancelMessage;
        startOrder(typeEnum);
    }

    private void startOrder(OrderListenerTypeEnum typeEnum) {
        RefillHelper.resetMapValues();
        switch (typeEnum) {
            case PAY:
                isForPayNow = true;
                doM2Payment();
                break;
            case M1_RETRY:
                doRetry();
                break;
            case RE_ORDER:
                startM1ReOrder();
                break;
            case M2_RE_ORDER:
                doM2ReOrder();
                break;
            case PAY_WITH_COD:
                isPayWithCash = true;
                doM2Payment();
                break;
            case CANCEL_ORDER:
                doCancelOrder();
                break;
            case SUBSCRIPTION_PAY_NOW:
                initMstarApi(APIServiceManager.MSTAR_SUBSCRIPTION_PAYNOW);
                break;
            case EDIT_ORDER:
                isEditOrder = true;
                PaymentHelper.setIsExternalDoctor(false);
                doM2Payment();
        }
    }

    private void doCancelOrder() {
        initMstarApi(APIServiceManager.C_MSTAR_CANCEL_ORDER);
    }


    private void doRetry() {
        isForRetry = true;
        PaymentHelper.setIsTempCart(true);
        initMstarApi(APIServiceManager.MSTAR_GET_RETRY_CART);
    }

    private void doM2ReOrder() {
        PaymentHelper.setIsTempCart(false);
        PaymentHelper.setTempCartId(0);
        initMstarApi(APIServiceManager.MSTAR_GET_REORDER_CART);
    }

    private void doM2Payment() {
        PaymentHelper.setIsTempCart(true);
        PaymentHelper.setIsPayAndEditM2Order(true);
        initMstarApi(APIServiceManager.MSTAR_GET_FILLED_M2_CART);
    }

    private void startM1ReOrder() {
        PaymentHelper.setIsTempCart(false);
        PaymentHelper.setTempCartId(0);
        initMstarApi(APIServiceManager.MSTAR_GET_REORDER_CART);
    }


    private void initMstarApi(int transactionId) {
        boolean isConnected = callback.isNetworkConnected();
        callback.showNoNetworkError(isConnected);
        if (isConnected) {
            callback.showLoader();
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_MY_ORDER_DETAILS:
                    APIServiceManager.getInstance().MstarGetOrderDetails(this, basePreference.getMstarBasicHeaderMap(), getMstarOrderDetails());
                    break;
                case APIServiceManager.MSTAR_GET_FILLED_M2_CART:
                    APIServiceManager.getInstance().mstarGetFilledM2Cart(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarCustomerId(), orderId);
                    break;
                case APIServiceManager.MSTAR_GET_REORDER_CART:
                    APIServiceManager.getInstance().mstarGetReorderCart(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarCustomerId(), orderId);
                    break;
                case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                    ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
                    APIServiceManager.getInstance().deliveryEstimation(this, getDeliveryDateEstimateRequest(), configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDeliveryDateApi()) ? configurationResponse.getResult().getConfigDetails().getDeliveryDateApi() + "/" : "");
                    break;
                case APIServiceManager.MSTAR_GET_RETRY_CART:
                    APIServiceManager.getInstance().getMstarRetryCart(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarCustomerId(), orderId);
                    break;
                case APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK:
                    APIServiceManager.getInstance().checkCODEligibleForCreateNewOrder(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
                    APIServiceManager.getInstance().mStarOrderCompleteWithCOD(this, CommonUtils.getFingerPrintHeaderMap(callback.getContext()), cartId);
                    break;
                //todo need to merge(APIServiceManager.C_MSTAR_PAYMENT_LOG and PaymentServiceManager.C_MSTAR_PAYMENT_LOG)
                case APIServiceManager.C_MSTAR_PAYMENT_LOG:
                    APIServiceManager.getInstance().mStarPaymentLog(this, basePreference.getMstarBasicHeaderMap(), getTransactionLogRequest());
                    break;
                case APIServiceManager.C_MSTAR_CANCEL_ORDER:
                    APIServiceManager.getInstance().MstarCancelOrder(this, basePreference.getMstarBasicHeaderMap(), getOrderCancelRequest());
                    break;
                case APIServiceManager.MSTAR_SUBSCRIPTION_PAYNOW:
                    APIServiceManager.getInstance().subscriptionPayNow(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarCustomerId(), SubscriptionHelper.getInstance().getOrderId(), SubscriptionHelper.getInstance().getIssueId());
                    break;
                case APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS:
                    APIServiceManager.getInstance().getM3OrderDetails(this, basePreference.getMstarBasicHeaderMap(), SubscriptionHelper.getInstance().getIssueId(), SubscriptionHelper.getInstance().getOrderId());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                    if (basePreference.getPreviousCartBillingAddressId() > 0 || billingAddressID > 0)
                        APIServiceManager.getInstance().mStarSingleAddressDetails(this, basePreference.getMstarBasicHeaderMap(), basePreference.getPreviousCartShippingAddressId() > 0 ? basePreference.getPreviousCartShippingAddressId() : billingAddressID);
                    break;
                case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAddressID, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                    break;
                case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAddressID, APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId,
                            SubscriptionHelper.getInstance().getSubscriptionCartId());
                    break;
            }
        }

    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_MY_ORDER_DETAILS:
                onOrderDetailsResponse(data);
                break;
            case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                deliveryEstimateResponse(data);
                break;
            case APIServiceManager.C_MSTAR_PAYMENT_LOG:
                transactionLogResponse();
                break;
            case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
                orderCompleteWithCODResponse(data);
                break;
            case APIServiceManager.C_MSTAR_CANCEL_ORDER:
                cancelOrderSuccess(data);
                break;
            case APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK:
                codEligibleCheckResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_FILLED_M2_CART:
            case APIServiceManager.MSTAR_GET_REORDER_CART:
            case APIServiceManager.MSTAR_GET_RETRY_CART:
                onReorderResponse(data);
                break;
            case APIServiceManager.MSTAR_SUBSCRIPTION_PAYNOW:
                onSubscriptionPayNowResponse(data);
                break;
            case APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS:
                onSubscriptionItemDetails(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails(data);
                break;
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                singleAddressResponse(data);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, 0);
                break;
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                break;
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        callback.hideLoader();
        callback.onFailedFromHelper(transactionId, data, isFromHelper);
    }

    private void codEligibleCheckResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel codEligibleStatus = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (codEligibleStatus != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(codEligibleStatus.getStatus()) && codEligibleStatus.getResult() != null && codEligibleStatus.getResult().isCodEnable()) {
                startCashOnDeliveryProcess();
            } else {
                callback.hideLoader();
                callback.showMessage(getApplication().getResources().getString(R.string.text_cod_not_eligible));
            }
        } else {
            callback.hideLoader();
            callback.showMessage(getApplication().getResources().getString(R.string.text_cod_not_eligible));
        }
    }

    private void cancelOrderSuccess(String data) {
        CancelOrderResponseModel cancelOrderResponse = new Gson().fromJson(data, CancelOrderResponseModel.class);
        String message = cancelOrderResponse.getStatus();
        if (AppConstant.API_SUCCESS_STATUS.toLowerCase().equals(cancelOrderResponse.getStatus().toLowerCase())) {

            /*FireBase Analytics Event*/
            MstarOrderDetailsResult orderDetailsResult = PaymentHelper.getMstarOrderDetailsResult();
            if (orderDetailsResult != null) {
                /*Google Tag Manager + FireBase Refund Event*/
                FireBaseAnalyticsHelper.getInstance().logFireBaseRefundsEvent(getApplication().getApplicationContext(), orderDetailsResult);
            }
            callback.hideLoader();
            callback.cancelOrderSuccessful(message);
        } else {
            callback.hideLoader();
            callback.showCancelOrderError(message);
        }
    }

    private void orderCompleteWithCODResponse(String data) {
        callback.hideLoader();
        if (!TextUtils.isEmpty(data)) {
            setCompleteOrderResponse(data);
            MStarBasicResponseTemplateModel codDetailsResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (codDetailsResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(codDetailsResponse.getStatus())) {
                callback.navigateToSuccessPage(data);
            } else callback.navigateToCodFailure(totalAmount, cartId, orderId, false);
        }
    }

    private void transactionLogResponse() {
    }

    private MstarTransactionLogRequest getTransactionLogRequest() {
        MstarTransactionLogRequest transactionalLogRequest = new MstarTransactionLogRequest();
        transactionalLogRequest.setOrderId(!TextUtils.isEmpty(orderId) ? orderId : "");
        transactionalLogRequest.setPaymentMethod(CASH_ON_DELIVERY);
        String data = ORDERID + orderId + ", " + AppConstant.CARTID + ":" + cartId + ", " + CASH_ON_DELIVERY + ":" + totalAmount;
        transactionalLogRequest.setRequest(data);
        transactionalLogRequest.setResponse(data);
        transactionalLogRequest.setSourceName(AppConstant.SOURCE_NAME);
        return transactionalLogRequest;
    }

    private MultipartBody getOrderCancelRequest() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.ORDER_ID, orderId).addFormDataPart(AppConstant.CUSTOMER_MSG, cancelMessage).addFormDataPart(AppConstant.REASON, String.valueOf(cancelMessageIndex)).build();
    }


    private MultipartBody getMstarOrderDetails() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.ORDER_ID, orderId).build();
    }

    private DeliveryEstimateRequest getDeliveryDateEstimateRequest() {
        DeliveryEstimateRequest request = new DeliveryEstimateRequest();
        request.setCallingToRoute(false);
        request.setDoNotSplit(false);
        request.setPincode(PaymentHelper.getDeliveryPinCode());
        List<DeliveryEstimateLineItem> lineItems = new ArrayList<>();
        for (MStarProductDetails lineItem : cartDetails.getLines()) {
            DeliveryEstimateLineItem items = new DeliveryEstimateLineItem();
            items.setItemcode(String.valueOf(lineItem.getProductCode()));
            items.setQty(lineItem.getCartQuantity());
            lineItems.add(items);
        }
        request.setLstdrug(lineItems);
        return request;
    }

    private void onReorderResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            boolean isPrblmAddingInCart = false;
            boolean isProductsNotAdded = false;
            Map<String, MStarAddedProductsResult> outOfStockProducts = new HashMap<>();
            List<String> maxLimitBreachProducts = new ArrayList<>();
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && response.getResult().getAddedProductsResultMap() != null && !response.getResult().getAddedProductsResultMap().isEmpty() && response.getResult().getCartDetails() != null) {
                for (String productCode : response.getResult().getAddedProductsResultMap().keySet()) {
                    if (response.getResult().getAddedProductsResultMap().get(productCode) != null &&
                            !AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResult()) &&
                            AppConstant.MSTAR_ADD_PRODUCT_FAILURE.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResult())) {
                        if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResultReasonCode())) {
                            isProductsNotAdded = true;
                            outOfStockProducts.put(productCode, response.getResult().getAddedProductsResultMap().get(productCode));
                        } else if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResultReasonCode())) {
                            isProductsNotAdded = true;
                            maxLimitBreachProducts.add(productCode);
                        }
                        isPrblmAddingInCart = true;
                    }
                }
                if (response.getResult().getCartDetails().getPrescriptions() != null && !response.getResult().getCartDetails().getPrescriptions().isEmpty())
                    setPrescriptions(response.getResult().getCartDetails());
                this.isCartOpen = AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(response.getResult().getCartDetails().getCartStatus());
                this.cartId = response.getResult().getCartDetails().getId();
                this.totalAmount = response.getResult().getCartDetails().getNetPayableAmount();
                if (isEditOrder || isForPayNow || isForRetry || isPayWithCash) {
                    PaymentHelper.setIsTempCartOpen(isCartOpen);
                    PaymentHelper.setTempCartId(response.getResult().getCartDetails().getId());
                }
                cartDetails = response.getResult().getCartDetails();
                PaymentHelper.setCartLineItems(cartDetails.getLines());
                RefillHelper.setPrblmInAddingProducts(isPrblmAddingInCart);
//                if (isPrblmAddingInCart) {
//                    //DO NOTHING
//                    //TODO add failure case for Cart Status failure
//                    callback.hideLoader();
//                } else
                if (isEditOrder || !(isForPayNow || isForRetry || isPayWithCash) || isProductsNotAdded) {
                    callback.navigateToCartActivity(outOfStockProducts, maxLimitBreachProducts, isEditOrder, orderId);
                    callback.hideLoader();
                } else {
                    setPaymentDetailsAndStartPayment(response);
                }
            }
        }
    }

    private void setPrescriptions(MStarCartDetails cartDetails) {
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
        ArrayList<MStarUploadPrescription> prescriptionArrayList = new ArrayList<>();
        for (String uploadedPrescriptionId : cartDetails.getPrescriptions()) {
            MStarUploadPrescription prescriptionDetails = new MStarUploadPrescription();
            prescriptionDetails.setPrescriptionUploaded(true);
            prescriptionDetails.setUploadedPrescriptionId(uploadedPrescriptionId);
            prescriptionDetails.setPastPrescriptionId(uploadedPrescriptionId);
            prescriptionDetails.setDigitalized(true);
            prescriptionDetails.setDigitalizedPrescriptionId(uploadedPrescriptionId);
            prescriptionArrayList.add(prescriptionDetails);
        }
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(prescriptionArrayList);
    }

    private void deliveryEstimateResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            DeliveryEstimateResponse response = new Gson().fromJson(data, DeliveryEstimateResponse.class);
            if (response != null && response.getStatus() && response.getResult() != null) {
                if (!response.getResult().getaStatus().equalsIgnoreCase(getApplication().getString(R.string.text_ok))) {
                    callback.hideLoader();
                    if (response.getResult().getDeliveryEstimateAdditionalData() != null) {
                        if (isPayWithCash && !response.getResult().getDeliveryEstimateAdditionalData().getCodSupported()) {
                            callback.navigateToCodFailure(null, null, null, false);
                        } else if (response.getResult().getDeliveryEstimateAdditionalData().getInterCityProductList() != null && !response.getResult().getDeliveryEstimateAdditionalData().getInterCityProductList().isEmpty()) {
                            callback.navigateToCartForColdStorage(response.getResult().getDeliveryEstimateAdditionalData().getInterCityProductList());
                        } else {
                            callback.navigateToCartForColdStorage(new ArrayList<String>());
                        }
                    }else{
                        callback.navigateToCartForColdStorage(new ArrayList<String>());
                    }
                } else {
                    if (isPayWithCash) {
                        setDeliveryEstimation(response.getResult());
                        initMstarApi(APIServiceManager.C_MSTAR_COD_ELIGIBLE_CHECK);
                    } else {
                        moveFurtherWithPrepaidPayment(response);
                    }
                }
            }
        } else {
            callback.onFailedFromHelper(0, null, true);
        }
    }

    private void moveFurtherWithPrepaidPayment(DeliveryEstimateResponse response) {
        setDeliveryEstimation(response.getResult());
        if (SubscriptionHelper.getInstance().isSubscriptionFlag() || SubscriptionHelper.getInstance().isPayNowSubscription())
            initMstarApi(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
        else {
            callback.hideLoader();
            callback.redirectToPayment();
        }
    }

    private String getPincodeError(DeliveryEstimateResult response) {
        if (response.getDeliveryEstimateAdditionalData() != null && !TextUtils.isEmpty(response.getDeliveryEstimateAdditionalData().getSpecialMessage())) {
            return response.getDeliveryEstimateAdditionalData().getSpecialMessage();
        } else {
            return response.getaStatus();
        }
    }

    private void setDeliveryEstimation(DeliveryEstimateResult result) {
        minDateRageList = new ArrayList<>();
        maxDateRageList = new ArrayList<>();
        List<String> estimation = new ArrayList<>();
        List<String> estimationWithYear = new ArrayList<>();
        if (result.getPinCodeResultList() != null && result.getPinCodeResultList().size() > 0) {
            for (DeliveryEstimatePinCodeResult pinCodeResult : result.getPinCodeResultList()) {
                if (cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
                    for (MStarProductDetails cartLineItem : cartDetails.getLines()) {
                        if (pinCodeResult.getProductsList().contains(String.valueOf(cartLineItem.getProductCode()))) {
                            estimation.add(checkDeliveryEstimateRange(pinCodeResult.getDeliveryEstimate().getInDates().getOnOrAfter(), pinCodeResult.getDeliveryEstimate().getInDates().getOnOrBefore(), false));
                            estimationWithYear.add(checkDeliveryEstimateRange(pinCodeResult.getDeliveryEstimate().getInDates().getOnOrAfter(), pinCodeResult.getDeliveryEstimate().getInDates().getOnOrBefore(), true));
                        }
                    }
                }
            }
        }
        calculateMaxAndMinDeliveryEstimateDate();
        PaymentHelper.setDeliveryEstimateWithYear(DateTimeUtils.getInstance().currentTime(maxDateRageList.get(0), DateTimeUtils.ddMMMyyyy));
    }

    private void calculateMaxAndMinDeliveryEstimateDate() {
        Collections.sort(minDateRageList);
        Collections.sort(maxDateRageList, Collections.<Date>reverseOrder());
        Date minDate = minDateRageList.get(0);
        Date maxDate = maxDateRageList.get(0);
        Calendar minRange = Calendar.getInstance();
        minRange.setTime(minDate);
        Calendar maxRange = Calendar.getInstance();
        maxRange.setTime(maxDate);
        PaymentHelper.setDeliveryEstimate((minRange.get(Calendar.MONTH) == maxRange.get(Calendar.MONTH) ? minRange.get(Calendar.DAY_OF_MONTH) : DateTimeUtils.getInstance().currentTime(minDateRageList.get(0), DateTimeUtils.ddMMM)) + " to " + DateTimeUtils.getInstance().currentTime(maxDateRageList.get(0), DateTimeUtils.ddMMM));
    }

    private String checkDeliveryEstimateRange(String minDate, String maxDate, boolean isConvertWithYear) {
        String deliveryEstimateRange;
        Calendar minRange = DateTimeUtils.getInstance().getSeparatedCalendar(minDate);
        Calendar maxRange = DateTimeUtils.getInstance().getSeparatedCalendar(maxDate);
        minDateRageList.add(minRange.getTime());
        maxDateRageList.add(maxRange.getTime());
        if (minRange.get(Calendar.MONTH) == maxRange.get(Calendar.MONTH)) {
            deliveryEstimateRange = String.valueOf(minRange.get(Calendar.DAY_OF_MONTH)) + " to " +
                    DateTimeUtils.getInstance().stringDate(maxDate, DateTimeUtils.yyyyMMdd, isConvertWithYear ? DateTimeUtils.ddMMMyyyy : DateTimeUtils.ddMMM);
        } else
            deliveryEstimateRange = DateTimeUtils.getInstance().stringDate(minDate, DateTimeUtils.yyyyMMdd, DateTimeUtils.ddMMM) + " to " +
                    DateTimeUtils.getInstance().stringDate(maxDate, DateTimeUtils.yyyyMMdd, isConvertWithYear ? DateTimeUtils.ddMMMyyyy : DateTimeUtils.ddMMM);
        return deliveryEstimateRange;
    }

    private void setPaymentDetailsAndStartPayment(MStarBasicResponseTemplateModel response) {
        if (response.getResult() != null && response.getResult().getCartDetails() != null && !response.getResult().getCartDetails().getLines().isEmpty()) {
            MStarCartDetails cartDetails = response.getResult().getCartDetails();
            PaymentHelper.setCartLineItems(response.getResult().getCartDetails().getLines());

            if (cartDetails.getUsedWalletAmount() != null) {
                PaymentHelper.setIsNMSSuperCashApplied(cartDetails.getUsedWalletAmount().getNmsSuperCash().compareTo(BigDecimal.ZERO) > 0);
            }

            if (!cartDetails.getPrescriptions().isEmpty()) {
                ArrayList<MStarUploadPrescription> uploadPrescriptionList = new ArrayList<>();
                for (String prescriptionId : cartDetails.getPrescriptions()) {
                    MStarUploadPrescription uploadPrescription = new MStarUploadPrescription();
                    uploadPrescription.setDigitalized(true);
                    uploadPrescription.setDigitalizedPrescriptionId(prescriptionId);
                    uploadPrescription.setMethod2(true);
                    uploadPrescription.setPrescriptionUploaded(true);
                    uploadPrescription.setBitmapFromGalleryOrCamera(false);
                    uploadPrescription.setOrderReview(false);
                    uploadPrescription.setUploadedPrescriptionId(prescriptionId);
                    uploadPrescriptionList.add(uploadPrescription);
                }
                MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(uploadPrescriptionList);
            }
            if (SubscriptionHelper.getInstance().isPayNowSubscription() || SubscriptionHelper.getInstance().isSubscriptionEdit()) {
                initApiCall(APIServiceManager.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS);
            } else if (!(cartDetails.getBillingAddressId() > 0 && cartDetails.getShippingAddressId() > 0)) {
                callback.hideLoader();
                callback.navigateToAddAddress();
            } else {
                initMstarApi(APIServiceManager.C_MSTAR_MY_ORDER_DETAILS);
            }
        }
    }


    private void onOrderDetailsResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getOrderDetail() != null && response.getResult().getOrderDetail().getOrderBillingInfo() != null) {
                MStarAddressModel billingAddress = new MStarAddressModel();
                PaymentHelper.setMstarOrderDetailsResult(response.getResult().getOrderDetail());
                MstarOrderBillingAddress orderBillingAddress = response.getResult().getOrderDetail().getOrderBillingInfo();
                billingAddress.setFirstname(orderBillingAddress.getBillingFirstName());
                billingAddress.setLastname(orderBillingAddress.getBillingLastName());
                billingAddress.setStreet(orderBillingAddress.getBillingAddress1());
                billingAddress.setCity(orderBillingAddress.getBillingCity());
                billingAddress.setState(orderBillingAddress.getBillingState());
                billingAddress.setPin(orderBillingAddress.getBillingZipCode());
                billingAddress.setMobileNo(orderBillingAddress.getPhoneNumber());
                PaymentHelper.setDeliveryPinCode(orderBillingAddress.getBillingZipCode());
                PaymentHelper.setCustomerAddress(billingAddress);
                initMstarApi(APIServiceManager.DELIVERY_DATE_ESTIMATE);
            } else {
                callback.navigateToAddAddress();
                callback.hideLoader();
            }
        } else {
            callback.navigateToAddAddress();
            callback.hideLoader();
        }
    }

    private void onSubscriptionItemDetails(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getSubscriptinItemDetails() != null && response.getResult().getSubscriptinItemDetails().getShippingAddress() != null) {
                MStarAddressModel billingAddress = new MStarAddressModel();
                OrderShippingInfo orderBillingAddress = response.getResult().getSubscriptinItemDetails().getShippingAddress();
                billingAddress.setFirstname(orderBillingAddress.getShippingFirstName());
                billingAddress.setLastname(orderBillingAddress.getShippingLastName());
                billingAddress.setStreet(orderBillingAddress.getShippingAddress1());
                billingAddress.setCity(orderBillingAddress.getShippingCity());
                billingAddress.setState(orderBillingAddress.getShippingState());
                billingAddress.setPin(orderBillingAddress.getShippingZipCode());
                billingAddress.setMobileNo(orderBillingAddress.getShippingPhoneNumber());
                PaymentHelper.setDeliveryPinCode(orderBillingAddress.getShippingZipCode());
                PaymentHelper.setCustomerAddress(billingAddress);
                initMstarApi(APIServiceManager.DELIVERY_DATE_ESTIMATE);
            } else {
                callback.navigateToAddAddress();
                callback.hideLoader();
            }
        } else {
            callback.navigateToAddAddress();
            callback.hideLoader();
        }
    }

    private void onSubscriptionPayNowResponse(String data) {
        callback.hideLoader();
        if (!TextUtils.isEmpty(data)) {
            boolean isProductNotadded = false;
            boolean isProblemInAddingProduct = false;
            Map<String, MStarAddedProductsResult> outOfStockProducts = new HashMap<>();
            List<String> maxLimitBreachProducts = new ArrayList<>();
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && response.getResult().getAddedProductsResultMap() != null && !response.getResult().getAddedProductsResultMap().isEmpty()) {
                for (String productCode : response.getResult().getAddedProductsResultMap().keySet()) {
                    if (response.getResult().getAddedProductsResultMap().get(productCode) != null &&
                            !AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResult()) &&
                            AppConstant.MSTAR_ADD_PRODUCT_FAILURE.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResult())) {
                        if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResultReasonCode())) {
                            outOfStockProducts.put(productCode, response.getResult().getAddedProductsResultMap().get(productCode));
                            isProductNotadded = true;
                        } else if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(response.getResult().getAddedProductsResultMap().get(productCode).getResultReasonCode())) {
                            maxLimitBreachProducts.add(productCode);
                            isProductNotadded = true;
                        }
                        isProblemInAddingProduct = true;
                    }
                }
                this.isCartOpen = AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(response.getResult().getCartDetails().getCartStatus());
                this.cartId = response.getResult().getCartDetails().getId();
                SubscriptionHelper.getInstance().setSubscriptionCartId(cartId);
                cartDetails = response.getResult().getCartDetails();
                PaymentHelper.setCartLineItems(cartDetails.getLines());
                this.totalAmount = response.getResult().getCartDetails().getNetPayableAmount();
                RefillHelper.setPrblmInAddingProducts(isProblemInAddingProduct);
                if (isProductNotadded) {
                    callback.navigateToCartActivity(outOfStockProducts, maxLimitBreachProducts, isEditOrder, orderId);
                } else {
                    setPaymentDetailsAndStartPayment(response);
                }
            }
        }
    }

    private void getCustomerDetails(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getCustomerDetails() != null) {
            billingAddressID = response.getResult().getCustomerDetails().getPreferredBillingAddress();
            initMstarApi(APIServiceManager.MSTAR_GET_ADDRESS_BY_ID);
        }
    }

    private void singleAddressResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getAddressModel() != null) {
            PaymentHelper.setSingle_address(new Gson().toJson(response.getResult().getAddressModel()));
            PaymentHelper.setCustomerBillingAddress(response.getResult().getAddressModel());
            setShippingAndBillingAddress();
        }
    }

    private void settingBillingAndShippingAddressResponse(String data, int mstarSetPreferredShippingAddress) {
        callback.hideLoader();
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && mstarSetPreferredShippingAddress == 50013) {
            callback.redirectToPayment();
        }
    }

    private void setShippingAndBillingAddress() {
        initMstarApi(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
        initMstarApi(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
        initMstarApi(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
        initMstarApi(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
    }

    private void startCashOnDeliveryProcess() {
        initMstarApi(APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD);
    }

    public void setCompleteOrderResponse(String completeOrderResponse) {
        this.completeOrderResponse = completeOrderResponse;
    }

    public interface OrderHelperCallback {

        void navigateToAddAddress();

        void redirectToPayment();

        void navigateToCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, List<String> maxLimitBreachProducts, boolean isEditOrder, String orderId);

        void hideLoader();

        boolean isNetworkConnected();

        void showNoNetworkError(boolean isConnected);

        void showLoader();

        void showCancelOrderError(String s);

        void onFailedFromHelper(int transactionId, String data, boolean isFromHelper);

        void cancelOrderSuccessful(String message);

        void navigateToSuccessPage(String data);

        void navigateToCodFailure(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct);

        void showMessage(String string);

        void navigateToCartForColdStorage(ArrayList<String> interCityProductList);

        Context getContext();
    }

}
