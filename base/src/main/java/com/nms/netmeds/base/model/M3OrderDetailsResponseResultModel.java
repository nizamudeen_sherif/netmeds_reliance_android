package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M3OrderDetailsResponseResultModel {

    @SerializedName("subscriptionId")
    private String subscriptionId;
    @SerializedName("issueOrderId")
    private String issueOrderId;
    @SerializedName("primaryOrderId")
    private String primaryOrderId;
    @SerializedName("subscriptionStatus")
    private String subscriptionStatus;
    @SerializedName("patientName")
    private String patientName;
    @SerializedName("paymentButtonStatus")
    private String paymentButtonStatus;
    @SerializedName("issueStatus")
    private String issueStatus;
    @SerializedName("issueStatusDescription")
    private String issueStatusDescription;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("subTotal")
    private String subTotal;
    @SerializedName("discount")
    private String discount;
    @SerializedName("isSkipped")
    private Boolean isSkipped;
    @SerializedName("isReschedule")
    private Boolean isReschedule;
    @SerializedName("isEditable")
    private Boolean isEditable;
    @SerializedName("orderPlacedDate")
    private String orderPlacedDate;
    @SerializedName("promiseDeliveryDate")
    private String promiseDeliveryDate;
    @SerializedName("issueDate")
    private String issueDate;
    @SerializedName("subscribedDate")
    private String subscribedDate;
    @SerializedName("shippingAddress")
    private OrderShippingInfo shippingAddress;
    @SerializedName("lineItems")
    private List<DrugDetail> lineItems = null;
    @SerializedName("rxId")
    private List<String> rxId = null;

    public List<String> getRxId() {
        return rxId;
    }

    public void setRxId(List<String> rxId) {
        this.rxId = rxId;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getIssueOrderId() {
        return issueOrderId;
    }

    public void setIssueOrderId(String issueOrderId) {
        this.issueOrderId = issueOrderId;
    }

    public String getPrimaryOrderId() {
        return primaryOrderId;
    }

    public void setPrimaryOrderId(String primaryOrderId) {
        this.primaryOrderId = primaryOrderId;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPaymentButtonStatus() {
        return paymentButtonStatus;
    }

    public void setPaymentButtonStatus(String paymentButtonStatus) {
        this.paymentButtonStatus = paymentButtonStatus;
    }

    public String getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(String issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getIssueStatusDescription() {
        return issueStatusDescription;
    }

    public void setIssueStatusDescription(String issueStatusDescription) {
        this.issueStatusDescription = issueStatusDescription;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Boolean getIsSkipped() {
        return isSkipped;
    }

    public void setIsSkipped(Boolean isSkipped) {
        this.isSkipped = isSkipped;
    }

    public Boolean getIsReschedule() {
        return isReschedule;
    }

    public void setIsReschedule(Boolean isReschedule) {
        this.isReschedule = isReschedule;
    }

    public Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public String getOrderPlacedDate() {
        return orderPlacedDate;
    }

    public void setOrderPlacedDate(String orderPlacedDate) {
        this.orderPlacedDate = orderPlacedDate;
    }

    public String getPromiseDeliveryDate() {
        return promiseDeliveryDate;
    }

    public void setPromiseDeliveryDate(String promiseDeliveryDate) {
        this.promiseDeliveryDate = promiseDeliveryDate;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getSubscribedDate() {
        return subscribedDate;
    }

    public void setSubscribedDate(String subscribedDate) {
        this.subscribedDate = subscribedDate;
    }

    public OrderShippingInfo getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(OrderShippingInfo shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<DrugDetail> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<DrugDetail> lineItems) {
        this.lineItems = lineItems;
    }

}