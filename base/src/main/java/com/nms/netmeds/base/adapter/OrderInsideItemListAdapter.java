package com.nms.netmeds.base.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.databinding.AdapterOrderItemListBinding;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarDrugDetail;
import com.nms.netmeds.base.utils.BasePreference;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.nms.netmeds.base.CommonUtils.getPriceInFormat;

public class OrderInsideItemListAdapter extends RecyclerView.Adapter<OrderInsideItemListAdapter.ItemListViewHolder> {

    private final Context context;
    private final List<MstarDrugDetail> detailsList;
    private Map<String, MStarProductDetails> productCodeWithPriceMap;
    private final boolean isM2Order;
    private final boolean isPaymentPending;
    private final boolean isOrderDelivered;
    private OrderItemListCallBack callBack;

    public OrderInsideItemListAdapter(Context context, List<MstarDrugDetail> detailsList, Map<String, MStarProductDetails> productCodeWithPriceMap,
                                      boolean isM2Order, boolean isOrderDelivered, boolean isPaymentPending, OrderItemListCallBack callBack) {
        this.context = context;
        this.detailsList = detailsList;
        this.isM2Order = isM2Order;
        this.isPaymentPending = isPaymentPending;
        this.isOrderDelivered = isOrderDelivered;
        this.callBack = callBack;
        this.productCodeWithPriceMap = productCodeWithPriceMap;
    }

    @NonNull
    @Override
    public ItemListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AdapterOrderItemListBinding listBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_order_item_list, viewGroup, false);
        return new ItemListViewHolder(listBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemListViewHolder holder, int position) {
        final MstarDrugDetail drugDetail = detailsList.get(position);
        boolean isOriginalPriceAvailable =  isPaymentPending && productCodeWithPriceMap!= null && !productCodeWithPriceMap.isEmpty() && productCodeWithPriceMap.containsKey(String.valueOf(drugDetail.getDrugCode()));
        holder.listBinding.tvDrugName.setText(drugDetail.getBrandName());
        holder.listBinding.tvAlgoliaPrice.setText(getPriceInFormat( isOriginalPriceAvailable ?
                (productCodeWithPriceMap.get(String.valueOf(drugDetail.getDrugCode())).getSellingPrice().multiply(BigDecimal.valueOf(drugDetail.getPurchaseQuantity())).doubleValue()) :
                drugDetail.getPurchasePrice().doubleValue()));
        holder.listBinding.trQuantity.setVisibility(isPrimeProduct(drugDetail.getDrugCode()) ? View.GONE : View.VISIBLE);
        holder.listBinding.imgProductImage.setImageDrawable(ContextCompat.getDrawable(context, isPrimeProduct(drugDetail.getDrugCode()) ? isOrderDelivered ? R.drawable.ic_super_member : R.drawable.ic_crown_circle : R.drawable.ic_no_image));
        holder.listBinding.tvDrugQuantity.setText(getBoldFormatQuantity(drugDetail.getPurchaseQuantity()));
        holder.listBinding.imgRxImage.setVisibility(drugDetail.getPrescriptionNeeded() != null && drugDetail.getPrescriptionNeeded().equals(AppConstant.PRESCRIPTION_NEEDED_Y) ? View.VISIBLE : View.GONE);
        holder.listBinding.divider.setVisibility((isM2Order && (getItemCount() - 1 == position)) ? View.GONE : View.VISIBLE);
        holder.listBinding.tvDrugName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onItemClickedCallBack(drugDetail.getDrugCode(), isPrimeProduct(drugDetail.getDrugCode()));
            }
        });
    }

    private boolean isPrimeProduct(long drugCode) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        List<String> primeCodeList = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode().size() > 0 ? configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode() : new ArrayList<String>();
        return primeCodeList.contains(String.valueOf(drugCode));
    }
    @Override
    public int getItemCount() {
        return detailsList.size();
    }

    private SpannableString getBoldFormatQuantity(int productQuantity) {
        String quantity = String.valueOf(productQuantity);
        String totalString = String.format(context.getString(R.string.text_quantity_with_count), quantity);
        SpannableString resultString = new SpannableString(totalString);
        resultString.setSpan(new StyleSpan(Typeface.BOLD), (totalString.length() - quantity.length()), totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return resultString;
    }

    class ItemListViewHolder extends RecyclerView.ViewHolder {

        private final AdapterOrderItemListBinding listBinding;

        ItemListViewHolder(@NonNull AdapterOrderItemListBinding listBinding) {
            super(listBinding.getRoot());
            this.listBinding = listBinding;
        }
    }

    public interface OrderItemListCallBack {
        void onItemClickedCallBack(int productCode, boolean isPrimeProduct);
    }
}
