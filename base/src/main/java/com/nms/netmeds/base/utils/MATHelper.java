/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.utils;


import android.app.Application;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.tune.ITune;
import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;
import com.tune.TuneGender;

import java.util.List;

public class MATHelper {
    public static final String NON_PRESCRIPTION = "Non-Prescription";
    public static final String PRESCRIPTION = "Prescription";
    public static final String MIXED_ORDER = "Mixed Order";
    private static final String EVENT_NAME_FIRST_PURCHASE = "First Purchase";
    private static final String EVENT_NAME_FIRST_CONSULTATION_PURCHASE = "First Consultation Purchase";
    private static final String EVENT_NAME_FIRST_DIAGNOSTIC_PURCHASE = "First Diagnostic Purchase";
    private static final String EVENT_NAME_CONSULTATION_PURCHASE = "Consultation Purchase";
    private static final String EVENT_NAME_DIAGNOSTIC_PURCHASE = "Diagnostic Purchase";
    private static final String CURRENCY = "INR";
    private static final String FIRST = "FIRST";
    private static MATHelper matHelper;
    private static final String FROM_CONSULTATION = "from consultation";
    private static final String FROM_DIAGNOSTIC = "from diagnostic";


    /**
     * Create a getInstance() method to return the instance of
     * MATHelper.This function ensures that
     * the variable is instantiated only once and the same
     * instance is used entire application
     *
     * @return MATHelper instance
     */
    public static MATHelper getInstance() {
        if (matHelper == null) {
            matHelper = new MATHelper();
        }
        return matHelper;
    }

    /**
     * init tune sdk and set tune advertise id,tune conversion key
     *
     * @param application application instance
     */
    public void initTune(Application application) {
        ConfigMap configMap = ConfigMap.getInstance();
        Tune.init(application.getApplicationContext(), configMap.getProperty(ConfigConstant.TUNE_ADVERTISER_ID), configMap.getProperty(ConfigConstant.TUNE_CONVERSION_KEY));
    }

    /**
     * Set existing user flag
     *
     * @param application application instance
     */
    public void setExistingUserMAT(Application application) {
        Tune.getInstance().setExistingUser(BasePreference.getInstance(application.getApplicationContext()).isMstarSessionIdAvailable());
    }

    /**
     * Tune login/registration measure event
     *
     * @param basePreference shared preference object
     * @param isRegistration check event from login or registration
     */
    public void loginAndRegistrationMATEvent(BasePreference basePreference, boolean isRegistration) {
        if (CommonUtils.isBuildVariantProdRelease()) {
            MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
            String userId = customerDetails != null ? Integer.toString(customerDetails.getId()) : "";
            String firstName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "";
            String email = customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "";
            String phoneNo = customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "";

            if (Tune.getInstance() != null) {
                ITune tune = Tune.getInstance();
                tune.setUserEmail(email);
                tune.setUserName(firstName);
                tune.setUserId(userId);
                tune.setGender(TuneGender.UNKNOWN);
                tune.setPhoneNumber(phoneNo);
                tune.measureEvent(isRegistration ? TuneEvent.REGISTRATION : TuneEvent.LOGIN);
            }
        }
    }

    /**
     * Tune purchase event
     *
     * @param basePreference shared preference instance
     * @param items          line items list
     * @param revenue        total amount
     * @param orderId        order id
     * @param appFirstOrder  check order is first or not
     * @param orderType      order type like mixed,prescription,OTC
     */
    public void purchaseMATEvent(BasePreference basePreference, List<TuneEventItem> items, double revenue, String orderId, String appFirstOrder, String orderType, String whereFrom) {
        if (CommonUtils.isBuildVariantProdRelease()) {
            String firstOrder = !TextUtils.isEmpty(appFirstOrder) ? appFirstOrder.equalsIgnoreCase(AppConstant.YES) ? FIRST : "" : "";
            MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
            String userId = customerDetails != null ? Integer.toString(customerDetails.getId()) : "";
            String eventName = whereFrom.equals(FROM_CONSULTATION) ? EVENT_NAME_CONSULTATION_PURCHASE : whereFrom.equals(FROM_DIAGNOSTIC) ?
                    EVENT_NAME_DIAGNOSTIC_PURCHASE : TuneEvent.PURCHASE;
            postEvent(eventName, userId, firstOrder, items, revenue, orderId, orderType);
            if (firstOrder.equals(FIRST)) {
                if (whereFrom.equals(FROM_CONSULTATION)) {
                    postEvent(EVENT_NAME_FIRST_CONSULTATION_PURCHASE, userId, firstOrder, items, revenue, orderId, orderType);
                } else if (whereFrom.equals(FROM_DIAGNOSTIC)) {
                    postEvent(EVENT_NAME_FIRST_DIAGNOSTIC_PURCHASE, userId, firstOrder, items, revenue, orderId, orderType);
                } else {
                    postEvent(EVENT_NAME_FIRST_PURCHASE, userId, firstOrder, items, revenue, orderId, orderType);
                }
            }
        }
    }

    private void postEvent(String eventName, String userId, String firstOrder, List<TuneEventItem> items, double revenue, String orderId, String orderType) {
        if (Tune.getInstance() != null) {
            ITune iTune = Tune.getInstance();
            iTune.setUserId(userId);
            iTune.measureEvent(new TuneEvent(eventName).withEventItems(items)
                    .withRevenue(revenue)
                    .withCurrencyCode(CURRENCY)
                    .withAttribute1(firstOrder)
                    .withAttribute2(orderType)
                    .withAdvertiserRefId(orderId));
        }
    }
}
