package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartResponseItem {

    @SerializedName("items")
    private List<CartItemResult> result = null;
    @SerializedName("cartCount")
    private int cartCount;
    @SerializedName("cartId")
    private String cartId;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public List<CartItemResult> getResult() {
        return result;
    }

    public void setResult(List<CartItemResult> result) {
        this.result = result;
    }

    public int getCartCount() {
        return cartCount;
    }

    public void setCartCount(int cartCount) {
        this.cartCount = cartCount;
    }
}
