package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.PastOneWeekUnDigitizedRxList;

public class GetPastPrescriptionResult {
    @SerializedName("pastOneWeekUnDigitizedRxCount")
    private int pastOneWeekUnDigitizedRxCount;
    @SerializedName("pastOneWeekUnDigitizedRxList")
    private PastOneWeekUnDigitizedRxList pastOneWeekUnDigitizedRxList;
    @SerializedName("digitizedPrescriptionCount")
    private int digitizedPrescriptionCount;
    @SerializedName("digitizedPrescriptionList")
    private PastOneWeekUnDigitizedRxList digitizedPrescriptionList;

    public Integer getPastOneWeekUnDigitizedRxCount() {
        return pastOneWeekUnDigitizedRxCount;
    }

    public void setPastOneWeekUnDigitizedRxCount(Integer pastOneWeekUnDigitizedRxCount) {
        this.pastOneWeekUnDigitizedRxCount = pastOneWeekUnDigitizedRxCount;
    }

    public PastOneWeekUnDigitizedRxList getPastOneWeekUnDigitizedRxList() {
        return pastOneWeekUnDigitizedRxList;
    }

    public void setPastOneWeekUnDigitizedRxList(PastOneWeekUnDigitizedRxList pastOneWeekUnDigitizedRxList) {
        this.pastOneWeekUnDigitizedRxList = pastOneWeekUnDigitizedRxList;
    }

    public Integer getDigitizedPrescriptionCount() {
        return digitizedPrescriptionCount;
    }

    public void setDigitizedPrescriptionCount(Integer digitizedPrescriptionCount) {
        this.digitizedPrescriptionCount = digitizedPrescriptionCount;
    }

    public PastOneWeekUnDigitizedRxList getDigitizedPrescriptionList() {
        return digitizedPrescriptionList;
    }

    public void setDigitizedPrescriptionList(PastOneWeekUnDigitizedRxList digitizedPrescriptionList) {
        this.digitizedPrescriptionList = digitizedPrescriptionList;
    }
}
