package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliveryEstimateRequest {
    @SerializedName("lstdrug")
    private List<DeliveryEstimateLineItem> lstdrug = null;
    @SerializedName("pincode")
    private String pincode;
    @SerializedName("do_not_split")
    private Boolean doNotSplit;
    @SerializedName("calling_to_route")
    private Boolean callingToRoute;

    public List<DeliveryEstimateLineItem> getLstdrug() {
        return lstdrug;
    }

    public void setLstdrug(List<DeliveryEstimateLineItem> lstdrug) {
        this.lstdrug = lstdrug;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Boolean getDoNotSplit() {
        return doNotSplit;
    }

    public void setDoNotSplit(Boolean doNotSplit) {
        this.doNotSplit = doNotSplit;
    }

    public Boolean getCallingToRoute() {
        return callingToRoute;
    }

    public void setCallingToRoute(Boolean callingToRoute) {
        this.callingToRoute = callingToRoute;
    }
}
