/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OtcProductListResponse {
    @SerializedName("items")
    private ArrayList<HRVProduct> OtcProductList = null;
    @SerializedName("count")
    private Integer count;
    @SerializedName("totalcount")
    private Integer totalCount;
    @SerializedName("sortoptionLabel")
    private List<SortOptionLabel> sortOptionLabel;
    @SerializedName("category_banner")
    private ArrayList<MstarBanner> mstarBanner = null;
    @SerializedName("show_banner")
    private String showBanner;
    @SerializedName("slide_banner")
    private List<SlideBanner> slideBanner;
    @SerializedName("brandFilter")
    private List<BrandFilter> brandFilter = null;

    public List<BrandFilter> getBrandFilter() {
        return brandFilter;
    }

    public void setBrandFilter(List<BrandFilter> brandFilter) {
        this.brandFilter = brandFilter;
    }

    public ArrayList<HRVProduct> getOtcProductList() {
        return OtcProductList;
    }

    public void setOtcProductList(ArrayList<HRVProduct> otcProductList) {
        OtcProductList = otcProductList;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public List<SortOptionLabel> getSortOptionLabel() {
        return sortOptionLabel;
    }

    public void setSortOptionLabel(List<SortOptionLabel> sortOptionLabel) {
        this.sortOptionLabel = sortOptionLabel;
    }

    public ArrayList<MstarBanner> getMstarBanner() {
        return mstarBanner;
    }

    public void setMstarBanner(ArrayList<MstarBanner> mstarBanner) {
        this.mstarBanner = mstarBanner;
    }

    public List<SlideBanner> getSlideBanner() {
        return slideBanner;
    }

    public String getShowBanner() {
        return showBanner;
    }

    public void setShowBanner(String showBanner) {
        this.showBanner = showBanner;
    }

    public void setSlideBanner(List<SlideBanner> slideBanner) {
        this.slideBanner = slideBanner;
    }
}
