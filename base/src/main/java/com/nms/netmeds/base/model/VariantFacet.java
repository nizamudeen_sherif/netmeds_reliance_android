package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class VariantFacet {
    @SerializedName("current")
    private boolean isCurrentSelectedVariant;
    @SerializedName("product_code")
    private int productCode;
    @SerializedName("product_url")
    private String product_url;
    @SerializedName("value")
    private String value;

    public boolean isCurrentSelectedVariant() {
        return isCurrentSelectedVariant;
    }

    public void setCurrentSelectedVariant(boolean currentSelectedVariant) {
        this.isCurrentSelectedVariant = currentSelectedVariant;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }
}
