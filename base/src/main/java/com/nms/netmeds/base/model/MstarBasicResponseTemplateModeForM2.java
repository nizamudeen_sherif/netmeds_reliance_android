package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MstarBasicResponseTemplateModeForM2 {
    @SerializedName("status")
    private String status;
    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel reason;
    @SerializedName("result")
    private ArrayList<Integer> method_2_carts;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MstarBasicResponseFailureReasonTemplateModel getReason() {
        return reason;
    }

    public void setReason(MstarBasicResponseFailureReasonTemplateModel reason) {
        this.reason = reason;
    }

    public ArrayList<Integer> getMethod_2_carts() {
        return method_2_carts;
    }

    public void setMethod_2_carts(ArrayList<Integer> method_2_carts) {
        this.method_2_carts = method_2_carts;
    }
}
