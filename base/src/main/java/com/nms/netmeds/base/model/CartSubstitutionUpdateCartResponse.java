package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSubstitutionUpdateCartResponse {
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private String result;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public String getResult() {
        return result;
    }
}
