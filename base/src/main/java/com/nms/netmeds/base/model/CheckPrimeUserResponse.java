package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CheckPrimeUserResponse extends BaseResponse {
    @SerializedName("result")
    private PrimeUserResult primeUserResult;

    public PrimeUserResult getPrimeUserResult() {
        return primeUserResult;
    }

    public void setPrimeUserResult(PrimeUserResult primeUserResult) {
        this.primeUserResult = primeUserResult;
    }
}
