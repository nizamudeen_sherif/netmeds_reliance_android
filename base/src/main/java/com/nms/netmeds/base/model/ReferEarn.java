package com.nms.netmeds.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferEarn {
    @SerializedName("invite")
    private ReferEarnInvite invite;
    @SerializedName("cashBack")
    private ReferEarnCashBack cashBack;
    @SerializedName("offer")
    private ReferEarnOffer offer;
    @SerializedName("orderSuccess")
    @Expose
    private OrderSuccess orderSuccess;

    public ReferEarnInvite getInvite() {
        return invite;
    }

    public void setInvite(ReferEarnInvite invite) {
        this.invite = invite;
    }

    public ReferEarnCashBack getCashBack() {
        return cashBack;
    }

    public void setCashBack(ReferEarnCashBack cashBack) {
        this.cashBack = cashBack;
    }

    public ReferEarnOffer getOffer() {
        return offer;
    }

    public void setOffer(ReferEarnOffer offer) {
        this.offer = offer;
    }

    public OrderSuccess getOrderSuccess() {
        return orderSuccess;
    }

    public void setOrderSuccess(OrderSuccess orderSuccess) {
        this.orderSuccess = orderSuccess;
    }


}
