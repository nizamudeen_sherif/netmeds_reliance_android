package com.nms.netmeds.base.retrofit;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseServiceManager;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ApplyVoucherRequest;
import com.nms.netmeds.base.model.ApplyVoucherResponse;
import com.nms.netmeds.base.model.CancelOrderResponseModel;
import com.nms.netmeds.base.model.CartSubstitutionBasicResponseTemplateModel;
import com.nms.netmeds.base.model.CartSubstitutionRequest;
import com.nms.netmeds.base.model.CartSubstitutionResponse;
import com.nms.netmeds.base.model.CartSubstitutionUpdateCartRequest;
import com.nms.netmeds.base.model.CartSubstitutionUpdateCartResponse;
import com.nms.netmeds.base.model.CheckPrimeUserResponse;
import com.nms.netmeds.base.model.ColdStorageResponse;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.DigitizedRxResponse;
import com.nms.netmeds.base.model.Flow2SubscriptionRequest;
import com.nms.netmeds.base.model.GenerateOrderIdRequest;
import com.nms.netmeds.base.model.GetCityStateFromPinCodeResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResponse;
import com.nms.netmeds.base.model.M3Flow2SubscriptionResponse;
import com.nms.netmeds.base.model.M3SubscriptionLogRequest;
import com.nms.netmeds.base.model.MStarAddAddressModelRequest;
import com.nms.netmeds.base.model.MStarAddMultipleProductsResponse;
import com.nms.netmeds.base.model.MStarBase2Response;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerRegistrationResponse;
import com.nms.netmeds.base.model.MStarLinkedPaymentResponse;
import com.nms.netmeds.base.model.MstarAddressResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarBasicResponseTemplateModeForM2;
import com.nms.netmeds.base.model.MstarChangePasswordResponse;
import com.nms.netmeds.base.model.MstarEditAddressRequest;
import com.nms.netmeds.base.model.MstarGenericMedicinesResponse;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.nms.netmeds.base.model.MstarSubmitMethod2Response;
import com.nms.netmeds.base.model.MstarSwitchAlternateProductRequest;
import com.nms.netmeds.base.model.RecommendedProductResponse;
import com.nms.netmeds.base.model.Request.MStarRegistrationRequest;
import com.nms.netmeds.base.model.Request.MstarBrandProductListRequest;
import com.nms.netmeds.base.model.Request.MstarCategoryDetailsRequest;
import com.nms.netmeds.base.model.Request.MstarManufacturerDetailsRequest;
import com.nms.netmeds.base.model.Request.MstarTransactionLogRequest;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;
import com.nms.netmeds.base.utils.BasePreference;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APIServiceManager extends BaseServiceManager {

    public static final int DELIVERY_DATE_ESTIMATE = 155;
    public static final int CHECK_COLD_STORAGE = 152;
    public static final int M3_FLOW2_SUBSCRIPTION = 412;
    public static final int M3_SUBSCRIPTION_LOG = 156;
    public static final int REMOVE_VOUCHER = 243;
    public static final int CHECK_PRIME_USER = 1001;

    //Morning Star
    public static final int MSTAR_CONFIG_PATH_URL = 49999;
    public static final int MSTAR_GET_PAYMENT_WALLET_BALANCE = 51001;
    public static final int MSTAR_CREATE_CART = 50002;
    public static final int MSTAR_ADD_PRODUCT_TO_CART = 50003;
    public static final int MSTAR_REMOVE_PRODUCT_FROM_CART = 50004;
    public static final int MSTAR_GET_CART_DETAILS = 50005;
    public static final int MSTAR_INITIATE_CHECKOUT = 50006;
    public static final int MSTAR_APPLY_PROMO_CODE = 50007;
    public static final int MSTAR_UN_APPLY_PROMO_CODE = 50008;
    public static final int MSTAR_CONTINUE_SHOPPING = 50009;
    public static final int MSTAR_GET_ALL_ADDRESS = 50010;
    public static final int MSTAR_DELETE_ADDRESS = 50011;
    public static final int MSTAR_SET_PREFERRED_BILLING_ADDRESS = 50012;
    public static final int MSTAR_SET_PREFERRED_SHIPPING_ADDRESS = 50013;
    public static final int MSTAR_CUSTOMER_DETAILS = 50014;
    public static final int MSTAR_GET_ADDRESS_BY_ID = 50015;
    public static final int MSTAR_ADD_NEW_ADDRESS = 50016;
    public static final int MSTAR_UPDATE_ADDRESS = 50017;
    public static final int MSTAR_PRODUCT_DETAILS = 50019;
    public static final int MSTAR_PRODUCT_DETAILS_AND_ADD_TO_CART = 50020;
    public static final int MSTAR_GET_PRODUCT_FOR_ID_LIST = 50021;
    public static final int MSTAR_SIMILAR_PRODUCT = 50022;
    public static final int MSTAR_FREQUENTLY_BROUGHT_TOGETHER_PRODUCT = 50023;
    public static final int MSTAR_CATEGORY_DETAILS = 50030;
    public static final int MSTAR_MANUFACTURER_DETAILS = 50031;
    public static final int MSTAR_SENT_OTP = 50032;
    public static final int MSTAR_VERIFY_OTP = 50033;
    public static final int MSTAR_PRIME_PRODUCT = 50034;
    public static final int MSTAR_APPLY_VOUCHER = 50035;
    public static final int MSTAR_UNAPPLY_VOUCHER = 50036;
    public static final int MSTAR_UPLOAD_PRESCRIPTION = 50037;
    public static final int MSTAR_DETACH_PRESCRIPTION = 50038;
    public static final int MSTAR_GET_METHOD_2_CARTS = 50039;
    public static final int MSTAR_SUBMIT_METHOD_2 = 50040;
    public static final int MSTAR_BRAND_PRODUCT_LIST = 50041;
    public static final int MSTAR_CLEAR_UNIVERSAL_CART = 50042;
    public static final int MSTAR_INITIATE_CHANGE_PSSWRD = 50044;
    public static final int MSTAR_COMPLETE_CHANGE_PSSWRD = 50045;
    public static final int MSTAR_RESEND_SAME_OTP = 50046;
    public static final int MSTAR_SET_M2_BILLING_ADDRESS = 50047;
    public static final int MSTAR_SET_M2_SHIPPING_ADDRESS = 50048;

    public static final int MSTAR_ADD_MULTIPLE_PRODUCTS = 50049;

    public static final int MSTAR_LOGIN = 50052;
    public static final int MSTAR_CREATE_GUEST_SESSION = 50053;
    public static final int MSTAR_GET_USER_STATUS = 50054;
    public static final int MSTAR_CUSTOMER_REGISTRATION = 50055;
    public static final int MSTAR_UPDATE_CUSTOMER = 50056;
    public static final int MSTAR_GET_REORDER_CART = 50057;
    public static final int MSTAR_GET_FILLED_M2_CART = 50058;
    public static final int MSTAR_GET_RETRY_CART = 50059;
    public static final int MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART = 50060;
    public static final int MSTAR_SWITCH_ALTERNATE_PRODUCT = 50061;
    public static final int MSTAR_REVERT_ORIGINAL_CART = 50062;

    public static final int C_MSTAR_HOME_BANNER = 50100;
    public static final int C_MSTAR_WALLET_DETAILS = 50101;
    public static final int C_MSTAR_REFER_AND_EARN = 50102;
    public static final int C_MSTAR_ORDERS_HEADER = 50103;
    public static final int C_MSTAR_MY_ORDER_DETAILS = 50104;
    public static final int C_MSTAR_TRACK_ORDER = 50105;
    public static final int C_MSTAR_CANCEL_ORDER = 50106;
    public static final int C_MSTAR_METHOD2_ORDER_DETAIL = 50107;
    public static final int C_MSTAR_ALL_OFFERS = 50108;
    public static final int C_MSTAR_GATEWAY_OFFERS = 50109;
    public static final int C_MSTAR_DIGITIZED_RX = 50110;
    public static final int C_MSTAR_PAST_PRESCRIPTION = 50111;
    public static final int C_MSTAR_HEALTH_ARTICLES = 50112;
    public static final int C_MSTAR_WELLNESS_PAGE_DETAILS = 50113;
    public static final int C_MSTAR_HIGH_RANGE_TICKET_POPUP = 50114;
    public static final int C_MSTAR_REQUEST_NEW_PRODUCT = 50115;
    public static final int C_MSTAR_NOTIFY_ME = 50116;
    public static final int C_MSTAR_MY_PRESCRIPTION = 50117;
    public static final int C_MSTAR_PROMOTION_BANNER = 50118;
    public static final int C_MSTAR_HEALTHCONCERN_BANNER = 50119;
    public static final int C_MSTAR_GET_LINKED_PAYMENT = 50120;
    public static final int C_MSTAR_COUPON_LIST = 50121;
    public static final int C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS = 50122;
    public static final int C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS = 50123;
    public static final int C_MSTAR_NEED_HELP = 50124;
    public static final int C_MSTAR_CATLOG_BANNERS = 50125;
    public static final int C_MSTAR_GET_CITY_STATE_FROM_PINCODE = 50126;
    public static final int C_MSTAR_SOCIAL_LOGIN = 50127;
    public static final int C_MSTAR_GET_ALTERNATE_PRODUCT = 50128;
    public static final int C_MSTAR_WELLNESS_PAGE_DETAILS_V2 = 50129;
    public static final int C_MSTAR_PAST_MEDICINES = 50130;
    public static final int C_MSTAR_RETRIEVE_ANY_URL = 50131;
    public static final int C_MSTAR_RATE_ORDERS = 50132;

    public static final int MSTAR_APPLY_WALLET = 51002;
    public static final int MSTAR_UN_APPLY_WALLET = 51003;
    public static final int PAY_TM_DE_LINK = 51004;
    public static final int MSTAR_ORDER_COMPLETE_WITH_COD = 51000;
    public static final int C_MSTAR_COD_ELIGIBLE_CHECK = 50202;
    public static final int MSTAR_GENERATE_ORDER_ID = 51005;
    public static final int C_MSTAR_PAYMENT_LOG = 51006;
    public static final int C_MSTAR_GENERIC_PRODUCTS_DETAILS = 51007;

    public static final int CART_SUBSTITUTION = 90001;
    public static final int CART_SUBSTITUTION_UPDATE_CART = 90002;
    public static final int C_MEDICINE_PAGE_BANNER = 90003;

    //Subscription
    public final static int C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS = 80001;
    public static final int MSTAR_CREATE_SUBSCRIPTION = 80004;
    public static final int MSTAR_SUBSCRIPTION_PAYNOW = 80005;
    public static final int MSTAR_SUBSCRIPTION_CREATE_CART = 80006;


    private static APIServiceManager apiServiceManager;

    public static APIServiceManager getInstance() {
        if (apiServiceManager == null)
            apiServiceManager = new APIServiceManager();
        return apiServiceManager;
    }

    public <T extends AppViewModel> void getConfigurationData(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<ConfigurationResponse> call = apiService.configuration();
        call.enqueue(new Callback<ConfigurationResponse>() {
            @Override
            public void onResponse(@NonNull Call<ConfigurationResponse> call, @NonNull Response<ConfigurationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CONFIG_PATH_URL);
                } else {
                    viewModel.onFailed(MSTAR_CONFIG_PATH_URL, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ConfigurationResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CONFIG_PATH_URL, null);
            }
        });
    }

    public <T extends AppViewModel> void deliveryEstimation(final T viewModel, DeliveryEstimateRequest request, String url) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().dynamicClient().create(RetrofitAPIInterface.class);
        Call<DeliveryEstimateResponse> call = apiService.deliveryDateEstimate(request, url);
        call.enqueue(new Callback<DeliveryEstimateResponse>() {
            @Override
            public void onResponse(@NonNull Call<DeliveryEstimateResponse> call, @NonNull Response<DeliveryEstimateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), DELIVERY_DATE_ESTIMATE);
                } else {
                    viewModel.onFailed(DELIVERY_DATE_ESTIMATE, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeliveryEstimateResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(DELIVERY_DATE_ESTIMATE, null);
            }
        });
    }


    public <T extends AppViewModel> void checkColdStorage(final T viewModel, String pinCode, String url) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(url).create(RetrofitAPIInterface.class);
        Call<ColdStorageResponse> call = apiService.checkColdStorage(pinCode);
        call.enqueue(new Callback<ColdStorageResponse>() {
            @Override
            public void onResponse(@NonNull Call<ColdStorageResponse> call, @NonNull Response<ColdStorageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), CHECK_COLD_STORAGE);
                } else {
                    viewModel.onFailed(CHECK_COLD_STORAGE, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ColdStorageResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(CHECK_COLD_STORAGE, null);
            }
        });
    }

    public <T extends AppViewModel> void refillFlow2Subscription(final T viewModel, String authToken, Flow2SubscriptionRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL), authToken).create(RetrofitAPIInterface.class);
        Call<M3Flow2SubscriptionResponse> call = apiService.refillSubscription(request);
        call.enqueue(new Callback<M3Flow2SubscriptionResponse>() {
            @Override
            public void onResponse(@NonNull Call<M3Flow2SubscriptionResponse> call, @NonNull Response<M3Flow2SubscriptionResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), M3_FLOW2_SUBSCRIPTION);
                } else {
                    viewModel.onFailed(M3_FLOW2_SUBSCRIPTION, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<M3Flow2SubscriptionResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(M3_FLOW2_SUBSCRIPTION, null);
            }
        });
    }

    public <T extends AppViewModel> void subscriptionLog(final T viewModel, M3SubscriptionLogRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL)).create(RetrofitAPIInterface.class);
        Call<Object> call = apiService.subscriptionLog(request);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), M3_SUBSCRIPTION_LOG);
                } else {
                    viewModel.onFailed(M3_SUBSCRIPTION_LOG, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                viewModel.onFailed(M3_SUBSCRIPTION_LOG, null);
            }
        });
    }

    public <T extends AppViewModel> void removeVoucher(final T viewModel, ApplyVoucherRequest request, String authToken) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL), authToken).create(RetrofitAPIInterface.class);
        Call<ApplyVoucherResponse[]> call = apiService.removeVoucher(request);
        call.enqueue(new Callback<ApplyVoucherResponse[]>() {
            @Override
            public void onResponse(@NonNull Call<ApplyVoucherResponse[]> call, @NonNull Response<ApplyVoucherResponse[]> response) {
                if (response.isSuccessful() && response.body() != null) {
                    for (ApplyVoucherResponse voucherResponse : response.body())
                        viewModel.onSyncData(new Gson().toJson(voucherResponse), REMOVE_VOUCHER);
                } else {
                    viewModel.onFailed(REMOVE_VOUCHER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApplyVoucherResponse[]> call, @NonNull Throwable t) {
                viewModel.onFailed(REMOVE_VOUCHER, null);
            }
        });
    }

    public <T extends AppViewModel> void checkPrimeUser(final T viewModel, Map<String, String> header) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL)).create(RetrofitAPIInterface.class);
        Call<CheckPrimeUserResponse> call = apiService.checkPrimeUser(header);
        call.enqueue(new Callback<CheckPrimeUserResponse>() {
            @Override
            public void onResponse(@NonNull Call<CheckPrimeUserResponse> call, @NonNull Response<CheckPrimeUserResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), CHECK_PRIME_USER);
                } else {
                    viewModel.onFailed(CHECK_PRIME_USER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckPrimeUserResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(CHECK_PRIME_USER, null);
            }
        });
    }

    public <T extends AppViewModel> void fetchCartSubstitutionDetails(final T viewModel, List<CartSubstitutionRequest> request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CART_SUBSTITUTION_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<CartSubstitutionResponse> call = apiService.cartSubstitution(request);
        call.enqueue(new Callback<CartSubstitutionResponse>() {
            @Override
            public void onResponse(@NonNull Call<CartSubstitutionResponse> call, @NonNull Response<CartSubstitutionResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), CART_SUBSTITUTION);
                } else {
                    viewModel.onFailed(CART_SUBSTITUTION, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CartSubstitutionResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(CART_SUBSTITUTION, null);
            }
        });
    }

    public <T extends AppViewModel> void updateCart(final T viewModel, String authToken, CartSubstitutionUpdateCartRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL), authToken).create(RetrofitAPIInterface.class);
        Call<CartSubstitutionUpdateCartResponse> call = apiService.cartSubstitutionUpdateCart(request);
        call.enqueue(new Callback<CartSubstitutionUpdateCartResponse>() {
            @Override
            public void onResponse(@NonNull Call<CartSubstitutionUpdateCartResponse> call, @NonNull Response<CartSubstitutionUpdateCartResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), CART_SUBSTITUTION_UPDATE_CART);
                } else {
                    viewModel.onFailed(CART_SUBSTITUTION_UPDATE_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CartSubstitutionUpdateCartResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(CART_SUBSTITUTION_UPDATE_CART, null);
            }
        });
    }


    //MORNING STAR
    public void getConfigURLPaths(final BasePreference basePreference) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getConfigURL();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    basePreference.setConfigURLPath(new Gson().toJson(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
            }
        });
    }

    public <T extends AppViewModel> void mStarCreateCart(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarCreateCart(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CREATE_CART);
                } else {
                    viewModel.onFailed(MSTAR_CREATE_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CREATE_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarAddProductToCart(final T viewModel, Map<String, String> mStarBasicHeaderMap, int productCode, int productQty, Integer cartId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarAddProductToCart(mStarBasicHeaderMap, productCode, productQty, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_ADD_PRODUCT_TO_CART);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_ADD_PRODUCT_TO_CART);
                    } else {
                        viewModel.onFailed(MSTAR_ADD_PRODUCT_TO_CART, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_ADD_PRODUCT_TO_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarRemoveProductFromCart(final T viewModel, Map<String, String> mStarBasicHeaderMap, int productCode, int productQty, Integer cart_id) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarRemoveProductFromCart(mStarBasicHeaderMap, productCode, productQty, cart_id);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_REMOVE_PRODUCT_FROM_CART);
                } else {
                    viewModel.onFailed(MSTAR_REMOVE_PRODUCT_FROM_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_REMOVE_PRODUCT_FROM_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetCartDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cart_id, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarGetCartDetails(mStarBasicHeaderMap, cart_id);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && AppConstant.CART_NOT_FOUND.equalsIgnoreCase(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), transactionId);
                    } else {
                        viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetAlternateProductForCart(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cart_id, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<CartSubstitutionBasicResponseTemplateModel> call = apiService.mStarGetAlternateProductForCart(mStarBasicHeaderMap, cart_id);
        call.enqueue(new Callback<CartSubstitutionBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<CartSubstitutionBasicResponseTemplateModel> call, @NonNull Response<CartSubstitutionBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CartSubstitutionBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarSwitchAlternateProduct(final T viewModel, Map<String, String> mStarBasicHeaderMap, List<MstarSwitchAlternateProductRequest> request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarSwitchAlternateProduct(mStarBasicHeaderMap, request);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_SWITCH_ALTERNATE_PRODUCT);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && AppConstant.CART_NOT_FOUND.equalsIgnoreCase(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_SWITCH_ALTERNATE_PRODUCT);
                    } else {
                        viewModel.onFailed(MSTAR_SWITCH_ALTERNATE_PRODUCT, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_SWITCH_ALTERNATE_PRODUCT, null);
            }
        });
    }

    public <T extends AppViewModel> void setMstarRevertOriginalCart(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.setMstarRevertOriginalCart(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_REVERT_ORIGINAL_CART);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && AppConstant.CART_NOT_FOUND.equalsIgnoreCase(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_REVERT_ORIGINAL_CART);
                    } else {
                        viewModel.onFailed(MSTAR_REVERT_ORIGINAL_CART, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_REVERT_ORIGINAL_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarInitiateCheckOut(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cart_id, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarInititateCheckout(mStarBasicHeaderMap, cart_id);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), transactionId);
                    } else {
                        viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarApplyPromoCode(final T viewModel, Map<String, String> mStarBasicHeaderMap, String promoCode, Integer cart_id) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarApplyPromoCode(mStarBasicHeaderMap, promoCode, cart_id);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_APPLY_PROMO_CODE);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_APPLY_PROMO_CODE);
                    } else {
                        viewModel.onFailed(MSTAR_APPLY_PROMO_CODE, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_APPLY_PROMO_CODE, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarUnApplyPromoCode(final T viewModel, Map<String, String> mStarBasicHeaderMap, String promoCode, Integer cart_id) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarUnApplyPromoCode(mStarBasicHeaderMap, promoCode, cart_id);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_UN_APPLY_PROMO_CODE);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_UN_APPLY_PROMO_CODE);
                    } else {
                        viewModel.onFailed(MSTAR_UN_APPLY_PROMO_CODE, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_UN_APPLY_PROMO_CODE, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarContinueShopping(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cartId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarContinueShopping(mStarBasicHeaderMap, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CONTINUE_SHOPPING);
                } else {
                    viewModel.onFailed(MSTAR_CONTINUE_SHOPPING, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CONTINUE_SHOPPING, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetAllAddress(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarGetAllAddress(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_ALL_ADDRESS);
                } else {
                    viewModel.onFailed(MSTAR_GET_ALL_ADDRESS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GET_ALL_ADDRESS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarDeleteAddress(final T viewModel, Map<String, String> mStarBasicHeaderMap, int deleteAddressId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarDeleteAddress(mStarBasicHeaderMap, String.valueOf(deleteAddressId));
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_DELETE_ADDRESS);
                } else {
                    MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mStarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mStarBasicResponseTemplateModel != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBasicResponseTemplateModel), MSTAR_DELETE_ADDRESS);
                    } else {
                        viewModel.onFailed(MSTAR_DELETE_ADDRESS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_DELETE_ADDRESS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarSetShippingAndBillingAddress(final T viewModel, Map<String, String> mStarBasicHeaderMap, int addressId, boolean isShippingAddress, int m2CartId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = isShippingAddress ? apiService.mStarSetM2ShippingAddress(mStarBasicHeaderMap, addressId, m2CartId != 0 ? m2CartId : null) :
                apiService.mStarSetM2BillingAddress(mStarBasicHeaderMap, addressId, m2CartId != 0 ? m2CartId : null);
        final int transactionId = isShippingAddress ? MSTAR_SET_M2_SHIPPING_ADDRESS : MSTAR_SET_M2_BILLING_ADDRESS;
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        MStarBase2Response mStarBase2Response = new Gson().fromJson(response.errorBody().charStream(), MStarBase2Response.class);
                        mStarBasicResponseTemplateModel = new MStarBasicResponseTemplateModel();
                        mStarBasicResponseTemplateModel.setReason(mStarBase2Response.getReason());
                    }
                    if (mStarBasicResponseTemplateModel != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBasicResponseTemplateModel), transactionId);
                    } else {
                        viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarSetPreferredShippingAndBillingAddress(final T viewModel, Map<String, String> mStarBasicHeaderMap, int addressId, boolean isShippingAddress) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = isShippingAddress ? apiService.mStarSetPreferredShippingAddress(mStarBasicHeaderMap, addressId) : apiService.mStarSetPreferredBillingAddress(mStarBasicHeaderMap, addressId);
        final int transactionId = isShippingAddress ? MSTAR_SET_PREFERRED_SHIPPING_ADDRESS : MSTAR_SET_PREFERRED_BILLING_ADDRESS;
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        MStarBase2Response mStarBase2Response = new Gson().fromJson(response.errorBody().charStream(), MStarBase2Response.class);
                        mStarBasicResponseTemplateModel = new MStarBasicResponseTemplateModel();
                        mStarBasicResponseTemplateModel.setReason(mStarBase2Response.getReason());
                    }
                    if (mStarBasicResponseTemplateModel != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBasicResponseTemplateModel), transactionId);
                    } else {
                        viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarCustomerDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarCustomerDetails(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CUSTOMER_DETAILS);
                } else {
                    viewModel.onFailed(MSTAR_CUSTOMER_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CUSTOMER_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarSingleAddressDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, int addressId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MstarAddressResponse> call = apiService.mStarSingleAddressDetails(mStarBasicHeaderMap, String.valueOf(addressId));
        call.enqueue(new Callback<MstarAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarAddressResponse> call, @NonNull Response<MstarAddressResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_ADDRESS_BY_ID);
                } else {
                    viewModel.onFailed(MSTAR_GET_ADDRESS_BY_ID, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarAddressResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GET_ADDRESS_BY_ID, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarAddAddress(final T viewModel, Map<String, String> mStarBasicHeaderMap, MStarAddAddressModelRequest addressModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarNewAddress(mStarBasicHeaderMap, addressModel);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_ADD_NEW_ADDRESS);
                } else {
                    MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mStarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mStarBasicResponseTemplateModel != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBasicResponseTemplateModel), MSTAR_ADD_NEW_ADDRESS);
                    } else {
                        viewModel.onFailed(MSTAR_ADD_NEW_ADDRESS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_ADD_NEW_ADDRESS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarUpdateAddress(final T viewModel, Map<String, String> mStarBasicHeaderMap, MstarEditAddressRequest addressModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarUpdateAddress(mStarBasicHeaderMap, addressModel);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_UPDATE_ADDRESS);
                } else {
                    MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mStarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mStarBasicResponseTemplateModel != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBasicResponseTemplateModel), MSTAR_UPDATE_ADDRESS);
                    } else {
                        viewModel.onFailed(MSTAR_UPDATE_ADDRESS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_UPDATE_ADDRESS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarProductDetails(final T viewModel, int productId, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarProductDetails(productId, AppConstant.ALL);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarProductDetailsForIdList(final T viewModel, String productList, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarProductDetailsForIdList(productList);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarCategoryDetails(final T viewModel, MstarCategoryDetailsRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarCategoryDetails(request.getCategoryId(), request.getReturnFacets(), request.getReturnProduct(), request.getPageNo(), request.getPageSize(), request.getProductFieldsetName(), request.getSubCategoryDepth(), request.getProductsSortOrder());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(Call<MStarBasicResponseTemplateModel> call, Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CATEGORY_DETAILS);
                } else {
                    viewModel.onFailed(MSTAR_CATEGORY_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<MStarBasicResponseTemplateModel> call, Throwable t) {
                viewModel.onFailed(MSTAR_CATEGORY_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarManufacturerDetails(final T viewModel, MstarManufacturerDetailsRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarManufacturerDetails(request.getManufacturerId(), request.getReturnFacets(), request.getReturnProducts(), request.getPageNo(), request.getPageSize(), request.getProductFieldsetName(), request.getProductsSortOrder(), request.getProductsSelector());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(Call<MStarBasicResponseTemplateModel> call, Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_MANUFACTURER_DETAILS);
                } else {
                    viewModel.onFailed(MSTAR_MANUFACTURER_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<MStarBasicResponseTemplateModel> call, Throwable t) {
                viewModel.onFailed(MSTAR_MANUFACTURER_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void initiateSentOtp(final T viewModel, String phoneNumber) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarSentOtp(phoneNumber);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_SENT_OTP);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_SENT_OTP);
                    } else {
                        viewModel.onFailed(MSTAR_SENT_OTP, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_SENT_OTP, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarVerifyOtp(final T viewModel, String phoneNumber, String randomKey, String otp) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarVerifyOtp(phoneNumber, randomKey, otp, AppConstant.MSTAR_CHANNEL_NAME);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_VERIFY_OTP);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_VERIFY_OTP);
                    } else {
                        viewModel.onFailed(MSTAR_VERIFY_OTP, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_VERIFY_OTP, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarUploadPrescription(final T viewModel, Map<String, String> mStarHeaderMap, int cart_id, String newMethodCart2, MultipartBody requestBody) {

        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = null;
        if (cart_id == 0 && newMethodCart2.isEmpty()) {
            // for universal cart -method 1
            call = apiService.mstarUploadPrescription(mStarHeaderMap, newMethodCart2, requestBody);

        } else if (cart_id == 0 && AppConstant.YES.equalsIgnoreCase(newMethodCart2)) {
            //for method 2 where new cart is to be created.
            call = apiService.mstarUploadPrescription(mStarHeaderMap, newMethodCart2, requestBody);

        } else if (cart_id != 0) {
            //for method 2 where method 2's cart is available
            call = apiService.mstarUploadPrescriptionWithCartID(mStarHeaderMap, cart_id, requestBody);
        }
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(Call<MStarBasicResponseTemplateModel> call, Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_UPLOAD_PRESCRIPTION);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_UPLOAD_PRESCRIPTION);
                    } else {
                        viewModel.onFailed(MSTAR_UPLOAD_PRESCRIPTION, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<MStarBasicResponseTemplateModel> call, Throwable t) {
                viewModel.onFailed(MSTAR_UPLOAD_PRESCRIPTION, null);
            }
        });
    }

    public <T extends AppViewModel> void mStartDetachPrescription(final T viewModel, Map<String, String> mStarHeaderMap, Map<String, String> detachPrescriptionMap, Integer cart_id) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStartDetachPrescription(mStarHeaderMap, cart_id, detachPrescriptionMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(Call<MStarBasicResponseTemplateModel> call, Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_DETACH_PRESCRIPTION);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_DETACH_PRESCRIPTION);
                    } else {
                        viewModel.onFailed(MSTAR_DETACH_PRESCRIPTION, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<MStarBasicResponseTemplateModel> call, Throwable t) {
                viewModel.onFailed(MSTAR_DETACH_PRESCRIPTION, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetMethod2Carts(final T viewModel, Map<String, String> mStarHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MstarBasicResponseTemplateModeForM2> call = apiService.mStarGetMethod2Carts(mStarHeaderMap);
        call.enqueue(new Callback<MstarBasicResponseTemplateModeForM2>() {
            @Override
            public void onResponse(Call<MstarBasicResponseTemplateModeForM2> call, Response<MstarBasicResponseTemplateModeForM2> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_METHOD_2_CARTS);
                } else {
                    MstarBasicResponseTemplateModeForM2 mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MstarBasicResponseTemplateModeForM2.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_GET_METHOD_2_CARTS);
                    } else {
                        viewModel.onFailed(MSTAR_GET_METHOD_2_CARTS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<MstarBasicResponseTemplateModeForM2> call, Throwable t) {
                viewModel.onFailed(MSTAR_GET_METHOD_2_CARTS, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarSubmitMethod2(final T viewModel, Map<String, String> mStarHeaderMap, int m2CartId, boolean isDoctorConsult) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MstarSubmitMethod2Response> call = apiService.mStarSubmitMethod2(mStarHeaderMap, m2CartId, isDoctorConsult ? null : AppConstant.YES);
        call.enqueue(new Callback<MstarSubmitMethod2Response>() {
            @Override
            public void onResponse(Call<MstarSubmitMethod2Response> call, Response<MstarSubmitMethod2Response> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_SUBMIT_METHOD_2);
                } else {
                    MstarSubmitMethod2Response mstarSubmitMethod2Response = null;
                    if (response.errorBody() != null) {
                        mstarSubmitMethod2Response = new Gson().fromJson(response.errorBody().charStream(), MstarSubmitMethod2Response.class);
                    }
                    if (mstarSubmitMethod2Response != null && mstarSubmitMethod2Response.getReason() != null) {
                        viewModel.onSyncData(new Gson().toJson(mstarSubmitMethod2Response), MSTAR_SUBMIT_METHOD_2);
                    } else {
                        viewModel.onFailed(MSTAR_SUBMIT_METHOD_2, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(Call<MstarSubmitMethod2Response> call, Throwable t) {
                viewModel.onFailed(MSTAR_SUBMIT_METHOD_2, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarPrimeProduct(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MstarPrimeProductResult> call = apiService.mstarGetPrimeProducts();
        call.enqueue(new Callback<MstarPrimeProductResult>() {
            @Override
            public void onResponse(Call<MstarPrimeProductResult> call, Response<MstarPrimeProductResult> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_PRIME_PRODUCT);
                } else {
                    viewModel.onFailed(MSTAR_PRIME_PRODUCT, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<MstarPrimeProductResult> call, Throwable t) {
                viewModel.onFailed(MSTAR_PRIME_PRODUCT, null);
            }
        });

    }

    public <T extends AppViewModel> void mStarApplyVoucher(final T viewModel, Map<String, String> mStarBasicHeaderMap, String voucher, Integer cartId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarApplyVoucher(mStarBasicHeaderMap, voucher, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_APPLY_VOUCHER);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_APPLY_VOUCHER);
                    } else {
                        viewModel.onFailed(MSTAR_APPLY_VOUCHER, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_APPLY_VOUCHER, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarUnapplyVoucher(final T viewModel, Map<String, String> mStarBasicHeaderMap, String voucher, Integer cartId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarUnapplyVoucher(mStarBasicHeaderMap, voucher, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_UNAPPLY_VOUCHER);
                } else {
                    viewModel.onFailed(MSTAR_UNAPPLY_VOUCHER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_UNAPPLY_VOUCHER, null);
            }
        });
    }

    public <T extends AppViewModel> void getMstarHomeBanner(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarHomeBanner();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_HOME_BANNER);
                } else {
                    viewModel.onFailed(C_MSTAR_HOME_BANNER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_HOME_BANNER, null);
            }
        });
    }

    public <T extends AppViewModel> void getMstarWalletDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetWalletDetails(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_WALLET_DETAILS);
                } else {
                    viewModel.onFailed(C_MSTAR_WALLET_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_WALLET_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void MstarReferAndEarn(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarReferAndEarn(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_REFER_AND_EARN);
                } else {
                    viewModel.onFailed(C_MSTAR_REFER_AND_EARN, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_REFER_AND_EARN, null);
            }
        });
    }

    public <T extends AppViewModel> void MstarGetOrderHeader(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetOrderHeader(mStarBasicHeaderMap, request);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_ORDERS_HEADER);
                } else {
                    viewModel.onFailed(C_MSTAR_ORDERS_HEADER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_ORDERS_HEADER, null);
            }
        });
    }

    public <T extends AppViewModel> void MstarGetOrderDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetOrderDetails(mStarBasicHeaderMap, request);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_MY_ORDER_DETAILS);
                } else {
                    viewModel.onFailed(C_MSTAR_MY_ORDER_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_MY_ORDER_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void MstarGetBrandProductList(final T viewModel, MstarBrandProductListRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarBrandProductList(request.getBrandID(), request.getReturnFacets(), request.getReturnProduct(), request.getPageNo(), request.getPageSize());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_BRAND_PRODUCT_LIST);
                } else {
                    viewModel.onFailed(MSTAR_BRAND_PRODUCT_LIST, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_BRAND_PRODUCT_LIST, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarClearCart(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarClearCart(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CLEAR_UNIVERSAL_CART);
                } else {
                    viewModel.onFailed(MSTAR_CLEAR_UNIVERSAL_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CLEAR_UNIVERSAL_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void MstarGetOrderTrackDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody request) {
        Call<MStarBasicResponseTemplateModel> call;
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        if (SubscriptionHelper.getInstance().isSubscriptionFlag())
            call = apiService.mstarSubscriptionTrackOrderDetails(mStarBasicHeaderMap, request);
        else
            call = apiService.mstarTrackOrderDetails(mStarBasicHeaderMap, request);

        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_TRACK_ORDER);
                } else {
                    viewModel.onFailed(C_MSTAR_TRACK_ORDER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_TRACK_ORDER, null);
            }
        });
    }

    public <T extends AppViewModel> void MstarCancelOrder(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<CancelOrderResponseModel> call = apiService.mstarCancelOrder(mStarBasicHeaderMap, request);
        call.enqueue(new Callback<CancelOrderResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<CancelOrderResponseModel> call, @NonNull Response<CancelOrderResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_CANCEL_ORDER);
                } else {
                    viewModel.onFailed(C_MSTAR_CANCEL_ORDER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CancelOrderResponseModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_CANCEL_ORDER, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarLogIn(final T viewModel, String userId, String password, String mergeSession) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarLogIn(userId, password, AppConstant.MSTAR_CHANNEL_NAME, mergeSession);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_LOGIN);
                } else {
                    MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = null;
                    if (response.errorBody() != null) {
                        mstarBasicResponseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                    }
                    if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getReason() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getReason().getReason_code())) {
                        viewModel.onSyncData(new Gson().toJson(mstarBasicResponseTemplateModel), MSTAR_LOGIN);
                    } else {
                        viewModel.onFailed(MSTAR_LOGIN, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_LOGIN, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetDigitizedRx(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody requestBody) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<DigitizedRxResponse> call = apiService.mStarGetDigitizedRx(mStarBasicHeaderMap, requestBody);
        call.enqueue(new Callback<DigitizedRxResponse>() {
            @Override
            public void onResponse(@NonNull Call<DigitizedRxResponse> call, @NonNull Response<DigitizedRxResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_DIGITIZED_RX);
                } else {
                    DigitizedRxResponse digitizedRxResponse = null;
                    if (response.errorBody() != null) {
                        digitizedRxResponse = new Gson().fromJson(response.errorBody().charStream(), DigitizedRxResponse.class);
                    }
                    if (digitizedRxResponse != null && digitizedRxResponse.getResult() != null) {
                        viewModel.onSyncData(new Gson().toJson(digitizedRxResponse), C_MSTAR_DIGITIZED_RX);
                    } else {
                        viewModel.onFailed(C_MSTAR_DIGITIZED_RX, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DigitizedRxResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_DIGITIZED_RX, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetPastPrescription(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody requestBody) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<GetPastPrescriptionResponse> call = apiService.getMstarPastPrescription(mStarBasicHeaderMap, requestBody);
        call.enqueue(new Callback<GetPastPrescriptionResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetPastPrescriptionResponse> call, @NonNull Response<GetPastPrescriptionResponse> response) {
                if (response.isSuccessful()) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAST_PRESCRIPTION);
                } else {
                    viewModel.onFailed(C_MSTAR_PAST_PRESCRIPTION, errorHandling(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetPastPrescriptionResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PAST_PRESCRIPTION, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetAllOffers(final T viewModel, Integer id) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarAllOffers(id);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_ALL_OFFERS);
                } else {
                    viewModel.onFailed(C_MSTAR_ALL_OFFERS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_ALL_OFFERS, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetGatewayOffers(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGatewayOffers();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_GATEWAY_OFFERS);
                } else {
                    viewModel.onFailed(C_MSTAR_GATEWAY_OFFERS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_GATEWAY_OFFERS, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetM2OrderDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, MultipartBody request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetM2OrderDetails(mStarBasicHeaderMap, request);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_METHOD2_ORDER_DETAIL);
                } else {
                    viewModel.onFailed(C_MSTAR_METHOD2_ORDER_DETAIL, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_METHOD2_ORDER_DETAIL, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarInitiateChangePassword(final T viewModel, String userid) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiInterface.mstarInitialteChangePassword(userid);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_INITIATE_CHANGE_PSSWRD);
                } else {
                    MStarBasicResponseTemplateModel responseTemplateModel = null;
                    if (response.errorBody() != null) {
                        responseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                        if (responseTemplateModel != null && responseTemplateModel.getReason() != null && responseTemplateModel.getReason().getReason_code() != null) {
                            viewModel.onSyncData(new Gson().toJson(responseTemplateModel), MSTAR_INITIATE_CHANGE_PSSWRD);
                        }
                    } else {
                        viewModel.onFailed(MSTAR_INITIATE_CHANGE_PSSWRD, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_INITIATE_CHANGE_PSSWRD, null);

            }
        });
    }

    public <T extends AppViewModel> void mstarResendSameOTP(final T viewModel, String randomKey) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiInterface.mstarResendSameOtp(randomKey);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_RESEND_SAME_OTP);
                } else {
                    MStarBasicResponseTemplateModel responseTemplateModel = null;
                    if (response.errorBody() != null) {
                        responseTemplateModel = new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class);
                        if (responseTemplateModel != null && responseTemplateModel.getReason() != null && responseTemplateModel.getReason().getReason_code() != null) {
                            viewModel.onSyncData(new Gson().toJson(responseTemplateModel), MSTAR_RESEND_SAME_OTP);
                        }
                    } else {
                        viewModel.onFailed(MSTAR_RESEND_SAME_OTP, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_RESEND_SAME_OTP, null);

            }
        });
    }

    public <T extends AppViewModel> void mstarCompleteChangePassword(final T viewModel, String mobileNo, String randomKey, String otp, String newPassword) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MstarChangePasswordResponse> call = apiInterface.mstarCompleteChangePassword(mobileNo, randomKey, otp, newPassword);
        call.enqueue(new Callback<MstarChangePasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarChangePasswordResponse> call, @NonNull Response<MstarChangePasswordResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_COMPLETE_CHANGE_PSSWRD);
                } else {
                    MstarChangePasswordResponse mstarChangePasswordResponse = null;
                    if (response.errorBody() != null) {
                        mstarChangePasswordResponse = new Gson().fromJson(response.errorBody().charStream(), MstarChangePasswordResponse.class);
                        if (mstarChangePasswordResponse != null && mstarChangePasswordResponse.getReason() != null && mstarChangePasswordResponse.getReason().getReason_code() != null) {
                            viewModel.onSyncData(new Gson().toJson(mstarChangePasswordResponse), MSTAR_COMPLETE_CHANGE_PSSWRD);
                        }
                    } else {
                        viewModel.onFailed(MSTAR_COMPLETE_CHANGE_PSSWRD, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarChangePasswordResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_COMPLETE_CHANGE_PSSWRD, null);

            }
        });
    }

    public <T extends AppViewModel> void mstarGetHealthArticles(final T viewModel) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiInterface.getHealthArticle();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_HEALTH_ARTICLES);
                } else {
                    viewModel.onFailed(C_MSTAR_HEALTH_ARTICLES, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_HEALTH_ARTICLES, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetWellnessPageDetails(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetWellnessPageDetailsVersion2();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_WELLNESS_PAGE_DETAILS_V2);
                } else {
                    viewModel.onFailed(C_MSTAR_WELLNESS_PAGE_DETAILS_V2, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_WELLNESS_PAGE_DETAILS_V2, null);
            }
        });


    }

    public <T extends AppViewModel> void highRangeTicketPopUp(final T viewModel, MultipartBody body) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MstarRequestPopupResponse> call = apiService.highRangeProduct(body);
        call.enqueue(new Callback<MstarRequestPopupResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarRequestPopupResponse> call, @NonNull Response<MstarRequestPopupResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_HIGH_RANGE_TICKET_POPUP);
                } else {
                    viewModel.onFailed(C_MSTAR_HIGH_RANGE_TICKET_POPUP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarRequestPopupResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_HIGH_RANGE_TICKET_POPUP, null);
            }
        });
    }

    public <T extends AppViewModel> void requestNewProduct(final T viewModel, MultipartBody body) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MstarRequestPopupResponse> call = apiService.requestNewProduct(body);
        call.enqueue(new Callback<MstarRequestPopupResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarRequestPopupResponse> call, @NonNull Response<MstarRequestPopupResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_REQUEST_NEW_PRODUCT);
                } else {
                    viewModel.onFailed(C_MSTAR_REQUEST_NEW_PRODUCT, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarRequestPopupResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_REQUEST_NEW_PRODUCT, null);
            }
        });
    }

    public <T extends AppViewModel> void notifyMe(final T viewModel, MultipartBody body) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MstarRequestPopupResponse> call = apiService.notifyMe(body);
        call.enqueue(new Callback<MstarRequestPopupResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarRequestPopupResponse> call, @NonNull Response<MstarRequestPopupResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_NOTIFY_ME);
                } else {
                    viewModel.onFailed(C_MSTAR_NOTIFY_ME, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarRequestPopupResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_NOTIFY_ME, null);
            }
        });
    }

    public <T extends AppViewModel> void getMstarMyPrescriptions(final T viewModel, Map<String, String> mStarBasicHeaderMap, int pageIndex, int pageSize) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarMyPrescription(mStarBasicHeaderMap, String.valueOf(pageIndex), String.valueOf(pageSize));
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_MY_PRESCRIPTION);
                } else {
                    viewModel.onFailed(C_MSTAR_MY_PRESCRIPTION, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_MY_PRESCRIPTION, null);
            }
        });
    }

    public <T extends AppViewModel> void getPromotionBanner(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarPromotionBanner();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PROMOTION_BANNER);
                } else {
                    viewModel.onFailed(C_MSTAR_PROMOTION_BANNER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PROMOTION_BANNER, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarCreateGuestSession(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.createGuestSession(AppConstant.MSTAR_CHANNEL_NAME);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CREATE_GUEST_SESSION);
                } else {
                    if (response.errorBody() != null) {
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class)), MSTAR_CREATE_GUEST_SESSION);
                    } else {
                        viewModel.onFailed(MSTAR_CREATE_GUEST_SESSION, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CREATE_GUEST_SESSION, null);
            }
        });
    }

    public <T extends AppViewModel> void getHealthConcernBanner(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarHealthConcernBanner();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_HEALTHCONCERN_BANNER);
                } else {
                    viewModel.onFailed(C_MSTAR_HEALTHCONCERN_BANNER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_HEALTHCONCERN_BANNER, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetUserStatus(final T viewModel, String userId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetUserStatus(userId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_USER_STATUS);
                } else {
                    if (response.errorBody() != null && !TextUtils.isEmpty(response.errorBody().source().toString())) {
                        Type collectionType = new TypeToken<MStarBasicResponseTemplateModel>() {
                        }.getType();

                        MStarBasicResponseTemplateModel enums = new Gson().fromJson(response.errorBody().charStream(), collectionType);
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(new Gson().toJson(enums), MStarBasicResponseTemplateModel.class)), MSTAR_GET_USER_STATUS);
                    } else {
                        viewModel.onFailed(MSTAR_GET_USER_STATUS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GET_USER_STATUS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarCustomerRegistration(final T viewModel, MStarRegistrationRequest registrationRequest, String mergeSession, String googleAdvId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarCustomerRegistrationResponse> call = apiService.mstarCustomerRegistration(googleAdvId, registrationRequest, AppConstant.MSTAR_CHANNEL_NAME, mergeSession);
        call.enqueue(new Callback<MStarCustomerRegistrationResponse>() {
            @Override
            public void onResponse(@NonNull Call<MStarCustomerRegistrationResponse> call, @NonNull Response<MStarCustomerRegistrationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CUSTOMER_REGISTRATION);
                } else {
                    if (response.errorBody() != null) {
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(response.errorBody().charStream(), MStarCustomerRegistrationResponse.class)), MSTAR_CUSTOMER_REGISTRATION);
                    } else {
                        viewModel.onFailed(MSTAR_CUSTOMER_REGISTRATION, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarCustomerRegistrationResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CUSTOMER_REGISTRATION, null);
            }
        });
    }

    public <T extends AppViewModel> void getMStarPaymentWalletBalance(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cartId, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getMStarPaymentWalletBalance(mStarBasicHeaderMap, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarApplyAndUnApplyWalletAndSuperCash(final T viewModel, Map<String, String> mStarBasicHeaderMap, String walletApplied, String nmsSuperCashAppied, Integer cartId, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getMStarApplyWallet(mStarBasicHeaderMap, walletApplied, nmsSuperCashAppied, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    if (response.errorBody() != null) {
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(response.errorBody().charStream(), MStarCustomerRegistrationResponse.class)), transactionId);
                    } else {
                        viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarUnApplyWallet(final T viewModel, Map<String, String> mStarBasicHeaderMap, String walletAmount, String nmsSuperCashAmount, Integer cartId, final int transactionId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarUnApplyWallet(mStarBasicHeaderMap, walletAmount, nmsSuperCashAmount, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), transactionId);
                } else {
                    viewModel.onFailed(transactionId, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(transactionId, null);
            }
        });
    }

    public <T extends AppViewModel> void couponList(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.couponList();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_COUPON_LIST);
                } else {
                    viewModel.onFailed(C_MSTAR_COUPON_LIST, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_COUPON_LIST, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarUpdateCustomer(final T viewModel, Map<String, String> mStarBasicHeaderMap, MstarUpdateCustomerRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarUpdateCustomer(mStarBasicHeaderMap, request);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_UPDATE_CUSTOMER);
                } else {
                    if (response.errorBody() != null && !TextUtils.isEmpty(response.errorBody().source().toString())) {
                        Type collectionType = new TypeToken<MStarBasicResponseTemplateModel>() {
                        }.getType();

                        MStarBasicResponseTemplateModel enums = new Gson().fromJson(response.errorBody().charStream(), collectionType);
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(new Gson().toJson(enums), MStarBasicResponseTemplateModel.class)), MSTAR_UPDATE_CUSTOMER);
                    } else {
                        viewModel.onFailed(MSTAR_UPDATE_CUSTOMER, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_UPDATE_CUSTOMER, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarAddMutlipleProducts(final T viewModel, Map<String, String> mStarBasicHeaderMap, Map<String, Integer> productList) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarAddMultipleProductsResponse> call = apiService.mstarAddMultipleProducts(mStarBasicHeaderMap, productList);
        call.enqueue(new Callback<MStarAddMultipleProductsResponse>() {
            @Override
            public void onResponse(@NonNull Call<MStarAddMultipleProductsResponse> call, @NonNull Response<MStarAddMultipleProductsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_ADD_MULTIPLE_PRODUCTS);
                } else {
                    viewModel.onFailed(MSTAR_ADD_MULTIPLE_PRODUCTS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarAddMultipleProductsResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_ADD_MULTIPLE_PRODUCTS, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarGetLinkedPayment(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarLinkedPaymentResponse> call = apiInterface.mStarGetLinkedPayment(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarLinkedPaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<MStarLinkedPaymentResponse> call, @NonNull Response<MStarLinkedPaymentResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_GET_LINKED_PAYMENT);
                } else {
                    viewModel.onFailed(C_MSTAR_GET_LINKED_PAYMENT, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarLinkedPaymentResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_GET_LINKED_PAYMENT, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetCartSuggestedProducts(final T viewModel, String url) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<RecommendedProductResponse> call = apiInterface.getCartSuggestedProductList(url);
        call.enqueue(new Callback<RecommendedProductResponse>() {
            @Override
            public void onResponse(@NonNull Call<RecommendedProductResponse> call, @NonNull Response<RecommendedProductResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS);
                } else {
                    viewModel.onFailed(C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<RecommendedProductResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetPeopleAlosViewedProducts(final T viewModel, String url) {
        RetrofitAPIInterface apiInterface = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<RecommendedProductResponse> call = apiInterface.getCartSuggestedProductList(url);
        call.enqueue(new Callback<RecommendedProductResponse>() {
            @Override
            public void onResponse(@NonNull Call<RecommendedProductResponse> call, @NonNull Response<RecommendedProductResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS);
                } else {
                    viewModel.onFailed(C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<RecommendedProductResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetReorderCart(final T viewModel, Map<String, String> mStarBasicHeaderMap, String customerId, String orderId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getMstarReorderCart(mStarBasicHeaderMap, customerId, orderId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_REORDER_CART);
                } else {
                    viewModel.onFailed(MSTAR_GET_REORDER_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GET_REORDER_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetFilledM2Cart(final T viewModel, Map<String, String> mStarBasicHeaderMap, String customerId, String orderId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getFilledM2Cart(mStarBasicHeaderMap, customerId, orderId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_FILLED_M2_CART);
                } else {
                    viewModel.onFailed(MSTAR_GET_FILLED_M2_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GET_FILLED_M2_CART, null);
            }
        });
    }


    public <T extends AppViewModel> void mstarStillNeedHelp(final T viewModel, Map<String, String> mStarBasicHeaderMap, String titleTxt, String subject, String fullname, String description, String phone, String email) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBase2Response> call = apiService.getNeedHelp(mStarBasicHeaderMap, titleTxt, subject, fullname, description, phone, email);
        call.enqueue(new Callback<MStarBase2Response>() {
            @Override
            public void onResponse(@NonNull Call<MStarBase2Response> call, @NonNull Response<MStarBase2Response> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_NEED_HELP);
                } else {
                    MStarBase2Response mStarBase2Response = new Gson().fromJson(response.errorBody().charStream(), MStarBase2Response.class);
                    if (mStarBase2Response != null && mStarBase2Response.getReason() != null && !TextUtils.isEmpty(mStarBase2Response.getReason().getReason_eng())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBase2Response), C_MSTAR_NEED_HELP);
                    }
                    viewModel.onFailed(C_MSTAR_NEED_HELP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBase2Response> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_NEED_HELP, null);
            }
        });
    }


    public <T extends AppViewModel> void createSubscription(final T viewModel, Map<String, String> mStarBasicHeaderMap, String primaryOrderId, String daysBetweenIssue, String fromPage) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getMstarReorderCart(mStarBasicHeaderMap, primaryOrderId, daysBetweenIssue, fromPage);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_CREATE_SUBSCRIPTION);
                } else {
                    MStarBase2Response mStarBase2Response = new Gson().fromJson(response.errorBody().charStream(), MStarBase2Response.class);
                    if (mStarBase2Response != null && mStarBase2Response.getReason() != null && !TextUtils.isEmpty(mStarBase2Response.getReason().getReason_eng())) {
                        viewModel.onSyncData(new Gson().toJson(mStarBase2Response), C_MSTAR_NEED_HELP);
                    }
                    viewModel.onFailed(C_MSTAR_NEED_HELP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_CREATE_SUBSCRIPTION, null);
            }
        });
    }


    public <T extends AppViewModel> void getMstarRetryCart(final T viewModel, Map<String, String> mStarBasicHeaderMap, String customerId, String orderId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getRetryCart(mStarBasicHeaderMap, customerId, orderId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GET_RETRY_CART);
                } else {
                    viewModel.onFailed(MSTAR_GET_RETRY_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GET_RETRY_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void revokePayTmAccess(final T viewModel, String linkToken, Map<String, String> payTmDeLinkHeader) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.PAY_TM_ACCOUNT_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<Object> call = apiService.deLinkPayTm(linkToken, payTmDeLinkHeader.get(AppConstant.KEY_AUTHORIZATION), payTmDeLinkHeader.get(AppConstant.KEY_CONTENT_TYPE), payTmDeLinkHeader.get(AppConstant.KEY_CONTENT_LENGTH), payTmDeLinkHeader.get(AppConstant.KEY_HOST));
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData("", PAY_TM_DE_LINK);
                } else {
                    viewModel.onFailed(PAY_TM_DE_LINK, "");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                viewModel.onFailed(PAY_TM_DE_LINK, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarOrderCompleteWithCOD(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cartId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarCodPayment(mStarBasicHeaderMap, cartId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_ORDER_COMPLETE_WITH_COD);
                } else {
                    viewModel.onFailed(MSTAR_ORDER_COMPLETE_WITH_COD, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_ORDER_COMPLETE_WITH_COD, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarPaymentLog(final T viewModel, Map<String, String> mStarBasicHeaderMap, MstarTransactionLogRequest transactionLog) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.paymentLog(mStarBasicHeaderMap, transactionLog.getOrderId(), transactionLog.getPaymentMethod(), transactionLog.getRequest(), transactionLog.getResponse(), transactionLog.getAppVersion(), transactionLog.getSourceName());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAYMENT_LOG);
                } else {
                    if (response.errorBody() != null) {
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(response.errorBody().charStream(), MStarBasicResponseTemplateModel.class)), C_MSTAR_PAYMENT_LOG);
                    } else {
                        viewModel.onFailed(C_MSTAR_PAYMENT_LOG, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PAYMENT_LOG, null);
            }
        });
    }

    public <T extends AppViewModel> void generateOrderId(final T viewModel, Map<String, String> mStarBasicHeaderMap, Integer cartId, String isDrConsultantNeeded, GenerateOrderIdRequest request, boolean isforceNew, String editScope) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.generateOrderId(mStarBasicHeaderMap, cartId, isDrConsultantNeeded, isforceNew ? AppConstant.YES : AppConstant.NO, request, editScope);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_GENERATE_ORDER_ID);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), MSTAR_GENERATE_ORDER_ID);
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_GENERATE_ORDER_ID, null);
            }
        });
    }

    public <T extends AppViewModel> void subscriptionPayNow(final T viewModel, Map<String, String> mStarBasicHeaderMap, String customerId, String primaryOrderId, String issueId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarSubscriptionPayNow(mStarBasicHeaderMap, customerId, primaryOrderId, issueId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_SUBSCRIPTION_PAYNOW);
                } else {
                    viewModel.onSyncData(errorHandling(response.errorBody()), MSTAR_SUBSCRIPTION_PAYNOW);
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_SUBSCRIPTION_PAYNOW, null);
            }
        });
    }

    public <T extends AppViewModel> void getM3OrderDetails(final T viewModel, Map<String, String> mStarBasicHeaderMap, String issueId, String primaryOrderId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.getM3OrderDetails(issueId, primaryOrderId, mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS);
                } else {
                    viewModel.onFailed(C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void getMstarCatlogBanners(final T viewModel, String type, String categoryId, String manufactureId, String brandId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarCatlogBanneres(type, categoryId, manufactureId, brandId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_CATLOG_BANNERS);
                } else {
                    viewModel.onFailed(C_MSTAR_CATLOG_BANNERS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_CATLOG_BANNERS, null);
            }
        });
    }

    public <T extends AppViewModel> void getMstarPincodeDetails(final T viewModel, String pincode) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<GetCityStateFromPinCodeResponse> call = apiService.mstarGetPinCodeDetials(pincode);
        call.enqueue(new Callback<GetCityStateFromPinCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCityStateFromPinCodeResponse> call, @NonNull Response<GetCityStateFromPinCodeResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_GET_CITY_STATE_FROM_PINCODE);
                } else {
                    if (response.errorBody() != null && !TextUtils.isEmpty(response.errorBody().source().toString())) {
                        Type collectionType = new TypeToken<GetCityStateFromPinCodeResponse>() {
                        }.getType();
                        GetCityStateFromPinCodeResponse enums = new Gson().fromJson(response.errorBody().charStream(), collectionType);
                        GetCityStateFromPinCodeResponse getCityStateFromPinCodeResponse = new Gson().fromJson(new Gson().toJson(enums), GetCityStateFromPinCodeResponse.class);
                        if (getCityStateFromPinCodeResponse != null && !TextUtils.isEmpty(getCityStateFromPinCodeResponse.getReason())) {
                            viewModel.onSyncData(new Gson().toJson(getCityStateFromPinCodeResponse), C_MSTAR_GET_CITY_STATE_FROM_PINCODE);
                        }
                    } else {
                        viewModel.onFailed(C_MSTAR_GET_CITY_STATE_FROM_PINCODE, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCityStateFromPinCodeResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_GET_CITY_STATE_FROM_PINCODE, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarCreateSubscriptionCart(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarSubscriptionCreateCart(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), MSTAR_SUBSCRIPTION_CREATE_CART);
                } else {
                    viewModel.onFailed(MSTAR_SUBSCRIPTION_CREATE_CART, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(MSTAR_SUBSCRIPTION_CREATE_CART, null);
            }
        });
    }

    public <T extends AppViewModel> void checkCODEligibleForCreateNewOrder(final T viewModel, Map<String, String> mStarBasicHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.checkCODEligibleForCreateNewOrder(mStarBasicHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_COD_ELIGIBLE_CHECK);
                } else {
                    viewModel.onFailed(C_MSTAR_COD_ELIGIBLE_CHECK, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_COD_ELIGIBLE_CHECK, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarGetAlternateProduct(final T viewModel, Integer productId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MstarAlgoliaResponse> call = apiService.mstarGetAlternateProduct(productId);
        call.enqueue(new Callback<MstarAlgoliaResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarAlgoliaResponse> call, @NonNull Response<MstarAlgoliaResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_GET_ALTERNATE_PRODUCT);
                } else {
                    viewModel.onFailed(C_MSTAR_GET_ALTERNATE_PRODUCT, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarAlgoliaResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_GET_ALTERNATE_PRODUCT, null);
            }
        });
    }

    public <T extends AppViewModel> void mStarSocialLogin(final T viewModel, Map<String, String> socialRequest, String googleAdvId) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mStarSocialLogin(socialRequest.get(AppConstant.SIGNATURE), socialRequest.get(AppConstant.PAYLOAD), socialRequest.get(AppConstant.ACCESS_TOKEN), socialRequest.get(AppConstant.SOCIAL_FLAG), socialRequest.get(AppConstant.SOURCE), socialRequest.get(AppConstant.MOBILE_NUMBER), socialRequest.get(AppConstant.RANDOM_KEY), googleAdvId);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_SOCIAL_LOGIN);
                } else {
                    if (response.errorBody() != null && !TextUtils.isEmpty(response.errorBody().source().toString())) {
                        Type collectionType = new TypeToken<MStarBasicResponseTemplateModel>() {
                        }.getType();

                        MStarBasicResponseTemplateModel enums = new Gson().fromJson(response.errorBody().charStream(), collectionType);
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(new Gson().toJson(enums), MStarBasicResponseTemplateModel.class)), C_MSTAR_SOCIAL_LOGIN);
                    } else {
                        viewModel.onFailed(C_MSTAR_SOCIAL_LOGIN, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_SOCIAL_LOGIN, null);
            }
        });
    }


    public <T extends AppViewModel> void mStargetBuyAgainProducts(final T viewModel, Map<String, String> mstarHeaderMap) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetPastMedicines(mstarHeaderMap);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_PAST_MEDICINES);
                } else {
                    viewModel.onFailed(C_MSTAR_PAST_MEDICINES, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_PAST_MEDICINES, null);
            }
        });
    }

    public <T extends AppViewModel> void getMStarGenericProductDetails(final T viewModel, Integer productCode, String type) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MstarGenericMedicinesResponse> call = apiService.mstarGetGenericMedicineDetails(productCode, 1, type);
        call.enqueue(new Callback<MstarGenericMedicinesResponse>() {
            @Override
            public void onResponse(@NonNull Call<MstarGenericMedicinesResponse> call, @NonNull Response<MstarGenericMedicinesResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_GENERIC_PRODUCTS_DETAILS);
                } else {
                    viewModel.onFailed(C_MSTAR_GENERIC_PRODUCTS_DETAILS, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MstarGenericMedicinesResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_GENERIC_PRODUCTS_DETAILS, null);
            }
        });
    }

    public <T extends AppViewModel> void mstarRetrieveProductsFromUrl(final T viewModel, String url, MstarCategoryDetailsRequest request) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarGetDataByUrl(url, request.getReturnFacets(), request.getReturnProduct(), request.getProductsSelector(), request.getSubCategoryDepth(), request.getPageNo(), request.getPageSize(), request.getProductFieldSetForCategory(), request.getProductFieldSetForMfr(), request.getProductFieldSetForProduct(), request.getProductsSortOrder());
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_RETRIEVE_ANY_URL);
                } else {
                    viewModel.onFailed(C_MSTAR_RETRIEVE_ANY_URL, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_RETRIEVE_ANY_URL, null);
            }
        });

    }

    public <T extends AppViewModel> void MstarRateOrders(final T viewModel, Map<String, String> mstarHeaderMap, MultipartBody requestBody) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.mstarRateOrder(mstarHeaderMap, requestBody);
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MSTAR_RATE_ORDERS);
                } else {
                    if (response.errorBody() != null && !TextUtils.isEmpty(response.errorBody().source().toString())) {
                        Type collectionType = new TypeToken<MStarBasicResponseTemplateModel>() {
                        }.getType();

                        MStarBasicResponseTemplateModel enums = new Gson().fromJson(response.errorBody().charStream(), collectionType);
                        viewModel.onSyncData(new Gson().toJson(new Gson().fromJson(new Gson().toJson(enums), MStarBasicResponseTemplateModel.class)), C_MSTAR_RATE_ORDERS);
                    } else {
                        viewModel.onFailed(C_MSTAR_RATE_ORDERS, new Gson().toJson(response.errorBody()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MSTAR_RATE_ORDERS, null);
            }
        });
    }

    public <T extends AppViewModel> void medicineHomeBanner(final T viewModel) {
        RetrofitAPIInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL)).create(RetrofitAPIInterface.class);
        Call<MStarBasicResponseTemplateModel> call = apiService.medicineHomePage();
        call.enqueue(new Callback<MStarBasicResponseTemplateModel>() {
            @Override
            public void onResponse(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Response<MStarBasicResponseTemplateModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), C_MEDICINE_PAGE_BANNER);
                } else {
                    viewModel.onFailed(C_MEDICINE_PAGE_BANNER, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MStarBasicResponseTemplateModel> call, @NonNull Throwable t) {
                viewModel.onFailed(C_MEDICINE_PAGE_BANNER, null);
            }
        });
    }
}
