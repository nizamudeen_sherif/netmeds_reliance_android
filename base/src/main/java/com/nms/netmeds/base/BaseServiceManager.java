package com.nms.netmeds.base;

import java.io.IOException;

import okhttp3.ResponseBody;

public class BaseServiceManager {

    public String errorHandling(ResponseBody responseBody) {
        String errorResponse = "";
        try {
            errorResponse = responseBody != null ? responseBody.string() : "";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return errorResponse;
    }
}
