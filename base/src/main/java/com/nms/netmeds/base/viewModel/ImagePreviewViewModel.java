package com.nms.netmeds.base.viewModel;

import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.databinding.DialogImagePreviewBinding;

public class ImagePreviewViewModel extends AppViewModel {
    private ImagePreviewViewModelListener imagePreviewViewModelListener;

    public ImagePreviewViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(Context context, Object url, final DialogImagePreviewBinding imagePreviewBinding, ImagePreviewViewModelListener imagePreviewViewModelListener) {
        this.imagePreviewViewModelListener = imagePreviewViewModelListener;
        Glide.with(context)
                .load(url)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        imagePreviewBinding.previewClose.setVisibility(View.VISIBLE);
                        return false;
                    }
                }).into(imagePreviewBinding.previewImage);
    }

    public void onPreviewClose() {
        imagePreviewViewModelListener.closeDialog();
    }

    public interface ImagePreviewViewModelListener {
        void closeDialog();
    }
}
