package com.nms.netmeds.base.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.databinding.DialogImagePreviewBinding;
import com.nms.netmeds.base.viewModel.ImagePreviewViewModel;

@SuppressLint("ValidFragment")
public class ImagePreviewBottomSheetDialog extends BaseDialogFragment implements ImagePreviewViewModel.ImagePreviewViewModelListener {
    private final Object url;

    @SuppressLint("ValidFragment")
    public ImagePreviewBottomSheetDialog(Object url) {
        this.url = url;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setWindowAnimations(com.nms.netmeds.base.R.style.BottomSheetDialogAnimation);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setCancelable(true);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogImagePreviewBinding imagePreviewBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_image_preview, container, false);
        ImagePreviewViewModel imagePreviewViewModel = new ImagePreviewViewModel(getActivity().getApplication());
        imagePreviewViewModel.onDataAvailable(getActivity(), url, imagePreviewBinding, this);
        imagePreviewBinding.setViewModel(imagePreviewViewModel);
        return imagePreviewBinding.getRoot();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // Add back button listener
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                // getAction to make sure this doesn't double fire
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    closeDialog();
                    return true; // Capture onKey
                }
                return false; // Don't capture
            }
        });

        return dialog;
    }

    @Override
    public void closeDialog() {
        this.dismissAllowingStateLoss();
    }
}
