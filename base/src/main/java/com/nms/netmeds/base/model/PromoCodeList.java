package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PromoCodeList {

    @SerializedName("couponCode")
    private String couponCode;
    @SerializedName("productType")
    private String productType;
    @SerializedName("discount")
    private String discount;
    @SerializedName("minOrderValue")
    private String minOrderValue;
    @SerializedName("maxOrderValue")
    private String maxOrderValue;
    @SerializedName("activeStatus")
    private String activeStatus;
    @SerializedName("activeFrom")
    private String activeFrom;
    @SerializedName("activeTo")
    private String activeTo;
    @SerializedName(value = "couponDesc", alternate = {"description"})
    private String description;
    @SerializedName("couponId")
    private String couponId;
    transient private double nmsSuperCashAmount;
    @SerializedName("position")
    private int position;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getMinOrderValue() {
        return minOrderValue;
    }

    public void setMinOrderValue(String minOrderValue) {
        this.minOrderValue = minOrderValue;
    }

    public String getMaxOrderValue() {
        return maxOrderValue;
    }

    public void setMaxOrderValue(String maxOrderValue) {
        this.maxOrderValue = maxOrderValue;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(String activeFrom) {
        this.activeFrom = activeFrom;
    }

    public String getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(String activeTo) {
        this.activeTo = activeTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public double getNmsSuperCashAmount() {
        return nmsSuperCashAmount;
    }

    public void setNmsSuperCashAmount(double nmsSuperCashAmount) {
        this.nmsSuperCashAmount = nmsSuperCashAmount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
