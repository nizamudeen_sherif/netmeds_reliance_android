package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarManufacturerDetails {
    @SerializedName("is_active")
    private Boolean isActive;

    @SerializedName("product_count")
    private Integer productCount;

    @SerializedName("json_ext")
    private String jsonExt;

    @SerializedName("instock_product_count")
    private Integer instockProductCount;

    @SerializedName("filters")
    private MstarFilter filters;

    @SerializedName("makes_prescription_products")
    private Boolean makesPrescriptionProducts;

    @SerializedName("products")
    private List<MStarProductDetails> products;

    @SerializedName("makes_npnfmcg_products")
    private Boolean makesNpnfmcgProducts;

    @SerializedName("masthead_image_path")
    private String mastheadImagePath;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private Integer id;

    @SerializedName("thumbnail_image_path")
    private String thumbnailImagePath;

    @SerializedName("url_path")
    private String urlPath;


    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getJsonExt() {
        return jsonExt;
    }

    public void setJsonExt(String jsonExt) {
        this.jsonExt = jsonExt;
    }

    public void setThumbnailImagePath(String thumbnailImagePath) {
        this.thumbnailImagePath = thumbnailImagePath;
    }

    public Integer getInstockProductCount() {
        return instockProductCount;
    }

    public void setInstockProductCount(Integer instockProductCount) {
        this.instockProductCount = instockProductCount;
    }

    public MstarFilter getFilters() {
        return filters;
    }

    public void setFilters(MstarFilter filters) {
        this.filters = filters;
    }

    public Boolean getMakesPrescriptionProducts() {
        return makesPrescriptionProducts;
    }

    public void setMakesPrescriptionProducts(Boolean makesPrescriptionProducts) {
        this.makesPrescriptionProducts = makesPrescriptionProducts;
    }

    public List<MStarProductDetails> getProducts() {
        return products;
    }

    public void setProducts(List<MStarProductDetails> products) {
        this.products = products;
    }

    public Boolean getMakesNpnfmcgProducts() {
        return makesNpnfmcgProducts;
    }

    public void setMakesNpnfmcgProducts(Boolean makesNpnfmcgProducts) {
        this.makesNpnfmcgProducts = makesNpnfmcgProducts;
    }

    public String getMastheadImagePath() {
        return mastheadImagePath;
    }

    public void setMastheadImagePath(String mastheadImagePath) {
        this.mastheadImagePath = mastheadImagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

}
