package com.nms.netmeds.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberShipIcon {
    @SerializedName("linkpage")
    @Expose
    public String linkpage;
    @SerializedName("linktype")
    @Expose
    public String linktype;
    @SerializedName("imageUrl")
    @Expose
    public String imageUrl;

    public String getLinkpage() {
        return linkpage;
    }

    public void setLinkpage(String linkpage) {
        this.linkpage = linkpage;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
