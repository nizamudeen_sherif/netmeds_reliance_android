package com.nms.netmeds.base.model;

import android.graphics.Bitmap;

import java.io.File;

public class MStarUploadPrescription {


    private File file;
    private String uploadKey;
    private String pastPrescriptionId;
    private String uploadedPrescriptionId;
    private String digitalizedPrescriptionId;
    private String imageUrl;
    private Bitmap bitmapImage;
    private boolean isBitmapFromGalleryOrCamera;
    private boolean isDigitalized;
    private boolean isPrescriptionUploaded;
    private boolean isMethod2;
    private boolean isOrderReview;
    private boolean isChecked;

    public boolean isPrescriptionUploaded() {
        return isPrescriptionUploaded;
    }

    public void setPrescriptionUploaded(boolean prescriptionUploaded) {
        isPrescriptionUploaded = prescriptionUploaded;
    }

    public boolean isBitmapFromGalleryOrCamera() {
        return isBitmapFromGalleryOrCamera;
    }

    public void setBitmapFromGalleryOrCamera(boolean bitmapFromGalleryOrCamera) {
        isBitmapFromGalleryOrCamera = bitmapFromGalleryOrCamera;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getPastPrescriptionId() {
        return pastPrescriptionId;
    }

    public void setPastPrescriptionId(String pastPrescriptionId) {
        this.pastPrescriptionId = pastPrescriptionId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getBitmapImage() {
        return bitmapImage;
    }

    public void setBitmapImage(Bitmap bitmapImage) {
        this.bitmapImage = bitmapImage;
    }

    public boolean isDigitalized() {
        return isDigitalized;
    }

    public void setDigitalized(boolean digitalized) {
        isDigitalized = digitalized;
    }

    public String getDigitalizedPrescriptionId() {
        return digitalizedPrescriptionId;
    }

    public void setDigitalizedPrescriptionId(String digitalizedPrescriptionId) {
        this.digitalizedPrescriptionId = digitalizedPrescriptionId;
    }

    public String getUploadKey() {
        return uploadKey;
    }

    public void setUploadKey(String uploadKey) {
        this.uploadKey = uploadKey;
    }

    public String getUploadedPrescriptionId() {
        return uploadedPrescriptionId;
    }

    public void setUploadedPrescriptionId(String uploadedPrescriptionId) {
        this.uploadedPrescriptionId = uploadedPrescriptionId;
    }

    public boolean isMethod2() {
        return isMethod2;
    }

    public void setMethod2(boolean method2) {
        isMethod2 = method2;
    }

    public boolean isOrderReview() {
        return isOrderReview;
    }

    public void setOrderReview(boolean orderReview) {
        isOrderReview = orderReview;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
