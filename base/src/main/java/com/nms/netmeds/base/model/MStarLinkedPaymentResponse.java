package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarLinkedPaymentResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private MStarLinkedPaymentResult linkedPaymentResult;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MStarLinkedPaymentResult getLinkedPaymentResult() {
        return linkedPaymentResult;
    }

    public void setLinkedPaymentResult(MStarLinkedPaymentResult linkedPaymentResult) {
        this.linkedPaymentResult = linkedPaymentResult;
    }
}
