package com.nms.netmeds.base.retrofit;

import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.model.ApplyVoucherRequest;
import com.nms.netmeds.base.model.ApplyVoucherResponse;
import com.nms.netmeds.base.model.CancelOrderResponseModel;
import com.nms.netmeds.base.model.CartSubstitutionBasicResponseTemplateModel;
import com.nms.netmeds.base.model.CartSubstitutionRequest;
import com.nms.netmeds.base.model.CartSubstitutionResponse;
import com.nms.netmeds.base.model.CartSubstitutionUpdateCartRequest;
import com.nms.netmeds.base.model.CartSubstitutionUpdateCartResponse;
import com.nms.netmeds.base.model.CheckPrimeUserResponse;
import com.nms.netmeds.base.model.ColdStorageResponse;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.DigitizedRxResponse;
import com.nms.netmeds.base.model.Flow2SubscriptionRequest;
import com.nms.netmeds.base.model.GenerateOrderIdRequest;
import com.nms.netmeds.base.model.GetCityStateFromPinCodeResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResponse;
import com.nms.netmeds.base.model.M3Flow2SubscriptionResponse;
import com.nms.netmeds.base.model.M3SubscriptionLogRequest;
import com.nms.netmeds.base.model.MStarAddAddressModelRequest;
import com.nms.netmeds.base.model.MStarAddMultipleProductsResponse;
import com.nms.netmeds.base.model.MStarBase2Response;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerRegistrationResponse;
import com.nms.netmeds.base.model.MStarLinkedPaymentResponse;
import com.nms.netmeds.base.model.MstarAddressResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarBasicResponseTemplateModeForM2;
import com.nms.netmeds.base.model.MstarChangePasswordResponse;
import com.nms.netmeds.base.model.MstarEditAddressRequest;
import com.nms.netmeds.base.model.MstarGenericMedicinesResponse;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.nms.netmeds.base.model.MstarSubmitMethod2Response;
import com.nms.netmeds.base.model.MstarSwitchAlternateProductRequest;
import com.nms.netmeds.base.model.RecommendedProductResponse;
import com.nms.netmeds.base.model.Request.MStarRegistrationRequest;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface RetrofitAPIInterface {
    @GET(APIPathConstant.C_MSTAR_CONFIGURATION)
    Call<ConfigurationResponse> configuration();

    @GET("{pinCode}")
    Call<ColdStorageResponse> checkColdStorage(@Path("pinCode") String request);

    @POST()
    Call<DeliveryEstimateResponse> deliveryDateEstimate(@Body DeliveryEstimateRequest request, @Url String url);

    @POST(APIPathConstant.REFILL_SUBSCRIPTION)
    Call<M3Flow2SubscriptionResponse> refillSubscription(@Body Flow2SubscriptionRequest request);

    @POST(APIPathConstant.SUBSCRIPTION_LOG)
    Call<Object> subscriptionLog(@Body M3SubscriptionLogRequest request);

    @POST(APIPathConstant.REMOVE_VOUCHER)
    Call<ApplyVoucherResponse[]> removeVoucher(@Body ApplyVoucherRequest request);

    @GET(APIPathConstant.CHECK_PRIME_USER)
    Call<CheckPrimeUserResponse> checkPrimeUser(@HeaderMap Map<String, String> header);


    //MORNING STAR
    @GET(APIPathConstant.MSTAR_CONFIG_URL_PATH)
    Call<MStarBasicResponseTemplateModel> getConfigURL();


    @GET(APIPathConstant.MSTAR_CREATE_CART)
    Call<MStarBasicResponseTemplateModel> mStarCreateCart(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.MSTAR_ADD_PRODUCT_TO_CART)
    Call<MStarBasicResponseTemplateModel> mStarAddProductToCart(@HeaderMap Map<String, String> header, @Query(AppConstant.QUERY_PRODUCT_CODE) int productCode, @Query(AppConstant.QUERY_PRODUCT_QUANTITY) int productQuantity, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_REMOVE_PRODUCT_FROM_CART)
    Call<MStarBasicResponseTemplateModel> mStarRemoveProductFromCart(@HeaderMap Map<String, String> header, @Query(AppConstant.QUERY_PRODUCT_CODE) int productCode, @Query(AppConstant.QUERY_PRODUCT_QUANTITY) int productQuantity, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_GET_CART_DETAILS)
    Call<MStarBasicResponseTemplateModel> mStarGetCartDetails(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART)
    Call<CartSubstitutionBasicResponseTemplateModel> mStarGetAlternateProductForCart(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cart_id);

    @POST(APIPathConstant.MSTAR_SWITCH_ALTERNATE_PRODUCT)
    Call<MStarBasicResponseTemplateModel> mStarSwitchAlternateProduct(@HeaderMap Map<String, String> header, @Body List<MstarSwitchAlternateProductRequest> requests);

    @GET(APIPathConstant.MSTAR_REVERT_ORGINAL_CART)
    Call<MStarBasicResponseTemplateModel> setMstarRevertOriginalCart(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.MSTAR_INITIATE_CHECKOUT)
    Call<MStarBasicResponseTemplateModel> mStarInititateCheckout(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_CONTINUE_SHOPPING)
    Call<MStarBasicResponseTemplateModel> mStarContinueShopping(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_APPLY_PROMO_CODE)
    Call<MStarBasicResponseTemplateModel> mStarApplyPromoCode(@HeaderMap Map<String, String> header, @Query(AppConstant.QUERY_COUPON_CODE) String couponCode, @Query(AppConstant.CART_ID) Integer m2CartID);

    @GET(APIPathConstant.MSTAR_UN_APPLY_PROMO_CODE)
    Call<MStarBasicResponseTemplateModel> mStarUnApplyPromoCode(@HeaderMap Map<String, String> header, @Query(AppConstant.QUERY_COUPON_CODE) String couponCode, @Query(AppConstant.CART_ID) Integer m2CartID);

    @GET(APIPathConstant.MSTAR_GET_ALL_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarGetAllAddress(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.MSTAR_DELETE_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarDeleteAddress(@HeaderMap Map<String, String> header, @Path(AppConstant.QUERY_ADDRESS_ID) String deleteAddressId);

    @GET(APIPathConstant.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarSetPreferredShippingAddress(@HeaderMap Map<String, String> header, @Path(AppConstant.QUERY_ADDR_ID) int addressId);

    @GET(APIPathConstant.MSTAR_SET_PREFERRED_BILLING_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarSetPreferredBillingAddress(@HeaderMap Map<String, String> header, @Path(AppConstant.QUERY_ADDR_ID) int addressId);

    @GET(APIPathConstant.MSTAR_SET_M2_SHIPPING_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarSetM2ShippingAddress(@HeaderMap Map<String, String> header, @Query(AppConstant.QUERY_ADDRESS_ID) int addressId, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_SET_M2_BILLING_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarSetM2BillingAddress(@HeaderMap Map<String, String> header, @Query(AppConstant.QUERY_ADDRESS_ID) int addressId, @Query(AppConstant.CART_ID) Integer cart_id);

    @GET(APIPathConstant.MSTAR_CUSTOMER_DETAILS)
    Call<MStarBasicResponseTemplateModel> mStarCustomerDetails(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.MSTAR_SINGLE_ADDRESS_DETAILS)
    Call<MstarAddressResponse> mStarSingleAddressDetails(@HeaderMap Map<String, String> header, @Path(AppConstant.QUERY_ADDRESS_ID) String addressId);

    @POST(APIPathConstant.MSTAR_NEW_ADDRESS)
    Call<MStarBasicResponseTemplateModel> mStarNewAddress(@HeaderMap Map<String, String> header, @Body MStarAddAddressModelRequest addressModel);

    @POST(APIPathConstant.MSTAR_UPDATE_ADDRESS_DETAILS)
    Call<MStarBasicResponseTemplateModel> mStarUpdateAddress(@HeaderMap Map<String, String> header, @Body MstarEditAddressRequest addressModel);

    @GET(APIPathConstant.MSTAR_PRODUCT_DETAILS)
    Call<MStarBasicResponseTemplateModel> mStarProductDetails(@Path(AppConstant.PRODUCT_ID) int productId, @Query(AppConstant.PRODUCT_FIELD_SET_NAME) String productFieldSetName);

    @GET(APIPathConstant.MSTAR_PRODUCT_DETAILS_FOR_ID_LIST)
    Call<MStarBasicResponseTemplateModel> mStarProductDetailsForIdList(@Query(AppConstant.PRODUCT_ID_LIST) String productIdList);

    @GET(APIPathConstant.MSTAR_GET_CATEGORY_DETAILS)
    Call<MStarBasicResponseTemplateModel> mStarCategoryDetails(@Path(AppConstant.MSTAR_CATEGORY_ID) Integer categoryId, @Query(AppConstant.RETURN_FACETS) String returnFacets, @Query(AppConstant.RETURN_PRODUCTS) String returnProducts, @Query(AppConstant.PAGE_NO) Integer pgNo, @Query(AppConstant.PAGE_SIZE) Integer pageSize, @Query(AppConstant.PRODUCT_FIELD_SET_NAME) String fieldsetName, @Query(AppConstant.SUB_CATEGORY_DEPTH) Integer subCategoryDepth, @Query(AppConstant.PRODUCT_SORT_ORDER) String productSortOrder);

    @GET(APIPathConstant.MSTAR_GET_MANUFACTURERE_DETAILS)
    Call<MStarBasicResponseTemplateModel> mStarManufacturerDetails(@Path(AppConstant.MSTAR_MANUFACTURER_ID) Integer manufacturerId, @Query(AppConstant.RETURN_FACETS) String returnFacets, @Query(AppConstant.RETURN_PRODUCTS) String returnProducts, @Query(AppConstant.PAGE_NO) Integer pgNo, @Query(AppConstant.PAGE_SIZE) Integer pageSize, @Query(AppConstant.PRODUCT_FIELD_SET_NAME) String fieldsetName, @Query(AppConstant.PRODUCT_SORT_ORDER) String productSortOrder, @Query(AppConstant.PRODUCTS_SELECTOR) String productsSelector);

    @GET(APIPathConstant.MSTAR_SENT_OTP)
    Call<MStarBasicResponseTemplateModel> mStarSentOtp(@Query(AppConstant.MOBILE_NO) String phoneNumber);

    @GET(APIPathConstant.MSTAR_VERIFY_OTP)
    Call<MStarBasicResponseTemplateModel> mStarVerifyOtp(@Query(AppConstant.MOBILE_NO) String phoneNumber, @Query(AppConstant.RK) String rk, @Query(AppConstant.OTP) String otp, @Query(AppConstant.MSTAR_CHANNEL) String channel);

    @POST(APIPathConstant.MSTAR_UPLOAD_PRESCRIPTION)
    Call<MStarBasicResponseTemplateModel> mstarUploadPrescription(@HeaderMap Map<String, String> header, @Query(AppConstant.NEW_METHOD2_CART) String newMethodCart2, @Body RequestBody requestBody);

    @POST(APIPathConstant.MSTAR_UPLOAD_PRESCRIPTION)
    Call<MStarBasicResponseTemplateModel> mstarUploadPrescriptionWithCartID(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer method2CartID, @Body RequestBody requestBody);

    @GET(APIPathConstant.MSTAR_DETACH_PRESCRIPTION)
    Call<MStarBasicResponseTemplateModel> mStartDetachPrescription(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cart_id, @QueryMap Map<String, String> detachPrescriptionListMap);

    @GET(APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION)
    Call<MStarBasicResponseTemplateModel> mStartDownloadPrescription(@HeaderMap Map<String, String> header, @Query(AppConstant.PID) String pid);

    @GET(APIPathConstant.MSTAR_PRIME_PRODUCTS)
    Call<MstarPrimeProductResult> mstarGetPrimeProducts();

    @GET(APIPathConstant.MSTAR_APPLY_VOUCHER)
    Call<MStarBasicResponseTemplateModel> mstarApplyVoucher(@HeaderMap Map<String, String> header, @Query(AppConstant.VOUCHER_CODE) String voucherCode, @Query(AppConstant.CART_ID) Integer cartId);

    @GET(APIPathConstant.MSTAR_UNAPPLY_VOUCHER)
    Call<MStarBasicResponseTemplateModel> mstarUnapplyVoucher(@HeaderMap Map<String, String> header, @Query(AppConstant.VOUCHER_CODE) String voucherCode, @Query(AppConstant.CART_ID) Integer cartId);

    @GET(APIPathConstant.C_MSTAR_HOME_BANNER)
    Call<MStarBasicResponseTemplateModel> mstarHomeBanner();

    @GET(APIPathConstant.C_MSTAR_WALLET_DETAILS)
    Call<MStarBasicResponseTemplateModel> mstarGetWalletDetails(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.C_MSTAR_REFER_AND_EARN)
    Call<MStarBasicResponseTemplateModel> mstarReferAndEarn(@HeaderMap Map<String, String> header);

    @POST(APIPathConstant.C_MSTAR_ORDER_HEADER)
    Call<MStarBasicResponseTemplateModel> mstarGetOrderHeader(@HeaderMap Map<String, String> header, @Body RequestBody request);

    @POST(APIPathConstant.C_MSTAR_ORDER_DETAILS)
    Call<MStarBasicResponseTemplateModel> mstarGetOrderDetails(@HeaderMap Map<String, String> header, @Body RequestBody request);

    @GET(APIPathConstant.MSTAR_BRAND_PRODUCTLIST)
    Call<MStarBasicResponseTemplateModel> mstarBrandProductList(@Path(AppConstant.MSTAR_BRAND_ID) int brandId, @Query(AppConstant.RETURN_FACETS) String returnFacets, @Query(AppConstant.RETURN_PRODUCTS) String returnProducts, @Query(AppConstant.PAGE_NO) Integer pgNo, @Query(AppConstant.PAGE_SIZE) Integer pageSize);

    @GET(APIPathConstant.MSTAR_CLEAR_CART)
    Call<MStarBasicResponseTemplateModel> mstarClearCart(@HeaderMap Map<String, String> header);

    @POST(APIPathConstant.C_MSTAR_TRACK_ORDER)
    Call<MStarBasicResponseTemplateModel> mstarTrackOrderDetails(@HeaderMap Map<String, String> header, @Body RequestBody request);

    @POST(APIPathConstant.C_MSTAR_SUBSCRIPTION_TRACK_ORDER)
    Call<MStarBasicResponseTemplateModel> mstarSubscriptionTrackOrderDetails(@HeaderMap Map<String, String> header, @Body RequestBody request);

    @POST(APIPathConstant.C_MSTAR_CANCEL_ORDER)
    Call<CancelOrderResponseModel> mstarCancelOrder(@HeaderMap Map<String, String> header, @Body RequestBody request);

    @FormUrlEncoded
    @POST(APIPathConstant.MSTAR_LOGIN)
    Call<MStarBasicResponseTemplateModel> mstarLogIn(@Field(AppConstant.MSTAR_API_HEADER_USER_ID) String userid, @Field(AppConstant.PASSWORD) String password, @Field(AppConstant.MSTAR_CHANNEL) String channel, @Field(AppConstant.MERGE_SESSION) String mergeSession);

    @GET(APIPathConstant.MSTAR_GET_METHOD_2_CART)
    Call<MstarBasicResponseTemplateModeForM2> mStarGetMethod2Carts(@HeaderMap Map<String, String> mStarHeaderMap);

    @GET(APIPathConstant.MSTAR_SUBMIT_METHOD_2)
    Call<MstarSubmitMethod2Response> mStarSubmitMethod2(@HeaderMap Map<String, String> mStarHeaderMap, @Query(AppConstant.CART_ID) Integer m2CartId, @Query(AppConstant.DOC_CONSULT_NEEDED) String dr_cons_needed);

    @POST(APIPathConstant.C_MSTAR_DIGITALIZED_RX)
    Call<DigitizedRxResponse> mStarGetDigitizedRx(@HeaderMap Map<String, String> header, @Body MultipartBody requestBody);

    @POST(APIPathConstant.C_MSTAR_PAST_RX)
    Call<GetPastPrescriptionResponse> getMstarPastPrescription(@HeaderMap Map<String, String> header, @Body MultipartBody requestBodye);

    @GET(APIPathConstant.C_MSTAR_ALL_OFFERS)
    Call<MStarBasicResponseTemplateModel> mstarAllOffers(@Query(AppConstant.ID) Integer id);

    @GET(APIPathConstant.C_MSTAR_GATEWAY_OFFERS)
    Call<MStarBasicResponseTemplateModel> mstarGatewayOffers();

    @POST(APIPathConstant.C_MSTAR_M2_ORDER_DETAILS)
    Call<MStarBasicResponseTemplateModel> mstarGetM2OrderDetails(@HeaderMap Map<String, String> header, @Body RequestBody request);

    @FormUrlEncoded
    @POST(APIPathConstant.MSTAR_INITIATE_CHANGE_PASSWORD)
    Call<MStarBasicResponseTemplateModel> mstarInitialteChangePassword(@Field(AppConstant.MSTAR_API_HEADER_USER_ID) String userid);

    @FormUrlEncoded
    @POST(APIPathConstant.MSTAR_COMPLETE_CHANGE_PASSWORD)
    Call<MstarChangePasswordResponse> mstarCompleteChangePassword(@Field(AppConstant.MOBILE_NO) String mobileNo, @Field(AppConstant.RK) String randomKey, @Field(AppConstant.OTP) String otp, @Field(AppConstant.NEW_PASSWORD) String newPassword);

    @GET(APIPathConstant.MSTAR_RESEND_SAME_OTP)
    Call<MStarBasicResponseTemplateModel> mstarResendSameOtp(@Query(AppConstant.RK) String randomKey);

    @GET(APIPathConstant.C_MSTAR_WELLNESSPAGE_DETAILS)
    Call<MStarBasicResponseTemplateModel> mstarGetWellnessPageDetails();

    @GET(APIPathConstant.C_MSTAR_WELLNESSPAGE_DETAILS_V2)
    Call<MStarBasicResponseTemplateModel> mstarGetWellnessPageDetailsVersion2();

    @POST(APIPathConstant.CART_SUBSTITUTION)
    Call<CartSubstitutionResponse> cartSubstitution(@Body List<CartSubstitutionRequest> request);

    @POST(APIPathConstant.CART_SUBSTITUTION_UPDATE_CART)
    Call<CartSubstitutionUpdateCartResponse> cartSubstitutionUpdateCart(@Body CartSubstitutionUpdateCartRequest request);

    @GET(APIPathConstant.C_MSTAR_HEALTH_ARTICLE)
    Call<MStarBasicResponseTemplateModel> getHealthArticle();

    @POST(APIPathConstant.C_MSTAR_HIGH_RANGE_PRODUCT)
    Call<MstarRequestPopupResponse> highRangeProduct(@Body MultipartBody multipartBody);

    @POST(APIPathConstant.C_MSTAR_REQUEST_NEW_PRODUCT)
    Call<MstarRequestPopupResponse> requestNewProduct(@Body MultipartBody multipartBody);

    @POST(APIPathConstant.C_MSTAR_NOTIFY_ME)
    Call<MstarRequestPopupResponse> notifyMe(@Body MultipartBody body);

    @FormUrlEncoded
    @POST(APIPathConstant.C_MSTAR_MY_PRESCRIPTION)
    Call<MStarBasicResponseTemplateModel> mstarMyPrescription(@HeaderMap Map<String, String> header, @Field(AppConstant.PAGE_INDEX) String pageIndex, @Field(AppConstant.PAGESIZE) String pageSize);

    @GET(APIPathConstant.C_MSTAR_PROMOTION_BANNER)
    Call<MStarBasicResponseTemplateModel> mstarPromotionBanner();

    @GET(APIPathConstant.MSTAR_CREATE_GUEST_SESSION)
    Call<MStarBasicResponseTemplateModel> createGuestSession(@Query(AppConstant.MSTAR_CHANNEL) String channelName);

    @GET(APIPathConstant.C_MSTAR_HEALTHCONCERN_AND_WELLNESS_PRODUCTS)
    Call<MStarBasicResponseTemplateModel> mstarHealthConcernBanner();

    @GET(APIPathConstant.MSTAR_GET_USER_STATUS)
    Call<MStarBasicResponseTemplateModel> mstarGetUserStatus(@Path(AppConstant.MSTAR_API_HEADER_USER_ID) String userId);

    @POST(APIPathConstant.MSTAR_CUSTOMER_REGISTRATION)
    Call<MStarCustomerRegistrationResponse> mstarCustomerRegistration(@Header(AppConstant.MSTAR_FINGERPRINT) String googleAdvId, @Body MStarRegistrationRequest registrationRequest, @Query(AppConstant.MSTAR_CHANNEL) String channel, @Query(AppConstant.MERGE_SESSION) String mergeSession);

    @GET(APIPathConstant.MSTAR_GET_PAYMENT_WALLET_BALANCE)
    Call<MStarBasicResponseTemplateModel> getMStarPaymentWalletBalance(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cartId);

    @GET(APIPathConstant.MSTAR_APPLY_WALLET)
    Call<MStarBasicResponseTemplateModel> getMStarApplyWallet(@HeaderMap Map<String, String> header, @Query(AppConstant.WALLET) String walletApplied, @Query(AppConstant.NMS_SUPER_CASH) String superCashApplied, @Query(AppConstant.CART_ID) Integer cartId);

    @GET(APIPathConstant.MSTAR_UNAPPLY_WALLET)
    Call<MStarBasicResponseTemplateModel> mstarUnApplyWallet(@HeaderMap Map<String, String> header, @Query(AppConstant.WALLET) String walletAmount, @Query(AppConstant.NMS_SUPER_CASH) String nmsSuperCashAmount, @Query(AppConstant.CART_ID) Integer cartId);

    @GET(APIPathConstant.C_MSTAR_COUPON_LIST)
    Call<MStarBasicResponseTemplateModel> couponList();

    @POST(APIPathConstant.MSTAR_UPDATE_CUSTOMER_DETAILS)
    Call<MStarBasicResponseTemplateModel> mstarUpdateCustomer(@HeaderMap Map<String, String> header, @Body MstarUpdateCustomerRequest request);

    @POST(APIPathConstant.MSTAR_ADD_MULTIPLE_PRODUCTS)
    Call<MStarAddMultipleProductsResponse> mstarAddMultipleProducts(@HeaderMap Map<String, String> header, @Body Map<String, Integer> productList);

    @GET(APIPathConstant.C_MSTAR_GET_LINKED_PAYMENT)
    Call<MStarLinkedPaymentResponse> mStarGetLinkedPayment(@HeaderMap Map<String, String> header);

    @GET()
    Call<RecommendedProductResponse> getCartSuggestedProductList(@Url String url);

    @GET(APIPathConstant.MSTAR_GET_REORDER_CART)
    Call<MStarBasicResponseTemplateModel> getMstarReorderCart(@HeaderMap Map<String, String> header, @Query(AppConstant.MSTAR_CUSTOMER_ID) String customerId, @Query(AppConstant.MSTAR_ORDER_ID) String orderId);

    @GET(APIPathConstant.MSTAR_GET_FILLED_METHOD2_CART)
    Call<MStarBasicResponseTemplateModel> getFilledM2Cart(@HeaderMap Map<String, String> header, @Query(AppConstant.MSTAR_CUSTOMER_ID) String customerId, @Query(AppConstant.MSTAR_ORDER_ID) String orderId);

    @FormUrlEncoded
    @POST(APIPathConstant.C_MSTAR_NEED_HELP)
    Call<MStarBase2Response> getNeedHelp(@HeaderMap Map<String, String> header, @Field(AppConstant.TITLE_TEXT) String titleTxt, @Field(AppConstant.SUBJECT) String subject, @Field(AppConstant.FULL_NAME_SMALL) String fullname, @Field(AppConstant.DESCRIPTION) String description, @Field(AppConstant.PHONE) String phone, @Field(AppConstant.EMAIL) String email);

    @GET(APIPathConstant.MSTAR_CREATE_SUBSCRIPTION)
    Call<MStarBasicResponseTemplateModel> getMstarReorderCart(@HeaderMap Map<String, String> header, @Query(AppConstant.PRIMARY_ORDER_ID) String primaryOrderId, @Query(AppConstant.DAYS_BETWEEN_ISSUE) String daysBetweenIssue, @Query(AppConstant.FROM_PAGE) String fromPage);


    @GET(APIPathConstant.MSTAR_GET_RETRY_CART)
    Call<MStarBasicResponseTemplateModel> getRetryCart(@HeaderMap Map<String, String> header, @Query(AppConstant.MSTAR_CUSTOMER_ID) String customerId, @Query(AppConstant.MSTAR_ORDER_ID) String orderId);

    @DELETE(APIPathConstant.DE_LINK)
    Call<Object> deLinkPayTm(@Path(AppConstant.KEY_LINK_TOKEN) String linkToken, @Header(AppConstant.KEY_AUTHORIZATION) String authorization, @Header(AppConstant.KEY_CONTENT_TYPE) String contentType, @Header(AppConstant.KEY_CONTENT_LENGTH) String contentLength, @Header(AppConstant.KEY_HOST) String host);

    //payment

    @GET(APIPathConstant.MSTAR_COD_PAYMENT)
    Call<MStarBasicResponseTemplateModel> mStarCodPayment(@HeaderMap Map<String, String> header, @Query(AppConstant.CART_ID) Integer cartId);

    @FormUrlEncoded
    @POST(APIPathConstant.C_MSTAR_PAYMENT_LOG)
    Call<MStarBasicResponseTemplateModel> paymentLog(@HeaderMap Map<String, String> header,
                                                     @Field(AppConstant.ORDER_ID) String orderId,
                                                     @Field(AppConstant.PAYMENT_METHOD) String paymentMethod,
                                                     @Field(AppConstant.REQUEST) String request,
                                                     @Field(AppConstant.RESPONSE) String response,
                                                     @Field(AppConstant.APP_VERSION) String appVersion,
                                                     @Field(AppConstant.APP_SOURCE_NAME) String sourceName);

    @POST(APIPathConstant.C_MSTAR_COD_ELIGIBLE_CHECK)
    Call<MStarBasicResponseTemplateModel> checkCODEligibleForCreateNewOrder(@HeaderMap Map<String, String> header);

    @POST(APIPathConstant.MSTAR_GENERATE_ORDER_ID)
    Call<MStarBasicResponseTemplateModel> generateOrderId(@HeaderMap Map<String, String> header,
                                                          @Query(AppConstant.CART_ID) Integer cartId,
                                                          @Query(AppConstant.API_DR_CONSUL_NEEDED) String isDrConsultantNeeded,
                                                          @Query(AppConstant.FORCE_NEW) String isForceNew,
                                                          @Body GenerateOrderIdRequest request, @Query(AppConstant.EDIT_SCOPE) String editScope);

    @GET(APIPathConstant.MSTAR_SUBSCRIPTION_PAYNOW)
    Call<MStarBasicResponseTemplateModel> mstarSubscriptionPayNow(@HeaderMap Map<String, String> header, @Query(AppConstant.MSTAR_CUSTOMER_ID) String customerId, @Query(AppConstant.PRIMARY_ORDER_ID) String primaryOrderID, @Query(AppConstant.ISSUE_ID) String issueID);

    @FormUrlEncoded
    @POST(APIPathConstant.C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS)
    Call<MStarBasicResponseTemplateModel> getM3OrderDetails(@Field(AppConstant.SUBSCRIPTION_ISSUE_ID) String issueId, @Field(AppConstant.SUBSCRIPTION_PRIMARY_ORDER_ID) String primaryOrderId, @HeaderMap Map<String, String> header);

    @GET(APIPathConstant.C_MSTAR_CATLOG_BANNERS)
    Call<MStarBasicResponseTemplateModel> mstarCatlogBanneres(@Query(AppConstant.TYPE) String type, @Query(AppConstant.C_CATEGORY_ID) String primaryOrderID, @Query(AppConstant.C_MANUFACTURE_ID) String issueID,@Query(AppConstant.C_BRAND_ID) String bannerId);

    @GET(APIPathConstant.C_MSTAR_GET_CITY_STATE_FROM_PINCODE)
    Call<GetCityStateFromPinCodeResponse> mstarGetPinCodeDetials(@Path(AppConstant.PINCODE) String pincode);

    @GET(APIPathConstant.MSTAR_CREATE_SUBSCRIPTION_CART)
    Call<MStarBasicResponseTemplateModel> mStarSubscriptionCreateCart(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.C_MSTAR_GET_ALTERNATE_PRODUCT)
    Call<MstarAlgoliaResponse> mstarGetAlternateProduct(@Query(AppConstant.PRODUCTID) int productId);

    @FormUrlEncoded
    @POST(APIPathConstant.C_MSTAR_SOCIAL_LOGIN)
    Call<MStarBasicResponseTemplateModel> mStarSocialLogin(@Field(AppConstant.SIGNATURE) String signature,
                                                           @Field(AppConstant.PAYLOAD) String payload,
                                                           @Field(AppConstant.ACCESS_TOKEN) String accessToken,
                                                           @Field(AppConstant.SOCIAL_FLAG) String socialFlag,
                                                           @Field(AppConstant.SOURCE) String source,
                                                           @Field(AppConstant.MOBILE_NUMBER) String mobileNo,
                                                           @Field(AppConstant.RANDOM_KEY) String randomKey,
                                                           @Header(AppConstant.MSTAR_FINGERPRINT) String fingerprint);

    @GET(APIPathConstant.C_MSTAR_PAST_MEDICINES)
    Call<MStarBasicResponseTemplateModel> mstarGetPastMedicines(@HeaderMap Map<String, String> header);

    @GET(APIPathConstant.C_MEDICINE_HOME_BANNER)
    Call<MStarBasicResponseTemplateModel> medicineHomePage();


    @GET(APIPathConstant.C_MSTAR_GENERIC_MEDICINE_DETAIL)
    Call<MstarGenericMedicinesResponse> mstarGetGenericMedicineDetails(@Query(AppConstant.DRUGCODE) Integer productId, @Query(AppConstant.QUERY_PRODUCT_QUANTITY) Integer qty,@Query(AppConstant.TYPE) String type);

    @GET(APIPathConstant.C_MSTAR_RETRIEVE_BY_URL)
    Call<MStarBasicResponseTemplateModel> mstarGetDataByUrl(@Path(AppConstant.URL) String brandUrl,
                                                            @Query(AppConstant.RETURN_FACETS) String returnFacets, @Query(AppConstant.RETURN_PRODUCTS) String returnProducts,
                                                            @Query(AppConstant.PRODUCTS_SELECTOR) String productSelector,@Query(AppConstant.SUB_CATEGORY_DEPTH) Integer subCategoryDepth,
                                                            @Query(AppConstant.PAGE_NO) Integer pgNo, @Query(AppConstant.PAGE_SIZE) Integer pageSize,
                                                            @Query(AppConstant.PRODUCT_FIELD_SET_NAME_FOR_CATEGORY) String fieldsetNameForCategory,
                                                            @Query(AppConstant.PRODUCT_FIELD_SET_NAME_FOR_MANUFACTURER) String fieldsetNameForMfr,
                                                            @Query(AppConstant.PRODUCT_FIELD_SET_NAME_FOR_PRODUCT) String fieldsetNameForProduct,
                                                            @Query(AppConstant.PRODUCT_SORT_ORDER) String productSortOrder);
    @POST(APIPathConstant.C_MSTAR_RATE_ORDERS)
    Call<MStarBasicResponseTemplateModel> mstarRateOrder(@HeaderMap Map<String, String> header, @Body RequestBody requestBody);
}
