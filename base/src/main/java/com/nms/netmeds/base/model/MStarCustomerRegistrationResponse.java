package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarCustomerRegistrationResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private MStarRegistrationResult result;
    @SerializedName("response_time")
    private String responseTime;
    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel mStarRegistrationReason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MStarRegistrationResult getResult() {
        return result;
    }

    public void setResult(MStarRegistrationResult result) {
        this.result = result;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public MstarBasicResponseFailureReasonTemplateModel getmStarRegistrationReason() {
        return mStarRegistrationReason;
    }

    public void setmStarRegistrationReason(MstarBasicResponseFailureReasonTemplateModel mStarRegistrationReason) {
        this.mStarRegistrationReason = mStarRegistrationReason;
    }
}
