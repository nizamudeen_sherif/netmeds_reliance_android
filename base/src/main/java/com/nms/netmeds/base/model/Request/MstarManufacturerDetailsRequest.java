package com.nms.netmeds.base.model.Request;

import com.google.gson.annotations.SerializedName;

public class MstarManufacturerDetailsRequest {

    @SerializedName("mfgr_id")
    private Integer manufacturerId;
    @SerializedName("return_products")
    private String returnProducts;
    @SerializedName("return_facets")
    private String returnFacets;
    @SerializedName("page_size")
    private Integer pageSize;
    @SerializedName("page_no")
    private Integer pageNo;
    @SerializedName("product_fieldset_name")
    private String productFieldsetName;

    private String productsSelector;
    private String productsSortOrder;

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getReturnProducts() {
        return returnProducts;
    }

    public void setReturnProducts(String returnProducts) {
        this.returnProducts = returnProducts;
    }

    public String getReturnFacets() {
        return returnFacets;
    }

    public void setReturnFacets(String returnFacets) {
        this.returnFacets = returnFacets;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getProductFieldsetName() {
        return productFieldsetName;
    }

    public void setProductFieldsetName(String productFieldsetName) {
        this.productFieldsetName = productFieldsetName;
    }

    public String getProductsSelector() {
        return productsSelector;
    }

    public void setProductsSelector(String productsSelector) {
        this.productsSelector = productsSelector;
    }

    public String getProductsSortOrder() {
        return productsSortOrder;
    }

    public void setProductsSortOrder(String productsSortOrder) {
        this.productsSortOrder = productsSortOrder;
    }
}
