package com.nms.netmeds.base;

import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarProductDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RefillHelper {

    private static Map<String, MStarAddedProductsResult> outOfStockProductsMap = new HashMap<>();
    private static Map<String, String> maxLimitBreachedProductsMap = new HashMap<>();
    private static boolean isPrblmInAddingProducts;

    private static Integer M2ShippingAddress = 0;
    private static ArrayList<String> newRxIdList = new ArrayList<>();


    public static Map<String, MStarAddedProductsResult> getOutOfStockProductsMap() {
        return outOfStockProductsMap;
    }


    public static void setPrblmInAddingProducts(boolean productNotAdded) {
        isPrblmInAddingProducts = productNotAdded;
    }

    public static void resetMapValues() {
        outOfStockProductsMap.clear();
        maxLimitBreachedProductsMap.clear();
        newRxIdList.clear();
    }

    public static Integer getM2ShippingAddress() {
        return M2ShippingAddress;
    }

    public static void setM2ShippingAddress(Integer m2ShippingAddress) {
        M2ShippingAddress = m2ShippingAddress;
    }

    public static ArrayList<String> getNewRxIdList() {
        return newRxIdList;
    }

    public static Map<String, String> getMaxLimitBreachedProductsMap() {
        return maxLimitBreachedProductsMap;
    }
}
