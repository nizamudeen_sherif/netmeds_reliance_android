package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class DrugCode {

    @SerializedName(value = "drugcode", alternate = {"drugCode"})
    private String drugCode;
    @SerializedName("qty")
    private String qty;
    @SerializedName("drugName")
    private String drugName;

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }
}
