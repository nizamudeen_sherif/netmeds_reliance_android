package com.nms.netmeds.base;

public class SubscriptionHelper {

    private static SubscriptionHelper helper;

    private String issueId;
    private String orderId;
    private String nextDeliveryIntervalDate;
    private String currentDeliveryDate;
    private String issueOrderDate;
    private String editOrderRequest;
    private String editOrderResponse;
    private String cartItemResponse = "";
    private String SubscriptionThisIssue;
    private Integer customerId;
    private Integer SubscriptionCartId = 0;

    private int tabPosition = 0;
    private int recyclerViewPosition = 0;

    private boolean createNewFillFlag = false;
    private boolean subscriptionFlag = false;
    private boolean subscriptionPromocodeApplied = false;
    private boolean orderSuccessFalg = false;
    private boolean lastMonthIssueOrderFlag = false;
    private boolean editorderquantityflag = false;
    private boolean primaryOrderFlag = false;
    private boolean isThisIssueOrder = false;
    private boolean uploadPrescription = false;
    private boolean SubscriptionIssueOrderProductDetailFlag = false;
    private boolean isRefillSubscription = false;
    private boolean isPayNowSubscription = false;
    private boolean isSubscriptionEdit = false;

    public boolean isUploadPrescription() {
        return uploadPrescription;
    }

    public void setUploadPrescription(boolean uploadPrescription) {
        this.uploadPrescription = uploadPrescription;
    }

    public static SubscriptionHelper getInstance() {
        if (helper == null) {
            helper = new SubscriptionHelper();
        }
        return helper;
    }

    public boolean getLastMonthIssueOrderFlag() {
        return lastMonthIssueOrderFlag;
    }

    public void setLastMonthIssueOrderFlag(boolean lastMonthIssueOrderFlag) {
        this.lastMonthIssueOrderFlag = lastMonthIssueOrderFlag;
    }

    public static SubscriptionHelper getHelper() {
        return helper;
    }

    public static void setHelper(SubscriptionHelper helper) {
        SubscriptionHelper.helper = helper;
    }


    public boolean getPrimaryOrderFlag() {
        return primaryOrderFlag;
    }

    public void setPrimaryOrderFlag(boolean primaryOrderFlag) {
        this.primaryOrderFlag = primaryOrderFlag;
    }

    public boolean isEditorderquantityflag() {
        return editorderquantityflag;
    }

    public void setEditorderquantityflag(boolean editorderquantityflag) {
        this.editorderquantityflag = editorderquantityflag;
    }

    public String getEditOrderRequest() {
        return editOrderRequest;
    }

    public void setEditOrderRequest(String editOrderRequest) {
        this.editOrderRequest = editOrderRequest;
    }

    public String getEditOrderResponse() {
        return editOrderResponse;
    }

    public void setEditOrderResponse(String editOrderResponse) {
        this.editOrderResponse = editOrderResponse;
    }

    public boolean isSubscriptionFlag() {
        return subscriptionFlag;
    }

    public void setSubscriptionFlag(boolean subscriptionFlag) {
        this.subscriptionFlag = subscriptionFlag;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public boolean isCreateNewFillFlag() {
        return createNewFillFlag;
    }

    public void setCreateNewFillFlag(boolean createNewFillFlag) {
        this.createNewFillFlag = createNewFillFlag;
    }

    public void setNextDeliveryIntervalDate(String nextDeliveryIntervalDate) {
        this.nextDeliveryIntervalDate = nextDeliveryIntervalDate;
    }

    public String getNextDeliveryIntervalDate() {
        return nextDeliveryIntervalDate;
    }

    public void setCurrentDeliveryDate(String currentDeliveryDate) {
        this.currentDeliveryDate = currentDeliveryDate;
    }

    public String getCurrentDeliveryDate() {
        return currentDeliveryDate;
    }

    public void setSubscriptionPromocodeApplied(boolean subscriptionPromocodeApplied) {
        this.subscriptionPromocodeApplied = subscriptionPromocodeApplied;
    }

    public boolean getSubscriptionPromocodeApplied() {
        return subscriptionPromocodeApplied;
    }

    public String getIssueOrderDate() {
        return issueOrderDate;
    }

    public void setIssueOrderDate(String issueOrderDate) {
        this.issueOrderDate = issueOrderDate;
    }

    public boolean getOrderSuccessFalg() {
        return orderSuccessFalg;
    }

    public void setOrderSuccessFalg(boolean orderSuccessFalg) {
        this.orderSuccessFalg = orderSuccessFalg;
    }

    public int getTabPosition() {
        return tabPosition;
    }

    public void setTabPosition(int tabPosition) {
        this.tabPosition = tabPosition;
    }

    public int getRecyclerviewPostion() {
        return recyclerViewPosition;
    }

    public void setRecyclerviewPostion(int recyclerviewPostion) {
        this.recyclerViewPosition = recyclerviewPostion;
    }

    public void saveCartItemResponse(String data) {
        this.cartItemResponse = data;
    }

    public String getCartItemResponse() {
        return cartItemResponse;
    }

    public boolean isThisIssueOrder() {
        return isThisIssueOrder;
    }

    public void setThisIsIssueOrder(boolean thisIssueOrder) {
        isThisIssueOrder = thisIssueOrder;
    }

    public boolean isSubscriptionIssueOrderProductDetailFlag() {
        return SubscriptionIssueOrderProductDetailFlag;
    }

    public void setSubscriptionIssueOrderProductDetailFlag(boolean subscriptionIssueOrderProductDetailFlag) {
        SubscriptionIssueOrderProductDetailFlag = subscriptionIssueOrderProductDetailFlag;
    }

    public boolean isRefillSubscription() {
        return isRefillSubscription;
    }

    public void setRefillSubscription(boolean refillSubscription) {
        isRefillSubscription = refillSubscription;
    }

    public boolean isPayNowSubscription() {
        return isPayNowSubscription;
    }

    public void setPayNowSubscription(boolean payNowSubscription) {
        isPayNowSubscription = payNowSubscription;
    }

    public Integer getSubscriptionCartId() {
        return SubscriptionCartId;
    }

    public void setSubscriptionCartId(Integer subscriptionCartId) {
        SubscriptionCartId = subscriptionCartId;
    }

    public boolean isSubscriptionEdit() {
        return isSubscriptionEdit;
    }

    public void setSubscriptionEdit(boolean subscriptionEdit) {
        isSubscriptionEdit = subscriptionEdit;
    }

    public String getSubscriptionThisIssue() {
        return SubscriptionThisIssue;
    }

    public void setSubscriptionThisIssue(String subscriptionThisIssue) {
        SubscriptionThisIssue = subscriptionThisIssue;
    }
}
