 /*
  *  "(c) NetmedsMarketPlace.  All rights reserved"
  */

 package com.nms.netmeds.base.model;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;
 import java.util.ArrayList;
 import java.util.Map;

 public class AlgoliaHitResults implements Serializable {
     @SerializedName("name")
     private String name;
     @SerializedName("pack_size_type_label")
     private String packSizeLabel;
     @SerializedName("url")
     private String url;
     @SerializedName("visibility_search")
     private Integer visibilitySearch;
     @SerializedName("visibility_catalog")
     private Integer visibilityCatalog;
     @SerializedName("drug_schedule")
     private Integer drugSchedule;
     @SerializedName("thumbnail_url")
     private String thumbnailUrl;
     @SerializedName("image_url")
     private String imageUrl;
     @SerializedName("manufacturer")
     private String manufacturer;
     @SerializedName("genericname")
     private String genericName;
     @SerializedName("drug_type")
     private String drugType;
     @SerializedName("sku")
     private String sku;
     @SerializedName("available_status")
     private String availableStatus;
     @SerializedName("price")
     private AlgoliaPrice algoliaPrice;
     @SerializedName("categories_without_path")
     private ArrayList<String> categoryWithOutPathList;
     @SerializedName("discount")
     private String discount;
     @SerializedName("cold_storage_status")
     private String isColdStorage;
     private String type;
     private String categoryName;
     @SerializedName("formulation_types")
     private String formulationTypes;
     @SerializedName("category_ids")
     private ArrayList<String> categoryIdList;
     @SerializedName("dosage")
     private String dosage;
     @SerializedName("max_sale_qty")
     private int maxSaleQty;
     @SerializedName("categories")
     private Map<String, ArrayList<Object>> categoryList;
     @SerializedName("manufacturer_id")
     private String manufacturerId;
     private int minimumQuantity;
     @SerializedName("objectID")
     private String objectId;

     public String isColdStorage() {
         return isColdStorage;
     }

     public String getCategoryName() {
         return categoryName;
     }

     public void setCategoryName(String categoryName) {
         this.categoryName = categoryName;
     }

     public String getType() {
         return type;
     }

     public void setType(String type) {
         this.type = type;
     }

     public ArrayList<String> getCategoryWithOutPathList() {
         return categoryWithOutPathList;
     }
     public void setCategoryWithOutPathList(ArrayList<String> categoryWithOutPathList) {
         this.categoryWithOutPathList = categoryWithOutPathList;
     }

     public AlgoliaPrice getAlgoliaPrice() {
         return algoliaPrice;
     }

     public void setAlgoliaPrice(AlgoliaPrice algoliaPrice) {
         this.algoliaPrice = algoliaPrice;
     }

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public String getUrl() {
         return url;
     }

     public void setUrl(String url) {
         this.url = url;
     }

     public Integer getVisibilitySearch() {
         return visibilitySearch;
     }

     public Integer getVisibilityCatalog() {
         return visibilityCatalog;
     }

     public String getThumbnailUrl() {
         return thumbnailUrl;
     }

     public String getImageUrl() {
         return imageUrl;
     }

     public String getManufacturer() {
         return manufacturer;
     }

     public String getDrugType() {
         return drugType;
     }


     public String getSku() {
         return sku;
     }

     public void setSku(String sku) {
         this.sku = sku;
     }

     public String getAvailableStatus() {
         return availableStatus;
     }

     public String getDiscount() {
         return discount;
     }

     public void setDiscount(String discount) {
         this.discount = discount;
     }

     public String getPackSizeLabel() {
         return packSizeLabel;
     }

     public Integer getDrugSchedule() {
         return drugSchedule;
     }

     public void setDrugSchedule(Integer drugSchedule) {
         this.drugSchedule = drugSchedule;
     }

     public String getFormulationTypes() {
         return formulationTypes;
     }

     public void setFormulationTypes(String formulationTypes) {
         this.formulationTypes = formulationTypes;
     }

     public ArrayList<String> getCategoryIdList() {
         return categoryIdList;
     }

     public void setCategoryIdList(ArrayList<String> categoryIdList) {
         this.categoryIdList = categoryIdList;
     }

     public String getDosage() {
         return dosage;
     }

     public void setDosage(String dosage) {
         this.dosage = dosage;
     }

     public Map<String, ArrayList<Object>> getCategoryList() {
         return categoryList;
     }

     public void setCategoryList(Map<String, ArrayList<Object>> categoryList) {
         this.categoryList = categoryList;
     }

     public int getMaxSaleQty() {
         return maxSaleQty;
     }

     public void setMaxSaleQty(int maxSaleQty) {
         this.maxSaleQty = maxSaleQty;
     }

     public String getGenericName() {
         return genericName;
     }

     public void setGenericName(String genericName) {
         this.genericName = genericName;
     }

     public String getManufacturerId() {
         return manufacturerId;
     }

     public void setManufacturerId(String manufacturerId) {
         this.manufacturerId = manufacturerId;
     }

     public int getMinimumQuantity() {
         return minimumQuantity;
     }

     public void setMinimumQuantity(int minimumQuantity) {
         this.minimumQuantity = minimumQuantity;
     }
     public String getObjectId() {
         return objectId;
     }

     public void setObjectId(String objectId) {
         this.objectId = objectId;
     }
 }
