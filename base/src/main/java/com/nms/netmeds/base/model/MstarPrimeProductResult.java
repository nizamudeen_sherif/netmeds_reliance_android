package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MstarPrimeProductResult {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private List<MstarPrimeProduct> primeProducts;

    private List<String> primeProductCodesList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MstarPrimeProduct> getPrimeProducts() {
        return primeProducts;
    }

    public void setPrimeProducts(List<MstarPrimeProduct> primeProducts) {
        this.primeProducts = primeProducts;
    }

    public List<String> getPrimeProductCodesList() {
        return primeProductCodesList;
    }

    public void setPrimeProductCodesList(List<String> primeProductCodesList) {
        this.primeProductCodesList = primeProductCodesList;
    }
}
