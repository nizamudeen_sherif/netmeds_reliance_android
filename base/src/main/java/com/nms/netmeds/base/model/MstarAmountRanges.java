package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class MstarAmountRanges {
    @SerializedName("min_mrp")
    private BigDecimal minMrp = BigDecimal.ZERO;

    @SerializedName("max_mrp")
    private BigDecimal maxMrp = BigDecimal.ZERO;

    @SerializedName("max_disc_pct")
    private BigDecimal maxDisc = BigDecimal.ZERO;

    @SerializedName("min_disc_pct")
    private BigDecimal minDisc = BigDecimal.ZERO;

    public BigDecimal getMinMrp() {
        return minMrp;
    }

    public void setMinMrp(BigDecimal minMrp) {
        this.minMrp = minMrp;
    }

    public BigDecimal getMaxMrp() {
        return maxMrp;
    }

    public void setMaxMrp(BigDecimal maxMrp) {
        this.maxMrp = maxMrp;
    }

    public BigDecimal getMaxDisc() {
        return maxDisc;
    }

    public void setMaxDisc(BigDecimal maxDisc) {
        this.maxDisc = maxDisc;
    }

    public BigDecimal getMinDisc() {
        return minDisc;
    }

    public void setMinDisc(BigDecimal minDisc) {
        this.minDisc = minDisc;
    }
}
