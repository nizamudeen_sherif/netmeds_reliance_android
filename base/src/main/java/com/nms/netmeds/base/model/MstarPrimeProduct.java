package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class MstarPrimeProduct {

    @SerializedName("name")
    private String name;
    @SerializedName("product_code")
    private int productCode;
    @SerializedName("duration")
    private int duration;
    @SerializedName("recommended")
    private boolean recommended;
    @SerializedName("mrp")
    private BigDecimal mrp = BigDecimal.ZERO;
    @SerializedName("selling_price")
    private BigDecimal sellingPrice = BigDecimal.ZERO;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
}
