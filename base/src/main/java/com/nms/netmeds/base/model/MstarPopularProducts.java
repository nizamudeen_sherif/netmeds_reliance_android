package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarPopularProducts {

    @SerializedName("position")
    private int position;
    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("id")
    private int id = 0;
    @SerializedName("type")
    private String type;
    @SerializedName("list")
    private List<MStarProductDetails> productsList;
    @SerializedName("tagline")
    private String tagline;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public List<MStarProductDetails> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<MStarProductDetails> productsList) {
        this.productsList = productsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }
}
