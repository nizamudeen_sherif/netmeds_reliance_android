/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class AlgoliaPrice {
    @SerializedName("INR")
    private Price price;

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}
