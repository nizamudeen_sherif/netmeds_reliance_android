package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CancelOrderReason {
    @SerializedName("key")
    private Integer key;
    @SerializedName("value")
    private String value;
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
