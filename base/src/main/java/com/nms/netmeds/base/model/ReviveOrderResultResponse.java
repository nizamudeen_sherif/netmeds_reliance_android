package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ReviveOrderResultResponse {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
