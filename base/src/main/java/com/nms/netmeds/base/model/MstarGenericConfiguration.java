package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarGenericConfiguration {

    @SerializedName("banner")
    private List<GenericBanner> genericBannerList;
    @SerializedName("content")
    private GenericContent genericContent;
    @SerializedName("saveUpTo")
    private String saveUpTo;
    @SerializedName("genericMedicinePrice")
    private String genericMedicinePrice;
    @SerializedName("brandMedicinePrice")
    private String brandMedicinePrice;
    @SerializedName("genericWorks")
    private String genericWorks;


    public List<GenericBanner> getGenericBannerList() {
        return genericBannerList;
    }

    public void setGenericBannerList(List<GenericBanner> genericBannerList) {
        this.genericBannerList = genericBannerList;
    }

    public GenericContent getGenericContent() {
        return genericContent;
    }

    public void setGenericContent(GenericContent genericContent) {
        this.genericContent = genericContent;
    }

    public String getSaveUpTo() {
        return saveUpTo;
    }

    public void setSaveUpTo(String saveUpTo) {
        this.saveUpTo = saveUpTo;
    }

    public String getGenericMedicinePrice() {
        return genericMedicinePrice;
    }

    public void setGenericMedicinePrice(String genericMedicinePrice) {
        this.genericMedicinePrice = genericMedicinePrice;
    }

    public String getBrandMedicinePrice() {
        return brandMedicinePrice;
    }

    public void setBrandMedicinePrice(String brandMedicinePrice) {
        this.brandMedicinePrice = brandMedicinePrice;
    }

    public String getGenericWorks() {
        return genericWorks;
    }

    public void setGenericWorks(String genericWorks) {
        this.genericWorks = genericWorks;
    }
}
