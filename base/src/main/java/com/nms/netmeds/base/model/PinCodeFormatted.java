package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PinCodeFormatted {
    @SerializedName("format1")
    private String format1;
    @SerializedName("format2")
    private String format2;
    @SerializedName("format-android1")
    private String formatAndroid1;
    @SerializedName("format-ios1")
    private String formatIos1;

    public String getFormat1() {
        return format1;
    }

    public void setFormat1(String format1) {
        this.format1 = format1;
    }

    public String getFormat2() {
        return format2;
    }

    public void setFormat2(String format2) {
        this.format2 = format2;
    }

    public String getFormatAndroid1() {
        return formatAndroid1;
    }

    public void setFormatAndroid1(String formatAndroid1) {
        this.formatAndroid1 = formatAndroid1;
    }

    public String getFormatIos1() {
        return formatIos1;
    }

    public void setFormatIos1(String formatIos1) {
        this.formatIos1 = formatIos1;
    }
}
