package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class M2DeliveryCharges {
    @SerializedName("freeShipEligibilityValue")
    private double freeShipEligibilityValue;
    @SerializedName("cutOffValue")
    private double cutOffValue;
    @SerializedName("minShippingCharges")
    private double minShippingCharges;
    @SerializedName("maxShippingCharges")
    private double maxShippingCharges;
    @SerializedName("mrpContent")
    private String mrpContent;
    @SerializedName("deliveryContent")
    private String deliveryContent;

    public double getFreeShipEligibilityValue() {
        return freeShipEligibilityValue;
    }

    public void setFreeShipEligibilityValue(double freeShipEligibilityValue) {
        this.freeShipEligibilityValue = freeShipEligibilityValue;
    }

    public double getCutOffValue() {
        return cutOffValue;
    }

    public void setCutOffValue(double cutOffValue) {
        this.cutOffValue = cutOffValue;
    }

    public double getMinShippingCharges() {
        return minShippingCharges;
    }

    public void setMinShippingCharges(double minShippingCharges) {
        this.minShippingCharges = minShippingCharges;
    }

    public double getMaxShippingCharges() {
        return maxShippingCharges;
    }

    public void setMaxShippingCharges(double maxShippingCharges) {
        this.maxShippingCharges = maxShippingCharges;
    }

    public String getMrpContent() {
        return mrpContent;
    }

    public void setMrpContent(String mrpContent) {
        this.mrpContent = mrpContent;
    }

    public String getDeliveryContent() {
        return deliveryContent;
    }

    public void setDeliveryContent(String deliveryContent) {
        this.deliveryContent = deliveryContent;
    }
}
