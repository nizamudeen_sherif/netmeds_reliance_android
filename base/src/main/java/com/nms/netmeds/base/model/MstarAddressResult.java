package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarAddressResult {
    @SerializedName("address")
    private MStarAddressModel address;

    public MStarAddressModel getAddress() {
        return address;
    }

    public void setAddress(MStarAddressModel address) {
        this.address = address;
    }
}
