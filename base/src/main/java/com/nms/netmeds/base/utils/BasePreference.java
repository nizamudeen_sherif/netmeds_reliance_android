package com.nms.netmeds.base.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class BasePreference {

    public final static String PREF_KEY_LOCALE_ENGLISH = "en";
    public final static String PREF_KEY_LOCALE_HINDI = "hi";
    public final static String PREF_KEY_LOCALE_TAMIL = "ta";
    public final static String PREF_KEY_JUST_DOC_USER_DETAIL = "just_doc_user_detail";
    public final static String PREF_KEY_USER_DETAILS = "user_details";
    public final static String PREF_KEY_CART_ID = "cart_id";
    public final static String PREF_KEY_CUSTOMER_DETAILS = "customer_details";
    public final static String PREF_KEY_CART_COUNT = "cart_count";
    public final static String PREF_KEY_PIN_CODE = "pin_code";
    public static final String PREF_KEY_JUST_DOC_CONFIG = "JUST_DOC_CONFIG";
    public static final String PREF_KEY_PAYMENT_DETAIL = "PREF_KEY_PAYMENT_DETAIL";
    public static final String PREF_KEY_CONFIGURATION = "PREF_KEY_CONFIGURATION";
    public static final String PREF_KEY_SUB_DELIVERY_INTERVAL = "PREF_KEY_SUB_DELIVERY_INTERVAL";
    private final static String PREF_NAME = "netmeds_pref";
    private final static String PREF_KEY_AUTH_TOKEN = "magento_auth_token";
    private final static String PREF_KEY_LOGAN_SESSION = "logan_session";
    private static final String PREF_KEY_OUT_OF_STOCK_PRODUCTS = "OutOfStockProduct";
    private static final String PREF_KEY_FCM_TOKEN = "fcm_token";
    private static final String CLEAR_PREFERENCE = "Clear Preference";
    private static final String PREF_KEY_FCM_TOKEN_REFRESH = "fcm_token_refresh";
    private static final String PREF_KEY_IS_GUEST_USER = "PREF_IS_GUEST_USER";
    public final static String PREF_KEY_GUEST_CART_ID = "PREF_KEY_GUEST_CART_ID";
    public final static String PREF_KEY_GUEST_SESSION_VALID_TIME = "PREF_KEY_GUEST_SESSION_VALID_TIME";
    public final static String PREF_KEY_GUEST_CART_SHIPPING_METHOD_CODE = "PREF_KEY_GUEST_CART_SHIPPING_METHOD_CODE";
    public final static String PREF_KEY_GUEST_CART_SHIPPING_CARRIER_CODE = "PREF_KEY_GUEST_CART_SHIPPING_CARRIER_CODE";
    private static final String PREF_KEY_DIAGNOSTIC_PIN_CODE = "diagnostic_pin_code";
    public final static String PREF_KEY_DIAGNOSTIC_USER_DETAIL = "diagnostic_user_detail";
    private final static String PREF_KEY_CONSULTATION_RATING = "pref_key_consultation_rating";
    private static final String PREF_KEY_DIAGNOSTIC_CITY = "diagnostic_city";
    private static final String PREF_KEY_LAB_SELECT_PIN_CODE = "key_pin_lab_code";
    private static final String PREF_KEY_PATIENT_AGE = "key_patient_age";
    private static final String PREF_KEY_PATIENT_GENDER = "key_patient_gender";
    private static final String PREF_KEY_DEFAULT_ADDRESS = "key_default_address";
    private static final String PREF_KEY_CHANGES_DONE = "key_changes_done";
    private static final String PREF_KEY_CHANGES_DIAGNOSTIC_HOME = "key_changes_diagnostic_home";
    private static final String PREF_KEY_DIAGNOSTIC_FIRST_ORDER = "key_diagnostic_first_order";
    private final static String PREF_PRIME_RESPONSE = "prime_response";
    private final static String PREF_KEY_ALGOLIA_QUERY_ID = "PREF_KEY_ALGOLIA_QUERY_ID";
    private final static String PREF_KEY_IS_CART_SUBSTITUTED = "PREF_KEY_IS_CART_SUBSTITUTED";
    private final static String PREF_KEY_ALTERNATE_AND_ORIGINAL_CART_PRODUCT = "PREF_KEY_ALTERNATE_AND_ORIGINAL_CART_PRODUCT";
    private final static String PREF_KEY_DEFAULT_ADDRESS_ID = "PREF_KEY_DEFAULT_ADDRESS_ID";
    private final static String PREF_KEY_DIA_FIRST_TIME_LOGIN = "PREF_KEY_DIA_FIRST_TIME_LOGIN";

    //MORNING STAR
    private static final String MORNING_STAR_SESSION_ID = "MORNING_STAR_SESSION_ID";
    private static final String MORNING_STAR_CUSTOMER_ID = "MORNING_STAR_CUSTOMER_ID";
    private static final String MORNING_STAR_HEADER_MAP = "MORNING_STAR_HEADER_MAP";
    private static final String MORNING_STAR_HEADER_MAP_WITH_FINGERPRINT = "MORNING_STAR_HEADER_MAP_WITH_FINGERPRINT";
    private static final String MORNING_STAR_CART_ID = "MORNING_STAR_CART_ID";
    private static final String MORNING_STAR_CART_SHIPPING_ADDRESS_ID = "MORNING_STAR_CART_SHIPPING_ADDRESS_ID";
    private static final String MORNING_STAR_CART_BILLING_ADDRESS_ID = "MORNING_STAR_CART_BILLING_ADDRESS_ID";
    private static final String MORNING_STAR_EDIT_ADD_ADDRESS_CALLBACK = "MORNING_STAR_EDIT_ADD_ADDRESS_CALLBACK";
    private static final String MORNING_STAR_M1_CART_STATUS = "MORNING_STAR_M1_CART_STATUS";
    private static final String MORNING_STAR_M2_CART_STATUS = "MORNING_STAR_M2_CART_STATUS";
    private static final String MORNING_STAR_CONFIG_URL_PATH = "MORNING_STAR_CONFIG_URL_PATH";
    private static final String MSTAR_METHOD_2_CART_ID = "MSTAR_METHOD_2_CART_ID";
    private static final String MSTAR_PRIME_MEMBERSHIP = "MSTAR_PRIME_MEMBERSHIP";
    private static final String MSAR_M2_CART_COUNT = "MSAR_M2_CART_COUNT";
    private static final String MSTAR_IS_APP_START_FIRST = "MSTAR_IS_APP_START_FIRST";
    private static final String MSTAR_GOOGLE_ADVERTISING_ID = "MSTAR_GOOGLE_ADVERTISING_ID";
    private static final String GENERIC_PRODUCUT_MAP = "GENERIC_PRODUCUT_MAP";
    private static final String GENERIC_BRAND_PRODUCUT_MAP = "GENERIC_BRAND_PRODUCUT_MAP";
    private static final String MSTAR_CLEAR_PREVIOUS_ACTIVITIES = "MSTAR_CLEAR_PREVIOUS_ACTIVITIES";
    private static final String MSTAR_DIAGNOSTIC_CART_ID = "MSTAR_DIAGNOSTIC_CART_ID";

    private static BasePreference basePreference;
    private SharedPreferences mSharedPreferences;

    private BasePreference(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static BasePreference getInstance(Context context) {
        if (basePreference == null) {
            basePreference = new BasePreference(context);
        }
        return basePreference;
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(PREF_KEY_AUTH_TOKEN, null);
    }

    public String getLoganSession() {
        return mSharedPreferences.getString(PREF_KEY_LOGAN_SESSION, null);
    }

    public void setLoganSession(final String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_LOGAN_SESSION, token);
        editor.apply();
    }

    public String getCartId() {
        return mSharedPreferences.getString(PREF_KEY_CART_ID, null);
    }

    public void setCartId(final String quoteId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_CART_ID, quoteId);
        editor.apply();
    }

    public String getJustDocUserResponse() {
        return mSharedPreferences.getString(PREF_KEY_JUST_DOC_USER_DETAIL, null);
    }

    public void setJustDocUserResponse(String detail) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_JUST_DOC_USER_DETAIL, detail);
        editor.apply();
    }

    public String getJustDocConfig() {
        return mSharedPreferences.getString(PREF_KEY_JUST_DOC_CONFIG, null);
    }

    public void setJustDocConfig(String config) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_JUST_DOC_CONFIG, config);
        editor.apply();
    }

    public String getUserDetails() {
        return mSharedPreferences.getString(PREF_KEY_USER_DETAILS, null);
    }

    public void setUserDetails(String userDetail) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_USER_DETAILS, userDetail);
        editor.apply();
    }

    public String getCustomerDetails() {
        return mSharedPreferences.getString(PREF_KEY_CUSTOMER_DETAILS, null);
    }

    public void setCustomerDetails(String userDetail) {
        setDiagDefaultAddressId(new Gson().fromJson(userDetail, MStarCustomerDetails.class).getPreferredBillingAddress());
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_CUSTOMER_DETAILS, userDetail);
        editor.apply();
    }

    public int getDiagDefaultAddressId() {
        return mSharedPreferences.getInt(PREF_KEY_DEFAULT_ADDRESS_ID, 0);
    }

    public void setDiagDefaultAddressId(final int addressId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(PREF_KEY_DEFAULT_ADDRESS_ID, addressId);
        editor.apply();
    }

    public int getCartCount() {
        return mSharedPreferences.getInt(PREF_KEY_CART_COUNT, 0);
    }

    public void setCartCount(final int quoteId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(PREF_KEY_CART_COUNT, quoteId);
        editor.apply();
    }

    public int getM2CartCount() {
        return mSharedPreferences.getInt(MSAR_M2_CART_COUNT, 0);
    }

    public void setM2CartCount(final int quoteId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(MSAR_M2_CART_COUNT, quoteId);
        editor.apply();
    }


    public String getPinCode() {
        return mSharedPreferences.getString(PREF_KEY_PIN_CODE, "");
    }

    public void setPinCode(final String quoteId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_PIN_CODE, quoteId);
        editor.apply();
    }

    public String getPaymentCredentials() {
        return mSharedPreferences.getString(PREF_KEY_PAYMENT_DETAIL, "");
    }

    public void setPaymentCredentials(final String paymentCredentials) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_PAYMENT_DETAIL, paymentCredentials);
        editor.apply();
    }

    public void setOutOfStockProductMessage(String responseData) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_OUT_OF_STOCK_PRODUCTS, responseData);
        editor.apply();
    }

    public String getConfiguration() {
        return mSharedPreferences.getString(PREF_KEY_CONFIGURATION, "");
    }

    public void setConfiguration(final String data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_CONFIGURATION, data);
        editor.apply();
    }

    public int getDeliveryIntervalPreference() {
        return mSharedPreferences.getInt(PREF_KEY_SUB_DELIVERY_INTERVAL, 0);
    }

    public void setDeliveryIntervalPreference(int interval) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(PREF_KEY_SUB_DELIVERY_INTERVAL, interval);
        editor.apply();
    }

    public String getFcmToken() {
        return mSharedPreferences.getString(PREF_KEY_FCM_TOKEN, "");
    }

    public void setFcmToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_FCM_TOKEN, token);
        editor.apply();
    }

    public boolean isFcmTokenRefreshed() {
        return mSharedPreferences.getBoolean(PREF_KEY_FCM_TOKEN_REFRESH, false);
    }

    public void setFcmTokenRefresh(boolean isTokenRefreshed) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_FCM_TOKEN_REFRESH, isTokenRefreshed);
        editor.apply();
    }

    public boolean getClearPreference() {
        return mSharedPreferences.getBoolean(CLEAR_PREFERENCE, false);
    }

    public void setClearPreference(boolean signIn) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(CLEAR_PREFERENCE, signIn);
        editor.apply();
    }

    public boolean isGuestCart() {
        return mSharedPreferences.getBoolean(PREF_KEY_IS_GUEST_USER, false);
    }

    public void setGuestCart(boolean isGuestCart) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_IS_GUEST_USER, isGuestCart);
        editor.apply();
    }

    public void clear() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(PREF_KEY_AUTH_TOKEN);
        editor.remove(PREF_KEY_LOGAN_SESSION);
        editor.remove(PREF_KEY_CUSTOMER_DETAILS);
        editor.remove(PREF_KEY_CART_ID);
        editor.remove(PREF_KEY_JUST_DOC_USER_DETAIL);
        editor.remove(PREF_KEY_JUST_DOC_CONFIG);
        editor.remove(PREF_KEY_PAYMENT_DETAIL);
        editor.remove(PREF_KEY_GUEST_CART_SHIPPING_CARRIER_CODE);
        editor.remove(PREF_KEY_GUEST_CART_SHIPPING_METHOD_CODE);
        editor.remove(PREF_KEY_IS_GUEST_USER);
        editor.remove(PREF_KEY_GUEST_CART_ID);
        editor.remove(PREF_KEY_GUEST_SESSION_VALID_TIME);
        editor.remove(PREF_KEY_PIN_CODE);
        editor.remove(PREF_KEY_DIAGNOSTIC_PIN_CODE);
        editor.remove(PREF_KEY_DIAGNOSTIC_USER_DETAIL);
        editor.remove(PREF_KEY_USER_DETAILS);
        editor.remove(PREF_KEY_CONSULTATION_RATING);
        editor.remove(PREF_KEY_DIAGNOSTIC_CITY);
        editor.remove(PREF_KEY_LAB_SELECT_PIN_CODE);
        editor.remove(PREF_KEY_PATIENT_GENDER);
        editor.remove(PREF_KEY_PATIENT_AGE);
        editor.remove(PREF_KEY_DEFAULT_ADDRESS);
        editor.remove(PREF_KEY_CHANGES_DONE);
        editor.remove(PREF_KEY_CHANGES_DIAGNOSTIC_HOME);
        editor.remove(PREF_PRIME_RESPONSE);
        editor.remove(PREF_KEY_ALGOLIA_QUERY_ID);
        editor.remove(PREF_KEY_DEFAULT_ADDRESS_ID);
        editor.remove(PREF_KEY_DIA_FIRST_TIME_LOGIN);
        //MORNING STAR
        editor.remove(MORNING_STAR_CART_ID);
        editor.remove(MORNING_STAR_M1_CART_STATUS);
        editor.remove(MORNING_STAR_M2_CART_STATUS);
        editor.remove(MORNING_STAR_CART_SHIPPING_ADDRESS_ID);
        editor.remove(MORNING_STAR_CART_BILLING_ADDRESS_ID);
        editor.remove(MORNING_STAR_SESSION_ID);
        editor.remove(MORNING_STAR_HEADER_MAP);
        editor.remove(MORNING_STAR_EDIT_ADD_ADDRESS_CALLBACK);
        editor.remove(MSTAR_METHOD_2_CART_ID);
        editor.remove(MORNING_STAR_CUSTOMER_ID);
        editor.remove(MSAR_M2_CART_COUNT);
        editor.remove(PREF_KEY_DIAGNOSTIC_FIRST_ORDER);
        editor.remove(GENERIC_PRODUCUT_MAP);
        editor.remove(GENERIC_BRAND_PRODUCUT_MAP);
        editor.remove(MSTAR_CLEAR_PREVIOUS_ACTIVITIES);
        editor.remove(MSTAR_DIAGNOSTIC_CART_ID);
        editor.apply();
    }

    public void clearM2CartId() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(MSTAR_METHOD_2_CART_ID);
        editor.remove(PREF_KEY_ALGOLIA_QUERY_ID);

        //Alternate Cart.
        editor.remove(PREF_KEY_IS_CART_SUBSTITUTED);
        editor.remove(PREF_KEY_ALTERNATE_AND_ORIGINAL_CART_PRODUCT);

        editor.apply();
    }

    public void setDiagnosticPinCode(String pinCode) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_DIAGNOSTIC_PIN_CODE, pinCode);
        editor.apply();
    }

    public String getDiagnosticPinCode() {
        return mSharedPreferences.getString(PREF_KEY_DIAGNOSTIC_PIN_CODE, "");
    }

    public void setLabPinCode(String pinCode) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_LAB_SELECT_PIN_CODE, pinCode);
        editor.apply();
    }

    public String getLabPinCode() {
        return mSharedPreferences.getString(PREF_KEY_LAB_SELECT_PIN_CODE, "");
    }

    public void setDiagnosticCity(String city) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_DIAGNOSTIC_CITY, city);
        editor.apply();
    }

    public String getDiagnosticCity() {
        return mSharedPreferences.getString(PREF_KEY_DIAGNOSTIC_CITY, "");
    }

    public String getDiagnosticLoginResponse() {
        return mSharedPreferences.getString(PREF_KEY_DIAGNOSTIC_USER_DETAIL, null);
    }

    public void setDiagnosticLoinResponse(String detail) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_DIAGNOSTIC_USER_DETAIL, detail);
        editor.apply();
    }

    public void setRatingMap(String consultationId, String rating) {
        HashMap<String, String> ratingMap = new HashMap<>();
        if (getRatingMap().equals("null")) {
            if (consultationId != null && (!TextUtils.isEmpty(consultationId))) {
                ratingMap.put(consultationId, rating);
            }
        } else {
            String existingMap = getRatingMap();
            ratingMap = new Gson().fromJson(existingMap, new TypeToken<HashMap<String, String>>() {
            }.getType());
            if (consultationId != null && (!TextUtils.isEmpty(consultationId))) {
                ratingMap.put(consultationId, rating);
            }

        }
        Gson gson = new Gson();
        String ratingMapString = gson.toJson(ratingMap);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_CONSULTATION_RATING, ratingMapString);
        editor.apply();
    }

    public String getRating(String consultationId) {
        HashMap<String, String> ratingMap;
        if (getRatingMap().equals("null"))
            return "null";
        String existingMap = getRatingMap();
        ratingMap = new Gson().fromJson(existingMap, new TypeToken<HashMap<String, String>>() {
        }.getType());
        if (ratingMap.containsKey(consultationId))
            return ratingMap.get(consultationId);
        return "null";
    }

    public String getRatingMap() {
        return mSharedPreferences.getString(PREF_KEY_CONSULTATION_RATING, "null");
    }

    public String getPatientAge() {
        return mSharedPreferences.getString(PREF_KEY_PATIENT_AGE, "");
    }

    public String getPatientGender() {
        return mSharedPreferences.getString(PREF_KEY_PATIENT_GENDER, "");
    }

    public void setPatientAge(String city) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_PATIENT_AGE, city);
        editor.apply();
    }

    public void setPatientGender(String city) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_PATIENT_GENDER, city);
        editor.apply();
    }

    public void setPrimeUser(String response) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_PRIME_RESPONSE, response);
        editor.apply();
    }

    public String getPrefPrimeResponse() {
        return mSharedPreferences.getString(PREF_PRIME_RESPONSE, "");
    }

    public void setMstarPrimeMembership(String response) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(MSTAR_PRIME_MEMBERSHIP, response);
        editor.apply();
    }

    public String getMstarPrimeMembership() {
        return mSharedPreferences.getString(MSTAR_PRIME_MEMBERSHIP, "");
    }

    public void setMStarSessionId(String sessionId, String userId) {
        setMstarBasicHeaderMap(sessionId, userId);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(MORNING_STAR_SESSION_ID, sessionId);
        editor.apply();
    }

    public String getMStarSessionId() {
        return mSharedPreferences.getString(MORNING_STAR_SESSION_ID, "");
    }

    public String getMstarCustomerId() {
        return mSharedPreferences.getString(MORNING_STAR_CUSTOMER_ID, "");
    }

    public void setMstarCustomerId(String customerId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(MORNING_STAR_CUSTOMER_ID, customerId);
        editor.apply();
    }

    public void setMstarBasicHeaderMap(String sessionId, String userId) {
        Map<String, String> mstarBasicHeaderMap = new HashMap<>();
        mstarBasicHeaderMap.put(AppConstant.MSTAR_AUTH_TOKEN, sessionId);
        mstarBasicHeaderMap.put(AppConstant.MSTAR_API_HEADER_USER_ID, userId);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(MORNING_STAR_HEADER_MAP, new Gson().toJson(mstarBasicHeaderMap));
        editor.apply();
    }

    public Map<String, String> getMstarBasicHeaderMap() {
        Type type = new TypeToken<HashMap<String, String>>() {
        }.getType();
        HashMap<String, String> mstarBasicHeaderMap = new Gson().fromJson(mSharedPreferences.getString(MORNING_STAR_HEADER_MAP, ""), type);
        return mstarBasicHeaderMap;
    }

    public void setMStarCartId(int cart_id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(MORNING_STAR_CART_ID, cart_id);
        editor.apply();
    }

    public int getMStarCartId() {
        return mSharedPreferences.getInt(MORNING_STAR_CART_ID, 0);
    }

    public void setMstarMethod2CartId(int cart_id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(MSTAR_METHOD_2_CART_ID, cart_id);
        editor.apply();
    }

    public int getMstarMethod2CartId() {
        return mSharedPreferences.getInt(MSTAR_METHOD_2_CART_ID, 0);
    }

    public void setPreviousCartShippingAddressId(int shippingAddressId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(MORNING_STAR_CART_SHIPPING_ADDRESS_ID, shippingAddressId);
        editor.apply();
    }

    public int getPreviousCartShippingAddressId() {
        return mSharedPreferences.getInt(MORNING_STAR_CART_SHIPPING_ADDRESS_ID, -1);
    }

    public void setPreviousCartBillingAddressId(int billingAddressId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(MORNING_STAR_CART_BILLING_ADDRESS_ID, billingAddressId);
        editor.apply();
    }

    public int getPreviousCartBillingAddressId() {
        return mSharedPreferences.getInt(MORNING_STAR_CART_BILLING_ADDRESS_ID, -1);
    }

    public void setAddOrEditAddressCallback(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MORNING_STAR_EDIT_ADD_ADDRESS_CALLBACK, value);
        editor.apply();

    }

    public boolean getAddOrEditAddressCallback() {
        return mSharedPreferences.getBoolean(MORNING_STAR_EDIT_ADD_ADDRESS_CALLBACK, false);
    }

    public void setMStarM1CartIsOpen(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MORNING_STAR_M1_CART_STATUS, value);
        editor.apply();
    }

    public boolean isMStarM1CartOpen() {
        return mSharedPreferences.getBoolean(MORNING_STAR_M1_CART_STATUS, true);
    }

    public void setMStarM2CartIsOpen(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MORNING_STAR_M2_CART_STATUS, value);
        editor.apply();
    }

    public boolean isMStarM2CartOpen() {
        return mSharedPreferences.getBoolean(MORNING_STAR_M2_CART_STATUS, true);
    }

    public void setConfigURLPath(String configURLPath) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(MORNING_STAR_CONFIG_URL_PATH, configURLPath);
        editor.apply();
    }

    public String getConfigURLPath() {
        return mSharedPreferences.getString(MORNING_STAR_CONFIG_URL_PATH, "");
    }

    public boolean isMstarSessionIdAvailable() {
        return !TextUtils.isEmpty(getMStarSessionId());
    }

    public void setAlgoliaQueryId(final String queryId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_ALGOLIA_QUERY_ID, queryId);
        editor.apply();
    }

    public String getAlgoliaQueryId() {
        return mSharedPreferences.getString(PREF_KEY_ALGOLIA_QUERY_ID, "");
    }

    public boolean isCartSubstituted() {
        return mSharedPreferences.getBoolean(PREF_KEY_IS_CART_SUBSTITUTED, false);
    }

    public void setCartSubstituted(boolean cartSubstituted) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_IS_CART_SUBSTITUTED, cartSubstituted);
        editor.apply();
    }

    public void setAlternateAndOriginalCartProductDetails(String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_ALTERNATE_AND_ORIGINAL_CART_PRODUCT, value);
        editor.apply();
    }

    public String getAlternateAndOriginalCartProductDetails() {
        return mSharedPreferences.getString(PREF_KEY_ALTERNATE_AND_ORIGINAL_CART_PRODUCT, "");
    }

    public void saveDefaultAddress(String city) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_DEFAULT_ADDRESS, city);
        editor.apply();
    }

    public String getDefaultAddress() {
        return mSharedPreferences.getString(PREF_KEY_DEFAULT_ADDRESS, null);
    }

    public void setChangesDone(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_CHANGES_DONE, value);
        editor.apply();
    }

    public Boolean getChangesDone() {
        return mSharedPreferences.getBoolean(PREF_KEY_CHANGES_DONE, false);
    }

    public void setChangeDiagnosticHome(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_CHANGES_DIAGNOSTIC_HOME, value);
        editor.apply();
    }

    public Boolean getChangeDiagnosticHome() {
        return mSharedPreferences.getBoolean(PREF_KEY_CHANGES_DIAGNOSTIC_HOME, false);
    }

    public void setDiagnosticFirstOrder(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_DIAGNOSTIC_FIRST_ORDER, value);
        editor.apply();
    }

    public Boolean isDiagnosticFirstOrder() {
        return mSharedPreferences.getBoolean(PREF_KEY_DIAGNOSTIC_FIRST_ORDER, false);
    }

    public String getGuestSessionValidTime() {
        return mSharedPreferences.getString(PREF_KEY_GUEST_SESSION_VALID_TIME, "");
    }

    public void setGuestSessionValidTime(String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREF_KEY_GUEST_SESSION_VALID_TIME, value);
        editor.apply();
    }

    public boolean getIsAppStartFirst() {
        return mSharedPreferences.getBoolean(MSTAR_IS_APP_START_FIRST, false);
    }

    public void setAppStartFirst(boolean appStartFirst) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MSTAR_IS_APP_START_FIRST, appStartFirst);
        editor.apply();
    }

    public String getMstarGoogleAdvertisingId() {
        return mSharedPreferences.getString(MSTAR_GOOGLE_ADVERTISING_ID, "");
    }

    public void setMstarGoogleAdvertisingId(String googleAdvertisingId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(MSTAR_GOOGLE_ADVERTISING_ID, googleAdvertisingId);
        editor.apply();
    }

    public boolean isDiaFirstTimeLogin() {
        return mSharedPreferences.getBoolean(PREF_KEY_DIA_FIRST_TIME_LOGIN, false);
    }

    public void setDiaFirstTimeLogin(boolean fromDiagnosticAsGuest) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(PREF_KEY_DIA_FIRST_TIME_LOGIN, fromDiagnosticAsGuest);
        editor.apply();
    }

    public void setGenericProductMap(Map<String, MStarProductDetails> genericProductMap) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(GENERIC_PRODUCUT_MAP, new Gson().toJson(genericProductMap));
        editor.apply();
    }

    public Map<String, MStarProductDetails> getGenericProductMap() {
        Type type = new TypeToken<HashMap<String, MStarProductDetails>>() {
        }.getType();
        HashMap<String, MStarProductDetails> genericProductMap = new Gson().fromJson(mSharedPreferences.getString(GENERIC_PRODUCUT_MAP, ""), type);
        return genericProductMap != null ? genericProductMap : new HashMap<String, MStarProductDetails>();
    }

    public void setGenericBrandProductMap(Map<String, MStarProductDetails> genericBrandProductMap) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(GENERIC_BRAND_PRODUCUT_MAP, new Gson().toJson(genericBrandProductMap));
        editor.apply();
    }

    public Map<String, MStarProductDetails> getGenericBrandProductMap() {
        Type type = new TypeToken<HashMap<String, MStarProductDetails>>() {
        }.getType();
        HashMap<String, MStarProductDetails> genericBrandProductMap = new Gson().fromJson(mSharedPreferences.getString(GENERIC_BRAND_PRODUCUT_MAP, ""), type);
        return genericBrandProductMap != null ? genericBrandProductMap : new HashMap<String, MStarProductDetails>();
    }

    public boolean isPreviousActivitiesCleared() {
        return mSharedPreferences.getBoolean(MSTAR_CLEAR_PREVIOUS_ACTIVITIES, false);
    }

    public void setPreviousActivitiesCleared(boolean appStartFirst) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MSTAR_CLEAR_PREVIOUS_ACTIVITIES, appStartFirst);
        editor.apply();
    }

    public int getMstarDiagnosticCartId() {
        return mSharedPreferences.getInt(MSTAR_DIAGNOSTIC_CART_ID, 0);
    }

    public void setMstarDiagnosticCartId(int mstarDiagnosticCartId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(MSTAR_DIAGNOSTIC_CART_ID, mstarDiagnosticCartId);
        editor.apply();
    }
}