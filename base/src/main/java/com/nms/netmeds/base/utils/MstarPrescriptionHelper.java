package com.nms.netmeds.base.utils;

import com.nms.netmeds.base.model.MStarUploadPrescription;

import java.util.ArrayList;
import java.util.List;

public class MstarPrescriptionHelper {

    ArrayList<MStarUploadPrescription> MStarUploadPrescriptionList = new ArrayList<>();
    List<String> selectedPastRxIds = new ArrayList<>();

    private static MstarPrescriptionHelper helper;

    public static MstarPrescriptionHelper getInstance(){
        if(helper== null){
            helper = new MstarPrescriptionHelper();
        }
        return helper;
    }

    public ArrayList<MStarUploadPrescription> getMStarUploadPrescriptionList() {
        return MStarUploadPrescriptionList;
    }

    public void setMStarUploadPrescriptionList(ArrayList<MStarUploadPrescription> MStarUploadPrescriptionList) {
        this.MStarUploadPrescriptionList = MStarUploadPrescriptionList;
    }

    public static MstarPrescriptionHelper getHelper() {
        return helper;
    }

    public static void setHelper(MstarPrescriptionHelper helper) {
        MstarPrescriptionHelper.helper = helper;
    }

    public List<String> getSelectedPastRxIds() {
        return selectedPastRxIds;
    }

    public void setSelectedPastRxIds(List<String> selectedPastRxIds) {
        this.selectedPastRxIds = selectedPastRxIds;
    }
}
