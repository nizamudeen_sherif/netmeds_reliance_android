package com.nms.netmeds.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M3SubscriptionLogRequest extends BaseRequest {
    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("paymentmethod")
    @Expose
    private String paymentmethod;
    @SerializedName("request")
    @Expose
    private String request;
    @SerializedName("response")
    @Expose
    private String response;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
