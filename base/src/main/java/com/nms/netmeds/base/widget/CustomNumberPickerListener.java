package com.nms.netmeds.base.widget;

public interface CustomNumberPickerListener {
    void onHorizontalNumberPickerChanged(CustomNumberPicker customNumberPicker, int value);
}
