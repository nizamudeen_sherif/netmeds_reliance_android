package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

public class DigitizedRxResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private DigitizedRxResult result;

    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel reason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DigitizedRxResult getResult() {
        return result;
    }

    public void setResult(DigitizedRxResult result) {
        this.result = result;
    }

    public MstarBasicResponseFailureReasonTemplateModel getReason() {
        return reason;
    }

    public void setReason(MstarBasicResponseFailureReasonTemplateModel reason) {
        this.reason = reason;
    }
}
