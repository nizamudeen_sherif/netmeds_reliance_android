/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

public class AlgoliaResponse extends BaseResponse {
    @SerializedName("hits")
    private ArrayList<AlgoliaHitResults> algoliaHitList = null;
    @SerializedName("nbHits")
    private Integer nbHits;
    @SerializedName("page")
    private Integer page;
    @SerializedName("nbPages")
    private Integer nbPages;
    @SerializedName("hitsPerPage")
    private Integer hitsPerPage;
    @SerializedName("processingTimeMS")
    private Integer processingTimeMS;
    @SerializedName("exhaustiveFacetsCount")
    private Boolean exhaustiveFacetsCount;
    @SerializedName("exhaustiveNbHits")
    private Boolean exhaustiveNbHits;
    @SerializedName("query")
    private String query;
    @SerializedName("params")
    private String params;
    @SerializedName("facets")
    private Map<String, Map<String, String>> facetList;
    @SerializedName("queryID")
    private String queryId;

    public ArrayList<AlgoliaHitResults> getAlgoliaHitList() {
        return algoliaHitList;
    }

    public void setAlgoliaHitList(ArrayList<AlgoliaHitResults> algoliaHitList) {
        this.algoliaHitList = algoliaHitList;
    }

    public Integer getNbHits() {
        return nbHits;
    }

    public void setNbHits(Integer nbHits) {
        this.nbHits = nbHits;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getNbPages() {
        return nbPages;
    }

    public void setNbPages(Integer nbPages) {
        this.nbPages = nbPages;
    }

    public Integer getHitsPerPage() {
        return hitsPerPage;
    }

    public void setHitsPerPage(Integer hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public Integer getProcessingTimeMS() {
        return processingTimeMS;
    }

    public void setProcessingTimeMS(Integer processingTimeMS) {
        this.processingTimeMS = processingTimeMS;
    }

    public Boolean getExhaustiveFacetsCount() {
        return exhaustiveFacetsCount;
    }

    public void setExhaustiveFacetsCount(Boolean exhaustiveFacetsCount) {
        this.exhaustiveFacetsCount = exhaustiveFacetsCount;
    }

    public Boolean getExhaustiveNbHits() {
        return exhaustiveNbHits;
    }

    public void setExhaustiveNbHits(Boolean exhaustiveNbHits) {
        this.exhaustiveNbHits = exhaustiveNbHits;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Map<String, Map<String, String>> getFacetList() {
        return facetList;
    }

    public void setFacetList(Map<String, Map<String, String>> facetList) {
        this.facetList = facetList;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }
}
