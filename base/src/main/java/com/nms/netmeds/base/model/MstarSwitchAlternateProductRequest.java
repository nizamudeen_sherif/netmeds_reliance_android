package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarSwitchAlternateProductRequest {

    @SerializedName("cart_product_code")
    private int cartProductCode;

    public int getCartProductCode() {
        return cartProductCode;
    }

    public void setCartProductCode(int cartProductCode) {
        this.cartProductCode = cartProductCode;
    }
}
