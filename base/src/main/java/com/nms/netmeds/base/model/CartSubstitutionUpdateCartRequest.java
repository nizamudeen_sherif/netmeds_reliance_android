package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartSubstitutionUpdateCartRequest {
    @SerializedName("quoteId")
    private Long quoteId;
    @SerializedName("addparam")
    private List<CartSubstitutionUpdateCartAddparam> addparam = null;
    @SerializedName("rmparam")
    private List<CartSubstitutionUpdateCartRmparam> rmparam = null;

    public Long getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Long quoteId) {
        this.quoteId = quoteId;
    }

    public List<CartSubstitutionUpdateCartAddparam> getAddparam() {
        return addparam;
    }

    public void setAddparam(List<CartSubstitutionUpdateCartAddparam> addparam) {
        this.addparam = addparam;
    }

    public List<CartSubstitutionUpdateCartRmparam> getRmparam() {
        return rmparam;
    }

    public void setRmparam(List<CartSubstitutionUpdateCartRmparam> rmparam) {
        this.rmparam = rmparam;
    }

}