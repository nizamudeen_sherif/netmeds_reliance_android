package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

public class DeliveryEstimatePinCodeResult {
    @SerializedName("fc")
    private String fc;
    @SerializedName("fc_name")
    private String fcName;
    @SerializedName("fc_address")
    private String fcAddress;
    @SerializedName("products")
    private ArrayList<String> productsList = null;
    @SerializedName("product_details")
    private Map<String, ArrayList<DeliveryEstimateProductDetail>> productDetails;
    @SerializedName("shipper_code")
    private String shipperCode;
    @SerializedName("shipper_service_code")
    private String shipperServiceCode;
    @SerializedName("shipper_name")
    private String shipperName;
    @SerializedName("delivery_estimate")
    private PincodeDeliveryEstimate deliveryEstimate;

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public String getFcAddress() {
        return fcAddress;
    }

    public void setFcAddress(String fcAddress) {
        this.fcAddress = fcAddress;
    }

    public ArrayList<String> getProductsList() {
        return productsList;
    }

    public void setProductsList(ArrayList<String> productsList) {
        this.productsList = productsList;
    }

    public Map<String, ArrayList<DeliveryEstimateProductDetail>> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(Map<String, ArrayList<DeliveryEstimateProductDetail>> productDetails) {
        this.productDetails = productDetails;
    }

    public String getShipperCode() {
        return shipperCode;
    }

    public void setShipperCode(String shipperCode) {
        this.shipperCode = shipperCode;
    }

    public String getShipperServiceCode() {
        return shipperServiceCode;
    }

    public void setShipperServiceCode(String shipperServiceCode) {
        this.shipperServiceCode = shipperServiceCode;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public PincodeDeliveryEstimate getDeliveryEstimate() {
        return deliveryEstimate;
    }

    public void setDeliveryEstimate(PincodeDeliveryEstimate deliveryEstimate) {
        this.deliveryEstimate = deliveryEstimate;
    }

    public String getFcName() {
        return fcName;
    }

    public void setFcName(String fcName) {
        this.fcName = fcName;
    }
}
