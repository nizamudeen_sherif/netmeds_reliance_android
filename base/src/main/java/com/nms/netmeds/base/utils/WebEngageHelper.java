package com.nms.netmeds.base.utils;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.CartAlternateAndOrignalProductWebEngageModel;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.model.MstarCategoryDetails;
import com.nms.netmeds.base.model.MstarManufacturerDetails;
import com.nms.netmeds.base.model.MstarSubCategoryResult;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.model.WebEngageModel;
import com.webengage.sdk.android.Analytics;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.UserProfile;
import com.webengage.sdk.android.WebEngage;
import com.webengage.sdk.android.utils.Gender;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class WebEngageHelper {
    public static final String DIRECT_SIGN_UP = "Direct Sign up";
    public static final String SOCIAL_WIDGET = "Social Widget";
    public static final String DIRECT_SIGN_IN = "Direct Sign In";
    public static final String CASH_DISCOUNT = "cash discount";
    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";
    public static final String EVENT_NAME_CART_UPDATED = "Cart Updated";
    public static final String EVENT_NAME_COUPON_CODE_APPLIED = "Coupon Code Applied";
    public static final String EVENT_NAME_CART_VIEWED = "Cart Viewed";
    public static final String EVENT_NAME_REMOVED_FROM_CART = "Removed from Cart";
    private static final String MALE = "1";
    private static final String MALE_VALUE = "male";
    private static final String FEMALE = "0";
    private static final String INR = "INR";
    private static final String PRESCRIPTION = "Prescription";
    private static final String OTC = "OTC";
    public static final String TEXT_MALE = "Male";
    public static final String TEXT_FEMALE = "Female";
    public static final String TEXT_OTHERS = "Others";
    private static final String MOBILE_NO = "mobileno";
    private static final String NORMAL_SHIPMENT = "normal shipment";
    private static final String NEW = "New";
    private static final String EXIST = "Exist";
    private static final String METHOD2 = "Method2";
    private static final String METHOD1 = "Method1";
    private static final String SEARCH_PAGE = "Search page";
    private static final String MIXED = "Mixed";
    private static final String SEARCH_TYPE = "Search Autocomplete";
    private static final String FEMALE_VALUE = "female";
    private static final String OTHERS_VALUE = "others";
    public static final String TRANSACTION_FAILED_STATUS = "Transaction Declined / Failed";
    public static final String NMS_WALLET = "NMS Wallet";
    public static final String E_VOUCHER = "e-voucher";
    public static final String AND = "and";
    public static final String GENERIC = "Generic";

    //Event names
    private static final String EVENT_NAME_REGISTRATION_DONE = "Registration Done";
    private static final String EVENT_NAME_USER_LOGGED_IN = "User Logged In";
    private static final String EVENT_NAME_ADDED_TO_CART = "Added To Cart";
    private static final String EVENT_NAME_SEARCH_AUTOCOMPLETE = "Search autocomplete";
    private static final String EVENT_NAME_PRODUCT_VIEWED = "Product Viewed";
    private static final String EVENT_NAME_PERSONAL_INFORMATION_FILLED = "Personal Information Filled";
    private static final String EVENT_NAME_DELIVERY_INFORMATION_FILLED = "Delivery Information Filled";
    private static final String EVENT_NAME_BILLING_INFORMATION_FILLED = "Billing Information Filled";
    private static final String EVENT_NAME_PIN_CODE_ENTERED = "Pin Code Entered";
    private static final String EVENT_NAME_PRESCRIPTION_UPLOADED = "Prescription Uploaded";
    private static final String EVENT_NAME_PAYMENT_FAILURE = "Payment Failure";
    private static final String EVENT_NAME_CHECKOUT_STARTED = "Checkout Started";
    private static final String EVENT_NAME_CHECKOUT_CONTINUED = "Checkout Continued";
    private static final String EVENT_NAME_CHECKOUT_COMPLETED = "Checkout Completed";
    private static final String EVENT_NAME_SUB_CATEGORY_VIEWED = "Sub-Category Viewed";
    private static final String EVENT_NAME_M2_ORDER_REQUEST_PLACED = "M2 Order Request Placed";
    private static final String EVENT_NAME_PRODUCT_SEARCHED = "Product Searched";
    private static final String EVENT_NAME_REGISTER_FOR_DIAGNOSTICS = "Registered for diagnostics";
    private static final String EVENT_NAME_TEST_PACKAGE_VIEWED = "Test/Package Viewed";
    private static final String EVENT_NAME_DIAGTYPE_SELECTED = "Diagtype selected";
    private static final String EVENT_NAME_DIAGNOSTIC_ORDER_NMS = "Diagnostic Order - NMS Cash";
    private static final String EVENT_NAME_ENTERS_PINCODE = "Enters pincode";
    private static final String EVENT_NAME_TEST_ADDED = "Test Added";
    private static final String EVENT_NAME_DIAGNOSTICS_CART = "Diagnostics cart ";
    private static final String EVENT_NAME_DIAGNOSTICS_CART_LAB_UPDATED = "Diagnostics cart - Lab updated";
    private static final String EVENT_NAME_PACKAGE_ADDED = "Package Added";
    private static final String EVENT_NAME_PATIENT_DETAILS = "Patient Details";
    private static final String EVENT_NAME_COLLECTION_ADDRESS = "Collection Address";
    private static final String EVENT_NAME_SLOT_SELECTION = "Slot Selection";
    private static final String EVENT_NAME_DIAGNOSTIC_ORDER_PLACED = "Diag Order placed";
    private static final String EVENT_NAME_REPORTS_GENERATED = "Reports Generated";
    private static final String EVENT_NAME_ORDER_CANCELLED = "Order Cancelled";
    private static final String EVENT_NAME_MANUFATURER_VIEWED = "Manufacturer Viewed";
    private static final String EVENT_NAME_ALTERNATE_CART = "Alt Cart";
    private static final String EVENT_NAME_CATEGORY_VIEWED = "Category Viewed";

    //Event params
    private static final String EVENT_PARAM_CAMPAIGN = "Campaign";
    private static final String EVENT_PARAM_MEDIUM = "Medium";
    private static final String EVENT_PARAM_MODE = "Mode";
    private static final String EVENT_PARAM_SOURCE = "Source";
    private static final String EVENT_PARAM_SOURCE_ATTRIBUTION = "Source Attribution";
    private static final String EVENT_PARAM_CATEGORY = "Category";
    private static final String EVENT_PARAM_CATEGORY_ID = "Category Id";
    private static final String EVENT_PARAM_CURRENCY = "Currency";
    private static final String EVENT_PARAM_DISCOUNT = "Discount";
    private static final String EVENT_PARAM_FORMULATIONS = "Formulations";
    private static final String EVENT_PARAM_GENERIC_DOSAGE = "Generic dosage";
    private static final String EVENT_PARAM_DOSAGE = "Generic Dosage";
    private static final String EVENT_PARAM_GENERIC_NAME = "Generic Name";
    private static final String EVENT_PARAM_NUMBER_OF_SUBSTITUES = "Number of Substitues";
    private static final String EVENT_PARAM_PRICE = "Price";
    private static final String EVENT_PARAM_PRODUCT_ID = "Product Id";
    private static final String EVENT_PARAM_PRODUCT_NAME = "Product Name";
    private static final String EVENT_PARAM_QUANTITY = "Quantity";
    private static final String EVENT_PARAM_STRENGTH = "Strength";
    private static final String EVENT_PARAM_SUB_CATEGORY = "Sub-category";
    private static final String EVENT_PARAM_SUB_CATEGORY_ID = "Sub-category Id";
    private static final String EVENT_PARAM_SUB_CATEGORY_IDS = "Sub-Category Id";
    private static final String EVENT_PARAM_TYPE_OF_PRODUCT = "Type of Product";
    private static final String EVENT_PARAM_PRODUCT = "Product";
    private static final String EVENT_PARAM_PRODUCT_SKU = "Product Sku";
    private static final String EVENT_PARAM_SEARCH_KEYWORD = "Search Keyword";
    private static final String EVENT_PARAM_SEARCH_TYPE = "Search Type";
    private static final String EVENT_PARAM_PRODUCT_SEARCH_ARRAY = "Product search array";
    private static final String EVENT_PARAM_PRODUCT_RATING = "Product Rating";
    private static final String EVENT_PARAM_DATE_OF_BIRTH = "Date of Birth";
    private static final String EVENT_PARAM_EMAIL_ID = "Email ID";
    private static final String EVENT_PARAM_FIRST_NAME = "First name";
    private static final String EVENT_PARAM_LAST_NAME = "Last name";
    private static final String EVENT_PARAM_GENDER = "Gender";
    private static final String EVENT_PARAM_MOBILE_NO = "Mobile no";
    private static final String EVENT_PARAM_ADDRESS = "Address";
    private static final String EVENT_PARAM_CITY = "City";
    private static final String EVENT_PARAM_TEST_OR_PACKAGE_NAME = "Test Or Package Name";
    private static final String EVENT_PARAM_LABS_AVAILABLE = "Labs Available";
    private static final String EVENT_PARAM_PRICES = "Prices";
    private static final String EVENT_PARAM_NMS_CASH_AMOUNT = "NMS Amount";
    private static final String EVENT_PARAM_NMS_CASH_UTILIZED_AMOUNT = "NMS Utilized";
    private static final String EVENT_PARAM_SKU_CODE = "SKU Code";


    private static final String EVENT_PARAM_COUNTRY = "Country";
    private static final String EVENT_PARAM_PINCODE = "Pin Code";
    private static final String EVENT_PARAM_STATE = "State";
    private static final String EVENT_PARAM_NO_OF_PRODUCTS = "No Of Products";
    private static final String EVENT_PARAM_NEW_PRODUCT_DETAILS = "New Product Details";
    private static final String EVENT_PARAM_TOTAL_VALUE = "Total Value";
    private static final String EVENT_PARAM_ESTIMATED_DELIVERY_TIME = "Estimated Delivery Time";
    private static final String EVENT_PARAM_PAGE = "Page";
    private static final String EVENT_PARAM_NO_OF_PRESCRIPTIONS = "No of prescriptions";
    private static final String EVENT_PARAM_PRESCRIPTION_IMAGE = "Prescription Image";
    private static final String EVENT_PARAM_PRESCRIPTION_ID = "Prescriptions Id";
    private static final String EVENT_PARAM_M1_OR_M2_EVENT = "M1 or M2 event";
    private static final String EVENT_PARAM_AMOUNT = "Amount";
    private static final String EVENT_PARAM_REASON = "Reason";
    private static final String EVENT_PARAM_TRANSACTION_ID = "Transaction Id";
    private static final String EVENT_PARAM_CART_VALUE_AFTER_DISCOUNT = "Cart Value After Discount";
    private static final String EVENT_PARAM_CART_VALUE_BEFORE_DISCOUNT = "Cart Value Before Discount";
    private static final String EVENT_PARAM_COUPON_CODE = "Coupon Code";
    private static final String EVENT_PARAM_COUPON_CODE_TYPE = "Coupon Code Type";
    private static final String EVENT_PARAM_DISCOUNT_VALUE = "Discount Value";
    private static final String EVENT_PARAM_STATUS = "Status";
    private static final String EVENT_PARAM_LINE_ITEM_PRICE = "Line Item Price";
    private static final String EVENT_PARAM_PRODUCT_DETAILS = "Product Details";
    private static final String EVENT_PARAM_NEW_ADDRESS = "New Address";
    private static final String EVENT_PARAM_SHIPPING_INFORMATION = "Shipping Information";
    private static final String EVENT_PARAM_SHIPPING_MODE = "Shipping Mode";
    private static final String EVENT_PARAM_CATEGORY_NAME = "Category Name";
    private static final String EVENT_PARAM_CUSTOMER_TYPE = "Customer Type";
    private static final String EVENT_PARAM_NEW_BILLING_INFORMATION = "New Billing Information";
    private static final String EVENT_PARAM_NEW_SHIPPING_INFORMATION = "New Shipping Information";
    private static final String EVENT_PARAM_ORDER_TYPE = "Order Type";
    private static final String EVENT_PARAM_PAYMENT_TYPE = "Payment Type";
    private static final String EVENT_PARAM_PRESCRIPTION_PROVIDED = "Prescription Provided";
    private static final String EVENT_PARAM_PRESCRIPTION_REQUIRED = "Prescription Required";
    private static final String EVENT_PARAM_QTY = "qty";
    private static final String EVENT_PARAM_Sub_Category_Name = "Sub-Category Name";
    private static final String EVENT_PARAM_ITEM_COUNT = "Item Count";
    private static final String EVENT_PARAM_CUSTOMER_NAME = "Customer name";
    private static final String EVENT_PARAM_NEW_CART_DETAILS = "New Cart Details";
    private static final String EVENT_PARAM_ORDER_ID = "orderId";
    private static final String EVENT_PARAM_SEARCH_FILTER_CATEGORY = "SearchFilter category";
    private static final String EVENT_PARAM_SEARCH_FILTER_IN_STOCK = "SearchFilter in stock";
    private static final String EVENT_PARAM_SEARCH_FILTER_MANUFACTURER = "SearchFilter manufacturer";
    private static final String EVENT_PARAM_SEARCH_FILTER_PRICE = "SearchFilter price";
    private static final String EVENT_PARAM_PRODUCT_DETAILS_ARRAY = "Product Details Array";
    private static final String EVENT_PARAM_PRODUCT_DETAILS_ARRAY_1 = "Product Details Array1";
    private static final String EVENT_PARAM_PHONE_NUMBER = "Phone no";
    private static final String EVENT_PARAM_TEST_SELECTED = "Tests selected";
    private static final String EVENT_PARAM_NUMBER_OF_TESTS = "Number of tests";
    private static final String EVENT_PARAM_VALUE_OF_TESTS = "Value of tests";
    private static final String EVENT_PARAM_LIST_OF_LABS_PRICES_AND_DISCOUNDS = "List of Labs & Prices & discounts";
    private static final String EVENT_PARAM_LAB_SELECTED = "Lab selected";
    private static final String EVENT_PARAM_PACKAGE_SELECTED = "Package selected";
    private static final String EVENT_PARAM_VALUE_OF_PACKAGE = "Value of package";
    private static final String EVENT_PARAM_AGE = "Age";
    private static final String EVENT_PARAM_PATIENTS_NAME = "Patients name";
    private static final String EVENT_PARAM_SPECIALTY = "Specialty";
    private static final String EVENT_PARAM_SLOT_SELECETD_DAY = "Slot selected day";
    private static final String EVENT_PARAM_SLOT_SELECETD_TIME = "Slot selected time";
    private static final String EVENT_PARAM_ORDER_SUCCESSFUL = "Order successful";
    private static final String EVENT_PARAM_ORDER_AMOUNT = "Order Amount";
    private static final String EVENT_PARAM_PAYMENT_AMOUNT = "Payment Amount";
    private static final String EVENT_PARAM_ORDER_DATE = "Order date";
    private static final String EVENT_PARAM_REPORTS_GENERATED = "Reports generated";
    private static final String EVENT_PARAM_PACKAGE_DETAILS = "Package details";
    private static final String EVENT_PARAM_COLLECTION_ADDRESS = "Address";

    private static final String EVENT_PARAM_DIAGNOSTIC_CUSTOMER_NAME = "Customer Name";
    private static final String EVENT_PARAM_DIAGNOSTIC_CUSTOMER_EMAIL = "Email";

    private static final String EVENT_PARAM_MANUFACTURER_ID = "Manufacturer Id";
    private static final String EVENT_PARAM_MANUFACTURER_NAME = "Manufacturer Name";
    private static final String EVENT_PARAM_APP_VERSION = "App Version";

    // Consultation Event
    private static final String EVENT_NAME_REGISTERED_FOR_CONSULTATION = "Registered for consultation";
    private static final String EVENT_NAME_LOGIN_FOR_CONSULTATION = "Login for consultation";
    private static final String EVENT_NAME_CONSULTATION_INITIATED = "Consultation Initiated";
    private static final String EVENT_NAME_PATIENT_IDENTIFIED = "Patient identified";
    private static final String EVENT_NAME_SPECIALITY_SELECTED = "Specialty Selected";
    private static final String EVENT_NAME_SYMPTOMS_ENTERED = "Symptoms entered";
    private static final String EVENT_NAME_MODE_SELECTED = "Mode Selected";
    private static final String EVENT_NAME_CONSULTATION_TYPE_SELECTED = "Consultation Type Selected";
    private static final String EVENT_NAME_PAYMENT_MODE_SELECTED = "Payment mode selected";
    private static final String EVENT_NAME_PAYMENT_SUCCESSFULLY_RECEIVED_ORDER_PLACED = "" +
            "Payment successfully received/Order Placed";

    private static final String EVENT_NAME_CONSULTATION_COMPLTE_PRESCRIPTION_READY = "Consultation Complete/Prescription is ready";
    private static final String EVENT_NAME_LABTEST_IS_PRESCRIBED = "If Labtests is prescribed";
    private static final String EVENT_NAME_FOLLOW_UP = "Follow Up";
    private static final String EVENT_DOCTOR_HAS_REPIED_TO_PATIENT = "Doctor has replied to the patient";
    private static final String EVENT_NAME_NOTIFIY_DOCTOR_WHEN_PATIENT_AVAILABLE = "Patient available notification";
    private static final String EVENT_NAME_IF_MEDICINE_OR_LAB_TEST_IS_PRESCRIBED = "If medicine or lab test is prescribed";
    private static final String EVENT_NAME_RATING = "Rating";
    private static final String EVENT_NAME_CHAT_CONSULTATION_COMPELTED = "Chat consultation is completed";

    // Consultation Event PARAM
    private static final String EVENT_PARAM_EMAIL = "Email";
    private static final String EVENT_PARAM_PATIENT = "Patient";
    private static final String EVENT_PARAM_SYMPTOM = "Symptom";
    private static final String EVENT_PARAM_TYPE = "Type";
    private static final String EVENT_PARAM_PAYMENT_MODE = "Payment mode";
    private static final String EVENT_PARAM_PAYMENT_STATUS = "Payment Status";
    private static final String EVENT_PARAM_PRESCRIPTION_STATUS = "Prescription status";
    private static final String EVENT_PARAM_Lab_TEST_PRESCRIBED = "Labtest prescribed";
    private static final String EVENT_PARAM_Lab_TEST_NAME = "Labtest name";
    private static final String EVENT_PARAM_Lab_FOLLOW_UP_STATUS = "Followup status";
    private static final String EVENT_PARAM_PRESCRIBED_MEDICINE = "Prescribed medicine";
    private static final String EVENT_PARAM_DOSAGE_PRESCRIBED = "Dosage prescribed";
    private static final String EVENT_PARAM_RATING = "Rating";

    //ALTERNATE CART
    private static final String EVENT_PARAM_ORIGINAL_PRODUCT = "Original Products";
    private static final String EVENT_PARAM_ALTERNATE_CART_PRODUCT_ARRAY = "Alternate Cart Products Array";
    private static final String EVENT_PARAM_ORIGINAL_AMOUNT = "Original Amount";
    private static final String EVENT_PARAM_ALTR_CART_AMOUNT = "Alt Cart Amount";
    private static final String EVENT_PARAM_ALTR_PRODUCT_COUNT = "Alt Products Count";

    /******************************************In App Notification Screen Name Start****************************************************/

    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_HOME_PAGE = "Home Page";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_MY_ORDER = "My Order";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_CONSULTATION_PAGE = "Consultation Page";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_MY_WALLET = "My Wallet";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_REFER_AND_EARN = "Refer and Earn";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_PRODUCT_DETAILS_PAGE = "Product Details";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_OFFER_PAGE = "Offer Page";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_WELLNESS_PAGE = "Wellness Page";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_CATEGORY_LIST = "Category List";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_CART_PAGE = "Cart Page";
    public static final String IN_APP_NOTIFICATION_SCREEN_NAME_Product_List_Page = "Product List Page";

    /********************************************In App Notification Screen Name End**********************************************************************************/

    private static WebEngageHelper webEngageHelper;
    private HashMap<String, Object> postParam;
    private DecimalFormat df = new DecimalFormat("#.##");
    private JustDocUserResponse diagnosticUserDetail;

    public static WebEngageHelper getInstance() {
        if (webEngageHelper == null) {
            webEngageHelper = new WebEngageHelper();
        }
        return webEngageHelper;
    }

    //get user from webEngage
    public User getUser() {
        return WebEngage.get().user();
    }

    //set Login at webEngage
    private void webEngageLogin(String firstName, String lastName, String email, String phoneNo) {
        getUser().setUserProfile(new UserProfile.Builder()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setEmail(email)
                .setPhoneNumber(phoneNo)
                .setBirthDate("")
                .setGender(getGender(""))
                .build());
    }

    private Gender getGender(String gender) {
        switch (gender) {
            case MALE:
                return Gender.MALE;
            case FEMALE:
                return Gender.FEMALE;
            default:
                return Gender.OTHER;
        }
    }

    private void addParam(String key, Object value) {
        postParam.put(key, value);
    }

    private void postEvent(String eventName) {
        WebEngage.get().analytics().track(eventName, postParam, new Analytics.Options().setHighReportingPriority(true));
    }

    /**************************************************Medicine Module Events Start******************************************************************/

    public void setWebEngageLogin(BasePreference basePreference, String mode, boolean isRegistration, Context context) {
        MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        String userId = customerDetails != null ? Integer.toString(customerDetails.getId()) : "";
        String firstName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "";
        String lastName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "";
        String email = customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "";
        String phoneNo = customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "";
        //set webEngage user id(Unique id)
        webEngageHelper.getUser().login(userId);
        //set webEngage user profile
        webEngageHelper.webEngageLogin(firstName, lastName, email, phoneNo);
        //webEngage registration event
        webEngageHelper.registrationAndLoggedInEvent(context, mode, isRegistration);
    }

    private void registrationAndLoggedInEvent(Context context, String mode, boolean isRegistration) {
        postParam = new HashMap<>();
        postParam.putAll(getCommonMap(context));
        addParam(EVENT_PARAM_MODE, mode);

        postEvent(isRegistration ? EVENT_NAME_REGISTRATION_DONE : EVENT_NAME_USER_LOGGED_IN);
    }

    private Map<String, Object> getCommonMap(Context context) {
        Map<String, Object> map = new HashMap<>();
        map.put(EVENT_PARAM_CAMPAIGN, "");
        map.put(EVENT_PARAM_MEDIUM, "");
        map.put(EVENT_PARAM_SOURCE, "");
        map.put(EVENT_PARAM_SOURCE_ATTRIBUTION, "");
        map.put(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        return map;
    }

    public void productSearchedEvent(Context context, int itemCount, String searchKeyWord, boolean isFrom) {
        postParam = new HashMap<>();

        addParam(EVENT_PARAM_ITEM_COUNT, itemCount);
        addParam(EVENT_PARAM_PAGE, isFrom ? GENERIC : SEARCH_PAGE);
        addParam(EVENT_PARAM_SEARCH_KEYWORD, searchKeyWord);
        addParam(EVENT_PARAM_SEARCH_TYPE, SEARCH_PAGE);
        addParam(EVENT_PARAM_SEARCH_FILTER_CATEGORY, "");
        addParam(EVENT_PARAM_SEARCH_FILTER_IN_STOCK, "");
        addParam(EVENT_PARAM_SEARCH_FILTER_MANUFACTURER, "");
        addParam(EVENT_PARAM_SEARCH_FILTER_PRICE, "");
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postParam.putAll(getCommonMap(context));

        postEvent(EVENT_NAME_PRODUCT_SEARCHED);
    }

    public void searchAutocompleteEvent(Context context, MstarAlgoliaResult algoliaHitResults, String searchKeyword) {
        postParam = new HashMap<>();
        String categoryName = getCategoryLevelFromAlgoliaResult(algoliaHitResults, AppConstant.LEVEL_0);
        String subCategoryName = getCategoryLevelFromAlgoliaResult(algoliaHitResults, !TextUtils.isEmpty(algoliaHitResults.getProductType()) ? getLevelBasedOnProductType(algoliaHitResults) : "");
        int categoryIndex = getCategoryIdIndexFromAlgoliaResult(algoliaHitResults, categoryName);
        int subCategoryIndex = getCategoryIdIndexFromAlgoliaResult(algoliaHitResults, subCategoryName);

        addParam(EVENT_PARAM_CATEGORY, categoryName);
        addParam(EVENT_PARAM_CATEGORY_ID, algoliaHitResults.getCategoryIds() != null && algoliaHitResults.getCategoryIds().size() > 0 && categoryIndex != -1 && categoryIndex < algoliaHitResults.getCategoryIds().size() ? Long.toString(algoliaHitResults.getCategoryIds().get(categoryIndex)) : "");
        addParam(EVENT_PARAM_FORMULATIONS, checkEmpty(algoliaHitResults.getFormulationType()));
        addParam(EVENT_PARAM_GENERIC_NAME, checkEmpty(algoliaHitResults.getGeneric()));
        addParam(EVENT_PARAM_PRICE, algoliaHitResults.getSellingPrice().toString());
        addParam(EVENT_PARAM_PRODUCT, checkEmpty(algoliaHitResults.getDisplayName()));
        addParam(EVENT_PARAM_PRODUCT_ID, checkEmpty(algoliaHitResults.getObjectID()));
        addParam(EVENT_PARAM_PRODUCT_SKU, algoliaHitResults.getProductCode());
        addParam(EVENT_PARAM_SEARCH_KEYWORD, searchKeyword);
        addParam(EVENT_PARAM_SEARCH_TYPE, SEARCH_TYPE);
        addParam(EVENT_PARAM_SUB_CATEGORY, subCategoryName);
        addParam(EVENT_PARAM_SUB_CATEGORY_ID, algoliaHitResults.getCategoryIds() != null && algoliaHitResults.getCategoryIds().size() > 0 && subCategoryIndex != -1 && subCategoryIndex < algoliaHitResults.getCategoryIds().size() ? Long.toString(algoliaHitResults.getCategoryIds().get(subCategoryIndex)) : "");
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        addParam(EVENT_PARAM_TYPE_OF_PRODUCT, checkEmpty(algoliaHitResults.getProductType()));
        addParam(EVENT_PARAM_STRENGTH, checkEmpty(algoliaHitResults.getDosage()));
        postParam.putAll(getCommonMap(context));

        postEvent(EVENT_NAME_SEARCH_AUTOCOMPLETE);
    }


    public void addToCartEvent(Context context, MStarProductDetails product, int quantity, boolean isFromGeneric) {
        if (product != null) {
            postParam = new HashMap<>();
            double price = 0.0;
            double actualPrice = 0.0;
            price = product.getMrp().doubleValue();
            actualPrice = product.getSellingPrice().doubleValue();
            String productType = checkEmpty(product.getProductType());
            int subCategoryLevel = productType.equalsIgnoreCase(AppConstant.DRUG_TYPE_P) ? 1 : productType.equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? 2 : -1;

            addParam(EVENT_PARAM_CATEGORY, getCategoryName(product.getCategories(), 0));
            addParam(EVENT_PARAM_CATEGORY_ID, getCategoryId(product.getCategories(), 0));
            addParam(EVENT_PARAM_CURRENCY, INR);
            addParam(EVENT_PARAM_DISCOUNT, price != actualPrice ? Double.parseDouble(df.format((price - actualPrice))) : 0);
            addParam(EVENT_PARAM_FORMULATIONS, checkEmpty(product.getFormulationType()));
            addParam(EVENT_PARAM_GENERIC_DOSAGE, product.getGenericWithDosage() != null ? checkEmpty(product.getGenericWithDosage().getName()) : "");
            addParam(EVENT_PARAM_GENERIC_NAME, product.getGeneric() != null ? checkEmpty(product.getGeneric().getName()) : "");
            addParam(EVENT_PARAM_PRICE, actualPrice);
            addParam(EVENT_PARAM_PRODUCT_ID, Integer.toString(product.getProductCode()));
            addParam(EVENT_PARAM_PRODUCT_NAME, checkEmpty(product.getDisplayName()));
            addParam(EVENT_PARAM_QUANTITY, quantity);
            addParam(EVENT_PARAM_STRENGTH, product.getDosage());
            addParam(EVENT_PARAM_SUB_CATEGORY, getCategoryName(product.getCategories(), subCategoryLevel));
            addParam(EVENT_PARAM_SUB_CATEGORY_ID, getCategoryId(product.getCategories(), subCategoryLevel));
            addParam(EVENT_PARAM_TYPE_OF_PRODUCT, !TextUtils.isEmpty(checkEmpty(product.getProductType())) ? product.getProductType().equalsIgnoreCase("p") ? PRESCRIPTION : OTC : "");
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
            addParam(EVENT_PARAM_PAGE, isFromGeneric ? GENERIC : "");
            //For Buy Again webengage --only
            if (product.getPageName() != null && !TextUtils.isEmpty(product.getPageName())) {
                addParam(EVENT_PARAM_PAGE, product.getPageName());
            }
            postParam.putAll(getCommonMap(context));

            postEvent(EVENT_NAME_ADDED_TO_CART);
        }
    }

    public void productViewedEvent(Context context, MStarProductDetails productDetails) {
        postParam = new HashMap<>();
        double price = 0.0;
        double strikePrice = 0.0;
        price = productDetails.getSellingPrice().doubleValue();
        strikePrice = productDetails.getMrp().doubleValue();
        String categoryId = getCategoryId(productDetails.getCategories(), 0);
        String productType = checkEmpty(productDetails.getProductType());
        int subCategoryLevel = productType.equalsIgnoreCase(AppConstant.DRUG_TYPE_P) ? 1 : productType.equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? 2 : -1;
        String subCategoryId = getCategoryId(productDetails.getCategories(), subCategoryLevel);

        addParam(EVENT_PARAM_CATEGORY, getCategoryName(productDetails.getCategories(), 0));
        addParam(EVENT_PARAM_CATEGORY_ID, !TextUtils.isEmpty(categoryId) && TextUtils.isDigitsOnly(categoryId) ? Integer.parseInt(categoryId) : 0);
        addParam(EVENT_PARAM_DISCOUNT, price != strikePrice ? Double.parseDouble(getDiscountValue(strikePrice - price).replace(",", "").trim()) : 0);
        addParam(EVENT_PARAM_FORMULATIONS, checkEmpty(productDetails.getFormulationType()));
        addParam(EVENT_PARAM_DOSAGE, productDetails.getGenericWithDosage() != null ? checkEmpty(productDetails.getGenericWithDosage().getName()) : "");
        addParam(EVENT_PARAM_GENERIC_NAME, productDetails.getGeneric() != null ? checkEmpty(productDetails.getGeneric().getName()) : "");
        addParam(EVENT_PARAM_PRICE, price);
        addParam(EVENT_PARAM_PRODUCT_ID, productDetails.getProductCode());
        addParam(EVENT_PARAM_PRODUCT_RATING, "");
        addParam(EVENT_PARAM_PRODUCT_SKU, productDetails.getProductCode());
        addParam(EVENT_PARAM_QUANTITY, productDetails.getMaxQtyInOrder());
        addParam(EVENT_PARAM_STRENGTH, checkEmpty(productDetails.getDosage()));
        addParam(EVENT_PARAM_SUB_CATEGORY, getCategoryName(productDetails.getCategories(), subCategoryLevel));
        addParam(EVENT_PARAM_SUB_CATEGORY_ID, !TextUtils.isEmpty(subCategoryId) && TextUtils.isDigitsOnly(subCategoryId) ? Integer.parseInt(subCategoryId) : 0);
        addParam(EVENT_PARAM_TYPE_OF_PRODUCT, checkEmpty(productDetails.getProductType()));
        addParam(EVENT_PARAM_PRODUCT_NAME, checkEmpty(productDetails.getDisplayName()));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postParam.putAll(getCommonMap(context));

        postEvent(EVENT_NAME_PRODUCT_VIEWED);
    }

    public void personalInformationFilledEvent(Context context, MStarCustomerDetails customerDetails) {
        postParam = new HashMap<>();

        addParam(EVENT_PARAM_FIRST_NAME, checkEmpty(customerDetails.getFirstName()));
        addParam(EVENT_PARAM_LAST_NAME, checkEmpty(customerDetails.getLastName()));
        addParam(EVENT_PARAM_GENDER, getGenderValue(customerDetails.getGender()));
        addParam(EVENT_PARAM_EMAIL_ID, checkEmpty(customerDetails.getEmail()));
        addParam(EVENT_PARAM_DATE_OF_BIRTH, checkEmpty(customerDetails.getDateOfBirth()));
        addParam(EVENT_PARAM_MOBILE_NO, checkEmpty(customerDetails.getMobileNo()));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

        postEvent(EVENT_NAME_PERSONAL_INFORMATION_FILLED);
    }

    public void deliveryInformationFilledEvent(Context context, MStarAddressModel addressModel) {
        if (addressModel != null) {
            postParam = new HashMap<>();
            addParam(EVENT_PARAM_FIRST_NAME, checkEmpty(addressModel.getFirstname()));
            addParam(EVENT_PARAM_LAST_NAME, checkEmpty(addressModel.getLastname()));
            addParam(EVENT_PARAM_MOBILE_NO, checkEmpty(addressModel.getMobileNo()));
            addParam(EVENT_PARAM_ADDRESS, checkEmpty(addressModel.getStreet()));
            addParam(EVENT_PARAM_PINCODE, checkEmpty(addressModel.getPin()));
            addParam(EVENT_PARAM_CITY, checkEmpty(addressModel.getCity()));
            addParam(EVENT_PARAM_STATE, checkEmpty(addressModel.getState()));
            addParam(EVENT_PARAM_COUNTRY, AppConstant.IN);
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            postEvent(EVENT_NAME_DELIVERY_INFORMATION_FILLED);
        }
    }

    public void billingInformationFilledEvent(Context context, MStarAddressModel addressModel) {
        if (addressModel != null) {
            postParam = new HashMap<>();

            addParam(EVENT_PARAM_FIRST_NAME, checkEmpty(addressModel.getFirstname()));
            addParam(EVENT_PARAM_LAST_NAME, checkEmpty(addressModel.getLastname()));
            addParam(EVENT_PARAM_MOBILE_NO, checkEmpty(addressModel.getMobileNo()));
            addParam(EVENT_PARAM_ADDRESS, checkEmpty(addressModel.getStreet()));
            addParam(EVENT_PARAM_PINCODE, checkEmpty(addressModel.getPin()));
            addParam(EVENT_PARAM_CITY, checkEmpty(addressModel.getCity()));
            addParam(EVENT_PARAM_STATE, checkEmpty(addressModel.getState()));
            addParam(EVENT_PARAM_COUNTRY, AppConstant.IN);
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            postEvent(EVENT_NAME_BILLING_INFORMATION_FILLED);
        }
    }

    public void pinCodeEnteredEvent(Context context, String pinCode, String page, String estimatedDeliveryTime) {
        postParam = new HashMap<>();

        addParam(EVENT_PARAM_ESTIMATED_DELIVERY_TIME, estimatedDeliveryTime);
        addParam(EVENT_PARAM_PAGE, page);
        addParam(EVENT_PARAM_PINCODE, checkEmpty(pinCode));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postParam.putAll(getCommonMap(context));

        postEvent(EVENT_NAME_PIN_CODE_ENTERED);
    }

    public void prescriptionUploadedEvent(Context context, List<String> prescriptionId, String isFrom) {
        postParam = new HashMap<>();

        addParam(EVENT_PARAM_NO_OF_PRESCRIPTIONS, prescriptionId.size());
        addParam(EVENT_PARAM_PRESCRIPTION_IMAGE, prescriptionId.size() > 0 ? prescriptionId.toString().substring(1, prescriptionId.toString().length() - 1).replaceAll("\"", "") : "");
        addParam(EVENT_PARAM_M1_OR_M2_EVENT, isFrom);
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postParam.putAll(getCommonMap(context));

        postEvent(EVENT_NAME_PRESCRIPTION_UPLOADED);
    }

    public void couponCodeAppliedEvent(Context context, MStarCartDetails mStarCartDetails, String couponType, String status) {
        postParam = new HashMap<>();
        double beforeDiscountAmount = (mStarCartDetails.getNetPayableAmount().doubleValue() + mStarCartDetails.getTotalSavings().doubleValue());
        addParam(EVENT_PARAM_CART_VALUE_AFTER_DISCOUNT, mStarCartDetails.getNetPayableAmount().doubleValue());
        addParam(EVENT_PARAM_CART_VALUE_BEFORE_DISCOUNT, beforeDiscountAmount);
        addParam(EVENT_PARAM_COUPON_CODE, !TextUtils.isEmpty(mStarCartDetails.getAppliedCoupons()) ? mStarCartDetails.getAppliedCoupons() : "");
        addParam(EVENT_PARAM_COUPON_CODE_TYPE, couponType);
        addParam(EVENT_PARAM_DISCOUNT_VALUE, mStarCartDetails.getTotalSavings().doubleValue());
        addParam(EVENT_PARAM_STATUS, status);
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postParam.putAll(getCommonMap(context));

        postEvent(EVENT_NAME_COUPON_CODE_APPLIED);
    }

    public void cartViewedEvent(Context context, MStarCartDetails mStarCartDetails, List<MStarProductDetails> productDetailList) {
        if (mStarCartDetails != null && productDetailList != null) {
            postParam = new HashMap<>();
            addParam(EVENT_PARAM_LINE_ITEM_PRICE, "");
            addParam(EVENT_PARAM_NO_OF_PRODUCTS, mStarCartDetails.getLines() != null ? mStarCartDetails.getLines().size() : 0);
            addParam(EVENT_PARAM_PRODUCT_DETAILS, "");
            addParam(EVENT_PARAM_TOTAL_VALUE, mStarCartDetails.getNetPayableAmount().doubleValue());
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            StringBuilder qtyBuilder = new StringBuilder();
            if (mStarCartDetails.getLines() != null && mStarCartDetails.getLines().size() > 0) {
                for (MStarProductDetails items : mStarCartDetails.getLines()) {
                    if (qtyBuilder.length() > 0)
                        qtyBuilder.append(",").append(items.getCartQuantity());
                    else
                        qtyBuilder.append(items.getCartQuantity());
                }
            }
            addParam(EVENT_PARAM_PRODUCT_DETAILS_ARRAY_1, getProductDetailList(mStarCartDetails, productDetailList));
            addParam(EVENT_PARAM_QUANTITY, qtyBuilder.length() > 0 ? qtyBuilder.toString() : "");
            postParam.putAll(getCommonMap(context));

            postEvent(EVENT_NAME_CART_VIEWED);
        }
    }

    public void cartUpdateEvent(Context context, MStarCartDetails mStarCartDetails, List<MStarProductDetails> productDetailList) {
        if (mStarCartDetails != null && productDetailList != null) {
            postParam = new HashMap<>();
            addParam(EVENT_PARAM_NO_OF_PRODUCTS, mStarCartDetails.getLines() != null ? mStarCartDetails.getLines().size() : 0);
            addParam(EVENT_PARAM_TOTAL_VALUE, mStarCartDetails.getNetPayableAmount().doubleValue());
            addParam(EVENT_PARAM_PRODUCT_DETAILS_ARRAY, getProductDetailList(mStarCartDetails, productDetailList));
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
            //For Buy Again webengage --only
            if (!TextUtils.isEmpty(mStarCartDetails.getPageName())) {
                addParam(EVENT_PARAM_PAGE, mStarCartDetails.getPageName());
            }
            postParam.putAll(getCommonMap(context));

            postEvent(EVENT_NAME_CART_UPDATED);
        }
    }

    public void removedFromCartEvent(Context context, int qty, MStarProductDetails productDetails) {
        if (productDetails != null) {
            postParam = new HashMap<>();
            String typeOfProduct = !TextUtils.isEmpty(productDetails.getProductType()) ? productDetails.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? OTC : PRESCRIPTION : "";

            addParam(EVENT_PARAM_CATEGORY, getCategoryName(productDetails.getCategories(), 0));
            addParam(EVENT_PARAM_CATEGORY_ID, getCategoryId(productDetails.getCategories(), 0));
            addParam(EVENT_PARAM_CURRENCY, INR);
            addParam(EVENT_PARAM_FORMULATIONS, checkEmpty(productDetails.getFormulationType()));
            addParam(EVENT_PARAM_GENERIC_NAME, productDetails.getGeneric() != null ? checkEmpty(productDetails.getGeneric().getName()) : "");
            addParam(EVENT_PARAM_NO_OF_PRODUCTS, 1);
            addParam(EVENT_PARAM_PRICE, productDetails.getSellingPrice().doubleValue());
            addParam(EVENT_PARAM_PRODUCT_ID, Integer.toString(productDetails.getProductCode()));
            addParam(EVENT_PARAM_PRODUCT_NAME, checkEmpty(productDetails.getDisplayName()));
            addParam(EVENT_PARAM_PRODUCT_SKU, Integer.toString(productDetails.getProductCode()));
            addParam(EVENT_PARAM_QUANTITY, qty);
            addParam(EVENT_PARAM_STRENGTH, checkEmpty(productDetails.getDosage()));
            addParam(EVENT_PARAM_SUB_CATEGORY, getCategoryName(productDetails.getCategories(), 1));
            addParam(EVENT_PARAM_SUB_CATEGORY_ID, getCategoryId(productDetails.getCategories(), 1));
            addParam(EVENT_PARAM_TYPE_OF_PRODUCT, typeOfProduct);
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            postEvent(EVENT_NAME_REMOVED_FROM_CART);
        }
    }

    public void checkoutStartedEvent(Context context, MStarCartDetails mStarCartDetails) {
        postParam = new HashMap<>();

        addParam(EVENT_PARAM_CITY, "");
        addParam(EVENT_PARAM_COUPON_CODE, checkEmpty(mStarCartDetails.getAppliedCoupons()));
        addParam(EVENT_PARAM_COUPON_CODE_TYPE, !TextUtils.isEmpty(mStarCartDetails.getAppliedCoupons()) ? CASH_DISCOUNT : "");
        addParam(EVENT_PARAM_DISCOUNT_VALUE, mStarCartDetails.getTotalSavings().doubleValue());
        addParam(EVENT_PARAM_NEW_ADDRESS, "");
        addParam(EVENT_PARAM_NO_OF_PRODUCTS, mStarCartDetails.getLines() != null ? mStarCartDetails.getLines().size() : 0);
        addParam(EVENT_PARAM_SHIPPING_INFORMATION, "");
        addParam(EVENT_PARAM_SHIPPING_MODE, NORMAL_SHIPMENT);
        addParam(EVENT_PARAM_TOTAL_VALUE, mStarCartDetails.getNetPayableAmount().doubleValue());
        addParam(EVENT_PARAM_ORDER_TYPE, getOrderType(mStarCartDetails.getLines()));
        addParam(EVENT_PARAM_PRODUCT_DETAILS_ARRAY, getProductDetailArray(mStarCartDetails));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));


        postEvent(EVENT_NAME_CHECKOUT_STARTED);
    }

    public void checkoutContinuedEvent(Context context, MStarCartDetails cartDetails, MStarAddressModel addressModel) {
        postParam = new HashMap<>();
        String city = addressModel != null && !TextUtils.isEmpty(addressModel.getCity()) ? addressModel.getCity() : "";
        addParam(EVENT_PARAM_ADDRESS, addressModel != null ? new Gson().toJson(addressModel) : "");
        addParam(EVENT_PARAM_CITY, city);
        addParam(EVENT_PARAM_COUPON_CODE, checkEmpty(cartDetails.getAppliedCoupons()));
        addParam(EVENT_PARAM_COUPON_CODE_TYPE, !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? CASH_DISCOUNT : "");
        addParam(EVENT_PARAM_DISCOUNT_VALUE, Double.parseDouble(df.format(cartDetails.getTotalSavings().doubleValue())));
        addParam(EVENT_PARAM_NEW_ADDRESS, addressModel != null ? new Gson().toJson(addressModel) : "");
        addParam(EVENT_PARAM_NO_OF_PRODUCTS, cartDetails.getLines() != null ? cartDetails.getLines().size() : 0);
        addParam(EVENT_PARAM_TOTAL_VALUE, cartDetails.getNetPayableAmount().doubleValue());
        addParam(EVENT_PARAM_ORDER_TYPE, getOrderType(cartDetails.getLines()));
        addParam(EVENT_PARAM_PRODUCT_DETAILS_ARRAY, getProductDetailArray(cartDetails));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

        postEvent(EVENT_NAME_CHECKOUT_CONTINUED);
    }

    public void checkoutCompletedEvent(Context context, String orderId, MStarAddressModel addressModel, String appFirstOrder, MStarCartDetails cartDetails,
                                       List<MStarUploadPrescription> prescriptionList, boolean prescriptionOrder, String paymentMethod,
                                       MStarAddressModel customerBillingAddress, List<MStarProductDetails> webEngageProductDetails) {


        postParam = new HashMap<>();
        String customerType = !TextUtils.isEmpty(appFirstOrder) ? appFirstOrder.equalsIgnoreCase(AppConstant.YES) ? NEW : EXIST : "";
        double grandTotal = cartDetails != null ? cartDetails.getNetPayableAmount().doubleValue() : 0;
        double discount = cartDetails != null ? cartDetails.getTotalSavings().doubleValue() : 0;
        String couponCode = cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? cartDetails.getAppliedCoupons() : "";

        addParam(EVENT_PARAM_CITY, addressModel != null ? checkEmpty(addressModel.getCity()) : "");
        addParam(EVENT_PARAM_COUPON_CODE, couponCode);
        addParam(EVENT_PARAM_COUPON_CODE_TYPE, !TextUtils.isEmpty(couponCode) ? CASH_DISCOUNT : "");
        addParam(EVENT_PARAM_CUSTOMER_TYPE, customerType);
        addParam(EVENT_PARAM_DISCOUNT_VALUE, Double.parseDouble(df.format(discount)));
        addParam(EVENT_PARAM_NEW_BILLING_INFORMATION, customerBillingAddress != null ? new Gson().toJson(customerBillingAddress) : addressModel != null ? new Gson().toJson(addressModel) : "");
        addParam(EVENT_PARAM_NEW_SHIPPING_INFORMATION, addressModel != null ? new Gson().toJson(addressModel) : "");
        addParam(EVENT_PARAM_NO_OF_PRODUCTS, cartDetails != null && cartDetails.getLines() != null ? cartDetails.getLines().size() : 0);
        addParam(EVENT_PARAM_ORDER_TYPE, cartDetails != null && cartDetails.getLines() != null ? getOrderType(cartDetails.getLines()) : "");
        addParam(EVENT_PARAM_PAYMENT_TYPE, paymentMethod);
        addParam(EVENT_PARAM_PRESCRIPTION_IMAGE, getPrescriptionImagesAndIds(prescriptionList));
        addParam(EVENT_PARAM_PRESCRIPTION_ID, getPrescriptionImagesAndIds(prescriptionList));
        addParam(EVENT_PARAM_NO_OF_PRESCRIPTIONS, Integer.toString(prescriptionList.size()));
        addParam(EVENT_PARAM_PRESCRIPTION_PROVIDED, prescriptionList.size() > 0);
        addParam(EVENT_PARAM_PRESCRIPTION_REQUIRED, prescriptionOrder);

        addParam(EVENT_PARAM_SHIPPING_MODE, NORMAL_SHIPMENT);
        addParam(EVENT_PARAM_TOTAL_VALUE, grandTotal);
        addParam(EVENT_PARAM_TRANSACTION_ID, checkEmpty(orderId));

        StringBuilder productIdBuilder = new StringBuilder();
        StringBuilder productNameBuilder = new StringBuilder();
        StringBuilder priceBuilder = new StringBuilder();
        StringBuilder qtyBuilder = new StringBuilder();

        ArrayList<HashMap<String, Object>> productDetailsArray = new ArrayList<>();
        if (cartDetails != null && cartDetails.getLines() != null && cartDetails.getLines().size() > 0) {
            for (MStarProductDetails items : cartDetails.getLines()) {
                HashMap<String, Object> productDetail = new HashMap<>();
                productDetail.put(EVENT_PARAM_SKU_CODE, items.getProductCode());
                productDetail.put(EVENT_PARAM_PRODUCT_NAME, checkEmpty(items.getDisplayName()));
                productDetail.put(EVENT_PARAM_TYPE_OF_PRODUCT, checkEmpty(items.getProductType()));
                productDetail.put(EVENT_PARAM_QTY, items.getCartQuantity());
                productDetail.put(EVENT_PARAM_PRICE, items.getSellingPrice());
                productDetailsArray.add(productDetail);

                if (productIdBuilder.length() > 0)
                    productIdBuilder.append(",").append(items.getProductCode());
                else
                    productIdBuilder.append(items.getProductCode());

                if (productNameBuilder.length() > 0)
                    productNameBuilder.append(",").append(items.getDisplayName());
                else
                    productNameBuilder.append(items.getDisplayName());

                if (priceBuilder.length() > 0)
                    priceBuilder.append(",").append(items.getSellingPrice());
                else
                    priceBuilder.append(items.getSellingPrice());

                if (qtyBuilder.length() > 0)
                    qtyBuilder.append(",").append(items.getCartQuantity());
                else
                    qtyBuilder.append(items.getCartQuantity());
            }
        }

        StringBuilder categoryNameBuilder = new StringBuilder();
        StringBuilder categoryIdBuilder = new StringBuilder();
        StringBuilder subCategoryIdBuilder = new StringBuilder();
        StringBuilder subCategoryNameBuilder = new StringBuilder();

        if (webEngageProductDetails != null && webEngageProductDetails.size() > 0) {
            for (MStarProductDetails details : webEngageProductDetails) {
                String categoryId = getCategoryId(details.getCategories(), 0);
                String categoryName = getCategoryName(details.getCategories(), 0);
                String subCategoryId = getCategoryId(details.getCategories(), 1);
                String subCategoryName = getCategoryName(details.getCategories(), 1);

                if (categoryNameBuilder.length() > 0)
                    categoryNameBuilder.append(",").append(categoryName);
                else
                    categoryNameBuilder.append(categoryName);

                if (categoryIdBuilder.length() > 0)
                    categoryIdBuilder.append(",").append(categoryId);
                else
                    categoryIdBuilder.append(categoryId);

                if (subCategoryIdBuilder.length() > 0)
                    subCategoryIdBuilder.append(",").append(subCategoryId);
                else
                    subCategoryIdBuilder.append(subCategoryId);

                if (subCategoryNameBuilder.length() > 0)
                    subCategoryNameBuilder.append(",").append(subCategoryName);
                else
                    subCategoryNameBuilder.append(subCategoryName);
            }

        }

        addParam(EVENT_PARAM_PRODUCT_ID, productIdBuilder.length() > 0 ? productIdBuilder.toString() : "");
        addParam(EVENT_PARAM_PRODUCT_NAME, productNameBuilder.length() > 0 ? productNameBuilder.toString() : "");
        addParam(EVENT_PARAM_PRICE, priceBuilder.length() > 0 ? priceBuilder.toString() : "");
        addParam(EVENT_PARAM_QTY, qtyBuilder.length() > 0 ? qtyBuilder.toString() : "");
        addParam(EVENT_PARAM_PRODUCT_DETAILS_ARRAY, productDetailsArray);
        addParam(EVENT_PARAM_CATEGORY_ID, categoryIdBuilder.length() > 0 ? categoryIdBuilder.toString() : "");
        addParam(EVENT_PARAM_CATEGORY_NAME, categoryNameBuilder.length() > 0 ? categoryNameBuilder.toString() : "");
        addParam(EVENT_PARAM_SUB_CATEGORY_IDS, subCategoryIdBuilder.length() > 0 ? subCategoryIdBuilder.toString() : "");
        addParam(EVENT_PARAM_Sub_Category_Name, subCategoryNameBuilder.length() > 0 ? subCategoryNameBuilder.toString() : "");
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

        postEvent(EVENT_NAME_CHECKOUT_COMPLETED);
    }

    private Object getPrescriptionImagesAndIds(List<MStarUploadPrescription> prescriptionList) {
        ArrayList<String> imageOrIdList = new ArrayList<>();
        for (MStarUploadPrescription prescription : prescriptionList) {
            imageOrIdList.add(prescription.getUploadedPrescriptionId());
        }
        return imageOrIdList.toString();
    }

    public void paymentFailureEvent(Context context, String orderId, String webEngageJusPayOrderStatus, double jusPayOrderAmount) {
        postParam = new HashMap<>();
        addParam(EVENT_PARAM_AMOUNT, String.format(Locale.getDefault(), "%.2f", jusPayOrderAmount));
        addParam(EVENT_PARAM_REASON, checkEmpty(webEngageJusPayOrderStatus));
        addParam(EVENT_PARAM_TRANSACTION_ID, checkEmpty(orderId));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

        postEvent(EVENT_NAME_PAYMENT_FAILURE);
    }

    public void m2OrderRequestPlaced(Context context, BasePreference basePreference, MStarCartDetails cartDetails, MStarAddressModel addressModel, List<MStarUploadPrescription> prescriptionList, MStarAddressModel customerBillingAddress) {
        postParam = new HashMap<>();
        MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        String firstName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "";
        String lastName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "";

        addParam(EVENT_PARAM_CUSTOMER_NAME, String.format("%s %s", firstName, lastName));
        addParam(EVENT_PARAM_NEW_BILLING_INFORMATION, customerBillingAddress != null ? new Gson().toJson(customerBillingAddress) : addressModel != null ? new Gson().toJson(addressModel) : "");
        addParam(EVENT_PARAM_NEW_SHIPPING_INFORMATION, addressModel != null ? new Gson().toJson(addressModel) : "");
        addParam(EVENT_PARAM_NO_OF_PRESCRIPTIONS, prescriptionList != null && prescriptionList.size() > 0 ? Integer.toString(prescriptionList.size()) : "");
        addParam(EVENT_PARAM_ORDER_ID, "");
        addParam(EVENT_PARAM_PRESCRIPTION_IMAGE, prescriptionList != null ? getPrescriptionImagesAndIds(prescriptionList) : "");
        addParam(EVENT_PARAM_NEW_CART_DETAILS, getProductDetailArray(cartDetails));
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

        postEvent(EVENT_NAME_M2_ORDER_REQUEST_PLACED);
    }

    public void subCategoryViewedEvent(Context context, MstarCategoryDetails productListResponse) {
        if (productListResponse != null) {
            postParam = new HashMap<>();
            StringBuilder subCategoryIdBuilder = new StringBuilder();
            StringBuilder subCategoryNameBuilder = new StringBuilder();
            for (MstarSubCategoryResult mstarSubCategoryResult : productListResponse.getSubCategories()) {
                if (subCategoryIdBuilder.length() > 0)
                    subCategoryIdBuilder.append(",").append(mstarSubCategoryResult.getId());
                else
                    subCategoryIdBuilder.append(mstarSubCategoryResult.getId());

                if (subCategoryNameBuilder.length() > 0)
                    subCategoryNameBuilder.append(",").append(checkEmpty(mstarSubCategoryResult.getName()));
                else
                    subCategoryNameBuilder.append(checkEmpty(mstarSubCategoryResult.getName()));
            }
            addParam(EVENT_PARAM_CATEGORY_ID, Integer.toString(productListResponse.getId()));
            addParam(EVENT_PARAM_CATEGORY_NAME, checkEmpty(productListResponse.getName()));
            addParam(EVENT_PARAM_ITEM_COUNT, productListResponse.getInstockProductCount());
            addParam(EVENT_PARAM_SUB_CATEGORY_ID, subCategoryIdBuilder.toString());
            addParam(EVENT_PARAM_Sub_Category_Name, subCategoryNameBuilder.toString());
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            postEvent(EVENT_NAME_SUB_CATEGORY_VIEWED);
        }
    }

    public void reportEventManufacturerViewed(Context context, MstarManufacturerDetails productListResponse) {
        if (productListResponse != null) {
            postParam = new HashMap<>();
            MStarProductDetails productDetails = productListResponse.getProducts() != null && productListResponse.getProducts().size() > 0 ? productListResponse.getProducts().get(0) : new MStarProductDetails();
            String manufacturerName = productDetails != null && productDetails.getManufacturer() != null && !TextUtils.isEmpty(productDetails.getManufacturer().getName()) ? productDetails.getManufacturer().getName() : "";
            String manufacturerId = productDetails != null && productDetails.getManufacturer() != null ? Integer.toString(productDetails.getManufacturer().getId()) : "";
            addParam(EVENT_PARAM_MANUFACTURER_NAME, manufacturerName);
            addParam(EVENT_PARAM_MANUFACTURER_ID, manufacturerId);
            addParam(EVENT_PARAM_ITEM_COUNT, productListResponse.getInstockProductCount());
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            postEvent(EVENT_NAME_MANUFATURER_VIEWED);
        }
    }

    public void categoryViewedEvent(Context context, MstarCategoryDetails mstarCategoryDetails) {
        if (mstarCategoryDetails != null) {
            postParam = new HashMap<>();
            addParam(EVENT_PARAM_CATEGORY_ID, Integer.toString(mstarCategoryDetails.getId()));
            addParam(EVENT_PARAM_CATEGORY_NAME, mstarCategoryDetails.getName());
            addParam(EVENT_PARAM_ITEM_COUNT, mstarCategoryDetails.getInstockProductCount());
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));

            postEvent(EVENT_NAME_CATEGORY_VIEWED);
        }
    }


    /***************************************----ALTERNATE CART EVENT START----***********************************************/

    public void updateAlternateCartEvent(MStarCartDetails cartDetails) {
        postParam = new HashMap<>();
        addParam(EVENT_PARAM_ORIGINAL_PRODUCT, getProductList(cartDetails, false));//Before update
        addParam(EVENT_PARAM_ALTERNATE_CART_PRODUCT_ARRAY, getProductList(cartDetails, true)); //After Update
        addParam(EVENT_PARAM_ORIGINAL_AMOUNT, PaymentHelper.getOriginalCartAmount());
        addParam(EVENT_PARAM_ALTR_CART_AMOUNT, cartDetails.getNetPayableAmount());
        addParam(EVENT_PARAM_ALTR_PRODUCT_COUNT, getAlternateProductCount(cartDetails));
        postEvent(EVENT_NAME_ALTERNATE_CART);
    }

    private int getAlternateProductCount(MStarCartDetails cartDetails) {
        int i = 0;
        for (MStarProductDetails productDetails : cartDetails.getLines()) {
            if (productDetails.isAlternateSwitched()) {
                i = i + 1;
            }
        }
        return i;
    }

    private Object getProductList(MStarCartDetails cartDetails, boolean isOriginalProductNeeded) {
        ArrayList<HashMap<String, Object>> productDetailsArray = new ArrayList<>();
        for (MStarProductDetails productDetails : cartDetails.getLines()) {
            CartAlternateAndOrignalProductWebEngageModel productModel = new CartAlternateAndOrignalProductWebEngageModel();
            if (isOriginalProductNeeded) {
                productModel.setProductCode(String.valueOf(isOriginalProductNeeded ? productDetails.getProductCode() : 0/*todo productDetails.getAlternateProductCode() need to add from cart details */));
                productModel.setProductQuantity(isOriginalProductNeeded ? productDetails.getCartQuantity() : 0/*todo productDetails.getAlternateProductQuantity() need to add from cart details */);
            }
            productModel.setProductName(isOriginalProductNeeded ? productDetails.getDisplayName() : productDetails.getAlternateProductName());
            productModel.setProductPrice(isOriginalProductNeeded ? productDetails.getLineMrp().doubleValue() : productDetails.getAlternateProductMrp().doubleValue());
            String jsonObject = new Gson().toJson(productModel);
            Type cartItemType = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            HashMap<String, Object> productDetail = new Gson().fromJson(jsonObject, cartItemType);
            productDetailsArray.add(productDetail);
        }
        return productDetailsArray;
    }

    /***************************************----ALTERNATE CART EVENT END----*********************************************/

    private String checkEmpty(String value) {
        if (!TextUtils.isEmpty(value)) {
            return value;
        } else return "";
    }

    private ArrayList<HashMap<String, Object>> getProductDetailArray(MStarCartDetails mStarCartDetails) {
        ArrayList<HashMap<String, Object>> productDetailsArray = new ArrayList<>();
        if (mStarCartDetails != null && mStarCartDetails.getLines() != null && mStarCartDetails.getLines().size() > 0) {
            for (MStarProductDetails items : mStarCartDetails.getLines()) {
                HashMap<String, Object> productDetail = new HashMap<>();
                productDetail.put(EVENT_PARAM_SKU_CODE, items.getProductCode());
                productDetail.put(EVENT_PARAM_PRODUCT_NAME, checkEmpty(items.getDisplayName()));
                productDetail.put(EVENT_PARAM_TYPE_OF_PRODUCT, checkEmpty(items.getProductType()));
                productDetail.put(EVENT_PARAM_QTY, items.getCartQuantity());
                productDetail.put(EVENT_PARAM_PRICE, items.getSellingPrice());
                productDetailsArray.add(productDetail);
            }
        }
        return productDetailsArray;
    }

    private ArrayList<HashMap<String, Object>> getProductDetailList(MStarCartDetails mStarCartDetails, List<MStarProductDetails> productDetailList) {
        ArrayList<HashMap<String, Object>> productDetailsArray = new ArrayList<>();
        if (productDetailList != null && productDetailList.size() > 0) {
            for (MStarProductDetails items : productDetailList) {
                HashMap<String, Object> productDetail = new HashMap<>();
                productDetail.put(EVENT_PARAM_SKU_CODE, items.getProductCode());
                productDetail.put(EVENT_PARAM_PRODUCT_NAME, checkEmpty(items.getDisplayName()));
                productDetail.put(EVENT_PARAM_TYPE_OF_PRODUCT, checkEmpty(items.getProductType()));
                productDetail.put(EVENT_PARAM_QUANTITY, getCartQuantity(mStarCartDetails, items.getProductCode()));
                productDetail.put(EVENT_PARAM_PRICE, items.getSellingPrice());
                productDetail.put(EVENT_PARAM_CATEGORY_ID, getCategoryId(items.getCategories(), 0));
                productDetail.put(EVENT_PARAM_CATEGORY, getCategoryName(items.getCategories(), 0));
                productDetail.put(EVENT_PARAM_SUB_CATEGORY_ID, getCategoryId(items.getCategories(), 1));
                productDetail.put(EVENT_PARAM_SUB_CATEGORY, getCategoryName(items.getCategories(), 1));
                productDetail.put(EVENT_PARAM_GENERIC_NAME, items.getGeneric() != null ? checkEmpty(items.getGeneric().getName()) : "");
                productDetail.put(EVENT_PARAM_STRENGTH, checkEmpty(items.getDosage()));
                productDetail.put(EVENT_PARAM_FORMULATIONS, checkEmpty(items.getFormulationType()));

                productDetailsArray.add(productDetail);
            }
        }
        return productDetailsArray;
    }

    private int getCartQuantity(MStarCartDetails mStarCartDetails, int productCode) {
        if (mStarCartDetails != null && mStarCartDetails.getLines() != null && mStarCartDetails.getLines().size() > 0) {
            for (MStarProductDetails details : mStarCartDetails.getLines()) {
                if (details.getProductCode() == productCode)
                    return details.getCartQuantity();
            }
        }
        return 0;
    }

    private String getAddress(List<String> addressList) {
        StringBuilder stringBuilder = new StringBuilder();
        if (addressList != null && addressList.size() > 0) {
            for (String address : addressList) {
                if (stringBuilder.length() > 1)
                    stringBuilder.append(",").append(address);
                else
                    stringBuilder.append(address);
            }
        }
        return stringBuilder.toString();
    }

    private String getGenderValue(String gender) {
        switch (gender) {
            case MALE_VALUE:
                return TEXT_MALE;
            case FEMALE_VALUE:
                return TEXT_FEMALE;
            case OTHERS_VALUE:
                return TEXT_OTHERS;
            default:
                return "";
        }
    }

    private String getOrderType(List<MStarProductDetails> cartLineItems) {
        String orderType = "";
        boolean non_prescription = false;
        boolean prescription = false;

        if (cartLineItems != null && cartLineItems.size() > 0) {
            for (MStarProductDetails cartItem : cartLineItems) {
                if (!TextUtils.isEmpty(cartItem.getProductType())) {
                    switch (cartItem.getProductType()) {
                        case "P":
                            prescription = true;
                            break;
                        case "O":
                            non_prescription = true;
                            break;
                    }
                }
            }
            orderType = (prescription && non_prescription) ? MIXED : (prescription ? PRESCRIPTION : (non_prescription ? OTC : ""));
        }
        return orderType;
    }

    private String getDiscountValue(double value) {
        return df.format(value);
    }

    private String getCategoryName(List<MStarCategory> categoryList, int level) {
        String categoryName = "";
        if (categoryList != null && categoryList.size() > 0) {
            MStarCategory mStarCategory = categoryList.get(0);
            if (mStarCategory != null && mStarCategory.getBreadCrumbs() != null && mStarCategory.getBreadCrumbs().size() > 0) {
                List<MStarBreadCrumb> mStarBreadCrumbs = mStarCategory.getBreadCrumbs();
                for (MStarBreadCrumb breadCrumb : mStarBreadCrumbs) {
                    if (breadCrumb.getLevel() == level) {
                        return breadCrumb.getName();
                    }
                }
            }
        }
        return categoryName;
    }

    private String getCategoryId(List<MStarCategory> categoryList, int level) {
        String categoryId = "";
        if (categoryList != null && categoryList.size() > 0) {
            MStarCategory mStarCategory = categoryList.get(0);
            if (mStarCategory != null && mStarCategory.getBreadCrumbs() != null && mStarCategory.getBreadCrumbs().size() > 0) {
                List<MStarBreadCrumb> mStarBreadCrumbs = mStarCategory.getBreadCrumbs();
                for (MStarBreadCrumb breadCrumb : mStarBreadCrumbs) {
                    if (breadCrumb.getLevel() == level) {
                        return Integer.toString(breadCrumb.getId());
                    }
                }
            }
        }
        return categoryId;
    }

    private String getCategoryLevelFromAlgoliaResult(MstarAlgoliaResult algoliaHitResults, String level) {
        if (algoliaHitResults != null && algoliaHitResults.getCategoryTreeList() != null && algoliaHitResults.getCategoryTreeList().size() > 0) {
            for (Map.Entry<String, ArrayList<Object>> entry : algoliaHitResults.getCategoryTreeList().entrySet()) {
                String key = entry != null && !TextUtils.isEmpty(entry.getKey()) ? entry.getKey() : "";
                if (key.equalsIgnoreCase(level)) {
                    ArrayList<Object> levelList = entry != null && entry.getValue() != null && entry.getValue().size() > 0 ? entry.getValue() : new ArrayList<Object>();
                    if (levelList != null && levelList.size() > 0 && !TextUtils.isEmpty(levelList.get(0).toString().trim())) {
                        String[] categoryList = levelList.get(0).toString().split("///");
                        return categoryList.length > 0 ? !TextUtils.isEmpty(Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1)) ? Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1) : "" : "";
                    }
                }
            }
        }
        return "";
    }

    private String getLevelBasedOnProductType(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_P) ? AppConstant.LEVEL_1 : algoliaHitResults.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? AppConstant.LEVEL_2 : "";
    }

    private int getCategoryIdIndexFromAlgoliaResult(MstarAlgoliaResult algoliaHitResults, String categoryName) {
        if (algoliaHitResults.getCategories() != null && algoliaHitResults.getCategories().size() > 0) {
            int i = 0;
            for (String name : algoliaHitResults.getCategories()) {
                if (name.equals(categoryName))
                    return i;
                i++;
            }
        }
        return -1;
    }


    /************************************************Medicine Module Events End*************************************************************

     /************************************************Diagnostic Module Events Start**********************************************************/

    public void diagnosticsRegistrationEvent(BasePreference basePreference, Context context) {
        postParam = new HashMap<>();
        diagnosticUserDetail = getDiagnosticUserDetail(basePreference);
        addParam(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_NAME, diagnosticCustomerName());
        addParam(EVENT_PARAM_PHONE_NUMBER, diagnosticCustomerPhoneNo());
        addParam(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_EMAIL, diagnosticCustomerEmail());
        if (context != null)
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postEvent(EVENT_NAME_REGISTER_FOR_DIAGNOSTICS);
    }

    public void diagnosticEnterPinCodeEvent(BasePreference basePreference, String city, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));

        postEvent(EVENT_NAME_ENTERS_PINCODE);
    }

    public void diagnosticTestAddedEvent(BasePreference basePreference, String city, List<Test> selectedTestList, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));

        ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
        if (selectedTestList.size() > 0) {
            for (Test test : selectedTestList) {
                String jsonObject = new Gson().toJson(test);
                Type testType = new TypeToken<HashMap<String, Object>>() {
                }.getType();
                HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                testArray.add(testDetail);
            }
        }
        addParam(EVENT_PARAM_TEST_SELECTED, testArray);
        addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());

        postEvent(EVENT_NAME_TEST_ADDED);
    }

    public void diagnosticsCartEvent(BasePreference basePreference, String city, List<Test> selectedTestList, ArrayList<HashMap<String, Object>> labArray, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));

        ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
        if (selectedTestList.size() > 0) {
            for (Test test : selectedTestList) {
                String jsonObject = new Gson().toJson(test);
                Type testType = new TypeToken<HashMap<String, Object>>() {
                }.getType();
                HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                testArray.add(testDetail);
            }
        }
        addParam(EVENT_PARAM_TEST_SELECTED, testArray);
        addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
        addParam(EVENT_PARAM_LIST_OF_LABS_PRICES_AND_DISCOUNDS, labArray);

        postEvent(EVENT_NAME_DIAGNOSTICS_CART);
    }

    public void diagnosticsLabUpdatedEvent(BasePreference basePreference, String city, List<Test> selectedTestList, double valueOfTest, String selectedLab, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));

        ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
        if (selectedTestList.size() > 0) {
            for (Test test : selectedTestList) {
                String jsonObject = new Gson().toJson(test);
                Type testType = new TypeToken<HashMap<String, Object>>() {
                }.getType();
                HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                testArray.add(testDetail);
            }
        }
        addParam(EVENT_PARAM_TEST_SELECTED, testArray);
        addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
        addParam(EVENT_PARAM_VALUE_OF_TESTS, valueOfTest);
        addParam(EVENT_PARAM_LAB_SELECTED, selectedLab);

        postEvent(EVENT_NAME_DIAGNOSTICS_CART_LAB_UPDATED);
    }

    public void packageAddedEvent(BasePreference basePreference, String city, HashMap<String, Object> packageMap, String selectedLab, double valueOfPackage, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));
        addParam(EVENT_PARAM_PACKAGE_SELECTED, packageMap);
        addParam(EVENT_PARAM_LAB_SELECTED, selectedLab);
        addParam(EVENT_PARAM_VALUE_OF_PACKAGE, valueOfPackage);

        postEvent(EVENT_NAME_PACKAGE_ADDED);
    }

    public void patientDetailsEvent(BasePreference basePreference, String city, String age, String gender, String patientName, boolean type, List<Test> selectedTestList, HashMap<String, Object> packageMap, String selectedLab, double value, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));
        addParam(EVENT_PARAM_AGE, age);
        addParam(EVENT_PARAM_GENDER, gender);
        addParam(EVENT_PARAM_PATIENTS_NAME, patientName);
        if (type) {
            ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
            if (selectedTestList.size() > 0) {
                for (Test test : selectedTestList) {
                    String jsonObject = new Gson().toJson(test);
                    Type testType = new TypeToken<HashMap<String, Object>>() {
                    }.getType();
                    HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                    testArray.add(testDetail);
                }
            }
            addParam(EVENT_PARAM_TEST_SELECTED, testArray);
            addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
            addParam(EVENT_PARAM_VALUE_OF_TESTS, value);
        } else {
            addParam(EVENT_PARAM_PACKAGE_SELECTED, packageMap);
            addParam(EVENT_PARAM_VALUE_OF_PACKAGE, value);
        }
        addParam(EVENT_PARAM_LAB_SELECTED, selectedLab);

        postEvent(EVENT_NAME_PATIENT_DETAILS);
    }

    public void collectionAddressEvent(BasePreference basePreference, String city, String age, String gender, String patientName, boolean type, List<Test> selectedTestList, HashMap<String, Object> packageMap, String selectedLab, double value, HashMap<String, Object> addressMap
            , Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));
        addParam(EVENT_PARAM_AGE, age);
        addParam(EVENT_PARAM_GENDER, gender);
        addParam(EVENT_PARAM_PATIENTS_NAME, patientName);
        if (type) {
            ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
            if (selectedTestList.size() > 0) {
                for (Test test : selectedTestList) {
                    String jsonObject = new Gson().toJson(test);
                    Type testType = new TypeToken<HashMap<String, Object>>() {
                    }.getType();
                    HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                    testArray.add(testDetail);
                }
            }
            addParam(EVENT_PARAM_TEST_SELECTED, testArray);
            addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
            addParam(EVENT_PARAM_VALUE_OF_TESTS, value);
        } else {
            addParam(EVENT_PARAM_PACKAGE_SELECTED, packageMap);
            addParam(EVENT_PARAM_VALUE_OF_PACKAGE, value);
        }
        addParam(EVENT_PARAM_LAB_SELECTED, selectedLab);
        addParam(EVENT_PARAM_COLLECTION_ADDRESS, addressMap);

        postEvent(EVENT_NAME_COLLECTION_ADDRESS);
    }

    public void slotSelectionEvent(BasePreference basePreference, String city, String age, String gender, String patientName, boolean type, List<Test> selectedTestList, HashMap<String, Object> packageMap, String selectedLab, double value, HashMap<String, Object> addressMap, String slotSelectDay, String slotSelectTime,
                                   Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));
        addParam(EVENT_PARAM_AGE, age);
        addParam(EVENT_PARAM_GENDER, gender);
        addParam(EVENT_PARAM_PATIENTS_NAME, patientName);
        if (type) {
            ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
            if (selectedTestList.size() > 0) {
                for (Test test : selectedTestList) {
                    String jsonObject = new Gson().toJson(test);
                    Type testType = new TypeToken<HashMap<String, Object>>() {
                    }.getType();
                    HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                    testArray.add(testDetail);
                }
            }
            addParam(EVENT_PARAM_TEST_SELECTED, testArray);
            addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
            addParam(EVENT_PARAM_VALUE_OF_TESTS, value);
        } else {
            addParam(EVENT_PARAM_PACKAGE_SELECTED, packageMap);
            addParam(EVENT_PARAM_VALUE_OF_PACKAGE, value);
        }
        addParam(EVENT_PARAM_LAB_SELECTED, selectedLab);
        addParam(EVENT_PARAM_COLLECTION_ADDRESS, addressMap);
        addParam(EVENT_PARAM_SLOT_SELECETD_DAY, slotSelectDay);
        addParam(EVENT_PARAM_SLOT_SELECETD_TIME, slotSelectTime);

        postEvent(EVENT_NAME_SLOT_SELECTION);
    }

    public void orderPlacedEvent(BasePreference basePreference, String city, Bundle orderDetailBundle, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));
        if (orderDetailBundle != null) {
            addParam(EVENT_PARAM_AGE, orderDetailBundle.containsKey(DiagnosticConstant.KEY_AGE) ? orderDetailBundle.getString(DiagnosticConstant.KEY_AGE) : "");
            addParam(EVENT_PARAM_GENDER, orderDetailBundle.containsKey(DiagnosticConstant.KEY_GENDER) ? orderDetailBundle.getString(DiagnosticConstant.KEY_GENDER) : "");
            addParam(EVENT_PARAM_PATIENTS_NAME, orderDetailBundle.containsKey(DiagnosticConstant.KEY_PATIENT_NAME) ? orderDetailBundle.getString(DiagnosticConstant.KEY_PATIENT_NAME) : "");
            addParam(EVENT_PARAM_LAB_SELECTED, orderDetailBundle.containsKey(DiagnosticConstant.KEY_LAB_NAME) ? orderDetailBundle.getString(DiagnosticConstant.KEY_LAB_NAME) : "");
            addParam(EVENT_PARAM_SLOT_SELECETD_DAY, orderDetailBundle.containsKey(DiagnosticConstant.KEY_SELECTED_DAY) ? orderDetailBundle.getString(DiagnosticConstant.KEY_SELECTED_DAY) : "");
            addParam(EVENT_PARAM_SLOT_SELECETD_TIME, orderDetailBundle.containsKey(DiagnosticConstant.KEY_SELECTED_TIME) ? orderDetailBundle.getString(DiagnosticConstant.KEY_SELECTED_TIME) : "");
            addParam(EVENT_PARAM_ORDER_SUCCESSFUL, orderDetailBundle.containsKey(DiagnosticConstant.KEY_ORDER_SUCCESS) ? orderDetailBundle.getString(DiagnosticConstant.KEY_ORDER_SUCCESS) : "");
            addParam(EVENT_PARAM_ORDER_ID, orderDetailBundle.containsKey(DiagnosticConstant.KEY_ORDER_ID) ? orderDetailBundle.getString(DiagnosticConstant.KEY_ORDER_ID) : "");
            addParam(EVENT_PARAM_ORDER_AMOUNT, orderDetailBundle.containsKey(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) ? orderDetailBundle.getDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) : 0);
            addParam(EVENT_PARAM_ORDER_DATE, orderDetailBundle.containsKey(DiagnosticConstant.KEY_ORDER_DATE) ? orderDetailBundle.getString(DiagnosticConstant.KEY_ORDER_DATE) : "");
            addParam(EVENT_PARAM_COLLECTION_ADDRESS, orderDetailBundle.containsKey(DiagnosticConstant.KEY_CUSTOMER_ADDRESS) ? orderDetailBundle.getSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS) : new HashMap<>());

            List<Test> testList = orderDetailBundle.containsKey(DiagnosticConstant.KEY_SELECTED_TEST_LIST) ? (List<Test>) orderDetailBundle.getSerializable(DiagnosticConstant.KEY_SELECTED_TEST_LIST) : new ArrayList<Test>();
            ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
            String type = "";
            if (testList != null && testList.size() > 0) {
                for (Test test : testList) {
                    type = test.getType();
                    String jsonObject = new Gson().toJson(test);
                    Type testType = new TypeToken<HashMap<String, Object>>() {
                    }.getType();
                    HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                    testArray.add(testDetail);
                }
            }
            if (type.equalsIgnoreCase(DiagnosticConstant.TEST) || type.equalsIgnoreCase(DiagnosticConstant.TEST_TYPE_PROFILE)) {
                addParam(EVENT_PARAM_TEST_SELECTED, testArray);
                addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
                addParam(EVENT_PARAM_VALUE_OF_TESTS, orderDetailBundle.containsKey(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) ? orderDetailBundle.getDouble(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) : 0);
            } else {
                addParam(EVENT_PARAM_PACKAGE_SELECTED, testArray);
                addParam(EVENT_PARAM_VALUE_OF_PACKAGE, orderDetailBundle.containsKey(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) ? orderDetailBundle.getDouble(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) : 0);
            }
        }
        postEvent(EVENT_NAME_DIAGNOSTIC_ORDER_PLACED);
    }

    public void reportGenerateEvent(BasePreference basePreference, String city, List<Test> selectedTest, String type, Bundle orderDetailBundle, Context context) {
        postParam = new HashMap<>();
        postParam.putAll(getDiagnosticUserDetailsMap(basePreference, city, context));
        if (orderDetailBundle != null) {
            addParam(EVENT_PARAM_AGE, orderDetailBundle.containsKey(DiagnosticConstant.KEY_AGE) ? orderDetailBundle.getString(DiagnosticConstant.KEY_AGE) : "");
            addParam(EVENT_PARAM_GENDER, orderDetailBundle.containsKey(DiagnosticConstant.KEY_GENDER) ? orderDetailBundle.getString(DiagnosticConstant.KEY_GENDER) : "");
            addParam(EVENT_PARAM_PATIENTS_NAME, orderDetailBundle.containsKey(DiagnosticConstant.KEY_PATIENT_NAME) ? orderDetailBundle.getString(DiagnosticConstant.KEY_PATIENT_NAME) : "");
            addParam(EVENT_PARAM_LAB_SELECTED, orderDetailBundle.containsKey(DiagnosticConstant.KEY_LAB_NAME) ? orderDetailBundle.getString(DiagnosticConstant.KEY_LAB_NAME) : "");
            addParam(EVENT_PARAM_SLOT_SELECETD_DAY, orderDetailBundle.containsKey(DiagnosticConstant.KEY_SELECTED_DAY) ? orderDetailBundle.getString(DiagnosticConstant.KEY_SELECTED_DAY) : "");
            addParam(EVENT_PARAM_SLOT_SELECETD_TIME, orderDetailBundle.containsKey(DiagnosticConstant.KEY_SELECTED_TIME) ? orderDetailBundle.getString(DiagnosticConstant.KEY_SELECTED_TIME) : "");
            addParam(EVENT_PARAM_ORDER_SUCCESSFUL, orderDetailBundle.containsKey(DiagnosticConstant.KEY_ORDER_SUCCESS) ? orderDetailBundle.getString(DiagnosticConstant.KEY_ORDER_SUCCESS) : "");
            addParam(EVENT_PARAM_ORDER_ID, orderDetailBundle.containsKey(DiagnosticConstant.KEY_ORDER_ID) ? orderDetailBundle.getString(DiagnosticConstant.KEY_ORDER_ID) : "");
            addParam(EVENT_PARAM_ORDER_AMOUNT, orderDetailBundle.containsKey(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) ? orderDetailBundle.getDouble(DiagnosticConstant.KEY_DIAGNOSTIC_TOTAL_AMOUNT) : 0);
            addParam(EVENT_PARAM_ORDER_DATE, orderDetailBundle.containsKey(DiagnosticConstant.KEY_ORDER_DATE) ? orderDetailBundle.getString(DiagnosticConstant.KEY_ORDER_DATE) : "");
            addParam(EVENT_PARAM_COLLECTION_ADDRESS, orderDetailBundle.containsKey(DiagnosticConstant.KEY_CUSTOMER_ADDRESS) ? orderDetailBundle.getSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS) : new HashMap<>());

            ArrayList<HashMap<String, Object>> testArray = new ArrayList<>();
            if (selectedTest != null && selectedTest.size() > 0) {
                for (Test test : selectedTest) {
                    String jsonObject = new Gson().toJson(test);
                    Type testType = new TypeToken<HashMap<String, Object>>() {
                    }.getType();
                    HashMap<String, Object> testDetail = new Gson().fromJson(jsonObject, testType);
                    testArray.add(testDetail);
                }
            }
            if (type.equalsIgnoreCase(DiagnosticConstant.TEST)) {
                addParam(EVENT_PARAM_TEST_SELECTED, testArray);
                addParam(EVENT_PARAM_NUMBER_OF_TESTS, testArray.size());
                addParam(EVENT_PARAM_VALUE_OF_TESTS, orderDetailBundle.containsKey(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) ? orderDetailBundle.getDouble(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) : 0);
            } else {
                addParam(EVENT_PARAM_PACKAGE_SELECTED, testArray);
                addParam(EVENT_PARAM_VALUE_OF_PACKAGE, orderDetailBundle.containsKey(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) ? orderDetailBundle.getDouble(DiagnosticConstant.KEY_VALUE_OF_TEST_OR_PACKAGE) : 0);
            }
        }
        addParam(EVENT_PARAM_REPORTS_GENERATED, DiagnosticConstant.YES);

        postEvent(EVENT_NAME_REPORTS_GENERATED);
    }

    public void testOrPackageViewed(BasePreference basePreference, String testOrPackageName, String type, String labs, String prices, String source, Context context) {
        postParam = new HashMap<>();
        diagnosticUserDetail = getDiagnosticUserDetail(basePreference);
        addParam(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_NAME, diagnosticCustomerName());
        addParam(EVENT_PARAM_PHONE_NUMBER, diagnosticCustomerPhoneNo());
        addParam(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_EMAIL, diagnosticCustomerEmail());
        addParam(EVENT_PARAM_PINCODE, basePreference.getDiagnosticPinCode());
        addParam(EVENT_PARAM_CITY, basePreference.getDiagnosticCity());
        addParam(EVENT_PARAM_TEST_OR_PACKAGE_NAME, testOrPackageName);
        addParam(EVENT_PARAM_TYPE, type);
        addParam(EVENT_PARAM_LABS_AVAILABLE, labs);
        addParam(EVENT_PARAM_PRICES, prices);
        addParam(EVENT_PARAM_SOURCE, source);
        if (context != null)
            addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postEvent(EVENT_NAME_TEST_PACKAGE_VIEWED);

    }

    private JustDocUserResponse getDiagnosticUserDetail(BasePreference basePreference) {
        return new Gson().fromJson(basePreference.getDiagnosticLoginResponse(), JustDocUserResponse.class);
    }

    private String diagnosticCustomerName() {
        return diagnosticUserDetail != null && !TextUtils.isEmpty(diagnosticUserDetail.getName()) ? diagnosticUserDetail.getName() : "";
    }

    private String diagnosticCustomerPhoneNo() {
        return diagnosticUserDetail != null && !TextUtils.isEmpty(diagnosticUserDetail.getMobile()) ? diagnosticUserDetail.getMobile() : "";
    }

    private String diagnosticCustomerEmail() {
        return diagnosticUserDetail != null && !TextUtils.isEmpty(diagnosticUserDetail.getEmail()) ? diagnosticUserDetail.getEmail() : "";
    }

    private Map<String, Object> getDiagnosticUserDetailsMap(BasePreference basePreference, String city, Context mContext) {
        diagnosticUserDetail = getDiagnosticUserDetail(basePreference);
        Map<String, Object> userMap = new HashMap<>();
        userMap.put(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_NAME, diagnosticCustomerName());
        userMap.put(EVENT_PARAM_PHONE_NUMBER, diagnosticCustomerPhoneNo());
        userMap.put(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_EMAIL, diagnosticCustomerEmail());
        userMap.put(EVENT_PARAM_PINCODE, basePreference.getDiagnosticPinCode());
        userMap.put(EVENT_PARAM_CITY, city);
        userMap.put(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(mContext));
        return userMap;
    }

    /****************************************************Diagnostic Module Events End*********************************************************************

     /***************************************************Consultation Module Events Start****************************************************************/

    private static HashMap<String, Object> commonUserMap(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_NAME, TextUtils.isEmpty(engageModel.getCustomerName()) ? "" : engageModel.getCustomerName());
        map.put(EVENT_PARAM_PHONE_NUMBER, TextUtils.isEmpty(engageModel.getPhone()) ? "" : engageModel.getPhone());
        map.put(EVENT_PARAM_EMAIL, TextUtils.isEmpty(engageModel.getEmail()) ? "" : engageModel.getEmail());
        map.put(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        return map;
    }

    public void registeredForConsultation(WebEngageModel engageModel, Context context) {
        WebEngage.get().analytics().track(EVENT_NAME_REGISTERED_FOR_CONSULTATION, commonUserMap(engageModel, context));
    }

    public void loginForConsultationEvent(WebEngageModel engageModel, Context context) {
        WebEngage.get().analytics().track(EVENT_NAME_LOGIN_FOR_CONSULTATION, commonUserMap(engageModel, context));
    }

    public void consultationInitiatedEvent() {
        WebEngage.get().analytics().track(EVENT_NAME_CONSULTATION_INITIATED);
    }


    public void patientIdentifiedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        WebEngage.get().analytics().track(EVENT_NAME_PATIENT_IDENTIFIED, map);
    }

    public void specialitySelectedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        WebEngage.get().analytics().track(EVENT_NAME_SPECIALITY_SELECTED, map);
    }

    public void SymptomEnteredEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        WebEngage.get().analytics().track(EVENT_NAME_SYMPTOMS_ENTERED, map);
    }

    public void modeSelectedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        WebEngage.get().analytics().track(EVENT_NAME_MODE_SELECTED, map);
    }

    public void consultationTypeSelectedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        WebEngage.get().analytics().track(EVENT_NAME_CONSULTATION_TYPE_SELECTED, map);
    }

    public void paymentModeSelectedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PAYMENT_MODE, engageModel.getPaymentMode());
        WebEngage.get().analytics().track(EVENT_NAME_PAYMENT_MODE_SELECTED, map);
    }

    public void paymentSuccessOrderPlacedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PAYMENT_MODE, engageModel.getPaymentMode());
        map.put(EVENT_PARAM_PAYMENT_STATUS, engageModel.getPaymentStatus());
        WebEngage.get().analytics().track(EVENT_NAME_PAYMENT_SUCCESSFULLY_RECEIVED_ORDER_PLACED, map);
        WebEngageHelper.getInstance().notifyDoctorWhenPatientAvailableEvent();

    }

    public void consultationCompletePrescriptionReadyEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PRESCRIPTION_STATUS, engageModel.getPrescriptionStatus());
        WebEngage.get().analytics().track(EVENT_NAME_CONSULTATION_COMPLTE_PRESCRIPTION_READY, map);
    }

    public void ifLabTestPrescribedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PRESCRIPTION_STATUS, engageModel.getPrescriptionStatus());
        map.put(EVENT_PARAM_Lab_TEST_PRESCRIBED, engageModel.getLabTestPrescribed());
        map.put(EVENT_PARAM_Lab_TEST_NAME, engageModel.getLabTestName());
        WebEngage.get().analytics().track(EVENT_NAME_LABTEST_IS_PRESCRIBED, map);
    }

    public void followUpEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PRESCRIPTION_STATUS, engageModel.getPrescriptionStatus());
        map.put(EVENT_PARAM_Lab_TEST_PRESCRIBED, engageModel.getLabTestPrescribed());
        map.put(EVENT_PARAM_Lab_TEST_NAME, engageModel.getLabTestName());
        map.put(EVENT_PARAM_Lab_FOLLOW_UP_STATUS, engageModel.getFollowUpStatus());
        WebEngage.get().analytics().track(EVENT_NAME_FOLLOW_UP, map);

    }

    public void doctorHasRepliedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        WebEngage.get().analytics().track(EVENT_DOCTOR_HAS_REPIED_TO_PATIENT, map);

    }

    private void notifyDoctorWhenPatientAvailableEvent() {
        WebEngage.get().analytics().track(EVENT_NAME_NOTIFIY_DOCTOR_WHEN_PATIENT_AVAILABLE);
    }

    public void ifLabTestOrMedicineIsPrescribed(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PRESCRIPTION_STATUS, engageModel.getPrescriptionStatus());
        map.put(EVENT_PARAM_Lab_TEST_PRESCRIBED, engageModel.getLabTestPrescribed());
        map.put(EVENT_PARAM_Lab_TEST_NAME, engageModel.getLabTestName());
        map.put(EVENT_PARAM_PRESCRIBED_MEDICINE, engageModel.getPrescribedMedicines());
        map.put(EVENT_PARAM_DOSAGE_PRESCRIBED, engageModel.getDosagePrescribed());
        WebEngage.get().analytics().track(EVENT_NAME_IF_MEDICINE_OR_LAB_TEST_IS_PRESCRIBED, map);
    }


    public void ratingEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PRESCRIPTION_STATUS, engageModel.getPrescriptionStatus());
        map.put(EVENT_PARAM_Lab_TEST_PRESCRIBED, engageModel.getLabTestPrescribed());
        map.put(EVENT_PARAM_Lab_TEST_NAME, engageModel.getLabTestName());
        map.put(EVENT_PARAM_Lab_FOLLOW_UP_STATUS, "No");
        map.put(EVENT_PARAM_PRESCRIBED_MEDICINE, engageModel.getPrescribedMedicines());
        map.put(EVENT_PARAM_DOSAGE_PRESCRIBED, engageModel.getDosagePrescribed());
        map.put(EVENT_PARAM_RATING, engageModel.getRating());
        WebEngage.get().analytics().track(EVENT_NAME_RATING, map);
    }

    public void chatConsultationIsCompletedEvent(WebEngageModel engageModel, Context context) {
        HashMap<String, Object> map = new HashMap<>(commonUserMap(engageModel, context));
        map.put(EVENT_PARAM_PATIENT, engageModel.getPatientIdentified());
        map.put(EVENT_PARAM_SPECIALTY, engageModel.getSpeciality());
        map.put(EVENT_PARAM_SYMPTOM, engageModel.getSymptoms());
        map.put(EVENT_PARAM_MODE, engageModel.getMode());
        map.put(EVENT_PARAM_TYPE, engageModel.getType());
        map.put(EVENT_PARAM_PRESCRIPTION_STATUS, engageModel.getPrescriptionStatus());
        map.put(EVENT_PARAM_Lab_TEST_PRESCRIBED, engageModel.getLabTestPrescribed());
        map.put(EVENT_PARAM_Lab_TEST_NAME, engageModel.getLabTestName());
        map.put(EVENT_PARAM_Lab_FOLLOW_UP_STATUS, "No");
        WebEngage.get().analytics().track(EVENT_NAME_CHAT_CONSULTATION_COMPELTED, map);
    }

    public void digTypeSelected(BasePreference basePreference, String type, Context context) {
        postParam = new HashMap<>();
        diagnosticUserDetail = getDiagnosticUserDetail(basePreference);
        addParam(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_NAME, diagnosticCustomerName());
        addParam(EVENT_PARAM_PHONE_NUMBER, diagnosticCustomerPhoneNo());
        addParam(EVENT_PARAM_DIAGNOSTIC_CUSTOMER_EMAIL, diagnosticCustomerEmail());
        addParam(EVENT_PARAM_TYPE, type);
        addParam(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        postEvent(EVENT_NAME_DIAGTYPE_SELECTED);
    }
    /*********************************************************Consultation Module Events End********************************************************************************/
}
