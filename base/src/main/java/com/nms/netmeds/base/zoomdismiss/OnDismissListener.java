

package com.nms.netmeds.base.zoomdismiss;

interface OnDismissListener {

    void onDismiss();
}
