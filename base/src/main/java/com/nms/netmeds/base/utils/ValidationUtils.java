package com.nms.netmeds.base.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.nms.netmeds.base.R;

public class ValidationUtils {

    public static boolean checkLoginPassword(Context context, TextView element, String blankMsg, String errMsg, TextView textView) {
        String value = element.getText().toString();
        if (TextUtils.isEmpty(value)) {
            textView.setText(blankMsg);
            return false;
        } else if (value.length() < 6) {
            textView.setText(errMsg);
            return false;
        } else if (checkAlphanumeric(value)) {
            textView.setText(context.getResources().getString(R.string.error_lngthpassword));
            return false;
        }
        textView.setText(null);
        return true;
    }

    public static boolean checkLoginSetPassword(EditText element, TextInputLayout inputLayout, boolean isAlphanumericCheck, String blankMessage, String errorMessage) {
        String value = element.getText().toString();
        if (value.contains(" ")) {
            inputLayout.setError(blankMessage);
            return false;
        }
        if (TextUtils.isEmpty(value)) {
            inputLayout.setError(blankMessage);
            return false;
        }
        if (value.length() < 8) {
            inputLayout.setError(errorMessage);
            return false;
        }
        if (isAlphanumericCheck) {
            if (checkAlphanumeric(value)) {
                inputLayout.setError(blankMessage);
                return false;
            }
        }
        inputLayout.setError(null);
        return true;
    }

    private static boolean checkAlphanumeric(String str) {
        boolean alpha = false;
        boolean numeric = false;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c))
                numeric = true;
            if (Character.isLetter(c))
                alpha = true;
        }
        return !numeric || !alpha;
    }

    public static boolean checkText(TextView element, boolean isRequired, String blankMsg, int minLength, int maxLength, TextView errorText) {
        element.setError(null);
        String value = element.getText().toString().trim();
        int length = element.getText().toString().length();

        if (TextUtils.isEmpty(value)) {
            if (isRequired) {
                // element.requestFocus();
                errorText.setText(blankMsg);
                return false;
            } else {
                errorText.setText(null);
                return true;
            }
        }
        if (length > maxLength) {
            errorText.setText(String.format("Maximum %s chars allowed", String.valueOf(maxLength)));
            return false;
        }
        if (length < minLength) {
            errorText.setText(String.format("Minimum %s chars required", String.valueOf(minLength)));
            return false;
        }
        errorText.setText(null);
        return true;
    }

    public static boolean checkTextInputLayoutPhonenumber(EditText element, boolean isRequired, String blankMsg, int minLength, int maxLength, TextInputLayout inputUsername) {
        element.setError(null);
        String value = element.getText().toString().trim();
        int length = element.getText().toString().length();

        if (TextUtils.isEmpty(value)) {
            if (isRequired) {
                inputUsername.setError(blankMsg);
                return false;
            } else {
                inputUsername.setError(null);
                return true;
            }
        }
        if (length > maxLength) {
            inputUsername.setError("Maximum " + String.valueOf(maxLength) + " chars allowed");
            return false;
        }
        if (length < minLength) {
            inputUsername.setError("Minimum " + String.valueOf(minLength) + " chars required");
            return false;
        }
        inputUsername.setError(null);
        return true;
    }

    public static boolean userName(EditText element, boolean isRequired, String blankMsg, int minLength, int maxLength, TextView errorTextView) {
        errorTextView.setText(null);
        String value = element.getText().toString().trim();
        int length = element.getText().toString().length();

        if (TextUtils.isEmpty(value)) {
            if (isRequired) {
                errorTextView.setText(blankMsg);
                return false;
            } else {
                errorTextView.setText(null);
                return true;
            }
        }
        if (length > maxLength) {
            errorTextView.setText(String.format("Maximum %s chars allowed", String.valueOf(maxLength)));
            return false;
        }
        if (length < minLength) {
            errorTextView.setText(String.format("Minimum %s chars required", String.valueOf(minLength)));
            return false;
        }
        errorTextView.setText(null);
        return true;
    }

    public static boolean checkEmailValidation(EditText element, TextInputLayout inputEmail, Context context) {
        String value = element.getText().toString();

        String[] splitValue = null;

        if (value.contains("@"))
            splitValue = value.split("@");

        if (TextUtils.isEmpty(value)) {
            inputEmail.setError(context.getResources().getString(R.string.txt_error_email));
            return false;
        } else if (!isEmail(value)) {
            inputEmail.setError(context.getResources().getString(R.string.txt_error_valid_email));
            return false;
        } else {
            assert splitValue != null;
            if (value.startsWith(".") || value.contains("+") || splitValue[0].endsWith(".") || value.contains("..")) {
                inputEmail.setError(context.getResources().getString(R.string.txt_error_valid_email));
                return false;
            } else {
                inputEmail.setError(null);
                return true;
            }
        }
    }

    public static boolean checkTextInputLayout(EditText element, boolean isRequired, String blankMsg, String spaceError, String continuousWhiteSpaceError, int minLength, int maxLength, TextInputLayout inputUsername) {
        element.setError(null);
        String value = element.getText().toString();
        if (value.endsWith(" ")) {
            value = value.trim();
            element.setText(value.trim());
        }
        if (TextUtils.isEmpty(value)) {
            if (isRequired) {
                // element.requestFocus();
                inputUsername.setError(blankMsg);
                return false;
            } else {
                inputUsername.setError(null);
                return true;
            }
        }
        if (value.startsWith(" ")) {
            inputUsername.setError(spaceError);
            return false;
        }
        if (value.contains("  ")) {
            inputUsername.setError(continuousWhiteSpaceError);
            return false;
        }

        int length = value.length();

        if (length > maxLength) {
            inputUsername.setError("Maximum " + String.valueOf(maxLength) + " chars allowed");
            return false;
        }
        if (length < minLength) {
            inputUsername.setError("Minimum " + String.valueOf(minLength) + " chars required");
            return false;
        }
        inputUsername.setError(null);
        return true;
    }

    private static boolean isEmail(String value) {
        return value != null && android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches();
    }

    public static boolean checkPinCode(EditText element, boolean isRequired, String blankMsg, String errMsg, int minLength, TextView errorText) {
        errorText.setText(null);
        String value = element.getText().toString();
        int length = element.getText().toString().length();

        if (TextUtils.isEmpty(value)) {
            if (isRequired) {
                errorText.requestFocus();
                errorText.setText(blankMsg);
                return false;
            } else {
                errorText.setText(null);
                return true;
            }
        }
        if (length < minLength) {
            errorText.setText(errMsg);
            errorText.requestFocus();
            return false;
        }
        errorText.setText(null);
        return true;
    }

    public static String validateString(String stringValue) {
        return !TextUtils.isEmpty(stringValue) ? stringValue : "";
    }

    public static boolean checkDigit(TextView element, boolean isRequired, String errorMessage, TextView errorText) {
        element.setError(null);
        String value = element.getText().toString().trim();

        if (TextUtils.isEmpty(value)) {
            if (isRequired) {
                errorText.setText(errorMessage);
                return false;
            } else {
                errorText.setText(null);
                return true;
            }
        }
        if (!TextUtils.isDigitsOnly(value)) {
            errorText.setText(errorMessage);
            return false;
        }
        errorText.setText(null);
        return true;
    }

    public static boolean checkIsPasswordSame(String newPassword, String confirmPassword) {
        return newPassword.equals(confirmPassword);
    }

    public static boolean checkConfirmPassword(EditText password, EditText confirmPassword, TextInputLayout inputLayout, String errMsg) {
        String passwordValue = password.getText().toString();
        String confirmPasswordValue = confirmPassword.getText().toString();
        if (!passwordValue.equals(confirmPasswordValue)) {
            inputLayout.setError(errMsg);
            return false;
        }
        inputLayout.setError(null);
        return true;
    }
}
