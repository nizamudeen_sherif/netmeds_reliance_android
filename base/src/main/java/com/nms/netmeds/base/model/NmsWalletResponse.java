package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.NmsWalletResult;

public class NmsWalletResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private NmsWalletResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NmsWalletResult getResult() {
        return result;
    }

    public void setResult(NmsWalletResult result) {
        this.result = result;
    }
}
