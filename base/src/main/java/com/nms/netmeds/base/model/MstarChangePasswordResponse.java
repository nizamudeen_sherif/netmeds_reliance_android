package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarChangePasswordResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private String result;
    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel reason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public MstarBasicResponseFailureReasonTemplateModel getReason() {
        return reason;
    }

    public void setReason(MstarBasicResponseFailureReasonTemplateModel reason) {
        this.reason = reason;
    }
}
