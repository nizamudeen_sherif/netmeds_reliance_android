package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSubstitutionUpdateCartAddparam {
    @SerializedName("drugcode")
    private String drugcode;
    @SerializedName("qty")
    private String qty;

    public String getDrugcode() {
        return drugcode;
    }

    public void setDrugcode(String drugcode) {
        this.drugcode = drugcode;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

}