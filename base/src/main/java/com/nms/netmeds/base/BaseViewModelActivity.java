package com.nms.netmeds.base;

import android.view.View;

import androidx.appcompat.widget.Toolbar;

public abstract class BaseViewModelActivity<VM extends BaseViewModel> extends BaseWebViewActivity {

    protected abstract VM onCreateViewModel();

    protected void toolBarSetUp(Toolbar toolbar) {
        super.toolBarSetUp(toolbar);
        toolbar.setTitle("");
    }

    public <T extends BaseViewModel> void onRetry(final T viewModel) {
        findViewById(R.id.btn_api_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.onRetryClickListener();
            }
        });

        findViewById(R.id.btn_network_reload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.onRetryClickListener();
            }
        });
    }
}
