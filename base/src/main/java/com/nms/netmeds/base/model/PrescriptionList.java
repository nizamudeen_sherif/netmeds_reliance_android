package com.nms.netmeds.base.model;

import android.graphics.Bitmap;

public class PrescriptionList {

    private String id;
    private boolean isChecked;
    private Bitmap bitmap;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
