package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MstarAlgoliaResult implements Serializable {
    @SerializedName(value = "product_code", alternate = "productCode")
    private int productCode;
    @SerializedName(value = "display_name", alternate = "displayName")
    private String displayName;
    @SerializedName("manufacturer_id")
    private int manufacturerId;
    @SerializedName(value = "manufacturer_name",alternate = "manufactureName")
    private String manufacturerName;
    @SerializedName(value = "availability_status",alternate = "availabilityStatus")
    private String availabilityStatus;
    @SerializedName("schedule")
    private String schedule;
    @SerializedName(value = "max_qty_in_order",alternate = "maxQty")
    private int maxQtyInOrder;
    @SerializedName("mrp")
    private BigDecimal mrp=BigDecimal.ZERO;
    @SerializedName(value = "selling_price",alternate = "sellingPrice")
    private BigDecimal sellingPrice=BigDecimal.ZERO;
    @SerializedName("in_stock")
    private int inStock;
    @SerializedName(value = "product_type",alternate = "productType")
    private String productType;
    @SerializedName("formulation_type")
    private String formulationType;
    @SerializedName("pack_label")
    private String packLabel;
    @SerializedName("generic_id")
    private long genericId;
    @SerializedName("generic")
    private String generic;
    @SerializedName("generic_with_dosage_id")
    private long genericWithDosageId;
    @SerializedName(value = "generic_with_dosage",alternate = "genericDosage")
    private String genericWithDosage;
    @SerializedName("url_path")
    private String urlPath;
    @SerializedName("rx_required")
    private int rxRequired;
    @SerializedName(value = "image_url", alternate = "image")
    private String imageUrl;
    @SerializedName("thumbnail_url")
    private String thumbnailUrl;
    @SerializedName("category_ids")
    private List<Long> categoryIds = null;
    @SerializedName(value = "categories",alternate = "categoryName")
    private List<String> categories = null;
    @SerializedName("objectID")
    private String objectID;
    @SerializedName("brand")
    private String brand;
    @SerializedName("brand_id")
    private String brandId;
    @SerializedName("discount_rate")
    private BigDecimal discountRate = BigDecimal.ZERO;
    @SerializedName("category_tree")
    private Map<String, ArrayList<Object>> categoryTreeList;
    @SerializedName("dosage")
    private String dosage;
    @SerializedName("is_gen_subs_available")
    private int isGenSubsAvailable;

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public int getMaxQtyInOrder() {
        return maxQtyInOrder;
    }

    public void setMaxQtyInOrder(int maxQtyInOrder) {
        this.maxQtyInOrder = maxQtyInOrder;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getFormulationType() {
        return formulationType;
    }

    public void setFormulationType(String formulationType) {
        this.formulationType = formulationType;
    }

    public String getPackLabel() {
        return packLabel;
    }

    public void setPackLabel(String packLabel) {
        this.packLabel = packLabel;
    }

    public long getGenericId() {
        return genericId;
    }

    public void setGenericId(long genericId) {
        this.genericId = genericId;
    }

    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public long getGenericWithDosageId() {
        return genericWithDosageId;
    }

    public void setGenericWithDosageId(long genericWithDosageId) {
        this.genericWithDosageId = genericWithDosageId;
    }

    public String getGenericWithDosage() {
        return genericWithDosage;
    }

    public void setGenericWithDosage(String genericWithDosage) {
        this.genericWithDosage = genericWithDosage;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public int getRxRequired() {
        return rxRequired;
    }

    public void setRxRequired(int rxRequired) {
        this.rxRequired = rxRequired;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    public Map<String, ArrayList<Object>> getCategoryTreeList() {
        return categoryTreeList;
    }

    public void setCategoryTreeList(Map<String, ArrayList<Object>> categoryTreeList) {
        this.categoryTreeList = categoryTreeList;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public int getIsGenSubsAvailable() {
        return isGenSubsAvailable;
    }

    public void setIsGenSubsAvailable(int isGenSubsAvailable) {
        this.isGenSubsAvailable = isGenSubsAvailable;
    }
}
