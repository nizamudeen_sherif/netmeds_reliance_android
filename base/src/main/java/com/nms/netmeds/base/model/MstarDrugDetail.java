package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class MstarDrugDetail {
    @SerializedName("drugCode")
    private int drugCode;
    @SerializedName("genericName")
    private String genericName;
    @SerializedName("brandName")
    private String brandName;
    @SerializedName("productStauts")
    private String productStauts;
    @SerializedName("purchaseQuantity")
    private int purchaseQuantity;
    @SerializedName("purchasePrice")
    private BigDecimal purchasePrice = BigDecimal.ZERO;
    @SerializedName("productDiscount")
    private BigDecimal productDiscount = BigDecimal.ZERO;
    @SerializedName("couponDiscount")
    private BigDecimal couponDiscount = BigDecimal.ZERO;
    @SerializedName("discount")
    private BigDecimal discount = BigDecimal.ZERO;
    @SerializedName("minimumQuantity")
    private int minimumQuantity;
    @SerializedName("pageType")
    private String pageType;
    @SerializedName("prescriptionNeeded")
    private String prescriptionNeeded;
    @SerializedName("alternativeDrugCode")
    private int alternativeDrugCode;
    @SerializedName("additionalShippingChargers")
    private String additionalShippingChargers;
    @SerializedName("subscriptionDiscountStatus")
    private String subscriptionDiscountStatus;

    public int getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(int drugCode) {
        this.drugCode = drugCode;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductStauts() {
        return productStauts;
    }

    public void setProductStauts(String productStauts) {
        this.productStauts = productStauts;
    }

    public int getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void setPurchaseQuantity(int purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public BigDecimal getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(BigDecimal productDiscount) {
        this.productDiscount = productDiscount;
    }

    public BigDecimal getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(BigDecimal couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public int getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(int minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPrescriptionNeeded() {
        return prescriptionNeeded;
    }

    public void setPrescriptionNeeded(String prescriptionNeeded) {
        this.prescriptionNeeded = prescriptionNeeded;
    }

    public int getAlternativeDrugCode() {
        return alternativeDrugCode;
    }

    public void setAlternativeDrugCode(int alternativeDrugCode) {
        this.alternativeDrugCode = alternativeDrugCode;
    }

    public String getAdditionalShippingChargers() {
        return additionalShippingChargers;
    }

    public void setAdditionalShippingChargers(String additionalShippingChargers) {
        this.additionalShippingChargers = additionalShippingChargers;
    }

    public String getSubscriptionDiscountStatus() {
        return subscriptionDiscountStatus;
    }

    public void setSubscriptionDiscountStatus(String subscriptionDiscountStatus) {
        this.subscriptionDiscountStatus = subscriptionDiscountStatus;
    }
}
