package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class MstarBasicResponseResultTemplateModel {

    @SerializedName("address_id")
    private int addressId;
    @SerializedName("cart_id")
    private int cart_id;
    @SerializedName("url_root_path")
    private String urlRootPath;
    @SerializedName("product_image_url_base_path")
    private String productImageUrlBasePath;
    @SerializedName("catalog_image_url_base_path")
    private String catalogImageUrlBasePath;
    @SerializedName("manufacturer_image_url_base_path")
    private String manufactureImageUrlBasePath;
    @SerializedName("session")
    private MStarSession session;
    @SerializedName("attachment_result")
    private Map<String, MstarPrescriptionDetails> attachment_result;
    @SerializedName("detachment_result")
    private Map<String, MstarPrescriptionDetails> detachment_result;
    @SerializedName(value = "cart", alternate = {"cart_details"})
    private MStarCartDetails cartDetails;
    @SerializedName("address_list")
    private List<MStarAddressModel> addressList;
    @SerializedName("your_details")
    private MStarCustomerDetails customerDetails;
    @SerializedName("product")
    private MStarProductDetails productDetails;
    @SerializedName("product_list")
    private List<MStarProductDetails> productDetailsList;
    @SerializedName("category_details")
    private MstarCategoryDetails categoryDetails;
    @SerializedName("manufacturer_details")
    private MstarManufacturerDetails manufacturerDetails;
    @SerializedName("otp_details")
    private MstarOtpDetails mstarOtpDetails;
    @SerializedName("homeBanners")
    private List<MstarBanner> homeMstarBanner;
    @SerializedName("walletBalance")
    private MstarWalletDetails walletDetails;
    @SerializedName("walletHistory")
    private List<MstarWalletHistory> walletHistoryList;
    @SerializedName("referEarn")
    private MstarReferAndEarn referAndEarn;
    @SerializedName("orderList")
    private List<MstarOrders> ordersList;
    @SerializedName("statusList")
    private List<MstarStatusItem> statusList;
    @SerializedName("orderDetails")
    private MstarOrderDetailsResult orderDetail;
    @SerializedName(value = "orderTracking", alternate = {"trackDetails"})
    private MstarOrderDetailsResult orderTrackDetails;
    @SerializedName("brand_details")
    private MstarCategoryDetails brandDetails;
    @SerializedName("alloffers")
    private List<MstarNetmedsOffer> offerList;
    @SerializedName("m2OrderDetail")
    private MstarM2OrderDetail m2OrderDetail;
    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("description")
    private String description;
    @SerializedName("articleList")
    private List<ArticleList> articleList;
    @SerializedName("wellnessSection")
    private List<MstarWellnessSectionDetails> wellnessSections;
    @SerializedName("exploreCategory")
    private MstarExploreCategories exploreCategories;
    @SerializedName("add_result")
    private Map<String, MStarAddedProductsResult> addedProductsResultMap;

    @SerializedName("wellnessBanner")
    private MstarWellnessBanner wellnessBanner;
    @SerializedName("popularBrands")
    private MstarPopularProducts popularProducts;
    @SerializedName("brandProducts")
    private MstarPopularProducts brandProducts;
    @SerializedName("categoryProducts")
    private MstarPopularProducts categoryProducts;
    @SerializedName("rxCount")
    private int rxCount;
    @SerializedName("rxList")
    private List<MstarMyPrescription> rxList;
    @SerializedName("categoryBanner")
    private List<MstarBanner> categoryBannerList;
    @SerializedName("manufactureBanner")
    private List<MstarBanner> manufactureBannerList;
    @SerializedName("brandBanner")
    private List<MstarBanner> brandBannerList;


    //PaymentList
    @SerializedName("displayName")
    private String displayName;
    // hide add new card for some lab adviser
    @SerializedName("hideCard")
    private boolean hideCard;
    // hide nms cash for some lab adviser
    @SerializedName("hide_nms")
    private boolean hide_nms;
    @SerializedName("list")
    private List<PaymentGatewayList> paymentGatewayLists;
    @SerializedName("promotionBanners")
    private MstarPromotionBanners promotionBanners;


    //OrderId
    @SerializedName(value = "order_id", alternate = "orderId")
    private String orderId;

    //OrderStatus
    @SerializedName("order_status")
    private MStarCompleteOrderStatusAfterPayment orderStatusAfterPayment;


    //Complete order response
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("display_status")
    private String displayStatus;
    //use Description;
    //use orderId;
    @SerializedName(value = "IsapfirstOrder", alternate = {"is_first_order"})
    private String isAppFirstOrder;

    @SerializedName("healthConcernCategory")
    private MstarHealthConcernCategory mstarHealthConcernCategory;
    @SerializedName("wellnessproducts")
    private MstarWellnessProducts wellnessProducts;
    @SerializedName("dealsOnTopBrands")
    private MstarWellnessSectionDetails dealsOnTopBrands;
    @SerializedName("brands")
    private MstarWellnessSectionDetails brands;


    @SerializedName("sku")
    private String sku;

    //PAYMENT_WALLET_BALANCE
    @SerializedName("wallet_balance")
    private BigDecimal walletBalance;
    @SerializedName("cashback_balance")
    private BigDecimal cashbackBalance;
    @SerializedName("cashback_useable_balance")
    private BigDecimal cashbackUseableBalance;
    @SerializedName("useable_balance_breakup")
    private List<MstarUseableBalanceBreakup> useableBalanceBreakupList;

    @SerializedName("couponList")
    private List<PromoCodeList> promoCodeLists;
    @SerializedName("superCash")
    private PromoCodeList mstarSuperCash;

    //COD elgible response
    @SerializedName("codEnable")
    private boolean codEnable;
    @SerializedName("itemDetails")
    private M3OrderDetailsResponseResultModel subscriptinItemDetails;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("random")
    private int random;
    @SerializedName("address")
    private MStarAddressModel addressModel;

    //Cart Switch Response
    @SerializedName("switch_result")
    private List<CartSwitchResult> cartSwitchResultsList;

    @SerializedName("medicineSection")
    private List<MstarWellnessSectionDetails> medicineHomeBanner;


    public MstarPromotionBanners getPromotionBanners() {
        return promotionBanners;
    }

    public MstarWellnessProducts getWellnessProducts() {
        return wellnessProducts;
    }

    public MstarHealthConcernCategory getMstarHealthConcernCategory() {
        return mstarHealthConcernCategory;
    }

    public void setPromotionBanners(MstarPromotionBanners promotionBanners) {
        this.promotionBanners = promotionBanners;
    }

    public MstarManufacturerDetails getManufacturerDetails() {
        return manufacturerDetails;
    }

    public void setManufacturerDetails(MstarManufacturerDetails manufacturerDetails) {
        this.manufacturerDetails = manufacturerDetails;
    }

    public MstarCategoryDetails getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(MstarCategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public String getUrlRootPath() {
        return urlRootPath;
    }

    public void setUrlRootPath(String urlRootPath) {
        this.urlRootPath = urlRootPath;
    }

    public String getProductImageUrlBasePath() {
        return productImageUrlBasePath;
    }

    public void setProductImageUrlBasePath(String productImageUrlBasePath) {
        this.productImageUrlBasePath = productImageUrlBasePath;
    }

    public String getCatalogImageUrlBasePath() {
        return catalogImageUrlBasePath;
    }

    public void setCatalogImageUrlBasePath(String catalogImageUrlBasePath) {
        this.catalogImageUrlBasePath = catalogImageUrlBasePath;
    }

    public String getManufactureImageUrlBasePath() {
        return manufactureImageUrlBasePath;
    }

    public void setManufactureImageUrlBasePath(String manufactureImageUrlBasePath) {
        this.manufactureImageUrlBasePath = manufactureImageUrlBasePath;
    }

    public MStarSession getSession() {
        return session;
    }

    public void setSession(MStarSession session) {
        this.session = session;
    }

    public MStarCartDetails getCartDetails() {
        return cartDetails;
    }

    public void setCartDetails(MStarCartDetails cartDetails) {
        this.cartDetails = cartDetails;
    }

    public List<MStarAddressModel> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<MStarAddressModel> addressList) {
        this.addressList = addressList;
    }

    public MStarCustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(MStarCustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public MStarProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(MStarProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public List<MStarProductDetails> getProductDetailsList() {
        return productDetailsList;
    }

    public void setProductDetailsList(List<MStarProductDetails> productDetailsList) {
        this.productDetailsList = productDetailsList;
    }

    public MstarOtpDetails getMstarOtpDetails() {
        return mstarOtpDetails;
    }

    public void setMstarOtpDetails(MstarOtpDetails mstarOtpDetails) {
        this.mstarOtpDetails = mstarOtpDetails;
    }

    public List<MstarBanner> getHomeMstarBanner() {
        return homeMstarBanner;
    }

    public void setHomeMstarBanner(List<MstarBanner> homeMstarBanner) {
        this.homeMstarBanner = homeMstarBanner;
    }

    public MstarWalletDetails getWalletDetails() {
        return walletDetails;
    }

    public void setWalletDetails(MstarWalletDetails walletDetails) {
        this.walletDetails = walletDetails;
    }

    public MstarReferAndEarn getReferAndEarn() {
        return referAndEarn;
    }

    public void setReferAndEarn(MstarReferAndEarn referAndEarn) {
        this.referAndEarn = referAndEarn;
    }

    public List<MstarWalletHistory> getWalletHistoryList() {
        return walletHistoryList;
    }

    public void setWalletHistoryList(List<MstarWalletHistory> walletHistoryList) {
        this.walletHistoryList = walletHistoryList;
    }


    public Map<String, MstarPrescriptionDetails> getAttachment_result() {
        return attachment_result;
    }

    public void setAttachment_result(Map<String, MstarPrescriptionDetails> attachment_result) {
        this.attachment_result = attachment_result;
    }

    public Map<String, MstarPrescriptionDetails> getDetachment_result() {
        return detachment_result;
    }

    public void setDetachment_result(Map<String, MstarPrescriptionDetails> detachment_result) {
        this.detachment_result = detachment_result;
    }

    public List<MstarOrders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<MstarOrders> ordersList) {
        this.ordersList = ordersList;
    }

    public List<MstarStatusItem> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<MstarStatusItem> statusList) {
        this.statusList = statusList;
    }

    public MstarOrderDetailsResult getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(MstarOrderDetailsResult orderDetail) {
        this.orderDetail = orderDetail;
    }

    public MstarOrderDetailsResult getOrderTrackDetails() {
        return orderTrackDetails;
    }

    public void setOrderTrackDetails(MstarOrderDetailsResult orderTrackDetails) {
        this.orderTrackDetails = orderTrackDetails;
    }

    public MstarCategoryDetails getBrandDetails() {
        return brandDetails;
    }

    public void setBrandDetails(MstarCategoryDetails brandDetails) {
        this.brandDetails = brandDetails;
    }

    public List<MstarNetmedsOffer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<MstarNetmedsOffer> offerList) {
        this.offerList = offerList;
    }

    public MstarM2OrderDetail getM2OrderDetail() {
        return m2OrderDetail;
    }

    public void setM2OrderDetail(MstarM2OrderDetail m2OrderDetail) {
        this.m2OrderDetail = m2OrderDetail;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ArticleList> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<ArticleList> articleList) {
        this.articleList = articleList;
    }

    public MstarExploreCategories getExploreCategories() {
        return exploreCategories;
    }

    public void setExploreCategories(MstarExploreCategories exploreCategories) {
        this.exploreCategories = exploreCategories;
    }

    public MstarWellnessBanner getWellnessBanner() {
        return wellnessBanner;
    }

    public void setWellnessBanner(MstarWellnessBanner wellnessBanner) {
        this.wellnessBanner = wellnessBanner;
    }

    public MstarPopularProducts getPopularProducts() {
        return popularProducts;
    }

    public void setPopularProducts(MstarPopularProducts popularBrands) {
        this.popularProducts = popularBrands;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<PaymentGatewayList> getPaymentGatewayLists() {
        return paymentGatewayLists;
    }

    public void setPaymentGatewayLists(List<PaymentGatewayList> list) {
        this.paymentGatewayLists = list;
    }

    public int getRxCount() {
        return rxCount;
    }

    public void setRxCount(int rxCount) {
        this.rxCount = rxCount;
    }

    public List<MstarMyPrescription> getRxList() {
        return rxList;
    }

    public void setRxList(List<MstarMyPrescription> rxList) {
        this.rxList = rxList;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(BigDecimal walletBalance) {
        this.walletBalance = walletBalance;
    }

    public BigDecimal getCashbackBalance() {
        return cashbackBalance;
    }

    public void setCashbackBalance(BigDecimal cashbackBalance) {
        this.cashbackBalance = cashbackBalance;
    }

    public BigDecimal getCashbackUseableBalance() {
        return cashbackUseableBalance;
    }

    public void setCashbackUseableBalance(BigDecimal cashbackUseableBalance) {
        this.cashbackUseableBalance = cashbackUseableBalance;
    }

    public List<MstarUseableBalanceBreakup> getUseableBalanceBreakupList() {
        return useableBalanceBreakupList;
    }

    public void setUseableBalanceBreakupList(List<MstarUseableBalanceBreakup> useableBalanceBreakupList) {
        this.useableBalanceBreakupList = useableBalanceBreakupList;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getIsAppFirstOrder() {
        return isAppFirstOrder;
    }

    public void setIsAppFirstOrder(String isAppFirstOrder) {
        this.isAppFirstOrder = isAppFirstOrder;
    }

    public MstarPopularProducts getBrandProducts() {
        return brandProducts;
    }

    public void setBrandProducts(MstarPopularProducts brandProducts) {
        this.brandProducts = brandProducts;
    }

    public List<PromoCodeList> getPromoCodeLists() {
        return promoCodeLists;
    }

    public void setPromoCodeLists(List<PromoCodeList> promoCodeLists) {
        this.promoCodeLists = promoCodeLists;
    }

    public PromoCodeList getMstarSuperCash() {
        return mstarSuperCash;
    }

    public void setMstarSuperCash(PromoCodeList mstarSuperCash) {
        this.mstarSuperCash = mstarSuperCash;
    }

    public MStarCompleteOrderStatusAfterPayment getOrderStatusAfterPayment() {
        return orderStatusAfterPayment;
    }

    public void setOrderStatusAfterPayment(MStarCompleteOrderStatusAfterPayment orderStatusAfterPayment) {
        this.orderStatusAfterPayment = orderStatusAfterPayment;
    }

    public MstarPopularProducts getCategoryProducts() {
        return categoryProducts;
    }

    public void setCategoryProducts(MstarPopularProducts categoryProducts) {
        this.categoryProducts = categoryProducts;
    }

    public Map<String, MStarAddedProductsResult> getAddedProductsResultMap() {
        return addedProductsResultMap;
    }

    public void setAddedProductsResultMap(Map<String, MStarAddedProductsResult> addedProductsResultMap) {
        this.addedProductsResultMap = addedProductsResultMap;
    }

    public boolean isCodEnable() {
        return codEnable;
    }

    public void setCodEnable(boolean codEnable) {
        this.codEnable = codEnable;
    }

    public M3OrderDetailsResponseResultModel getSubscriptinItemDetails() {
        return subscriptinItemDetails;
    }

    public void setSubscriptinItemDetails(M3OrderDetailsResponseResultModel subscriptinItemDetails) {
        this.subscriptinItemDetails = subscriptinItemDetails;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public List<MstarBanner> getCategoryBannerList() {
        return categoryBannerList;
    }

    public void setCategoryBannerList(List<MstarBanner> categoryBannerList) {
        this.categoryBannerList = categoryBannerList;
    }

    public List<MstarBanner> getManufactureBannerList() {
        return manufactureBannerList;
    }

    public void setManufactureBannerList(List<MstarBanner> manufactureBannerList) {
        this.manufactureBannerList = manufactureBannerList;
    }

    public List<MstarBanner> getBrandBannerList() {
        return brandBannerList;
    }

    public void setBrandBannerList(List<MstarBanner> brandBannerList) {
        this.brandBannerList = brandBannerList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRandom() {
        return random;
    }

    public void setRandom(int random) {
        this.random = random;
    }

    public MStarAddressModel getAddressModel() {
        return addressModel;
    }

    public List<CartSwitchResult> getCartSwitchResultsList() {
        return cartSwitchResultsList;
    }

    public void setCartSwitchResultsList(List<CartSwitchResult> cartSwitchResultsList) {
        this.cartSwitchResultsList = cartSwitchResultsList;
    }

    public boolean getHideCard() {
        return hideCard;
    }

    public void setHideCard(boolean hideCard) {
        this.hideCard = hideCard;
    }


    public List<MstarWellnessSectionDetails> getWellnessSections() {
        return wellnessSections;
    }

    public void setWellnessSections(List<MstarWellnessSectionDetails> wellnessSections) {
        this.wellnessSections = wellnessSections;
    }

    public MstarWellnessSectionDetails getDealsOnTopBrands() {
        return dealsOnTopBrands;
    }

    public void setDealsOnTopBrands(MstarWellnessSectionDetails dealsOnTopBrands) {
        this.dealsOnTopBrands = dealsOnTopBrands;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public boolean getHideNMS() {
        return hide_nms;
    }

    public void setHideNMS(boolean hide_nms) {
        this.hide_nms = hide_nms;
    }

    public MstarWellnessSectionDetails getBrands() {
        return brands;
    }

    public void setBrands(MstarWellnessSectionDetails brands) {
        this.brands = brands;
    }

    public List<MstarWellnessSectionDetails> getMedicineHomeBanner() {
        return medicineHomeBanner;
    }
}
