package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MStarCustomerDetails implements Serializable {
    @SerializedName("juspay_id")
    private String juspayId;
    @SerializedName("updated_time")
    private String updatedTime;
    @SerializedName("mobile_no_verified")
    private boolean mobileNoVerified;
    @SerializedName("gender")
    private String gender;
    @SerializedName("universal_cart_id")
    private int universalCartId;
    @SerializedName("date_of_birth")
    private String dateOfBirth;
    @SerializedName("paytm_customer_token")
    private String paytmCustomerToken;
    @SerializedName("mobile_no")
    private String mobileNo;
    @SerializedName("preferred_billing_address")
    private int preferredBillingAddress;
    @SerializedName("segments")
    private String segments;
    @SerializedName("prime_valid_till_time")
    private String primeValidTillTime;
    @SerializedName("password")
    private String password;
    @SerializedName("some_type")
    private String someType;
    @SerializedName("id")
    private int id;
    @SerializedName("email")
    private String email;
    @SerializedName("prime")
    private boolean prime;
    @SerializedName("created_time")
    private String createdTime;
    @SerializedName("registration_source")
    private String registrationSource;
    @SerializedName("email_verified")
    private boolean emailVerified;
    @SerializedName("cod_allowed")
    private boolean codAllowed;
    @SerializedName("new_customer")
    private String newCustomer;
    @SerializedName("group_name")
    private String groupName;
    @SerializedName("display_name_short")
    private String displayNameShort;
    @SerializedName("registered_channel")
    private String registeredChannel;
    @SerializedName("nms_customer_id")
    private String nmsCustomerId;
    @SerializedName("preferred_shipping_address")
    private int preferredShippingAddress;
    @SerializedName("lastname")
    private String lastName;
    @SerializedName("firstname")
    private String firstName;
    @SerializedName("primepackage")
    private String primePackage;

    public String getJuspayId() {
        return juspayId;
    }

    public void setJuspayId(String juspayId) {
        this.juspayId = juspayId;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public boolean isMobileNoVerified() {
        return mobileNoVerified;
    }

    public void setMobileNoVerified(boolean mobileNoVerified) {
        this.mobileNoVerified = mobileNoVerified;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getUniversalCartId() {
        return universalCartId;
    }

    public void setUniversalCartId(int universalCartId) {
        this.universalCartId = universalCartId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPaytmCustomerToken() {
        return paytmCustomerToken;
    }

    public void setPaytmCustomerToken(String paytmCustomerToken) {
        this.paytmCustomerToken = paytmCustomerToken;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public int getPreferredBillingAddress() {
        return preferredBillingAddress;
    }

    public void setPreferredBillingAddress(int preferredBillingAddress) {
        this.preferredBillingAddress = preferredBillingAddress;
    }

    public String getSegments() {
        return segments;
    }

    public void setSegments(String segments) {
        this.segments = segments;
    }

    public String getPrimeValidTillTime() {
        return primeValidTillTime;
    }

    public void setPrimeValidTillTime(String primeValidTillTime) {
        this.primeValidTillTime = primeValidTillTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSomeType() {
        return someType;
    }

    public void setSomeType(String someType) {
        this.someType = someType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPrime() {
        return prime;
    }

    public void setPrime(boolean prime) {
        this.prime = prime;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public boolean isCodAllowed() {
        return codAllowed;
    }

    public void setCodAllowed(boolean codAllowed) {
        this.codAllowed = codAllowed;
    }

    public String getNewCustomer() {
        return newCustomer;
    }

    public void setNewCustomer(String newCustomer) {
        this.newCustomer = newCustomer;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDisplayNameShort() {
        return displayNameShort;
    }

    public void setDisplayNameShort(String displayNameShort) {
        this.displayNameShort = displayNameShort;
    }

    public String getRegisteredChannel() {
        return registeredChannel;
    }

    public void setRegisteredChannel(String registeredChannel) {
        this.registeredChannel = registeredChannel;
    }

    public String getNmsCustomerId() {
        return nmsCustomerId;
    }

    public void setNmsCustomerId(String nmsCustomerId) {
        this.nmsCustomerId = nmsCustomerId;
    }

    public int getPreferredShippingAddress() {
        return preferredShippingAddress;
    }

    public void setPreferredShippingAddress(int preferredShippingAddress) {
        this.preferredShippingAddress = preferredShippingAddress;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPrimePackage() {
        return primePackage;
    }

    public void setPrimePackage(String primePackage) {
        this.primePackage = primePackage;
    }
}