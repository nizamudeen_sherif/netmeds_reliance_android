package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ConcernSlides {
    @SerializedName("title")
    private String title;
    @SerializedName("linktype")
    private String linktype;
    @SerializedName("image")
    private String image;
    @SerializedName("url")
    private String url;

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getLinktype() {
        return linktype;
    }

    public String getUrl() {
        return url;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
