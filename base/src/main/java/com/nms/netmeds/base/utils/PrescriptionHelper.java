package com.nms.netmeds.base.utils;

import android.graphics.Bitmap;

import com.nms.netmeds.base.model.DigitizedPrescription;
import com.nms.netmeds.base.model.UploadedPrescriptionList;

import java.util.ArrayList;
import java.util.List;

public class PrescriptionHelper {
    private static PrescriptionHelper helper;

    private ArrayList<Bitmap> prescriptionBitmapList = new ArrayList<>();
    private ArrayList<Bitmap> m1PrescriptionList = new ArrayList<>();
    private ArrayList<UploadedPrescriptionList> prescriptionList = new ArrayList<>();
    private List<String> mUploadedPrescriptionId = new ArrayList<>();
    private List<String> digitizedPrescription = new ArrayList<>();
    private ArrayList<Bitmap> bitmapList = new ArrayList<>();
    private ArrayList<Bitmap> CartPrescriptionBitmapList = new ArrayList<>();
    private boolean prescriptionFlag = false;

    public static PrescriptionHelper getInstance() {
        if (helper == null) {
            helper = new PrescriptionHelper();
        }
        return helper;
    }

    public static PrescriptionHelper getHelper() {
        return helper;
    }

    public static void setHelper(PrescriptionHelper helper) {
        PrescriptionHelper.helper = helper;
    }

    public List<String> getmUploadedPrescriptionId() {
        return mUploadedPrescriptionId;
    }

    public void setmUploadedPrescriptionId(List<String> mUploadedPrescriptionId) {
        this.mUploadedPrescriptionId = mUploadedPrescriptionId;
    }

    public ArrayList<Bitmap> getPrescriptionBitmapList() {
        return prescriptionBitmapList;
    }

    public void setPrescriptionBitmapList(ArrayList<Bitmap> prescriptionBitmapList) {
        this.prescriptionBitmapList = prescriptionBitmapList;
    }

    public ArrayList<UploadedPrescriptionList> getPrescriptionList() {
        return prescriptionList;
    }

    public void setPrescriptionList(ArrayList<UploadedPrescriptionList> prescriptionList) {
        this.prescriptionList = prescriptionList;
    }

    public List<String> getDigitizedPrescription() {
        return digitizedPrescription;
    }


    public void clearPrescription() {
        prescriptionBitmapList = new ArrayList<>();
        prescriptionList = new ArrayList<>();
        mUploadedPrescriptionId = new ArrayList<>();
        digitizedPrescription = new ArrayList<>();
        m1PrescriptionList = new ArrayList<>();
    }
    public ArrayList<Bitmap> getM1PrescriptionList() {
        return m1PrescriptionList;
    }

    public void setM1PrescriptionList(ArrayList<Bitmap> m1PrescriptionList) {
        this.m1PrescriptionList = m1PrescriptionList;
    }

    public ArrayList<Bitmap> getBitmapList() {
        return bitmapList;
    }

    public void setBitmapList(ArrayList<Bitmap> bitmapList) {
        this.bitmapList = bitmapList;
    }

    public ArrayList<Bitmap> getCartPrescriptionBitmapList() {
        return CartPrescriptionBitmapList;
    }


    public boolean isPrescriptionFlag() {
        return prescriptionFlag;
    }

    public void setPrescriptionFlag(boolean prescriptionFlag) {
        this.prescriptionFlag = prescriptionFlag;
    }
}