package com.nms.netmeds.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateSubscriptionRequest extends BaseRequest {
    @SerializedName("refillDays")
    @Expose
    private Integer refillDays;
    @SerializedName("subscriptionFlag")
    @Expose
    private Boolean subscriptionFlag;

    public Integer getRefillDays() {
        return refillDays;
    }

    public void setRefillDays(Integer refillDays) {
        this.refillDays = refillDays;
    }

    public Boolean getSubscriptionFlag() {
        return subscriptionFlag;
    }

    public void setSubscriptionFlag(Boolean subscriptionFlag) {
        this.subscriptionFlag = subscriptionFlag;
    }
}
