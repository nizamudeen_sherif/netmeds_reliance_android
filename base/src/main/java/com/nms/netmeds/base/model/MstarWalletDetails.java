package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class MstarWalletDetails {

    @SerializedName("NMSCASH")
    private BigDecimal NMSCASH = BigDecimal.ZERO;
    @SerializedName("NMSSUPERCASH")
    private BigDecimal NMSSUPERCASH = BigDecimal.ZERO;
    @SerializedName("TOTALBALANCE")
    private BigDecimal TOTALBALANCE = BigDecimal.ZERO;
    @SerializedName("NMSCASH_MSG")
    private String nmsCashMsg;
    @SerializedName("NMSSUPERCASH_MSG")
    private String nmsSuperCashMsg;
    @SerializedName("NONWITHDRAWAL")
    private BigDecimal nonWithdrawableAmount;
    @SerializedName("WITHDRAWAL")
    private BigDecimal withdrawableAmount;

    public BigDecimal getNMSCASH() {
        return  NMSCASH;
    }

    public void setNMSCASH(BigDecimal NMSCASH) {
        this.NMSCASH = NMSCASH;
    }

    public BigDecimal getNMSSUPERCASH() {
        return  NMSSUPERCASH;
    }

    public void setNMSSUPERCASH(BigDecimal NMSSUPERCASH) {
        this.NMSSUPERCASH = NMSSUPERCASH;
    }

    public BigDecimal getTOTALBALANCE() {
        return  TOTALBALANCE;
    }

    public void setTOTALBALANCE(BigDecimal TOTALBALANCE) {
        this.TOTALBALANCE = TOTALBALANCE;
    }

    public String getNmsCashMsg() {
        return nmsCashMsg;
    }

    public void setNmsCashMsg(String nmsCashMsg) {
        this.nmsCashMsg = nmsCashMsg;
    }

    public String getNmsSuperCashMsg() {
        return nmsSuperCashMsg;
    }

    public void setNmsSuperCashMsg(String nmsSuperCashMsg) {
        this.nmsSuperCashMsg = nmsSuperCashMsg;
    }

    public BigDecimal getNonWithdrawableAmount() {
        return nonWithdrawableAmount;
    }

    public void setNonWithdrawableAmount(BigDecimal nonWithdrawableAmount) {
        this.nonWithdrawableAmount = nonWithdrawableAmount;
    }

    public BigDecimal getWithdrawableAmount() {
        return withdrawableAmount;
    }

    public void setWithdrawableAmount(BigDecimal withdrawableAmount) {
        this.withdrawableAmount = withdrawableAmount;
    }
}
