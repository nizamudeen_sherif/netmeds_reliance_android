package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

public class DigitizedRxList {
    @SerializedName("rxId")
    private String rxId;
    @SerializedName("orderId")
    private String orderId;

    public String getRxId() {
        return rxId;
    }

    public void setRxId(String rxId) {
        this.rxId = rxId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
