package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PrimeOrderBannerConfig {
    @SerializedName("header")
    private String headerText;
    @SerializedName("description")
    private String descriptionText;

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }
}
