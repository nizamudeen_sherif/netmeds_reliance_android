package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DeliveryEstimateResult {
    @SerializedName("result")
    private ArrayList<DeliveryEstimatePinCodeResult> pinCodeResultList = null;
    @SerializedName("a_status")
    private String aStatus;
    @SerializedName("additional_data")
    private DeliveryEstimateAdditionalData deliveryEstimateAdditionalData;

    public ArrayList<DeliveryEstimatePinCodeResult> getPinCodeResultList() {
        return pinCodeResultList;
    }

    public void setPinCodeResultList(ArrayList<DeliveryEstimatePinCodeResult> pinCodeResultList) {
        this.pinCodeResultList = pinCodeResultList;
    }

    public String getaStatus() {
        return aStatus;
    }

    public void setaStatus(String aStatus) {
        this.aStatus = aStatus;
    }

    public DeliveryEstimateAdditionalData getDeliveryEstimateAdditionalData() {
        return deliveryEstimateAdditionalData;
    }

    public void setDeliveryEstimateAdditionalData(DeliveryEstimateAdditionalData deliveryEstimateAdditionalData) {
        this.deliveryEstimateAdditionalData = deliveryEstimateAdditionalData;
    }
}
