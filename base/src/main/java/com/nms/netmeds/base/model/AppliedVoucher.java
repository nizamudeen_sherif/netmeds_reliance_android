package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class AppliedVoucher {
    @SerializedName("code")
    private String code;
    @SerializedName("discount")
    private String discount;
    @SerializedName("giftvoucher_type")
    private String giftVoucherType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getGiftVoucherType() {
        return giftVoucherType;
    }

    public void setGiftVoucherType(String giftVoucherType) {
        this.giftVoucherType = giftVoucherType;
    }
}
