package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Test implements Serializable {

    @SerializedName("category")
    private String category;
    @SerializedName("meta")
    private String meta;
    @SerializedName(value = "Common Questions", alternate = "commonQuestions")
    private String commonQuestions;
    @SerializedName(value = "Expert Advice", alternate = "expertAdvice")
    private String expertAdvice;
    @SerializedName(value = "Test Sample", alternate = "testSample")
    private String testSample;
    // Test id
    @SerializedName(value = "itemId", alternate = {"id"})
    private String testId;
    @SerializedName("thyroId")
    private String thyroId;
    @SerializedName(value = "testName", alternate = {"itemName"})
    private String testName;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String testDescription;
    @SerializedName(value = "Test Preparation",alternate = "testPreparation")
    private String testPreparation;
    @SerializedName("url")
    private String url;
    @SerializedName("Also Known As")
    private String alsoKnownAs;
    @SerializedName("type")
    private String type;
    @SerializedName("whenToGetTested")
    private String whenToGetTested;
    @SerializedName("People Taken")
    private String peopleTaken;
    @SerializedName("Vimta ID")
    private String vimtaId;
    @SerializedName("Related Tests")
    private String relatedTests;
    @SerializedName("Available In")
    private String availableIn;
    @SerializedName("No. of tests")
    private String noOfTests;
    @SerializedName("Test Overview")
    private String testOverview;
    @SerializedName(value = "Test Result Interpretation", alternate = "testResultInterpretation")
    private String testInterpretation;
    @SerializedName(value = "price", alternate = {"price_info"})
    private PriceInfo priceInfo;
    private boolean checked;
    private String by;
    @SerializedName("child")
    private String child;
    @SerializedName("precautions")
    private String precautions;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @SerializedName("imageUrl")
    private String imageUrl;

    public String getFastingRequired() {
        return fastingRequired;
    }

    public void setFastingRequired(String fastingRequired) {
        this.fastingRequired = fastingRequired;
    }

    private String fastingRequired;

    @SerializedName(value = "Type", alternate = {"type"})

    public String getPrecautions() {
        return precautions;
    }

    public void setPrecautions(String precautions) {
        this.precautions = precautions;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getCommonQuestions() {
        return commonQuestions;
    }

    public void setCommonQuestions(String commonQuestions) {
        this.commonQuestions = commonQuestions;
    }

    public String getExpertAdvice() {
        return expertAdvice;
    }

    public void setExpertAdvice(String expertAdvice) {
        this.expertAdvice = expertAdvice;
    }

    public String getTestSample() {
        return testSample;
    }

    public void setTestSample(String testSample) {
        this.testSample = testSample;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getThyroId() {
        return thyroId;
    }

    public void setThyroId(String thyroId) {
        this.thyroId = thyroId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTestDescription() {
        return testDescription;
    }

    public void setTestDescription(String testDescription) {
        this.testDescription = testDescription;
    }

    public String getTestPreparation() {
        return testPreparation;
    }

    public void setTestPreparation(String testPreparation) {
        this.testPreparation = testPreparation;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAlsoKnownAs() {
        return alsoKnownAs;
    }

    public void setAlsoKnownAs(String alsoKnownAs) {
        this.alsoKnownAs = alsoKnownAs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWhenToGetTested() {
        return whenToGetTested;
    }

    public void setWhenToGetTested(String whenToGetTested) {
        this.whenToGetTested = whenToGetTested;
    }

    public String getPeopleTaken() {
        return peopleTaken;
    }

    public void setPeopleTaken(String peopleTaken) {
        this.peopleTaken = peopleTaken;
    }

    public String getVimtaId() {
        return vimtaId;
    }

    public void setVimtaId(String vimtaId) {
        this.vimtaId = vimtaId;
    }

    public String getRelatedTests() {
        return relatedTests;
    }

    public void setRelatedTests(String relatedTests) {
        this.relatedTests = relatedTests;
    }

    public String getAvailableIn() {
        return availableIn;
    }

    public void setAvailableIn(String availableIn) {
        this.availableIn = availableIn;
    }

    public String getNoOfTests() {
        return noOfTests;
    }

    public void setNoOfTests(String noOfTests) {
        this.noOfTests = noOfTests;
    }

    public String getTestOverview() {
        return testOverview;
    }

    public void setTestOverview(String testOverview) {
        this.testOverview = testOverview;
    }

    public String getTestInterpretation() {
        return testInterpretation;
    }

    public void setTestInterpretation(String testInterpretation) {
        this.testInterpretation = testInterpretation;
    }

    public PriceInfo getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(PriceInfo priceInfo) {
        this.priceInfo = priceInfo;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object obj) {
        Test otherObject = (Test) obj;
        return this.testId.equals(otherObject.testId);
    }
}
