package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarOrderShippingAddress {
    @SerializedName("shippingFirstName")
    private String shippingFirstName;
    @SerializedName("shippingLastName")
    private String shippingLastName;
    @SerializedName("shippingAddress1")
    private String shippingAddress1;
    @SerializedName("shippingAddress2")
    private String shippingAddress2;
    @SerializedName("shippingCity")
    private String shippingCity;
    @SerializedName("shippingState")
    private String shippingState;
    @SerializedName("shippingZipCode")
    private String shippingZipCode;
    @SerializedName("shippingCountry")
    private String shippingCountry;
    @SerializedName("shippingPhoneNumber")
    private String shippingPhoneNumber;

    public String getShippingFirstName() {
        return shippingFirstName;
    }

    public void setShippingFirstName(String shippingFirstName) {
        this.shippingFirstName = shippingFirstName;
    }

    public String getShippingLastName() {
        return shippingLastName;
    }

    public void setShippingLastName(String shippingLastName) {
        this.shippingLastName = shippingLastName;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingZipCode() {
        return shippingZipCode;
    }

    public void setShippingZipCode(String shippingZipCode) {
        this.shippingZipCode = shippingZipCode;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getShippingPhoneNumber() {
        return shippingPhoneNumber;
    }

    public void setShippingPhoneNumber(String shippingPhoneNumber) {
        this.shippingPhoneNumber = shippingPhoneNumber;
    }
}
