package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarBasicResponseTemplateModel {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private MstarBasicResponseResultTemplateModel result;
    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel reason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MstarBasicResponseResultTemplateModel getResult() {
        return result;
    }

    public void setResult(MstarBasicResponseResultTemplateModel result) {
        this.result = result;
    }

    public MstarBasicResponseFailureReasonTemplateModel getReason() {
        return reason;
    }

    public void setReason(MstarBasicResponseFailureReasonTemplateModel reason) {
        this.reason = reason;
    }
}
