package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarCompleteOrderStatusAfterPayment {

    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;
    @SerializedName("first_order")
    private boolean firstOrder;
    @SerializedName("display_status")
    private String displayStaus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDisplayStaus() {
        return displayStaus;
    }

    public void setDisplayStaus(String displayStaus) {
        this.displayStaus = displayStaus;
    }

    public boolean isFirstOrder() {
        return firstOrder;
    }

    public void setFirstOrder(boolean firstOrder) {
        this.firstOrder = firstOrder;
    }
}
