package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartSubstitutionResponseResult {

    @SerializedName("drug_code")
    private String drug_code;
    @SerializedName("alternatives")
    private List<CartSubstitutionAlternateDrugs> alternates;

    public String getDrug_code() {
        return drug_code;
    }

    public void setDrug_code(String drug_code) {
        this.drug_code = drug_code;
    }

    public List<CartSubstitutionAlternateDrugs> getAlternates() {
        return alternates;
    }

    public void setAlternates(List<CartSubstitutionAlternateDrugs> alternates) {
        this.alternates = alternates;
    }
}
