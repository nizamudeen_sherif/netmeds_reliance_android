package com.nms.netmeds.base.model.Request;

public class MstarCategoryDetailsRequest {

    private Integer categoryId;
    private String returnProduct;
    private Integer pageNo;
    private Integer pageSize;
    private String productFieldsetName;
    private Integer subCategoryDepth;
    private String returnFacets;
    private String productsSortOrder;
    //for brands url api
    private String productsSelector;
    private String productFieldSetForCategory;
    private String productFieldSetForMfr;
    private String productFieldSetForProduct;

    public String getReturnFacets() {
        return returnFacets;
    }

    public void setReturnFacets(String returnFacets) {
        this.returnFacets = returnFacets;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getReturnProduct() {
        return returnProduct;
    }

    public void setReturnProduct(String returnProduct) {
        this.returnProduct = returnProduct;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getProductFieldsetName() {
        return productFieldsetName;
    }

    public void setProductFieldsetName(String productFieldsetName) {
        this.productFieldsetName = productFieldsetName;
    }

    public Integer getSubCategoryDepth() {
        return subCategoryDepth;
    }

    public void setSubCategoryDepth(Integer subCategoryDepth) {
        this.subCategoryDepth = subCategoryDepth;
    }

    public String getProductsSortOrder() {
        return productsSortOrder;
    }

    public void setProductsSortOrder(String productsSortOrder) {
        this.productsSortOrder = productsSortOrder;
    }

    public String getProductsSelector() {
        return productsSelector;
    }

    public void setProductsSelector(String productsSelector) {
        this.productsSelector = productsSelector;
    }

    public String getProductFieldSetForCategory() {
        return productFieldSetForCategory;
    }

    public void setProductFieldSetForCategory(String productFieldSetForCategory) {
        this.productFieldSetForCategory = productFieldSetForCategory;
    }

    public String getProductFieldSetForMfr() {
        return productFieldSetForMfr;
    }

    public void setProductFieldSetForMfr(String productFieldSetForMfr) {
        this.productFieldSetForMfr = productFieldSetForMfr;
    }

    public String getProductFieldSetForProduct() {
        return productFieldSetForProduct;
    }

    public void setProductFieldSetForProduct(String productFieldSetForProduct) {
        this.productFieldSetForProduct = productFieldSetForProduct;
    }
}
