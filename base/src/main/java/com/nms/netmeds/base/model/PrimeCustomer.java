package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PrimeCustomer {
    @SerializedName("prime")
    private boolean prime;
    @SerializedName("primepackage")
    private String primePackage;
    @SerializedName("expiryDate")
    private String expiryDate;

    public boolean isPrime() {
        return prime;
    }

    public void setPrime(boolean prime) {
        this.prime = prime;
    }

    public String getPrimePackage() {
        return primePackage;
    }

    public void setPrimePackage(String primePackage) {
        this.primePackage = primePackage;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}
