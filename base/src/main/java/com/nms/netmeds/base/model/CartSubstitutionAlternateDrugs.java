package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSubstitutionAlternateDrugs {
    @SerializedName("priority")
    private Long priority;
    @SerializedName("drug_code")
    private String drugCode;
    @SerializedName("drug_name")
    private String drugName;
    @SerializedName("mfgr_name")
    private String mfgrName;
    @SerializedName("generic_dosage")
    private String genericDosage;
    @SerializedName("mrp")
    private Double mrp;
    @SerializedName("per_tablet_cost")
    private Double perTabletCost;
    @SerializedName("pack_size")
    private Long packSize;
    @SerializedName("margin")
    private Double margin;
    @SerializedName("per_tablet_price_diff")
    private Double perTabletPriceDiff;
    @SerializedName("parent_drug_code")
    private String parentDrugCode;
    @SerializedName("parent_mrp")
    private Double parentMrp;
    @SerializedName("parent_pack_size")
    private Long parentPackSize;
    @SerializedName("parent_per_tablet_cost")
    private Double parentPerTabletCost;
    @SerializedName("required_qty")
    private int requiredQty;
    @SerializedName("suggested_qty")
    private int suggestedQty;
    @SerializedName("total_price")
    private Double totalPrice;
    @SerializedName("total_diff")
    private Double totalDiff;


    private boolean isSwitchClicked = false;

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getMfgrName() {
        return mfgrName;
    }

    public void setMfgrName(String mfgrName) {
        this.mfgrName = mfgrName;
    }

    public String getGenericDosage() {
        return genericDosage;
    }

    public void setGenericDosage(String genericDosage) {
        this.genericDosage = genericDosage;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getPerTabletCost() {
        return perTabletCost;
    }

    public void setPerTabletCost(Double perTabletCost) {
        this.perTabletCost = perTabletCost;
    }

    public Long getPackSize() {
        return packSize;
    }

    public void setPackSize(Long packSize) {
        this.packSize = packSize;
    }

    public Double getMargin() {
        return margin;
    }

    public void setMargin(Double margin) {
        this.margin = margin;
    }

    public Double getPerTabletPriceDiff() {
        return perTabletPriceDiff;
    }

    public void setPerTabletPriceDiff(Double perTabletPriceDiff) {
        this.perTabletPriceDiff = perTabletPriceDiff;
    }

    public String getParentDrugCode() {
        return parentDrugCode;
    }

    public void setParentDrugCode(String parentDrugCode) {
        this.parentDrugCode = parentDrugCode;
    }

    public Double getParentMrp() {
        return parentMrp;
    }

    public void setParentMrp(Double parentMrp) {
        this.parentMrp = parentMrp;
    }

    public Long getParentPackSize() {
        return parentPackSize;
    }

    public void setParentPackSize(Long parentPackSize) {
        this.parentPackSize = parentPackSize;
    }

    public Double getParentPerTabletCost() {
        return parentPerTabletCost;
    }

    public void setParentPerTabletCost(Double parentPerTabletCost) {
        this.parentPerTabletCost = parentPerTabletCost;
    }

    public int getRequiredQty() {
        return requiredQty;
    }

    public void setRequiredQty(int requiredQty) {
        this.requiredQty = requiredQty;
    }

    public int getSuggestedQty() {
        return suggestedQty;
    }

    public void setSuggestedQty(int suggestedQty) {
        this.suggestedQty = suggestedQty;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalDiff() {
        return totalDiff;
    }

    public void setTotalDiff(Double totalDiff) {
        this.totalDiff = totalDiff;
    }

    public boolean isSwitchClicked() {
        return isSwitchClicked;
    }

    public void setSwitchClicked(boolean switchClicked) {
        isSwitchClicked = switchClicked;
    }
}
