package com.nms.netmeds.base.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;

import com.nms.netmeds.base.R;

import java.lang.reflect.Field;

public class LatoEditText extends androidx.appcompat.widget.AppCompatEditText {

    String customFont;

    public LatoEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LatoEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);

    }

    private void init(Context context, AttributeSet attrs) {
        style(context, attrs);
        setCursorColor(context.getResources().getColor(R.color.colorAccent));
    }

    private void style(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.latoFont);
        int cf = a.getInteger(R.styleable.latoFont_fontName, 0);
        int fontName;
        switch (cf) {
            case 1:
                fontName = R.string.lato_black;
                break;
            case 2:
                fontName = R.string.lato_black_italic;
                break;
            case 3:
                fontName = R.string.lato_bold;
                break;
            case 4:
                fontName = R.string.lato_bold_italic;
                break;
            case 5:
                fontName = R.string.lato_hair_line;
                break;
            case 6:
                fontName = R.string.lato_hair_line_italic;
                break;
            case 7:
                fontName = R.string.lato_italic;
                break;
            case 8:
                fontName = R.string.lato_light;
                break;
            case 9:
                fontName = R.string.lato_light_italic;
                break;
            case 10:
                fontName = R.string.lato_regular;
                break;
            default:
                fontName = R.string.lato_regular;
                break;
        }

        customFont = getResources().getString(fontName);

        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "font/" + customFont + ".ttf");
        setTypeface(tf);
        a.recycle();
    }

    public void setCursorColor(@ColorInt int color) {
        try {
            // Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            int drawableResId = field.getInt(this);

            // Get the editor
            field = TextView.class.getDeclaredField("mEditor");
            field.setAccessible(true);
            Object editor = field.get(this);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(this.getContext(), drawableResId);
            if (drawable != null) {
                drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
            }
            Drawable[] drawables = {drawable, drawable};

            // Set the drawables
            field = editor.getClass().getDeclaredField("mCursorDrawable");
            field.setAccessible(true);
            field.set(editor, drawables);
        } catch (Exception ignored) {
        }
    }
}
