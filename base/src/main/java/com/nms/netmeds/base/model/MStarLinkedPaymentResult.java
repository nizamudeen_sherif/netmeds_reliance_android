package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MStarLinkedPaymentResult {
    @SerializedName("clientAuthToken")
    private String clientAuthToken;
    @SerializedName("wallet")
    private List<LinkedWallet> walletList;
    @SerializedName("savedCards")
    private List<LinkedSavedCard> savedCardList;

    public String getClientAuthToken() {
        return clientAuthToken;
    }

    public void setClientAuthToken(String clientAuthToken) {
        this.clientAuthToken = clientAuthToken;
    }

    public List<LinkedWallet> getWalletList() {
        return walletList;
    }

    public void setWalletList(List<LinkedWallet> walletList) {
        this.walletList = walletList;
    }

    public List<LinkedSavedCard> getSavedCardList() {
        return savedCardList;
    }

    public void setSavedCardList(List<LinkedSavedCard> savedCardList) {
        this.savedCardList = savedCardList;
    }
}
