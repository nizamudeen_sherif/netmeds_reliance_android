package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarRegistrationResult {
    @SerializedName("created_time")
    private String createdTime;
    @SerializedName("cart_id")
    private String cartId;
    @SerializedName("valid_till")
    private String validTill;
    @SerializedName("is_valid")
    private boolean isValid;
    @SerializedName("channel")
    private String channel;
    @SerializedName("id")
    private String id;
    @SerializedName("customer_id")
    private Long customerId;
    @SerializedName("logan_session_id")
    private String loganSessionId;
    @SerializedName("message")
    private String message;

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getLoganSessionId() {
        return loganSessionId;
    }

    public void setLoganSessionId(String loganSessionId) {
        this.loganSessionId = loganSessionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
