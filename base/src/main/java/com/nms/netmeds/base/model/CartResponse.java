package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartResponse extends BaseResponse {

    @SerializedName("result")
    private CartResponseItem cartResponseItem;

    public CartResponseItem getCartResponseItem() {
        return cartResponseItem;
    }

    public void setCartResponseItem(CartResponseItem cartResponseItem) {
        this.cartResponseItem = cartResponseItem;
    }
}
