package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class DeliveryEstimateProductDetail {
    @SerializedName("expiry_message")
    private String expiryMessage;
    @SerializedName("batchno")
    private String batchNo;
    @SerializedName("expiry_date")
    private String expiryDate;
    @SerializedName("qty")
    private Integer qty;

    public String getExpiryMessage() {
        return expiryMessage;
    }

    public void setExpiryMessage(String expiryMessage) {
        this.expiryMessage = expiryMessage;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
