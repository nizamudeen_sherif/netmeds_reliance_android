package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class M2Coupon {
    @SerializedName("couponCode")
    private String couponCode;
    @SerializedName("description")
    private String description;
    @SerializedName("discount")
    private String discount;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
