package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class MStarAddMultipleProductsResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private Map<String, MStarAddedProductsResult> addedProductsResultMap;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, MStarAddedProductsResult> getAddedProductsResultMap() {
        return addedProductsResultMap;
    }

    public void setAddedProductsResultMap(Map<String, MStarAddedProductsResult> addedProductsResultMap) {
        this.addedProductsResultMap = addedProductsResultMap;
    }
}
