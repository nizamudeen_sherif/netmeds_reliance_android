package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MStarCategory {
    @SerializedName("is_active")
    private Boolean isActive;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private Integer id;
    @SerializedName("bread_crumbs")
    private List<MStarBreadCrumb> breadCrumbs = null;
    @SerializedName("url_path")
    private String urlPath;

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<MStarBreadCrumb> getBreadCrumbs() {
        return breadCrumbs;
    }

    public void setBreadCrumbs(List<MStarBreadCrumb> breadCrumbs) {
        this.breadCrumbs = breadCrumbs;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }
}
