package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConsultationPopularConcernConfigs {

    @SerializedName("enableFlag")
    private boolean enableFlag;
    @SerializedName("title1")
    private String title1;
    @SerializedName("title2")
    private String title2;
    @SerializedName("descp")
    private String descp;
    @SerializedName("slide")
    private List<ConcernSlides> slidesList;

    public boolean isEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(boolean enableFlag) {
        this.enableFlag = enableFlag;
    }

    public List<ConcernSlides> getSlidesList() {
        return slidesList;
    }

    public void setSlidesList(List<ConcernSlides> slidesList) {
        this.slidesList = slidesList;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }
}
