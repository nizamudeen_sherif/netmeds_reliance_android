package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ProductDetailsFlag {
    @SerializedName("packOfBuy")
    private boolean packOfBuy;
    @SerializedName("packOfBuyTitle")
    private String packOfBuyTitle;
    @SerializedName("comboEnable")
    private boolean comboEnable;
    @SerializedName("comboEnableTitle")
    private String comboEnableTitle;
    @SerializedName("similarEnable")
    private boolean similarEnable;
    @SerializedName("similarEnableTitle")
    private String similarEnableTitle;

    public boolean isPackOfBuy() {
        return packOfBuy;
    }

    public void setPackOfBuy(boolean packOfBuy) {
        this.packOfBuy = packOfBuy;
    }

    public String getPackOfBuyTitle() {
        return packOfBuyTitle;
    }

    public void setPackOfBuyTitle(String packOfBuyTitle) {
        this.packOfBuyTitle = packOfBuyTitle;
    }

    public boolean isComboEnable() {
        return comboEnable;
    }

    public void setComboEnable(boolean comboEnable) {
        this.comboEnable = comboEnable;
    }

    public String getComboEnableTitle() {
        return comboEnableTitle;
    }

    public void setComboEnableTitle(String comboEnableTitle) {
        this.comboEnableTitle = comboEnableTitle;
    }

    public boolean isSimilarEnable() {
        return similarEnable;
    }

    public void setSimilarEnable(boolean similarEnable) {
        this.similarEnable = similarEnable;
    }

    public String getSimilarEnableTitle() {
        return similarEnableTitle;
    }

    public void setSimilarEnableTitle(String similarEnableTitle) {
        this.similarEnableTitle = similarEnableTitle;
    }
}
