package com.nms.netmeds.base.viewModel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.databinding.DialogxSingleClickSubscriptionBinding;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SingleClickSubscriptionViewModel extends AppViewModel {
    private SingleClickSubscriptionViewModelListener listener;
    private DialogxSingleClickSubscriptionBinding binding;
    private String[] timePeriodArray;
    private String selectedTimePeriodsWithInterval;
    private String currentDeliveryDate;
    private String orderId;
    private int selectedTimePeriod = 30;
    private int[] timePeriodSlot;
    private BasePreference basePreference;

    public SingleClickSubscriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(DialogxSingleClickSubscriptionBinding binding, String currentDeliveryDate, String orderId, SingleClickSubscriptionViewModelListener listener, BasePreference basePreference) {
        this.binding = binding;
        this.listener = listener;
        this.currentDeliveryDate = currentDeliveryDate;
        this.orderId = orderId;
        this.basePreference = basePreference;
        initiateData();
    }

    private void initiateData() {
        timePeriodArray = listener.getContext().getResources().getStringArray(R.array.text_sub_time_periods);
        timePeriodSlot = listener.getContext().getResources().getIntArray(R.array.text_sub_time_period_slot);
        setSpinnerAdapter();
        initiateCurrentDeliveryDate();
    }

    private void initiateCurrentDeliveryDate() {
        Calendar calendar = Calendar.getInstance();
        String currentDate = calendar.get(Calendar.DATE) + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR);
        SubscriptionHelper.getInstance().setCurrentDeliveryDate(TextUtils.isEmpty(currentDeliveryDate) ? currentDate : currentDeliveryDate);
    }

    public String getSubscriptionPromoCode() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList().getCouponCode()) ? configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList().getCouponCode() : "";
    }

    public String getSubscriptionTimePeriodDescription() {
        return String.format(listener.getContext().getString(R.string.text_single_click_subscription_delivery_interval), !TextUtils.isEmpty(selectedTimePeriodsWithInterval) ? selectedTimePeriodsWithInterval : "");
    }

    public void onDismissDialog() {
        listener.onDismissDialog();
    }

    public void onClickSubscription() {
        initiateAPICall(APIServiceManager.MSTAR_CREATE_SUBSCRIPTION);
    }

    private void setSpinnerAdapter() {
        if (timePeriodArray == null || timePeriodArray.length == 0) return;
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(listener.getContext(), android.R.layout.simple_spinner_item, timePeriodArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spTimePeriod.setAdapter(spinnerArrayAdapter);
        binding.spTimePeriod.setSelection(0);
        binding.spTimePeriod.setOnItemSelectedListener(timePeriodSelectListener);
    }

    private AppCompatSpinner.OnItemSelectedListener timePeriodSelectListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedTimePeriod = timePeriodSlot[position];
            calculateNextDeliveryTimeInterval();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private void calculateNextDeliveryTimeInterval() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= 2; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getEstimateCalenderTime());
            calendar.add(Calendar.DATE, selectedTimePeriod * i);
            if (i == 1) {
                DecimalFormat mFormat = new DecimalFormat("00");
                String date = mFormat.format(Double.valueOf(calendar.get(Calendar.DATE))) + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).substring(0, 3) + " " + calendar.get(Calendar.YEAR);
                SubscriptionHelper.getInstance().setNextDeliveryIntervalDate(date);
            }
            String data = calendar.get(Calendar.DATE) + "" + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).substring(0, 3);
            stringBuilder.append(data + (i != 2 ? ", " : ""));
        }
        selectedTimePeriodsWithInterval = stringBuilder.toString();
        binding.invalidateAll();
    }

    private Date getEstimateCalenderTime() {
        Date date = new Date();
        try {
            if (currentDeliveryDate != null)
                date = new SimpleDateFormat(DateTimeUtils.ddMMMyyyy).parse(currentDeliveryDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(listener.getContext());
        if (isConnected) {
            listener.showProgress();
            if (transactionId == APIServiceManager.MSTAR_CREATE_SUBSCRIPTION) {
                APIServiceManager.getInstance().createSubscription(this, basePreference.getMstarBasicHeaderMap(), orderId, String.valueOf(selectedTimePeriod), AppConstant.PAYMENT_SUCCESS);
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.dismissProgress();
        if (transactionId == APIServiceManager.MSTAR_CREATE_SUBSCRIPTION) {
            subscriptionInitiateSuccessResponse(data);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.dismissProgress();
        f2SubscriptionFailure();
    }

    private void subscriptionInitiateSuccessResponse(String data) {
        SubscriptionHelper.getInstance().setEditOrderResponse(data);
        MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (responseTemplateModel != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())) {
            listener.subscriptionF2InitiatedSuccessful();
        } else if (responseTemplateModel != null && responseTemplateModel.getStatus().equalsIgnoreCase(AppConstant.API_MSTAR_FAILURE_STATUS)) {
            listener.snackBarView(responseTemplateModel.getStatus());
        }
    }

    private void f2SubscriptionFailure() {
        listener.subscriptionF2InitiatedSuccessful();
        listener.onDismissDialog();
    }

    public interface SingleClickSubscriptionViewModelListener {

        void showProgress();

        void dismissProgress();

        void onDismissDialog();

        void subscriptionF2InitiatedSuccessful();

        void snackBarView(String message);

        Context getContext();

        DialogxSingleClickSubscriptionBinding binding();
    }
}
