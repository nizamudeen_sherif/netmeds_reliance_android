package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class GetPastPrescriptionResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private GetPastPrescriptionResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GetPastPrescriptionResult getResult() {
        return result;
    }

    public void setResult(GetPastPrescriptionResult result) {
        this.result = result;
    }
}
