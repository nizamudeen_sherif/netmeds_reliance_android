package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class MstarM2OrderDetail {
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("status")
    private String status;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("emailId")
    private String emailId;
    @SerializedName("customerDob")
    private String customerDob;
    @SerializedName("mobileNo")
    private String mobileNo;
    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("coupon_code")
    private String couponCode;
    @SerializedName("prescriptions")
    private List<String> prescriptions = null;
    @SerializedName("getBillingAddress")
    private MethodTwoAddress getBillingAddress;
    @SerializedName("getShippingAddress")
    private MethodTwoAddress getShippingAddress;
    @SerializedName("voucher_code")
    private String voucherCode;
    @SerializedName("nms_cash")
    private BigDecimal nmsCash = BigDecimal.ZERO;
    @SerializedName("nms_supercash")
    private BigDecimal nmsSuperCash = BigDecimal.ZERO;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCustomerDob() {
        return customerDob;
    }

    public void setCustomerDob(String customerDob) {
        this.customerDob = customerDob;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public List<String> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<String> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public MethodTwoAddress getGetBillingAddress() {
        return getBillingAddress;
    }

    public void setGetBillingAddress(MethodTwoAddress getBillingAddress) {
        this.getBillingAddress = getBillingAddress;
    }

    public MethodTwoAddress getGetShippingAddress() {
        return getShippingAddress;
    }

    public void setGetShippingAddress(MethodTwoAddress getShippingAddress) {
        this.getShippingAddress = getShippingAddress;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public BigDecimal getNmsCash() {
        return nmsCash;
    }

    public void setNmsCash(BigDecimal nmsCash) {
        this.nmsCash = nmsCash;
    }

    public BigDecimal getNmsSuperCash() {
        return nmsSuperCash;
    }

    public void setNmsSuperCash(BigDecimal nmsSuperCash) {
        this.nmsSuperCash = nmsSuperCash;
    }
}
