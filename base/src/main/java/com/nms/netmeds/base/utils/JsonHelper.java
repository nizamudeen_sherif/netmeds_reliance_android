package com.nms.netmeds.base.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonHelper {

    public static JsonReader readJSONFromAsset(Context context, String jsonFile) throws IOException {
        Resources appResources = context.getResources();
        AssetManager wagAssetManager = appResources.getAssets();
        InputStream inputStream = wagAssetManager.open(jsonFile);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
        return new JsonReader(reader);
    }
}
