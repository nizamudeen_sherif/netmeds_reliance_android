package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class M3Flow2SubscriptionResponse {

    @SerializedName("updatedOn")
    private long updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private M3Flow2SubscriptionResponseResultDetails result;

    public long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public M3Flow2SubscriptionResponseResultDetails getResult() {
        return result;
    }

    public void setResult(M3Flow2SubscriptionResponseResultDetails result) {
        this.result = result;
    }

}