package com.nms.netmeds.base;

public class IntentConstant {

    public static final int CANCEL_ORDER_RQUEST_CODE = 1234;

    public static final String HOME_FLAG_FROM_SUBSCRIPTION_ORDER_PLACED_SUCCESSFUL = "HOME_FLAG_FROM_SUBSCRIPTION_ORDER_PLACED_SUCCESSFUL";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String EMAIL = "EMAIL";
    public static final String FIRSTNAME = "FIRSTNAME";
    public static final String LASTNAME = "LASTNAME";
    public static final String REGISTER_INTENT_ARGUMENT = "REGISTER_INTENT_ARGUMENT";
    public static final String SOCIAL_LOGIN_FLAG = "SOCIAL_LOGIN_FLAG";
    public static final String SOCIAL_INTENT_ARGUMENT = "SOCIAL_INTENT_ARGUMENT";
    public static final String RESET_PASSWORD_FLAG = "RESET_PASSWORD_FLAG";
    public static final String TRUE_CALLER_FLAG = "TRUE_CALLER_FLAG";
    public static final String WEB_PAGE_URL = "WEB_PAGE_URL";
    public static final String DOMAIN = "DOMAIN";
    public static final String TOKEN = "TOKEN";
    public static final String FROM_CART = "FROM_CART";
    public static final String RANDOM_KEY = "RANDOM_KEY";
    public static final String FROM_PRIME = "FROM_PRIME";

    //Cart Screen Constants
    public static final String RX_MEDICINE_LIST = "RX_MEDICINE_LIST";
    public static final String ORDER_VALUE = "ORDER_VALUE";
    public static final String REDIRECT_TO_UPLOAD = "REDIRECT_TO_UPLOAD";
    public static final String SKU_LIST = "SKU_LIST";

    //Product Detail
    public static final String PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE = "PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE";
    public static final String ALTERNATE_PRODUCTS = "ALTERNATE_PRODUCTS";
    public static final String PRODUCT_CODE = "PRODUCT_CODE";

    /*Order Screen*/
    public static final String ORDER_ID = "MSTAR_ORDER_ID";
    public static final int CANCEL_ORDER_RESULT_OK = 01020304;
    public static final String ORDER_DETAILS = "ORDER_DETAILS";

    /*Review Order Tag name*/
    public static final String REVIEW_ORDER_AGAIN_DIALOG_FRAGMENT_TAG_NAME = "REVIEW_ORDER_AGAIN_DIALOG_FRAGMENT";

    /*Address Constants*/
    public static final String ADDRESS_TO_EDIT = "ADDRESS_TO_EDIT";
    public static final String EDIT_ADDRESS = "EDIT_ADDRESS";
    public static final String EDIT_PROFILE_DIALOG_FRAGMENT_TAG_NAME = "EDIT_PROFILE_DIALOG_FRAGMENT_TAG_NAME";
    public static final String SINGLE_CLICK_SUBSCRIPTION_DIALOG_FLAG = "SINGLE_CLICK_SUBSCRIPTION_DIALOG_FLAG";
    public static final String PAGE_TITLE_KEY = "PAGE_TITLE_KEY";
    public static final String IS_FROM_ARTICLE = "IS_FROM_ARTICLE";
    public static final String SAVE_SHIPPING = "SAVE_SHIPPING";

    /*Legal info and help*/
    public static final String LEGAL_INFO_HELP_TITLE_KEY = "LEGAL_INFO_HELP_TITLE_KEY";

    /*Account Constant*/
    public static final String MOBILE_NO_UPDATE_FLAG = "MOBILE_NO_UPDATE_FLAG";
    public static final int UPDATE_MOBILE_NO_RESULT_OK = 12345678;
    public static final String TRANSACTION_RESULT = "TRANSACTION_RESULT";

    public static final String FAQ_CATEGORY_LIST = "FAQ_CATEGORY_LIST";
    public static final String FAQ_CONTENT_DATA = "FAQ_CONTENT_DATA";

    public static final String FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY = "FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY";
    public static final String OFFER_DETAILS_PAGE_ID = "OFFER_DETAILS_PAGE_ID";
    public static final String DELETE_CARD_CONFIRM_DILAOG = "DELETE_CARD_CONFIRM_DILAOG";
    public static final String OFFER_DETAILS = "OFFER_DETAILS";
    public static final String OFFER_MORE_DETAILS = "OFFER_MORE_DETAILS";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String FROM_CATEGORY = "FROM_CATEGORY";
    public static final String CATEGORY_NAME = "CATEGORY_NAME";
    public static final String BRAINSINS_FLAG = "BRAINSINS_FLAG";
    public static final String MANUFACTURER_FLAG = "MANUFACTURER_FLAG";
    public static final String IS_COLD_STORAGE = "IS_COLD_STORAGE";
    public static final String INTER_CITY_PRODUCT_LIST = "INTER_CITY_PRODUCT_LIST";
    public static final String IS_OUT_OF_STOCK = "IS_OUT_OF_STOCK";
    public static final String TRACK_ORDER_POSITION = "TRACK_ORDER_POSITION";
    public static final String SUB_CATEGORY_ID = "SUB_CATEGORY_ID";
    public static final String BRAND_FILTER_FLAG = "BRAND_FILTER_FLAG";
    public static final String BRAND_FILTER = "BRAND_FILTER";


    public static final String PLACED_SUCCESSFUL_FROM_FLAG = "PLACED_SUCCESSFUL_FROM_FLAG";
    public static final String SUCCESS = "success";
    public static final String ISSUE_ID = "issueId";
    public static final String SUBSCRIPTION_CANCEL_FLAG = "SUBSCRIPTION_CANCEL_FLAG";
    public static final String PRIMARY_ORDER_ID = "primaryOrderId";
    public static final String CUSTOMER_ID = "CUSTOMER_ID";
    public static final String M3_UNSUBSCRIBE_ORDER_STATUS = "Active";
    public static final String M3_PAYNOW_ORDER_STATUS = "Paynow";
    public static final String M3_TRACK_ORDER_ORDER_STATUS = "TrackOrder";
    public static final String SUBSCRIPTION_STATUS = "SUBSCRIPTION_STATUS";
    public static final String FROM_SUBSCRIPTION = "FROM_SUBSCRIPTION";
    public static final String IS_PRIME_USER = "IS_PRIME_USER";
    public static final String OFFER_PARTICULAR_LIST = "OFFER_PARTICULAR_LIST";
    public static final String OFFER_LIST = "OFFER_LIST";
    public static final String OFFER_PAGE_TITLE = "OFFER_PAGE_TITLE";
    public static final String ORDER_CONFIRMATION_PENDING_FLAG = "ORDER_CONFIRMATION_PENDING_FLAG";
    public static final String HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL = "HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL";



    //MORNING STAR
    public static final String ADDRESS = "ADDRESS";
    public static final String ADD_ADDRESS_FLAG = "ADD_ADDRESS_FLAG";
    public static final String MANUFACTURER_ID = "MANUFACTURER_ID";
    public static final String MANUFACTURER_NAME = "MANUFACTURER_NAME";
    public static final String BRAND_ID = "BRAND_ID";
    public static final String BRAND_NAME = "BRAND_NAME";
    public static final String BANNER_AND_CATEGORY_RESPONSE = "BANNER_AND_CATEGORY_RESPONSE";
    public static final String OTP = "OTP";
    public static final String INTENT_FROM_ALTERNATE_SALT = "ALTERNATE_SALT";
    public static final String INTENT_FROM_PEOPLE_ALSO_VIEWED = "PEOPLE_ALSO_VIEWED";
    public static final String ALTERNATE_SALT_FACET_FILTER_QUERY = "ALTERNATE_SALT_FACET_FILTER_QUERY";
    public static final String ALTERNATE_SALT_FILTER_QUERY = "ALTERNATE_SALT_FILTER_QUERY";
    public static final String ALTERNATE_SALT_PRODUCT_CODE = "ALTERNATE_SALT_PRODUCT_CODE";
    public static final String IS_M2_ORDER = "IS_M2_ORDER";
    public static final String ALTERNATE_SALT_LIST = "ALTERNATE_SALT_LIST";
    public static final String PEOPLE_ALSO_VIEWED_LIST = "PEOPLE_ALSO_VIEWED_LIST";
    public static final String ADDED_PRODUCT_RESULT = "ADDED_PRODUCT_RESULT";
    public static final String WELLNESS_VIEW_ALL_CODES = "WELLNESS_VIEW_ALL_CODES";
    public static final String IS_MSTAR_TEMP_ORDER = "IS_MSTAR_TEMP_ORDER";
    public static final String IS_FROM_PAYMENT = "IS_FROM_PAYMENT";
    public static final String FROM_PAYMENT_FAILURE = "FROM_PAYMENT_FAILURE";
    public static final String SEARCH_QUERY_TEXT = "SEARCH_QUERY_TEXT";
    public static final String INDIVIDUAL_OFFER_SUB_LIST = "INDIVIDUAL_OFFER_SUB_LIST";
    public static final String CART_SUBSTITUTION = "CART_SUBSTITUTION";
    public static final String OUT_OF_STOCK_LIST = "OUT_OF_STOCK_LIST";
    public static final String MAX_LIMIT_REACH_LIST = "MAX_LIMIT_REACH_LIST";
    public static final String CART_DETAILS = "CART_DETAILS";
    public static final String IS_FROM_HOME_SCREEN = "IS_FROM_HOME_SCREEN";
    public static final String IS_FROM_SEARCH = "IS_FROM_SEARCH";
    public static final String IS_FROM_NEW_CUSTOMER= "IS_FROM_NEW_CUSTOMER";

    public static final String IS_FROM_GENERIC = "IS_FROM_GENERIC";
    public static final String INTENT_FROM_BRAND_URL ="IS_FOR_BRAND_URL" ;
    public static final String BRAND_URL ="BRAND_URL" ;
    public static final String PRODUCT_TYPE ="PRODUCT_TYPE";
    public static final String GENERIC_SEARCH_TEXT = "GENERIC_SEARCH_TEXT";
    public static final String RESEND_OTP_ERROR_MESSAGE = "RESEND_OTP_ERROR_MESSAGE";
    public static final String RESEND_OTP_ERROR_MOBILE_NO = "RESEND_OTP_ERROR_MOBILE_NO";
}
