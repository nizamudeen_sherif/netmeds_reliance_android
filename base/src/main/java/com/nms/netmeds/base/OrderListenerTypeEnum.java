package com.nms.netmeds.base;

public enum OrderListenerTypeEnum {
    TRACK_ORDER,
    VIEW_ORDER_DETAILS,
    RE_ORDER, M2_RE_ORDER, PAY, M1_RETRY, PAY_WITH_COD, CANCEL_ORDER, EDIT_ORDER, SUBSCRIPTION_PAY_NOW
}
