package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class CartSubstitutionResponseModel {

    @SerializedName("alternatives")
    private List<Alternative> alternatives = null;
    @SerializedName("total_savings")
    private BigDecimal totalSavings = BigDecimal.ZERO;
    @SerializedName("total_savings_pct")
    private String totalSavingsPercentage;
    @SerializedName("original_cart_value")
    private BigDecimal originalCartTotalValue = BigDecimal.ZERO;
    @SerializedName("alternate_cart_value")
    private BigDecimal alternateCartTotalValue = BigDecimal.ZERO;

    public List<Alternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    public BigDecimal getTotalSavings() {
        return totalSavings;
    }

    public void setTotalSavings(BigDecimal totalSavings) {
        this.totalSavings = totalSavings;
    }

    public String getTotalSavingsPercentage() {
        return totalSavingsPercentage;
    }

    public void setTotalSavingsPercentage(String totalSavingsPercentage) {
        this.totalSavingsPercentage = totalSavingsPercentage;
    }

    public BigDecimal getOriginalCartTotalValue() {
        return originalCartTotalValue;
    }

    public void setOriginalCartTotalValue(BigDecimal originalCartTotalValue) {
        this.originalCartTotalValue = originalCartTotalValue;
    }

    public BigDecimal getAlternateCartTotalValue() {
        return alternateCartTotalValue;
    }

    public void setAlternateCartTotalValue(BigDecimal alternateCartTotalValue) {
        this.alternateCartTotalValue = alternateCartTotalValue;
    }
}