package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class MstarOrders implements Serializable {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("name")
    private String name;
    @SerializedName("purchasedDate")
    private String purchasedDate;
    @SerializedName("orderAmount")
    private BigDecimal orderAmount = BigDecimal.ZERO;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("statusDescription")
    private String statusDescription;
    @SerializedName("statusColor")
    private String statusColor;
    @SerializedName("item")
    private List<String> item;
    @SerializedName("items")
    private List<MstarItems> itemsList;
    @SerializedName("itemCount")
    private int itemCount;
    @SerializedName("isPrimeMembershipOrder")
    private boolean isPrimeMembershipOrder;
    @SerializedName("isPrimeCustomerOrder")
    private Boolean isPrimeCustomerOrder;
    @SerializedName("displayStatus")
    private String displayStatus;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public List<String> getItem() {
        return item;
    }

    public void setItem(List<String> item) {
        this.item = item;
    }


    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public boolean getPrimeMembershipOrder() {
        return isPrimeMembershipOrder;
    }

    public void setPrimeMembershipOrder(Boolean primeMembershipOrder) {
        isPrimeMembershipOrder = primeMembershipOrder;
    }

    public Boolean getPrimeCustomerOrder() {
        return isPrimeCustomerOrder;
    }

    public void setPrimeCustomerOrder(Boolean primeCustomerOrder) {
        isPrimeCustomerOrder = primeCustomerOrder;
    }

    public List<MstarItems> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<MstarItems> itemsList) {
        this.itemsList = itemsList;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }
}
