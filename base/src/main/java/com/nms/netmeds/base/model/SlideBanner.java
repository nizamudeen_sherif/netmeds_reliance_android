package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class SlideBanner {
    @SerializedName("linkpage")
    private String linkpage;
    @SerializedName("linktype")
    private String linktype;
    @SerializedName("imageUrl")
    private String imageUrl;

    public void setLinkpage(String linkpage) {
        this.linkpage = linkpage;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getLinkpage() {
        return linkpage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLinktype() {
        return linktype;
    }
}
