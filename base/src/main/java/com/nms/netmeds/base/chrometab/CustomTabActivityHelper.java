package com.nms.netmeds.base.chrometab;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.browser.customtabs.CustomTabsCallback;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;

import com.nms.netmeds.base.chrometab.shared.CustomTabsHelper;
import com.nms.netmeds.base.chrometab.shared.ServiceConnection;
import com.nms.netmeds.base.chrometab.shared.ServiceConnectionCallback;

import java.util.List;

/**
 * Helper class to manage connection to the custom tab activity
 * <p>
 * https://github.com/GoogleChrome/custom-tabs-client/blob/master/demos/src/main/java/org/chromium/customtabsdemos/CustomTabActivityHelper.java
 */

public class CustomTabActivityHelper implements ServiceConnectionCallback {

    private CustomTabsSession mCustomTabsSession;
    private CustomTabsClient mCustomTabsClient;
    private CustomTabsServiceConnection mConnection;
    private CustomTabConnectionCallback mConnectionCallback;
    private CustomSessionCallBackListener mCustomSessionCallBackListener;
    private static CustomTabActivityHelper customTabActivityHelper;

    /**
     * Interface to handle cases where the chrome custom tab cannot be opened.
     */
    public interface CustomTabFallback {
        void openUri(Activity activity, Uri uri);
    }

    /**
     * Interface to handle connection callbacks to the custom tab. We'll use this to handle UI changes
     * when the connection is either connected or disconnected.
     */
    public interface CustomTabConnectionCallback {
        void onCustomTabConnected();

        void onCustomTabDisconnected();
    }

    public static CustomTabActivityHelper getInstance() {
        if (customTabActivityHelper == null)
            customTabActivityHelper = new CustomTabActivityHelper();
        return customTabActivityHelper;
    }

    /**
     * Utility method for opening a custom tab
     *
     * @param activity         Host activity
     * @param customTabsIntent custom tabs intent
     * @param uri              uri to open
     * @param fallback         fallback to handle case where custom tab cannot be opened
     */
    public static void openCustomTab(Activity activity, CustomTabsIntent customTabsIntent,
                                     Uri uri, CustomTabFallback fallback) {
        String packageName = CustomTabsHelper.getPackageNameToUse(activity);

        if (packageName == null) {
            // no package name, means there's no chromium browser.
            // Trigger fallback
            if (fallback != null) {
                fallback.openUri(activity, uri);
            }
        } else {
            // set package name to use
            customTabsIntent.intent.setPackage(packageName);
            customTabsIntent.launchUrl(activity, uri);
        }
    }


    /**
     * Binds the activity to the custom tabs service.
     *
     * @param context activity to be "bound" to the service
     */
    public void bindCustomTabsService(Context context) {
        if (mCustomTabsClient != null) return;

        String packageName = CustomTabsHelper.getPackageNameToUse(context);
        if (packageName == null) return;

        mConnection = new ServiceConnection(this);
        CustomTabsClient.bindCustomTabsService(context, packageName, mConnection);
    }

    /**
     * Unbinds the activity from the custom tabs service
     *
     * @param context
     */
    public void unbindCustomTabsService(Context context) {
        if (mConnection == null) return;
        context.unbindService(mConnection);
        mCustomTabsClient = null;
        mCustomTabsSession = null;
        mConnection = null;
    }

    /**
     * Creates or retrieves an exiting CustomTabsSession.
     *
     * @return a CustomTabsSession.
     */
    public CustomTabsSession getSession() {
        if (mCustomTabsClient == null) {
            mCustomTabsSession = null;
        } else if (mCustomTabsSession == null) {
            if (mCustomSessionCallBackListener != null)
                mCustomTabsSession = mCustomTabsClient.newSession(new CustomTabsCallback() {
                    @Override
                    public void onNavigationEvent(int navigationEvent, Bundle extras) {
                        switch (navigationEvent) {
                            case NAVIGATION_FINISHED:
                                mCustomSessionCallBackListener.onNavigationFinished();
                                break;
                        }
                    }
                });
            else
                mCustomTabsSession = mCustomTabsClient.newSession(null);
        }
        return mCustomTabsSession;
    }

    /**
     * Register a Callback to be called when connected or disconnected from the Custom Tabs Service.
     *
     * @param connectionCallback
     */
    public void setConnectionCallback(CustomTabConnectionCallback connectionCallback) {
        this.mConnectionCallback = connectionCallback;
    }

    /**
     * Register a Callback to be called when Custom session Navigation Finished.
     *
     * @param customSessionCallBackListener
     */
    public void setCustomSessionCallback(CustomSessionCallBackListener customSessionCallBackListener) {
        this.mCustomSessionCallBackListener = customSessionCallBackListener;
    }

    /**
     * @return true if call to mayLaunchUrl was accepted.
     * @see {@link CustomTabsSession#mayLaunchUrl(Uri, Bundle, List)}.
     */
    public boolean mayLaunchUrl(Uri uri, Bundle extras, List<Bundle> otherLikelyBundles) {
        if (mCustomTabsClient == null) return false;

        CustomTabsSession session = getSession();
        return session != null && session.mayLaunchUrl(uri, extras, otherLikelyBundles);
    }

    @Override
    public void onServiceConnected(CustomTabsClient client) {
        mCustomTabsClient = client;
        mCustomTabsClient.warmup(0L);
        if (mConnectionCallback != null) {
            mConnectionCallback.onCustomTabConnected();
        }
    }

    @Override
    public void onServiceDisconnected() {
        mCustomTabsClient = null;
        mConnection = null;
        if (mConnectionCallback != null) {
            mConnectionCallback.onCustomTabDisconnected();
        }
    }

    /**
     * Creates or retrieves an exiting CustomTabsClient.
     *
     * @return a CustomTabsClient.
     */
    public CustomTabsClient getClient() {
        return mCustomTabsClient;
    }


    /**
     * Interface for CustomSession callback listener.
     */
    public interface CustomSessionCallBackListener {
        void onNavigationFinished();
    }
}
