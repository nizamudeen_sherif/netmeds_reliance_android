package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarUseableBalanceBreakup {

    @SerializedName("balance_type")
    private String balanceType;
    @SerializedName("balance_id")
    private Integer balanceId;
    @SerializedName("balance_amt")
    private Float balanceAmt;
    @SerializedName("enddate")
    private String enddate;

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public Integer getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(Integer balanceId) {
        this.balanceId = balanceId;
    }

    public Float getBalanceAmt() {
        return balanceAmt;
    }

    public void setBalanceAmt(Float balanceAmt) {
        this.balanceAmt = balanceAmt;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

}
