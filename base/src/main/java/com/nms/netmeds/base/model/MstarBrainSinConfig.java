package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarBrainSinConfig {

    @SerializedName("productTryAndBuy")
    private MstarBrainSinConfigDetails productTryAndBuy;
    @SerializedName("cartTryAndBuy")
    private MstarBrainSinConfigDetails cartTryAndBuy;
    @SerializedName("peopleAlsoView")
    private MstarBrainSinConfigDetails peopleAlsoView;

    public MstarBrainSinConfigDetails getProductTryAndBuy() {
        return productTryAndBuy;
    }

    public void setProductTryAndBuy(MstarBrainSinConfigDetails productTryAndBuy) {
        this.productTryAndBuy = productTryAndBuy;
    }

    public MstarBrainSinConfigDetails getCartTryAndBuy() {
        return cartTryAndBuy;
    }

    public void setCartTryAndBuy(MstarBrainSinConfigDetails cartTryAndBuy) {
        this.cartTryAndBuy = cartTryAndBuy;
    }

    public MstarBrainSinConfigDetails getPeopleAlsoView() {
        return peopleAlsoView;
    }

    public void setPeopleAlsoView(MstarBrainSinConfigDetails peopleAlsoView) {
        this.peopleAlsoView = peopleAlsoView;
    }
}
