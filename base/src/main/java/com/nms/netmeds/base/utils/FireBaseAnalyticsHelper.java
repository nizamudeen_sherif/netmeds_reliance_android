/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.utils;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.ConcernSlides;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarDrugDetail;
import com.nms.netmeds.base.model.MstarItems;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.model.MstarOrderDetail;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;
import com.nms.netmeds.base.model.MstarOrders;
import com.nms.netmeds.base.model.Variant;
import com.nms.netmeds.base.model.VariantFacet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FireBaseAnalyticsHelper {
    //Event Name
    private static final String EVENT_NAME_ECOMMERCE_PURCHASE_FIRST = "ecommerce_purchase_first";
    private static final String EVENT_NAME_ALTERNATE_CART = "alternate_cart";
    private static final String CURRENCY = "INR";
    private static final String FIRST = "FIRST";
    private static final String EVENT_PARAM_ORDER_ID = "order_id";
    private static final String EVENT_PARAM_OLD_PRICE = "old_price";
    private static final String EVENT_PARAM_NEW_PRICE = "new_price";
    private static final String EVENT_PARAM_DATE = "date";
    private static final String EVENT_PARAM_ALTERNATE_PRODUCT_NAME = "alternate_product_name";
    private static final String EVENT_PARAM_ALTERNATE_PRODUCT_SKU_ID = "alternate_product_sku_id";
    private static final String EVENT_PARAM_ALTERNATE_PRODUCT_QUANTITY = "alternate_product_quantity";
    private static final String EVENT_PARAM_ALTERNATE_PRODUCT_PRICE = "alternate_product_price";
    private static final String EVENT_PARAM_ORIGINAL_PRODUCT_NAME = "original_product_name";
    private static final String EVENT_PARAM_ORIGINAL_PRODUCT_SKU_ID = "original_product_sku_id";
    private static final String EVENT_PARAM_ORIGINAL_PRODUCT_QUANTITY = "original_product_quantity";
    private static final String EVENT_PARAM_ORIGINAL_PRODUCT_PRICE = "original_product_price";
    private static final String EVENT_PARAM_CART_ITEMS = "cart_items";
    private static final String EVENT_PARAM_APP_OPEN = "app_open";
    private static final String EVENT_PARAM_CART_ID = "cart_id";
    private static final String EVENT_PARAM_NMS_SUPERCASH = "nms_superCash";
    private static final String EVENT_PARAM_NMS_CASH = "nms_cash";
    private static final String EVENT_PARAM_VOUCHER = "voucher";
    private static final String EVENT_PARAM_CHECKOUT_ADDRESS_SELECTED = "checkout_address";
    private static final String EVENT_PARAM_CHECKOUT_ADD_ADDRESS = "checkout_address_add";
    private static final String EVENT_PARAM_CHECKOUT_UPLOAD = "checkout_upload";
    private static final String EVENT_PARAM_ADDRESS_SELECTED = "address_selected";
    private static final String EVENT_PARAM_NEW_ADDRESS_ADDED = "new_address_added";
    private static final String EVENT_PARAM_UPLOAD_DOCTOR_CONSULT = "upload_doctor_consult_done";
    private static final String EVENT_PARAM_CHECKOUT_ORDER_REVIEW = "checkout_order_review";
    private static final String EVENT_PARAM_CHECKOUT_ORDER_REVIEW_DONE = "order_review_done";
    private static final String EVENT_PARAM_LOGIN = "login";
    private static final String EVENT_PARAM_SIGNUP = "signup";
    private static final String EVENT_PARAM_ORDER_CANCELLATION = "order_cancellation";
    private static final String EVENT_PARAM_REASON = "reason";
    private static final String EVENT_PARAM_NET_PAYABLE = "net_payable";
    private static final String EVENT_PARAM_COUPON_DISCOUNT = "coupon_discount";
    private static final String EVENT_PARAM_DEFAULT_DISCOUNT = "default_discount";
    private static final String EVENT_PARAM_IS_FIRST_USER = "isfirst_user";
    private static final String EVENT_PARAM_IS_ORDER_ID = "order_id";
    private static final String EVENT_PARAM_WALLET = "wallet";
    private static final String EVENT_PARAM_QTY = "qty";
    private static final String EVENT_PARAM_ITEMS = "items";
    public static final String EVENT_PARAM_SEARCH_RESULTS = "Search Results";
    public static final String EVENT_PARAM_CATEGORY_LIST = "Category List";
    public static final String EVENT_PARAM_CART_PAGE = "Cart Page";
    public static final String EVENT_PARAM_ATTACH_PRESCRIPTION = "Attach Prescription";
    public static final String EVENT_PARAM_SELECT_ADDRESS = "Select Address";
    public static final String EVENT_PARAM_ORDER_REVIEW = "Order Review";
    public static final String EVENT_PARAM_PAYMENT_DETAILS = "Payment Details";
    private static final String EVENT_PARAM_PROMOTIONS = "promotions";
    private static final String EVENT_PARAM_INTERNAL_PROMOTIONS = "Internal Promotions";
    public static final String EVENT_PARAM_HOME_MAIN = "home_main";
    public static final String EVENT_PARAM_HOME_PAYMENT = "home_payment";
    public static final String EVENT_PARAM_HOME_DIAGNOSTIC_POPULAR = "home_diag_popular";
    public static final String EVENT_PARAM_HOME_DEALS = "home_deals";
    public static final String EVENT_PARAM_HOME_TRENDING = "home_trending";
    private static final String EVENT_PARAM_PAGE = "PAGE";
    private static final String TRANSACTION_AFFILIATION = "Netmeds Mobile";
    private static final long CHECKOUT_STEP_1 = 1;
    public static final long CHECKOUT_STEP_2 = 2;
    public static final long CHECKOUT_STEP_3 = 3;
    public static final long CHECKOUT_STEP_4 = 4;
    public static final long CHECKOUT_STEP_5 = 5;
    public static final String MAIN = "main";
    public static final String WELLNESS = "wellness";
    public static final String VIEW_ON_PAGE = "VIEW_ON_PAGE";
    public static final String VIEW_SEARCH_RESULTS = "VIEW_SEARCH_RESULTS ";
    private static final String RESULTS = "Results ";
    private static final String TRUE = "true";
    private static final String FALSE = "false";

    private static FireBaseAnalyticsHelper fireBaseAnalyticsHelper;
    private Bundle params;
    private String EVENT_PARAM_BRAND_TOTAL_PRICE = "brand_total_price";
    private String EVENT_PARAM_GENERIC_TOTAL_PRICE = "generic_total_price";
    private String EVENT_PARAM_GENERIC_PRODUCT_NAME = "generic_product_name";
    private String EVENT_PARAM_BRAND_PRODUCT_NAME = "brand_product_name";
    private String EVENT_PARAM_COMPOLSITION = "composition";
    private String EVENT_NAME_GENERIC_PURCHASE = "generic_purchase";
    private String EVENT_NAME_BUY_AGAIN = "buy_again";
    private String EVENT_NAME_PRODUCT_NAME = "product_name";
    private String EVENT_NAME_GENRIC_TOTAL_PRICE = "total_price";

    /**
     * Create a getInstance() method to return the instance of
     * FireBaseAnalyticsHelper.This function ensures that
     * the variable is instantiated only once and the same
     * instance is used entire application
     *
     * @return FireBaseAnalyticsHelper instance
     */
    public static FireBaseAnalyticsHelper getInstance() {
        if (fireBaseAnalyticsHelper == null) {
            fireBaseAnalyticsHelper = new FireBaseAnalyticsHelper();
        }
        return fireBaseAnalyticsHelper;
    }

    /**
     * Method to add string value to the bundle
     *
     * @param name  event param name
     * @param value sending string value
     */
    private void putString(String name, String value) {
        params.putString(name, value);
    }

    /**
     * Method to add double value to the bundle
     *
     * @param name  event param name
     * @param value sending double value
     */
    private void putDouble(String name, double value) {
        params.putDouble(name, value);
    }

    /**
     * @param context   current state of the application/object
     * @param eventName name of the event
     * @param bundle    sending params
     */
    private void logFireBaseEvent(Context context, String eventName, Bundle bundle) {
        getFireBaseInstance(context).logEvent(eventName, bundle);
    }

    /**
     * @param context        current state of the application/object
     * @param orderId        transaction id of the order
     * @param appFirstOrder  check order is first or not
     * @param totalAmount    order total Amount
     * @param shippingAmount order shipping amount
     * @param cartDetails    order total information
     */
    public void fireBasePurchaseEvent(Context context, String orderId, String appFirstOrder, double totalAmount, double shippingAmount, MStarCartDetails cartDetails, double nmsSuperCash, double nmsCash, String voucher, String city, double subTotal, double netPayable, double couponDiscount, double defaultDiscount) {
        if (context != null) {
            params = new Bundle();
            String firstOrder = !TextUtils.isEmpty(appFirstOrder) ? appFirstOrder.equalsIgnoreCase(AppConstant.YES) ? FIRST : "" : "";
            String couponCode = cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? cartDetails.getAppliedCoupons() : "";

            putString(Param.COUPON, couponCode);
            putString(Param.CURRENCY, CURRENCY);
            putDouble(Param.VALUE, totalAmount);
            putDouble(Param.SHIPPING, shippingAmount);
            putString(EVENT_PARAM_IS_ORDER_ID, orderId);
            putString(Param.MEDIUM, AppConstant.ANDROID);
            putString(Param.LOCATION, city);
            putDouble(Param.VALUE, subTotal);
            putDouble(EVENT_PARAM_NET_PAYABLE, netPayable);
            putDouble(EVENT_PARAM_COUPON_DISCOUNT, couponDiscount);
            putDouble(EVENT_PARAM_DEFAULT_DISCOUNT, defaultDiscount);
            putDouble(EVENT_PARAM_NMS_SUPERCASH, nmsSuperCash);
            putDouble(EVENT_PARAM_NMS_CASH, nmsCash);
            putString(EVENT_PARAM_VOUCHER, voucher);
            if (firstOrder.equals(FIRST)) {
                primeCheck();
                logFireBaseEvent(context, EVENT_NAME_ECOMMERCE_PURCHASE_FIRST, params);
            } else {
                primeCheck();
                logFireBaseEvent(context, FirebaseAnalytics.Event.ECOMMERCE_PURCHASE + 1, params);
            }

        }
    }

    /**
     * Alternate cart event
     *
     * @param context current state of the application/object
     */
    public void alternateCartFireBaseEvent(Context context, MStarCartDetails cartDetails) {
        if (context != null) {
            params = new Bundle();
            putString(EVENT_PARAM_ORDER_ID, cartDetails.getOrder_id());
            putDouble(EVENT_PARAM_OLD_PRICE, PaymentHelper.getOriginalCartAmount().doubleValue());
            putDouble(EVENT_PARAM_NEW_PRICE, cartDetails.getNetPayableAmount().doubleValue());
            putString(Param.CURRENCY, CURRENCY);
            putString(EVENT_PARAM_DATE, DateTimeUtils.getInstance().currentDate(DateTimeUtils.yyyyMMddHHmmss));
            params.putParcelableArrayList(EVENT_PARAM_CART_ITEMS, getAlternateCartList(cartDetails));

            logFireBaseEvent(context, EVENT_NAME_ALTERNATE_CART, params);
        }
    }

    private ArrayList<Bundle> getAlternateCartList(MStarCartDetails cartDetails) {
        ArrayList<Bundle> alternateCartList = new ArrayList<>();
        for (MStarProductDetails productDetails : cartDetails.getLines()) {
            if (productDetails.isAlternateSwitched()) {
                Bundle bundle = new Bundle();
                bundle.putString(EVENT_PARAM_ALTERNATE_PRODUCT_NAME, !TextUtils.isEmpty(productDetails.getAlternateProductName()) ? productDetails.getAlternateProductName() : "");
                bundle.putDouble(EVENT_PARAM_ALTERNATE_PRODUCT_PRICE, productDetails.getAlternateProductLineValue().doubleValue());
                bundle.putString(EVENT_PARAM_ORIGINAL_PRODUCT_NAME, !TextUtils.isEmpty(productDetails.getDisplayName()) ? productDetails.getDisplayName() : "");
                bundle.putString(EVENT_PARAM_ORIGINAL_PRODUCT_SKU_ID, String.valueOf(productDetails.getProductCode()));
                bundle.putInt(EVENT_PARAM_ORIGINAL_PRODUCT_QUANTITY, productDetails.getCartQuantity());
                bundle.putDouble(EVENT_PARAM_ORIGINAL_PRODUCT_PRICE, productDetails.getLineValue().doubleValue());
                //todo need to get from API
                // bundle.putString(EVENT_PARAM_ALTERNATE_PRODUCT_SKU_ID, !TextUtils.isEmpty(productDetails.getA()) ? productDetails.getAlternateProductCode() : "");
                // bundle.putInt(EVENT_PARAM_ALTERNATE_PRODUCT_QUANTITY, productDetails.getAlternateProductQuantity());
                putString(Param.CURRENCY, CURRENCY);
                alternateCartList.add(bundle);
            }
        }
        return alternateCartList;
    }

    public void openApp(Context context, String openApp) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_APP_OPEN, openApp);
            logFireBaseEvent(context, FirebaseAnalytics.Event.APP_OPEN, bundle);
        }
    }

    public void CheckoutAddressSelectEvent(Context context, String address) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_ADDRESS_SELECTED, address);
            logFireBaseEvent(context, EVENT_PARAM_CHECKOUT_ADDRESS_SELECTED, bundle);
        }
    }

    public void CheckoutAddAddressEvent(Context context, String address) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_NEW_ADDRESS_ADDED, address);
            logFireBaseEvent(context, EVENT_PARAM_CHECKOUT_ADD_ADDRESS, bundle);
        }
    }

    public void CheckoutUploadEvent(Context context, String consult) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_UPLOAD_DOCTOR_CONSULT, consult);
            logFireBaseEvent(context, EVENT_PARAM_CHECKOUT_UPLOAD, bundle);
        }
    }

    public void CheckoutOrderReviewEvent(Context context, String method) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_CHECKOUT_ORDER_REVIEW_DONE, method);
            logFireBaseEvent(context, EVENT_PARAM_CHECKOUT_ORDER_REVIEW, bundle);
        }
    }

    public void loginEvent(Context context, String method) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_LOGIN, method);
            logFireBaseEvent(context, FirebaseAnalytics.Event.LOGIN, bundle);
        }
    }

    public void shareEvent(Context context, String contentType, String shareCode, String method) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Param.CONTENT_TYPE, contentType);
            bundle.putString(Param.ITEM_ID, shareCode);
            bundle.putString(Param.METHOD, method);
            logFireBaseEvent(context, FirebaseAnalytics.Event.SHARE, bundle);
        }
    }

    public void sigupEvent(Context context, String method) {
        if (context != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EVENT_PARAM_SIGNUP, method);
            logFireBaseEvent(context, FirebaseAnalytics.Event.SIGN_UP, bundle);
        }
    }

    private void primeCheck() {
        if (PaymentHelper.isIsPrime()) {
            putString(EVENT_PARAM_IS_FIRST_USER, "true");
        } else {
            putString(EVENT_PARAM_IS_FIRST_USER, "false");
        }
    }

    /**
     * Create firebase analytics object
     *
     * @param context current state of the application/object
     * @return FirebaseAnalytics instance
     */
    private FirebaseAnalytics getFireBaseInstance(Context context) {
        return FirebaseAnalytics.getInstance(context);
    }

    /**
     * Tracking screen Name
     *
     * @param context    context current state of the application/object
     * @param screenName activity name
     */
    void logFireBaseAnalyticsScreenName(Context context, String screenName) {
        getFireBaseInstance(context).setCurrentScreen((Activity) context, screenName, null);
    }

    public void genericProductFireBaseEvent(Context context, String orderId, MStarCartDetails cartDetails, BasePreference basePreference) {
        String composition = "";
        String generic_product_name = "";
        String brand_product_name = "";
        double brand_total_price = 0;
        double generic_total_price = 0;
        for (MStarProductDetails cartProductDetails : cartDetails.getLines()) {
            if (basePreference.getGenericProductMap() != null && basePreference.getGenericBrandProductMap() != null && basePreference.getGenericProductMap().containsKey(String.valueOf(cartProductDetails.getProductCode()))) {
                MStarProductDetails genericProductDetails = basePreference.getGenericProductMap().get(String.valueOf(cartProductDetails.getProductCode()));
                MStarProductDetails genericBrandProductDetails = basePreference.getGenericBrandProductMap().get(String.valueOf(cartProductDetails.getProductCode()));
                brand_total_price = brand_total_price + genericBrandProductDetails.getSellingPrice().doubleValue();
                generic_total_price = generic_total_price + genericProductDetails.getLineValue().doubleValue();
                if (!TextUtils.isEmpty(generic_product_name)) {
                    generic_product_name = generic_product_name + ", " + genericProductDetails.getDisplayName();
                    brand_product_name = brand_product_name + ", " + genericBrandProductDetails.getDisplayName();
                    composition = composition + ", " + genericProductDetails.getGenericDosage();
                } else {
                    generic_product_name = genericProductDetails.getDisplayName();
                    brand_product_name = genericBrandProductDetails.getDisplayName();
                    composition = genericProductDetails.getGenericDosage();
                }
            }
        }
        if (context != null && !TextUtils.isEmpty(generic_product_name)) {
            params = new Bundle();
            putString(EVENT_PARAM_IS_ORDER_ID, orderId);
            putDouble(EVENT_PARAM_BRAND_TOTAL_PRICE, brand_total_price);
            putDouble(EVENT_PARAM_GENERIC_TOTAL_PRICE, generic_total_price);
            putString(EVENT_PARAM_GENERIC_PRODUCT_NAME, generic_product_name);
            putString(EVENT_PARAM_BRAND_PRODUCT_NAME, brand_product_name);
            putString(EVENT_PARAM_COMPOLSITION, composition);
            logFireBaseEvent(context, EVENT_NAME_GENERIC_PURCHASE, params);
        }
    }

    public void buyAndAgain(Context context, String orderId, MStarCartDetails productDetails) {
        Bundle bundle = new Bundle();
        if (PaymentHelper.getmStarProductDetails() != null && productDetails != null && context != null) {
            String productName = "";
            int quantity = 0;
            BigDecimal sellingPrice = null;
            String totalPrice = "";
            String productQuantity = "";
            for (MStarProductDetails cartDetails : productDetails.getLines()) {
                for (MStarProductDetails mStarProductDetails : PaymentHelper.getmStarProductDetails()) {
                    if (mStarProductDetails.getDisplayName().equalsIgnoreCase(cartDetails.getDisplayName())) {
                        if (!TextUtils.isEmpty(productName)) {
                            quantity = cartDetails.getCartQuantity();
                            productName = productName + "," + cartDetails.getDisplayName();
                            totalPrice = sellingPrice + "," + mStarProductDetails.getSellingPrice().multiply(BigDecimal.valueOf(quantity));
                            productQuantity = productQuantity + "," + cartDetails.getCartQuantity();
                        } else {
                            quantity = cartDetails.getCartQuantity();
                            sellingPrice = mStarProductDetails.getSellingPrice().multiply(BigDecimal.valueOf(quantity));
                            productName = cartDetails.getDisplayName();
                            productQuantity = String.valueOf(quantity);
                        }


                    }

                }

            }
            bundle.putString(EVENT_NAME_PRODUCT_NAME, productName);
            bundle.putString(EVENT_PARAM_IS_ORDER_ID, !TextUtils.isEmpty(orderId) ? orderId : "");
            bundle.putString(EVENT_PARAM_QTY, productQuantity);
            bundle.putString(EVENT_NAME_GENRIC_TOTAL_PRICE, totalPrice);
            logFireBaseEvent(context, EVENT_NAME_BUY_AGAIN, params);

        }
    }

    /******************************************************Google Tag Manager Event Start***********************************************************************/

    public <T> void logFireBaseProductImpressionEvent(Context context, List<T> productImpressionList, int startingPosition, String listName, String page) {
        if (context != null) {
            ArrayList<Bundle> itemsList = new ArrayList<>();
            Bundle productImpression;
            if (productImpressionList != null && productImpressionList.size() > 0) {
                int position = startingPosition;
                for (T productDetail : productImpressionList) {
                    if (productDetail instanceof MstarAlgoliaResult) {
                        MstarAlgoliaResult mstarAlgoliaResult = (MstarAlgoliaResult) productDetail;
                        productImpression = new Bundle();
                        productImpression.putString(Param.ITEM_ID, Integer.toString(mstarAlgoliaResult.getProductCode()));
                        productImpression.putString(Param.ITEM_NAME, checkEmpty(mstarAlgoliaResult.getDisplayName()));
                        productImpression.putString(Param.ITEM_CATEGORY, getCategoryName(mstarAlgoliaResult, getLevelBasedOnProductType(mstarAlgoliaResult)));
                        productImpression.putString(Param.ITEM_VARIANT, "");
                        productImpression.putString(Param.ITEM_BRAND, checkEmpty(mstarAlgoliaResult.getBrand()));
                        productImpression.putDouble(Param.PRICE, mstarAlgoliaResult.getSellingPrice().doubleValue());
                        productImpression.putString(Param.CURRENCY, CURRENCY);
                        productImpression.putLong(Param.INDEX, (long) position);
                        itemsList.add(productImpression);
                    } else if (productDetail instanceof MStarProductDetails) {
                        MStarProductDetails mStarProductDetails = (MStarProductDetails) productDetail;
                        productImpression = new Bundle();
                        productImpression.putString(Param.ITEM_ID, Integer.toString(mStarProductDetails.getProductCode()));
                        productImpression.putString(Param.ITEM_NAME, checkEmpty(mStarProductDetails.getDisplayName()));
                        productImpression.putString(Param.ITEM_CATEGORY, getCategoryName(mStarProductDetails.getCategories(), getProductType(mStarProductDetails)));
                        productImpression.putString(Param.ITEM_VARIANT, getProductVariant(mStarProductDetails.getProductVariant()));
                        productImpression.putString(Param.ITEM_BRAND, checkEmpty(mStarProductDetails.getBrandName()));
                        productImpression.putDouble(Param.PRICE, mStarProductDetails.getSellingPrice().doubleValue());
                        productImpression.putString(Param.CURRENCY, CURRENCY);
                        productImpression.putLong(Param.INDEX, (long) position);
                        itemsList.add(productImpression);
                    }
                    position++;
                }
            }

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putParcelableArrayList(EVENT_PARAM_ITEMS, itemsList);
            if (!listName.equals(VIEW_SEARCH_RESULTS)) {
                ecommerceBundle.putString(EVENT_PARAM_PAGE, page);
                ecommerceBundle.putString(Param.ITEM_LIST, page + " " + RESULTS);
            } else
                ecommerceBundle.putString(Param.ITEM_LIST, EVENT_PARAM_SEARCH_RESULTS);
            logFireBaseEvent(context, listName, ecommerceBundle);
        }
    }

    public <T> void logFireBaseProductClickAndSelectionEvent(Context context, T productDetails, int position, String listName) {
        if (context != null && productDetails != null) {
            Bundle productClick = new Bundle();
            if (productDetails instanceof MstarAlgoliaResult) {
                MstarAlgoliaResult mstarAlgoliaResult = (MstarAlgoliaResult) productDetails;
                productClick.putString(Param.ITEM_ID, Integer.toString(mstarAlgoliaResult.getProductCode()));
                productClick.putString(Param.ITEM_NAME, checkEmpty(mstarAlgoliaResult.getDisplayName()));
                productClick.putString(Param.ITEM_CATEGORY, getCategoryName(mstarAlgoliaResult, getLevelBasedOnProductType(mstarAlgoliaResult)));
                productClick.putString(Param.ITEM_VARIANT, "");
                productClick.putString(Param.ITEM_BRAND, checkEmpty(mstarAlgoliaResult.getBrand()));
                productClick.putDouble(Param.PRICE, mstarAlgoliaResult.getSellingPrice().doubleValue());
                productClick.putString(Param.CURRENCY, CURRENCY);
                productClick.putLong(Param.INDEX, (long) position);
            } else if (productDetails instanceof MStarProductDetails) {
                MStarProductDetails productDetail = (MStarProductDetails) productDetails;
                productClick.putString(Param.ITEM_ID, Integer.toString(productDetail.getProductCode()));
                productClick.putString(Param.ITEM_NAME, checkEmpty(productDetail.getDisplayName()));
                productClick.putString(Param.ITEM_CATEGORY, getCategoryName(productDetail.getCategories(), getProductType(productDetail)));
                productClick.putString(Param.ITEM_VARIANT, getProductVariant(productDetail.getProductVariant()));
                productClick.putString(Param.ITEM_BRAND, checkEmpty(productDetail.getBrandName()));
                productClick.putDouble(Param.PRICE, productDetail.getSellingPrice().doubleValue());
                productClick.putString(Param.CURRENCY, CURRENCY);
                productClick.putLong(Param.INDEX, (long) position);
            }

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putBundle(EVENT_PARAM_ITEMS, productClick);
            ecommerceBundle.putString(Param.ITEM_LIST, listName);
            logFireBaseEvent(context, Event.SELECT_CONTENT, ecommerceBundle);
        }
    }


    public void logFireBaseProductDetailViewEvent(Context context, MStarProductDetails productDetails) {
        if (context != null) {
            Bundle productDetail = new Bundle();
            productDetail.putString(Param.ITEM_ID, Integer.toString(productDetails.getProductCode()));
            productDetail.putString(Param.ITEM_NAME, checkEmpty(productDetails.getDisplayName()));
            productDetail.putString(Param.ITEM_CATEGORY, getCategoryName(productDetails.getCategories(), getProductType(productDetails)));
            productDetail.putString(Param.ITEM_VARIANT, getProductVariant(productDetails.getProductVariant()));
            productDetail.putString(Param.ITEM_BRAND, checkEmpty(productDetails.getBrandName()));
            productDetail.putDouble(Param.PRICE, productDetails.getSellingPrice().doubleValue());
            productDetail.putString(Param.CURRENCY, CURRENCY);

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putBundle(EVENT_PARAM_ITEMS, productDetail);
            logFireBaseEvent(context, Event.VIEW_ITEM, ecommerceBundle);
        }
    }

    public void logFireBaseAddToCartEvent(Context context, MStarProductDetails product, int quantity) {
        if (context != null) {
            Bundle addToCart = new Bundle();
            addToCart.putString(Param.ITEM_ID, Integer.toString(product.getProductCode()));
            addToCart.putString(Param.ITEM_NAME, checkEmpty(product.getDisplayName()));
            addToCart.putString(Param.ITEM_CATEGORY, getCategoryName(product.getCategories(), getProductType(product)));
            addToCart.putString(Param.ITEM_VARIANT, getProductVariant(product.getProductVariant()));
            addToCart.putString(Param.ITEM_BRAND, checkEmpty(product.getBrandName()));
            addToCart.putDouble(Param.PRICE, product.getSellingPrice().doubleValue());
            addToCart.putString(Param.CURRENCY, CURRENCY);
            addToCart.putLong(Param.QUANTITY, (long) quantity);

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putBundle(EVENT_PARAM_ITEMS, addToCart);
            logFireBaseEvent(context, Event.ADD_TO_CART, ecommerceBundle);
        }
    }

    public void logFireBaseRemoveFromCartEvent(Context context, MStarProductDetails product, int quantity) {
        if (context != null) {
            Bundle removeFromCart = new Bundle();
            removeFromCart.putString(Param.ITEM_ID, Integer.toString(product.getProductCode()));
            removeFromCart.putString(Param.ITEM_NAME, checkEmpty(product.getDisplayName()));
            removeFromCart.putString(Param.ITEM_CATEGORY, getCategoryName(product.getCategories(), getProductType(product)));
            removeFromCart.putString(Param.ITEM_VARIANT, getProductVariant(product.getProductVariant()));
            removeFromCart.putString(Param.ITEM_BRAND, checkEmpty(product.getBrandName()));
            removeFromCart.putDouble(Param.PRICE, product.getSellingPrice().doubleValue());
            removeFromCart.putString(Param.CURRENCY, CURRENCY);
            removeFromCart.putLong(Param.QUANTITY, (long) quantity);

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putBundle(EVENT_PARAM_ITEMS, removeFromCart);
            logFireBaseEvent(context, Event.REMOVE_FROM_CART, ecommerceBundle);
        }
    }

    public <T> void logFireBasePromotionImpressionEvent(Context context, List<T> promotionImpressionList, String creativeName, int startPosition) {
        if (context != null) {
            String promotionId = "";
            ArrayList<Bundle> itemsList = new ArrayList<>();
            if (promotionImpressionList != null && promotionImpressionList.size() > 0) {
                int position = startPosition;
                for (T promotion : promotionImpressionList) {
                    if (promotion instanceof MStarProductDetails) {
                        MStarProductDetails productDetails = (MStarProductDetails) promotion;
                        promotionId = Integer.toString(productDetails.getId());
                        Bundle promotionImpression = new Bundle();
                        promotionImpression.putString(Param.ITEM_ID, promotionId);
                        promotionImpression.putString(Param.ITEM_NAME, checkEmpty(productDetails.getName()));
                        promotionImpression.putString(Param.CREATIVE_NAME, creativeName);
                        promotionImpression.putString(Param.CREATIVE_SLOT, creativeName + position);
                        itemsList.add(promotionImpression);
                    } else if (promotion instanceof ConcernSlides) {
                        ConcernSlides concernSlides = (ConcernSlides) promotion;
                        promotionId = Integer.toString(position);
                        Bundle promotionImpression = new Bundle();
                        promotionImpression.putString(Param.ITEM_ID, promotionId);
                        promotionImpression.putString(Param.ITEM_NAME, checkEmpty(concernSlides.getTitle()));
                        promotionImpression.putString(Param.CREATIVE_NAME, creativeName);
                        promotionImpression.putString(Param.CREATIVE_SLOT, creativeName + position);
                        itemsList.add(promotionImpression);
                    } else if (promotion instanceof MstarNetmedsOffer) {
                        MstarNetmedsOffer mstarNetmedsOffer = (MstarNetmedsOffer) promotion;
                        promotionId = Integer.toString(position);
                        Bundle promotionImpression = new Bundle();
                        promotionImpression.putString(Param.ITEM_ID, promotionId);
                        promotionImpression.putString(Param.ITEM_NAME, checkEmpty(mstarNetmedsOffer.getTitle()));
                        promotionImpression.putString(Param.CREATIVE_NAME, creativeName);
                        promotionImpression.putString(Param.CREATIVE_SLOT, creativeName + position);
                        itemsList.add(promotionImpression);
                    } else if (promotion instanceof MstarBanner) {
                        MstarBanner mstarBanner = (MstarBanner) promotion;
                        promotionId = mstarBanner.getId();
                        String name = !TextUtils.isEmpty(mstarBanner.getBannerTitle()) ? mstarBanner.getBannerTitle() : !TextUtils.isEmpty(mstarBanner.getName()) ? mstarBanner.getName() : "";
                        Bundle promotionImpression = new Bundle();
                        promotionImpression.putString(Param.ITEM_ID, promotionId);
                        promotionImpression.putString(Param.ITEM_NAME, name);
                        promotionImpression.putString(Param.CREATIVE_NAME, creativeName);
                        promotionImpression.putString(Param.CREATIVE_SLOT, creativeName + position);
                        itemsList.add(promotionImpression);
                    }
                    position++;
                }
            }

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putParcelableArrayList(EVENT_PARAM_PROMOTIONS, itemsList);
            logFireBaseEvent(context, Event.VIEW_ITEM, ecommerceBundle);
        }
    }

    public <T> void logFireBasePromotionClickAndSelectionEvent(Context context, T promotion, String creativeName, int position) {
        if (context != null && promotion != null) {
            String promotionId = "";
            Bundle promotionClick = new Bundle();
            if (promotion instanceof MStarProductDetails) {
                MStarProductDetails productDetails = (MStarProductDetails) promotion;
                promotionId = Integer.toString(productDetails.getId());
                promotionClick.putString(Param.ITEM_ID, promotionId);
                promotionClick.putString(Param.ITEM_NAME, checkEmpty(productDetails.getName()));
            } else if (promotion instanceof ConcernSlides) {
                ConcernSlides concernSlides = (ConcernSlides) promotion;
                promotionId = Integer.toString(position);
                promotionClick.putString(Param.ITEM_ID, promotionId);
                promotionClick.putString(Param.ITEM_NAME, checkEmpty(concernSlides.getTitle()));
            } else if (promotion instanceof MstarNetmedsOffer) {
                MstarNetmedsOffer mstarNetmedsOffer = (MstarNetmedsOffer) promotion;
                promotionId = Integer.toString(position);
                promotionClick.putString(Param.ITEM_ID, promotionId);
                promotionClick.putString(Param.ITEM_NAME, checkEmpty(mstarNetmedsOffer.getTitle()));
            } else if (promotion instanceof MstarBanner) {
                MstarBanner mstarBanner = (MstarBanner) promotion;
                promotionId = mstarBanner.getId();
                String name = !TextUtils.isEmpty(mstarBanner.getBannerTitle()) ? mstarBanner.getBannerTitle() : !TextUtils.isEmpty(mstarBanner.getName()) ? mstarBanner.getName() : "";
                promotionClick.putString(Param.ITEM_ID, promotionId);
                promotionClick.putString(Param.ITEM_NAME, name);
            }

            promotionClick.putString(Param.CREATIVE_NAME, creativeName);
            promotionClick.putString(Param.CREATIVE_SLOT, creativeName + position);

            ArrayList<Bundle> promotionsClickList = new ArrayList<>();
            promotionsClickList.add(promotionClick);

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putParcelableArrayList(EVENT_PARAM_PROMOTIONS, promotionsClickList);
            ecommerceBundle.putString(Param.CONTENT_TYPE, EVENT_PARAM_INTERNAL_PROMOTIONS);
            ecommerceBundle.putString(Param.ITEM_ID, promotionId);
            logFireBaseEvent(context, Event.SELECT_CONTENT, ecommerceBundle);
        }
    }

    public void logFireBaseEcommercePurchaseEvent(Context context, String orderId, MStarCartDetails cartDetails, List<MStarProductDetails> webEngageProductDetails, double shippingAmount, String voucher, String city) {
        if (context != null) {
            ArrayList<Bundle> itemsList = new ArrayList<>();
            if (webEngageProductDetails != null && webEngageProductDetails.size() > 0) {
                for (MStarProductDetails item : webEngageProductDetails) {
                    Bundle purchase = new Bundle();
                    purchase.putString(Param.ITEM_ID, Integer.toString(item.getProductCode()));
                    purchase.putString(Param.ITEM_NAME, checkEmpty(item.getDisplayName()));
                    purchase.putString(Param.ITEM_CATEGORY, getCategoryName(item.getCategories(), getProductType(item)));
                    purchase.putString(Param.ITEM_VARIANT, getProductVariant(item.getProductVariant()));
                    purchase.putString(Param.ITEM_BRAND, checkEmpty(item.getBrandName()));
                    purchase.putDouble(Param.PRICE, item.getSellingPrice().doubleValue());
                    purchase.putString(Param.CURRENCY, CURRENCY);
                    purchase.putLong(Param.QUANTITY, (long) getCartQuantity(cartDetails, item.getProductCode()));
                    itemsList.add(purchase);
                }
            }

            String couponCode = cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? cartDetails.getAppliedCoupons() : "";
            double subTotal = cartDetails != null ? cartDetails.getSubTotalAmount().doubleValue() : 0;
            double couponDiscount = cartDetails != null ? cartDetails.getCoupon_discount_total().doubleValue() : 0;
            double defaultProductDiscount = cartDetails != null ? cartDetails.getProduct_discount_total().doubleValue() : 0;
            double nmsCashValue = cartDetails != null && cartDetails.getUsedWalletAmount() != null ? cartDetails.getUsedWalletAmount().getWalletCash().doubleValue() : 0;
            double nmsSuperCashValue = cartDetails != null && cartDetails.getUsedWalletAmount() != null ? cartDetails.getUsedWalletAmount().getNmsSuperCash().doubleValue() : 0;

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putParcelableArrayList(EVENT_PARAM_ITEMS, itemsList);
            ecommerceBundle.putString(Param.TRANSACTION_ID, orderId);
            ecommerceBundle.putString(Param.AFFILIATION, TRANSACTION_AFFILIATION);
            ecommerceBundle.putDouble(Param.VALUE, subTotal);
            ecommerceBundle.putDouble(Param.TAX, 0);
            ecommerceBundle.putDouble(Param.SHIPPING, shippingAmount);
            ecommerceBundle.putString(Param.CURRENCY, CURRENCY);
            ecommerceBundle.putString(Param.COUPON, couponCode);
            ecommerceBundle.putString(Param.LOCATION, city);
            ecommerceBundle.putDouble(EVENT_PARAM_COUPON_DISCOUNT, couponDiscount);
            ecommerceBundle.putDouble(EVENT_PARAM_DEFAULT_DISCOUNT, defaultProductDiscount);
            ecommerceBundle.putString(EVENT_PARAM_IS_FIRST_USER, PaymentHelper.isIsPrime() ? TRUE : FALSE);
            ecommerceBundle.putDouble(EVENT_PARAM_NMS_CASH, nmsCashValue);
            ecommerceBundle.putDouble(EVENT_PARAM_NMS_SUPERCASH, nmsSuperCashValue);
            ecommerceBundle.putString(EVENT_PARAM_VOUCHER, voucher);
            ecommerceBundle.putString(Param.MEDIUM, AppConstant.ANDROID);

            logFireBaseEvent(context, Event.ECOMMERCE_PURCHASE, ecommerceBundle);
        }
    }

    public void logFireBaseBeginCheckoutEvent(Context context, MStarCartDetails mStarCartDetails, List<MStarProductDetails> productDetailList) {
        if (context != null) {
            ArrayList<Bundle> itemsList = new ArrayList<>();
            if (productDetailList != null && productDetailList.size() > 0) {
                for (MStarProductDetails productDetails : productDetailList) {
                    Bundle beginCheckout = new Bundle();
                    beginCheckout.putString(Param.ITEM_ID, Integer.toString(productDetails.getProductCode()));
                    beginCheckout.putString(Param.ITEM_NAME, checkEmpty(productDetails.getDisplayName()));
                    beginCheckout.putString(Param.ITEM_CATEGORY, getCategoryName(productDetails.getCategories(), getProductType(productDetails)));
                    beginCheckout.putString(Param.ITEM_VARIANT, getProductVariant(productDetails.getProductVariant()));
                    beginCheckout.putString(Param.ITEM_BRAND, productDetails.getBrandName());
                    beginCheckout.putDouble(Param.PRICE, productDetails.getSellingPrice().doubleValue());
                    beginCheckout.putString(Param.CURRENCY, CURRENCY);
                    beginCheckout.putLong(Param.QUANTITY, (long) getCartQuantity(mStarCartDetails, productDetails.getProductCode()));
                    itemsList.add(beginCheckout);
                }
            }
            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putParcelableArrayList(EVENT_PARAM_ITEMS, itemsList);
            ecommerceBundle.putLong(Param.CHECKOUT_STEP, CHECKOUT_STEP_1);
            ecommerceBundle.putString(Param.CHECKOUT_OPTION, EVENT_PARAM_CART_PAGE);
            logFireBaseEvent(context, Event.BEGIN_CHECKOUT, ecommerceBundle);
        }
    }

    public void logFireBaseCheckoutProgressEvent(Context context, List<MStarProductDetails> cartItemList, List<MStarProductDetails> productList, long checkoutStep, String checkoutOption) {
        if (context != null) {
            ArrayList<Bundle> itemsList = new ArrayList<>();
            if (cartItemList != null && cartItemList.size() > 0) {
                for (MStarProductDetails item : cartItemList) {
                    Bundle checkoutProgress = new Bundle();
                    checkoutProgress.putString(Param.ITEM_ID, Integer.toString(item.getProductCode()));
                    checkoutProgress.putString(Param.ITEM_NAME, checkEmpty(item.getDisplayName()));
                    checkoutProgress.putString(Param.ITEM_CATEGORY, getCategoryName(item.getCategories(), getProductType(item)));
                    checkoutProgress.putString(Param.ITEM_VARIANT, getProductVariant(item.getProductVariant()));
                    checkoutProgress.putString(Param.ITEM_BRAND, checkEmpty(item.getBrandName()));
                    checkoutProgress.putDouble(Param.PRICE, item.getSellingPrice().doubleValue());
                    checkoutProgress.putString(Param.CURRENCY, CURRENCY);
                    checkoutProgress.putLong(Param.QUANTITY, (long) item.getCartQuantity());
                    itemsList.add(checkoutProgress);
                }
            }
            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putParcelableArrayList(EVENT_PARAM_ITEMS, itemsList);
            ecommerceBundle.putLong(Param.CHECKOUT_STEP, checkoutStep);
            ecommerceBundle.putString(Param.CHECKOUT_OPTION, checkoutOption);
            logFireBaseEvent(context, Event.CHECKOUT_PROGRESS, ecommerceBundle);
        }
    }

    public <T> void logFireBaseRefundsEvent(Context context, T refundDetail) {
        if (context != null && refundDetail != null) {
            ArrayList<Bundle> items = new ArrayList<>();
            Bundle ecommerceBundle = new Bundle();
            if (refundDetail instanceof MstarOrderDetailsResult) {
                MstarOrderDetailsResult orderDetailsResult = (MstarOrderDetailsResult) refundDetail;
                if (orderDetailsResult.getOrderdetail() != null && orderDetailsResult.getOrderdetail().size() > 0) {
                    for (MstarOrderDetail orderDetail : orderDetailsResult.getOrderdetail()) {
                        if (orderDetail.getDrugDetails() != null && orderDetail.getDrugDetails().size() > 0) {
                            for (MstarDrugDetail drugDetail : orderDetail.getDrugDetails()) {
                                Bundle refundedProduct = new Bundle();
                                refundedProduct.putString(Param.ITEM_ID, Integer.toString(drugDetail.getDrugCode()));
                                refundedProduct.putLong(Param.QUANTITY, (long) drugDetail.getPurchaseQuantity());
                                items.add(refundedProduct);
                            }
                        }
                    }
                }
                ecommerceBundle.putString(Param.TRANSACTION_ID, checkEmpty(orderDetailsResult.getOrderId()));
                ecommerceBundle.putDouble(Param.VALUE, orderDetailsResult.getTotalAmount().doubleValue());
                ecommerceBundle.putParcelableArrayList(EVENT_PARAM_ITEMS, items);
            } else if (refundDetail instanceof MstarOrders) {
                MstarOrders mstarOrders = (MstarOrders) refundDetail;
                if (mstarOrders.getItemsList() != null && mstarOrders.getItemsList().size() > 0) {
                    for (MstarItems mstarItems : mstarOrders.getItemsList()) {
                        Bundle refundedProduct = new Bundle();
                        refundedProduct.putString(Param.ITEM_ID, Integer.toString(mstarItems.getSku()));
                        refundedProduct.putLong(Param.QUANTITY, 0);
                        items.add(refundedProduct);
                    }
                    ecommerceBundle.putString(Param.TRANSACTION_ID, checkEmpty(mstarOrders.getOrderId()));
                    ecommerceBundle.putDouble(Param.VALUE, mstarOrders.getOrderAmount().doubleValue());
                    ecommerceBundle.putParcelableArrayList(EVENT_PARAM_ITEMS, items);
                }
            }
            logFireBaseEvent(context, Event.PURCHASE_REFUND, ecommerceBundle);
        }
    }

    private String checkEmpty(String value) {
        return !TextUtils.isEmpty(value) ? value : "";
    }

    private String getCategoryName(List<MStarCategory> categoryList, int level) {
        String categoryName = "";
        if (categoryList != null && categoryList.size() > 0) {
            MStarCategory mStarCategory = categoryList.get(0);
            if (mStarCategory != null && mStarCategory.getBreadCrumbs() != null && mStarCategory.getBreadCrumbs().size() > 0) {
                List<MStarBreadCrumb> mStarBreadCrumbs = mStarCategory.getBreadCrumbs();
                for (MStarBreadCrumb breadCrumb : mStarBreadCrumbs) {
                    if (breadCrumb.getLevel() == level) {
                        return breadCrumb.getUrlPath();
                    }
                }
            }
        }
        return categoryName;
    }

    private String getCategoryName(MstarAlgoliaResult algoliaHitResults, String level) {
        if (algoliaHitResults != null && algoliaHitResults.getCategoryTreeList() != null && algoliaHitResults.getCategoryTreeList().size() > 0) {
            for (Map.Entry<String, ArrayList<Object>> entry : algoliaHitResults.getCategoryTreeList().entrySet()) {
                String key = entry != null && !TextUtils.isEmpty(entry.getKey()) ? entry.getKey() : "";
                if (key.equalsIgnoreCase(level)) {
                    ArrayList<Object> levelList = entry != null && entry.getValue() != null && entry.getValue().size() > 0 ? entry.getValue() : new ArrayList<Object>();
                    if (levelList != null && levelList.size() > 0 && !TextUtils.isEmpty(levelList.get(0).toString().trim())) {
                        return !TextUtils.isEmpty(levelList.get(0).toString()) ? levelList.get(0).toString().replace("///", "/") : "";
                    }
                }
            }
        }
        return "";
    }

    private String getProductVariant(List<Variant> variantList) {
        String productVariant = "";
        if (variantList != null && variantList.size() > 0) {
            for (Variant variant : variantList) {
                if (variant.getVariantFacetList() != null && variant.getVariantFacetList().size() > 0) {
                    for (VariantFacet variantFacet : variant.getVariantFacetList()) {
                        if (variantFacet.isCurrentSelectedVariant()) {
                            if (!TextUtils.isEmpty(productVariant))
                                productVariant = "/" + variantFacet.getValue();
                            else
                                productVariant = variantFacet.getValue();
                        }
                    }
                }
            }
        }
        return productVariant;
    }

    private int getCartQuantity(MStarCartDetails mStarCartDetails, int productCode) {
        if (mStarCartDetails != null && mStarCartDetails.getLines() != null && mStarCartDetails.getLines().size() > 0) {
            for (MStarProductDetails details : mStarCartDetails.getLines()) {
                if (details.getProductCode() == productCode)
                    return details.getCartQuantity();
            }
        }
        return 0;
    }

    private String getLevelBasedOnProductType(MstarAlgoliaResult algoliaHitResults) {
        return !TextUtils.isEmpty(algoliaHitResults.getProductType()) ? algoliaHitResults.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_P) ? AppConstant.LEVEL_1 : algoliaHitResults.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? AppConstant.LEVEL_2 : "" : "";
    }

    private int getProductType(MStarProductDetails productDetails) {
        return !TextUtils.isEmpty(productDetails.getProductType()) ? productDetails.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_P) ? 1 : productDetails.getProductType().equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? 2 : -1 : -1;
    }
}
