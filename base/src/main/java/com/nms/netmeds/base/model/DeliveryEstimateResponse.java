package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class DeliveryEstimateResponse {
    @SerializedName("Status")
    private Boolean status;
    @SerializedName("Message")
    private String message;
    @SerializedName("MessageCode")
    private String messageCode;
    @SerializedName("Result")
    private DeliveryEstimateResult result;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public DeliveryEstimateResult getResult() {
        return result;
    }

    public void setResult(DeliveryEstimateResult result) {
        this.result = result;
    }
}
