/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class Price {
    @SerializedName("default")
    private String defaultPrice;
    @SerializedName("default_formated")
    private String defaultFormat;
    @SerializedName("default_original_formated")
    private String defaultOriginalFormat;


    public String getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(String defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public String getDefaultFormat() {
        return defaultFormat;
    }

    public void setDefaultFormat(String defaultFormat) {
        this.defaultFormat = defaultFormat;
    }

    public String getDefaultOriginalFormat() {
        return defaultOriginalFormat;
    }

    public void setDefaultOriginalFormat(String defaultOriginalFormat) {
        this.defaultOriginalFormat = defaultOriginalFormat;
    }


}

