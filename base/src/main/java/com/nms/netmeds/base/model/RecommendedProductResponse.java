/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RecommendedProductResponse extends BaseResponse {
    @SerializedName("status")
    private String status;
    @SerializedName(value = "ResponseData", alternate = {"list"})
    private ArrayList<HRVProduct> recommendedProductList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<HRVProduct> getRecommendedProductList() {
        return recommendedProductList;
    }

    public void setRecommendedProductList(ArrayList<HRVProduct> recommendedProductList) {
        this.recommendedProductList = recommendedProductList;
    }
}
