package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class WellnessDiscountList {

    @SerializedName(value = "imageName", alternate = "name")
    private String imageName;
    @SerializedName(value = "imageUrl", alternate = "image")
    private String imageUrl;
    @SerializedName(value = "id", alternate = "categoryId")
    private String id;
    @SerializedName("slideId")
    private String slideId;
    @SerializedName("url")
    private String url;
    @SerializedName("linktype")
    private String linktype;
    @SerializedName("linkpage")
    private String linkpage;
    @SerializedName("displayFrom")
    private String displayFrom;
    @SerializedName("displayTo")
    private String displayTo;
    @SerializedName("discount")
    private String discount;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlideId() {
        return slideId;
    }

    public void setSlideId(String slideId) {
        this.slideId = slideId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getLinkpage() {
        return linkpage;
    }

    public void setLinkpage(String linkpage) {
        this.linkpage = linkpage;
    }

    public String getDisplayFrom() {
        return displayFrom;
    }

    public void setDisplayFrom(String displayFrom) {
        this.displayFrom = displayFrom;
    }

    public String getDisplayTo() {
        return displayTo;
    }

    public void setDisplayTo(String displayTo) {
        this.displayTo = displayTo;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}

