package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarPrescriptionDetails {

    @SerializedName("prescription_id")
    private String prescription_id;
    @SerializedName("status")
    private String status;

    public String getPrescription_id() {
        return prescription_id;
    }

    public void setPrescription_id(String prescription_id) {
        this.prescription_id = prescription_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
