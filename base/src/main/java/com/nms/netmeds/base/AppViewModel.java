package com.nms.netmeds.base;

import android.app.Application;
import android.os.CountDownTimer;

import androidx.annotation.NonNull;

public class AppViewModel extends BaseViewModel implements Interactor<String> {

    public AppViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void onRetryClickListener() {

    }

    @Override
    public void onSyncData(String data, int transactionId) {

    }

    @Override
    public void onFailed(int transactionId, String data) {

    }

    public void onCountDownTimer(final CountDownTimerListener listener) {
        new CountDownTimer(45000, 1000) {
            public void onTick(long millisUntilFinished) {
                listener.onTick(millisUntilFinished);
            }

            public void onFinish() {
                listener.onFinish();

            }
        }.start();
    }

    public interface CountDownTimerListener {
        void onTick(long millisUntilFinished);

        void onFinish();
    }
}
