package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MStarSession {

    @SerializedName("created_time")
    private String createdTime;
    @SerializedName("valid_till")
    private String validTill;
    @SerializedName("is_valid")
    private boolean isValid;
    @SerializedName("id")
    private String id;
    @SerializedName("customer_id")
    private Long customerId;
    @SerializedName("logan_session_id")
    private String loganSessionId;

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getLoganSessionId() {
        return loganSessionId;
    }

    public void setLoganSessionId(String loganSessionId) {
        this.loganSessionId = loganSessionId;
    }
}
