package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarReferAndEarn {
    @SerializedName("Referral_code")
    private String referralCode;
    @SerializedName("ReferCount")
    private int referCount;
    @SerializedName("Referral_Status")
    private String referralStatus;
    @SerializedName("Referral_Url")
    private String referralUrl;
    @SerializedName("Referral_Text")
    private String referralText;
    @SerializedName("Referral_Mail_Content")
    private String referralMailContent;
    @SerializedName("Referral_Message_Content")
    private String referralMessageContent;
    @SerializedName("Referral_Title_Content")
    private String referralTitleContent;
    @SerializedName("Referral_Failure_Content")
    private String referralFailureContent;


    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public int getReferCount() {
        return referCount;
    }

    public void setReferCount(int referCount) {
        this.referCount = referCount;
    }

    public String getReferralStatus() {
        return referralStatus;
    }

    public void setReferralStatus(String referralStatus) {
        this.referralStatus = referralStatus;
    }

    public String getReferralUrl() {
        return referralUrl;
    }

    public void setReferralUrl(String referralUrl) {
        this.referralUrl = referralUrl;
    }

    public String getReferralText() {
        return referralText;
    }

    public void setReferralText(String referralText) {
        this.referralText = referralText;
    }

    public String getReferralMailContent() {
        return referralMailContent;
    }

    public void setReferralMailContent(String referralMailContent) {
        this.referralMailContent = referralMailContent;
    }

    public String getReferralMessageContent() {
        return referralMessageContent;
    }

    public void setReferralMessageContent(String referralMessageContent) {
        this.referralMessageContent = referralMessageContent;
    }

    public String getReferralTitleContent() {
        return referralTitleContent;
    }

    public void setReferralTitleContent(String referralTitleContent) {
        this.referralTitleContent = referralTitleContent;
    }

    public String getReferralFailureContent() {
        return referralFailureContent;
    }

    public void setReferralFailureContent(String referralFailureContent) {
        this.referralFailureContent = referralFailureContent;
    }
}
