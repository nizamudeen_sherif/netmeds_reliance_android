package com.nms.netmeds.base.utils;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;

import com.nms.netmeds.base.R;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.util.UUID;

public class ImageUtils {

    public static boolean isValidURL(String url) {
        String urlPattern = "^http(s?)://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
        return !TextUtils.isEmpty(url) && url.matches(urlPattern);
    }

    public static String checkImage(String url) {
        return String.valueOf(TextUtils.isEmpty(url) || !isValidURL(url) ? R.drawable.ic_no_image : url);
    }

    public static Uri getImageUri(ContentValues values, Application application) {
        Uri uri;
        try {
            uri = application.getContentResolver().insert(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } catch (UnsupportedOperationException e) {
            try {
                uri = application.getContentResolver().insert(android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
            } catch (UnsupportedOperationException ex) {
                return null;
            }
        }
        return uri;
    }

    public static Bitmap getScaledBitmap(Bitmap bitmap) {
        int limit = 1024;
        double originalBitmapHeight = bitmap.getHeight();
        double originalBitmapWidth = bitmap.getWidth();
        double scalingFactor;
        double scaledHeight;
        double scaledWidth;
        if (originalBitmapHeight < originalBitmapWidth) {
            if (originalBitmapHeight > limit) {
                scalingFactor = limit / originalBitmapHeight;
                scaledHeight = limit;
                scaledWidth = originalBitmapWidth * scalingFactor;
                bitmap = Bitmap.createScaledBitmap(bitmap, (int) scaledWidth, (int) scaledHeight, true);
                return bitmap;
            } else {
                return bitmap;
            }
        } else {
            if (originalBitmapWidth > limit) {
                scalingFactor = limit / originalBitmapWidth;
                scaledWidth = limit;
                scaledHeight = originalBitmapHeight * scalingFactor;

                bitmap = Bitmap.createScaledBitmap(bitmap, (int) scaledWidth, (int) scaledHeight, true);
                return bitmap;
            } else {

                return bitmap;
            }
        }
    }


    public static Bitmap convertBase64StringToBitmap(String base64, Context context) {
        FileOutputStream fos = null;
        byte[] decodedString = null;
        if (TextUtils.isEmpty(base64) || context == null) return null;
        try {
            fos = context.openFileOutput(UUID.randomUUID().toString() + "prescription.png", Context.MODE_PRIVATE);
            decodedString = Base64.decode(base64, Base64.DEFAULT);
            fos.write(decodedString);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                fos = null;
            }
        }
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }


    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


}

