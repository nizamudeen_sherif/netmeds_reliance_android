package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NmsWalletBalance {
    @SerializedName("wallet_balance")
    private double walletBalance;
    @SerializedName("cashback_balance")
    private double cashBackBalance;
    @SerializedName("cashback_useable_balance")
    private double cashBackUseAbleBalance;
    @SerializedName("useable_balance_breakup")
    private ArrayList<Object> useAbleBalanceBreakup = null;

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public double getCashBackBalance() {
        return cashBackBalance;
    }

    public void setCashBackBalance(double cashBackBalance) {
        this.cashBackBalance = cashBackBalance;
    }

    public double getCashBackUseAbleBalance() {
        return cashBackUseAbleBalance;
    }

    public void setCashBackUseAbleBalance(double cashBackUseAbleBalance) {
        this.cashBackUseAbleBalance = cashBackUseAbleBalance;
    }

    public ArrayList<Object> getUseAbleBalanceBreakup() {
        return useAbleBalanceBreakup;
    }

    public void setUseAbleBalanceBreakup(ArrayList<Object> useAbleBalanceBreakup) {
        this.useAbleBalanceBreakup = useAbleBalanceBreakup;
    }
}
