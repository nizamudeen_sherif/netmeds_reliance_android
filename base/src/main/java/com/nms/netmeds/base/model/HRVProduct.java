/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HRVProduct implements Serializable {

    @SerializedName("packSizeTypeLabel")
    private String packSizeTypeLabel;
    @SerializedName("parentCategoryName")
    private String parentCategoryName;
    @SerializedName(value = "subCategoryName", alternate = {"categoryName"})
    private String subCategoryName;
    @SerializedName("id")
    private String id;
    @SerializedName(value = "brandName", alternate = {"name"})
    private String brandName;
    @SerializedName("sku")
    private String sku;
    @SerializedName(value = "Price", alternate = {"price"})
    private String price;
    @SerializedName(value = "formulationTypes", alternate = {"formulation_types"})
    private String formulationTypes;
    @SerializedName(value = "actualPrice", alternate = {"actual_price"})
    private String actualPrice;
    @SerializedName("image")
    private String image;
    @SerializedName("smallImage")
    private String smallImage;
    @SerializedName("discountPersentage")
    private String discountPercentage;
    @SerializedName("categoryId")
    private long categoryId;
    @SerializedName("subcategoryIds")
    private List<Object> subCategoryIds = null;
    @SerializedName("discount")
    private String discount;
    @SerializedName(value = "availableStatus", alternate = {"available_status"})
    private String availableStatus;
    @SerializedName(value = "quantity", alternate = {"qty"})
    private int quantity;
    @SerializedName("minimumQuantity")
    private int minimumQuantity;
    @SerializedName(value = "maxselectqty", alternate = {"maximumSelectableQuantity"})
    private int maxSelectQty;
    @SerializedName("manufactureId")
    private String manufacturerId;
    @SerializedName(value = "manufacturerName", alternate = {"manufacturer_name"})
    private String manufacturerName;
    @SerializedName(value = "schedule", alternate = {"drugSchedule"})
    private String schedule;
    @SerializedName(value = "drugType", alternate = {"drug_type"})
    private String drugType;
    @SerializedName("genericName")
    private String genericName;
    @SerializedName("generic_dosage")
    private String genericDosage;
    @SerializedName("subcatids")
    private ArrayList<String> subCategoryId;
    @SerializedName("pcatid")
    private String parentCategoryId;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("misc")
    private String misc;

    private String type;
    private ArrayList<MstarBanner> mstarBanner;
    private List<SlideBanner> slideBannerList;
    private int rxRequired;

    public List<SlideBanner> getSlideBannerList() {
        return slideBannerList;
    }

    public void setSlideBannerList(List<SlideBanner> slideBannerList) {
        this.slideBannerList = slideBannerList;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getPackSizeTypeLabel() {
        return packSizeTypeLabel;
    }

    public void setPackSizeTypeLabel(String packSizeTypeLabel) {
        this.packSizeTypeLabel = packSizeTypeLabel;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFormulationTypes() {
        return formulationTypes;
    }

    public void setFormulationTypes(String formulationTypes) {
        this.formulationTypes = formulationTypes;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public List<Object> getSubCategoryIds() {
        return subCategoryIds;
    }

    public void setSubCategoryIds(List<Object> subCategoryIds) {
        this.subCategoryIds = subCategoryIds;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public int getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(Integer minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public int getMaxSelectQty() {
        return maxSelectQty;
    }

    public void setMaxSelectQty(Integer maxSelectQty) {
        this.maxSelectQty = maxSelectQty;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<MstarBanner> getMstarBanner() {
        return mstarBanner;
    }

    public void setMstarBanner(ArrayList<MstarBanner> mstarBanner) {
        this.mstarBanner = mstarBanner;
    }

    public String getGenericDosage() {
        return genericDosage;
    }

    public void setGenericDosage(String genericDosage) {
        this.genericDosage = genericDosage;
    }

    public ArrayList<String> getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(ArrayList<String> subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public int getRxRequired() {
        return rxRequired;
    }

    public void setRxRequired(int rxRequired) {
        this.rxRequired = rxRequired;
    }
}
