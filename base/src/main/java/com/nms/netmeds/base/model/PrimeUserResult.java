package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PrimeUserResult {
    @SerializedName("customer")
    private PrimeCustomer primeCustomer;

    public PrimeCustomer getPrimeCustomer() {
        return primeCustomer;
    }

    public void setPrimeCustomer(PrimeCustomer primeCustomer) {
        this.primeCustomer = primeCustomer;
    }
}
