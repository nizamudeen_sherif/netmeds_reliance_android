package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MStarCartDetails {
    @SerializedName("billing_address_id")
    private int billingAddressId = -1;
    @SerializedName("created_time")
    private String createdTime;
    @SerializedName("applied_general_vouchers")
    private String appliedGeneralVouchers;
    @SerializedName("general_voucher_amount")
    private BigDecimal generalVoucherAmount = BigDecimal.ZERO;
    @SerializedName("applied_iho_vouchers")
    private String appliedIhoVouchers;
    @SerializedName("iho_voucher_amount")
    private BigDecimal ihoVoucherAmount = BigDecimal.ZERO;
    @SerializedName("used_voucher_amount")
    private BigDecimal usedVoucherAmount = BigDecimal.ZERO;
    @SerializedName("prepaid_amount")
    private BigDecimal prepaidAmount = BigDecimal.ZERO;
    @SerializedName("applied_coupons")
    private String appliedCoupons;
    @SerializedName("supercash_amount")
    private BigDecimal supercashAmount = BigDecimal.ZERO;
    @SerializedName("nmscash_amount")
    private BigDecimal nmscashAmount = BigDecimal.ZERO;
    @SerializedName("cod_amount")
    private BigDecimal codAmount = BigDecimal.ZERO;
    @SerializedName("use_wallet_balance")
    private boolean isUseWalletBalance;
    @SerializedName("use_nms_cash")
    private boolean isUseNMSCash;
    @SerializedName("use_super_cash")
    private boolean isUseSuperCash;
    @SerializedName("shipping_charges")
    private BigDecimal shippingCharge = BigDecimal.ZERO;
    @SerializedName("wallet_amount")
    private BigDecimal wallet_amount = BigDecimal.ZERO;
    @SerializedName("net_payable")
    private BigDecimal netPayableAmount = BigDecimal.ZERO;
    @SerializedName("sub_total")
    private BigDecimal subTotalAmount = BigDecimal.ZERO;
    @SerializedName("total_savings")
    private BigDecimal totalSavings = BigDecimal.ZERO;
    @SerializedName("shipping_address_id")
    private int shippingAddressId = -1;
    @SerializedName("checkout_channel")
    private Object checkoutChannel;
    @SerializedName("payment_type")
    private Object paymentType;
    @SerializedName("balance_info")
    private Object balanceInfo;
    @SerializedName("cart_status")
    private String cartStatus;
    @SerializedName("id")
    private int id;
    @SerializedName("customer_id")
    private Long customerId;
    @SerializedName("created_channel")
    private String createdChannel;
    @SerializedName("lines")
    private List<MStarProductDetails> lines = null;
    @SerializedName("order_id")
    private String order_id;
    @SerializedName("prescriptions")
    private ArrayList<String> prescriptions;
    @SerializedName("method2")
    private boolean ismethod2;
    @SerializedName("payment_aggregator")
    private String paymentAggregator;
    @SerializedName("product_discount_total")
    private BigDecimal product_discount_total;
    @SerializedName("coupon_discount_total")
    private BigDecimal coupon_discount_total;
    @SerializedName("shipping_discount")
    private BigDecimal shipping_discount;
    @SerializedName("used_wallet_amounts")
    private MStarUsedWalletAmountModel usedWalletAmount;
    @SerializedName("is_edited")
    private boolean isEdited;
    @SerializedName("doctor_consultation_needed")
    private boolean isDoctorConsultationNeeded;
    @SerializedName("shipping_charges_original")
    private BigDecimal shippingChargesOriginal = BigDecimal.ZERO;
    @SerializedName("shipping_charges_final")
    private BigDecimal shippingChargesFinal = BigDecimal.ZERO;

    private String pageName;

    public int getBillingAddressId() {
        return billingAddressId;
    }

    public void setBillingAddressId(int billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getAppliedGeneralVouchers() {
        return appliedGeneralVouchers;
    }

    public void setAppliedGeneralVouchers(String appliedGeneralVouchers) {
        this.appliedGeneralVouchers = appliedGeneralVouchers;
    }

    public BigDecimal getPrepaidAmount() {
        return prepaidAmount;
    }

    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    public BigDecimal getSupercashAmount() {
        return supercashAmount;
    }

    public void setSupercashAmount(BigDecimal supercashAmount) {
        this.supercashAmount = supercashAmount;
    }

    public int getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(int shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public Object getCheckoutChannel() {
        return checkoutChannel;
    }

    public void setCheckoutChannel(Object checkoutChannel) {
        this.checkoutChannel = checkoutChannel;
    }

    public BigDecimal getGeneralVoucherAmount() {
        return generalVoucherAmount;
    }

    public void setGeneralVoucherAmount(BigDecimal generalVoucherAmount) {
        this.generalVoucherAmount = generalVoucherAmount;
    }

    public BigDecimal getCodAmount() {
        return codAmount;
    }

    public void setCodAmount(BigDecimal codAmount) {
        this.codAmount = codAmount;
    }

    public BigDecimal getIhoVoucherAmount() {
        return ihoVoucherAmount;
    }

    public void setIhoVoucherAmount(BigDecimal ihoVoucherAmount) {
        this.ihoVoucherAmount = ihoVoucherAmount;
    }

    public String getAppliedCoupons() {
        return appliedCoupons;
    }

    public void setAppliedCoupons(String appliedCoupons) {
        this.appliedCoupons = appliedCoupons;
    }

    public Object getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Object paymentType) {
        this.paymentType = paymentType;
    }

    public Object getBalanceInfo() {
        return balanceInfo;
    }

    public void setBalanceInfo(Object balanceInfo) {
        this.balanceInfo = balanceInfo;
    }

    public String getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(String cartStatus) {
        this.cartStatus = cartStatus;
    }

    public String getAppliedIhoVouchers() {
        return appliedIhoVouchers;
    }

    public void setAppliedIhoVouchers(String appliedIhoVouchers) {
        this.appliedIhoVouchers = appliedIhoVouchers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getNmscashAmount() {
        return nmscashAmount;
    }

    public void setNmscashAmount(BigDecimal nmscashAmount) {
        this.nmscashAmount = nmscashAmount;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCreatedChannel() {
        return createdChannel;
    }

    public void setCreatedChannel(String createdChannel) {
        this.createdChannel = createdChannel;
    }

    public List<MStarProductDetails> getLines() {
        return lines;
    }

    public void setLines(List<MStarProductDetails> lines) {
        this.lines = lines;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public ArrayList<String> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(ArrayList<String> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public boolean isIsmethod2() {
        return ismethod2;
    }

    public void setIsmethod2(boolean ismethod2) {
        this.ismethod2 = ismethod2;
    }

    public BigDecimal getWallet_amount() {
        return wallet_amount;
    }

    public void setWallet_amount(BigDecimal wallet_amount) {
        this.wallet_amount = wallet_amount;
    }

    public BigDecimal getNetPayableAmount() {
        return netPayableAmount;
    }

    public void setNetPayableAmount(BigDecimal netPayableAmount) {
        this.netPayableAmount = netPayableAmount;
    }

    public BigDecimal getUsedVoucherAmount() {
        return usedVoucherAmount;
    }

    public void setUsedVoucherAmount(BigDecimal usedVoucherAmount) {
        this.usedVoucherAmount = usedVoucherAmount;
    }

    public BigDecimal getTotalSavings() {
        return totalSavings;
    }

    public void setTotalSavings(BigDecimal totalSavings) {
        this.totalSavings = totalSavings;
    }

    public BigDecimal getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public BigDecimal getSubTotalAmount() {
        return subTotalAmount;
    }

    public void setSubTotalAmount(BigDecimal subTotalAmount) {
        this.subTotalAmount = subTotalAmount;
    }

    public String getPaymentAggregator() {
        return paymentAggregator;
    }

    public void setPaymentAggregator(String paymentAggregator) {
        this.paymentAggregator = paymentAggregator;
    }

    public MStarUsedWalletAmountModel getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(MStarUsedWalletAmountModel usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public BigDecimal getProduct_discount_total() {
        return product_discount_total;
    }

    public void setProduct_discount_total(BigDecimal product_discount_total) {
        this.product_discount_total = product_discount_total;
    }

    public BigDecimal getCoupon_discount_total() {
        return coupon_discount_total;
    }

    public void setCoupon_discount_total(BigDecimal coupon_discount_total) {
        this.coupon_discount_total = coupon_discount_total;
    }

    public BigDecimal getShipping_discount() {
        return shipping_discount;
    }

    public void setShipping_discount(BigDecimal shipping_discount) {
        this.shipping_discount = shipping_discount;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    public boolean isDoctorConsultationNeeded() {
        return isDoctorConsultationNeeded;
    }

    public void setDoctorConsultationNeeded(boolean doctorConsultationNeeded) {
        isDoctorConsultationNeeded = doctorConsultationNeeded;
    }

    public boolean isUseWalletBalance() {
        return isUseWalletBalance;
    }

    public void setUseWalletBalance(boolean useWalletBalance) {
        isUseWalletBalance = useWalletBalance;
    }

    public boolean isUseNMSCash() {
        return isUseNMSCash;
    }

    public void setUseNMSCash(boolean useNMSCash) {
        isUseNMSCash = useNMSCash;
    }

    public boolean isUseSuperCash() {
        return isUseSuperCash;
    }

    public void setUseSuperCash(boolean useSuperCash) {
        isUseSuperCash = useSuperCash;
    }

    public BigDecimal getShippingChargesOriginal() {
        return shippingChargesOriginal;
    }

    public void setShippingChargesOriginal(BigDecimal shippingChargesOriginal) {
        this.shippingChargesOriginal = shippingChargesOriginal;
    }

    public BigDecimal getShippingChargesFinal() {
        return shippingChargesFinal;
    }

    public void setShippingChargesFinal(BigDecimal shippingChargesFinal) {
        this.shippingChargesFinal = shippingChargesFinal;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }


}
