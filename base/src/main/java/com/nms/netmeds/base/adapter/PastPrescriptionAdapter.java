package com.nms.netmeds.base.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.retrofit.APIPathConstant;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;

import java.util.List;

public class PastPrescriptionAdapter extends RecyclerView.Adapter<PastPrescriptionAdapter.PastPrescriptionHolder> {

    private List<MStarUploadPrescription> mStarUploadPrescriptionList;
    private final Context mContext;
    private final UploadedPrescriptionListener mPastPrescriptionListener;
    private final int checkboxVisibility;

    public PastPrescriptionAdapter(List<MStarUploadPrescription> mStarUploadPrescriptionList, Context context, UploadedPrescriptionListener pastPrescriptionListener, int checkboxVisibility) {
        this.mStarUploadPrescriptionList = mStarUploadPrescriptionList;
        this.mContext = context;
        this.mPastPrescriptionListener = pastPrescriptionListener;
        this.checkboxVisibility = checkboxVisibility;
    }

    @NonNull
    @Override
    public PastPrescriptionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_past_prescription, viewGroup, false);
        return new PastPrescriptionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PastPrescriptionHolder holder, int position) {
        if(mStarUploadPrescriptionList!=null){
        final MStarUploadPrescription prescriptionDetail = mStarUploadPrescriptionList.get(position);
        String url = "";
        if (prescriptionDetail.isDigitalized()) {
            url = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + prescriptionDetail.getDigitalizedPrescriptionId();
        } else {
            url = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + prescriptionDetail.getUploadedPrescriptionId();

        }

        holder.prescriptionImage.setVisibility(checkboxVisibility);
        holder.viewOrderPresImage.setVisibility(checkboxVisibility == View.GONE ? View.VISIBLE : View.GONE);
        holder.prescriptionCheckBox.setVisibility(checkboxVisibility);
        if(!MstarPrescriptionHelper.getInstance().getSelectedPastRxIds().isEmpty() && MstarPrescriptionHelper.getInstance().getSelectedPastRxIds().size()>0){
        for (String id : MstarPrescriptionHelper.getInstance().getSelectedPastRxIds())
            if (id.equalsIgnoreCase(prescriptionDetail.getPastPrescriptionId())) {
                holder.prescriptionCheckBox.setChecked(true);
                holder.prescriptionCheckBox.setEnabled(false);
            }
        }
        //noinspection deprecation
        RequestOptions options = new RequestOptions().dontAnimate().dontTransform();
        GlideUrl glideUrl = CommonUtils.getGlideUrl(url, BasePreference.getInstance(mContext).getMstarBasicHeaderMap());
        Glide.with(mContext.getApplicationContext())
                .asBitmap()
                .apply(options)
                .load(glideUrl)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
                        ImageView imageView = checkboxVisibility == View.GONE ? holder.viewOrderPresImage : holder.prescriptionImage;
                        imageView.setImageBitmap(resource);
                        prescriptionDetail.setBitmapImage(resource);
                    }
                });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prescriptionDetail.isDigitalized()) {
                    mPastPrescriptionListener.launchPreviewPrescription(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + prescriptionDetail.getDigitalizedPrescriptionId());
                } else {
                    mPastPrescriptionListener.launchPreviewPrescription(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + prescriptionDetail.getUploadedPrescriptionId());
                }

            }
        });
        holder.prescriptionCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(mContext).getConfiguration(), ConfigurationResponse.class);
                int prescriptionLimit = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null ? response.getResult().getConfigDetails().getUploadPrescriptionLimit() : 0;
                if (isChecked) {
                    if (mPastPrescriptionListener.getPrescriptionCount() < prescriptionLimit)
                        if (prescriptionDetail.isDigitalized()) {
                            mPastPrescriptionListener.prescriptionSelected(prescriptionDetail.getDigitalizedPrescriptionId());
                        } else {
                            mPastPrescriptionListener.prescriptionSelected(prescriptionDetail.getUploadedPrescriptionId());
                        }

                    else {
                        mPastPrescriptionListener.setAlert();
                        holder.prescriptionCheckBox.setChecked(false);
                    }
                } else {
                    if (prescriptionDetail.isDigitalized()) {
                        mPastPrescriptionListener.removePrescription(prescriptionDetail.getDigitalizedPrescriptionId());
                    } else {
                        mPastPrescriptionListener.removePrescription(prescriptionDetail.getUploadedPrescriptionId());
                    }
                }
            }
        });
        }
    }

    @Override
    public int getItemCount() {
        return mStarUploadPrescriptionList.size();
    }


    public interface UploadedPrescriptionListener {

        void prescriptionSelected(String Id);

        void removePrescription(String id);

        void launchPreviewPrescription(String url);

        void setAlert();

        int getPrescriptionCount();
    }

    class PastPrescriptionHolder extends RecyclerView.ViewHolder {

        private final ImageView prescriptionImage;
        private final ImageView viewOrderPresImage;
        private final CheckBox prescriptionCheckBox;
        private final FrameLayout parentLayout;

        PastPrescriptionHolder(@NonNull View itemView) {
            super(itemView);
            prescriptionImage = itemView.findViewById(R.id.prescriptionImage);
            viewOrderPresImage = itemView.findViewById(R.id.view_order_prescription);
            prescriptionCheckBox = itemView.findViewById(R.id.prescriptionCheckBox);
            parentLayout = itemView.findViewById(R.id.parentLayout);
        }
    }
}
