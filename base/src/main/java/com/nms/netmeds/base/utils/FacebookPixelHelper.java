/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.nms.netmeds.base.utils;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;

import java.util.List;

/**
 * Singleton  class
 */
public class FacebookPixelHelper {
    public static final String EVENT_PARAM_CHECKOUT_PRESCRIPTION = "Prescription";
    public static final String EVENT_PARAM_CHECKOUT_ADDRESS = "Shipping & Delivery";
    public static final String EVENT_PARAM_CHECKOUT_REVIEW = "Review page";
    private static final String PRESCRIPTIONS = "Prescriptions";
    private static final String NON_PRESCRIPTIONS = "Non-Prescriptions";
    private static final String OTC = "OTC";
    private static final String NEW = "New";
    private static final String EXIST = "Exist";
    private static final String M1_ORDER = "M1";
    private static final String M2_ORDER = "M2";
    private static final String MIXED = "Mixed";
    private static final String RX = "Rx";
    //Event Name
    private static final String EVENT_NAME_ADDED_TO_CART = "Add To Cart";
    private static final String EVENT_NAME_PURCHASE = "Purchase";
    //Event Params
    private static final String EVENT_PARAM_PRODUCT_TYPE = "Product Type";
    private static final String EVENT_PARAM_PRODUCT_QUANTITY = "Quantity";
    private static final String EVENT_PARAM_PRODUCT_CATEGORY_NAME = "Category Name";
    private static final String EVENT_PARAM_PRODUCT_SUBCATEGORY_NAME = "sub category name";
    private static final String EVENT_PARAM_DISCOUNT = "Discount";
    private static final String EVENT_PARAM_PRODUCT_PRICE = "Price";
    private static final String EVENT_PARAM_PRODUCT_NAME = "Product name";
    private static final String EVENT_PARAM_TYPE_OF_ORDER = "Type of Order";
    private static final String EVENT_PARAM_ORDER_ID = "Order ID";
    private static final String EVENT_PARAM_METHOD_OF_ORDER = "Method of order";
    private static final String EVENT_PARAM_PAYMENT_METHOD = "Payment method";
    private static final String EVENT_PARAM_TOTAL_VALUE = "Total Value";
    private static final String EVENT_PARAM_PAGE_TYPE = "Page Type";
    private static final String EVENT_PARAM_CHECKOUT_INCOMPLETE_STAGE = "Checkout incomplete stage";
    private static final String EVENT_PARAM_CUSTOMER_TYPE = "Customer Type";
    private static final String EVENT_PARAM_APP_VERSION = "App Version";
    private static FacebookPixelHelper facebookPixelHelper;
    private Bundle params;

    /**
     * Create a getInstance() method to return the instance of
     * FacebookPixelHelper.This function ensures that
     * the variable is instantiated only once and the same
     * instance is used entire application
     *
     * @return FacebookPixelHelper instance
     */

    public static FacebookPixelHelper getInstance() {
        if (facebookPixelHelper == null) {
            facebookPixelHelper = new FacebookPixelHelper();
        }
        return facebookPixelHelper;
    }

    /**
     * Method to add string value to the bundle
     *
     * @param name  event param name
     * @param value sending string value
     */
    private void putString(String name, String value) {
        params.putString(name, value);
    }

    /**
     * Method to add integer value to the bundle
     *
     * @param name  event param name
     * @param value sending integer value
     */
    private void putInt(String name, Integer value) {
        params.putInt(name, value);
    }

    /**
     * Method to add double value to the bundle
     *
     * @param name  event param name
     * @param value sending double value
     */
    private void putDouble(String name, double value) {
        params.putDouble(name, value);
    }

    /**
     * This function return AppEventsLogger instance every call.
     * AppEventsLogger Instance used to log events
     *
     * @return instance
     */
    private AppEventsLogger getLoggerInstance(Context context) {
        return AppEventsLogger.newLogger(context);
    }

    /**
     * Log an addedToCart event while added item to shopping cart
     *
     * @param context  current state of the application/object
     * @param product  product object
     * @param quantity selected quantity
     */
    public void logAddedToCartEvent(Context context, MStarProductDetails product, int quantity) {
        params = new Bundle();
        double price = 0.0;
        double actualPrice = 0.0;
        price = product.getMrp().doubleValue();
        actualPrice = product.getSellingPrice().doubleValue();

        putString(EVENT_PARAM_PRODUCT_CATEGORY_NAME, !TextUtils.isEmpty(getCategoryName(product.getCategories(), 1)) ? getCategoryName(product.getCategories(), 1) : getBaseCategory(checkEmpty(product.getProductType())));
        putString(EVENT_PARAM_PRODUCT_SUBCATEGORY_NAME, checkEmpty(getCategoryName(product.getCategories(), 2)));
        putString(EVENT_PARAM_PRODUCT_PRICE, Double.toString(actualPrice));
        putString(EVENT_PARAM_PRODUCT_TYPE, !TextUtils.isEmpty(checkEmpty(product.getProductType())) ? product.getProductType().equalsIgnoreCase("P") ? PRESCRIPTIONS : OTC : "");
        putString(EVENT_PARAM_PRODUCT_NAME, checkEmpty(product.getDisplayName()));
        putInt(EVENT_PARAM_PRODUCT_QUANTITY, quantity);
        putString(EVENT_PARAM_DISCOUNT, price != actualPrice ? Double.toString(price - actualPrice) : "");
        putString(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        logEvent(context, EVENT_NAME_ADDED_TO_CART, params);
    }

    private String getBaseCategory(String drugType) {
        return !TextUtils.isEmpty(drugType) ? drugType.equalsIgnoreCase("P") ? PRESCRIPTIONS : NON_PRESCRIPTIONS : "";
    }

    /**
     * Purchase event triggers while completing successful payment
     *
     * @param context           current state of the application/object
     * @param orderId           order id
     * @param cartDetails       total information object
     * @param appFirstOrder     this param used to check customer type like new or exist
     * @param paymentMethod     type of payment (Card, wallet, COD, Net banking, etc.,)
     * @param isIncompleteOrder this param used to check order type like m1 or m2
     * @param cartLineItems     purchased product item list
     */
    public void logPurchaseEvent(Context context, String orderId, MStarCartDetails cartDetails, String appFirstOrder, String paymentMethod, boolean isIncompleteOrder, List<MStarProductDetails> cartLineItems) {
        params = new Bundle();
        String customerType = !TextUtils.isEmpty(appFirstOrder) ? appFirstOrder.equalsIgnoreCase(AppConstant.YES) ? NEW : EXIST : "";
        double grandTotal = cartDetails != null ? cartDetails.getNetPayableAmount().doubleValue() : 0;
        double discount = cartDetails != null ? cartDetails.getTotalSavings().doubleValue() : 0;

        putString(EVENT_PARAM_ORDER_ID, orderId);
        putDouble(EVENT_PARAM_TOTAL_VALUE, grandTotal);
        putDouble(EVENT_PARAM_DISCOUNT, discount);

        putString(EVENT_PARAM_TYPE_OF_ORDER, getTypeOfOrder(cartLineItems));
        putString(EVENT_PARAM_METHOD_OF_ORDER, isIncompleteOrder ? M2_ORDER : M1_ORDER);
        putString(EVENT_PARAM_CUSTOMER_TYPE, customerType);
        putString(EVENT_PARAM_PAYMENT_METHOD, paymentMethod);
        putString(AppEventsConstants.EVENT_PARAM_CONTENT, new Gson().toJson(cartLineItems));
        putString(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        logEvent(context, EVENT_NAME_PURCHASE, params);
    }

    /**
     * Log completed registration event while registration done through facebook,google or normal registration through app
     *
     * @param context current state of the application/object
     */
    public void logRegistrationEvent(Context context) {
        params = new Bundle();
        putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, "");
        putString(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        logEvent(context, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
    }

    /**
     * User visited screens will trigger
     *
     * @param context    current state of the application/object
     * @param screenName visited screen name
     */
    public void logScreenEvent(Context context, String screenName) {
        if (context != null) {
            params = new Bundle();
            putString(EVENT_PARAM_PAGE_TYPE, screenName);
            putString(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
            logEvent(context, AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, params);
        }
    }

    /**
     * User checkout process will trigger
     *
     * @param context      current state of the application/object
     * @param screenName   visited screen name
     * @param customerType type of customer old or new
     */
    public void logCheckoutEvent(Context context, String screenName, String customerType) {
        params = new Bundle();
        putString(EVENT_PARAM_CHECKOUT_INCOMPLETE_STAGE, screenName);
        putString(EVENT_PARAM_CUSTOMER_TYPE, customerType);
        putString(EVENT_PARAM_APP_VERSION, CommonUtils.getAppVersion(context));
        logEvent(context, AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, params);
    }

    private void logEvent(Context context, String eventName, Bundle params) {
        if (CommonUtils.isBuildVariantProdRelease())
            getLoggerInstance(context).logEvent(eventName, params);
    }

    private String checkEmpty(String value) {
        if (!TextUtils.isEmpty(value)) {
            return value;
        } else return "";
    }

    private String getTypeOfOrder(List<MStarProductDetails> cartLineItems) {
        boolean non_prescription = false;
        boolean prescription = false;
        if (cartLineItems != null && !cartLineItems.isEmpty()) {
            for (MStarProductDetails itemResult : cartLineItems) {
                if (!TextUtils.isEmpty(itemResult.getProductType())) {
                    switch (itemResult.getProductType()) {
                        case "P":
                            prescription = true;
                            break;
                        case "O":
                            non_prescription = true;
                            break;
                    }
                }
            }
            return prescription && non_prescription ? MIXED : prescription ? RX : OTC;
        } else {
            return "";
        }
    }

    private String getCategoryName(List<MStarCategory> categoryList, int level) {
        String categoryName = "";
        if (categoryList != null && categoryList.size() > 0) {
            MStarCategory mStarCategory = categoryList.get(0);
            if (mStarCategory != null && mStarCategory.getBreadCrumbs() != null && mStarCategory.getBreadCrumbs().size() > 0) {
                List<MStarBreadCrumb> mStarBreadCrumbs = mStarCategory.getBreadCrumbs();
                for (MStarBreadCrumb breadCrumb : mStarBreadCrumbs) {
                    if (breadCrumb.getLevel() == level) {
                        return breadCrumb.getName();
                    }
                }
            }
        }
        return categoryName;
    }
}
