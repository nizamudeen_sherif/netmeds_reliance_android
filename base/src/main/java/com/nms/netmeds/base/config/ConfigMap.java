package com.nms.netmeds.base.config;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigMap {

    private static ConfigMap mConfig;
    private Properties mProperty;

    public static ConfigMap getInstance() {
        if (mConfig == null)
            mConfig = new ConfigMap();
        return mConfig;
    }

    public void initialize(Context context) {
        AssetManager assetManager = context.getResources().getAssets();
        InputStream inputStream = null;
        mProperty = new Properties();
        try {
            inputStream = assetManager.open("appconfig");
            mProperty.load(inputStream);
        } catch (IOException var14) {
            var14.printStackTrace();
        } finally {
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public String getProperty(String key) {
        return mProperty.getProperty(key);
    }
}
