package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PinCodeInDates {
    @SerializedName("on_or_after")
    private String onOrAfter;
    @SerializedName("on_or_before")
    private String onOrBefore;

    public String getOnOrAfter() {
        return onOrAfter;
    }

    public void setOnOrAfter(String onOrAfter) {
        this.onOrAfter = onOrAfter;
    }

    public String getOnOrBefore() {
        return onOrBefore;
    }

    public void setOnOrBefore(String onOrBefore) {
        this.onOrBefore = onOrBefore;
    }
}
