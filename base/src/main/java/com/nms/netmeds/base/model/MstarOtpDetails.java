package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarOtpDetails {

    @SerializedName("valid_till_time")
    private String validTillTime;
    @SerializedName("valid_duration_in_secs")
    private Integer validDurationInSecs;
    @SerializedName("random_key")
    private String randomKey;

    public String getValidTillTime() {
        return validTillTime;
    }

    public void setValidTillTime(String validTillTime) {
        this.validTillTime = validTillTime;
    }

    public Integer getValidDurationInSecs() {
        return validDurationInSecs;
    }

    public void setValidDurationInSecs(Integer validDurationInSecs) {
        this.validDurationInSecs = validDurationInSecs;
    }

    public String getRandomKey() {
        return randomKey;
    }

    public void setRandomKey(String randomKey) {
        this.randomKey = randomKey;
    }
}
