package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class OnBoardingResult {
    @SerializedName("onBoardingImage")
    private String onBoardingImage;
    @SerializedName("onBoardingTitle")
    private String onBoardingTitle;
    @SerializedName("onBoardingDescription")
    private String onBoardingDescription;

    public String getOnBoardingImage() {
        return onBoardingImage;
    }

    public void setOnBoardingImage(String onBoardingImage) {
        this.onBoardingImage = onBoardingImage;
    }

    public String getOnBoardingTitle() {
        return onBoardingTitle;
    }

    public void setOnBoardingTitle(String onBoardingTitle) {
        this.onBoardingTitle = onBoardingTitle;
    }

    public String getOnBoardingDescription() {
        return onBoardingDescription;
    }

    public void setOnBoardingDescription(String onBoardingDescription) {
        this.onBoardingDescription = onBoardingDescription;
    }

}
