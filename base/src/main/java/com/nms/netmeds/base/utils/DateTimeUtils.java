package com.nms.netmeds.base.utils;

import android.annotation.SuppressLint;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtils {
    public static final String yyyyMMdd = "yyyy-MM-dd";
    public static final String EMMMdd = "E, MMM dd";
    public static final String yyyyMMddThhmmss = "yyyy-MM-dd'T'hh:mm:ss";
    public static final String MMMddyyyyhhmma = "MMM dd, yyyy hh:mm a";
    public static final String ddMMMyyyy = "dd MMM yyyy";
    public static final String MMddyyyy = "MM/dd/yyyy";
    public static final String ddMMMhhmma = "dd MMM, hh:mm a";
    public static final String ddMMM = "dd-MMM";
    public static final String MMMyyyy = "MMM yyyy";
    public static final String HHmma = "HH:mm a";
    public static final String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
    public static final String yyyyMMddTHHmmssSSS = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String yyyyMMddTHHmmss = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String dd_MMM = "dd MMM";
    public static final String MMMMddyyyy = "MMMM, dd  yyyy";

    private static DateTimeUtils dateTimeUtils;

    public static DateTimeUtils getInstance() {
        if (dateTimeUtils == null)
            dateTimeUtils = new DateTimeUtils();
        return dateTimeUtils;
    }

    public String dateTime(String date, String inputDateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputDateFormat);
        String formatDate = "";
        try {
            Date d = inputFormat.parse(date);
            PrettyTime prettyTime = new PrettyTime();
            formatDate = prettyTime.format(d);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return formatDate;
    }

    public String stringDate(String date, String inputDateFormat, String outputDateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputDateFormat);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(outputDateFormat);
        String formatDate = "";
        try {
            Date d = inputFormat.parse(date);
            formatDate = outputFormat.format(d);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return formatDate;
    }

    public String currentDate(String dateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        String formatDate = "";
        Date d = new Date();
        formatDate = simpleDateFormat.format(d);
        return formatDate;
    }

    public boolean isCouponExpired(String date, String inputDateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputDateFormat);
        boolean isCouponExpired = true;
        try {
            Date startDate = inputFormat.parse(currentDate(DateTimeUtils.yyyyMMddTHHmmss));
            Date endDate = inputFormat.parse(date);
            if (startDate.compareTo(endDate) < 0 || startDate.compareTo(endDate) == 0)
                isCouponExpired = false;
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return isCouponExpired;
    }

    public Calendar getSeparatedCalendar(String dob) {
        Calendar calendar = Calendar.getInstance();
        String[] dobArray;
        if (dob.contains("-")) {
            dobArray = dob.split("-");
        } else {
            dobArray = dob.split("/");
        }
        calendar.set(Calendar.YEAR, Integer.valueOf(dobArray[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(dobArray[1]) - 1);
        if (dobArray[2].contains(" ")) {
            calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dobArray[2].split(" ")[0]));
        } else {
            calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dobArray[2]));
        }
        return calendar;
    }

    public String calculateAgeFromDOB(int year, int month, int dayOfMonth) {
        Calendar now = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        dob.set(year, month, dayOfMonth);
        int year1 = now.get(Calendar.YEAR);
        int year2 = dob.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dob.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dob.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }
        if (age == 0) {
            return "";
        } else {
            return String.valueOf(age);
        }

    }

    public String currentTime(Date date, String inputDateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputDateFormat);
        return inputFormat.format(date);
    }

    public String utcToLocal(String dateFormatInPut, String dateFomratOutPut, String datesToConvert) {
        String localDateTime = "";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat utcSimpleDateFormat = new SimpleDateFormat(dateFormatInPut);
        utcSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        @SuppressLint("SimpleDateFormat") SimpleDateFormat localTimeZoneFormat = new SimpleDateFormat(dateFomratOutPut);
        localTimeZoneFormat.setTimeZone(TimeZone.getDefault());

        try {
            Date parseDate = utcSimpleDateFormat.parse(datesToConvert);
            return localTimeZoneFormat.format(parseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return localDateTime;
    }

    public String convertMillisecondToDateTimeFormat(long milliseconds, String outputDateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(outputDateFormat);
        String formatDate = "";
        Date date = new Date(milliseconds);
        formatDate = outputFormat.format(date);
        return formatDate;
    }

    public long convertDateToMilliSecond(String date, String dateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(dateFormat);
        String formatDate = "";
        Date outputDate = new Date();
        try {
            outputDate = inputFormat.parse(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return outputDate.getTime();
    }

    public String CalenderToDateString(Calendar calendar, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = calendar.getTime();
        String output = "";
        output = dateFormat.format(date);
        return output;

    }
}
