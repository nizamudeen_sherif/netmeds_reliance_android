package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class MstarOrderDetailsResult {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("isPrimeMembershipOrder")
    private boolean isPrimeMembershipOrder;
    @SerializedName("isPrimeCustomerOrder")
    private boolean isPrimeCustomerOrder;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("orderDate")
    private String orderDate;
    @SerializedName("orderStatusCode")
    private String orderStatusCode;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("cancelStatus")
    private boolean cancelStatus;
    @SerializedName("expectedDeliveryDateString")
    private String expectedDeliveryDateString;
    @SerializedName("promisedDeliveryDate")
    private String promisedDeliveryDate;
    @SerializedName("orderBillingInfo")
    private MstarOrderBillingAddress orderBillingInfo;
    @SerializedName("orderShippingInfo")
    private MstarOrderShippingAddress orderShippingInfo;
    @SerializedName("orderdetail")
    private List<MstarOrderDetail> orderdetail = null;
    @SerializedName("canceledItemsDetails")
    private List<MstarDrugDetail> canceledItemsDetails = null;
    @SerializedName("productAmount")
    private BigDecimal productAmount = BigDecimal.ZERO;
    @SerializedName("couponDiscount")
    private BigDecimal couponDiscount = BigDecimal.ZERO;
    @SerializedName("shippingAmount")
    private BigDecimal shippingAmount = BigDecimal.ZERO;
    @SerializedName("codCharge")
    private BigDecimal codCharge = BigDecimal.ZERO;
    @SerializedName("totalAmount")
    private BigDecimal totalAmount = BigDecimal.ZERO;
    @SerializedName("voucherAmount")
    private BigDecimal voucherAmount = BigDecimal.ZERO;
    @SerializedName("usedWalletAmount")
    private BigDecimal usedWalletAmount = BigDecimal.ZERO;
    @SerializedName("transactionAmount")
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    @SerializedName("discountAmount")
    private BigDecimal discountAmount = BigDecimal.ZERO;
    @SerializedName("savingsAmount")
    private BigDecimal savingsAmount = BigDecimal.ZERO;
    @SerializedName("rxId")
    private List<String> rxId = null;
    @SerializedName("expressStatus")
    private String expressStatus;
    @SerializedName("isSubscriptionSubscribed")
    private boolean isSubscriptionSubscribed;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public boolean isPrimeMembershipOrder() {
        return isPrimeMembershipOrder;
    }

    public void setPrimeMembershipOrder(boolean primeMembershipOrder) {
        isPrimeMembershipOrder = primeMembershipOrder;
    }

    public boolean isPrimeCustomerOrder() {
        return isPrimeCustomerOrder;
    }

    public void setPrimeCustomerOrder(boolean primeCustomerOrder) {
        isPrimeCustomerOrder = primeCustomerOrder;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatusCode() {
        return orderStatusCode;
    }

    public void setOrderStatusCode(String orderStatusCode) {
        this.orderStatusCode = orderStatusCode;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean isCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(boolean cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getExpectedDeliveryDateString() {
        return expectedDeliveryDateString;
    }

    public void setExpectedDeliveryDateString(String expectedDeliveryDateString) {
        this.expectedDeliveryDateString = expectedDeliveryDateString;
    }

    public String getPromisedDeliveryDate() {
        return promisedDeliveryDate;
    }

    public void setPromisedDeliveryDate(String promisedDeliveryDate) {
        this.promisedDeliveryDate = promisedDeliveryDate;
    }

    public MstarOrderBillingAddress getOrderBillingInfo() {
        return orderBillingInfo;
    }

    public void setOrderBillingInfo(MstarOrderBillingAddress orderBillingInfo) {
        this.orderBillingInfo = orderBillingInfo;
    }

    public MstarOrderShippingAddress getOrderShippingInfo() {
        return orderShippingInfo;
    }

    public void setOrderShippingInfo(MstarOrderShippingAddress orderShippingInfo) {
        this.orderShippingInfo = orderShippingInfo;
    }

    public List<MstarOrderDetail> getOrderdetail() {
        return orderdetail;
    }

    public void setOrderdetail(List<MstarOrderDetail> orderdetail) {
        this.orderdetail = orderdetail;
    }

    public List<MstarDrugDetail> getCanceledItemsDetails() {
        return canceledItemsDetails;
    }

    public void setCanceledItemsDetails(List<MstarDrugDetail> canceledItemsDetails) {
        this.canceledItemsDetails = canceledItemsDetails;
    }

    public BigDecimal getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(BigDecimal productAmount) {
        this.productAmount = productAmount;
    }

    public BigDecimal getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(BigDecimal couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public BigDecimal getShippingAmount() {
        return shippingAmount;
    }

    public void setShippingAmount(BigDecimal shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public BigDecimal getCodCharge() {
        return codCharge;
    }

    public void setCodCharge(BigDecimal codCharge) {
        this.codCharge = codCharge;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(BigDecimal voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public BigDecimal getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(BigDecimal usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getSavingsAmount() {
        return savingsAmount;
    }

    public void setSavingsAmount(BigDecimal savingsAmount) {
        this.savingsAmount = savingsAmount;
    }

    public List<String> getRxId() {
        return rxId;
    }

    public void setRxId(List<String> rxId) {
        this.rxId = rxId;
    }

    public String getExpressStatus() {
        return expressStatus;
    }

    public void setExpressStatus(String expressStatus) {
        this.expressStatus = expressStatus;
    }

    //For Order Track Details
    @SerializedName("trackDetails")
    private List<MstarOrderTrackDetail> trackDetails;

    public List<MstarOrderTrackDetail> getTrackDetails() {
        return trackDetails;
    }

    public void setTrackDetails(List<MstarOrderTrackDetail> trackDetails) {
        this.trackDetails = trackDetails;
    }

    public boolean isSubscriptionSubscribed() {
        return isSubscriptionSubscribed;
    }
}
