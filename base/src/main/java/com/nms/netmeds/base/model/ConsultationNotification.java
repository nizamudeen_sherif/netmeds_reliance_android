package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsultationNotification implements Serializable {
    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private NotificationBody body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public NotificationBody getBody() {
        return body;
    }

    public void setBody(NotificationBody body) {
        this.body = body;
    }
}
