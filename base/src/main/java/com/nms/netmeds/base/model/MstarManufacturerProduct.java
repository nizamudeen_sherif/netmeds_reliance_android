package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarManufacturerProduct {
    @SerializedName("brand_name")
    private String brandName;

    @SerializedName("product_code")
    private Integer productCode;

    @SerializedName("display_name")
    private String displayName;

    @SerializedName("url_path")
    private String urlPath;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getProductCode() {
        return productCode;
    }

    public void setProductCode(Integer productCode) {
        this.productCode = productCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }
}
