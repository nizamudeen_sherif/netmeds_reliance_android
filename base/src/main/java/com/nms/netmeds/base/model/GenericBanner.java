package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class GenericBanner {
    @SerializedName("name")
    private String imageName;
    @SerializedName("imageUrl")
    private String url;
    @SerializedName("linktype")
    private String linktype;
    @SerializedName("displayFrom")
    private String displayFrom;
    @SerializedName("displayTo")
    private String displayTo;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getDisplayFrom() {
        return displayFrom;
    }

    public void setDisplayFrom(String displayFrom) {
        this.displayFrom = displayFrom;
    }

    public String getDisplayTo() {
        return displayTo;
    }

    public void setDisplayTo(String displayTo) {
        this.displayTo = displayTo;
    }
}
