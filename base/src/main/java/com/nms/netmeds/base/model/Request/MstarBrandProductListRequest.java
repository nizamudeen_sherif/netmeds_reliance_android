package com.nms.netmeds.base.model.Request;

public class MstarBrandProductListRequest {

    private String returnProduct;
    private Integer pageNo;
    private Integer pageSize;
    private String returnFacets;
    private int brandID;

    public String getReturnProduct() {
        return returnProduct;
    }

    public void setReturnProduct(String returnProduct) {
        this.returnProduct = returnProduct;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getReturnFacets() {
        return returnFacets;
    }

    public void setReturnFacets(String returnFacets) {
        this.returnFacets = returnFacets;
    }

    public int getBrandID() {
        return brandID;
    }

    public void setBrandID(int brandID) {
        this.brandID = brandID;
    }
}
