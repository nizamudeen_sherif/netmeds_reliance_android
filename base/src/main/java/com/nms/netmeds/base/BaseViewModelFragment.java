package com.nms.netmeds.base;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

public abstract class BaseViewModelFragment<VM extends BaseViewModel> extends BaseFragment implements LoaderHelper {

    protected Dialog dialog = null;

    protected abstract VM onCreateViewModel();

    @Override
    public void showProgress(Context context) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context, R.style.CustomProgressTheme);
                dialog.setContentView(R.layout.custom_progress);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showHorizontalProgressBar(boolean isClickable, ProgressBar progressBar) {
        if(getActivity()!=null) {
            if (!isClickable) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                progressBar.setVisibility(View.VISIBLE);
            } else {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void dismissProgress() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = null;
    }

    public <T extends BaseViewModel> void onRetry(final T viewModel, View view) {
        view.findViewById(R.id.btn_api_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.onRetryClickListener();
            }
        });

        view.findViewById(R.id.btn_network_reload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.onRetryClickListener();
            }
        });
    }

    @Override
    public void showTransactionProgress(Context context) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context, R.style.TransactionProgressTheme);
                dialog.setContentView(R.layout.custom_card_progress);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
