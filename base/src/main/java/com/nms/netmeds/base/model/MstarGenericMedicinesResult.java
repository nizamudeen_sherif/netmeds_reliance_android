package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarGenericMedicinesResult {
    @SerializedName("saveUpTo")
    private String saveUpTo;
    @SerializedName("mainDrug")
    private MStarProductDetails mainDrug;
    @SerializedName("genericBrands")
    private List<MStarProductDetails> genericBrands;
    @SerializedName("discount")
    private GenericDiscount genericDiscount;

    public String getSaveUpTo() {
        return saveUpTo;
    }

    public void setSaveUpTo(String saveUpTo) {
        this.saveUpTo = saveUpTo;
    }

    public MStarProductDetails getMainDrug() {
        return mainDrug;
    }

    public void setMainDrug(MStarProductDetails mainDrug) {
        this.mainDrug = mainDrug;
    }

    public List<MStarProductDetails> getGenericBrands() {
        return genericBrands;
    }

    public void setGenericBrands(List<MStarProductDetails> genericBrands) {
        this.genericBrands = genericBrands;
    }

    public GenericDiscount getGenericDiscount() {
        return genericDiscount;
    }

    public void setGenericDiscount(GenericDiscount genericDiscount) {
        this.genericDiscount = genericDiscount;
    }

    public class GenericDiscount {
        @SerializedName("content")
        private String content;
        @SerializedName("tooltip")
        private String tooltip;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getTooltip() {
            return tooltip;
        }

        public void setTooltip(String tooltip) {
            this.tooltip = tooltip;
        }
    }
}
