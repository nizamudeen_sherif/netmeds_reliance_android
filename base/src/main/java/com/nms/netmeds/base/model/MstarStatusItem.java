package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarStatusItem {
    @SerializedName("value")
    private String value;
    @SerializedName("title")
    private String title;
    @SerializedName("count")
    private String count;
    private boolean isChecked;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
