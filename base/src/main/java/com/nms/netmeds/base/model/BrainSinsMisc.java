package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class BrainSinsMisc {
    @SerializedName("original_price")
    private String originalPrice;
    @SerializedName("discount")
    private String discount;

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
