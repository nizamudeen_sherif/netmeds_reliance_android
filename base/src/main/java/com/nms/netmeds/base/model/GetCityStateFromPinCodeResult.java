package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class GetCityStateFromPinCodeResult {

    @SerializedName("cod_supported")
    private boolean cod_supported;
    @SerializedName("cold_storage_supported")
    private boolean cold_storage_supported;
    @SerializedName("pin_blocked")
    private boolean pin_blocked;
    @SerializedName("state")
    private String state;
    @SerializedName("district")
    private String district;
    @SerializedName("state_name")
    private String state_name;
    @SerializedName("state_code")
    private String state_code;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public boolean isCod_supported() {
        return cod_supported;
    }

    public void setCod_supported(boolean cod_supported) {
        this.cod_supported = cod_supported;
    }

    public boolean isCold_storage_supported() {
        return cold_storage_supported;
    }

    public void setCold_storage_supported(boolean cold_storage_supported) {
        this.cold_storage_supported = cold_storage_supported;
    }

    public boolean isPin_blocked() {
        return pin_blocked;
    }

    public void setPin_blocked(boolean pin_blocked) {
        this.pin_blocked = pin_blocked;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }
}
