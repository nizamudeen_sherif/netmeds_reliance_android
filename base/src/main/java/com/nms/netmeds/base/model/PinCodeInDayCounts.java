package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class PinCodeInDayCounts {
    @SerializedName("on_or_after")
    private Integer onOrAfter;
    @SerializedName("on_or_before")
    private Integer onOrBefore;

    public Integer getOnOrAfter() {
        return onOrAfter;
    }

    public void setOnOrAfter(Integer onOrAfter) {
        this.onOrAfter = onOrAfter;
    }

    public Integer getOnOrBefore() {
        return onOrBefore;
    }

    public void setOnOrBefore(Integer onOrBefore) {
        this.onOrBefore = onOrBefore;
    }

}
