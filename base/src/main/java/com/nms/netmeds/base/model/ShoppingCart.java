package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ShoppingCart {
    @SerializedName("header")
    public String header;
    @SerializedName("description")
    public String description;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
