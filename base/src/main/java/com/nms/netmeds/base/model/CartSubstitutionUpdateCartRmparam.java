package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSubstitutionUpdateCartRmparam {

    @SerializedName("itemid")
    private String itemid;

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

}
