package com.nms.netmeds.base.config;

public class ConfigConstant {

    public static final String MSTAR_BASE_URL = "Mstar_base_url";
    public static final String MSTAR_CUSTOM_API_URL = "Mstar_custom_api";
    public static final String MSTAR_PRODUCT_IMAGE_BASE_URL = "Mstar_product_image_base_url";

    public static final String CART_SUBSTITUTION_BASE_URL = "Cart_Substitution_Base_Url";

    public static final String BASE_URL = "Base_Url";
    public static final String BASE_DEFAULT_URL = "Base_Default_Url";
    public static final String BASE_URL_V2 = "Base_Url_V2";
    public static final String CONSULTATION_BASE_URL = "Consultation_base_url";
    public static final String CONSULTATION_IMAGE_URL = "Consultation_image_url";
    public static final String PAY_TM_BASE_URL = "Pay_Tm_Base_Url";
    public static final String PAY_TM_ACCOUNT_BASE_URL = "Pay_Tm_Account_Base_Url";
    public static final String STORE_ID = "Store_Id";
    public static final String RECOMMENDATION_ENGINE_URL = "Recommendation_Engine_Url";
    public static final String RECOMMENDATION_ENGINE_TOKEN = "Recommendation_Engine_Token";
    public static final String RECOMMENDATION_lANG = "Recommendation_lang";
    public static final String HEALTH_LIBRARY_URL = "Health_Library_Url";
    public static final String REQUEST_POPUP_BASE_URL = "Request_Popup_Url";
    public static final String GCM_SENDER_ID = "GCM_Sender_ID";
    public static final String BANNER_BASE_URL = "Banner_base_url";
    public static final String TUNE_ADVERTISER_ID = "Tune_Advertiser_ID";
    public static final String TUNE_CONVERSION_KEY = "Tune_Conversion_Key";
    public static final String GOOGLE_ANALYTICS_ID = "Google_Analytics_ID";
    public static final String WEBSITE_DOMAIN = "Website_Domain";
    public static final String MOBILE_DOMAIN = "Mobile_Domain";
    public static final String SHORT_URL_DOMAIN = "Short_Url_Domain";
    public static final String BASE_IMAGE_URL = "Base_Image_Url";
    public static final String FORCE_UPDATE_VERSION = "Force_Update_Version";
    public static final String WEB_ENGAGE_KEY = "Web_Engage_Key";
    public static final String DIAGNOSTIC_BASE_URL = "Diagnostic_base_url";
    public static final String CONSULTATION_DOMAIN = "Consultation_Domain";
    public static final String DIAGNOSTIC_DOMAIN = "Diagnostic_Domain";
    public static final String JUSPAY_CLIENT_ID = "JusPay_Client_Id";
    public static final String ALGOLIA_INDEX_RADIO = "algolia_index_radio";
    public static final String ALGOLIA_APP_KEY = "aloglia_app_key";
    /**
     * JusPay Constant
     */
    public static final String PRIVACY_POLICY = "Privacy_policy_url";
    public static final String TERMS_CONDITION = "Terms_and_condition_url";
    public static final String FAQ_URL = "FAQ_url";

    //PayPal Credentials
    public static final String PAYTM_LINK_OTP_RESPONSETYPE = "Paytm_Link_Otp_Responsetype";
    public static final String PAYTM_LINK_OTP_SCOPE = "Paytm_Link_Otp_Scope";
    public static final String PAYTM_LINK_AUTHMODE = "Paytm_Link_Authmode";
    public static final String PAYTM_LINK_PAYMENTMODE = "Paytm_Link_Paymentmode";
    public static final String PAYTM_LINK_WITHDRAW_REQUESTTYPE = "Paytm_Link_Withdraw_Requesttype";
    public static final String PAYTM_LINK_CURRENCY = "Paytm_Link_Currency";
    public static final String PAYTM_LINK_ADDMONEY_REQUESTTYPE = "Paytm_Link_Addmoney_Requesttype";
    public static final String PAYTM_LINK_APPIP = "Paytm_Link_Appip";

    //Request Popup
    public static final String REQUEST_API_KEY = "Request_api_key";
    public static final String REQUEST_SET_RECIPIENTS = "Request_setrecipients";
    public static final String REQUEST_SUBJECT = "Request_subject";
    public static final String REQUEST_FROM_MAIL = "Request_mail";
    public static final String FAQ_KAPTURE_CRM = "kapture_crm";
    public static final String FAQ_ALL_CATEGORY_AUTH_TOKEN = "faq_all_category_auth_token";
    public static final String FAQ_CATEGORY_CONTENT_AUTH_TOKEN = "faq_category_content_auth_token";
    public static final String REGISTRATION_DETAILS = "Registration_details";
    public static final String OS = "Os";
    public static final String SEND_MAIL_TYPE_ID = "SendEmailTypeid";
    public static final String Pop_MAIL_TYPE_ID = "PopMailRequestMedicineTypeid";

    //social Login
    public static final String SOURCE_APP = "Source_app";
    public static final String GOOGLE_FLAG = "GoogleFlag";
    public static final String FACEBOOK_FLAG = "FacebookFlag";
    public static final String TRUECALLER_FLAG = "TrueCallerFlag";

    /*CLOUD_CHERRY*/
    public static final String CLOUD_CHERRY_URL = "Cloud_Cherry_URL";
    public static final String CLOUD_CHERRY_LOGIN_URL = "Cloud_Cherry_Login_URL";
    public static final String CLOUD_CHERRY_PRECISSION_URL = "Cloud_Cherry_Precission_URL";
    public static final String INVOICE_DOWNLOAD_BASE_URL = "Invoice_Download_base_url";

    //CLICK POST
    public static final String CLICK_POST_URL = "Click_Post_Url";

    //BRAIN SINS
    public static final String RECOMMENDED_PRODUCT_URL = "Recommended_Product_Url";
    public static final String RECOMMENDED_CART_URL = "Recommended_Cart_Url";

    //Product Flavor
    public static final String PRODUCT_FLAVOR = "Product_flavor";

    //Boomrang
    public static final String BOOMRANG_BASE_URL = "Boomrang";
    public static final String BOOMRANG_AUTH_TOKEN = "BoomrangAuthToken";

}

