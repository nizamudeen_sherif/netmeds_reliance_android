package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarWellnessSectionDetails {

    @SerializedName("designType")
    private String designType;
    @SerializedName("parseType")
    private String parseType;
    @SerializedName("position")
    private int position;
    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("tagline")
    private String tagline;
    @SerializedName("bgColor")
    private String bgColor;
    @SerializedName("bgImage")
    private String bgImage;
    @SerializedName("viewAll")
    private String viewAll;
    @SerializedName("type")
    private String type;
    @SerializedName("redirectType")
    private String redirectType;

    @SerializedName("list")
    private List<MStarProductDetails> productDetails;

    public String getDesignType() {
        return designType;
    }

    public void setDesignType(String designType) {
        this.designType = designType;
    }

    public String getParseType() {
        return parseType;
    }

    public void setParseType(String parseType) {
        this.parseType = parseType;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getBgImage() {
        return bgImage;
    }

    public void setBgImage(String bgImage) {
        this.bgImage = bgImage;
    }

    public String getViewAll() {
        return viewAll;
    }

    public void setViewAll(String viewAll) {
        this.viewAll = viewAll;
    }

    public List<MStarProductDetails> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(List<MStarProductDetails> productDetails) {
        this.productDetails = productDetails;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(String redirectType) {
        this.redirectType = redirectType;
    }
}
