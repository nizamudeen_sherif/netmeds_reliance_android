package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class CartSubstitutionBasicResponseTemplateModel {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private CartSubstitutionResponseModel result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CartSubstitutionResponseModel getResult() {
        return result;
    }

    public void setResult(CartSubstitutionResponseModel result) {
        this.result = result;
    }
}