package com.nms.netmeds.base.utils;

import android.content.Context;

import com.algolia.instantsearch.insights.Insights;
import com.algolia.instantsearch.insights.event.EventObjects;
import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarCustomerDetails;

import java.util.ArrayList;
import java.util.List;

public class AlgoliaClickAndConversionHelper {
    //Event Name
    public static final String EVENT_NAME_ADDED_TO_CART = "Added to Cart";
    public static final String EVENT_NAME_PRODUCT_VIEWED = "Product Viewed";
    private static final int CONNECTION_TIME = 5000;
    private static AlgoliaClickAndConversionHelper algoliaClickAndConversionHelper;

    /**
     * Create a getInstance() method to return the instance of
     * AlgoliaClickAndConversionHelper.This function ensures that
     * the variable is instantiated only once and the same
     * instance is used entire application
     *
     * @return AlgoliaClickAndConversionHelper instance
     */
    public static AlgoliaClickAndConversionHelper getInstance() {
        if (algoliaClickAndConversionHelper == null) {
            algoliaClickAndConversionHelper = new AlgoliaClickAndConversionHelper();
        }
        return algoliaClickAndConversionHelper;
    }

    /**
     * Algolia instant search configuration
     *
     * @param context current state of the application/object
     */
    private void initAlgoliaInstantSearchInsight(Context context) {
        MStarCustomerDetails customerDetails = new Gson().fromJson(BasePreference.getInstance(context).getCustomerDetails(), MStarCustomerDetails.class);
        String userId = customerDetails != null ? Integer.toString(customerDetails.getId()) : "";

        Insights.Configuration configuration = new Insights.Configuration(CONNECTION_TIME, CONNECTION_TIME);
        Insights insights = Insights.register(context, CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(context), AppConstant.ALGOLIA_APP_ID), CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(context), AppConstant.ALGOLIA_API_KEY), CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(context), AppConstant.ALGOLIA_INDEX), configuration);
        insights.setUserToken(userId);
        insights.setMinBatchSize(1);
        insights.setLoggingEnabled(false);
    }

    /**
     * Algolia click event
     *
     * @param skuId    item id
     * @param position item position
     * @param queryId  search result set id
     */
    public void onAlgoliaClickEvent(Context context, String skuId, int position, String queryId,String eventName) {
        if (context != null && !BasePreference.getInstance(context).isGuestCart()) {
            initAlgoliaInstantSearchInsight(context);
            List<Integer> positionList = new ArrayList<>();
            positionList.add(position);
            Insights.shared(CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(context), AppConstant.ALGOLIA_INDEX)).clickedAfterSearch(
                    eventName,
                    queryId,
                    new EventObjects.IDs(skuId),
                    positionList
            );
        }
    }

    /**
     * Algolia conversion event
     *
     * @param skuId   item id
     * @param queryId search result set id
     */
    public void onAlgoliaConversionEvent(Context context, String skuId, String queryId) {
        if (context != null && !BasePreference.getInstance(context).isGuestCart()) {
            initAlgoliaInstantSearchInsight(context);
            Insights.shared(CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(context), AppConstant.ALGOLIA_INDEX)).convertedAfterSearch(
                    EVENT_NAME_ADDED_TO_CART,
                    queryId,
                    new EventObjects.IDs(skuId)
            );
        }
    }
}
