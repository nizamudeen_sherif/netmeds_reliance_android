package com.nms.netmeds.base;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;

public class BaseActivity extends AppCompatActivity implements LoaderHelper {

    protected Dialog dialog = null;
    String variant;
    String flavor;
    String str = BuildConfig.BUILD_TYPE;
    WindowManager windowManager;
    View view;


    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void toolBarSetUp(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }
    }

    @Override
    public void showProgress(Context context) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context, R.style.CustomProgressTheme);
                dialog.setContentView(R.layout.custom_progress);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showHorizontalProgressBar(boolean isClickable, ProgressBar progressBar) {
        if(!isClickable){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.VISIBLE);
        }else{
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    public Dialog getProgressDialog() {
        return dialog;
    }


    public boolean isSimilarFragment(Fragment fragment) {
        return fragment != null && getActiveFragment() != null && fragment.getClass().getName().equals(getActiveFragment().getClass().getName());
    }

    public Fragment getActiveFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void dismissProgress() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = null;
    }

    @Override
    public void showTransactionProgress(Context context) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context, R.style.TransactionProgressTheme);
                dialog.setContentView(R.layout.custom_card_progress);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        variant = str.substring(0, 1).toUpperCase() + str.substring(1);
        flavor = ConfigMap.getInstance().getProperty(ConfigConstant.PRODUCT_FLAVOR);
        str = flavor + variant;

        if (!str.equals("prodRelease")) {
            bannerWindow(flavor, variant);
        }

        //Disable auto fill option
        disableAutoFill();
    }

    private void bannerWindow(String flavor, String variant) {
        int statusBarHeight = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_DRAWN_APPLICATION,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,    // Keeps the button presses from going to the background window and Draws over status bar
                PixelFormat.TRANSLUCENT);

        parameters.y = statusBarHeight;
        parameters.gravity = Gravity.LEFT | Gravity.TOP;
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        LinearLayout ll = new LinearLayout(this);
        view = ll.inflate(this, R.layout.banner_variant, ll);

        LinearLayout.LayoutParams layoutParameteres = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ll.setLayoutParams(layoutParameteres);

        TextView tvBuildFlavour = view.findViewById(R.id.banner_text_view);
        if (variant.equals("Debug")) {
            tvBuildFlavour.setBackgroundColor(Color.parseColor("#B3ff0000"));
        }
        tvBuildFlavour.setText(str);
        windowManager.addView(view, parameters);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!str.equals("prodRelease"))
            windowManager.removeViewImmediate(view);
    }

    private void disableAutoFill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }
    }
}
