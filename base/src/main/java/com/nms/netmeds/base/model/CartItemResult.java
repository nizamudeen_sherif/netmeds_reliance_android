package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CartItemResult implements Serializable {

    @SerializedName("skuId")
    private String skuId;
    @SerializedName("name")
    private String name;
    @SerializedName("manufacturerName")
    private String manufacturerName;
    @SerializedName("coldStorage")
    private Boolean coldStorage;
    @SerializedName("drugSchedule")
    private String drugSchedule;
    @SerializedName("drugType")
    private String drugType;
    @SerializedName("maxQuantity")
    private Integer maxQuantity = 0;
    @SerializedName("qty")
    private Integer quantity = 0;
    @SerializedName("availableStatus")
    private String availableStatus;
    @SerializedName("availableStatusText")
    private String availableStatusText;
    @SerializedName("strikePrice")
    private String strikePrice;
    @SerializedName("actualPrice")
    private String actualPrice;
    @SerializedName("packSizeTypeLabel")
    private String packSizeTypeLabel;
    @SerializedName("itemId")
    private String itemId;

    transient private String deliveryEstimate;
    transient private String itemSeller;
    transient private String sellerAddress;
    transient private String itemExpiry;
    transient private boolean isOutOfStockProduct;


    //for alternate Cart UI
    private boolean isCartRevertAvailable;
    private String originalProductName;
    private String originalProductCode;
    private double originalProductPrice;
    private int originalProductQuantity;

    public String getDeliveryEstimate() {
        return deliveryEstimate;
    }

    public void setDeliveryEstimate(String deliveryEstimate) {
        this.deliveryEstimate = deliveryEstimate;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Boolean getColdStorage() {
        return coldStorage;
    }

    public void setColdStorage(Boolean coldStorage) {
        this.coldStorage = coldStorage;
    }

    public String getDrugSchedule() {
        return drugSchedule;
    }

    public void setDrugSchedule(String drugSchedule) {
        this.drugSchedule = drugSchedule;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus;
    }

    public String getAvailableStatusText() {
        return availableStatusText;
    }

    public void setAvailableStatusText(String availableStatusText) {
        this.availableStatusText = availableStatusText;
    }

    public String getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(String strikePrice) {
        this.strikePrice = strikePrice;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getPackSizeTypeLabel() {
        return packSizeTypeLabel;
    }

    public void setPackSizeTypeLabel(String packSizeTypeLabel) {
        this.packSizeTypeLabel = packSizeTypeLabel;
    }

    public String getItemSeller() {
        return itemSeller;
    }

    public void setItemSeller(String itemSeller) {
        this.itemSeller = itemSeller;
    }

    public String getItemExpiry() {
        return itemExpiry;
    }

    public void setItemExpiry(String itemExpiry) {
        this.itemExpiry = itemExpiry;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public boolean isOutOfStockProduct() {
        return isOutOfStockProduct;
    }

    public void setOutOfStockProduct(boolean outOfStockProduct) {
        isOutOfStockProduct = outOfStockProduct;
    }

    public boolean isCartRevertAvailable() {
        return isCartRevertAvailable;
    }

    public void setCartRevertAvailable(boolean cartRevertAvailable) {
        isCartRevertAvailable = cartRevertAvailable;
    }

    public String getOriginalProductName() {
        return originalProductName;
    }

    public void setOriginalProductName(String originalProductName) {
        this.originalProductName = originalProductName;
    }

    public String getOriginalProductCode() {
        return originalProductCode;
    }

    public void setOriginalProductCode(String originalProductCode) {
        this.originalProductCode = originalProductCode;
    }

    public double getOriginalProductPrice() {
        return originalProductPrice;
    }

    public void setOriginalProductPrice(double originalProductPrice) {
        this.originalProductPrice = originalProductPrice;
    }

    public int getOriginalProductQuantity() {
        return originalProductQuantity;
    }

    public void setOriginalProductQuantity(int originalProductQuantity) {
        this.originalProductQuantity = originalProductQuantity;
    }
}
