package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class MStarProductDetails {

    @SerializedName(value = "selling_price", alternate = "sellingPrice")
    private BigDecimal sellingPrice = BigDecimal.ZERO;
    @SerializedName("updated_time")
    private String updatedTime;
    @SerializedName(value = "rx_required", alternate = "rxRequired")
    private boolean rxRequired;
    @SerializedName("manufacturer_url")
    private String manufacturerUrl;
    @SerializedName("is_cold_storage")
    private boolean isColdStorage;
    @SerializedName("health_concern_id")
    private Object healthConcernId;
    @SerializedName("json_ext")
    private Object jsonExt;
    @SerializedName(value = "discount_rate", alternate = {"discountRate"})
    private BigDecimal discount = BigDecimal.ZERO;
    @SerializedName(value = "product_code", alternate = {"productCode"})
    private int productCode;
    @SerializedName(value = "pack_label", alternate = "packLabel")
    private String packLabel;
    @SerializedName(value = "stock_qty", alternate = "stockQty")
    private int stockQty;
    //In Cart Page mention the selected Quantity
    @SerializedName("qty")
    private int cartQuantity;
    @SerializedName("generic_with_dosage_id")
    private int genericWithDosageId;
    @SerializedName("type_of_package")
    private String typeOfPackage;
    @SerializedName(value = "categories", alternate = "categoryName")
    private List<MStarCategory> categories = null;
    @SerializedName("url_path")
    private String urlPath;
    @SerializedName("multiple_qty_rules")
    private MStarPackOfNRulesModel packOfNRules;
    @SerializedName("manufacturer_id")
    private int manufacturerId;
    @SerializedName("generic_id")
    private int genericId;
    @SerializedName("image_paths")
    private List<String> imagePaths;
    @SerializedName(value = "max_qty_in_order", alternate = "maxQty")
    private int maxQtyInOrder;
    @SerializedName("mrp")
    private BigDecimal mrp = BigDecimal.ZERO;
    @SerializedName("brand_name")
    private String brandName;
    @SerializedName(value = "manufacturer_name", alternate = {"manufactureName"})
    private String manufacturerName;
    @SerializedName(value = "display_name", alternate = {"displayName"})
    private String displayName;
    @SerializedName("brand_id")
    private Object brandId;
    @SerializedName("similar_products")
    private String similarProducts;
    @SerializedName("frequently_bought_together")
    private String frequentlyBoughtTogether;
    @SerializedName(value = "availability_status", alternate = "availabilityStatus")
    private String availabilityStatus;
    @SerializedName(value = "schedule", alternate = "product_schedule")
    private String schedule;
    @SerializedName(value = "product_type", alternate = "productType")
    private String productType;
    @SerializedName("pack_size")
    private String packSize;
    @SerializedName("is_dpco")
    private boolean isDpco;
    @SerializedName("formulation_type")
    private String formulationType;
    @SerializedName("manufacturer")
    private MstarBaseIdUrl manufacturer;
    @SerializedName("generic_with_dosage")
    private MstarBaseIdUrl genericWithDosage;
    @SerializedName("generic")
    private MstarBaseIdUrl generic;
    @SerializedName(value = "product_image_path", alternate = {"image", "imageUrl"})
    private String product_image_path;
    @SerializedName("discount_pct")
    private BigDecimal discountPercentage = BigDecimal.ZERO;
    @SerializedName("is_alternate_available")
    private boolean isAlternateAvailable;
    @SerializedName("is_alternate_switched")
    private boolean isAlternateSwitched;
    @SerializedName("is_returnable")
    private boolean isReturnable;
    @SerializedName("variants")
    private List<Variant> productVariant;

    //for cart
    @SerializedName("unit_product_discount")
    private BigDecimal unitProductDiscount = BigDecimal.ZERO;
    @SerializedName("line_product_discount")
    private BigDecimal lineProductDiscount = BigDecimal.ZERO;
    @SerializedName("unit_coupon_discount")
    private BigDecimal unitCouponDiscount = BigDecimal.ZERO;
    @SerializedName("line_coupon_discount")
    private BigDecimal lineCouponDiscount = BigDecimal.ZERO;
    @SerializedName("line_mrp")
    private BigDecimal lineMrp = BigDecimal.ZERO;
    @SerializedName("rate")
    private BigDecimal rate = BigDecimal.ZERO;
    @SerializedName("line_value")
    private BigDecimal lineValue = BigDecimal.ZERO;
    @SerializedName("dosage")
    private String dosage;
    @SerializedName("out_of_stock")
    private boolean outofStock = false;

    //for Wellness Section -Banner,Product,Category
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("level")
    private int level;
    @SerializedName("imageName")
    private String imageName;
    @SerializedName("url")
    private String url;
    @SerializedName("linktype")
    private String linktype;
    @SerializedName("displayFrom")
    private String displayFrom;
    @SerializedName("displayTo")
    private String displayTo;
    @SerializedName("linkpage")
    private String linkpage;
    @SerializedName("discount")
    private String discountUpto;

    //ForAlternate
    @SerializedName("alternate_product_name")
    private String alternateProductName;
    @SerializedName("alternate_product_mrp")
    private BigDecimal alternateProductMrp = BigDecimal.ZERO;
    @SerializedName("alternate_product_line_value")
    private BigDecimal alternateProductLineValue = BigDecimal.ZERO;


    //for Generics
    @SerializedName("genericDosage")
    private String genericDosage;
    @SerializedName("perTabletCost")
    private String perTabletCost;
    @SerializedName("youSave")
    private String youSave;
    @SerializedName("cheaper")
    private String cheaper;


    //In order review screen
    private String itemSeller;
    private String sellerAddress;
    private String itemExpiry;
    private String deliveryEstimate;
    private boolean isOutOfStockProduct;

    //For BuyAgain variables
    private boolean isNotAbleToAddToCartStatus;
    private String notAddedToCartMessage;
    private int addToCartVisibility;
    private int quantityPickerVisibility;


    //for Brainsins
    private boolean isForPeopleAlsoViewed;

    //For cart Page
    private MStarAddedProductsResult productAddResult;

    //for Notify user -out of stock product
    private static MStarProductDetails productDetails;
    private boolean isNotifyGuestUser = false;

    //for WebEngage event
    private String pageName;

    public static MStarProductDetails getInstance() {
        if (productDetails == null) {
            productDetails = new MStarProductDetails();
        }
        return productDetails;
    }

    public static MStarProductDetails getProductDetails() {
        return productDetails;
    }

    public static void setProductDetails(MStarProductDetails productDetails) {
        MStarProductDetails.productDetails = productDetails;
    }

    public boolean isNotifyGuestUser() {
        return isNotifyGuestUser;
    }

    public void setNotifyGuestUser(boolean notifyUser) {
        isNotifyGuestUser = notifyUser;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getManufacturerUrl() {
        return manufacturerUrl;
    }

    public void setManufacturerUrl(String manufacturerUrl) {
        this.manufacturerUrl = manufacturerUrl;
    }

    public Object getHealthConcernId() {
        return healthConcernId;
    }

    public void setHealthConcernId(Object healthConcernId) {
        this.healthConcernId = healthConcernId;
    }

    public Object getJsonExt() {
        return jsonExt;
    }

    public void setJsonExt(Object jsonExt) {
        this.jsonExt = jsonExt;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getPackLabel() {
        return packLabel;
    }

    public void setPackLabel(String packLabel) {
        this.packLabel = packLabel;
    }

    public int getStockQty() {
        return stockQty;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;
    }

    public int getGenericWithDosageId() {
        return genericWithDosageId;
    }

    public void setGenericWithDosageId(int genericWithDosageId) {
        this.genericWithDosageId = genericWithDosageId;
    }

    public String getTypeOfPackage() {
        return typeOfPackage;
    }

    public void setTypeOfPackage(String typeOfPackage) {
        this.typeOfPackage = typeOfPackage;
    }

    public List<MStarCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<MStarCategory> categories) {
        this.categories = categories;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public int getGenericId() {
        return genericId;
    }

    public void setGenericId(int genericId) {
        this.genericId = genericId;
    }

    public List<String> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(List<String> imagePaths) {
        this.imagePaths = imagePaths;
    }

    public int getMaxQtyInOrder() {
        return maxQtyInOrder;
    }

    public void setMaxQtyInOrder(int maxQtyInOrder) {
        this.maxQtyInOrder = maxQtyInOrder;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Object getBrandId() {
        return brandId;
    }

    public void setBrandId(Object brandId) {
        this.brandId = brandId;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPackSize() {
        return packSize;
    }

    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    public String getFormulationType() {
        return formulationType;
    }

    public void setFormulationType(String formulationType) {
        this.formulationType = formulationType;
    }

    public MStarPackOfNRulesModel getPackOfNRules() {
        return packOfNRules;
    }

    public void setPackOfNRules(MStarPackOfNRulesModel packOfNRules) {
        this.packOfNRules = packOfNRules;
    }

    public String getSimilarProducts() {
        return similarProducts;
    }

    public void setSimilarProducts(String similarProducts) {
        this.similarProducts = similarProducts;
    }

    public String getFrequentlyBoughtTogether() {
        return frequentlyBoughtTogether;
    }

    public void setFrequentlyBoughtTogether(String frequentlyBoughtTogether) {
        this.frequentlyBoughtTogether = frequentlyBoughtTogether;
    }

    public MstarBaseIdUrl getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(MstarBaseIdUrl manufacturer) {
        this.manufacturer = manufacturer;
    }

    public MstarBaseIdUrl getGenericWithDosage() {
        return genericWithDosage;
    }

    public void setGenericWithDosage(MstarBaseIdUrl genericWithDosage) {
        this.genericWithDosage = genericWithDosage;
    }

    public MstarBaseIdUrl getGeneric() {
        return generic;
    }

    public void setGeneric(MstarBaseIdUrl generic) {
        this.generic = generic;
    }

    public boolean isRxRequired() {
        return rxRequired;
    }

    public void setRxRequired(boolean rxRequired) {
        this.rxRequired = rxRequired;
    }

    public boolean isColdStorage() {
        return isColdStorage;
    }

    public void setColdStorage(boolean coldStorage) {
        isColdStorage = coldStorage;
    }

    public boolean isDpco() {
        return isDpco;
    }

    public void setDpco(boolean dpco) {
        isDpco = dpco;
    }

    //For Brand Filter in CategoryActvity

    private List<MstarFacetItemDetails> brandList;

    public List<MstarFacetItemDetails> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<MstarFacetItemDetails> brandList) {
        this.brandList = brandList;
    }

    public int getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(int cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

    public String getItemSeller() {
        return itemSeller;
    }

    public void setItemSeller(String itemSeller) {
        this.itemSeller = itemSeller;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public String getItemExpiry() {
        return itemExpiry;
    }

    public void setItemExpiry(String itemExpiry) {
        this.itemExpiry = itemExpiry;
    }

    public boolean isOutOfStockProduct() {
        return isOutOfStockProduct;
    }

    public void setOutOfStockProduct(boolean outOfStockProduct) {
        isOutOfStockProduct = outOfStockProduct;
    }

    public String getDeliveryEstimate() {
        return deliveryEstimate;
    }

    public void setDeliveryEstimate(String deliveryEstimate) {
        this.deliveryEstimate = deliveryEstimate;
    }

    public String getProduct_image_path() {
        return product_image_path;
    }

    public void setProduct_image_path(String product_image_path) {
        this.product_image_path = product_image_path;
    }

    public BigDecimal getUnitProductDiscount() {
        return unitProductDiscount;
    }

    public void setUnitProductDiscount(BigDecimal unitProductDiscount) {
        this.unitProductDiscount = unitProductDiscount;
    }

    public BigDecimal getLineProductDiscount() {
        return lineProductDiscount;
    }

    public void setLineProductDiscount(BigDecimal lineProductDiscount) {
        this.lineProductDiscount = lineProductDiscount;
    }

    public BigDecimal getUnitCouponDiscount() {
        return unitCouponDiscount;
    }

    public void setUnitCouponDiscount(BigDecimal unitCouponDiscount) {
        this.unitCouponDiscount = unitCouponDiscount;
    }

    public BigDecimal getLineCouponDiscount() {
        return lineCouponDiscount;
    }

    public void setLineCouponDiscount(BigDecimal lineCouponDiscount) {
        this.lineCouponDiscount = lineCouponDiscount;
    }

    public BigDecimal getLineMrp() {
        return lineMrp;
    }

    public void setLineMrp(BigDecimal lineMrp) {
        this.lineMrp = lineMrp;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getLineValue() {
        return lineValue;
    }

    public void setLineValue(BigDecimal lineValue) {
        this.lineValue = lineValue;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public MStarAddedProductsResult getProductAddResult() {
        return productAddResult;
    }

    public void setProductAddResult(MStarAddedProductsResult productAddResult) {
        this.productAddResult = productAddResult;
    }

    public boolean isForPeopleAlsoViewed() {
        return isForPeopleAlsoViewed;
    }

    public void setForPeopleAlsoViewed(boolean forPeopleAlsoViewed) {
        isForPeopleAlsoViewed = forPeopleAlsoViewed;
    }

    public boolean isOutofStock() {
        return outofStock;
    }

    public void setOutofStock(boolean outofStock) {
        this.outofStock = outofStock;
    }

    public boolean isAlternateAvailable() {
        return isAlternateAvailable;
    }

    public void setAlternateAvailable(boolean alternateAvailable) {
        isAlternateAvailable = alternateAvailable;
    }

    public boolean isAlternateSwitched() {
        return isAlternateSwitched;
    }

    public void setAlternateSwitched(boolean alternateSwitched) {
        isAlternateSwitched = alternateSwitched;
    }

    public String getAlternateProductName() {
        return alternateProductName;
    }

    public void setAlternateProductName(String alternateProductName) {
        this.alternateProductName = alternateProductName;
    }

    public BigDecimal getAlternateProductMrp() {
        return alternateProductMrp;
    }

    public void setAlternateProductMrp(BigDecimal alternateProductMrp) {
        this.alternateProductMrp = alternateProductMrp;
    }

    public BigDecimal getAlternateProductLineValue() {
        return alternateProductLineValue;
    }

    public void setAlternateProductLineValue(BigDecimal alternateProductLineValue) {
        this.alternateProductLineValue = alternateProductLineValue;
    }

    public boolean isReturnable() {
        return isReturnable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getDisplayFrom() {
        return displayFrom;
    }

    public void setDisplayFrom(String displayFrom) {
        this.displayFrom = displayFrom;
    }

    public String getDisplayTo() {
        return displayTo;
    }

    public void setDisplayTo(String displayTo) {
        this.displayTo = displayTo;
    }

    public String getLinkpage() {
        return linkpage;
    }

    public void setLinkpage(String linkpage) {
        this.linkpage = linkpage;
    }

    public String getDiscountUpto() {
        return discountUpto;
    }

    public void setDiscountUpto(String discountUpto) {
        this.discountUpto = discountUpto;
    }

    public String getNotAddedToCartMessage() {
        return notAddedToCartMessage;
    }

    public void setNotAddedToCartMessage(String notAddedToCartMessage) {
        this.notAddedToCartMessage = notAddedToCartMessage;
    }

    public int getAddToCartVisibility() {
        return addToCartVisibility;
    }

    public void setAddToCartVisibility(int addToCartVisibility) {
        this.addToCartVisibility = addToCartVisibility;
    }

    public int getQuantityPickerVisibility() {
        return quantityPickerVisibility;
    }

    public void setQuantityPickerVisibility(int quantityPickerVisibility) {
        this.quantityPickerVisibility = quantityPickerVisibility;
    }

    public boolean isNotAbleToAddToCartStatus() {
        return isNotAbleToAddToCartStatus;
    }

    public void setNotAbleToAddToCartStatus(boolean notAbleToAddToCartStatus) {
        isNotAbleToAddToCartStatus = notAbleToAddToCartStatus;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<Variant> getProductVariant() {
        return productVariant;
    }

    public void setProductVariant(List<Variant> productVariant) {
        this.productVariant = productVariant;
    }

    public String getGenericDosage() {
        return genericDosage;
    }

    public void setGenericDosage(String genericDosage) {
        this.genericDosage = genericDosage;
    }

    public String getPerTabletCost() {
        return perTabletCost;
    }

    public void setPerTabletCost(String perTabletCost) {
        this.perTabletCost = perTabletCost;
    }

    public String getYouSave() {
        return youSave;
    }

    public void setYouSave(String youSave) {
        this.youSave = youSave;
    }

    public String getCheaper() {
        return cheaper;
    }

    public void setCheaper(String cheaper) {
        this.cheaper = cheaper;
    }
}
