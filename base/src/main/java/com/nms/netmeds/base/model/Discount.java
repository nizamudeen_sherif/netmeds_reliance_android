package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Discount implements Serializable {

    @SerializedName("offer_limit")
    private int offer_limit;
    @SerializedName("chat_offer_value")
    private int chat_offer_value;
    @SerializedName("general_offer_value")
    private int general_offer_value;

    public int getOffer_limit() {
        return offer_limit;
    }

    public void setOffer_limit(int offer_limit) {
        this.offer_limit = offer_limit;
    }

    public int getChat_offer_value() {
        return chat_offer_value;
    }

    public void setChat_offer_value(int chat_offer_value) {
        this.chat_offer_value = chat_offer_value;
    }

    public int getGeneral_offer_value() {
        return general_offer_value;
    }

    public void setGeneral_offer_value(int general_offer_value) {
        this.general_offer_value = general_offer_value;
    }


}
