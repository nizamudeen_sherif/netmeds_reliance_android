package com.nms.netmeds.base.model;

public class CartSubstitutionUpdatedCart {

    private String primaryCartDrugSkuId;
    private String substitutionCartDrugSkuId;
    private String primaryCartDrugName;
    private String substitutionCartDrugName;
    private int primaryCartDrugMaximumQuantity;
    private int substitutionCartDrugMaximumQuantity;
    private int primaryCartDrugPurchasedQuantity;
    private int substitutionCartDrugPurchasedQuantity;
    private double substitutionCartDrugTotalPrice;

    public String getPrimaryCartDrugSkuId() {
        return primaryCartDrugSkuId;
    }

    public void setPrimaryCartDrugSkuId(String primaryCartDrugSkuId) {
        this.primaryCartDrugSkuId = primaryCartDrugSkuId;
    }

    public String getSubstitutionCartDrugSkuId() {
        return substitutionCartDrugSkuId;
    }

    public void setSubstitutionCartDrugSkuId(String substitutionCartDrugSkuId) {
        this.substitutionCartDrugSkuId = substitutionCartDrugSkuId;
    }

    public String getPrimaryCartDrugName() {
        return primaryCartDrugName;
    }

    public void setPrimaryCartDrugName(String primaryCartDrugName) {
        this.primaryCartDrugName = primaryCartDrugName;
    }

    public String getSubstitutionCartDrugName() {
        return substitutionCartDrugName;
    }

    public void setSubstitutionCartDrugName(String substitutionCartDrugName) {
        this.substitutionCartDrugName = substitutionCartDrugName;
    }

    public int getPrimaryCartDrugMaximumQuantity() {
        return primaryCartDrugMaximumQuantity;
    }

    public void setPrimaryCartDrugMaximumQuantity(int primaryCartDrugMaximumQuantity) {
        this.primaryCartDrugMaximumQuantity = primaryCartDrugMaximumQuantity;
    }

    public int getSubstitutionCartDrugMaximumQuantity() {
        return substitutionCartDrugMaximumQuantity;
    }

    public void setSubstitutionCartDrugMaximumQuantity(int substitutionCartDrugMaximumQuantity) {
        this.substitutionCartDrugMaximumQuantity = substitutionCartDrugMaximumQuantity;
    }

    public int getPrimaryCartDrugPurchasedQuantity() {
        return primaryCartDrugPurchasedQuantity;
    }

    public void setPrimaryCartDrugPurchasedQuantity(int primaryCartDrugPurchasedQuantity) {
        this.primaryCartDrugPurchasedQuantity = primaryCartDrugPurchasedQuantity;
    }

    public int getSubstitutionCartDrugPurchasedQuantity() {
        return substitutionCartDrugPurchasedQuantity;
    }

    public void setSubstitutionCartDrugPurchasedQuantity(int substitutionCartDrugPurchasedQuantity) {
        this.substitutionCartDrugPurchasedQuantity = substitutionCartDrugPurchasedQuantity;
    }

    public double getSubstitutionCartDrugTotalPrice() {
        return substitutionCartDrugTotalPrice;
    }

    public void setSubstitutionCartDrugTotalPrice(double substitutionCartDrugTotalPrice) {
        this.substitutionCartDrugTotalPrice = substitutionCartDrugTotalPrice;
    }
}
