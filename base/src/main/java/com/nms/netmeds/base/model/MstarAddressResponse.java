package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MstarAddressResponse implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private MstarAddressResult result;
    @SerializedName("response_time")
    private String responseTime;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MstarAddressResult getResult() {
        return result;
    }

    public void setResult(MstarAddressResult result) {
        this.result = result;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }
}
