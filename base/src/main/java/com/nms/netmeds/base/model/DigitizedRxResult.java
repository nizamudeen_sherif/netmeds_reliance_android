package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DigitizedRxResult {
    @SerializedName("rxUnmatchDrugList")
    private String rxUnmatchDrugList;
    @SerializedName("rxDigitizedCount")
    private String rxDigitizedCount;
    @SerializedName("rxNotDigitizedCount")
    private String rxNotDigitizedCount;
    @SerializedName("pastOneWeekUnDigitizedRxCount")
    private Integer pastOneWeekUnDigitizedRxCount;
    @SerializedName("rxDigitizedList")
    private List<DigitizedRxList> rxDigitizedList = null;
    @SerializedName("pastOneWeekUnDigitizedRxList")
    private PastOneWeekUnDigitizedRxList pastOneWeekUnDigitizedRxList;
    @SerializedName("rxNotDigitizedList")
    private ArrayList<String> rxNotDigitizedList;


    public String getRxUnmatchDrugList() {
        return rxUnmatchDrugList;
    }

    public void setRxUnmatchDrugList(String rxUnmatchDrugList) {
        this.rxUnmatchDrugList = rxUnmatchDrugList;
    }

    public String getRxDigitizedCount() {
        return rxDigitizedCount;
    }

    public void setRxDigitizedCount(String rxDigitizedCount) {
        this.rxDigitizedCount = rxDigitizedCount;
    }

    public String getRxNotDigitizedCount() {
        return rxNotDigitizedCount;
    }

    public void setRxNotDigitizedCount(String rxNotDigitizedCount) {
        this.rxNotDigitizedCount = rxNotDigitizedCount;
    }

    public Integer getPastOneWeekUnDigitizedRxCount() {
        return pastOneWeekUnDigitizedRxCount;
    }

    public void setPastOneWeekUnDigitizedRxCount(Integer pastOneWeekUnDigitizedRxCount) {
        this.pastOneWeekUnDigitizedRxCount = pastOneWeekUnDigitizedRxCount;
    }

    public List<DigitizedRxList> getRxDigitizedList() {
        return rxDigitizedList;
    }

    public void setRxDigitizedList(List<DigitizedRxList> rxDigitizedList) {
        this.rxDigitizedList = rxDigitizedList;
    }

    public PastOneWeekUnDigitizedRxList getPastOneWeekUnDigitizedRxList() {
        return pastOneWeekUnDigitizedRxList;
    }

    public void setPastOneWeekUnDigitizedRxList(PastOneWeekUnDigitizedRxList pastOneWeekUnDigitizedRxList) {
        this.pastOneWeekUnDigitizedRxList = pastOneWeekUnDigitizedRxList;
    }

    public ArrayList<String> getRxNotDigitizedList() {
        return rxNotDigitizedList;
    }

    public void setRxNotDigitizedList(ArrayList<String> rxNotDigitizedList) {
        this.rxNotDigitizedList = rxNotDigitizedList;
    }
}
