package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarExploreCategories {

    @SerializedName("position")
    private int position;
    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("list")
    private List<MstarCategoryDetails> categoryDetails;
    @SerializedName("viewAll")
    private String viewAll;
    @SerializedName("tagline")
    private String tagline;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public List<MstarCategoryDetails> getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(List<MstarCategoryDetails> categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    public String getViewAll() {
        return viewAll;
    }

    public void setViewAll(String viewAll) {
        this.viewAll = viewAll;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
