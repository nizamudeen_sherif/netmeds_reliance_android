package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ReviveOrderRequest {

    @SerializedName("param")
    private String param;

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
