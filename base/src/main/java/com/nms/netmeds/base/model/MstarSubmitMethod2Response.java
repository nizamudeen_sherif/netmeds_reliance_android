package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarSubmitMethod2Response {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private MstarSubmitMethod2 result;
    @SerializedName("reason")
    private MstarBasicResponseFailureReasonTemplateModel reason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MstarSubmitMethod2 getResult() {
        return result;
    }

    public void setResult(MstarSubmitMethod2 result) {
        this.result = result;
    }

    public MstarBasicResponseFailureReasonTemplateModel getReason() {
        return reason;
    }

    public void setReason(MstarBasicResponseFailureReasonTemplateModel reason) {
        this.reason = reason;
    }
}
