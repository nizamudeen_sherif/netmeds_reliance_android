package com.nms.netmeds.base.retrofit;

public class APIPathConstant {

    public static final String DOWNLOAD_PRESCRIPTION = "cart/download_prescription/";
    static final String REFILL_SUBSCRIPTION = "api/refillsubscriptionorder";
    static final String SUBSCRIPTION_LOG = "netmedsapp-paytmsubscribtionlog/paytmsubscribtionlog";
    static final String REMOVE_VOUCHER = "giftcard/remove";
    static final String CHECK_PRIME_USER = "api/checkprimeuser";

    static final String CART_SUBSTITUTION = "rest/action/drug_master/get_excluded_ba";
    static final String CART_SUBSTITUTION_UPDATE_CART = "api/updatecartitems";

    //MORNING STAR
    public static final String MSTAR_CONFIG_URL_PATH = "config/url_paths";
    public static final String MSTAR_CREATE_CART = "cart/create";
    public static final String MSTAR_ADD_PRODUCT_TO_CART = "cart/add_item";
    public static final String MSTAR_ADD_MULTIPLE_PRODUCTS = "cart/add_multiple_items";
    public static final String MSTAR_REMOVE_PRODUCT_FROM_CART = "cart/remove_item";
    public static final String MSTAR_GET_CART_DETAILS = "cart/get";
    public static final String MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART = "cart/get_alternate_products";
    public static final String MSTAR_SWITCH_ALTERNATE_PRODUCT = "cart/switch_alternate_products";
    public static final String MSTAR_REVERT_ORGINAL_CART = "cart/revert_alternate_products";
    public static final String MSTAR_INITIATE_CHECKOUT = "cart/initiate_checkout";
    public static final String MSTAR_CONTINUE_SHOPPING = "cart/continue_shopping";
    public static final String MSTAR_APPLY_PROMO_CODE = "cart/apply_coupon";
    public static final String MSTAR_UN_APPLY_PROMO_CODE = "cart/unapply_coupon";
    public static final String MSTAR_GET_ALL_ADDRESS = "address/get/all";
    public static final String MSTAR_DELETE_ADDRESS = "address/del/{address_id}";
    public static final String MSTAR_SET_M2_BILLING_ADDRESS = "cart/set_address/billing";
    public static final String MSTAR_SET_M2_SHIPPING_ADDRESS = "cart/set_address/shipping";
    public static final String MSTAR_SET_PREFERRED_BILLING_ADDRESS = "entity/customer/set_preferred_billing_address/{addr_id}";
    public static final String MSTAR_SET_PREFERRED_SHIPPING_ADDRESS = "entity/customer/set_preferred_shipping_address/{addr_id}";
    public static final String MSTAR_CUSTOMER_DETAILS = "entity/customer/get_details";
    public static final String MSTAR_SINGLE_ADDRESS_DETAILS = "address/get/{address_id}";
    public static final String MSTAR_NEW_ADDRESS = "address";
    public static final String MSTAR_UPDATE_ADDRESS_DETAILS = "address/update";
    public static final String MSTAR_PRODUCT_DETAILS = "catalog/product/get/{product_id}";
    public static final String MSTAR_PRODUCT_DETAILS_FOR_ID_LIST = "catalog/product/get_list";
    public static final String MSTAR_GET_CATEGORY_DETAILS = "catalog/category/get_details/{category_id}";
    public static final String MSTAR_GET_MANUFACTURERE_DETAILS = "catalog/manufacturer/get_details/{mfgr_id}";
    public static final String MSTAR_SENT_OTP = "session/initiate/using_mobileno_n_otp";
    public static final String MSTAR_VERIFY_OTP = "session/complete/using_mobileno_n_otp";
    public static final String MSTAR_PRIME_PRODUCTS = "catalog/product/get_prime_list";
    public static final String MSTAR_APPLY_VOUCHER = "cart/apply_voucher";
    public static final String MSTAR_UNAPPLY_VOUCHER = "cart/unapply_voucher";
    public static final String MSTAR_BRAND_PRODUCTLIST = "catalog/brand/get_details/{brand_id}";
    public static final String MSTAR_CLEAR_CART = "cart/clear_universal_cart";
    public static final String MSTAR_LOGIN = "session/create/using_userid_n_passwd";
    public static final String MSTAR_INITIATE_CHANGE_PASSWORD = "session/initiate/change_password";
    public static final String MSTAR_COMPLETE_CHANGE_PASSWORD = "session/complete/change_password";
    public static final String MSTAR_RESEND_SAME_OTP = "session/resend/same_otp";
    public static final String MSTAR_GET_PAYMENT_WALLET_BALANCE = "cart/get_wallet_balances";
    public static final String MSTAR_UPDATE_CUSTOMER_DETAILS = "update_customer";
    //Upload Prescription
    public static final String MSTAR_UPLOAD_PRESCRIPTION = "cart/upload_prescriptions";
    public static final String MSTAR_DETACH_PRESCRIPTION = "cart/detach_prescriptions";
    public static final String MSTAR_DOWNLOAD_PRESCRIPTION = "cart/download_prescription/";
    //Method 2 cart API
    public static final String MSTAR_GET_METHOD_2_CART = "cart/get_method2_carts";
    public static final String MSTAR_SUBMIT_METHOD_2 = "cart/submit_method2_order";
    //Create Guest Session
    public static final String MSTAR_CREATE_GUEST_SESSION = "session/create/guest";
    public static final String MSTAR_GET_USER_STATUS = "id/details/{userid}";
    public static final String MSTAR_CUSTOMER_REGISTRATION = "register";
    public static final String MSTAR_GET_REORDER_CART = "cart/get_reorder_cart?";
    public static final String MSTAR_GET_FILLED_METHOD2_CART = "cart/get_filled_method2_cart?";
    public static final String MSTAR_GET_RETRY_CART = "cart/get_retry_cart?";
    public static final String MSTAR_APPLY_WALLET = "cart/apply_wallet";
    public static final String MSTAR_UNAPPLY_WALLET = "cart/unapply_wallet";

    //Subscription
    public static final String MSTAR_CREATE_SUBSCRIPTION = "cart/setup_subscription";
    public static final String MSTAR_SUBSCRIPTION_PAYNOW = "cart/get_subs_issue_cart";
    static final String C_MSTAR_SUBSCRIPTION_ITEM_ORDER_DETAILS = "api/v1/subscription/getsubscriptionitemdetails";
    static final String MSTAR_CREATE_SUBSCRIPTION_CART = "cart/create/subssetup";
    public static final String C_MSTAR_SUBSCRIPTION_TRACK_ORDER = "api/v1/subscription/getsubscriptionordertracking";


    /**
     * CUSTOM API GIVEN BY
     */

    public static final String C_MSTAR_HOME_BANNER = "api/v1/homesection/mainbanner";
    public static final String C_MSTAR_WALLET_DETAILS = "api/v1/wallets/getwalletdetails";
    public static final String C_MSTAR_REFER_AND_EARN = "api/v1/wallets/referandearn";
    public static final String C_MSTAR_ORDER_HEADER = "api/v1/myorders/getmainorders";
    public static final String C_MSTAR_ORDER_DETAILS = "api/v1/myorders/getorderdetails";
    public static final String C_MSTAR_TRACK_ORDER = "api/v1/myorders/getordertracking";
    public static final String C_MSTAR_CANCEL_ORDER = "api/v1/myorders/ordercancel";
    public static final String C_MSTAR_ALL_OFFERS = "api/v1/offers/getalloffers";
    public static final String C_MSTAR_GATEWAY_OFFERS = "api/v1/offers/gatewayoffers";
    public static final String C_MSTAR_M2_ORDER_DETAILS = "api/v1/myorders/getmethodtwoorderdetails";
    public static final String C_MSTAR_WELLNESSPAGE_DETAILS = "api/v1/wellness/get";
    public static final String C_MSTAR_WELLNESSPAGE_DETAILS_V2 = "api/v2/wellness/get";
    public static final String C_MSTAR_HIGH_RANGE_PRODUCT = "api/v1/emailservice/highrangeproduct";
    public static final String C_MSTAR_REQUEST_NEW_PRODUCT = "api/v1/emailservice/requestnewproduct";
    public static final String C_MSTAR_NOTIFY_ME = "api/v1/emailservice/notifyme";
    public static final String C_MSTAR_HEALTH_ARTICLE = "api/v1/homesection/healtharticle";
    public static final String C_MSTAR_MY_PRESCRIPTION = "api/v1/prescription/myprescription";
    public static final String C_MSTAR_PROMOTION_BANNER = "api/v1/homesection/promotionalbanner";
    public static final String C_MSTAR_HEALTHCONCERN_AND_WELLNESS_PRODUCTS = "api/v1/homesection/wellnessproducts";
    public static final String C_MSTAR_COUPON_LIST = "api/v1/offers/couponlist";
    public static final String C_MSTAR_NEED_HELP = "api/v1/emailservice/needhelp";
    public static final String C_MSTAR_CATLOG_BANNERS = "api/v1/catalog/banners?";
    public static final String C_MSTAR_GET_CITY_STATE_FROM_PINCODE = "nmsd/rest/v2/pin/{pin}";
    public static final String C_MSTAR_SOCIAL_LOGIN = "api/v1/login/sociallogin";
    public static final String C_MSTAR_GET_ALTERNATE_PRODUCT = "api/v1/catalog/viewalternate";
    public static final String C_MSTAR_PAST_MEDICINES = "api/v1/customer/buyagain";
    public static final String C_MSTAR_GENERIC_MEDICINE_DETAIL = "api/v1/catalog/genericmedicine?";
    public static final String C_MSTAR_RETRIEVE_BY_URL = "catalog/get_details/url/{url}";
    public static final String C_MSTAR_RATE_ORDERS = "api/v1/myorders/ratingreview";
    public static final String C_MEDICINE_HOME_BANNER = "api/v1/homesection/medicine";


    //Upload Prescription
    public static final String C_MSTAR_DIGITALIZED_RX = "api/v1/prescription/getdrescriptiondigitizedrx";
    public static final String C_MSTAR_PAST_RX = "api/v1/prescription/recentprescription";
    //Config
    public static final String C_MSTAR_CONFIGURATION = "api/v1/appconfig/get";
    public static final String C_MSTAR_GET_LINKED_PAYMENT = "api/v1/gateway/getlinkedpayments";

    public static final String DE_LINK = "oauth2/accessToken/{linkToken}";

    //PAYMENT
    public static final String MSTAR_COD_PAYMENT = "cart/cod_payment_processed";
    public static final String MSTAR_GENERATE_ORDER_ID = "cart/get_order_id";
    public static final String C_MSTAR_PAYMENT_LOG = "api/v1/log/paymentlog";
    public static final String C_MSTAR_COD_ELIGIBLE_CHECK = "api/v1/customer/checkcodeligible";

}
