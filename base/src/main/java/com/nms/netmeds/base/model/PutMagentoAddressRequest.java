package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.SetMagentoCustomer;

public class PutMagentoAddressRequest {
    @SerializedName("customer")
    private SetMagentoCustomer customer;

    public SetMagentoCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(SetMagentoCustomer customer) {
        this.customer = customer;
    }
}
