package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.PaymentGatewayResult;
import com.nms.netmeds.base.model.ServiceStatus;

public class PaymentGatewayResponse {
    @SerializedName("updatedOn")
    private int updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private PaymentGatewayResult result;

    public int getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(int updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return this.serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public PaymentGatewayResult getResult() {
        return this.result;
    }

    public void setResult(PaymentGatewayResult result) {
        this.result = result;
    }
}
