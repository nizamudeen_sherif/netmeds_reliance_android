package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ClickPostTrackStatusResponse extends BaseResponse{
    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("clickpost_status_code")
    private String clickpostStatusCode;
    @SerializedName("location")
    private String location;
    @SerializedName("clickpost_status_description")
    private String clickpostStatusDescription;
    @SerializedName("clickpost_status_bucket")
    private String clickpostStatusBucket;
    @SerializedName("clickpost_status_bucket_description")
    private String clickpostStatusBucketDescription;
    @SerializedName("remark")
    private String remark;
    @SerializedName("status")
    private String status;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getClickpostStatusCode() {
        return clickpostStatusCode;
    }

    public void setClickpostStatusCode(String clickpostStatusCode) {
        this.clickpostStatusCode = clickpostStatusCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getClickpostStatusDescription() {
        return clickpostStatusDescription;
    }

    public void setClickpostStatusDescription(String clickpostStatusDescription) {
        this.clickpostStatusDescription = clickpostStatusDescription;
    }

    public String getClickpostStatusBucket() {
        return clickpostStatusBucket;
    }

    public void setClickpostStatusBucket(String clickpostStatusBucket) {
        this.clickpostStatusBucket = clickpostStatusBucket;
    }

    public String getClickpostStatusBucketDescription() {
        return clickpostStatusBucketDescription;
    }

    public void setClickpostStatusBucketDescription(String clickpostStatusBucketDescription) {
        this.clickpostStatusBucketDescription = clickpostStatusBucketDescription;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
