package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class DrugDetail {

    @SerializedName(value = "drugCode", alternate = {"Drug_Code", "sku"})
    private int drugCode;
    @SerializedName(value = "genericName", alternate = {"Generic_Name"})
    private String genericName;
    @SerializedName(value = "brandName", alternate = {"Us_Brand_Name", "drugName"})
    private String brandName;
    @SerializedName(value = "productStauts", alternate = {"Product_Stauts"})
    private String productStauts;
    @SerializedName(value = "purchaseQuantity", alternate = {"Purchase_Qty", "unShipQuantity", "quantity"})
    private int purchaseQuantity;
    @SerializedName(value = "purchasePrice", alternate = {"Purchase_Price", "refundValue", "price"})
    private Object purchasePrice;
    @SerializedName(value = "productDiscount", alternate = {"Prod_Discount"})
    private double productDiscount;
    @SerializedName("discount")
    private double discount;
    @SerializedName(value = "minimumQuantity", alternate = {"Min_Qty"})
    private long minimumQuantity;
    @SerializedName(value = "pageType", alternate = {"Page_Type"})
    private String pageType;
    @SerializedName(value = "prescriptionNeeded", alternate = {"Prescription_Needed"})
    private String prescriptionNeeded;
    @SerializedName("alternativeDrugCode")
    private long alternativeDrugCode;
    @SerializedName("additionalShippingChargers")
    private String additionalShippingChargers;
    @SerializedName(value = "subscriptionDiscountStatus", alternate = {"subs_discount_status"})
    private String subscriptionDiscountStatus;
    @SerializedName(value = "prodImage", alternate = {"prod_image"})
    private String prodImage;
    @SerializedName("coldStorage")
    private Boolean coldStorage;
    @SerializedName("drugSchedule")
    private String drugSchedule;
    @SerializedName("drugType")
    private String drugType;
    @SerializedName("rxRequired")
    private String rxRequired;

    public int getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(int drugCode) {
        this.drugCode = drugCode;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductStauts() {
        return productStauts;
    }

    public void setProductStauts(String productStauts) {
        this.productStauts = productStauts;
    }

    public int getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void setPurchaseQuantity(int purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

    public Object getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Object purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(double productDiscount) {
        this.productDiscount = productDiscount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public long getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(long minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPrescriptionNeeded() {
        return prescriptionNeeded;
    }

    public void setPrescriptionNeeded(String prescriptionNeeded) {
        this.prescriptionNeeded = prescriptionNeeded;
    }

    public long getAlternativeDrugCode() {
        return alternativeDrugCode;
    }

    public void setAlternativeDrugCode(long alternativeDrugCode) {
        this.alternativeDrugCode = alternativeDrugCode;
    }

    public String getAdditionalShippingChargers() {
        return additionalShippingChargers;
    }

    public void setAdditionalShippingChargers(String additionalShippingChargers) {
        this.additionalShippingChargers = additionalShippingChargers;
    }

    public String getSubscriptionDiscountStatus() {
        return subscriptionDiscountStatus;
    }

    public void setSubscriptionDiscountStatus(String subscriptionDiscountStatus) {
        this.subscriptionDiscountStatus = subscriptionDiscountStatus;
    }

    public String getProdImage() {
        return prodImage;
    }

    public void setProdImage(String prodImage) {
        this.prodImage = prodImage;
    }

    public Boolean getColdStorage() {
        return coldStorage;
    }

    public void setColdStorage(Boolean coldStorage) {
        this.coldStorage = coldStorage;
    }

    public String getDrugSchedule() {
        return drugSchedule;
    }

    public void setDrugSchedule(String drugSchedule) {
        this.drugSchedule = drugSchedule;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getRxRequired() {
        return rxRequired;
    }

    public void setRxRequired(String rxRequired) {
        this.rxRequired = rxRequired;
    }
}