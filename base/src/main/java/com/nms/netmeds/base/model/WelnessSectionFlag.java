package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class WelnessSectionFlag {
    @SerializedName("category")
    private boolean category;
    @SerializedName("popularProducts")
    private boolean popularProducts;
    @SerializedName("fitness")
    private boolean fitness;
    @SerializedName("familyCare")
    private boolean familyCare;
    @SerializedName("personalCare")
    private boolean personalCare;
    @SerializedName("brandSale")
    private boolean brandSale;
    @SerializedName("prodBrandSale")
    private boolean prodBrandSale;
    @SerializedName("promoOffer")
    private boolean promoOffer;
    @SerializedName("sexualWellness")
    private boolean sexualWellness;
    @SerializedName("supplimentBanner")
    private boolean supplimentBanner;
    @SerializedName("healthConcern")
    private boolean healthConcern = false;

    public boolean getCategory() {
        return category;
    }

    public void setCategory(boolean category) {
        this.category = category;
    }

    public boolean getPopularProducts() {
        return popularProducts;
    }

    public void setPopularProducts(boolean popularProducts) {
        this.popularProducts = popularProducts;
    }

    public boolean getFitness() {
        return fitness;
    }

    public void setFitness(boolean fitness) {
        this.fitness = fitness;
    }

    public boolean getFamilyCare() {
        return familyCare;
    }

    public void setFamilyCare(boolean familyCare) {
        this.familyCare = familyCare;
    }

    public boolean getPersonalCare() {
        return personalCare;
    }

    public void setPersonalCare(boolean personalCare) {
        this.personalCare = personalCare;
    }

    public boolean getBrandSale() {
        return brandSale;
    }

    public void setBrandSale(boolean brandSale) {
        this.brandSale = brandSale;
    }

    public boolean getProdBrandSale() {
        return prodBrandSale;
    }

    public void setProdBrandSale(boolean prodBrandSale) {
        this.prodBrandSale = prodBrandSale;
    }

    public boolean getPromoOffer() {
        return promoOffer;
    }

    public void setPromoOffer(boolean promoOffer) {
        this.promoOffer = promoOffer;
    }

    public boolean getSexualWellness() {
        return sexualWellness;
    }

    public void setSexualWellness(boolean sexualWellness) {
        this.sexualWellness = sexualWellness;
    }

    public boolean getSupplimentBanner() {
        return supplimentBanner;
    }

    public void setSupplimentBanner(boolean supplimentBanner) {
        this.supplimentBanner = supplimentBanner;
    }

    public boolean getHealthConcern() {
        return healthConcern;
    }

    public void setHealthConcern(boolean healthConcern) {
        this.healthConcern = healthConcern;
    }
}
