package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarFilter {

    @SerializedName("amount_ranges")
    private MstarAmountRanges amountRanges;

    @SerializedName("facets")
    private MstarFacets facets;

    public MstarAmountRanges getAmountRanges() {
        return amountRanges;
    }

    public void setAmountRanges(MstarAmountRanges amountRanges) {
        this.amountRanges = amountRanges;
    }

    public MstarFacets getFacets() {
        return facets;
    }

    public void setFacets(MstarFacets facets) {
        this.facets = facets;
    }
}
