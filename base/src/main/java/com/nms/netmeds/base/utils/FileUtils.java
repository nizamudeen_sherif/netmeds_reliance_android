package com.nms.netmeds.base.utils;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;

public class FileUtils {
    public static final String TEMP_DIRECTORY_PATH = "Capture.jpg";
    public static final String PROVIDER = ".provider";
    public static final String IMAGE_JPEG = "image/jpeg";
    public static final String GALLERY_IMAGE_TYPE = "image/*";
    public static final String MULTI_PART_TYPE = "multipart/form-data";
    public static final String APPLICATION_ID = "com.NetmedsMarketplace.Netmeds";
    public static final String MIME_TYPE_DOC = "application/msword";
    public static final String MIME_TYPE_PDF = "application/pdf";
    public static final Long MAX_IMAGE_SIZE_IN_BYTES = (long) 10 * 1024 * 1024;
    public static final Long MAX_DOCUMENT_SIZE_IN_BYTES = (long) 8 * 1024 * 1024;
    private static final String CONTENT = "content";
    private static final String FILE = "file";
    private static final String DATA = "_data";
    private static final String TAG = "FileUtils";
    private static final boolean DEBUG = false;
    private static final String DOCUMENTS_DIR = "documents";
    private static String[] validPictureFileExtension = {
            "jpg",
            "jpeg",
            "png"
    };
    private static String[] validDocumentFileExtension = {
            "doc",
            "docx",
            "pdf"
    };

    public static String getTempDirectoryPath(Context context) {
        File cache;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            cache = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/Android/data/" + context.getPackageName() + "/cache/");
        } else {
            cache = context.getCacheDir();
        }

        cache.mkdirs();
        return cache.getAbsolutePath();
    }

    @SuppressLint("NewApi")
    public static String getPath(Context context, Uri uri) {
        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (context != null && isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);

                if (id != null && id.startsWith("raw:")) {
                    return id.substring(4);
                }

                String[] contentUriPrefixesToTry = new String[]{
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads",
                        "content://downloads/all_downloads"
                };

                for (String contentUriPrefix : contentUriPrefixesToTry) {
                    try {
                        Uri contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(id));
                        String path = getDataColumn(context, contentUri);
                        if (path != null) {
                            return path;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                String fileName = getFileName(context, uri);
                File cacheDir = getDocumentCacheDir(context);
                File file = generateFileName(fileName, cacheDir);
                String destinationPath = null;
                if (file != null) {
                    destinationPath = file.getAbsolutePath();
                    saveFileFromUri(context, uri, destinationPath);
                }

                return destinationPath;
            } else {
                String filePath = uri.getPath();

                if (FileUtils.CONTENT.equals(uri.getScheme())) {
                    Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Files.FileColumns.DATA}, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        filePath = cursor.getString(0);
                        cursor.close();
                    }
                }

                return filePath;
            }
        } else if (context != null && FileUtils.CONTENT.equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri);
        } else if (FILE.equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static String getDataColumn(Context context, Uri uri) {
        Cursor cursor = null;
        final String column = DATA;
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, null, null,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isValidPictureFileExtension(String filename) {
        boolean result = false;
        int length;
        length = validPictureFileExtension.length;
        for (int i = 0; i < length; i++) {
            if ((filename.toLowerCase()).endsWith(validPictureFileExtension[i])) {
                result = true;
                break;
            }
        }
        return result;
    }

    public static boolean isValidSize(String url, Long uploadSize) {
        File f = new File(url);
        long size = f.length();
        return size <= uploadSize;
    }

    public static boolean isValidDocumentFileExtension(String filename) {
        boolean result = false;
        int length;
        length = validDocumentFileExtension.length;
        for (int i = 0; i < length; i++) {
            if ((filename.toLowerCase()).endsWith(validDocumentFileExtension[i])) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static String getFileName(@NonNull Context context, Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        String filename = null;

        if (mimeType == null) {
            String path = getPath(context, uri);
            if (path == null) {
                filename = getName(uri.toString());
            } else {
                File file = new File(path);
                filename = file.getName();
            }
        } else {
            Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                filename = returnCursor.getString(nameIndex);
                returnCursor.close();
            }
        }

        return filename;
    }

    private static File getDocumentCacheDir(@NonNull Context context) {
        File dir = new File(context.getCacheDir(), DOCUMENTS_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        logDir(context.getCacheDir());
        logDir(dir);

        return dir;
    }

    private static File generateFileName(@Nullable String name, File directory) {
        if (name == null) {
            return null;
        }

        File file = new File(directory, name);

        if (file.exists()) {
            String fileName = name;
            String extension = "";
            int dotIndex = name.lastIndexOf('.');
            if (dotIndex > 0) {
                fileName = name.substring(0, dotIndex);
                extension = name.substring(dotIndex);
            }

            int index = 0;

            while (file.exists()) {
                index++;
                name = fileName + '(' + index + ')' + extension;
                file = new File(directory, name);
            }
        }

        try {
            if (!file.createNewFile()) {
                return null;
            }
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }

        logDir(directory);

        return file;
    }

    private static void logDir(File dir) {
        if (!DEBUG) return;
        Log.d(TAG, "Dir=" + dir);
        File[] files = dir.listFiles();
        for (File file : files) {
            Log.d(TAG, "File=" + file.getPath());
        }
    }

    private static void saveFileFromUri(Context context, Uri uri, String destinationPath) {
        InputStream is = null;
        BufferedOutputStream bos = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            bos = new BufferedOutputStream(new FileOutputStream(destinationPath, false));
            byte[] buf = new byte[1024];
            is.read(buf);
            do {
                bos.write(buf);
            } while (is.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getName(String filename) {
        if (filename == null) {
            return null;
        }
        int index = filename.lastIndexOf('/');
        return filename.substring(index + 1);
    }

    public static String fileToMD5(String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);
            byte[] buffer = new byte[1024];
            MessageDigest digest = MessageDigest.getInstance("MD5");
            int numRead = 0;
            while (numRead != -1) {
                numRead = inputStream.read(buffer);
                if (numRead > 0)
                    digest.update(buffer, 0, numRead);
            }
            byte[] md5Bytes = digest.digest();
            return convertHashToString(md5Bytes);
        } catch (Exception e) {
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String convertHashToString(byte[] md5Bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte md5Byte : md5Bytes) {
            stringBuilder.append(Integer.toString((md5Byte & 0xff) + 0x100, 16).substring(1));
        }
        return stringBuilder.append(System.currentTimeMillis()).toString().toUpperCase();
    }
}
