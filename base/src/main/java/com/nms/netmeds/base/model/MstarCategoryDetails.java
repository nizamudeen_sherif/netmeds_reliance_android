package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MstarCategoryDetails {

    @SerializedName("created_time")
    private String createdTime;

    @SerializedName("updated_time")
    private String updatedTime;

    @SerializedName("is_active")
    private Boolean isActive;

    @SerializedName("level")
    private Integer level;

    @SerializedName("product_count")
    private Integer productCount;

    @SerializedName("json_ext")
    private String jsonExt;

    @SerializedName("description")
    private String description;

    @SerializedName("category_type")
    private String categoryType;

    @SerializedName("instock_product_count")
    private Integer instockProductCount;

    @SerializedName("valid_from_time")
    private String validFromTime;

    @SerializedName("bread_crumbs")
    private List<MStarBreadCrumb> mstarBreadCrumbs;

    @SerializedName("aggregate_product_count")
    private Integer aggregateProductCount;

    @SerializedName("sub_categories")
    private List<MstarSubCategoryResult> subCategoriesResult;

    @SerializedName("valid_till_time")
    private String validTillTime;

    @SerializedName("parent_id")
    private String parentId;

    @SerializedName("masthead_image_path")
    private String mastheadImagePath;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private Integer id;

    @SerializedName("position")
    private Integer position;

    @SerializedName(value = "thumbnail_image_path", alternate = {"image"})
    private String thumbnailImagePath;

    @SerializedName("products")
    private List<MStarProductDetails> productDetailsList;

    @SerializedName("url_path")
    private String urlPath;

    @SerializedName("filters")
    private MstarFilter filters;

    public List<MstarSubCategoryResult> getSubCategoriesResult() {
        return subCategoriesResult;
    }

    public void setSubCategoriesResult(List<MstarSubCategoryResult> subCategoriesResult) {
        this.subCategoriesResult = subCategoriesResult;
    }

    public List<MStarProductDetails> getProductDetailsList() {
        return productDetailsList;
    }

    public void setProductDetailsList(List<MStarProductDetails> productDetailsList) {
        this.productDetailsList = productDetailsList;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public Object getJsonExt() {
        return jsonExt;
    }

    public void setJsonExt(String jsonExt) {
        this.jsonExt = jsonExt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public Integer getInstockProductCount() {
        return instockProductCount;
    }

    public void setInstockProductCount(Integer instockProductCount) {
        this.instockProductCount = instockProductCount;
    }

    public Object getValidFromTime() {
        return validFromTime;
    }

    public void setValidFromTime(String validFromTime) {
        this.validFromTime = validFromTime;
    }

    public List<MStarBreadCrumb> getMstarBreadCrumbs() {
        return mstarBreadCrumbs;
    }

    public void setMstarBreadCrumbs(List<MStarBreadCrumb> mstarBreadCrumbs) {
        this.mstarBreadCrumbs = mstarBreadCrumbs;
    }

    public Integer getAggregateProductCount() {
        return aggregateProductCount;
    }

    public void setAggregateProductCount(Integer aggregateProductCount) {
        this.aggregateProductCount = aggregateProductCount;
    }

    public List<MstarSubCategoryResult> getSubCategories() {
        return subCategoriesResult;
    }

    public void setSubCategories(List<MstarSubCategoryResult> subCategories) {
        this.subCategoriesResult = subCategories;
    }

    public Object getValidTillTime() {
        return validTillTime;
    }

    public void setValidTillTime(String validTillTime) {
        this.validTillTime = validTillTime;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMastheadImagePath() {
        return mastheadImagePath;
    }

    public void setMastheadImagePath(String mastheadImagePath) {
        this.mastheadImagePath = mastheadImagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Object getThumbnailImagePath() {
        return thumbnailImagePath;
    }

    public void setThumbnailImagePath(String thumbnailImagePath) {
        this.thumbnailImagePath = thumbnailImagePath;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public MstarFilter getFilters() {
        return filters;
    }

    public void setFilters(MstarFilter filters) {
        this.filters = filters;
    }

    //For Brand Details

    @SerializedName("has_prescription_products")
    private boolean hasPrescriptionProducts;
    @SerializedName("has_npnfmcg_products")
    private boolean hasNpnfmcgProducts;

    public boolean isHasPrescriptionProducts() {
        return hasPrescriptionProducts;
    }

    public void setHasPrescriptionProducts(boolean hasPrescriptionProducts) {
        this.hasPrescriptionProducts = hasPrescriptionProducts;
    }

    public boolean isHasNpnfmcgProducts() {
        return hasNpnfmcgProducts;
    }

    public void setHasNpnfmcgProducts(boolean hasNpnfmcgProducts) {
        this.hasNpnfmcgProducts = hasNpnfmcgProducts;
    }
}
