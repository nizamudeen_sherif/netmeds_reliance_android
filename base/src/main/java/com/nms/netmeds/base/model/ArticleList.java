package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class ArticleList {
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;
    @SerializedName("image")
    private String imageName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
