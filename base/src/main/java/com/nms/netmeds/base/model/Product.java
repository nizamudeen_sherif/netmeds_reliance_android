package com.nms.netmeds.base.model;

import java.util.ArrayList;
import java.util.List;

public class Product {

    private String name;
    private String skuID;
    private String CategoryName;
    private String price;
    private String actualPrice;
    private String manufacturerName;
    private String productDetail;
    private List<String> imageUrl;
    private int maxQuantity;
    private int minQuantity;
    private int quantity;
    private String drugType;
    private String drugSchedule;
    private String subCategoryName;
    private String genericName;
    private String formulationType;
    private String dosage;
    private ArrayList<String> categoryIdList;
    private String availability;
    private ArrayList<String> subCategoryId;
    private String parentCategoryId;

    public String getDrugSchedule() {
        return drugSchedule;
    }

    public void setDrugSchedule(String drugSchedule) {
        this.drugSchedule = drugSchedule;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkuID() {
        return skuID;
    }

    public void setSkuID(String skuID) {
        this.skuID = skuID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    public List<String> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public int getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(int minQuantity) {
        this.minQuantity = minQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getFormulationType() {
        return formulationType;
    }

    public void setFormulationType(String formulationType) {
        this.formulationType = formulationType;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public ArrayList<String> getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(ArrayList<String> categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public ArrayList<String> getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(ArrayList<String> subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }
}
