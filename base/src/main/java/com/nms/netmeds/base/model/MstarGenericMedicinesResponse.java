package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarGenericMedicinesResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private MstarGenericMedicinesResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MstarGenericMedicinesResult getResult() {
        return result;
    }

    public void setResult(MstarGenericMedicinesResult result) {
        this.result = result;
    }
}
