package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

public class MstarMyPrescription {

    @SerializedName("rxId")
    private String rxId;
    @SerializedName("uploadDate")
    private String uploadDate;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("prescriptionStatus")
    private String prescriptionStatus;
    @SerializedName("orderStatusDesc")
    private String orderStatusDescription;
    @SerializedName("customerId")
    private Integer customerId;
    @SerializedName("drugCode")
    private String drugCodeList;
    @SerializedName("brandName")
    private String brandNameList;
    @SerializedName("drugCodebrandName")
    private String drugCodebrandName;

    public String getRxId() {
        return rxId;
    }

    public void setRxId(String rxId) {
        this.rxId = rxId;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPrescriptionStatus() {
        return prescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
        this.prescriptionStatus = prescriptionStatus;
    }

    public String getOrderStatusDescription() {
        return orderStatusDescription;
    }

    public void setOrderStatusDescription(String orderStatusDescription) {
        this.orderStatusDescription = orderStatusDescription;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getDrugCodeList() {
        return drugCodeList;
    }

    public void setDrugCodeList(String drugCodeList) {
        this.drugCodeList = drugCodeList;
    }

    public String getBrandNameList() {
        return brandNameList;
    }

    public void setBrandNameList(String brandNameList) {
        this.brandNameList = brandNameList;
    }

    public String getDrugCodebrandName() {
        return drugCodebrandName;
    }

    public void setDrugCodebrandName(String drugCodebrandName) {
        this.drugCodebrandName = drugCodebrandName;
    }
}
