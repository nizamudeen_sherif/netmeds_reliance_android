package com.nms.netmeds.base.utils;

public class PermissionConstants {
    public static final int CAMERA_PERMISSION = 200;
    public static final int GALLERY_PERMISSION = 201;
    public static final int ACTION_IMAGE_CAPTURE = 202;
    public static final int ACTION_GALLERY_REQUEST = 203;
    public static final int DOCUMENT_PERMISSION = 204;
    public static final int ACTION_DOCUMENT_REQUEST = 205;
    public static final int DOWNLOAD_PERMISSION = 206;
    public static final int CALL_PHONE = 207;
    public static final int DOWNLOAD_ATTACHMENT = 208;
    public static final int LOCATION_PERMISSION = 209;
}
