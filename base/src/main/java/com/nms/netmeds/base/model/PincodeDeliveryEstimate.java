package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

public class PincodeDeliveryEstimate {
    @SerializedName("formatted")
    private PinCodeFormatted formatted;
    @SerializedName("in_day_counts")
    private PinCodeInDayCounts inDayCounts;
    @SerializedName("in_dates")
    private PinCodeInDates inDates;

    public PinCodeFormatted getFormatted() {
        return formatted;
    }

    public void setFormatted(PinCodeFormatted formatted) {
        this.formatted = formatted;
    }

    public PinCodeInDayCounts getInDayCounts() {
        return inDayCounts;
    }

    public void setInDayCounts(PinCodeInDayCounts inDayCounts) {
        this.inDayCounts = inDayCounts;
    }

    public PinCodeInDates getInDates() {
        return inDates;
    }

    public void setInDates(PinCodeInDates inDates) {
        this.inDates = inDates;
    }
}