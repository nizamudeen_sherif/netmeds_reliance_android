package com.nms.netmeds.base.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartSubstitutionResponse {

    @SerializedName("result")
    private List<CartSubstitutionResponseResult> result;
    @SerializedName("status")
    private String status;

    public List<CartSubstitutionResponseResult> getResult() {
        return result;
    }

    public void setResult(List<CartSubstitutionResponseResult> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
