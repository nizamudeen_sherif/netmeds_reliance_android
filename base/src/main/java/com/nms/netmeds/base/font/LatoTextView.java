package com.nms.netmeds.base.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.nms.netmeds.base.R;

public class LatoTextView extends androidx.appcompat.widget.AppCompatTextView {

    String customFont;

    public LatoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context, attrs);
    }

    public LatoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context, attrs);

    }

    private void style(Context context, AttributeSet attrs) {

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.latoFont);
        int cf = a.getInteger(R.styleable.latoFont_fontName, 0);
        int fontName;
        switch (cf) {
            case 1:
                fontName = R.string.lato_black;
                break;
            case 2:
                fontName = R.string.lato_black_italic;
                break;
            case 3:
                fontName = R.string.lato_bold;
                break;
            case 4:
                fontName = R.string.lato_bold_italic;
                break;
            case 5:
                fontName = R.string.lato_hair_line;
                break;
            case 6:
                fontName = R.string.lato_hair_line_italic;
                break;
            case 7:
                fontName = R.string.lato_italic;
                break;
            case 8:
                fontName = R.string.lato_light;
                break;
            case 9:
                fontName = R.string.lato_light_italic;
                break;
            case 10:
                fontName = R.string.lato_regular;
                break;
            default:
                fontName = R.string.lato_regular;
                break;
        }

        customFont = getResources().getString(fontName);

        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "font/" + customFont + ".ttf");
        setTypeface(tf);
        a.recycle();
    }
}
