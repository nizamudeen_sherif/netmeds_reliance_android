package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MStarAddressModel implements Serializable {

    @SerializedName("pin")
    private String pin;
    @SerializedName("city")
    private String city;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("is_active")
    private boolean is_active;
    @SerializedName("name")
    private String name;
    @SerializedName("mobile_no")
    private String mobileNo;
    @SerializedName("id")
    private int id = -1;
    @SerializedName("state")
    private String state;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("landmark")
    private String landmark;
    @SerializedName("bo_address_id")
    private Long bo_address_id;
    @SerializedName("street")
    private String street;
    private boolean selectedAddress;
    private int itemType = 0;
    transient private boolean isChecked = false;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setSelectedAddress(boolean isAddressSelected) {
        this.selectedAddress = isAddressSelected;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public Long getBo_address_id() {
        return bo_address_id;
    }

    public void setBo_address_id(Long bo_address_id) {
        this.bo_address_id = bo_address_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public boolean getSelectedAddress() {
        return selectedAddress;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
