package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MstarWellnessProducts {

    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("list")
    private ArrayList<MstarBanner> wellnessProducts = null;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public ArrayList<MstarBanner> getWellnessProducts() {
        return wellnessProducts;
    }


}
