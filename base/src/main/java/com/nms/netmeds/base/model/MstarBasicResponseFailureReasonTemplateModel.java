package com.nms.netmeds.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class MstarBasicResponseFailureReasonTemplateModel {

    @SerializedName("reason_eng")
    private String reason_eng;

    @SerializedName("reason_code")
    private String reason_code;

    @SerializedName("additional_info")
    private Map<String, String> additional_info;

    public String getReason_eng() {
        return reason_eng;
    }

    public void setReason_eng(String reason_eng) {
        this.reason_eng = reason_eng;
    }

    public String getReason_code() {
        return reason_code;
    }

    public void setReason_code(String reason_code) {
        this.reason_code = reason_code;
    }

    public Map<String, String> getAdditional_info() {
        return additional_info;
    }

    public void setAdditional_info(Map<String, String> additional_info) {
        this.additional_info = additional_info;
    }
}
