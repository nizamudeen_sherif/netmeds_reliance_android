package com.nms.netmeds.base;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeRouter;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeUtil;
import com.nms.netmeds.base.chrometab.CustomTabActivityHelper;
import com.nms.netmeds.base.chrometab.shared.CustomTabsHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.font.LatoTextView;
import com.nms.netmeds.base.model.AlgoliaCredentialDetail;
import com.nms.netmeds.base.model.ConcernSlides;
import com.nms.netmeds.base.model.ConfigDetails;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.CustomAttribute;
import com.nms.netmeds.base.model.CustomerResponse;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.SlideBanner;
import com.nms.netmeds.base.retrofit.APIPathConstant;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DeepLinkConstant;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.EncryptionUtils;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.VIBRATOR_SERVICE;

public class CommonUtils {

    private static final long VIBRATION_TIME = 100;
    private static final String BROWSER = "browser";
    private static final String OFFERS = "offers";
    private static final String WEBVIEW = "webview";
    private static final String CUSTOM = "custom";
    private static final int PROPERTY_DEFAULT_VALUE = -1;
    private static final List<String> PRODUCT_SCHEDULE_OTC_LIST = new ArrayList<>();

    public static List<String> getPRODUCT_SCHEDULE_OTC_LIST() {
        PRODUCT_SCHEDULE_OTC_LIST.clear();
        PRODUCT_SCHEDULE_OTC_LIST.add(AppConstant.OTC_SCHEDULE_TYPE_F);
        PRODUCT_SCHEDULE_OTC_LIST.add(AppConstant.OTC_SCHEDULE_TYPE_M);
        PRODUCT_SCHEDULE_OTC_LIST.add(AppConstant.OTC_SCHEDULE_TYPE_O);
        PRODUCT_SCHEDULE_OTC_LIST.add(AppConstant.DRUG_TYPE_NP);
        return PRODUCT_SCHEDULE_OTC_LIST;
    }

    public static Typeface getTypeface(Context context, String fontStyle) {
        return Typeface.createFromAsset(context.getAssets(), fontStyle);
    }

    public static void errorWatcher(final EditText editText, final TextInputLayout inputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().toString().length() > 0 && inputLayout.isErrorEnabled()) {
                    inputLayout.setError(null);
                    inputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static String convertToTitleCase(String text) {
        if (!TextUtils.isEmpty(text)) {
            String[] strArray = text.split(" ");
            StringBuilder builder = new StringBuilder();
            for (String s : strArray) {
                if (!TextUtils.isEmpty(s.trim())) {
                    String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                    builder.append(cap).append(" ");
                }
            }
            return builder.toString();
        } else return "";
    }

    public static void downloadPrescription(Context context, String url, String downloadDescription, String subPath, String downloadFormat) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle(context.getResources().getString(R.string.app_name));
        request.setDescription(downloadDescription);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, subPath + downloadFormat);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        assert downloadManager != null;
        downloadManager.enqueue(request);
        Toast.makeText(context, context.getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();
    }

    public static String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder;
        String[] amountSpiltWithDecimal = amount.split("\\.");
        String decimalValue = "";
        if (amountSpiltWithDecimal.length == 2) {
            String value = amountSpiltWithDecimal[0];
            decimalValue = amountSpiltWithDecimal[1];
            stringBuilder = getAmountValue(value);
        } else {
            stringBuilder = getAmountValue(amount);
        }
        return stringBuilder.reverse().toString() + "." + decimalValue;
    }

    private static StringBuilder getAmountValue(String value) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = value.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder;
    }

    public static String getPriceInFormat(double price) {
        return "\u20B9 " + getAmountWithTwoDecimalFormat(price);
    }

    public static String getPriceInFormat(BigDecimal price) {
        return "\u20B9 " + getAmountWithTwoDecimalFormat(price.doubleValue());
    }

    public static String getPriceInFormatWithHyphen(BigDecimal price) {
        return "\u20B9 - " + getAmountWithTwoDecimalFormat(price.doubleValue());
    }

    public static String getPriceInFormatWithHyphen(Double price) {
        return "\u20B9 - " + getAmountWithTwoDecimalFormat(price);
    }

    public static String getAmountWithTwoDecimalFormat(double price) {
        return String.format(Locale.getDefault(), "%.2f", price);
    }

    public static String getPriceInFormat(String price) {
        return "\u20B9 " + getAmountWithTwoDecimalFormat(Double.parseDouble(price.replace(",", "")));
    }

    public static void snapHelper(RecyclerView recyclerView) {
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
    }

    public static void hideKeyboard(Context context, View view) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (view != null && imm != null && imm.isAcceptingText())
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getValidStringWithFirstLetterUpperCase(String str) {
        return !TextUtils.isEmpty(str) ? str.substring(0, 1).toUpperCase() + str.substring(1) : "";
    }

    public static void initiateVibrate(Context context) {
        Vibrator v = ((Vibrator) context.getSystemService(VIBRATOR_SERVICE));
        if (Build.VERSION.SDK_INT >= 26) {
            v.vibrate(VibrationEffect.createOneShot(VIBRATION_TIME, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            v.vibrate(VIBRATION_TIME);
        }
    }

    public static void navigateToScreens(Context context, MstarBanner banner, Activity activity) {
        if (banner != null && banner.getLinktype() != null) {
            switch (banner.getLinktype()) {
                case BROWSER:
                    launchChromeTab(banner.getUrl(), activity);
                    break;
                case OFFERS:
                    Intent offers = new Intent();
                    offers.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    offers.putExtra("OFFER_DETAILS_PAGE_ID", banner.getNewPageId() != null ? banner.getNewPageId() : "");
                    LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_view_offer_details), offers, context);
                    break;
                case WEBVIEW:
                    Intent intent = new Intent();
                    intent.putExtra("WEB_PAGE_URL", banner.getUrl() != null ? banner.getUrl() : "");
                    intent.putExtra(AppConstant.BANNER_TITLE, banner.getBannerTitle() != null ? banner.getBannerTitle() : "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_netmeds_web_view), intent, context);
                    break;
                case CUSTOM:
                    if (!TextUtils.isEmpty(banner.getUrl()) && banner.getUrl().contains(DeepLinkConstant.DTYPE)) {
                        ArrayList<String> pathList = splitPath(banner.getUrl());
                        if (pathList != null && pathList.size() > 0) {
                            Intent homeIntent = new Intent();
                            homeIntent.putStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM, pathList);
                            LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_diagnostic_home), homeIntent, context);
                        }
                        return;
                    }
                    try {
                        UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(Uri.parse(banner.getUrl())), context, true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public static void activityNavigation(Context context, Uri uri) {
        if (uri != null) {
            List<String> pathSegmentList = uri.getPathSegments();
            if (pathSegmentList != null && pathSegmentList.size() > 0) {
                String path = !TextUtils.isEmpty(pathSegmentList.get(0)) ? pathSegmentList.get(0) : "";
                String id = pathSegmentList.size() > 1 && !TextUtils.isEmpty(pathSegmentList.get(1)) ? pathSegmentList.get(1) : "";
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (context.getResources().getString(R.string.route_product_list).equalsIgnoreCase(path)) {
                    if (TextUtils.isDigitsOnly(id)) {
                        String intentKey = pathSegmentList.size() > 2 && !TextUtils.isEmpty(pathSegmentList.get(2)) ? pathSegmentList.get(2).equals(AppConstant.KEY_OTC) || pathSegmentList.get(2).equals(AppConstant.KEY_PRESCRIPTION) ? IntentConstant.CATEGORY_ID : pathSegmentList.get(2).equals(AppConstant.KEY_MANUFACTURER) ? IntentConstant.MANUFACTURER_ID : "" : "";
                        intent.putExtra(intentKey, checkDigitOnly(id));
                        onRouteNodeNavigation(context, context.getResources().getString(R.string.route_product_list), intent);
                    } else {
                        intent.putExtra(IntentConstant.INTENT_FROM_BRAND_URL, AppConstant.BRAND);
                        intent.putExtra(IntentConstant.BRAND_URL, AppConstant.BRANDS_URL + id);
                        intent.putExtra(IntentConstant.BRAND_NAME, id);
                        onRouteNodeNavigation(context, context.getResources().getString(R.string.route_product_list), intent);
                    }
                } else if (context.getResources().getString(R.string.route_product_detail).equalsIgnoreCase(path)) {
                    intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, checkDigitOnly(id));
                    onRouteNodeNavigation(context, context.getResources().getString(R.string.route_product_detail), intent);
                } else if (context.getResources().getString(R.string.route_category_activity).equalsIgnoreCase(path)) {
                    intent.putExtra(IntentConstant.CATEGORY_ID, checkDigitOnly(id));
                    onRouteNodeNavigation(context, context.getResources().getString(R.string.route_category_activity), intent);
                } else if (context.getResources().getString(R.string.route_view_offer_details).equalsIgnoreCase(path)) {
                    intent.putExtra(IntentConstant.OFFER_DETAILS_PAGE_ID, id);
                    onRouteNodeNavigation(context, context.getResources().getString(R.string.route_view_offer_details), intent);
                } else if (context.getResources().getString(R.string.route_search).equalsIgnoreCase(path)) {
                    intent.putExtra(IntentConstant.SEARCH_QUERY_TEXT, id);
                    onRouteNodeNavigation(context, context.getResources().getString(R.string.route_search), intent);
                } else if (DeepLinkConstant.SUBSCRIPTION.equalsIgnoreCase(path)
                        || DeepLinkConstant.WALLET.equalsIgnoreCase(path)
                        || DeepLinkConstant.ORDER_HISTORY.equalsIgnoreCase(path)
                        || DeepLinkConstant.ACCOUNT.equalsIgnoreCase(path)
                        || DeepLinkConstant.HOME.equalsIgnoreCase(path)) {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(path);
                    intent.putExtra(AppConstant.EXTRA_PATH_PARAMS, list);
                    onRouteNodeNavigation(context, context.getResources().getString(R.string.route_navigation_activity), intent);
                } else {
                    onRouteNodeNavigation(context, path, null);
                }
            }
        }
    }

    private static int checkDigitOnly(String id) {
        return TextUtils.isDigitsOnly(id) ? Integer.parseInt(id) : 0;
    }

    private static void onRouteNodeNavigation(Context context, String routeNode, Intent intent) {
        LaunchIntentManager.routeToActivity(routeNode, intent, context);
    }

    public static void wellnessNavigateToScreens(Context context, MstarBanner banner, Activity activity) {
        if (banner != null && banner.getLinktype() != null) {
            switch (banner.getLinktype()) {
                case BROWSER:
                    launchChromeTab(banner.getUrl(), activity);
                    break;
                case OFFERS:
                    Intent offers = new Intent();
                    offers.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    offers.putExtra("OFFER_DETAILS_PAGE_ID", banner.getId() != null ? banner.getId() : "");
                    LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_view_offer_details), offers, context);
                    break;
                case WEBVIEW:
                    Intent intent = new Intent();
                    intent.putExtra("WEB_PAGE_URL", banner.getUrl() != null ? banner.getUrl() : "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_netmeds_web_view), intent, context);
                    break;
                case CUSTOM:
                    activityNavigation(context, Uri.parse(banner.getUrl()));
                    break;
            }
        }
    }

    public static void consultationNavigateToScreens(Context context, ConcernSlides slides, Activity activity) {
        if (slides != null && slides.getLinktype() != null) {
            switch (slides.getLinktype()) {
                case BROWSER:
                    launchChromeTab(slides.getUrl(), activity);
                    break;
                case WEBVIEW:
                    Intent intent = new Intent();
                    intent.putExtra("WEB_PAGE_URL", slides.getUrl() != null ? slides.getUrl() : "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_netmeds_web_view), intent, context);
                    break;
                case CUSTOM:
                    if (!TextUtils.isEmpty(slides.getUrl()) && slides.getUrl().contains(DeepLinkConstant.DTYPE)) {
                        if (splitPath(slides.getUrl()) != null && splitPath(slides.getUrl()).size() > 0) {
                            Intent homeIntent = new Intent();
                            homeIntent.putStringArrayListExtra(DiagnosticConstant.INTENT_KEY_EXTRA_PARAM, splitPath(slides.getUrl()));
                            LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_diagnostic_home), homeIntent, context);
                        }
                        return;
                    }
                    try {
                        UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(Uri.parse(slides.getUrl())), context, true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public static void consultationNavigateToScreens(Context context, SlideBanner slides, Activity activity) {
        if (slides != null && slides.getLinktype() != null) {
            switch (slides.getLinktype()) {
                case BROWSER:
                    launchChromeTab(slides.getLinkpage(), activity);
                    break;
                case WEBVIEW:
                    Intent intent = new Intent();
                    intent.putExtra("WEB_PAGE_URL", slides.getLinkpage() != null ? slides.getLinkpage() : "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_netmeds_web_view), intent, context);
                    break;
                case CUSTOM:
                    try {
                        UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(Uri.parse(slides.getLinkpage())), context, true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * Method to launch Custom Tab
     *
     * @param url String
     */
    public static void launchChromeTab(String url, Activity activity) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(CustomTabActivityHelper.getInstance().getSession());
        builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary)).setShowTitle(true);
        CustomTabsIntent customTabsIntent = builder.build();
        CustomTabsHelper.addKeepAliveExtra(activity, customTabsIntent.intent);
        customTabsIntent.launchUrl(activity, Uri.parse(url));
    }


    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap rotateImage(Context context, Bitmap bitmap, String path) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = 0;
        if (exifInterface != null) {
            orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        }
        Matrix matrix = new Matrix();
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            matrix.postRotate(90);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            matrix.postRotate(180);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            matrix.postRotate(270);
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static void setBoldFace(TextView textView, Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                "font/" + mContext.getString(com.nms.netmeds.base.R.string.lato_bold) + ".ttf");
        textView.setTypeface(tf);
        textView.setTextColor(mContext.getResources().getColor(R.color.colorBlueLight));
    }

    public static void setRegularFace(TextView textView, Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                "font/" + mContext.getString(com.nms.netmeds.base.R.string.lato_regular) + ".ttf");
        textView.setTypeface(tf);
        textView.setTextColor(mContext.getResources().getColor(R.color.colorLightBlueGrey));
    }

    public static void setSelectedTabCustom(TabLayout.Tab selectedTab, Context mContext) {
        LatoTextView tabTitle = (LatoTextView) selectedTab.getCustomView();
        assert tabTitle != null;
        setBoldFace(tabTitle, mContext);
    }

    public static void setUnselectedTabCustom(TabLayout.Tab selectedTab, Context mContext) {
        LatoTextView tabTitle = (LatoTextView) selectedTab.getCustomView();
        assert tabTitle != null;
        setRegularFace(tabTitle, mContext);
    }

    private static ArrayList<String> splitPath(String url) {
        if (TextUtils.isEmpty(url)) return null;
        String[] splitUrls = url.split("/");
        if (splitUrls.length == 0) return null;
        String[] localPath = splitUrls[splitUrls.length - 1].split("\\?");
        if (localPath.length == 0) return null;
        String actualPath = localPath[0];
        String type = localPath[localPath.length - 1].replaceAll(DeepLinkConstant.DTYPE_EQUAL, "");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(actualPath);
        arrayList.add(type);
        return arrayList;
    }

    public static boolean isJSONValid(String response) {
        try {
            new JSONObject(response);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }

    public static boolean isRxRequiredProduct(String productSchedule) {
        return !getPRODUCT_SCHEDULE_OTC_LIST().contains(!TextUtils.isEmpty(productSchedule) ? productSchedule.toUpperCase() : "");
    }

    public static double getDeliveryCharges(BasePreference basePreference) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        String deliveryCharges = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges()) ? configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges() : "0";
        return Double.parseDouble(deliveryCharges);
    }


    public static String getAddressFromObject(MStarAddressModel addressModel) {
        StringBuilder addressFormat = new StringBuilder();
        if (addressModel != null) {
            addressFormat.append(getAddressLines(addressModel.getStreet()));
            addressFormat.append(getAddressLines(addressModel.getLandmark()));
            if (!TextUtils.isEmpty(addressModel.getCity())) {
                addressFormat.append(addressModel.getCity());
                addressFormat.append(", ");
            }
            if (!TextUtils.isEmpty(addressModel.getState())) {
                addressFormat.append(addressModel.getState());
                addressFormat.append(" - ");
            }
            if (!TextUtils.isEmpty(addressModel.getPin())) {
                addressFormat.append(addressModel.getPin());
            }
            if (!TextUtils.isEmpty(addressModel.getMobileNo())) {
                addressFormat.append("\n");
                addressFormat.append("mobile no: +91-" + addressModel.getMobileNo() + ".");
            }
        }
        return String.valueOf(addressFormat);
    }

    private static StringBuilder getAddressLines(String addressText) {
        StringBuilder addressFormat = new StringBuilder();
        if (!TextUtils.isEmpty(addressText)) {
            addressFormat.append(addressText);
            addressFormat.append("\n");
        }
        return addressFormat;
    }

    public static GlideUrl getGlideUrl(String url, Map<String, String> mStarHeaderMap) {

        GlideUrl glideUrl = null;
        if (!TextUtils.isEmpty(url)) {
            Map<String, String> headerMap = mStarHeaderMap;
            String user_id = "";
            String authToken = "";

            for (Map.Entry<String, String> details : headerMap.entrySet()) {

                if (AppConstant.MSTAR_API_HEADER_USER_ID.equalsIgnoreCase(details.getKey())) {
                    user_id = details.getValue();

                }
                if (AppConstant.MSTAR_AUTH_TOKEN.equalsIgnoreCase(details.getKey())) {
                    authToken = details.getValue();
                }

            }


            glideUrl = new GlideUrl(url, new LazyHeaders.Builder()
                    .addHeader(AppConstant.MSTAR_API_HEADER_USER_ID, user_id)
                    .addHeader(AppConstant.MSTAR_AUTH_TOKEN, authToken)
                    .build());
            return glideUrl;
        }

        return null;
    }

    public static boolean isProductAvailableWithStock(MStarProductDetails productDetails) {
        if (productDetails != null && !TextUtils.isEmpty(productDetails.getAvailabilityStatus()) && AppConstant.PRODUCT_AVAILABLE_CODE.equalsIgnoreCase(productDetails.getAvailabilityStatus())) {
            return productDetails.getStockQty() > 0 && productDetails.getMaxQtyInOrder() > 0;
        }
        return productDetails != null && (AppConstant.MSTAR_PRIME_PRODUCT_CODE_12_MON == productDetails.getProductCode() || AppConstant.MSTAR_PRIME_PRODUCT_CODE_6_MON == productDetails.getProductCode());
    }

    public static boolean isPrimeProduct(int productCode, BasePreference basePreference) {
        MstarPrimeProductResult primeProductResult = new Gson().fromJson(basePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
        return primeProductResult != null && !primeProductResult.getPrimeProductCodesList().isEmpty() &&
                primeProductResult.getPrimeProductCodesList().contains(String.valueOf(productCode));
    }

    public static Map<String, String> getFingerPrintHeaderMap(Context context) {
        BasePreference basePreference = BasePreference.getInstance(context);
        Map<String, String> mstarBasicHeaderMap = basePreference.getMstarBasicHeaderMap();
        mstarBasicHeaderMap.put(AppConstant.MSTAR_FINGERPRINT, basePreference.getMstarGoogleAdvertisingId());
        return mstarBasicHeaderMap;
    }

    public static boolean isProductTypeOTC(String productType) {
        return !TextUtils.isEmpty(productType) && (AppConstant.OTC_SCHEDULE_TYPE_F.equalsIgnoreCase(productType) || AppConstant.OTC_SCHEDULE_TYPE_M.equalsIgnoreCase(productType) || AppConstant.OTC_SCHEDULE_TYPE_O.equalsIgnoreCase(productType));
    }

    // Conversion to Bitmap Images from Glide
    public static class BitmapImageAsync extends AsyncTask<Void, Void, String> {

        private int position = 0;
        private String url = "";
        private boolean isM2Method;

        private BitmapImageConversionInterface listener;
        private ArrayList<MStarUploadPrescription> prescriptionsList;
        private BasePreference mBasePreference;


        public BitmapImageAsync(BitmapImageConversionInterface listener, ArrayList<MStarUploadPrescription> prescriptionsList, boolean isM2Method) {
            this.listener = listener;
            this.prescriptionsList = prescriptionsList;
            this.mBasePreference = BasePreference.getInstance(listener.getContext());
            this.isM2Method = isM2Method;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.showProgressBar();

        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!prescriptionsList.isEmpty()) {
                for (MStarUploadPrescription prescription : prescriptionsList) {
                    if (prescription.getBitmapImage() == null) {
                       /* if (prescription.isMethod2()) {
                            url = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + prescription.getUploadedPrescriptionId();
                        } else {
                            url = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + prescription.getUploadedPrescriptionId();
                        }*/
                        url = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION + (prescription.getUploadedPrescriptionId() != null ? prescription.getUploadedPrescriptionId() : prescription.getDigitalizedPrescriptionId());

                        // CREATE COMMON METHOD
                        GlideUrl glideUrl = getGlideUrl(url, mBasePreference.getMstarBasicHeaderMap());
                        position = prescriptionsList.indexOf(prescription);


                        Bitmap imageBitmap = null;
                        try {
                            imageBitmap = Glide.with(listener.getContext()).asBitmap().load(glideUrl).into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
                            prescription.setBitmapImage(imageBitmap);
                            prescription.setImageUrl(url);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!prescription.isMethod2()) {

                        }
                        prescriptionsList.set(position, prescription);
                    }
                }
                MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(prescriptionsList);
               /* if (!isM2Method) {
                    MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(prescriptionsList);
                }*/
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.hideProgressBar();
            listener.setBitmapImageAdapter();
        }


    }

    public interface BitmapImageConversionInterface {
        void showProgressBar();

        void hideProgressBar();

        Context getContext();

        void setBitmapImageAdapter();

    }

    public static String convertToProperCase(String value) {
        String[] splitArray = value.split(" ");
        if (splitArray.length > 0) {
            return spitValueWithProperCase(splitArray);
        }
        return value;
    }

    private static String spitValueWithProperCase(String[] splitArray) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String value : splitArray) {
            if (value.length() > 1) {
                stringBuilder.append(value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase()).append(" ");
            } else {
                stringBuilder.append(value.toUpperCase()).append(" ");
            }
        }
        return String.valueOf(stringBuilder);
    }

    /**
     * This method used to get algolia credential from configuration response
     *
     * @param key value
     * @return algolia Api key,app id,index
     */
    public static String getAlgoliaCredentials(BasePreference basePreference, String key) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        String value = "";
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) {
            ConfigDetails result = configurationResponse.getResult().getConfigDetails();
            if (result != null && result.getAlgoliaCredentialDetail() != null) {
                AlgoliaCredentialDetail algoliaCredentialDetail = result.getAlgoliaCredentialDetail();
                switch (key) {
                    case AppConstant.ALGOLIA_HITS:
                        value = TextUtils.isEmpty(algoliaCredentialDetail.getAlgoliaHits()) ? "" : algoliaCredentialDetail.getAlgoliaHits();
                        break;
                    case AppConstant.ALGOLIA_API_KEY:
                        value = TextUtils.isEmpty(algoliaCredentialDetail.getAlgoliaApiKey()) ? "" : EncryptionUtils.decryptAESCBCEncryption(algoliaCredentialDetail.getAlgoliaApiKey());
                        break;
                    case AppConstant.ALGOLIA_INDEX:
                        value = TextUtils.isEmpty(algoliaCredentialDetail.getAlgoliaIndex()) ? "" : algoliaCredentialDetail.getAlgoliaIndex();
                        break;
                    case AppConstant.ALGOLIA_APP_ID:
                        value = TextUtils.isEmpty(algoliaCredentialDetail.getAlgoliaAppId()) ? "" : EncryptionUtils.decryptAESCBCEncryption(algoliaCredentialDetail.getAlgoliaAppId());
                        break;
                }
            }
        }
        return value;
    }

    public static String getRoundedOffBigDecimal(BigDecimal number) {
        DecimalFormat df = new DecimalFormat();

        df.setMaximumFractionDigits(2);

        df.setMinimumFractionDigits(0);

        df.setGroupingUsed(false);

        return df.format(number);

    }

    public static String getAppVersion(Context context) {
        String version = "";
        if (context != null) {
            try {
                PackageManager manager = context.getPackageManager();
                PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
                version = info.versionName + "(" + info.versionCode + ")";
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return version;
    }

    public static CustomerResponse convertMStarObjectToCustomer(MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel) {
        CustomerResponse customerResponse = new CustomerResponse();
        if (mStarBasicResponseTemplateModel == null || mStarBasicResponseTemplateModel.getResult() == null
                || mStarBasicResponseTemplateModel.getResult().getCustomerDetails() == null)
            return customerResponse;
        customerResponse.setId(mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getId());
        customerResponse.setGroupId(PROPERTY_DEFAULT_VALUE);
        customerResponse.setDefaultBilling("" + mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getPreferredBillingAddress());
        customerResponse.setDefaultShipping("" + mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getPreferredShippingAddress());
        customerResponse.setCreatedAt(!TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getCreatedTime())
                ? mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getCreatedTime() : "");
        customerResponse.setUpdatedAt(!TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getUpdatedTime())
                ? mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getUpdatedTime() : "");
        customerResponse.setCreatedIn("");
        customerResponse.setDob(!TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getDateOfBirth())
                ? mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getDateOfBirth() : "");
        customerResponse.setFirstname(!TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getFirstName())
                ? mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getFirstName() : "");
        customerResponse.setLastname(!TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getLastName())
                ? mStarBasicResponseTemplateModel.getResult().getCustomerDetails().getLastName() : "");
        customerResponse.setGender(0);
        customerResponse.setStoreId(PROPERTY_DEFAULT_VALUE);
        customerResponse.setWebsiteId(PROPERTY_DEFAULT_VALUE);
        customerResponse.setAddresses(new ArrayList<MStarAddressModel>());
        customerResponse.setDisableAutoGroupChange(PROPERTY_DEFAULT_VALUE);
        customerResponse.setCustomAttributes(new ArrayList<CustomAttribute>());
        return customerResponse;
    }

    public static String getAppVersionCode(Context context) {
        String version = "";
        if (context != null) {
            try {
                PackageManager manager = context.getPackageManager();
                PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
                return String.valueOf(info.versionCode);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return version;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (TextUtils.isEmpty(html)) return new SpannableString("");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        return Html.fromHtml(html);
    }

    public static boolean isBuildVariantProdRelease() {
        String buildType = BuildConfig.BUILD_TYPE;
        String variant = !TextUtils.isEmpty(buildType) ? buildType.substring(0, 1).toUpperCase() + buildType.substring(1) : "";
        String buildVariant = ConfigMap.getInstance().getProperty(ConfigConstant.PRODUCT_FLAVOR) + variant;
        return AppConstant.PROD_RELEASE.equals(buildVariant);
    }

    // clear activity's stack and redirect the  user to sign-in
    public static void redirectToSignIn(Context context, boolean firstTimeLogin) {
        if (context == null) return;
        BasePreference.getInstance(context).setDiaFirstTimeLogin(firstTimeLogin);
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LaunchIntentManager.routeToActivity(context.getString(R.string.route_sign_in_activity), intent, context);
    }

    // check if user login to diagnostic first time && not a guest user
    public static boolean isFirstTimeLoginToDiagnostic(Context context) {
        return BasePreference.getInstance(context).isDiaFirstTimeLogin() && !BasePreference.getInstance(context).isGuestCart();
    }

    public static void setAnimation(View itemView, boolean do_animate, int delayDuration, Context context) {
        if (do_animate && checkSystemAnimationsDuration(context) > 0) {
            itemView.setTranslationX(itemView.getX() + 400);
            itemView.setAlpha(0.f);
            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(itemView, "translationX", itemView.getX() + 400, 0);
            ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(itemView, "alpha", 1.f);
            ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
            animatorTranslateY.setStartDelay(delayDuration);
            animatorTranslateY.setDuration(delayDuration);
            animatorSet.playTogether(animatorTranslateY, animatorAlpha);
            animatorSet.start();
        }
    }

    private static float checkSystemAnimationsDuration(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getFloat(
                    context.getContentResolver(),
                    Settings.Global.ANIMATOR_DURATION_SCALE,
                    0);
        } else {
            return Settings.System.getFloat(
                    context.getContentResolver(),
                    Settings.System.ANIMATOR_DURATION_SCALE,
                    0);
        }
    }

    public static boolean isProductNotAddToCartStatus(String availabilityStatus) {
        return isProductOutOfStock(availabilityStatus) || isProductNotAvailable(availabilityStatus) || isProductNotForSale(availabilityStatus);
    }

    public static boolean isProductNotForSale(String availabilityStatus) {
        return AppConstant.NOT_FOR_SALE.equalsIgnoreCase(availabilityStatus);
    }

    public static boolean isProductNotAvailable(String availabilityStatus) {
        return AppConstant.NOT_AVAILABLE_STATUS_C.equalsIgnoreCase(availabilityStatus) || AppConstant.NOT_AVAILABLE_STATUS_T.equalsIgnoreCase(availabilityStatus);
    }

    public static boolean isProductOutOfStock(String availabilityStatus) {
        return AppConstant.OUT_OF_STOCK_STATUS.equalsIgnoreCase(availabilityStatus);
    }

    public static void resizeView(final View view, int sizeToChange, long duration) {
        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), view.getMeasuredHeight() - sizeToChange);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = val;
                view.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(duration);
        anim.start();
    }

    // get measured height for the view
    public static int getViewHeight(View view, Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int deviceWidth;
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthMeasureSpec, heightMeasureSpec);
        return view.getMeasuredHeight();
    }

    // expand the view with target height
    public static void expand(final View v, int duration, int targetHeight) {
        int prevHeight = v.getHeight();
        v.setVisibility(View.VISIBLE);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    // collapse the view with target height
    public static void collapse(final View v, int duration, int targetHeight) {
        int prevHeight = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public static boolean isExpiryDate(MStarCustomerDetails primeCustomer) {
        try {
            if (primeCustomer != null && !TextUtils.isEmpty(primeCustomer.getPrimeValidTillTime())) {
                @SuppressLint("SimpleDateFormat") Date date = new SimpleDateFormat("yyyy-MM-dd").parse(primeCustomer.getPrimeValidTillTime());
                return new Date().before(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String getPackageInfo(Context context) {
        if (context == null) return "";
        return context.getApplicationContext().getPackageName();
    }

    public static String validateString(final String value) {
        return TextUtils.isEmpty(value) ? "" : value;
    }

}