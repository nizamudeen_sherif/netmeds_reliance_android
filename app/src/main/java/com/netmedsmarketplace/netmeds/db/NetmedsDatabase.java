package com.netmedsmarketplace.netmeds.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {SearchHistory.class}, version = NetmedsDatabase.CURRENT_DB_VERSION, exportSchema = false)
public abstract class NetmedsDatabase extends RoomDatabase {

    static final String NETMEDS_DATABASE_NAME = "netmeds.db";
    static final int CURRENT_DB_VERSION = 3;

    public abstract SearchHistoryDAO searchHistoryDAO();
}
