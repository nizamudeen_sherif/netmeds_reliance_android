package com.netmedsmarketplace.netmeds.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "search_history")
public class SearchHistory implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "sku")
    private int id;

    @ColumnInfo(name = "algoliaResult")
    private String algoliaResult;

    @ColumnInfo(name = "current_timestamp")
    private long currentTimestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlgoliaResult() {
        return algoliaResult;
    }

    public void setAlgoliaResult(String algoliaResult) {
        this.algoliaResult = algoliaResult;
    }

    public long getCurrentTimestamp() {
        return currentTimestamp;
    }

    public void setCurrentTimestamp(long currentTimestamp) {
        this.currentTimestamp = currentTimestamp;
    }
}
