package com.netmedsmarketplace.netmeds.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SearchHistoryDAO {

    @Query("SELECT * FROM search_history")
    List<SearchHistory> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SearchHistory searchHistory);

    @Query("DELETE FROM search_history")
    void delete();
}
