package com.netmedsmarketplace.netmeds.db;

import androidx.annotation.NonNull;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

class DBMigrationHelper {

    private DBMigrationHelper() {

    }

    static Migration[] getMigrations() {
        return new Migration[]{
                getMigration()};
    }

    private static Migration getMigration() {
        return new Migration(0, NetmedsDatabase.CURRENT_DB_VERSION) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase database) {
                switch (0) {
                    case 0://DB for the first time.
                    default:
                        // Not necessary since all the possible versions are handled on there respective cases
                        break;
                }
            }
        };
    }

    static final Migration getMigration = new Migration(2, NetmedsDatabase.CURRENT_DB_VERSION) { // From version 2 to version 3
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DELETE FROM search_history");
        }
    };
}
