package com.netmedsmarketplace.netmeds.db;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

public class DatabaseClient {

    @SuppressLint("StaticFieldLeak")
    private static DatabaseClient mInstance;

    //our app database object
    private final NetmedsDatabase appDatabase;

    private DatabaseClient(Context context) {
        //creating the app database with Room database builder
        //Netmeds is the name of the database
        RoomDatabase.Builder<NetmedsDatabase> appDatabaseBuilder = Room.databaseBuilder(context, NetmedsDatabase.class, NetmedsDatabase.NETMEDS_DATABASE_NAME);
        appDatabaseBuilder.allowMainThreadQueries();
        appDatabaseBuilder.addMigrations(DBMigrationHelper.getMigration);
        appDatabaseBuilder.fallbackToDestructiveMigration();
        appDatabase = appDatabaseBuilder.build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

    public NetmedsDatabase getAppDatabase() {
        return appDatabase;
    }
}
