package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentxHomeBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ArticleList;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.model.PrimeHomePageConfig;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.payment.ui.PaymentServiceManager;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentKeyResponse;

import java.util.Arrays;
import java.util.List;

import okhttp3.MultipartBody;

public class HomeViewModel extends AppViewModel implements InstallStateUpdatedListener {

    private final MutableLiveData<MstarBasicResponseResultTemplateModel> dashboardResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MStarBasicResponseTemplateModel> promotionBannerResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MStarBasicResponseTemplateModel> healthconcernAndWellnessProductsResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MStarBasicResponseTemplateModel> healthArticleResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MStarBasicResponseTemplateModel> pgOfferResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<InstallState> installStateMutableLiveData = new MutableLiveData<>();
    private HomeListener listener;
    private MStarBasicResponseTemplateModel healthArticleResponse;
    private BasePreference mPreference;
    private ConfigurationResponse configurationResponse;
    private GoogleAnalyticsHelper googleAnalyticsHelper;
    private PrimeConfig primeConfig;
    private MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel, mstarHealthBasicResponseTemplateModel, mstarWellnessCategoryBasicResponseTemplateModel;
    private MStarProductDetails productDetails;

    public PrimeConfig getPrimeConfig() {
        return primeConfig;
    }

    public void setPrimeConfig(PrimeConfig primeConfig) {
        this.primeConfig = primeConfig;
    }

    public HomeViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(HomeListener listener, BasePreference mPreference) {
        configurationResponse = new Gson().fromJson(mPreference.getConfiguration(), ConfigurationResponse.class);
        this.mPreference = mPreference;
        this.listener = listener;
        googleAnalyticsHelper = GoogleAnalyticsHelper.getInstance();
        setPrimeConfig(configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null ? configurationResponse.getResult().getConfigDetails().getPrimeConfig() : new PrimeConfig());
        initPrimeHomeOfferAdapter();
        checkForSoftUpdate();
        if (MStarProductDetails.getInstance().isNotifyGuestUser()) {
            submitNotifyRequest();
        }
    }

    private void submitNotifyRequest() {
        if (MStarProductDetails.getProductDetails() != null) {
            this.productDetails = MStarProductDetails.getProductDetails();
            initMstarApi(APIServiceManager.C_MSTAR_NOTIFY_ME);

        }
    }

    private void checkForSoftUpdate() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isInAppSoftUpdateEnableAndroidFlag()) {
            if (!TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getInAppSoftUpdateAndroidVersion())) {
                String currentVersionCode = CommonUtils.getAppVersionCode(listener.getContext());
                if (Double.parseDouble(currentVersionCode) < Double.parseDouble(configurationResponse.getResult().getConfigDetails().getInAppSoftUpdateAndroidVersion()) && !M2Helper.getInstance().isAppOpenForFirstTime()) {
                    listener.showUpdateAppBanner(configurationResponse.getResult().getConfigDetails().isInAppSoftUpdateEnableAndroidFlag());
                }
            }
        }
    }

    public void initMstarApi(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(listener.getContext());
        if (isConnected) {
            listener.vmShowProgress();
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_HOME_BANNER:
                    APIServiceManager.getInstance().getMstarHomeBanner(this);
                    break;
                case APIServiceManager.C_MSTAR_GATEWAY_OFFERS:
                    APIServiceManager.getInstance().mstarGetGatewayOffers(this);
                    break;
                case APIServiceManager.C_MSTAR_HEALTH_ARTICLES:
                    APIServiceManager.getInstance().mstarGetHealthArticles(this);
                    break;
                case APIServiceManager.C_MSTAR_PROMOTION_BANNER:
                    APIServiceManager.getInstance().getPromotionBanner(this);
                    break;
                case APIServiceManager.C_MSTAR_HEALTHCONCERN_BANNER:
                    APIServiceManager.getInstance().getHealthConcernBanner(this);
                    break;
                case APIServiceManager.C_MSTAR_NOTIFY_ME:
                    APIServiceManager.getInstance().notifyMe(this, getMultiPartNotifyRequest());
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        listener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_HOME_BANNER:
                if (!TextUtils.isEmpty(data)) {
                    MStarBasicResponseTemplateModel model = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                    dashboardResponseMutableLiveData.setValue(model.getResult());
                }
                break;
            case APIServiceManager.C_MSTAR_PROMOTION_BANNER:
                promotionBannerResponseMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));
                break;
            case APIServiceManager.C_MSTAR_HEALTH_ARTICLES:
                healthArticleResponseMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));
                break;
            case APIServiceManager.C_MSTAR_GATEWAY_OFFERS:
                pgOfferResponseMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));
                break;
            case PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL:
                paymentGatewayResponse(data);
                break;
            case APIServiceManager.C_MSTAR_HEALTHCONCERN_BANNER:
                healthconcernAndWellnessProductsResponseMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));
                break;
            case APIServiceManager.C_MSTAR_NOTIFY_ME:
                onNotifyResponse(data);
                break;

        }
        listener.smoothScrollTop();
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.swipeRefresh(false);
        listener.smoothScrollTop();
    }

    private MultipartBody getMultiPartNotifyRequest() {
        MStarCustomerDetails customerData = new Gson().fromJson(BasePreference.getInstance(getApplication()).getCustomerDetails(), MStarCustomerDetails.class);
        if (customerData != null) {
            String firstName = !TextUtils.isEmpty(customerData.getFirstName()) ? customerData.getFirstName() : "";
            String lastName = !TextUtils.isEmpty(customerData.getLastName()) ? customerData.getLastName() : "";

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(AppConstant.FULL_NAME, firstName + " " + lastName);
            builder.addFormDataPart(AppConstant.ACCOUNT_EMAIL, !TextUtils.isEmpty(customerData.getEmail()) ? customerData.getEmail() : "");
            builder.addFormDataPart(AppConstant.DRUGCODE, !TextUtils.isEmpty(String.valueOf(productDetails.getProductCode())) ? String.valueOf(productDetails.getProductCode()) : "");
            builder.addFormDataPart(AppConstant.PRODUCT_NAME, productDetails != null && !TextUtils.isEmpty(productDetails.getDisplayName()) ? productDetails.getDisplayName() : "");
            builder.addFormDataPart(AppConstant.DRUG_TYPE, productDetails != null && !TextUtils.isEmpty(productDetails.getProductType()) ? productDetails.getProductType() : "");
            builder.addFormDataPart(AppConstant.PHONE, customerData.getMobileNo());
            return builder.build();
        } else
            return null;
    }

    public boolean isWellnessAvailable() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getWellnessProductsActiveFlag();
    }

    public boolean isWinterSaleAvailable() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getWinterSaleActiveFlag();
    }

    public boolean isOfferGatewayAvailable() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getGatewayOffersActiveFlag();
    }

    public boolean isHealthArticleAvailable() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getFeaturedArticleFlag();
    }

    public String uploadPrescriptionHeader() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader2()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader2() : "";
    }

    public String uploadPrescriptionTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader1()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader1() : "";
    }

    public String uploadPrescriptionDescription() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getDescription()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getDescription() : "";
    }

    public String uploadPrescriptionButtonText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getButtonText()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getButtonText() : "";
    }

    public String m2UploadPrescriptionButtonText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getM2ButtonText()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getM2ButtonText() : "";
    }

    public String uploadPrescriptionOrderText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getOrderText()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getOrderText() : "";
    }

    public boolean prescriptionUpload() {
        return !isGenericMedicinesVisible() && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null;
    }

    public String consultationIntroducing() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getHeader1()) ? configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getHeader1() : "";
    }

    public String onlineConsultation() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getHeader2()) ? configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getHeader2() : "";
    }

    public String ConsultationOrderText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getOrderText()) ? configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getOrderText() : "";
    }

    public String ConsultationButtonText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getButtonText()) ? configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getButtonText() : "";
    }

    public String consultationDescription() {
        String currentString = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getDescription()) ? configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getDescription() : "";
        String[] separated = currentString.split("\\|");
        return separated.length > 1 ? listener.getContext().getString(R.string.text_dot) + " " + separated[0] + "\n" + listener.getContext().getString(R.string.text_dot) + separated[1] : "";
    }

    public boolean isConsultationAvailable() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadEnableFlag();
    }

    public String getPromotionHeater() {
        return mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getResult() != null && mstarBasicResponseTemplateModel.getResult().getPromotionBanners() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getHeader()) ? mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getHeader() : "";
    }

    public String getPromotionSubHeater() {
        return mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getResult() != null && mstarBasicResponseTemplateModel.getResult().getPromotionBanners() != null && !TextUtils.isEmpty(mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getSubHeader()) ? mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getSubHeader() : "";
    }

    public String getHealthConcernHeater() {
        return mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getMstarHealthConcernCategory() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getMstarHealthConcernCategory().getHeader()) ? mstarHealthBasicResponseTemplateModel.getResult().getMstarHealthConcernCategory().getHeader() : "";
    }

    public String getHealthConcernSubHeater() {
        return mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getMstarHealthConcernCategory() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getMstarHealthConcernCategory().getSubHeader()) ? mstarHealthBasicResponseTemplateModel.getResult().getMstarHealthConcernCategory().getSubHeader() : "";
    }

    public String getBannerHeader() {
        return mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getBrands() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getBrands().getHeader()) ? mstarHealthBasicResponseTemplateModel.getResult().getBrands().getHeader() : "";
    }


    public String getBannerSubHeader() {
        return mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getBrands() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getBrands().getSubHeader()) ? mstarHealthBasicResponseTemplateModel.getResult().getBrands().getSubHeader() : "";
    }


    public String getBannerDescription() {
        return mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getBrands() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getBrands().getTagline()) ? mstarHealthBasicResponseTemplateModel.getResult().getBrands().getTagline() : "";
    }

    public void navigateSearch(View v) {
        /*Google Analytics Click Event*/
        if (v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString()))
            switch (v.getTag().toString()) {
                case GoogleAnalyticsHelper.EVENT_ACTION_SEARCH:
                    googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, v.getTag().toString(), GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                    break;
                case GoogleAnalyticsHelper.EVENT_ACTION_MEDICINE:
                    googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_CATEGORY, v.getTag().toString(), GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                    break;
            }
        listener.vmNavigateSearch();
    }

    public void navigateMedicineHomePage(View v) {
        listener.navigateMedicinePageActivity();
    }

    public void navigateWellness(View v) {
        /*Google Analytics Click Event*/
        if (v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString())) {
            switch (v.getTag().toString()) {
                case GoogleAnalyticsHelper.EVENT_ACTION_WELLNESS:
                    googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_CATEGORY, v.getTag().toString(), GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                    break;
                case GoogleAnalyticsHelper.EVENT_ACTION_WELLNESS_PRODUCTS:
                case GoogleAnalyticsHelper.EVENT_ACTION_HEALTH_CONCERNS:
                    googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_WELLNESS_SECTION, v.getTag().toString(), GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                    break;
            }
        }
        listener.vmNavigateWellness();
    }

    public void navigateConsultation(View v) {
        /*Google Analytics Click Event*/
        if (v.getTag() != null && !TextUtils.isEmpty(v.getTag().toString())) {
            switch (v.getTag().toString()) {
                case GoogleAnalyticsHelper.EVENT_ACTION_CONSULTATION:
                    googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_CATEGORY, v.getTag().toString(), GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                    break;
                case GoogleAnalyticsHelper.EVENT_ACTION_ONLINE_CONSULTATION:
                case GoogleAnalyticsHelper.EVENT_ACTION_POPULAR_CONCERNS_CONSULTATION:
                    googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_CONSULTATION_SECTION, v.getTag().toString(), GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                    break;
            }
        }
        if (checkUserLogin()) {
            listener.vmNavigateConsultation();
        }
    }


    public void navigateDiagnosis(View v) {
        // redirect the user directly to Diagnostic Home page, Login will happen later after selecting the lab test
        listener.vmNavigateDiagnostic();
    }

    public void buyAgain(View v) {
        if (checkUserLogin()) {
            listener.vmNavigateToBuyAgainProducts();
        }
    }

    public void openGenericPage() {
        listener.navigateGenericPage();
    }

    public boolean checkUserLogin() {
        if (mPreference.isGuestCart()) {
            listener.redirectToLogin();
            return false;
        } else return true;
    }

    public void navigateDiagnostic() {
        // redirect the user directly to Diagnostic Home page, Login will happen later after selecting the lab test
        /*Google Analytics Click Event*/
        googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_CATEGORY, GoogleAnalyticsHelper.EVENT_ACTION_LAB_TESTS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
        listener.vmNavigateDiagnostic();
    }

    public MutableLiveData<MstarBasicResponseResultTemplateModel> getDashboardResponseMutableLiveData() {
        return dashboardResponseMutableLiveData;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getPromotionBannerResponseMutableLiveData() {
        return promotionBannerResponseMutableLiveData;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getHealthArticleResponseMutableLiveData() {
        return healthArticleResponseMutableLiveData;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getHealthconcernAndWellnessProductsResponseMutableLiveData() {
        return healthconcernAndWellnessProductsResponseMutableLiveData;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getPgOfferResponseMutableLiveData() {
        return pgOfferResponseMutableLiveData;
    }

    public MutableLiveData<InstallState> getInstallStateMutableLiveData() {
        return installStateMutableLiveData;
    }

    public void getHomeBannerData() {
        initMstarApi(APIServiceManager.C_MSTAR_HOME_BANNER);
    }

    public void getSeasonSale() {
        if (isWinterSaleAvailable()) {
        }
        //todo
        //AppServiceManager.getInstance().getSeasonSale(this);
    }

    public void getWellness() {
        if (isWellnessAvailable()) {
        }
        //todo
        //AppServiceManager.getInstance().getWellness(this);
    }

    public void getPGOffer() {
        if (isOfferGatewayAvailable())
            initMstarApi(APIServiceManager.C_MSTAR_GATEWAY_OFFERS);
    }

    public void getHealthConcerns() {
        initMstarApi(APIServiceManager.C_MSTAR_HEALTHCONCERN_BANNER);
    }

    public void getDiscountSale() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPromotionBannerFlag())
            initMstarApi(APIServiceManager.C_MSTAR_PROMOTION_BANNER);

    }

    public void getHealthArticle() {
        if (isHealthArticleAvailable())
            initMstarApi(APIServiceManager.C_MSTAR_HEALTH_ARTICLES);
    }

    public void refillOrder() {
        //TODO refill
    }

    public void navigateSubscription() {
        // TODO Subscription
    }

    public void referralNavigation() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_REFERRAL, GoogleAnalyticsHelper.EVENT_ACTION_REFER_EARN, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
        listener.vmReferralNavigation();
    }

    public void navigateHealthArticle() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OTHER_ACTIONS, GoogleAnalyticsHelper.EVENT_ACTION_BLOGS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
        if (healthArticleResponse != null && healthArticleResponse.getResult() != null && !TextUtils.isEmpty(healthArticleResponse.getResult().getHeader()))
            listener.vmNavigateHealthArticle(healthArticleResponse.getResult().getHeader());
    }

    public void uploadPrescription() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_UPLOAD_PRESCRIPTION, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
        if (checkUserLogin())
            listener.vmUploadPrescription();
    }


    public void onOfferDataAvailable(MstarBasicResponseResultTemplateModel response) {
        if (response != null && response.getHomeMstarBanner() != null && response.getHomeMstarBanner().size() > 0) {
            listener.offerAdapter(response.getHomeMstarBanner());
        }
        listener.smoothScrollTop();
        listener.swipeRefresh(false);
    }

    public void onHealthArticleAvailable(MStarBasicResponseTemplateModel response) {
        this.healthArticleResponse = response;
        if (showHealthArticle()) {
            listener.healthArticleAdapter(healthArticleResponse.getResult().getArticleList());
        }
        listener.smoothScrollTop();
    }

    public String healthArticleDescription() {
        return healthArticleResponse != null && healthArticleResponse.getResult() != null && !TextUtils.isEmpty(healthArticleResponse.getResult().getDescription()) ? healthArticleResponse.getResult().getDescription() : "";
    }

    public String healthArticleHeader() {
        return healthArticleResponse != null && healthArticleResponse.getResult() != null && !TextUtils.isEmpty(healthArticleResponse.getResult().getHeader()) ? healthArticleResponse.getResult().getHeader() : "";
    }

    public String healthArticleSubHeader() {
        return healthArticleResponse != null && healthArticleResponse.getResult() != null && !TextUtils.isEmpty(healthArticleResponse.getResult().getSubHeader()) ? healthArticleResponse.getResult().getSubHeader() : "";
    }

    private boolean showHealthArticle() {
        return healthArticleResponse != null && healthArticleResponse.getResult() != null && healthArticleResponse.getResult().getArticleList() != null && !healthArticleResponse.getResult().getArticleList().isEmpty();
    }

    public String dealsHeader() {
        if (mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands() != null) {
            return (mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getHeader() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getHeader()) ? mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getHeader() : "");
        } else return "";
    }

    public String dealsSubHeader() {
        if (mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands() != null) {
            return (mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getSubHeader() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getSubHeader()) ? mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getSubHeader() : "");
        } else return "";
    }

    public String dealsDescription() {
        if (mstarHealthBasicResponseTemplateModel != null && mstarHealthBasicResponseTemplateModel.getResult() != null && mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands() != null) {
            return (mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getTagline() != null && !TextUtils.isEmpty(mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getTagline()) ? mstarHealthBasicResponseTemplateModel.getResult().getDealsOnTopBrands().getTagline() : "");
        } else return "";
    }

    public String seasonSaleDescription() {
        //return seasonSaleResponse != null && seasonSaleResponse.getResult() != null && !TextUtils.isEmpty(seasonSaleResponse.getResult().getDescription()) ? seasonSaleResponse.getResult().getDescription() : "";
        return "";
    }

    public String seasonSaleHeader() {
        //return seasonSaleResponse != null && seasonSaleResponse.getResult() != null && !TextUtils.isEmpty(seasonSaleResponse.getResult().getHeader()) ? seasonSaleResponse.getResult().getHeader() : "";
        return "";
    }

    public String seasonSaleSubHeader() {
        //return seasonSaleResponse != null && seasonSaleResponse.getResult() != null && !TextUtils.isEmpty(seasonSaleResponse.getResult().getSubHeader()) ? seasonSaleResponse.getResult().getSubHeader() : "";
        return "";
    }

    public String wellnessDescription() {
        //return wellnessResponse != null && wellnessResponse.getResult() != null && !TextUtils.isEmpty(wellnessResponse.getResult().getDescription()) ? wellnessResponse.getResult().getDescription() : "";
        return "";
    }

    public String wellnessHeader() {
        return mstarWellnessCategoryBasicResponseTemplateModel != null && mstarWellnessCategoryBasicResponseTemplateModel.getResult() != null && mstarWellnessCategoryBasicResponseTemplateModel.getResult().getWellnessProducts() != null && !TextUtils.isEmpty(mstarWellnessCategoryBasicResponseTemplateModel.getResult().getWellnessProducts().getHeader()) ? mstarWellnessCategoryBasicResponseTemplateModel.getResult().getWellnessProducts().getHeader() : "";
    }

    public String wellnessSubHeader() {
        return mstarWellnessCategoryBasicResponseTemplateModel != null && mstarWellnessCategoryBasicResponseTemplateModel.getResult() != null && mstarWellnessCategoryBasicResponseTemplateModel.getResult().getWellnessProducts() != null && !TextUtils.isEmpty(mstarWellnessCategoryBasicResponseTemplateModel.getResult().getWellnessProducts().getSubHeader()) ? mstarWellnessCategoryBasicResponseTemplateModel.getResult().getWellnessProducts().getSubHeader() : "";
    }

    public void onHealthConcernAvailable(MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel) {
        listener.healthConcernAdapter(mstarBasicResponseTemplateModel);
        mstarWellnessCategoryBasicResponseTemplateModel = mstarBasicResponseTemplateModel;
        mstarHealthBasicResponseTemplateModel = mstarBasicResponseTemplateModel;
        listener.smoothScrollTop();
    }

    public void onDiscountSaleAvailable(MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel) {
        this.mstarBasicResponseTemplateModel = mstarBasicResponseTemplateModel;
        //listener.binding().discountView.setLayoutManager(listener.linearLayoutManager());
        listener.discountAdapter(mstarBasicResponseTemplateModel);
        listener.smoothScrollTop();
    }


    public void onPGOfferAvailable(MStarBasicResponseTemplateModel response) {
        if (response.getResult() != null && response.getResult().getOfferList() != null && response.getResult().getOfferList().size() > 0) {
            listener.pgOfferAdapter(response.getResult().getOfferList());
        }
        listener.smoothScrollTop();
    }

    public void getPaymentGatewayDetails() {
        if (TextUtils.isEmpty(mPreference.getPaymentCredentials()))
            PaymentServiceManager.getInstance().getPaymentGatewayCredentialDetails(this, mPreference.getMstarBasicHeaderMap(), PaymentServiceManager.C_MSTAR_PAYMENT_GATEWAY_CREDIENTIAL);
    }


    private void onNotifyResponse(String data) {
        MstarRequestPopupResponse response = new Gson().fromJson(data, MstarRequestPopupResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null) {
            MStarProductDetails.getInstance().setNotifyGuestUser(false);
            listener.showSnackBarMessage(response.getResult());
        }
    }

    private void paymentGatewayResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            PaymentKeyResponse paymentKeyResponse = new Gson().fromJson(data, PaymentKeyResponse.class);
            if (paymentKeyResponse != null && !TextUtils.isEmpty(paymentKeyResponse.getStatus()) && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(paymentKeyResponse.getStatus())) {
                mPreference.setPaymentCredentials(data);
            }
        }
    }

    public void onClickExplorePlans(View view) {
        //GA Post Event
        googleAnalyticsHelper.postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NETMEDS_FIRST, GoogleAnalyticsHelper.EVENT_ACTION_EXPLORE_PLANS_BUTTON, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
        listener.vmNavigationPrime(view);
    }


    public void googleEvent(String bannerFrom) {
        switch (bannerFrom) {
            case GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS:
                GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_BANNER, GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
            case GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER:
                GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_WELLNESS_SECTION, GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
        }
    }

    public boolean isShowDiagnostic() {
        ConfigurationResponse response = new Gson().fromJson(mPreference.getConfiguration(), ConfigurationResponse.class);
        return (response != null && response.getResult() != null && response.getResult().getConfigDetails() != null) && response.getResult().getConfigDetails().isDiagnosisEnableFlag();
    }

    public boolean isGenericMedicinesVisible() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isGenericCornerAndroidEnableFlag();
    }

    public boolean isBuyAgainVisible() {
        return !mPreference.isGuestCart() && isGenericMedicinesVisible() && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isBuyAgainAndroidEnableDisable();
    }

    public boolean isM2BuyAgainVisible() {
        return !mPreference.isGuestCart() && !isGenericMedicinesVisible() && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isBuyAgainAndroidEnableDisable();
    }

    public String genericMedicine() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getGenericLabelText()) ? configurationResponse.getResult().getConfigDetails().getGenericLabelText() : "";
    }

    public String genericMedicinePercentage() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getGenericConfiguration() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getGenericConfiguration().getSaveUpTo()) ? configurationResponse.getResult().getConfigDetails().getGenericConfiguration().getSaveUpTo() : "";

    }

    public String buyAgainText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getBuyAgainLabelText()) ? configurationResponse.getResult().getConfigDetails().getBuyAgainLabelText() : "";
    }

    public boolean isShowConsultation() {
        ConfigurationResponse response = new Gson().fromJson(mPreference.getConfiguration(), ConfigurationResponse.class);
        return (response != null && response.getResult() != null && response.getResult().getConfigDetails() != null) && response.getResult().getConfigDetails().isConsultIconEnableFlag();
    }

    private void initPrimeHomeOfferAdapter() {
        String[] splitPrimeOffer = primeHeader().split("\\|");
        if (splitPrimeOffer.length > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(listener.getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            listener.binding().primeOfferList.setLayoutManager(linearLayoutManager);
            listener.primeHomeAdapter(Arrays.asList(splitPrimeOffer));
        }
    }

    private PrimeHomePageConfig primeHomePageConfig() {
        return getPrimeConfig() != null && getPrimeConfig().getHomePageConfig() != null ? getPrimeConfig().getHomePageConfig() : new PrimeHomePageConfig();
    }

    public String primeTitle() {
        return primeHomePageConfig() != null && !TextUtils.isEmpty(primeHomePageConfig().getTitleText()) ? primeHomePageConfig().getTitleText() : "";
    }

    public String primeDescription() {
        return primeHomePageConfig() != null && !TextUtils.isEmpty(primeHomePageConfig().getDescription()) ? primeHomePageConfig().getDescription() : "";
    }

    public String primeButtonText() {
        return primeHomePageConfig() != null && !TextUtils.isEmpty(primeHomePageConfig().getButtonText()) ? primeHomePageConfig().getButtonText() : "";
    }

    private String primeHeader() {
        return primeHomePageConfig() != null && !TextUtils.isEmpty(primeHomePageConfig().getHeaderText()) ? primeHomePageConfig().getHeaderText() : "";
    }

    public boolean isEnablePrime() {
        return primeHomePageConfig() != null && primeHomePageConfig().isEnableFlag() && getPrimeConfig().getPrimeEnableFlag();
    }

    public boolean isQuickConsultationEnabled() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConcernConfigs() != null && configurationResponse.getResult().getConfigDetails().getConcernConfigs().isEnableFlag();
    }

    public boolean isQuickDiagnosisEnabled() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().isEnableFlag();
    }

    public boolean isConsultationBannerEnabled() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getSliderConfigs() != null && configurationResponse.getResult().getConfigDetails().getSliderConfigs().isEnableFlag();
    }

    public boolean isDiagnosisBannerEnabled() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisSliderConfigs() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisSliderConfigs().isEnableFlag();
    }

    public void onLater() {
        M2Helper.getInstance().setAppOpenForFirstTime(true);
        listener.showUpdateAppBanner(false);
    }

    public void onDownload() {
        listener.onFlexibleUpdate();
    }

    @Override
    public void onStateUpdate(InstallState state) {
        installStateMutableLiveData.setValue(state);
    }

    public void setInstallStateMutableLiveData(InstallState state) {
        installStateMutableLiveData.setValue(state);
    }

    public String getInAppUpdateTitle() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getInAppSoftUpdateTitle()))
            return configurationResponse.getResult().getConfigDetails().getInAppSoftUpdateTitle();
        else return "";
    }

    public String getInAppUpdateDesc() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getInAppSoftUpdateDesc()))
            return configurationResponse.getResult().getConfigDetails().getInAppSoftUpdateDesc();
        else return "";
    }

    public boolean isEmergencyMessageVisible() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isEmergencyMessageHomeEnabled();
    }

    public boolean emergencyMessageTitleVisibility() {
        return !TextUtils.isEmpty(emergencyMessageTitle());
    }

    public String emergencyMessage() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getEmergencyHomeContent() : "";
    }

    public String emergencyMessageTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getEmergencyHomeTitle() : "";
    }

    public interface HomeListener {
        void vmNavigateSearch();

        void vmNavigateWellness();

        void vmNavigateConsultation();

        void vmNavigateDiagnostic();

        void vmUploadPrescription();

        void vmNavigateHealthArticle(String title);

        void vmReferralNavigation();

        void vmShowProgress();

        void vmDismissProgress();

        void smoothScrollTop();

        void redirectToLogin();

        void vmNavigationPrime(View view);

        Context getContext();

        void swipeRefresh(boolean swipeRefresh);

        void offerAdapter(List<MstarBanner> homeMstarBanner);

        void pgOfferAdapter(List<MstarNetmedsOffer> offerList);

        void discountAdapter(MStarBasicResponseTemplateModel promotionBanners);

        void healthConcernAdapter(MStarBasicResponseTemplateModel healthConcernCategory);

        LinearLayoutManager linearLayoutManager();

        FragmentxHomeBinding binding();

        void primeHomeAdapter(List<String> asList);

        void healthArticleAdapter(List<ArticleList> articleList);

        void showUpdateAppBanner(boolean isUpdateAvailable);

        void onFlexibleUpdate();

        void showSnackBarMessage(String message);

        void vmNavigateToBuyAgainProducts();

        void navigateGenericPage();

        void navigateMedicinePageActivity();
    }
}
