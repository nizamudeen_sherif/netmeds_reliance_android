package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.NotifyDialogueBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;

import okhttp3.MultipartBody;

public class NotifyDialogueViewModel extends AppViewModel {
    private NotifyListener listener;
    private String productName;
    private String drugType;
    private String drugCode;
    private NofifyFragmentDialogueListener fragmentDialogueListener;
    private BasePreference preference;

    public NotifyDialogueViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(NotifyListener listener, String productName, String drugType, String drugCode, NofifyFragmentDialogueListener nofifyFragmentDialogueListener, BasePreference preference) {
        this.listener = listener;
        this.drugType = drugType;
        this.productName = productName;
        this.drugCode = drugCode;
        this.fragmentDialogueListener = nofifyFragmentDialogueListener;
        this.preference = preference;

    }

    public String getName() {
        String firstName = mStarCustomerDetails()!= null && !TextUtils.isEmpty(mStarCustomerDetails().getFirstName()) ? mStarCustomerDetails().getFirstName() : "";
        String lastName = mStarCustomerDetails()!= null && !TextUtils.isEmpty(mStarCustomerDetails().getLastName()) ? mStarCustomerDetails().getLastName() : "";
        return String.format("%s %s", firstName, lastName);
    }

    public String getEmail() {
        return mStarCustomerDetails() != null && mStarCustomerDetails().getEmail() != null ? mStarCustomerDetails().getEmail() : "";

    }

    public String getMobileNumber() {
        return mStarCustomerDetails() != null && mStarCustomerDetails().getMobileNo() != null ? mStarCustomerDetails().getMobileNo() : "";

    }

    public void onNameTextChanged() {
        listener.binding().tvNotifyErrorName.setText(null);

    }

    public void onPhoneTextChanged() {
        listener.binding().tvNotifyErrorNumber.setText(null);

    }

    public void onEmailTextChanged() {
        listener.binding().tvNotifyErrorEmail.setText(null);

    }

    public void onClickRequest() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        if (isConnected && validateForm()) {
            /*if (preference.isGuestCart()) {
                listener.navigateSiginActivity();
            } else {*/
                listener.loderPrgress();
                APIServiceManager.getInstance().notifyMe(this, getMultiPartRequest());
            //}
        }
    }

    private MultipartBody getMultiPartRequest() {
        MStarCustomerDetails addressData = new Gson().fromJson(BasePreference.getInstance(getApplication()).getCustomerDetails(), MStarCustomerDetails.class);
        if (addressData != null) {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(AppConstant.FULL_NAME, listener.binding().notifyName.getText().toString());
            builder.addFormDataPart(AppConstant.ACCOUNT_EMAIL, !TextUtils.isEmpty(addressData.getEmail()) ? addressData.getEmail() : "");
            builder.addFormDataPart(AppConstant.DRUGCODE, drugCode);
            builder.addFormDataPart(AppConstant.PRODUCT_NAME, productName);
            builder.addFormDataPart(AppConstant.DRUG_TYPE, drugType);
            builder.addFormDataPart(AppConstant.PHONE, listener.binding().notifyPhoneno.getText().toString());
            return builder.build();
        } else
            return null;
    }

    private MStarCustomerDetails mStarCustomerDetails() {
        MStarCustomerDetails addressData = new Gson().fromJson(preference.getCustomerDetails(), MStarCustomerDetails.class);
        return addressData;
    }

    public void dismissDialogue() {
        listener.dismissFragment();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        listener.loderDismiss();
        listener.dismissFragment();
        notifyMeResponse(data);

    }

    private void notifyMeResponse(String data) {
        MstarRequestPopupResponse response = new Gson().fromJson(data, MstarRequestPopupResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null) {
            fragmentDialogueListener.snackBarCallBack(response.getResult());
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.loderDismiss();
    }

    private boolean validateForm() {
        boolean isValidate = true;
        if (!ValidationUtils.userName(listener.binding().notifyName, true, getApplication().getString(R.string.text_enter_name), 3, 30, listener.binding().tvNotifyErrorName)) {
            isValidate = false;
        } else if (!ValidationUtils.checkText(listener.binding().notifyPhoneno, true, getApplication().getString(R.string.error_mobile), 10, 10, listener.binding().tvNotifyErrorNumber)) {
            isValidate = false;
        } else if (!checkEmailValidation(listener.binding().notifyEmail, listener.binding().tvNotifyErrorEmail, getApplication())) {
            isValidate = false;
        }
        return isValidate;
    }

    private static boolean checkEmailValidation(EditText element, TextView inputEmail, Context context) {
        String value = element.getText().toString();

        String[] splitValue = null;

        if (value.contains("@"))
            splitValue = value.split("@");

        if (TextUtils.isEmpty(value)) {
            inputEmail.setText(context.getResources().getString(R.string.txt_error_email));
            return false;
        } else if (!isEmail(value)) {
            inputEmail.setText(context.getResources().getString(R.string.txt_error_valid_email));
            return false;
        } else {
            assert splitValue != null;
            if (value.startsWith(".") || value.contains("+") || splitValue[0].endsWith(".") || value.contains("..")) {
                inputEmail.setText(context.getResources().getString(R.string.txt_error_valid_email));
                return false;
            } else {
                inputEmail.setText(null);
                return true;
            }
        }
    }

    private static boolean isEmail(String value) {
        return value != null && android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches();
    }

    public interface NotifyListener {
        void dismissFragment();

        void loderPrgress();

        void loderDismiss();

        NotifyDialogueBinding binding();

        void navigateSiginActivity();

    }

    public interface NofifyFragmentDialogueListener {
        void snackBarCallBack(String message);
    }
}
