package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.adpater.AllTransactionAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityAllTransactionBinding;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.MstarWalletHistory;

import java.util.List;

public class AllTransactionViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityAllTransactionBinding binding;
    private List<MstarWalletHistory> transactionResultList;

    public AllTransactionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityAllTransactionBinding binding, List<MstarWalletHistory> resultList) {
        this.context = context;
        this.binding = binding;
        this.transactionResultList = resultList;
        initiateTransactionList();
    }

    private void initiateTransactionList() {
        AllTransactionAdapter adapter = new AllTransactionAdapter(transactionResultList, transactionResultList.size());
        binding.rvTransactionList.setLayoutManager(new LinearLayoutManager(context));
        binding.rvTransactionList.setNestedScrollingEnabled(false);
        binding.rvTransactionList.setAdapter(adapter);
    }

}
