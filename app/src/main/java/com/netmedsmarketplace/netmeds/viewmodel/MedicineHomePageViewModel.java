package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarWellnessSectionDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;
import java.util.List;

public class MedicineHomePageViewModel extends AppViewModel {
    private MedicineHomeListener listener;
    private ConfigurationResponse configurationResponse;
    private String productIdList;
    private int addProductCode;
    private String wellnessViewAll;
    private final MutableLiveData<MStarBasicResponseTemplateModel> pgOfferResponseMutableLiveData = new MutableLiveData<>();


    public MedicineHomePageViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getPgOfferResponseMutableLiveData() {
        return pgOfferResponseMutableLiveData;
    }

    public void init(MedicineHomeListener listener) {
        this.listener = listener;
        listener.showProgressBar();
        getMedicineBanner();
        configurationResponse = new Gson().fromJson(listener.basePreference().getConfiguration(), ConfigurationResponse.class);
        if (!listener.basePreference().isGuestCart()) {
            initMstarApi(APIServiceManager.C_MSTAR_PAST_MEDICINES);
        }
    }

    public void initMstarApi(int transactionId) {
        switch (transactionId) {
            case APIServiceManager.C_MEDICINE_PAGE_BANNER:
                APIServiceManager.getInstance().medicineHomeBanner(this);
                break;
            case APIServiceManager.C_MSTAR_PAST_MEDICINES:
                APIServiceManager.getInstance().mStargetBuyAgainProducts(this, listener.basePreference().getMstarBasicHeaderMap());
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                APIServiceManager.getInstance().mStarProductDetailsForIdList(this, productIdList, APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                APIServiceManager.getInstance().mStarCreateCart(this, listener.basePreference().getMstarBasicHeaderMap());
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                APIServiceManager.getInstance().mStarAddProductToCart(this, listener.basePreference().getMstarBasicHeaderMap(), addProductCode, 1, null);
                break;

        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.C_MEDICINE_PAGE_BANNER:
                pgOfferResponseMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));
                break;
            case APIServiceManager.C_MSTAR_PAST_MEDICINES:
                onBuyAgainResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                onProductListAvailable(data);
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                onCreateCartResponse(data);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                onProductAddedToCart(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.dismissProgressBar();
        super.onFailed(transactionId, data);
    }

    private void onBuyAgainResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                if (!TextUtils.isEmpty(response.getResult().getMessage()) && AppConstant.NO_RECORD_FOUND.equalsIgnoreCase(response.getResult().getMessage())) {
                    listener.dismissProgressBar();
                    listener.snackBarMessage(response.getResult().getMessage(), false);
                } else if (response.getResult().getSku() != null) {
                    productIdList = response.getResult().getSku();
                    initMstarApi(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                }
            }
        }
    }

    private void onProductListAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                if (response.getResult().getProductDetailsList() != null && !response.getResult().getProductDetailsList().isEmpty()) {
                    List<MStarProductDetails> buyAgainProductDetailsList = new ArrayList<>(response.getResult().getProductDetailsList());
                    listener.setBuyAgainAdapter(buyAgainProductDetailsList);
                    listener.dismissProgressBar();
                }
            }
        }
    }

    private void onCreateCartResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
                listener.basePreference().setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
                initMstarApi(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
            }
        }
    }

    private void onProductAddedToCart(String data) {
        listener.dismissProgressBar();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (responseTemplateModel != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())) {
                listener.snackBarMessage("", true);
            } else if (responseTemplateModel != null && responseTemplateModel.getReason() != null) {
                if (!TextUtils.isEmpty(responseTemplateModel.getReason().getReason_code()) && AppConstant.CART_NOT_FOUND.equalsIgnoreCase(responseTemplateModel.getReason().getReason_code())) {
                    initMstarApi(APIServiceManager.MSTAR_CREATE_CART);
                } else {
                    listener.snackBarMessage(!TextUtils.isEmpty(responseTemplateModel.getReason().getReason_eng()) ? responseTemplateModel.getReason().getReason_eng() : "", false);
                }
            }
        }
    }

    public void uploadPrescription() {
        if (checkUserLogin())
            listener.vmUploadPrescription();
    }

    public String medicineDescription() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getorderDescripText()) ? configurationResponse.getResult().getConfigDetails().getorderDescripText() : "";

    }

    public void search() {
        listener.navigateSearch();
    }

    public String m2UploadPrescriptionButtonText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getM2ButtonText()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getM2ButtonText() : "";
    }

    public void openGenericPage() {
        listener.navigateGenericPage();
    }

    public String genericMedicine() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getGenericLabelText()) ? configurationResponse.getResult().getConfigDetails().getGenericLabelText() : "";
    }

    public String genericMedicinePercentage() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getGenericConfiguration() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getGenericConfiguration().getSaveUpTo()) ? configurationResponse.getResult().getConfigDetails().getGenericConfiguration().getSaveUpTo() : "";
    }

    public boolean prescriptionUpload() {
        return !isGenericMedicinesVisible() && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null;
    }

    public String uploadPrescriptionHeader() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader2()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader2() : "";
    }

    public String uploadPrescriptionDescription() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getDescription()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getDescription() : "";
    }

    public String uploadPrescriptionTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader1()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getHeader1() : "";
    }

    public String uploadPrescriptionOrderText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getOrderText()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getOrderText() : "";
    }

    public String uploadPrescriptionButtonText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getButtonText()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getButtonText() : "";
    }

    private void getMedicineBanner() {
        initMstarApi(APIServiceManager.C_MEDICINE_PAGE_BANNER);
    }

    public boolean isBuyAgainVisible() {
        return !listener.basePreference().isGuestCart() && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isBuyAgainAndroidEnableDisable();
    }

    public boolean isGenericMedicinesVisible() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isGenericCornerAndroidEnableFlag();
    }

    public void onMedicineAvailable(MStarBasicResponseTemplateModel response) {
        if (response.getResult() != null && response.getResult().getMedicineHomeBanner() != null && response.getResult().getMedicineHomeBanner().size() > 0) {
            for (MstarWellnessSectionDetails sectionDetails : response.getResult().getMedicineHomeBanner()) {
                if (AppConstant.CATEGORY.equalsIgnoreCase(sectionDetails.getParseType())) {
                    int index = response.getResult().getMedicineHomeBanner().indexOf(sectionDetails);
                    listener.WellnessAdapter(response.getResult().getMedicineHomeBanner(), index);
                    if (!TextUtils.isEmpty(response.getResult().getMedicineHomeBanner().get(index).getViewAll()))
                        wellnessViewAll = response.getResult().getMedicineHomeBanner().get(index).getViewAll();
                } else if (AppConstant.PRODUCTS.equalsIgnoreCase(sectionDetails.getParseType())) {
                    //TODO
                } else if (AppConstant.BANNER.equalsIgnoreCase(sectionDetails.getParseType())) {
                    listener.medicineBannerAdapter(response.getResult().getMedicineHomeBanner(), response.getResult().getMedicineHomeBanner().indexOf(sectionDetails));
                }
            }
        }
        if (listener.basePreference().isGuestCart()) {
            listener.dismissProgressBar();
        }
    }

    public boolean checkUserLogin() {
        if (listener.basePreference().isGuestCart()) {
            listener.redirectToLogin();
            return false;
        } else return true;
    }

    public String buyAgainText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getBuyAgainLabelText()) ? configurationResponse.getResult().getConfigDetails().getBuyAgainLabelText() : "";
    }

    public void onclickWellnessViewAll() {
        listener.navigateWellnessActivity(wellnessViewAll);
    }

    public void onclickBuyAgainViewAll() {
        listener.navigateBuyAgainActivity();
    }

    public void productCode(int productCode) {
        addProductCode = productCode;
        if (!TextUtils.isEmpty(listener.basePreference().getCartId()))
            initMstarApi(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
        else
            initMstarApi(APIServiceManager.MSTAR_CREATE_CART);
    }

    public interface MedicineHomeListener {
        void vmUploadPrescription();

        BasePreference basePreference();

        void navigateGenericPage();

        void medicineBannerAdapter(List<MstarWellnessSectionDetails> offerList, int index);

        void setBuyAgainAdapter(List<MStarProductDetails> buyAgainProductDetailsList);

        void WellnessAdapter(List<MstarWellnessSectionDetails> mstarBasicResponseTemplateModel, int index);

        void navigateSearch();

        void navigateWellnessActivity(String wellnessViewAll);

        void navigateBuyAgainActivity();

        void snackBarMessage(String message, boolean isSuccessN);

        void showProgressBar();

        void dismissProgressBar();

        void redirectToLogin();
    }
}
