package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogConfirmDeleteSavedCardsBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModel;

public class DeleteCardConfirmDialogViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private DialogConfirmDeleteSavedCardsBinding binding;
    private DeleteCardConfirmDialogViewModelListener listener;
    private String value;
    private String clickFrom;

    public DeleteCardConfirmDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DialogConfirmDeleteSavedCardsBinding binding, DeleteCardConfirmDialogViewModelListener listener, String value, String clickFrom) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        this.value = value;
        this.clickFrom = clickFrom;
        setTextWithCardDetails();
    }

    private void setTextWithCardDetails() {
        if (AppConstant.SAVED_CARDS.equalsIgnoreCase(clickFrom)) {
            binding.tvCardDesc.setText(getCardWithText());
        } else {
            binding.tvHeading.setText(context.getString(R.string.text_remove_wallet));
            binding.tvCardDesc.setText(getCardWithText());
        }
    }


    private SpannableString getCardWithText() {
        String boldValue, totalString;
        SpannableString resultString;
        if (AppConstant.SAVED_CARDS.equalsIgnoreCase(clickFrom)) {
            boldValue = String.format(context.getString(R.string.text_saved_card_secret_with_star), value);
            totalString = String.format(context.getString(R.string.text_delete_saved_cards_with_text), boldValue);
            resultString = new SpannableString(totalString);
        } else {
            boldValue = value;
            totalString = String.format(context.getString(R.string.text_delete_wallet_with_text), boldValue.toUpperCase());
            resultString = new SpannableString(String.format("%s %s", totalString, context.getString(R.string.text_wallet_with_question)));
        }
        resultString.setSpan(new StyleSpan(Typeface.BOLD), (totalString.length() - boldValue.length()), totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return resultString;
    }

    public void closeView() {
        listener.onDismissConfirmDialog();
    }

    public void onCancelClick() {
        listener.onDismissConfirmDialog();
    }

    public void onProceedClick() {
        listener.onProceedDeleteCard();
    }

    public interface DeleteCardConfirmDialogViewModelListener {

        void onDismissConfirmDialog();

        void onProceedDeleteCard();
    }
}
