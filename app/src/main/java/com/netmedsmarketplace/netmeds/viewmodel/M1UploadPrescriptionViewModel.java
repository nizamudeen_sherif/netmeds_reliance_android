package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;

public class M1UploadPrescriptionViewModel extends AppViewModel {

    private M1PrescriptionListener mPrescriptionListener;

    public M1UploadPrescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(M1PrescriptionListener prescriptionListener) {
        this.mPrescriptionListener = prescriptionListener;
    }

    public void closeView() {
        mPrescriptionListener.closeDialog();
    }

    public void cameraView() {
        mPrescriptionListener.cameraView();
    }

    public void galleryView() {
        mPrescriptionListener.galleryView();
    }

    public void pastRxView() {
        mPrescriptionListener.pastPrescriptionView();
    }

    public interface M1PrescriptionListener {

        void closeDialog();

        void cameraView();

        void galleryView();

        void pastPrescriptionView();

    }
}