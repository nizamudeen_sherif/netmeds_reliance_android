package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.PromoCodeList;

public class PromoCodeViewModel extends BaseViewModel {
    private PromoCodeList consultationCoupon;

    public PromoCodeViewModel(Application application) {
        super(application);
    }

    public void onDataAvailable(PromoCodeList consultationCoupon) {
        this.consultationCoupon = consultationCoupon;
    }

    public String couponCode() {
        return consultationCoupon != null && !TextUtils.isEmpty(consultationCoupon.getCouponCode()) ? consultationCoupon.getCouponCode() : "";
    }

    public String couponMessage() {
        return consultationCoupon != null && !TextUtils.isEmpty(consultationCoupon.getDescription()) ? consultationCoupon.getDescription() : "";
    }

    public boolean showCouponDescription() {
        return TextUtils.isEmpty(couponMessage());
    }

    public boolean showCouponCode() {
        return TextUtils.isEmpty(couponCode());
    }

    public String saving() {
        return String.format(getApplication().getResources().getString(R.string.text_promo_saving), consultationCoupon.getDiscount()) + "%";
    }
}
