package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.BaseViewModel;


public class StillNeedHelpViewModel extends BaseViewModel {

    private StillNeedHelpListener listener;
    private boolean isChatEnable;

    public StillNeedHelpViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(StillNeedHelpListener listener, boolean isChatEnable) {
        this.listener = listener;
        this.isChatEnable = isChatEnable;
    }

    public void closeView() {
        listener.closeDialog();
    }

    public void email() {
        listener.eMail();
    }

    public void openKapChat() {
        listener.openKapChat();
    }

    public boolean isChatEnable() {
        return isChatEnable;
    }

    public void call() {
        listener.call();
    }

    public interface StillNeedHelpListener {

        void closeDialog();

        void eMail();

        void call();

        boolean checkCallPermissionEnable();

        void requestEnableCallPermission();

        void openKapChat();
    }
}