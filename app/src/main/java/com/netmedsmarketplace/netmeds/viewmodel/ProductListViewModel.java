package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.adpater.MstarCategoryProductAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.VisibleItemListener;
import com.nms.netmeds.base.model.BrainSinsMisc;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarBaseIdUrl;
import com.nms.netmeds.base.model.MstarCategoryDetails;
import com.nms.netmeds.base.model.MstarManufacturerDetails;
import com.nms.netmeds.base.model.MstarSubCategoryResult;
import com.nms.netmeds.base.model.Request.MstarBrandProductListRequest;
import com.nms.netmeds.base.model.Request.MstarCategoryDetailsRequest;
import com.nms.netmeds.base.model.Request.MstarManufacturerDetailsRequest;
import com.nms.netmeds.base.model.SortOptionLabel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.WebEngageHelper;

import org.json.JSONArray;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductListViewModel extends AppViewModel {


    private int mstarCategoryId;
    private int brandId;
    private int pageIndex = 1;
    private int algoliaPageIndex = 0;
    private int manufacturerId;
    private int alternateSaltProductCode;
    private int product_code;
    private final int pageSize = 15;

    private long totalPageCount = 0;

    private String intentFrom;
    private String filterQuery = "";
    private String genericName = "";
    private String imageURLPath;
    private String brandUrl = "";
    private String productType = "";

    private boolean clearList = false;
    private boolean isFacetValueFirst = true;
    private boolean isFilterApplied = false;
    private boolean isCategoryAPIComplete = false;
    private boolean isCatlogBannerAPIComplete = false;
    private boolean isManufactureAPIComplete = false;
    private boolean isManufacture = false;
    public boolean isFromAlgolia = false;
    private boolean isFromBanner;
    private boolean webEngageFlag = false;

    private ProductListListener productListListener;
    public MstarCategoryProductAdapter mstarCategoryProductAdapter;
    private SortOptionLabel sortOption;
    private BasePreference basePreference;
    private List<SortOptionLabel> sortOptionList;
    private List<String> defaultBannerList = new ArrayList<>();
    private List<MstarBanner> mstarBannersList = new ArrayList<>();
    private List<MStarProductDetails> mStarProductDetailsList = new ArrayList<>();
    private MutableLiveData<MStarProductDetails> mStarProductDetailsMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<MstarManufacturerDetails> manufacturerDetailsMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<MstarCategoryDetails> mstarCategoryDetailsMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<MstarAlgoliaResponse> algoliaMutableLiveData = new MutableLiveData<>();
    private Map<String, Map<String, String>> algoliaFilterOption;
    private List<MstarSubCategoryResult> subCategoryList;
    private static VisibleItemListener visibleItemListener;

    private String getFilterQuery() {
        return filterQuery;
    }

    public void setFilterQuery(String filterQuery) {
        this.filterQuery = filterQuery;//set filter query
    }

    public boolean isFilterApplied() {
        return isFilterApplied;
    }

    private void setFilterApplied(boolean filterApplied) {
        isFilterApplied = filterApplied;//set filter applied or not
    }

    public MutableLiveData<MstarCategoryDetails> getMstarCategoryDetailsMutableLiveData() {
        return mstarCategoryDetailsMutableLiveData;
    }

    public MutableLiveData<MstarAlgoliaResponse> getAlgoliaMutableLiveData() {
        return algoliaMutableLiveData;
    }

    public ProductListViewModel(@NonNull Application application) {
        super(application);
    }

    public String getIntentFrom() {
        return intentFrom;
    }

    public boolean setFilterAndSortVisibility() {
        return AppConstant.CATEGORY.equalsIgnoreCase(intentFrom) || AppConstant.MANUFACTURER.equalsIgnoreCase(intentFrom) || AppConstant.BRAND.equalsIgnoreCase(intentFrom) || AppConstant.URL.equalsIgnoreCase(intentFrom);
    }

    public void setIntentFrom(String intentFrom) {
        this.intentFrom = intentFrom;
    }

    public void init(ProductListListener productListListener, BasePreference basePreference) {
        this.productListListener = productListListener;
        this.basePreference = basePreference;
        MStarBasicResponseTemplateModel response = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageURLPath = response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getCatalogImageUrlBasePath()) ? response.getResult().getCatalogImageUrlBasePath() : "";
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        productListListener.showWebserviceError(false);
        productListListener.retry();
    }

    public void onManufacturerDataAvailable(MstarManufacturerDetails mstarManufacturerDetails) {
        productListListener.setTitle(mstarManufacturerDetails != null && !TextUtils.isEmpty(mstarManufacturerDetails.getName()) ? mstarManufacturerDetails.getName() : "");
        if (mstarManufacturerDetails != null && mstarManufacturerDetails.getProducts() != null && mstarManufacturerDetails.getProducts().size() > 0) {
            setSearchLimit(mstarManufacturerDetails.getInstockProductCount());
            if(this.mStarProductDetailsList ==null && this.mStarProductDetailsList.isEmpty())
                this.mStarProductDetailsList.clear();
            this.mStarProductDetailsList.addAll(mstarManufacturerDetails.getProducts());
            setSearchAdapter(mstarManufacturerDetails.getProducts());
            productListListener.setTitle(mstarManufacturerDetails.getProducts().get(0).getManufacturer() != null && !TextUtils.isEmpty(mstarManufacturerDetails.getProducts().get(0).getManufacturer().getName()) ? mstarManufacturerDetails.getProducts().get(0).getManufacturer().getName() : "");
            if (!TextUtils.isEmpty(mstarManufacturerDetails.getMastheadImagePath())) {
                imageURLPath = imageURLPath + mstarManufacturerDetails.getMastheadImagePath();
                defaultBannerList.add(imageURLPath);
            }
            if (isFacetValueFirst)
                fetchFacetValueFromAlgolia();
            webEngageEvent(new Gson().toJson(mstarManufacturerDetails));
        } else {
            productListListener.showEmptyView(true);
        }
    }

    public static void setVisibleItemListener(VisibleItemListener itemListener) {
        visibleItemListener = itemListener;
    }

    private void webEngageEvent(String data) {
        if (!webEngageFlag) {
            webEngageFlag = true;
            switch (getIntentFrom()) {
                case AppConstant.MANUFACTURER:
                    /*WebEngage Manufacturer Viewed Event*/
                    WebEngageHelper.getInstance().reportEventManufacturerViewed(productListListener.getContext(), new Gson().fromJson(data, MstarManufacturerDetails.class));
                    break;
                case AppConstant.CATEGORY:
                    /*WebEngage SubCategory Viewed Event*/
                    WebEngageHelper.getInstance().subCategoryViewedEvent(productListListener.getContext(), new Gson().fromJson(data, MstarCategoryDetails.class));
                    break;
            }
        }
    }

    /**
     * set filter option first time from category details API response
     *
     * @param response category details response
     */
    private void setFilterOption(MstarCategoryDetails response) {
        if (response != null && response.getSubCategories() != null && response.getSubCategories().size() > 0) {
            setSubCategoryList(response.getSubCategories());
        }
    }

    private void onBrandproductListDataAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getBrandDetails() != null && response.getResult().getBrandDetails().getProductDetailsList() != null && response.getResult().getBrandDetails().getProductDetailsList().size() > 0) {
                productListListener.showEmptyView(false);
                setSearchLimit(response.getResult().getBrandDetails().getInstockProductCount());
                setSearchAdapter(response.getResult().getBrandDetails().getProductDetailsList());
                if (isFacetValueFirst)
                    fetchFacetValueFromAlgolia();
            }
        }
    }


    private void setSearchLimit(int productCount) {
        calculateTotalPageCount(productCount);
    }

    private void calculateTotalPageCount(int productCount) {
        totalPageCount = (long) Math.ceil(Double.valueOf(String.valueOf(productCount)) / Double.valueOf(String.valueOf(pageSize)));
    }

    private void setSearchAdapter(List<MStarProductDetails> productDetailsList) {
        if (mstarCategoryProductAdapter != null) {
            mstarCategoryProductAdapter.updateProductList(productDetailsList, isClearList());
        } else {
            mstarCategoryProductAdapter = productListListener.setAdapter(productDetailsList != null && productDetailsList.size() > 0 ? productDetailsList : new ArrayList<MStarProductDetails>(), defaultBannerList != null && defaultBannerList.size() > 0 ? defaultBannerList : new ArrayList<String>(), mstarBannersList != null && mstarBannersList.size() > 0 ? mstarBannersList : new ArrayList<MstarBanner>(), false);
        }
    }

    public void onProductListDataAvailable(MstarCategoryDetails response) {
        if (response.getProductDetailsList() != null && response.getProductDetailsList().size() > 0) {
            if (!TextUtils.isEmpty(response.getMastheadImagePath()) && defaultBannerList.isEmpty()) {
                imageURLPath = imageURLPath + response.getMastheadImagePath();
                defaultBannerList.add(imageURLPath);
            }
            productListListener.setTitle(!TextUtils.isEmpty(response.getName()) ? response.getName() : "");
            setSearchLimit(response.getInstockProductCount());
            if(this.mStarProductDetailsList ==null && this.mStarProductDetailsList.isEmpty())
                this.mStarProductDetailsList.clear();
            this.mStarProductDetailsList.addAll(response.getProductDetailsList());
            setSearchAdapter(response.getProductDetailsList());
            setFilterOption(response);
            if (isFacetValueFirst)
                fetchFacetValueFromAlgolia();
            webEngageEvent(new Gson().toJson(response));
        } else {
            productListListener.showEmptyView(true);
        }
    }

    private void getMoreProducts() {
        if (isFromAlgolia || getIntentFrom().equals(AppConstant.ALTERNATE_SALT)) {
            initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
        } else if (AppConstant.CATEGORY.equalsIgnoreCase(intentFrom)) {
            initMstarAPIcall(APIServiceManager.MSTAR_CATEGORY_DETAILS);
        } else if (AppConstant.MANUFACTURER.equalsIgnoreCase(intentFrom)) {
            initMstarAPIcall(APIServiceManager.MSTAR_MANUFACTURER_DETAILS);
        } else if (AppConstant.BRAND.equalsIgnoreCase(intentFrom)) {
            initMstarAPIcall(APIServiceManager.MSTAR_BRAND_PRODUCT_LIST);
        } else if (AppConstant.URL.equalsIgnoreCase(intentFrom)) {
            initMstarAPIcall(APIServiceManager.C_MSTAR_RETRIEVE_ANY_URL);
        }
    }


    public void initMstarAPIcall(int transactionId) {
        boolean isConnected = productListListener.isNetworkConnected();
        productListListener.showNoNetworkView(isConnected);
        if (isConnected) {
            productListListener.vmShowProgress();
            switch (transactionId) {
                case APIServiceManager.MSTAR_CATEGORY_DETAILS:
                    APIServiceManager.getInstance().mStarCategoryDetails(this, getCategoryRequest());
                    break;
                case APIServiceManager.C_MSTAR_CATLOG_BANNERS:
                    if (isManufacture){
                        APIServiceManager.getInstance().getMstarCatlogBanners(this, AppConstant.MANUFACTURE, "", String.valueOf(manufacturerId),"");
                    }else if(isFromBanner){
                        APIServiceManager.getInstance().getMstarCatlogBanners(this, AppConstant.BRAND, "","",String.valueOf(brandId));
                    }else {
                        APIServiceManager.getInstance().getMstarCatlogBanners(this, AppConstant.CATEGORY.toLowerCase(), String.valueOf(mstarCategoryId), "","");
                    }
                    break;
                case APIServiceManager.MSTAR_MANUFACTURER_DETAILS:
                    APIServiceManager.getInstance().mStarManufacturerDetails(this, getManufacturerRequest());
                    break;
                case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                    if (product_code != 0) {
                        APIServiceManager.getInstance().mStarProductDetails(this, product_code, transactionId);
                    }
                    break;
                case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
                    APIServiceManager.getInstance().MstarGetBrandProductList(this, getBrandProductRequest());
                    break;
                case APIServiceManager.C_MSTAR_RETRIEVE_ANY_URL:
                    APIServiceManager.getInstance().mstarRetrieveProductsFromUrl(this, brandUrl, getMstarRetrieveRequest());
                    break;
                case AppServiceManager.ALGOLIA_SEARCH:
                    AppServiceManager.getInstance().algoliaSearch(ProductListViewModel.this, "", algoliaPageIndex,
                            (TextUtils.isEmpty(CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_HITS)) ? AppConstant.DEFAULT_ALGOLIA_HITS : Integer.valueOf(CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_HITS))), CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_API_KEY), getAlgoliaSortIndex(),
                            CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_APP_ID), false, getFacetFilterQuery(), getFilterQuery());
                    break;
            }
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        productListListener.vmDismissProgress();
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        switch (transactionId) {
            case APIServiceManager.MSTAR_MANUFACTURER_DETAILS:
                isManufactureAPIComplete = true;
                manufacturerDetailsMutableLiveData.setValue(response.getResult().getManufacturerDetails());
                break;
            case APIServiceManager.MSTAR_CATEGORY_DETAILS:
                isCategoryAPIComplete = true;
                if (response.getResult() != null && response.getResult().getCategoryDetails() != null)
                    mstarCategoryDetailsMutableLiveData.setValue(response.getResult().getCategoryDetails());
                break;
            case APIServiceManager.C_MSTAR_CATLOG_BANNERS:
                isCatlogBannerAPIComplete = true;
                setCatlogBannersResponse(data);
                break;
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                if (response.getResult() != null && response.getResult().getProductDetails() != null)
                    mStarProductDetailsMutableLiveData.setValue(response.getResult().getProductDetails());
                break;
            case AppServiceManager.ALGOLIA_SEARCH:
                MstarAlgoliaResponse mstarAlgoliaResponse = new Gson().fromJson(data, MstarAlgoliaResponse.class);
                algoliaMutableLiveData.setValue(mstarAlgoliaResponse);
                break;
            case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
                onBrandproductListDataAvailable(data);
                break;
            case APIServiceManager.C_MSTAR_RETRIEVE_ANY_URL:
                onRetrieveResponse(data);
                break;

        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        productListListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_MANUFACTURER_DETAILS:
            case APIServiceManager.MSTAR_CATEGORY_DETAILS:
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
            case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
            case APIServiceManager.C_MSTAR_CATLOG_BANNERS:
            case AppServiceManager.ALGOLIA_SEARCH:
                productListListener.showWebserviceError(true);
                break;
            case APIServiceManager.C_MSTAR_RETRIEVE_ANY_URL:
                if (!TextUtils.isEmpty(intentFrom))
                    productListListener.navigateToHome();
                else
                    productListListener.showWebserviceError(true);
                break;

        }
    }

    public RecyclerView.OnScrollListener getProductListPagenation(final LinearLayoutManager linearLayoutManager) {
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItem) >= totalItemCount && firstVisibleItem >= 0 && totalItemCount >= pageSize) {
                    if (isFromAlgolia) {
                        if (algoliaPageIndex < totalPageCount) {
                            algoliaPageIndex += 1;
                            setClearList(false);
                            getMoreProducts();
                        }
                    } else {
                        if (pageIndex < totalPageCount) {
                            pageIndex += 1;
                            setClearList(false);
                            getMoreProducts();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                visibleItemListener.viewVisiblePosition(firstVisibleItem, lastVisibleItem);
            }
        };
    }

    private MstarCategoryDetailsRequest getMstarRetrieveRequest() {
        MstarCategoryDetailsRequest request = new MstarCategoryDetailsRequest();
        request.setReturnFacets(AppConstant.YES);
        request.setReturnProduct(AppConstant.YES);
        request.setProductsSelector(AppConstant.NON_PRESC);
        request.setSubCategoryDepth(AppConstant.TWO);
        request.setPageNo(pageIndex);
        request.setPageSize(pageSize);
        request.setProductFieldSetForCategory(AppConstant.MFR_SMALL);
        request.setProductFieldSetForMfr(AppConstant.MFR_SMALL);
        request.setProductFieldSetForProduct(AppConstant.MFR_SMALL);
        request.setProductsSortOrder(AppConstant.NAME_POPULARITY);
        return request;
    }


    private void onRetrieveResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getBrandDetails() != null && response.getResult().getBrandDetails().getProductDetailsList() != null && response.getResult().getBrandDetails().getProductDetailsList().size() > 0) {
                brandId = response.getResult().getBrandDetails().getId() != null ? response.getResult().getBrandDetails().getId() : 0;
                if(!isCatlogBannerAPIComplete)
                    initMstarAPIcall(APIServiceManager.C_MSTAR_CATLOG_BANNERS);
                onProductListDataAvailable(response.getResult().getBrandDetails());
            }
        }
    }

    private void setCatlogBannersResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null ) {
                if(response.getResult().getCategoryBannerList() != null && response.getResult().getCategoryBannerList().size() > 0) {
                    mstarBannersList = response.getResult().getCategoryBannerList();
                }else if(response.getResult().getManufactureBannerList() != null && response.getResult().getManufactureBannerList().size() > 0) {
                    mstarBannersList = response.getResult().getManufactureBannerList();
                } else if(response.getResult().getBrandBannerList() != null && response.getResult().getBrandBannerList().size() > 0) {
                    mstarBannersList = response.getResult().getBrandBannerList();

                }
            }
            mstarCategoryProductAdapter = productListListener.setAdapter(this.mStarProductDetailsList != null && this.mStarProductDetailsList.size() > 0 ? this.mStarProductDetailsList : new ArrayList<MStarProductDetails>(), defaultBannerList != null && defaultBannerList.size() > 0 ? defaultBannerList : new ArrayList<String>(), mstarBannersList != null && mstarBannersList.size() > 0 ? mstarBannersList : new ArrayList<MstarBanner>(), false);
        }
    }


    public void getProductList(int categoryId) {
        this.mstarCategoryId = categoryId;
        isManufacture = false;
        pageIndex = 1;
        isFromAlgolia = false;
        isFromBanner =false;
        initMstarAPIcall(APIServiceManager.MSTAR_CATEGORY_DETAILS);
        initMstarAPIcall(APIServiceManager.C_MSTAR_CATLOG_BANNERS);
    }

    public void getManufactureList(int manufacturerId, String productType) {
        pageIndex = 1;
        isFromAlgolia = false;
        this.manufacturerId = manufacturerId;
        isManufacture = true;
        isFromBanner = false;
        this.productType = productType;
        initMstarAPIcall(APIServiceManager.MSTAR_MANUFACTURER_DETAILS);
        initMstarAPIcall(APIServiceManager.C_MSTAR_CATLOG_BANNERS);
    }


    public void getProductsFromBrandURL(String brandUrl) {
        this.brandUrl = brandUrl;
        pageIndex = 1;
        isFromAlgolia = false;
        isManufacture = false;
        isFromBanner = true;
        initMstarAPIcall(APIServiceManager.C_MSTAR_RETRIEVE_ANY_URL);
    }

    private MstarBrandProductListRequest getBrandProductRequest() {
        MstarBrandProductListRequest request = new MstarBrandProductListRequest();
        request.setBrandID(brandId);
        request.setReturnFacets(AppConstant.YES);
        request.setReturnProduct(AppConstant.NO);
        request.setPageNo(pageIndex);
        request.setPageSize(pageSize);
        return request;
    }

    public void getProductDetailRequest(int product_code) {
        this.product_code = product_code;
        initMstarAPIcall(APIServiceManager.MSTAR_PRODUCT_DETAILS);
    }

    private MstarManufacturerDetailsRequest getManufacturerRequest() {
        MstarManufacturerDetailsRequest request = new MstarManufacturerDetailsRequest();
        request.setManufacturerId(manufacturerId);
        request.setPageNo(pageIndex);
        request.setPageSize(pageSize);
        request.setReturnFacets(AppConstant.NO);
        request.setReturnProducts(AppConstant.YES);
        request.setProductFieldsetName(AppConstant.ALL);
        request.setProductsSortOrder(AppConstant.NAME_POPULARITY);
        request.setProductsSelector(CommonUtils.isProductTypeOTC(productType) ? AppConstant.NON_PRESC : AppConstant.PRESC);
        return request;
    }

    private MstarCategoryDetailsRequest getCategoryRequest() {
        MstarCategoryDetailsRequest request = new MstarCategoryDetailsRequest();
        request.setCategoryId(mstarCategoryId);
        request.setPageNo(pageIndex);
        request.setPageSize(pageSize);
        request.setProductFieldsetName(AppConstant.ALL);
        request.setReturnProduct(AppConstant.YES);
        request.setSubCategoryDepth(AppConstant.TWO);
        request.setReturnFacets(AppConstant.YES);
        request.setProductsSortOrder(AppConstant.NAME_POPULARITY);
        return request;
    }

    private boolean isClearList() {
        return clearList;
    }

    public void setClearList(boolean clearList) {
        this.clearList = clearList;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }


    public void navigateToHome() {
        productListListener.navigateToHome();
    }

    public MutableLiveData<MstarManufacturerDetails> getManufacturerDetailsMutableLiveData() {
        return manufacturerDetailsMutableLiveData;
    }


    public MutableLiveData<MStarProductDetails> getmStarProductDetailsMutableLiveData() {
        return mStarProductDetailsMutableLiveData;
    }

    public void setBrandId(int mstarBrandID) {
        this.brandId = mstarBrandID;
    }


    public Map<String, Map<String, String>> getAlgoliaFilterOption() {
        return algoliaFilterOption;
    }

    private void setAlgoliaFilterOption(Map<String, Map<String, String>> algoliaFilterOption) {
        this.algoliaFilterOption = algoliaFilterOption;
    }

    /**
     * Handling algolia response
     *
     * @param mstarAlgoliaResponse algolia response
     */
    public void onAlgoliaFilterDataAvailable(MstarAlgoliaResponse mstarAlgoliaResponse) {
        if (mstarAlgoliaResponse != null && mstarAlgoliaResponse.getNbHits() > 0) {
            totalPageCount = (long) mstarAlgoliaResponse.getNbPages() - 1;//total pages

            if (!isFacetValueFirst)
                setAlgoliaHitList(mstarAlgoliaResponse.getAlgoliaResultList());

            setFacetFilterOption(mstarAlgoliaResponse);
            productListListener.showEmptyView(false);
        } else {
            productListListener.showEmptyView(true);
        }
    }

    private void setFacetFilterOption(MstarAlgoliaResponse mstarAlgoliaResponse) {
        if (isFacetValueFirst && mstarAlgoliaResponse.getFacetList() != null && mstarAlgoliaResponse.getFacetList().size() > 0) {
            this.isFacetValueFirst = false;
            Map<String, Map<String, String>> filterFacetMap = new HashMap<>();
            if (getSubCategoryList() != null && getSubCategoryList().size() > 0) {
                Map<String, String> subCategoryMap = new HashMap<>();
                for (MstarSubCategoryResult subCategoryResult : getSubCategoryList()) {
                    subCategoryMap.put(subCategoryResult.getName(), String.valueOf(subCategoryResult.getProductCount()));
                }
                filterFacetMap.put(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL, subCategoryMap);
            }

            filterFacetMap.putAll(mstarAlgoliaResponse.getFacetList());
            setAlgoliaFilterOption(filterFacetMap);
        }
    }

    /**
     * This method used to make algolia filter based on filter selection
     */
    public void onApplyFilterDataAvailable() {
        setFilterApplied(true);
        setFilterQuery(getQuery());
        algoliaPageIndex = 0;//Algolia index
        isFromAlgolia = true;
        setClearList(true);
        initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
    }

    /**
     * set filter query based on filter selection
     *
     * @return filter query
     */
    private String getQuery() {
        StringBuilder manufactureQuery = new StringBuilder();
        StringBuilder priceQuery = new StringBuilder();
        StringBuilder availabilityQuery = new StringBuilder();
        StringBuilder categoryQuery = new StringBuilder();
        StringBuilder brandQuery = new StringBuilder();
        StringBuilder discountQuery = new StringBuilder();

        if (MstarFilterHelper.getAvailabilitySelection() != null && MstarFilterHelper.getAvailabilitySelection().size() > 0) {
            for (String availability : MstarFilterHelper.getAvailabilitySelection()) {
                if (availabilityQuery.length() > 1) {
                    availabilityQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK).append("\":\"").append(availability).append("\"");
                } else {
                    availabilityQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK + "\":\"" + availability + "\"");
                }
            }
        }
        if (MstarFilterHelper.getCategorySelection() != null && MstarFilterHelper.getCategorySelection().size() > 0) {
            for (String categoryName : MstarFilterHelper.getCategorySelection()) {
                if (categoryQuery.length() > 1) {
                    categoryQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_CATEGORIES).append("\":\"").append(categoryName).append("\"");
                } else {
                    if (availabilityQuery.length() > 1)
                        categoryQuery = new StringBuilder(" " + AppConstant.AND + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_CATEGORIES + "\":\"" + categoryName + "\"");
                    else
                        categoryQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_CATEGORIES + "\":\"" + categoryName + "\"");
                }
            }
        }

        if (MstarFilterHelper.getManufactureSelection() != null && MstarFilterHelper.getManufactureSelection().size() > 0) {
            for (String manufacture_name : MstarFilterHelper.getManufactureSelection()) {
                if (manufactureQuery.length() > 1) {
                    manufactureQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME).append("\":\"").append(manufacture_name).append("\"");
                } else {
                    if (availabilityQuery.length() > 1 || categoryQuery.length() > 1)
                        manufactureQuery = new StringBuilder(" " + AppConstant.AND + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME + "\":\"" + manufacture_name + "\"");
                    else
                        manufactureQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME + "\":\"" + manufacture_name + "\"");
                }
            }
        }

        if (MstarFilterHelper.getBrandSelection() != null && MstarFilterHelper.getBrandSelection().size() > 0) {
            for (String brand : MstarFilterHelper.getBrandSelection()) {
                if (brandQuery.length() > 1) {
                    brandQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_BRAND).append("\":\"").append(brand).append("\"");
                } else {
                    if (availabilityQuery.length() > 1 || categoryQuery.length() > 1 || manufactureQuery.length() > 1)
                        brandQuery = new StringBuilder(" " + AppConstant.AND + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_BRAND + "\":\"" + brand + "\"");
                    else
                        brandQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_BRAND + "\":\"" + brand + "\"");
                }
            }
        }

        if (MstarFilterHelper.getPriceSelection() != null && MstarFilterHelper.getPriceSelection().size() > 0) {
            for (String price : MstarFilterHelper.getPriceSelection()) {
                if (price.contains("-")) {
                    String[] priceSplit = price.split("-");
                    if (priceQuery.length() > 1) {
                        priceQuery.append(" ").append(AppConstant.OR).append(" ").append(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE).append(":").append(" ").append(priceSplit[0]).append(" ").append(AppConstant.TO).append(" ").append(priceSplit[1]);
                    } else {
                        if (availabilityQuery.length() > 1 || categoryQuery.length() > 1 || manufactureQuery.length() > 1 || brandQuery.length() > 1)
                            priceQuery = new StringBuilder(" " + AppConstant.AND + " " + AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE + ": " + priceSplit[0] + " " + AppConstant.TO + " " + priceSplit[1]);
                        else
                            priceQuery = new StringBuilder(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE + ": " + priceSplit[0] + " " + AppConstant.TO + " " + priceSplit[1]);
                    }
                }
            }
        }

        if (MstarFilterHelper.getDiscountSelection() != null && MstarFilterHelper.getDiscountSelection().size() > 0) {
            for (String discount : MstarFilterHelper.getDiscountSelection()) {
                if (discount.contains(" ")) {
                    String[] discountSplit = discount.split(" ");
                    String discountFrom = discountSplit.length > 0 ? discountSplit[0].replace("%", "").trim() : "";
                    String discountTo = discountSplit.length > 1 ? discountSplit[2].replace("%", "").trim() : "";
                    if (!TextUtils.isEmpty(discountFrom) && !TextUtils.isEmpty(discountTo)) {
                        if (discountQuery.length() > 1) {
                            discountQuery.append(" ").append(AppConstant.OR).append(" ").append(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT).append(":").append(" ").append(discountFrom).append(" ").append(AppConstant.TO).append(" ").append(discountTo);
                        } else {
                            if (availabilityQuery.length() > 1 || categoryQuery.length() > 1 || manufactureQuery.length() > 1 || brandQuery.length() > 1 || priceQuery.length() > 1)
                                discountQuery = new StringBuilder(" " + AppConstant.AND + " " + AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT + ": " + discountFrom + " " + AppConstant.TO + " " + discountTo);
                            else
                                discountQuery = new StringBuilder(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT + ": " + discountFrom + " " + AppConstant.TO + " " + discountTo);
                        }
                    }
                }
            }
        }
        return availabilityQuery.toString() + categoryQuery.toString() + manufactureQuery.toString() + brandQuery.toString() + priceQuery.toString() + discountQuery.toString();
    }

    /**
     * This method return facetFilter option like category ids,manufacturer id and brand id
     *
     * @return facetFilter jsonArray
     */
    private JSONArray getFacetFilterQuery() {
        JSONArray facetFilter = new JSONArray();
        switch (getIntentFrom()) {
            case AppConstant.CATEGORY:
                facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_IDS + ":" + mstarCategoryId);
                break;
            case AppConstant.MANUFACTURER:
                facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_ID + ":" + manufacturerId);
                break;
            case AppConstant.URL:
            case AppConstant.BRAND:
                facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_BRAND_ID + ":" + brandId);
                break;
            case AppConstant.ALTERNATE_SALT:
                facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_GENERIC_WITH_DOSAGE_ID + ":" + genericName);
                break;
        }
        return facetFilter;
    }

    private SortOptionLabel getSortOption() {
        return sortOption;
    }

    public void setSortOption(SortOptionLabel sortOption) {
        this.sortOption = sortOption;//set selected sort option
    }

    public List<SortOptionLabel> getSortOptionList() {
        return sortOptionList;
    }

    public void setSortOptionList(List<SortOptionLabel> sortOptionList) {
        this.sortOptionList = sortOptionList;//set sort option list
    }

    /**
     * This method used to make algolia sorting based on sort selection
     *
     * @param sortOption selected sort option
     */
    public void onSortFilterApplied(SortOptionLabel sortOption) {
        setSortOption(sortOption);
        setFilterApplied(true);
        algoliaPageIndex = 0; // algoliaIndex
        isFromAlgolia = true;
        setClearList(true);
        initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
    }

    /**
     * This method return sort index based on sort selection
     *
     * @return algolia sort index
     */
    private String getAlgoliaSortIndex() {
        if (getSortOptionList() != null && getSortOptionList().size() > 0) {
            for (SortOptionLabel sortOptionLabel : getSortOptionList()) {
                if (!TextUtils.isEmpty(getSortOption().getKey()) && sortOptionLabel.getKey().equals(getSortOption().getKey()))
                    return sortOptionLabel.getValue();
            }
        }
        return "";
    }

    public void onAlternateSaltDataAvailable(Intent intent) {
        productListListener.vmDismissProgress();
        if (intent != null && intent.getExtras() != null) {
            this.genericName = intent.getStringExtra(IntentConstant.ALTERNATE_SALT_FACET_FILTER_QUERY);
            setFilterQuery(intent.getStringExtra(IntentConstant.ALTERNATE_SALT_FILTER_QUERY));
            this.alternateSaltProductCode = intent.getIntExtra(IntentConstant.ALTERNATE_SALT_PRODUCT_CODE, 0);
            List<MstarAlgoliaResult> alternateSaltList = (List<MstarAlgoliaResult>) intent.getSerializableExtra(IntentConstant.ALTERNATE_SALT_LIST);
            productListListener.setTitle("");
            setClearList(true);
            algoliaPageIndex = 0; // algoliaIndex
            isFromAlgolia = true;
            setSortOption(null);
            setFilterApplied(false);
            setAlgoliaHitList(alternateSaltList);
        }
    }

    public void onPeopleAlsoViewedDataAvailable(Intent intent) {
        productListListener.vmDismissProgress();
        if (intent != null && intent.getExtras() != null) {
            ArrayList<HRVProduct> hrvProductArrayList = (ArrayList<HRVProduct>) intent.getSerializableExtra(IntentConstant.PEOPLE_ALSO_VIEWED_LIST);
            pageIndex = 1;
            isFromAlgolia = false;
            setSortOption(null);
            setFilterApplied(false);
            setPeopleAlsoViewedList(hrvProductArrayList);
        }
    }

    private void setPeopleAlsoViewedList(ArrayList<HRVProduct> hrvProductArrayList) {
        if (hrvProductArrayList != null && !hrvProductArrayList.isEmpty()) {
            List<MStarProductDetails> productDetailsList = new ArrayList<>();
            for (HRVProduct hrvProduct : hrvProductArrayList) {
                MStarProductDetails productDetails = new MStarProductDetails();
                productDetails.setProductCode(Integer.parseInt(hrvProduct.getId()));
                productDetails.setBrandName(hrvProduct.getBrandName());
                productDetails.setDisplayName(hrvProduct.getBrandName());
                productDetails.setUrlPath(hrvProduct.getImageUrl());
                productDetails.setSellingPrice(BigDecimal.valueOf(Double.valueOf(hrvProduct.getPrice())));
                if (!TextUtils.isEmpty(hrvProduct.getMisc())) {
                    BrainSinsMisc misc = new Gson().fromJson(hrvProduct.getMisc(), BrainSinsMisc.class);
                    String mrp = misc.getOriginalPrice().replaceAll(",", "");
                    productDetails.setMrp(new BigDecimal(mrp));
                }
                productDetails.setRxRequired(hrvProduct.getRxRequired() == 1);
                productDetails.setMaxQtyInOrder(hrvProduct.getMaxSelectQty());
                productDetails.setForPeopleAlsoViewed(true);
                productDetailsList.add(productDetails);
            }
            mstarCategoryProductAdapter = productListListener.setAdapter(productDetailsList.size() > 0 ? productDetailsList : new ArrayList<MStarProductDetails>(), defaultBannerList != null && defaultBannerList.size() > 0 ? defaultBannerList : new ArrayList<String>(), mstarBannersList != null && mstarBannersList.size() > 0 ? mstarBannersList : new ArrayList<MstarBanner>(), true);
        }
    }

    private void setAlgoliaHitList(List<MstarAlgoliaResult> algoliaHitList) {
        if (algoliaHitList != null && algoliaHitList.size() > 0) {
            List<MStarProductDetails> productDetailsList = new ArrayList<>();
            for (MstarAlgoliaResult algoliaResult : algoliaHitList) {
                MStarProductDetails productDetails = new MStarProductDetails();
                productDetails.setSellingPrice(algoliaResult.getSellingPrice());
                productDetails.setBrandName(!TextUtils.isEmpty(algoliaResult.getBrand()) ? algoliaResult.getBrand() : "");
                productDetails.setDisplayName(!TextUtils.isEmpty(algoliaResult.getDisplayName()) ? algoliaResult.getDisplayName() : "");
                productDetails.setManufacturerName(!TextUtils.isEmpty(algoliaResult.getManufacturerName()) ? algoliaResult.getManufacturerName() : "");
                productDetails.setMrp(algoliaResult.getMrp());
                productDetails.setMaxQtyInOrder(algoliaResult.getMaxQtyInOrder());
                productDetails.setAvailabilityStatus(!TextUtils.isEmpty(algoliaResult.getAvailabilityStatus()) ? algoliaResult.getAvailabilityStatus() : "");
                productDetails.setProductType(!TextUtils.isEmpty(algoliaResult.getProductType()) ? algoliaResult.getProductType() : "");
                productDetails.setFormulationType(!TextUtils.isEmpty(algoliaResult.getFormulationType()) ? algoliaResult.getFormulationType() : "");
                productDetails.setProductCode(algoliaResult.getProductCode());
                productDetails.setManufacturerId(algoliaResult.getManufacturerId());
                productDetails.setSchedule(!TextUtils.isEmpty(algoliaResult.getSchedule()) ? algoliaResult.getSchedule() : "");
                productDetails.setBrandId(!TextUtils.isEmpty(algoliaResult.getBrandId()) ? algoliaResult.getBrandId() : "");
                productDetails.setUrlPath(!TextUtils.isEmpty(algoliaResult.getUrlPath()) ? algoliaResult.getUrlPath() : "");
                productDetails.setPackLabel(!TextUtils.isEmpty(algoliaResult.getPackLabel()) ? algoliaResult.getPackLabel() : "");
                productDetails.setDiscount(algoliaResult.getDiscountRate());
                productDetails.setRxRequired(algoliaResult.getRxRequired() == 1);
                List<String> imagePath = new ArrayList<>();
                imagePath.add(!TextUtils.isEmpty(algoliaResult.getThumbnailUrl()) ? algoliaResult.getThumbnailUrl() : "");
                productDetails.setImagePaths(imagePath);
                MstarBaseIdUrl mstarBaseIdUrl = new MstarBaseIdUrl();
                mstarBaseIdUrl.setName(!TextUtils.isEmpty(algoliaResult.getManufacturerName()) ? algoliaResult.getManufacturerName() : "");
                mstarBaseIdUrl.setId(algoliaResult.getManufacturerId());
                productDetails.setManufacturer(mstarBaseIdUrl);
                if (algoliaResult.getCategoryTreeList() != null && algoliaResult.getCategoryTreeList().size() > 0) {
                    List<MStarCategory> categoriesList = new ArrayList<>();
                    MStarCategory mStarCategory = new MStarCategory();
                    mStarCategory.setName("");
                    List<MStarBreadCrumb> breadCrumbs = new ArrayList<>();
                    int level = 0;
                    for (Map.Entry<String, ArrayList<Object>> entry : algoliaResult.getCategoryTreeList().entrySet()) {
                        ArrayList<Object> levelList = entry != null && entry.getValue() != null && entry.getValue().size() > 0 ? entry.getValue() : new ArrayList<Object>();
                        if (levelList != null && levelList.size() > 0 && !TextUtils.isEmpty(levelList.get(0).toString().trim())) {
                            String[] categoryList = levelList.get(0).toString().split("///");
                            MStarBreadCrumb mStarBreadCrumb = new MStarBreadCrumb();
                            mStarBreadCrumb.setName(categoryList.length > 0 ? !TextUtils.isEmpty(Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1)) ? Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1) : "" : "");
                            mStarBreadCrumb.setLevel(level);
                            breadCrumbs.add(mStarBreadCrumb);
                            mStarCategory.setBreadCrumbs(breadCrumbs);
                        }
                        level++;
                    }
                    categoriesList.add(mStarCategory);
                    productDetails.setCategories(categoriesList);
                }
                if (getIntentFrom().equals(AppConstant.ALTERNATE_SALT)) {
                    if (alternateSaltProductCode != algoliaResult.getProductCode())
                        productDetailsList.add(productDetails);
                } else
                    productDetailsList.add(productDetails);
            }
            setSearchAdapter(getIntentFrom().equals(AppConstant.ALTERNATE_SALT) ? getPriceSortingForAlternateSalts(productDetailsList) : productDetailsList);
        }
    }

    private List<MStarProductDetails> getPriceSortingForAlternateSalts(List<MStarProductDetails> productDetailsList) {
        if (productDetailsList != null && productDetailsList.size() > 0) {
            Collections.sort(productDetailsList, new Comparator<MStarProductDetails>() {
                @Override
                public int compare(MStarProductDetails p1, MStarProductDetails p2) {
                    int compareStatus = p1.getAvailabilityStatus().compareTo(p2.getAvailabilityStatus());
                    return compareStatus == 0 ? Double.compare(p1.getSellingPrice().doubleValue(), p2.getSellingPrice().doubleValue()) : compareStatus;
                }
            });
            return productDetailsList;
        }
        return productDetailsList;
    }

    public void fetchFacetValueFromAlgolia() {
        if (!getIntentFrom().equals(AppConstant.ALTERNATE_SALT) && !getIntentFrom().equals(AppConstant.PEOPLE_ALSO_VIEWED)) {
            algoliaPageIndex = 0; // algoliaIndex
            initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
        }
    }

    private List<MstarSubCategoryResult> getSubCategoryList() {
        return subCategoryList;
    }

    private void setSubCategoryList(List<MstarSubCategoryResult> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }

    public interface ProductListListener {
        void vmShowProgress();

        void vmDismissProgress();

        void retry();

        void showEmptyView(boolean isEmpty);

        void setTitle(String title);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceError(boolean isWebError);

        MstarCategoryProductAdapter setAdapter(List<MStarProductDetails> mStarProductDetailsList, List<String> defaultBannerList, List<MstarBanner> mstarBannersList, boolean isPeopleAlsoView);

        void navigateToHome();

        boolean isNetworkConnected();

        Context getContext();
    }
}
