package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerRegistrationResponse;
import com.nms.netmeds.base.model.Request.MStarRegistrationRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.util.Map;

import static com.nms.netmeds.base.IntentConstant.EMAIL;
import static com.nms.netmeds.base.IntentConstant.FIRSTNAME;
import static com.nms.netmeds.base.IntentConstant.LASTNAME;

public class SignUpViewModel extends AppViewModel {
    private SignUpListener callback;
    private String phoneNumber;
    private int failedTransactionId;
    private Intent intent;
    private String randomKey = "";
    private boolean isFromTrueCaller;

    public void init(SignUpListener signUpListener, String phoneNumber, Intent intent) {
        this.callback = signUpListener;
        this.phoneNumber = phoneNumber;
        this.intent = intent;
    }

    public boolean isFromTrueCaller() {
        return isFromTrueCaller;
    }

    public void setFromTrueCaller(boolean fromTrueCaller) {
        isFromTrueCaller = fromTrueCaller;
    }

    public String getRandomKey() {
        return randomKey;
    }

    public void setRandomKey(String randomKey) {
        this.randomKey = randomKey;
    }

    public SignUpViewModel(@NonNull Application application) {
        super(application);
    }

    public void onEmailEditTextChanged() {
        callback.resetError(EDIT_TEXT.EMAIL);
    }

    public String getPhoneNumber() {
        return String.format(callback.getContext().getString(R.string.text_state_code), phoneNumber);
    }

    public void onNewPasswordEditTextChanged() {
        callback.resetError(EDIT_TEXT.PASSWORD);
    }

    public void onConfirmPasswordEditTextChanged() {
        callback.resetError(EDIT_TEXT.CONFIRM_PASSWORD);
    }

    public void onFirstNameEditTextChanged() {
        callback.resetError(EDIT_TEXT.F_NAME);
    }

    public void onLastNameEditTextChanged() {
        callback.resetError(EDIT_TEXT.L_NAME);
    }

    public void onPinCodeEditTextEdited(int position, int count) {
        callback.onPinCodeEdited(position, count);
    }

    public String getEmail() {
        return intent != null && !TextUtils.isEmpty(intent.getStringExtra(EMAIL)) ? intent.getStringExtra(EMAIL) : "";
    }

    public String getFirstName() {
        return intent != null && !TextUtils.isEmpty(intent.getStringExtra(FIRSTNAME)) ? intent.getStringExtra(FIRSTNAME) : "";
    }

    public String getLastName() {
        return intent != null && !TextUtils.isEmpty(intent.getStringExtra(LASTNAME)) ? intent.getStringExtra(LASTNAME) : "";

    }

    public void onClickVerify() {
        if (callback.validateForm())
            registration();
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.vmNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_CUSTOMER_REGISTRATION:
                    APIServiceManager.getInstance().mStarCustomerRegistration(this, getRegistrationRequest(), getPreference().isGuestCart() ? getMergeSessionId() : "", getPreference().getMstarGoogleAdvertisingId());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, getPreference().getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                    APIServiceManager.getInstance().mstarResendSameOTP(this, getRandomKey());
                    break;
            }
        } else {
            callback.vmDismissProgress();
            failedTransactionId = transactionId;
        }
    }

    public void onClickResendOtp() {
        sendOtp();
    }

    public void onClickChangeNumber() {
        callback.vmCallBackOnNavigationMobileNumberActivity();
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        onRetry(failedTransactionId);
        callback.vmWebserviceErrorView(false);
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                sendOtp();
                break;
            case APIServiceManager.MSTAR_CUSTOMER_REGISTRATION:
                registration();
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails();
                break;
        }
    }

    private void sendOtp() {
        callback.vmShowProgress();
        initiateAPICall(APIServiceManager.MSTAR_RESEND_SAME_OTP);
    }

    private void OtpResponse(String data) {
        callback.vmDismissProgress();
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getStatus() != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && response.getResult().getMstarOtpDetails() != null && response.getResult().getMstarOtpDetails().getRandomKey() != null) {
            callback.vmSnackBarMessage(getApplication().getResources().getString(R.string.text_otp_resend));
            callback.vmCounterTimer();
            callback.resetPinEditText();
            setRandomKey(response.getResult().getMstarOtpDetails().getRandomKey());

        } else if (response != null && response.getReason() != null && response.getReason().getReason_eng() != null) {
            callback.vmSnackBarMessage(response.getReason().getReason_eng());
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        callback.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                OtpResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_REGISTRATION:
                registrationResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
        }

    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callback.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
            case APIServiceManager.MSTAR_CUSTOMER_REGISTRATION:
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                vmOnError(transactionId);
                break;
        }
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        callback.vmWebserviceErrorView(true);
    }

    public void registration() {
        callback.vmShowProgress();
        initiateAPICall(APIServiceManager.MSTAR_CUSTOMER_REGISTRATION);
    }

    private MStarRegistrationRequest getRegistrationRequest() {
        MStarRegistrationRequest mStarRegistrationRequest = new MStarRegistrationRequest();
        mStarRegistrationRequest.setFirstName(callback.getFirstName());
        mStarRegistrationRequest.setLastName(callback.getLastName());
        mStarRegistrationRequest.setMobileNo(phoneNumber);
        mStarRegistrationRequest.setEmail(callback.getEmail());
        mStarRegistrationRequest.setRandomKey(getRandomKey());
        mStarRegistrationRequest.setOtp(callback.getOtp());
        mStarRegistrationRequest.setPassword(callback.getPassword());
        return mStarRegistrationRequest;
    }

    private void getCustomerDetails() {
        initiateAPICall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    private void setFailureResponse(MStarCustomerRegistrationResponse response) {
        callback.vmDismissProgress();
        if (response != null && response.getStatus() != null && AppConstant.API_FAILURE_STATUS.contains(response.getStatus()) && response.getmStarRegistrationReason() != null && response.getmStarRegistrationReason().getAdditional_info() != null) {
            callback.showTextErrors(response.getmStarRegistrationReason().getAdditional_info());
        } else if (response != null && response.getStatus() != null && AppConstant.API_FAILURE_STATUS.contains(response.getStatus()) && response.getmStarRegistrationReason() != null && !TextUtils.isEmpty(response.getmStarRegistrationReason().getReason_eng())) {
            callback.vmSnackBarMessage(response.getmStarRegistrationReason().getReason_eng());
        }
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(callback.getContext());
    }

    private String getMergeSessionId() {
        Map<String, String> headerMap = getPreference().getMstarBasicHeaderMap();
        return headerMap != null && !headerMap.isEmpty() ? headerMap.get(AppConstant.MSTAR_AUTH_TOKEN) : "";
    }

    private void registrationResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarCustomerRegistrationResponse registrationResponse = new Gson().fromJson(data, MStarCustomerRegistrationResponse.class);
            if (registrationResponse != null && registrationResponse.getStatus() != null) {
                switch (registrationResponse.getStatus()) {
                    case AppConstant.SUCCESS_STATUS:
                        setSuccessResponse(registrationResponse);
                        /*FireBase Analytics Event*/
                        FireBaseAnalyticsHelper.getInstance().sigupEvent(callback.getContext(), !TextUtils.isEmpty(PaymentHelper.getSigup_method()) ? PaymentHelper.getSigup_method() : AppConstant.PASSWORD_WITH_OTP);
                        break;
                    case AppConstant.API_MSTAR_FAILURE_STATUS:
                        setFailureResponse(registrationResponse);
                        break;
                }
            } else {
                callback.vmDismissProgress();
                vmOnError(APIServiceManager.MSTAR_CUSTOMER_REGISTRATION);
            }
        } else {
            callback.vmDismissProgress();
            vmOnError(APIServiceManager.MSTAR_CUSTOMER_REGISTRATION);
        }
    }

    private void setSuccessResponse(MStarCustomerRegistrationResponse registrationResponse) {
        String sessionId = registrationResponse.getResult() != null && !TextUtils.isEmpty(registrationResponse.getResult().getId()) ? registrationResponse.getResult().getId() : "";
        long customerId = registrationResponse.getResult() != null ? registrationResponse.getResult().getCustomerId() : 0;
        String loganSessionId = registrationResponse.getResult() != null && !TextUtils.isEmpty(registrationResponse.getResult().getLoganSessionId()) ? registrationResponse.getResult().getLoganSessionId() : "";

        getPreference().setMStarSessionId(sessionId, Long.toString(customerId));
        getPreference().setGuestCart(false);
        getPreference().setMstarCustomerId(Long.toString(customerId));
        getPreference().setLoganSession(loganSessionId);

        getCustomerDetails();
    }

    public enum EDIT_TEXT {EMAIL, PASSWORD, CONFIRM_PASSWORD, F_NAME, L_NAME}


    private void customerDetailsResponse(String data) {
        callback.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && addressData.getResult() != null && addressData.getResult().getCustomerDetails() != null) {
                getPreference().setPreviousCartShippingAddressId(addressData.getResult().getCustomerDetails().getPreferredShippingAddress());
                getPreference().setCustomerDetails(new Gson().toJson(addressData.getResult().getCustomerDetails()));

                //Set WebEngage Login
                WebEngageHelper.getInstance().setWebEngageLogin(getPreference(), isFromTrueCaller() ? WebEngageHelper.SOCIAL_WIDGET : WebEngageHelper.DIRECT_SIGN_UP, true, callback.getContext());
                //MAT Registration Event
                MATHelper.getInstance().loginAndRegistrationMATEvent(getPreference(), true);
                //Facebook Registration Event
                FacebookPixelHelper.getInstance().logRegistrationEvent(callback.getContext());
                //Google Analytics Event
                GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_REGISTERED_SUCCESSFUL, GoogleAnalyticsHelper.EVENT_LABEL_SIGN_UP);

                callback.vmNavigateToHome();
            }
        }
    }

    public interface SignUpListener {
        void vmCallBackOnNavigationMobileNumberActivity();

        void vmShowProgress();

        void vmDismissProgress();

        void vmCounterTimer();

        void vmSnackBarMessage(String message);

        Context getContext();

        void vmNavigateToHome();

        void vmNoNetworkView(boolean isConnected);

        void vmWebserviceErrorView(boolean isWebserviceError);

        void showTextErrors(Map<String, String> additional_info);

        boolean validateForm();

        void resetPinEditText();

        String getOtp();

        String getFirstName();

        String getLastName();

        String getEmail();

        String getPassword();

        void onPinCodeEdited(int position, int count);

        void resetError(EDIT_TEXT type);
    }
}
