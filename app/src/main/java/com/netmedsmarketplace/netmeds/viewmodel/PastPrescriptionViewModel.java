package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;

import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentPastPrescriptionBinding;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;

import java.util.ArrayList;
import java.util.List;

public class PastPrescriptionViewModel extends BaseViewModel {

    private PastPrescriptionListener mPastPrescriptionListener;
    private FragmentPastPrescriptionBinding pastPrescriptionBinding;
    private GetPastPrescriptionResult mPrescriptionList;

    public PastPrescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(GetPastPrescriptionResult prescriptionList, PastPrescriptionListener pastPrescriptionListener, FragmentPastPrescriptionBinding pastPrescriptionBinding) {
        this.mPastPrescriptionListener = pastPrescriptionListener;
        this.pastPrescriptionBinding = pastPrescriptionBinding;
        this.mPrescriptionList = prescriptionList;
        setView();
    }

    private void setView() {
        if (mPrescriptionList != null) {
            setRecentPrescriptionView();
            setDigitizedPrescriptionView();
            setViewVisibility();
        }
    }

    private void setRecentPrescriptionView() {
        if (mPrescriptionList.getPastOneWeekUnDigitizedRxCount() > 0 && mPrescriptionList.getPastOneWeekUnDigitizedRxList() != null && mPrescriptionList.getPastOneWeekUnDigitizedRxList().getRxId() != null
                && mPrescriptionList.getPastOneWeekUnDigitizedRxList().getRxId().size() > 0) {
            mPastPrescriptionListener.setRecentPrescription(mPrescriptionList.getPastOneWeekUnDigitizedRxList().getRxId(),false);
        }
    }

    private void setDigitizedPrescriptionView() {
        if (mPrescriptionList.getDigitizedPrescriptionCount() > 0 && mPrescriptionList.getDigitizedPrescriptionList() != null && mPrescriptionList.getDigitizedPrescriptionList().getRxId() != null
                && mPrescriptionList.getDigitizedPrescriptionList().getRxId().size() > 0) {
            mPastPrescriptionListener.setPastPrescriptionAdapter(mPrescriptionList.getDigitizedPrescriptionList().getRxId(),true);
        }
    }

    private void setViewVisibility() {
        pastPrescriptionBinding.recentPrescription.setVisibility(mPrescriptionList.getPastOneWeekUnDigitizedRxCount() > 0 ? View.VISIBLE : View.GONE);
        pastPrescriptionBinding.recentUploadClose.setVisibility(mPrescriptionList.getPastOneWeekUnDigitizedRxCount() > 0 ? View.VISIBLE : View.GONE);
        pastPrescriptionBinding.verifiedPrescription.setVisibility(mPrescriptionList.getDigitizedPrescriptionCount() > 0 ? View.VISIBLE : View.GONE);
        pastPrescriptionBinding.verifiedPrescriptionClose.setVisibility(mPrescriptionList.getPastOneWeekUnDigitizedRxCount() < 0 ? View.VISIBLE : View.GONE);
    }

    public void prescriptionSelection() {
        if (validateUploadedPrescription(mPastPrescriptionListener.getSelectedPrescriptionList())) {
            enableContinue(true);
            mPastPrescriptionListener.setSelectedPrescription();
            mPastPrescriptionListener.dismissDialog();
        } else {
            enableContinue(false);
        }
    }

    private boolean validateUploadedPrescription(List<MStarUploadPrescription> list) {
        boolean isSelected = false;
        if (list != null && list.size() > 0) {
            for (MStarUploadPrescription prescriptionList : list) {
                if (prescriptionList.isChecked()) {
                    isSelected = true;
                }
            }
            return isSelected;
        } else return false;
    }

    public void enableContinue(boolean enable) {
        pastPrescriptionBinding.doneButton.setBackgroundDrawable(enable ? getApplication().getResources().getDrawable(R.drawable.accent_button) : getApplication().getResources().getDrawable(R.drawable.secondary_button));
        pastPrescriptionBinding.doneButton.setEnabled(enable);
    }



    public void dismissDialog() {
        MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(new ArrayList<String>());
        mPastPrescriptionListener.dismissDialog();
    }

    public interface PastPrescriptionListener {

        void setRecentPrescription(List<String> recentPrescriptionList,boolean isDigitized);

        void setPastPrescriptionAdapter(List<String> digitizedPrescriptionList,boolean isDigitized);

        void dismissDialog();

        List<MStarUploadPrescription> getSelectedPrescriptionList();

        void setSelectedPrescription();

    }
}
