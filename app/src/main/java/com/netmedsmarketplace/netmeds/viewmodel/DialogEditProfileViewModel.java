package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.app.DatePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogEditProfileBinding;
import com.netmedsmarketplace.netmeds.model.UpdateMobileNoResponse;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.Calendar;
import java.util.Map;

public class DialogEditProfileViewModel extends AppViewModel {

    private DialogEditProfileViewModelListener listener;
    private BasePreference basePreference;
    private MStarCustomerDetails customerDetails;
    private Calendar calendar;
    private Context context;

    private final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            listener.setAgeText(DateTimeUtils.getInstance().calculateAgeFromDOB(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        }
    };
    private String[] genderArray;
    private int failedTransactionId = 0;
    private boolean isNetworkFailure = false;
    private boolean isAPIFailure = false;

    public DialogEditProfileViewModel(@NonNull Application application) {
        super(application);
    }

    public String getMailId() {
        return customerDetails != null ? customerDetails.getEmail() : "";
    }

    public String getMobileNo() {
        return customerDetails != null ? customerDetails.getMobileNo() : "";
    }

    public String getAge() {
        return customerDetails != null ? DateTimeUtils.getInstance().calculateAgeFromDOB(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)) : "";
    }

    public String getFirstName() {
        return customerDetails != null ? (TextUtils.isEmpty(customerDetails.getFirstName()) ? "" : customerDetails.getFirstName()) : "";
    }

    public String getLastName() {
        return customerDetails != null ? (TextUtils.isEmpty(customerDetails.getLastName()) ? "" : customerDetails.getLastName()) : "";
    }

    public void agaPickerDialog() {
        listener.agaPickerDialog(date, calendar);
    }

    public void init(DialogEditProfileViewModelListener listener, BasePreference basePreference) {
        this.listener = listener;
        this.basePreference = basePreference;
        initObjectsValue();
        this.context = getApplication();
    }

    private void initObjectsValue() {
        this.customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        this.calendar = Calendar.getInstance();
        genderArray = getApplication().getResources().getStringArray(R.array.text_gender_type);
        setDefaultProperties();
    }

    private void setDefaultProperties() {
        listener.setErrorWatcher();
        listener.setSpinnerAdapter(genderArray);
        updateFields();
    }

    private void updateFields() {
        if (customerDetails != null) {
            int genderPosition = customerDetails.getGender() != null ? customerDetails.getGender().equalsIgnoreCase(AppConstant.MALE) ? 1 : customerDetails.getGender().equalsIgnoreCase(AppConstant.FEMALE) ? 2 : customerDetails.getGender().equalsIgnoreCase(AppConstant.OTHERS) ? 3 : -1 : -1;
            listener.getBinding().spGender.setSelection(genderPosition);
            if (customerDetails.getDateOfBirth() != null) {
                calendar = DateTimeUtils.getInstance().getSeparatedCalendar(customerDetails.getDateOfBirth());
            }
        }
    }

    public void closeView() {
        listener.onClickDismissDialog();
    }

    public void onSaveChanges() {
        if (listener.validateChanges()) {
            if (isChangesHappened() || isMobileNoChanged()) {
                if (isChangesHappened()) {
                    updateCustomerDetails();
                    //  checkMobileNoUpdate();
                } else {
                    //todo
                    // checkMobileNoUpdate();
                }
            } else {
                listener.onClickDismissDialog();
            }
        }
    }

    private void updateCustomerDetails() {
        APIServiceManager.getInstance().mstarUpdateCustomer(this, basePreference.getMstarBasicHeaderMap(), getUpdateCustomerRequest());
    }

    private void checkMobileNoUpdate() {
        if (isMobileNoChanged()) {
            listener.showLoader();
            mobileNoUpdate();
        }
    }

    private void mobileNoUpdate() {
        Boolean isConnected = NetworkUtils.isConnected(context);
        if (isConnected) {
            Toast.makeText(getApplication(), "Unable to update Mobile number", Toast.LENGTH_SHORT).show();
        } else {
            isNetworkFailure = !isConnected;
            isAPIFailure = isConnected;
            failedTransactionId = AppServiceManager.MOBILE_NO_UPDATE;
            listener.networkError(failedTransactionId, isNetworkFailure, isAPIFailure);
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case AppServiceManager.MOBILE_NO_UPDATE:
                updateMobileNo(data);
                break;
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                if (!TextUtils.isEmpty(data)) {
                    MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                    onUpdateCustomerResponse(response);
                }
        }
    }

    private void onUpdateCustomerResponse(MStarBasicResponseTemplateModel response) {
        if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
            listener.onClickDismissDialog();
            listener.showSnackMessage(context.getString(R.string.text_update_customer_success), false);
        } else if (response != null && response.getStatus() != null && AppConstant.API_FAILURE_STATUS.contains(response.getStatus()) && response.getReason() != null && response.getReason().getAdditional_info() != null) {
            listener.showTextErrors(response.getReason().getAdditional_info());
        } else {
            listener.showSnackMessage(context.getString(R.string.text_update_customer_error), true);
        }
    }

    private void updateMobileNo(String responseData) {
        listener.dismissLoader();
        UpdateMobileNoResponse updateMobileNoResponse = new Gson().fromJson(responseData, UpdateMobileNoResponse.class);
        if (updateMobileNoResponse.getUpdateMobileNoStatusResponse().get(0).isCustomerStatus()) {
            listener.getBinding().ilMobileNo.setError(updateMobileNoResponse.getUpdateMobileNoStatusResponse().get(0).getMessage());
        } else {
            listener.navigateToVerifyMobileNo(listener.getMobileNumber());
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        isNetworkFailure = false;
        isAPIFailure = true;
        failedTransactionId = AppServiceManager.MOBILE_NO_UPDATE;
        listener.networkError(failedTransactionId, isNetworkFailure, isAPIFailure);
    }

    private boolean isMobileNoChanged() {
        return !customerDetails.getMobileNo().equals(listener.getMobileNumber());
    }

    private boolean isChangesHappened() {
        Calendar serverCalendar = (customerDetails.getDateOfBirth() != null) ? DateTimeUtils.getInstance().getSeparatedCalendar(customerDetails.getDateOfBirth()) : Calendar.getInstance();
        int genderValue = customerDetails.getGender() != null ? customerDetails.getGender().equalsIgnoreCase("m") ? 1 : customerDetails.getGender().equalsIgnoreCase("f") ? 2 : -1 : -1;
        if (!((TextUtils.isEmpty(customerDetails.getFirstName()) ? "" : customerDetails.getFirstName()).equals(listener.getFirstName()) && ((TextUtils.isEmpty(customerDetails.getLastName()) ? "" : customerDetails.getLastName()).equals(listener.getLastName())))) {
            return true;
        } else if (genderValue != listener.getGenderSelectedValue()) {
            return true;
        } else
            return (serverCalendar.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH)) ||
                    (serverCalendar.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) ||
                    (serverCalendar.get(Calendar.YEAR) != calendar.get(Calendar.YEAR));
    }


    private MstarUpdateCustomerRequest getUpdateCustomerRequest() {
        MstarUpdateCustomerRequest request = new MstarUpdateCustomerRequest();
        request.setFirstName(listener.getFirstName());
        request.setLastName(listener.getLastName());
        request.setGender(listener.getGender());
        request.setEmail(listener.getEmail());
        request.setJuspayId(customerDetails.getJuspayId());
        request.setPaytmCustomerToken(customerDetails.getPaytmCustomerToken());
        request.setDob(getDobString());
        return request;
    }

    private String getDobString() {
        return DateTimeUtils.getInstance().CalenderToDateString(calendar, DateTimeUtils.yyyyMMdd);
    }

    public interface DialogEditProfileViewModelListener {
        void onClickDismissDialog();

        void showLoader();

        void dismissLoader();

        void navigateToVerifyMobileNo(String mobileNo);

        void networkError(int failedTransactionId, boolean isNetworkFailure, boolean isAPIFailure);

        String getFirstName();

        String getLastName();

        String getGender();

        boolean validateChanges();

        String getDob();

        void setAgeText(String age);

        String getMobileNumber();

        void setErrorWatcher();

        int getGenderSelectedValue();

        void setSpinnerAdapter(String[] genderArray);

        DialogEditProfileBinding getBinding();

        void agaPickerDialog(DatePickerDialog.OnDateSetListener date, Calendar calendar);

        String getEmail();

        void showSnackMessage(String string, boolean isForEditDialog);

        void showTextErrors(Map<String, String> additional_info);
    }
}
