package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.OrderTrackStatusAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityTrackOrderDetailsBinding;
import com.netmedsmarketplace.netmeds.model.BoomrangResponse;
import com.netmedsmarketplace.netmeds.model.BoomrangResult;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.CancelOrderResponseModel;
import com.nms.netmeds.base.model.ClickPostTrackStatusResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;
import com.nms.netmeds.base.model.MstarOrderShippingAddress;
import com.nms.netmeds.base.model.MstarOrderTrackDetail;
import com.nms.netmeds.base.model.MstarOrderTrackStatus;
import com.nms.netmeds.base.model.MstarOrders;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;

public class TrackOrderDetailsViewModel extends AppViewModel implements OrderTrackStatusAdapter.OrderTrackStatusAdapterListener {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityTrackOrderDetailsBinding binding;
    private TrackOrderDetailsListener listener;
    private BasePreference basePreference;
    private MstarOrderDetailsResult orderTrackDetails;
    private MstarOrderTrackDetail trackOrderDetails;

    private final MutableLiveData<MstarOrderDetailsResult> trackOrderMutableLiveData = new MutableLiveData<>();
    private Map<Integer, MstarOrderTrackDetail> trackDetailsUpdateMap = new HashMap<>();

    private String orderId;
    private String cancelMessage;
    private String updatedLocation;
    private String updatedDate;
    private String boomRangDate = "";
    private int tabSelectedPosition = 0;
    private int failedTransactionId = 0;
    private int trackOrderPositionFromViewOrderDetails;
    private int cancelMessageIndex;
    private boolean isFromSubscription;
    private boolean isFromDeeplinking;
    private MstarOrders cancelOrderDetail;

    public TrackOrderDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityTrackOrderDetailsBinding binding, String orderId, int trackOrderPositionFromViewOrderDetails, boolean isFromSubscription, TrackOrderDetailsListener listener, boolean isFromDeeplinking) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        this.orderId = orderId;
        this.isFromDeeplinking = isFromDeeplinking;
        this.trackOrderPositionFromViewOrderDetails = trackOrderPositionFromViewOrderDetails;
        this.isFromSubscription = isFromSubscription;
        this.basePreference = BasePreference.getInstance(context);
        this.boomRangDate = PaymentHelper.getBoomDate();
        fetchOrderTrackDetails();
    }

    public boolean isSplitOrder() {
        return orderTrackDetails.getTrackDetails().size() > 1;
    }

    public boolean isOnTrack() {
        return !TextUtils.isEmpty(getDeliveryDate()) && !isNegativeOrderStatus() && !isSubscriptionDeliveryPendingOrder() && !TextUtils.isEmpty(boomRangDate);
    }

    public String getOrderDeliveryLabelName() {
        return context.getString(isOrderDelivered() ? R.string.text_order_delivered_label : R.string.text_order_delivery);
    }

    public String getDeliveryDate() {
        if (orderTrackDetails != null && orderTrackDetails.getOrderStatusCode() != null && orderTrackDetails.getOrderStatusCode().equalsIgnoreCase(AppConstant.INTRANSIT)) {
            return !TextUtils.isEmpty(PaymentHelper.getBoomDate()) ? PaymentHelper.getBoomDate() : "";
        }
        return orderTrackDetails != null && !TextUtils.isEmpty(orderTrackDetails.getExpectedDeliveryDateString()) ? orderTrackDetails.getExpectedDeliveryDateString() : "";
    }

    private MstarOrderTrackDetail getTrackDetail() {
        return trackOrderDetails;
    }

    public String getNoOfItemContains() {
        if (trackOrderDetails != null && trackOrderDetails.getProductCount() != null && Integer.valueOf(trackOrderDetails.getProductCount()) > 0) {
            return context.getResources().getQuantityString(R.plurals.text_order_contain_item_count, Integer.valueOf(!TextUtils.isEmpty(trackOrderDetails.getProductCount()) ? trackOrderDetails.getProductCount() : "0"), Integer.valueOf(!TextUtils.isEmpty(trackOrderDetails.getProductCount()) ? trackOrderDetails.getProductCount() : "0"));
        } else {
            return "";
        }
    }


    public boolean getOrderContainsVisibility() {
        return orderTrackDetails != null && orderTrackDetails.getTrackDetails() != null && trackOrderDetails != null && trackOrderDetails.getProductCount() != null && Integer.valueOf(trackOrderDetails.getProductCount()) > 0;
    }

    public String getContactPersonName() {
        return String.format("%s %s",
                CommonUtils.getValidStringWithFirstLetterUpperCase(orderTrackDetails != null ? (orderTrackDetails.getOrderShippingInfo() != null ? (!TextUtils.isEmpty(orderTrackDetails.getOrderShippingInfo().getShippingFirstName()) ? orderTrackDetails.getOrderShippingInfo().getShippingFirstName() : "") : "") : ""),
                CommonUtils.getValidStringWithFirstLetterUpperCase(orderTrackDetails != null ? (orderTrackDetails.getOrderShippingInfo() != null ? (!TextUtils.isEmpty(orderTrackDetails.getOrderShippingInfo().getShippingLastName()) ? orderTrackDetails.getOrderShippingInfo().getShippingLastName() : "") : "") : ""));
    }

    public String getDeliveryAddress() {
        return orderTrackDetails != null ? (orderTrackDetails.getOrderShippingInfo() != null ? getFormatDeliveryAddress(orderTrackDetails.getOrderShippingInfo()) : "") : "";
    }

    private void fetchOrderTrackDetails() {
        initMstarApiCall(APIServiceManager.C_MSTAR_TRACK_ORDER);
    }

    public boolean isSubscriptionDeliveryPendingOrder() {
        if (trackOrderDetails != null && trackOrderDetails.getOrderId() != null) {
            return trackOrderDetails.getOrderId().contains(AppConstant.SRF);
        } else {
            return false;
        }
    }

    private void initMstarApiCall(int callId) {
        failedTransactionId = callId;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            listener.showLoader();
            switch (callId) {
                case APIServiceManager.C_MSTAR_TRACK_ORDER:
                    showAndHideMainView(false);
                    binding.shimmerView.setVisibility(View.VISIBLE);
                    binding.shimmerView.startShimmerAnimation();
                    APIServiceManager.getInstance().MstarGetOrderTrackDetails(this, basePreference.getMstarBasicHeaderMap(), getTrackOrderRequest());
                    break;
                case APIServiceManager.C_MSTAR_CANCEL_ORDER:
                    APIServiceManager.getInstance().MstarCancelOrder(this, basePreference.getMstarBasicHeaderMap(), getOrderCancelRequest());
                    break;
                case AppServiceManager.BOOMERANG:
                    AppServiceManager.getInstance().boomerang(this, orderId);
                    break;
                case AppServiceManager.CLICK_POST:
                    listener.showLoader();
                    ConfigMap configMap = ConfigMap.getInstance();
                    String trackUrl = String.format(configMap.getProperty(ConfigConstant.CLICK_POST_URL));
                    AppServiceManager.getInstance().getClickPostDetails(this, trackUrl, !TextUtils.isEmpty(trackOrderDetails.getClickPostId()) ? trackOrderDetails.getClickPostId() : "", trackOrderDetails.getTrackingNumber(), context);
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.dismissLoader();
        switch (transactionId) {
            case AppServiceManager.BOOMERANG:
                BoomrangResponse boomrangResponse = new Gson().fromJson(data, BoomrangResponse.class);
                boomRangResponse(boomrangResponse);
                break;
            case APIServiceManager.C_MSTAR_TRACK_ORDER:
                trackOrderSuccess(data);
                break;
            case APIServiceManager.C_MSTAR_CANCEL_ORDER:
                cancelOrderSuccess(data);
                break;
            case AppServiceManager.CLICK_POST:
                clickPostResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.dismissLoader();
        failedTransactionId = transactionId;
        binding.shimmerView.setVisibility(View.GONE);
        binding.shimmerView.stopShimmerAnimation();
        if (AppServiceManager.CLICK_POST == transactionId) {
            checkFinalProcess();
        } else {
            showApiError(transactionId);
        }
    }

    @Override
    public void onRetryClickListener() {
        showWebserviceErrorView(false);
        fetchOrderTrackDetails();
    }

    private MultipartBody getOrderCancelRequest() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.ORDER_ID, orderId).addFormDataPart(AppConstant.CUSTOMER_MSG, cancelMessage).addFormDataPart(AppConstant.REASON, String.valueOf(cancelMessageIndex)).build();
    }

    private MultipartBody getTrackOrderRequest() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.ORDER_ID, orderId).build();
    }

    public void cancelOrder(String cancelMessage, int cancelMessageIndex) {
        this.cancelMessage = cancelMessage;
        this.cancelMessageIndex = cancelMessageIndex;
        initMstarApiCall(APIServiceManager.C_MSTAR_CANCEL_ORDER);
    }


    public void boomRangResponse(BoomrangResponse boomrangResponse) {
        if (boomrangResponse != null) {
            listener.dismissLoader();
            BoomrangResult result = boomrangResponse.getResult();
            boomRangDate = result != null && !TextUtils.isEmpty(result.getDate()) ? result.getDate() : "";
            PaymentHelper.setBoomDate(boomRangDate);
            checkFinalProcess();
        }
    }

    private void trackOrderSuccess(String data) {
        binding.shimmerView.setVisibility(View.GONE);
        binding.shimmerView.stopShimmerAnimation();
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (AppConstant.API_SUCCESS_STATUS.equals(response.getStatus())) {
            if (response.getResult() != null && response.getResult().getOrderTrackDetails() != null) {
                showAndHideMainView(true);
                orderTrackDetails = response.getResult().getOrderTrackDetails();
                trackOrderDetails = response.getResult().getOrderTrackDetails().getTrackDetails().get(0);
                LoadSingleOrSplitOrderDetails();
                trackOrderMutableLiveData.setValue(orderTrackDetails);
            } else {
                if (isFromDeeplinking) {
                    listener.onEmptyOrderCallBackFromDeeplinking();
                }
            }
        } else {
            listener.showSnackbar(response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng()) ? response.getReason().getReason_eng() : "");
        }
    }

    private void cancelOrderSuccess(String data) {
        CancelOrderResponseModel cancelOrderResponse = new Gson().fromJson(data, CancelOrderResponseModel.class);
        String message = cancelOrderResponse.getStatus();
        if (AppConstant.API_SUCCESS_STATUS.toLowerCase().equals(cancelOrderResponse.getStatus().toLowerCase())) {
            /*Google Tag Manger + FireBase Refund Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseRefundsEvent(context,getCancelOrderDetail());
            listener.cancelOrderSuccessful(message);
        } else {
            listener.showSnackbar(message);
        }
    }

    private void clickPostResponse(String data) {
        try {
            JSONObject getResponseObj = new JSONObject(data);
            if (getResponseObj.has("meta") && getResponseObj.getJSONObject("meta").getBoolean("success")) {
                JSONObject resultObj = getResponseObj.getJSONObject("result");
                JSONObject trackIdObj = resultObj.getJSONObject(trackOrderDetails.getTrackingNumber());
                ClickPostTrackStatusResponse clickPostTrackStatusResponse = new Gson().fromJson(trackIdObj.getString("latest_status"), ClickPostTrackStatusResponse.class);
                if (clickPostTrackStatusResponse != null && !TextUtils.isEmpty(clickPostTrackStatusResponse.getLocation()) && !TextUtils.isEmpty(clickPostTrackStatusResponse.getTimestamp())) {
                    String trackStatus = !TextUtils.isEmpty(clickPostTrackStatusResponse.getTimestamp()) ? "Last Updated : " + clickPostTrackStatusResponse.getTimestamp() : "";
                    trackStatus += !TextUtils.isEmpty(clickPostTrackStatusResponse.getLocation()) ? "\n" + "Location : " + clickPostTrackStatusResponse.getLocation() : "";
                    updatedLocation = "Location : " + clickPostTrackStatusResponse.getLocation();
                    updatedDate = DateTimeUtils.getInstance().stringDate(clickPostTrackStatusResponse.getTimestamp(), DateTimeUtils.yyyyMMddHHmmss, DateTimeUtils.yyyyMMddTHHmmss);
                    checkFinalProcess();
                } else {
                    checkFinalProcess();
                }
            } else {
                if (!TextUtils.isEmpty(PaymentHelper.getBoomDate())) {
                    initMstarApiCall(AppServiceManager.BOOMERANG);
                } else
                    checkFinalProcess();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            checkFinalProcess();
        }
    }

    private void checkFinalProcess() {
        if (trackOrderDetails.getTrackStatusDetails().size() > 2) {
            addTrackIdWithClickPost(trackOrderDetails.getTrackStatusDetails().size() - 2);
            inflateOrderTrackLayouts();
        }
    }

    private void addTrackIdWithClickPost(int position) {
        trackOrderDetails.getTrackStatusDetails().get(position).setTrackIdVisibility(!TextUtils.isEmpty(trackOrderDetails.getTrackingNumber()) ? View.VISIBLE : View.GONE);
        trackOrderDetails.getTrackStatusDetails().get(position).setClickPostTrackId(trackOrderDetails.getClickPostId());
        trackOrderDetails.getTrackStatusDetails().get(position).setTrackUrl(trackOrderDetails.getTrackingUrl());
        trackOrderDetails.getTrackStatusDetails().get(position).setTrackId(TextUtils.isEmpty(updatedLocation) ? !TextUtils.isEmpty(trackOrderDetails.getTrackingNumber()) ? String.format(context.getString(R.string.text_track_id), trackOrderDetails.getTrackingNumber()) : "" : updatedLocation);
    }

    public MutableLiveData<MstarOrderDetailsResult> getTrackOrderMutableLiveData() {
        return trackOrderMutableLiveData;
    }

    private void LoadSingleOrSplitOrderDetails() {
        int subOrderSize = orderTrackDetails.getTrackDetails().size();
        loadSplitOrderTrackDetails();
        for (int i = 0; i < subOrderSize; i++) {
            binding.tabs.addTab(binding.tabs.newTab().setText(String.format(context.getString(R.string.text_orders_count), i + 1, subOrderSize)));
        }
        tabSelectedPosition = trackOrderPositionFromViewOrderDetails;
        binding.tabs.getTabAt(tabSelectedPosition).select();
    }

    private void loadSplitOrderTrackDetails() {
        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabSelectedPosition = binding.tabs.getSelectedTabPosition();
                listener.refreshPage();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void initiateOrderTrackDetails() {
        boolean isClickPostAvailable = false;
        boolean isOrderReachedIntransitStatus = false;
        if (trackDetailsUpdateMap != null && !trackDetailsUpdateMap.containsKey(tabSelectedPosition)) {
            trackOrderDetails = orderTrackDetails.getTrackDetails().get(tabSelectedPosition);
            String orderId = orderTrackDetails.getOrderId();
            if (orderTrackDetails != null && orderTrackDetails.getTrackDetails() != null && orderTrackDetails.getTrackDetails().size() > 0 && orderTrackDetails.getTrackDetails() != null && orderTrackDetails.getTrackDetails().size() > 0 && orderTrackDetails.getTrackDetails() != null && orderTrackDetails.getTrackDetails().size() > 0 && orderTrackDetails.getTrackDetails().get(tabSelectedPosition).getTrackStatusDetails() != null && orderTrackDetails.getTrackDetails().get(tabSelectedPosition).getTrackStatusDetails().size() > 0) {
                String drugStatusCode = orderTrackDetails.getTrackDetails().get(tabSelectedPosition).getTrackStatusDetails().get(orderTrackDetails.getTrackDetails().get(tabSelectedPosition).getTrackStatusDetails().size() - 1).getShortStatus();
                if (AppConstant.ORDER_PLACE.equals(drugStatusCode)) {
                    if (!isSubscriptionDeliveryPendingOrder()) {
                        trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_awaiting_prescription, AppConstant.AWAITING_PRESCRIPTION, ""));
                    }
                    trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_processing, AppConstant.PROCESSING, ""));
                    trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_in_transit, AppConstant.SHIPPED, ""));
                    trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_delivered, AppConstant.DELIVERED, ""));
                } else if (AppConstant.PRESCRIPTION_STATUS_PENDING.equals(drugStatusCode) || AppConstant.AWAITING_PRESCRIPTION.equals(drugStatusCode)) {
                    if (AppConstant.M2_ORDER.equalsIgnoreCase(String.valueOf(orderId.charAt(orderId.length() - 1))) && AppConstant.CONSULTATION_SCHEDULED.equalsIgnoreCase(orderTrackDetails.getOrderStatusCode())) {
                        trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_payment_pending, AppConstant.PAYMENT_PENDING, ""));
                    }
                    addRemainingTrackStatus();
                } else if (AppConstant.PAYMENT_PENDING.equals(drugStatusCode) || AppConstant.CONSULTATION_SCHEDULED.equals(drugStatusCode)) {
                    addRemainingTrackStatus();
                } else if (AppConstant.PROCESSING.equals(drugStatusCode)) {
                    trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_in_transit, AppConstant.SHIPPED, ""));
                    trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_delivered, AppConstant.DELIVERED, ""));
                } else if (AppConstant.SHIPPED.equals(drugStatusCode)) {
                    isClickPostAvailable = true;
                    callClickPostDetails();
                    trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_delivered, AppConstant.DELIVERED, ""));
                } else if (AppConstant.DELIVERED.equals(drugStatusCode)) {
                    isClickPostAvailable = true;
                    callClickPostDetails();
                }
                trackDetailsUpdateMap.put(tabSelectedPosition, trackOrderDetails);
            }
        }
        if (!isClickPostAvailable) {
            trackOrderDetails = orderTrackDetails.getTrackDetails().get(tabSelectedPosition);
            boolean isClickPostDetailsAvailable = false;
            if (trackDetailsUpdateMap != null && trackDetailsUpdateMap.containsKey(tabSelectedPosition)) {
                for (MstarOrderTrackStatus trackStatusDetail : trackDetailsUpdateMap.get(tabSelectedPosition).getTrackStatusDetails()) {
                    if (!isOrderReachedIntransitStatus) {
                        isOrderReachedIntransitStatus = trackStatusDetail.getShortStatus().equalsIgnoreCase(AppConstant.SHIPPED);
                    }
                    if (!isClickPostDetailsAvailable && trackStatusDetail.getShortStatus().equalsIgnoreCase(AppConstant.SHIPPED)) {
                        isClickPostDetailsAvailable = !TextUtils.isEmpty(trackStatusDetail.getTrackId());
                    }
                }
            }
            if (isClickPostDetailsAvailable && isOrderReachedIntransitStatus) {
                inflateOrderTrackLayouts();
            } else {
                if (!isOrderReachedIntransitStatus) {
                    inflateOrderTrackLayouts();
                } else {
                    initMstarApiCall(AppServiceManager.CLICK_POST);
                }
            }
        }
    }

    private void callClickPostDetails() {
        initMstarApiCall(AppServiceManager.CLICK_POST);
    }

    private void addRemainingTrackStatus() {
        trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_processing, AppConstant.PROCESSING, ""));
        trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_in_transit, AppConstant.SHIPPED, ""));
        trackOrderDetails.getTrackStatusDetails().add(getRemainingTrackStatus(R.string.text_order_track_delivered, AppConstant.DELIVERED, ""));
    }

    @Override
    public void onClickTrackStatus(String url) {
        listener.onLaunchTrackStatusInfo(url);
    }

    private MstarOrderTrackStatus getRemainingTrackStatus(int statusMessageId, String
            statusCode, String trackId) {
        MstarOrderTrackStatus trackStatusDetail = new MstarOrderTrackStatus();
        trackStatusDetail.setStatus(context.getString(statusMessageId));
        trackStatusDetail.setShortStatus(statusCode);
        return trackStatusDetail;
    }

    private void inflateOrderTrackLayouts() {
        if (trackDetailsUpdateMap != null && trackDetailsUpdateMap.get(tabSelectedPosition) != null && trackDetailsUpdateMap.get(tabSelectedPosition).getTrackStatusDetails() != null && !trackDetailsUpdateMap.get(tabSelectedPosition).getTrackStatusDetails().isEmpty()) {
            binding.cvTrackOrder.setVisibility(View.VISIBLE);
            OrderTrackStatusAdapter adapter = new OrderTrackStatusAdapter(context, trackDetailsUpdateMap.get(tabSelectedPosition).getTrackStatusDetails(), getActiveDrawables(), getInActiveDrawables(), this);
            binding.rvOrderTrackStatus.setLayoutManager(new LinearLayoutManager(context));
            binding.rvOrderTrackStatus.setNestedScrollingEnabled(false);
            binding.rvOrderTrackStatus.setAdapter(adapter);
        } else {
            binding.cvTrackOrder.setVisibility(View.GONE);
        }
    }

    private Map<String, Integer> getInActiveDrawables() {
        Map<String, Integer> map = new HashMap<>();
        map.put(AppConstant.ORDER_PLACE, R.drawable.ic_shopping_cart_added_inactive);
        map.put(AppConstant.AWAITING_PRESCRIPTION, R.drawable.ic_rx_primary_inactive);
        map.put(AppConstant.PRESCRIPTION_STATUS_PENDING, R.drawable.ic_rx_primary_inactive);
        map.put(AppConstant.PAYMENT_PENDING, R.drawable.ic_rupee_inactive);
        map.put(AppConstant.PAYMENT_RECEIVED, R.drawable.ic_rupee_inactive);
        map.put(AppConstant.PROCESSING, R.drawable.ic_order_in_process_inactive);
        map.put(AppConstant.SHIPPED, R.drawable.ic_payment_rocket_inactive);
        map.put(AppConstant.DELIVERED, R.drawable.ic_delivery_inactive);
        map.put(AppConstant.CANCELLED, R.drawable.ic_clear);
        map.put(AppConstant.DECLINED, R.drawable.ic_clear);
        return map;
    }

    private Map<String, Integer> getActiveDrawables() {
        Map<String, Integer> map = new HashMap<>();
        map.put(AppConstant.ORDER_PLACE, R.drawable.ic_payment_shopping_cart_added_active);
        map.put(AppConstant.AWAITING_PRESCRIPTION, R.drawable.ic_rx_primary_active);
        map.put(AppConstant.PRESCRIPTION_STATUS_PENDING, R.drawable.ic_rx_primary_active);
        map.put(AppConstant.PAYMENT_PENDING, R.drawable.ic_payment_rupee_active);
        map.put(AppConstant.PAYMENT_RECEIVED, R.drawable.ic_payment_rupee_active);
        map.put(AppConstant.PROCESSING, R.drawable.ic_order_in_process_active);
        map.put(AppConstant.SHIPPED, R.drawable.ic_payment_rocket_active);
        map.put(AppConstant.DELIVERED, R.drawable.ic_delivery_active);
        map.put(AppConstant.CANCELLED, R.drawable.ic_clear);
        map.put(AppConstant.DECLINED, R.drawable.ic_clear);
        return map;
    }

    public void onViewClick() {
        listener.onCallbackOfViewItemsInOrder(orderId);
    }

    public boolean isNegativeOrderStatus() {
        MstarOrderTrackDetail trackDetail = getTrackDetail();
        if (trackDetail != null && trackDetail.getDrugStatusCode() != null) {
            return AppConstant.CANCELLED.equals(trackDetail.getDrugStatusCode()) ||
                    AppConstant.DECLINED.equals(trackDetail.getDrugStatusCode()) ||
                    AppConstant.RETURN.equals(trackDetail.getDrugStatusCode());
        } else
            return false;
    }

    public boolean cancelOrderVisibility() {
        return isOrderDelivered() || orderTrackDetails != null && !orderTrackDetails.isCancelStatus();
    }

    public boolean isOrderDelivered() {
        if (getTrackDetail() != null && getTrackDetail().getDrugStatusCode() != null) {
            return AppConstant.DELIVERED.equals(getTrackDetail().getDrugStatusCode());
        } else
            return false;
    }

    public void onCancelOrderClick() {
        listener.onCallbackOfCancelOrder(getTrackDetail().getOrderId());
    }

    public void onNeedHelpClick() {
        listener.onCallbackOfNeedHelpForOrder(getTrackDetail().getOrderId());
    }

    private void showApiError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void showNoNetworkView(boolean isConnected) {
        showAndHideMainView(isConnected);
        binding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        showAndHideMainView(!isWebserviceError);
        binding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void showAndHideMainView(boolean isEnable) {
        binding.llDetailsView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        binding.llBottomButtonView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
    }


    private String getFormatDeliveryAddress(MstarOrderShippingAddress orderShippingInfo) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(orderShippingInfo.getShippingAddress1()).append(",");
        if (!TextUtils.isEmpty(orderShippingInfo.getShippingAddress2())) {
            stringBuilder.append("\n");
            stringBuilder.append(orderShippingInfo.getShippingAddress2()).append(",");
        }
        stringBuilder.append("\n");
        stringBuilder.append(orderShippingInfo.getShippingCity()).append(" - ").append(orderShippingInfo.getShippingZipCode()).append(".");
        stringBuilder.append("\n");
        stringBuilder.append(String.format(context.getString(R.string.text_state_code), orderShippingInfo.getShippingPhoneNumber()));
        return stringBuilder.toString();
    }

    public MstarOrders getCancelOrderDetail() {
        return cancelOrderDetail;
    }

    public void setCancelOrderDetail(MstarOrders cancelOrderDetail) {
        this.cancelOrderDetail = cancelOrderDetail;
    }

    public interface TrackOrderDetailsListener {

        void showLoader();

        void dismissLoader();

        void onCallbackOfCancelOrder(String orderId);

        void onCallbackOfNeedHelpForOrder(String orderId);

        void onCallbackOfViewItemsInOrder(String orderId);

        void showSnackbar(String message);

        void refreshPage();

        void cancelOrderSuccessful(String message);

        void onLaunchTrackStatusInfo(String url);

        void onEmptyOrderCallBackFromDeeplinking();
    }
}