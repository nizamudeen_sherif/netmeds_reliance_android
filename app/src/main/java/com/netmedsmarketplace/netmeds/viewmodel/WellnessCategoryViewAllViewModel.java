package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.adpater.WellnessCategoryViewAllAdapter;
import com.netmedsmarketplace.netmeds.databinding.WellnessViewAllBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarSubCategoryResult;
import com.nms.netmeds.base.model.Request.MstarCategoryDetailsRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;
import java.util.List;

public class WellnessCategoryViewAllViewModel extends AppViewModel implements WellnessCategoryViewAllAdapter.WellnessCategoryInterface {
    private final MutableLiveData<MStarBasicResponseTemplateModel> CategoryMutableLiveData = new MutableLiveData<>();
    private WellnessViewAllBinding binding;
    private WellnessCategoryViewAllAdapter.WellnessCategoryAdapterListener listener;
    private String viewAllCodes;
    private CategoryViewAllCallback callback;

    public MutableLiveData<MStarBasicResponseTemplateModel> getCategoryMutableLiveData() {
        return CategoryMutableLiveData;
    }

    public WellnessCategoryViewAllViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(WellnessViewAllBinding binding, WellnessCategoryViewAllAdapter.WellnessCategoryAdapterListener listener, CategoryViewAllCallback callback, String viewAllCodes) {
        this.binding = binding;
        this.listener = listener;
        this.callback = callback;
        this.viewAllCodes = viewAllCodes;
        getCategoryList();
    }

    private void getCategoryList() {
        boolean isConnected = callback.isNetworkConnected();
        if (isConnected) {
            callback.showLoader();
            APIServiceManager.getInstance().mStarCategoryDetails(this, getCategoryRequest());
        } else {
            callback.showNoNetworkError(false);
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        callback.showNoNetworkError(true);
        callback.showWebserviceErrorView(false);
        getCategoryList();
    }

    private MstarCategoryDetailsRequest getCategoryRequest() {
        MstarCategoryDetailsRequest request = new MstarCategoryDetailsRequest();
        request.setCategoryId(98);
        request.setPageNo(1);
        request.setPageSize(10);
        request.setProductFieldsetName(AppConstant.ALL);
        request.setReturnProduct(AppConstant.NO);
        request.setSubCategoryDepth(2);
        request.setReturnFacets(AppConstant.NO);
        request.setProductsSortOrder(AppConstant.NAME_ASCENDING);
        return request;
    }

    public void onAvailableDiscount(MStarBasicResponseTemplateModel response) {
        if (response != null && response.getResult() != null && response.getResult() != null && response.getResult().getCategoryDetails() != null && response.getResult().getCategoryDetails().getSubCategories() != null) {
            binding.recyclerview.setLayoutManager(setLinearLayoutManager());
            WellnessCategoryViewAllAdapter adapter = new WellnessCategoryViewAllAdapter(getApplication(), getMstarSubCategory(response.getResult().getCategoryDetails().getSubCategories()), listener, this);
            binding.recyclerview.setAdapter(adapter);
        }
    }

    private List<MstarSubCategoryResult> getMstarSubCategory(List<MstarSubCategoryResult> subCategoryList) {
        List<MstarSubCategoryResult> mstarSubCategoryList = new ArrayList<>();
        for (MstarSubCategoryResult subCategory : subCategoryList) {
            if (this.viewAllCodes.contains(String.valueOf(subCategory.getId()))) {
                mstarSubCategoryList.add(subCategory);
            }
        }
        return mstarSubCategoryList;
    }

    private LinearLayoutManager setLinearLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplication());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        return linearLayoutManager;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        callback.dismissLoader();
        CategoryMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));

    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callback.showWebserviceErrorView(true);
    }

    @Override
    public String getImageUrl(MstarSubCategoryResult result) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(callback.getBasePreference().getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        return response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getCatalogImageUrlBasePath()) ? response.getResult().getCatalogImageUrlBasePath() : "";
    }

    public interface CategoryViewAllCallback {

        void showWebserviceErrorView(boolean isWebserviceError);

        void showNoNetworkError(boolean isNetworkAvailable);

        void showLoader();

        void dismissLoader();

        boolean isNetworkConnected();

        BasePreference getBasePreference();
    }
}
