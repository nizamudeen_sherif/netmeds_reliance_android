package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

public class NetmedsWebViewModel extends AppViewModel {

    private BasePreference basePreference;
    private webViewCallback callback;
    private int stoppedTransactionId;
    private String productCode;

    public NetmedsWebViewModel(@NonNull Application application) {
        super(application);
    }

    public void addProductToCart(String productCode, BasePreference basePreference, webViewCallback callback) {
        this.callback = callback;
        this.basePreference = basePreference;
        this.productCode = productCode;

        if (!TextUtils.isEmpty(productCode) && Integer.parseInt(productCode) != 0) {
            if (PaymentHelper.isIsTempCart() || basePreference.getMStarCartId() > 0) {
                initApiCall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
            } else {
                stoppedTransactionId = APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART;
                initApiCall(APIServiceManager.MSTAR_CREATE_CART);
            }

        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                onProductAddedToCart(data);
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                onCartCreated(data);
                break;
        }
    }


    private void onCartCreated(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            basePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            initApiCall(stoppedTransactionId);
        }
    }

    private void initApiCall(int transactionId) {
        if (callback.isNetworkConnected()) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basePreference.getMstarBasicHeaderMap(), Integer.parseInt(productCode), 1, getCartId());
                    break;
                case APIServiceManager.MSTAR_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                    break;
            }
        }
    }

    private void onProductAddedToCart(String data) {
        callback.dismissLoader();
        MStarBasicResponseTemplateModel addToCartResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addToCartResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addToCartResponse.getStatus())) {
//            /*WebEngage AddToCart Event*/
//            WebEngageHelper.getInstance().addToCartEvent(getApplication(), productDetails, quantity);
//            /*FireBase AddToCart Event*/
//            FireBaseAnalyticsHelper.getInstance().addToCartEvent(getApplication(), productDetails.getProductCode(), productDetails.getDisplayName(), fireBaseEventProductCategory(), quantity, productDetails.getSellingPrice().doubleValue());
//            /*Facebook Pixel AddToCart Event*/
//            FacebookPixelHelper.getInstance().logAddedToCartEvent(getApplication(), productDetails, quantity);
            callback.showAddCartSuccessfullMsg();
        } else if (addToCartResponse != null && addToCartResponse.getReason() != null) {
            if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(addToCartResponse.getReason().getReason_code())) {
                callback.showMessage(R.string.text_medicine_max_limit, null);
            } else if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(addToCartResponse.getReason().getReason_code())) {
                callback.showMessage(R.string.text_medicine_out_of_stock, null);
            } else {
                callback.showMessage(0, addToCartResponse.getReason().getReason_eng());
            }
        } else {
            callback.showMessage(R.string.text_unable_to_add_to_cart, null);

        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callback.showMessage(R.string.text_unable_to_add_to_cart, null);
    }

    private Integer getCartId() {
        return PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() :
                SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().getSubscriptionCartId() :
                        M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() :
                                null;
    }

    public interface webViewCallback {

        boolean isNetworkConnected();

        void dismissLoader();

        void showMessage(int resId, String reason);

        void showLoader();

        void showAddCartSuccessfullMsg();
    }
}
