package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySigninBinding;
import com.netmedsmarketplace.netmeds.model.FacebookLoginResponse;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarSession;
import com.nms.netmeds.base.model.MstarBasicResponseFailureReasonTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarOtpDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignInViewModel extends AppViewModel {

    private final int GOOGLE_CLICK = 1;
    private final int FACEBOOK_CLICK = 2;
    private OnSignInListener callback;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;
    private ActivitySigninBinding signinBinding;
    private int failedTransactionId;
    private String accessToken;
    private String socialFlag;

    public SignInViewModel(@NonNull Application application) {
        super(application);
    }

    public GoogleSignInClient getmGoogleSignInClient() {
        return mGoogleSignInClient;
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public void init(ActivitySigninBinding signinBinding, OnSignInListener onSignInListener) {
        this.signinBinding = signinBinding;
        this.callback = onSignInListener;
        initiateGoogleSigninApiClient();
        initFacebookLoginCallbackManager();
        imageOptionButtonClick();
        onSignInListener.checkIntentValue();
    }

    public void onClickSignIn() {
        /*Google Analytics Click Event*/
        setButtonProperties(false);
        GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_MOBILE_NUMBER_ENTERED, GoogleAnalyticsHelper.EVENT_LABEL_LOGIN_REGISTRATION);
        if (validateForm()) {
            mStarGetUserStatus();
        } else {
            setButtonProperties(true);
        }
    }

    public void setButtonProperties(boolean isEnable) {
        signinBinding.btnGo.setEnabled(isEnable);
        signinBinding.btnGo.setClickable(isEnable);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        onRetry(failedTransactionId);
        callback.vmWebServiceError(false);
    }

    public void onEditText() {
        signinBinding.errorText.setText("");
    }

    public void onGoogleClickListener() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_GOOGLE_LOGIN, GoogleAnalyticsHelper.EVENT_LABEL_LOGIN_REGISTRATION);
        /*FireBase Analytics Event*/
        FireBaseAnalyticsHelper.getInstance().loginEvent(callback.getContext(), AppConstant.GOOGLE);
        PaymentHelper.setSigup_method(AppConstant.GOOGLE);
        if (checkGetAccountPermission()) {
            initApiCall(GOOGLE_CLICK);
        } else {
            callback.vmPermissionForGoogleGetAccount();
        }
    }

    public void onFacebookClickListener() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_FACEBOOK_LOGIN, GoogleAnalyticsHelper.EVENT_LABEL_LOGIN_REGISTRATION);
        /*FireBase Analytics Event*/
        FireBaseAnalyticsHelper.getInstance().loginEvent(callback.getContext(), AppConstant.FACEBOOK);
        PaymentHelper.setSigup_method(AppConstant.FACEBOOK);
        initApiCall(FACEBOOK_CLICK);
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case GOOGLE_CLICK:
                onGoogleClickListener();
                break;
            case FACEBOOK_CLICK:
                onFacebookClickListener();
                break;
            case APIServiceManager.MSTAR_CREATE_GUEST_SESSION:
                createGuestSession();
                break;
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                mStarGetUserStatus();
                break;
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLogin();
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails();
                break;
        }
    }

    private boolean checkGetAccountPermission() {
        return (ContextCompat.checkSelfPermission(callback.getContext(), android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED);
    }

    @SuppressLint("RestrictedApi")
    public void googleSignInResult(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        handleSignInResult(task);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                setSocialFlag(getConfig().getProperty(ConfigConstant.GOOGLE_FLAG));
                setAccessToken(!TextUtils.isEmpty(account.getIdToken()) ? account.getIdToken() : "");
                socialLogin();
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            callback.vmDismissProgress();
        }
    }

    @SuppressLint("RestrictedApi")
    private void initiateGoogleSigninApiClient() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(callback.getContext().getString(R.string.text_webclient_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(callback.getContext(), googleSignInOptions);
    }

    private void initFacebookLoginCallbackManager() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        final AccessToken accessToken = loginResult.getAccessToken();
                        FacebookLoginResponse facebookLoginResponse = new Gson().fromJson(new Gson().toJson(accessToken), FacebookLoginResponse.class);
                        setAccessToken(!TextUtils.isEmpty(facebookLoginResponse.getToken()) ? facebookLoginResponse.getToken() : "");
                        setSocialFlag(getConfig().getProperty(ConfigConstant.FACEBOOK_FLAG));
                        socialLogin();
                        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(final JSONObject object, GraphResponse response) {
                                LoginManager.getInstance().logOut();
                            }
                        });

                        Bundle bundle = new Bundle();
                        bundle.putString("fields", "email,id,name");
                        graphRequest.setParameters(bundle);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        callback.vmDismissProgress();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        callback.vmDismissProgress();
                    }
                });
    }

    private void socialLogin() {
        callback.vmShowProgress();
        initApiCall(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_GUEST_SESSION:
                createGuestSessionResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                getUserStatusResponse(data);
                break;
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLoginResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        callback.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_GUEST_SESSION:
            case APIServiceManager.MSTAR_GET_USER_STATUS:
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                vmOnError(transactionId);
                break;
        }
    }

    private void socialLoginResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel socialLoginResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (socialLoginResponse != null && socialLoginResponse.getStatus() != null) {
                if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {
                    setSocialLoginSuccessResponse(socialLoginResponse);
                } else if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_MSTAR_FAILURE_STATUS)) {
                    setSocialLoginFailureResponse(socialLoginResponse);
                }
            } else {
                callback.vmDismissProgress();
                vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
            }
        } else {
            callback.vmDismissProgress();
            vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
        }
    }

    private void setSocialLoginSuccessResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        if (socialLoginResponse.getResult() != null) {
            MstarBasicResponseResultTemplateModel mStarSocialLoginResult = socialLoginResponse.getResult();
            if (!TextUtils.isEmpty(mStarSocialLoginResult.getCode())) {
                switch (mStarSocialLoginResult.getCode()) {
                    case AppConstant.EXISTS:
                        String sessionId = socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null && !TextUtils.isEmpty(socialLoginResponse.getResult().getSession().getId()) ? socialLoginResponse.getResult().getSession().getId() : "";
                        long customerId = socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null ? socialLoginResponse.getResult().getSession().getCustomerId() : 0;
                        String loganSessionId = socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null && !TextUtils.isEmpty(socialLoginResponse.getResult().getSession().getLoganSessionId()) ? socialLoginResponse.getResult().getSession().getLoganSessionId() : "";

                        getPreference().setMStarSessionId(sessionId, Long.toString(customerId));
                        getPreference().setGuestCart(false);
                        getPreference().setMstarCustomerId(Long.toString(customerId));
                        getPreference().setLoganSession(loganSessionId);

                        getCustomerDetails();
                        break;
                    case AppConstant.USER_NOT_EXISTS:
                        callback.vmDismissProgress();
                        callback.vmNavigateToMobileVerification();
                        break;
                }
            } else callback.vmDismissProgress();
        } else callback.vmDismissProgress();
    }

    private void setSocialLoginFailureResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        callback.vmDismissProgress();
        if (socialLoginResponse.getReason() != null && socialLoginResponse.getReason().getReason_eng() != null && !TextUtils.isEmpty(socialLoginResponse.getReason().getReason_eng())) {
            callback.vmSnackBarMessage(socialLoginResponse.getReason().getReason_eng());
        }
    }

    private void customerDetailsResponse(String data) {
        callback.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel customerDetailResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (customerDetailResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(customerDetailResponse.getStatus()) && customerDetailResponse.getResult() != null && customerDetailResponse.getResult().getCustomerDetails() != null) {
                getPreference().setPreviousCartShippingAddressId(customerDetailResponse.getResult().getCustomerDetails().getPreferredShippingAddress());
                getPreference().setCustomerDetails(new Gson().toJson(customerDetailResponse.getResult().getCustomerDetails()));

                //Set WebEngage Login
                WebEngageHelper.getInstance().setWebEngageLogin(getPreference(), WebEngageHelper.SOCIAL_WIDGET, false, callback.getContext());
                //MAT Login Event
                MATHelper.getInstance().loginAndRegistrationMATEvent(getPreference(), false);

                callback.vmCallBackOnNavigationActivity();
            }
        }
    }

    private void vmOnError(int transactionId) {
        setButtonProperties(true);
        failedTransactionId = transactionId;
        callback.vmWebServiceError(true);
    }

    private boolean validateForm() {
        boolean isValidate = true;
        if (!ValidationUtils.checkDigit(signinBinding.name, true, callback.getContext().getString(R.string.error_mobile), signinBinding.errorText) || !ValidationUtils.checkText(signinBinding.name, true, callback.getContext().getString(R.string.error_mobile), 10, 10, signinBinding.errorText)) {
            isValidate = false;
        }
        return isValidate;
    }

    private void imageOptionButtonClick() {
        signinBinding.name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateForm()) {
                        mStarGetUserStatus();
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

    public void skip() {
        // disable the diagnostic first time boolean to false
        getPreference().setDiaFirstTimeLogin(false);
        if (!TextUtils.isEmpty(getPreference().getGuestSessionValidTime())) {
            if (DateTimeUtils.getInstance().convertDateToMilliSecond(getPreference().getGuestSessionValidTime(), DateTimeUtils.yyyyMMddHHmmss) < System.currentTimeMillis()) {
                createGuestSession();
            } else {
                callback.vmCallBackOnNavigationActivity();
            }
        } else
            createGuestSession();
    }

    private void createGuestSession() {
        callback.vmShowProgress();
        initApiCall(APIServiceManager.MSTAR_CREATE_GUEST_SESSION);
    }

    private void createGuestSessionResponse(String data) {
        callback.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel guestSessionResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (guestSessionResponse != null && guestSessionResponse.getStatus() != null) {
                switch (guestSessionResponse.getStatus()) {
                    case AppConstant.SUCCESS_STATUS:
                        setGuestSession(guestSessionResponse);
                        break;
                    case AppConstant.API_MSTAR_FAILURE_STATUS:
                        vmOnError(APIServiceManager.MSTAR_CREATE_GUEST_SESSION);
                        break;
                }
            }
        } else
            vmOnError(APIServiceManager.MSTAR_CREATE_GUEST_SESSION);
    }

    private void setGuestSession(MStarBasicResponseTemplateModel createGuestSessionResponse) {
        if (createGuestSessionResponse.getResult() != null && createGuestSessionResponse.getResult().getSession() != null) {
            MStarSession guestSession = createGuestSessionResponse.getResult().getSession();
            if (!TextUtils.isEmpty(guestSession.getId())) {
                getPreference().setMStarSessionId(guestSession.getId(), Integer.toString(AppConstant.GUEST_CUSTOMER_ID));
                getPreference().setMstarCustomerId(Integer.toString(AppConstant.GUEST_CUSTOMER_ID));
                getPreference().setGuestCart(true);
                getPreference().setGuestSessionValidTime(guestSession.getValidTill());
                if (callback.getContext() != null)
                    callback.vmCallBackOnNavigationActivity();
            } else
                vmOnError(APIServiceManager.MSTAR_CREATE_GUEST_SESSION);
        } else
            vmOnError(APIServiceManager.MSTAR_CREATE_GUEST_SESSION);
    }


    private void mStarGetUserStatus() {
        callback.vmShowProgress();
        initApiCall(APIServiceManager.MSTAR_GET_USER_STATUS);
    }

    private void getUserStatusResponse(String data) {
        callback.vmDismissProgress();
        setButtonProperties(true);
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel userStatusResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (userStatusResponse != null && userStatusResponse.getStatus() != null) {
                switch (userStatusResponse.getStatus()) {
                    case AppConstant.SUCCESS_STATUS:
                        callback.vmCallBackOnNavigationSignInWithPasswordActivity();
                        break;
                    case AppConstant.API_MSTAR_FAILURE_STATUS:
                        checkFailureReason(userStatusResponse);
                        break;
                }
            } else
                vmOnError(APIServiceManager.MSTAR_GET_USER_STATUS);
        } else
            vmOnError(APIServiceManager.MSTAR_GET_USER_STATUS);
    }

    private void checkFailureReason(MStarBasicResponseTemplateModel userStatusResponse) {
        if (userStatusResponse.getReason() != null) {
            MstarBasicResponseFailureReasonTemplateModel mStarUserStatusReason = userStatusResponse.getReason();
            if (!TextUtils.isEmpty(mStarUserStatusReason.getReason_code()) && mStarUserStatusReason.getReason_code().equalsIgnoreCase(AppConstant.NOT_FOUND)) {
                if (userStatusResponse.getResult() != null && userStatusResponse.getResult().getMstarOtpDetails() != null) {
                    MstarOtpDetails mstarOtpDetails = userStatusResponse.getResult().getMstarOtpDetails();
                    if (!TextUtils.isEmpty(mstarOtpDetails.getRandomKey())) {
                        callback.vmCallBackOnNavigationSignUpActivity(mstarOtpDetails.getRandomKey());
                    }
                }
            } else {
                callback.vmSnackBarMessage(!TextUtils.isEmpty(mStarUserStatusReason.getReason_eng()) ? mStarUserStatusReason.getReason_eng() : "");
            }
        }
    }

    private String getPhoneNo() {
        return signinBinding.name != null && signinBinding.name.getText() != null ? signinBinding.name.getText().toString().trim() : "";
    }

    private void initApiCall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.vmShowNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_GET_USER_STATUS:
                    APIServiceManager.getInstance().mStarGetUserStatus(this, getPhoneNo());
                    break;
                case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                    APIServiceManager.getInstance().mStarSocialLogin(this, getSocialLoginRequest(),getPreference().getMstarGoogleAdvertisingId());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, getPreference().getMstarBasicHeaderMap());
                    break;
                case GOOGLE_CLICK:
                    callback.vmGoogleSignIn();
                    break;
                case FACEBOOK_CLICK:
                    callback.vmFacebookSignIn();
                    break;
                case APIServiceManager.MSTAR_CREATE_GUEST_SESSION:
                    APIServiceManager.getInstance().mStarCreateGuestSession(this);
                    break;
            }
        } else {
            setButtonProperties(true);
            callback.vmDismissProgress();
            failedTransactionId = transactionId;
        }
    }

    private Map<String, String> getSocialLoginRequest() {
        Map<String, String> socialLoginRequest = new HashMap<>();
        socialLoginRequest.put(AppConstant.SIGNATURE, "");
        socialLoginRequest.put(AppConstant.PAYLOAD, "");
        socialLoginRequest.put(AppConstant.ACCESS_TOKEN, getAccessToken() != null && !TextUtils.isEmpty(getAccessToken()) ? getAccessToken() : "");
        socialLoginRequest.put(AppConstant.SOCIAL_FLAG, getSocialFlag() != null && !TextUtils.isEmpty(getSocialFlag()) ? getSocialFlag() : "");
        socialLoginRequest.put(AppConstant.SOURCE, AppConstant.SOURCE_NAME);
        socialLoginRequest.put(AppConstant.MOBILE_NUMBER, "");
        socialLoginRequest.put(AppConstant.RANDOM_KEY, "");
        return socialLoginRequest;
    }

    private ConfigMap getConfig() {
        return ConfigMap.getInstance();
    }

    public String getAccessToken() {
        return accessToken;
    }

    private void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSocialFlag() {
        return socialFlag;
    }

    private void setSocialFlag(String socialFlag) {
        this.socialFlag = socialFlag;
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(callback.getContext());
    }

    private void getCustomerDetails() {
        initApiCall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    public interface OnSignInListener {

        void vmShowProgress();

        void vmDismissProgress();

        void vmPermissionForGoogleGetAccount();

        void vmGoogleSignIn();

        void vmFacebookSignIn();

        void vmNavigateToMobileVerification();

        void vmCallBackOnNavigationSignInWithPasswordActivity();

        void vmCallBackOnNavigationSignUpActivity(String randomKey);

        void vmCallBackOnNavigationActivity();

        void vmSnackBarMessage(String Message);

        void vmShowNoNetworkView(boolean isConnected);

        void vmWebServiceError(boolean isWebserviceError);

        Context getContext();

        void checkIntentValue();
    }
}
