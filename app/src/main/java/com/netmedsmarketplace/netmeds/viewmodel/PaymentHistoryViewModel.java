package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.LinkedWalletListAdapter;
import com.netmedsmarketplace.netmeds.adpater.SavedCardsListAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityPaymentHistoryBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.LinkedSavedCard;
import com.nms.netmeds.base.model.LinkedWallet;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarLinkedPaymentResponse;
import com.nms.netmeds.base.model.MStarLinkedPaymentResult;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.payment.ui.PaymentConstants;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import amazonpay.silentpay.APayCallback;
import amazonpay.silentpay.APayError;
import amazonpay.silentpay.AmazonPay;
import amazonpay.silentpay.GetBalanceRequest;

import static android.app.Activity.RESULT_OK;

public class PaymentHistoryViewModel extends AppViewModel implements LinkedWalletListAdapter.LinkedPaymentAdapterListener {
    private ActivityPaymentHistoryBinding binding;
    private PaymentHistoryListener callBack;
    private boolean isAmazonPayEnabled = false;
    private String linkedWalletToken = "";
    private String jusPayClientAuthToken = "";
    private MutableLiveData<MStarLinkedPaymentResponse> linkedPaymentMutableLiveData = new MutableLiveData<>();

    public PaymentHistoryViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivityPaymentHistoryBinding binding, PaymentHistoryListener listener) {
        this.binding = binding;
        this.callBack = listener;
        if (CommonUtils.isBuildVariantProdRelease()) {
            getAmazonPayBalance();
        }
        fetchLinkedPaymentsFromServer();
    }

    public MutableLiveData<MStarLinkedPaymentResponse> getLinkedPaymentMutableLiveData() {
        return linkedPaymentMutableLiveData;
    }

    public void fetchLinkedPaymentsFromServer() {
        callBack.showLoader();
        initApiCall(APIServiceManager.C_MSTAR_GET_LINKED_PAYMENT);
    }

    private void initApiCall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(callBack.getContext());
        callBack.vmNoNetworkErrorView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_GET_LINKED_PAYMENT:
                    APIServiceManager.getInstance().mStarGetLinkedPayment(this, getPreference().getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.PAY_TM_DE_LINK:
                    APIServiceManager.getInstance().revokePayTmAccess(this, getLinkedWalletToken(), getPayTmDeLinkHeader());
                    break;
                case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                    APIServiceManager.getInstance().mstarUpdateCustomer(this, getPreference().getMstarBasicHeaderMap(), removePayTmTokenRequest());
                    break;
            }
        } else
            callBack.dismissLoader();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_GET_LINKED_PAYMENT:
                MStarLinkedPaymentResponse mStarLinkedPaymentResponse = new Gson().fromJson(data, MStarLinkedPaymentResponse.class);
                linkedPaymentMutableLiveData.setValue(mStarLinkedPaymentResponse);
                break;
            case APIServiceManager.PAY_TM_DE_LINK:
                removePayTmAccessToken();
                break;
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                updatePayTmTokenResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_GET_LINKED_PAYMENT:
                callBack.dismissLoader();
                showApiError();
                break;
            case APIServiceManager.PAY_TM_DE_LINK:
                removePayTmAccessToken();
                break;
            case APIServiceManager.MSTAR_UPDATE_CUSTOMER:
                callBack.dismissLoader();
                callBack.showSnackBar(callBack.getContext().getResources().getString(R.string.text_delink_failure));
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        callBack.vmWebserviceErrorView(false);
        fetchLinkedPaymentsFromServer();
    }

    private void getAmazonPayBalance() {
        if (callBack.getContext() != null && !TextUtils.isEmpty(PaymentUtils.amazonPayCredential(getPreference().getPaymentCredentials()))) {
            AmazonPay.getBalance(callBack.getContext(), new GetBalanceRequest(PaymentUtils.amazonPayCredential(getPreference().getPaymentCredentials()), false), new APayCallback() {
                @Override
                public void onSuccess(Bundle bundle) {
                    isAmazonPayEnabled = true;
                    callBack.vmAmazonPayCheckBalanceResponse();
                }

                @Override
                public void onError(final APayError aPayError) {
                    isAmazonPayEnabled = false;
                    callBack.vmAmazonPayCheckBalanceResponse();
                }
            });
        }
    }

    public void onLinkedPaymentDataAvailable(MStarLinkedPaymentResponse paymentHistoryResponse) {
        callBack.dismissLoader();
        if (paymentHistoryResponse != null && paymentHistoryResponse.getLinkedPaymentResult() != null) {
            MStarLinkedPaymentResult mStarLinkedPaymentResult = paymentHistoryResponse.getLinkedPaymentResult();
            if ((getLinkedWalletList(mStarLinkedPaymentResult.getWalletList()) != null && getLinkedWalletList(mStarLinkedPaymentResult.getWalletList()).size() > 0) || (mStarLinkedPaymentResult.getSavedCardList() != null && mStarLinkedPaymentResult.getSavedCardList().size() > 0)) {
                initLinkedWalletListAdapter(mStarLinkedPaymentResult.getWalletList());
                initSavedCardsListAdapter(mStarLinkedPaymentResult.getSavedCardList());
                callBack.vmContentViewEnableDisable(true);
                callBack.vmEmptyViewEnableDisable(false);
            } else {
                callBack.vmContentViewEnableDisable(false);
                callBack.vmEmptyViewEnableDisable(true);
            }
            setJusPayClientAuthToken(!TextUtils.isEmpty(mStarLinkedPaymentResult.getClientAuthToken()) ? mStarLinkedPaymentResult.getClientAuthToken() : "");
        } else {
            callBack.vmContentViewEnableDisable(false);
            callBack.vmEmptyViewEnableDisable(true);
        }
    }

    private void initSavedCardsListAdapter(List<LinkedSavedCard> savedCardList) {
        binding.tvSavedCardsTitle.setVisibility(savedCardList != null && savedCardList.size() != 0 ? View.VISIBLE : View.GONE);
        binding.rvCardList.setVisibility(savedCardList != null && savedCardList.size() != 0 ? View.VISIBLE : View.GONE);
        SavedCardsListAdapter adapter = new SavedCardsListAdapter(savedCardList, this, AppConstant.SAVED_CARDS);
        binding.rvCardList.setLayoutManager(new LinearLayoutManager(callBack.getContext()));
        binding.rvCardList.setNestedScrollingEnabled(false);
        binding.rvCardList.setAdapter(adapter);
    }

    private void initLinkedWalletListAdapter(List<LinkedWallet> walletList) {
        List<LinkedWallet> linkedWalletList = getLinkedWalletList(walletList);
        binding.cvLinkedWallets.setVisibility(linkedWalletList != null && linkedWalletList.size() != 0 ? View.VISIBLE : View.GONE);
        LinkedWalletListAdapter adapter = new LinkedWalletListAdapter(callBack.getContext(), linkedWalletList, this, AppConstant.LINKED_WALLET);
        binding.rvWalletList.setLayoutManager(new LinearLayoutManager(callBack.getContext()));
        binding.rvWalletList.setNestedScrollingEnabled(false);
        binding.rvWalletList.setAdapter(adapter);
    }

    private List<LinkedWallet> getLinkedWalletList(List<LinkedWallet> walletList) {
        List<LinkedWallet> linkedWalletList = new ArrayList<>();
        for (LinkedWallet wallet : walletList) {
            if (wallet.getLinked() || ((AppConstant.AMAZONPAY_TITLE.equals(wallet.getDisplayName())) && (isAmazonPayEnabled))) {
                linkedWalletList.add(wallet);
            }
        }
        return linkedWalletList;
    }


    private String getLastFourDigitText(String title) {
        String[] titleSplit = title.split("-");
        return titleSplit[titleSplit.length - 1];
    }

    @Override
    public void onItemClick(Object obj, String clickFrom) {
        if (AppConstant.SAVED_CARDS.equalsIgnoreCase(clickFrom)) {
            LinkedSavedCard savedCard = (LinkedSavedCard) obj;
            callBack.showDeleteSavedCardsAlert(getLastFourDigitText(savedCard.getCardNumber()), clickFrom, obj);
        } else {
            LinkedWallet wallet = (LinkedWallet) obj;
            callBack.showDeleteSavedCardsAlert(wallet.getDisplayName(), clickFrom, obj);
        }
    }

    private Bundle getDeLinkBundle(Object obj, String from) {
        Bundle args = new Bundle();
        ArrayList<String> endUrl = new ArrayList<>();
        endUrl.add(getJusPayEndUrl());
        args.putString(in.juspay.godel.core.PaymentConstants.MERCHANT_ID, getJusPayMerchantId());
        args.putString(in.juspay.godel.core.PaymentConstants.CLIENT_ID, getJusPayClientId());
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_ID, AppConstant.SUFFIX_NETMEDS + getCustomerId());
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_MOBILE, getMobileNo());
        args.putString(in.juspay.godel.core.PaymentConstants.CUSTOMER_EMAIL, getEmail());
        args.putString(in.juspay.godel.core.PaymentConstants.ENV, in.juspay.godel.core.PaymentConstants.ENVIRONMENT.PRODUCTION);
        args.putString(in.juspay.godel.core.PaymentConstants.SERVICE, PaymentConstants.JUSPAY_SERVICE);
        args.putStringArrayList(in.juspay.godel.core.PaymentConstants.END_URLS, endUrl);
        args.putString(in.juspay.godel.core.PaymentConstants.CLIENT_AUTH_TOKEN, getJusPayClientAuthToken());
        args.putString(in.juspay.godel.core.PaymentConstants.PAYLOAD, getPayLoad(obj, from));
        args.putInt(PaymentConstants.REQUEST_CODE, PaymentIntentConstant.DE_LINK_WALLET_JUST_PAY);
        return args;
    }

    private String getPayLoad(Object obj, String from) {
        JSONObject deLinkPayLoad = new JSONObject();
        try {
            if (AppConstant.SAVED_CARDS.equalsIgnoreCase(from)) {
                LinkedSavedCard savedCard = (LinkedSavedCard) obj;
                deLinkPayLoad.put(AppConstant.OPNAME, AppConstant.DELETE_CARD);
                deLinkPayLoad.put(AppConstant.CARDTOKEN, savedCard.getToken());
            } else {
                LinkedWallet wallet = (LinkedWallet) obj;
                deLinkPayLoad.put(AppConstant.OPNAME, AppConstant.DE_LINK_WALLET);
                deLinkPayLoad.put(AppConstant.WALLET_ID, wallet.getKey());
                deLinkPayLoad.put(AppConstant.WALLET_NAME, wallet.getDisplayName());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return deLinkPayLoad.toString();
    }

    public void deLinkUpdate(int resultCode, boolean isFromAmazonPay) {
        if (resultCode == RESULT_OK) {
            if (CommonUtils.isBuildVariantProdRelease() && isFromAmazonPay)
                getAmazonPayBalance();
            else
                fetchLinkedPaymentsFromServer();
        } else {
            callBack.showSnackBar(callBack.getContext().getString(R.string.text_delink_failure));
        }
    }

    public void proceedDeleteCards(Object obj, String clickFrom) {
        callBack.showLoader();
        if (AppConstant.SAVED_CARDS.equalsIgnoreCase(clickFrom)) {
            LinkedSavedCard savedCard = (LinkedSavedCard) obj;
            callBack.onDeLinkOfJustPayWallet(getDeLinkBundle(savedCard, AppConstant.SAVED_CARDS));
        } else {
            LinkedWallet wallet = (LinkedWallet) obj;
            if (AppConstant.PAYTM_TITLE.equals(wallet.getDisplayName())) {
                callBack.onDeLinkPayTM(wallet);
            } else if (AppConstant.AMAZONPAY_TITLE.equals(wallet.getDisplayName())) {
                callBack.onDeLinkAmazonPay();
            } else {
                callBack.onDeLinkOfJustPayWallet(getDeLinkBundle(wallet, AppConstant.LINKED_WALLET));
            }
        }
    }

    public void deLinkPayTm(LinkedWallet linkedWallet) {
        setLinkedWalletToken(linkedWallet != null && !TextUtils.isEmpty(linkedWallet.getLinkToken()) ? linkedWallet.getLinkToken() : "");
        callBack.showLoader();
        initApiCall(APIServiceManager.PAY_TM_DE_LINK);
    }

    private Map<String, String> getPayTmDeLinkHeader() {
        String clientId = PaymentUtils.payTmCredentials(PaymentConstants.PAYTM_LINK_CLIENT_ID, getPreference().getPaymentCredentials());
        String secretKey = PaymentUtils.payTmCredentials(PaymentConstants.PAYTM_LINK_SECRET_KEY, getPreference().getPaymentCredentials());
        String credential = String.format("%s:%s", clientId, secretKey);
        String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
        Map<String, String> headers = new HashMap<>();
        headers.put(AppConstant.KEY_AUTHORIZATION, auth);
        headers.put(AppConstant.KEY_CONTENT_TYPE, AppConstant.CONTENT_TYPE_JSON);
        headers.put(AppConstant.KEY_CONTENT_LENGTH, "0");
        headers.put(AppConstant.KEY_HOST, AppConstant.PAY_TM_HOST);
        return headers;
    }

    private void removePayTmAccessToken() {
        initApiCall(APIServiceManager.MSTAR_UPDATE_CUSTOMER);
    }

    private void showApiError() {
        callBack.vmWebserviceErrorView(true);
    }

    private String getJusPayMerchantId() {
        return getJusPayCredentials(PaymentConstants.JUS_PAY_MERCHENT_ID);
    }

    private String getJusPayClientId() {
        return getJusPayCredentials(PaymentConstants.JUS_PAY_CLIENT_ID);
    }

    private String getJusPayEndUrl() {
        return getJusPayCredentials((PaymentConstants.JUS_PAY_RETURN_URL));
    }

    private String getJusPayCredentials(int id) {
        return PaymentUtils.jusPayCredential(id, getPreference().getPaymentCredentials());
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(callBack.getContext());
    }

    private MStarCustomerDetails getCustomerDetail() {
        return new Gson().fromJson(getPreference().getCustomerDetails(), MStarCustomerDetails.class);
    }

    private String getCustomerId() {
        return getCustomerDetail() != null ? Integer.toString(getCustomerDetail().getId()) : "";

    }

    private String getEmail() {
        return getCustomerDetail() != null && !TextUtils.isEmpty(getCustomerDetail().getEmail()) ? getCustomerDetail().getEmail() : "";
    }

    private String getMobileNo() {
        return getCustomerDetail() != null && !TextUtils.isEmpty(getCustomerDetail().getMobileNo()) ? getCustomerDetail().getMobileNo() : "";
    }

    private String getLinkedWalletToken() {
        return linkedWalletToken;
    }

    private void setLinkedWalletToken(String linkedWalletToken) {
        this.linkedWalletToken = linkedWalletToken;
    }

    private String getJusPayClientAuthToken() {
        return jusPayClientAuthToken;
    }

    private void setJusPayClientAuthToken(String jusPayClientAuthToken) {
        this.jusPayClientAuthToken = jusPayClientAuthToken;
    }

    private MstarUpdateCustomerRequest removePayTmTokenRequest() {
        MStarCustomerDetails customerDetails = new Gson().fromJson(getPreference().getCustomerDetails(), MStarCustomerDetails.class);
        String firstName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "";
        String lastName = customerDetails != null && !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "";
        String email = customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "";
        String gender = customerDetails != null && !TextUtils.isEmpty(customerDetails.getGender()) ? customerDetails.getGender() : "";

        MstarUpdateCustomerRequest removePayTmTokenRequest = new MstarUpdateCustomerRequest();
        removePayTmTokenRequest.setFirstName(firstName);
        removePayTmTokenRequest.setLastName(lastName);
        removePayTmTokenRequest.setEmail(email);
        removePayTmTokenRequest.setGender(gender);
        removePayTmTokenRequest.setPaytmCustomerToken("Nil");

        return removePayTmTokenRequest;
    }

    private void updatePayTmTokenResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel payTmUpdateTokenResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (payTmUpdateTokenResponse != null && payTmUpdateTokenResponse.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(payTmUpdateTokenResponse.getStatus())) {
                callBack.showSnackBar(callBack.getContext().getResources().getString(R.string.text_delink_successful));
                fetchLinkedPaymentsFromServer();
            }
        }
    }

    public interface PaymentHistoryListener {

        void showLoader();

        void dismissLoader();

        void showSnackBar(String message);

        void onDeLinkOfJustPayWallet(Bundle deLinkBundle);

        void onDeLinkAmazonPay();

        void onDeLinkPayTM(LinkedWallet wallet);

        void showDeleteSavedCardsAlert(String name, String clickFrom, Object obj);

        Context getContext();

        void vmEmptyViewEnableDisable(boolean visible);

        void vmContentViewEnableDisable(boolean visible);

        void vmWebserviceErrorView(boolean isWebserviceError);

        void vmNoNetworkErrorView(boolean isConnected);

        void vmAmazonPayCheckBalanceResponse();
    }
}
