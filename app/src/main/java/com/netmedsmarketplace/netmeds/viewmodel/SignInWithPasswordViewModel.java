package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySignWithPasswordBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.Map;

import static com.nms.netmeds.base.IntentConstant.PHONE_NUMBER;

public class SignInWithPasswordViewModel extends AppViewModel {

    private SignInWithPasswordListener listener;
    private ActivitySignWithPasswordBinding binding;
    private String phoneNumber;
    private int failedTransactionId;
    private boolean resetFlag;

    public SignInWithPasswordViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivitySignWithPasswordBinding binding, SignInWithPasswordListener signInWithPasswordListener, Intent intent) {
        this.listener = signInWithPasswordListener;
        this.binding = binding;
        this.phoneNumber = intent.getStringExtra(PHONE_NUMBER);
        binding.phoneNumber.setText(intent.getStringExtra(PHONE_NUMBER));
        imageOptionButtonClick();
    }

    private void initiateAPICall(int transactionID) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        showNoNetworkView(isConnected);
        if (isConnected) {
            listener.vmShowProgress();
            switch (transactionID) {
                case APIServiceManager.MSTAR_LOGIN:
                    APIServiceManager.getInstance().mStarLogIn(this, !TextUtils.isEmpty(phoneNumber) ? phoneNumber : "", binding.passwordEdit.getText() != null && !TextUtils.isEmpty(binding.passwordEdit.getText().toString()) ? binding.passwordEdit.getText().toString() : "", getPreference().isGuestCart() ? getMergeSessionId() : "");
                    break;
                case APIServiceManager.MSTAR_SENT_OTP:
                    APIServiceManager.getInstance().initiateSentOtp(this, !TextUtils.isEmpty(phoneNumber) ? phoneNumber : "");
                    break;
                case APIServiceManager.MSTAR_INITIATE_CHANGE_PSSWRD:
                    APIServiceManager.getInstance().mstarInitiateChangePassword(this, phoneNumber);
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, BasePreference.getInstance(getApplication()).getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    APIServiceManager.getInstance().mStarGetCartDetails(this, BasePreference.getInstance(getApplication()).getMstarBasicHeaderMap(), M2Helper.getInstance().isM2Order() ? BasePreference.getInstance(getApplication()).getMstarMethod2CartId() : 0, APIServiceManager.MSTAR_GET_CART_DETAILS);
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_INITIATE_CHANGE_PSSWRD:
                initiateChangeResponse(data);
                break;
            case APIServiceManager.MSTAR_SENT_OTP:
                OtpResponse(data);
                break;
            case APIServiceManager.MSTAR_LOGIN:
                loginResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                getCartDetails(data);
                break;
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        listener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_INITIATE_CHANGE_PSSWRD:
            case APIServiceManager.MSTAR_SENT_OTP:
            case APIServiceManager.MSTAR_LOGIN:
                vmOnError(transactionId);
                break;
        }
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.MSTAR_INITIATE_CHANGE_PSSWRD:
                break;
            case APIServiceManager.MSTAR_LOGIN:
                logIn();
                break;
            case APIServiceManager.MSTAR_SENT_OTP:
                initiateAPICall(APIServiceManager.MSTAR_SENT_OTP);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                initiateAPICall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
                break;
        }
    }

    private void getCartDetails(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getResult() != null && response.getResult().getCartDetails() != null) {
            if (response.getResult().getCartDetails().getLines() != null && !response.getResult().getCartDetails().getLines().isEmpty()) {
                BasePreference.getInstance(getApplication()).setCartCount(response.getResult().getCartDetails().getLines().size());
            } else {
                BasePreference.getInstance(getApplication()).setCartCount(0);
            }
        }
    }


    private void initiateChangeResponse(String data) {
        OtpResponse(data);
    }

    public void onClickChangeNumber() {
        listener.vmCallBackOnNavigationMobileNumberActivity();
    }

    public void onEditText() {
        binding.errorText.setText("");
    }

    public void onClickLoginWithOtp() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getApplication(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_LOGIN_WITH_OTP_CLICK, GoogleAnalyticsHelper.EVENT_LABEL_LOGIN_WITH_PASSWORD);
        initiateAPICall(APIServiceManager.MSTAR_SENT_OTP);
    }

    public void onClickForgotPassword() {
        resetFlag = true;
        initiateAPICall(APIServiceManager.MSTAR_INITIATE_CHANGE_PSSWRD);

    }

    public void onClickLogin() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getApplication(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_PASSWORD_ENTERED_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_LOGIN_WITH_PASSWORD);
        if (validateForm()) {
            logIn();
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        onRetry(failedTransactionId);
        showWebserviceErrorView(false);
    }

    private boolean validateForm() {
        return checkPassword(binding.passwordEdit, getApplication().getString(R.string.error_emptypassword), binding.errorText);
    }

    private boolean checkPassword(TextView editText, String emptyErrorMessage, TextView textView) {
        return ValidationUtils.checkLoginPassword(getApplication(), editText, emptyErrorMessage, getApplication().getString(R.string.error_lngthpassword), textView);
    }

    private void logIn() {
        initiateAPICall(APIServiceManager.MSTAR_LOGIN);
        failedTransactionId = APIServiceManager.MSTAR_LOGIN;
    }

    private void OtpResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null) {
            if (!resetFlag) {
                listener.vmCallBackOnNavigationSigInWithOtpActivity(response.getResult() != null && response.getResult().getMstarOtpDetails() != null && !TextUtils.isEmpty(response.getResult().getMstarOtpDetails().getRandomKey()) ? response.getResult().getMstarOtpDetails().getRandomKey() : "");
            } else {
                listener.vmCallBackOnNavigationSocialLoginActivity(response.getResult() != null && response.getResult().getMstarOtpDetails() != null && !TextUtils.isEmpty(response.getResult().getMstarOtpDetails().getRandomKey()) ? response.getResult().getMstarOtpDetails().getRandomKey() : "");
            }
        } else {
            SnackBarHelper.snackBarCallBack(binding.siginInViewContent, getApplication(), response != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng()) ? response.getReason().getReason_eng() : "");

        }
    }

    private void customerDetailsResponse(String data) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && addressData.getResult() != null && addressData.getResult().getCustomerDetails() != null) {
            BasePreference.getInstance(getApplication()).setCustomerDetails(new Gson().toJson(addressData.getResult().getCustomerDetails()));
            BasePreference.getInstance(getApplication()).setPreviousCartShippingAddressId(addressData.getResult().getCustomerDetails().getPreferredShippingAddress());

            //Set WebEngage Login
            WebEngageHelper.getInstance().setWebEngageLogin(getPreference(), WebEngageHelper.DIRECT_SIGN_IN, false, listener.getContext());
            //MAT Login Event
            MATHelper.getInstance().loginAndRegistrationMATEvent(getPreference(), false);
        }
        listener.vmNavigateToHome();
    }

    private void loginResponse(String data) {
        listener.vmDismissProgress();
        MStarBasicResponseTemplateModel basicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (basicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(basicResponseTemplateModel.getStatus()) && basicResponseTemplateModel.getResult() != null && basicResponseTemplateModel.getResult().getSession() != null) {
            BasePreference.getInstance(getApplication()).setMStarSessionId(!TextUtils.isEmpty(basicResponseTemplateModel.getResult().getSession().getId()) ? basicResponseTemplateModel.getResult().getSession().getId() : "", String.valueOf(basicResponseTemplateModel.getResult().getSession().getCustomerId() != null ? basicResponseTemplateModel.getResult().getSession().getCustomerId() : ""));
            BasePreference.getInstance(getApplication()).setGuestCart(false);
            BasePreference.getInstance(getApplication()).setMstarCustomerId(String.valueOf(basicResponseTemplateModel.getResult().getSession().getCustomerId()));
            BasePreference.getInstance(getApplication()).setLoganSession(basicResponseTemplateModel.getResult().getSession().getLoganSessionId());
            //Set WebEngage Login
            WebEngageHelper.getInstance().setWebEngageLogin(getPreference(), WebEngageHelper.DIRECT_SIGN_IN, false, listener.getContext());
            //MAT Login Event
            MATHelper.getInstance().loginAndRegistrationMATEvent(getPreference(), false);
            /*FireBase Analytics Event*/
            FireBaseAnalyticsHelper.getInstance().loginEvent(listener.getContext(), AppConstant.FIREBASE_PASSWORD);
            initiateAPICall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
        } else if (basicResponseTemplateModel != null && basicResponseTemplateModel.getReason() != null && basicResponseTemplateModel.getReason().getReason_eng() != null) {
            listener.vmSnackBarMessage(basicResponseTemplateModel.getReason().getReason_eng());
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        binding.siginInViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        binding.siginInViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void imageOptionButtonClick() {
        binding.passwordEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateForm()) {
                        if (validateForm()) {
                            logIn();
                        }
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

    private String getMergeSessionId() {
        Map<String, String> headerMap = getPreference().getMstarBasicHeaderMap();
        return headerMap != null && !headerMap.isEmpty() ? headerMap.get(AppConstant.MSTAR_AUTH_TOKEN) : "";
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(listener.getContext());
    }

    public interface SignInWithPasswordListener {
        void vmCallBackOnNavigationSigInWithOtpActivity(String randomKey);

        void vmCallBackOnNavigationMobileNumberActivity();

        void vmCallBackOnNavigationSocialLoginActivity(String randomKey);

        void vmShowProgress();

        void vmDismissProgress();

        void vmNavigateToHome();

        void vmSnackBarMessage(String message);

        Context getContext();
    }
}
