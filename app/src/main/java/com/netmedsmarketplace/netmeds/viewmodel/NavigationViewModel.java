package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityNavigationBinding;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;

public class NavigationViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityNavigationBinding navigationBinding;
    private NavigationListener listener;

    public NavigationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityNavigationBinding homeBinding, NavigationListener listener, Intent intent) {
        this.context = context;
        this.navigationBinding = homeBinding;
        this.listener = listener;
        checkIntentValue(intent);
    }

    private void checkIntentValue(Intent intent) {
        if (intent.getBooleanExtra(PaymentIntentConstant.HOME_FLAG_FROM_ORDER_PLACED_SUCCESSFUL, false)) {
            navigateOrders();
        } else if (intent.getBooleanExtra(PaymentIntentConstant.HOME_FLAG_FROM_REFILL_PLACED_SUCCESSFUL, false)) {
            navigateSubscription();
        } else if (intent.getBooleanExtra(IntentConstant.HOME_FLAG_FROM_SUBSCRIPTION_ORDER_PLACED_SUCCESSFUL, false)) {
            navigateSubscription();
        }
    }

    private void resetHelperData() {
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        PaymentHelper.setIsCartSwitched(false);
        M2Helper.getInstance().setM2Order(false);
        PaymentHelper.setIsTempCart(false);
        PaymentHelper.setTempCartId(0);
        PaymentHelper.setIsEditOrder(false);
        PaymentHelper.setIsExternalDoctor(false);
        PaymentHelper.setIsPayAndEditM2Order(false);
        RefillHelper.resetMapValues();
        PaymentHelper.setIsPayAndEditM2Order(false);
        M2Helper.getInstance().setPayNow(false);
        M2Helper.getInstance().setRetry(false);
    }

    public boolean subscriptionStatus() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) {
            return configurationResponse.getResult().getConfigDetails().getSubscriptionStatus();
        } else
            return false;
    }

    public void navigateHome() {
        resetHelperData();
        navigationBinding.homeImage.setImageResource(R.drawable.ic_netmeds_home_active);
        navigationBinding.orderImage.setImageResource(R.drawable.ic_shopping_cart_inactive);
        navigationBinding.subscriptionImage.setImageResource(R.drawable.ic_subscription_inactive);
        navigationBinding.accountImage.setImageResource(R.drawable.ic_profile_inactive);
        navigationBinding.walletImage.setImageResource(R.drawable.ic_wallet_inactive);

        navigationBinding.homeText.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
        navigationBinding.orderText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.subscriptionText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.accountText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.walletText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        listener.vmNavigateHome();
    }

    public void navigateOrders() {
        resetHelperData();
        setOrderActive();
        listener.vmNavigateOrders();
    }

    public void navigateSubscription() {
        resetHelperData();
        navigationBinding.homeImage.setImageResource(R.drawable.ic_netmeds_home_inactive);
        navigationBinding.orderImage.setImageResource(R.drawable.ic_shopping_cart_inactive);
        navigationBinding.subscriptionImage.setImageResource(R.drawable.ic_subscription_active);
        navigationBinding.accountImage.setImageResource(R.drawable.ic_profile_inactive);
        navigationBinding.walletImage.setImageResource(R.drawable.ic_wallet_inactive);

        navigationBinding.homeText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.orderText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.subscriptionText.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
        navigationBinding.accountText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.walletText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        listener.vmNavigateSubscription();
    }

    public void navigateAccount() {
        resetHelperData();
        setAccountActive();
        listener.vmNavigateAccount();
    }

    public void navigateWallet() {
        resetHelperData();
        setWalletActive();
        listener.vmNavigateWallet();
    }

    public interface NavigationListener {
        void vmNavigateHome();

        void vmNavigateOrders();

        void vmNavigateSubscription();

        void vmNavigateAccount();

        void vmNavigateWallet();
    }

    public void setWalletActive() {
        navigationBinding.homeImage.setImageResource(R.drawable.ic_netmeds_home_inactive);
        navigationBinding.orderImage.setImageResource(R.drawable.ic_shopping_cart_inactive);
        navigationBinding.subscriptionImage.setImageResource(R.drawable.ic_subscription_inactive);
        navigationBinding.accountImage.setImageResource(R.drawable.ic_profile_inactive);
        navigationBinding.walletImage.setImageResource(R.drawable.ic_wallet_active);

        navigationBinding.homeText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.orderText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.subscriptionText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.accountText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.walletText.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
    }

    public void setOrderActive() {
        navigationBinding.homeImage.setImageResource(R.drawable.ic_netmeds_home_inactive);
        navigationBinding.orderImage.setImageResource(R.drawable.ic_shopping_cart_active);
        navigationBinding.subscriptionImage.setImageResource(R.drawable.ic_subscription_inactive);
        navigationBinding.accountImage.setImageResource(R.drawable.ic_profile_inactive);
        navigationBinding.walletImage.setImageResource(R.drawable.ic_wallet_inactive);

        navigationBinding.homeText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.orderText.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
        navigationBinding.subscriptionText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.accountText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.walletText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
    }

    public void setAccountActive() {
        navigationBinding.homeImage.setImageResource(R.drawable.ic_netmeds_home_inactive);
        navigationBinding.orderImage.setImageResource(R.drawable.ic_shopping_cart_inactive);
        navigationBinding.subscriptionImage.setImageResource(R.drawable.ic_subscription_inactive);
        navigationBinding.accountImage.setImageResource(R.drawable.ic_profile_active);
        navigationBinding.walletImage.setImageResource(R.drawable.ic_wallet_inactive);

        navigationBinding.homeText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.orderText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.subscriptionText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
        navigationBinding.accountText.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
        navigationBinding.walletText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
    }
}
