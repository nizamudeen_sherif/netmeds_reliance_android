package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySocialLoginOtpBinding;
import com.netmedsmarketplace.netmeds.ui.SocialLoginOtpActivity;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarChangePasswordResponse;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.EncryptionUtils;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.util.HashMap;
import java.util.Map;

import static com.nms.netmeds.base.IntentConstant.MOBILE_NO_UPDATE_FLAG;
import static com.nms.netmeds.base.IntentConstant.RANDOM_KEY;
import static com.nms.netmeds.base.IntentConstant.RESET_PASSWORD_FLAG;

public class SocialLoginOtpViewModel extends AppViewModel {

    private ActivitySocialLoginOtpBinding activitySocialLoginOtpBinding;
    private SocialLoginOtpListener callback;
    private int failedTransactionId;
    private Intent getIntent;
    private boolean resetFlag = false;
    private String otp;

    public SocialLoginOtpViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivitySocialLoginOtpBinding signinWithOtpBinding, SocialLoginOtpActivity signInWithOtpListener, Intent intent) {
        this.activitySocialLoginOtpBinding = signinWithOtpBinding;
        this.callback = signInWithOtpListener;
        this.getIntent = intent;
        this.resetFlag = intent.getBooleanExtra(RESET_PASSWORD_FLAG, false);
        if (intent.getBooleanExtra(MOBILE_NO_UPDATE_FLAG, false)) {
            sendOtp();
        }
        imageOptionButtonClick();
    }

    private void imageOptionButtonClick() {
        activitySocialLoginOtpBinding.txtOtp6.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isFromSocialLogin()) {
                        if (TextUtils.isEmpty(getOtp())) {
                            callback.vmSnackBarMessage(callback.getContext().getString(R.string.text_enter_otp));
                        } else
                            socialLogin();
                    } else {
                        if (validateForm()) {
                            changePassword();
                        }
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.vmNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_COMPLETE_CHANGE_PSSWRD:
                    APIServiceManager.getInstance().mstarCompleteChangePassword(this, getPhoneNo(), getRandomKey(), getOtp(), getNewPassword());
                    break;
                case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                    APIServiceManager.getInstance().mstarResendSameOTP(this, getRandomKey());
                    break;
                case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                    APIServiceManager.getInstance().mStarSocialLogin(this, getSocialLoginRequest(),getPreference().getMstarGoogleAdvertisingId());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, getPreference().getMstarBasicHeaderMap());
                    break;
            }
        } else {
            callback.vmDismissProgress();
            failedTransactionId = transactionId;
        }
    }

    private String getNewPassword() {
        return activitySocialLoginOtpBinding.newPassword.getText() != null ? activitySocialLoginOtpBinding.newPassword.getText().toString() : "";
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.MSTAR_COMPLETE_CHANGE_PSSWRD:
                completeChangePasswordResponse(data);
                break;
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                resendSameOTPResponse(data);
                break;
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLoginResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
        }
    }

    private void resendSameOTPResponse(String data) {
        callback.vmDismissProgress();
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null) {
            callback.vmCounterTimer();
            callback.vmSnackBarMessage(callback.getContext().getResources().getString(R.string.text_successful_otp_sent));
        } else {
            callback.vmSnackBarMessage(response != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng()) ? response.getReason().getReason_eng() : "");
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        callback.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_COMPLETE_CHANGE_PSSWRD:
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                vmOnError(transactionId);
                break;
        }
    }


    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.MSTAR_COMPLETE_CHANGE_PSSWRD:
                changePassword();
                break;
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                sendOtp();
                break;
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLogin();
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails();
                break;
        }
    }

    public void onEditTextFirstTextChanged(int count) {
        if (count > 0) {
            activitySocialLoginOtpBinding.txtOtp2.requestFocus();
        }
    }

    public void onEditTextSecondTextChanged(int count) {
        if (count > 0) {
            activitySocialLoginOtpBinding.txtOtp3.requestFocus();
        } else {

            activitySocialLoginOtpBinding.txtOtp1.requestFocus();
        }

    }

    public void onEditTextThirdTextChanged(int count) {
        if (count > 0) {
            activitySocialLoginOtpBinding.txtOtp4.requestFocus();
        } else {
            activitySocialLoginOtpBinding.txtOtp2.requestFocus();
        }
    }

    public void onEditTextFourTextChanged(int count) {
        if (count > 0) {
            activitySocialLoginOtpBinding.txtOtp5.requestFocus();
        } else {
            activitySocialLoginOtpBinding.txtOtp3.requestFocus();
        }
    }

    public void onEditTextFiveTextChanged(int count) {
        if (count > 0) {
            activitySocialLoginOtpBinding.txtOtp6.requestFocus();
        } else {
            activitySocialLoginOtpBinding.txtOtp4.requestFocus();
        }
    }

    public void onEditTextSixTextChanged(int count) {
        if (count <= 0) {
            activitySocialLoginOtpBinding.txtOtp5.requestFocus();
        }

    }

    public void onclickVerify() {
        if (isFromSocialLogin()) {
            if (TextUtils.isEmpty(getOtp())) {
                callback.vmSnackBarMessage(callback.getContext().getString(R.string.text_enter_otp));
            } else
                socialLogin();
        } else {
            if (validateForm()) {
                changePassword();
            }
        }
    }

    public void onclickResendOtp() {
        sendOtp();
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        onRetry(failedTransactionId);
        callback.vmWebserviceErrorView(false);
    }


    public void onclickChangePhoneNumber() {
        callback.vmCallBackOnNavigationMobileNumberActivity();
    }

    public void onNewPasswordEditTextChanged() {
        activitySocialLoginOtpBinding.inputPassword.setError(null);
    }

    public void onConfirmPasswordEditTextChanged() {
        activitySocialLoginOtpBinding.confirmInputPassword.setError(null);
    }

    private void sendOtp() {
        callback.vmShowProgress();
        initiateAPICall(APIServiceManager.MSTAR_RESEND_SAME_OTP);
    }

    private void changePassword() {
        callback.vmShowProgress();
        initiateAPICall(APIServiceManager.MSTAR_COMPLETE_CHANGE_PSSWRD);
    }

    private String getOtp() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(activitySocialLoginOtpBinding.txtOtp1 != null && activitySocialLoginOtpBinding.txtOtp1.getText() != null ? activitySocialLoginOtpBinding.txtOtp1.getText().toString() : "");
        stringBuilder.append(activitySocialLoginOtpBinding.txtOtp2 != null && activitySocialLoginOtpBinding.txtOtp2.getText() != null ? activitySocialLoginOtpBinding.txtOtp2.getText().toString() : "");
        stringBuilder.append(activitySocialLoginOtpBinding.txtOtp3 != null && activitySocialLoginOtpBinding.txtOtp3.getText() != null ? activitySocialLoginOtpBinding.txtOtp3.getText().toString() : "");
        stringBuilder.append(activitySocialLoginOtpBinding.txtOtp4 != null && activitySocialLoginOtpBinding.txtOtp4.getText() != null ? activitySocialLoginOtpBinding.txtOtp4.getText().toString() : "");
        stringBuilder.append(activitySocialLoginOtpBinding.txtOtp5 != null && activitySocialLoginOtpBinding.txtOtp5.getText() != null ? activitySocialLoginOtpBinding.txtOtp5.getText().toString() : "");
        stringBuilder.append(activitySocialLoginOtpBinding.txtOtp6 != null && activitySocialLoginOtpBinding.txtOtp6.getText() != null ? activitySocialLoginOtpBinding.txtOtp6.getText().toString() : "");
        otp = String.valueOf(stringBuilder);
        return String.valueOf(stringBuilder);
    }


    private boolean validateForm() {
        boolean isValidate = true;
        if (TextUtils.isEmpty(getOtp())) {
            isValidate = false;
            callback.vmSnackBarMessage(callback.getContext().getString(R.string.text_enter_otp));
        } else if (TextUtils.isEmpty(activitySocialLoginOtpBinding.newPassword.getText().toString())) {
            activitySocialLoginOtpBinding.inputPassword.setError(callback.getContext().getResources().getString(R.string.text_password_validate));
            isValidate = false;
        } else if (!ValidationUtils.checkLoginSetPassword(activitySocialLoginOtpBinding.confirmPassword, activitySocialLoginOtpBinding.confirmInputPassword, true, callback.getContext().getString(R.string.error_password_empty), callback.getContext().getResources().getString(R.string.error_password_length))) {
            isValidate = false;
        } else if (!ValidationUtils.checkConfirmPassword(activitySocialLoginOtpBinding.newPassword, activitySocialLoginOtpBinding.confirmPassword, activitySocialLoginOtpBinding.confirmInputPassword, callback.getContext().getResources().getString(R.string.text_password_mismatch))) {
            isValidate = false;
        }
        return isValidate;
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        callback.vmWebserviceErrorView(true);
    }

    private void completeChangePasswordResponse(String data) {
        callback.vmDismissProgress();
        MstarChangePasswordResponse response = new Gson().fromJson(data, MstarChangePasswordResponse.class);
        if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
            if (getIntent.getBooleanExtra(IntentConstant.MOBILE_NO_UPDATE_FLAG, false)) {
                callback.vmUpdateMobileNO();
            } else if (getIntent.getBooleanExtra(RESET_PASSWORD_FLAG, false)) {
                callback.vmCallbackSignInWithPasswordActivity();
                Toast.makeText(callback.getContext(), callback.getContext().getResources().getString(R.string.text_password_change_successful), Toast.LENGTH_SHORT).show();
            }
        } else if (response != null && AppConstant.API_MSTAR_FAILURE_STATUS.equalsIgnoreCase(response.getStatus()) && response.getReason() != null) {
            callback.vmSnackBarMessage(!TextUtils.isEmpty(response.getReason().getReason_eng()) ? response.getReason().getReason_eng() : callback.getContext().getResources().getString(R.string.text_something_went_wrong));
        }
    }

    private Map<String, String> getSocialLoginRequest() {
        Map<String, String> socialLoginRequest = new HashMap<>();
        socialLoginRequest.put(AppConstant.SIGNATURE, "");
        socialLoginRequest.put(AppConstant.PAYLOAD, "");
        socialLoginRequest.put(AppConstant.ACCESS_TOKEN, getAccessToken() != null && !TextUtils.isEmpty(getAccessToken()) ? getAccessToken() : "");
        socialLoginRequest.put(AppConstant.SOCIAL_FLAG, getSocialFlag() != null && !TextUtils.isEmpty(getSocialFlag()) ? getSocialFlag() : "");
        socialLoginRequest.put(AppConstant.SOURCE, AppConstant.SOURCE_NAME);
        socialLoginRequest.put(AppConstant.MOBILE_NUMBER, getPhoneNo());
        socialLoginRequest.put(AppConstant.RANDOM_KEY, String.valueOf(EncryptionUtils.base64Conversion(String.format("%s%s%s", getRandomKey(), "+", getOtp()))));
        return socialLoginRequest;
    }

    private void socialLogin() {
        callback.vmShowProgress();
        initiateAPICall(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
    }

    private void socialLoginResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel socialLoginResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (socialLoginResponse != null && socialLoginResponse.getStatus() != null) {
                if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {
                    setSocialLoginSuccessResponse(socialLoginResponse);
                } else if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_MSTAR_FAILURE_STATUS)) {
                    setSocialLoginFailureResponse(socialLoginResponse);
                }
            } else {
                callback.vmDismissProgress();
                vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
            }
        } else {
            callback.vmDismissProgress();
            vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
        }
    }

    private void setSocialLoginSuccessResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        if (socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null) {
            String sessionId = !TextUtils.isEmpty(socialLoginResponse.getResult().getSession().getId()) ? socialLoginResponse.getResult().getSession().getId() : "";
            long customerId = socialLoginResponse.getResult().getSession().getCustomerId();
            String loganSessionId = !TextUtils.isEmpty(socialLoginResponse.getResult().getSession().getLoganSessionId()) ? socialLoginResponse.getResult().getSession().getLoganSessionId() : "";

            getPreference().setMStarSessionId(sessionId, Long.toString(customerId));
            getPreference().setGuestCart(false);
            getPreference().setMstarCustomerId(Long.toString(customerId));
            getPreference().setLoganSession(loganSessionId);

            getCustomerDetails();
        } else callback.vmDismissProgress();
    }

    private void setSocialLoginFailureResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        callback.vmDismissProgress();
        if (socialLoginResponse.getReason() != null && socialLoginResponse.getReason().getReason_eng() != null && !TextUtils.isEmpty(socialLoginResponse.getReason().getReason_eng())) {
            callback.vmSnackBarMessage(socialLoginResponse.getReason().getReason_eng());
        }
    }

    private void getCustomerDetails() {
        initiateAPICall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    private void customerDetailsResponse(String data) {
        callback.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel customerDetailResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (customerDetailResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(customerDetailResponse.getStatus()) && customerDetailResponse.getResult() != null && customerDetailResponse.getResult().getCustomerDetails() != null) {
                getPreference().setPreviousCartShippingAddressId(customerDetailResponse.getResult().getCustomerDetails().getPreferredShippingAddress());
                getPreference().setCustomerDetails(new Gson().toJson(customerDetailResponse.getResult().getCustomerDetails()));

                //Set WebEngage Login
                WebEngageHelper.getInstance().setWebEngageLogin(getPreference(), WebEngageHelper.SOCIAL_WIDGET, true, callback.getContext());
                //MAT Registration Event
                MATHelper.getInstance().loginAndRegistrationMATEvent(getPreference(), true);
                //Facebook Registration Event
                FacebookPixelHelper.getInstance().logRegistrationEvent(callback.getContext());
                //Google Analytics Event
                GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_REGISTERED_SUCCESSFUL, GoogleAnalyticsHelper.EVENT_LABEL_SIGN_UP);

                callback.vmNavigateToHome();
            }
        }
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(callback.getContext());
    }

    public String getAccessToken() {
        return getIntent != null && getIntent.hasExtra(IntentConstant.TOKEN) ? getIntent.getStringExtra(IntentConstant.TOKEN) : "";
    }

    public String getSocialFlag() {
        return getIntent != null && getIntent.hasExtra(IntentConstant.DOMAIN) ? getIntent.getStringExtra(IntentConstant.DOMAIN) : "";
    }

    private String getRandomKey() {
        return getIntent != null && getIntent.hasExtra(RANDOM_KEY) ? getIntent.getStringExtra(IntentConstant.RANDOM_KEY) : "";
    }

    public String getPhoneNo() {
        return getIntent != null && getIntent.hasExtra(IntentConstant.PHONE_NUMBER) ? getIntent.getStringExtra(IntentConstant.PHONE_NUMBER) : "";
    }

    public boolean isFromSocialLogin() {
        return getIntent != null && getIntent.hasExtra(IntentConstant.SOCIAL_LOGIN_FLAG) && getIntent.getBooleanExtra(IntentConstant.SOCIAL_LOGIN_FLAG, false);
    }

    public interface SocialLoginOtpListener {
        void vmCallBackOnNavigationMobileNumberActivity();

        void vmShowProgress();

        void vmDismissProgress();

        void vmNavigateToHome();

        void vmCounterTimer();

        void vmCallbackSignInWithPasswordActivity();

        void vmUpdateMobileNO();

        Context getContext();

        void vmNoNetworkView(boolean isConnected);

        void vmWebserviceErrorView(boolean isWebserviceError);

        void vmSnackBarMessage(String message);
    }
}
