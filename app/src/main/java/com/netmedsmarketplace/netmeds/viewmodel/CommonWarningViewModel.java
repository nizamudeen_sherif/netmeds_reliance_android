package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.BaseViewModel;

public class CommonWarningViewModel extends BaseViewModel {

    public interface M2UpdatedWarningDialogViewModelListener {
        void onPositiveButtonClicked();

        void onNegativeButtonClicked();

        void onDismissButtonClicked();
    }

    public CommonWarningViewModel(@NonNull Application application) {
        super(application);
    }

    private M2UpdatedWarningDialogViewModelListener listener;

    public void setM2UpdatedWarningDialogViewModelListener(M2UpdatedWarningDialogViewModelListener listener) {
        this.listener = listener;
    }

    public void onPositiveButtonClicked() {
        listener.onPositiveButtonClicked();
    }

    public void onNegativeButtonClicked() {
        listener.onNegativeButtonClicked();
    }

    public void OnclickDismiss() {
        listener.onDismissButtonClicked();
    }
}
