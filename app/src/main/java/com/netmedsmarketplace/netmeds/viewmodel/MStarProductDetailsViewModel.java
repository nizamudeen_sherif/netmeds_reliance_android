package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.FrequentlyBroughtTogetherProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.MstarCouponCodeAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductBundleAdapter;
import com.netmedsmarketplace.netmeds.adpater.RelatedProductListAdapter;
import com.netmedsmarketplace.netmeds.adpater.SimilarProductsAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.model.MStarAddMultipleProductsResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.nms.netmeds.base.model.ProductPackRulesModel;
import com.nms.netmeds.base.model.PromoCodeList;
import com.nms.netmeds.base.model.RecommendedProductResponse;
import com.nms.netmeds.base.model.Variant;
import com.nms.netmeds.base.model.VariantFacet;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.AlgoliaClickAndConversionHelper;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.webengage.sdk.android.WebEngage;

import org.json.JSONArray;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;

public class MStarProductDetailsViewModel extends AppViewModel implements RelatedProductListAdapter.RelatedProductListAdapterListener,
        ProductBundleAdapter.ProductBundleInterface, SimilarProductsAdapter.ProductCombinationInterface, FrequentlyBroughtTogetherProductAdapter.ProductCombinationInterface {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private BasePreference basePreference;
    private MStarProductDetails productDetails;
    private ConfigurationResponse configurationResponse;
    private MStarProductDetailViewModelCallBack callBack;

    private String patientName;
    private String medicineName;
    private String phoneNumber;
    private String productIdList;
    private String pageName;

    private BigDecimal frequentlyBroughtTogetherTotalAmount = BigDecimal.ZERO;

    private int productId;
    private static int FREQ_BOUGHT_QTY = 1;
    private int productSelectedInQtyPicker = 1;
    private int productSelectedQuantity = 1;
    private int similarOrFrequentlyBroughtTogetherTransactionId;
    private int originalProductCode = 0;

    private boolean isPrescriptionProduct = false;
    private boolean isClickViewAll = false;
    private boolean isForAlternate = false;
    private boolean isForMultipleAdd = false;

    private final MutableLiveData<MstarAlgoliaResponse> mstarAlgoliaMutableLiveData = new MutableLiveData<>();

    private List<MStarProductDetails> similarProductList = new ArrayList<>();
    private List<MStarProductDetails> frqBuyTogetherProductList = new ArrayList<>();
    private List<MstarAlgoliaResult> alternateSaltList;
    private List<PromoCodeList> promoCodeLists = new ArrayList<>();
    private ArrayList<HRVProduct> hrvProductArrayList = new ArrayList<>();
    public ArrayList<String> outOfStockProductCodeList = new ArrayList<>();


    public MStarProductDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void initiateViewModel(Context context, MStarProductDetailViewModelCallBack callBack, BasePreference basePreference, int productId, String pageName) {
        this.context = context;
        this.callBack = callBack;
        this.basePreference = basePreference;
        this.productId = productId;
        this.originalProductCode = productId;
        this.configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        this.pageName = !TextUtils.isEmpty(pageName) ? pageName : "";
        initiateAPICall(APIServiceManager.MSTAR_PRODUCT_DETAILS);
    }

    public void addAllProducts() {
        if (frqBuyTogetherProductList != null && frqBuyTogetherProductList.size() > 0) {
            isForMultipleAdd = true;
            checkCartStatusToAddMultipleToCart();
        }
    }


    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        callBack.showNoNetworkView(isConnected);
        if (isConnected) {
            callBack.vmShowProgress();
            switch (transactionId) {
                case APIServiceManager.MSTAR_PRODUCT_DETAILS_AND_ADD_TO_CART:
                case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                    callBack.hideAndShowParentView(transactionId == APIServiceManager.MSTAR_PRODUCT_DETAILS);
                    APIServiceManager.getInstance().mStarProductDetails(this, productId, transactionId);
                    break;
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basePreference.getMstarBasicHeaderMap(), productId, getProductSelectedQuantity(), isSubscription() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() : null);
                    break;
                case APIServiceManager.MSTAR_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                    APIServiceManager.getInstance().mStarProductDetailsForIdList(this, productIdList, similarOrFrequentlyBroughtTogetherTransactionId);
                    break;
                case APIServiceManager.C_MSTAR_HIGH_RANGE_TICKET_POPUP:
                    APIServiceManager.getInstance().highRangeTicketPopUp(this, getMultiPartRequest());
                    break;
                case AppServiceManager.ALGOLIA_SEARCH:
                    AppServiceManager.getInstance().algoliaSearch(MStarProductDetailsViewModel.this, "", 0, AppConstant.ALTERNATE_SALTS_HIT_COUNT, CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_API_KEY), CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_INDEX),
                            CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_APP_ID), false, getFacetFilterQuery(), getFilterQuery());
                    break;
                case APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS:
                    APIServiceManager.getInstance().mstarAddMutlipleProducts(this, basePreference.getMstarBasicHeaderMap(), getProductList());
                    break;
                case APIServiceManager.C_MSTAR_COUPON_LIST:
                    APIServiceManager.getInstance().couponList(this);
                    break;
                case APIServiceManager.C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS:
                    APIServiceManager.getInstance().mstarGetPeopleAlosViewedProducts(this, getPeopleAlsoViewedUrl());
                    break;
                case APIServiceManager.C_MSTAR_NOTIFY_ME:
                    APIServiceManager.getInstance().notifyMe(this, getMultiPartNotifyRequest());
                    break;

            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        callBack.setIsFromVariantFlagChange(false);
        switch (transactionId) {
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                productDetailsResponse(data);
                break;
            case APIServiceManager.MSTAR_PRODUCT_DETAILS_AND_ADD_TO_CART:
                productDetailsForAddToCartResponse(data);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                addProductToCartResponse(data);
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                continueAfterCreateAndShoppingCartResponse(data);
                break;
            case APIServiceManager.C_MSTAR_HIGH_RANGE_TICKET_POPUP:
                requestPopupResponse(data);
                break;
            case APIServiceManager.MSTAR_FREQUENTLY_BROUGHT_TOGETHER_PRODUCT:
            case APIServiceManager.MSTAR_SIMILAR_PRODUCT:
                similarOrFrequentlyBroughtTogetherProductResponse(data, transactionId);
                break;
            case AppServiceManager.ALGOLIA_SEARCH:
                MstarAlgoliaResponse mstarAlgoliaResponse = new Gson().fromJson(data, MstarAlgoliaResponse.class);
                mstarAlgoliaMutableLiveData.setValue(mstarAlgoliaResponse);
                break;
            case APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS:
                onAddedProductResponse(data);
                break;
            case APIServiceManager.C_MSTAR_COUPON_LIST:
                couponResponse(data);
                break;
            case APIServiceManager.C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS:
                onBrainSinsPeopleAlsoViewedProductsResponse(data);
                break;
            case APIServiceManager.C_MSTAR_NOTIFY_ME:
                onNotifyResponse(data);
                break;
        }
        callBack.vmDismissProgress();
    }


    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callBack.setIsFromVariantFlagChange(false);
        callBack.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
            case APIServiceManager.C_MSTAR_HIGH_RANGE_TICKET_POPUP:
                callBack.showWebserviceErrorView(true);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
            case APIServiceManager.MSTAR_CREATE_CART:
            case APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS:
                showUnableToAddErrorMessage();
                break;
            case AppServiceManager.GET_PRODUCT_RECOMMENDATION_ENGINE:
                //TODO BRAIN SINS DATA FAILED THEN CALL ALGOLIA TO GET RELATED PRODUCTS
                break;
            case APIServiceManager.MSTAR_FREQUENTLY_BROUGHT_TOGETHER_PRODUCT:
            case APIServiceManager.MSTAR_SIMILAR_PRODUCT:
                similarOrFrequentlyBroughtTogetherProductResponse(null, transactionId);
                break;
        }
    }

    private String getPeopleAlsoViewedUrl() {
        String url = ConfigMap.getInstance().getProperty(ConfigConstant.RECOMMENDATION_ENGINE_URL) + String.format(ConfigMap.getInstance().getProperty(ConfigConstant.RECOMMENDED_PRODUCT_URL),
                this.productDetails.getProductCode(), basePreference.getMstarCustomerId(), PaymentUtils.getDeCodedString(configurationResponse.getResult().getConfigDetails().getBrainsinsKey()), !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getPeopleAlsoView().getTemplateId()) ? configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getPeopleAlsoView().getTemplateId() : "");
        return url;
    }

    private HashMap<String, Integer> getProductList() {
        HashMap<String, Integer> productList = new HashMap<>();
        productList.put(String.valueOf(productDetails.getProductCode()), productSelectedQuantity);
        for (MStarProductDetails product : frqBuyTogetherProductList) {
            productList.put(String.valueOf(product.getProductCode()), FREQ_BOUGHT_QTY);
        }
        return productList;
    }

    private MultipartBody getMultiPartRequest() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart(AppConstant.PATIENT_NAME, patientName);
        builder.addFormDataPart(AppConstant.CUSTOMER_PHONE, phoneNumber);
        builder.addFormDataPart(AppConstant.HIGH_PRODUCT_NAME, medicineName);
        return builder.build();

    }

    private MultipartBody getMultiPartNotifyRequest() {
        MStarCustomerDetails customerData = new Gson().fromJson(BasePreference.getInstance(getApplication()).getCustomerDetails(), MStarCustomerDetails.class);
        if (customerData != null) {
            String firstName = !TextUtils.isEmpty(customerData.getFirstName()) ? customerData.getFirstName() : "";
            String lastName = !TextUtils.isEmpty(customerData.getLastName()) ? customerData.getLastName() : "";

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(AppConstant.FULL_NAME, firstName + " " + lastName);
            builder.addFormDataPart(AppConstant.ACCOUNT_EMAIL, !TextUtils.isEmpty(customerData.getEmail()) ? customerData.getEmail() : "");
            builder.addFormDataPart(AppConstant.DRUGCODE, getDrugCode());
            builder.addFormDataPart(AppConstant.PRODUCT_NAME, productName());
            builder.addFormDataPart(AppConstant.DRUG_TYPE, getDrugType());
            builder.addFormDataPart(AppConstant.PHONE, customerData.getMobileNo());
            return builder.build();
        } else
            return null;
    }


    private void onNotifyResponse(String data) {
        MstarRequestPopupResponse response = new Gson().fromJson(data, MstarRequestPopupResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null) {
            callBack.snackBarCallback(response.getResult());
        }
    }

    private void onBrainSinsPeopleAlsoViewedProductsResponse(String data) {
        RecommendedProductResponse recommendedProductResponse = new Gson().fromJson(data, RecommendedProductResponse.class);
        if (recommendedProductResponse != null && recommendedProductResponse.getRecommendedProductList() != null && recommendedProductResponse.getRecommendedProductList().size() > 0) {
            this.hrvProductArrayList = recommendedProductResponse.getRecommendedProductList();
            RelatedProductListAdapter relatedProductListAdapter = new RelatedProductListAdapter(context, recommendedProductResponse.getRecommendedProductList(), this, true);
            callBack.setRelatedProductAdapter(relatedProductListAdapter);
        }
    }

    private void onAddedProductResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            boolean isProductNotAdded = false;
            boolean isProductOutOfStock = false;
            boolean isProblemInAddingProduct = false;
            MStarAddMultipleProductsResponse response = new Gson().fromJson(data, MStarAddMultipleProductsResponse.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getAddedProductsResultMap() != null && !response.getAddedProductsResultMap().isEmpty()) {
                for (String productCode : response.getAddedProductsResultMap().keySet()) {
                    if (!AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getAddedProductsResultMap().get(productCode).getResult())) {
                        if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(response.getAddedProductsResultMap().get(productCode).getResultReasonCode())) {
                            isProductOutOfStock = true;
                            outOfStockProductCodeList.add(productCode);
                        } else if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(response.getAddedProductsResultMap().get(productCode).getResultReasonCode()))
                            isProductNotAdded = true;
                        isProblemInAddingProduct = true;
                    }
                }
                RefillHelper.setPrblmInAddingProducts(isProblemInAddingProduct);
                if (isProductOutOfStock) {
                    if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isCartAlternateEnableFlag())
                        showAlternateProduct();
                    else
                        callBack.showMessage(true, null, R.string.text_view_cart, R.string.text_added_to_cart, null);

                } else if (isProductNotAdded) {
                    callBack.showMessage(true, null, R.string.text_ok, R.string.text_max_quantity_breached, null);
                } else {
                    callBack.showMessage(true, null, R.string.text_view_cart, R.string.text_added_to_cart, null);
                }
            }
        }
    }

    public void showAlternateProduct() {
        if (outOfStockProductCodeList != null && !outOfStockProductCodeList.isEmpty()) {
            callBack.showAlternateProductForOutOfStock(outOfStockProductCodeList.get(0));
        } else {
            callBack.showMessage(true, null, R.string.text_view_cart, R.string.text_added_to_cart, null);
        }
    }

    private void productDetailsResponse(String data) {
        callBack.vmDismissProgress();
        MStarBasicResponseTemplateModel productDetails = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (productDetails != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(productDetails.getStatus()) && productDetails.getResult() != null && productDetails.getResult().getProductDetails() != null) {
            initiateAPICall(APIServiceManager.C_MSTAR_COUPON_LIST);
            callBack.hideAndShowParentView(false);
            this.productDetails = productDetails.getResult().getProductDetails();
            loadPageUsingProductDetailsResponse();
            this.isPrescriptionProduct = this.productDetails.isRxRequired();

            /*In-app message screen name*/
            Map<String, Object> inAppMessageScreenData = new HashMap<String, Object>();
            inAppMessageScreenData.put(AppConstant.PRODUCT_NAME, productName());
            inAppMessageScreenData.put(AppConstant.PRODUCT_CODE, productCode());
            String drugType = !TextUtils.isEmpty(getDrugType()) ? getDrugType().equalsIgnoreCase(AppConstant.DRUG_TYPE_O) ? AppConstant.OTC : AppConstant.RX : "";
            WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_PRODUCT_DETAILS_PAGE + " - " + drugType, inAppMessageScreenData);

            /*Google Analytics Post Screen*/
            GoogleAnalyticsHelper.getInstance().postScreen(callBack.getContext(), productName());

            /*WebEngage Product Viewed Event*/
            WebEngageHelper.getInstance().productViewedEvent(callBack.getContext(), productDetails.getResult().getProductDetails());
            /*Google Tag Manager + FireBase product detail view event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseProductDetailViewEvent(callBack.getContext(), productDetails.getResult().getProductDetails());
        } else {
            callBack.showWebserviceErrorView(true);
        }
    }

    private void productDetailsForAddToCartResponse(String data) {
        callBack.vmDismissProgress();
        MStarBasicResponseTemplateModel productDetails = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (productDetails != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(productDetails.getStatus()) && productDetails.getResult() != null && productDetails.getResult().getProductDetails() != null) {
            callBack.launchAddToCartBottomSheet(productDetails.getResult().getProductDetails());
        } else {
            callBack.showWebserviceErrorView(true);
        }
    }

    private void loadPageUsingProductDetailsResponse() {
        initiateQuantityPicker();
        loadProductImages(false);
        getBrainSinsProducts();
        genericNameWithUrl();
        loadProductVariants();
        if (!isProductTypeP() && CommonUtils.isProductAvailableWithStock(productDetails)) {
            initiatePackOfBuyProcess();
            //FOR SIMILAR PRODUCT LIST
            initiateSimilarAndFrequentlyBroughtTogetherProducts(productDetails.getSimilarProducts(), APIServiceManager.MSTAR_SIMILAR_PRODUCT);
            //FOR FREQUENTLY BROUGHT TOGETHER PRODUCT LIST
            initiateSimilarAndFrequentlyBroughtTogetherProducts(productDetails.getFrequentlyBoughtTogether(), APIServiceManager.MSTAR_FREQUENTLY_BROUGHT_TOGETHER_PRODUCT);
        }
        callBack.reInitiateDataUsingViewModel(CommonUtils.isProductAvailableWithStock(productDetails));
        if (isPrescriptionRequired()) {
            callBack.showMessage(true, null, R.string.text_got_it, R.string.text_require_rx, null);
        }
    }

    private void loadProductVariants() {
        List<Variant> productVariantList = productDetails.getProductVariant();
        boolean isVariantsEnableFromConfig = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isVarientAndroidEnableFlag();
        boolean isVariantAvailable = productVariantList != null && !productVariantList.isEmpty() && isVariantsEnableFromConfig;
        callBack.setProductVariantVisibility(isVariantAvailable);
        if (isVariantAvailable) {
            callBack.initProductVariant(productVariantList);
        }
    }

    private void initiateQuantityPicker() {
        callBack.initiateProductQuantityPicker(productDetails.getStockQty() < productDetails.getMaxQtyInOrder() ? productDetails.getStockQty() : productDetails.getMaxQtyInOrder());
    }

    private void loadProductImages(boolean isForViewimages) {
        if (productDetails.getImagePaths() != null && !productDetails.getImagePaths().isEmpty()) {
            MStarBasicResponseTemplateModel resultTemplateModel = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
            String imageUrlBasePath = resultTemplateModel != null && resultTemplateModel.getResult() != null && !TextUtils.isEmpty(resultTemplateModel.getResult().getProductImageUrlBasePath()) ?
                    resultTemplateModel.getResult().getProductImageUrlBasePath() : ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_PRODUCT_IMAGE_BASE_URL);
            List<String> imageUrlPathList = new ArrayList<>();
            for (String imageUrl : this.productDetails.getImagePaths()) {
                imageUrlPathList.add(imageUrlBasePath + AppConstant.MSTAR_IMAGE_SIZE_600 + "/" + imageUrl);
            }
            if (isForViewimages) {
                callBack.onItemClickListenerFromSearch(imageUrlPathList);
            } else {
                callBack.loadProductImages(imageUrlPathList);
            }
        }
    }

    private void getBrainSinsProducts() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getBrainsinsKey()) && configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues() != null
                && configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getPeopleAlsoView() != null && configurationResponse.getResult().getConfigDetails().getBrainsinStatusFlag() && configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getPeopleAlsoView().isEnableStatus() && isProductTypeO()) {
            initiateAPICall(APIServiceManager.C_MSTAR_PEOPLE_ALSO_VIEWED_BRAINSINS);
        } else {
            fetchAlternateSaltsFromAlgolia();
        }
    }

    private void fetchAlternateSaltsFromAlgolia() {
        if (isProductTypeP() && !TextUtils.isEmpty(getGenericName())) {
            initiateAPICall(AppServiceManager.ALGOLIA_SEARCH);
        } else {
            callBack.setRelatedProductAdapter(null);
        }
    }

    private void initiatePackOfBuyProcess() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getProductDetailsFlags() != null &&
                configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().isPackOfBuy() && isAvailable() && productDetails != null && productDetails.getPackOfNRules() != null &&
                !productDetails.getPackOfNRules().getRules().isEmpty()) {
            MStarBasicResponseTemplateModel resultTemplateModel = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
            List<ProductPackRulesModel> packOfNRulesModelList = new ArrayList<>();
            int productActualQuantity = productDetails.getStockQty() < productDetails.getMaxQtyInOrder() ? productDetails.getStockQty() : productDetails.getMaxQtyInOrder();
            for (ProductPackRulesModel packRulesModel : productDetails.getPackOfNRules().getRules()) {
                if (packRulesModel.getQty() <= productActualQuantity) {
                    packRulesModel.setProductCode(productDetails.getProductCode());
                    packRulesModel.setProductName(productDetails.getDisplayName());
                    String imageBaseUrl = resultTemplateModel != null && resultTemplateModel.getResult() != null && !TextUtils.isEmpty(resultTemplateModel.getResult().getProductImageUrlBasePath()) ?
                            resultTemplateModel.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" : ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_PRODUCT_IMAGE_BASE_URL);
                    packRulesModel.setProductImageUrl(productDetails.getImagePaths().isEmpty() ? "" : imageBaseUrl + productDetails.getImagePaths().get(0));
                    packOfNRulesModelList.add(packRulesModel);
                }
            }
            if (!packOfNRulesModelList.isEmpty()) {
                callBack.setPayOfBuyViewWithAdapter(new ProductBundleAdapter(packOfNRulesModelList, this, callBack.getContext()));
            } else {
                callBack.setPayOfBuyViewWithAdapter(null);
            }
        } else {
            callBack.setPayOfBuyViewWithAdapter(null);
        }
    }

    private void initiateSimilarAndFrequentlyBroughtTogetherProducts(String productIdList, int similarOrFrequentlyBroughtTogetherTransactionId) {
        if (!TextUtils.isEmpty(productIdList)) {
            this.productIdList = productIdList;
            this.similarOrFrequentlyBroughtTogetherTransactionId = similarOrFrequentlyBroughtTogetherTransactionId;
            initiateAPICall(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
        }
    }

    private void addProductToCartResponse(String data) {
        callBack.vmDismissProgress();
        MStarBasicResponseTemplateModel addToCartResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addToCartResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addToCartResponse.getStatus())) {
            /*WebEngage AddToCart Event*/
            if (productDetails != null && !TextUtils.isEmpty(pageName)) {
                productDetails.setPageName(callBack.getContext().getString(R.string.text_products_ordered));
            }
            WebEngageHelper.getInstance().addToCartEvent(callBack.getContext(), productDetails, getProductSelectedQuantity(), false);
            /*Facebook Pixel AddToCart Event*/
            FacebookPixelHelper.getInstance().logAddedToCartEvent(getApplication(), productDetails, getProductSelectedQuantity());
            /*Google Tag Manager + FireBase AddToCart Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseAddToCartEvent(getApplication(), productDetails, getProductSelectedQuantity());

            if (isForAlternate && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isCartAlternateEnableFlag())
                showAlternateProduct();
            callBack.vmAddCartCallBack();
        } else if (addToCartResponse != null && addToCartResponse.getReason() != null) {
            callBack.snackBarCallback(addToCartResponse.getReason().getReason_eng());
        } else {
            showUnableToAddErrorMessage();
        }
    }

    private void continueAfterCreateAndShoppingCartResponse(String data) {
        MStarBasicResponseTemplateModel productDetails = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (productDetails != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(productDetails.getStatus())) {
            basePreference.setMStarCartId(productDetails.getResult().getCart_id());
            if (isForMultipleAdd)
                initiateAPICall(APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS);
            else
                checkCartStatusToAddToCart();
        } else {
            callBack.vmDismissProgress();
            showUnableToAddErrorMessage();
        }
    }

    private void similarOrFrequentlyBroughtTogetherProductResponse(String data, int transactionID) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel basicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (basicResponseTemplateModel.getResult() != null && basicResponseTemplateModel.getResult().getProductDetailsList() != null && !basicResponseTemplateModel.getResult().getProductDetailsList().isEmpty() &&
                    configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getProductDetailsFlags() != null &&
                    ((APIServiceManager.MSTAR_SIMILAR_PRODUCT == transactionID) ? configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().isSimilarEnable() : configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().isComboEnable())) {

                for (MStarProductDetails productDetails : basicResponseTemplateModel.getResult().getProductDetailsList()) {
                    if (CommonUtils.isProductAvailableWithStock(productDetails) && APIServiceManager.MSTAR_SIMILAR_PRODUCT == transactionID) {
                        similarProductList.add(productDetails);
                    }

                    if (APIServiceManager.MSTAR_FREQUENTLY_BROUGHT_TOGETHER_PRODUCT == transactionID) {
                        frqBuyTogetherProductList.add(productDetails);
                    }
                }

                if (!frqBuyTogetherProductList.contains(this.productDetails)) {
                    frqBuyTogetherProductList.add(0, this.productDetails);
                }

                if (APIServiceManager.MSTAR_SIMILAR_PRODUCT == transactionID && !similarProductList.isEmpty()) {
                    callBack.initiateSimilarProductList(new SimilarProductsAdapter(similarProductList, this, callBack.getContext()));
                } else if (APIServiceManager.MSTAR_FREQUENTLY_BROUGHT_TOGETHER_PRODUCT == transactionID && !frqBuyTogetherProductList.isEmpty() && frqBuyTogetherProductList.size() > 1) {
                    setTotalAmount(frqBuyTogetherProductList);
                    if (frqBuyTogetherProductList.isEmpty()) {
                        callBack.initiateFrequentlyBroughtTogetherProductList(null);
                    } else {
                        callBack.initiateFrequentlyBroughtTogetherProductList(new FrequentlyBroughtTogetherProductAdapter(frqBuyTogetherProductList, this, callBack.getContext()));
                    }
                } else {
                    callBack.initiateSimilarProductList(null);
                }
            } else {
                hideBundleView(transactionID);
            }

        } else {
            hideBundleView(transactionID);
        }
    }

    private void setTotalAmount(List<MStarProductDetails> similarOrFrqBuyTogetherProductList) {
        if (!similarOrFrqBuyTogetherProductList.isEmpty()) {
            for (MStarProductDetails product : similarOrFrqBuyTogetherProductList) {
                frequentlyBroughtTogetherTotalAmount = frequentlyBroughtTogetherTotalAmount.add(product.getSellingPrice());
            }
        }
    }

    private void hideBundleView(int transactionID) {
        if (APIServiceManager.MSTAR_SIMILAR_PRODUCT == transactionID) {
            callBack.initiateSimilarProductList(null);
        } else {
            callBack.initiateFrequentlyBroughtTogetherProductList(null);
        }
    }

    public String getMorePhotosText() {
        return productDetails != null && productDetails.getImagePaths() != null && !productDetails.getImagePaths().isEmpty() && productDetails.getImagePaths().size() > 1 ? callBack.getImageMoreText(productDetails.getImagePaths().size() - 1) : "";
    }

    private void requestPopupResponse(String data) {
        callBack.vmDismissProgress();
        MstarRequestPopupResponse response = new Gson().fromJson(data, MstarRequestPopupResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null) {
            callBack.snackBarCallback(response.getResult());

        }
    }

    private void couponResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);

            if (response != null && response.getResult() != null && response.getResult().getPromoCodeLists() != null && response.getResult().getPromoCodeLists().size() > 0 && productDetails != null && productDetails.getSchedule() != null) {
                for (int i = 0; i < response.getResult().getPromoCodeLists().size(); i++) {
                    if (response.getResult().getPromoCodeLists().get(i).getProductType().contains(productDetails.getSchedule())) {
                        promoCodeLists.add(response.getResult().getPromoCodeLists().get(i));
                        callBack.offerViewVisibility(true);
                        callBack.promoCodeAdapter(new MstarCouponCodeAdapter(callBack.getContext(), promoCodeLists, false), false);
                    } else
                        callBack.offerViewVisibility(false);
                }
            } else {
                callBack.offerViewVisibility(false);
            }
        }
    }

    private void showUnableToAddErrorMessage() {
        callBack.showMessage(false, null, 0, R.string.text_unable_to_add_to_cart, null);
    }

    public void navigateToHome() {
        callBack.navigateToHomePage();
    }

    public void checkColdStorageAvailability() {
        callBack.checkPinCodeAvailability(true, productName());
    }

    public void checkExpiryAndDeliveryDateDetailsForProduct() {
        callBack.checkPinCodeAvailability(false, productName());
    }

    public boolean checkExpiryAndDeliveryDateDetailsVisible() {
        return CommonUtils.isProductAvailableWithStock(productDetails);
    }

    public void previewImage() {
        loadProductImages(true);
    }

    public void submitProductRequest() {
        initiateAPICall(APIServiceManager.C_MSTAR_NOTIFY_ME);
    }

    public void addCart() {
        this.productId = originalProductCode;
        this.isForMultipleAdd = false;
        setProductSelectedQuantity(productSelectedInQtyPicker);
        if (!M2Helper.getInstance().isM2Order() && !PaymentHelper.isIsTempCart()) {
            if (basePreference.getMStarCartId() > 0) {
                setProductSelectedQuantity(getProductSelectedInQtyPicker());
                checkCartStatusToAddToCart();
            } else {
                initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
            }
        } else {
            checkCartStatusToAddToCart();
        }

        //Algolia conversion event
        postAlgoliaConversionEvent();
    }

    /**
     * Checking intent from search activity or other activity.
     * If condition true algolia analytics event send to the algolia server.
     * If condition false event not send
     */
    private void postAlgoliaConversionEvent() {
        if (callBack.isFromSearchActivity())
            AlgoliaClickAndConversionHelper.getInstance().onAlgoliaConversionEvent(context, Integer.toString(getProductCode()), BasePreference.getInstance(context).getAlgoliaQueryId());
    }

    private void checkCartStatusToAddToCart() {
        initiateAPICall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);

    }

    private void checkCartStatusToAddMultipleToCart() {
        if (PaymentHelper.isIsTempCart() || M2Helper.getInstance().isM2Order()) {
            initiateAPICall(APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS);
        } else {
            if (basePreference.getMStarCartId() > 0) {
                initiateAPICall(APIServiceManager.MSTAR_ADD_MULTIPLE_PRODUCTS);
            } else {
                isForMultipleAdd = true;
                initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
            }
        }
    }

    public void requestProduct() {
        if (basePreference.isGuestCart())
            callBack.navigateSiginActivity();
        else
            callBack.requestProduct();
    }

    public void initiateRequestProductAPICall(String patientName, String medicineName, String phoneNumber) {
        this.patientName = patientName;
        this.medicineName = medicineName;
        this.phoneNumber = phoneNumber;

        initiateAPICall(APIServiceManager.C_MSTAR_HIGH_RANGE_TICKET_POPUP);
    }

    public BigDecimal getCombinationProductTotalAmount() {
        return frequentlyBroughtTogetherTotalAmount;
    }


    public int productCode() {
        return productDetails != null ? productDetails.getProductCode() : 0;
    }

    public String productName() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getDisplayName()) ? productDetails.getDisplayName() : "";
    }

    public String getDrugType() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getProductType()) ? productDetails.getProductType() : "";
    }

    public String getDrugCode() {
        return String.valueOf(productDetails != null ? productDetails.getProductCode() : 0);
    }

    public String categoryName() {
        return productDetails != null && productDetails.getCategories() != null && !productDetails.getCategories().isEmpty() && !TextUtils.isEmpty(productDetails.getCategories().get(0).getName()) ? productDetails.getCategories().get(0).getName() : "";
    }

    public boolean isPrescriptionRequired() {
        return productDetails != null && productDetails.isRxRequired();
    }

    public boolean isProductTypeP() {
        return productDetails != null && AppConstant.DRUG_TYPE_P.equalsIgnoreCase(productDetails.getProductType());
    }

    public boolean isProductTypeO() {
        return productDetails != null && AppConstant.DRUG_TYPE_O.equalsIgnoreCase(productDetails.getProductType());
    }

    public boolean isColdStorage() {
        return productDetails != null && productDetails.isColdStorage() && !outOfStockButtonText().equalsIgnoreCase(context.getString(R.string.text_stock_not_available));
    }

    public String returnableMessage() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getReturnProductText()) ? configurationResponse.getResult().getConfigDetails().getReturnProductText() : "";
    }

    public boolean returnableMessageVisibility() {
        if (productDetails != null && productDetails.isColdStorage())
            return true;
        else
            return productDetails != null && productDetails.isReturnable();
    }

    public String drugDetail() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getPackLabel()) ? productDetails.getPackLabel() : "";
    }

    public String manufacturerName() {
        return productDetails != null && productDetails.getManufacturer() != null && !TextUtils.isEmpty(productDetails.getManufacturer().getName()) ? AppConstant.MFR + " " + productDetails.getManufacturer().getName() : "";
    }

    public String price() {
        return productDetails != null ? CommonUtils.getPriceInFormat(productDetails.getSellingPrice()) : "";
    }

    public String discount() {
        return productDetails != null && productDetails.getDiscountPercentage().compareTo(BigDecimal.ZERO) != 0 ? CommonUtils.getRoundedOffBigDecimal(productDetails.getDiscountPercentage()) + callBack.getContext().getResources().getString(R.string.text_off) : "";
    }

    public String strikePrice() {
        callBack.setPaintForStrikePrize();
        return productDetails != null && productDetails.getDiscount().compareTo(BigDecimal.ZERO) > 0 ? CommonUtils.getPriceInFormat(productDetails.getMrp()) : "";
    }

    public boolean isAvailable() {
        return CommonUtils.isProductAvailableWithStock(productDetails);

    }

    public String outOfStockButtonText() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getAvailabilityStatus()) ? context.getString(isNotAvailableProduct() ? R.string.text_stock_not_available : isNotForSale() ? R.string.text_stock_not_for_sale : R.string.text_out_of_stock) : "";
    }

    public boolean isVisibleOffer() {
        return productDetails != null && productDetails.getDiscountPercentage().compareTo(BigDecimal.ZERO) != 0;
    }

    private boolean isNotAvailableProduct() {
        return AppConstant.PRODUCT_NOT_AVAILABLE_CODE_T.equalsIgnoreCase(productDetails.getAvailabilityStatus()) ||
                AppConstant.PRODUCT_NOT_AVAILABLE_CODE_C.equalsIgnoreCase(productDetails.getAvailabilityStatus());
    }

    private boolean isNotForSale() {
        return AppConstant.PRODUCT_NOT_AVAILABLE_CODE_R.equalsIgnoreCase(productDetails.getAvailabilityStatus());
    }

    public boolean showHighPriceRequest() {
        return productDetails != null && productDetails.getSellingPrice().doubleValue() > 5000 && isProductTypeP();
    }

    public boolean isAlternateAvailable() {
//        return productListResult != null && productListResult.getPackSizeVariant() != null && productListResult.getPackSizeVariant().get(0).getAvailableStatusCode().equalsIgnoreCase("A");
        return false;
    }

    private boolean isSubscription() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag();
    }

    public String alternateDrugDetail() {
        return "";
    }

    public String alternateManufacturerName() {
        return "";
    }

    public String requestProductDescription() {
        return context.getString(R.string.text_request_product_priced);
    }

    public String defaultDiscount() {
        return productDetails != null && productDetails.getDiscountPercentage().compareTo(BigDecimal.ZERO) != 0 ? callBack.showAppliedDefaultDiscount(productDetails.getDiscountPercentage()) : "";
    }

    public String alternatePrice() {
        return "";
    }

    public String alternateStrikePrice() {
        return "";
    }

    public String promoCode() {
        return "";
    }

    public String genericName() {
        if (productDetails != null) {
            if (productDetails.getGenericWithDosage() != null) {
                return !TextUtils.isEmpty(productDetails.getGenericWithDosage().getName()) ? productDetails.getGenericWithDosage().getName() : "";
            } else if (productDetails.getGeneric() != null) {
                return !TextUtils.isEmpty(productDetails.getGeneric().getName()) ? productDetails.getGeneric().getName() : "";
            }
        }
        return "";
    }

    private void genericNameWithUrl() {
        if (productDetails != null && !productDetails.isRxRequired()) {
            callBack.loadProductContentWithUrl(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL) + productDetails.getUrlPath());
        } else {
            String genericContent = getGenericContent();
            if (!TextUtils.isEmpty(genericContent)) {
                //TODO GET FROM CONFIG
                callBack.loadProductContentWithUrl(ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_CUSTOM_API_URL) + genericContent);
            }
        }
    }

    private String getGenericContent() {
        return productDetails != null && isProductTypeP() && !TextUtils.isEmpty(productDetails.getUrlPath()) ? productDetails.getUrlPath() : "";
    }

    //TODO LOAD EXTRA CONTENT FOR PRODUCT WHICH WAS LOAD IN BOTTOM OF THE PAGE INSTEAD OF NEW PAGE IN WEBVIEW.
    public boolean isProductExtraDetailsAvailable() {
        return false;
    }

    public String promoCodeDescription() {
        return "";
    }

    public String promoCodeDiscount() {
        return "";
    }

    public boolean isDiscountAvailable() {
//        return promoCodeLists != null && promoCodeLists.size() > 0 && !TextUtils.isEmpty(promoCodeLists.get(0).getDiscount());
        return false;
    }

    public String relatedTitle() {
        return context.getString(isProductTypeP() ? R.string.text_alternate_salt : R.string.text_people_also_viewed);
    }

    public void setProductSelectedQuantity(int productSelectedQuantityUsingPicker) {
        this.productSelectedQuantity = productSelectedQuantityUsingPicker;
    }

    public int getProductSelectedQuantity() {
        return productSelectedQuantity;
    }

    public String getComboEnableTitleText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getProductDetailsFlags() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().getComboEnableTitle()) ? configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().getComboEnableTitle() : context.getResources().getString(R.string.text_frequently_bought) : context.getResources().getString(R.string.text_frequently_bought);
    }

    public String getPackOfBuyTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getProductDetailsFlags() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().getPackOfBuyTitle()) ? configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().getPackOfBuyTitle() : context.getResources().getString(R.string.text_product_combination) : context.getResources().getString(R.string.text_product_combination);
    }

    public String getSimilarEnableTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getProductDetailsFlags() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().getSimilarEnableTitle()) ? configurationResponse.getResult().getConfigDetails().getProductDetailsFlags().getSimilarEnableTitle() : context.getResources().getString(R.string.text_similar_product) : context.getResources().getString(R.string.text_similar_product);
    }

    public void onManufacturerNameClicked() {
        callBack.openManufacturerList(productDetails != null && productDetails.getManufacturerId() > 0 ? productDetails.getManufacturerId() : 0, productDetails != null && productDetails.getManufacturer() != null && !TextUtils.isEmpty(productDetails.getManufacturer().getName()) ? productDetails.getManufacturer().getName() : "", productDetails != null && productDetails.getProductType() != null && !TextUtils.isEmpty(productDetails.getProductType()) ? productDetails.getProductType() : "");
    }

    public void onClickAllOffer() {
        if (!isClickViewAll) {
            this.isClickViewAll = true;
            callBack.promoCodeAdapter(new MstarCouponCodeAdapter(callBack.getContext(), promoCodeLists, true), true);
        } else {
            this.isClickViewAll = false;
            callBack.promoCodeAdapter(new MstarCouponCodeAdapter(callBack.getContext(), promoCodeLists, false), false);

        }
    }

    public void onClickNotify() {
        MStarProductDetails.setProductDetails(this.productDetails);
        callBack.notifyDialogue();
    }

    public boolean outOfStackAvailable() {
        /** To Do solve it from cherry pick conflicts*/
        return !CommonUtils.isProductAvailableWithStock(productDetails);

    }

    public boolean outOfStack() {
        /** To Do solve it from cherry pick conflicts*/
        return !CommonUtils.isProductAvailableWithStock(productDetails);

    }

    public boolean isStatusButtonAvailable() {
        /** To Do solve it from cherry pick conflicts*/
        return !CommonUtils.isProductAvailableWithStock(productDetails) && outOfStockButtonText().equalsIgnoreCase(context.getString(R.string.text_out_of_stock));
    }

    @Override
    public String getPackString(Integer qty) {
        return callBack.getPackStringWithQty(qty);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public String getImageUrl(MStarProductDetails productDetails) {
        MStarBasicResponseTemplateModel resultTemplateModel = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        return productDetails != null && !productDetails.getImagePaths().isEmpty() && resultTemplateModel != null && resultTemplateModel.getResult() != null && !TextUtils.isEmpty(resultTemplateModel.getResult().getProductImageUrlBasePath()) ? resultTemplateModel.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" + productDetails.getImagePaths().get(0) : "";
    }

    @Override
    public View.OnClickListener onAddBundleClickListener(final ProductPackRulesModel packRulesModel) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productId = packRulesModel.getProductCode();
                setProductSelectedQuantity(packRulesModel.getQty());
                isForMultipleAdd = false;
                if (basePreference.getMStarCartId() > 0) {
                    checkCartStatusToAddToCart();

                } else {
                    initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
                }
            }
        };
    }

    @Override
    public View.OnClickListener navigateToProduct(final int sku) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callBack.navigateToProductDetailsPage(sku);
            }
        };
    }

    @Override
    public View.OnClickListener getProductDetailsForAddToCart(final int productID) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                productId = productID;
                initiateAPICall(APIServiceManager.MSTAR_PRODUCT_DETAILS_AND_ADD_TO_CART);
            }
        };
    }

    @Override
    public View.OnClickListener launchAddToCartBottomSheet(final MStarProductDetails productDetails) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callBack.launchAddToCartBottomSheet(productDetails);
            }
        };
    }

    /**
     * This method return facetFilter option like category ids,manufacturer id and brand id
     *
     * @return facetFilter jsonArray
     */
    private JSONArray getFacetFilterQuery() {
        JSONArray facetFilter = new JSONArray();
        facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_GENERIC_WITH_DOSAGE_ID + ":" + getGenericWithDosage());
        return facetFilter;
    }

    public String getFilterQuery() {
        return "\"" + AppConstant.ALGOLIA_FILTER_KEY_FORMULATION_TYPE + "\":\"" + getFormulationType() + "\"" + " " + AppConstant.AND + " " + "(" + "\"" + AppConstant.ALGOLIA_FILTER_KEY_AVAILABILITY_STATUS + "\":\"" + AppConstant.PRODUCT_AVAILABLE_CODE + "\"" + " " + AppConstant.OR + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_AVAILABILITY_STATUS + "\":\"" + AppConstant.PRODUCT_OUT_OF_STOCK + "\")";
    }

    public MutableLiveData<MstarAlgoliaResponse> getMstarAlgoliaMutableLiveData() {
        return mstarAlgoliaMutableLiveData;
    }

    /**
     * Handling alternate salt response
     *
     * @param mstarAlgoliaResponse alternate salt response
     */
    public void onAlternateDataAvailable(MstarAlgoliaResponse mstarAlgoliaResponse) {
        if (mstarAlgoliaResponse != null && mstarAlgoliaResponse.getNbHits() > 0) {
            ArrayList<HRVProduct> alternateSaltList = new ArrayList<>();
            for (MstarAlgoliaResult mstarAlgoliaResult : mstarAlgoliaResponse.getAlgoliaResultList()) {
                if (productId != mstarAlgoliaResult.getProductCode()) {
                    HRVProduct hrvProduct = new HRVProduct();
                    hrvProduct.setBrandName(!TextUtils.isEmpty(mstarAlgoliaResult.getDisplayName()) ? mstarAlgoliaResult.getDisplayName() : "");
                    hrvProduct.setManufacturerName(!TextUtils.isEmpty(mstarAlgoliaResult.getManufacturerName()) ? mstarAlgoliaResult.getManufacturerName() : "");
                    hrvProduct.setPackSizeTypeLabel(!TextUtils.isEmpty(mstarAlgoliaResult.getPackLabel()) ? mstarAlgoliaResult.getPackLabel() : "");
                    hrvProduct.setDrugType(!TextUtils.isEmpty(mstarAlgoliaResult.getProductType()) ? mstarAlgoliaResult.getProductType() : "");
                    hrvProduct.setParentCategoryName(mstarAlgoliaResult.getCategories() != null && mstarAlgoliaResult.getCategories().size() > 0 ? mstarAlgoliaResult.getCategories().get(mstarAlgoliaResult.getCategories().size() - 1) : "");
                    hrvProduct.setPrice(mstarAlgoliaResult.getSellingPrice().toString());
                    hrvProduct.setActualPrice(mstarAlgoliaResult.getMrp().toString());
                    hrvProduct.setAvailableStatus(!TextUtils.isEmpty(mstarAlgoliaResult.getAvailabilityStatus()) ? mstarAlgoliaResult.getAvailabilityStatus() : "");
                    hrvProduct.setImageUrl(!TextUtils.isEmpty(mstarAlgoliaResult.getThumbnailUrl()) ? mstarAlgoliaResult.getThumbnailUrl() : "");
                    hrvProduct.setId(Integer.toString(mstarAlgoliaResult.getProductCode()));
                    hrvProduct.setRxRequired(mstarAlgoliaResult.getRxRequired());
                    alternateSaltList.add(hrvProduct);
                }
            }
            setAlternateSaltList(mstarAlgoliaResponse.getAlgoliaResultList());
            if (alternateSaltList.size() > 0) {
                Collections.sort(alternateSaltList, new Comparator<HRVProduct>() {
                    @Override
                    public int compare(HRVProduct p1, HRVProduct p2) {
                        int compareStatus = p1.getAvailableStatus().compareTo(p2.getAvailableStatus());
                        return compareStatus == 0 ? Double.compare(Double.parseDouble(p1.getPrice()), Double.parseDouble(p2.getPrice())) : compareStatus;
                    }
                });
                RelatedProductListAdapter relatedProductListAdapter = new RelatedProductListAdapter(context, alternateSaltList, this, false);
                callBack.setRelatedProductAdapter(relatedProductListAdapter);
                callBack.hideAndShowViewAll(alternateSaltList.size() > 3);
                callBack.hideRelatedProductView(true);
            } else {
                callBack.hideRelatedProductView(false);
                callBack.setRelatedProductAdapter(null);
            }
        } else {
            callBack.hideRelatedProductView(false);
            callBack.setRelatedProductAdapter(null);
        }
    }


    public String getGenericName() {
        return productDetails != null && productDetails.getGenericWithDosage() != null && !TextUtils.isEmpty(productDetails.getGenericWithDosage().getName()) ? productDetails.getGenericWithDosage().getName() : productDetails != null && productDetails.getGeneric() != null && !TextUtils.isEmpty(productDetails.getGeneric().getName()) ? productDetails.getGeneric().getName() : "";
    }

    public int getProductCode() {
        return productDetails != null ? productDetails.getProductCode() : 0;
    }

    private String getFormulationType() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getFormulationType()) ? productDetails.getFormulationType() : "";
    }

    public String getGenericWithDosage() {
        return productDetails != null && productDetails.getGenericWithDosage() != null ? Integer.toString(productDetails.getGenericWithDosage().getId()) : "";
    }

    public List<MstarAlgoliaResult> getAlternateSaltList() {
        return alternateSaltList;
    }

    private void setAlternateSaltList(List<MstarAlgoliaResult> alternateSaltList) {
        this.alternateSaltList = alternateSaltList;
    }

    public int getProductSelectedInQtyPicker() {
        return productSelectedInQtyPicker;
    }

    public void setProductSelectedInQtyPicker(int productSelectedInQtyPicker) {
        this.productSelectedInQtyPicker = productSelectedInQtyPicker;
    }

    public boolean isPrescriptionProduct() {
        return isPrescriptionProduct;
    }

    public void setPrescriptionProduct(boolean prescriptionProduct) {
        isPrescriptionProduct = prescriptionProduct;
    }


    public List<HRVProduct> getPeopleAlsoViewedList() {
        return this.hrvProductArrayList;
    }

    public void addAlternateSalt(int productToAdd, int quantity, int outOfStockProductCode) {
        this.productId = productToAdd;
        this.isForAlternate = true;
        setProductSelectedQuantity(quantity);
        initiateAPICall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
        if (!outOfStockProductCodeList.isEmpty() && outOfStockProductCodeList.contains(String.valueOf(outOfStockProductCode)))
            outOfStockProductCodeList.remove(String.valueOf(outOfStockProductCode));
    }

    private String fireBaseEventProductCategory() {
        if (productDetails != null) {
            if (productDetails.getProductType().equalsIgnoreCase(AppConstant.OTC_ORDER)) {
                return AppConstant.OTC;
            } else if (productDetails.getProductType().equalsIgnoreCase(AppConstant.PRESCRIPTION_STATUS_PENDING)) {
                if (productDetails.isRxRequired()) {
                    return AppConstant.PHARMA;
                } else
                    return AppConstant.NON_PHARMA;
            }
        }
        return "";
    }

    public int getSelectedVariantPosition(Variant variant) {
        for (VariantFacet variantFacet : variant.getVariantFacetList()) {
            if (variantFacet.isCurrentSelectedVariant()) {
                return variant.getVariantFacetList().indexOf(variantFacet);
            }
        }
        return 0;
    }

    public void resetDataAndInitiateProductDetailsCall(int productId) {
        this.productId = productId;
        originalProductCode = productId;
        productSelectedInQtyPicker = 1;
        productSelectedQuantity = 1;
        similarProductList = new ArrayList<>();
        frqBuyTogetherProductList = new ArrayList<>();
        promoCodeLists = new ArrayList<>();
        hrvProductArrayList = new ArrayList<>();
        outOfStockProductCodeList = new ArrayList<>();
        isPrescriptionProduct = false;
        isClickViewAll = false;
        isForAlternate = false;
        isForMultipleAdd = false;
        frequentlyBroughtTogetherTotalAmount = BigDecimal.ZERO;
        initiateAPICall(APIServiceManager.MSTAR_PRODUCT_DETAILS);
    }

    public interface MStarProductDetailViewModelCallBack {
        void vmShowProgress();

        void vmDismissProgress();

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        void showMessage(boolean isActionEnabled, String message, int actionMessageId, int messageId, Object data);

        void vmAddCartCallBack();

        void reInitiateDataUsingViewModel(boolean productAvailableWithStock);

        void navigateToHomePage();

        void setPaintForStrikePrize();

        void initiateProductQuantityPicker(int maxSelectableQuantity);

        String showAppliedDefaultDiscount(BigDecimal discount);

        void checkPinCodeAvailability(boolean isFromColdStorageCheck, String productName);

        void loadProductImages(List<String> imagePaths);

        void requestProduct();

        void setRelatedProductAdapter(RelatedProductListAdapter adapter);

        void setPayOfBuyViewWithAdapter(ProductBundleAdapter adapter);

        String getPackStringWithQty(Integer qty);

        void initiateSimilarProductList(SimilarProductsAdapter adapter);

        void initiateFrequentlyBroughtTogetherProductList(FrequentlyBroughtTogetherProductAdapter adapter);

        void navigateToProductDetailsPage(int productCode);

        void launchAddToCartBottomSheet(MStarProductDetails productDetails);

        String getImageMoreText(int imageListSize);

        void onItemClickListenerFromSearch(List<String> urlList);

        void loadProductContentWithUrl(String url);

        void hideAndShowParentView(boolean isVisible);

        void openManufacturerList(int manufacturerId, String manufacturerName, String productType);

        void notifyDialogue();

        void snackBarCallback(String message);

        void hideAndShowViewAll(boolean isVisible);

        void hideRelatedProductView(boolean isVisible);

        void promoCodeAdapter(MstarCouponCodeAdapter promoCodeAdapter, boolean isClickOffer);

        Context getContext();

        void offerViewVisibility(boolean isVisible);

        void navigateSiginActivity();

        void showAlternateProductForOutOfStock(String outOfStockProductCode);

        boolean isFromSearchActivity();

        void setProductVariantVisibility(boolean isVariantVisible);

        void initProductVariant(List<Variant> productVariantList);

        void setIsFromVariantFlagChange(boolean isVariantResetFlag);
    }
}
