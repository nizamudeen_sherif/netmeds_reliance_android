package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.BaseViewModel;

public class CODWarningDialogViewModel extends BaseViewModel{

    private CODWarningDialogViewModelListener listener;

    public CODWarningDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public interface CODWarningDialogViewModelListener {

        void onPositiveButtonClicked();

        void onNegativeButtonClicked();

    }

    public void setListener(CODWarningDialogViewModelListener listener) {
        this.listener = listener;
    }

    public void onNegativeButtonClicked(){
        listener.onNegativeButtonClicked();
    }

    public void onPositiveButtonClicked(){
        listener.onPositiveButtonClicked();
    }

}
