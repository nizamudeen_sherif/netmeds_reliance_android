package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.NetmedsApp;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResult;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class AddressViewModel extends AppViewModel {

    private BasePreference mBasePreference;
    private AddressListener mListener;
    private MStarAddressModel diagnosticCustomerAddress;
    private Bundle diagnosticOrderCreation;
    private DiagnosticPackage diagnosticPackage;
    private AvailableLab availableLab;
    private LabDescription labDescription;
    private String diagnosticSelectedType = "";
    private String intentFrom = "";

    private int failedTransactionId = 0;

    //MORNING_STAR
    private MStarAddressModel selectedAddress;

    private List<MStarAddressModel> allAddressList = new ArrayList<>();

    private int billingAndShippingAddressId = 0;

    private int addressId = -1;
    private boolean deleteFlag = false;

    private boolean isSetShippingAddressAPICallCompleted = false;
    private boolean isSetBillingAddressAPICallCompleted = false;
    private boolean isSetShippingAddressCompleted = false;
    private boolean isSetBillingAddressCompleted = false;
    private MStarAddressModel selectingAddress;
    public boolean isNewAddress;
    private boolean fromDiagnosticHome, fromDiagnosticPatientDetails = false;


    public AddressViewModel(Application application) {
        super(application);
    }

    public void initViewModel(BasePreference basePreference, AddressListener listener) {
        this.mBasePreference = basePreference;
        this.mListener = listener;
        mListener.enableAddressList(true);
        getLabDescription();
    }

    private void onRetry(int failedTransactionId) {
        initiateAPICall(failedTransactionId);
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        mListener.showNoNetworkView(isConnected);
        if (isConnected) {
            mListener.vmShowProgress();
            switch (transactionId) {
                case APIServiceManager.MSTAR_GET_ALL_ADDRESS:
                    APIServiceManager.getInstance().mStarGetAllAddress(this, mBasePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateCart(this, mBasePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_DELETE_ADDRESS:
                    APIServiceManager.getInstance().mStarDeleteAddress(this, mBasePreference.getMstarBasicHeaderMap(), addressId);
                    break;
                case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, mBasePreference.getMstarBasicHeaderMap(), billingAndShippingAddressId, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                    break;
                case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, mBasePreference.getMstarBasicHeaderMap(), billingAndShippingAddressId, APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId, getCartId());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, mBasePreference.getMstarBasicHeaderMap());
                    break;
            }
        } else {
            failedTransactionId = transactionId;
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_CART:
                createCartResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_ALL_ADDRESS:
                NetmedsApp.getInstance().setAddAddress(false);
                getCustomerDeliverAddressResponse(data);
                if (isFromDiagnosticHome())
                    mListener.navigateToAddOrUpdateAddress(new MStarAddressModel(), false);
                break;
            case APIServiceManager.MSTAR_DELETE_ADDRESS:
                deleteAddressResponse(data);
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                getPinCodeResponse(data);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                showApiError(DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK);
                mListener.hideBottomView();
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
            case APIServiceManager.MSTAR_GET_ALL_ADDRESS:
                showApiError(transactionId);
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        onRetry(failedTransactionId);
        mListener.showWebserviceErrorView(false);
        mListener.showNoNetworkView(true);
    }


    public void addNewAddress() {
        /*Google Analytics Click Events*/
        GoogleAnalyticsHelper.getInstance().postEvent(mListener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NEW_ADDRESS, GoogleAnalyticsHelper.EVENT_ACTION_ADD_NEW_ADDRESS, GoogleAnalyticsHelper.EVENT_LABEL_SELECT_ADDRESS_PAGE);
        mListener.navigateToAddOrUpdateAddress(new MStarAddressModel(), false);
    }

    public String getButtonText() {
        return mListener.getContext().getString(R.string.text_order_review);
    }

    public void getCustomerAllAddress() {
        initiateAPICall(APIServiceManager.MSTAR_GET_ALL_ADDRESS);
    }

    public void deleteAddressFromList(int id) {
        this.addressId = id;
        initiateAPICall(APIServiceManager.MSTAR_DELETE_ADDRESS);
        if (mBasePreference.getPreviousCartBillingAddressId() == id) {
            deleteFlag = true;
            updateAddressAndContinue();
        }
    }

    private void setDiagBillingAndShippingAddress(){
        if(mBasePreference.getMstarDiagnosticCartId() > 0){
            initiateAPICall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
        }else{
            initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
        }
    }

    public void setPreferedBillingAndShippingAddressCall() {
        if(isFromDiagPatientDetails()){
            setDiagBillingAndShippingAddress();
            return;
        }
        if (getCartId() > 0) {
            initiateAPICall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
        } else {
            initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
        }
    }

    private int getCartId() {
        return M2Helper.getInstance().isM2Order() ? mBasePreference.getMstarMethod2CartId() : PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : isSubscription();
    }

    public void updateAddressAndContinue() {
        /*Google Analytics Click Events*/

        /*FireBase Analytics Events*/
        FireBaseAnalyticsHelper.getInstance().CheckoutAddressSelectEvent(mListener.getContext(), AppConstant.ADDRESS_SELECTED);
        /*Google Tag Manager + FireBase Checkout Progress Step3 Event*/
        FireBaseAnalyticsHelper.getInstance().logFireBaseCheckoutProgressEvent(mListener.getContext(),PaymentHelper.getCartLineItems(),PaymentHelper.getAnalyticalTrackingProductList(),FireBaseAnalyticsHelper.CHECKOUT_STEP_3,FireBaseAnalyticsHelper.EVENT_PARAM_SELECT_ADDRESS);

        resetFlag();
        if (!deleteFlag)
            mListener.navigateToReviewPages();
    }

    private void resetFlag() {
        isSetShippingAddressAPICallCompleted = false;
        isSetBillingAddressAPICallCompleted = false;
        isSetShippingAddressCompleted = false;
        isSetBillingAddressCompleted = false;
    }

    private void getCustomerDeliverAddressResponse(String data) {
        mListener.vmDismissProgress();
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        /**
         * PREVIOUS ORDER  BILLING AND SHIPPING ADDRESS SELECTION CHECKING
         */
        if (addressData != null && addressData.getResult() != null && addressData.getResult().getAddressList() != null && !addressData.getResult().getAddressList().isEmpty()) {
            isNewAddress = false;
            allAddressList = addressData.getResult().getAddressList();
            if (!((mBasePreference.getPreviousCartBillingAddressId() != -1) && (mBasePreference.getPreviousCartShippingAddressId() != -1))) {
                initiateAPICall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
            } else {
                mListener.setAddressListAdapter(allAddressList);
            }
        } else {
            isNewAddress = true;
            mListener.navigateToAddOrUpdateAddress(new MStarAddressModel(), false);
        }
    }

    private void createCartResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
                mBasePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
                if(isFromDiagPatientDetails()){
                    mBasePreference.setMstarDiagnosticCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
                }
                // call set billing and shipping address after created the universal cart
                setPreferedBillingAndShippingAddressCall();
            }
        }
    }

    private void settingBillingAndShippingAddressResponse(String data, int transactionId) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus())) {
            if ((APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS == transactionId) || (APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS == transactionId)) {
                isSetBillingAddressAPICallCompleted = true;
            } else if ((APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId) || (APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId)) {
                isSetShippingAddressAPICallCompleted = true;
            }
            if (isSetBillingAddressAPICallCompleted) {
                isSetBillingAddressCompleted = AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus());
            }
            if (isSetShippingAddressAPICallCompleted) {
                isSetShippingAddressCompleted = AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus());
            }
            if (isSetShippingAddressAPICallCompleted && isSetBillingAddressAPICallCompleted) {
                mListener.vmDismissProgress();
                mListener.settingBillingAndShippingAddressResponse(isSetShippingAddressCompleted, isSetBillingAddressCompleted, deleteFlag);
            }
        } else {
            if (addressData != null && AppConstant.CART_NOT_FOUND.equalsIgnoreCase(addressData.getReason() != null ? addressData.getReason().getReason_code() : "")) {
                initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
                return;
            }
        }
    }

    private void deleteAddressResponse(String data) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus())) {
            getCustomerAllAddress();
            mListener.showErrorMessage(mListener.getContext().getResources().getString(R.string.text_address_removed_sucess));
        } else {
            mListener.vmDismissProgress();
            if (addressData.getReason() != null && !TextUtils.isEmpty(addressData.getReason().getReason_eng())) {
                mListener.showErrorMessage(addressData.getReason().getReason_eng());
            }
        }
        if (getIntentFrom() != null && getIntentFrom().equals(DiagnosticConstant.KEY_DIAGNOSTIC))
            mListener.hideBottomView();
    }

    private void customerDetailsResponse(String data) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && addressData.getResult() != null) {
            mBasePreference.setPreviousCartBillingAddressId(addressData.getResult().getCustomerDetails().getPreferredBillingAddress());
            mBasePreference.setPreviousCartShippingAddressId(addressData.getResult().getCustomerDetails().getPreferredShippingAddress());
            mListener.setAddressListAdapter(allAddressList);
        } else {
            mListener.vmDismissProgress();
        }
    }

    public void pinCodeServiceCheck(String code) {
        boolean isConnected = NetworkUtils.isConnected(mListener.getContext());
        if (isConnected) {
            PinCodeServiceCheckRequest serviceCheckRequest = new PinCodeServiceCheckRequest();
            serviceCheckRequest.setPinCode(code);
            serviceCheckRequest.setLabId(getLabId());
            mListener.vmShowProgress();
            DiagnosticServiceManager.getInstance().pinCodeServiceAvailabilityCheck(this, serviceCheckRequest, mBasePreference);
        } else
            failedTransactionId = DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK;
    }

    private void getPinCodeResponse(String data) {
        mListener.vmDismissProgress();
        PinCodeResponse pinCodeResponse = new Gson().fromJson(data, PinCodeResponse.class);
        pinCodeDataAvailable(pinCodeResponse);
    }

    private void pinCodeDataAvailable(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse != null) {
            checkPinCodeServiceStatus(pinCodeResponse);
        } else {
            mListener.hideBottomView();
            showApiError(DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK);
        }
    }

    private void checkPinCodeServiceStatus(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getServiceStatus() != null && pinCodeResponse.getServiceStatus().getStatusCode() != null && pinCodeResponse.getServiceStatus().getStatusCode() == 200) {
            getPinCodeResponseResult(pinCodeResponse);
        } else {
            mListener.hideBottomView();
            setPinCodeServiceErrorMessage(pinCodeResponse.getResult());
        }
    }

    private void setPinCodeServiceErrorMessage(PinCodeResult result) {
        if (result != null && !TextUtils.isEmpty(result.getMessage()))
            mListener.showErrorMessage(result.getMessage());
    }

    private void getPinCodeResponseResult(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getResult() != null) {
            getPinCodeService(pinCodeResponse.getResult());
        } else {
            mListener.hideBottomView();
            showApiError(DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK);
        }
    }

    private void getPinCodeService(PinCodeResult pinCodeResult) {
        if (pinCodeResult.getService()) {
            String pinCode = !TextUtils.isEmpty(pinCodeResult.getPinCode()) ? pinCodeResult.getPinCode() : "";
            String city = !TextUtils.isEmpty(pinCodeResult.getCity()) ? pinCodeResult.getCity() : "";
            mBasePreference.setDiagnosticPinCode(pinCode);
            DiagnosticHelper.setCity(city);
            mBasePreference.setDiagnosticCity(city);
            setBottomView();
            /*Diagnostic WebEngage PinCode Enter Event*/
            WebEngageHelper.getInstance().diagnosticEnterPinCodeEvent(mBasePreference, city, mListener.getContext());
        } else {
            mListener.hideBottomView();
            mListener.showErrorMessage(mListener.getContext().getResources().getString(R.string.text_diagnostic_pin_code_service));
        }
    }

    public boolean showContinueButton() {
        return getIntentFrom() != null && getIntentFrom().equals(DiagnosticConstant.KEY_DIAGNOSTIC);
    }

    public void onNextCallback() {
        mListener.vmCallBackNext();
    }

    public void onBottomCallback() {
        List<Test> testList = null;
        if (getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST))
            testList = availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? availableLab.getTestList() : new ArrayList<Test>();
        else
            testList = diagnosticPackage != null && diagnosticPackage.getTestList() != null && diagnosticPackage.getTestList().size() > 0 ? diagnosticPackage.getTestList() : new ArrayList<Test>();

        mListener.vmInitBottomView(testList);
    }

    public void setBottomView() {
        if (getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST))
            setBottomViewTest();
        else
            setBottomViewPackage();
    }

    private void setBottomViewTest() {
        if (diagnosticOrderCreation != null && diagnosticOrderCreation.containsKey(DiagnosticConstant.KEY_SELECTED_LAB)) {
            availableLab = (AvailableLab) diagnosticOrderCreation.getSerializable(DiagnosticConstant.KEY_SELECTED_LAB);
            PriceDescription priceDescription = availableLab != null && availableLab.getPriceDescription() != null ? availableLab.getPriceDescription() : new PriceDescription();
            if (!TextUtils.isEmpty(priceDescription.getFinalPrice()))
                mListener.setBottomViewTestAndPackage(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, Double.parseDouble(priceDescription.getFinalPrice())));
            mListener.showTextLabel();
        }
    }

    private void setBottomViewPackage() {
        if (diagnosticOrderCreation != null && diagnosticOrderCreation.containsKey(DiagnosticConstant.KEY_SELECTED_PACKAGE)) {
            diagnosticPackage = (DiagnosticPackage) diagnosticOrderCreation.getSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE);
            PriceDescription priceDescription = diagnosticPackage != null && diagnosticPackage.getPriceDescription() != null ? diagnosticPackage.getPriceDescription() : new PriceDescription();
            if (!TextUtils.isEmpty(priceDescription.getFinalPrice()))
                mListener.setBottomViewTestAndPackage(String.format(Locale.getDefault(), "%s %.2f", DiagnosticConstant.INR, Double.parseDouble(priceDescription.getFinalPrice())));
        }
    }

    private void getLabDescription() {
        if (getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST)) {
            if (diagnosticOrderCreation != null && diagnosticOrderCreation.containsKey(DiagnosticConstant.KEY_SELECTED_LAB)) {
                availableLab = (AvailableLab) diagnosticOrderCreation.getSerializable(DiagnosticConstant.KEY_SELECTED_LAB);
                labDescription = availableLab != null && availableLab.getLabDescription() != null ? availableLab.getLabDescription() : new LabDescription();
            }
        } else {
            if (diagnosticOrderCreation != null && diagnosticOrderCreation.containsKey(DiagnosticConstant.KEY_SELECTED_PACKAGE)) {
                diagnosticPackage = (DiagnosticPackage) diagnosticOrderCreation.getSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE);
                labDescription = diagnosticPackage != null && diagnosticPackage.getLabDescription() != null ? diagnosticPackage.getLabDescription() : new LabDescription();
            }
        }
    }

    private int isSubscription() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : mBasePreference.getMStarCartId();
    }

    public int getLabId() {
        return labDescription != null ? labDescription.getId() : 0;
    }

    public List<MStarAddressModel> getAddressWithSelection(List<MStarAddressModel> addresses) {
        for (MStarAddressModel addressModel : addresses) {
            addressModel.setSelectedAddress(PaymentHelper.isIsEditOrder() && RefillHelper.getM2ShippingAddress() > 0 ? RefillHelper.getM2ShippingAddress() == addressModel.getId() : mBasePreference.getPreviousCartShippingAddressId() == addressModel.getId());
            if (addressModel.getSelectedAddress()) {
                Collections.swap(addresses, 0, addresses.indexOf(addressModel));
            }
        }
        return addresses;
    }

    public MStarAddressModel getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(MStarAddressModel selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    public void setShippingAndBillingAddress(int addressId) {
        billingAndShippingAddressId = addressId;
    }

    public int getBillingAndShippingAddressId() {
        return billingAndShippingAddressId;
    }

    public String getDiagnosticSelectedType() {
        return diagnosticSelectedType;
    }

    public void setDiagnosticSelectedType(String diagnosticSelectedType) {
        this.diagnosticSelectedType = diagnosticSelectedType;
    }

    public Bundle getDiagnosticOrderCreation() {
        return diagnosticOrderCreation;
    }

    public void setDiagnosticOrderCreation(Bundle diagnosticOrderCreation) {
        this.diagnosticOrderCreation = diagnosticOrderCreation;
        if (diagnosticOrderCreation != null) {
            setDiagnosticSelectedType(diagnosticOrderCreation.containsKey(DiagnosticConstant.KEY_SELECTED_LAB) ? DiagnosticConstant.TEST : DiagnosticConstant.PACKAGE);
        }
    }

    public MStarAddressModel getDiagnosticCustomerAddress() {
        return diagnosticCustomerAddress;
    }

    public void setDiagnosticCustomerAddress(MStarAddressModel diagnosticCustomerAddress) {
        this.diagnosticCustomerAddress = diagnosticCustomerAddress;
    }

    public String getIntentFrom() {
        return intentFrom;
    }

    public void setIntentFrom(String intentFrom) {
        this.intentFrom = intentFrom;
    }

    public void setSelectingAddress(MStarAddressModel selectingAddress) {
        this.selectingAddress = selectingAddress;
    }

    public MStarAddressModel getSelectingAddress() {
        return selectingAddress;
    }


    private void showApiError(int transactionId) {
        failedTransactionId = transactionId;
        mListener.showWebserviceErrorView(true);
    }

    public boolean isFromDiagnosticHome() {
        return fromDiagnosticHome;
    }

    public void setFromDiagnosticHome(boolean fromDiagnosticHome) {
        this.fromDiagnosticHome = fromDiagnosticHome;
    }

    public boolean isFromDiagPatientDetails() {
        return fromDiagnosticPatientDetails;
    }

    public void setFromDiagPatientDetails(boolean fromDiagnosticPatientDetails) {
        this.fromDiagnosticPatientDetails = fromDiagnosticPatientDetails;
    }

    public interface AddressListener {

        void setAddressListAdapter(List<MStarAddressModel> addresses);

        void navigateToAddOrUpdateAddress(MStarAddressModel addressModel, boolean isAddAddress);

        void vmShowProgress();

        void vmDismissProgress();

        void vmCallBackNext();

        void vmInitBottomView(List<Test> testList);

        void settingBillingAndShippingAddressResponse(boolean isShippingAddressSet, boolean isBillingAddressSet, boolean deleteFlag);

        void showErrorMessage(String errorMessage);

        void enableAddressList(boolean isAddressList);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isError);

        Context getContext();

        void hideBottomView();

        void setBottomViewTestAndPackage(String format);

        void showTextLabel();

        void navigateToReviewPages();
    }
}
