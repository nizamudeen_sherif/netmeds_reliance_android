package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.netmedsmarketplace.netmeds.databinding.DialogNeedHelpEmailBinding;
import com.nms.netmeds.base.BaseViewModel;

public class NeedHelpThroughEmailDialogViewModel extends BaseViewModel {

    private DialogNeedHelpEmailBinding binding;
    private NeedHelpThroughEmailDialogViewModelListener listener;
    private String subject;

    public NeedHelpThroughEmailDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(DialogNeedHelpEmailBinding binding, NeedHelpThroughEmailDialogViewModelListener listener, String subject) {
        this.binding = binding;
        this.listener = listener;
        this.subject = subject;
    }

    public String getEmailReason() {
        return subject;
    }

    public void closeView() {
        listener.onCancel();
    }

    public void onCancelClick() {
        listener.onCancel();
    }

    public void onSubmitClick() {
        if (binding.edtOtherReason.getText().toString().trim().isEmpty())
            listener.showErrorMessage();
        else
            listener.onSubmit(binding.edtOtherReason.getText().toString());
    }

    public interface NeedHelpThroughEmailDialogViewModelListener {

        void onSubmit(String reason);

        void onCancel();

        void showErrorMessage();
    }
}
