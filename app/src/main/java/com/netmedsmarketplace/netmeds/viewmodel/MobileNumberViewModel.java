package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityMobileNumberBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseFailureReasonTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarOtpDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;

import java.util.HashMap;
import java.util.Map;

public class MobileNumberViewModel extends AppViewModel {

    private ActivityMobileNumberBinding activityMobileNumberBinding;
    private OnMobileNumberListener callback;
    private int failedTransactionId;
    private Intent intentValue;

    public MobileNumberViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivityMobileNumberBinding activityMobileNumberBinding, OnMobileNumberListener onMobileNumberListener, Intent intent) {
        this.activityMobileNumberBinding = activityMobileNumberBinding;
        this.callback = onMobileNumberListener;
        this.intentValue = intent;
        this.activityMobileNumberBinding.tvEnterMobileNoDesc.setText(R.string.text_phone_no_access);
        imeOptionButtonClick();
    }

    private void imeOptionButtonClick() {
        activityMobileNumberBinding.phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateForm()) {
                        onIntentFrom();
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLoginResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                getUserStatusResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        callback.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                vmOnError(transactionId);
                break;
        }
    }

    public void onEditText() {
        activityMobileNumberBinding.errorText.setText("");
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        onRetry(failedTransactionId);
        callback.vmWebServiceErrorView(false);
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLogin();
                break;
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                mStarGetUserStatus();
                break;
        }
    }

    public void onClickSignIn() {
        setButtonProperties(false);
        if (validateForm()) {
            onIntentFrom();
        } else setButtonProperties(true);

    }

    private void onIntentFrom() {
        if (isFromSocialLogin())
            socialLogin();
        else
            mStarGetUserStatus();
    }

    public void setButtonProperties(boolean isEnable) {
        activityMobileNumberBinding.btnGo.setEnabled(isEnable);
        activityMobileNumberBinding.btnGo.setClickable(isEnable);
    }

    private void socialLogin() {
        callback.vmShowProgress();
        initApiCall(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
    }

    private Map<String, String> getSocialLoginRequest() {
        Map<String, String> socialLoginRequest = new HashMap<>();
        socialLoginRequest.put(AppConstant.SIGNATURE, "");
        socialLoginRequest.put(AppConstant.PAYLOAD, "");
        socialLoginRequest.put(AppConstant.ACCESS_TOKEN, getAccessToken());
        socialLoginRequest.put(AppConstant.SOCIAL_FLAG, getSocialFlag());
        socialLoginRequest.put(AppConstant.SOURCE, AppConstant.SOURCE_NAME);
        socialLoginRequest.put(AppConstant.MOBILE_NUMBER, getPhoneNo());
        socialLoginRequest.put(AppConstant.RANDOM_KEY, "");
        return socialLoginRequest;
    }

    private String getPhoneNo() {
        return activityMobileNumberBinding.phone != null && activityMobileNumberBinding.phone.getText() != null ? activityMobileNumberBinding.phone.getText().toString() : "";
    }

    private String getAccessToken() {
        return intentValue != null && intentValue.hasExtra(IntentConstant.TOKEN) ? intentValue.getStringExtra(IntentConstant.TOKEN) : "";
    }

    private String getSocialFlag() {
        return intentValue != null && intentValue.hasExtra(IntentConstant.DOMAIN) ? intentValue.getStringExtra(IntentConstant.DOMAIN) : "";
    }

    private boolean isFromSocialLogin() {
        return intentValue != null && intentValue.hasExtra(IntentConstant.SOCIAL_LOGIN_FLAG) && intentValue.getBooleanExtra(IntentConstant.SOCIAL_LOGIN_FLAG, false);
    }

    private boolean validateForm() {
        boolean isValidate = true;
        if (!ValidationUtils.checkText(activityMobileNumberBinding.phone, true, callback.getContext().getResources().getString(R.string.error_mobile), 10, 10, activityMobileNumberBinding.errorText)) {
            isValidate = false;
        }
        return isValidate;
    }


    private void vmOnError(int transactionId) {
        setButtonProperties(true);
        failedTransactionId = transactionId;
        callback.vmWebServiceErrorView(true);
    }

    private void socialLoginResponse(String data) {
        callback.vmDismissProgress();
        setButtonProperties(true);
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel socialLoginResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (socialLoginResponse != null && socialLoginResponse.getStatus() != null) {
                if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {
                    setSocialLoginSuccessResponse(socialLoginResponse);
                } else if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_MSTAR_FAILURE_STATUS)) {
                    setSocialLoginFailureResponse(socialLoginResponse);
                }
            } else
                vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
        } else
            vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
    }

    private void setSocialLoginSuccessResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        if (socialLoginResponse.getResult() != null) {
            MstarBasicResponseResultTemplateModel mStarSocialLoginResult = socialLoginResponse.getResult();
            if (!TextUtils.isEmpty(mStarSocialLoginResult.getCode()) && AppConstant.USER_EXISTS.equalsIgnoreCase(mStarSocialLoginResult.getCode())) {
                callback.vmSnackBarMessage(!TextUtils.isEmpty(mStarSocialLoginResult.getMessage()) ? mStarSocialLoginResult.getMessage() : "");
            } else
                callback.vnCallBackOnSocialLoginOtpActivity(mStarSocialLoginResult.getRandom());
        }
    }

    private void setSocialLoginFailureResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        if (socialLoginResponse.getReason() != null && socialLoginResponse.getReason().getReason_eng() != null && !TextUtils.isEmpty(socialLoginResponse.getReason().getReason_eng())) {
            callback.vmSnackBarMessage(socialLoginResponse.getReason().getReason_eng());
        }
    }

    private void initApiCall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.vmNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_GET_USER_STATUS:
                    APIServiceManager.getInstance().mStarGetUserStatus(this, getPhoneNo());
                    break;
                case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                    APIServiceManager.getInstance().mStarSocialLogin(this, getSocialLoginRequest(), BasePreference.getInstance(callback.getContext()).getMstarGoogleAdvertisingId());
                    break;
            }
        } else {
            setButtonProperties(true);
            callback.vmDismissProgress();
            failedTransactionId = transactionId;
        }
    }

    private void mStarGetUserStatus() {
        callback.vmShowProgress();
        initApiCall(APIServiceManager.MSTAR_GET_USER_STATUS);
    }

    private void getUserStatusResponse(String data) {
        callback.vmDismissProgress();
        setButtonProperties(true);
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel userStatusResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (userStatusResponse != null && userStatusResponse.getStatus() != null) {
                switch (userStatusResponse.getStatus()) {
                    case AppConstant.SUCCESS_STATUS:
                        callback.vmCallBackOnNavigationSignInWithPasswordActivity();
                        break;
                    case AppConstant.API_MSTAR_FAILURE_STATUS:
                        checkFailureReason(userStatusResponse);
                        break;
                }
            } else
                vmOnError(APIServiceManager.MSTAR_GET_USER_STATUS);
        } else
            vmOnError(APIServiceManager.MSTAR_GET_USER_STATUS);
    }

    private void checkFailureReason(MStarBasicResponseTemplateModel userStatusResponse) {
        if (userStatusResponse.getReason() != null) {
            MstarBasicResponseFailureReasonTemplateModel mStarUserStatusReason = userStatusResponse.getReason();
            if (!TextUtils.isEmpty(mStarUserStatusReason.getReason_code()) && mStarUserStatusReason.getReason_code().equalsIgnoreCase(AppConstant.NOT_FOUND)) {
                if (userStatusResponse.getResult() != null && userStatusResponse.getResult().getMstarOtpDetails() != null) {
                    MstarOtpDetails mstarOtpDetails = userStatusResponse.getResult().getMstarOtpDetails();
                    if (!TextUtils.isEmpty(mstarOtpDetails.getRandomKey())) {
                        callback.vmCallBackOnNavigationSignUpActivity(mstarOtpDetails.getRandomKey());
                    }
                }
            } else {
                callback.vmSnackBarMessage(!TextUtils.isEmpty(mStarUserStatusReason.getReason_eng()) ? mStarUserStatusReason.getReason_eng() : "");
            }
        }
    }

    public interface OnMobileNumberListener {
        void vmCallBackOnNavigationSignInWithPasswordActivity();

        void vmCallBackOnNavigationSignUpActivity(String randomKey);

        void vnCallBackOnSocialLoginOtpActivity(int randomKey);

        void vmShowProgress();

        void vmDismissProgress();

        void vmNoNetworkView(boolean isConnected);

        void vmWebServiceErrorView(boolean isWebserviceError);

        Context getContext();

        void vmSnackBarMessage(String message);
    }
}
