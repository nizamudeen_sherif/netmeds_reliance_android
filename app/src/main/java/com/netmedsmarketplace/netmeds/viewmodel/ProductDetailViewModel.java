/*
package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.AlternateBrainSinProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.AlternateProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.CartSuggestionAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductAgeVariantAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductBundleAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductColorVariantAdapter;
import com.netmedsmarketplace.netmeds.adpater.SimilarProductsAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductFlavourVariantAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductSizeVariantAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductUnitVariantAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductVariantAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityProductDetailBinding;
import com.netmedsmarketplace.netmeds.model.BundleDrugDetails;
import com.netmedsmarketplace.netmeds.model.BundleDrugDetailsList;
import com.netmedsmarketplace.netmeds.model.PackProducts;
import com.netmedsmarketplace.netmeds.model.ProductDetailResponse;
import com.netmedsmarketplace.netmeds.model.ProductDetailResult;
import com.netmedsmarketplace.netmeds.model.ProductListResult;
import com.netmedsmarketplace.netmeds.model.ProductMapping;
import com.netmedsmarketplace.netmeds.model.ProductVariantResponse;
import com.netmedsmarketplace.netmeds.model.ProductVariants;
import com.nms.netmeds.base.model.PromoCodeList;
import com.netmedsmarketplace.netmeds.model.PromoCodeResponse;
import com.nms.netmeds.base.model.RecommendedProductResponse;
import com.netmedsmarketplace.netmeds.model.RelatedProductResponse;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.netmedsmarketplace.netmeds.model.VariantType;
import com.netmedsmarketplace.netmeds.model.request.AddToCartRequest;
import com.netmedsmarketplace.netmeds.model.request.CartItems;
import com.netmedsmarketplace.netmeds.model.request.RequestPopup;
import com.netmedsmarketplace.netmeds.ui.M1ProductCheckPincodePopupDialogue;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.CartIdResponse;
import com.nms.netmeds.base.model.CartItemResult;
import com.nms.netmeds.base.model.CartResponse;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateLineItem;
import com.nms.netmeds.base.model.DeliveryEstimateProductDetail;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.DrugCode;
import com.nms.netmeds.base.model.DrugDetail;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.model.OrderSkuList;
import com.nms.netmeds.base.model.PincodeDeliveryEstimate;
import com.nms.netmeds.base.model.Product;
import com.nms.netmeds.base.model.ProductDetailsFlag;
import com.nms.netmeds.base.model.ProductItem;
import com.nms.netmeds.base.model.ReOrderStockInfoRequest;
import com.nms.netmeds.base.model.RefillCartRequest;
import com.nms.netmeds.base.model.RefillCartResponse;
import com.nms.netmeds.base.model.ServiceStatus;
import com.nms.netmeds.base.model.StockInformationResponse;
import com.nms.netmeds.base.model.UserDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.AlgoliaClickAndConversionHelper;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.base.widget.CustomNumberPicker;
import com.nms.netmeds.base.widget.CustomNumberPickerListener;
import com.nms.netmeds.payment.ui.PaymentServiceManager;
import com.nms.netmeds.payment.ui.model.CartItemsRequest;
import com.nms.netmeds.payment.ui.vm.CartResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.netmedsmarketplace.netmeds.AppServiceManager.ADD_CART;

public class ProductDetailViewModel extends AppViewModel implements CustomNumberPickerListener, ProductVariantAdapter.ProductVariantListener, CartSuggestionAdapter.CartSuggestionInterface, ProductBundleAdapter.ProductBundleInterface, SimilarProductsAdapter.ProductCombinationInterface {

    public static final String VARIANT_UNIT = "variant_units";
    public static final String VARIANT_AGE_GROUP = "variant_age_group";
    public static final String VARIANT_SIZE = "variant_size";
    public static final String VARIANT_FLAVOUR = "variant_flavour";
    public static final String VARIANT_COLOR = "variant_color";
    private static PincodeListener pincodeListener;
    private final MutableLiveData<ProductDetailResponse> detailResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<RecommendedProductResponse> recommendedProductResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ProductVariantResponse> productVariantResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<RecommendedProductResponse> suggestedProductsListMutableData = new MutableLiveData<>();
    private final MutableLiveData<CartResponse> cartItemMutableData = new MutableLiveData<>();
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityProductDetailBinding productDetailBinding;
    private int quantity = 1;
    private int pickerQuantity = 1;
    ConfigurationResponse configurationResponse;
    private ProductDetailListener productDetailListener;
    private AlternateProductAdapter.AlternateProductAdapterListener alternateProductAdapterListener;
    private AlternateBrainSinProductAdapter.AlternateBrainSinProductAdapterListener alternateBrainSinProductAdapterListener;
    private ProductListResult productListResult;
    private ArrayList<HRVProduct> alternateProductList;
    private List<PromoCodeList> promoCodeLists;
    private ProductVariants productVariants;
    private String skuId;
    private String selectedUnit = "";
    private String selectedColor = "";
    private String selectedFlavour = "";
    private String selectedSize = "";
    private String selectedAge = "";
    private String pinCode = "";
    private M1ProductCheckPincodePopupDialogue.ProductCheckPincodeSuccessListener productCheckPincodeSuccessListener;
    private boolean brainSinsFlag;
    private boolean fromCombination = false;
    private boolean isAddToCartFromAlterNateSalt;
    private int bundleLeastCount = 0;
    public int bundleCount = 1;
    private List<BundleDrugDetailsList> bundleDetailsList;
    public boolean bundleFlag = false;

    public boolean isAddToCartFromAlterNateSalt() {
        return isAddToCartFromAlterNateSalt;
    }

    public void setAddToCartFromAlterNateSalt(boolean addToCartFromAlterNateSalt) {
        isAddToCartFromAlterNateSalt = addToCartFromAlterNateSalt;
    }

    public MutableLiveData<CartResponse> getCartItemMutableData() {
        return cartItemMutableData;
    }

    public ProductDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public static void setPincodeListener(PincodeListener listener) {
        pincodeListener = listener;
    }

    public boolean isBrainSinsFlag() {
        return brainSinsFlag;
    }

    private void setBrainSinsFlag(boolean brainSinsFlag) {
        this.brainSinsFlag = brainSinsFlag;
    }

    public void init(Context context, ActivityProductDetailBinding productDetailBinding, ProductDetailListener productDetailListener, AlternateProductAdapter.AlternateProductAdapterListener alternateProductAdapterListener, M1ProductCheckPincodePopupDialogue.ProductCheckPincodeSuccessListener productCheckPincodeSuccessListener, AlternateBrainSinProductAdapter.AlternateBrainSinProductAdapterListener alternateBrainSinProductAdapterListener) {
        this.context = context;
        this.productDetailBinding = productDetailBinding;
        this.productDetailListener = productDetailListener;
        this.alternateProductAdapterListener = alternateProductAdapterListener;
        this.alternateBrainSinProductAdapterListener = alternateBrainSinProductAdapterListener;
        this.productCheckPincodeSuccessListener = productCheckPincodeSuccessListener;
        this.configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
    }

    public void checkExpiryDeliveryDetails() {
        productDetailListener.vmCheckPinCode(false);
    }


    public void getExpiryDate(String pinCode) {
        this.pinCode = pinCode;
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getDeliveryDateApi() != null && !TextUtils.isEmpty(pinCode))
            APIServiceManager.getInstance().deliveryEstimation(this, getDeliveryDateEstimate(pinCode), configurationResponse.getResult().getDeliveryDateApi() + "/");
    }

    private DeliveryEstimateRequest getDeliveryDateEstimate(String pinCode) {
        DeliveryEstimateRequest request = new DeliveryEstimateRequest();
        request.setCallingToRoute(false);
        request.setDoNotSplit(false);
        request.setPincode(pinCode);
        List<DeliveryEstimateLineItem> lineItems = new ArrayList<>();
        if (productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getSkuId()) && !TextUtils.isEmpty(productListResult.getProduct().getSkuId())) {
            DeliveryEstimateLineItem items = new DeliveryEstimateLineItem();
            items.setItemcode(productListResult.getProduct().getSkuId());
            items.setQty(quantity);
            lineItems.add(items);
        }
        request.setLstdrug(lineItems);
        return request;
    }

    public void viewAll() {
        productDetailListener.vmViewAll(skuId);
    }

    public MutableLiveData<ProductDetailResponse> getDetailResponseMutableLiveData() {
        return detailResponseMutableLiveData;
    }

    public MutableLiveData<RecommendedProductResponse> getRecommendedProductResponseMutableLiveData() {
        return recommendedProductResponseMutableLiveData;
    }

    public MutableLiveData<ProductVariantResponse> getProductVariantResponseMutableLiveData() {
        return productVariantResponseMutableLiveData;
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        onDataAvailable(skuId);
    }

    public void onDataAvailable(String skuId) {
        this.skuId = skuId;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            productDetailBinding.productView.setVisibility(View.GONE);
            productDetailListener.vmShowProgress();
            postRecommendationEngine();
            getDetail(skuId);
            getProductOfferBasedOnSKU(skuId);
        }
        productDetailBinding.strikePrice.setPaintFlags(productDetailBinding.strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    private void getProductSuggestionList(ProductDetailResponse productDetailResponse) {
        ConfigMap config = ConfigMap.getInstance();
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getTryAndBuyEnableStatus() &&
                productDetailResponse != null && productDetailResponse.getResult() != null && !TextUtils.isEmpty(productDetailResponse.getResult().getTryAndBuyTemplateId())) {
            String url = config.getProperty(ConfigConstant.RECOMMENDATION_ENGINE_URL) + String.format(config.getProperty(ConfigConstant.RECOMMENDED_PRODUCT_URL),
                    getUserId(), config.getProperty(ConfigConstant.RECOMMENDATION_ENGINE_TOKEN), productDetailResponse.getResult().getTryAndBuyTemplateId());
            AppServiceManager.getInstance().getCartSuggestedProductList(this, url);
        }
    }

    public String categoryName() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getSubCategoryName()) ? productListResult.getProduct().getSubCategoryName() : "";
    }

    public void getDetail(String sku) {
        AppServiceManager.getInstance().getProductDetailBasedOnSKU(this, sku);
    }

    private void getProductOfferBasedOnSKU(String sku) {
        AppServiceManager.getInstance().getProductOfferBasedOnSKU(this, sku);
    }

    private void getProductVariantBasedOnSKU(String groupId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            AppServiceManager.getInstance().getProductVariantBasedOnSKU(this, groupId);
        }
    }

    public void checkPinCode() {
        productDetailListener.vmCheckPinCode(true);
    }

    public void requestProduct() {
        */
/*Google Analytics Event*//*

        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_HIGH_ORDER_VALUE, GoogleAnalyticsHelper.EVENT_ACTION_HOV_EXTRA_DISCOUNT, GoogleAnalyticsHelper.EVENT_LABEL_PRODUCT_DETAILS_PAGE);
        productDetailListener.vmRequestProduct();
    }

    private String getComboEnableTitleText() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getProductDetailsFlags() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getProductDetailsFlags().getComboEnableTitle()) ? configurationResponse.getResult().getProductDetailsFlags().getComboEnableTitle() : context.getResources().getString(R.string.text_frequently_bought) : context.getResources().getString(R.string.text_frequently_bought);
    }

    public String getPackOfBuyTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getProductDetailsFlags() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getProductDetailsFlags().getPackOfBuyTitle()) ? configurationResponse.getResult().getProductDetailsFlags().getPackOfBuyTitle() : context.getResources().getString(R.string.text_product_combination) : context.getResources().getString(R.string.text_product_combination);
    }

    public String getSimilarEnableTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getProductDetailsFlags() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getProductDetailsFlags().getSimilarEnableTitle()) ? configurationResponse.getResult().getProductDetailsFlags().getSimilarEnableTitle() : context.getResources().getString(R.string.text_similar_product) : context.getResources().getString(R.string.text_similar_product);
    }

    public String offerText() {
        return context.getString(R.string.text_request_product_priced);
    }

    public String drugDetail() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getPackSizeTypeLabel()) ? productListResult.getProduct().getPackSizeTypeLabel() : "";
    }

    public String returnableMessage() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getReturnableMessage()) ? productListResult.getProduct().getReturnableMessage() : "";
    }

    public String manufacturerName() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getManufacturerName()) ? "By " + productListResult.getProduct().getManufacturerName() : "";
    }

    public String discount() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDiscount()) ? productListResult.getProduct().getDiscount() : "";
    }

    public String defaultDiscount() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDiscount()) ? context.getString(R.string.text_you_get) + " " + productListResult.getProduct().getDiscount() + " " + context.getString(R.string.text_on_this_product) : "";
    }

    public boolean defaultDiscountVisibility() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDiscount());
    }

    public boolean discountVisibility() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDiscount());
    }

    public boolean strikeMrpVisibility() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getStrikePrice()) && !productListResult.getProduct().getActualPrice().equals(productListResult.getProduct().getStrikePrice());
    }

    public String alternateDrugDetail() {
        return productListResult != null && productListResult.getPackSizeVariant() != null && !TextUtils.isEmpty(productListResult.getPackSizeVariant().get(0).getPackSizeTypeLabel()) ? productListResult.getPackSizeVariant().get(0).getPackSizeTypeLabel() : "";
    }

    public String alternateManufacturerName() {
        return productListResult != null && productListResult.getPackSizeVariant() != null && !TextUtils.isEmpty(productListResult.getPackSizeVariant().get(0).getManufacturerName()) ? "By " + productListResult.getPackSizeVariant().get(0).getManufacturerName() : "";
    }

    public String productName() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getName()) ? productListResult.getProduct().getName() : "";
    }

    public String getDrugType() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDrugType()) ? productListResult.getProduct().getDrugType() : "";
    }


    public String promoCode() {
        return promoCodeLists != null && promoCodeLists.size() > 0 && !TextUtils.isEmpty(promoCodeLists.get(0).getCouponCode()) ? promoCodeLists.get(0).getCouponCode() : "";
    }

    public String promoCodeDescription() {
        return promoCodeLists != null && promoCodeLists.size() > 0 && !TextUtils.isEmpty(promoCodeLists.get(0).getDescription()) ? promoCodeLists.get(0).getDescription() : "";
    }

    public String promoCodeDiscount() {
        return isDiscountAvailable() ? !TextUtils.isEmpty(promoCodeLists.get(0).getDiscount()) ? "You Save " + promoCodeLists.get(0).getDiscount() + "%" : "" : "";
    }

    public boolean isDiscountAvailable() {
        return promoCodeLists != null && promoCodeLists.size() > 0 && !TextUtils.isEmpty(promoCodeLists.get(0).getDiscount());
    }

    public boolean isPromoCodeAvailable() {
        return promoCodeLists != null && promoCodeLists.size() > 0;
    }

    public String relatedTitle() {
        return context.getString(isAlternateSaltsLabelEnable() ? R.string.text_alternate_salt : R.string.text_people_also_viewed);
    }

    private boolean isAlternateSaltsLabelEnable() {
        String drugType = productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDrugType()) ? productListResult.getProduct().getDrugType() : AppConstant.DRUG_TYPE_O;
        return AppConstant.DRUG_TYPE_P.equalsIgnoreCase(drugType) || ((AppConstant.DRUG_TYPE_NP.equalsIgnoreCase(drugType)) && !isPureOTCProduct());
    }

    private boolean isPureOTCProduct() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getRxRequired()) && (AppConstant.OTC_SCHEDULE_TYPE_F.equalsIgnoreCase(productListResult.getProduct().getSchedule()) ||
                AppConstant.OTC_SCHEDULE_TYPE_M.equalsIgnoreCase(productListResult.getProduct().getSchedule()) ||
                AppConstant.OTC_SCHEDULE_TYPE_O.equalsIgnoreCase(productListResult.getProduct().getSchedule()));
    }

    public String strikePrice() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getStrikePrice()) && !productListResult.getProduct().getActualPrice().equals(productListResult.getProduct().getStrikePrice()) ? "\u20B9 " + productListResult.getProduct().getStrikePrice() : "";
    }

    public String price() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getActualPrice()) ? "\u20B9 " + productListResult.getProduct().getActualPrice() : "";
    }

    public String alternateStrikePrice() {
        return productListResult != null && productListResult.getPackSizeVariant() != null && !TextUtils.isEmpty(productListResult.getPackSizeVariant().get(0).getStrikePrice()) && !productListResult.getPackSizeVariant().get(0).getActualPrice().equals(productListResult.getPackSizeVariant().get(0).getStrikePrice()) ? "\u20B9 " + productListResult.getPackSizeVariant().get(0).getStrikePrice() : "";
    }

    public boolean isMrpAlternateAvailable() {
        return !TextUtils.isEmpty(alternateStrikePrice());
    }

    public String alternatePrice() {
        return productListResult != null && productListResult.getPackSizeVariant() != null && !TextUtils.isEmpty(productListResult.getPackSizeVariant().get(0).getActualPrice()) ? "\u20B9 " + productListResult.getPackSizeVariant().get(0).getActualPrice() : "";
    }

    public String unitVariantTitle() {
        return productVariants != null ? getVariantTitle(VARIANT_UNIT) : "";
    }

    public String ageVariantTitle() {
        return productVariants != null ? getVariantTitle(VARIANT_AGE_GROUP) : "";
    }

    public String sizeVariantTitle() {
        return productVariants != null ? getVariantTitle(VARIANT_SIZE) : "";
    }

    public String flavourVariantTitle() {
        return productVariants != null ? getVariantTitle(VARIANT_FLAVOUR) : "";
    }

    public String colorVariantTitle() {
        return productVariants != null ? getVariantTitle(VARIANT_COLOR) : "";
    }

    private String getVariantTitle(String name) {
        for (VariantType variantType : productVariants.getTypes()) {
            if (variantType.getKey().equals(name))
                return variantType.getValue();
        }
        return null;
    }

    private void loadNumberPicker() {
        CustomNumberPicker defaultNumberPicker = new CustomNumberPicker(context);
        defaultNumberPicker.setListener(this);
        int maxQuantity = productListResult.getProduct().getQuantity() <= productListResult.getProduct().getMaximumSelectableQuantity() ? productListResult.getProduct().getQuantity() : productListResult.getProduct().getMaximumSelectableQuantity();
        defaultNumberPicker.setMaxValue(maxQuantity);
        productDetailBinding.qtyPicker.removeAllViews();
        productDetailBinding.qtyPicker.addView(defaultNumberPicker);
    }

    public boolean isColdStorage() {
        return productListResult != null && productListResult.getProduct() != null && productListResult.getProduct().isColdStorage();
    }

    public boolean isPrescriptionRequired() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getRxRequired()) && productListResult.getProduct().getRxRequired().equalsIgnoreCase("Y");
    }

    public boolean isPrescriptionImage() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getDrugType()) && (productListResult.getProduct().getDrugType().equalsIgnoreCase("P") || productListResult.getProduct().getDrugType().equalsIgnoreCase("NP"));
    }

    public boolean isAvailable() {
        return productListResult != null && productListResult.getProduct() != null && productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase("A");
    }

    public boolean rxVisibility() {
        return isPrescriptionRequired() && isAvailable();
    }

    public boolean otcVisibiliity() {
        return !isPrescriptionRequired() && isAvailable();
    }

    public String stockButtonText() {
        if (productListResult != null && productListResult.getProduct() != null && productListResult.getProduct().getAvailableStatusCode() != null) {
            if (productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase(AppConstant.OUT_OF_STOCK_STATUS)) {
                return context.getString(R.string.text_notify);
            } else if (productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase(AppConstant.NOT_AVAILABLE) || productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase(AppConstant.NOT_AVAILABLE_STATUS) || productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase(AppConstant.NOT_SALE)) {
                return context.getString(R.string.text_not_available);
            } else return "";
        } else
            return "";
    }

    public boolean isStatusButtonAvailable() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getAvailableStatusCode()) && !TextUtils.isEmpty(stockButtonText());
    }

    public boolean outOfStackAvailable() {
        return (productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getAvailableStatus())) && productListResult.getProduct().getAvailableStatus().equalsIgnoreCase(AppConstant.OUT_OF_STACK);
    }

    public void onClickNotify() {
        if (context != null && BasePreference.getInstance(context).isGuestCart())
            productDetailListener.navigateSigInActivity();
        else if (productListResult != null && productListResult.getProduct() != null && productListResult.getProduct().getAvailableStatusCode() != null && productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase(AppConstant.OUT_OF_STOCK_STATUS))
            productDetailListener.notifyFragmentDialogue();
    }

    public boolean showHighPriceRequest() {
        if (productListResult != null && productListResult.getProduct() != null && Double.valueOf(productListResult.getProduct().getActualPrice().replace(",", "")) > 5000)
            return true;
        else
            return productListResult != null && productListResult.getAlternateProduct() != null && Double.valueOf(productListResult.getAlternateProduct().get(0).getActualPrice().replace(",", "")) > 5000;
    }

    public boolean isAlternateAvailable() {
        return productListResult != null && productListResult.getPackSizeVariant() != null && productListResult.getPackSizeVariant().get(0).getAvailableStatusCode().equalsIgnoreCase("A");
    }

    public void onAlternateSelected() {
        onDataAvailable(productListResult.getPackSizeVariant().get(0).getSkuId());
    }

    @Override
    public void onHorizontalNumberPickerChanged(CustomNumberPicker customNumberPicker, int value) {
        pickerQuantity = value;
        quantity = value;
    }

    public void addCart() {
        quantity = pickerQuantity;
        setFromCombination(false);
        addItemsToCart();

        //Algolia conversion event
        String skuId = productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getSkuId()) ? productListResult.getProduct().getSkuId() : "";
        AlgoliaClickAndConversionHelper.getInstance().onAlgoliaConversionEvent(context, skuId, BasePreference.getInstance(context).getAlgoliaQueryId());
    }

    public void addItemsToCart() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            productDetailListener.vmShowProgress();
            BasePreference preference = BasePreference.getInstance(context);
            if (preference.isGuestCart()) {
                addItemToGuestCart(preference);
            } else {
                if (preference.isQuoteIdAvailable())
                    addToCart(preference.getCartId());
                else
                    APIServiceManager.getInstance().getCartId(this, BasePreference.getInstance(context).getAuthToken());
            }
        }
    }

    private void addItemToGuestCart(BasePreference preference) {
        if (!TextUtils.isEmpty(preference.getGuestCartId()))
            AppServiceManager.getInstance().addGuestCart(this, getCartRequest(preference.getGuestCartId()), BasePreference.getInstance(context).getAuthToken(), preference.getGuestCartId());
        else
            AppServiceManager.getInstance().getGuestCartId(this);
    }

    private void showNoNetworkView(boolean isConnected) {
        productDetailBinding.productView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        productDetailBinding.productDetailsNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        productDetailBinding.productView.setVisibility(!isWebserviceError ? View.VISIBLE : View.GONE);
        productDetailBinding.productDetailsApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.CREATE_CART:
                BasePreference.getInstance(context).setCartId(data);
                addToCart(data);
                break;
            case AppServiceManager.RE_ORDER_STOCK_INFO:
                reOrderStockInfo(data);
                break;
            case AppServiceManager.ADD_CART:
                quantity = 1;
                if (productListResult != null && productListResult.getProduct() != null) {
                    Product product = new Product();
                    ProductItem productItem = productListResult.getProduct();
                    product.setName(productItem.getName());
                    product.setCategoryName(productItem.getCategoryName());
                    product.setActualPrice(productItem.getActualPrice());
                    product.setPrice(productItem.getStrikePrice());
                    product.setDrugType(productItem.getDrugType());
                    product.setManufacturerName(productItem.getManufacturerName());
                    product.setSkuID(productItem.getSkuId());
                    product.setGenericName(productItem.getGenericName());
                    ArrayList<String> categoryIdList = new ArrayList<>();
                    categoryIdList.add(productItem.getCategoryId());
                    product.setCategoryIdList(categoryIdList);
                    product.setSubCategoryName(productItem.getSubCategoryName());
                    product.setParentCategoryId(productItem.getCategoryId());

                    */
/*WebEngage AddToCart Event*//*

                    WebEngageHelper.getInstance().addToCartEvent(product, quantity);
                    */
/*Facebook Pixel AddToCart Event*//*

                    FacebookPixelHelper.getInstance().logAddedToCartEvent(context, product, quantity);
                }
                productDetailListener.vmAddCartCallBack();
                break;
            case APIServiceManager.REFILL_CART:
                refillCartResponse(data);
                break;
            case AppServiceManager.GET_PRODUCT_DETAIL_BASED_ON_SKU:
                ProductDetailResponse productDetailResponse = new Gson().fromJson(data, ProductDetailResponse.class);
                if (!isAddToCartFromAlterNateSalt()) {
                    productDetailListener.vmDismissProgress();
                    if (productDetailResponse.getResult() != null) {
                        detailResponseMutableLiveData.setValue(productDetailResponse);
                        getProductSuggestionList(productDetailResponse);
                    } else {
                        productDetailBinding.productView.setVisibility(View.GONE);
                        productDetailBinding.emptyCartView.setVisibility(View.VISIBLE);
                        productDetailBinding.tvEmptyText.setText(productDetailResponse.getServiceStatus() != null && !TextUtils.isEmpty(productDetailResponse.getServiceStatus().getMessage()) ? productDetailResponse.getServiceStatus().getMessage() : "");
                    }
                } else
                    detailResponseMutableLiveData.setValue(productDetailResponse);
                break;
            case AppServiceManager.GET_PRODUCT_RECOMMENDATION_ENGINE:
                recommendedProductResponseMutableLiveData.setValue(new Gson().fromJson(data, RecommendedProductResponse.class));
                break;
            case AppServiceManager.RELATED_PRODUCTS_FROM_MAGENTO:
                onRelatedDataAvailable(new Gson().fromJson(data, RelatedProductResponse.class));
                break;
            case AppServiceManager.GET_PRODUCT_OFFER_BASED_ON_SKU:
                onOfferDataAvailable(new Gson().fromJson(data, PromoCodeResponse.class));
                break;
            case AppServiceManager.GET_PRODUCT_VARIANT_BASED_ON_SKU:
                productVariantResponseMutableLiveData.setValue(new Gson().fromJson(data, ProductVariantResponse.class));
                break;
            case AppServiceManager.REQUEST_POPUP:
                productDetailListener.vmDismissProgress();
                MstarRequestPopupResponse requestPopupResponse = new Gson().fromJson(!TextUtils.isEmpty(data) ? data : "", MstarRequestPopupResponse.class);
                if (requestPopupResponse != null && requestPopupResponse.getResult() != null)
                    SnackBarHelper.snackBarCallBack(productDetailBinding.viewContent, context, requestPopupResponse.getResult());
                break;
            case APIServiceManager.GET_MAGENTO_CART_ID:
                CartIdResponse cartResponse = new Gson().fromJson(data, CartIdResponse.class);
                if (cartResponse != null && cartResponse.getId() > 0) {
                    BasePreference.getInstance(context).setCartId(String.valueOf(cartResponse.getId()));
                    addToCart(String.valueOf(cartResponse.getId()));
                } else
                    APIServiceManager.getInstance().createCart(this, BasePreference.getInstance(context).getAuthToken());
                break;
            case AppServiceManager.BRAINSINS_SUGGESTED_PRODUCTS:
                RecommendedProductResponse suggestionResponse = new Gson().fromJson(data, RecommendedProductResponse.class);
                suggestedProductsListMutableData.setValue(suggestionResponse);
                break;
            case AppServiceManager.GUEST_CART_ID:
                BasePreference preference = BasePreference.getInstance(context);
                String token = data.replace("\"", "");
                preference.setGuestCartId(token);
                AppServiceManager.getInstance().addGuestCart(this, getCartRequest(preference.getGuestCartId()), BasePreference.getInstance(context).getAuthToken(), preference.getGuestCartId());
                break;
            case AppServiceManager.ADD_GUEST_CART_ITEMS:
                productDetailListener.vmAddCartCallBack();
                break;
            case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                expiryDateResponse(data);
                break;
            case PaymentServiceManager.CART_ITEM:
                CartResponse response = new Gson().fromJson(data, CartResponse.class);
                cartItemMutableData.setValue(response);
                break;
        }
    }

    private void expiryDateResponse(String data) {
        productDetailListener.vmDismissProgress();
        DeliveryEstimateResponse response = new Gson().fromJson(data, DeliveryEstimateResponse.class);
        if (response != null && response.getStatus() && response.getResult() != null && response.getResult().getPinCodeResultList() != null && response.getResult().getPinCodeResultList().size() > 0
                && response.getResult().getPinCodeResultList().get(0) != null && response.getResult().getPinCodeResultList().get(0).getProductDetails() != null && response.getResult().getaStatus().equalsIgnoreCase(getApplication().getResources().getString(R.string.text_ok))) {

            productDetailBinding.expiryLayout.checkDates.setVisibility(View.GONE);
            productDetailBinding.expiryLayout.detailsLayout.setVisibility(View.VISIBLE);

            productDetailBinding.expiryDetailLayout.checkDates.setVisibility(View.GONE);
            productDetailBinding.expiryDetailLayout.detailsLayout.setVisibility(View.VISIBLE);


            //Get and set Expiry Date
            DeliveryEstimateProductDetail detail = response.getResult().getPinCodeResultList().get(0).getProductDetails().get(productListResult.getProduct().getSkuId()).get(0);
            productDetailBinding.expiryLayout.expiryDate.setText(!TextUtils.isEmpty(detail.getExpiryDate()) ? DateTimeUtils.getInstance().stringDate(detail.getExpiryDate(), DateTimeUtils.yyyyMMdd, DateTimeUtils.MMMyyyy) : "");
            productDetailBinding.expiryDetailLayout.expiryDate.setText(!TextUtils.isEmpty(detail.getExpiryDate()) ? DateTimeUtils.getInstance().stringDate(detail.getExpiryDate(), DateTimeUtils.yyyyMMdd, DateTimeUtils.MMMyyyy) : "");


            // Get and set Est delivery time
            PincodeDeliveryEstimate pincodeDeliveryEstimate = response.getResult().getPinCodeResultList().get(0).getDeliveryEstimate();
            productDetailBinding.expiryLayout.deliveryDate.setText(!TextUtils.isEmpty(pincodeDeliveryEstimate.getFormatted().getFormat2()) ? pincodeDeliveryEstimate.getFormatted().getFormat2() : "");
            productDetailBinding.expiryDetailLayout.deliveryDate.setText(!TextUtils.isEmpty(pincodeDeliveryEstimate.getFormatted().getFormat2()) ? pincodeDeliveryEstimate.getFormatted().getFormat2() : "");

            setPinCodeCheckCallBack(true, "");
            productCheckPincodeSuccessListener.onPincodeTextChange(true);

        } else {
            setPinCodeCheckCallBack(false, response.getResult().getaStatus());
            productCheckPincodeSuccessListener.onPincodeTextChange(false);
        }

        String productName = productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getName()) ? productListResult.getProduct().getName() : "";
        WebEngageHelper.getInstance().pinCodeEnteredEvent(pinCode, productName);
    }

    private void setPinCodeCheckCallBack(boolean isVerified, String errorMessage) {
        if (pincodeListener != null)
            pincodeListener.onPincodeError(isVerified, errorMessage);
    }

    private void onOfferDataAvailable(PromoCodeResponse promoCodeResponse) {
        promoCodeLists = promoCodeResponse.getResult();
        productDetailBinding.setProductViewModel(productDetailBinding.getProductViewModel());
    }

    public void onVariantDataAvailable(ProductVariantResponse productVariantResponse) {
        if (productVariantResponse != null && productVariantResponse.getResult() != null && productVariantResponse.getResult().getVariants() != null) {
            productVariants = productVariantResponse.getResult().getVariants();
            productDetailBinding.variantView.setVisibility(View.VISIBLE);
            productDetailBinding.unit.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.ageGroup.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.color.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.flavour.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.size.setLayoutManager(setLinearLayoutManager());
            ProductMapping productMappings = productVariants.getProductMapping() != null && productVariants.getProductMapping().size() > 0 ? getProductMappingBasedOnSKU(productVariants.getProductMapping()) : null;
            if (productMappings != null) {
                if (isUnitVariantAvailable()) {
                    ProductUnitVariantAdapter variantAdapter = new ProductUnitVariantAdapter(context, productVariants.getUnits(), productMappings, VARIANT_UNIT, this);
                    if (productMappings.getUnits() != null)
                        productDetailBinding.unit.smoothScrollToPosition(getScrollPosition(productVariants.getUnits(), productMappings.getUnits()));
                    productDetailBinding.unit.setAdapter(variantAdapter);
                }
                if (isAgeGroupVariantAvailable()) {
                    ProductAgeVariantAdapter variantAdapter = new ProductAgeVariantAdapter(context, productVariants.getAgeGroup(), productMappings, VARIANT_AGE_GROUP, this);
                    if (productMappings.getAgeGroup() != null)
                        productDetailBinding.ageGroup.smoothScrollToPosition(getScrollPosition(productVariants.getAgeGroup(), productMappings.getAgeGroup()));
                    productDetailBinding.ageGroup.setAdapter(variantAdapter);
                }
                if (isColorVariantAvailable()) {
                    ProductColorVariantAdapter variantAdapter = new ProductColorVariantAdapter(context, productVariants.getColor(), productMappings, VARIANT_COLOR, this);
                    if (productMappings.getColor() != null)
                        productDetailBinding.color.smoothScrollToPosition(getScrollPosition(productVariants.getColor(), productMappings.getColor()));
                    productDetailBinding.color.setAdapter(variantAdapter);
                }
                if (isFlavourVariantAvailable()) {
                    ProductFlavourVariantAdapter variantAdapter = new ProductFlavourVariantAdapter(context, productVariants.getFlavour(), productMappings, VARIANT_FLAVOUR, this);
                    if (productMappings.getFlavour() != null)
                        productDetailBinding.flavour.smoothScrollToPosition(getScrollPosition(productVariants.getFlavour(), productMappings.getFlavour()));
                    productDetailBinding.flavour.setAdapter(variantAdapter);
                }
                if (isSizeVariantAvailable()) {
                    ProductSizeVariantAdapter variantAdapter = new ProductSizeVariantAdapter(context, productVariants.getSize(), productMappings, VARIANT_SIZE, this);
                    if (productMappings.getSize() != null)
                        productDetailBinding.size.smoothScrollToPosition(getScrollPosition(productVariants.getSize(), productMappings.getSize()));
                    productDetailBinding.size.setAdapter(variantAdapter);
                }
            } else productDetailBinding.variantView.setVisibility(View.GONE);
        } else productDetailBinding.variantView.setVisibility(View.GONE);
    }

    private int getScrollPosition(List<String> list, String value) {
        int position = list.indexOf(value);
        int secPosition = list.indexOf(value.toLowerCase());
        return position >= 0 ? position : secPosition >= 0 ? secPosition : 0;
    }

    private ProductMapping getProductMappingBasedOnSKU(List<ProductMapping> productMappingList) {
        for (ProductMapping productMapping : productMappingList) {
            if (productMapping.getSku().equals(skuId)) {
                return productMapping;
            }
        }
        return null;
    }

    private LinearLayoutManager setLinearLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    public boolean isUnitVariantAvailable() {
        return productVariants != null && productVariants.getUnits() != null && productVariants.getUnits().size() > 0;
    }

    public boolean isAgeGroupVariantAvailable() {
        return productVariants != null && productVariants.getAgeGroup() != null && productVariants.getAgeGroup().size() > 0;
    }

    public boolean isSizeVariantAvailable() {
        return productVariants != null && productVariants.getSize() != null && productVariants.getSize().size() > 0;
    }

    public boolean isColorVariantAvailable() {
        return productVariants != null && productVariants.getColor() != null && productVariants.getColor().size() > 0;
    }

    public boolean isFlavourVariantAvailable() {
        return productVariants != null && productVariants.getFlavour() != null && productVariants.getFlavour().size() > 0;
    }

    public void onDetailAvailable(ProductDetailResponse productDetailResponse) {
        if (productDetailResponse != null && productDetailResponse.getResult() != null) {
            productListResult = productDetailResponse.getResult();
            setProductBundle(productListResult);

            setBrainSinsFlag(productListResult.getBrainsinFlag());
            loadNumberPicker();

            */
/*Google Analytics Post Product Name*//*

            GoogleAnalyticsHelper.getInstance().postScreen(context, productName());
            if (productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getGroupId()))
                getProductVariantBasedOnSKU(productListResult.getProduct().getGroupId());
            if (isPrescriptionRequired())
                productDetailListener.vmPrescriptionRequired();
            loadImageInViewPager(productListResult);
            if (!isAvailable() && productListResult != null && productListResult.getAlternateProduct() != null)
                productDetailListener.vmAlternateRequired(productListResult.getAlternateProduct().get(0));
            ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
            if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getBrainsinStatusFlag() && !isPrescriptionRequired())
                showRelatedProducts();
            else loadRelatedProductsFromMagento();

            */
/*WebEngage Product Viewed Event*//*

            WebEngageHelper.getInstance().productViewedEvent(productListResult.getProduct());
        }
    }

    private void setProductBundle(ProductListResult productListResult) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getProductDetailsFlags() != null) {
            ProductDetailsFlag productDetailsFlag = configurationResponse.getResult().getProductDetailsFlags();
            productDetailBinding.packProducts.packProductLayout.setVisibility(productDetailsFlag.isPackOfBuy() && productListResult != null && productListResult.getPackProducts() != null && productListResult.getPackProducts().size() > 0 ? View.VISIBLE : View.GONE);
            productDetailBinding.productCombination.bundleProductLayout.setVisibility(productDetailsFlag.isComboEnable() ? View.VISIBLE : View.GONE);
            if (productListResult != null && productListResult.getSimilarProduct() != null && productListResult.getSimilarProduct().size() > 0) {
                List<BundleDrugDetailsList> drugDetailsList = new ArrayList<>();
                for (BundleDrugDetailsList similarProduct : productListResult.getSimilarProduct()) {
                    if (similarProduct.getAvailableStatusCode().equalsIgnoreCase("A")) {
                        drugDetailsList.add(similarProduct);
                    }
                }
                productDetailBinding.similarProduct.similarLayout.setVisibility(productDetailsFlag.isSimilarEnable() && drugDetailsList.size() > 0 ? View.VISIBLE : View.GONE);
                setSimilarProductView(drugDetailsList);
            }
            setPackProducts(productListResult, productDetailsFlag.isPackOfBuy());
            setBundle(productListResult.getBundleDrugDetails(), productDetailsFlag.isComboEnable());
            setSimilarProduct(productListResult.getSimilarProduct(), productDetailsFlag.isSimilarEnable());
        }
    }

    private void setSimilarProduct(List<BundleDrugDetailsList> similarProduct, boolean similarEnable) {
        if (similarEnable && similarProduct != null && similarProduct.size() > 0)
            checkSimilarProductAvailability(similarProduct);
    }

    private void checkSimilarProductAvailability(List<BundleDrugDetailsList> similarProduct) {
        List<BundleDrugDetailsList> productList = new ArrayList<>();
        for (BundleDrugDetailsList list : similarProduct) {
            if (!TextUtils.isEmpty(list.getAvailableStatusCode()) && list.getAvailableStatusCode().equalsIgnoreCase("A"))
                productList.add(list);
        }

        if (productListResult != null && productList.size() > 0) {
            productListResult.setSimilarProduct(productList);
            setSimilarProductView(productListResult.getSimilarProduct());
        }
    }

    private void setSimilarProductView(List<BundleDrugDetailsList> similarProduct) {
        if (similarProduct.size() > 0 && setSimilarProduct()) {
            productDetailBinding.similarProduct.similarProductTitle.setText(getSimilarEnableTitle());
            SimilarProductsAdapter adapter = new SimilarProductsAdapter(similarProduct, context, this, false);
            productDetailBinding.similarProduct.similarProduct.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.similarProduct.similarProduct.setAdapter(adapter);
            productDetailBinding.similarProduct.similarLayout.setVisibility(View.VISIBLE);
        } else {
            productDetailBinding.similarProduct.similarLayout.setVisibility(View.GONE);
        }
    }

    private void setBundle(BundleDrugDetails bundleDrugDetails, boolean comboEnable) {
        if (comboEnable && bundleDrugDetails != null && bundleDrugDetails.getList() != null && bundleDrugDetails.getList().size() > 0) {
            SimilarProductsAdapter adapter = new SimilarProductsAdapter(bundleDrugDetails.getList(), context, this, true);
            productDetailBinding.productCombination.productCombination.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.productCombination.productCombination.setAdapter(adapter);
            productDetailBinding.productCombination.combinationAmount.setText(!TextUtils.isEmpty(bundleDrugDetails.getTotalAmount()) ? CommonUtils.getPriceInFormat(CommonUtils.getIndianCurrencyFormat(bundleDrugDetails.getTotalAmount())) : "");
            productDetailBinding.productCombination.title.setText(getComboEnableTitleText());
            productDetailBinding.productCombination.bundleProductLayout.setVisibility(productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase("A") ? View.VISIBLE : View.GONE);
            bundleDetailsList = bundleDrugDetails.getList();
            List<Integer> list = new ArrayList<>();
            for (BundleDrugDetailsList bundleDrugDetailsList : bundleDrugDetails.getList()) {
                list.add(bundleDrugDetailsList.getMaximumSelectableQuantity());
            }
            bundleLeastCount = findMin(list);
            for (BundleDrugDetailsList bundleDrugDetailsList : bundleDrugDetails.getList()) {
                if (bundleDrugDetailsList.getMaximumSelectableQuantity().equals(bundleLeastCount)) {
                    PaymentHelper.setBundleSkuId(bundleDrugDetailsList.getSkuId());
                }
            }

        } else {
            productDetailBinding.productCombination.bundleProductLayout.setVisibility(View.GONE);
            setPackView();
        }

    }

    private Integer findMin(List<Integer> list) {
        if (list == null || list.size() == 0) {
            return Integer.MAX_VALUE;
        }
        List<Integer> sortedlist = new ArrayList<>(list);
        Collections.sort(sortedlist);
        return sortedlist.get(0);
    }

    private void setPackProducts(ProductListResult productListResult, boolean packOfBuy) {
        if (packOfBuy && productListResult != null && productListResult.getPackProducts() != null)
            checkPackAvailability(productListResult.getPackProducts());
    }

    private void checkPackAvailability(List<PackProducts> packProducts) {
        List<PackProducts> packProductsList = new ArrayList<>();
        for (PackProducts product : packProducts) {
            if (productListResult != null && productListResult.getProduct() != null && product.getPackSize() <= productListResult.getProduct().getQuantity())
                packProductsList.add(product);
        }

        if (productListResult != null && packProductsList.size() > 0) {
            productListResult.setPackProducts(packProductsList);
            setPackView();
        }
    }

    private void setPackView() {
        if (productListResult != null && productListResult.getPackProducts() != null && productListResult.getPackProducts().size() > 0 && setBackConfigFlag()) {
            ProductBundleAdapter adapter = new ProductBundleAdapter(productListResult.getPackProducts(), getProductImageUrl(productListResult), context, this);
            productDetailBinding.packProducts.productCombination.setLayoutManager(setLinearLayoutManager());
            productDetailBinding.packProducts.productCombination.setAdapter(adapter);
            productDetailBinding.packProducts.packProductLayout.setVisibility(productListResult.getProduct().getAvailableStatusCode().equalsIgnoreCase("A") ? View.VISIBLE : View.GONE);
        } else productDetailBinding.packProducts.packProductLayout.setVisibility(View.GONE);
    }

    private String getProductImageUrl(ProductListResult productListResult) {
        return productListResult != null && productListResult.getProduct() != null && productListResult.getProduct().getImageURL() != null && productListResult.getProduct().getImageURL().size() > 0
                ? productListResult.getProduct().getImageURL().get(0) : "";
    }

    private void loadImageInViewPager(ProductListResult productListResult) {
        productDetailListener.loadProductImageViewPager(productListResult);
    }

    private void postRecommendationEngine() {
        String engineBaseUrl = ConfigMap.getInstance().getProperty(ConfigConstant.RECOMMENDATION_ENGINE_URL);
        AppServiceManager.getInstance().sendProductToRecommendationEngine(this, engineBaseUrl, skuId, getUserId());
    }

    private void showRelatedProducts() {
        ConfigMap configMap = ConfigMap.getInstance();
        String url = configMap.getProperty(ConfigConstant.RECOMMENDATION_ENGINE_URL);
        String sku = getSkuID();
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && !TextUtils.isEmpty(configurationResponse.getResult().getPeopleViewTemplateId()))
            AppServiceManager.getInstance().getBrainSinsRelatedProduct(this, url, sku, getUserId(), AppConstant.BRAIN_SINS_PRODUCT_MAX_COUNT, configurationResponse.getResult().getPeopleViewTemplateId());
    }

    private boolean setBackConfigFlag() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getProductDetailsFlags() != null && configurationResponse.getResult().getProductDetailsFlags().isPackOfBuy();
    }

    private boolean setSimilarProduct() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getProductDetailsFlags() != null && configurationResponse.getResult().getProductDetailsFlags().isSimilarEnable();
    }

    public MutableLiveData<RecommendedProductResponse> getSuggestedProductsListMutableData() {
        return suggestedProductsListMutableData;
    }

    private String getSkuID() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getSkuId()) ? productListResult.getProduct().getSkuId() : "";
    }

    public String genericName() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getGenericName()) ? productListResult.getProduct().getGenericName() : "";
    }

    public String getCategoryID() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getCategoryId()) ? productListResult.getProduct().getCategoryId() : "";
    }

    private String getUserId() {
        UserDetails userDetails = new Gson().fromJson(BasePreference.getInstance(context).getUserDetails(), UserDetails.class);
        return BasePreference.getInstance(context).isGuestCart() ? BasePreference.getInstance(context).getGuestCartId() : userDetails != null && !TextUtils.isEmpty(userDetails.getCustomerId()) ? userDetails.getCustomerId() : "";
    }

    public String productDetailedContent() {
        return String.valueOf(productListResult != null && productListResult.getProductDetail() != null && !TextUtils.isEmpty(productListResult.getProductDetail().getShortDescription()) ? Html.fromHtml(productListResult.getProductDetail().getShortDescription()) : "");
    }

    public void loadDrugDetail() {
        productDetailListener.vmLoadDrugDetail(productListResult.getProductDetail(), productListResult.getProduct().getName());
    }

    public boolean showDetail() {
        return !TextUtils.isEmpty(productDetailedContent()) && !isPrescriptionRequired();
    }

    public boolean hasDetail() {
        return productListResult != null && productListResult.getProductDetail() != null && !TextUtils.isEmpty(productListResult.getProductDetail().getContent());
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        productDetailListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.CREATE_CART:
                productDetailListener.vmCartFailed(context.getResources().getString(R.string.text_unable_to_add_to_cart));
                break;
            case ADD_CART:
                if (!TextUtils.isEmpty(data)) {
                    CartResult cartResult = new Gson().fromJson(data, CartResult.class);
                    if (cartResult != null && cartResult.getServiceStatus() != null) {
                        ServiceStatus serviceStatus = cartResult.getServiceStatus();
                        if (serviceStatus != null && serviceStatus.getStatusCode() == 400 && !TextUtils.isEmpty(serviceStatus.getMessage())) {
                            productDetailListener.vmCartFailed(serviceStatus.getMessage());
                        }
                    }
                } else {
                    productDetailListener.vmShowProgress();
                    APIServiceManager.getInstance().getCartId(this, BasePreference.getInstance(context).getAuthToken());
                }
                break;
            case AppServiceManager.GET_PRODUCT_RECOMMENDATION_ENGINE:
                loadRelatedProductsFromMagento();
                break;
            case AppServiceManager.GET_PRODUCT_OFFER_BASED_ON_SKU:
                productDetailBinding.offersView.setVisibility(View.GONE);
                break;
            case AppServiceManager.GET_PRODUCT_DETAIL_BASED_ON_SKU:
                showWebserviceErrorView(true);
                break;
            case APIServiceManager.CHECK_COLD_STORAGE:
                productCheckPincodeSuccessListener.onPincodeTextChange(false);
                pincodeListener.onPincodeError(false, "");
                break;
            case AppServiceManager.REQUEST_POPUP:
                productDetailListener.vmDismissProgress();
                showWebserviceErrorView(true);
                break;
            case APIServiceManager.GET_MAGENTO_CART_ID:
                if (!TextUtils.isEmpty(BasePreference.getInstance(context).getAuthToken()))
                    APIServiceManager.getInstance().createCart(this, BasePreference.getInstance(context).getAuthToken());
                break;
            case AppServiceManager.BRAINSINS_SUGGESTED_PRODUCTS:
                enableDisableSuggestedProductList(View.GONE);
                break;
            case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                setPinCodeCheckCallBack(false, "");
                break;
            case PaymentServiceManager.CART_ITEM:
                showWebserviceErrorView(true);
                break;
            case AppServiceManager.ADD_GUEST_CART_ITEMS:
                SnackBarHelper.snackBarCallBack(productDetailBinding.viewContent, context, context.getString(R.string.text_exceed_quantity));
                break;
        }
    }

    private void enableDisableSuggestedProductList(int visibility) {
        productDetailBinding.productSuggestion.mainLayout.setVisibility(visibility);
    }

    */
/**
 * add to cart request payload
 *
 * @param quoteId String
 * @return addToCartRequest
 *//*

    private AddToCartRequest getCartRequest(String quoteId) {
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        CartItems cartItems = new CartItems();
        cartItems.setQuoteId(quoteId);
        cartItems.setSku(skuId);
        cartItems.setQty(quantity);
        addToCartRequest.setCartItem(cartItems);
        return addToCartRequest;
    }

    public void onRecommendedProductAvailable(RecommendedProductResponse detailResponse) {
        if (isRecommendationAvailable(detailResponse)) {
            alternateProductList = detailResponse.getRecommendedProductList();
            setAdapter();
        } else {
            loadRelatedProductsFromMagento();
        }
    }

    private boolean isRecommendationAvailable(RecommendedProductResponse detailResponse) {
        return detailResponse != null && detailResponse.getRecommendedProductList() != null && !detailResponse.getRecommendedProductList().isEmpty();
    }

    private void loadRelatedProductsFromMagento() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            AppServiceManager.getInstance().getRelatedProductBasedOnSKU(this, getCategoryID() != null && !TextUtils.isEmpty(getCategoryID()) ? Integer.parseInt(getCategoryID()) : 0, getSkuID() != null && !TextUtils.isEmpty(getSkuID()) ? Integer.parseInt(getSkuID()) : 0, 1);
        }
    }

    private void onRelatedDataAvailable(RelatedProductResponse relatedProductResponse) {
        if (relatedProductResponse != null && relatedProductResponse.getResult() != null && relatedProductResponse.getResult().getList() != null && relatedProductResponse.getResult().getList().size() > 0) {
            alternateProductList = relatedProductResponse.getResult().getList();
            setAdapter();
        } else {
            productDetailBinding.relatedProductView.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        if (alternateProductList != null && alternateProductList.size() > 3) {
            productDetailBinding.clearAll.setVisibility(View.VISIBLE);
        } else {
            productDetailBinding.clearAll.setVisibility(View.GONE);
        }
        if (alternateProductList != null && alternateProductList.isEmpty()) {
            productDetailBinding.relatedProductView.setVisibility(View.GONE);
        } else {
            productDetailBinding.relatedProductView.setVisibility(View.VISIBLE);
        }
        if (productListResult.getBrainsinFlag()) {
            AlternateBrainSinProductAdapter searchAdapter = new AlternateBrainSinProductAdapter(context, alternateProductList, alternateBrainSinProductAdapterListener);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            productDetailBinding.productList.setLayoutManager(linearLayoutManager);
            productDetailBinding.productList.setAdapter(searchAdapter);
        } else {
            AlternateProductAdapter searchAdapter = new AlternateProductAdapter(context, alternateProductList, alternateProductAdapterListener, false, null, null, null);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            productDetailBinding.productList.setLayoutManager(linearLayoutManager);
            productDetailBinding.productList.setAdapter(searchAdapter);
        }
    }

    @Override
    public void onUnitSelected(String unit, boolean onClicked) {
        selectedUnit = unit;
        if (onClicked) {
            String selectedSkuId = getSkuIdAfterSelection();
            if (!TextUtils.isEmpty(selectedSkuId) && skuId != selectedSkuId)
                onDataAvailable(selectedSkuId);
        }
    }

    @Override
    public void onAgeSelected(String age, boolean onClicked) {
        selectedAge = age;
        if (onClicked) {
            String selectedSkuId = getSkuIdAfterSelection();
            if (!TextUtils.isEmpty(selectedSkuId) && skuId != selectedSkuId)
                onDataAvailable(selectedSkuId);
        }
    }

    @Override
    public void onFlavourSelected(String flavour, boolean onClicked) {
        selectedFlavour = flavour;
        if (onClicked) {
            String selectedSkuId = getSkuIdAfterSelection();
            if (!TextUtils.isEmpty(selectedSkuId) && skuId != selectedSkuId)
                onDataAvailable(selectedSkuId);
        }
    }

    @Override
    public void onColorSelected(String color, boolean onClicked) {
        selectedColor = color;
        if (onClicked) {
            String selectedSkuId = getSkuIdAfterSelection();
            if (!TextUtils.isEmpty(selectedSkuId) && skuId != selectedSkuId)
                onDataAvailable(selectedSkuId);
        }
    }

    @Override
    public void onSizeSelected(String size, boolean onClicked) {
        selectedSize = size;
        if (onClicked) {
            String selectedSkuId = getSkuIdAfterSelection();
            if (!TextUtils.isEmpty(selectedSkuId) && skuId != selectedSkuId)
                onDataAvailable(selectedSkuId);
        }
    }

    private String getSkuIdAfterSelection() {
        Map<String, ProductMapping> mappingMap = new HashMap<>();
        for (ProductMapping productMapping : productVariants.getProductMapping()) {
            String key = getStringValue(productMapping.getAgeGroup()) + "" + getStringValue(productMapping.getUnits()) + "" + getStringValue(productMapping.getFlavour()) + "" + getStringValue(productMapping.getColor()) + "" + getStringValue(productMapping.getSize()).trim().toLowerCase();
            mappingMap.put(key, productMapping);
        }
        String selectedItemKey = getStringValue(selectedAge) + "" + getStringValue(selectedUnit) + "" + getStringValue(selectedFlavour) + "" + getStringValue(selectedColor) + "" + getStringValue(selectedSize).trim().toLowerCase();
        return mappingMap.containsKey(selectedItemKey) ? mappingMap.get(selectedItemKey).getSku() : "";
    }

    private String getStringValue(String string) {
        return TextUtils.isEmpty(string) ? "" : string;
    }

    public void onCheckDeliveryDate(String pinCode) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            productDetailListener.vmShowProgress();
            getExpiryDate(pinCode);
        }
    }

    public void onClickRequest(String patientName, String medicineName, String phoneNumber) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            productDetailListener.vmShowProgress();
            RequestPopup requestPopup = new RequestPopup();
            requestPopup.setPatientname(patientName);
            requestPopup.setProductname(medicineName);
            requestPopup.setCustomerphone(phoneNumber);
            AppServiceManager.getInstance().requestPopup(this, requestPopup);
        }
    }

    public String medicineName() {
        return productListResult != null && productListResult.getProduct() != null && !TextUtils.isEmpty(productListResult.getProduct().getName()) ? productListResult.getProduct().getName() : "";
    }

    public void navigateToHome() {
        if (context != null) {
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_navigation_activity), intent, context);
        }
    }

    public void onSuggestionDataAvailable(RecommendedProductResponse recommendedProductResponse) {
        if (recommendedProductResponse != null && !TextUtils.isEmpty(recommendedProductResponse.getStatus()) &&
                recommendedProductResponse.getStatus().equalsIgnoreCase("200") && recommendedProductResponse.getRecommendedProductList() != null && recommendedProductResponse.getRecommendedProductList().size() > 0) {
            setSuggestionAdapter(recommendedProductResponse.getRecommendedProductList());
            setSuggestionView();
        }
    }

    public void onCartAvailable(CartResponse cartResponse) {
        productDetailListener.vmDismissProgress();
        if (cartResponse != null && cartResponse.getCartResponseItem() != null && cartResponse.getCartResponseItem().getResult() != null && cartResponse.getCartResponseItem().getResult().size() > 0) {
            PaymentHelper.setCartLineItems(cartResponse.getCartResponseItem().getResult());
            compareBundleList(cartResponse.getCartResponseItem().getResult());
        } else {
            checkBundle();
        }
    }

    private void setSuggestionView() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null) {
            productDetailBinding.productSuggestion.title.setText(!TextUtils.isEmpty(configurationResponse.getResult().getTryAndBuyTitle()) ? configurationResponse.getResult().getTryAndBuyTitle() : "");
            productDetailBinding.productSuggestion.subTitle.setText(!TextUtils.isEmpty(configurationResponse.getResult().getTryAndBuySubTitle()) ? configurationResponse.getResult().getTryAndBuySubTitle() : "");
            productDetailBinding.productSuggestion.description.setText(!TextUtils.isEmpty(configurationResponse.getResult().getTryAndBuyDescription()) ? configurationResponse.getResult().getTryAndBuyDescription() : "");
        }
    }

    private void setSuggestionAdapter(ArrayList<HRVProduct> recommendedProductList) {
        CartSuggestionAdapter cartSuggestionAdapter = new CartSuggestionAdapter(recommendedProductList, getApplication(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        productDetailBinding.productSuggestion.suggestionList.setLayoutManager(linearLayoutManager);
        productDetailBinding.productSuggestion.suggestionList.setAdapter(cartSuggestionAdapter);
        enableDisableSuggestedProductList(View.VISIBLE);
    }

    public void addCombinationToCart() {
        getCartItem();
    }

    private void compareBundleList(List<CartItemResult> result) {
        if (bundleDetailsList != null && result != null) {
            for (CartItemResult cartItemResult : result) {
                for (BundleDrugDetailsList bundleDrugDetailsList : bundleDetailsList) {
                    if (cartItemResult.getSkuId().equalsIgnoreCase(bundleDrugDetailsList.getSkuId())) {
                        if (cartItemResult.getQuantity().equals(cartItemResult.getMaxQuantity())) {
                            bundleFlag = true;
                        } else {
                            if (PaymentHelper.getBundleSkuId() != null && PaymentHelper.getBundleSkuId().equals(cartItemResult.getSkuId()) && cartItemResult.getMaxQuantity().equals(bundleLeastCount)) {
                                bundleCount = cartItemResult.getQuantity();
                                PaymentHelper.setBundleCount(cartItemResult.getQuantity());
                            }
                        }

                    }
                }
            }
            checkBundle();
        }
    }

    private void checkBundle() {
        if (!bundleFlag && PaymentHelper.getBundleCount() < bundleLeastCount) {
            setFromCombination(true);
            addItemsToCart();
            PaymentHelper.setBundleCount(bundleCount++);
        } else {
            productDetailListener.exceededMaximumQuantitySnackBar();
        }
    }

    private void getCartItem() {
        productDetailListener.vmShowProgress();
        PaymentServiceManager.getInstance().getCart(this, cartItemsRequest());
    }

    private CartItemsRequest cartItemsRequest() {
        CartItemsRequest request = new CartItemsRequest();
        request.setToken(!TextUtils.isEmpty(BasePreference.getInstance(context).getAuthToken()) ? BasePreference.getInstance(context).getAuthToken().replace("bearer", "").trim() : "");
        request.setGuestToken(!TextUtils.isEmpty(BasePreference.getInstance(context).getGuestCartId()) ? BasePreference.getInstance(context).getGuestCartId() : "");
        request.setGuestFlag(BasePreference.getInstance(context).isGuestCart());
        return request;
    }

    public void addToCart(String cartId) {
        if (isFromCombination())
            APIServiceManager.getInstance().getReOrderStockInfo(this, BasePreference.getInstance(context).getAuthToken(), getReOrderStockInfoRequest());
        else if (context != null && !TextUtils.isEmpty(BasePreference.getInstance(context).getAuthToken()))
            AppServiceManager.getInstance().addCart(this, getCartRequest(cartId), BasePreference.getInstance(context).getAuthToken(), context);
    }

    private ReOrderStockInfoRequest getReOrderStockInfoRequest() {
        ReOrderStockInfoRequest request = new ReOrderStockInfoRequest();
        List<OrderSkuList> orderSkuLists = new ArrayList<>();
        double totalAmount = 0.0;
        if (productListResult.getBundleDrugDetails() != null && productListResult.getBundleDrugDetails().getList() != null && productListResult.getBundleDrugDetails().getList().size() > 0) {
            for (BundleDrugDetailsList orderDetail : productListResult.getBundleDrugDetails().getList()) {
                OrderSkuList skuList = new OrderSkuList();
                skuList.setDrugcode(Long.parseLong(orderDetail.getSkuId()));
                skuList.setProductName(orderDetail.getName());
                skuList.setPrice(Double.valueOf(String.valueOf(orderDetail.getActualPrice().replace(",", ""))));
                skuList.setQty(1);
                orderSkuLists.add(skuList);
                totalAmount = totalAmount + (1 * Double.valueOf(String.valueOf(orderDetail.getActualPrice().replace(",", ""))));

            }
        }
        request.setTotalamount(totalAmount);
        request.setSku(orderSkuLists);
        return request;
    }

    private void reOrderStockInfo(String data) {
        StockInformationResponse response = new Gson().fromJson(data, StockInformationResponse.class);
        if (response != null) {
            if (TextUtils.isEmpty(response.getResponseData()))
                refillBundle();
            else outOfStockAlert(response.getResponseData(), response.getTotalprice());
        }
    }

    private void outOfStockAlert(String responseData, double totalPrice) {
        productDetailListener.vmDismissProgress();
        productDetailListener.outOfStockView(responseData, totalPrice);
    }

    public void refillBundle() {
        APIServiceManager.getInstance().refillCart(this, refillOrderRequest());
    }

    private RefillCartRequest refillOrderRequest() {
        Map<String, DrugDetail> orderItemModelMap = new HashMap<>();
        RefillCartRequest refillOrderRequest = new RefillCartRequest();
        String[] token = BasePreference.getInstance(context).getAuthToken().split("\\s+");
        refillOrderRequest.setToken(token[1]);
        refillOrderRequest.setQuoteId(BasePreference.getInstance(context).getCartId());
        ArrayList<DrugCode> itemList = new ArrayList<>();
        if (productListResult.getBundleDrugDetails() != null && productListResult.getBundleDrugDetails().getList() != null && productListResult.getBundleDrugDetails().getList().size() > 0)
            for (BundleDrugDetailsList drugDetailsList : productListResult.getBundleDrugDetails().getList()) {
                DrugCode drugCode = new DrugCode();
                drugCode.setDrugCode(String.valueOf(drugDetailsList.getSkuId()));
                drugCode.setQty("1");
                itemList.add(drugCode);
            }
        PaymentHelper.setItemInsideOrderMap(orderItemModelMap);
        refillOrderRequest.setSkuList(itemList);
        return refillOrderRequest;
    }


    private void refillCartResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            RefillCartResponse response = new Gson().fromJson(data, RefillCartResponse.class);
            if (response != null && (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()))) {
                if (productListResult != null && productListResult.getBundleDrugDetails() != null && productListResult.getBundleDrugDetails().getList() != null &&
                        productListResult.getBundleDrugDetails().getList().size() > 0) {
                    for (BundleDrugDetailsList productList : productListResult.getBundleDrugDetails().getList()) {
                        Product product = new Product();
                        product.setName(productList.getName());
                        product.setCategoryName(productList.getCategoryName());
                        product.setActualPrice(productList.getActualPrice());
                        product.setPrice(productList.getStrikePrice());
                        product.setDrugType(productList.getDrugType());
                        product.setManufacturerName(productList.getManufacturerName());
                        product.setSkuID(productList.getSkuId());
                        product.setGenericName(productList.getGenericName());
                        ArrayList<String> categoryIdList = new ArrayList<>();
                        categoryIdList.add(productList.getCategoryId());
                        product.setCategoryIdList(categoryIdList);
                        product.setSubCategoryName(productList.getSubCategoryName());
                        product.setParentCategoryId(productList.getCategoryId());
                        */
/*WebEngage AddToCart Event*//*

                        WebEngageHelper.getInstance().addToCartEvent(product, quantity);
                        */
/*Facebook Pixel AddToCart Event*//*

                        FacebookPixelHelper.getInstance().logAddedToCartEvent(context, product, quantity);
                    }
                }
                productDetailListener.vmAddCartCallBack();
            } else {
                productDetailListener.vmCartFailed(context.getResources().getString(R.string.text_unable_to_add_to_cart));
            }
        }
    }

    @Override
    public void addProductToCart(String sku) {
        setFromCombination(false);
        skuId = sku;
        quantity = 1;
        addItemsToCart();
    }

    @Override
    public void redirectToProductPage(String sku) {
        productDetailListener.toProductPage(sku);
    }

    @Override
    public void addPack(int packSize) {
        setFromCombination(false);
        quantity = packSize;
        addItemsToCart();
    }

    private boolean isFromCombination() {
        return fromCombination;
    }

    private void setFromCombination(boolean fromCombination) {
        this.fromCombination = fromCombination;
    }

    @Override
    public void navigateToProduct(String sku) {
        productDetailListener.toProductPage(sku);
    }

    @Override
    public void launchAddToCartBottomSheet(BundleDrugDetailsList drugDetail) {
        setFromCombination(false);
        redirectToCartView(drugDetail);
    }

    private void redirectToCartView(BundleDrugDetailsList list) {
        Product product = new Product();
        product.setName(list.getName());
        product.setCategoryName(list.getSubCategoryName());
        product.setActualPrice(list.getActualPrice());
        product.setPrice(list.getStrikePrice());
        product.setDrugType(list.getDrugType());
        List<String> imageDetails = new ArrayList<>();
        imageDetails.add(list.getImageURL());
        product.setImageUrl(imageDetails);
        product.setManufacturerName(list.getManufacturerName());
        product.setProductDetail(list.getPackSizeTypeLabel());
        product.setSkuID(list.getSkuId());
        product.setAvailability(list.getAvailableStatus());
        product.setDrugSchedule(list.getSchedule());
        product.setGenericName(list.getGenericName());
        product.setFormulationType("");
        product.setQuantity(list.getQuantity());
        product.setMaxQuantity(list.getMaximumSelectableQuantity());
        product.setParentCategoryId("");
        product.setSubCategoryId(new ArrayList<String>());
        product.setSubCategoryName(list.getSubCategoryName());
        productDetailListener.addToCart(product);
    }


    public interface ProductDetailListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmAddCartCallBack();

        void vmPrescriptionRequired();

        void vmAlternateRequired(ProductItem productItem);

        void vmViewAll(String skuID);

        void vmCheckPinCode(boolean isFromColdStorageCheck);

        void vmRequestProduct();

        void vmCartFailed(String message);

        void previewFullImage(String imageUrl);

        void vmLoadDrugDetail(ProductDetailResult productDetail, String title);

        void loadProductImageViewPager(ProductListResult productListResult);

        void toProductPage(String sku);

        void exceededMaximumQuantitySnackBar();

        void outOfStockView(String message, double totalPrice);

        void addToCart(Product product);

        void notifyFragmentDialogue();

        void navigateSigInActivity();
    }

    public interface PincodeListener {
        void onPincodeError(boolean error, String errorMessage);

    }
}
*/
