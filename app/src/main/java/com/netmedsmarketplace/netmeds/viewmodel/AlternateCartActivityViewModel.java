package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.AlternateCartHeaderAdapter;
import com.netmedsmarketplace.netmeds.adpater.AlternateCartProductListAdapter;
import com.netmedsmarketplace.netmeds.model.AlternateCartHeaderModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.Alternative;
import com.nms.netmeds.base.model.CartSubstitutionResponseModel;
import com.nms.netmeds.base.model.CartSwitchResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarSwitchAlternateProductRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

public class AlternateCartActivityViewModel extends AppViewModel implements AlternateCartHeaderAdapter.AlternateCartHeaderAdapterCallback {

    private BasePreference basePreference;
    private AlternateCartActivityViewModelListener callback;
    private CartSubstitutionResponseModel cartSubstitutionResponseModel;

    private int failedTransactionId;
    private String imageUrl = "";

    public AlternateCartActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public void initiateAlternateCart(AlternateCartActivityViewModelListener listener, BasePreference basePreference) {
        this.callback = listener;
        this.basePreference = basePreference;
        initializeDataUsingIndent();
    }

    private void initiateAPICall(int transactionID) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.showNoNetworkView(isConnected);
        failedTransactionId = transactionID;
        if (isConnected) {
            switch (transactionID) {
                case APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT:
                    callback.showLoader();
                    APIServiceManager.getInstance().mStarSwitchAlternateProduct(this, basePreference.getMstarBasicHeaderMap(), getSwitchAlternateProductRequest());
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT:
                updateCartSubstitutionResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        failedTransactionId = transactionId;
        switch (transactionId) {
            case APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT:
                callback.showWebserviceErrorView(true);
                updateCartSubstitutionResponse(null);
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        initiateAPICall(failedTransactionId);
    }

    private List<MstarSwitchAlternateProductRequest> getSwitchAlternateProductRequest() {
        List<MstarSwitchAlternateProductRequest> mstarSwitchAlternateProductRequestList = new ArrayList<>();
        MstarSwitchAlternateProductRequest mstarSwitchAlternateProductRequest;
        for (Alternative alternative : cartSubstitutionResponseModel.getAlternatives()) {
            mstarSwitchAlternateProductRequest = new MstarSwitchAlternateProductRequest();
            mstarSwitchAlternateProductRequest.setCartProductCode(alternative.getCartProductCode());
            mstarSwitchAlternateProductRequestList.add(mstarSwitchAlternateProductRequest);
        }
        return mstarSwitchAlternateProductRequestList;
    }

    private void updateCartSubstitutionResponse(String data) {
        callback.dismissLoader();
        List<Alternative> outOfStockProductList = new ArrayList<>();
        List<Alternative> maxLimitReachedProductList = new ArrayList<>();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                for (CartSwitchResult cartSwitchResult : response.getResult().getCartSwitchResultsList()) {
                    if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(cartSwitchResult.getResultReasonCode())) {
                        outOfStockProductList.add(getNotAddedProduct(cartSwitchResult.getProductCode()));
                    } else if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(cartSwitchResult.getResultReasonCode())) {
                        maxLimitReachedProductList.add(getNotAddedProduct(cartSwitchResult.getProductCode()));
                    }
                }
                callback.launchAfterUpdateCartSuccessful(outOfStockProductList, maxLimitReachedProductList, response);
            } else {
                callback.showSnackBar("ERROR");
            }
        } else {
            callback.showSnackBar("ERROR");
        }
    }

    private Alternative getNotAddedProduct(int productCode) {
        for (Alternative alternative : cartSubstitutionResponseModel.getAlternatives()) {
            if (productCode == alternative.getCartProductCode()) {
                return alternative;
            }
        }
        return null;
    }

    private void initializeDataUsingIndent() {
        MStarBasicResponseTemplateModel model = new Gson().fromJson(basePreference != null && !TextUtils.isEmpty(basePreference.getConfigURLPath()) ? basePreference.getConfigURLPath() : "", MStarBasicResponseTemplateModel.class);
        if (model != null && model.getResult() != null && model.getResult().getProductImageUrlBasePath() != null) {
            imageUrl = model.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/";
        }
        cartSubstitutionResponseModel = callback.getIndentData();
        if (cartSubstitutionResponseModel != null) {
            if (getHeaderList() != null) {
                callback.setHeaderAdapter(new AlternateCartHeaderAdapter(getHeaderList(), this));
            }
            onClickOfSuggestedCart();
            updateOriginalAndUpdatedCartAmount();
            callback.dismissLoader();
        }
    }

    private List<AlternateCartHeaderModel> getHeaderList() {
        if (cartSubstitutionResponseModel != null && !cartSubstitutionResponseModel.getAlternatives().isEmpty()) {
            Alternative alternative = cartSubstitutionResponseModel.getAlternatives().get(0);
            List<AlternateCartHeaderModel> modelList = new ArrayList<>();
            AlternateCartHeaderModel alternateCartHeaderModel = new AlternateCartHeaderModel();
            alternateCartHeaderModel.setPositionOneVisible(true);
            alternateCartHeaderModel.setFirstValue(callback.getContext().getString(R.string.text_save).concat(" ").concat(CommonUtils.getPriceInFormat(cartSubstitutionResponseModel.getTotalSavings())));
            alternateCartHeaderModel.setThirdValue(getPercentage());
            modelList.add(alternateCartHeaderModel);

            AlternateCartHeaderModel alternateCartHeaderModel2 = new AlternateCartHeaderModel();
            alternateCartHeaderModel2.setPositionTwoVisible(true);
            String imageUrl = "";
            MStarBasicResponseTemplateModel model = new Gson().fromJson(basePreference != null && !TextUtils.isEmpty(basePreference.getConfigURLPath()) ? basePreference.getConfigURLPath() : "", MStarBasicResponseTemplateModel.class);
            if (model != null && model.getResult() != null && model.getResult().getProductImageUrlBasePath() != null) {
                imageUrl = model.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/";
            }
            alternateCartHeaderModel2.setMedicine1Image(imageUrl + alternative.getCartProductImagePath());
            alternateCartHeaderModel2.setMedicine2Image(imageUrl + alternative.getAlternateProductImagePath());

            alternateCartHeaderModel2.setSecondDescription(getMedicineManufactureName(alternative));

            alternateCartHeaderModel2.setThirdValue(alternative.getAlternateProductGeneric());
            alternateCartHeaderModel2.setThirdDescription(String.format(callback.getContext().getString(R.string.text_alternate_generic), alternative.getAlternateProductGeneric()));
            modelList.add(alternateCartHeaderModel2);

            AlternateCartHeaderModel alternateCartHeaderModel3 = new AlternateCartHeaderModel();
            alternateCartHeaderModel3.setPositionThreeVisible(true);
            modelList.add(alternateCartHeaderModel3);

            return modelList;
        }
        return null;
    }

    private SpannableString getMedicineManufactureName(Alternative firstAlternateProduct) {
        String medicine1 = getMedicineName(firstAlternateProduct.getCartProductName());
        String medicine2 = getMedicineName(firstAlternateProduct.getAlternateProductName());
        String medDesc1 = String.format(callback.getContext().getString(R.string.text_alternate_medicine_des), medicine1, firstAlternateProduct.getCartProductMfgName().replace(AppConstant.MFR, "")) + " and ";
        String medDesc2 = String.format(callback.getContext().getString(R.string.text_alternate_medicine_des), medicine2, firstAlternateProduct.getAlternateProductMfgrName().replace(AppConstant.MFR, "")) + ".";
        String finalString = medDesc1 + medDesc2;
        SpannableString spannableString = new SpannableString(finalString);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, medicine1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), medDesc1.length(), medDesc1.length() + medicine2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private String getMedicineName(String productName) {
        String[] stringarray = productName.split(" ");
        if (stringarray.length > 0) {
            return stringarray[0];
        }
        return productName;
    }

    private String getPercentage() {
        return cartSubstitutionResponseModel.getTotalSavingsPercentage();
    }

    private void updateOriginalAndUpdatedCartAmount() {
        callback.updateBottomSaveAmountTextAndUpdateCartButton(cartSubstitutionResponseModel);
    }

    public String getPrice(boolean isSuggestedCartHeadingSelected, Alternative model) {
        return CommonUtils.getPriceInFormat(isSuggestedCartHeadingSelected ? model.getAlternateProductLineValue() : model.getCartProductLineValue());
    }

    public String getSavedPrice(boolean isSuggestedCartHeadingSelected, Alternative model) {
        return callback.getContext().getString(R.string.text_you_save).concat(" ").concat(CommonUtils.getPriceInFormat(isSuggestedCartHeadingSelected ? model.getSavings() : model.getSavings()));
    }

    public String getOriginalProductPrice(Alternative model) {
        return CommonUtils.getPriceInFormat(model.getCartProductLineValue());
    }

    public void onClickOfSuggestedCart() {
        callback.changeHeadingProperties(true);
        callback.setAdapter(new AlternateCartProductListAdapter(this, imageUrl, cartSubstitutionResponseModel.getAlternatives(), true));
    }

    public void onClickOfOriginalCart() {
        callback.changeHeadingProperties(false);
        callback.setAdapter(new AlternateCartProductListAdapter(this,imageUrl, cartSubstitutionResponseModel.getAlternatives(), false));
    }

    public void updateCart() {
        /**
         * initially check alternate product stock status using reorder stock info and then update available alternate product to cart.
         */
        initiateAPICall(APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT);
    }

    @Override
    public Context getContext() {
        return callback.getContext();
    }

    public interface AlternateCartActivityViewModelListener {
        void showLoader();

        void dismissLoader();

        void launchAfterUpdateCartSuccessful(List<Alternative> outOfStockProductList, List<Alternative> maxLimitReachedProductList, MStarBasicResponseTemplateModel updateCartDetails);

        void setAdapter(AlternateCartProductListAdapter adapter);

        void changeHeadingProperties(boolean isNewSuggestionSelected);

        Context getContext();

        void updateBottomSaveAmountTextAndUpdateCartButton(CartSubstitutionResponseModel cartSubstitutionResponseModel);

        void showSnackBar(String errorMessage);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isAPIError);

        void setHeaderAdapter(AlternateCartHeaderAdapter adapter);

        AlternateCartHeaderAdapter getAdapter();

        CartSubstitutionResponseModel getIndentData();
    }
}
