package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.PromoCodeAdapter;
import com.netmedsmarketplace.netmeds.databinding.DialogPromoCodeBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.Interactor;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.PromoCodeList;
import com.nms.netmeds.payment.ui.PaymentServiceManager;
import com.nms.netmeds.payment.ui.vm.NmsApplyWalletResponse;

import java.util.List;

public class PromoCodeDialogViewModel extends AppViewModel implements Interactor<String>, PromoCodeAdapter.SelectedCoupon {


    @SuppressLint("StaticFieldLeak")
    private Activity context;
    private DialogPromoCodeBinding promoCodeBinding;
    private PromoCodeListener promoCodeListener;
    private String promoCode;
    private double nmsSuperCash = 0;
    private boolean applyCouponAfterSuperCashRemoval = false;
    private boolean applySuperCashAfterCouponRemoval = false;

    public PromoCodeDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void applyPromoCode() {
        if (!TextUtils.isEmpty(promoCode)) {
            promoCodeListener.unApplyMStarPromoCode(promoCode);
        } else {
            promoCodeListener.applyMStarPromoCode(promoCodeBinding.promoCode != null && promoCodeBinding.promoCode.getText() != null ? promoCodeBinding.promoCode.getText().toString() : "");
        }
    }

    public void onEditTextTextChanged(int count) {
        if (!TextUtils.isEmpty(promoCodeBinding.promoCode.getText().toString())) {
            promoCodeBinding.applyRemoveCouponBtn.setTextColor(ContextCompat.getColor(getApplication(), R.color.colorMediumPink));
            promoCodeBinding.applyRemoveCouponBtn.setEnabled(true);
        } else {
            promoCodeBinding.applyRemoveCouponBtn.setTextColor(ContextCompat.getColor(getApplication(), R.color.colorLightPink));
            promoCodeBinding.applyRemoveCouponBtn.setEnabled(false);
        }

    }

    public void init(Activity context, DialogPromoCodeBinding promoCodeBinding, PromoCodeListener promoCodeListener, String promoCode, List<PromoCodeList> promoCodeLists) {
        this.context = context;
        this.promoCodeBinding = promoCodeBinding;
        this.promoCodeListener = promoCodeListener;
        this.promoCode = promoCode;
        PaymentHelper.setPromoCodeDialoguePop(true);
        promoCodeBinding.applyRemoveCouponBtn.setText(TextUtils.isEmpty(promoCode)  ? context.getResources().getString(R.string.text_apply) : context.getResources().getString(R.string.text_remove));
        if (TextUtils.isEmpty(promoCode) || PaymentHelper.isNMSSuperCashApplied()) {
            promoCodeBinding.promoCode.setEnabled(true);
        } else {
            promoCodeBinding.promoCode.setEnabled(false);
        }
        setCouponAdapter(promoCodeLists);
        applyButtonClick();

        boolean isRemoveEnalbe = !TextUtils.isEmpty(promoCode);
        promoCodeBinding.applyRemoveCouponBtn.setEnabled(isRemoveEnalbe);
        promoCodeBinding.applyRemoveCouponBtn.setTextColor(ContextCompat.getColor(getApplication(), isRemoveEnalbe ? R.color.colorMediumPink : R.color.colorLightPink));

    }

    public void onBottomSheetDismiss() {
        PaymentHelper.setPromoCodeDialoguePop(false);
        promoCodeListener.vmPromoCodeDismiss();
    }

    public String promoCode() {
        return promoCode;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case PaymentServiceManager.DELETE_COUPON:
                if (applySuperCashAfterCouponRemoval && data.equalsIgnoreCase("true")) {
                    PaymentHelper.setCouponCode("");
                    applyNMSSuperCash(nmsSuperCash);
                } else {
                    promoCodeListener.vmDismissProgress();
                    onBottomSheetDismiss();
                    promoCodeListener.vmRemovePromoCode(context.getResources().getString(R.string.text_promo_code_successfully_removed));
                }
                break;
            case PaymentServiceManager.REMOVE_NMS_WALLET:
                removeNMSWalletResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        promoCodeListener.vmDismissProgress();
        onBottomSheetDismiss();
        switch (transactionId) {
            case PaymentServiceManager.REMOVE_NMS_WALLET:
                promoCodeListener.vmPromoCodeAppliedFailed(context.getResources().getString(R.string.text_remove_super_cash_failed));
                break;
        }
    }

    @Override
    public void pdAppliedCouponCode(PromoCodeList promoCodeList) {
        promoCodeListener.applyMStarPromoCode(promoCodeList.getCouponCode());
    }

    private void applyNMSSuperCash(double nmsSuperCash) {
        if (nmsSuperCash > 0) {
            //todo PaymentServiceManager.REDEEM_NMS_WALLET
            //applyRemoveNMSSuperCash(String.valueOf(nmsSuperCash), PaymentServiceManager.REDEEM_NMS_WALLET);
        } else {
            promoCodeListener.vmDismissProgress();
            promoCodeListener.setError(context.getString(R.string.text_super_cash_not_applicable));
        }
    }

    private void setCouponAdapter(List<PromoCodeList> promoCodeLists) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        promoCodeBinding.promoCodeList.setLayoutManager(linearLayoutManager);
        PromoCodeAdapter appliedCouponAdapter = new PromoCodeAdapter(context, promoCodeLists, promoCode, this);
        promoCodeBinding.promoCodeList.setAdapter(appliedCouponAdapter);
    }

    private void removeNMSWalletResponse(String data) {
        PaymentHelper.setReloadTotalInformation(true);
        if (!TextUtils.isEmpty(data)) {
            NmsApplyWalletResponse response = new Gson().fromJson(data, NmsApplyWalletResponse.class);
            if (response != null && !TextUtils.isEmpty(response.getStatus()) && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResponseData() != null) {
                PaymentHelper.setIsNMSSuperCashApplied(false);
                PaymentHelper.setAppliedNMSCash(Math.abs(response.getResponseData().getNmsCash()));
                PaymentHelper.setNmsWalletAmount(response.getResponseData().getNmsCash() + response.getResponseData().getNmsSuperCash());
                if (!applyCouponAfterSuperCashRemoval) {
                    promoCodeListener.vmDismissProgress();
                    promoCodeListener.vmPromoCodeDismiss();
                }
            } else {
                promoCodeListener.vmDismissProgress();
                promoCodeListener.vmPromoCodeDismiss();
                promoCodeListener.vmPromoCodeAppliedFailed(context.getResources().getString(R.string.text_remove_super_cash_failed));
            }
        }
    }

    private void applyButtonClick() {
        promoCodeBinding.promoCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    applyPromoCode();
                    handled = true;
                }
                return handled;
            }
        });
    }

    public interface PromoCodeListener {
        void vmPromoCodeDismiss();

        void vmRemovePromoCode(String string);

        void vmShowProgress();

        void vmDismissProgress();

        void vmPromoCodeApplied(String string);

        void vmPromoCodeAppliedFailed(String message);

        void setError(String string);

        void applyMStarPromoCode(String promoCode);

        void unApplyMStarPromoCode(String removingPromoCode);
    }

}
