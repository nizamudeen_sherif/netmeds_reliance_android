package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.CancelOrderAdapter;
import com.netmedsmarketplace.netmeds.databinding.DialogCancelOrderBinding;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.CancelOrderReason;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.List;

public class CancelOrderDialogviewModel extends BaseViewModel implements CancelOrderAdapter.CancelOrderAdapterListener {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private CancelOrderViewModelListener listener;
    private List<CancelOrderReason> cancelOrderReasonMessageList;
    private DialogCancelOrderBinding binding;
    private CancelOrderAdapter cancelOrderAdapter;
    private String selectedMessage = "";
    private int selectedMessageIndex;

    public CancelOrderDialogviewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DialogCancelOrderBinding binding, CancelOrderViewModelListener listener) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        initCancelOrderReasonMessageOrderList();
    }

    public String getSelectedMessage() {
        return selectedMessage;
    }

    private void initCancelOrderReasonMessageOrderList() {
        if (!TextUtils.isEmpty(BasePreference.getInstance(context).getConfiguration())) {
            ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
            if (configurationResponse != null) {
                if (configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) {
                    cancelOrderReasonMessageList = configurationResponse.getResult().getConfigDetails().getCancelOrderReason();
                    cancelOrderAdapter = new CancelOrderAdapter(cancelOrderReasonMessageList, this);
                    binding.rvCancelOrderMessageList.setLayoutManager(new LinearLayoutManager(context));
                    binding.rvCancelOrderMessageList.setNestedScrollingEnabled(false);
                    binding.rvCancelOrderMessageList.setAdapter(cancelOrderAdapter);
                }
            }
        }
    }

    public void closeView() {
        listener.dismissCancelDialog();
    }

    public void onCancelOrder() {
        if (!TextUtils.isEmpty(selectedMessage)) {
            if (selectedMessageIndex == cancelOrderReasonMessageList.size() - 1) {
                if (TextUtils.isEmpty(binding.edtOtherReason.getText().toString().trim())) {
                    binding.edtOtherReason.setError(context.getString(R.string.text_cancel_order_other_error_message));
                } else {
                    closeView();
                    listener.onClickCancelOrder(binding.edtOtherReason.getText().toString(), selectedMessageIndex);
                }
            } else {
                closeView();
                listener.onClickCancelOrder(selectedMessage, selectedMessageIndex);
            }
        }
    }

    public void onDontCancelOrder() {
        closeView();
    }

    public int getEditTextVisibility() {
        binding.edtOtherReason.setText("");
        return selectedMessageIndex == cancelOrderReasonMessageList.size() - 1 ? View.VISIBLE : View.GONE;
    }

    @Override
    public void onSelectItems(int position) {
        selectedMessageIndex = position;
        selectedMessage = cancelOrderReasonMessageList.get(position).getValue();
        for (int i = 0; i < cancelOrderReasonMessageList.size(); i++) {
            CancelOrderReason messageModel = cancelOrderReasonMessageList.get(i);
            if (messageModel.isSelected() && (i == position)) {
                messageModel.setSelected(false);
                selectedMessageIndex = -1;
                selectedMessage = "";
            } else {
                messageModel.setSelected(i == position);
                binding.edtOtherReason.setText(" ");
            }
            cancelOrderReasonMessageList.set(i, messageModel);
        }
        CommonUtils.hideKeyboard(context, binding.getRoot());
        cancelOrderAdapter.notifyDataSetChanged();
        binding.setViewModel(this);
    }

    public interface CancelOrderViewModelListener {

        void dismissCancelDialog();

        void onClickCancelOrder(String message, int messageIndex);

    }

}
