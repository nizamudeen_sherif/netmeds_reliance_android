package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityOrderConfirmationBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

public class M2OrderConfirmationViewModel extends AppViewModel {
    private Listener mListener;

    private ActivityOrderConfirmationBinding orderConfirmationBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private BasePreference basePreference;
    private int billingAddressID = 0;

    public M2OrderConfirmationViewModel(Application application) {
        super(application);
    }

    public void initViewModel(Context context, ActivityOrderConfirmationBinding orderConfirmationBinding, Listener listener, BasePreference basePreference) {
        this.context = context;
        this.orderConfirmationBinding = orderConfirmationBinding;
        this.mListener = listener;
        this.basePreference = basePreference;
        callFromNetmeds();
        initApiCall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    private void initApiCall(int transactionId) {
        mListener.vmShowProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                if (basePreference.getPreviousCartBillingAddressId() > 0 || billingAddressID > 0)
                    APIServiceManager.getInstance().mStarSingleAddressDetails(this, basePreference.getMstarBasicHeaderMap(), basePreference.getPreviousCartShippingAddressId() > 0 ? basePreference.getPreviousCartShippingAddressId() : billingAddressID);
                else
                    mListener.onClickContinueButton();
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                APIServiceManager.getInstance().mStarCustomerDetails(this, basePreference.getMstarBasicHeaderMap());
                break;

            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAddressID, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                break;
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAddressID, APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId,
                        M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() : 0);
                break;
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                singleAddressResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails(data);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, 0);
                break;
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        mListener.vmDismissProgress();
    }

    public void searchAddMedicine() {
        M2Helper.getInstance().setM2Order(true);
        PaymentHelper.setIsExternalDoctor(false);
        setCheckBoxSelection(orderConfirmationBinding.searchAddCheckBox, orderConfirmationBinding.searchAddImgBg, orderConfirmationBinding.searchAddImg, orderConfirmationBinding.searchAdd);
    }

    public void callFromNetmeds() {
        M2Helper.getInstance().setM2Order(true);
        PaymentHelper.setIsExternalDoctor(true);
        setCheckBoxSelection(orderConfirmationBinding.callCheckBox, orderConfirmationBinding.callFromNetmedsImgBg, orderConfirmationBinding.callFromNetmedsImg, orderConfirmationBinding.callFromNetmeds);
    }

    public void orderPerPrescription() {
        M2Helper.getInstance().setM2Order(true);
        PaymentHelper.setIsExternalDoctor(false);
        setCheckBoxSelection(orderConfirmationBinding.orderPerPrescriptionCheckBox, orderConfirmationBinding.orderPerPrescriptionImgBg, orderConfirmationBinding.orderPerPrescriptionImg, orderConfirmationBinding.orderPerPrescription);
    }

    public void onContinue() {
        initApiCall(APIServiceManager.MSTAR_GET_ADDRESS_BY_ID);
    }

    private void proceedOrderConfirmation() {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_ORDER_CONFIRMATION_CONTINUED, GoogleAnalyticsHelper.EVENT_LABEL_M2_ORDER_CONFIRMATION);
        if (orderConfirmationBinding.searchAddCheckBox.isChecked()) {
            mListener.initSearch(BasePreference.getInstance(context).getM2CartCount());
        } else if (orderConfirmationBinding.callCheckBox.isChecked()) {
            mListener.initDeliveryAddress();
        }
    }

    private void setCheckBoxSelection(CheckBox checkBox, LinearLayout imageBg, ImageView image, TextView textView) {
        if (checkBox.isChecked()) {
            checkBox.setChecked(false);
            enableContinue(false);
        } else {
            setConfirmation(checkBox);
            setImageSelection(imageBg, image, textView);
            enableContinue(true);
        }
    }

    private void setConfirmation(CheckBox checkBox) {
        orderConfirmationBinding.searchAddCheckBox.setChecked(orderConfirmationBinding.searchAddCheckBox.getId() == checkBox.getId());
        orderConfirmationBinding.callCheckBox.setChecked(orderConfirmationBinding.callCheckBox.getId() == checkBox.getId());
        orderConfirmationBinding.orderPerPrescriptionCheckBox.setChecked(orderConfirmationBinding.orderPerPrescriptionCheckBox.getId() == checkBox.getId());
    }

    private void enableContinue(boolean enable) {
        orderConfirmationBinding.continueButton.setEnabled(enable);
        orderConfirmationBinding.continueButton.setBackgroundDrawable(enable ? getApplication().getResources().getDrawable(R.drawable.accent_button) : getApplication().getResources().getDrawable(R.drawable.secondary_button));
        if (!enable) {
            disEnableView();
        }
    }

    private void setImageSelection(LinearLayout imageBg, ImageView image, TextView textView) {
        setBackGround(imageBg);
        setImage(image);
        setTextBackGround(textView);
    }

    private void setBackGround(LinearLayout imageBg) {
        orderConfirmationBinding.searchAddImgBg.setBackground(setBgDrawable(orderConfirmationBinding.searchAddImgBg.getId() == imageBg.getId()));
        orderConfirmationBinding.callFromNetmedsImgBg.setBackground(setBgDrawable(orderConfirmationBinding.callFromNetmedsImgBg.getId() == imageBg.getId()));
        orderConfirmationBinding.orderPerPrescriptionImgBg.setBackground(setBgDrawable(orderConfirmationBinding.orderPerPrescriptionImgBg.getId() == imageBg.getId()));
    }

    private Drawable setBgDrawable(boolean enable) {
        if (enable) {
            return getApplication().getResources().getDrawable(R.drawable.pale_blue_button_round_background);
        } else
            return getApplication().getResources().getDrawable(R.drawable.subscription_disabled_button_background);
    }

    private void setImage(ImageView image) {
        orderConfirmationBinding.searchAddImg.setImageDrawable(orderConfirmationBinding.searchAddImg.getId() == image.getId() ? getApplication().getResources().getDrawable(R.drawable.ic_doctor) : getApplication().getResources().getDrawable(R.drawable.ic_doctor_inactive));
        orderConfirmationBinding.callFromNetmedsImg.setImageDrawable(orderConfirmationBinding.callFromNetmedsImg.getId() == image.getId() ? getApplication().getResources().getDrawable(R.drawable.ic_call) : getApplication().getResources().getDrawable(R.drawable.ic_call_inactive));
        orderConfirmationBinding.orderPerPrescriptionImg.setImageDrawable(orderConfirmationBinding.orderPerPrescriptionImg.getId() == image.getId() ? getApplication().getResources().getDrawable(R.drawable.ic_prescription) : getApplication().getResources().getDrawable(R.drawable.ic_prescription_inactive));
    }

    private void disEnableView() {
        orderConfirmationBinding.searchAddImgBg.setBackground(getApplication().getResources().getDrawable(R.drawable.subscription_disabled_button_background));
        orderConfirmationBinding.callFromNetmedsImgBg.setBackground(getApplication().getResources().getDrawable(R.drawable.subscription_disabled_button_background));
        orderConfirmationBinding.orderPerPrescriptionImgBg.setBackground(getApplication().getResources().getDrawable(R.drawable.subscription_disabled_button_background));

        orderConfirmationBinding.searchAddImg.setImageDrawable(getApplication().getResources().getDrawable(R.drawable.ic_doctor_inactive));
        orderConfirmationBinding.callFromNetmedsImg.setImageDrawable(getApplication().getResources().getDrawable(R.drawable.ic_call_inactive));
        orderConfirmationBinding.orderPerPrescriptionImg.setImageDrawable(getApplication().getResources().getDrawable(R.drawable.ic_prescription_inactive));

        setTextTypeFace(orderConfirmationBinding.searchAdd, false);
        setTextTypeFace(orderConfirmationBinding.callFromNetmeds, false);
        setTextTypeFace(orderConfirmationBinding.orderPerPrescription, false);
    }

    private void setTextBackGround(TextView textView) {
        if (textView.getId() == orderConfirmationBinding.searchAdd.getId()) {
            setTextTypeFace(textView, true);
            setTextTypeFace(orderConfirmationBinding.callFromNetmeds, false);
            setTextTypeFace(orderConfirmationBinding.orderPerPrescription, false);
        } else if (textView.getId() == orderConfirmationBinding.callFromNetmeds.getId()) {
            setTextTypeFace(textView, true);
            setTextTypeFace(orderConfirmationBinding.searchAdd, false);
            setTextTypeFace(orderConfirmationBinding.orderPerPrescription, false);
        } else if (textView.getId() == orderConfirmationBinding.orderPerPrescription.getId()) {
            setTextTypeFace(textView, true);
            setTextTypeFace(orderConfirmationBinding.callFromNetmeds, false);
            setTextTypeFace(orderConfirmationBinding.searchAdd, false);
        }
    }

    private void singleAddressResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getAddressModel() != null) {
            PaymentHelper.setSingle_address(new Gson().toJson(response.getResult().getAddressModel()));
            PaymentHelper.setCustomerBillingAddress(response.getResult().getAddressModel());
            this.billingAddressID = response.getResult().getAddressModel().getId();
            setShippingAndBillingAddress();

        }
    }

    private void setShippingAndBillingAddress() {
        initApiCall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
        initApiCall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
        initApiCall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
        initApiCall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
    }

    private void getCustomerDetails(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getCustomerDetails() != null) {
            billingAddressID = response.getResult().getCustomerDetails().getPreferredBillingAddress();
        }
    }

    private void settingBillingAndShippingAddressResponse(String data, int mstarSetPreferredShippingAddress) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && mstarSetPreferredShippingAddress == 50013) {
            proceedOrderConfirmation();
        }
    }

    private void setTextTypeFace(TextView textView, boolean isBold) {
        Typeface typeface = Typeface.createFromAsset(getApplication().getAssets(), isBold ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf");
        textView.setTypeface(typeface);
    }

    public interface Listener {
        void initSearch(int m2CartCount);

        void initDeliveryAddress();

        void onClickContinueButton();

        void vmShowProgress();

        void vmDismissProgress();
    }

}
