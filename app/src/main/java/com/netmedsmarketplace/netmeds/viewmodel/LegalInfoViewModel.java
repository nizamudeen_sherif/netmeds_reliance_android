package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.LegalDetailsAndSupportAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityLegalInfoHelpBinding;
import com.netmedsmarketplace.netmeds.model.FAQCategoryModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;
import java.util.List;

public class LegalInfoViewModel extends BaseViewModel implements LegalDetailsAndSupportAdapter.LegalDetailsAndSupportAdapterListener {
    private ActivityLegalInfoHelpBinding binding;
    private LegalInfoListener listener;

    public LegalInfoViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivityLegalInfoHelpBinding binding, LegalInfoListener listener) {
        this.binding = binding;
        this.listener = listener;
        initializeAdapter();
    }

    private void initializeAdapter() {
        List<FAQCategoryModel> categoryModelList = getList();
        if (!categoryModelList.isEmpty()) {
            LegalDetailsAndSupportAdapter adapter = new LegalDetailsAndSupportAdapter(categoryModelList, this);
            binding.rvList.setLayoutManager(new LinearLayoutManager(listener.getContext()));
            binding.rvList.setNestedScrollingEnabled(false);
            binding.rvList.setAdapter(adapter);
        }
    }

    private List<FAQCategoryModel> getList() {
        List<FAQCategoryModel> list = new ArrayList<>();
        FAQCategoryModel model = new FAQCategoryModel();
        model.setName(listener.getContext().getString(R.string.text_privacy_policy));
        model.setCategoryPath(ConfigConstant.PRIVACY_POLICY);
        list.add(model);
        FAQCategoryModel model1 = new FAQCategoryModel();
        model1.setName(listener.getContext().getString(R.string.text_terms_condition));
        model1.setCategoryPath(ConfigConstant.TERMS_CONDITION);
        list.add(model1);
        FAQCategoryModel subscriptionModel = new FAQCategoryModel();
        subscriptionModel.setName(listener.getContext().getString(R.string.text_subscription_program));
        subscriptionModel.setCategoryPath(getSubscriptionTerms());
        list.add(subscriptionModel);
        return list;
    }

    @Override
    public void onItemClick(FAQCategoryModel model) {
        if (model.getName().equalsIgnoreCase(AppConstant.SUBSCRIPTION_PROGRAMME))
            listener.onClickInfo(model.getCategoryPath(), model.getName());
        else
            listener.onClickInfo(ConfigMap.getInstance().getProperty(model.getCategoryPath()), model.getName());
    }

    public interface LegalInfoListener {

        void onClickInfo(String url, String title);

        Context getContext();
    }

    private String getSubscriptionTerms() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(listener.getContext()).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getSubscriptionTerms()) ? configurationResponse.getResult().getConfigDetails().getSubscriptionTerms() : "";
    }
}
