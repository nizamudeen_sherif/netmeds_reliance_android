package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogHowToUseBinding;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;

public class HowToUseDialogViewModel extends BaseViewModel {

    private HowToUseDialogListener listener;
    @SuppressLint("StaticFieldLeak")
    private Context context;

    private String dialogFor;

    public HowToUseDialogViewModel(@NonNull Application application) {
        super(application);
    }

    private String getDialogFor() {
        return dialogFor;
    }

    public void setDialogFor(String dialogFor) {
        this.dialogFor = dialogFor;
    }

    public void init(Context context, HowToUseDialogListener listener, DialogHowToUseBinding binding) {
        this.context = context;
        this.listener = listener;
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) {

            if (!TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getNmscashTermsConditions())) {
                String currentString = configurationResponse.getResult().getConfigDetails().getNmscashTermsConditions();
                String[] separated = currentString.split("\\|");
                if (separated.length > 0) {
                    binding.tvNmscashTerms1.setText(separated[0]);
                    binding.tvNmscashTerms2.setText(separated[1]);
                    binding.tvNmscashTerms3.setText(separated[2]);
                }
            }
            if (!TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getNmssupercashTermsConditions())) {
                String nmsSuperCash = configurationResponse.getResult().getConfigDetails().getNmssupercashTermsConditions();
                String[] nmsSuperCashSepared = nmsSuperCash.split("\\|");
                if (nmsSuperCashSepared.length > 0) {
                    binding.tvNmsSuperCashTerms1.setText(nmsSuperCashSepared[0]);
                    binding.tvNmsSuperCashTerms2.setText(nmsSuperCashSepared[1]);
                    binding.tvNmsSuperCashTerms3.setText(nmsSuperCashSepared[2]);
                    binding.tvNmsSuperCashTerms4.setText(nmsSuperCashSepared[3]);
                    binding.tvNmsSuperCashTerms5.setText(nmsSuperCashSepared[4]);
                }
            }

        }
    }

    public String getDialogForTitle() {
        return String.format(context.getString(R.string.text_how_to_use_with_string), getDialogFor());
    }

    public boolean isNMSCash() {
        return context.getString(R.string.text_nms_cash).equals(getDialogFor());
    }

    public void closeView() {
        listener.dismissDialog();
    }

    public interface HowToUseDialogListener {
        void dismissDialog();
    }
}
