package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MStarCartAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.CartSubstitutionBasicResponseTemplateModel;
import com.nms.netmeds.base.model.CartSubstitutionRequest;
import com.nms.netmeds.base.model.CartSubstitutionResponseModel;
import com.nms.netmeds.base.model.CartSwitchResult;
import com.nms.netmeds.base.model.ConfigDetails;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateLineItem;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarBrainSinConfigDetails;
import com.nms.netmeds.base.model.MstarPrescriptionDetails;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.MstarSwitchAlternateProductRequest;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.model.PromoCodeList;
import com.nms.netmeds.base.model.RecommendedProductResponse;
import com.nms.netmeds.base.model.ShoppingCart;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MStarCartViewModel extends AppViewModel implements MStarCartAdapter.MStarCartAdapterCallBack, CommonUtils.BitmapImageConversionInterface {

    private MStarCartAdapter adapter;
    private MStarCartDetails cartDetails;
    private BasePreference basePreference;
    private MStarProductDetails removeProduct;
    private MStarCartViewModelCallback callback;
    private ConfigurationResponse configurationResponse;
    private DeliveryEstimateRequest deliveryEstimateRequest;
    private CartSubstitutionResponseModel cartSubstitutionResponseModel;

    public String orderId = "";
    private String applingPromoCode = "";
    private String removingPromoCoe = "";
    private String webEngageTrackEvent = "";
    public int billingAddressID = 0;

    private int updateingProductCode;
    private int transactionId = 0;
    private int updatedQuantity = 0;
    private int addingProductQuantity = 0;
    private int addingProductCode = 0;

    private boolean isRxProductAvailableInCart = false;
    private boolean isPrimeCustomer = false;
    public boolean isOutOfStock = false;
    private static boolean isOnFirstTime = false;
    public boolean isEdited = false;

    private boolean isFromEditOrder = false;
    private boolean isForAlternate = false;
    private boolean isEditable = false;
    private boolean isMultipleDelete = false;
    private boolean isPrimeDeletedFromCart = false;
    public boolean isOutOfStockProductInCart = false;
    private boolean isAlternateProductSwitchedAlteastOnce = false;
    private boolean isAlternateProductAvailableInCart = false;
    private boolean isCartEmpty = false;
    private boolean isWebEngageProductCall = false;

    private ArrayList<String> rxProductNameList = new ArrayList<>();
    public ArrayList<String> interCityProductList = new ArrayList<>();
    private ArrayList<String> rxProductIdList = new ArrayList<>();
    private List<PromoCodeList> promoCodeLists = new ArrayList<>();
    private List<MStarProductDetails> notAddedProducts = new ArrayList<>();
    private ArrayList<String> rxidListAfterEditOrderCancel = new ArrayList<>();
    private ArrayList<String> outOfStockProductNameList = new ArrayList<>();
    private ArrayList<String> productCodeListInCart = new ArrayList<>();


    private Map<String, String> unwantedPrescriptions = new HashMap<>();
    private Map<String, MStarAddedProductsResult> outOfStockProductsResultMap = new HashMap<>();

    private MutableLiveData<MStarCartDetails> cartDetailsForPrimeMutableLiveData = new MutableLiveData<>();
    public Map<Integer, MStarProductDetails> outOfStockProductCodeMap = new HashMap<>();
    public MStarProductDetails mStarProductDetails;
    private MStarCustomerDetails mStarCustomerDetails;

    //API CALLS
    private static final int GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION = 100001;
    private static final int GET_WALLET_BALLANCE_ONLY = 100002;
    private static final int GET_BALLANCE_AND_CHECK_ALTERNATE = 100003;
    private static final int GET_ALTERNATE_AND_GET_CART_DETAILS_ONLY = 100004;

    private List<MstarSwitchAlternateProductRequest> switchAlternateProductRequest;

    public MStarCartViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<MStarCartDetails> getCartDetailsForPrimeMutableLiveData() {
        return cartDetailsForPrimeMutableLiveData;
    }

    @Override
    public void onRetryClickListener() {
        callback.showWebserviceErrorView(false);
        callback.showLoader();
        initiateAPICalls(transactionId);
    }

    public void initiateCart(MStarCartViewModelCallback callback, BasePreference basePreference) {
        this.callback = callback;
        this.basePreference = basePreference;
        this.configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_VIEWED);
        outOfStockProductsResultMap = RefillHelper.getOutOfStockProductsMap();
        this.isFromEditOrder = PaymentHelper.isIsEditOrder();
        callback.showLoader();
        outOfStockProductCodeMap.clear();
    }

    public void checkM1OrM2ThenProceedFurther() {
        /**
         * If M2 Order comes we need to apply M2 Promo code by default.
         */
        showProgressBar();
        callback.enableNoRecordView(false);
        if ((M2Helper.getInstance().isPayNow() || M2Helper.getInstance().isRetry())) {
            if (!outOfStockProductsResultMap.isEmpty()) {
                if (!TextUtils.isEmpty(getProductList())) {
                    initiateAPICalls(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                } else {
                    outOfStockProductsResultMap.clear();
                }
            }
        }
        callback.parentViewVisible(true);
        if (!PaymentHelper.isInitiateCheckoutCalled() && isAlternateCartEnable()) {
            initiateAPICalls(APIServiceManager.MSTAR_INITIATE_CHECKOUT);
        } else {
            if (isAlternateCartEnable() && !PaymentHelper.isCartSwitched()) {
                initiateAPICalls(APIServiceManager.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART);
            } else {
                initiateAPICalls(GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION);
            }
        }
        if (isM2Order() && !basePreference.isGuestCart()) {
            ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
            applingPromoCode = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getM2Coupon().getCouponCode()) ? response.getResult().getConfigDetails().getM2Coupon().getCouponCode() : "";
        }
    }

    private boolean isAlternateCartEnable() {
        return configurationResponse != null && configurationResponse.getResult() != null &&
                configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isAltCartEnableFlag() &&
                !isM2Order() && !isFromEditOrder && !SubscriptionHelper.getInstance().isSubscriptionFlag();
    }

    public void initiateAPICalls(int transactionId) {
        boolean isConnected = callback.isNetworkConnected();
        callback.showNoNetworkView(isConnected);
        this.transactionId = transactionId;
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                case GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION:
                    APIServiceManager.getInstance().mStarGetCartDetails(this, basePreference.getMstarBasicHeaderMap(), getCartId(), transactionId);
                    break;
                case APIServiceManager.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART:
                case GET_ALTERNATE_AND_GET_CART_DETAILS_ONLY:
                    APIServiceManager.getInstance().mStarGetAlternateProductForCart(this, basePreference.getMstarBasicHeaderMap(), getCartId(), transactionId);
                    break;
                case APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT:
                    callback.showLoader();
                    APIServiceManager.getInstance().mStarSwitchAlternateProduct(this, basePreference.getMstarBasicHeaderMap(), getSwitchAlternateProductRequest());
                    break;
                case APIServiceManager.MSTAR_REVERT_ORIGINAL_CART:
                    callback.showLoader();
                    PaymentHelper.setIsInitiateCheckoutCalled(false);
                    APIServiceManager.getInstance().setMstarRevertOriginalCart(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basePreference.getMstarBasicHeaderMap(), addingProductCode, addingProductQuantity, getCartId());
                    break;
                case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                    APIServiceManager.getInstance().mStarRemoveProductFromCart(this, basePreference.getMstarBasicHeaderMap(), updateingProductCode,
                            isMultipleDelete || isForAlternate ? outOfStockProductCodeMap.containsKey(updateingProductCode) ? outOfStockProductCodeMap.get(updateingProductCode).getCartQuantity() : 0 : updatedQuantity, getCartId());
                    break;
                case APIServiceManager.MSTAR_INITIATE_CHECKOUT:
                    PaymentHelper.setIsCartSwitched(false);
                    APIServiceManager.getInstance().mStarInitiateCheckOut(this, basePreference.getMstarBasicHeaderMap(), getCartId(), transactionId);
                    break;
                case APIServiceManager.MSTAR_APPLY_PROMO_CODE:
                    APIServiceManager.getInstance().mStarApplyPromoCode(this, basePreference.getMstarBasicHeaderMap(), applingPromoCode, getCartId());
                    break;
                case APIServiceManager.MSTAR_UN_APPLY_PROMO_CODE:
                    APIServiceManager.getInstance().mStarUnApplyPromoCode(this, basePreference.getMstarBasicHeaderMap(), removingPromoCoe, getCartId());
                    break;
                case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                    APIServiceManager.getInstance().mStartDetachPrescription(this, basePreference.getMstarBasicHeaderMap(), isFromEditOrder ? getPrescriptionMapForEditOrder() : getPrescriptionMap(), getCartId());
                    break;
                case APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE:
                case GET_BALLANCE_AND_CHECK_ALTERNATE:
                case GET_WALLET_BALLANCE_ONLY:
                    APIServiceManager.getInstance().getMStarPaymentWalletBalance(this, basePreference.getMstarBasicHeaderMap(), getCartId(), transactionId);
                    break;
                case APIServiceManager.MSTAR_APPLY_WALLET:
                    APIServiceManager.getInstance().mStarApplyAndUnApplyWalletAndSuperCash(this, basePreference.getMstarBasicHeaderMap(), AppConstant.NO, AppConstant.YES, getCartId(), transactionId);
                    break;
                case APIServiceManager.C_MSTAR_COUPON_LIST:
                    APIServiceManager.getInstance().couponList(this);
                    break;
                case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                    APIServiceManager.getInstance().mStarUnapplyVoucher(this, basePreference.getMstarBasicHeaderMap(),
                            !TextUtils.isEmpty(cartDetails.getAppliedIhoVouchers()) ? cartDetails.getAppliedIhoVouchers() :
                                    !TextUtils.isEmpty(cartDetails.getAppliedGeneralVouchers()) ? cartDetails.getAppliedGeneralVouchers() : "", getCartId());
                    break;
                case APIServiceManager.CART_SUBSTITUTION:
                    APIServiceManager.getInstance().fetchCartSubstitutionDetails(this, getCartSubsRequest());
                    break;
                case APIServiceManager.C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS:
                    APIServiceManager.getInstance().mstarGetCartSuggestedProducts(this, getRecommendedProductUrl());
                    break;
                case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                    APIServiceManager.getInstance().mStarProductDetailsForIdList(this, isWebEngageProductCall ? getProductCodeList() : getProductList(), APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                    break;
                case APIServiceManager.MSTAR_GET_FILLED_M2_CART:
                    APIServiceManager.getInstance().mstarGetFilledM2Cart(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarCustomerId(), orderId);
                    break;
                case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                    APIServiceManager.getInstance().mStarSingleAddressDetails(this, basePreference.getMstarBasicHeaderMap(), basePreference.getPreviousCartShippingAddressId() > 0 ? basePreference.getPreviousCartShippingAddressId() : billingAddressID);
                    break;

                case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAddressID, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                    break;
                case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                    APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAddressID, APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId,
                            M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() : SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : 0);
                    break;
            }
        } else {
            callback.dismissLoader();
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
            case APIServiceManager.MSTAR_GET_FILLED_M2_CART:
            case GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION:
                cartDetailsResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART:
            case GET_ALTERNATE_AND_GET_CART_DETAILS_ONLY:
                cartSubstitutionResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT:
            case APIServiceManager.MSTAR_REVERT_ORIGINAL_CART:
                updateCartSubstitutionResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails(data);
                break;
            case APIServiceManager.C_MSTAR_COUPON_LIST:
                couponListResponse(data);
                break;
            case APIServiceManager.MSTAR_INITIATE_CHECKOUT:
                initiateCheckOutResponse(data);
                break;
            case APIServiceManager.MSTAR_APPLY_PROMO_CODE:
                onApplyPromoCodeResponce(data);
                break;
            case APIServiceManager.MSTAR_UN_APPLY_PROMO_CODE:
                onUnApplyPromoCodeResponce(data);
                break;
            case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                onDetachedResponse(data);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                onAddtoCartResponse(data);
                break;
            case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                onRemoveProductResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE:
            case GET_BALLANCE_AND_CHECK_ALTERNATE:
            case GET_WALLET_BALLANCE_ONLY:
                walletBalanceResponse(data, transactionId);
                break;
            case APIServiceManager.MSTAR_APPLY_WALLET:
                onApplyWalletResponse(data);
                break;
            case APIServiceManager.C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS:
                onSuggestedProductResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                onNotAddedProductDetailsAvailable(data);
                disableWebEngageProductCall();
                break;
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                singleAddressResponse(data);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                settingBillingAndShippingAddressResponse(data, 0);
                break;
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        this.transactionId = transactionId;
        switch (transactionId) {
            case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:

            case APIServiceManager.MSTAR_INITIATE_CHECKOUT:
                initiateAPICalls(GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION);
                break;
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                callback.showWebserviceErrorView(true);
                break;
            case APIServiceManager.MSTAR_APPLY_PROMO_CODE:
            case APIServiceManager.MSTAR_UN_APPLY_PROMO_CODE:
                initiateAPICalls(APIServiceManager.MSTAR_CONTINUE_SHOPPING);
                failedToApplyPromocode("");
                break;
            case APIServiceManager.MSTAR_CONTINUE_SHOPPING:
                initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
                break;
            case APIServiceManager.C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS:
                failedToShowSuggestedProducts();
                break;
            case APIServiceManager.MSTAR_GET_PAYMENT_WALLET_BALANCE:
            case GET_WALLET_BALLANCE_ONLY:
                walletBalanceResponse(null, transactionId);
                break;
            case APIServiceManager.MSTAR_APPLY_WALLET:
                initiateAPICalls(APIServiceManager.MSTAR_CONTINUE_SHOPPING);
                callback.showWebserviceErrorView(true);
                break;
            case APIServiceManager.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART:
            case GET_ALTERNATE_AND_GET_CART_DETAILS_ONLY:
                callback.updateCartSubstitutionView(false, false, null);
                initiateAPICalls(GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION);
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                disableWebEngageProductCall();
                break;
        }
        callback.dismissLoader();
    }

    private void disableWebEngageProductCall() {
        if (isWebEngageProductCall)
            isWebEngageProductCall = false;
    }


    private Integer getCartId() {
        return PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() :
                SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().getSubscriptionCartId() :
                        isM2Order() ? basePreference.getMstarMethod2CartId() :
                                null;
    }

    private String getProductList() {
        String productCodeListString = "";
        if (!outOfStockProductsResultMap.isEmpty()) {
            StringBuilder productCodeList = new StringBuilder();
            for (String productCode : outOfStockProductsResultMap.keySet()) {
                productCodeList.append(productCode).append(",");
            }
            productCodeListString = productCodeList.toString();
        }
        return productCodeListString.substring(0, productCodeListString.length() - 1);
    }

    private Map<String, String> getPrescriptionMap() {
        if (unwantedPrescriptions != null && cartDetails.getPrescriptions() != null && cartDetails.getPrescriptions().size() > 0) {
            for (String prescriptionId : cartDetails.getPrescriptions()) {
                unwantedPrescriptions.put(AppConstant.PID + getProperIndexValue(cartDetails.getPrescriptions().indexOf(prescriptionId)), prescriptionId);
            }
        }
        return unwantedPrescriptions;
    }

    private Map<String, String> getPrescriptionMapForEditOrder() {
        if (rxidListAfterEditOrderCancel != null && !rxidListAfterEditOrderCancel.isEmpty() && cartDetails.getPrescriptions() != null && cartDetails.getPrescriptions().size() > 0) {
            for (String prescriptionId : rxidListAfterEditOrderCancel) {
                unwantedPrescriptions.put(AppConstant.PID + getProperIndexValue(cartDetails.getPrescriptions().indexOf(prescriptionId)), prescriptionId);
            }
        }
        return unwantedPrescriptions;
    }

    private ArrayList<CartSubstitutionRequest> getCartSubsRequest() {
        CartSubstitutionRequest request = new CartSubstitutionRequest();
        request.setDrug_code("");
        request.setQty(1);
        ArrayList<CartSubstitutionRequest> requestsList = new ArrayList<>();
        requestsList.add(request);
        return requestsList;
    }

    public void cartDetailsResponse(String data, int transactionId) {
        MStarBasicResponseTemplateModel cartData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (cartData != null && cartData.getResult() != null && cartData.getResult().getCartDetails() != null) {
            isEditable = cartData.getResult().getCartDetails().isEdited();
        }
        if (cartData != null && cartData.getResult() != null && cartData.getResult().getCartDetails() != null &&
                cartData.getResult().getCartDetails().getLines() != null && !cartData.getResult().getCartDetails().getLines().isEmpty()) {
            this.cartDetails = cartData.getResult().getCartDetails();
            setDefaultProperties();
            setCartItemInModel(cartDetails.getLines());
            basePreference.setPreviousCartShippingAddressId(cartDetails.getShippingAddressId());
            basePreference.setPreviousCartBillingAddressId(cartDetails.getBillingAddressId());
            isOutOfStockProductInCart = false;
            loadPrescriptionIfPresentInCart();
            loadScreenWithCartDetails();
            loadBrainSinsRecommendedProducts();
            if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isCartAlternateEnableFlag())
                callback.disableProceedButton(isOutOfStockProductInCart);

            /*WebEngage Event*/
            if (webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_COUPON_CODE_APPLIED))
                onCouponAppliedEvent(WebEngageHelper.SUCCESS);

            if (!RefillHelper.getMaxLimitBreachedProductsMap().isEmpty()) {
                callback.showMaxLimitBreachedMessage();
                RefillHelper.getMaxLimitBreachedProductsMap().clear();
            }
            if (isM2Order() || isFromEditOrder) {
                callback.showAppliedPromocodeView(isM2Order() ? getM2Coupon() : cartDetails.getAppliedCoupons(), cartDetails);
            } else if (SubscriptionHelper.getInstance().isSubscriptionFlag()) {
                callback.showAppliedPromocodeView(getSubscriptionPromoCode(), cartDetails);
                callback.couponDescription(getSubscriptionDiscriptoin());
            }
            if ((M2Helper.getInstance().isRetry() || M2Helper.getInstance().isPayNow() || isFromEditOrder) && isOnFirstTime) {
                RefillHelper.setM2ShippingAddress(cartDetails.getShippingAddressId());
                PaymentHelper.setIsTempCartOpen(AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(cartDetails.getCartStatus()));
                PaymentHelper.setTempCartId(cartDetails.getId());
                PaymentHelper.setIsExternalDoctor(cartDetails.isDoctorConsultationNeeded());
            }
            callback.parentViewVisible(true);
            callback.productViewVisible(true);
            callback.couponViewVisible(isPrimeProductOnlyAvailableInCart() || basePreference.isGuestCart());
            isOnFirstTime = true;
            callback.updateCartPaymentDetails(cartDetails);
            initialAPIS(transactionId);
            checkCartSubstitutionDetails();
            if (GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION != transactionId) {
                callback.dismissLoader();
            }
            isCartEmpty = false;
        } else {
            emptyCartProperties();
            callback.enableNoRecordView(true);
            callback.dismissLoader();
            isCartEmpty = true;
        }

        /*Get product details and pass to the webEngage event
         * remove event, add event and also Cart Viewed event must track.*/
        webEngageProductDetailsApiCall();
    }

    private void webEngageProductDetailsApiCall() {
        if ((!isCartEmpty && (webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_CART_VIEWED) || webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_CART_UPDATED))) ||
                webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_REMOVED_FROM_CART)) {
            isWebEngageProductCall = true;
            callback.showLoader();
            initiateAPICalls(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
        }
    }

    private void checkCartSubstitutionDetails() {
        boolean isAlternateDataIsThere = getCartSubstitutionResponseModel() != null && getCartSubstitutionResponseModel().getAlternatives() != null && !getCartSubstitutionResponseModel().getAlternatives().isEmpty();
        /**
         * If alternate Product is switched then we need to show only revert to original cart view.
         */
        if (isAlternateProductSwitchedAlteastOnce) {
            callback.updateCartSubstitutionView(true, false, isAlternateDataIsThere ? getCartSubstitutionResponseModel().getOriginalCartTotalValue() : PaymentHelper.getOriginalCartAmount());
        } else {
            /**
             * in cart details itself we come to know alternate is availalbe or not for particular product.
             * is Alternate is available then "isAlternateProductAvailableInCart" becomes true.
             */
            if (isAlternateProductAvailableInCart) {
                PaymentHelper.setIsCartSwitched(false);
                callback.updateCartSubstitutionView(isAlternateDataIsThere, true, isAlternateDataIsThere ? getCartSubstitutionResponseModel().getTotalSavings() : BigDecimal.ZERO);
            } else {
                PaymentHelper.setIsCartSwitched(false);
                callback.updateCartSubstitutionView(false, false, null);
            }
        }
    }

    /**
     * Check prime for this customer by call Customer details API at very first time.
     *
     * @param transactionId
     */
    private void initialAPIS(int transactionId) {
        if (GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION == transactionId) {
            initiateAPICalls(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
            if (!isPrimeProductOnlyAvailableInCart()) {
                if (!SubscriptionHelper.getInstance().isSubscriptionFlag())
                    initiateAPICalls(APIServiceManager.C_MSTAR_COUPON_LIST);
            }
            if (!isM2Order() && !SubscriptionHelper.getInstance().isSubscriptionFlag()) {
                initiateAPICalls(GET_WALLET_BALLANCE_ONLY);
            }
        } else {
            if (isPrimeDeletedFromCart) {
                if (cartDetailsForPrimeMutableLiveData != null && cartDetails != null) {
                    cartDetailsForPrimeMutableLiveData.setValue(cartDetails);
                }
            }
        }
    }

    private void cartSubstitutionResponse(String data, int transactionId) {
        if (!TextUtils.isEmpty(data)) {
            CartSubstitutionBasicResponseTemplateModel cartSubstitution = new Gson().fromJson(data, CartSubstitutionBasicResponseTemplateModel.class);
            if (cartSubstitution != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(cartSubstitution.getStatus()) && !cartSubstitution.getResult().getAlternatives().isEmpty()) {
                setCartSubstitutionResponseModel(cartSubstitution.getResult());
            } else {
                setCartSubstitutionResponseModel(null);
            }
            PaymentHelper.setOriginalCartAmount(cartSubstitution.getResult().getOriginalCartTotalValue());
        }
        initiateAPICalls(APIServiceManager.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART == transactionId ? GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION : APIServiceManager.MSTAR_GET_CART_DETAILS);
    }

    private void updateCartSubstitutionResponse(String data, int transactionId) {
        callback.dismissLoader();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                if (APIServiceManager.MSTAR_REVERT_ORIGINAL_CART == transactionId) {
                    callback.updateCartSubstitutionView(false, false, null);
                    PaymentHelper.setIsCartSwitched(false);
                    PaymentHelper.setIsInitiateCheckoutCalled(false);
                    updateCartDetailsAfterCartUpdationFromAlternate(response);
                } else {
                    if (!response.getResult().getCartSwitchResultsList().isEmpty()) {
                        for (CartSwitchResult cartSwitchResult : response.getResult().getCartSwitchResultsList()) {
                            if (!(AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(cartSwitchResult.getResultReasonCode()) && AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(cartSwitchResult.getResultReasonCode()))) {
                                updateCartDetailsAfterCartUpdationFromAlternate(response);
                            }
                        }
                    }
                }
            } else {
                callback.updateCartSubstitutionView(false, false, null);
            }
        } else {
            callback.updateCartSubstitutionView(false, false, null);
        }
    }

    public void updateCartDetailsAfterCartUpdationFromAlternate(MStarBasicResponseTemplateModel response) {
        isAlternateProductSwitchedAlteastOnce = false;
        isAlternateProductAvailableInCart = false;
        this.cartDetails = response.getResult().getCartDetails();
        setCartItemInModel(cartDetails.getLines());
        loadScreenWithCartDetails();
        loadBrainSinsRecommendedProducts();
        callback.updateCartPaymentDetails(cartDetails);
        if (cartDetails.getUsedWalletAmount() != null && cartDetails.getUsedWalletAmount().getTotalWallet().compareTo(BigDecimal.ZERO) > 0) {
            if (isAlternateCartEnable() && !PaymentHelper.isCartSwitched()) {
                initiateAPICalls(GET_BALLANCE_AND_CHECK_ALTERNATE);
            } else {
                checkCartSubstitutionDetails();
                initiateAPICalls(GET_WALLET_BALLANCE_ONLY);
            }
        } else {
            if (isAlternateCartEnable()) {
                if (PaymentHelper.isCartSwitched()) {
                    initiateAPICalls(GET_WALLET_BALLANCE_ONLY);
                    checkCartSubstitutionDetails();
                } else {
                    initiateAPICalls(GET_BALLANCE_AND_CHECK_ALTERNATE);
                }
            } else {
                checkCartSubstitutionDetails();
            }
        }
    }

    private String getM2Coupon() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getM2Coupon() != null &&
                !TextUtils.isEmpty(response.getResult().getConfigDetails().getM2Coupon().getCouponCode()) ? response.getResult().getConfigDetails().getM2Coupon().getCouponCode() : "";
    }

    public String getM2OrderCouponDiscount() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getM2Coupon().getDiscount()) ? response.getResult().getConfigDetails().getM2Coupon().getDiscount() : "";
    }

    private String getSubscriptionPromoCode() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList().getCouponCode()) ? configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList().getCouponCode() : "";
    }

    private String getSubscriptionDiscriptoin() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList().getDescription()) ? configurationResponse.getResult().getConfigDetails().getSubscriptionCouponList().getDescription() : "";
    }

    public String getSubscriptionCouponDiscount() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getSubscriptionCouponList().getDiscount()) ? response.getResult().getConfigDetails().getSubscriptionCouponList().getDiscount() : "";
    }

    private boolean isPrimeProductOnlyAvailableInCart() {
        MstarPrimeProductResult primeProductResult = new Gson().fromJson(basePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
        boolean isPrimeProductAvailable = false;
        if (cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
            for (MStarProductDetails productDetails : cartDetails.getLines()) {
                if (!isPrimeProductAvailable) {
                    isPrimeProductAvailable = primeProductResult != null && !primeProductResult.getPrimeProductCodesList().isEmpty() &&
                            primeProductResult.getPrimeProductCodesList().contains(String.valueOf(productDetails.getProductCode()));
                }
            }
        }
        return cartDetails.getLines().size() == 1 && isPrimeProductAvailable;
    }

    private void getCustomerDetails(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getCustomerDetails() != null) {
            isPrimeCustomer = response.getResult().getCustomerDetails().isPrime();
            mStarCustomerDetails = response.getResult().getCustomerDetails();
            PaymentHelper.setIsPrime(response.getResult().getCustomerDetails().isPrime());
            billingAddressID = response.getResult().getCustomerDetails().getPreferredBillingAddress();
            //this mutable for prime product
            cartDetailsForPrimeMutableLiveData.setValue(cartDetails);
        }
    }

    private void singleAddressResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getAddressModel() != null) {
            PaymentHelper.setSingle_address(new Gson().toJson(response.getResult().getAddressModel()));
            PaymentHelper.setCustomerBillingAddress(response.getResult().getAddressModel());
            setShippingAndBillingAddress();
        }
    }

    private void settingBillingAndShippingAddressResponse(String data, int mstarSetPreferredShippingAddress) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && mstarSetPreferredShippingAddress == 50013) {
            callback.onClickProceedButton(false);
        }
    }

    public void onMStarProductDetailsForPrimeAvailable(List<MStarProductDetails> productDetailsList) {
        MstarPrimeProductResult primeProductResult = new Gson().fromJson(basePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
        boolean isPrimeProductInCart = false;
        for (MStarProductDetails mStarProductDetails : productDetailsList) {
            if (primeProductResult != null && primeProductResult.getPrimeProductCodesList() != null && primeProductResult.getPrimeProductCodesList().contains(String.valueOf(mStarProductDetails.getProductCode()))) {
                isPrimeProductInCart = true;
                break;
            }
        }
        callback.showPrimeMemberShipBanner(isPrimeProductInCart || (isPrimeCustomer && CommonUtils.isExpiryDate(mStarCustomerDetails != null ? mStarCustomerDetails : new MStarCustomerDetails())) || isM2Order() || SubscriptionHelper.getInstance().isSubscriptionFlag());
        callback.dismissLoader();
    }

    private void couponListResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getPromoCodeLists() != null && response.getResult().getPromoCodeLists().size() > 0) {
                promoCodeLists = response.getResult().getPromoCodeLists();
                callback.poromocodeDescription(promoCodeLists);
            }
            if (response != null && response.getResult() != null && response.getResult().getMstarSuperCash() != null) {
                callback.nmsSuperCashDescription(response.getResult().getMstarSuperCash());
            }
            if (promoCodeLists != null && promoCodeLists.size() > 0 && cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()))
                for (PromoCodeList promoCodeList : promoCodeLists) {
                    if (promoCodeList.getCouponCode().equals(cartDetails.getAppliedCoupons())) {
                        callback.couponDescription(promoCodeList.getDescription());
                    }
                }
        }
        callback.dismissLoader();
    }

    private void walletBalanceResponse(String data, int transactionId) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel walletResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (walletResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(walletResponse.getStatus()) && walletResponse.getResult().getCashbackBalance().compareTo(BigDecimal.ZERO) > 0) {
                callback.walletBalance(walletResponse.getResult() != null && walletResponse.getResult().getCashbackUseableBalance().compareTo(BigDecimal.ZERO) > 0 ? String.valueOf(walletResponse.getResult().getCashbackUseableBalance()) : "", true);
            } else
                callback.walletBalance("", false);
        } else {
            callback.walletBalance("", false);
        }
        if (GET_BALLANCE_AND_CHECK_ALTERNATE == transactionId) {
            if (isAlternateCartEnable() && !PaymentHelper.isCartSwitched()) {
                initiateAPICalls(GET_ALTERNATE_AND_GET_CART_DETAILS_ONLY);
            } else {
                initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
            }
        } else {
            if (GET_WALLET_BALLANCE_ONLY != transactionId) {
                initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
            }
        }
    }

    private String getProperIndexValue(int position) {
        if (position < 9 && position != 0) {
            return "0" + (position + 1);
        } else if (position == 0) {
            return "01";
        } else {
            return String.valueOf(position);
        }
    }

    private void onNotAddedProductDetailsAvailable(String data) {
        callback.dismissLoader();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && !response.getResult().getProductDetailsList().isEmpty()) {
                this.notAddedProducts = response.getResult().getProductDetailsList();
                PaymentHelper.setAnalyticalTrackingProductList(response.getResult().getProductDetailsList());//store analytical tracking details

                /*WebEngage Event*/
                if (webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_CART_VIEWED)
                        || webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_CART_UPDATED)
                        || webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_REMOVED_FROM_CART)) {
                    webEngageEvent(cartDetails);
                }
            }
        }
    }

    private void onSuggestedProductResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            RecommendedProductResponse recommendedProductResponse = new Gson().fromJson(data, RecommendedProductResponse.class);
            if (recommendedProductResponse != null && recommendedProductResponse.getRecommendedProductList() != null && !recommendedProductResponse.getRecommendedProductList().isEmpty() && recommendedProductResponse.getRecommendedProductList().size() > 0) {
                callback.setProductSuggestionView(configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getCartTryAndBuy(), true);
                callback.setProductSuggestionAdapter(recommendedProductResponse.getRecommendedProductList());
            } else {
                callback.setProductSuggestionView(null, false);
            }
        }
    }

    private void onDetachedResponse(String data) {
        boolean isCompletelyDetached = true;
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null
                    && response.getResult().getDetachment_result() != null && !response.getResult().getDetachment_result().isEmpty()) {

                Map<String, MstarPrescriptionDetails> detachDetails = response.getResult().getDetachment_result();

                for (Map.Entry<String, MstarPrescriptionDetails> detachEntry : detachDetails.entrySet()) {
                    if (!detachEntry.getValue().getStatus().equalsIgnoreCase(AppConstant.DETACHED)) {
                        isCompletelyDetached = false;
                    }
                }
            }
        }
        if (!isFromEditOrder && isCompletelyDetached) {
            unwantedPrescriptions.clear();
            MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
            proceedAfterRemovingUnwantedPrescritpion();
        }
    }

    private void onAddtoCartResponse(String data) {
        callback.dismissLoader();
        MStarBasicResponseTemplateModel addToCartResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addToCartResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addToCartResponse.getStatus())) {
            removeOutOfStockProductNameAndDetails();
            callWalletThenCartDetails(GET_BALLANCE_AND_CHECK_ALTERNATE);
        } else if (addToCartResponse != null && addToCartResponse.getReason() != null) {
            if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(addToCartResponse.getReason().getReason_code())) {
                callback.showErrorMessage(callback.getContext().getResources().getString(R.string.text_medicine_max_limit));
            }
        }
    }

    private void onRemoveProductResponse(String data) {
        if (!TextUtils.isEmpty(data) && new Gson().fromJson(data, MStarBasicResponseTemplateModel.class).getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {

            /*WebEngage RemoveFromCart Event*/
            if (getRemoveProduct() != null) {
                setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_REMOVED_FROM_CART);
            }

            removeOutOfStockProductNameAndDetails();
            if (isMultipleDelete && !outOfStockProductCodeMap.isEmpty()) {
                removeAllOutOfStockProduct();
            } else if (isForAlternate) {
                initiateAPICalls(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
            } else {
                isAlternateProductSwitchedAlteastOnce = false;
                isAlternateProductAvailableInCart = false;
                callWalletThenCartDetails(GET_BALLANCE_AND_CHECK_ALTERNATE);
            }
        } else
            callWalletThenCartDetails(0);
    }

    private void removeOutOfStockProductNameAndDetails() {
        if (outOfStockProductCodeMap.containsKey(updateingProductCode))
            outOfStockProductCodeMap.remove(updateingProductCode);
        if (outOfStockProductsResultMap.containsKey(String.valueOf(updateingProductCode)))
            outOfStockProductsResultMap.remove(String.valueOf(updateingProductCode));
    }

    private void emptyCartProperties() {
        if (PaymentHelper.isIsTempCart()) {
            PaymentHelper.setIsTempCartOpen(true);
        } else if (isM2Order()) {
            basePreference.setMStarM2CartIsOpen(true);
        } else {
            basePreference.setMStarCartId(0);
            basePreference.setMStarM1CartIsOpen(true);
            basePreference.setCartCount(0);
        }
    }

    private void setDefaultProperties() {
        if (PaymentHelper.isIsTempCart()) {
            PaymentHelper.setIsTempCartOpen(AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(cartDetails.getCartStatus()));
        } else if (!isM2Order()) {
            MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
            basePreference.setMStarM1CartIsOpen(AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(cartDetails.getCartStatus()));
            basePreference.setMStarCartId(cartDetails.getId());
            basePreference.setCartCount(cartDetails.getLines().size());
        } else {
            basePreference.setMstarMethod2CartId(cartDetails.getId());
            basePreference.setMStarM2CartIsOpen(AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(cartDetails.getCartStatus()));
        }
        /**
         * If coupon or super cash apply already just need to update the view in cart page.
         */
        showAppliedPromocoe();
        showAppliedNMSSuperCash();
    }

    private void showAppliedPromocoe() {
        callback.showAppliedPromocodeView(!TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? cartDetails.getAppliedCoupons() : "", cartDetails);
    }

    private void showAppliedNMSSuperCash() {
        PaymentHelper.setIsNMSSuperCashApplied(cartDetails.isUseSuperCash());
        callback.showNNMSSuperCashView(cartDetails.isUseSuperCash());
    }

    private void loadPrescriptionIfPresentInCart() {
        if (MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().isEmpty()) {
            ArrayList<MStarUploadPrescription> prescriptionArrayList = new ArrayList<>();
            for (String uploadedPrescriptionId : cartDetails.getPrescriptions()) {
                MStarUploadPrescription prescriptionDetails = new MStarUploadPrescription();
                prescriptionDetails.setPrescriptionUploaded(true);
                prescriptionDetails.setUploadedPrescriptionId(uploadedPrescriptionId);
                prescriptionDetails.setPastPrescriptionId(uploadedPrescriptionId);
                prescriptionArrayList.add(prescriptionDetails);
            }
            MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(prescriptionArrayList);
        }
    }


    private void loadBrainSinsRecommendedProducts() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null
                && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getBrainsinsKey()) && configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues() != null && configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getCartTryAndBuy() != null) {
            if (configurationResponse.getResult().getConfigDetails().getBrainsinStatusFlag() && configurationResponse.getResult().getConfigDetails().getBrainSinConfigValues().getCartTryAndBuy().isEnableStatus()) {
                initiateAPICalls(APIServiceManager.C_MSTAR_BRAINSINS_SUGGESTED_PRODUCTS);
            } else {
                callback.setProductSuggestionView(null, false);
            }
        } else
            callback.setProductSuggestionView(null, false);
    }

    private String getRecommendedProductUrl() {
        String url;
        ConfigDetails configDetails = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ?
                configurationResponse.getResult().getConfigDetails() : new ConfigDetails();
        String brainsinsKey = !TextUtils.isEmpty(configDetails.getBrainsinsKey()) ? configDetails.getBrainsinsKey() : "";
        String cartTryAndBuyTemplateId = configDetails.getBrainSinConfigValues() != null && configDetails.getBrainSinConfigValues().getCartTryAndBuy() != null &&
                !TextUtils.isEmpty(configDetails.getBrainSinConfigValues().getCartTryAndBuy().getTemplateId()) ? configDetails.getBrainSinConfigValues().getCartTryAndBuy().getTemplateId() : "";
        url = ConfigMap.getInstance().getProperty(ConfigConstant.RECOMMENDATION_ENGINE_URL) + String.format(ConfigMap.getInstance().getProperty(ConfigConstant.RECOMMENDED_CART_URL),
                basePreference.isGuestCart() ? cartDetails.getId() : basePreference.getMstarCustomerId(), PaymentUtils.getDeCodedString(brainsinsKey), cartTryAndBuyTemplateId);
        return url;
    }


    private void initiateCheckOutResponse(String data) {
        MStarBasicResponseTemplateModel initiateCheckoutModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (initiateCheckoutModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(initiateCheckoutModel.getStatus())) {
            if (isAlternateCartEnable()) {
                PaymentHelper.setIsInitiateCheckoutCalled(true);
                initiateAPICalls(APIServiceManager.MSTAR_GET_ALTERNATE_PRODUCT_FOR_CART);
            } else {
                initiateAPICalls(GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION);
            }
        } else {
            initiateAPICalls(GET_CART_DETAILS_AND_CHECK_INITIAL_CONDITION);
        }
    }

    private void onApplyPromoCodeResponce(String data) {
        MStarBasicResponseTemplateModel initiateCheckoutModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (initiateCheckoutModel != null && !AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(initiateCheckoutModel.getStatus())) {
            failedToApplyPromocode(initiateCheckoutModel.getReason() != null ? initiateCheckoutModel.getReason().getReason_eng() : "");
            onCouponAppliedEvent(WebEngageHelper.FAILURE);
        } else if (initiateCheckoutModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(initiateCheckoutModel.getStatus()))
            setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_COUPON_CODE_APPLIED);
        continueToCart();
    }

    private void onUnApplyPromoCodeResponce(String data) {
        MStarBasicResponseTemplateModel initiateCheckoutModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (initiateCheckoutModel != null && !AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(initiateCheckoutModel.getStatus())) {
            failedToApplyPromocode(initiateCheckoutModel.getReason() != null ? initiateCheckoutModel.getReason().getReason_eng() : "");
        } else if (initiateCheckoutModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(initiateCheckoutModel.getStatus()))
            setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_UPDATED);
        continueToCart();
    }

    private void onApplyWalletResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (responseTemplateModel != null && responseTemplateModel.getStatus() != null && responseTemplateModel.getStatus().equalsIgnoreCase(AppConstant.API_MSTAR_FAILURE_STATUS)) {
                failedToApplyPromocode(responseTemplateModel.getReason() != null ? responseTemplateModel.getReason().getReason_eng() : "");
            } else if (responseTemplateModel != null && responseTemplateModel.getStatus() != null && responseTemplateModel.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {
                setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_UPDATED);
            }
        }
        continueToCart();
    }

    private void continueToCart() {
        callWalletThenCartDetails(0);
    }

    private void callWalletThenCartDetails(int transactionId) {
        isForAlternate = false;
        isMultipleDelete = false;
        if (!isM2Order() && !SubscriptionHelper.getInstance().isSubscriptionFlag() && !basePreference.isGuestCart() && 0 != transactionId) {
            initiateAPICalls(GET_BALLANCE_AND_CHECK_ALTERNATE);
        } else {
            initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
        }
    }

    private void failedToShowSuggestedProducts() {
        callback.dismissLoader();
        callback.setProductSuggestionView(null, false);
    }

    private void failedToApplyPromocode(String message) {
        callback.dismissLoader();
        if (!TextUtils.isEmpty(message)) {
            callback.showErrorMessage(message);
        }
    }

    private void loadScreenWithCartDetails() {
        String imageUrl = "";
        List<MStarProductDetails> localUpdatedCartItems;
        MStarBasicResponseTemplateModel model = new Gson().fromJson(basePreference != null && !TextUtils.isEmpty(basePreference.getConfigURLPath()) ? basePreference.getConfigURLPath() : "", MStarBasicResponseTemplateModel.class);
        if (model != null && model.getResult() != null && model.getResult().getProductImageUrlBasePath() != null) {
            imageUrl = model.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/";
        }
        MstarPrimeProductResult primeProductResult = new Gson().fromJson(basePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
        PaymentHelper.setCartLineItems(cartDetails.getLines());
        setProductCodeListInCart(cartDetails.getLines());
        localUpdatedCartItems = new ArrayList<>(cartDetails.getLines());
        isRxProductAvailableInCart = false;
        if (!outOfStockProductsResultMap.isEmpty() && !notAddedProducts.isEmpty()) {
            for (MStarProductDetails product : notAddedProducts) {
                if (outOfStockProductsResultMap.containsKey(String.valueOf(product.getProductCode()))) {
                    product.setProductAddResult(outOfStockProductsResultMap.get(String.valueOf(product.getProductCode())));
                    product.setOutofStock(true);
                }
            }
            localUpdatedCartItems.addAll(notAddedProducts);
        }
        isEdited = cartDetails.isEdited();
        if (outOfStockProductNameList != null)
            outOfStockProductNameList.clear();
        if (outOfStockProductCodeMap != null)
            outOfStockProductCodeMap.clear();
        if (localUpdatedCartItems.size() > 0 && primeProductResult != null && primeProductResult.getPrimeProductCodesList() != null && primeProductResult.getPrimeProductCodesList().size() > 0) {
            adapter = new MStarCartAdapter(callback.getContext(), this, localUpdatedCartItems, imageUrl, primeProductResult.getPrimeProductCodesList(), interCityProductList);
            callback.updateCartProductAdapter(adapter);
        }
    }

    private void setProductCodeListInCart(List<MStarProductDetails> lines) {
        productCodeListInCart.clear();
        for (MStarProductDetails productDetails : lines) {
            productCodeListInCart.add(String.valueOf(productDetails.getProductCode()));
        }
    }

    public void onApplyPromocode() {
        if (!SubscriptionHelper.getInstance().isSubscriptionFlag() && !M2Helper.getInstance().isM2Order() && !isFromEditOrder) {
            callback.onLaunchApplyPromocodeDialog(cartDetails.getAppliedCoupons(), promoCodeLists);
        }
    }

    public void onApplyPromoCodeFromDialog(String promoCode) {
        callback.showLoader();
        applingPromoCode = promoCode;
        initiateAPICalls(APIServiceManager.MSTAR_APPLY_PROMO_CODE);

    }

    public void onApplyNmsSuperCash() {
        callback.showLoader();
        initiateAPICalls(APIServiceManager.MSTAR_APPLY_WALLET);

    }

    public String promoCodeTitle() {
        return isM2Order() || isFromEditOrder || SubscriptionHelper.getInstance().isSubscriptionFlag() ? callback.getContext().getString(R.string.text_subscription_coupon_text) : callback.getContext().getString(R.string.text_promo_code);
    }

    public void navigateToSearch() {
        callback.onClickSearchButton();
    }

    public void onLaunchOfCartSubstitution() {
        if (isAlternateProductSwitchedAlteastOnce) {
            callback.showConfirmationDialogForRevertToOriginalCart();
        } else {
            PaymentHelper.setIsCartSwitched(false);
            callback.onLaunchOfCartSubstitution();
        }
    }

    public void proceed() {
        /*for avoiding to open new activity twice*/
        callback.showLoader();
        callback.disableProceedButton(true);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isCartAlternateEnableFlag()) {
            checkOutOfStockDetails();
        } else {
            checkM2AndSubscriptionWarning();
        }
    }

    public void checkOutOfStockDetails() {
        if (!outOfStockProductCodeMap.isEmpty()) {
            callback.showOutOfStockWarning(outOfStockProductNameList, outOfStockProductCodeMap);
        } else checkM2AndSubscriptionWarning();
    }

    private void checkM2AndSubscriptionWarning() {
        if (isEditable && SubscriptionHelper.getInstance().isSubscriptionEdit()) {
            callback.subscriptionEditDialogue();
        } else if (isFromEditOrder && cartDetails.isEdited()) {
            callback.showM2UpdatedWarning();
        } else
            proceedToNextPage();
    }

    public void doM2Updation() {
        proceedAfterRemovingUnwantedPrescritpion();
    }

    public void getOriginalM2RetryCart() {
        initiateAPICalls(APIServiceManager.MSTAR_GET_FILLED_M2_CART);
        if (!RefillHelper.getNewRxIdList().isEmpty()) {
            rxidListAfterEditOrderCancel = RefillHelper.getNewRxIdList();
            initiateAPICalls(APIServiceManager.MSTAR_DETACH_PRESCRIPTION);
        }
    }

    public void proceedToNextPage() {
        if (cartDetails != null && !cartDetails.isIsmethod2() && !isRxProductAvailableInCart && cartDetails.getPrescriptions() != null && cartDetails.getPrescriptions().size() > 0) {
            removeUnwantedPrescription();
        } else {
            proceedAfterRemovingUnwantedPrescritpion();
        }
    }

    private void proceedAfterRemovingUnwantedPrescritpion() {
        if (basePreference.isGuestCart()) {
            callback.navigateToSignIn();
        } else {
            int addressId = PaymentHelper.getCustomerBillingAddress() != null ? PaymentHelper.getCustomerBillingAddress().getId() : 0;
            if (basePreference.getPreviousCartBillingAddressId() > 0 || billingAddressID > 0) {
                if (addressId > 0 && addressId == basePreference.getPreviousCartBillingAddressId()) {
                    callback.onClickProceedButton(false);
                } else {
                    initiateAPICalls(APIServiceManager.MSTAR_GET_ADDRESS_BY_ID);
                }
            } else {
                callback.onClickProceedButton(true);
            }
        }
    }

    private void removeUnwantedPrescription() {
        initiateAPICalls(APIServiceManager.MSTAR_DETACH_PRESCRIPTION);
    }

    public String cartPrimeBannerHeader() {
        return shoppingCart() != null && !TextUtils.isEmpty(shoppingCart().getHeader()) ? shoppingCart().getHeader() : "";
    }

    public String cartPrimeBannerDescription() {
        return shoppingCart() != null && !TextUtils.isEmpty(shoppingCart().getDescription()) ? shoppingCart().getDescription() : "";
    }

    private ShoppingCart shoppingCart() {
        return primeConfig() != null && primeConfig().getShoppingCart() != null ? primeConfig().getShoppingCart() : new ShoppingCart();
    }

    private PrimeConfig primeConfig() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(callback.getContext()).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getPrimeConfig() : new PrimeConfig();
    }

    @Override
    public void increaseQuantity(int productCode, int updatedQuantity) {
        this.addingProductCode = productCode;
        this.addingProductQuantity = updatedQuantity;
        setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_UPDATED);
        callback.showLoader();
        if (updatedQuantity > 0) {
            initiateAPICalls(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
        }
    }

    @Override
    public void decreaseQuantity(int productCode, int updatedQuantity) {
        this.updateingProductCode = productCode;
        this.updatedQuantity = updatedQuantity;
        setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_UPDATED);
        callback.showLoader();
        if (updatedQuantity > 0) {
            initiateAPICalls(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
        }
    }

    @Override
    public View.OnClickListener removeProductFromCart(final MStarProductDetails productDetails, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStarProductDetails = productDetails;
                if (notAddedProducts != null && !notAddedProducts.isEmpty() && notAddedProducts.contains(productDetails)) {
                    notAddedProducts.remove(productDetails);
                    if (RefillHelper.getOutOfStockProductsMap().containsKey(String.valueOf(productDetails.getProductCode()))) {
                        RefillHelper.getOutOfStockProductsMap().remove(String.valueOf(productDetails.getProductCode()));
                    }
                    if (outOfStockProductsResultMap.containsKey(String.valueOf(productDetails.getProductCode())))
                        outOfStockProductsResultMap.remove(String.valueOf(productDetails.getProductCode()));
                    loadScreenWithCartDetails();
                } else {
                    updateingProductCode = productDetails.getProductCode();
                    updatedQuantity = productDetails.getCartQuantity();
                    setRemoveProduct(productDetails);
                    callback.showLoader();
                    if (CommonUtils.isPrimeProduct(productDetails.getProductCode(), basePreference)) {
                        if (!isPrimeDeletedFromCart) {
                            isPrimeDeletedFromCart = true;
                        }
                    }
                    initiateAPICalls(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
                }
            }
        };
    }

    @Override
    public void viewAlternateClickCallback(int productCode) {
        callback.showAlternateSalt(productCode);
    }

    @Override
    public void setOutOfStockCodeList(Integer outOfStockProductCode, MStarProductDetails productDetails) {
        outOfStockProductCodeMap.put(outOfStockProductCode, productDetails);
        outOfStockProductNameList.add(productDetails.getDisplayName());
        this.isOutOfStockProductInCart = true;
    }

    @Override
    public boolean isPrimeProduct(int productCode) {
        return CommonUtils.isPrimeProduct(productCode, basePreference);
    }

    @Override
    public void initiateTapToRevert(int productCode) {
        List<MstarSwitchAlternateProductRequest> mstarSwitchAlternateProductRequestList = new ArrayList<>();
        MstarSwitchAlternateProductRequest mstarSwitchAlternateProductRequest;
        mstarSwitchAlternateProductRequest = new MstarSwitchAlternateProductRequest();
        mstarSwitchAlternateProductRequest.setCartProductCode(productCode);
        mstarSwitchAlternateProductRequestList.add(mstarSwitchAlternateProductRequest);
        setSwitchAlternateProductRequest(mstarSwitchAlternateProductRequestList);
        initiateAPICalls(APIServiceManager.MSTAR_SWITCH_ALTERNATE_PRODUCT);
    }

    private void checkAlternateSwitchedCondition(boolean alternateSwitched, boolean alternateAvailable) {
        if (!isAlternateProductSwitchedAlteastOnce) {
            isAlternateProductSwitchedAlteastOnce = alternateSwitched;
        }
        if (!isAlternateProductAvailableInCart && !isAlternateProductSwitchedAlteastOnce) {
            isAlternateProductAvailableInCart = alternateAvailable;
        }
    }


    @Override
    public void isRxRequiredProductAvailableInCart(boolean rxRequiredProductAvailability, MStarProductDetails cartItem) {
        if (rxRequiredProductAvailability) {
            getRxProductIdList().add(String.valueOf(cartItem.getProductCode()));
            getRxProductNameList().add(cartItem.getDisplayName());
        }
        if (!isRxProductAvailableInCart) {
            isRxProductAvailableInCart = rxRequiredProductAvailability;
        }
    }

    @Override
    public View.OnClickListener navigateToProductDetails(final int productCode) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.navigateToProductDetails(productCode);
            }
        };
    }

    public boolean isRxProductAvailableInCart() {
        return isRxProductAvailableInCart;
    }

    public ArrayList<String> getRxProductNameList() {
        return rxProductNameList;
    }

    public ArrayList<String> getRxProductIdList() {
        return rxProductIdList;
    }

    @Override
    public void showProgressBar() {
        callback.showLoader();
    }

    @Override
    public void hideProgressBar() {
        callback.dismissLoader();
    }

    @Override
    public Context getContext() {
        return callback.getContext();
    }

    @Override
    public void setBitmapImageAdapter() {

    }

    public boolean isM2Order() {
        return M2Helper.getInstance().isM2Order() || (cartDetails != null && cartDetails.isIsmethod2());
    }

    private void setWebEngageTrackEvent(String webEngageTrackEvent) {
        this.webEngageTrackEvent = webEngageTrackEvent;
    }

    private void webEngageEvent(MStarCartDetails cartDetails) {
        switch (webEngageTrackEvent) {
            case WebEngageHelper.EVENT_NAME_CART_VIEWED:
                onCartViewedEvent(cartDetails);
                break;
            case WebEngageHelper.EVENT_NAME_CART_UPDATED:
                onCartUpdateEvent(cartDetails);
                break;
            case WebEngageHelper.EVENT_NAME_REMOVED_FROM_CART:
                onRemovedCartEvent();
                break;

        }
    }

    /*WebEngage CartViewed Event*/
    private void onCartViewedEvent(MStarCartDetails cartDetails) {
        WebEngageHelper.getInstance().cartViewedEvent(callback.getContext(), cartDetails, notAddedProducts);
        setWebEngageTrackEvent("");
    }

    /*WebEngage CartUpdate Event*/
    private void onCartUpdateEvent(MStarCartDetails cartDetails) {
        WebEngageHelper.getInstance().cartUpdateEvent(callback.getContext(), cartDetails, notAddedProducts);
        setWebEngageTrackEvent("");
    }

    /*WebEngage CouponCode Applied Event*/
    private void onCouponAppliedEvent(String status) {
        setWebEngageTrackEvent("");
        if (cartDetails != null) {
            WebEngageHelper.getInstance().couponCodeAppliedEvent(callback.getContext(), cartDetails, WebEngageHelper.CASH_DISCOUNT, status);
            if (WebEngageHelper.SUCCESS.equalsIgnoreCase(status))
                onCartUpdateEvent(cartDetails);
        }
    }

    /*WebEngage Removed Cart Event*/
    private void onRemovedCartEvent() {
        setWebEngageTrackEvent("");
        if (notAddedProducts != null && notAddedProducts.size() > 0) {
            MStarProductDetails removedProductDetails = null;
            for (Iterator<MStarProductDetails> iterator = notAddedProducts.iterator(); iterator.hasNext(); ) {
                MStarProductDetails details = iterator.next();
                if (String.valueOf(details.getProductCode()).equals(getRemoveProductCode())) {
                    removedProductDetails = details;
                    iterator.remove();
                }
            }
            WebEngageHelper.getInstance().removedFromCartEvent(callback.getContext(), getRemoveProduct() != null ? getRemoveProduct().getCartQuantity() : 0, removedProductDetails);
            if (!isCartEmpty)
                onCartUpdateEvent(cartDetails);

            /*Google Tag Manager + FireBase Removed From Cart Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseRemoveFromCartEvent(callback.getContext(), removedProductDetails, getRemoveProduct() != null ? getRemoveProduct().getCartQuantity() : 0);
        }
    }

    private void setCartItemInModel(List<MStarProductDetails> result) {
        deliveryEstimateRequest = new DeliveryEstimateRequest();
        deliveryEstimateRequest.setCallingToRoute(false);
        deliveryEstimateRequest.setDoNotSplit(false);
        List<DeliveryEstimateLineItem> lineItems = new ArrayList<>();
        for (MStarProductDetails item : result) {
            DeliveryEstimateLineItem items = new DeliveryEstimateLineItem();
            items.setItemcode(String.valueOf(item.getProductCode()));
            items.setQty(item.getCartQuantity());
            lineItems.add(items);
            checkAlternateSwitchedCondition(item.isAlternateSwitched(), item.isAlternateAvailable());
        }
        deliveryEstimateRequest.setLstdrug(lineItems);
    }

    public DeliveryEstimateRequest getDeliveryEstimateRequest() {
        return deliveryEstimateRequest;
    }

    public void onCheckoutStartEvent() {
        if (cartDetails != null)
            WebEngageHelper.getInstance().checkoutStartedEvent(callback.getContext(), cartDetails);

        /*Google Tag Manager + FireBase Begin Checkout Event*/
        FireBaseAnalyticsHelper.getInstance().logFireBaseBeginCheckoutEvent(callback.getContext(), cartDetails, notAddedProducts);
    }

    private MStarProductDetails getRemoveProduct() {
        return removeProduct;
    }

    private void setRemoveProduct(MStarProductDetails removeProduct) {
        this.removeProduct = removeProduct;
    }

    public void addAlternateProduct(int productToAdd, int quantity, int outOfStockProductCode) {
        this.addingProductQuantity = quantity;
        this.addingProductCode = productToAdd;
        this.isForAlternate = true;
        if (productCodeListInCart.contains(String.valueOf(outOfStockProductCode))) {
            this.updateingProductCode = outOfStockProductCode;
            initiateAPICalls(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);

        } else {
            initiateAPICalls(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
        }
    }

    public void unApplyPromoCode(String removingPromoCode) {
        this.removingPromoCoe = removingPromoCode;
        callback.showLoader();
        initiateAPICalls(APIServiceManager.MSTAR_UN_APPLY_PROMO_CODE);
    }

    public void removeAllOutOfStockProduct() {
        isMultipleDelete = true;
        if (!outOfStockProductCodeMap.isEmpty()) {
            Map.Entry<Integer, MStarProductDetails> productDetailsEntry = outOfStockProductCodeMap.entrySet().iterator().hasNext() ? outOfStockProductCodeMap.entrySet().iterator().next() : null;
            if (productDetailsEntry != null) {
                this.updateingProductCode = productDetailsEntry.getKey();
                initiateAPICalls(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
            }
        } else {
            callWalletThenCartDetails(0);
        }
    }

    public void startViewAlternateProcess() {
        if (!outOfStockProductCodeMap.isEmpty()) {
            for (Map.Entry<Integer, MStarProductDetails> productDetailsEntry : outOfStockProductCodeMap.entrySet()) {
                MStarProductDetails productDetails = productDetailsEntry.getValue();
                if (productDetails.isRxRequired()) {
                    callback.showAlternateSalt(productDetailsEntry.getKey());
                    break;
                }
            }
        } else {
            checkOutOfStockDetails();
        }
    }

    private double fireBaseEventNmsSuperCash() {
        return cartDetails.getUsedWalletAmount().getNmsSuperCash().doubleValue();

    }

    private String fireBaseEventProductCategory() {
        if (mStarProductDetails != null) {
            if (mStarProductDetails.getProductType().equalsIgnoreCase(AppConstant.OTC_ORDER)) {
                return AppConstant.OTC;
            } else if (mStarProductDetails.getProductType().equalsIgnoreCase(AppConstant.PRESCRIPTION_STATUS_PENDING)) {
                if (mStarProductDetails.isRxRequired()) {
                    return AppConstant.PHARMA;
                } else
                    return AppConstant.NON_PHARMA;
            }
        }
        return "";
    }

    private void setShippingAndBillingAddress() {
        initiateAPICalls(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
        initiateAPICalls(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
        initiateAPICalls(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
        initiateAPICalls(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
    }

    public CartSubstitutionResponseModel getCartSubstitutionResponseModel() {
        return cartSubstitutionResponseModel;
    }

    public void setCartSubstitutionResponseModel(CartSubstitutionResponseModel cartSubstitutionResponseModel) {
        this.cartSubstitutionResponseModel = cartSubstitutionResponseModel;
    }

    public List<MstarSwitchAlternateProductRequest> getSwitchAlternateProductRequest() {
        return switchAlternateProductRequest;
    }

    public void setSwitchAlternateProductRequest(List<MstarSwitchAlternateProductRequest> switchAlternateProductRequest) {
        this.switchAlternateProductRequest = switchAlternateProductRequest;
    }

    private String getRemoveProductCode() {
        return getRemoveProduct() != null ? String.valueOf(getRemoveProduct().getProductCode()) : "";
    }

    private String getProductCodeList() {
        if (isCartEmpty)
            callback.enableNoRecordView(true);

        StringBuilder productCodeBuilder = new StringBuilder();
        if (webEngageTrackEvent.equals(WebEngageHelper.EVENT_NAME_REMOVED_FROM_CART))
            productCodeBuilder.append(getRemoveProductCode());

        if (!isCartEmpty && cartDetails != null && cartDetails.getLines() != null && cartDetails.getLines().size() > 0) {
            for (MStarProductDetails productDetails : cartDetails.getLines()) {
                if (productCodeBuilder.length() > 0)
                    productCodeBuilder.append(",").append(productDetails.getProductCode());
                else
                    productCodeBuilder.append(productDetails.getProductCode());
            }
        }
        return productCodeBuilder.length() > 0 ? productCodeBuilder.toString() : "";
    }

    public interface MStarCartViewModelCallback {

        void showLoader();

        void dismissLoader();

        void showErrorMessage(String message);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        boolean isNetworkConnected();

        void enableNoRecordView(boolean isNoRecordFound);

        void onClickProceedButton(boolean isNewCustomer);

        void onClickSearchButton();

        Context getContext();

        void updateCartProductAdapter(MStarCartAdapter adapter);

        void showAppliedPromocodeView(String promoCode, MStarCartDetails cartDetails);

        void onLaunchApplyPromocodeDialog(String appliedPromocode, List<PromoCodeList> promoCodeLists);

        void navigateToProductDetails(int productCode);

        void showPrimeMemberShipBanner(boolean isPrimeCustomer);

        void navigateToSignIn();

        void walletBalance(String balance, boolean isVisibleNMSCash);

        void showNNMSSuperCashView(boolean ishowNNMSSuperCashView);

        void nmsSuperCashDescription(PromoCodeList mstarSuperCash);

        void poromocodeDescription(List<PromoCodeList> promoCodeLists);

        void showAlternateSalt(int productCode);

        void setProductSuggestionView(MstarBrainSinConfigDetails configDetails, boolean isVisible);

        void setProductSuggestionAdapter(ArrayList<HRVProduct> hrvProductList);

        void showM2UpdatedWarning();

        void parentViewVisible(boolean isVisible);

        void subscriptionEditDialogue();

        void showMaxLimitBreachedMessage();

        void couponDescription(String couponDescription);

        void updateCartPaymentDetails(MStarCartDetails cartDetails);

        void productViewVisible(boolean isVisible);

        void showOutOfStockWarning(ArrayList<String> outOfStockProductNameList, Map<Integer, MStarProductDetails> outOfStockProductCodeMap);

        void disableProceedButton(boolean isOutOfStockProductInCart);

        void couponViewVisible(boolean isVisible);

        void updateCartSubstitutionView(boolean isEnableCartSubstitutionView, boolean isAlternateCartForward, BigDecimal alternateCartSaveAndOriginalCartAmount);

        void onLaunchOfCartSubstitution();

        void showConfirmationDialogForRevertToOriginalCart();
    }
}
