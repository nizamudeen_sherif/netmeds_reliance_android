package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.adpater.OrderAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.OrderListenerTypeEnum;
import com.nms.netmeds.base.OrderPayNowRetryReorderHelper;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MstarOrders;
import com.nms.netmeds.base.model.MstarStatusItem;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.model.PrimeOrderBannerConfig;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;


public class OrdersViewModel extends AppViewModel implements OrderAdapter.OrderAdapterListener, OrderPayNowRetryReorderHelper.OrderHelperCallback {

    private OrdersListener listener;
    private BasePreference basePreference;
    private OrderAdapter orderAdapter;
    private PrimeConfig primeConfig;
    private Application application;
    private MstarOrders selectedOrder;

    private final MutableLiveData<List<MstarOrders>> orderListMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> isPrimeUserLiveData = new MutableLiveData<>();

    private String orderFilterStatus;
    private String selectedOrderId;
    private int failedTransactionId = 0;
    private final long pageSize = 15;
    private int pageIndex = 1;
    private long totalOrdersCount = 0;
    private long totalPageCount = 0;
    private boolean loadMore = true;
    public boolean isPrimeMember = false;
    private boolean containsProduct = false;
    private boolean isFromHelper = false;
    private List<MstarStatusItem> orderStatusList;
    private MStarCustomerDetails mStarCustomerDetails;

    public PrimeConfig getPrimeConfig() {
        return primeConfig;
    }

    public void setPrimeConfig(PrimeConfig primeConfig) {
        this.primeConfig = primeConfig;
    }

    public OrdersViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public void init(BasePreference basePreference, String recentStatus, OrdersListener listener) {
        this.listener = listener;
        this.basePreference = basePreference;
        orderFilterStatus = recentStatus;
        isPrimeUserLiveData.setValue(false);
        listener.initiateSwipeToRefresh();
        isPrimeUser();
        PaymentHelper.setIsPrescriptionOrder(false);
        PaymentHelper.setRetroWalletApplied(false);
        PaymentHelper.setIsVoucherApplied(false);
    }


    public void fetchOrderListFromServer() {
        listener.setEmptyCartViewVisibility(View.GONE);
        listener.setOrderContainerVisibility(View.GONE);
        orderAdapter = null;
        loadMore = false;
        totalOrdersCount = 0;
        totalPageCount = 0;
        pageIndex = 1;
        listener.showLoader();
        startAPICommunication();
    }

    private void isPrimeUser() {
        initMstarApi(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    public void startAPICommunication() {
        orderAdapter = null;
        loadMore = false;
        totalOrdersCount = 0;
        totalPageCount = 0;
        pageIndex = 1;
        initMstarApi(APIServiceManager.C_MSTAR_ORDERS_HEADER);
    }

    private void initMstarApi(int transactionId) {
        boolean isConnected = listener.isNetworkConnected();
        listener.showNetworkErrorView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_ORDERS_HEADER:
                    APIServiceManager.getInstance().MstarGetOrderHeader(this, basePreference.getMstarBasicHeaderMap(), getMstarOrderHeaderRequest());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    listener.showLoader();
                    APIServiceManager.getInstance().mStarCustomerDetails(this, basePreference.getMstarBasicHeaderMap());
                    break;
            }
        } else {
            failedTransactionId = transactionId;
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_ORDERS_HEADER:
                initiateOrderHearList(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                checkPrimeUserResponse(data);
                break;
        }
        listener.hideLoader();
    }

    @Override
    public void onFailed(int transactionId, String data) {
        failedTransactionId = transactionId;
        listener.hideLoader();
        listener.enableShimmer(false);
        showApiError();
        listener.setrefreshing(false);
    }

    public void onPrimeDataAvailable(Boolean isPrimeMember) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        setPrimeConfig(configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getPrimeConfig() : new PrimeConfig());
        this.isPrimeMember = isPrimeMember;
        listener.setPrimeVisibility((isPrimeMember && CommonUtils.isExpiryDate(mStarCustomerDetails != null ? mStarCustomerDetails : new MStarCustomerDetails())) ? View.VISIBLE : View.GONE);
    }

    private MultipartBody getMstarOrderHeaderRequest() {
        return new MultipartBody.Builder().addFormDataPart(AppConstant.PAGE_INDEX, String.valueOf(pageIndex)).setType(MultipartBody.FORM)
                .addFormDataPart(AppConstant.PAGESIZE, String.valueOf(pageSize)).addFormDataPart(AppConstant.ORDER_STATUS_TEXT, orderFilterStatus).build();

    }

    private void checkPrimeUserResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getResult() != null && response.getResult().getCustomerDetails() != null) {
            mStarCustomerDetails = response.getResult().getCustomerDetails();
            isPrimeUserLiveData.setValue(response.getResult().getCustomerDetails().isPrime());

        }
    }

    @Override
    public void onRetryClickListener() {
        listener.showWebserviceErrorView(false);
        if (isFromHelper) {
            new OrderPayNowRetryReorderHelper(application).initApiCall(failedTransactionId);
        } else {
            initMstarApi(failedTransactionId);
        }
    }

    private void initiateOrderHearList(String data) {
        listener.setrefreshing(false);
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getStatus().equals(AppConstant.API_SUCCESS_STATUS)) {
            if (response.getResult() != null) {
                calculateTotalPageCount(response.getResult().getStatusList());
                orderListMutableLiveData.setValue(response.getResult().getOrdersList());
                loadMore = (response.getResult().getOrdersList() != null && response.getResult().getOrdersList().size() > 0) && response.getResult().getOrdersList().size() < totalOrdersCount;
                listener.enableEmptyView(false);
                listener.enableShimmer(false);
                listener.setOrderContainerVisibility(View.VISIBLE);
                listener.setFilterLayoutVisibility(View.VISIBLE);
                setOrderFilterOption(response);
            } else {
                if (AppConstant.RECENT.equalsIgnoreCase(orderFilterStatus))
                    listener.setFilterLayoutVisibility(View.GONE);
                listener.enableEmptyView(pageIndex == 1);
            }
        } else {
            listener.enableEmptyView(true);
        }
    }

    private void calculateTotalPageCount(List<MstarStatusItem> result) {

        if (result != null && result.size() > 0) {
            for (MstarStatusItem statusItem : result) {
                totalOrdersCount = totalOrdersCount + Integer.valueOf(statusItem.getCount());
            }
            totalPageCount = (long) Math.ceil(Double.valueOf(String.valueOf(totalOrdersCount)) / Double.valueOf(String.valueOf(pageSize)));
        } else {
            totalOrdersCount = 0;
            totalPageCount = 0;
        }
    }

    public void initiateOrderFromServer(List<MstarOrders> ordersList) {
        orderAdapter = listener.initiateOrderList(ordersList, orderAdapter);
    }

    public void applyFilter(String filterType) {
        orderFilterStatus = filterType;
        fetchOrderListFromServer();
    }

    public MutableLiveData<List<MstarOrders>> getMutableOrderListData() {
        return orderListMutableLiveData;
    }

    public MutableLiveData<Boolean> getIsPrimeUserLiveData() {
        return isPrimeUserLiveData;
    }

    public void filter() {
        listener.launchFilterOptionForOrder(orderFilterStatus, getOrderStatusList() != null ? getOrderStatusList() : new ArrayList<MstarStatusItem>());
    }

    public RecyclerView.OnScrollListener paginationForOrderList(
            final LinearLayoutManager layoutManager) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (loadMore && pageIndex < totalPageCount) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= pageSize) {
                        pageIndex += 1;
                        loadMore = false;
                        initMstarApi(APIServiceManager.C_MSTAR_ORDERS_HEADER);
                    }
                }
            }
        };
    }


    @Override
    public void onClickOfTrackAndViewOrder(int position, MstarOrders order, OrderListenerTypeEnum orderFromType) {
        resetFlags();
        this.selectedOrder = order;
        setSelectedOrderId(order.getOrderId());
        this.containsProduct = !order.getItemsList().isEmpty();
        if (orderFromType == OrderListenerTypeEnum.TRACK_ORDER) {
            listener.onLaunchTrackOrder(order.getOrderId(), order);
        } else if (orderFromType == OrderListenerTypeEnum.PAY) {
            M2Helper.getInstance().setPaynowinitated(true);
            doM2Payment();
        } else if (OrderListenerTypeEnum.M1_RETRY == orderFromType) {
            doM1Retry();
        } else if (OrderListenerTypeEnum.RE_ORDER == orderFromType) {
            /*Google Analytics Event*/
            GoogleAnalyticsHelper.getInstance().postEvent(getApplication(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_3, OrderListenerTypeEnum.RE_ORDER == orderFromType ? GoogleAnalyticsHelper.EVENT_ACTION_REORDER : GoogleAnalyticsHelper.EVENT_ACTION_RETRY, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER);
            if (order.getOrderId().endsWith(AppConstant.DRUG_TYPE_P)) {
                startOrder(containsProduct);
            } else {
                startM1ReOrder();
            }
        } else if (orderFromType == OrderListenerTypeEnum.M2_RE_ORDER) {
            startOrder(containsProduct);
        } else {
            listener.onLaunchOrderDetails(order);
        }
    }

    private void resetFlags() {
        PaymentHelper.setIsEditOrder(false);
        PaymentHelper.setIsPayAndEditM2Order(false);
        M2Helper.getInstance().setPayNow(false);
        M2Helper.getInstance().setRetry(false);
    }

    private void doM1Retry() {
        M2Helper.getInstance().setRetry(true);
        M2Helper.getInstance().setPayNow(false);
        new OrderPayNowRetryReorderHelper(application).initiate(this, selectedOrderId, OrderListenerTypeEnum.M1_RETRY, basePreference);
    }

    private void startOrder(boolean containsProduct) {
        if (containsProduct) {
            doM2ReOrder();
        } else {
            listener.launchM2UploadPrescription();
        }
    }

    private void doM2ReOrder() {
        M2Helper.getInstance().setRetry(false);
        M2Helper.getInstance().setPayNow(false);
        new OrderPayNowRetryReorderHelper(application).initiate(this, selectedOrderId, OrderListenerTypeEnum.M2_RE_ORDER, basePreference);
    }

    private void doM2Payment() {
        M2Helper.getInstance().setRetry(false);
        M2Helper.getInstance().setPayNow(true);
        new OrderPayNowRetryReorderHelper(application).initiate(this, selectedOrderId, OrderListenerTypeEnum.PAY, basePreference);
    }

    private void startM1ReOrder() {
        M2Helper.getInstance().setRetry(false);
        M2Helper.getInstance().setPayNow(false);
        new OrderPayNowRetryReorderHelper(application).initiate(this, selectedOrderId, OrderListenerTypeEnum.RE_ORDER, basePreference);
    }


    public void setSelectedOrderId(String selectedOrderId) {
        this.selectedOrderId = selectedOrderId;
    }

    public String getSelectedOrderId() {
        return selectedOrderId;
    }

    private void showApiError() {

        listener.hideLoader();
        if (orderAdapter != null) {
            if (orderAdapter.getItemCount() == 0) {
                listener.showWebserviceErrorView(true);
            }
        } else {
            listener.showWebserviceErrorView(true);
        }
    }

    private PrimeOrderBannerConfig orderBannerConfig() {
        return getPrimeConfig() != null && getPrimeConfig().getOrderBannerConfig() != null ? getPrimeConfig().getOrderBannerConfig() : new PrimeOrderBannerConfig();
    }

    public String primeBannerHeader() {
        return orderBannerConfig() != null && !TextUtils.isEmpty(orderBannerConfig().getHeaderText()) ? orderBannerConfig().getHeaderText() : "";
    }

    public String primeBannerDescription() {
        return orderBannerConfig() != null && !TextUtils.isEmpty(orderBannerConfig().getDescriptionText()) ? orderBannerConfig().getDescriptionText() : "";
    }

    public void onPrimeBannerClick() {
        listener.onNavigationPrime();
    }

    @Override
    public void navigateToAddAddress() {
        listener.navigateToAddAddress();
    }

    @Override
    public void redirectToPayment() {
        listener.redirectToPayment();
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, List<String> maxLimitBreachProducts, boolean isEditOrder, String orderId) {
        if (!maxLimitBreachProducts.isEmpty()) {
            for (String productCode : maxLimitBreachProducts) {
                RefillHelper.getMaxLimitBreachedProductsMap().put(productCode, "");
            }
        }
        listener.navigateToCartActivity(notAddedProducts, orderId);
    }

    @Override
    public void hideLoader() {
        listener.hideLoader();
    }

    @Override
    public boolean isNetworkConnected() {
        return listener.isNetworkConnected();
    }

    @Override
    public void showNoNetworkError(boolean isConnected) {
        listener.showNetworkErrorView(isConnected);
    }

    @Override
    public void showLoader() {
        listener.showLoader();
    }

    @Override
    public void showCancelOrderError(String s) {
        //No cancel button in OrderFragment
    }

    @Override
    public void onFailedFromHelper(int transactionId, String data, boolean isFromHelper) {
        this.isFromHelper = isFromHelper;
        failedTransactionId = transactionId;
    }

    @Override
    public void cancelOrderSuccessful(String message) {
        // Cancel Order in not Available in Orders Fragment
    }

    @Override
    public void navigateToSuccessPage(String data) {
        //No cod on OrdersFragment
    }

    @Override
    public void navigateToCodFailure(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct) {
        //No cod on OrdersFragment
    }

    @Override
    public void showMessage(String message) {
        listener.showSnackBar(message);
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        listener.navigateToCartForColdStorage(interCityProductList);
    }

    @Override
    public Context getContext() {
        return listener.getContext();
    }

    private List<MstarStatusItem> getOrderStatusList() {
        return orderStatusList;
    }

    private void setOrderStatusList(List<MstarStatusItem> orderStatusList) {
        this.orderStatusList = orderStatusList;
    }

    private void setOrderFilterOption(MStarBasicResponseTemplateModel response) {
        if (response.getResult().getStatusList() != null && response.getResult().getStatusList().size() > 0)
            setOrderStatusList(response.getResult().getStatusList());
    }

    public interface OrdersListener {

        void showLoader();

        void hideLoader();

        void onLaunchOrderDetails(MstarOrders order);

        void onLaunchTrackOrder(String orderId, MstarOrders order);

        void redirectToPayment();

        void navigateToCartActivity(Map<String, MStarAddedProductsResult> addedProductsResultMap, String orderId);

        void launchFilterOptionForOrder(String orderFilterStatus, List<MstarStatusItem> statusList);

        void launchM2UploadPrescription();

        void onNavigationPrime();

        void initiateSwipeToRefresh();

        boolean isNetworkConnected();

        void setOrderContainerVisibility(int visibility);

        void setFilterLayoutVisibility(int visibility);

        void setPrimeVisibility(int visibility);

        void enableEmptyView(boolean enable);

        void setEmptyCartViewVisibility(int visibility);

        void enableShimmer(boolean isShimmerEnable);

        void setrefreshing(boolean refresh);

        OrderAdapter initiateOrderList(List<MstarOrders> ordersList, OrderAdapter orderAdapter);

        void showNetworkErrorView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        void navigateToAddAddress();

        void navigateToCartForColdStorage(ArrayList<String> interCityProductList);

        Context getContext();

        void showSnackBar(String message);
    }
}
