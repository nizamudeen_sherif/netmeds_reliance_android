package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarReferAndEarn;
import com.nms.netmeds.base.model.ReferEarn;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

public class ReferEarnViewModel extends AppViewModel {

    private final MutableLiveData<MStarBasicResponseTemplateModel> referralCodeResponseMutableLiveData = new MutableLiveData<>();
    private ReferEarnListener referEarnListener;
    public String title;
    private String mail_content;
    private String mail_message;
    private String url;
    private MstarReferAndEarn referralDetails;
    private ReferEarn referEarnContent;
    private BasePreference basePreference;

    public ReferEarnViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getReferralCodeResponseMutableLiveData() {
        return referralCodeResponseMutableLiveData;
    }

    public void init(ReferEarnListener referEarnListener, BasePreference basePreference) {
        this.referEarnListener = referEarnListener;
        this.basePreference = basePreference;
        setContent();
        referEarn();
    }

    private void setContent() {
        if (!TextUtils.isEmpty(basePreference.getConfiguration())) {
            ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
            if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getReferEarn() != null)
                referEarnContent = configurationResponse.getResult().getConfigDetails().getReferEarn();
        }
    }

    public void onclickShare() {
        referEarnListener.shareIntent(title, mail_content, mail_message, url);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        referEarn();
        referEarnListener.showWebserviceErrorViewCallback(false);
    }

    private void referEarn() {
        initMstarApi(APIServiceManager.C_MSTAR_REFER_AND_EARN);
    }

    private void initMstarApi(int transactionId) {
        boolean isConnected = referEarnListener.isNetworkConnectCallback();
        referEarnListener.showNoNetworkErrorCallback(isConnected);
        if (isConnected && transactionId == APIServiceManager.C_MSTAR_REFER_AND_EARN) {
            referEarnListener.vmShowProgress();
            APIServiceManager.getInstance().MstarReferAndEarn(this, basePreference.getMstarBasicHeaderMap());
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        if (data != null) {
            referralCodeResponseMutableLiveData.setValue(new Gson().fromJson(data, MStarBasicResponseTemplateModel.class));
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        referEarnListener.showWebserviceErrorViewCallback(true);
        referEarnListener.vmDismissProgress();
        super.onFailed(transactionId, data);
    }

    public void onReferralAvailable(MStarBasicResponseTemplateModel response) {
        referEarnListener.vmDismissProgress();
        if (response != null && response.getStatus().equals("Success")) {
            MstarBasicResponseResultTemplateModel result = response.getResult();
            if (result != null) {
                referralDetails = result.getReferAndEarn();
                if (referralDetails != null) {
                    title = referralDetails.getReferralTitleContent() != null && !TextUtils.isEmpty(referralDetails.getReferralTitleContent()) ? referralDetails.getReferralTitleContent() : "";
                    mail_content = referralDetails.getReferralMailContent() != null && !TextUtils.isEmpty(referralDetails.getReferralMailContent()) ? referralDetails.getReferralMailContent() : "";
                    mail_message = referralDetails.getReferralMessageContent() != null && !TextUtils.isEmpty(referralDetails.getReferralMessageContent()) ? referralDetails.getReferralMessageContent() : "";
                    url = referralDetails.getReferralUrl() != null && !TextUtils.isEmpty(referralDetails.getReferralUrl()) ? referralDetails.getReferralUrl() : "";
                }
            }
        }
    }

    public String referralCode() {
        return referralDetails != null && !TextUtils.isEmpty(referralDetails.getReferralCode()) ? referralDetails.getReferralCode() : "";
    }

    public String inviteCount() {
        return referralDetails != null && referralDetails.getReferralText() != null && !TextUtils.isEmpty(referralDetails.getReferralText()) ? referralDetails.getReferralText() : "";
    }

    public String inviteTitle() {
        return referEarnContent != null && referEarnContent.getInvite() != null && !TextUtils.isEmpty(referEarnContent.getInvite().getTitle()) ? referEarnContent.getInvite().getTitle() : "";
    }

    public String inviteMessage() {
        return referEarnContent != null && referEarnContent.getInvite() != null && !TextUtils.isEmpty(referEarnContent.getInvite().getContent()) ? referEarnContent.getInvite().getContent() : "";
    }

    public String cashBackTitle() {
        return referEarnContent != null && referEarnContent.getCashBack() != null && !TextUtils.isEmpty(referEarnContent.getCashBack().getTitle()) ? referEarnContent.getCashBack().getTitle() : "";
    }

    public String cashBackMessage() {
        return referEarnContent != null && referEarnContent.getCashBack() != null && !TextUtils.isEmpty(referEarnContent.getCashBack().getContent()) ? referEarnContent.getCashBack().getContent() : "";
    }

    public String offerTitle() {
        return referEarnContent != null && referEarnContent.getOffer() != null && !TextUtils.isEmpty(referEarnContent.getOffer().getTitle()) ? referEarnContent.getOffer().getTitle() : "";
    }

    public String offerMessage() {
        return referEarnContent != null && referEarnContent.getOffer() != null && !TextUtils.isEmpty(referEarnContent.getOffer().getContent()) ? referEarnContent.getOffer().getContent() : "";
    }

    public void copyReferalCode() {
        referEarnListener.copyReferalCode();
    }

    public interface ReferEarnListener {

        void vmShowProgress();

        void vmDismissProgress();

        void shareIntent(String title, String mailContent, String message, String url);

        boolean isNetworkConnectCallback();

        void showNoNetworkErrorCallback(boolean isConnected);

        void showWebserviceErrorViewCallback(boolean isWebserviceError);

        void copyReferalCode();

    }
}
