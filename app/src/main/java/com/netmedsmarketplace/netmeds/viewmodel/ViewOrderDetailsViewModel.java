package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.model.BoomrangResponse;
import com.netmedsmarketplace.netmeds.model.BoomrangResult;
import com.netmedsmarketplace.netmeds.model.ViewOrderPaymentDetailsModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.OrderListenerTypeEnum;
import com.nms.netmeds.base.OrderPayNowRetryReorderHelper;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.M2DeliveryCharges;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarDrugDetail;
import com.nms.netmeds.base.model.MstarOrderDetail;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;
import com.nms.netmeds.base.model.MstarOrderShippingAddress;
import com.nms.netmeds.base.model.OrderAuthConfirm;
import com.nms.netmeds.base.model.OrderConfirmedPending;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.payment.ui.PaymentConstants;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;
import com.tune.TuneEventItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;


public class ViewOrderDetailsViewModel extends AppViewModel implements OrderPayNowRetryReorderHelper.OrderHelperCallback {

    private static ViewOrderDetailsListener listener;
    private BasePreference basePreference;
    private MstarOrderDetailsResult singleOrderResponse;
    private Application application;
    private ConfigurationResponse configurationResponse;

    private MutableLiveData<MstarOrderDetailsResult> mutableOrderDetailsLiveData = new MutableLiveData<>();
    private MutableLiveData<BoomrangResponse> mutableBoomDateLiveData = new MutableLiveData<>();

    private String orderId = "";
    private String boomRangDate;
    private String boomRangMessage;
    private String userEmailId;
    private int ratingCount;
    private boolean isPaymentInitiatedFromDeeplinking = false;
    private boolean isfromDeepLinking = false;
    private boolean isSubscriptionCancel = false;
    private Map<String, MStarProductDetails> productCodeWithPriceMap = new HashMap<>();


    public MutableLiveData<BoomrangResponse> getMutableBoomDateLiveData() {
        return mutableBoomDateLiveData;
    }

    public ViewOrderDetailsViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public void init(BasePreference basePreference, ViewOrderDetailsListener listener) {
        ViewOrderDetailsViewModel.listener = listener;
        this.basePreference = basePreference;
        configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        getUserEmailId();
        fetchOrderDetailsFromServer();
    }

    private void getUserEmailId() {
        MStarCustomerDetails customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
        this.userEmailId = customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "";
    }

    public String getPromisedDeliveryDate() {
        if (singleOrderResponse != null && (singleOrderResponse.getOrderStatusCode() != null && singleOrderResponse.getOrderStatusCode().equalsIgnoreCase(AppConstant.INTRANSIT))) {
            return boomRangDate;
        } else
            return (singleOrderResponse != null && !TextUtils.isEmpty(singleOrderResponse.getExpectedDeliveryDateString())) ? singleOrderResponse.getExpectedDeliveryDateString() : "";
    }

    public String getPromisedDescription() {
        return singleOrderResponse != null && singleOrderResponse.getOrderStatusCode() != null && singleOrderResponse.getOrderStatusCode().equalsIgnoreCase(AppConstant.INTRANSIT) ? boomRangMessage : listener.getStringValue(R.string.text_track_to_be_delivered);
    }

    public String getName() {
        return String.format("%s %s",
                getValidString(singleOrderResponse != null ? (singleOrderResponse.getOrderBillingInfo() != null ? (!TextUtils.isEmpty(singleOrderResponse.getOrderBillingInfo().getBillingFirstName()) ? singleOrderResponse.getOrderBillingInfo().getBillingFirstName() : "") : "") : ""),
                getValidString(singleOrderResponse != null ? (singleOrderResponse.getOrderBillingInfo() != null ? (!TextUtils.isEmpty(singleOrderResponse.getOrderBillingInfo().getBillingLastName()) ? singleOrderResponse.getOrderBillingInfo().getBillingLastName() : "") : "") : ""));
    }

    public String getOrderId() {
        return isfromDeepLinking ? orderId : (singleOrderResponse != null && !TextUtils.isEmpty(singleOrderResponse.getOrderId())) ? singleOrderResponse.getOrderId() : "";
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderPlacedDateAndTime() {
        return (singleOrderResponse != null && !TextUtils.isEmpty(singleOrderResponse.getOrderDate())) ? DateTimeUtils.getInstance().stringDate(singleOrderResponse.getOrderDate(), DateTimeUtils.yyyyMMddTHHmmss, DateTimeUtils.MMMddyyyyhhmma) : "";
    }

    public BigDecimal getTotalAmount() {
        return singleOrderResponse != null ? singleOrderResponse.getTotalAmount() : BigDecimal.ZERO;
    }

    public String getDeliveryAddress() {
        return singleOrderResponse != null ? (singleOrderResponse.getOrderShippingInfo() != null ? getFormatDeliveryAddress(singleOrderResponse.getOrderShippingInfo()) : "") : "";
    }

    public String getContactPersonName() {
        return String.format("%s %s",
                getValidString(singleOrderResponse != null ? (singleOrderResponse.getOrderShippingInfo() != null ? (!TextUtils.isEmpty(singleOrderResponse.getOrderShippingInfo().getShippingFirstName()) ? singleOrderResponse.getOrderShippingInfo().getShippingFirstName() : "") : "") : ""),
                getValidString(singleOrderResponse != null ? (singleOrderResponse.getOrderShippingInfo() != null ? (!TextUtils.isEmpty(singleOrderResponse.getOrderShippingInfo().getShippingLastName()) ? singleOrderResponse.getOrderShippingInfo().getShippingLastName() : "") : "") : ""));
    }

    public int getPrescriptionVisibility() {
        return singleOrderResponse != null && singleOrderResponse.getRxId() != null && singleOrderResponse.getRxId().size() != 0 ? View.VISIBLE : View.GONE;
    }

    public int getInfoTileVisibility() {
        return singleOrderResponse != null && (AppConstant.PAYMENT_PENDING.equals(singleOrderResponse.getOrderStatusCode()) || AppConstant.CANCELLED.equals(singleOrderResponse.getOrderStatusCode()) || (AppConstant.DECLINED.equals(singleOrderResponse.getOrderStatusCode()) || isPaymentPendingOrFailedAfterProcess())) ? View.VISIBLE : View.GONE;
    }

    public int getOnTrackDetailsVisibility() {
        return (singleOrderResponse == null
                || isReOrderEnableInsteadNeedHelp() ||
                AppConstant.CONSULTATION_SCHEDULED.equals(singleOrderResponse.getOrderStatusCode()) ||
                AppConstant.PAYMENT_PENDING.equals(singleOrderResponse.getOrderStatusCode())
                || isSubscriptionDeliveryPendingOrder() || isNetmedsFirstOrder() || isPaymentPendingOrFailedAfterProcess()) || TextUtils.isEmpty(getPromisedDeliveryDate()) || TextUtils.isEmpty(boomRangDate) ? View.GONE : View.VISIBLE;
    }


    public boolean isReviewPrescription() {
        return singleOrderResponse != null && AppConstant.PAYMENT_PENDING.equals(singleOrderResponse.getOrderStatusCode());
    }

    public boolean isEnablePayment() {
        return singleOrderResponse != null && AppConstant.PAYMENT_PENDING_DESC.equals(singleOrderResponse.getOrderStatus());
    }

    public boolean isOrderDelivered() {
        return singleOrderResponse != null && AppConstant.DELIVERED.equals(singleOrderResponse.getOrderStatusCode());
    }

    public String getInfoTitleText() {
        return singleOrderResponse != null ? AppConstant.CANCELLED.equals(singleOrderResponse.getOrderStatusCode()) ? listener.getStringValue(R.string.text_cancel_order_title_text) :
                AppConstant.DECLINED.equals(singleOrderResponse.getOrderStatusCode()) ? getDeclineStatusText() : singleOrderResponse.getOrderStatus() : "";
    }

    private String getDeclineStatusText() {
        return AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ? setOrderPendingTitle() : AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ? setOrderAuthTitle() : listener.getStringValue(R.string.text_declined_order_title_text);
    }

    public String getInfoDescriptionText() {
        return AppConstant.CONFIRMATION_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ? listener.getStringValue(R.string.text_confirmation_pending_status) : AppConstant.PAYMENT_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ? listener.getStringValue(R.string.text_payment_pending) : AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ? setOrderPendingDescription() : AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ? setOrderAuthDescription() : getPromisedDeliveryDate();
    }

    public boolean getInfoDescriptionVisibilityStatus() {
        return AppConstant.PAYMENT_PENDING.equals(singleOrderResponse.getOrderStatusCode()) || AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) || AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus());
    }

    public boolean isContainsProduct() {
        return singleOrderResponse != null && singleOrderResponse.getOrderdetail() != null && !singleOrderResponse.getOrderdetail().isEmpty() && singleOrderResponse.getOrderdetail().get(0) != null && singleOrderResponse.getOrderdetail().get(0).getDrugDetails() != null && !singleOrderResponse.getOrderdetail().get(0).getDrugDetails().isEmpty();
    }

    public MutableLiveData<MstarOrderDetailsResult> getViewOrderMutableData() {
        return mutableOrderDetailsLiveData;
    }


    public boolean isReOrderEnableInsteadNeedHelp() {
        return (isOrderDelivered() || (isNegativeOrderStatus() && !isPaymentPendingOrFailedAfterProcess())) && !isNetmedsFirstOrder();
    }

    public boolean isNegativeOrderStatus() {
        if (singleOrderResponse != null) {
            String statusCode = singleOrderResponse.getOrderStatusCode();
            return AppConstant.CANCELLED.equals(statusCode) ||
                    AppConstant.DECLINED.equals(statusCode) ||
                    AppConstant.RETURN.equals(statusCode);
        } else return false;
    }

    public boolean isSubscriptionDeliveryPendingOrder() {
        return singleOrderResponse != null && AppConstant.SUBSCRIPTION_DELIVERY_PENDING.equals(singleOrderResponse.getOrderStatusCode());
    }

    public void onShowDetailsClick() {
        listener.onCallbackOfShowDetails(singleOrderResponse.getOrderId(), 0);
    }

    public boolean cancelOrderVisibility() {
        return singleOrderResponse != null && (singleOrderResponse.isCancelStatus() || (isNegativeOrderStatus() && !isNetmedsFirstOrder()) || isOrderDelivered() || (singleOrderResponse.getOrderStatusCode() != null && !singleOrderResponse.getOrderStatusCode().equalsIgnoreCase(AppConstant.INTRANSIT) && isPaymentPendingOrFailedAfterProcess()));
    }

    public boolean needHelpBtn() {
        return isPaymentPendingFailed() && isNetmedsFirstOrder();
    }

    public boolean isPaymentPendingOrFailedAfterProcess() {
        return singleOrderResponse != null && (isPaymentPendingInProgress() || isPaymentPendingFailed());
    }

    public boolean isPaymentPendingInProgress() {
        return singleOrderResponse != null && AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus());
    }

    public boolean isPaymentPendingFailed() {
        return singleOrderResponse != null && AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus());
    }

    private void boomerang() {
        if (singleOrderResponse != null && singleOrderResponse.getOrderStatusCode() != null && singleOrderResponse.getOrderStatusCode().equalsIgnoreCase(AppConstant.INTRANSIT))
            initMstarApiCall(AppServiceManager.BOOMERANG);
    }

    public String getTextInsteadOfCancel() {
        int nameId;
        if (isOrderDelivered()) {
            nameId = R.string.text_invoice;
        } else if (isEnablePayment()) {
            nameId = R.string.text_cash_on_delivery;
        } else if (singleOrderResponse.isCancelStatus()) {
            nameId = R.string.text_cancel_order;
        } else {
            nameId = R.string.text_need_help;
        }
        return listener.getStringValue(nameId);
    }

    public int getTextColorForCancelSubscribe() {
        int colorId;
        if (isOrderDelivered()) {
            colorId = R.color.colorPrimary;
        } else if (isEnablePayment() && isEligibleForCod()) {
            colorId = R.color.colorPrimary;
        } else if (singleOrderResponse.isCancelStatus()) {
            colorId = R.color.colorLightBlueGrey;
        } else {
            colorId = R.color.colorPrimary;
        }
        return listener.getColors(colorId);
    }

    public String getTextInsteadOfNeedHelp() {
        int stringId = R.string.text_need_help;
        if (isReOrderEnableInsteadNeedHelp()) {
            stringId = R.string.text_re_order;
        } else if (isReviewPrescription() && isEnablePayment()) {
            stringId = R.string.text_pay_now;
        } else if (isPaymentPendingOrFailedAfterProcess()) {
            stringId = R.string.text_retry;
        }
        return listener.getStringValue(stringId);
    }

    public void onReviewNeedHelp() {
        if (isOrderDelivered()) {
            listener.onCallbackOfNeedHelpForOrder(singleOrderResponse.getOrderId());
        } else {
            listener.onCallbackOfCancelOrder(singleOrderResponse.getOrderId());
        }
    }

    public void onNeedHelpClick() {
        setRetryFlag();
        if (isReOrderEnableInsteadNeedHelp()) {
            onReOrder();
        } else if ((isReviewPrescription() && isEnablePayment())) {
            doM2Payment();
        } else if (isPaymentPendingOrFailedAfterProcess()) {
            doRetry();
        } else {
            listener.onCallbackOfNeedHelpForOrder(orderId);
        }
    }

    public void onCancelOrderClick() {
        if (singleOrderResponse != null && !TextUtils.isEmpty(singleOrderResponse.getOrderId())) {
            String orderId = singleOrderResponse.getOrderId();
            if (isOrderDelivered()) {
                downloadInvoice();
            } else if (isEnablePayment()) {
                if (isEligibleForCod()) {
                    listener.showCashOnDeliveryWarningDialog();
                } else {
                    listener.setError(listener.getStringValue(com.nms.netmeds.payment.R.string.text_cod_error));
                }
            } else if (singleOrderResponse.isCancelStatus()) {
                if (isSubscriptionCancel)
                    listener.subscriptionDialogue(orderId);
                else
                    listener.onCallbackOfCancelOrder(orderId);
            } else {
                listener.onCallbackOfNeedHelpForOrder(orderId);
            }
        }
    }

    public void downloadInvoice() {
        if (listener.isStoragePermissionAllowed()) {
            if (singleOrderResponse != null && singleOrderResponse.getOrderdetail() != null && !singleOrderResponse.getOrderdetail().isEmpty()) {
                for (MstarOrderDetail orderDetail : singleOrderResponse.getOrderdetail()) {
                    listener.downLoadInvoice(userEmailId, orderDetail.getOrderId());
                }
            }
        }
    }

    private void doRetry() {
        M2Helper.getInstance().setRetry(true);
        M2Helper.getInstance().setPayNow(false);
        new OrderPayNowRetryReorderHelper(application).initiate(this, getOrderId(), OrderListenerTypeEnum.M1_RETRY, basePreference);
    }


    private void doM2Payment() {
        M2Helper.getInstance().setPayNow(true);
        M2Helper.getInstance().setRetry(false);
        new OrderPayNowRetryReorderHelper(application).initiate(this, getOrderId(), OrderListenerTypeEnum.PAY, basePreference);
    }

    public void onReOrder() {
        M2Helper.getInstance().setRetry(false);
        M2Helper.getInstance().setPayNow(false);
        if (singleOrderResponse != null && singleOrderResponse.getOrderId().endsWith(AppConstant.DRUG_TYPE_P)) {
            if (isContainsProduct()) {
                new OrderPayNowRetryReorderHelper(application).initiate(this, singleOrderResponse.getOrderId(), OrderListenerTypeEnum.M2_RE_ORDER, basePreference);
            } else listener.navigateToM2Flow();
        } else if (singleOrderResponse != null) {
            new OrderPayNowRetryReorderHelper(application).initiate(this, singleOrderResponse.getOrderId(), OrderListenerTypeEnum.RE_ORDER, basePreference);
        }
    }

    public void initCOD() {
        new OrderPayNowRetryReorderHelper(application).initiate(this, getOrderId(), OrderListenerTypeEnum.PAY_WITH_COD, basePreference);
    }

    private void OnEditOrderClick() {
        RefillHelper.resetMapValues();
        new OrderPayNowRetryReorderHelper(application).initiate(this, getOrderId(), OrderListenerTypeEnum.EDIT_ORDER, basePreference);
    }

    private void setRetryFlag() {
        /*Google Analytics Event*/
        if (isPaymentPendingOrFailedAfterProcess())
            GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_RETRY, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER_DETAILS);
        PaymentHelper.setIsRetryClick(isPaymentPendingOrFailedAfterProcess());
        PaymentHelper.setIsFromReOrder(isReOrderEnableInsteadNeedHelp());
    }

    private void fetchOrderDetailsFromServer() {
        PaymentHelper.setRetroWalletApplied(false);
        PaymentHelper.setIsVoucherApplied(false);
        initMstarApiCall(APIServiceManager.C_MSTAR_MY_ORDER_DETAILS);
    }

    private void initMstarApiCall(int transactionId) {
        boolean isConnected = listener.isNetworkConnected();
        listener.showNoNetworkView(isConnected);
        if (isConnected) {
            listener.showLoader();
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_MY_ORDER_DETAILS:
                    listener.enableShimmer(true);
                    APIServiceManager.getInstance().MstarGetOrderDetails(this, basePreference.getMstarBasicHeaderMap(), getMyOrderRequest());
                    break;
                case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                    APIServiceManager.getInstance().mStarProductDetailsForIdList(this, getProductList(), APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                    break;
                case AppServiceManager.BOOMERANG:
                    AppServiceManager.getInstance().boomerang(this, orderId);
                    break;
            }
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        listener.dismissLoader();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_MY_ORDER_DETAILS:
                singOrderDetailsSuccessResponse(data);
                boomerang();
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                productListResponse(data);
                break;
            case AppServiceManager.BOOMERANG:
                BoomrangResponse boomrangResponse = new Gson().fromJson(data, BoomrangResponse.class);
                boomRangResponse(boomrangResponse);
                break;
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        switch (transactionId) {
            default:
                listener.dismissLoader();
                showApiError(transactionId);
                break;
        }
    }


    private MultipartBody getMyOrderRequest() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.ORDER_ID, orderId).build();
    }

    private String getProductList() {
        String productCodeListString = "";
        if (singleOrderResponse.getOrderdetail() != null && !singleOrderResponse.getOrderdetail().isEmpty()) {
            StringBuilder productCodeList = new StringBuilder();
            for (MstarOrderDetail orderDetail : singleOrderResponse.getOrderdetail()) {
                for (MstarDrugDetail drugDetail : orderDetail.getDrugDetails()) {
                    productCodeList.append(drugDetail.getDrugCode()).append(",");
                }
            }
            productCodeListString = productCodeList.toString();
        }
        //todo need to ask jeyaraj for this substring
        return productCodeListString.substring(0, productCodeListString.length() - 1);
    }

    @Override
    public void navigateToAddAddress() {
        listener.navigateToAddress();

    }

    @Override
    public void redirectToPayment() {
        listener.navigateToPayment();
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, List<String> maxLimitBreachProducts, boolean isEditOrder, String orderId) {
        if (!maxLimitBreachProducts.isEmpty() && singleOrderResponse != null && singleOrderResponse.getOrderdetail() != null && !singleOrderResponse.getOrderdetail().isEmpty() && singleOrderResponse.getOrderdetail().get(0) != null && singleOrderResponse.getOrderdetail().get(0).getDrugDetails() != null && !singleOrderResponse.getOrderdetail().get(0).getDrugDetails().isEmpty()) {
            for (String productCode : maxLimitBreachProducts) {
                for (MstarDrugDetail drugDetail : singleOrderResponse.getOrderdetail().get(0).getDrugDetails()) {
                    if (drugDetail != null && String.valueOf(drugDetail.getDrugCode()).equalsIgnoreCase(productCode)) {
                        RefillHelper.getMaxLimitBreachedProductsMap().put(productCode, drugDetail.getGenericName());
                    }
                }
            }
        }
        listener.navigateToCartActivity(notAddedProducts, isEditOrder, orderId);
    }

    @Override
    public void hideLoader() {
        listener.dismissLoader();
    }

    @Override
    public boolean isNetworkConnected() {
        return listener.isNetworkConnected();
    }

    @Override
    public void showNoNetworkError(boolean isConnected) {
        listener.showNoNetworkView(isConnected);
    }

    @Override
    public void showLoader() {
        listener.showLoader();
    }

    @Override
    public void showCancelOrderError(String message) {
        listener.showSnackBar(message);
    }

    @Override
    public void onFailedFromHelper(int transactionId, String data, boolean isFromHelper) {

    }

    @Override
    public void cancelOrderSuccessful(String message) {
        listener.cancelOrderSuccess(message);
    }

    @Override
    public void navigateToSuccessPage(String data) {
        M2Helper.getInstance().setPayNow(false);
        M2Helper.getInstance().setRetry(false);
        sendToEcommerceTracker();
        listener.navigateToSuccessPageAfterCOD(data);
    }

    @Override
    public void navigateToCodFailure(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct) {
        listener.navigateToCodFailurePage(totalAmount, cartId, orderId, isPrimeProduct);
    }

    @Override
    public void showMessage(String message) {
        listener.showSnackBar(message);
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        listener.navigateToCartForColdStorage(interCityProductList);
    }

    @Override
    public Context getContext() {
        return listener.getContext();
    }

    @Override
    public void onRetryClickListener() {
        listener.showWebserviceErrorView(false);
        fetchOrderDetailsFromServer();
    }

    private void singOrderDetailsSuccessResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getStatus().equals(AppConstant.API_SUCCESS_STATUS)) {
            isSubscriptionCancel = response.getResult() != null && response.getResult().getOrderDetail() != null && response.getResult().getOrderDetail().isSubscriptionSubscribed();
            if (response.getResult() != null && response.getResult().getOrderDetail() != null) {
                PaymentHelper.setMstarOrderDetailsResult(response.getResult().getOrderDetail());
            }
            if (response.getResult() != null) {
                listener.enableShimmer(false);
                mutableOrderDetailsLiveData.setValue(response.getResult().getOrderDetail());
                checkIsFromDeepLinking();
            } else {
                if (isFromDeepinking()) {
                    listener.onEmptyOrderCallBackFromDeeplinking();
                    return;
                }
                listener.showEmptyView(true);
            }
        } else {
            listener.showSnackBar(response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng()) ? response.getReason().getReason_eng() : "");
        }
    }

    private void productListResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null && !response.getResult().getProductDetailsList().isEmpty() && response.getResult().getProductDetailsList().size() > 0) {
                BigDecimal totalAmount = BigDecimal.valueOf(0.0);
                for (int i = 0; i < response.getResult().getProductDetailsList().size(); i++) {
                    MStarProductDetails productDetails = response.getResult().getProductDetailsList().get(i);
                    productCodeWithPriceMap.put(String.valueOf(productDetails.getProductCode()), productDetails);
                }
                for (MstarOrderDetail orderDetail : singleOrderResponse.getOrderdetail()) {
                    for (MstarDrugDetail drugDetail : orderDetail.getDrugDetails()) {
                        if (productCodeWithPriceMap.containsKey(String.valueOf(drugDetail.getDrugCode())))
                            totalAmount = totalAmount.add(productCodeWithPriceMap.get(String.valueOf(drugDetail.getDrugCode())).getSellingPrice().multiply(BigDecimal.valueOf(drugDetail.getPurchaseQuantity())));
                    }
                }
                //todo load total amount in amount view.
                // listener.totalAmount(totalAmount);
            }
        }
        inflateOrderList();
    }

    private void checkIsFromDeepLinking() {
        if (isPaymentInitiatedFromDeeplinking) {
            //Iniitate Paynow from Deeplinking.
            initiatePaymentFromDeeplinking(false);
            doM2Payment();
        }

    }

    public void loadOrderDetailsInPage(MstarOrderDetailsResult response) {
        singleOrderResponse = response;
        if (isPaymentPending() && singleOrderResponse.getOrderdetail() != null && !singleOrderResponse.getOrderdetail().isEmpty()) {
            initMstarApiCall(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
        } else {
            inflateOrderList();
        }
        listener.loadPrescription(singleOrderResponse);
        listener.loadOrderPaymentDetails(singleOrderResponse);
        listener.loadCancellationAndRefundOrderDetails(singleOrderResponse, isPaymentPending());
    }

    private void inflateOrderList() {
        if (singleOrderResponse != null && singleOrderResponse.getOrderdetail() != null) {
            int totalOrderCount = singleOrderResponse.getOrderdetail().size();
            for (int i = 0; i < totalOrderCount; i++) {
                MstarOrderDetail orderDetail = singleOrderResponse.getOrderdetail().get(i);
                boolean isM2Order = AppConstant.CONSULTATION_SCHEDULED_DESC.trim().toLowerCase().equals(orderDetail.getDrugStatus().trim().toLowerCase()) || AppConstant.PAYMENT_PENDING_DESC.trim().toLowerCase().equals(orderDetail.getDrugStatus().trim().toLowerCase());
                LayoutInflater inflater = LayoutInflater.from(listener.getContext());
                LinearLayout container = listener.getContainer();
                @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_split_order_list, null);
                TextView tvOrderCount = view.findViewById(R.id.tv_split_order_count);
                RecyclerView rvOrderList = view.findViewById(R.id.rv_order_item_list);
                TableRow trShowOrderDetails = view.findViewById(R.id.tr_show_ordeR_details_row);
                TextView tvBottomTitleText = view.findViewById(R.id.tv_bottom_text);
                tvOrderCount.setText((totalOrderCount == 1) ? listener.getStringValue(R.string.text_orders) : String.format(listener.getStringValue(R.string.text_orders_count), i + 1, totalOrderCount));
                tvBottomTitleText.setText(listener.getStringValue(isReviewPrescription() ? R.string.text_review_medicine : R.string.text_show_order_status));
                tvBottomTitleText.setOnClickListener(isReviewPrescription() ? setOnEditOrderClickListner() : setOnShowDetailsListner());
                trShowOrderDetails.setVisibility(isM2Order || isNetmedsFirstOrder() || isPaymentPendingOrFailedAfterProcess() ? View.GONE : View.VISIBLE);
                trShowOrderDetails.setOnClickListener(onClickOfShowOrderDetails(orderDetail, i));
                listener.setAdapterForOrderList(rvOrderList, orderDetail, productCodeWithPriceMap, isM2Order, isPaymentPending());
                container.addView(view);
            }
        }
    }

    public boolean isPaymentPending() {
        return singleOrderResponse != null && AppConstant.PAYMENT_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus());
    }

    private View.OnClickListener setOnEditOrderClickListner() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnEditOrderClick();
            }
        };
    }


    private View.OnClickListener setOnShowDetailsListner() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTrackOrderStatus(singleOrderResponse.getOrderId());
            }
        };
    }

    public boolean isNetmedsFirstOrder() {
        String orderId = getOrderId();
        if (!TextUtils.isEmpty(orderId)) {
            if (orderId.contains(AppConstant.RETRUN_ORDER))
                return false;
            else
                return (AppConstant.NETMEDS_FIRST_ORDER_ID_S.equalsIgnoreCase(orderId.substring(orderId.length() - 1)));
        }
        return false;
    }


    public List<ViewOrderPaymentDetailsModel> getPaymentDetails() {
        List<ViewOrderPaymentDetailsModel> paymentDetailsList = new ArrayList<>();
        ViewOrderPaymentDetailsModel model;
        if (singleOrderResponse != null && singleOrderResponse.getProductAmount().compareTo(BigDecimal.ZERO) == 1) {
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(listener.getStringValue(R.string.text_sub_total));
            model.setValue(CommonUtils.getPriceInFormat(!isPaymentPending() ? singleOrderResponse.getProductAmount().add(singleOrderResponse.getCouponDiscount()) : getSubTotal()));
            paymentDetailsList.add(model);
        }
        model = new ViewOrderPaymentDetailsModel();
        model.setLabel(listener.getStringValue(R.string.text_shipping_charges));
        if (!TextUtils.isEmpty(getOrderId()) && String.valueOf(getOrderId().charAt(getOrderId().length() - 1)).equalsIgnoreCase("P") &&
                (AppConstant.CONSULTATION_SCHEDULED_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ||
                        AppConstant.CONFIRMATION_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()) ||
                        AppConstant.PAYMENT_PENDING_DESC.equalsIgnoreCase(singleOrderResponse.getOrderStatus()))) {
            if (singleOrderResponse.getOrderId().contains(AppConstant.KP)) {
                model.setDeliveryChargeDescriptionVisibility(false);
                String deliveryCharges = configurationResponse != null ? configurationResponse.getResult() != null ? configurationResponse.getResult().getConfigDetails() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges()) ?
                        CommonUtils.getAmountWithTwoDecimalFormat(Double.parseDouble(configurationResponse.getResult() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges()) ? configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges() : "")) : "" : "" : "" : "";
                model.setStrikeValue(CommonUtils.getPriceInFormat(deliveryCharges));
                model.setStrikeValueVisible(BigDecimal.ZERO.compareTo(singleOrderResponse.getShippingAmount()) > 0);
                model.setValue(singleOrderResponse != null ?
                        CommonUtils.getPriceInFormat(singleOrderResponse.getShippingAmount()) : listener.getStringValue(R.string.text_mrp));
            } else {
                model.setDeliveryChargeDescriptionVisibility(true);
                model.setValue(getM2DeliveryCharges(singleOrderResponse.getProductAmount()) != 0 ?
                        CommonUtils.getPriceInFormat(getM2DeliveryCharges(singleOrderResponse.getProductAmount())) : listener.getStringValue(R.string.text_mrp));
            }


        } else {
            model.setValue(CommonUtils.getPriceInFormat(singleOrderResponse != null ? singleOrderResponse.getShippingAmount() : BigDecimal.ZERO));
            ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
            String deliveryCharges = configurationResponse != null ? configurationResponse.getResult() != null ? configurationResponse.getResult().getConfigDetails() != null ? !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges()) ?
                    CommonUtils.getAmountWithTwoDecimalFormat(Double.parseDouble(configurationResponse.getResult() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges()) ? configurationResponse.getResult().getConfigDetails().getDefaultDeliveryCharges() : "0")) : "0" : "0" : "0" : "0";

            model.setStrikeValue(CommonUtils.getPriceInFormat(deliveryCharges));
            model.setStrikeValueVisible(singleOrderResponse.getShippingAmount() == BigDecimal.ZERO);
        }
        paymentDetailsList.add(model);

        if (singleOrderResponse != null && singleOrderResponse.getDiscountAmount().compareTo(BigDecimal.ZERO) > 0) {
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(listener.getStringValue(R.string.text_additional_discount));
            model.setValue("- " + CommonUtils.getPriceInFormat(singleOrderResponse.getDiscountAmount()));
            paymentDetailsList.add(model);
        }

        if (singleOrderResponse != null && singleOrderResponse.getCouponDiscount().compareTo(BigDecimal.ZERO) > 0) {
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(listener.getStringValue(R.string.text_coupon_discount));
            model.setValue("- " + CommonUtils.getPriceInFormat(singleOrderResponse.getCouponDiscount()));
            paymentDetailsList.add(model);
        }

        if (singleOrderResponse != null && singleOrderResponse.getUsedWalletAmount().compareTo(BigDecimal.ZERO) > 0) {
            PaymentHelper.setIsWalletApplied(true);
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(listener.getStringValue(R.string.text_wallet));
            model.setValue("- " + CommonUtils.getPriceInFormat(singleOrderResponse.getUsedWalletAmount()));
            paymentDetailsList.add(model);
        }
        if (singleOrderResponse != null && singleOrderResponse.getVoucherAmount().compareTo(BigDecimal.ZERO) > 0) {
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(listener.getStringValue(R.string.text_voucher_discount));
            model.setValue("- " + CommonUtils.getPriceInFormat(singleOrderResponse.getVoucherAmount()));
            paymentDetailsList.add(model);
        }
        if (singleOrderResponse != null) {
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(isReviewPrescription() && isEnablePayment() ? listener.getStringValue(R.string.text_net_payable) : listener.getStringValue(R.string.text_net_amount));
            model.setValue(CommonUtils.getPriceInFormat(singleOrderResponse.getTransactionAmount()));
            paymentDetailsList.add(model);
        }
        if (singleOrderResponse != null && !TextUtils.isEmpty(singleOrderResponse.getPaymentMode())) {
            model = new ViewOrderPaymentDetailsModel();
            model.setLabel(listener.getStringValue(R.string.text_payment_mode));
            model.setValue(singleOrderResponse.getPaymentMode());
            paymentDetailsList.add(model);
        }
        for (int i = 0; i < paymentDetailsList.size(); i++) {
            paymentDetailsList.get(i).setBackground(listener.getColors(i % 2 == 0 ? R.color.colorPrimary : R.color.colorPaleGrey));
        }
        return paymentDetailsList;
    }

    private BigDecimal getSubTotal() {
        BigDecimal subtotal = BigDecimal.ZERO;
        if (singleOrderResponse != null && singleOrderResponse.getOrderdetail() != null && singleOrderResponse.getOrderdetail().size() > 0 && singleOrderResponse.getOrderdetail().get(0) != null && singleOrderResponse.getOrderdetail().get(0).getDrugDetails() != null && singleOrderResponse.getOrderdetail().get(0).getDrugDetails().size() > 0) {
            for (MstarDrugDetail drugDetail : singleOrderResponse.getOrderdetail().get(0).getDrugDetails()) {
                subtotal = subtotal.add(drugDetail.getPurchasePrice().multiply(BigDecimal.valueOf(drugDetail.getPurchaseQuantity())));
            }
        }
        return subtotal;
    }

    private double getM2DeliveryCharges(BigDecimal totalAmount) {
        double deliveryCharges = 0;
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        if (totalAmount.compareTo(BigDecimal.ZERO) == 1 && response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getM2DeliveryCharges() != null) {
            M2DeliveryCharges m2DeliveryCharges = response.getResult().getConfigDetails().getM2DeliveryCharges();
            if (totalAmount.compareTo(BigDecimal.valueOf(m2DeliveryCharges.getFreeShipEligibilityValue())) >= 0) {
                deliveryCharges = 0;
            } else if (totalAmount.compareTo(BigDecimal.valueOf(m2DeliveryCharges.getCutOffValue())) < 0)
                deliveryCharges = m2DeliveryCharges.getMaxShippingCharges();
            else if (totalAmount.compareTo(BigDecimal.valueOf(m2DeliveryCharges.getCutOffValue())) >= 0)
                deliveryCharges = m2DeliveryCharges.getMinShippingCharges();
        }
        return deliveryCharges;
    }


    private View.OnClickListener onClickOfShowOrderDetails(final MstarOrderDetail orderDetail, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isReviewPrescription()) {
                    /*Google Analytics Event*/
                    PaymentHelper.setIsIncompleteOrder(true);
                    PaymentHelper.setInCompleteOrderId(getOrderId());
                    GoogleAnalyticsHelper.getInstance().postEvent(listener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_EDIT_ORDER, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER_DETAILS);
                } else {
                    listener.onCallbackOfShowDetails(singleOrderResponse.getOrderId(), position);
                }
            }
        };
    }

    public String getValidString(String str) {
        return !TextUtils.isEmpty(str) ? str.substring(0, 1).toUpperCase() + str.substring(1) : "";
    }

    private String getFormatDeliveryAddress(MstarOrderShippingAddress orderShippingInfo) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(orderShippingInfo.getShippingAddress1()).append(",");
        if (!TextUtils.isEmpty(orderShippingInfo.getShippingAddress2())) {
            stringBuilder.append("\n");
            stringBuilder.append(orderShippingInfo.getShippingAddress2()).append(",");
        }
        stringBuilder.append("\n");
        stringBuilder.append(orderShippingInfo.getShippingCity()).append(" - ").append(orderShippingInfo.getShippingZipCode()).append(".");
        stringBuilder.append("\n");
        stringBuilder.append(String.format(listener.getStringValue(R.string.text_state_code), orderShippingInfo.getShippingPhoneNumber()));
        return stringBuilder.toString();
    }

    private void showApiError(int transactionId) {
        listener.showWebserviceErrorView(true);
    }

    public void initiatePaymentFromDeeplinking(boolean isPaymentInitiatedFromDeeplinking) {
        this.isPaymentInitiatedFromDeeplinking = isPaymentInitiatedFromDeeplinking;
    }

    public boolean isFromDeepinking() {
        return isfromDeepLinking;
    }

    public void setFromDeepinking(boolean fromDeepinking) {
        this.isfromDeepLinking = fromDeepinking;
    }

    public boolean isEligibleForCod() {
        return isOrderAmountAboveMinimumPrice()
                && isOrderAmountNotExceedMaxPrice();
    }

    private boolean isOrderAmountAboveMinimumPrice() {
        return singleOrderResponse.getTransactionAmount().compareTo(BigDecimal.ZERO) > 0 && !TextUtils.isEmpty(PaymentUtils.codCredential(PaymentConstants.COD_MINIMUM_AMOUNT, basePreference.getPaymentCredentials())) && singleOrderResponse.getTransactionAmount().doubleValue() > Double.valueOf(PaymentUtils.codCredential(PaymentConstants.COD_MINIMUM_AMOUNT, basePreference.getPaymentCredentials()));
    }

    private boolean isOrderAmountNotExceedMaxPrice() {
        return !TextUtils.isEmpty(PaymentUtils.codCredential(PaymentConstants.COD_MAXIMUM_AMOUNT, basePreference.getPaymentCredentials())) && singleOrderResponse.getTransactionAmount().doubleValue() < Double.valueOf(PaymentUtils.codCredential(PaymentConstants.COD_MAXIMUM_AMOUNT, basePreference.getPaymentCredentials())) && !PaymentHelper.isNMSCashApplied();
    }

    private boolean checkPinCodeStatus() {
        return PaymentHelper.getPinCodeStatus();
    }


    private void sendToEcommerceTracker() {
        boolean non_prescription = false;
        boolean prescription = false;

        ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                .setTransactionId(orderId)
                .setTransactionAffiliation(GoogleAnalyticsHelper.TRANSACTION_AFFILIATION)
                .setTransactionRevenue(singleOrderResponse.getTotalAmount().doubleValue())
                .setTransactionTax(0)
                .setTransactionShipping(singleOrderResponse.getShippingAmount().doubleValue());

        if (PaymentHelper.getCartLineItems() != null && PaymentHelper.getCartLineItems().size() > 0) {
            List<TuneEventItem> matItemList = new ArrayList<>();
            for (MStarProductDetails itemResult : PaymentHelper.getCartLineItems()) {

                Product product = new Product()
                        .setId(String.valueOf(itemResult.getProductCode()))
                        .setName(!TextUtils.isEmpty(itemResult.getDisplayName()) ? itemResult.getDisplayName() : "")
                        .setCategory(!TextUtils.isEmpty(itemResult.getProductType()) ? itemResult.getProductType() : "")
                        .setPrice(itemResult.getSellingPrice().doubleValue())
                        .setQuantity(itemResult.getCartQuantity());

                GoogleAnalyticsHelper.getInstance().postTransactionEvent(listener.getContext(), new HitBuilders.ScreenViewBuilder()
                        .addProduct(product)
                        .setProductAction(productAction));

                matItemList.add(new TuneEventItem(!TextUtils.isEmpty(itemResult.getDisplayName()) ? itemResult.getDisplayName() : "") // Required
                        .withQuantity(itemResult.getCartQuantity()) // Rest are optional
                        .withUnitPrice(itemResult.getMrp().doubleValue())
                        .withRevenue(itemResult.getSellingPrice().doubleValue())
                        .withAttribute1(String.valueOf(itemResult.getProductCode())));

                if (!TextUtils.isEmpty(itemResult.getProductType())) {
                    switch (itemResult.getProductType()) {
                        case AppConstant.PRESCRIPTION_STATUS_PENDING:
                            prescription = true;
                            break;
                        case AppConstant.OTC_ORDER:
                            non_prescription = true;
                            break;
                    }
                }
            }
            String orderType = (prescription && non_prescription) ? MATHelper.MIXED_ORDER : (prescription ? MATHelper.PRESCRIPTION : (non_prescription ? MATHelper.NON_PRESCRIPTION : ""));
            MATHelper.getInstance().purchaseMATEvent(basePreference, matItemList, singleOrderResponse.getTotalAmount().doubleValue(), orderId, listener.getStringValue(R.string.txt_first_order_no), orderType, "");
        }
    }

    private String getWebEngagePaymentMethod() {
        StringBuilder paymentMethodBuilder = new StringBuilder();
        paymentMethodBuilder.append(PaymentConstants.CASH_ON_DELIVERY);

        String webEngagePaymentMethod = "";
        if (PaymentHelper.isVoucherApplied() && PaymentHelper.isNMSCashApplied()) {
            webEngagePaymentMethod = WebEngageHelper.E_VOUCHER + " " + WebEngageHelper.AND + " " + WebEngageHelper.NMS_WALLET;
        } else if (PaymentHelper.isVoucherApplied()) {
            webEngagePaymentMethod = WebEngageHelper.E_VOUCHER;
        } else if (PaymentHelper.isNMSCashApplied() || PaymentHelper.isNMSSuperCashApplied()) {
            webEngagePaymentMethod = WebEngageHelper.NMS_WALLET;
        }

        if (!TextUtils.isEmpty(webEngagePaymentMethod) && webEngagePaymentMethod.length() > 0)
            paymentMethodBuilder.append(" ").append(WebEngageHelper.AND).append(" ").append(webEngagePaymentMethod);
        else
            paymentMethodBuilder.append(webEngagePaymentMethod);

        return paymentMethodBuilder.toString();
    }

    public void boomRangResponse(BoomrangResponse boomrangResponse) {
        if (boomrangResponse != null) {
            listener.dismissLoader();
            BoomrangResult result = boomrangResponse.getResult();
            boomRangDate = result != null && !TextUtils.isEmpty(result.getDate()) ? result.getDate() : "";
            boomRangMessage = result != null && !TextUtils.isEmpty(result.getMessage()) ? result.getMessage() : "";
            PaymentHelper.setBoomDate(boomRangDate);
        }
    }

    private OrderConfirmedPending getOrderConfirmedPending() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getOrderConfirmedPending() != null ? configurationResponse.getResult().getConfigDetails().getOrderConfirmedPending() : new OrderConfirmedPending();
    }

    private String setOrderPendingDescription() {
        return getOrderConfirmedPending() != null && !TextUtils.isEmpty(getOrderConfirmedPending().getDescription()) ? getOrderConfirmedPending().getDescription() : "";
    }

    private OrderAuthConfirm getOrderAuthConfirm() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getOrderAuthConfirm() != null ? configurationResponse.getResult().getConfigDetails().getOrderAuthConfirm() : new OrderAuthConfirm();
    }

    private String setOrderAuthDescription() {
        return getOrderAuthConfirm() != null && !TextUtils.isEmpty(getOrderAuthConfirm().getDescription()) ? getOrderAuthConfirm().getDescription() : "";
    }

    private String setOrderPendingTitle() {
        return getOrderConfirmedPending() != null && !TextUtils.isEmpty(getOrderConfirmedPending().getTitle()) ? getOrderConfirmedPending().getTitle() : "";
    }

    private String setOrderAuthTitle() {
        return getOrderAuthConfirm() != null && !TextUtils.isEmpty(getOrderAuthConfirm().getTitle()) ? getOrderAuthConfirm().getTitle() : "";
    }

    public boolean isKiviOrder() {
        return singleOrderResponse.getOrderId().contains(AppConstant.KP);
    }

    public void cancelOrder(String cancelMessage, int cancelMessageIndex) {
        new OrderPayNowRetryReorderHelper(application).cancelOrder(this, orderId, OrderListenerTypeEnum.CANCEL_ORDER, basePreference, cancelMessageIndex, cancelMessage);
    }


    public interface ViewOrderDetailsListener {

        void showLoader();

        void dismissLoader();

        void onCallbackOfShowDetails(String orderId, int position);

        void onCallbackOfCancelOrder(String orderId);

        void onCallbackOfNeedHelpForOrder(String orderId);

        void showSnackBar(String message);

        void navigateToCartActivity(Map<String, MStarAddedProductsResult> notAddedProducts, boolean isEditOrder, String orderId);

        void navigateToM2Flow();

        void setError(String errorMessage);

        void showCashOnDeliveryWarningDialog();

        void onEmptyOrderCallBackFromDeeplinking();

        void navigateToPayment();

        String getStringValue(int stringId);

        boolean isNetworkConnected();


        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        LinearLayout getContainer();

        void enableShimmer(boolean isEnable);

        void setAdapterForOrderList(RecyclerView rvOrderList, MstarOrderDetail orderDetail, Map<String, MStarProductDetails> productCodeWithPriceMap, boolean isM2Order, boolean paymentPending);

        void showEmptyView(boolean isEmpty);

        void loadOrderPaymentDetails(MstarOrderDetailsResult singleOrderResponse);

        void loadCancellationAndRefundOrderDetails(MstarOrderDetailsResult singleOrderResponse, boolean paymentPending);

        void loadPrescription(MstarOrderDetailsResult singleOrderResponse);

        void navigateToAddress();

        int getColors(int colorId);

        Context getContext();

        void cancelOrderSuccess(String message);

        void navigateToSuccessPageAfterCOD(String data);

        void navigateToCodFailurePage(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct);

        void onTrackOrderStatus(String orderId);

        void downLoadInvoice(String userEmailId, String orderId);

        void navigateToCartForColdStorage(ArrayList<String> interCityProductList);

        boolean isStoragePermissionAllowed();

        void subscriptionDialogue(String orderId);

    }

    @SuppressLint("StaticFieldLeak")
    public static class DownloadInvoice extends AsyncTask<String, Void, String> {
        String Filepath;
        String URL;
        String orderId;
        Context context;

        public DownloadInvoice(String URL, String orderId, Context context) {
            this.URL = URL;
            this.orderId = orderId;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(URL));
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                request.setAllowedOverRoaming(false);
                request.setTitle(context.getResources().getString(R.string.text_netmeds));
                request.setDescription("Invoice.");
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, orderId + ".pdf");
                downloadManager.enqueue(request);
                listener.showSnackBar(context.getString(R.string.text_downloading));
            } catch (Exception e) {
                e.printStackTrace();
                listener.dismissLoader();
            }
            return Filepath;
        }

        @Override
        protected void onPostExecute(String result) {
            listener.dismissLoader();
            listener.showSnackBar(context.getString(R.string.text_download_successful));

        }

        @Override
        protected void onPreExecute() {
            listener.showLoader();
        }
    }

}
