package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.adpater.SearchAdapter;
import com.netmedsmarketplace.netmeds.db.DatabaseClient;
import com.netmedsmarketplace.netmeds.db.SearchHistory;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.VisibleItemListener;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.model.MstarRequestPopupResponse;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.MultipartBody;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SearchViewModel extends AppViewModel {

    private SearchAdapter.SearchAdapterListener adapterListener;
    private BasePreference basePreference;
    private SearchAdapter searchAdapter;
    private SearchListener searchListener;

    public int pageIndex = 0;
    private int skuId = 0;
    public long totalPageCount = 0;

    public boolean isAlgoliaSearching;
    private boolean isPagenationCall = false;
    public boolean isFromGeneric = false;
    private boolean isGenericSubsAvailable = true;

    private String medicineName = "";
    private String phoneNumber = "";
    private String patientName = "";
    private static VisibleItemListener visibleItemListener;
    private final MutableLiveData<MstarAlgoliaResponse> mstarAlgoliaMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MStarProductDetails> productDetailResponseMutableLiveData = new MutableLiveData<>();

    public SearchViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void onRetryClickListener() {
        if (TextUtils.isEmpty(searchListener.getSearchKeyWord()))
            getHistoryList();
        else
            searchApiCall(false);
        searchListener.vmWebserviceErrorView(false);
    }

    public MutableLiveData<MStarProductDetails> getProductDetailResponseMutableLiveData() {
        return productDetailResponseMutableLiveData;
    }

    public MutableLiveData<MstarAlgoliaResponse> getMstarAlgoliaMutableLiveData() {
        return mstarAlgoliaMutableLiveData;
    }

    public void init(SearchAdapter.SearchAdapterListener adapterListener, SearchListener searchListener, BasePreference basePreference) {
        this.adapterListener = adapterListener;
        this.searchListener = searchListener;
        this.basePreference = basePreference;
        getHistoryList();
        searchListener.initEditorListener();
    }

    /**
     * This method used to make API service calls
     *
     * @param transactionId service call unique id
     */
    private void initApiCall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(getApplicationContext());//check network connected or not
        searchListener.vmNoNetworkView();
        if (isConnected) {
            switch (transactionId) {
                case AppServiceManager.ALGOLIA_SEARCH:
                    AppServiceManager.getInstance().algoliaSearch(SearchViewModel.this, searchListener.getSearchKeyWord(), pageIndex, (TextUtils.isEmpty(CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_HITS)) ? AppConstant.DEFAULT_ALGOLIA_HITS : Integer.valueOf(CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_HITS))), CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_API_KEY), CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_INDEX), CommonUtils.getAlgoliaCredentials(basePreference, AppConstant.ALGOLIA_APP_ID), true, null, isFromGeneric ? getFilterQueryGenericSubsAvailable() : "");
                    break;
                case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                    APIServiceManager.getInstance().mStarProductDetails(this, skuId, APIServiceManager.MSTAR_PRODUCT_DETAILS);
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case AppServiceManager.ALGOLIA_SEARCH:
                isAlgoliaSearching = false;
                MstarAlgoliaResponse mstarAlgoliaResponse = new Gson().fromJson(data, MstarAlgoliaResponse.class);
                mstarAlgoliaMutableLiveData.setValue(mstarAlgoliaResponse);
                break;
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                productDetailsResponse(data);
                break;
            case APIServiceManager.C_MSTAR_REQUEST_NEW_PRODUCT:
                requestNewProductResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        switch (transactionId) {
            case AppServiceManager.ALGOLIA_SEARCH:
            case APIServiceManager.C_MSTAR_REQUEST_NEW_PRODUCT:
                vmOnError();
                break;
        }
    }

    public void onDataAvailable(MstarAlgoliaResponse algoliaResponse) {
        //algolia query id for algolia click and conversion event
        BasePreference.getInstance(getApplicationContext()).setAlgoliaQueryId(algoliaResponse != null && !TextUtils.isEmpty(algoliaResponse.getQueryID()) ? algoliaResponse.getQueryID() : "");
        if (algoliaResponse != null && algoliaResponse.getNbHits() > 0) {
            if (!TextUtils.isEmpty(searchListener.getSearchKeyWord())) {
                totalPageCount = algoliaResponse.getNbPages() - 1;
                if (searchAdapter == null)
                    searchAdapter = searchListener.setSearchAdapter(algoliaResponse.getAlgoliaResultList());
                else {
                    searchAdapter.updateAlgoliaHitList(algoliaResponse.getAlgoliaResultList(), isPagenationCall);
                }
                searchListener.vmSearchResultCountWithText();
                /*WebEngage Product Searched Event*/
                WebEngageHelper.getInstance().productSearchedEvent(searchListener.getContext(), algoliaResponse.getNbHits(), searchListener.getSearchKeyWord(), isFromGeneric);
            }
        } else {
            searchListener.vmRequestProductView();
            searchListener.vmSetNoResultText();
        }

        /*Checking algolia response query matching with typing character,If not matching again fetching data from server*/
        if (algoliaResponse != null && !TextUtils.isEmpty(algoliaResponse.getQuery()) && !algoliaResponse.getQuery().trim().equals(searchListener.getSearchKeyWord().toString().trim())) {
            searchApiCall(false);
        }
    }
    
    //Get product details through sku
    public void getDetail(int sku) {
        this.skuId = sku;
        initApiCall(APIServiceManager.MSTAR_PRODUCT_DETAILS);
    }

    private void requestNewProductResponse(String data) {
        searchListener.vmDismissProgress();
        MstarRequestPopupResponse response = new Gson().fromJson(data, MstarRequestPopupResponse.class);
        if (response != null && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && response.getResult() != null) {
            searchListener.snackBarCallback(response.getResult());
        }
    }

    public TextWatcher getSearchWatcher() {
        return searchListener.getSearchWatcher();
    }

    //Algolia search
    public void searchApiCall(boolean isPagenationCall) {
        this.isPagenationCall = isPagenationCall;
        if (!isAlgoliaSearching && !TextUtils.isEmpty(searchListener.getSearchKeyWord())) {
            isAlgoliaSearching = true;
            initApiCall(AppServiceManager.ALGOLIA_SEARCH);
        }
    }

    //Fetching search history from local database
    public void getHistoryList() {
        new GetSearchHistory().execute();
    }

    /**
     * If searching drug not available,that time showing drug request popup
     */
    public void requestProduct() {
        // Todo to request a product
        searchListener.vmRequestPopup();
    }

    //Navigate to M2 attach prescription page
    public void uploadPrescription() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getApplicationContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_UPLOAD_SEARCH, GoogleAnalyticsHelper.EVENT_LABEL_SEARCH_PAGE);
        searchListener.vmUploadPrescription();
    }

    //check order M2 or not
    public boolean uploadVisibility() {
        return !M2Helper.getInstance().isM2Order();
    }

    public boolean uploadPrescriptionVisibility() {
        return searchListener.uploadPrescriptionVisibilty();
    }

    //Check stock available or not
    public boolean isProductAvailableWithStock(MStarProductDetails productDetails) {
        if (productDetails != null && !TextUtils.isEmpty(productDetails.getAvailabilityStatus()) && AppConstant.PRODUCT_AVAILABLE_CODE.equalsIgnoreCase(productDetails.getAvailabilityStatus())) {
            return productDetails.getStockQty() > 0 && productDetails.getMaxQtyInOrder() > 0;
        }
        return false;
    }

    private void productDetailsResponse(String data) {
        MStarBasicResponseTemplateModel productDetails = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (productDetails != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(productDetails.getStatus()) && productDetails.getResult() != null && productDetails.getResult().getProductDetails() != null) {
            productDetailResponseMutableLiveData.setValue(productDetails.getResult().getProductDetails());
        }
    }

    //Clear search history from local database
    public void clearAll() {
        new ClearSearchHistory().execute();
    }

    public ViewTreeObserver.OnScrollChangedListener paginationForOrderList(final NestedScrollView nestedScrollView) {
        return new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView.getScrollY()));

                if (diff == 0) {
                    if (pageIndex <= totalPageCount) {
                        pageIndex += 1;
                        getMoreProducts();
                    }
                }
            }
        };
    }

    public RecyclerView.OnScrollListener onPageScroll() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                    int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                        if (pageIndex < totalPageCount) {
                            pageIndex += 1;
                            getMoreProducts();
                        }
                    }
                    visibleItemListener.viewVisiblePosition(firstVisibleItem, lastVisibleItem);
                }
            }
        };
    }

    private void getMoreProducts() {
        isAlgoliaSearching = false;
        searchApiCall(true);
    }


    private void vmOnError() {
        isAlgoliaSearching = false;
        searchListener.vmWebserviceErrorView(true);
    }

    public void onClickRequest(String patientName, String medicineName, String phoneNumber) {
        this.patientName = patientName;
        this.medicineName = medicineName;
        this.phoneNumber = phoneNumber;
        searchListener.vmShowProgressBar();
        requestProductAPICall();
    }

    private void requestProductAPICall() {
        APIServiceManager.getInstance().requestNewProduct(this, getMultiPartRequest());
    }

    private MultipartBody getMultiPartRequest() {
        MStarCustomerDetails addressData = new Gson().fromJson(BasePreference.getInstance(getApplication()).getCustomerDetails(), MStarCustomerDetails.class);
        if (addressData != null) {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(AppConstant.FULL_NAME, patientName);
            builder.addFormDataPart(AppConstant.ACCOUNT_EMAIL, !TextUtils.isEmpty(addressData.getEmail()) ? addressData.getEmail() : "");
            builder.addFormDataPart(AppConstant.PRODUCT_NAME, medicineName);
            builder.addFormDataPart(AppConstant.PHONE, phoneNumber);
            return builder.build();
        } else
            return null;
    }

    /*WebEngage Search autocomplete event*/
    public void onSearchAutoCompleteEvent(MstarAlgoliaResult algoliaHitResults) {
        if (algoliaHitResults != null)
            WebEngageHelper.getInstance().searchAutocompleteEvent(searchListener.getContext(), algoliaHitResults, searchListener.getSearchKeyWord());
    }

    public void openNormalSearch() {
        searchListener.openNormalSearch();
    }


    /**
     * Fetching search history from local database and set search adapter
     */
    @SuppressLint("StaticFieldLeak")
    class GetSearchHistory extends AsyncTask<Void, Void, List<SearchHistory>> {

        @Override
        protected List<SearchHistory> doInBackground(Void... voids) {
            return DatabaseClient
                    .getInstance(getApplicationContext())
                    .getAppDatabase()
                    .searchHistoryDAO()
                    .getAll();
        }

        @Override
        protected void onPostExecute(List<SearchHistory> searchHistoryList) {
            super.onPostExecute(searchHistoryList);
            ArrayList<MstarAlgoliaResult> algoliaHistoryList = new ArrayList<>();
            Collections.sort(searchHistoryList, new Comparator<SearchHistory>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public int compare(SearchHistory o1, SearchHistory o2) {
                    return Long.compare(o1.getCurrentTimestamp(), o2.getCurrentTimestamp());
                }
            });
            Collections.reverse(searchHistoryList);
            if (searchHistoryList.size() > 10)
                searchHistoryList.subList(10, searchHistoryList.size()).clear();

            for (SearchHistory searchHistory : searchHistoryList) {
                MstarAlgoliaResult result = new Gson().fromJson(searchHistory.getAlgoliaResult(), MstarAlgoliaResult.class);
                if (isFromGeneric) {
                    if (result != null && result.getIsGenSubsAvailable() == 1)
                        algoliaHistoryList.add(result);
                } else {
                    algoliaHistoryList.add(result);
                }
            }

            searchListener.resetViews(true);


            if (algoliaHistoryList.size() > 0) {
                if (searchAdapter == null)
                    searchAdapter = searchListener.setSearchAdapter(algoliaHistoryList);
                else {
                    searchAdapter.updateAlgoliaHitList(algoliaHistoryList, isPagenationCall);
                    searchAdapter.notifyDataSetChanged();
                }
                searchListener.setEmptyView(false);
                if (uploadVisibility())
                    searchListener.setNoResultView(SubscriptionHelper.getInstance().isCreateNewFillFlag() || isFromGeneric);
            } else {
                searchListener.vmNoProductView();
            }
        }
    }

    /**
     * Clear locally stored search history[Room db]
     */
    @SuppressLint("StaticFieldLeak")
    class ClearSearchHistory extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                    .searchHistoryDAO()
                    .delete();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            searchListener.vmNoProductView();
        }
    }

    private String getFilterQueryGenericSubsAvailable() {
        return "\"" + AppConstant.ALGOLIA_FILTER_KEY_IS_GEN_SUBS_AVAILABLE + "\":\"" + (isGenericSubsAvailable ? 1 : 0) + "\"";
    }

    public static void setVisibleItemListener(VisibleItemListener itemListener) {
        visibleItemListener = itemListener;
    }

    /**
     * View model callback
     */
    public interface SearchListener {
        void vmUploadPrescription();

        void vmRequestPopup();

        void vmSearchResultCountWithText();

        void vmRequestProductView();

        void vmNoProductView();

        void vmNoNetworkView();

        void vmWebserviceErrorView(boolean isWebServiceError);

        void vmSetNoResultText();

        //This method used to get entered search keyword
        String getSearchKeyWord();

        TextWatcher getSearchWatcher();

        SearchAdapter setSearchAdapter(List<MstarAlgoliaResult> algoliaHitList);

        void initEditorListener();

        void setEmptyView(boolean isEmpty);

        void resetViews(boolean reset);

        void setNoResultView(boolean isNoResult);

        boolean uploadPrescriptionVisibilty();

        void snackBarCallback(String message);

        void vmShowProgressBar();

        void vmDismissProgress();

        Context getContext();

        void openNormalSearch();
    }
}
