package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentFilterBinding;
import com.netmedsmarketplace.netmeds.model.MstarAlgoliaFilterTitle;
import com.netmedsmarketplace.netmeds.model.MstarFilterOption;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class FilterViewModel extends AppViewModel {
    private FragmentFilterBinding mBinding;
    private FilterListener mListener;
    private Map<String, Map<String, String>> filterOptionList;
    private static final int DIVIDE_VALUE = 5;
    private String intentFrom;

    public FilterViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(FragmentFilterBinding binding, Map<String, Map<String, String>> filterOptionList, FilterListener listener, String intentFrom) {
        this.mBinding = binding;
        this.mListener = listener;
        this.filterOptionList = filterOptionList;
        this.intentFrom = intentFrom;
        onFilterOptionDataAvailable();
    }

    public void closeView() {
        mListener.closeDialog();
    }

    public void clearFilter() {
        mListener.resetFilterView();
    }

    public void applyFilter() {
        if ((MstarFilterHelper.getManufactureSelection() != null && MstarFilterHelper.getManufactureSelection().size() > 0)
                || (MstarFilterHelper.getBrandSelection() != null && MstarFilterHelper.getBrandSelection().size() > 0)
                || (MstarFilterHelper.getPriceSelection() != null && MstarFilterHelper.getPriceSelection().size() > 0)
                || (MstarFilterHelper.getDiscountSelection() != null && MstarFilterHelper.getDiscountSelection().size() > 0)
                || (MstarFilterHelper.getCategorySelection() != null && MstarFilterHelper.getCategorySelection().size() > 0)
                || (MstarFilterHelper.getAvailabilitySelection() != null && MstarFilterHelper.getAvailabilitySelection().size() > 0)) {
            mListener.vmShowProgress();
            mListener.applyFilter();
        } else
            mListener.setError(getApplication().getResources().getString(R.string.text_filter_error));
    }

    public void setFilterVisibility(String titleSelection) {
        mListener.toggleKeyBoard();
        mBinding.search.setText("");
        mBinding.search.clearFocus();
        mBinding.search.setHint(getSearchHint(titleSelection));
        mBinding.searchView.setVisibility(titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME) || titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_BRAND) || titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL)
                ? View.VISIBLE : View.GONE);
    }

    private void onFilterOptionDataAvailable() {
        ArrayList<MstarAlgoliaFilterTitle> filterTitleList = new ArrayList<>();
        if (filterOptionList != null && filterOptionList.size() > 0) {
            for (Map.Entry<String, Map<String, String>> entry : filterOptionList.entrySet()) {
                String key = entry.getKey();
                Map<String, String> optionList = entry.getValue();

                if (optionList != null && optionList.size() > 0) {
                    if (key.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK)) {
                        boolean isProductAvailable = false;
                        for (Map.Entry<String, String> valueMap : entry.getValue().entrySet()) {
                            String childKey = valueMap.getKey();
                            if (!isProductAvailable) {
                                isProductAvailable = Double.valueOf(childKey) > 0;
                            } else {
                                break;
                            }
                        }
                        if (isProductAvailable) {
                            filterTitleList.add(new MstarAlgoliaFilterTitle(getApplication().getResources().getString(R.string.text_filter_title_availability), key, 1));
                        }
                    } else if (key.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL)) {
                        filterTitleList.add(new MstarAlgoliaFilterTitle(getApplication().getResources().getString(R.string.text_filter_title_categories), key, 2));
                    } else if (!AppConstant.BRAND.equalsIgnoreCase(intentFrom) && key.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_BRAND)) {
                        filterTitleList.add(new MstarAlgoliaFilterTitle(getApplication().getResources().getString(R.string.text_filter_title_brand), key, 3));
                    } else if (!AppConstant.MANUFACTURER.equalsIgnoreCase(intentFrom) && key.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME)) {
                        filterTitleList.add(new MstarAlgoliaFilterTitle(getApplication().getResources().getString(R.string.text_filter_title_manufacturer), key, 4));
                    } else if (key.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE)) {
                        filterTitleList.add(new MstarAlgoliaFilterTitle(getApplication().getResources().getString(R.string.text_filter_title_selling_price), key, 5));
                    } else if (key.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT)) {
                        boolean isDiscountAvailable = false;
                        for (Map.Entry<String, String> valueMap : entry.getValue().entrySet()) {
                            String childKey = valueMap.getKey();
                            if (!isDiscountAvailable) {
                                isDiscountAvailable = Double.valueOf(childKey) > 0;
                            } else {
                                break;
                            }
                        }
                        if (isDiscountAvailable) {
                            filterTitleList.add(new MstarAlgoliaFilterTitle(getApplication().getResources().getString(R.string.text_filter_title_discount), key, 6));
                        }
                    }
                }
            }

            if (filterTitleList.size() > 0) {
                Collections.sort(filterTitleList, new Comparator<MstarAlgoliaFilterTitle>() {
                    @Override
                    public int compare(MstarAlgoliaFilterTitle object1, MstarAlgoliaFilterTitle object2) {
                        return object1.getValue() - object2.getValue();
                    }
                });
                mListener.setFilterTitleAdapter(filterTitleList, filterTitleList.get(0).getKey());
                setFilterOption(filterOptionList.get(filterTitleList.get(0).getKey()), filterTitleList.get(0).getKey(), filterTitleList.get(0).getTitle());
            } else {
                closeView();
                mListener.setAlert(getApplication().getResources().getString(R.string.text_empty_filter_option));
            }
        }
    }

    public void setFilterOption(Map<String, String> values, String key, String filterTitle) {
        ArrayList<MstarFilterOption> optionList;
        if (key.equals(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE) || key.equals(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT)) {
            ArrayList<Integer> priceList = new ArrayList<>();
            for (String priceValue : values.keySet()) {
                double price = Double.parseDouble(priceValue);
                int round_value = (int) (Math.round(price * 100) / 100);
                priceList.add(round_value);
            }
            int max_value = getMaxValue(priceList);
            int nearestHundred = ((max_value + 99) / 100) * 100;
            Log.d("Maximum Value is", String.valueOf(max_value));
            Log.d("NearestHundred Value", String.valueOf(nearestHundred));
            int add_range = nearestHundred / DIVIDE_VALUE;
            int startRange = 0;
            int endRange = add_range;
            optionList = new ArrayList<>();
            if (max_value != 0) {
                for (int i = 0; i < DIVIDE_VALUE; i++) {
                    MstarFilterOption mstarFilterOption = new MstarFilterOption();
                    mstarFilterOption.setName(key.equals(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE) ? "Rs." + startRange + " - " + endRange : startRange + "%" + " to " + endRange + "%");
                    mstarFilterOption.setValue(key.equals(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE) ? startRange + " - " + endRange : startRange + "%" + " to " + endRange + "%");
                    optionList.add(mstarFilterOption);
                    startRange = endRange;
                    endRange += add_range;
                }
            }
        } else if (filterTitle.equalsIgnoreCase(getApplication().getResources().getString(R.string.text_filter_title_categories))) {
            optionList = new ArrayList<>();
            for (Map.Entry<String, String> entry : values.entrySet()) {
                MstarFilterOption mstarFilterOption = new MstarFilterOption();
                mstarFilterOption.setName(entry.getKey());
                mstarFilterOption.setValue(entry.getKey());
                mstarFilterOption.setCount(Integer.valueOf(entry.getValue()));
                optionList.add(mstarFilterOption);
            }
            Collections.sort(optionList, new Comparator<MstarFilterOption>() {
                @Override
                public int compare(MstarFilterOption count, MstarFilterOption compareCount) {
                    return compareCount.getCount().compareTo(count.getCount());
                }
            });
        } else if (key.equals(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK)) {
            optionList = new ArrayList<>();
            for (String value : values.keySet()) {
                if (value.equals(AppConstant.IN_STOCK)) {
                    MstarFilterOption mstarFilterOption = new MstarFilterOption();
                    mstarFilterOption.setName(AppConstant.EXCLUDE_OUT_OF_STOCK);
                    mstarFilterOption.setValue(value);
                    optionList.add(mstarFilterOption);
                }
            }
        } else {
            optionList = new ArrayList<>();
            for (String value : values.keySet()) {
                MstarFilterOption mstarFilterOption = new MstarFilterOption();
                mstarFilterOption.setName(value);
                mstarFilterOption.setValue(value);
                optionList.add(mstarFilterOption);
            }
        }
        mListener.setFilterOptionAdapter(optionList);
    }

    private static int getMaxValue(ArrayList<Integer> priceList) {
        int maxValue = priceList.get(0);
        for (int i = 1; i < priceList.size(); i++) {
            if (priceList.get(i) > maxValue) {
                maxValue = priceList.get(i);
            }
        }
        return maxValue;
    }

    private String getSearchHint(String title) {
        switch (title) {
            case AppConstant.ALGOLIA_FILTER_KEY_BRAND:
                return getApplication().getResources().getString(R.string.text_search_brands);
            case AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL:
                return getApplication().getResources().getString(R.string.text_search_category);
            case AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME:
                return getApplication().getResources().getString(R.string.text_search_manufacturer);
        }
        return "";
    }

    public interface FilterListener {
        void closeDialog();

        void setFilterTitleAdapter(List<MstarAlgoliaFilterTitle> titleList, String selectedFilter);

        void setFilterOptionAdapter(ArrayList<MstarFilterOption> filterOptions);

        void vmShowProgress();

        void vmDismissProgress();

        void applyFilter();

        void resetFilterView();

        void setError(String error);

        void toggleKeyBoard();

        void setAlert(String string);
    }
}
