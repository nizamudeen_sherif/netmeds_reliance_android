package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.PrimeMemberShipAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarPrimeProduct;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;

import java.lang.ref.WeakReference;
import java.util.List;

import static android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;

public class PrimeMemberShipViewModel extends AppViewModel implements PrimeMemberShipAdapter.PrimeMemberShipAdapterListener {

    private int failedTransactionId;
    private int productCode;
    private int productCodeToDelete;
    private int transactionIdAfterContinueShoppingAPI;
    private int cartId;
    private static int productQuantity = 1;
    private String appliedVoucher = "";

    private OnPrimeMembershipListener listener;
    private BasePreference basepreference;

    private MStarCustomerDetails primeCustomer;
    private ConfigurationResponse configurationResponse;
    private PrimeConfig primeConfig;
    private MStarProductDetails primeProduct;

    private MutableLiveData<MstarPrimeProductResult> primeProductMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<MStarBasicResponseTemplateModel> customerDetailsMutableLiveData = new MutableLiveData<>();

    private boolean primeUserFlag = false;


    public PrimeConfig getPrimeConfig() {
        return primeConfig;
    }

    private void setPrimeConfig(PrimeConfig primeConfig) {
        this.primeConfig = primeConfig;
    }

    private MStarCustomerDetails getPrimeCustomer() {
        return primeCustomer;
    }

    private void setPrimeCustomer(MStarCustomerDetails primeCustomer) {
        this.primeCustomer = primeCustomer;
    }

    private void setPrimeUserFlag(boolean primeUserFlag) {
        this.primeUserFlag = primeUserFlag;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public PrimeMemberShipViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(OnPrimeMembershipListener listener, BasePreference basePreference) {
        this.listener = listener;
        this.basepreference = basePreference;
        configurationResponse = new Gson().fromJson(BasePreference.getInstance(getApplication().getApplicationContext()).getConfiguration(), ConfigurationResponse.class);
        setPrimeConfig(configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getPrimeConfig() : new PrimeConfig());
        listener.initPrimeMemberShipIconAdapter(getPrimeConfig());
        listener.loadFlagCallback();
    }

    public MutableLiveData<MstarPrimeProductResult> getPrimeProductMutableLiveData() {
        return primeProductMutableLiveData;
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getCustomerDetailsMutableLiveData() {
        return customerDetailsMutableLiveData;
    }

    public void getPrimeProducts() {
        initMstarApi(APIServiceManager.MSTAR_PRIME_PRODUCT);
    }

    private void setProductCodeToDelete(int productCodeToDelete) {
        this.productCodeToDelete = productCodeToDelete;
    }

    private void addToCart() {
        if (basepreference.getMStarCartId() > 0)
            initMstarApi(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
        else
            initMstarApi(APIServiceManager.MSTAR_CREATE_CART);
    }

    /* add to cart request payload
     *
     * @param quoteId String
     * @return addToCartRequest
     */

    private void initMstarApi(int transactionId) {
        boolean isNetworkConnected = listener.isNetworkConnected();
        if (isNetworkConnected) {
            listener.onShowProgress();
            switch (transactionId) {
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, basepreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_PRIME_PRODUCT:
                    APIServiceManager.getInstance().mstarPrimeProduct(this);
                    break;
                case APIServiceManager.MSTAR_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateCart(this, basepreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    APIServiceManager.getInstance().mStarGetCartDetails(this, basepreference.getMstarBasicHeaderMap(), null, transactionId);
                    break;
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basepreference.getMstarBasicHeaderMap(), productCode, productQuantity, null);
                    break;
                case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                    APIServiceManager.getInstance().mStarRemoveProductFromCart(this, basepreference.getMstarBasicHeaderMap(), productCodeToDelete, productQuantity, M2Helper.getInstance().isM2Order() ? basepreference.getMstarMethod2CartId() : null);
                    break;
                case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                    APIServiceManager.getInstance().mStarUnapplyVoucher(this, basepreference.getMstarBasicHeaderMap(), appliedVoucher, M2Helper.getInstance().isM2Order() ? basepreference.getMstarMethod2CartId() : null);
                    break;

            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        listener.onDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_CART:
                onCartCreatedResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                getCartDetailsResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                if (data != null) {
                    MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                    customerDetailsMutableLiveData.setValue(response);
                }
                break;
            case APIServiceManager.MSTAR_PRIME_PRODUCT:
                if (data != null) {
                    primeProductMutableLiveData.setValue(new Gson().fromJson(data, MstarPrimeProductResult.class));
                }
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                onProductAddedResponse(data);
                break;
            case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                deleteCartItemResponse(data);
                break;
            case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                onVoucherUnappliedResponse(data);
                break;
        }
    }

    private void getCartDetailsResponse(String data) {
        if (data != null) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getCartDetails() != null) {
                cartItemResponse(response.getResult().getCartDetails());
            } else {
                primeProduct = null;
                initMstarApi(APIServiceManager.MSTAR_PRIME_PRODUCT);
            }
        }
    }

    private void onCheckoutInitiated(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                initMstarApi(APIServiceManager.MSTAR_UNAPPLY_VOUCHER);
            }
        }
    }


    private void onVoucherUnappliedResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                PaymentHelper.setIsVoucherApplied(false);
                onAddToCart(productCode);
            }
        } else {
            failedTransactionId = APIServiceManager.MSTAR_UNAPPLY_VOUCHER;
        }

    }

    private void onProductAddedResponse(String data) {
        MStarBasicResponseTemplateModel productDetails = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (productDetails != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(productDetails.getStatus())) {
            listener.onAddToCartCallBack();
        } else {
            listener.onShowMessage(getApplication().getString(R.string.text_unable_to_add_to_cart));
        }
    }

    private void onCartCreatedResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getResult() != null && response.getResult().getCart_id() > 0) {
            basepreference.setMStarCartId(response.getResult().getCart_id());
            addToCart();
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.onDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                failedTransactionId = APIServiceManager.MSTAR_CUSTOMER_DETAILS;
                break;
            case APIServiceManager.MSTAR_PRIME_PRODUCT:
                failedTransactionId = APIServiceManager.MSTAR_PRIME_PRODUCT;
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                failedTransactionId = APIServiceManager.MSTAR_CREATE_CART;
                break;
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                failedTransactionId = APIServiceManager.MSTAR_GET_CART_DETAILS;
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                failedTransactionId = APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART;
                break;
            case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                failedTransactionId = APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART;
                break;
            case APIServiceManager.MSTAR_UNAPPLY_VOUCHER:
                failedTransactionId = APIServiceManager.MSTAR_UNAPPLY_VOUCHER;
                break;
        }
    }

    public void checkUser() {
        boolean isConnected = listener.isNetworkConnected();
        listener.showNoNetworkViewCallback(isConnected);
        if (isConnected) {
            initMstarApi(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
        } else
            failedTransactionId = APIServiceManager.MSTAR_CUSTOMER_DETAILS;
    }

    public void onCheckUserDataAvailable(MStarCustomerDetails customerDetails) {
        if (customerDetails != null) {
            setPrimeUserFlag(customerDetails.isPrime());
            setPrimeCustomer(customerDetails);
            listener.checkPrimeUserOrNotCallback(isPrimeUserFlag(), getPrimeCustomer());
            if (!CommonUtils.isExpiryDate(getPrimeCustomer()) || !customerDetails.isPrime()) {
                initMstarApi(APIServiceManager.MSTAR_GET_CART_DETAILS);
            }
        } else
            vmOnError(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }


    public boolean isPrimeUserFlag() {
        return getPrimeCustomer() != null && getPrimeCustomer().isPrime();
    }

    public String setPrimeExpiryDate() {
        return getPrimeCustomer() != null && !TextUtils.isEmpty(getPrimeCustomer().getPrimeValidTillTime()) ? DateTimeUtils.getInstance().stringDate(getPrimeCustomer().getPrimeValidTillTime(), DateTimeUtils.yyyyMMddHHmmss, DateTimeUtils.ddMMMyyyy) : "Expiry Dat";

    }

    public String setPrimePlan() {
        return getPrimeCustomer() != null && !TextUtils.isEmpty(getPrimeCustomer().getPrimePackage()) ? getPrimeCustomer().getPrimePackage() : "";
    }

    private String primeMemberHeaderText() {
        return getPrimeConfig() != null && !TextUtils.isEmpty(getPrimeConfig().getPrimeMemberText()) ? getPrimeConfig().getPrimeMemberText() : "";
    }

    private String nonPrimeMemberHeaderText() {
        return getPrimeConfig() != null && !TextUtils.isEmpty(getPrimeConfig().getPrimeHeader()) ? getPrimeConfig().getPrimeHeader() : "";
    }

    public String primeHeaderText() {
        return CommonUtils.isExpiryDate(getPrimeCustomer()) && isPrimeUserFlag() ? primeMemberHeaderText() : nonPrimeMemberHeaderText();
    }

    public boolean getPrimePackageVisibility() {
        return getPrimeCustomer() != null && !TextUtils.isEmpty(getPrimeCustomer().getPrimePackage());
    }

    private void vmOnError(int transactionId) {
        listener.onDismissProgress();
        failedTransactionId = transactionId;
        listener.showWebServiceError(true);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        listener.showWebServiceError(false);
        onRetry(failedTransactionId);
        listener.loadFlagCallback();
    }

    /**
     * @return network connected or not
     */
    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
            case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                checkUser();
                break;
            case APIServiceManager.MSTAR_PRIME_PRODUCT:
                getPrimeProducts();
                break;
        }
    }


    public String getFagUrl() {
        if (configurationResponse != null) {
            return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeFag()) ? configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeFag() : "";
        } else {
            ConfigurationResponse configurationResponse = new Gson().fromJson(!TextUtils.isEmpty(basepreference.getPrefPrimeResponse()) ? basepreference.getPrefPrimeResponse() : "", ConfigurationResponse.class);
            return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeFag()) ? configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeFag() : "";
        }
    }

    public static class NetmedsWebViewClient extends WebViewClient {
        private final WeakReference<Activity> mActivityRef;


        public NetmedsWebViewClient(Context mContext) {
            mActivityRef = new WeakReference<>((Activity) mContext);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("mailto:")) {
                final Activity activity = mActivityRef.get();
                if (activity != null) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(activity, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                    activity.startActivity(i);
                    view.reload();
                    return true;
                }
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

        }

        private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            intent.putExtra(Intent.EXTRA_TEXT, body);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_CC, cc);
            intent.setType("message/rfc822");
            return intent;
        }
    }


    public void webViewSetting(WebView webView) {
        WebSettings webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSetting.setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSetting.setMixedContentMode(MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }


    public void primeProductDataAvailable(MstarPrimeProductResult primeProductResult) {
        listener.onDismissProgress();
        if (primeProductResult != null && primeProductResult.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS) && primeProductResult.getPrimeProducts() != null && primeProductResult.getPrimeProducts().size() > 0) {
            listener.setPrimeProductAdapterCallback(primeProductResult.getPrimeProducts(), primeProduct, this);
        } else
            failedTransactionId = APIServiceManager.MSTAR_PRIME_PRODUCT;
    }

    private void cartItemResponse(MStarCartDetails cartDetails) {
        if (cartDetails != null) {
            if (!TextUtils.isEmpty(cartDetails.getAppliedGeneralVouchers())) {
                appliedVoucher = cartDetails.getAppliedGeneralVouchers();
                PaymentHelper.setIsVoucherApplied(true);
            }
            if (cartDetails.getLines() != null && cartDetails.getLines().size() > 0) {
                setCartItemResult(cartDetails.getLines());
            } else {
                primeProduct = null;
            }
            basepreference.setMStarM1CartIsOpen(AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(cartDetails.getCartStatus()));
            initMstarApi(APIServiceManager.MSTAR_PRIME_PRODUCT);
        }
    }

    private void setCartItemResult(List<MStarProductDetails> cartResponseItem) {
        MStarProductDetails code = null;
        for (MStarProductDetails mstarCartLineItem : cartResponseItem) {
            if (mstarCartLineItem != null && (mstarCartLineItem.getProductCode() == AppConstant.MSTAR_PRIME_PRODUCT_CODE_6_MON || mstarCartLineItem.getProductCode() == AppConstant.MSTAR_PRIME_PRODUCT_CODE_12_MON)) {
                code = mstarCartLineItem;
            }
        }
        primeProduct = code;
    }

    private void deleteCartItem() {
        boolean isConnected = listener.isNetworkConnected();
        listener.showNoNetworkViewCallback(isConnected);
        if (isConnected) {
            listener.onShowProgress();
            initMstarApi(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
        } else
            failedTransactionId = APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART;
    }

    private void deleteCartItemResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                addToCart();
            } else {
                failedTransactionId = APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART;
            }
        }
    }

    public String primeMemberShipTitle() {
        return getPrimeConfig() != null && getPrimeConfig().getNetmedsMembership() != null && !TextUtils.isEmpty(getPrimeConfig().getNetmedsMembership().getTitle()) ? getPrimeConfig().getNetmedsMembership().getTitle() : "";
    }

    public String primeMemberShipHeader() {
        return getPrimeConfig() != null && getPrimeConfig().getNetmedsMembership() != null && !TextUtils.isEmpty(getPrimeConfig().getNetmedsMembership().getHeader()) ? getPrimeConfig().getNetmedsMembership().getHeader() : "";
    }

    public String primeMemberShipDescription() {
        return getPrimeConfig() != null && getPrimeConfig().getNetmedsMembership() != null && !TextUtils.isEmpty(getPrimeConfig().getNetmedsMembership().getDescription()) ? getPrimeConfig().getNetmedsMembership().getDescription() : "";
    }

    @Override
    public void onAddToCart(int primeMemberShipProductCode) {
        setProductCode(primeMemberShipProductCode);
        if (PaymentHelper.isVoucherApplied()) {
            unApplyVoucher();
        } else if (basepreference.isGuestCart()) {
            listener.onNavigateSignActivity();
        } else {
            listener.onShowProgress();
            addToCart();
        }
    }

    private void unApplyVoucher() {
        initMstarApi(APIServiceManager.MSTAR_UNAPPLY_VOUCHER);
    }

    @Override
    public void onDeleteCart(int primeMemberShipProductCode) {
        //if prime product is available in cart, then remove that product and add the another
        setProductCodeToDelete(primeMemberShipProductCode);
        deleteCartItem();
    }

    @Override
    public void setProductCodeToAdd(int productCodeToAdd) {
        setProductCode(productCodeToAdd);
    }

    public interface OnPrimeMembershipListener {
        void onShowProgress();

        void onDismissProgress();

        void onShowMessage(String message);

        void onAddToCartCallBack();

        void onNavigateSignActivity();

        void setPrimeProductAdapterCallback(List<MstarPrimeProduct> primeMembershipProduct, MStarProductDetails primeProduct, PrimeMemberShipViewModel model);

        void initPrimeMemberShipIconAdapter(PrimeConfig primeConfig);

        void showNoNetworkViewCallback(boolean isConnected);

        void showWebServiceError(boolean isWebError);

        void checkPrimeUserOrNotCallback(boolean primeCustomer, MStarCustomerDetails customer);

        void loadFlagCallback();

        boolean isNetworkConnected();
    }
}
