package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityResetPasswordBinding;
import com.netmedsmarketplace.netmeds.model.LoginResponse;
import com.netmedsmarketplace.netmeds.model.request.ResetPasswordRequest;
import com.netmedsmarketplace.netmeds.ui.ResetPasswordActivity;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import static com.nms.netmeds.base.IntentConstant.PHONE_NUMBER;

public class ResetPasswordViewModel extends AppViewModel {

    private ResetPasswordListener resetPasswordListener;
    private ActivityResetPasswordBinding activityResetPasswordBinding;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Intent getIntent;

    public ResetPasswordViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivityResetPasswordBinding signWithPasswordBinding, ResetPasswordActivity resetPasswordListener, Intent intent, Context context) {
        this.activityResetPasswordBinding = signWithPasswordBinding;
        this.resetPasswordListener = resetPasswordListener;
        this.getIntent = intent;
        this.context = context;
        imageOptionButtonClick();
    }

    public void onClickReset() {
        if (validateForm()) {
            resetPassword();
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        resetPassword();
    }

    public void onPasswordEditTextChanged() {
        activityResetPasswordBinding.inputPassword.setError(null);
    }

    private void resetPassword() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            resetPasswordListener.vmShowProgress();
            ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();
            resetPasswordRequest.setPassword(activityResetPasswordBinding.password.getText().toString());
            resetPasswordRequest.setPhoneno(getIntent.getStringExtra(PHONE_NUMBER));
            AppServiceManager.getInstance().resetPassword(this, resetPasswordRequest);
        }
    }

    private boolean validateForm() {
        boolean isValidate = true;
        if (!checkPassword(activityResetPasswordBinding.password, activityResetPasswordBinding.inputPassword, context.getString(R.string.error_password_empty))) {
            isValidate = false;
        }
        return isValidate;
    }

    private boolean checkPassword(EditText editText, TextInputLayout inputLayout, String emptyErrorMessage) {
        return ValidationUtils.checkLoginSetPassword(editText, inputLayout, true, emptyErrorMessage, this.context.getString(R.string.error_password_length));
    }

    private void showNoNetworkView(boolean isConnected) {
        activityResetPasswordBinding.resetViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        activityResetPasswordBinding.resetNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        activityResetPasswordBinding.resetViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        activityResetPasswordBinding.resetApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        loginResponse(data);
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        resetPasswordListener.vmDismissProgress();
        showWebserviceErrorView(true);
    }

    private void loginResponse(String data) {
        resetPasswordListener.vmDismissProgress();
        LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
        if (loginResponse.getServiceStatus().getStatus().equalsIgnoreCase("success")) {
            resetPasswordListener.vmCallbackSignInWithPasswordActivity();
        } else {
            SnackBarHelper.snackBarCallBack(activityResetPasswordBinding.resetViewContent, context, loginResponse.getServiceStatus().getMessage());
        }
    }

    private void imageOptionButtonClick() {
        activityResetPasswordBinding.password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateForm()) {
                        resetPassword();
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

    public interface ResetPasswordListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmCallbackSignInWithPasswordActivity();
    }
}
