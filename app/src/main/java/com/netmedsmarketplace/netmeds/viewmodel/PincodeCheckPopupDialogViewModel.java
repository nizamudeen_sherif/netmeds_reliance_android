package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateLineItem;
import com.nms.netmeds.base.model.DeliveryEstimateProductDetail;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.PincodeDeliveryEstimate;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;
import java.util.List;

public class PincodeCheckPopupDialogViewModel extends AppViewModel {

    private PincodeCheckPopupDialogViewModelCallBack callBack;
    private BasePreference basePreference;

    private int productCode;
    private int selectedQuantity;

    public PincodeCheckPopupDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(PincodeCheckPopupDialogViewModelCallBack callBack, BasePreference basePreference, int productCode, int selectedQuantity) {
        this.callBack = callBack;
        this.basePreference = basePreference;
        this.productCode = productCode;
        this.selectedQuantity = selectedQuantity;
        initiatePropertiesAndViews();
    }

    private void initiatePropertiesAndViews() {
        callBack.initiateViewBasedOnFlow();
        callBack.imeOptionButtonClick();
    }

    public void onclickCheckAvailability() {
        if (!callBack.isEnterPincodeIsValid()) {
            initiateAPICall(APIServiceManager.DELIVERY_DATE_ESTIMATE);
        }
    }

    private void initiateAPICall(int transactionID) {
        callBack.vmShowProgress();
        if (callBack.isNetworkConnected()) {
            switch (transactionID) {
                case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                    ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
                    APIServiceManager.getInstance().deliveryEstimation(this, getDeliveryDateEstimate(), configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDeliveryDateApi()) ? configurationResponse.getResult().getConfigDetails().getDeliveryDateApi() + "/" : "");
                    break;
            }
        } else {
            callBack.dismissLoader();
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                pinCodeCheckWithDeliveryEstimate(data);
                break;
        }
    }

    private void pinCodeCheckWithDeliveryEstimate(String data) {
        DeliveryEstimateResponse response = new Gson().fromJson(data, DeliveryEstimateResponse.class);
        if (response != null && response.getStatus() && response.getResult() != null &&
                response.getResult().getPinCodeResultList() != null &&
                response.getResult().getPinCodeResultList().size() > 0 &&
                response.getResult().getPinCodeResultList().get(0) != null &&
                response.getResult().getPinCodeResultList().get(0).getProductDetails() != null &&
                response.getResult().getaStatus().equalsIgnoreCase(getApplication().getResources().getString(R.string.text_ok))) {
            DeliveryEstimateProductDetail expiryDetails = response.getResult().getPinCodeResultList().get(0).getProductDetails().get(String.valueOf(productCode)) != null ? response.getResult().getPinCodeResultList().get(0).getProductDetails().get(String.valueOf(productCode)).get(0) : null;
            PincodeDeliveryEstimate deliveryDetails = response.getResult().getPinCodeResultList().get(0).getDeliveryEstimate();
            basePreference.setPinCode(callBack.getEnteredPinCode());
            callBack.onAvailabilityCheck(true, expiryDetails, deliveryDetails);
        } else if (response != null && response.getResult() != null) {
            callBack.dismissLoader();
            callBack.onPincodeError(response.getResult().getaStatus(), response.getResult().getDeliveryEstimateAdditionalData().getSpecialMessage());
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callBack.dismissLoader();
        callBack.clickDismiss();
    }

    private DeliveryEstimateRequest getDeliveryDateEstimate() {
        DeliveryEstimateRequest request = new DeliveryEstimateRequest();
        request.setCallingToRoute(false);
        request.setDoNotSplit(false);
        request.setPincode(callBack.getEnteredPinCode());
        List<DeliveryEstimateLineItem> lineItems = new ArrayList<>();
        DeliveryEstimateLineItem items = new DeliveryEstimateLineItem();
        items.setItemcode(String.valueOf(productCode));
        items.setQty(selectedQuantity);
        lineItems.add(items);
        request.setLstdrug(lineItems);
        return request;
    }

    public void clickDismiss() {
        callBack.clickDismiss();
    }

    public interface PincodeCheckPopupDialogViewModelCallBack {
        void initiateViewBasedOnFlow();

        boolean isEnterPincodeIsValid();

        void clickDismiss();

        void imeOptionButtonClick();

        void onPincodeError(String message, String specialMessage);

        void vmShowProgress();

        void dismissLoader();

        boolean isNetworkConnected();

        String getEnteredPinCode();

        void onAvailabilityCheck(boolean isAvailable, DeliveryEstimateProductDetail expiryDetails, PincodeDeliveryEstimate delieryDetails);
    }

}
