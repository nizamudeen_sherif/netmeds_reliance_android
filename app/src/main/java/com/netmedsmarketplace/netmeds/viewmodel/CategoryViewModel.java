package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.adpater.MstarCategoryAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.VisibleItemListener;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarBaseIdUrl;
import com.nms.netmeds.base.model.MstarCategoryDetails;
import com.nms.netmeds.base.model.MstarFacetItemDetails;
import com.nms.netmeds.base.model.MstarSubCategoryResult;
import com.nms.netmeds.base.model.Request.MstarBrandProductListRequest;
import com.nms.netmeds.base.model.Request.MstarCategoryDetailsRequest;
import com.nms.netmeds.base.model.SortOptionLabel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryViewModel extends AppViewModel implements MstarCategoryAdapter.PopularProductAdapterListener {

    private static int categoryId;
    private static int brandId;
    private CategoryListener listener;
    private String imageURLPath;
    private String from;
    private String response;
    private String categoryResponse;
    private String categoryName = "";

    private final int pageSize = 20;
    private int pageIndex = 1;
    private long totalPageCount = 0;
    private boolean clearList = false;
    private boolean isMstarCatlogBannerCompleted = false;
    private boolean isMstarCategoryDetailsCompleted = false;


    private List<MstarSubCategoryResult> subCategoryList;
    private List<String> defaultBannerLists;
    private List<MstarBanner> mstarBanners;
    private List<MstarFacetItemDetails> brandFilterList;
    private MstarCategoryDetails model;

    private Integer productCode;

    public MstarCategoryAdapter categoryAdapter;
    private boolean webEngageFlag = false;
    private List<SortOptionLabel> sortOptionList;
    private SortOptionLabel sortOption;
    private String filterQuery = "";
    public boolean isFacetValueFirst = true;
    private boolean isFilterApplied = false;
    private int failedTransactionId;
    private Map<String, Map<String, String>> algoliaFilterOption;
    private static VisibleItemListener visibleItemListener;
    private MutableLiveData<MstarAlgoliaResponse> algoliaMutableLiveData = new MutableLiveData<>();

    public CategoryViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<MstarAlgoliaResponse> getAlgoliaMutableLiveData() {
        return algoliaMutableLiveData;
    }

    public void init(BasePreference basePreference, CategoryListener listener) {
        this.listener = listener;
        MStarBasicResponseTemplateModel response = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageURLPath = response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getCatalogImageUrlBasePath()) ? response.getResult().getCatalogImageUrlBasePath() : "";
    }

    public void getCategoryList(int categoryId) {
        CategoryViewModel.categoryId = categoryId;
        from = AppConstant.CATEGORY;
        initMstarAPIcall(APIServiceManager.MSTAR_CATEGORY_DETAILS);
    }

    public void getBrandProductList(int brandId, String response) {
        this.response = response;
        this.model = new Gson().fromJson(response, MstarCategoryDetails.class);
        CategoryViewModel.brandId = brandId;
        from = AppConstant.BRAND;
        initMstarAPIcall(APIServiceManager.MSTAR_BRAND_PRODUCT_LIST);
    }

    private void initMstarAPIcall(int transactionId) {
        boolean isNetworkAvailable = listener.isNetworkConnected();
        listener.showNoNetworkView(!isNetworkAvailable);
        listener.vmShowProgress();
        if (isNetworkAvailable) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_CATEGORY_DETAILS:
                    APIServiceManager.getInstance().mStarCategoryDetails(this, getCategoryRequest());
                    break;
                case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                    APIServiceManager.getInstance().mStarProductDetails(this, productCode, APIServiceManager.MSTAR_PRODUCT_DETAILS);
                    break;
                case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
                    APIServiceManager.getInstance().MstarGetBrandProductList(this, getBrandProductListRequest());
                    break;
                case APIServiceManager.C_MSTAR_CATLOG_BANNERS:
                    APIServiceManager.getInstance().getMstarCatlogBanners(this, AppConstant.CATEGORY.toLowerCase(), String.valueOf(categoryId), "","");
                    break;
                case AppServiceManager.ALGOLIA_SEARCH:
                    AppServiceManager.getInstance().algoliaSearch(this, "", pageIndex,
                            (TextUtils.isEmpty(CommonUtils.getAlgoliaCredentials(getPreference(), AppConstant.ALGOLIA_HITS)) ? AppConstant.DEFAULT_ALGOLIA_HITS : Integer.valueOf(CommonUtils.getAlgoliaCredentials(getPreference(), AppConstant.ALGOLIA_HITS))), CommonUtils.getAlgoliaCredentials(getPreference(), AppConstant.ALGOLIA_API_KEY), getAlgoliaSortIndex(),
                            CommonUtils.getAlgoliaCredentials(getPreference(), AppConstant.ALGOLIA_APP_ID), false, getFacetFilterQuery(), getFilterQuery());
                    break;
            }
        } else {
            failedTransactionId = transactionId;
            listener.vmDismissProgress();
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CATEGORY_DETAILS:
                isMstarCategoryDetailsCompleted = true;
                categoryListResponse(data);
                break;
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                productDetailAvailable(data);
                break;
            case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
                brandProductDetailsAvailable(data);
                break;
            case APIServiceManager.C_MSTAR_CATLOG_BANNERS:
                isMstarCatlogBannerCompleted = true;
                catlogBannersResponse(data);
                break;
            case AppServiceManager.ALGOLIA_SEARCH:
                MstarAlgoliaResponse mstarAlgoliaResponse = new Gson().fromJson(data, MstarAlgoliaResponse.class);
                algoliaMutableLiveData.setValue(mstarAlgoliaResponse);
                break;
        }
        listener.vmDismissProgress();
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.vmDismissProgress();
        failedTransactionId = transactionId;
        switch (transactionId) {
            case APIServiceManager.MSTAR_CATEGORY_DETAILS:
            case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
            case AppServiceManager.ALGOLIA_SEARCH:
                listener.showWebError(true);
                break;
        }
        listener.vmDismissProgress();
    }

    private void onRetry(int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CATEGORY_DETAILS:
                getCategoryList(categoryId);
                break;
            case APIServiceManager.MSTAR_BRAND_PRODUCT_LIST:
                getBrandProductList(brandId, response);
                break;
            case AppServiceManager.ALGOLIA_SEARCH:
                initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
                break;
        }
    }

    private MstarBrandProductListRequest getBrandProductListRequest() {
        MstarBrandProductListRequest request = new MstarBrandProductListRequest();
        request.setPageSize(pageSize);
        request.setPageNo(pageIndex);
        request.setReturnProduct(AppConstant.YES);
        request.setReturnFacets(AppConstant.YES);
        request.setBrandID(brandId);
        return request;
    }


    private void catlogBannersResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getCategoryBannerList() != null && response.getResult().getCategoryBannerList().size() > 0) {
                setMstarBanners(response.getResult().getCategoryBannerList());
            }
            inflateBannerAndSubCatView();
        }
    }

    private void brandProductDetailsAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            setValuesForBannerAndSubCat(model);
            if (response != null && response.getResult() != null && response.getResult().getBrandDetails() != null && response.getResult().getBrandDetails().getProductDetailsList() != null && response.getResult().getBrandDetails().getProductDetailsList().size() > 0) {
                calculateTotalPageCount(response.getResult().getBrandDetails().getProductCount());
                addProducts(response);
            }
        }
    }

    private void productDetailAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getProductDetails() != null) {
                if (response.getResult().getProductDetails().getAvailabilityStatus().equalsIgnoreCase("A")) {
                    listener.OnAddCartCallBack(response.getResult().getProductDetails());
                }
            }
        }
    }

    private MstarCategoryDetailsRequest getCategoryRequest() {
        MstarCategoryDetailsRequest request = new MstarCategoryDetailsRequest();
        request.setCategoryId(categoryId);
        request.setPageNo(pageIndex);
        request.setPageSize(pageSize);
        request.setProductFieldsetName(AppConstant.ALL);
        request.setReturnProduct(AppConstant.YES);
        request.setSubCategoryDepth(AppConstant.TWO);
        request.setReturnFacets(AppConstant.YES);
        request.setProductsSortOrder(AppConstant.NAME_POPULARITY);
        return request;
    }

    private void categoryListResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult().getCategoryDetails() != null) {
                categoryResponse = new Gson().toJson(response.getResult().getCategoryDetails());
                categoryName = response.getResult().getCategoryDetails().getName();
                listener.setToolbarTitle(!TextUtils.isEmpty(categoryName) ? categoryName : "");
                calculateTotalPageCount(response.getResult().getCategoryDetails().getProductCount());
                setValuesForBannerAndSubCat(response.getResult().getCategoryDetails());
                if (response.getResult().getCategoryDetails().getProductDetailsList() != null && response.getResult().getCategoryDetails().getProductDetailsList().size() > 0) {
                    addProducts(response);
                }
                /*WebEngage Category Viewed Event*/
                if (!webEngageFlag) {
                    webEngageFlag = true;
                    WebEngageHelper.getInstance().categoryViewedEvent(listener.getContext(), response.getResult().getCategoryDetails());
                }
                if (pageIndex == 1 || pageIndex == 0) {
                    initMstarAPIcall(APIServiceManager.C_MSTAR_CATLOG_BANNERS);
                }
            }
        }
    }

    private void setValuesForBannerAndSubCat(MstarCategoryDetails response) {
        if (response.getSubCategories() != null && response.getSubCategories().size() > 0) {
            setSubCategoryList(response.getSubCategories());
        }
        if (!TextUtils.isEmpty(response.getMastheadImagePath())) {
            //banner default view
            ArrayList<String> bannerList = new ArrayList<>();
            imageURLPath = imageURLPath + response.getMastheadImagePath();
            bannerList.add(imageURLPath);
            setDefaultBannerLists(bannerList);
        }
        MstarCategoryDetails categoryDetails = response;
        if (categoryDetails.getFilters() != null && categoryDetails.getFilters().getFacets() != null && categoryDetails.getFilters().getFacets().getBrandList() != null) {
            setBrandFilter(categoryDetails.getFilters().getFacets().getBrandList());

        }
    }

    private void inflateBannerAndSubCatView() {
        if (isMstarCatlogBannerCompleted && isMstarCategoryDetailsCompleted) {
            listener.setBannerView(getDefaultBannerLists(), getMstarBanners());
        }
        listener.setSubCategoryView(getSubCategoryList());
    }

    private void setBrandFilter(List<MstarFacetItemDetails> brandList) {
        this.brandFilterList = brandList;
    }

    private void addProducts(MStarBasicResponseTemplateModel response) {
        List<MStarProductDetails> productList = AppConstant.CATEGORY.equalsIgnoreCase(from) ? response.getResult().getCategoryDetails().getProductDetailsList() : response.getResult().getBrandDetails().getProductDetailsList();
        if (categoryAdapter != null) {
            categoryAdapter.updateList(productList, isClearList());
        } else {
            categoryAdapter = listener.setAdapter(productList, categoryAdapter, getBrandFilterList(), categoryId, from);
            inflateBannerAndSubCatView();
        }

        if (isFacetValueFirst) {
            fetchFacetValueFromAlgolia();
        }
    }

    public ViewTreeObserver.OnScrollChangedListener paginationForOrderList(final NestedScrollView nestedScrollView, final LinearLayoutManager linearLayoutManager) {
        return new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView.getScrollY()));

                if (diff == 0) {
                    if (pageIndex <= totalPageCount) {
                        pageIndex += 1;
                        setClearList(false);
                        getMoreProducts();
                    }
                }

                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                visibleItemListener.viewVisiblePosition(firstVisibleItem, lastVisibleItem);
            }
        };
    }


    private void calculateTotalPageCount(int productCount) {
        totalPageCount = (long) Math.ceil(Double.valueOf(String.valueOf(productCount)) / Double.valueOf(String.valueOf(pageSize)));
    }


    private void getMoreProducts() {
        listener.vmDismissProgress();
        if (isFilterApplied()) {
            initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
        } else if (AppConstant.CATEGORY.equalsIgnoreCase(from)) {
            initMstarAPIcall(APIServiceManager.MSTAR_CATEGORY_DETAILS);
        } else {
            initMstarAPIcall(APIServiceManager.MSTAR_BRAND_PRODUCT_LIST);
        }
    }

    private void getProductDetailRequest(int productCode) {
        this.productCode = productCode;
        initMstarAPIcall(APIServiceManager.MSTAR_PRODUCT_DETAILS);
    }

    private boolean isClearList() {
        return clearList;
    }

    @Override
    public void OnAddCartCallBack(MStarProductDetails productDetails, int position) {
        getProductDetailRequest(productDetails.getProductCode());
        /*Google Tag Manager + FireBase Product Click Event*/
        onGoogleTagManagerEvent(productDetails, position);
    }

    @Override
    public void openManufacturerProductList(Integer manufacturerId, String ManufacturerName, String productType) {
        listener.openManufacturerProductList(manufacturerId, ManufacturerName, !TextUtils.isEmpty(productType) ? productType : "");
    }

    @Override
    public void openProductDetailsPage(int productCode, MStarProductDetails productDetails, int position) {
        /*Google Tag Manager + FireBase Product Click Event*/
        onGoogleTagManagerEvent(productDetails, position);
        listener.openProductDetailsPage(productCode);
    }

    @Override
    public void applyBrandFilter(Integer brandId, String brandName) {
        //CategoryName is send instead of BrandName
        listener.applyBrandFilter(brandId, categoryName, categoryResponse);
    }

    @Override
    public boolean isFromAlgolia() {
        return isFilterApplied();
    }

    private List<MstarSubCategoryResult> getSubCategoryList() {
        return subCategoryList;
    }

    private void setSubCategoryList(List<MstarSubCategoryResult> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }

    private List<String> getDefaultBannerLists() {
        return defaultBannerLists;
    }

    private void setDefaultBannerLists(List<String> defaultBannerLists) {
        this.defaultBannerLists = defaultBannerLists;
    }

    private List<MstarFacetItemDetails> getBrandFilterList() {
        return brandFilterList;
    }

    private List<MstarBanner> getMstarBanners() {
        return mstarBanners;
    }

    private void setMstarBanners(List<MstarBanner> mstarBannersList) {
        this.mstarBanners = mstarBannersList;
    }


    @Override
    public void onRetryClickListener() {
        listener.showNoNetworkView(false);
        listener.showWebError(false);
        onRetry(failedTransactionId);
    }

    /**
     * This method return facetFilter option like category ids,manufacturer id and brand id
     *
     * @return facetFilter jsonArray
     */
    private JSONArray getFacetFilterQuery() {
        JSONArray facetFilter = new JSONArray();
        switch (from) {
            case AppConstant.CATEGORY:
                facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_IDS + ":" + CategoryViewModel.categoryId);
                break;
            case AppConstant.BRAND:
                facetFilter.put(AppConstant.ALGOLIA_FILTER_KEY_BRAND_ID + ":" + CategoryViewModel.brandId);
                break;
        }
        return facetFilter;
    }

    /**
     * set filter query based on filter selection
     *
     * @return filter query
     */
    private String getQuery() {
        StringBuilder manufactureQuery = new StringBuilder();
        StringBuilder priceQuery = new StringBuilder();
        StringBuilder availabilityQuery = new StringBuilder();
        StringBuilder categoryQuery = new StringBuilder();
        StringBuilder brandQuery = new StringBuilder();
        StringBuilder discountQuery = new StringBuilder();

        if (MstarFilterHelper.getAvailabilitySelection() != null && MstarFilterHelper.getAvailabilitySelection().size() > 0) {
            for (String availability : MstarFilterHelper.getAvailabilitySelection()) {
                if (availabilityQuery.length() > 1) {
                    availabilityQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK).append("\":\"").append(availability).append("\"");
                } else {
                    availabilityQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK + "\":\"" + availability + "\"");
                }
            }
        }
        if (MstarFilterHelper.getCategorySelection() != null && MstarFilterHelper.getCategorySelection().size() > 0) {
            for (String categoryName : MstarFilterHelper.getCategorySelection()) {
                if (categoryQuery.length() > 1) {
                    categoryQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_CATEGORIES).append("\":\"").append(categoryName).append("\"");
                } else {
                    if (availabilityQuery.length() > 1)
                        categoryQuery = new StringBuilder(" " + AppConstant.AND + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_CATEGORIES + "\":\"" + categoryName + "\"");
                    else
                        categoryQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_CATEGORIES + "\":\"" + categoryName + "\"");
                }
            }
        }

        if (MstarFilterHelper.getManufactureSelection() != null && MstarFilterHelper.getManufactureSelection().size() > 0) {
            for (String manufacture_name : MstarFilterHelper.getManufactureSelection()) {
                if (manufactureQuery.length() > 1) {
                    manufactureQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME).append("\":\"").append(manufacture_name).append("\"");
                } else {
                    if (availabilityQuery.length() > 1 || categoryQuery.length() > 1)
                        manufactureQuery = new StringBuilder(" " + AppConstant.AND + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME + "\":\"" + manufacture_name + "\"");
                    else
                        manufactureQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME + "\":\"" + manufacture_name + "\"");
                }
            }
        }

        if (MstarFilterHelper.getBrandSelection() != null && MstarFilterHelper.getBrandSelection().size() > 0) {
            for (String brand : MstarFilterHelper.getBrandSelection()) {
                if (brandQuery.length() > 1) {
                    brandQuery.append(" ").append(AppConstant.OR).append(" ").append("\"").append(AppConstant.ALGOLIA_FILTER_KEY_BRAND).append("\":\"").append(brand).append("\"");
                } else {
                    if (availabilityQuery.length() > 1 || categoryQuery.length() > 1 || manufactureQuery.length() > 1)
                        brandQuery = new StringBuilder(" " + AppConstant.AND + " " + "\"" + AppConstant.ALGOLIA_FILTER_KEY_BRAND + "\":\"" + brand + "\"");
                    else
                        brandQuery = new StringBuilder("\"" + AppConstant.ALGOLIA_FILTER_KEY_BRAND + "\":\"" + brand + "\"");
                }
            }
        }

        if (MstarFilterHelper.getPriceSelection() != null && MstarFilterHelper.getPriceSelection().size() > 0) {
            for (String price : MstarFilterHelper.getPriceSelection()) {
                if (price.contains("-")) {
                    String[] priceSplit = price.split("-");
                    if (priceQuery.length() > 1) {
                        priceQuery.append(" ").append(AppConstant.OR).append(" ").append(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE).append(":").append(" ").append(priceSplit[0]).append(" ").append(AppConstant.TO).append(" ").append(priceSplit[1]);
                    } else {
                        if (availabilityQuery.length() > 1 || categoryQuery.length() > 1 || manufactureQuery.length() > 1 || brandQuery.length() > 1)
                            priceQuery = new StringBuilder(" " + AppConstant.AND + " " + AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE + ": " + priceSplit[0] + " " + AppConstant.TO + " " + priceSplit[1]);
                        else
                            priceQuery = new StringBuilder(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE + ": " + priceSplit[0] + " " + AppConstant.TO + " " + priceSplit[1]);
                    }
                }
            }
        }

        if (MstarFilterHelper.getDiscountSelection() != null && MstarFilterHelper.getDiscountSelection().size() > 0) {
            for (String discount : MstarFilterHelper.getDiscountSelection()) {
                if (discount.contains(" ")) {
                    String[] discountSplit = discount.split(" ");
                    String discountFrom = discountSplit.length > 0 ? discountSplit[0].replace("%", "").trim() : "";
                    String discountTo = discountSplit.length > 1 ? discountSplit[2].replace("%", "").trim() : "";
                    if (!TextUtils.isEmpty(discountFrom) && !TextUtils.isEmpty(discountTo)) {
                        if (discountQuery.length() > 1) {
                            discountQuery.append(" ").append(AppConstant.OR).append(" ").append(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT).append(":").append(" ").append(discountFrom).append(" ").append(AppConstant.TO).append(" ").append(discountTo);
                        } else {
                            if (availabilityQuery.length() > 1 || categoryQuery.length() > 1 || manufactureQuery.length() > 1 || brandQuery.length() > 1 || priceQuery.length() > 1)
                                discountQuery = new StringBuilder(" " + AppConstant.AND + " " + AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT + ": " + discountFrom + " " + AppConstant.TO + " " + discountTo);
                            else
                                discountQuery = new StringBuilder(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT + ": " + discountFrom + " " + AppConstant.TO + " " + discountTo);
                        }
                    }
                }
            }
        }
        return availabilityQuery.toString() + categoryQuery.toString() + manufactureQuery.toString() + brandQuery.toString() + priceQuery.toString() + discountQuery.toString();
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(listener.getContext());
    }

    private SortOptionLabel getSortOption() {
        return sortOption;
    }

    public void setSortOption(SortOptionLabel sortOption) {
        this.sortOption = sortOption;//set selected sort option
    }

    public List<SortOptionLabel> getSortOptionList() {
        return sortOptionList;
    }

    public void setSortOptionList(List<SortOptionLabel> sortOptionList) {
        this.sortOptionList = sortOptionList;//set sort option list
    }

    /**
     * This method return sort index based on sort selection
     *
     * @return algolia sort index
     */
    private String getAlgoliaSortIndex() {
        if (getSortOptionList() != null && getSortOptionList().size() > 0) {
            for (SortOptionLabel sortOptionLabel : getSortOptionList()) {
                if (!TextUtils.isEmpty(getSortOption().getKey()) && sortOptionLabel.getKey().equals(getSortOption().getKey()))
                    return sortOptionLabel.getValue();
            }
        }
        return "";
    }

    private String getFilterQuery() {
        return filterQuery;
    }

    public void setFilterQuery(String filterQuery) {
        this.filterQuery = filterQuery;//set filter query
    }

    /**
     * This method used to make algolia sorting based on sort selection
     *
     * @param sortOption selected sort option
     */
    public void onSortFilterApplied(SortOptionLabel sortOption) {
        setSortOption(sortOption);
        setFilterApplied(true);
        pageIndex = 0;
        setClearList(true);
        initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
    }

    /**
     * Handling algolia response
     *
     * @param mstarAlgoliaResponse algolia response
     */
    public void onAlgoliaDataAvailable(MstarAlgoliaResponse mstarAlgoliaResponse) {
        if (mstarAlgoliaResponse != null && mstarAlgoliaResponse.getNbHits() > 0) {
            totalPageCount = (long) mstarAlgoliaResponse.getNbPages() - 1;//total pages

            if (!isFacetValueFirst)
                setAlgoliaHitList(mstarAlgoliaResponse.getAlgoliaResultList());

            setFacetFilterOption(mstarAlgoliaResponse);
            listener.showEmptyView(false);
        } else {
            listener.showEmptyView(true);
        }
    }

    private void setFacetFilterOption(MstarAlgoliaResponse mstarAlgoliaResponse) {
        if (isFacetValueFirst && mstarAlgoliaResponse.getFacetList() != null && mstarAlgoliaResponse.getFacetList().size() > 0) {
            this.isFacetValueFirst = false;
            Map<String, Map<String, String>> filterFacetMap = new HashMap<>();
            if (getSubCategoryList() != null && getSubCategoryList().size() > 0) {
                Map<String, String> subCategoryMap = new HashMap<>();
                for (MstarSubCategoryResult subCategoryResult : getSubCategoryList()) {
                    subCategoryMap.put(subCategoryResult.getName(), String.valueOf(subCategoryResult.getProductCount()));
                }
                filterFacetMap.put(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL, subCategoryMap);
            }

            filterFacetMap.putAll(mstarAlgoliaResponse.getFacetList());
            setAlgoliaFilterOption(filterFacetMap);
        }
    }

    public Map<String, Map<String, String>> getAlgoliaFilterOption() {
        return algoliaFilterOption;
    }

    private void setAlgoliaFilterOption(Map<String, Map<String, String>> algoliaFilterOption) {
        this.algoliaFilterOption = algoliaFilterOption;
    }

    private void setAlgoliaHitList(List<MstarAlgoliaResult> algoliaHitList) {
        if (algoliaHitList != null && algoliaHitList.size() > 0) {
            List<MStarProductDetails> productDetailsList = new ArrayList<>();
            for (MstarAlgoliaResult algoliaResult : algoliaHitList) {
                MStarProductDetails productDetails = new MStarProductDetails();
                productDetails.setSellingPrice(algoliaResult.getSellingPrice());
                productDetails.setBrandName(!TextUtils.isEmpty(algoliaResult.getBrand()) ? algoliaResult.getBrand() : "");
                productDetails.setDisplayName(!TextUtils.isEmpty(algoliaResult.getDisplayName()) ? algoliaResult.getDisplayName() : "");
                productDetails.setManufacturerName(!TextUtils.isEmpty(algoliaResult.getManufacturerName()) ? algoliaResult.getManufacturerName() : "");
                productDetails.setMrp(algoliaResult.getMrp());
                productDetails.setMaxQtyInOrder(algoliaResult.getMaxQtyInOrder());
                productDetails.setAvailabilityStatus(!TextUtils.isEmpty(algoliaResult.getAvailabilityStatus()) ? algoliaResult.getAvailabilityStatus() : "");
                productDetails.setProductType(!TextUtils.isEmpty(algoliaResult.getProductType()) ? algoliaResult.getProductType() : "");
                productDetails.setFormulationType(!TextUtils.isEmpty(algoliaResult.getFormulationType()) ? algoliaResult.getFormulationType() : "");
                productDetails.setProductCode(algoliaResult.getProductCode());
                productDetails.setManufacturerId(algoliaResult.getManufacturerId());
                productDetails.setSchedule(!TextUtils.isEmpty(algoliaResult.getSchedule()) ? algoliaResult.getSchedule() : "");
                productDetails.setBrandId(!TextUtils.isEmpty(algoliaResult.getBrandId()) ? algoliaResult.getBrandId() : "");
                productDetails.setUrlPath(!TextUtils.isEmpty(algoliaResult.getUrlPath()) ? algoliaResult.getUrlPath() : "");
                productDetails.setPackLabel(!TextUtils.isEmpty(algoliaResult.getPackLabel()) ? algoliaResult.getPackLabel() : "");
                productDetails.setDiscount(algoliaResult.getDiscountRate());
                productDetails.setRxRequired(algoliaResult.getRxRequired() == 1);
                List<String> imagePath = new ArrayList<>();
                imagePath.add(!TextUtils.isEmpty(algoliaResult.getThumbnailUrl()) ? algoliaResult.getThumbnailUrl() : "");
                productDetails.setImagePaths(imagePath);
                MstarBaseIdUrl mstarBaseIdUrl = new MstarBaseIdUrl();
                mstarBaseIdUrl.setName(!TextUtils.isEmpty(algoliaResult.getManufacturerName()) ? algoliaResult.getManufacturerName() : "");
                mstarBaseIdUrl.setId(algoliaResult.getManufacturerId());
                productDetails.setManufacturer(mstarBaseIdUrl);
                if (algoliaResult.getCategoryTreeList() != null && algoliaResult.getCategoryTreeList().size() > 0) {
                    List<MStarCategory> categoriesList = new ArrayList<>();
                    MStarCategory mStarCategory = new MStarCategory();
                    mStarCategory.setName("");
                    List<MStarBreadCrumb> breadCrumbs = new ArrayList<>();
                    int level = 0;
                    for (Map.Entry<String, ArrayList<Object>> entry : algoliaResult.getCategoryTreeList().entrySet()) {
                        ArrayList<Object> levelList = entry != null && entry.getValue() != null && entry.getValue().size() > 0 ? entry.getValue() : new ArrayList<Object>();
                        if (levelList != null && levelList.size() > 0 && !TextUtils.isEmpty(levelList.get(0).toString().trim())) {
                            String[] categoryList = levelList.get(0).toString().split("///");
                            MStarBreadCrumb mStarBreadCrumb = new MStarBreadCrumb();
                            mStarBreadCrumb.setName(categoryList.length > 0 ? !TextUtils.isEmpty(Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1)) ? Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1) : "" : "");
                            mStarBreadCrumb.setLevel(level);
                            breadCrumbs.add(mStarBreadCrumb);
                            mStarCategory.setBreadCrumbs(breadCrumbs);
                        }
                        level++;
                    }
                    categoriesList.add(mStarCategory);
                    productDetails.setCategories(categoriesList);
                }
                productDetailsList.add(productDetails);
            }
            setSearchAdapter(productDetailsList);
            setClearList(true);
        }
    }

    private void setSearchAdapter(List<MStarProductDetails> categoryList) {
        if (categoryAdapter != null) {
            categoryAdapter.updateList(categoryList, isClearList());
        } else {
            categoryAdapter = listener.setAdapter(categoryList, categoryAdapter, new ArrayList<MstarFacetItemDetails>(), categoryId, from);
        }
    }

    public void fetchFacetValueFromAlgolia() {
        pageIndex = 0;
        initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
    }

    /**
     * This method used to make algolia filter based on filter selection
     */
    public void onApplyFilterDataAvailable() {
        setFilterApplied(true);
        setFilterQuery(getQuery());
        pageIndex = 0;
        setClearList(true);
        initMstarAPIcall(AppServiceManager.ALGOLIA_SEARCH);
    }

    public boolean isFilterApplied() {
        return isFilterApplied;
    }

    private void setFilterApplied(boolean filterApplied) {
        isFilterApplied = filterApplied;//set filter applied or not
    }

    public void setClearList(boolean clearList) {
        this.clearList = clearList;
    }

    private void onGoogleTagManagerEvent(MStarProductDetails productDetails, int position) {
        FireBaseAnalyticsHelper.getInstance().logFireBaseProductClickAndSelectionEvent(listener.getContext(), productDetails, position, FireBaseAnalyticsHelper.WELLNESS + "_" + categoryName);
    }

    public static void setVisibleItemListener(VisibleItemListener itemListener) {
        visibleItemListener = itemListener;
    }

    public interface CategoryListener {

        void OnAddCartCallBack(MStarProductDetails productDetails);

        void openManufacturerProductList(Integer manufacturerId, String ManufacturerName, String productType);

        void openProductDetailsPage(int productCode);

        void applyBrandFilter(Integer brandId, String brandName, String categoryResponse);

        void vmShowProgress();

        void vmDismissProgress();

        boolean isNetworkConnected();

        void setSubCategoryView(List<MstarSubCategoryResult> subCategoryList);

        void setBannerView(List<String> defaultBannerLists, List<MstarBanner> mstarBanners);

        void showNoNetworkView(boolean isNoNetwork);

        void showWebError(boolean isWebError);

        MstarCategoryAdapter setAdapter(List<MStarProductDetails> productList, MstarCategoryAdapter categoryAdapter, List<MstarFacetItemDetails> brandFilterList, int categoryId, String from);

        Context getContext();

        void showEmptyView(boolean isEmpty);

        void setToolbarTitle(String s);
    }
}
