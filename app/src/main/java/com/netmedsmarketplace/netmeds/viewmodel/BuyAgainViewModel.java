package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.BuyAgainAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.util.ArrayList;
import java.util.List;

public class BuyAgainViewModel extends AppViewModel implements BuyAgainAdapter.BuyAgainAdapterListener {

    private BuyAgainViewModelListener callback;
    private BasePreference basePreference;
    private MStarCartDetails cartDetails;
    private MStarProductDetails productDetails;

    private String productIdList;
    private int addProductCode = 0;
    private int removeProductCode = 0;
    private int productQty = 0;
    private int selectedQty = 0;
    private boolean isAddOrRemovedProduct;
    private boolean isRemoveAll;
    private String webEngageTrackEvent;


    public BuyAgainViewModel(@NonNull Application application) {
        super(application);
    }

    public void initiateModel(BuyAgainViewModelListener callback, BasePreference basePreference) {
        this.callback = callback;
        this.basePreference = basePreference;
    }

    private void initMstarAPICall(int transactionId) {
        boolean isConnected = callback.isNetworkConnected();
        callback.showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_PAST_MEDICINES:
                    callback.showLoader();
                    APIServiceManager.getInstance().mStargetBuyAgainProducts(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                    APIServiceManager.getInstance().mStarProductDetailsForIdList(this, productIdList, APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
                    break;
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    APIServiceManager.getInstance().mStarGetCartDetails(this, basePreference.getMstarBasicHeaderMap(), null, transactionId);
                    break;
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    callback.showLoader();
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basePreference.getMstarBasicHeaderMap(), addProductCode, productQty, null);
                    break;
                case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                    callback.showLoader();
                    APIServiceManager.getInstance().mStarRemoveProductFromCart(this, basePreference.getMstarBasicHeaderMap(), removeProductCode, productQty, null);
                    break;
                case APIServiceManager.MSTAR_CREATE_CART:
                    callback.showLoader();
                    APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                    break;
            }
        } else {
            callback.showNoNetworkView(false);
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_PAST_MEDICINES:
                onBuyAgainResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST:
                callback.dismissLoader();
                onProductListAvailable(data);
                break;
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                onCartDetailsAvailable(data);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                callback.dismissLoader();
                onProductAddedToCart(data);
                break;
            case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                callback.dismissLoader();
                onProductRemoveFromCart(data);
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                callback.dismissLoader();
                onCreateCartResponse(data);
                break;

        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callback.dismissLoader();
        callback.showWebserviceErrorView(true);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        initMstarAPICall(APIServiceManager.C_MSTAR_PAST_MEDICINES);
        callback.showWebserviceErrorView(false);
    }


    private void onBuyAgainResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                if (response.getResult().getSku() != null) {
                    productIdList = response.getResult().getSku();
                    initMstarAPICall(APIServiceManager.MSTAR_GET_CART_DETAILS);

                } else {
                    callback.dismissLoader();
                    callback.hideProceedbutton(productIdList);
                }
            }
        }
    }

    private void onCartDetailsAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (responseTemplateModel != null && responseTemplateModel.getResult() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())
                    && responseTemplateModel.getResult().getCartDetails() != null) {
                setCartDetailsProperties(responseTemplateModel);
            }
            if (!isAddOrRemovedProduct) {
                initMstarAPICall(APIServiceManager.MSTAR_GET_PRODUCT_FOR_ID_LIST);
            } else {
                callback.dismissLoader();
                if (basePreference.getCartCount() == 0)
                    callback.hideProceedbutton(productIdList);
            }
        }
    }

    private void setCartDetailsProperties(MStarBasicResponseTemplateModel responseTemplateModel) {
        cartDetails = responseTemplateModel.getResult().getCartDetails();
        if (cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
            basePreference.setCartCount(cartDetails.getLines().size());
            webEngageEvent(cartDetails);
            callback.animateProceedButton();
        } else {
            basePreference.setCartCount(0);
        }
    }

    private void setWebEngageTrackEvent(String webEngageTrackEvent) {
        this.webEngageTrackEvent = webEngageTrackEvent;
    }

    private void webEngageEvent(MStarCartDetails cartDetails) {
        if (!TextUtils.isEmpty(webEngageTrackEvent)) {
            switch (webEngageTrackEvent) {
                case WebEngageHelper.EVENT_NAME_CART_UPDATED:
                    onCartUpdatedEvent(cartDetails);
                    break;
            }
        }
    }

    /*WebEngage Cart update event*/
    private void onCartUpdatedEvent(MStarCartDetails cartDetails) {
        cartDetails.setPageName(callback.getContext().getString(R.string.text_products_ordered));
        WebEngageHelper.getInstance().cartUpdateEvent(callback.getContext(), cartDetails, cartDetails.getLines());
        setWebEngageTrackEvent("");
    }

    private void onProductListAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                if (response.getResult().getProductDetailsList() != null && !response.getResult().getProductDetailsList().isEmpty()) {
                    List<MStarProductDetails> buyAgainProductDetailsList = new ArrayList<>(response.getResult().getProductDetailsList());
                    if (cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty()) {
                        for (MStarProductDetails cartProductDetails : cartDetails.getLines()) {
                            for (MStarProductDetails buyAgainProductDetails : buyAgainProductDetailsList) {
                                if (buyAgainProductDetails.getProductCode() == cartProductDetails.getProductCode()) {
                                    buyAgainProductDetails.setCartQuantity(cartProductDetails.getCartQuantity());
                                    buyAgainProductDetailsList.set(buyAgainProductDetailsList.indexOf(buyAgainProductDetails), buyAgainProductDetails);
                                }
                            }

                        }
                    }
                    callback.setBuyAgainAdapter(buyAgainProductDetailsList, this);

                }
            }
        }
    }


    private void onProductAddedToCart(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (responseTemplateModel != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())) {
                isAddOrRemovedProduct = true;
                initMstarAPICall(APIServiceManager.MSTAR_GET_CART_DETAILS);
                callback.showMessageWithAction("");
                callback.updateAdapter(productDetails, productQty, selectedQty);
            } else if (responseTemplateModel != null && responseTemplateModel.getReason() != null) {
                isAddOrRemovedProduct = false;
                if (!TextUtils.isEmpty(responseTemplateModel.getReason().getReason_code()) && AppConstant.CART_NOT_FOUND.equalsIgnoreCase(responseTemplateModel.getReason().getReason_code())) {
                    initMstarAPICall(APIServiceManager.MSTAR_CREATE_CART);
                } else {
                    callback.showErrorMessage(!TextUtils.isEmpty(responseTemplateModel.getReason().getReason_eng()) ? responseTemplateModel.getReason().getReason_eng() : "");
                }
            }
        }
    }

    private void onProductRemoveFromCart(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (responseTemplateModel != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())) {
                isAddOrRemovedProduct = true;
                if (!isRemoveAll)
                    callback.updateAdapter(productDetails, productQty, selectedQty);
                else callback.updateAdapter(productDetails, productQty, 0);
                initMstarAPICall(APIServiceManager.MSTAR_GET_CART_DETAILS);
            } else if (responseTemplateModel != null && responseTemplateModel.getReason() != null) {
                isAddOrRemovedProduct = false;
                callback.showErrorMessage(!TextUtils.isEmpty(responseTemplateModel.getReason().getReason_eng()) ? responseTemplateModel.getReason().getReason_eng() : "");
            }
        }

    }

    private void onCreateCartResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
                basePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
                initMstarAPICall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
            }
        }
    }

    private void addProductToCart(int productCode, int quantity) {
        this.addProductCode = productCode;
        this.productQty = quantity;
        setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_UPDATED);
        initMstarAPICall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
    }

    private void removeProductFromCart(int productCode, int quantity) {
        this.removeProductCode = productCode;
        this.productQty = quantity;
        setWebEngageTrackEvent(WebEngageHelper.EVENT_NAME_CART_UPDATED);
        initMstarAPICall(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
    }

    public void proceedToCheckOut() {
        callback.navigateToCart();
    }

    public void onResumeActivity() {
        isAddOrRemovedProduct = false;
        initMstarAPICall(APIServiceManager.C_MSTAR_PAST_MEDICINES);
    }

    public List<MStarProductDetails> analyzeProductDetailsAndUpdateList(List<MStarProductDetails> productDetailsList) {
        for (MStarProductDetails productDetails : productDetailsList) {
            productDetails.setNotAbleToAddToCartStatus(CommonUtils.isProductNotAddToCartStatus(productDetails.getAvailabilityStatus()));
            productDetails.setNotAddedToCartMessage(callback.getContext().getString(CommonUtils.isProductOutOfStock(productDetails.getAvailabilityStatus()) ? R.string.text_out_of_stock :
                    CommonUtils.isProductNotAvailable(productDetails.getAvailabilityStatus()) ? R.string.text_stock_not_available : CommonUtils.isProductNotForSale(productDetails.getAvailabilityStatus()) ? R.string.text_stock_not_for_sale : R.string.text_available));
            productDetails.setQuantityPickerVisibility(productDetails.getCartQuantity() > 0 && !productDetails.isNotAbleToAddToCartStatus() ? View.VISIBLE : View.GONE);
            productDetails.setAddToCartVisibility(!productDetails.isNotAbleToAddToCartStatus() && productDetails.getCartQuantity() == 0 ? View.VISIBLE : View.GONE);
            productDetailsList.set(productDetailsList.indexOf(productDetails), productDetails);
        }
        return productDetailsList;
    }

    @Override
    public void onProductView(int productCode) {
        callback.onProductView(productCode);
    }

    @Override
    public void onRemoveProductFromCart(MStarProductDetails productDetails, int quantity, boolean isRemoveAll) {
        if (quantity > 0) {
            this.isRemoveAll = isRemoveAll;
            this.productDetails = productDetails;
            removeProductFromCart(productDetails.getProductCode(), quantity);
        }
    }

    @Override
    public void decreaseQuantity(MStarProductDetails productDetails, int quantity, int selectedQty) {
        if (quantity > 0) {
            this.selectedQty = selectedQty;
            this.isRemoveAll = false;
            this.productDetails = productDetails;
            removeProductFromCart(productDetails.getProductCode(), quantity);
        }
    }

    @Override
    public void increaseQuantity(MStarProductDetails productDetails, int quantity, int selectedQty) {
        if (quantity > 0) {
            this.selectedQty = selectedQty;
            this.isRemoveAll = false;
            this.productDetails = productDetails;
            addProductToCart(productDetails.getProductCode(), quantity);
        }

    }

    public interface BuyAgainViewModelListener {
        boolean isNetworkConnected();

        void showNoNetworkView(boolean isConnected);

        void showLoader();

        void dismissLoader();

        void showErrorMessage(String message);

        void showMessageWithAction(String message);

        void showWebserviceErrorView(boolean isWebserviceError);

        void setBuyAgainAdapter(List<MStarProductDetails> productDetailsList, BuyAgainAdapter.BuyAgainAdapterListener listener);

        void hideProceedbutton(String productIdList);

        void navigateToCart();

        Context getContext();

        void animateProceedButton();

        void onProductView(int productCode);

        void updateAdapter(MStarProductDetails productDetails, int productQty, int selectedQty);
    }
}