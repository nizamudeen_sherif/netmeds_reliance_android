package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.AlgoliaClickAndConversionHelper;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.math.BigDecimal;

public class AddCartBottomSheetDialogViewModel extends AppViewModel {

    private AddCartBottomSheetListener bottomSheetListener;
    private MStarProductDetails productDetails;
    private BasePreference basePreference;
    private int quantity = 1;

    public AddCartBottomSheetDialogViewModel(Application application) {
        super(application);
    }

    @Override
    public void onRetryClickListener() {

    }

    public void onBottomSheetDismiss() {
        bottomSheetListener.vmBottomSheetDismiss();
    }

    public void init(BasePreference basePreference, MStarProductDetails productDetails, AddCartBottomSheetListener bottomSheetListener) {
        this.productDetails = productDetails;
        this.bottomSheetListener = bottomSheetListener;
        this.basePreference = basePreference;
        getImageUrl(productDetails);
        bottomSheetListener.loadNumberPicker();
    }

    private void initiateAPICall(int transactionId) {
        if (bottomSheetListener.isNetworkConnected()) {
            bottomSheetListener.vmBottomSheetShowProgress();
            switch (transactionId) {
                case APIServiceManager.MSTAR_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basePreference.getMstarBasicHeaderMap(), productDetails.getProductCode(), quantity,
                            isSubscription() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() : null);
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_CART:
                createMStarCartResponse(data);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                addToCartResponse(data);
                break;
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_CART:
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                unableToAddCartError();
                break;
            default:
                bottomSheetListener.vmBottomSheetDismissProgress();
                break;
        }
    }

    private void addToCartResponse(String data) {
        MStarBasicResponseTemplateModel addToCartResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addToCartResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addToCartResponse.getStatus())) {
            /*WebEngage AddToCart Event*/
            WebEngageHelper.getInstance().addToCartEvent(getApplication(), productDetails, quantity, false);
            /*Facebook Pixel AddToCart Event*/
            FacebookPixelHelper.getInstance().logAddedToCartEvent(getApplication(), productDetails, quantity);
            /*Google Tag Manager + FireBase AddToCart Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseAddToCartEvent(getApplication(),productDetails,quantity);

            bottomSheetListener.vmBottomSheetAddCart();
            bottomSheetListener.updateCartCount(1);
        } else if (addToCartResponse != null && addToCartResponse.getReason() != null) {
            bottomSheetListener.vmBottomSheetDismissProgress();
            bottomSheetListener.vmBottomSheetDismiss();
            if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(addToCartResponse.getReason().getReason_code())) {
                bottomSheetListener.vmShowAlert(getApplication().getResources().getString(R.string.text_medicine_max_limit));
            } else if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(addToCartResponse.getReason().getReason_code())) {
                bottomSheetListener.vmShowAlert(getApplication().getResources().getString(R.string.text_medicine_out_of_stock));
            } else {
                bottomSheetListener.vmShowAlert(addToCartResponse.getReason().getReason_eng());
            }
        } else {
            unableToAddCartError();
        }
    }

    private void unableToAddCartError() {
        bottomSheetListener.vmBottomSheetDismissProgress();
        bottomSheetListener.vmBottomSheetDismiss();
        bottomSheetListener.vmShowAlert(getApplication().getResources().getString(R.string.text_unable_to_add_to_cart));
    }

    private void createMStarCartResponse(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            basePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            checkCartStatusToAddToCart();
        }
    }

    public void addToCart() {
        if (!M2Helper.getInstance().isM2Order()) {
            if (PaymentHelper.isIsTempCart() || basePreference.getMStarCartId() > 0) {
                checkCartStatusToAddToCart();
            } else {
                initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
            }
        } else {
            checkCartStatusToAddToCart();
        }
        //Algolia conversion event
        postAlgoliaConversionEvent(productDetails);
    }

    /**
     * Checking intent from search activity or other activity.
     * If condition true algolia analytics event send to the algolia server.
     * If condition false event not send
     *
     * @param productDetails details of the product
     */
    private void postAlgoliaConversionEvent(MStarProductDetails productDetails) {
        if (bottomSheetListener.isFromSearchActivity()) {
            String productCode = productDetails != null && productDetails.getProductCode() > 0 ? String.valueOf(productDetails.getProductCode()) : "";
            AlgoliaClickAndConversionHelper.getInstance().onAlgoliaConversionEvent(getApplication(), productCode, basePreference.getAlgoliaQueryId());
        }
    }

    private void checkCartStatusToAddToCart() {
        initiateAPICall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
    }

    public String drugName() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getDisplayName()) ? productDetails.getDisplayName() : "";
    }

    public String categoryName() {
        return productDetails != null && productDetails.getCategories() != null && !productDetails.getCategories().isEmpty() && !TextUtils.isEmpty(productDetails.getCategories().get(0).getName()) ? productDetails.getCategories().get(0).getName() : "";
    }

    public String drugDetail() {
        return productDetails != null && !TextUtils.isEmpty(productDetails.getPackLabel()) ? productDetails.getPackLabel() : "";
    }

    public String manufacturerName() {
        return productDetails != null && productDetails.getManufacturer() != null &&
                !TextUtils.isEmpty(productDetails.getManufacturer().getName()) ? AppConstant.MFR + productDetails.getManufacturer().getName() : productDetails != null && !TextUtils.isEmpty(productDetails.getManufacturerName()) ? AppConstant.MFR + productDetails.getManufacturerName() : "";
    }

    @SuppressLint("DefaultLocale")
    public String algoliaPrice() {
        return productDetails != null ? CommonUtils.getPriceInFormat(productDetails.getSellingPrice()) : "";
    }

    @SuppressLint("DefaultLocale")
    public String strikePrice() {
        return productDetails != null && isDiscountAvailable() ? CommonUtils.getPriceInFormat(productDetails.getMrp()) : "";
    }

    public boolean isDiscountAvailable() {
        return productDetails != null && productDetails.getDiscount().compareTo(BigDecimal.ZERO) > 0;
    }


    public void getImageUrl(MStarProductDetails productDetails) {
        MStarBasicResponseTemplateModel resultTemplateModel = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        if (productDetails != null && productDetails.getImagePaths() != null && !productDetails.getImagePaths().isEmpty() && resultTemplateModel != null
                && resultTemplateModel.getResult() != null && !TextUtils.isEmpty(resultTemplateModel.getResult().getProductImageUrlBasePath())) {
            bottomSheetListener.loadProductImage(resultTemplateModel.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_75 + "/" + productDetails.getImagePaths().get(0));
        }
    }

    public boolean isRxRequired() {
        return productDetails != null && productDetails.isRxRequired();
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private boolean isSubscription() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag();
    }

    private String fireBaseEventProductCategory() {
        if (productDetails != null) {
            if (productDetails.getProductType().equalsIgnoreCase(AppConstant.OTC_ORDER)) {
                return AppConstant.OTC;
            } else if (productDetails.getProductType().equalsIgnoreCase(AppConstant.PRESCRIPTION_STATUS_PENDING)) {
                if (productDetails.isRxRequired()) {
                    return AppConstant.PHARMA;
                } else
                    return AppConstant.NON_PHARMA;
            }
        }
        return "";
    }

    public interface AddCartBottomSheetListener {
        void vmBottomSheetDismiss();

        void vmBottomSheetAddCart();

        void vmBottomSheetShowProgress();

        void vmBottomSheetDismissProgress();

        void vmShowAlert(String message);

        void updateCartCount(int count);

        boolean isNetworkConnected();

        void loadNumberPicker();

        void loadProductImage(String url);

        boolean isFromSearchActivity();
    }
}
