package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.databinding.ActivityOfferBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OfferViewModel extends AppViewModel {

    private final MutableLiveData<MStarBasicResponseTemplateModel> offerMutableLiveData = new MutableLiveData<>();
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ActivityOfferBinding binding;
    private OfferListener listener;

    private Map<String, List<MstarNetmedsOffer>> offerList;

    public OfferViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getOfferMutableData() {
        return offerMutableLiveData;
    }

    public void init(Context context, ActivityOfferBinding binding, OfferListener listener, boolean isAgainLoadOfferPage) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        if (isAgainLoadOfferPage) {
            listener.initiateTabAndFragment(null);
        } else {
            onRetryClickListener();
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        fetchOfferFromServer();
    }

    public void fetchOfferFromServer() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            listener.showLoadingProgress();
            APIServiceManager.getInstance().mstarGetAllOffers(this, 0);
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == APIServiceManager.C_MSTAR_ALL_OFFERS) {
            initOffers(data);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.dismissLoadingProgress();
        showWebserviceErrorView();
    }

    private void initOffers(String data) {
        listener.dismissLoadingProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel offerResponseModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(offerResponseModel.getStatus())) {
                offerMutableLiveData.setValue(offerResponseModel);
            }
        }
    }

    public void loadOffer(MStarBasicResponseTemplateModel response) {
        if (response != null && response.getResult()!=null && response.getResult().getOfferList()!=null&&response.getResult().getOfferList().size()>0) {
            listener.initiateTabAndFragment(response.getResult().getOfferList());
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        binding.offerViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.offerNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView() {
        binding.offerViewContent.setVisibility(View.GONE);
        binding.offerApiErrorView.setVisibility(View.VISIBLE);
    }

    public Map<String, List<MstarNetmedsOffer>> getClarifiedOfferList(List<MstarNetmedsOffer> result) {
        offerList = new HashMap<>();
        for (MstarNetmedsOffer model : result) {
            getParticularOffer(model, AppConstant.S_FLAG_MEDICINE);
            getParticularOffer(model, AppConstant.S_FLAG_DIAGNOSTICS);
            getParticularOffer(model, AppConstant.S_FLAG_CONSULTATION);
        }
        return offerList;
    }

    private void getParticularOffer(MstarNetmedsOffer model, String flag) {
        if (model.getSectionType().contains(flag)) {
            if (offerList.containsKey(flag)) {
                offerList.get(flag).add(model);
            } else {
                List<MstarNetmedsOffer> modelList = new ArrayList<>();
                modelList.add(model);
                offerList.put(flag, modelList);
            }
        }
    }

    public interface OfferListener {
        void showLoadingProgress();

        void dismissLoadingProgress();

        void initiateTabAndFragment(List<MstarNetmedsOffer> result);
    }
}
