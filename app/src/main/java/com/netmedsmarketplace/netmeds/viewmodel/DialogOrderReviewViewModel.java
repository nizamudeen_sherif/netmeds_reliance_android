package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogOrderReviewBinding;
import com.nms.netmeds.base.BaseViewModel;

public class DialogOrderReviewViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private DialogOrderReviewBinding binding;
    private DialogOrderReviewViewModelListener listener;


    private int ratingCount;

    public DialogOrderReviewViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, DialogOrderReviewBinding binding, DialogOrderReviewViewModelListener listener, int ratingCount) {
        this.context = context;
        this.listener = listener;
        this.binding = binding;
        this.ratingCount = ratingCount;
        updateRatingView();
    }

    public void orderReviewClick(View view) {
        switch (view.getId()) {
            case R.id.img_rating_1:
                this.ratingCount = 1;
                break;
            case R.id.img_rating_2:
                this.ratingCount = 2;
                break;
            case R.id.img_rating_3:
                this.ratingCount = 3;
                break;
            case R.id.img_rating_4:
                this.ratingCount = 4;
                break;
            case R.id.img_rating_5:
                this.ratingCount = 5;
                break;
        }
        updateRatingView();
    }

    private void updateRatingView() {
        binding.imgRating1.setImageDrawable(ContextCompat.getDrawable(context, ratingCount >= 1 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive));
        binding.imgRating2.setImageDrawable(ContextCompat.getDrawable(context, ratingCount >= 2 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive));
        binding.imgRating3.setImageDrawable(ContextCompat.getDrawable(context, ratingCount >= 3 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive));
        binding.imgRating4.setImageDrawable(ContextCompat.getDrawable(context, ratingCount >= 4 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive));
        binding.imgRating5.setImageDrawable(ContextCompat.getDrawable(context, ratingCount >= 5 ? R.drawable.ic_rating_big_active : R.drawable.ic_rating_big_inactive));
    }

    public void onSubmitClick() {
        listener.onSubmitReviewWithFeedBack("", ratingCount);
    }

    public void onCancelClick() {
        listener.onCancelReview();
    }


    public interface DialogOrderReviewViewModelListener {

        void onSubmitReviewWithFeedBack(String reviewFeedback, int rating);

        void onCancelReview();

    }
}
