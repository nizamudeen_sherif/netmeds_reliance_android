package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

import okhttp3.MultipartBody;

public class RateAppViewModel extends AppViewModel {

    private BasePreference basePreference;
    private int ratingCount = 0;
    private RateAppCallback callback;

    public RateAppViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(BasePreference basePreference, RateAppCallback callback) {
        this.basePreference = basePreference;
        this.callback = callback;
    }

    @Override
    public void onRetryClickListener() {
        if (callback.isNetworkConnected()) {
            callback.showNetworkErrorView(true);
        }
        callback.showWebserviceErrorView(false);
        if (ratingCount > 0)
            submitFeedback();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        callback.vmDismissProgress();
        if (transactionId == APIServiceManager.C_MSTAR_RATE_ORDERS) {
            onRatedResponse(data);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        callback.vmDismissProgress();
        callback.showWebserviceErrorView(true);
    }

    private void onRatedResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus() != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null && response.getResult().getMessage() != null && AppConstant.RATING_SUCCESS_MESSAGE.equalsIgnoreCase(response.getResult().getMessage())) {
                if (ratingCount <= 3) {
                    callback.navigateBackToAccounts();
                } else {
                    callback.openPlayStoreRatingPopUp();
                }
            } else if (response != null && response.getStatus() != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng())) {
                callback.showSnackbarMessage(response.getReason().getReason_eng());
            }
        }
    }

    public void orderReviewClick(View view) {
        int id = view.getId();
        if (id == com.nms.netmeds.payment.R.id.img_rating_1) {
            this.ratingCount = 1;
        } else if (id == com.nms.netmeds.payment.R.id.img_rating_2) {
            this.ratingCount = 2;
        } else if (id == com.nms.netmeds.payment.R.id.img_rating_3) {
            this.ratingCount = 3;
        } else if (id == com.nms.netmeds.payment.R.id.img_rating_4) {
            this.ratingCount = 4;
        } else if (id == com.nms.netmeds.payment.R.id.img_rating_5) {
            this.ratingCount = 5;
        }
        callback.updateRatingView(ratingCount);
    }

    private MultipartBody getRatingBody() {
        return new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart(AppConstant.RATING_COUNT, String.valueOf(ratingCount)).addFormDataPart(AppConstant.FEEDBACK, callback.getFeedback())
                .addFormDataPart(AppConstant.CHANNEL, AppConstant.MSTAR_CHANNEL_NAME).addFormDataPart(AppConstant.RATING_EVENT, AppConstant.FROM_MY_ACCOUNT).build();
    }

    public void submitFeedback() {
        callback.showNetworkErrorView(callback.isNetworkConnected());
        if (callback.isNetworkConnected() && ratingCount > 0) {
            callback.vmShowProgress();
            APIServiceManager.getInstance().MstarRateOrders(this, basePreference.getMstarBasicHeaderMap(), getRatingBody());
        }
    }


    public interface RateAppCallback {

        String getFeedback();

        void vmShowProgress();

        void updateRatingView(int rating);

        void navigateBackToAccounts();

        void showSnackbarMessage(String message);

        void vmDismissProgress();

        void openPlayStoreRatingPopUp();

        boolean isNetworkConnected();

        void showNetworkErrorView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebError);
    }
}
