package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.databinding.ActivityFaqMessageBinding;
import com.netmedsmarketplace.netmeds.model.FAQCategoryCondentResponseModel;
import com.netmedsmarketplace.netmeds.model.request.FAQCatgoryContentRequest;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBase2Response;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("StaticFieldLeak")
public class FAQMessageViewModel extends AppViewModel {

    private final MutableLiveData<FAQCategoryCondentResponseModel> mutableFAQLiveData = new MutableLiveData<>();
    private Context context;
    private ActivityFaqMessageBinding binding;
    private FAQMessageViewModelListener listener;
    private String categoryId, enquirySubject, enquiryReason, titleTxt, fullname, phone, email;

    public FAQMessageViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityFaqMessageBinding binding, FAQMessageViewModelListener listener, String categoryId) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        this.categoryId = categoryId;
        fetchNeedHelpFromServer();
    }

    public MutableLiveData<FAQCategoryCondentResponseModel> getFAQContentMutableData() {
        return mutableFAQLiveData;
    }

    private void fetchNeedHelpFromServer() {
        initiateAPICall(AppServiceManager.FAQ_CATEGORY_CONTENT);
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            listener.showLoadingProgress();
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_NEED_HELP:
                    getCustomerEnquiry();
                    APIServiceManager.getInstance().mstarStillNeedHelp(this, BasePreference.getInstance(context).getMstarBasicHeaderMap(), titleTxt, enquirySubject, fullname, enquiryReason, phone, email);
                    break;
                case AppServiceManager.FAQ_CATEGORY_CONTENT:
                    showAndHideMainContentView(false);
                    AppServiceManager.getInstance().getFAQCatrgoryContentData(this, getFAQContentRequest());
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case AppServiceManager.FAQ_CATEGORY_CONTENT:
                faqCategoryResponse(data);
                break;
            case APIServiceManager.C_MSTAR_NEED_HELP:
                customerMailEnquiryResponse(data);
                break;
        }
    }

    private void customerMailEnquiryResponse(String data) {
        listener.dismissLoadingProgress();
        MStarBase2Response response = new Gson().fromJson(data, MStarBase2Response.class);
        if (response != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && !TextUtils.isEmpty(response.getResult())) {
            listener.showSnackBar(response.getResult());
        } else if (response != null && response.getReason() != null && !TextUtils.isEmpty(response.getReason().getReason_eng())) {
            listener.showSnackBar(response.getReason().getReason_eng());
        }

    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.dismissLoadingProgress();
        showWebserviceErrorView();
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        fetchNeedHelpFromServer();
    }

    private void faqCategoryResponse(String data) {
        listener.dismissLoadingProgress();
        showAndHideMainContentView(true);
        if (!TextUtils.isEmpty(data)) {
            FAQCategoryCondentResponseModel faqResponseModel = new Gson().fromJson(data, FAQCategoryCondentResponseModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(faqResponseModel.getStatus())) {
                mutableFAQLiveData.setValue(faqResponseModel);
            }
        }
    }

    public void onClickOfStillNeedHelp() {
        listener.needHelpAgain();
    }

    private void showNoNetworkView(boolean isConnected) {
        showAndHideMainContentView(isConnected);
        binding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView() {
        showAndHideMainContentView(false);
        binding.apiErrorView.setVisibility(View.VISIBLE);
    }

    private void showAndHideMainContentView(boolean isEnable) {
        binding.contentView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        binding.llBottomButtonView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void loadPageAfterResponse(FAQCategoryCondentResponseModel faqResponseModel) {
        binding.webview.setWebViewClient(new WebViewClient());
        binding.webview.getSettings().setJavaScriptEnabled(true);
        final String content;
        if (faqResponseModel.getMessage().getContents() != null) {
            content = faqResponseModel.getMessage().getContents().get(0).getDetail();
        } else {
            content = faqResponseModel.getMessage().getData();
        }
        binding.webview.loadDataWithBaseURL(null, content, "text/html", "utf-8", null);
        binding.webview.setVisibility(View.VISIBLE);
        binding.webview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void submitNeedHelpEmailReason(String subject, String reason, String titleTxt) {
        this.enquirySubject = subject;
        this.enquiryReason = reason;
        this.titleTxt = titleTxt;
        initiateAPICall(APIServiceManager.C_MSTAR_NEED_HELP);
    }

    private List<FAQCatgoryContentRequest> getFAQContentRequest() {
        List<FAQCatgoryContentRequest> requestList = new ArrayList<>();
        FAQCatgoryContentRequest faqCatgoryContentRequest = new FAQCatgoryContentRequest();
        faqCatgoryContentRequest.setCategoryId(categoryId);
        faqCatgoryContentRequest.setStartIndex("0");
        faqCatgoryContentRequest.setEndIndex("20");
        requestList.add(faqCatgoryContentRequest);
        return requestList;
    }

    private void getCustomerEnquiry() {
        MStarCustomerDetails customerDetails = new Gson().fromJson(BasePreference.getInstance(context).getCustomerDetails(), MStarCustomerDetails.class);
        this.fullname = customerDetails.getFirstName() + " " + customerDetails.getLastName();
        this.phone = customerDetails.getMobileNo();
        this.email = customerDetails.getEmail();
    }

    public interface FAQMessageViewModelListener {
        void showLoadingProgress();

        void dismissLoadingProgress();

        void needHelpAgain();

        void showSnackBar(String message);
    }
}
