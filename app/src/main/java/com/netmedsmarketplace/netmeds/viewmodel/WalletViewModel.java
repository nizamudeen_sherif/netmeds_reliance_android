package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarWalletDetails;
import com.nms.netmeds.base.model.MstarWalletHistory;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;
import java.util.List;

public class WalletViewModel extends AppViewModel {

    private final MutableLiveData<MStarBasicResponseTemplateModel> walletMutableLiveData = new MutableLiveData<>();
    private WalletViewListener listener;
    private BasePreference basePreference;
    private MstarWalletDetails walletDetails;
    private MStarBasicResponseTemplateModel basicResponse;

    public MStarBasicResponseTemplateModel getBasicResponse() {
        return basicResponse;
    }

    public List<MstarWalletHistory> getWalletHistory() {
        return walletHistory;
    }

    private List<MstarWalletHistory> walletHistory;

    public WalletViewModel(@NonNull Application application) {
        super(application);
    }

    public String getAvailableBalance() {
        return CommonUtils.getPriceInFormat(CommonUtils.getIndianCurrencyFormat(String.valueOf(walletDetails.getTOTALBALANCE())));
    }

    public boolean availableBalanceVisibility() {
        return listener.availableBalanceVisibilityCallback();
    }

    public String getNmsNormalCash() {
        return CommonUtils.getPriceInFormat(CommonUtils.getIndianCurrencyFormat(String.valueOf(walletDetails.getNMSCASH())));
    }

    public boolean nmsCashVisibility() {
        return listener.nmsCashVisibilityCallback();
    }

    public boolean isTransactionMoreThanThree() {
        return walletHistory != null && !walletHistory.isEmpty() && walletHistory.size() > 3;
    }

    public String getNmsSuperCash() {
        return CommonUtils.getPriceInFormat(CommonUtils.getIndianCurrencyFormat(String.valueOf(walletDetails.getNMSSUPERCASH())));
    }

    public boolean nmsSuperCashVisibility() {
        return listener.nmsSuperCashVisibilityCallback();
    }

    public String getNmsCashExpireDetails() {
        return walletDetails != null && !TextUtils.isEmpty(walletDetails.getNmsCashMsg()) ? walletDetails.getNmsCashMsg() : "";
    }

    public String getNmsSuperCashExpireDetails() {
        return walletDetails != null && !TextUtils.isEmpty(walletDetails.getNmsSuperCashMsg()) ? walletDetails.getNmsSuperCashMsg() : "";
    }

    public String getCollapsingAmount() {
        return CommonUtils.getPriceInFormat(CommonUtils.getIndianCurrencyFormat(String.valueOf(walletDetails.getTOTALBALANCE())));
    }

    public void init(BasePreference basePreference, WalletViewListener listener) {
        this.listener = listener;
        this.basePreference = basePreference;
        listener.initShimmer();
        listener.toolBarProperties();
        fetchWalletDetailsFromServer();
    }

    private void fetchWalletDetailsFromServer() {
        initMstarApi(APIServiceManager.C_MSTAR_WALLET_DETAILS);
    }

    private void initMstarApi(int transactionId) {
        boolean isConnected = listener.isNetworkConnetedCallback();
        listener.showNoNetworkErrorCallback(isConnected);
        if (isConnected) {
            if (transactionId == APIServiceManager.C_MSTAR_WALLET_DETAILS) {
                APIServiceManager.getInstance().getMstarWalletDetails(this, basePreference.getMstarBasicHeaderMap());
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == APIServiceManager.C_MSTAR_WALLET_DETAILS) {
            if (!TextUtils.isEmpty(data)) {
                MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                walletMutableLiveData.setValue(response);
            }
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        if (transactionId == APIServiceManager.C_MSTAR_WALLET_DETAILS) {
            listener.showWebserviceErrorViewCallback(true);
        }
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        fetchWalletDetailsFromServer();
    }

    public void inflateTransactionDetails(MStarBasicResponseTemplateModel basicResponse) {
        this.basicResponse = basicResponse;
        walletDetails = basicResponse.getResult() != null && basicResponse.getResult().getWalletDetails() != null ? basicResponse.getResult().getWalletDetails() : new MstarWalletDetails();
        walletHistory = basicResponse.getResult() != null && basicResponse.getResult().getWalletHistoryList() != null ? basicResponse.getResult().getWalletHistoryList() : new ArrayList<MstarWalletHistory>();
        listener.initiateAllTransactionAdapter(walletHistory);
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getWalletMutableLiveData() {
        return walletMutableLiveData;
    }

    public void referFriend() {
        listener.onReferFriend();
    }

    public void onViewAllTransaction() {
        listener.onViewAllTransaction();
    }

    public void howToUseNMSCash() {
        listener.onHowToUse(R.string.text_nms_cash);
    }

    public void howToUseNMSSuperCash() {
        listener.onHowToUse(R.string.text_nms_super_cash);
    }

    public interface WalletViewListener {
        void onViewAllTransaction();

        void onHowToUse(int stringId);

        void onReferFriend();

        void showSnackBarMessage(int messageId);

        boolean isNetworkConnetedCallback();

        void showNoNetworkErrorCallback(boolean isConnected);

        void showWebserviceErrorViewCallback(boolean isWebserviceError);

        void initShimmer();

        void initiateAllTransactionAdapter(List<MstarWalletHistory> transactionList);

        void toolBarProperties();

        boolean availableBalanceVisibilityCallback();

        boolean nmsCashVisibilityCallback();

        boolean nmsSuperCashVisibilityCallback();
    }
}
