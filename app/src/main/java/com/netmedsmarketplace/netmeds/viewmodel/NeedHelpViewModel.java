package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.LegalDetailsAndSupportAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityNeedHelpBinding;
import com.netmedsmarketplace.netmeds.model.FAQCategoryModel;
import com.netmedsmarketplace.netmeds.model.FAQResponseModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressLint("StaticFieldLeak")
public class NeedHelpViewModel extends AppViewModel implements LegalDetailsAndSupportAdapter.LegalDetailsAndSupportAdapterListener {

    private final Map<String, List<FAQCategoryModel>> categoryMap = new HashMap<>();
    private final MutableLiveData<List<FAQCategoryModel>> categoryMutableLiveDataList = new MutableLiveData<>();
    private Context context;
    private ActivityNeedHelpBinding binding;
    private NeedHelpViewModelListener listener;

    public NeedHelpViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ActivityNeedHelpBinding binding, NeedHelpViewModelListener listener, Intent intent) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        getIntentValue(intent);
    }

    private void getIntentValue(Intent intent) {
        binding.collapsingToolbar.setTitle(context.getString(R.string.text_need_help));
        if (intent != null) {
            if (intent.hasExtra(IntentConstant.FAQ_CATEGORY_LIST)) {
                List<FAQCategoryModel> subCategoryList = (List<FAQCategoryModel>) intent.getExtras().getSerializable(IntentConstant.FAQ_CATEGORY_LIST);
                binding.collapsingToolbar.setTitle(subCategoryList != null && !subCategoryList.isEmpty() && !TextUtils.isEmpty(subCategoryList.get(0).getParentCategoryName()) ? subCategoryList.get(0).getParentCategoryName() : "");
                categoryMutableLiveDataList.setValue(subCategoryList);
            } else {
                fetchNeedHelpFromServer();
            }
        } else {
            fetchNeedHelpFromServer();
        }
    }

    public MutableLiveData<List<FAQCategoryModel>> getFAQCategoryMutableData() {
        return categoryMutableLiveDataList;
    }

    private void fetchNeedHelpFromServer() {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            binding.rvTransactionList.setVisibility(View.GONE);
            binding.shimmerView.setVisibility(View.VISIBLE);
            binding.shimmerView.startShimmerAnimation();
            AppServiceManager.getInstance().getFAQData(this);
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case AppServiceManager.FAQ_ALL_CATEGORY:
                faqAllCategoryResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        showWebserviceErrorView(true);
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        showWebserviceErrorView(false);
        fetchNeedHelpFromServer();
    }

    private void faqAllCategoryResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            FAQResponseModel faqResponseModel = new Gson().fromJson(data, FAQResponseModel.class);
            if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(faqResponseModel.getStatus())) {
                iterateCategory(faqResponseModel);
            }
        }
    }

    private void iterateCategory(FAQResponseModel faqResponseModel) {
        for (FAQCategoryModel model : faqResponseModel.getMessage().getCategory()) {
            String key = model.getCategoryPath();
            if (categoryMap.containsKey(key)) {
                categoryMap.get(key).add(model);
            } else {
                List<FAQCategoryModel> modelList = new ArrayList<>();
                modelList.add(model);
                categoryMap.put(key, modelList);
            }
        }
        categoryMutableLiveDataList.setValue(categoryMap.get("549/"));
    }

    public void loadIteratedCategoryList(List<FAQCategoryModel> categoryList) {
        binding.shimmerView.setVisibility(View.GONE);
        binding.shimmerView.stopShimmerAnimation();
        if (categoryList != null && !categoryList.isEmpty()) {
            binding.rvTransactionList.setVisibility(View.VISIBLE);
            LegalDetailsAndSupportAdapter adapter = new LegalDetailsAndSupportAdapter(categoryList, this);
            binding.rvTransactionList.setLayoutManager(new LinearLayoutManager(context));
            binding.rvTransactionList.setNestedScrollingEnabled(false);
            binding.rvTransactionList.setAdapter(adapter);
        }
    }

    private void showNoNetworkView(boolean isConnected) {
        binding.contentView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.needHelpNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        binding.contentView.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.needHelpApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
        binding.shimmerView.setVisibility(View.GONE);
        binding.shimmerView.stopShimmerAnimation();
    }

    @Override
    public void onItemClick(FAQCategoryModel model) {
        listener.showCategoryContent(model);
    }

    public boolean isSubCategoryFound(FAQCategoryModel model) {
        String categoryPathKey = String.format("%1s%2s/", model.getCategoryPath(), model.getId());
        return categoryMap.containsKey(categoryPathKey);
    }

    public List<FAQCategoryModel> getSubCategoryListToShareToIntent(FAQCategoryModel model) {
        String categoryPathKey = String.format("%1s%2s/", model.getCategoryPath(), model.getId());
        return categoryMap.get(categoryPathKey);
    }

    public interface NeedHelpViewModelListener {

        void showCategoryContent(FAQCategoryModel model);
    }
}
