package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MstarPrimeProduct;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;

public class StartActivityViewModel extends AppViewModel {

    private final MutableLiveData<ConfigurationResponse> configurationResponseMutableLiveData = new MutableLiveData<>();
    private StartListener listener;
    private BasePreference basePreference;
    private MutableLiveData<MStarCartDetails> mStarCartDetailsMutableLiveData;

    public MutableLiveData<MStarCartDetails> getmStarCartDetailsMutableLiveData() {
        if (mStarCartDetailsMutableLiveData == null) {
            mStarCartDetailsMutableLiveData = new MutableLiveData<>();
        }
        return mStarCartDetailsMutableLiveData;
    }

    public StartActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<ConfigurationResponse> getConfigurationResponseMutableLiveData() {
        return configurationResponseMutableLiveData;
    }

    public void init(StartListener listener, BasePreference basePreference) {
        this.listener = listener;
        this.basePreference = basePreference;
        fetchConfiguration(listener.getContext());
    }

    private void fetchConfiguration(Context context) {
        boolean isConnected = NetworkUtils.isConnected(context);
        listener.showNoNetworkView(isConnected);
        if (isConnected) {
            APIServiceManager.getInstance().getConfigurationData(this);
            APIServiceManager.getInstance().mstarPrimeProduct(this);
        }
    }

    @Override
    public void onRetryClickListener() {
        listener.showWebserviceErrorView(false);
        fetchConfiguration(listener.getContext());
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CONFIG_PATH_URL:
                basePreference.setPrimeUser(data);
                configurationResponseMutableLiveData.setValue(new Gson().fromJson(data, ConfigurationResponse.class));
                break;
            case APIServiceManager.MSTAR_PRIME_PRODUCT:
                getPrimeProducts(data);
                break;
        }

    }

    private void getPrimeProducts(String data) {
        MstarPrimeProductResult result = new Gson().fromJson(data, MstarPrimeProductResult.class);
        result.getPrimeProductCodesList().clear();
        for (MstarPrimeProduct primeProduct : result.getPrimeProducts()) {
            result.getPrimeProductCodesList().add(String.valueOf(primeProduct.getProductCode()));
        }
        basePreference.setMstarPrimeMembership(new Gson().toJson(result));
    }

    @Override
    public void onFailed(int transactionId, String data) {
        listener.showWebserviceErrorView(true);
    }


    public interface StartListener {
        Context getContext();

        void showNoNetworkView(boolean isShowNoNetworkView);

        void showWebserviceErrorView(boolean isShowWebserviceErrorView);
    }
}
