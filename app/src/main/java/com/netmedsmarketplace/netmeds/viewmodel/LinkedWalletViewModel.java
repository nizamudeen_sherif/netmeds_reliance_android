package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;

public class LinkedWalletViewModel extends AppViewModel {
    public LinkedWalletViewModel(@NonNull Application application) {
        super(application);
    }
}
