package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MStarReviewPageItemAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityConfirmationBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateLineItem;
import com.nms.netmeds.base.model.DeliveryEstimatePinCodeResult;
import com.nms.netmeds.base.model.DeliveryEstimateRequest;
import com.nms.netmeds.base.model.DeliveryEstimateResponse;
import com.nms.netmeds.base.model.DeliveryEstimateResult;
import com.nms.netmeds.base.model.Flow2SubscriptionRequest;
import com.nms.netmeds.base.model.GenerateOrderIdRequest;
import com.nms.netmeds.base.model.GetPastPrescriptionResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.MstarSubmitMethod2;
import com.nms.netmeds.base.model.MstarSubmitMethod2Response;
import com.nms.netmeds.base.model.UploadedPrescriptionList;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.payment.ui.PaymentConstants;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import okhttp3.MultipartBody;

public class OrderConfirmationViewModel extends AppViewModel implements MStarReviewPageItemAdapter.MStarReviewPageItemAdapterCallback {

    private OrderConfirmationListener callBack;
    private MStarCartDetails cartDetails;
    private MStarReviewPageItemAdapter itemAdapter;
    private MStarAddressModel deliveryAddress;
    private BasePreference basePreference;
    private Intent intent;
    private ConfigurationResponse configurationResponse;
    private String appFirstOrder = "";

    private final ArrayList<Bitmap> prescriptionBitmapList = new ArrayList<>();
    private final ArrayList<UploadedPrescriptionList> prescriptionList = new ArrayList<>();
    private final ArrayList<String> uploadedPrescriptionList = new ArrayList<>();
    private List<Date> minDateRageList = new ArrayList<>();
    private List<Date> maxDateRageList = new ArrayList<>();
    public ArrayList<String> interCityProductList = new ArrayList<String>();

    private String orderId;
    private String subscriptionResponseData;
    private boolean isFromRefillSubscription = false;
    private boolean cartProductEmpty;
    private boolean isPrimeProduct = false;
    private boolean isNonPrimeProduct = false;
    private boolean isOutOfStockAvailableInCart = false;
    private boolean isCODSupported = false;
    private boolean isProceedFurther = true;
    private String errorMessage = "";
    private MStarProductDetails primeProduct;
    private BigDecimal nmsSuperCashAmount = BigDecimal.ZERO;

    public MStarProductDetails getPrimeProduct() {
        return primeProduct;
    }

    public void setPrimeProduct(MStarProductDetails primeProduct) {
        this.primeProduct = primeProduct;
    }

    public String getSubscriptionResponseData() {
        return subscriptionResponseData;
    }

    public void setSubscriptionResponseData(String subscriptionResponseData) {
        this.subscriptionResponseData = subscriptionResponseData;
    }

    public OrderConfirmationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(BasePreference basePreference, MStarAddressModel deliveryAddress, OrderConfirmationListener cartListener, Intent intent) {
        this.deliveryAddress = deliveryAddress;
        this.callBack = cartListener;
        this.intent = intent;
        this.basePreference = basePreference;
        this.isFromRefillSubscription = intent.getBooleanExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, false);
        this.configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        setInitialProperties();
        callBack.enableShimmer(true);
    }

    private void setInitialProperties() {
        prescriptionBitmapList.clear();
        callBack.enableOrDisableM2ProceedButton(M2Helper.getInstance().isM2Order() && PaymentHelper.isExternalDoctor());
        basePreference.setPinCode(PaymentHelper.getDeliveryPinCode());
        PaymentHelper.setCustomerAddress(deliveryAddress);
        PaymentHelper.setDeliveryPinCode(deliveryAddress.getPin());
        PaymentHelper.setIsPrescriptionOrder(false);
        PaymentHelper.setReloadTotalInformation(false);
        M2Helper.setCustomerAddress(PaymentHelper.getCustomerAddress());
        M2Helper.setmCustomerBillingAddress(PaymentHelper.getCustomerBillingAddress());
        if (isFromRefillSubscription()) {
            callBack.rxInfoVisibility(View.VISIBLE);
        } else {
            callBack.stockInfoVisibility(View.GONE);
        }
        fetchCartDetailsForReview();
    }

    public void fetchCartDetailsForReview() {
        initiateAPICall(APIServiceManager.MSTAR_GET_CART_DETAILS);
    }

    private void initiateAPICall(int transactionId) {
        boolean isConnected = callBack.isNetworkConnected();
        callBack.showNoNetworkView(isConnected);
        if (isConnected) {
            callBack.vmShowProgress();
            switch (transactionId) {
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    APIServiceManager.getInstance().mStarGetCartDetails(this, basePreference.getMstarBasicHeaderMap(), getCartId(), transactionId);
                    break;
                case APIServiceManager.MSTAR_SUBMIT_METHOD_2:
                    APIServiceManager.getInstance().mstarSubmitMethod2(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarMethod2CartId(), PaymentHelper.isExternalDoctor());
                    break;
                case APIServiceManager.MSTAR_GENERATE_ORDER_ID:
                    APIServiceManager.getInstance().generateOrderId(this, basePreference.getMstarBasicHeaderMap(), getCartId(), PaymentHelper.isExternalDoctor() ? AppConstant.YES : AppConstant.NO, getGenerateOrderIdRequest(), PaymentHelper.isIsTempCart(), null);
                    break;
                case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
                    APIServiceManager.getInstance().mStarOrderCompleteWithCOD(this, CommonUtils.getFingerPrintHeaderMap(callBack.getContext()), getCartId());
                    break;
                case APIServiceManager.MSTAR_CREATE_SUBSCRIPTION:
                    APIServiceManager.getInstance().createSubscription(this, basePreference.getMstarBasicHeaderMap(), orderId, String.valueOf(basePreference.getDeliveryIntervalPreference()), AppConstant.REFILL_NOW);
                    break;
                case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                    APIServiceManager.getInstance().deliveryEstimation(this, getDeliveryDateEstimateRequest(), configurationResponse.getResult().getConfigDetails().getDeliveryDateApi() + "/");
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                MStarBasicResponseTemplateModel cartDataDetails = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                if (isFromRefillSubscription && isPrimeProduct) {
                    //todo
                } else {
                    onCartItemDataAvailable(data);
                }
                break;

            case APIServiceManager.MSTAR_SUBMIT_METHOD_2:
                callBack.vmDismissProgress();
                getSubmitResponse(data);
                break;
            case APIServiceManager.DELIVERY_DATE_ESTIMATE:
                deliveryEstimateResponse(data);
                break;
            case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
                callBack.vmDismissProgress();
                pastPrescriptionResponse(data);
                break;
            case APIServiceManager.MSTAR_GENERATE_ORDER_ID:
                callBack.vmDismissProgress();
                getOrderIdResponse(data);
                break;
            case APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD:
                callBack.vmDismissProgress();
                orderCompleteWithCODPesponse(data);
                break;
            case APIServiceManager.MSTAR_CREATE_SUBSCRIPTION:
                callBack.vmDismissProgress();
                subscriptionInitiateSuccessResponse(data);
                break;
            case APIServiceManager.M3_SUBSCRIPTION_LOG:
                callBack.vmDismissProgress();
                callBack.vmOderSuccessFullyActivity(getSubscriptionResponseData(), intent.getBooleanExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, false));
                break;
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        callBack.vmDismissProgress();
        callBack.enableOrDisableProceedButton(true);
    }

    private GenerateOrderIdRequest getGenerateOrderIdRequest() {
        GenerateOrderIdRequest request = new GenerateOrderIdRequest();
        request.setSplitInfo(!TextUtils.isEmpty(PaymentHelper.getDeliveryEstimateResponse()) ? new Gson().fromJson(PaymentHelper.getDeliveryEstimateResponse(), DeliveryEstimateResponse.class) : new DeliveryEstimateResponse());
        return request;
    }

    private Integer getCartId() {
        return isSubscription() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() : null;
    }

    private boolean isSubscription() {
        return SubscriptionHelper.getInstance().isSubscriptionFlag() || SubscriptionHelper.getInstance().isPayNowSubscription() || SubscriptionHelper.getInstance().isSubscriptionEdit();
    }

    @Override
    public void onRetryClickListener() {
        fetchCartDetailsForReview();
    }

    private Flow2SubscriptionRequest getRefillSubRequest() {
        Flow2SubscriptionRequest request = new Flow2SubscriptionRequest();
        request.setOrderId(intent.getExtras().getString(SubscriptionIntentConstant.ORDER_ID));
        request.setRefillDays(String.valueOf(basePreference.getDeliveryIntervalPreference()));
        return request;
    }

    public void onCartItemDataAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (responseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())) {
                if (responseTemplateModel.getResult() != null && responseTemplateModel.getResult().getCartDetails() != null) {
                    this.cartDetails = responseTemplateModel.getResult().getCartDetails();
                    if (responseTemplateModel.getResult().getCartDetails().getUsedWalletAmount() != null)
                        nmsSuperCashAmount = responseTemplateModel.getResult().getCartDetails().getUsedWalletAmount().getNmsSuperCash();
                    PaymentHelper.setCartLineItems(cartDetails.getLines());
                    MstarPrimeProductResult primeProductResult = new Gson().fromJson(basePreference.getMstarPrimeMembership(), MstarPrimeProductResult.class);
                    uploadedPrescriptionList.clear();
                    uploadedPrescriptionList.addAll(cartDetails.getPrescriptions());
                    basePreference.setCartCount(cartDetails.getLines().size());
                    checkStockQuantityAndAvailability(cartDetails.getLines());

                    if (!cartDetails.getLines().isEmpty()) {
                        checkCartContainPrimeProduct(cartDetails.getLines(), primeProductResult != null && primeProductResult.getPrimeProductCodesList() != null && primeProductResult.getPrimeProductCodesList().size() > 0 ? primeProductResult.getPrimeProductCodesList() : new ArrayList<String>());
                        MStarBasicResponseTemplateModel model = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
                        String imageUrl = model != null && model.getResult() != null && !TextUtils.isEmpty(model.getResult().getProductImageUrlBasePath()) ?
                                model.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" : ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_PRODUCT_IMAGE_BASE_URL);
                        itemAdapter = new MStarReviewPageItemAdapter(!M2Helper.getInstance().isM2Order() && cartDetails.getLines().size() > 1 ? setPrimeProductFirstPosition(cartDetails.getLines(), primeProductResult != null && primeProductResult.getPrimeProductCodesList() != null && primeProductResult.getPrimeProductCodesList().size() > 0 ? primeProductResult.getPrimeProductCodesList() : new ArrayList<String>()) : cartDetails.getLines(),
                                isFromRefillSubscription, this, primeProductResult != null && primeProductResult.getPrimeProductCodesList() != null && primeProductResult.getPrimeProductCodesList().size() > 0 ? primeProductResult.getPrimeProductCodesList() : new ArrayList<String>(), imageUrl);
                        if (isM2Order() && PaymentHelper.isExternalDoctor()) {
                            callBack.getBinding().orderConfirmationProductView.setVisibility(View.GONE);
                        } else {
                            callBack.rxInfoSetVisibility(View.GONE);
                            callBack.setcartItemsAdapter(itemAdapter);
                        }
                    }
                    checkPinCode();
                } else {
                    callBack.vmDismissProgress();
                    setCartProductEmpty(true);
                }
            } else {
                callBack.vmDismissProgress();
                setCartProductEmpty(true);
            }
        } else {
            callBack.vmDismissProgress();
            setCartProductEmpty(true);
        }
        callBack.executeBinding();
    }

    private void getSubmitResponse(String data) {
        MstarSubmitMethod2Response response = new Gson().fromJson(data, MstarSubmitMethod2Response.class);
        if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
            basePreference.setM2CartCount(0);
            basePreference.setMstarMethod2CartId(0);
            basePreference.setMStarM2CartIsOpen(true);
            callBack.vmPlaceOrder(response.getResult());
        }
    }

    public boolean isM2Order() {
        return M2Helper.getInstance() != null && M2Helper.getInstance().isM2Order();
    }


    public boolean isFromRefillSubscription() {
        return isFromRefillSubscription;
    }

    public boolean isM1Order() {
        return !TextUtils.isEmpty(saving());
    }

    public boolean hideLayoutEditOder() {
        return SubscriptionHelper.getInstance().isCreateNewFillFlag() || isFromRefillSubscription;
    }


    public String currentDate() {
        return SubscriptionHelper.getInstance().getCurrentDeliveryDate();
    }

    public String nextDelivery() {
        return SubscriptionHelper.getInstance().getNextDeliveryIntervalDate();
    }

    public String promoCodeTextChange() {
        return M2Helper.getInstance().isM2Order() || SubscriptionHelper.getInstance().isSubscriptionFlag() || cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? getApplication().getString(R.string.text_subscription_coupon_text) : cartDetails != null && cartDetails.isUseSuperCash() ? callBack.getContext().getString(R.string.text_nms_supercash_applied) : "";
    }

    public void changeAddress() {
        callBack.changeAddress();
    }

    @Override
    public void isRxRequired() {
        PaymentHelper.setIsPrescriptionOrder(true);
        if (callBack.getBinding().orderConfirmationStockInfo.getVisibility() == View.VISIBLE) {
            callBack.rxInfoVisibility(View.GONE);
        } else {
            callBack.rxInfoVisibility(View.VISIBLE);
            callBack.stockInfoVisibility(View.GONE);

        }
    }

    @Override
    public void sellerDetailView(String sellerName, String sellerAddress) {
        callBack.sellerDetailDialog(sellerName, sellerAddress);
    }

    private void checkStockQuantityAndAvailability(List<MStarProductDetails> cartLineItems) {
        StringBuilder productUnAvailable = new StringBuilder();
        for (MStarProductDetails cartLineItem : cartLineItems) {
            if (!TextUtils.isEmpty(cartLineItem.getAvailabilityStatus()) && (!cartLineItem.getAvailabilityStatus().equalsIgnoreCase(AppConstant.PRODUCT_AVAILABLE_CODE)) || (cartLineItem.getCartQuantity() > cartLineItem.getStockQty())) {
                productUnAvailable.append(cartLineItem.getDisplayName());
                if (!isOutOfStockAvailableInCart) {
                    isOutOfStockAvailableInCart = true;
                }
                break;
            }
        }
    }

    public void toHome() {
        callBack.vmNavigateToHome();
    }

    public void takeAction() {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getApplication(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OUT_OF_STOCK, GoogleAnalyticsHelper.EVENT_ACTION_TAKE_ACTION, GoogleAnalyticsHelper.EVENT_LABEL_ORDER_REVIEW_PAGE);
        callBack.productUnAvailabilityAlert(isOutOfStockAvailableInCart, interCityProductList);
    }

    public String getPromoCode() {
        if (SubscriptionHelper.getInstance().isSubscriptionFlag())
            return SubscriptionHelper.getInstance().isSubscriptionFlag() ? getSubscriptionCoupon() : cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? cartDetails.getAppliedCoupons() : "";
        else if (cartDetails != null && cartDetails.isUseSuperCash())
            return callBack.getContext().getString(R.string.text_nms_super_cash);
        else
            return M2Helper.getInstance().isM2Order() || PaymentHelper.isIsEditOrder() ? getM2OrderCoupon() : cartDetails != null && !TextUtils.isEmpty(cartDetails.getAppliedCoupons()) ? cartDetails.getAppliedCoupons() : "";
    }

    private String getM2OrderCoupon() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getM2Coupon().getCouponCode()) ? response.getResult().getConfigDetails().getM2Coupon().getCouponCode() : "";
    }

    private String getM2OrderCouponDiscount() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getM2Coupon().getDiscount()) ? response.getResult().getConfigDetails().getM2Coupon().getDiscount() : "";
    }

    private String getSubscriptionCoupon() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getSubscriptionCouponList().getCouponCode()) ? response.getResult().getConfigDetails().getSubscriptionCouponList().getCouponCode() : "";
    }

    private String getSubscriptionDiscount() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getSubscriptionCouponList().getDiscount()) ? response.getResult().getConfigDetails().getSubscriptionCouponList().getDiscount() : "";
    }

    public String saving() {
        if (isM2Order() || isFromRefillSubscription() || PaymentHelper.isIsEditOrder()) {
            return String.format(getApplication().getResources().getString(R.string.text_promo_saving), isM2Order() ? getM2OrderCouponDiscount() + "%" : getRefillSubCouponDiscountPercentage() + "%");
        } else if (SubscriptionHelper.getInstance().isSubscriptionFlag() && cartDetails != null) {
            return String.format(getApplication().getResources().getString(R.string.text_promo_saving), (cartDetails.getCoupon_discount_total().compareTo(BigDecimal.ZERO) == 0 ? 0 : getSubscriptionDiscount()) + "%");
        } else {
            BigDecimal couponDisountValue = getCouponDiscount();
            return String.format(getApplication().getResources().getString(R.string.text_promo_saving), PaymentHelper.isNMSSuperCashApplied() ? cartDetails != null && cartDetails.getUsedWalletAmount() != null ? cartDetails.getUsedWalletAmount().getNmsSuperCash() : 0 :
                    couponDisountValue.compareTo(BigDecimal.ZERO) > 0 ? couponDisountValue : 0);
        }
    }

    public BigDecimal getIndividualProductDiscount() {
        BigDecimal productDisountValue = BigDecimal.ZERO;
        if (cartDetails != null && cartDetails.getLines() != null) {
            for (MStarProductDetails productDetails : cartDetails.getLines()) {
                productDisountValue = productDisountValue.add(productDetails.getLineProductDiscount());
            }
        }
        return productDisountValue;
    }

    public BigDecimal getCouponDiscount() {
        BigDecimal couponDisountValue = BigDecimal.ZERO;
        if (cartDetails != null && cartDetails.getLines() != null) {
            for (MStarProductDetails productDetails : cartDetails.getLines()) {
                couponDisountValue = couponDisountValue.add(productDetails.getLineCouponDiscount());
            }
        }
        return couponDisountValue;
    }

    private String getRefillSubCouponDiscountPercentage() {
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        return response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && response.getResult().getConfigDetails().getSubscriptionCouponList() != null && !TextUtils.isEmpty(String.valueOf(response.getResult().getConfigDetails().getSubscriptionCouponList().getDiscount())) ? String.valueOf(response.getResult().getConfigDetails().getSubscriptionCouponList().getDiscount()) : "";
    }

    public boolean isCouponApplied() {
        return cartDetails != null && (!TextUtils.isEmpty(cartDetails.getAppliedCoupons()) || M2Helper.getInstance().isM2Order() || SubscriptionHelper.getInstance().isSubscriptionFlag() || PaymentHelper.isIsEditOrder() || nmsSuperCashAmount.compareTo(BigDecimal.ZERO) > 0);
    }

    public String patientName() {
        return deliveryAddress != null && !TextUtils.isEmpty(deliveryAddress.getFirstname()) ? deliveryAddress.getFirstname().concat(" " + deliveryAddress.getLastname()) : "";
    }

    public String address() {
        return CommonUtils.getAddressFromObject(deliveryAddress);
    }

    public String getMrpDesc() {
        String mrpDesc = "";
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2DeliveryCharges() != null)
            mrpDesc = !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2DeliveryCharges().getMrpContent()) ? configurationResponse.getResult().getConfigDetails().getM2DeliveryCharges().getMrpContent() : "";
        return mrpDesc;
    }

    public String getDeliveryChargeDesc() {
        String delDesc = "";
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2DeliveryCharges() != null)
            delDesc = !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2DeliveryCharges().getDeliveryContent()) ? configurationResponse.getResult().getConfigDetails().getM2DeliveryCharges().getDeliveryContent() : "";
        return delDesc;
    }

    @SuppressLint("DefaultLocale")
    public void proceedToCheckout() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getApplication(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_REVIEW_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_ORDER_REVIEW_PAGE);

        /* FireBase Analytics Event*/
        FireBaseAnalyticsHelper.getInstance().CheckoutOrderReviewEvent(callBack.getContext(), AppConstant.CHECKOUT_ORDER_REVIEW);
        if (SubscriptionHelper.getInstance().isCreateNewFillFlag() || isFromRefillSubscription) {
            initiateAPICall(APIServiceManager.MSTAR_GENERATE_ORDER_ID);
        } else {
            callBack.enableOrDisableProceedButton(false);
            callBack.vmProceedFromConfirmation();
        }
    }

    private boolean isEligibleForCod() {
        return isOrderAmountAboveMinimumPrice()
                && isOrderAmountNotExceedMaxPrice()
                && !PaymentHelper.isNMSSuperCashApplied() && !PaymentHelper.isVoucherApplied();
    }

    public boolean isOrderAmountAboveMinimumPrice() {
        return cartDetails.getNetPayableAmount().compareTo(BigDecimal.ZERO) > 0 && cartDetails.getNetPayableAmount().doubleValue() > Double.valueOf(PaymentUtils.codCredential(PaymentConstants.COD_MINIMUM_AMOUNT, basePreference.getPaymentCredentials()));
    }

    public boolean isOrderAmountNotExceedMaxPrice() {
        return cartDetails.getNetPayableAmount().doubleValue() < Double.valueOf(PaymentUtils.codCredential(PaymentConstants.COD_MAXIMUM_AMOUNT, basePreference.getPaymentCredentials())) && !PaymentHelper.isNMSCashApplied();
    }

    public void placeOrder() {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getApplication(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_PLACE_ORDER_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_M2_Order_Review);
        callBack.vmShowProgress();
        initiateAPICall(APIServiceManager.MSTAR_SUBMIT_METHOD_2);
    }

    private String getPinCode() {
        MStarAddressModel address = PaymentHelper.getCustomerAddress();
        return address.getPin();
    }

    private void checkPinCode() {
        if ((isM2Order() && PaymentHelper.isExternalDoctor()) || getApplication() != null) {
            if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getDeliveryDateApi())) {
                interCityProductList = new ArrayList<>();
                if (!cartDetails.getLines().isEmpty()) {
                    initiateAPICall(APIServiceManager.DELIVERY_DATE_ESTIMATE);
                } else {
                    callBack.enableShimmer(false);
                    checkIsProceedOrPlaceOrderEnabledAndRxBannerView();
                    setOrderConfirmationView();
                }
            } else {
                callBack.enableShimmer(false);
                checkIsProceedOrPlaceOrderEnabledAndRxBannerView();
                setOrderConfirmationView();
            }
        }
    }

    private void deliveryEstimateResponse(String data) {
        PaymentHelper.setDeliveryEstimateResponse(data);
        DeliveryEstimateResponse response = new Gson().fromJson(data, DeliveryEstimateResponse.class);
        if (response.getStatus() != null && response.getStatus() && response.getResult() != null) {
            setDeliveryEstimation(response.getResult());
        }
    }

    private void setDeliveryEstimation(DeliveryEstimateResult result) {
        minDateRageList = new ArrayList<>();
        maxDateRageList = new ArrayList<>();
        List<String> estimation = new ArrayList<>();
        List<String> estimationWithYear = new ArrayList<>();
        isCODSupported = result.getaStatus().equalsIgnoreCase(getApplication().getString(R.string.text_ok)) && result.getDeliveryEstimateAdditionalData() != null && result.getDeliveryEstimateAdditionalData().getCodSupported();
        isProceedFurther = result.getaStatus().equalsIgnoreCase(getApplication().getString(R.string.text_ok));
        if (result.getDeliveryEstimateAdditionalData() != null && result.getDeliveryEstimateAdditionalData().getInterCityProductList() != null && !result.getDeliveryEstimateAdditionalData().getInterCityProductList().isEmpty()) {
            interCityProductList = result.getDeliveryEstimateAdditionalData().getInterCityProductList();
        }
        if (!result.getaStatus().equalsIgnoreCase(getApplication().getResources().getString(R.string.text_ok))) {
            errorMessage = result.getDeliveryEstimateAdditionalData().getSpecialMessage();
            if (TextUtils.isEmpty(errorMessage)) {
                errorMessage = getApplication().getString(R.string.text_split_post_default_error_message);
            }
        }
        PaymentHelper.setPinCodeStatus(isCODSupported);
        if (isProceedFurther) {
            if (result.getPinCodeResultList() != null && result.getPinCodeResultList().size() > 0) {
                for (DeliveryEstimatePinCodeResult pinCodeResult : result.getPinCodeResultList()) {
                    if (!cartDetails.getLines().isEmpty()) {
                        for (MStarProductDetails cartLineItem : cartDetails.getLines()) {
                            if (pinCodeResult.getProductsList().contains(String.valueOf(cartLineItem.getProductCode()))) {
                                cartLineItem.setItemSeller(pinCodeResult.getFcName());
                                cartLineItem.setSellerAddress(!TextUtils.isEmpty(pinCodeResult.getFcAddress()) ? pinCodeResult.getFcAddress() : "");
                                cartLineItem.setItemExpiry(DateTimeUtils.getInstance().stringDate(pinCodeResult.getProductDetails().get(String.valueOf(cartLineItem.getProductCode())).get(0).getExpiryDate(), DateTimeUtils.yyyyMMdd, DateTimeUtils.MMMyyyy));
                                cartLineItem.setDeliveryEstimate(checkDeliveryEstimateRange(pinCodeResult.getDeliveryEstimate().getInDates().getOnOrAfter(), pinCodeResult.getDeliveryEstimate().getInDates().getOnOrBefore(), false));
                                estimation.add(checkDeliveryEstimateRange(pinCodeResult.getDeliveryEstimate().getInDates().getOnOrAfter(), pinCodeResult.getDeliveryEstimate().getInDates().getOnOrBefore(), false));
                                estimationWithYear.add(checkDeliveryEstimateRange(pinCodeResult.getDeliveryEstimate().getInDates().getOnOrAfter(), pinCodeResult.getDeliveryEstimate().getInDates().getOnOrBefore(), true));
                            }
                        }
                    }
                }
                itemAdapter.notifyDataSetChanged();
            }
            calculateMaxAndMinDeliveryEstimateDate();
            PaymentHelper.setDeliveryEstimateWithYear(!maxDateRageList.isEmpty() ? DateTimeUtils.getInstance().currentTime(maxDateRageList.get(0), DateTimeUtils.ddMMMyyyy) : "");
        }
        callBack.enableShimmer(false);
        checkIsProceedOrPlaceOrderEnabledAndRxBannerView();
        setOrderConfirmationView();
    }

    private void calculateMaxAndMinDeliveryEstimateDate() {
        if (!minDateRageList.isEmpty() && !maxDateRageList.isEmpty()) {
            Collections.sort(minDateRageList);
            Collections.sort(maxDateRageList, Collections.<Date>reverseOrder());
            Date minDate = minDateRageList.get(0);
            Date maxDate = maxDateRageList.get(0);
            Calendar minRange = Calendar.getInstance();
            minRange.setTime(minDate);
            Calendar maxRange = Calendar.getInstance();
            maxRange.setTime(maxDate);
            PaymentHelper.setDeliveryEstimate((minRange.get(Calendar.MONTH) == maxRange.get(Calendar.MONTH) ? minRange.get(Calendar.DAY_OF_MONTH) : DateTimeUtils.getInstance().currentTime(minDateRageList.get(0), DateTimeUtils.ddMMM)) + " to " + DateTimeUtils.getInstance().currentTime(maxDateRageList.get(0), DateTimeUtils.ddMMM));
        }
    }

    private String checkDeliveryEstimateRange(String minDate, String maxDate, boolean isConvertWithYear) {
        String deliveryEstimateRange;
        Calendar minRange = DateTimeUtils.getInstance().getSeparatedCalendar(minDate);
        Calendar maxRange = DateTimeUtils.getInstance().getSeparatedCalendar(maxDate);
        minDateRageList.add(minRange.getTime());
        maxDateRageList.add(maxRange.getTime());
        if (minRange.get(Calendar.MONTH) == maxRange.get(Calendar.MONTH)) {
            deliveryEstimateRange = String.valueOf(minRange.get(Calendar.DAY_OF_MONTH)) + " to " +
                    DateTimeUtils.getInstance().stringDate(maxDate, DateTimeUtils.yyyyMMdd, isConvertWithYear ? DateTimeUtils.ddMMMyyyy : DateTimeUtils.ddMMM);
        } else
            deliveryEstimateRange = DateTimeUtils.getInstance().stringDate(minDate, DateTimeUtils.yyyyMMdd, DateTimeUtils.ddMMM) + " to " +
                    DateTimeUtils.getInstance().stringDate(maxDate, DateTimeUtils.yyyyMMdd, isConvertWithYear ? DateTimeUtils.ddMMMyyyy : DateTimeUtils.ddMMM);
        return deliveryEstimateRange;
    }

    private void checkIsProceedOrPlaceOrderEnabledAndRxBannerView() {
        boolean isFromSubscription = SubscriptionHelper.getInstance().isCreateNewFillFlag();
        if (!isProceedFurther || (isFromSubscription && !isCODSupported)) {
            callBack.enableOrDisableProceedButton(false);
            callBack.codStatusUndeliveredPinCodeView(errorMessage);
        } else {
            if (!isM2Order()) {
                if (isOutOfStockAvailableInCart) {
                    callBack.enableOrDisableProceedButton(false);
                    callBack.rxInfoSetVisibility(View.GONE);
                    callBack.stockInfoVisibility(View.VISIBLE);
                    callBack.setAlertMessage(getApplication().getResources().getString(R.string.text_out_of_stock), getApplication().getString(R.string.text_product_out_of_stock),
                            getApplication().getResources().getString(R.string.text_remove));
                } else if (isFromSubscription) {
                    if (!(isOrderAmountAboveMinimumPrice() && isOrderAmountNotExceedMaxPrice())) {
                        callBack.checkForSubscriptionProceed(isEligibleForCod(), isOrderAmountAboveMinimumPrice(), isOrderAmountNotExceedMaxPrice());
                        callBack.enableOrDisableProceedButton(false);
                    } else {
                        callBack.rxInfoSetVisibility(View.VISIBLE);
                        callBack.stockInfoVisibility(View.GONE);
                        callBack.enableOrDisableProceedButton(true);
                    }
                } else {
                    if (PaymentHelper.isExternalDoctor()) {
                        callBack.stockInfoVisibility(View.GONE);
                        callBack.rxInfoSetVisibility(View.VISIBLE);
                    }
                    callBack.enableOrDisableProceedButton(true);
                }
            } else {
                // For M2 Order
                if (!PaymentHelper.isExternalDoctor()) {
                    if (isOutOfStockAvailableInCart) {
                        callBack.enableOrDisableM2ProceedButton(false);
                        callBack.rxInfoSetVisibility(View.GONE);
                        callBack.stockInfoVisibility(View.VISIBLE);
                        callBack.setAlertMessage(getApplication().getResources().getString(R.string.text_out_of_stock), getApplication().getString(R.string.text_product_out_of_stock),
                                getApplication().getResources().getString(R.string.text_remove));
                    } else {
                        callBack.enableOrDisableM2ProceedButton(true);
                        if (PaymentHelper.isExternalDoctor()) {
                            callBack.stockInfoVisibility(View.GONE);
                            callBack.rxInfoSetVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    callBack.enableOrDisableM2ProceedButton(true);
                    if (PaymentHelper.isExternalDoctor()) {
                        callBack.stockInfoVisibility(View.GONE);
                        callBack.rxInfoSetVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    private void setOrderConfirmationView() {
        callBack.vmDismissProgress();
        callBack.getBinding().productList.setVisibility(cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty() ? View.VISIBLE : View.GONE);

        setCartProductEmpty(!M2Helper.getInstance().isM2Order() && cartDetails.getLines().isEmpty());
        callBack.updateCartPaymentDetails(cartDetails, isFromRefillSubscription);
        setAdapter(cartDetails.getPrescriptions());

        callBack.showSubscriptionView(hideLayoutEditOder());
        callBack.showPaymentDetails(isM2Order() || isFromRefillSubscription());
        callBack.showPrescriptionLayout(isPrescriptionUploaded());
        callBack.showOffersViewLayout(isCouponApplied());
        callBack.showSubPrescLayout(isPrescriptionUploadedForSub());
        callBack.showDisclaimerText(isM2Order());
        callBack.showPLaceOrderLayout(isM2Order());
        callBack.showTotalAmountLayout(isM2Order());
        callBack.setEmptyView(cartDetails == null);
        callBack.setEmergencyMessageVisibility(isEmergencyMessageVisible());
        if (isM2Order()) {
            callBack.showProductListView(cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty() && cartDetails.getLines().size() > 0 && !PaymentHelper.isExternalDoctor());
        } else {
            callBack.showProductListView(cartDetails != null && cartDetails.getLines() != null && !cartDetails.getLines().isEmpty() && cartDetails.getLines().size() > 0);
        }
    }

    private DeliveryEstimateRequest getDeliveryDateEstimateRequest() {
        DeliveryEstimateRequest request = new DeliveryEstimateRequest();
        request.setCallingToRoute(false);
        request.setDoNotSplit(false);
        request.setPincode(getPinCode());
        List<DeliveryEstimateLineItem> lineItems = new ArrayList<>();
        if (!cartDetails.getLines().isEmpty()) {
            for (MStarProductDetails lineItem : cartDetails.getLines()) {
                DeliveryEstimateLineItem items = new DeliveryEstimateLineItem();
                items.setItemcode(String.valueOf(lineItem.getProductCode()));
                items.setQty(lineItem.getCartQuantity());
                lineItems.add(items);
            }
        }
        request.setLstdrug(lineItems);
        return request;
    }


    public boolean isPrescriptionUploaded() {
        return ((isM2Order() && !uploadedPrescriptionList.isEmpty()) || (!SubscriptionHelper.getInstance().isSubscriptionFlag()
                && (!uploadedPrescriptionList.isEmpty())));
    }

    public boolean isPrescriptionUploadedForSub() {
        return ((SubscriptionHelper.getInstance().isSubscriptionFlag() && !uploadedPrescriptionList.isEmpty()));
    }

    public String m2OfferAmount() {
        return (isM2Order() ? getM2OrderCouponDiscount() + "%" : "");
    }

    public void getPastPrescription() {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        callBack.showNoNetworkView(isConnected);
        if (isConnected) {
            callBack.vmShowProgress();
            APIServiceManager.getInstance().mStarGetPastPrescription(this, basePreference.getMstarBasicHeaderMap(), getPastPrescriptionMultiPartRequest());
        }
    }

    private MultipartBody getPastPrescriptionMultiPartRequest() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart(AppConstant.PAGE_INDEX, AppConstant.PAGE_INDEX_RX);
        builder.addFormDataPart(AppConstant.PAGESIZE, AppConstant.PAGE_SIZE_RX); // pagesize is 1
        return builder.build();
    }

    private void pastPrescriptionResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            GetPastPrescriptionResponse pastPrescriptionResponse = new Gson().fromJson(response, GetPastPrescriptionResponse.class);
            if (pastPrescriptionResponse != null && pastPrescriptionResponse.getStatus() != null && pastPrescriptionResponse.getStatus().equalsIgnoreCase("Success")) {
                showPastPrescription(pastPrescriptionResponse.getResult());
            }
        }
        callBack.vmDismissProgress();
    }

    private void getOrderIdResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel orderIdData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (orderIdData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(orderIdData.getStatus()) && orderIdData.getResult() != null && !(TextUtils.isEmpty(orderIdData.getResult().getOrderId()))) {
                setOrderId(orderIdData.getResult().getOrderId());
                initiateAPICall(APIServiceManager.MSTAR_ORDER_COMPLETE_WITH_COD);
            }
        }
    }

    private void orderCompleteWithCODPesponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            setSubscriptionResponseData(data);
            MStarBasicResponseTemplateModel codDetailsResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (codDetailsResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(codDetailsResponse.getStatus())) {
                appFirstOrder = codDetailsResponse.getResult() != null && !TextUtils.isEmpty(codDetailsResponse.getResult().getIsAppFirstOrder()) ? codDetailsResponse.getResult().getIsAppFirstOrder() : "";
                initiateAPICall(APIServiceManager.MSTAR_CREATE_SUBSCRIPTION);
            }
        }
    }

    private void subscriptionInitiateSuccessResponse(String data) {
        MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (responseTemplateModel != null && AppConstant.SUCCESS_STATUS.equalsIgnoreCase(responseTemplateModel.getStatus())) {
            basePreference.setMStarM1CartIsOpen(true);
            basePreference.setMStarCartId(0);

            /*WebEngage Checkout Completed Event*/
            WebEngageHelper.getInstance().checkoutCompletedEvent(callBack.getContext(), orderId, PaymentHelper.getCustomerAddress(), appFirstOrder, cartDetails, MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList(),
                    PaymentHelper.isPrescriptionOrder(), AppConstant.COD, PaymentHelper.getCustomerBillingAddress(), PaymentHelper.getAnalyticalTrackingProductList());
            callBack.vmOderSuccessFullyActivity(getSubscriptionResponseData(), intent.getBooleanExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, false));

        }
    }

    private void showPastPrescription(GetPastPrescriptionResult result) {
        if (result != null && (result.getDigitizedPrescriptionCount() > 0 || result.getPastOneWeekUnDigitizedRxCount() > 0)) {
            callBack.pastPrescriptionView(result);
        } else {
            callBack.vmShowAlert(getApplication().getResources().getString(R.string.text_past_prescriotion_error));
        }
    }

    public void setPastPrescription(ArrayList<Bitmap> prescriptionBitmap, ArrayList<String> prescriptionList) {
        this.prescriptionBitmapList.addAll(prescriptionBitmap);
        setUploadedPastPrescription(prescriptionList);
        setAdapter(cartDetails.getPrescriptions());
    }

    private void setUploadedPastPrescription(ArrayList<String> prescriptionList) {
        for (String prescription : prescriptionList) {
            setUploadedImage(prescription, true);
        }
    }

    private void setAdapter(ArrayList<String> prescriptions) {
        if (!prescriptions.isEmpty() && prescriptions.size() > 0) {
            callBack.setPrescriptionViewVisibility(true);
            callBack.setUploadedPrescriptionView(prescriptions);
            //callBack.setDataView(true);
        } else callBack.setPrescriptionViewVisibility(false);
    }

    private void setUploadedImage(String path, boolean isDigitized) {
        UploadedPrescriptionList uploadedPrescriptionList = new UploadedPrescriptionList();
        uploadedPrescriptionList.setDigitized(isDigitized);
        uploadedPrescriptionList.setFile(path);
        prescriptionList.add(uploadedPrescriptionList);
    }

    public boolean isCartProductEmpty() {
        return cartProductEmpty;
    }

    public void setCartProductEmpty(boolean cartProductEmpty) {
        this.cartProductEmpty = cartProductEmpty;
    }

    public void checkRxInfoViewVisibilityForOnyPrime() {
        callBack.rxInfoSetVisibility(callBack.getRxInfoVisibility() == View.VISIBLE && !isNonPrimeProduct ? View.GONE : callBack.getRxInfoVisibility());
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    private void checkCartContainPrimeProduct(List<MStarProductDetails> lineItemsList, List<String> primeProductCodesList) {
        isPrimeProduct = false;
        isNonPrimeProduct = false;
        for (MStarProductDetails cartLineItem : lineItemsList) {
            if (primeProductCodesList.contains(String.valueOf(cartLineItem.getProductCode()))) {
                isPrimeProduct = true;
                setPrimeProduct(cartLineItem);
            } else
                isNonPrimeProduct = true;
        }
        checkRxInfoViewVisibilityForOnyPrime();
    }

    private List<MStarProductDetails> setPrimeProductFirstPosition(List<MStarProductDetails> lineItemsList, List<String> primeProductCodesList) {
        MStarProductDetails primeLineItem = null;
        for (Iterator<MStarProductDetails> iterator = lineItemsList.iterator(); iterator.hasNext(); ) {
            MStarProductDetails cartLineItem = iterator.next();
            if (primeProductCodesList.contains(String.valueOf(cartLineItem.getProductCode()))) {
                primeLineItem = cartLineItem;
                iterator.remove();
            }
        }
        if (primeLineItem != null)
            lineItemsList.add(0, primeLineItem);

        return lineItemsList;
    }

    public void onCheckoutContinueEvent() {
        if (cartDetails != null)
            WebEngageHelper.getInstance().checkoutContinuedEvent(callBack.getContext(), cartDetails, deliveryAddress);

        /*Google Tag Manager + FireBase Checkout Progress Step4 Event*/
        FireBaseAnalyticsHelper.getInstance().logFireBaseCheckoutProgressEvent(callBack.getContext(), PaymentHelper.getCartLineItems(), PaymentHelper.getAnalyticalTrackingProductList(), FireBaseAnalyticsHelper.CHECKOUT_STEP_4, FireBaseAnalyticsHelper.EVENT_PARAM_ORDER_REVIEW);
    }

    public boolean isEmergencyMessageVisible() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isEmergencyMessageOrderRevEnabled();
    }

    public boolean emergencyMessageTitleVisibility() {
        return !TextUtils.isEmpty(emergencyMessageTitle());
    }

    public String emergencyMessage() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getEmergencyOrderReviewContent() : "";
    }

    public String emergencyMessageTitle() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getEmergencyOrderReviewTitle() : "";
    }

    public interface OrderConfirmationListener {
        void vmShowProgress();

        void vmDismissProgress();

        void vmPlaceOrder(MstarSubmitMethod2 mstarSubmitMethod2);

        void vmProceedFromConfirmation();

        void vmShowAlert(String alertMessage);

        void vmNavigateToHome();

        void productUnAvailabilityAlert(boolean isOutOfStockAvailableInCart, ArrayList<String> interCityProductList);

        void pastPrescriptionView(GetPastPrescriptionResult result);

        void setUploadedPrescriptionView(ArrayList<String> prescription);

        void sellerDetailDialog(String sellerName, String sellerAddress);

        void vmOderSuccessFullyActivity(String data, boolean isFromRefill);

        void updateCartPaymentDetails(MStarCartDetails cartDetails, boolean isFromRefillSubscription);

        void setEmptyView(boolean isEmpty);

        void setcartItemsAdapter(MStarReviewPageItemAdapter itemAdapter);

        boolean isNetworkConnected();

        void enableOrDisableProceedButton(boolean isEnableProceedButton);

        void enableOrDisableM2ProceedButton(boolean isEnableProceedButton);


        void setPrescriptionViewVisibility(boolean visibility);

        void checkForSubscriptionProceed(boolean eligibleForCod, boolean orderAmountAboveMinimumPrice, boolean orderAmountNotExceedMaxPrice);

        void setAlertMessage(String title, String description, String buttonText);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        void rxInfoVisibility(int visibility);

        void stockInfoVisibility(int visibility);

        void codStatusUndeliveredPinCodeView(String errorMessage);

        void rxInfoSetVisibility(int view);

        int getRxInfoVisibility();

        ActivityConfirmationBinding getBinding();

        void enableShimmer(boolean isEnable);

        Context getContext();

        void changeAddress();

        void setDataView(boolean b);

        void showPaymentDetails(boolean isVisible);

        void showSubscriptionView(boolean isVisible);

        void showPrescriptionLayout(boolean isPrescriptionUploaded);

        void showOffersViewLayout(boolean couponApplied);

        void showSubPrescLayout(boolean prescriptionUploadedForSub);

        void showDisclaimerText(boolean isVisible);

        void showPLaceOrderLayout(boolean isVisible);

        void showTotalAmountLayout(boolean isVisible);

        void showProductListView(boolean isVisible);

        void setEmergencyMessageVisibility(boolean emergencyMessageVisible);

        void showErrorMessage(String errorMessage);

        void executeBinding();
    }
}
