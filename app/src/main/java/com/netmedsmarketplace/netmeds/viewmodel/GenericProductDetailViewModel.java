package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.GenericProductAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarGenericMedicinesResponse;
import com.nms.netmeds.base.model.MstarGenericMedicinesResult;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericProductDetailViewModel extends AppViewModel implements GenericProductAdapter.GenericAdapterCallback {

    private BasePreference basePreference;
    private DetailPageListener listener;
    private MStarProductDetails mainProductDetails, webEngageProductDetails;
    private MstarGenericMedicinesResult genericResult;

    private int updatingProductCode = 0;
    private int qty = 0;
    private int productCode;

    private String saveUpto;

    private boolean isFromGenericAdapter = false;

    // map has product code with qty in cart
    private Map<Integer, Integer> cartProductMap = new HashMap<>();

    private MutableLiveData<String> genericMutableLiveData = new MutableLiveData<>();
    private int stoppedTransactionId;


    public GenericProductDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(BasePreference basePreference, DetailPageListener listener, int productCode) {
        this.productCode = productCode;
        this.basePreference = basePreference;
        this.listener = listener;
    }

    public void getCartDetails() {
        initApiCall(APIServiceManager.MSTAR_GET_CART_DETAILS);
    }

    private void getGenericDetail() {
        isFromGenericAdapter = false;
        initApiCall(APIServiceManager.C_MSTAR_GENERIC_PRODUCTS_DETAILS);
    }


    private void initApiCall(int transactionId) {
        if (listener.isNetworkConnected()) {
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_GENERIC_PRODUCTS_DETAILS:
                    APIServiceManager.getInstance().getMStarGenericProductDetails(this, productCode, AppConstant.GENERIC);
                    break;
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    listener.showLoader();
                    APIServiceManager.getInstance().mStarGetCartDetails(this, basePreference.getMstarBasicHeaderMap(), null, APIServiceManager.MSTAR_GET_CART_DETAILS);
                    break;
                case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                    listener.showLoader();
                    APIServiceManager.getInstance().mStarAddProductToCart(this, basePreference.getMstarBasicHeaderMap(), updatingProductCode, qty, null);
                    break;
                case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                    listener.showLoader();
                    APIServiceManager.getInstance().mStarRemoveProductFromCart(this, basePreference.getMstarBasicHeaderMap(), updatingProductCode, qty, null);
                    break;
                case APIServiceManager.MSTAR_CREATE_CART:
                    APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                    break;

            }
        } else {
            listener.showNoNetworkView(false);
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_GENERIC_PRODUCTS_DETAILS:
                if (!TextUtils.isEmpty(data)) {
                    genericMutableLiveData.setValue(data);
                }
                listener.dismissLoader();
                break;
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                onCartDetailsAvailable(data);
                break;
            case APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART:
                listener.dismissLoader();
                onProductAddedToCart(data);
                break;
            case APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART:
                listener.dismissLoader();
                onRemoveProductResponse(data);
                break;
            case APIServiceManager.MSTAR_CREATE_CART:
                onCartCreated(data);
                break;

        }
    }

    private void onCartCreated(String data) {
        MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus())) {
            basePreference.setMStarCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
            initApiCall(stoppedTransactionId);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.dismissLoader();
        listener.showWebserviceErrorView(true);
    }

    private void onRemoveProductResponse(String data) {
        if (!TextUtils.isEmpty(data) && new Gson().fromJson(data, MStarBasicResponseTemplateModel.class).getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {

        }
        getCartDetails();
    }


    private void onProductAddedToCart(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
                listener.onProductAddedCallBack();
                /*WebEngage AddToCart Event*/
                if (webEngageProductDetails != null)
                    webEngageProductDetails.setPageName(WebEngageHelper.GENERIC);
                WebEngageHelper.getInstance().addToCartEvent(getApplication(), webEngageProductDetails, qty, true);
                /*Google Tag Manager + FireBase AddToCart Event*/
                FireBaseAnalyticsHelper.getInstance().logFireBaseAddToCartEvent(getApplication(), webEngageProductDetails, qty);
                getCartDetails();
            } else if (response != null && response.getReason() != null) {
                if (AppConstant.MSTAR_MAX_LIMIT_BREACH.equalsIgnoreCase(response.getReason().getReason_code())) {
                    listener.showErrorMessage(R.string.text_medicine_max_limit, null);
                } else if (AppConstant.MSTAR_OUT_OF_STOCK.equalsIgnoreCase(response.getReason().getReason_code())) {
                    listener.showErrorMessage(R.string.text_medicine_out_of_stock, null);
                } else {
                    listener.showErrorMessage(0, response.getReason().getReason_eng());
                }
            }
        }
    }

    private void onCartDetailsAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            Map<String, MStarProductDetails> productDetailsGenericMap = new HashMap<>();
            Map<String, MStarProductDetails> productDetailsGenericBrandMap = new HashMap<>();
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            cartProductMap = new HashMap<>();
            if (response != null && response.getResult() != null && response.getResult().getCartDetails() != null && response.getResult().getCartDetails().getLines() != null && response.getResult().getCartDetails().getLines().size() > 0) {
                for (MStarProductDetails productDetails : response.getResult().getCartDetails().getLines()) {
                    cartProductMap.put(productDetails.getProductCode(), productDetails.getCartQuantity());
                    if (isFromGenericAdapter && updatingProductCode == productDetails.getProductCode()) {
                        if (TextUtils.isEmpty(productDetails.getGenericDosage())) {
                            productDetails.setGenericDosage(mainProductDetails.getGenericDosage());
                        }
                        productDetailsGenericMap = basePreference.getGenericProductMap();
                        productDetailsGenericMap.put(String.valueOf(updatingProductCode), productDetails);
                        basePreference.setGenericProductMap(productDetailsGenericMap);
                        productDetailsGenericBrandMap = basePreference.getGenericBrandProductMap();
                        productDetailsGenericBrandMap.put(String.valueOf(updatingProductCode), mainProductDetails);
                        basePreference.setGenericBrandProductMap(productDetailsGenericBrandMap);
                    }
                }
                basePreference.setCartCount(cartProductMap.size());
            }
        }
        getGenericDetail();
    }


    public void onGenericDataAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MstarGenericMedicinesResponse response = new Gson().fromJson(data, MstarGenericMedicinesResponse.class);
            if (response != null && response.getResult() != null && response.getResult() != null && response.getResult().getMainDrug() != null) {
                this.mainProductDetails = response.getResult().getMainDrug();
                this.genericResult = response.getResult();
                listener.setProductsAdapter(response.getResult().getGenericBrands(), cartProductMap, this);
                if (response.getResult().getSaveUpTo() != null && !TextUtils.isEmpty(response.getResult().getSaveUpTo())) {
                    this.saveUpto = response.getResult().getSaveUpTo();
                } else {
                    listener.hideSaveIcon();
                }
                listener.HideOrShowItemPicker(cartProductMap, mainProductDetails);
            }
        }
    }

    @Override
    public void onRetryClickListener() {
        getCartDetails();
        super.onRetryClickListener();
    }

    public String drugName() {
        return mainProductDetails != null && !TextUtils.isEmpty(mainProductDetails.getDisplayName()) ? mainProductDetails.getDisplayName() : "";
    }

    public String perTabletPrice() {
        return mainProductDetails != null && !TextUtils.isEmpty(mainProductDetails.getPerTabletCost()) ? mainProductDetails.getPerTabletCost() : "";
    }

    public String drugDetail() {
        return mainProductDetails != null && !TextUtils.isEmpty(mainProductDetails.getPackLabel()) ? mainProductDetails.getPackLabel() : "";
    }

    public String manufacturerName() {
        return mainProductDetails != null && mainProductDetails.getManufacturer() != null &&
                !TextUtils.isEmpty(mainProductDetails.getManufacturer().getName()) ? AppConstant.MFR + mainProductDetails.getManufacturer().getName() : mainProductDetails != null && !TextUtils.isEmpty(mainProductDetails.getManufacturerName()) ? AppConstant.MFR + mainProductDetails.getManufacturerName() : "";
    }

    public String packSize() {
        return mainProductDetails != null && !TextUtils.isEmpty(mainProductDetails.getPackLabel()) ? mainProductDetails.getPackLabel() : "";
    }

    @SuppressLint("DefaultLocale")
    public String algoliaPrice() {
        return mainProductDetails != null ? CommonUtils.getPriceInFormat(mainProductDetails.getSellingPrice()) : "";
    }

    @SuppressLint("DefaultLocale")
    public String strikePrice() {
        return mainProductDetails != null && isDiscountAvailable() ? CommonUtils.getPriceInFormat(mainProductDetails.getMrp()) : "";
    }

    public boolean isDiscountAvailable() {
        return mainProductDetails != null && mainProductDetails.getDiscount().compareTo(BigDecimal.ZERO) > 0;
    }

    public String saveUpto() {
        return getApplication().getString(R.string.string_save_upto) + " " + this.saveUpto;
    }

    public String composition() {
        return mainProductDetails != null && !TextUtils.isEmpty(mainProductDetails.getGenericDosage()) ? mainProductDetails.getGenericDosage() : "";
    }

    public void openProductDetails() {
        if (mainProductDetails != null) {
            listener.navigateToProductDetailPage(mainProductDetails.getProductCode());
        }
    }

    public boolean isRxProduct() {
        return mainProductDetails != null && mainProductDetails.isRxRequired();
    }

    public void showMore() {
        listener.showMOre();
    }

    public void addMainProduct() {
        if (mainProductDetails != null) {
            webEngageProductDetails = mainProductDetails;
            increaseQuantity(mainProductDetails.getProductCode(), 1, mainProductDetails, false);
        }
    }

    public String genericOffer() {
        return genericResult != null && genericResult.getGenericDiscount() != null && !TextUtils.isEmpty(genericResult.getGenericDiscount().getContent()) ? genericResult.getGenericDiscount().getContent() : "";
    }

    public String toolTipText() {
        return genericResult != null && genericResult.getGenericDiscount() != null && !TextUtils.isEmpty(genericResult.getGenericDiscount().getTooltip()) ? genericResult.getGenericDiscount().getTooltip() : "";
    }

    public void showTooltip() {
        if (!TextUtils.isEmpty(toolTipText())) {
            listener.showToolTip(toolTipText());
        }
    }

    @Override
    public View.OnClickListener removeProductFromCart(final MStarProductDetails cartItems) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartItems != null) {
                    updatingProductCode = cartItems.getProductCode();
                    qty = cartItems.getCartQuantity();
                    initApiCall(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
                }
            }
        };
    }

    @Override
    public void increaseQuantity(int productCode, int updatedQuantity, MStarProductDetails productDetails, boolean isFromGenericAdapter) {
        this.isFromGenericAdapter = isFromGenericAdapter;
        this.updatingProductCode = productCode;
        this.qty = updatedQuantity;
        webEngageProductDetails = productDetails;
        if (PaymentHelper.isIsTempCart() || basePreference.getMStarCartId() > 0) {
            initApiCall(APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART);
        } else {
            stoppedTransactionId = APIServiceManager.MSTAR_ADD_PRODUCT_TO_CART;
            initApiCall(APIServiceManager.MSTAR_CREATE_CART);
        }

    }

    @Override
    public void decreaseQuantity(int productCode, int updatedQuantity, MStarProductDetails productDetails, boolean isFromGenericAdapter) {
        this.isFromGenericAdapter = isFromGenericAdapter;
        this.updatingProductCode = productCode;
        this.qty = updatedQuantity;
        webEngageProductDetails = productDetails;
        if (PaymentHelper.isIsTempCart() || basePreference.getMStarCartId() > 0) {
            initApiCall(APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART);
        } else {
            stoppedTransactionId = APIServiceManager.MSTAR_REMOVE_PRODUCT_FROM_CART;
            initApiCall(APIServiceManager.MSTAR_CREATE_CART);
        }
    }

    @Override
    public View.OnClickListener navigateToProductDetails(final int productCode) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.navigateToProductDetailPage(productCode);
            }
        };
    }

    public void itemQuantityPickerDialog() {
        listener.itemQuantityPickerDialog(mainProductDetails);
    }

    public MutableLiveData<String> getGenericMutableLiveData() {
        return genericMutableLiveData;
    }

    public interface DetailPageListener {

        boolean isNetworkConnected();

        void showLoader();

        void dismissLoader();

        void setProductsAdapter(List<MStarProductDetails> productDetailsList, Map<Integer, Integer> cartProductMap, GenericProductDetailViewModel genericProductDetailViewModel);

        void onProductAddedCallBack();

        void hideSaveIcon();

        void showMOre();

        void navigateToProductDetailPage(int productCode);

        void showNoNetworkView(boolean isConnected);

        void showWebserviceErrorView(boolean isWebserviceError);

        void HideOrShowItemPicker(Map<Integer, Integer> cartProductMap, MStarProductDetails mainProductDetails);

        void itemQuantityPickerDialog(MStarProductDetails mainProductDetails);

        void showErrorMessage(int text_medicine_max_limit, String message);

        void showToolTip(String message);
    }
}
