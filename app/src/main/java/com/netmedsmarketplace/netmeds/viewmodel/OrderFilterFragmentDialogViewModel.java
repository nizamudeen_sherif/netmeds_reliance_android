package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.OrderFilterMainCategoryAdapter;
import com.netmedsmarketplace.netmeds.adpater.OrderFilterSubCategoryAdapter;
import com.netmedsmarketplace.netmeds.databinding.DialogOrderFilterBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.MstarStatusItem;

import java.util.ArrayList;
import java.util.List;

public class OrderFilterFragmentDialogViewModel extends BaseViewModel implements OrderFilterMainCategoryAdapter.OrderFilterMainCategoryAdapterListener, OrderFilterSubCategoryAdapter.OrderFilterSubCategoryAdapterListener {
    private DialogOrderFilterBinding binding;
    private OrderFilterFragmentDialogViewModelListener callback;
    private List<MstarStatusItem> subCategoryModelList = new ArrayList<>();
    private OrderFilterSubCategoryAdapter adapter;
    private String selectedFilterCode;
    private List<MstarStatusItem> statusList;

    public OrderFilterFragmentDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(DialogOrderFilterBinding binding, OrderFilterFragmentDialogViewModelListener listener, String selectedFilter, List<MstarStatusItem> statusList) {
        this.binding = binding;
        this.callback = listener;
        this.selectedFilterCode = selectedFilter;
        this.statusList = statusList;
        getFilterStatusList();
        initFilterAdapter();
        initOrderStatusAdapter();
    }

    private void initFilterAdapter() {
        OrderFilterMainCategoryAdapter adapter = new OrderFilterMainCategoryAdapter(getFilterList(), this);
        binding.rvParentCategory.setNestedScrollingEnabled(false);
        binding.rvParentCategory.setLayoutManager(new LinearLayoutManager(callback.getContext()));
        binding.rvParentCategory.setAdapter(adapter);
    }

    private List<String> getFilterList() {
        List<String> categoryList = new ArrayList<>();
        categoryList.add(callback.getContext().getResources().getString(R.string.text_status));
        return categoryList;
    }

    private void getFilterStatusList() {
        subCategoryModelList = new ArrayList<>();
        subCategoryModelList.add(getFilterData(AppConstant.RECENT, callback.getContext().getString(R.string.text_order_recent)));
        subCategoryModelList.addAll(statusList);
        subCategoryModelList.add(getFilterData(AppConstant.ALL, callback.getContext().getString(R.string.text_order_filter_all)));
        for (MstarStatusItem mstarStatusItem : subCategoryModelList) {
            mstarStatusItem.setChecked(selectedFilterCode.equalsIgnoreCase(mstarStatusItem.getValue()));
        }
    }

    public void closeView() {
        callback.dismissDialog();
    }

    @Override
    public void onCategoryClickListener(int position, String category) {
    }

    private void initOrderStatusAdapter() {
        if (adapter == null) {
            adapter = new OrderFilterSubCategoryAdapter(subCategoryModelList, this);
            binding.rvSubCategory.setNestedScrollingEnabled(false);
            adapter.setHasStableIds(true);
            binding.rvSubCategory.setLayoutManager(new LinearLayoutManager(callback.getContext()));
            binding.rvSubCategory.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private MstarStatusItem getFilterData(String statusCode, String statusTitle) {
        MstarStatusItem mstarStatusItem = new MstarStatusItem();
        mstarStatusItem.setValue(statusCode);
        mstarStatusItem.setTitle(statusTitle);
        return mstarStatusItem;
    }

    @Override
    public void onOrderStatusSelected(List<MstarStatusItem> OrderStatus, int adapterPosition) {
        List<MstarStatusItem> orderFilterStatusModelList = calculateAfterListSelection(OrderStatus, adapterPosition);
        selectedFilterCode = getSelectedFilter(orderFilterStatusModelList);
        if (TextUtils.isEmpty(selectedFilterCode)) {
            orderFilterStatusModelList.get(1).setChecked(true);
            selectedFilterCode = orderFilterStatusModelList.get(1).getValue();

        }
        this.subCategoryModelList = orderFilterStatusModelList;
        initOrderStatusAdapter();
    }

    private List<MstarStatusItem> calculateAfterListSelection(List<MstarStatusItem> orderStatusList, int adapterPosition) {
        List<MstarStatusItem> filterList = new ArrayList<>();
        for (int i = 0; i < orderStatusList.size(); i++) {
            MstarStatusItem mstarStatusItem = orderStatusList.get(i);
            if (i == adapterPosition) {
                mstarStatusItem.setChecked(orderStatusList.get(adapterPosition).isChecked());
            } else {
                mstarStatusItem.setChecked(false);
            }
            filterList.add(mstarStatusItem);
        }
        return filterList;
    }

    private String getSelectedFilter(List<MstarStatusItem> orderStatusList) {
        String filterCode = "";
        for (MstarStatusItem mstarStatusItem : orderStatusList) {
            if (mstarStatusItem.isChecked())
                filterCode = mstarStatusItem.getValue();
        }
        return filterCode;
    }

    public void clearFilter() {
        selectedFilterCode = callback.getContext().getResources().getString(R.string.text_order_recent);
        callback.onApplyFilter(selectedFilterCode);
    }

    public void applyFilter() {
        callback.onApplyFilter(selectedFilterCode);
    }


    public interface OrderFilterFragmentDialogViewModelListener {
        void dismissDialog();

        void onApplyFilter(String filter);

        Context getContext();
    }
}
