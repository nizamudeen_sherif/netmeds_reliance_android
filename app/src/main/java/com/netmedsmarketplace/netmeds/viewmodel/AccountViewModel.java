package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.AppServiceManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentAccountBinding;
import com.netmedsmarketplace.netmeds.db.DatabaseClient;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.model.Request.MstarUpdateCustomerRequest;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.consultation.ConsultationConstant;
import com.nms.netmeds.payment.ui.PaymentConstants;
import com.nms.netmeds.payment.ui.ecrytionViewModel.PaymentUtils;

import java.util.Calendar;
import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AccountViewModel extends AppViewModel {

    private static final String ATTR_MOBILE_NO = "mobileno";
    private MutableLiveData<MStarBasicResponseTemplateModel> customerDetailsLiveData = new MutableLiveData<>();
    public int failedTransactionId = 0;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private FragmentAccountBinding binding;
    private AccountListener listener;
    private MStarCustomerDetails customerDetails;
    private BasePreference basePreference;
    private MstarUpdateCustomerRequest request;
    public boolean isFromEditCustomer = false;
    private MStarCustomerDetails mStarCustomerDetails;
    private ConfigurationResponse configurationResponse;

    public AccountViewModel(@NonNull Application application) {
        super(application);
    }


    public void init(Context context, FragmentAccountBinding binding, AccountListener listener) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        this.basePreference = BasePreference.getInstance(context);
        this.configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        binding.versionId.setText(applicationVersion());
        initShimmer();
        fetchCustomerDetailsFirstTime();
    }

    private void initShimmer() {
        binding.mailIdShimmer.shimmer.startShimmerAnimation();
        binding.profileNameShimmer.shimmer.startShimmerAnimation();
        binding.mobileNoShimmer.shimmer.startShimmerAnimation();
    }

    public String getProfileNameAge() {
        if (customerDetails != null) {
            if (customerDetails.getDateOfBirth() != null) {
                Calendar calendar = DateTimeUtils.getInstance().getSeparatedCalendar(customerDetails.getDateOfBirth());
                return String.format(context.getString(R.string.text_account_profile_name_age), getCustomerName(), DateTimeUtils.getInstance().calculateAgeFromDOB(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
            } else {
                return String.format(context.getString(R.string.text_account_profile_name_age), getCustomerName(), "").replace(", ", "");
            }
        } else {
            return String.format(context.getString(R.string.text_account_profile_name_age), getCustomerName(), "").replace(", ", "");
        }
    }

    private String getCustomerName() {
        String name = "";
        if (customerDetails != null) {
            String firstName = !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "";
            String lastName = !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "";
            name = String.format("%s %s", firstName, lastName);
        }
        return name;
    }

    public String getMailId() {
        return customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "";
    }

    public boolean mailIdVisibility() {
        if (customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()))
            binding.mailIdShimmer.shimmer.stopShimmerAnimation();
        return customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail());
    }

    public boolean profileNameVisibility() {
        if (!TextUtils.isEmpty(getProfileNameAge()) && !TextUtils.isEmpty(getCustomerName()))
            binding.profileNameShimmer.shimmer.stopShimmerAnimation();
        return !TextUtils.isEmpty(getProfileNameAge()) && !TextUtils.isEmpty(getCustomerName());
    }

    public boolean mobileNoVisibility() {
        if (!TextUtils.isEmpty(getMobileNo()))
            binding.mobileNoShimmer.shimmer.stopShimmerAnimation();
        return !TextUtils.isEmpty(getMobileNo());
    }

    public String getMobileNo() {
        return customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? String.format(context.getString(R.string.text_state_code), customerDetails.getMobileNo()) : "";
    }

    public void onEditProfile() {
        listener.onEditProfile();
    }

    public void onPayment() {
        listener.onPaymentClick();
    }

    public void onMedicineOrder() {
        listener.onMedicine();
    }

    public void onConsultation() {
        listener.onConsultation();
    }

    public void onDiagnostic() {
        listener.onDiagnostic();
    }

    public void onOfferClick() {
        listener.onOfferClick();
    }

    public void onHelp() {
        listener.onHelp();
    }

    public void onLegalInfo() {
        listener.onLegalInfo();
    }

    public void myPastPrescription() {
        listener.pastPrescriptions();
    }

    public void onEliteClick() {
        //GA Post Event
        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NETMEDS_FIRST, GoogleAnalyticsHelper.EVENT_ACTION_NETMEDS_FIRST_BUTTON, GoogleAnalyticsHelper.EVENT_LABEL_MY_ACCOUNT);
        listener.onElite();
    }

    public void onLogout() {
        pushUnRegisterFromConsultation();
        PaymentHelper.setSingle_address("");
        BasePreference.getInstance(context).clear();
        BasePreference.getInstance(context).setClearPreference(true);
        WebEngageHelper.getInstance().getUser().logout();
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
        new ClearSearchHistory().execute();
        listener.onLogout();
    }

    public void updateEditProfileToServer(MstarUpdateCustomerRequest request) {
        this.request = request;
        initMstarApi(APIServiceManager.MSTAR_UPDATE_CUSTOMER);
    }

    private void initMstarApi(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, basePreference.getMstarBasicHeaderMap());
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                if (!TextUtils.isEmpty(data)) {
                    MStarBasicResponseTemplateModel model = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                    if (model != null && model.getResult() != null && model.getResult().getCustomerDetails() != null)
                        mStarCustomerDetails = model.getResult().getCustomerDetails();
                    customerDetailsLiveData.setValue(model);
                }
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        if (AppServiceManager.UPDATE_JUS_PAY_CUSTOMER != transactionId) {
            showApiError(transactionId);
        }
    }

    public void onCustomerDetailAvailable(MStarCustomerDetails model) {
        if (model != null) {
            customerDetails = model;
            basePreference.setCustomerDetails(new Gson().toJson(model));
            onPrimeDataAvailable(model.isPrime());
            if (isFromEditCustomer) {
                /*WebEngage Personal Information Filled Event*/
                model.setDateOfBirth(model.getDateOfBirth().replaceAll("00:00:00", "")); // for webengage format
                WebEngageHelper.getInstance().personalInformationFilledEvent(context, model);
                //TODO
                // updateJusPayCustomerDetails();
            }
        }
    }


    private void updateJusPayCustomerDetails() {
        AppServiceManager.getInstance().updateJusPayCustomerDetails(this, updateCustomerRequest(), getJusPayCredentials());
    }

    private String getJusPayCredentials() {
        return PaymentUtils.jusPayCredential(PaymentConstants.JUS_PAY_UPDATE_CUSTOMER, basePreference.getPaymentCredentials());
    }

    private HashMap<String, String> updateCustomerRequest() {
        HashMap<String, String> request = new HashMap<>();
        request.put(PaymentConstants.MOBILE_NUMBER, customerDetails != null && !TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "");
        request.put(PaymentConstants.EMAIL_ADDRESS, customerDetails != null && !TextUtils.isEmpty(customerDetails.getEmail()) ? customerDetails.getEmail() : "");
        request.put(PaymentConstants.FIRST_NAME, customerDetails != null && !TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "");
        request.put(PaymentConstants.LAST_NAME, customerDetails != null && !TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "");
        request.put(PaymentConstants.CUSTOMER_ID, customerDetails != null && customerDetails.getId() > 0 ? String.valueOf(customerDetails.getId()) : "");
        return request;
    }

    private void updateJusPayCustomerResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            //todo
           /* GenerateQuoteResponse response = new Gson().fromJson(data, GenerateQuoteResponse.class);
            if (response != null && !TextUtils.isEmpty(response.getStatus()) && response.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS))
                if (!BuildConfig.DEBUG)
                    listener.deLinkAmazonPay();*/
        }
    }

    @Override
    public void onRetryClickListener() {
        showWebserviceErrorView(false);
        fetchCustomerDetailsFirstTime();
        if (failedTransactionId == AppServiceManager.PUSH_UN_REGISTER)
            pushUnRegisterFromConsultation();
    }

    public void fetchCustomerDetailsFirstTime() {
        initMstarApi(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    public void showApiError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    public void showNoNetworkView(boolean isConnected) {
        binding.contentView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.accountNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        binding.contentView.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.accountApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }


    private String applicationVersion() {
        return getPackageVersionName();
    }

    private String getPackageVersionName() {
        String version = "";
        if (context != null) {
            try {
                PackageManager manager = context.getPackageManager();
                PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
                version = info.versionName + " v" + info.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return version;
    }

    public void onRateAppClick() {
        listener.navigateToRateActivity();
    }

    public boolean isRateUsEnabled() {
        return (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) && configurationResponse.getResult().getConfigDetails().isRatingAppMyAccountEnabled();
    }

    @SuppressLint("StaticFieldLeak")
    class ClearSearchHistory extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                    .searchHistoryDAO()
                    .delete();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

    private void pushUnRegisterFromConsultation() {
        if (TextUtils.isEmpty(basePreference.getJustDocUserResponse())) return;
        boolean isConnected = NetworkUtils.isConnected(context);
        showNoNetworkView(isConnected);
        if (isConnected) {
            SendFcmTokenRequest sendFcmTokenRequest = new SendFcmTokenRequest();
            sendFcmTokenRequest.setEmail(getMailId());
            sendFcmTokenRequest.setToken(basePreference.getFcmToken());
            sendFcmTokenRequest.setUserType(ConsultationConstant.ROLE_USER);
            if (!TextUtils.isEmpty(basePreference.getFcmToken()))
                AppServiceManager.getInstance().pushUnRegisterFromConsultationServer(sendFcmTokenRequest, basePreference);
        } else
            failedTransactionId = AppServiceManager.PUSH_UN_REGISTER;
    }

    public boolean isShowDiagnostic() {
        return (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) && configurationResponse.getResult().getConfigDetails().isDiagnosisEnableFlag();
    }


    private void onPrimeDataAvailable(Boolean isPrimeMember) {
        if (isPrimeMember && CommonUtils.isExpiryDate(mStarCustomerDetails != null ? mStarCustomerDetails : new MStarCustomerDetails()))
            binding.imgProfileImage.setImageResource(R.drawable.ic_super_member);
        else
            binding.imgProfileImage.setImageResource(R.drawable.ic_profile_group);
    }

    private PrimeConfig getPrimeConfig() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null ? configurationResponse.getResult().getConfigDetails().getPrimeConfig() : new PrimeConfig();
    }

    public boolean isEnablePrime() {
        return getPrimeConfig() != null && getPrimeConfig().getPrimeEnableFlag();
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getCustomerDetailsLiveData() {
        return customerDetailsLiveData;
    }

    public interface AccountListener {

        void showSnackMessage(String error, boolean isFromEditOrder);

        void onEditProfile();

        void onPaymentClick();

        void onMedicine();

        void onConsultation();

        void onOfferClick();

        void onHelp();

        void onLegalInfo();

        void onLogout();

        void deLinkAmazonPay();

        void loadMobileNoUpdateScreen();

        void onDiagnostic();

        void onElite();

        void pastPrescriptions();

        void dismissEditDialog();

        void navigateToRateActivity();
    }
}
