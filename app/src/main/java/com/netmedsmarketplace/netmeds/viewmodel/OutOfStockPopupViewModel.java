package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.OutOfStockProductAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentOutOfStockPopUpBinding;
import com.netmedsmarketplace.netmeds.model.RemoveMultipleItemResponse;
import com.netmedsmarketplace.netmeds.model.request.RemoveItemId;
import com.netmedsmarketplace.netmeds.model.request.RemoveMultipleCartItemRequest;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.CartItemResult;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.OutOfStockPopupContent;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OutOfStockPopupViewModel extends AppViewModel {
    private OutOfStockPopupViewModelListener outOfStockPopupViewModelListener;
    private ConfigurationResponse configurationResponse;
    private FragmentOutOfStockPopUpBinding fragmentOutOfStockPopUpBinding;
    private OutOfStockPopupViewModel outOfStockPopupViewModel;
    private Context mContext;
    private BasePreference mBasePreference;
    private List<CartItemResult> outOfStockProductList;
    private boolean isFromCartPage;

    public OutOfStockPopupViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, FragmentOutOfStockPopUpBinding fragmentOutOfStockPopUpBinding, OutOfStockPopupViewModel outOfStockPopupViewModel, List<CartItemResult> outOfStockProductList, boolean isFromCartPage, OutOfStockPopupViewModelListener outOfStockPopupViewModelListener) {
        this.mContext = context;
        this.fragmentOutOfStockPopUpBinding = fragmentOutOfStockPopUpBinding;
        this.outOfStockPopupViewModel = outOfStockPopupViewModel;
        this.outOfStockProductList = outOfStockProductList;
        this.isFromCartPage = isFromCartPage;
        this.outOfStockPopupViewModelListener = outOfStockPopupViewModelListener;
        mBasePreference = BasePreference.getInstance(mContext);
        configurationResponse = new Gson().fromJson(mBasePreference.getConfiguration(), ConfigurationResponse.class);
        setOutOfStockProductAdapter(outOfStockProductList);
    }

    private void setOutOfStockProductAdapter(List<CartItemResult> outOfStockProductList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        fragmentOutOfStockPopUpBinding.outOfStockProductList.setLayoutManager(linearLayoutManager);
        OutOfStockProductAdapter outOfStockProductAdapter = new OutOfStockProductAdapter(mContext, outOfStockProductList);
        fragmentOutOfStockPopUpBinding.outOfStockProductList.setAdapter(outOfStockProductAdapter);
    }

    public void onOutOfStockPopupClose() {
        outOfStockPopupViewModelListener.vmDismiss();
    }

    public void onSubstituteCallBack() {
        outOfStockPopupViewModelListener.vmSubstitute(outOfStockProductList != null && outOfStockProductList.size() > 0 ? outOfStockProductList.get(0) : new CartItemResult());
    }

    public void onRemoveAndProceedCallBack() {
        if (isFromCartPage && (PaymentHelper.isIsFromReOrder() || SubscriptionHelper.getInstance().isPayNowSubscription() || M2Helper.getInstance().isPaynowinitated()))
            outOfStockPopupViewModelListener.vmRemoveOutOfStockProductFromLocal(outOfStockProductList);
        else
            removeOutOfStockProduct();
    }

    private OutOfStockPopupContent getPopupContent() {
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getOutOfStockPopupContent() != null ? configurationResponse.getResult().getConfigDetails().getOutOfStockPopupContent() : new OutOfStockPopupContent();
    }

    public String setTitle() {
        return getPopupContent() != null && !TextUtils.isEmpty(getPopupContent().getTitle()) ? getPopupContent().getTitle() : "";
    }

    public String setDescription() {
        return getPopupContent() != null && !TextUtils.isEmpty(getPopupContent().getDescription()) ? getPopupContent().getDescription() : "";
    }

    public boolean isShowTitle() {
        return TextUtils.isEmpty(setTitle());
    }

    public boolean isShowDescription() {
        return TextUtils.isEmpty(setDescription());
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        //todo
        /* if (transactionId == AppServiceManager.REMOVE_MULTIPLE_CART_ITEM) {
            onRemoveCartItemResponse(data);
        }*/
    }

    @Override
    public void onFailed(int transactionId, String data) {
        outOfStockPopupViewModelListener.vmDismissProgress();
        //todo
        /* if (transactionId == AppServiceManager.REMOVE_MULTIPLE_CART_ITEM) {
            outOfStockPopupViewModelListener.vmShowAlertMessage(mContext.getResources().getString(R.string.text_webservice_error_title));
        }*/
    }

    private void removeOutOfStockProduct() {
        if (NetworkUtils.isConnected(mContext)) {
            outOfStockPopupViewModelListener.vmShowProgress();
            //todo
            // Map<String, String> header = new HashMap<>();
            // header.put(AppConstant.KEY_AUTHORIZATION, mBasePreference.getAuthToken());
            // AppServiceManager.getInstance().removeMultipleItem(outOfStockPopupViewModel, header, getRemoveRequest());
        } else
            outOfStockPopupViewModelListener.vmShowAlertMessage(mContext.getResources().getString(R.string.text_nonetwork_error_message));
    }

    private RemoveMultipleCartItemRequest getRemoveRequest() {
        RemoveMultipleCartItemRequest removeMultipleCartItemRequest = new RemoveMultipleCartItemRequest();
        removeMultipleCartItemRequest.setAddParamList(new ArrayList<String>());
        removeMultipleCartItemRequest.setQuoteId(mBasePreference.getCartId());
        removeMultipleCartItemRequest.setRemoveItemIdList(getRemoveIdList());
        return removeMultipleCartItemRequest;
    }

    private ArrayList<RemoveItemId> getRemoveIdList() {
        ArrayList<RemoveItemId> removeItemIdList = new ArrayList<>();
        for (CartItemResult cartItemResult : outOfStockProductList) {
            RemoveItemId removeItemId = new RemoveItemId();
            removeItemId.setItemId(cartItemResult.getItemId());
            removeItemIdList.add(removeItemId);
        }
        return removeItemIdList;
    }

    private void onRemoveCartItemResponse(String data) {
        outOfStockPopupViewModelListener.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            RemoveMultipleItemResponse removeMultipleItemResponse = new Gson().fromJson(data, RemoveMultipleItemResponse.class);
            if (removeMultipleItemResponse != null && removeMultipleItemResponse.getServiceStatus() != null && removeMultipleItemResponse.getServiceStatus().getStatusCode() != null && removeMultipleItemResponse.getServiceStatus().getStatusCode() == 200 && !TextUtils.isEmpty(removeMultipleItemResponse.getServiceStatus().getMessage()) && removeMultipleItemResponse.getServiceStatus().getMessage().equalsIgnoreCase("success")) {
                outOfStockPopupViewModelListener.vmRemoveAndProceed();
            } else {
                outOfStockPopupViewModelListener.vmShowAlertMessage(mContext.getResources().getString(R.string.text_webservice_error_title));
            }
        } else {
            outOfStockPopupViewModelListener.vmShowAlertMessage(mContext.getResources().getString(R.string.text_webservice_error_title));
        }
    }

    public String setRemoveAndProceedButtonText() {
        return isFromCartPage ? mContext.getResources().getString(R.string.text_remove) : mContext.getResources().getString(R.string.text_remove_and_proceed);
    }

    public interface OutOfStockPopupViewModelListener {
        void vmDismiss();

        void vmSubstitute(CartItemResult cartItemResult);

        void vmRemoveAndProceed();

        void vmShowProgress();

        void vmDismissProgress();

        void vmShowAlertMessage(String message);

        void vmRemoveOutOfStockProductFromLocal(List<CartItemResult> outOfStockProductList);
    }
}
