package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityViewOfferDetailsBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.utils.NetworkUtils;

import java.util.ArrayList;

public class ViewOfferDetailsViewModel extends AppViewModel {
    private Intent intent;
    private ViewOfferDetailsListener callback;
    private ActivityViewOfferDetailsBinding binding;
    private MstarNetmedsOffer model;
    private boolean isFromDeepLink = false;
    private String offerId = "";
    private final MutableLiveData<MstarNetmedsOffer> offerMutableLiveData = new MutableLiveData<>();

    public ViewOfferDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<MstarNetmedsOffer> getOfferMutableData() {
        return offerMutableLiveData;
    }

    public void init(ActivityViewOfferDetailsBinding binding, ViewOfferDetailsListener listener, Intent intent) {
        this.binding = binding;
        this.callback = listener;
        this.intent = intent;
        loadPageUsingIntentData();
    }

    public String getOfferTitle() {
        return model.getTitle();
    }

    public String getOfferTitleSortDescription() {
        return model != null && !TextUtils.isEmpty(model.getCouponDescription()) ? model.getCouponDescription() : "";
    }

    public String getOfferCouponCode() {
        return model != null && !TextUtils.isEmpty(model.getCouponCode()) ? model.getCouponCode() : "";
    }

    public String getOfferEligibility() {
        return model != null && !TextUtils.isEmpty(model.getEligibility()) ? model.getEligibility() : "";
    }

    public String getContent() {
        return model != null && !TextUtils.isEmpty(model.getContent()) ? model.getContent() : "";
    }

    public String getOfferHowDoYouGetIt() {
        return model != null && !TextUtils.isEmpty(model.getHowToGet()) ? model.getHowToGet() : "";
    }

    public String getOfferConditionCancellation() {
        return model != null && !TextUtils.isEmpty(model.getConditions()) ? model.getConditions() : "";
    }

    private void loadPageUsingIntentData() {
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
                onFromDeepLink();
            } else {
                onFromOffers();
            }
        }
    }

    private void onFromOffers() {
        setFromDeepLink(false);
        if (intent.hasExtra(IntentConstant.OFFER_DETAILS)) {
            model = new Gson().fromJson(intent.getStringExtra(IntentConstant.OFFER_DETAILS), MstarNetmedsOffer.class);
            offerMutableLiveData.setValue(model);
            binding.contentView.setVisibility(View.VISIBLE);
        } else if (intent.hasExtra(IntentConstant.OFFER_DETAILS_PAGE_ID)) {
            callback.showLoadingProgress();
            offerId = intent.getStringExtra(IntentConstant.OFFER_DETAILS_PAGE_ID);
            initiateApiCall(intent.getStringExtra(IntentConstant.OFFER_DETAILS_PAGE_ID));
        }
    }

    private void onFromDeepLink() {
        setFromDeepLink(true);
        ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
        if (intentParamList != null && intentParamList.size() > 0) {
            offerId = intentParamList.get(0);
            callback.showLoadingProgress();
            initiateApiCall(!TextUtils.isEmpty(offerId) && TextUtils.isDigitsOnly(offerId) ? offerId : "0");
        }
    }

    private void initiateApiCall(String pageId) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        showNoNetworkView(isConnected);
        if (isConnected && !TextUtils.isEmpty(pageId) && TextUtils.isDigitsOnly(pageId)) {
            binding.contentView.setVisibility(View.GONE);
            APIServiceManager.getInstance().mstarGetAllOffers(this, Integer.valueOf(pageId));
        } else
            callback.dismissLoadingProgress();
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        if (transactionId == APIServiceManager.C_MSTAR_ALL_OFFERS) {
            initOffers(data);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        navigateToFallback();
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        initiateApiCall(!TextUtils.isEmpty(offerId) && TextUtils.isDigitsOnly(offerId) ? offerId : "0");
    }

    private void initOffers(String data) {
        if (TextUtils.isEmpty(data)) {
            navigateToFallback();
            return;
        }

        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null && response.getResult().getOfferList() != null && response.getResult().getOfferList().size() > 0) {
            validateOfferIdTextOrDigit(response);
        } else
            navigateToFallback();
    }

    private void validateOfferIdTextOrDigit(MStarBasicResponseTemplateModel response) {
        if (!TextUtils.isEmpty(offerId) && TextUtils.isDigitsOnly(offerId)) {
            callback.dismissLoadingProgress();
            validateOfferSubList(response);
        } else {
            offerId = getOfferIdFromAllOffers(response);
            checkOfferIdAvailable();
        }
    }

    private void validateOfferSubList(MStarBasicResponseTemplateModel response) {
        MstarNetmedsOffer offerDetail = response.getResult().getOfferList().get(0);
        if (offerDetail != null && offerDetail.getOfferList() != null && offerDetail.getOfferList().size() > 0) {
            callback.vmNavigateToOfferPage(new Gson().toJson(offerDetail.getOfferList()),offerDetail.getTitle()!=null &&!TextUtils.isEmpty(offerDetail.getTitle())? offerDetail.getTitle():"");
        } else {
            binding.contentView.setVisibility(View.VISIBLE);
            offerMutableLiveData.setValue(response.getResult().getOfferList().get(0));
        }
    }

    private void checkOfferIdAvailable() {
        if (!TextUtils.isEmpty(offerId) && TextUtils.isDigitsOnly(offerId))
            initiateApiCall(offerId);
        else
            navigateToFallback();
    }

    public void loadOffer(MstarNetmedsOffer response) {
        model = response;
        Glide.with(callback.getContext()).load(ImageUtils.checkImage(model.getImage()))
                .apply(new RequestOptions().circleCrop())
                .error(Glide.with(binding.imgOffer).load(R.drawable.ic_no_image))
                .into(binding.imgOffer);
        if (model != null && !TextUtils.isEmpty(model.getBannerImg())) {
            binding.cvOfferImage.setVisibility(View.VISIBLE);
            Glide.with(callback.getContext())
                    .load(ImageUtils.checkImage(model.getBannerImg()))
                    .error(Glide.with(binding.imgFullOffer).load(R.drawable.ic_no_image))
                    .into(binding.imgFullOffer);
        } else {
            binding.cvOfferImage.setVisibility(View.GONE);
        }
    }

    public void forMoreDetailsClick() {
        binding.cvForMoreDetails.setClickable(false);
        callback.forMoreDetailsClick(model);
    }

    public void onClickOfCopyCodeAndShop() {
        boolean doCopyClipboard = model.getBtnText().toLowerCase().contains(getApplication().getResources().getString(R.string.text_copy).toLowerCase());
        callback.onClickOfCopyCodeAndShop(getOfferCouponCode(), doCopyClipboard, model.getShopUrl());
    }

    private void showNoNetworkView(boolean isConnected) {
        binding.contentView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.viewOfferNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void showWebserviceErrorView() {
        binding.contentView.setVisibility(View.GONE);
        binding.viewOfferApiErrorView.setVisibility(View.VISIBLE);
    }

    public String setBtnText() {
        return model.getBtnText();
    }

    public boolean isFromDeepLink() {
        return isFromDeepLink;
    }

    private void setFromDeepLink(boolean fromDeepLink) {
        isFromDeepLink = fromDeepLink;
    }

    private String getOfferIdFromAllOffers(MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel) {
        if (mStarBasicResponseTemplateModel != null && mStarBasicResponseTemplateModel.getResult() != null && mStarBasicResponseTemplateModel.getResult().getOfferList() != null && mStarBasicResponseTemplateModel.getResult().getOfferList().size() > 0) {
            for (MstarNetmedsOffer mstarNetmedsOffer : mStarBasicResponseTemplateModel.getResult().getOfferList()) {
                Uri uri = !TextUtils.isEmpty(mstarNetmedsOffer.getUrl()) ? Uri.parse(mstarNetmedsOffer.getUrl()) : null;
                String path = uri != null ? uri.getLastPathSegment() : "";
                if (offerId.equals(path)) {
                    return !TextUtils.isEmpty(mstarNetmedsOffer.getPageId()) ? mstarNetmedsOffer.getPageId() : "";
                }
            }
        }
        return "";
    }

    private void navigateToFallback() {
        callback.dismissLoadingProgress();
        callback.vmNavigateToOfferPage("","");
    }

    public interface ViewOfferDetailsListener {
        void showLoadingProgress();

        void dismissLoadingProgress();

        void onClickOfCopyCodeAndShop(String couponCode, boolean doCopyClipboard, String shopUrl);

        void forMoreDetailsClick(MstarNetmedsOffer details);

        void vmNavigateToOfferPage(String offerList,String title);

        Context getContext();
    }
}
