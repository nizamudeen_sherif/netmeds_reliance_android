package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySigninWithOtpBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

public class SignInWithOtpViewModel extends AppViewModel {

    private ActivitySigninWithOtpBinding otpBinding;
    private SignInWithOtpListener signInWithOtpListener;
    private String phoneNumber;
    private int failedTransactionId;
    private Intent getIntent;

    public SignInWithOtpViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ActivitySigninWithOtpBinding signinWithOtpBinding, SignInWithOtpListener signInWithOtpListener, String phoneNumber, Intent getIntent) {
        this.otpBinding = signinWithOtpBinding;
        this.signInWithOtpListener = signInWithOtpListener;
        this.phoneNumber = phoneNumber;
        this.getIntent = getIntent;
        imageOptionButtonClick();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void onEditTextFirstTextChanged(int count) {
        if (count > 0) {
            otpBinding.txtOtp2.requestFocus();
        }
    }

    public void onEditTextSecondTextChanged(int count) {
        if (count > 0) {
            otpBinding.txtOtp3.requestFocus();
        } else {
            otpBinding.txtOtp1.requestFocus();
        }

    }

    public void onEditTextThirdTextChanged(int count) {
        if (count > 0) {
            otpBinding.txtOtp4.requestFocus();
        } else {
            otpBinding.txtOtp2.requestFocus();
        }
    }

    public void onEditTextFourTextChanged(int count) {
        if (count > 0) {
            otpBinding.txtOtp5.requestFocus();
        } else {
            otpBinding.txtOtp3.requestFocus();
        }
    }

    public void onEditTextFiveTextChanged(int count) {
        if (count > 0) {
            otpBinding.txtOtp6.requestFocus();
        } else {
            otpBinding.txtOtp4.requestFocus();
        }
    }

    public void onEditTextSixTextChanged(int count) {
        if (count <= 0) {
            otpBinding.txtOtp5.requestFocus();
        }

    }

    public void onclickVerify() {
        if (isOTPEmpty()) {
            signInWithOtpListener.vmSnackbarMessage(getApplication().getString(R.string.text_enter_otp));
        } else if (validateForm()) {
            verifyOtp();
        } else {
            signInWithOtpListener.vmSnackbarMessage(getApplication().getString(R.string.error_otp_validation));
        }
    }

    public void onclickResendOtp() {
        sendOtp();
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        onRetry(failedTransactionId);
        showWebserviceErrorView(false);
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case APIServiceManager.MSTAR_VERIFY_OTP:
                verifyOtp();
                break;
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                sendOtp();
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                getCustomerDetails();
                break;
        }
    }

    public void onclickLoginWithPassword() {
        signInWithOtpListener.vmCallBackOnNavigationSignInWithPasswordActivity();
    }

    public void onclickChangePhoneNumber() {
        signInWithOtpListener.vmCallBackOnNavigationMobileNumberActivity();
    }

    private void sendOtp() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            signInWithOtpListener.vmShowProgress();
            APIServiceManager.getInstance().mstarResendSameOTP(this, getIntent.getStringExtra(IntentConstant.RANDOM_KEY));
        } else {
            failedTransactionId = APIServiceManager.MSTAR_RESEND_SAME_OTP;
        }
    }

    private void verifyOtp() {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        showNoNetworkView(isConnected);
        if (isConnected) {
            signInWithOtpListener.vmShowProgress();
            APIServiceManager.getInstance().mStarVerifyOtp(this, phoneNumber, getIntent.getStringExtra(IntentConstant.RANDOM_KEY), getOtp());
        } else {
            failedTransactionId = APIServiceManager.MSTAR_VERIFY_OTP;
        }
    }

    private void initiateAPICall(int transactionID) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionID) {
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    signInWithOtpListener.vmShowProgress();
                    APIServiceManager.getInstance().mStarCustomerDetails(this, BasePreference.getInstance(getApplication()).getMstarBasicHeaderMap());
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.MSTAR_VERIFY_OTP:
                otpVerifyResponse(data);
                break;
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
                OtpResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        signInWithOtpListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_RESEND_SAME_OTP:
            case APIServiceManager.MSTAR_VERIFY_OTP:
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                vmOnError(transactionId);
                break;
        }
    }

    private void getCustomerDetails() {
        APIServiceManager.getInstance().mStarCustomerDetails(this, BasePreference.getInstance(getApplication()).getMstarBasicHeaderMap());

    }

    private String getOtp() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(otpBinding.txtOtp1 != null && otpBinding.txtOtp1.getText() != null ? otpBinding.txtOtp1.getText().toString() : "");
        stringBuilder.append(otpBinding.txtOtp2 != null && otpBinding.txtOtp2.getText() != null ? otpBinding.txtOtp2.getText().toString() : "");
        stringBuilder.append(otpBinding.txtOtp3 != null && otpBinding.txtOtp3.getText() != null ? otpBinding.txtOtp3.getText().toString() : "");
        stringBuilder.append(otpBinding.txtOtp4 != null && otpBinding.txtOtp4.getText() != null ? otpBinding.txtOtp4.getText().toString() : "");
        stringBuilder.append(otpBinding.txtOtp5 != null && otpBinding.txtOtp5.getText() != null ? otpBinding.txtOtp5.getText().toString() : "");
        stringBuilder.append(otpBinding.txtOtp6 != null && otpBinding.txtOtp6.getText() != null ? otpBinding.txtOtp6.getText().toString() : "");
        return String.valueOf(stringBuilder);
    }

    private boolean validateForm() {
        boolean isValidate = true;
        if (TextUtils.isEmpty(otpBinding.txtOtp1.getText().toString())) {
            isValidate = false;
        }
        if (TextUtils.isEmpty(otpBinding.txtOtp2.getText().toString())) {
            isValidate = false;
        }
        if (TextUtils.isEmpty(otpBinding.txtOtp3.getText().toString())) {
            isValidate = false;
        }
        if (TextUtils.isEmpty(otpBinding.txtOtp4.getText().toString())) {
            isValidate = false;
        }
        if (TextUtils.isEmpty(otpBinding.txtOtp5.getText().toString())) {
            isValidate = false;
        }
        if (TextUtils.isEmpty(otpBinding.txtOtp6.getText().toString())) {
            isValidate = false;
        }
        return isValidate;
    }

    private boolean isOTPEmpty() {
        return TextUtils.isEmpty(otpBinding.txtOtp1.getText().toString())
                && TextUtils.isEmpty(otpBinding.txtOtp2.getText().toString())
                && TextUtils.isEmpty(otpBinding.txtOtp3.getText().toString())
                && TextUtils.isEmpty(otpBinding.txtOtp4.getText().toString())
                && TextUtils.isEmpty(otpBinding.txtOtp5.getText().toString())
                && TextUtils.isEmpty(otpBinding.txtOtp6.getText().toString());
    }

    private void showNoNetworkView(boolean isConnected) {
        otpBinding.signOtpViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        otpBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        showWebserviceErrorView(true);
    }

    private void showWebserviceErrorView(boolean isWebserviceError) {
        otpBinding.signOtpViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        otpBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void otpVerifyResponse(String data) {
        signInWithOtpListener.vmDismissProgress();
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null && response.getResult().getSession() != null) {
            BasePreference basePreference = BasePreference.getInstance(getApplication());
            basePreference.setMStarSessionId(!TextUtils.isEmpty(response.getResult().getSession().getId()) ? response.getResult().getSession().getId() : "", String.valueOf(response.getResult().getSession().getCustomerId() != null ? response.getResult().getSession().getCustomerId() : ""));
            basePreference.setMstarCustomerId(String.valueOf(response.getResult().getSession().getCustomerId()));
            basePreference.setLoganSession(!TextUtils.isEmpty(response.getResult().getSession().getLoganSessionId()) ? response.getResult().getSession().getLoganSessionId() : "");
            //Google Analytics Event
            GoogleAnalyticsHelper.getInstance().postEvent(getApplication().getApplicationContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_LOGIN_WITH_OTP_SUCCESSFUL, GoogleAnalyticsHelper.EVENT_LABEL_LOGIN_WITH_OTP);

            /*FireBase Analytics Event*/
            FireBaseAnalyticsHelper.getInstance().loginEvent(getApplication().getApplicationContext(), AppConstant.LOGIN_OTP);
            basePreference.setGuestCart(false);
            getCustomerDetails();
        } else {
            SnackBarHelper.snackBarCallBack(otpBinding.signOtpViewContent, getApplication().getApplicationContext(), getApplication().getResources().getString(R.string.text_invalid_otp));
        }
    }

    private void customerDetailsResponse(String data) {
        BasePreference.getInstance(getApplication()).setUserDetails(data);
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && addressData.getResult() != null && addressData.getResult().getCustomerDetails() != null) {
            BasePreference.getInstance(getApplication()).setCustomerDetails(new Gson().toJson(addressData.getResult().getCustomerDetails()));

            //Set WebEngage Login
            WebEngageHelper.getInstance().setWebEngageLogin(BasePreference.getInstance(getApplication()), WebEngageHelper.DIRECT_SIGN_IN, false, signInWithOtpListener.getContext());
            //MAT Login Event
            MATHelper.getInstance().loginAndRegistrationMATEvent(BasePreference.getInstance(getApplication()), false);
        }
        signInWithOtpListener.vmNavigateToHome();
    }

    private void OtpResponse(String data) {
        signInWithOtpListener.vmDismissProgress();
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response != null && response.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus())) {
            signInWithOtpListener.vmSnackbarMessage(getApplication().getResources().getString(R.string.text_otp_resend));
            signInWithOtpListener.vmCounterTimer();
            otpBinding.txtOtp1.setText("");
            otpBinding.txtOtp2.setText("");
            otpBinding.txtOtp3.setText("");
            otpBinding.txtOtp4.setText("");
            otpBinding.txtOtp5.setText("");
            otpBinding.txtOtp6.setText("");
            otpBinding.txtOtp1.requestFocus();
        } else if (response != null && response.getReason() != null && response.getReason().getReason_eng() != null) {
            //signInWithOtpListener.vmSnackbarMessage(response.getReason().getReason_eng());
            signInWithOtpListener.navigateToLoginPageWithErrorMessage(response.getReason().getReason_eng());
        }
    }

    private void imageOptionButtonClick() {
        otpBinding.txtOtp6.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isOTPEmpty()) {
                        signInWithOtpListener.vmSnackbarMessage(getApplication().getString(R.string.text_enter_otp));
                    } else if (validateForm()) {
                        verifyOtp();
                    } else {
                        signInWithOtpListener.vmSnackbarMessage(getApplication().getString(R.string.error_otp_validation));
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

    public interface SignInWithOtpListener {
        void vmCallBackOnNavigationSignInWithPasswordActivity();

        void vmCallBackOnNavigationMobileNumberActivity();

        void vmShowProgress();

        void vmDismissProgress();

        void vmNavigateToHome();

        void vmCounterTimer();

        void vmSnackbarMessage(String message);

        Context getContext();

        void navigateToLoginPageWithErrorMessage(String errorMessage);
    }
}
