package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarWellnessSectionDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;

import java.util.ArrayList;
import java.util.List;

public class WellnessViewModel extends AppViewModel {

    private WellnessListener callbackListner;
    private List<MstarWellnessSectionDetails> adapterList = new ArrayList<>();

    private MutableLiveData<MStarBasicResponseTemplateModel> wellnessPageLiveData = new MutableLiveData<>();

    public MutableLiveData<MStarBasicResponseTemplateModel> getWellnessPageLiveData() {
        return wellnessPageLiveData;
    }

    public WellnessViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(WellnessListener listener) {
        this.callbackListner = listener;
        initCategory();

    }

    private void initCategory() {
        boolean isConnected = callbackListner.isNetworkConnected();
        callbackListner.showNoNetworkView(isConnected);
        if (isConnected) {
            callbackListner.vmShowProgress();
            APIServiceManager.getInstance().mstarGetWellnessPageDetails(this);
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        if (transactionId == APIServiceManager.C_MSTAR_WELLNESS_PAGE_DETAILS_V2) {
            if (!TextUtils.isEmpty(data)) {
                MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                wellnessPageLiveData.setValue(response);
            }
        }

    }

    public void onWellPageDetailsAvailable(MstarBasicResponseResultTemplateModel resultModel) {
        callbackListner.vmDismissProgress();
        if (resultModel != null && resultModel.getWellnessSections()!= null && !resultModel.getWellnessSections().isEmpty()) {
            adapterList.addAll(resultModel.getWellnessSections());
            callbackListner.setWellnessAdapter(adapterList);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        callbackListner.vmDismissProgress();
        super.onFailed(transactionId, data);
        callbackListner.showApiErrorView(true);
    }
//
//    private WelnessSectionFlag welnessSectionFlag() {
//        WelnessSectionFlag welnessSectionFlag = null;
//        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(mContext).getConfiguration(), ConfigurationResponse.class);
//        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getWelnessSectionFlag() != null) {
//            welnessSectionFlag = configurationResponse.getResult().getWelnessSectionFlag();
//        }
//        return welnessSectionFlag;
//    }


    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        callbackListner.showNoNetworkView(false);
        callbackListner.showApiErrorView(false);
        initCategory();

    }

    public interface WellnessListener {
        void vmShowProgress();

        void vmDismissProgress();

        boolean isNetworkConnected();

        void showNoNetworkView(boolean isConnected);

        void showApiErrorView(boolean isAPIerror);

        //void setWellnessAdapter(List<MstarBasicResponseResultTemplateModel> adapterList);
        void setWellnessAdapter(List<MstarWellnessSectionDetails> adapterList);
    }
}
