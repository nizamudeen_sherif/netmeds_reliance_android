package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.netmedsmarketplace.netmeds.adpater.AllOfferAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentOfferTabBinding;
import com.nms.netmeds.base.BaseViewModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

public class OfferTabFragmentViewModel extends BaseViewModel {

    private Context context;
    private FragmentOfferTabBinding binding;
    private OfferTabFragmentViewModelListener listener;
    private List<MstarNetmedsOffer> offerList;

    public OfferTabFragmentViewModel(@NonNull Application application) {
        super(application);
    }

   /* @BindingAdapter({"app:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .error(Glide.with(view).load(R.drawable.ic_no_image))
                .into(view);
    }*/

    public void init(Context context, String data, FragmentOfferTabBinding binding, OfferTabFragmentViewModelListener listener) {
        this.context = context;
        this.binding = binding;
        this.listener = listener;
        this.offerList = getOfferList(data);
        initiateAdapter();
    }

    private List<MstarNetmedsOffer> getOfferList(String data) {
        if (TextUtils.isEmpty(data)) {
            return new ArrayList<>();
        } else {
            return new Gson().fromJson(data, new TypeToken<List<MstarNetmedsOffer>>() {
            }.getType());
        }
    }

    private void initiateAdapter() {
        if (offerList != null) {
            binding.offerList.setVisibility(!offerList.isEmpty() ? View.VISIBLE : View.GONE);
            AllOfferAdapter offerAdapter = new AllOfferAdapter(offerList, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            binding.offerList.setLayoutManager(linearLayoutManager);
            binding.offerList.setAdapter(offerAdapter);
        } else {
            binding.offerList.setVisibility(View.GONE);
        }
    }

    public String getOfferAdapterExpiryDate(String endDate) {
        return DateTimeUtils.getInstance().stringDate(endDate, DateTimeUtils.yyyyMMddHHmmss, DateTimeUtils.ddMMMyyyy);
    }

    public void onOfferAdapterItemClick(MstarNetmedsOffer item) {
        listener.onViewParticularOfferDetails(item);
    }

    public interface OfferTabFragmentViewModelListener {
        void onViewParticularOfferDetails(MstarNetmedsOffer resultModel);
    }
}
