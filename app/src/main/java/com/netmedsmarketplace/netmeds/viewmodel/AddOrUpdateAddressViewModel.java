package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.GetCityStateFromPinCodeResponse;
import com.nms.netmeds.base.model.MStarAddAddressModelRequest;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MstarEditAddressRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.diagnostic.DiagnosticServiceManager;
import com.nms.netmeds.diagnostic.model.PinCodeResponse;
import com.nms.netmeds.diagnostic.model.PinCodeResult;
import com.nms.netmeds.diagnostic.model.Request.PinCodeServiceCheckRequest;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;

import java.util.Map;


public class AddOrUpdateAddressViewModel extends AppViewModel {

    private BasePreference basePreference;
    private ViewModelListener callBack;
    private MStarCustomerDetails customerDetails;

    private String city;
    private String state;
    private String pincode;
    private String intentFromDiagnostic = "";
    private int diagnosticLabId;
    private int failedTransactionId = 0;
    private int billingAndShippingAddressId = 0;
    private boolean isSetShippingAddressAPICallCompleted = false;
    private boolean isSetBillingAddressAPICallCompleted = false;
    private boolean isSetShippingAddressCompleted = false;
    private boolean isSetBillingAddressCompleted = false;
    private boolean isValidPinCode = false;
    private boolean isForAddAddress;
    private MStarAddressModel updatingAddress;
    private PinCodeServiceCheckRequest pinCodeServiceCheckRequest;
    private int billingAddressID = 0;
    private boolean fromDiagnosticPatientDetails = false;

    private int getDiagnosticLabId() {
        return diagnosticLabId;
    }

    public void setDiagnosticLabId(int diagnosticLabId) {
        this.diagnosticLabId = diagnosticLabId;
    }

    public String getIntentFromDiagnostic() {
        return intentFromDiagnostic;
    }

    public void setIntentFromDiagnostic(String intentFromDiagnostic) {
        this.intentFromDiagnostic = intentFromDiagnostic;
    }

    public AddOrUpdateAddressViewModel(@NonNull Application application) {
        super(application);
    }

    public void initViewModel(BasePreference preference, ViewModelListener viewModelListener) {
        this.basePreference = preference;
        this.callBack = viewModelListener;
        initiateProperties();
        callBack.vmDismissProgress();
    }

    private void initiateProperties() {
        setUserDetails();
        setErrorWatcher();
        if (getIntentFromDiagnostic() != null && getIntentFromDiagnostic().equals(DiagnosticConstant.KEY_DIAGNOSTIC))
            callBack.continueButtonVisibility(View.GONE);
        isForAddAddress = updatingAddress.getId() == -1;
        if (!isForAddAddress) {
            initiateEditAddressViews();
        } else {
            initiateNewAddressViews();
        }
    }

    public void initiateAPICall(int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_CART:
                APIServiceManager.getInstance().mStarCreateCart(this, basePreference.getMstarBasicHeaderMap());
                break;
            case APIServiceManager.C_MSTAR_GET_CITY_STATE_FROM_PINCODE:
                APIServiceManager.getInstance().getMstarPincodeDetails(this, getPincode());
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                DiagnosticServiceManager.getInstance().pinCodeServiceAvailabilityCheck(this, pinCodeServiceCheckRequest, basePreference);
                break;
            case APIServiceManager.MSTAR_ADD_NEW_ADDRESS:
                APIServiceManager.getInstance().mStarAddAddress(this, basePreference.getMstarBasicHeaderMap(), getAddAddressRequest(updatingAddress));
                break;
            case APIServiceManager.MSTAR_UPDATE_ADDRESS:
                APIServiceManager.getInstance().mStarUpdateAddress(this, basePreference.getMstarBasicHeaderMap(), getEditAddressRequest());
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                APIServiceManager.getInstance().mStarSetPreferredShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAndShippingAddressId, APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS == transactionId);
                break;
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
                APIServiceManager.getInstance().mStarSetShippingAndBillingAddress(this, basePreference.getMstarBasicHeaderMap(), billingAndShippingAddressId, APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS == transactionId,
                        M2Helper.getInstance().isM2Order() ? basePreference.getMstarMethod2CartId() : SubscriptionHelper.getInstance().isPayNowSubscription() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : 0);
                break;
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                APIServiceManager.getInstance().mStarSingleAddressDetails(this, basePreference.getMstarBasicHeaderMap(), billingAddressID);
                break;
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        switch (transactionId) {
            case APIServiceManager.MSTAR_CREATE_CART:
                createCartResponse(data);
                break;
            case APIServiceManager.C_MSTAR_GET_CITY_STATE_FROM_PINCODE:
                getCityStateFromPinCodeResponse(data);
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                getPinCodeResponse(data);
                break;
            case APIServiceManager.MSTAR_UPDATE_ADDRESS:
                updateAddressResponse(data);
                break;
            case APIServiceManager.MSTAR_ADD_NEW_ADDRESS:
                addAddressResponse(data);
                break;
            case APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS:
            case APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS:
                break;
            case APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS:
                isSetShippingAddressAPICallCompleted = true;
                settingBillingAndShippingAddressResponse(data);
                break;

            case APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS:
                isSetBillingAddressAPICallCompleted = true;
                settingBillingAndShippingAddressResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                singleAddressResponse(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        callBack.vmDismissProgress();
        failedTransactionId = transactionId;
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_GET_CITY_STATE_FROM_PINCODE:
                isValidPinCode = false;
                callBack.setPincodeError(getApplication().getResources().getString(R.string.text_valid_pincode));
                break;
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                isValidPinCode = false;
                callBack.setPincodeError(getApplication().getResources().getString(R.string.text_valid_pincode));
                callBack.continueButtonVisibility(View.GONE);
                break;
            case APIServiceManager.MSTAR_UPDATE_ADDRESS:
                updateAddressFailedResponse();
                break;
            case APIServiceManager.MSTAR_ADD_NEW_ADDRESS:
            case APIServiceManager.MSTAR_GET_ADDRESS_BY_ID:
                addAddressFailedResponse();
                break;
        }
    }

    @Override
    public void onRetryClickListener() {
        onRetry(failedTransactionId);
        callBack.showWebserviceErrorView(false);
    }


    private void createCartResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (mstarBasicResponseTemplateModel != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(mstarBasicResponseTemplateModel.getStatus()) && isFromDiagPatientDetails()) {
                basePreference.setMstarDiagnosticCartId(mstarBasicResponseTemplateModel.getResult().getCart_id());
                setDiagBillingAndShippingAddress();
            }
        }
    }


    private MStarAddAddressModelRequest getAddAddressRequest(MStarAddressModel updatingAddress) {
        MStarAddAddressModelRequest addressModel = new MStarAddAddressModelRequest();
        addressModel.setFirstname(updatingAddress.getFirstname());
        addressModel.setLastname(updatingAddress.getLastname());
        addressModel.setStreet(updatingAddress.getStreet());
        addressModel.setLandmark(updatingAddress.getLandmark());
        String state = callBack.getCity();
        String[] separated = state.split(",");
        if (separated.length > 0)
            addressModel.setCity(separated[0]);
        if (separated.length > 1)
            addressModel.setState(separated[1]);
        addressModel.setPin(updatingAddress.getPin());
        addressModel.setMobileNo(updatingAddress.getMobileNo());
        return addressModel;
    }


    private MstarEditAddressRequest getEditAddressRequest() {
        MstarEditAddressRequest addressModel = callBack.getMstarEditAddressRequest();
        String state = callBack.getCity();
        String[] separated = state.split(",");
        if (separated.length > 0)
            addressModel.setCity(separated[0]);
        if (separated.length > 1)
            addressModel.setState(separated[1]);
        if (getAddress().getId() > -1) {
            addressModel.setId(getAddress().getId());
        }
        return addressModel;
    }

    private void updateAddressResponse(String data) {
        callBack.vmDismissProgress();
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus())) {
            callBack.updateSuccessful();
        } else if (addressData != null && addressData.getReason() != null && addressData.getReason().getAdditional_info() != null) {
            callBack.isAddressButtonEnabled(true);
            showErrorMessage(addressData.getReason().getAdditional_info());
        } else if (addressData != null && addressData.getReason() != null && !TextUtils.isEmpty(addressData.getReason().getReason_code()) && AppConstant.API_FAILURE_REASON_CODE.equalsIgnoreCase(addressData.getReason().getReason_code())) {
            callBack.isAddressButtonEnabled(true);
            callBack.showSnackBar(addressData.getReason().getReason_eng());
        } else {
            updateAddressFailedResponse();
        }
    }

    private void showErrorMessage(Map<String, String> additional_info) {
        String errorMessage = "";
        for (Map.Entry<String, String> additionInfoDetails : additional_info.entrySet()) {
            errorMessage = additionInfoDetails.getKey() + ": " + additionInfoDetails.getValue();
        }
        if (!TextUtils.isEmpty(errorMessage)) {
            callBack.showSnackBar(errorMessage);
        }

    }

    private void addAddressResponse(String data) {
        callBack.vmDismissProgress();
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus()) && addressData.getResult() != null) {
            updatingAddress.setId(addressData.getResult().getAddressId());
            billingAddressID = addressData.getResult().getAddressId();
            setShippingAndBillingAddress(addressData.getResult().getAddressId());
        } else if (addressData != null && addressData.getReason() != null && addressData.getReason().getAdditional_info() != null) {
            callBack.isAddressButtonEnabled(true);
            showErrorMessage(addressData.getReason().getAdditional_info());
        } else if (addressData != null && addressData.getReason() != null && !TextUtils.isEmpty(addressData.getReason().getReason_code()) && AppConstant.API_FAILURE_REASON_CODE.equalsIgnoreCase(addressData.getReason().getReason_code())) {
            callBack.isAddressButtonEnabled(true);
            callBack.showSnackBar(addressData.getReason().getReason_eng());
        } else {
            addAddressFailedResponse();
        }
    }

    private void settingBillingAndShippingAddressResponse(String data) {
        MStarBasicResponseTemplateModel addressData = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (isSetBillingAddressAPICallCompleted) {
            isSetBillingAddressCompleted = addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus());
        }
        if (isSetShippingAddressAPICallCompleted) {
            isSetShippingAddressCompleted = addressData != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(addressData.getStatus());
        }
        if (isSetShippingAddressAPICallCompleted && isSetBillingAddressAPICallCompleted) {
            callBack.isAddressButtonEnabled(true);
            callBack.vmDismissProgress();
            if (!TextUtils.isEmpty(PaymentHelper.getSingle_address()))
                callBack.settingBillingAndShippingAddressResponse(isSetShippingAddressCompleted, isSetBillingAddressCompleted);
            else
                initiateAPICall(APIServiceManager.MSTAR_GET_ADDRESS_BY_ID);
        }
    }

    private void singleAddressResponse(String data) {
        MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
        if (response.getResult() != null && response.getResult().getAddressModel() != null) {
            PaymentHelper.setSingle_address(new Gson().toJson(response.getResult().getAddressModel()));
            PaymentHelper.setCustomerBillingAddress(response.getResult().getAddressModel());
            callBack.navigateToConfirmation(getAddress());
        }
    }

    private void setShippingAndBillingAddress(int addressId) {
        billingAndShippingAddressId = addressId;
        if(isFromDiagPatientDetails()){
            setDiagBillingAndShippingAddress();
            return;
        }
        initiateAPICall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
        initiateAPICall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
        initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
        initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
    }

    private void setDiagBillingAndShippingAddress(){
        if(basePreference.getMstarDiagnosticCartId() > 0){
            initiateAPICall(APIServiceManager.MSTAR_SET_M2_BILLING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_M2_SHIPPING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_BILLING_ADDRESS);
            initiateAPICall(APIServiceManager.MSTAR_SET_PREFERRED_SHIPPING_ADDRESS);
        }else{
            initiateAPICall(APIServiceManager.MSTAR_CREATE_CART);
        }
    }

    public boolean isFromDiagPatientDetails() {
        return fromDiagnosticPatientDetails;
    }

    public void setFromDiagPatientDetails(boolean fromDiagnosticPatientDetails) {
        this.fromDiagnosticPatientDetails = fromDiagnosticPatientDetails;
    }


    private void addAddressFailedResponse() {
        callBack.isAddressButtonEnabled(true);
        callBack.vmDismissProgress();
        //callBack.showErrorMessage("New Address Add  Failed. Please try after some times");
    }

    private void updateAddressFailedResponse() {
        callBack.isAddressButtonEnabled(true);
        callBack.vmDismissProgress();
        //callBack.showErrorMessage("Update Failed. Please try after some times");
    }

    private void onRetry(int failedTransactionId) {
        switch (failedTransactionId) {
            case DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK:
                pinCodeServiceCheck(callBack.getPincode());
                break;
        }
    }

    private void setUserDetails() {
        customerDetails = new Gson().fromJson(basePreference.getCustomerDetails(), MStarCustomerDetails.class);
    }


    private void initiateNewAddressViews() {
        if (customerDetails != null) {
            callBack.initiateNewAddressViews(customerDetails);
        }
    }


    private void initiateEditAddressViews() {
        if (updatingAddress != null) {
            callBack.initiateEditAddressViews(updatingAddress);
        }
    }

    private void showApiError(int transactionId) {
        failedTransactionId = transactionId;
        callBack.showWebserviceErrorView(true);
    }

    private void setErrorWatcher() {
        callBack.setErrorWatcher();
        callBack.applyCityStateFromPinCode();
    }

    private String getCityStateUrl() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getStateCityUrl())) {
            return configurationResponse.getResult().getConfigDetails().getStateCityUrl();
        }
        return "";
    }

    private void getCityStateFromPinCodeResponse(String response) {
        String cityState = "";
        if (!TextUtils.isEmpty(response)) {
            GetCityStateFromPinCodeResponse cityStateFromPinCodeResponse = new Gson().fromJson(response, GetCityStateFromPinCodeResponse.class);
            if (cityStateFromPinCodeResponse != null && !TextUtils.isEmpty(cityStateFromPinCodeResponse.getStatus()) && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(cityStateFromPinCodeResponse.getStatus())) {
                if (cityStateFromPinCodeResponse.getResult() != null) {
                    if (updatingAddress != null && !TextUtils.isEmpty(updatingAddress.getPin()) && updatingAddress.getPin().equals(getPincode()) && !TextUtils.isEmpty(updatingAddress.getCity()) && !TextUtils.isEmpty(updatingAddress.getState())) {
                        setCity(updatingAddress.getCity());
                        setState(updatingAddress.getState());
                    } else {
                        setCity(!TextUtils.isEmpty(cityStateFromPinCodeResponse.getResult().getDistrict()) ? cityStateFromPinCodeResponse.getResult().getDistrict().toLowerCase() : "");
                        setState(!TextUtils.isEmpty(cityStateFromPinCodeResponse.getResult().getState_name()) ? cityStateFromPinCodeResponse.getResult().getState_name().toLowerCase() : "");
                    }
                    cityState = cityState + getCity() + ", ";
                    cityState = cityState + getState();
                    isValidPinCode = true;
                }
            } else if (cityStateFromPinCodeResponse != null && !TextUtils.isEmpty(cityStateFromPinCodeResponse.getReason())) {
                callBack.setPincodeError(cityStateFromPinCodeResponse.getReason());
            }
        }
        callBack.setCityState(CommonUtils.convertToTitleCase(cityState));
    }

    public void addressContinue() {
        /*FireBase Analytics Event*/
        FireBaseAnalyticsHelper.getInstance().CheckoutAddAddressEvent(callBack.getContext(), AppConstant.NEW_ADDRESS_ADDED);

        if (validateAddressForm()) {
            callBack.isAddressButtonEnabled(false);
            if (isForAddAddress) {
                callBack.vmShowProgress();
                updatingAddress = getEnteredAddress();
                initiateAPICall(APIServiceManager.MSTAR_ADD_NEW_ADDRESS);
            } else {
                callBack.vmShowProgress();
                initiateAPICall(APIServiceManager.MSTAR_UPDATE_ADDRESS);
                //callBack.navigateToConfirmation(getEnteredAddress());
            }
        }
    }

    private MStarAddressModel getEnteredAddress() {
        MStarAddressModel addressModel = callBack.getEnteredAddress();
        addressModel.setCity(getCity());
        addressModel.setState(getState());
        return addressModel;
    }

    private boolean validateAddressForm() {

        if (TextUtils.isEmpty(callBack.getPincode())) {
            callBack.setPincodeError(getApplication().getResources().getString(R.string.text_enter_pincode));
            return false;
        } else if (TextUtils.isEmpty(callBack.getPincode()) || callBack.getPincode().length() != 6) {
            callBack.setPincodeError(getApplication().getResources().getString(R.string.text_enter_valid_pincode));
            return false;
        } else if (!callBack.validateFirstName()) {
            return false;
        } else if (!callBack.validateLastName()) {
            return false;
        } else if (TextUtils.isEmpty(callBack.getHouseNumber())) {
            callBack.setHouseNumberLayoutError(getApplication().getResources().getString(R.string.text_error_house_number));
            return false;
        } else if (callBack.getHouseNumber().length() < 5) {
            callBack.setHouseNumberLayoutError(getApplication().getResources().getString(R.string.text_minimum_char));
            return false;
        } else if (callBack.getLandMark().length() < 5) {
            callBack.setLandMarkLayoutError(getApplication().getResources().getString(R.string.text_minimum_char));
            return false;
        } else if (TextUtils.isEmpty(callBack.getCity())) {
            callBack.setcityLayoutError(getApplication().getResources().getString(R.string.text_error_city));
            return false;
        } else if (TextUtils.isEmpty(callBack.getMobileNumber())) {
            callBack.setmobileNumberLayoutError(getApplication().getResources().getString(R.string.text_error_mobile_number));
            return false;
        } else if (callBack.getMobileNumber().length() != 10) {
            callBack.setmobileNumberLayoutError(getApplication().getResources().getString(R.string.text_invalid_mobile_number));
            return false;
        } else if (!isValidPinCode) {
            callBack.setPincodeError(getApplication().getResources().getString(R.string.text_enter_valid_pincode));
            return false;
        } else {
            return true;
        }
    }

    private String getCity() {
        return city;
    }

    private void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    private String getPincode() {
        return pincode;
    }

    private void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public void pinCodeServiceCheck(String code) {
        boolean isConnected = NetworkUtils.isConnected(getApplication());
        if (isConnected) {
            pinCodeServiceCheckRequest = new PinCodeServiceCheckRequest();
            pinCodeServiceCheckRequest.setPinCode(code);
            pinCodeServiceCheckRequest.setLabId(getDiagnosticLabId());
            callBack.vmShowProgress();
            initiateAPICall(DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK);
        } else
            failedTransactionId = DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK;
    }

    private void getPinCodeResponse(String data) {
        callBack.vmDismissProgress();
        PinCodeResponse pinCodeResponse = new Gson().fromJson(data, PinCodeResponse.class);
        pinCodeDataAvailable(pinCodeResponse);
    }

    private void pinCodeDataAvailable(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse != null) {
            checkPinCodeServiceStatus(pinCodeResponse);
        } else {
            callBack.continueButtonVisibility(View.GONE);
            showApiError(DiagnosticServiceManager.PIN_CODE_SERVICE_CHECK);
        }
    }

    private void checkPinCodeServiceStatus(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getServiceStatus() != null && pinCodeResponse.getServiceStatus().getStatusCode() != null && pinCodeResponse.getServiceStatus().getStatusCode() == 200) {
            getPinCodeResponseResult(pinCodeResponse);
        } else {
            callBack.continueButtonVisibility(View.GONE);
            setPinCodeServiceErrorMessage(pinCodeResponse.getResult());
        }
    }

    private void setPinCodeServiceErrorMessage(PinCodeResult result) {
        if (result != null && !TextUtils.isEmpty(result.getMessage()))
            callBack.showSnackBar(result.getMessage());
        else
            callBack.showSnackBar(getApplication().getResources().getString(R.string.text_webservice_error_title));
    }

    private void getPinCodeResponseResult(PinCodeResponse pinCodeResponse) {
        if (pinCodeResponse.getResult() != null) {
            getPinCodeService(pinCodeResponse.getResult());
        } else {
            callBack.continueButtonVisibility(View.GONE);
            callBack.showSnackBar(getApplication().getResources().getString(R.string.text_webservice_error_title));
        }
    }

    private void getPinCodeService(PinCodeResult pinCodeResult) {
        if (pinCodeResult.getService()) {
            String city = !TextUtils.isEmpty(pinCodeResult.getCity()) ? pinCodeResult.getCity() : "";
            String state = !TextUtils.isEmpty(pinCodeResult.getState()) ? pinCodeResult.getState() : "";
            String pinCode = !TextUtils.isEmpty(pinCodeResult.getPinCode()) ? pinCodeResult.getPinCode() : "";
            setCity(city);
            setState(state);
            isValidPinCode = true;
            callBack.setCityState(CommonUtils.convertToTitleCase(String.format("%s%s%s", getCity(), ",", getState())));
            callBack.continueButtonVisibility(View.VISIBLE);
            DiagnosticHelper.setCity(city);
            basePreference.setDiagnosticCity(city);
        } else {
            callBack.continueButtonVisibility(View.GONE);
            callBack.showSnackBar(getApplication().getResources().getString(R.string.text_diagnostic_pin_code_service));
        }
    }

    public void setAddress(MStarAddressModel address) {
        this.updatingAddress = address;
    }

    public MStarAddressModel getAddress() {
        return updatingAddress;
    }

    public int getBillingAndShippingAddressId() {
        return billingAndShippingAddressId;
    }

    public void callPinCodeService(String pincode) {
        setPincode(pincode);
        if (getIntentFromDiagnostic() != null && getIntentFromDiagnostic().equals(DiagnosticConstant.KEY_DIAGNOSTIC))
            pinCodeServiceCheck(pincode);
        else
            initiateAPICall(APIServiceManager.C_MSTAR_GET_CITY_STATE_FROM_PINCODE);
    }


    public interface ViewModelListener {

        void vmShowProgress();

        void vmDismissProgress();

        void navigateToConfirmation(MStarAddressModel addressModel);

        void updateSuccessful();

        void settingBillingAndShippingAddressResponse(boolean isShippingAddressSet, boolean isBillingAddressSet);

        void continueButtonVisibility(int view);

        String getPincode();

        void setPincodeError(String message);

        MstarEditAddressRequest getMstarEditAddressRequest();

        void initiateNewAddressViews(MStarCustomerDetails customerDetails);

        void initiateEditAddressViews(MStarAddressModel updatingAddress);

        void showWebserviceErrorView(boolean enable);

        MStarAddressModel getEnteredAddress();

        void setErrorWatcher();

        void showSnackBar(String message);

        void applyCityStateFromPinCode();

        void setCityState(String cityState);

        void setHouseNumberLayoutError(String message);

        void setLandMarkLayoutError(String message);

        void setcityLayoutError(String message);

        void setmobileNumberLayoutError(String message);

        String getHouseNumber();

        String getLandMark();

        String getCity();

        String getMobileNumber();

        boolean validateFirstName();

        boolean validateLastName();

        Context getContext();

        void isAddressButtonEnabled(boolean isEnabled);
    }
}