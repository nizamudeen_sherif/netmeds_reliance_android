package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.adpater.SortAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentSortBinding;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.SortOptionLabel;

import java.util.List;

public class SortViewModel extends AppViewModel implements SortAdapter.SortListListener {

    private SortListener listener;
    private List<SortOptionLabel> sortOptionList;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private FragmentSortBinding binding;
    private SortOptionLabel sortOption;

    public SortViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(SortListener listener, FragmentSortBinding binding, List<SortOptionLabel> sortOptionList, Context mContext) {
        this.listener = listener;
        this.binding = binding;
        this.sortOptionList = setInitialSort(sortOptionList);
        this.mContext = mContext;
        setAdapter();
    }

    private List<SortOptionLabel> setInitialSort(List<SortOptionLabel> sortOptionList) {
        if (MstarFilterHelper.getSortOptionLabel() != null && !TextUtils.isEmpty(MstarFilterHelper.getSortOptionLabel().getKey())) {
            for (int i = 0; i < sortOptionList.size(); i++) {
                SortOptionLabel label = sortOptionList.get(i);
                label.setSelected(MstarFilterHelper.getSortOptionLabel().getKey().equalsIgnoreCase(label.getKey()));
            }
        } else {
            for (int i = 0; i < sortOptionList.size(); i++) {
                SortOptionLabel label = sortOptionList.get(i);
                label.setSelected(i == 0);
            }
        }
        return sortOptionList;
    }

    private void setAdapter() {
        SortAdapter sortAdapter = new SortAdapter(sortOptionList, this, mContext);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.sortList.setLayoutManager(linearLayoutManager);
        binding.sortList.setAdapter(sortAdapter);
    }

    public void closeView() {
        listener.dismissView();
    }

    public void clearSelection() {
        MstarFilterHelper.clearSortSelection();
        listener.dismissView();
        listener.clearSortSelection();
    }

    public void applySort() {
        if (getSortOption() != null && !TextUtils.isEmpty(getSortOption().getKey())) {
            MstarFilterHelper.setSortOptionLabel(getSortOption());
            listener.applySort(getSortOption());
        }
    }

    @Override
    public void setSortSelection(SortOptionLabel sortOption) {
        setSortOption(sortOption);
    }

    private SortOptionLabel getSortOption() {
        return sortOption;
    }

    private void setSortOption(SortOptionLabel sortOption) {
        this.sortOption = sortOption;
    }

    public interface SortListener {
        void dismissView();

        void applySort(SortOptionLabel sortOption);

        void clearSortSelection();
    }
}
