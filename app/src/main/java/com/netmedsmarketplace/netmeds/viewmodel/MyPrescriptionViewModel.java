package com.netmedsmarketplace.netmeds.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MyPrescriptionAdapter;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarMyPrescription;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.List;

public class MyPrescriptionViewModel extends AppViewModel {

    private MyPrescriptionListener listener;
    private MyPrescriptionAdapter adapter;
    private BasePreference basePreference;

    private final MutableLiveData<MStarBasicResponseTemplateModel> responseMutableLiveData = new MutableLiveData<>();

    private final int pageSize = 10;
    private int pageIndex = 1;
    private long totalPrescriptionCount = 0;
    private long totalPageCount = 0;

    public MyPrescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void initViewModel(BasePreference basePreference, MyPrescriptionListener listener) {
        this.basePreference = basePreference;
        this.listener = listener;
        initiatePaginationProcess();
        getPastPrescriptionDetails();
    }

    private void initiatePaginationProcess() {
        adapter = null;
        totalPrescriptionCount = 0;
        totalPageCount = 0;
        pageIndex = 1;
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        getPastPrescriptionDetails();
    }

    private void getPastPrescriptionDetails() {
        listener.showNoNetworkError(!listener.isNetworkConnected());
        if (listener.isNetworkConnected()) {
            listener.vmShowProgress();
            APIServiceManager.getInstance().getMstarMyPrescriptions(this, basePreference.getMstarBasicHeaderMap(), pageIndex, pageSize);
        }
    }


    public void onDetailsAvailable(MStarBasicResponseTemplateModel myPrescriptionRespose) {
        if (myPrescriptionRespose != null && myPrescriptionRespose.getResult() != null && myPrescriptionRespose.getResult().getRxList().size() > 0) {
            calculateTotalPageCount(myPrescriptionRespose.getResult());
            if (adapter == null) {
                adapter = listener.setPrescriptionAdapter(myPrescriptionRespose.getResult().getRxList());
            } else {
                adapter.updatePrescriptionData(myPrescriptionRespose.getResult().getRxList());
            }
        } else {
            listener.finishActivity();
        }
    }

    private void calculateTotalPageCount(MstarBasicResponseResultTemplateModel result) {
        totalPrescriptionCount = result.getRxCount();
        totalPageCount = (long) Math.ceil(Double.valueOf(String.valueOf(totalPrescriptionCount)) / Double.valueOf(String.valueOf(pageSize)));
    }

    public RecyclerView.OnScrollListener paginationForOrderList(final LinearLayoutManager layoutManager) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (pageIndex < totalPageCount) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= pageSize) {
                        pageIndex += 1;
                        getPastPrescriptionDetails();
                    }
                }
            }
        };
    }

    public MutableLiveData<MStarBasicResponseTemplateModel> getResponseMutableLiveData() {
        return responseMutableLiveData;
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        listener.vmDismissProgress();
        if (data != null && !TextUtils.isEmpty(data)) {
            if (transactionId == APIServiceManager.C_MSTAR_MY_PRESCRIPTION) {
                MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
                if (responseTemplateModel != null && responseTemplateModel.getResult() != null && responseTemplateModel.getResult().getRxList() != null && !responseTemplateModel.getResult().getRxList().isEmpty()) {
                    responseMutableLiveData.setValue(responseTemplateModel);
                } else {
                    listener.finishActivity();
                }
            }
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        listener.vmDismissProgress();
        listener.showWebViewError(true);
    }


    public interface MyPrescriptionListener {

        void vmShowProgress();

        void vmDismissProgress();

        void onRetry();

        void finishActivity();

        void viewDownloadedImage(String imagePath);

        void showMessage(String message);

        MyPrescriptionAdapter setPrescriptionAdapter(List<MstarMyPrescription> rxList);

        void showNoNetworkError(boolean isNetworkError);

        void showWebViewError(boolean isError);

        boolean isNetworkConnected();
    }

    @SuppressLint("StaticFieldLeak")
    public static class DownloadPrescriptionTask extends AsyncTask<Void, Void, String> {

        private Context mContext;
        private MyPrescriptionListener listener;

        private String Id;
        private String url;
        private String imagePath;

        private boolean isSuccess = true;

        public DownloadPrescriptionTask(String Id, String url, MyPrescriptionListener listener, Context context) {
            this.Id = Id;
            this.url = url;
            this.listener = listener;
            this.mContext = context;
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                DownloadManager downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(String.valueOf(url)));
                request.addRequestHeader(AppConstant.MSTAR_API_HEADER_USER_ID, BasePreference.getInstance(mContext).getMstarBasicHeaderMap().get(AppConstant.MSTAR_API_HEADER_USER_ID));
                request.addRequestHeader(AppConstant.MSTAR_AUTH_TOKEN, BasePreference.getInstance(mContext).getMstarBasicHeaderMap().get(AppConstant.MSTAR_AUTH_TOKEN));
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                request.setAllowedOverRoaming(false);
                request.setTitle(mContext.getResources().getString(R.string.app_name));
                request.setDescription(mContext.getResources().getString(R.string.text_download_file_name) + Id);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, mContext.getResources().getString(R.string.text_download_file_name) + Id + ".jpg");
                imagePath = mContext.getResources().getString(R.string.text_download_file_name) + Id + ".jpg";
                downloadManager.enqueue(request);
                listener.showMessage(mContext.getString(R.string.text_downloading));
            } catch (Exception e) {
                isSuccess = false;
                listener.vmDismissProgress();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            listener.vmShowProgress();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.vmDismissProgress();
            if (isSuccess) {
                listener.viewDownloadedImage(imagePath);
            } else {
                listener.showMessage(mContext.getResources().getString(R.string.text_download_failed));
            }
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
            listener.vmDismissProgress();
            listener.showMessage(mContext.getResources().getString(R.string.text_download_cancelled));
        }
    }
}
