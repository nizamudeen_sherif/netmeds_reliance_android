package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.NetmedsApp;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DigitizedRxList;
import com.nms.netmeds.base.model.DigitizedRxResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarPrescriptionDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.io.Closeable;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class M1AttachPrescriptionViewModel extends AppViewModel implements CommonUtils.BitmapImageConversionInterface {

    private int failedTransactionId = 0;
    private int prescriptionLimit;
    private boolean fullyDigitizedOrder = false;

    private ArrayList<MStarUploadPrescription> mStarPrescriptionList = new ArrayList<>();
    private ArrayList<String> rxIdsList = new ArrayList<>();
    private ArrayList<String> rxDrugCodes = new ArrayList<>();
    private ArrayList<String> rxDrugName = new ArrayList<>();
    private Map<String, String> detachPrescriptionMap = new HashMap<>();
    private ArrayList<MStarUploadPrescription> duplicateMstarPrescriptionList = new ArrayList<>();

    private M1AttachPrescriptionListener mListener;
    private BasePreference mBasePreference;
    private AsyncTask<Void, Void, String> asyncThread;
    public boolean isFromOrderReviewButtton;


    public M1AttachPrescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void initViewModel(BasePreference basePreference, M1AttachPrescriptionListener listener, ArrayList<String> rxDrugCodes, ArrayList<String> rxDrugName) {
        this.mListener = listener;
        this.mBasePreference = basePreference;
        this.rxDrugCodes = rxDrugCodes;
        this.rxDrugName = rxDrugName;
        ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(NetmedsApp.getInstance()).getConfiguration(), ConfigurationResponse.class);
        prescriptionLimit = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null ? response.getResult().getConfigDetails().getUploadPrescriptionLimit() : 0;
        PaymentHelper.setIsExternalDoctor(false);

        getUploadedPrescriptionsFromHelper();

        getDigitizedPrescription(rxDrugCodes);
    }

    private void getUploadedPrescriptionsFromHelper() {
        if (!MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().isEmpty()) {
            mStarPrescriptionList.clear();
            for (MStarUploadPrescription prescriptionDetail : MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList()) {
                if (!prescriptionDetail.isMethod2()) {
                    prescriptionDetail.setOrderReview(false);
                    mStarPrescriptionList.add(prescriptionDetail);
                }
            }
            setAdapter();
        }
    }

    private void getDigitizedPrescription(ArrayList<String> rxDrugCodes) {
        if (rxDrugCodes != null && rxDrugCodes.size() > 0) {
            mListener.enableSelectAddress(true);
            initiateAPICalls(APIServiceManager.C_MSTAR_DIGITIZED_RX);
        }
    }

    private void initiateAPICalls(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        mListener.showNoNetworkView(isConnected);
        if (isConnected) {
            mListener.vmShowProgress();
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_DIGITIZED_RX:
                    if (rxDrugCodes != null && !rxDrugCodes.isEmpty()) {
                        APIServiceManager.getInstance().mStarGetDigitizedRx(this, mBasePreference.getMstarBasicHeaderMap(), getDigitizedMultipartRequest(rxDrugCodes));
                    }
                    break;
                case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
                    APIServiceManager.getInstance().mStarGetPastPrescription(this, mBasePreference.getMstarBasicHeaderMap(), getPastPrescriptionMultiPartRequest());
                    break;
                case APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION:
                    MultipartBody multipartBody = getMultiPartRequestBody();
                    if (multipartBody != null) {
                        APIServiceManager.getInstance().mstarUploadPrescription(this, mBasePreference.getMstarBasicHeaderMap(), SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : 0, "", multipartBody);
                    } else {
                        navigateToOrderReviewPage();
                        mListener.vmDismissProgress();
                    }
                    break;
                case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                    APIServiceManager.getInstance().mStartDetachPrescription(this, mBasePreference.getMstarBasicHeaderMap(), detachPrescriptionMap, SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().getSubscriptionCartId() : PaymentHelper.isIsTempCart() ? PaymentHelper.getTempCartId() : null);
                    break;
            }
        } else {
            failedTransactionId = transactionId;
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_DIGITIZED_RX:
                digitizedRxResponse(data);
                break;
            case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
                pastPrescriptionResponse(data);
                break;
            case APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION:
                SubscriptionHelper.getInstance().setUploadPrescription(true);
                uploadPrescriptionResponse(data);
                break;
            case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                detachPrescriptionResponse(data);
                break;
        }

    }

    @Override
    public void onFailed(int transactionId, String data) {
        mListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
            case APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION:
                SubscriptionHelper.getInstance().setUploadPrescription(false);
                mListener.showWebserviceErrorView(true);
                break;

            case APIServiceManager.C_MSTAR_DIGITIZED_RX:
                removeDigitizedPrescription();
                fullyDigitizedOrder = false;
                break;
            case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                detachPrescriptionMap.clear();
                break;
        }
    }

    private MultipartBody getDigitizedMultipartRequest(ArrayList<String> rxDrugCodesList) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart(AppConstant.PAGE_INDEX, AppConstant.PAGE_INDEX_RX);
        builder.addFormDataPart(AppConstant.PAGESIZE, AppConstant.PAGE_SIZE_RX); // pagesize is 1

        StringBuilder allDrugcodes = new StringBuilder();
        if (rxDrugCodesList.size() == 1) {
            allDrugcodes.append(rxDrugCodes.get(0));
        } else {
            for (String rxDrugCode : rxDrugCodesList) {
                allDrugcodes.append(rxDrugCode + ",");
            }
            allDrugcodes.deleteCharAt(allDrugcodes.length() - 1);
        }
        builder.addFormDataPart(AppConstant.DRUGCODE, allDrugcodes.toString());
        return builder.build();
    }

    private MultipartBody getPastPrescriptionMultiPartRequest() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart(AppConstant.PAGE_INDEX, AppConstant.PAGE_INDEX_RX);
        builder.addFormDataPart(AppConstant.PAGESIZE, AppConstant.PAGE_SIZE_RX); // pagesize is 12
        return builder.build();
    }

    private void digitizedRxResponse(String data) {
        DigitizedRxResponse digitizedPrescriptionResponse = new Gson().fromJson(data, DigitizedRxResponse.class);
        if (AppConstant.API_FAILURE_STATUS.equalsIgnoreCase(digitizedPrescriptionResponse.getStatus())) {
            removeDigitizedPrescription();
            fullyDigitizedOrder = false;
        } else if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(digitizedPrescriptionResponse.getStatus()) && digitizedPrescriptionResponse.getReason() == null) {
            digitizedPrescriptionList(digitizedPrescriptionResponse);

            //RX Unmatched list of items

            if (!TextUtils.isEmpty(digitizedPrescriptionResponse.getResult().getRxUnmatchDrugList())) {
                ArrayList<String> items = new ArrayList<>(Arrays.asList(digitizedPrescriptionResponse.getResult().getRxUnmatchDrugList().split(",")));
                mListener.setProductNameListView(items);
                mListener.enableSelectAddress(true);
                fullyDigitizedOrder = false;
            } else {
                //For  fully digitalized view
                mListener.enableSelectAddress(true);
                mListener.selectAddressButton(true);
                fullyDigitizedOrder = true;
                mListener.setfullDigitalizedView();
            }
        } else {
            mListener.setProductNameListView(rxDrugName);
            mListener.enableSelectAddress(true);
            fullyDigitizedOrder = false;
        }
    }

    private void removeDigitizedPrescription() {
        mListener.enableSelectAddress(true);
        setAdapter();
    }

    public void getDuplicatePrescriptionList() {
        // Maintaining for detach prescription

        if (!mStarPrescriptionList.isEmpty()) {
            duplicateMstarPrescriptionList.clear();
            duplicateMstarPrescriptionList.addAll(mStarPrescriptionList);
        }
    }

    private void digitizedPrescriptionList(DigitizedRxResponse response) {
        if (response != null && response.getResult() != null && response.getResult().getRxDigitizedList() != null) {
            List<DigitizedRxList> digitizedPrescriptionLists = response.getResult().getRxDigitizedList();
            for (DigitizedRxList digitizedRxList : digitizedPrescriptionLists) {
                rxIdsList.add(digitizedRxList.getRxId());
            }
            if (rxIdsList != null && !rxIdsList.isEmpty()) {
                if (mStarPrescriptionList != null && !mStarPrescriptionList.isEmpty()) {
                    if (mStarPrescriptionList.size() < prescriptionLimit) {
                        ArrayList<String> idList = new ArrayList<>();
                        for (MStarUploadPrescription prescriptionDetail : mStarPrescriptionList) {
                            idList.add(prescriptionDetail.getUploadedPrescriptionId());
                        }

                        for (String rxId : rxIdsList) {
                            if (!idList.contains(rxId)) {
                                MStarUploadPrescription prescription = new MStarUploadPrescription();
                                prescription.setUploadedPrescriptionId(rxId);
                                prescription.setPastPrescriptionId(rxId);
                                prescription.setDigitalizedPrescriptionId(rxId);
                                mStarPrescriptionList.add(prescription);
                            }
                        }
                    }
                } else {
                    for (String rxID : rxIdsList) {
                        MStarUploadPrescription prescription = new MStarUploadPrescription();
                        prescription.setUploadedPrescriptionId(rxID);
                        prescription.setPastPrescriptionId(rxID);
                        prescription.setDigitalizedPrescriptionId(rxID);
                        mStarPrescriptionList.add(prescription);
                    }
                }
            }
        }
        //setBitmapImagesFromAPI();
        /** Download the digitalized RX and save as Bitmap for showing purpose in Order Review Screen.
         * Set bitmap Image from Download Prescription API if the helper class is empty.
         * Download Prescription API is in the loop
         */
        asyncThread = new CommonUtils.BitmapImageAsync(this, mStarPrescriptionList, false).execute();
    }

    @Override
    public void showProgressBar() {
        mListener.vmShowProgress();
    }

    @Override
    public void hideProgressBar() {
        mListener.vmDismissProgress();
    }

    @Override
    public Context getContext() {
        return mListener.getContext();
    }

    @Override
    public void setBitmapImageAdapter() {
        setAdapter();
    }


    private void uploadPrescriptionResponse(String data) {

        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);

            //Successfully attached prescription
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null
                    && response.getResult().getAttachment_result() != null && !response.getResult().getAttachment_result().isEmpty()) {
                Map<String, MstarPrescriptionDetails> prescriptionDetails = response.getResult().getAttachment_result();
                for (Map.Entry<String, MstarPrescriptionDetails> detailsEntry : prescriptionDetails.entrySet()) {
                    for (MStarUploadPrescription prescription : mStarPrescriptionList) {
                        if (prescription.getUploadKey() != null && prescription.getUploadKey().equalsIgnoreCase(detailsEntry.getKey())) {
                            prescription.setPrescriptionUploaded(AppConstant.PRESCRIPTION_ATTACHED.equalsIgnoreCase(detailsEntry.getValue().getStatus()) || AppConstant.PRESCRIPTION_UPLOADED.equalsIgnoreCase(detailsEntry.getValue().getStatus()));
                            prescription.setUploadedPrescriptionId(detailsEntry.getValue().getPrescription_id());
                            mStarPrescriptionList.set(mStarPrescriptionList.indexOf(prescription), prescription);
                            if (PaymentHelper.isIsEditOrder())
                                RefillHelper.getNewRxIdList().add(detailsEntry.getValue().getPrescription_id());
                        }
                    }
                }

                /*WebEngage Upload Prescription Event*/
                if (getUploadPrescriptionList().size() > 0) {
                    WebEngageHelper.getInstance().prescriptionUploadedEvent(mListener.getContext(), getUploadPrescriptionList(), AppConstant.M1);
                }

                navigateToOrderReviewPage();
            }
        }
    }

    private void detachPrescriptionResponse(String data) {
        detachPrescriptionMap.clear();

        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);

            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null
                    && response.getResult().getDetachment_result() != null && !response.getResult().getDetachment_result().isEmpty()) {

                Map<String, MstarPrescriptionDetails> detachDetails = response.getResult().getDetachment_result();

                for (Map.Entry<String, MstarPrescriptionDetails> detachEntry : detachDetails.entrySet()) {
                }

            }
        }
    }

    @Override
    public void onRetryClickListener() {
        onRetry(failedTransactionId);
        mListener.showWebserviceErrorView(false);
    }

    private void onRetry(int failedTransactionId) {
        initiateAPICalls(failedTransactionId);
    }

    public void uploadPrescription() {
        if (!isMaximumPrescriptionUploaded())
            mListener.uploadPrescriptionView();
        /*Google Analytics Click Events*/
        GoogleAnalyticsHelper.getInstance().postEvent(mListener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OTHER_ACTIONS, GoogleAnalyticsHelper.EVENT_ACTION_UPLOAD_PRESCRIPTION, GoogleAnalyticsHelper.EVENT_LABEL_M1_ATTACH_PRESCRIPTION_PAGE);
    }

    public boolean isMaximumPrescriptionUploaded() {
        ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(getApplication().getApplicationContext()).getConfiguration(), ConfigurationResponse.class);
        int prescriptionLimit = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null ? response.getResult().getConfigDetails().getUploadPrescriptionLimit() : 0;
        if (mStarPrescriptionList.size() >= prescriptionLimit) {
            mListener.setError(getApplication().getResources().getString(R.string.text_maximum_prescription_uploaded));
            return true;
        } else {
            return false;
        }
    }

    public void setCapturedImage(int resultCode) {
        validateImage(mListener.getUriFromCameraImage(resultCode));
    }


    public void setGalleryImage(int resultCode, Intent data) {
        validateImage(mListener.getUriFromGalleryImage(resultCode, data));
    }

    public void setDownloadImage(Uri uri) {
        validateImage(uri);
    }

    private void validateImage(Uri uri) {
        if (uri != null) {
            String path = FileUtils.getPath(getApplication(), uri);
            if (mListener.validateUploadedImage(path, getApplication(), mListener.getViewGroup(), FileUtils.MAX_IMAGE_SIZE_IN_BYTES, getApplication().getResources().getString(com.nms.netmeds.base.R.string.text_max_file_size))) {
                Resources r = mListener.getContext().getResources();
                int px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, r.getDimension(R.dimen.density_size_64), r.getDisplayMetrics()));
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap1 = BitmapFactory.decodeFile(path, bmOptions);
                Bitmap bitmap = ImageUtils.getScaledBitmap(CommonUtils.rotateImage(mListener.getContext(), bitmap1, path));
                OutputStream outputStream = null;
                try {
                    outputStream = mListener.getContext().getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                    }
                } catch (Exception ignored) {

                } finally {
                    closeSilently(outputStream);
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                }
                saveSelectedPrescriptionInList(CommonUtils.decodeSampledBitmapFromResource(path, px, px), path);
            }
        }
        setAdapter();
    }

    private static void closeSilently(@Nullable Closeable c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        } catch (Throwable t) {
            // Do nothing
        }
    }

    private void saveSelectedPrescriptionInList(Bitmap bitmap, String path) {
        MStarUploadPrescription prescription = new MStarUploadPrescription();
        prescription.setBitmapFromGalleryOrCamera(true);
        prescription.setBitmapImage(bitmap);
        prescription.setFile(new File(path));
        mStarPrescriptionList.add(prescription);
    }

    private void savePrescriptionFromPastRx(Bitmap bitmap, String prescriptionID) {
        MStarUploadPrescription prescription = new MStarUploadPrescription();
        prescription.setBitmapImage(bitmap);
        prescription.setPastPrescriptionId(prescriptionID);
        prescription.setUploadedPrescriptionId(prescriptionID);
        mStarPrescriptionList.add(prescription);
    }


    private void setAdapter() {
        if (!mStarPrescriptionList.isEmpty()) {
            enablePrescriptionView(true);
            mListener.selectAddressButton(MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().size() > 0 || fullyDigitizedOrder);
            mListener.setUploadPrescriptionVisibility(View.GONE);
            mListener.setPrescriptionListAdapter(mStarPrescriptionList);
            mListener.enableSetAddressOption(true);
        } else {
            enablePrescriptionView(false);
            mListener.setUploadPrescriptionVisibility(View.VISIBLE);
            mListener.enableSetAddressOption(false);
        }
        saveUploadedPrescription();
    }

    private void saveUploadedPrescription() {
        //Before Uploading Prescripion List with bitmap images
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(mStarPrescriptionList);
    }

    private void enablePrescriptionView(boolean isEnable) {
        mListener.enablePrescriptionView(isEnable);
        mListener.enableSetAddressOption(isEnable);
    }

    public void resetPrescriptionAdapter(int position, ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        if (!prescriptionArrayList.isEmpty()) {
            int pos = 0;
            for (MStarUploadPrescription prescription : prescriptionArrayList) {
                pos = prescriptionArrayList.indexOf(prescription);
                if (pos == position) {
                    prescriptionArrayList.remove(position);
                    if (prescription.isPrescriptionUploaded()) {
                        detachPrescriptionMap.put(AppConstant.PID + getProperIndexValue(detachPrescriptionMap.size()), prescription.getUploadedPrescriptionId());
                    }
                    break;
                }
            }
            mListener.resetPrescriptionAdapter(prescriptionArrayList, position);
        }

    }

    public void getPastPrescription() {
        initiateAPICalls(APIServiceManager.C_MSTAR_PAST_PRESCRIPTION);
    }

    private void pastPrescriptionResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            GetPastPrescriptionResponse pastPrescriptionResponse = new Gson().fromJson(response, GetPastPrescriptionResponse.class);
            if (pastPrescriptionResponse != null && pastPrescriptionResponse.getStatus() != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(pastPrescriptionResponse.getStatus())) {
                showPastPrescription(pastPrescriptionResponse.getResult());
            }
        }
        mListener.vmDismissProgress();
    }

    private void showPastPrescription(GetPastPrescriptionResult result) {
        if (result != null && (result.getDigitizedPrescriptionCount() > 0 || result.getPastOneWeekUnDigitizedRxCount() > 0)) {
            mListener.pastPrescriptionView(result);
        } else {
            mListener.setError(getApplication().getResources().getString(R.string.text_past_prescriotion_error));
        }
    }

    public void setPastPrescription(List<MStarUploadPrescription> prescriptionList) {

        for (MStarUploadPrescription prescription : prescriptionList) {
            if (prescription.isChecked()) {
                savePrescriptionFromPastRx(prescription.getBitmapImage(), prescription.getUploadedPrescriptionId());
            }
        }
        setAdapter();
    }


    public void selectAddressAction() {
        /*Google Analytics Click Event*/
        //initUploadPrescription();

        if(!mListener.isSelectAddressButtonEnabled()){
            Toast.makeText(mListener.getContext(), mListener.getContext().getResources().getString(R.string.text_message_when_btn_disabled), Toast.LENGTH_SHORT).show();
        }else{
            /*FireBase Analytics Event*/
            FireBaseAnalyticsHelper.getInstance().CheckoutUploadEvent(mListener.getContext(), AppConstant.CHECKOUT_UPLOAD);

            /*Google Tag Manager + FireBase Checkout Progress Step2 Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseCheckoutProgressEvent(mListener.getContext(),PaymentHelper.getCartLineItems(),PaymentHelper.getAnalyticalTrackingProductList(),FireBaseAnalyticsHelper.CHECKOUT_STEP_2,FireBaseAnalyticsHelper.EVENT_PARAM_ATTACH_PRESCRIPTION);
            isFromOrderReviewButtton = true;
            updatePrescription();

            GoogleAnalyticsHelper.getInstance().postEvent(mListener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_PROCEED_ATTACH_PRESCRIPTION, GoogleAnalyticsHelper.EVENT_LABEL_M1_ATTACH_PRESCRIPTION_PAGE);
            PaymentHelper.setIsExternalDoctor(mListener.isDontHavePrescriptionChecked());
        }
    }

    private void navigateToOrderReviewPage() {
        if(isFromOrderReviewButtton){
            mListener.launchAddressView(false);
        }
    }

    public String selectButton() {
        return getContext().getString(SubscriptionHelper.getInstance().isSubscriptionFlag() ? SubscriptionHelper.getInstance().isSubscriptionEdit() ? R.string.text_order_review : R.string.text_schedule_delivery : R.string.text_order_review);
    }

    public void updatePrescription() {
        if (!detachPrescriptionMap.isEmpty()) {
            initiateAPICalls(APIServiceManager.MSTAR_DETACH_PRESCRIPTION);
        }

        initiateAPICalls(APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION);
    }

    private MultipartBody getMultiPartRequestBody() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        boolean isNewPrescriptionAvailableForUpload = false;
        if (!mStarPrescriptionList.isEmpty()) {
            int position = 0;
            String index = "";

            for (MStarUploadPrescription prescription : mStarPrescriptionList) {

                position = mStarPrescriptionList.indexOf(prescription);

                index = getProperIndexValue(position);
                if (!prescription.isPrescriptionUploaded()) {
                    if (!isNewPrescriptionAvailableForUpload) {
                        isNewPrescriptionAvailableForUpload = true;
                    }
                    if (prescription.isBitmapFromGalleryOrCamera()) {
                        prescription.setUploadKey(AppConstant.PIMG + index);
                        builder.addFormDataPart(prescription.getUploadKey(), prescription.getFile().getName(), RequestBody.create(MediaType.parse(FileUtils.MULTI_PART_TYPE), prescription.getFile()));
                    } else {
                        prescription.setUploadKey(AppConstant.PID + index);
                        builder.addFormDataPart(prescription.getUploadKey(), prescription.getPastPrescriptionId());
                    }
                }
                mStarPrescriptionList.set(position, prescription);
            }
        }
        if (isNewPrescriptionAvailableForUpload) {
            return builder.build();
        } else {
            return null;
        }
    }

    private String getProperIndexValue(int position) {
        if (position < 9 && position != 0) {
            return "0" + (position + 1);
        } else if (position == 9) {
            return "10";
        } else if (position == 0) {
            return "01";
        } else {
            return String.valueOf(position + 1);
        }
    }

    public void validPrescription() {
        mListener.launchValidPrescriptionView();
    }

    private List<String> getUploadPrescriptionList() {
        List<String> uploadPrescriptionList = new ArrayList<>();
        for (MStarUploadPrescription mStarUploadPrescription : mStarPrescriptionList) {
            uploadPrescriptionList.add(mStarUploadPrescription.getUploadedPrescriptionId());
        }
        return uploadPrescriptionList;
    }

    public void setDontHavePrescription() {
        /*Google Analytics Click Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(mListener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OTHER_ACTIONS, GoogleAnalyticsHelper.EVENT_ACTION_EXTERNAL_DOCTOR_CONSULT, GoogleAnalyticsHelper.EVENT_LABEL_M1_ATTACH_PRESCRIPTION_PAGE);


        if (mListener.isDontHavePrescriptionChecked()) {
            mListener.setDisableDontHavePrescription(false);
            if (mStarPrescriptionList.size() > 0) {
                mListener.enableSetAddressOption(true);
            } else {
                mListener.enableSetAddressOption(false);
            }
        } else {
            mListener.setDisableDontHavePrescription(true);
            mListener.enableSetAddressOption(true);
            mListener.launchFreeConsultationView();
        }
    }

    public interface M1AttachPrescriptionListener {

        void uploadPrescriptionView();

        void initCameraIntent();

        void setPrescriptionListAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList);

        void pastPrescriptionView(GetPastPrescriptionResult result);

        void vmShowProgress();

        void vmDismissProgress();

        void launchAddressView(boolean finishActivity);

        void launchValidPrescriptionView();

        void launchFreeConsultationView();

        void dismissSnackBar();

        void setError(String error);

        Uri getUriFromCameraImage(int resultCode);

        Uri getUriFromGalleryImage(int resultCode, Intent data);

        boolean validateUploadedImage(String path, Application application, ViewGroup mainLayout, Long uploadSizeLimit, String maxFileSizeError);

        void setProductNameListView(ArrayList<String> productList);

        void enableSelectAddress(boolean isEnableShimmer);

        void setfullDigitalizedView();

        void enablePrescriptionView(boolean isEnable);

        ViewGroup getViewGroup();

        Context getContext();

        void selectAddressButton(boolean isEnable);

        void setUploadPrescriptionVisibility(int gone);

        void showWebserviceErrorView(boolean enable);

        void showNoNetworkView(boolean isNoNetWork);

        boolean isDontHavePrescriptionChecked();

        void setDisableDontHavePrescription(boolean check);

        void enableSetAddressOption(boolean enable);

        void resetPrescriptionAdapter(ArrayList<MStarUploadPrescription> mStarUploadPrescriptionsList, int position);

        boolean isSelectAddressButtonEnabled();
    }
}