package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nms.netmeds.base.AppViewModel;

public class GenericMedicinesHomePageViewModel extends AppViewModel {

    private GenericListener listener;

    public GenericMedicinesHomePageViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(GenericListener listener) {
        this.listener = listener;
    }

    public void navigateToSearchActivity() {
        listener.navigateToSearch(true);
    }

    public void openSlideFragment() {
        listener.openSlideFragment();
    }

    public void openGenericSearch() {
        listener.navigateToSearch(true);
    }

    public void openNormalSearch() {
        listener.navigateToSearch(false);
    }

    public interface GenericListener {

        boolean isNetworkConnected();

        void showLoader();

        void dismissLoader();

        void navigateToSearch(boolean isGeneric);

        void openSlideFragment();
    }
}
