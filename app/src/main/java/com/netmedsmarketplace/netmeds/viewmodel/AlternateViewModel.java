package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.widget.CustomNumberPicker;
import com.nms.netmeds.base.widget.CustomNumberPickerListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AlternateViewModel extends AppViewModel implements CustomNumberPickerListener {

    private AlternateInterface mListener;
    private int outOfStockProductCode;
    private MStarProductDetails outOfStockProduct;
    private MStarProductDetails alternateProduct;
    private int quantity = 1;

    public AlternateViewModel(@NonNull Application application) {
        super(application);
    }

    public void closeView() {
        mListener.closeView();
    }

    public void init(AlternateInterface listener, int outOfStockProduct) {
        this.mListener = listener;
        this.outOfStockProductCode = outOfStockProduct;
        initMstarApi(APIServiceManager.MSTAR_PRODUCT_DETAILS);
    }


    private void initMstarApi(int transactionID) {
        boolean isConnected = mListener.isNetworkConnected();
        mListener.showLoader();
        if (isConnected) {
            switch (transactionID) {
                case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                    APIServiceManager.getInstance().mStarProductDetails(this, outOfStockProductCode, APIServiceManager.MSTAR_PRODUCT_DETAILS);
                    break;
                case APIServiceManager.C_MSTAR_GET_ALTERNATE_PRODUCT:
                    APIServiceManager.getInstance().mstarGetAlternateProduct(this, outOfStockProductCode);
                    break;
            }
        }
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        super.onSyncData(data, transactionId);
        switch (transactionId) {
            case APIServiceManager.MSTAR_PRODUCT_DETAILS:
                onProductDetailsAvailable(data);
                break;
            case APIServiceManager.C_MSTAR_GET_ALTERNATE_PRODUCT:
                onAlternateSaltAvailable(data);
                break;
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        super.onFailed(transactionId, data);
        mListener.dismissLoader();
    }

    private void onProductDetailsAvailable(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && response.getResult() != null && response.getResult().getProductDetails() != null) {
                this.outOfStockProduct = response.getResult().getProductDetails();
                initMstarApi(APIServiceManager.C_MSTAR_GET_ALTERNATE_PRODUCT);
            } else {
                mListener.dismissLoader();
            }
        } else {
            mListener.dismissLoader();
        }
    }

    private void onAlternateSaltAvailable(String data) {
        alternateProduct = null;
        if (!TextUtils.isEmpty(data)) {
            MstarAlgoliaResponse mstarAlgoliaResponse = new Gson().fromJson(data, MstarAlgoliaResponse.class);
            if (mstarAlgoliaResponse != null && mstarAlgoliaResponse.getAlgoliaResultList() != null && !mstarAlgoliaResponse.getAlgoliaResultList().isEmpty()) {
                for (MstarAlgoliaResult product : mstarAlgoliaResponse.getAlgoliaResultList()) {
                    if (AppConstant.PRODUCT_AVAILABLE_CODE.equalsIgnoreCase(product.getAvailabilityStatus()) && product.getMaxQtyInOrder() > 0) {
                        MStarProductDetails productDetails = new MStarProductDetails();
                        productDetails.setSellingPrice(product.getSellingPrice());
                        productDetails.setMrp(product.getMrp());
                        productDetails.setAvailabilityStatus(product.getAvailabilityStatus());
                        productDetails.setManufacturerName(product.getManufacturerName());
                        productDetails.setCategories(getMstarCategories(product.getCategories()));
                        productDetails.setRxRequired(product.getRxRequired() == 1);
                        productDetails.setDisplayName(product.getDisplayName());
                        productDetails.setMaxQtyInOrder(product.getMaxQtyInOrder());
                        productDetails.setProduct_image_path(product.getImageUrl());
                        productDetails.setProductCode(product.getProductCode());
                        alternateProduct = productDetails;
                        break;
                    }
                }
            }
        }
        mListener.dismissLoader();
        if(alternateProduct!= null){
            setView();
        }else{
            mListener.dismissAlternateDialog();
        }
    }

    private void setView() {
        if (outOfStockProduct != null && alternateProduct != null && outOfStockProduct.getProductCode() != alternateProduct.getProductCode()) {
            mListener.enableDisableErrorView(false);
            setAlternateProductView();
            setOutOfStockProductView();
        } else mListener.enableDisableErrorView(true);

    }

    private void setAlternateProductView() {
        if (outOfStockProduct != null && alternateProduct != null) {
            mListener.enableAlternativeSalt(true);
            mListener.bindAlternativeView(alternateProduct, true);
        } else {
            mListener.setVisibilityToAddToCart(true);
            mListener.enableAlternativeSalt(false);
        }
    }


    public boolean isRxRequired(MStarProductDetails product) {
        if (product != null && !TextUtils.isEmpty(product.getProductType()) && !TextUtils.isEmpty(product.getSchedule()) && product.getSchedule().equals("1") && product.getProductType().equalsIgnoreCase("p"))
            return true;
        else if (product != null && !TextUtils.isEmpty(product.getProductType()) && !TextUtils.isEmpty(product.getSchedule()) && product.getSchedule().equals("0") && product.getProductType().equalsIgnoreCase("p"))
            return false;
        else
            return product != null && !TextUtils.isEmpty(product.getProductType()) && !TextUtils.isEmpty(product.getSchedule()) && !product.getSchedule().equalsIgnoreCase("f") && !product.getSchedule().equalsIgnoreCase("m") &&
                    !product.getSchedule().equalsIgnoreCase("o") && !product.getSchedule().equalsIgnoreCase("np") &&
                    product.getProductType().equalsIgnoreCase("p");
    }

    public String getStrikePrice(MStarProductDetails response) {
        return "\u20B9 " + String.format(Locale.getDefault(), "%.2f", response.getMrp().doubleValue());
    }

    public String getPrice(MStarProductDetails productItem) {
        return "\u20B9 " + String.format(Locale.getDefault(), "%.2f", productItem.getSellingPrice().doubleValue());
    }

    private void setOutOfStockProductView() {
        if (outOfStockProduct != null) {
            mListener.enableOutOfStockView(true);
            mListener.bindAlternativeView(outOfStockProduct, false);
        } else {
            mListener.enableOutOfStockView(false);
        }
    }

    @Override
    public void onHorizontalNumberPickerChanged(CustomNumberPicker customNumberPicker, int value) {
        quantity = value;
        mListener.setTotalAmount(alternateProduct, quantity);
    }

    public void addToCart() {
        if (alternateProduct != null && alternateProduct.getProductCode() > 0 && outOfStockProduct != null && outOfStockProduct.getProductCode() > 0)
            mListener.addAlternateToCart(alternateProduct.getProductCode(), quantity, outOfStockProduct.getProductCode());
    }

    private List<MStarCategory> getMstarCategories(List<String> categories) {
        if (categories != null && !categories.isEmpty()) {
            List<MStarCategory> categoryList = new ArrayList<>();
            for (String category : categories) {
                MStarCategory Mstarcategory = new MStarCategory();
                Mstarcategory.setName(category);
                categoryList.add(Mstarcategory);
            }
            return categoryList;
        }
        return null;
    }


    public interface AlternateInterface {
        void closeView();

        void addAlternateToCart(int productCodeToAdd, int quantity, int productCodeToRemove);

        void bindAlternativeView(MStarProductDetails productItem, boolean isAlternateSalt);

        void enableDisableErrorView(boolean enableErrorView);

        void enableOutOfStockView(boolean enable);

        void enableAlternativeSalt(boolean enable);

        void setTotalAmount(MStarProductDetails item, int quantity);

        void setVisibilityToAddToCart(boolean isGone);

        void showMessage(String message);

        boolean isNetworkConnected();

        void dismissAlternateDialog();

        void showLoader();

        void dismissLoader();
    }
}