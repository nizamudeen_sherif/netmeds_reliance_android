package com.netmedsmarketplace.netmeds.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.Interactor;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarBasicResponseTemplateModeForM2;
import com.nms.netmeds.base.model.MstarPrescriptionDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.utils.WebEngageHelper;

import java.io.Closeable;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class M2AttachPrescriptionViewModel extends AppViewModel implements Interactor<String>, CommonUtils.BitmapImageConversionInterface {


    private AttachPrescriptionListener mAttachPrescriptionListener;
    private Map<String, String> detachPrescriptionMap = new HashMap<>();
    private ArrayList<MStarUploadPrescription> m2PrescriptionList = new ArrayList<>();
    private BasePreference basePreference;
    private int failedTransactionId = 0;
    private int pos = 0;
    private boolean isMethod2WithCartID = false;
    public boolean isFromContinueButton;
    private AsyncTask<Void, Void, String> asyncThread;
    private static boolean isM2CartOpen = true;

    public M2AttachPrescriptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void initViewModel(AttachPrescriptionListener attachPrescriptionListener, BasePreference basePreference) {
        this.mAttachPrescriptionListener = attachPrescriptionListener;
        this.basePreference = basePreference;
        mAttachPrescriptionListener.enablePrescriptionView(false);
        initiateApiCall(APIServiceManager.MSTAR_GET_METHOD_2_CARTS);
    }


    private void initiateApiCall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        mAttachPrescriptionListener.showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.MSTAR_GET_METHOD_2_CARTS:
                    mAttachPrescriptionListener.vmShowProgress();
                    APIServiceManager.getInstance().mstarGetMethod2Carts(this, basePreference.getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_CART_DETAILS:
                    mAttachPrescriptionListener.vmShowProgress();
                    APIServiceManager.getInstance().mStarGetCartDetails(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarMethod2CartId(), transactionId);
                    break;
                case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
                    mAttachPrescriptionListener.vmShowProgress();
                    APIServiceManager.getInstance().mStarGetPastPrescription(this, basePreference.getMstarBasicHeaderMap(), getPastPrescriptionMultiPartRequest());
                    break;
                case APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION:
                    MultipartBody multipartBody = getMultiPartRequestBody();
                    if (multipartBody != null) {
                        if (isMethod2WithCartID || basePreference.getMstarMethod2CartId() != 0) {
                            mAttachPrescriptionListener.vmShowProgress();
                            APIServiceManager.getInstance().mstarUploadPrescription(this, basePreference.getMstarBasicHeaderMap(), basePreference.getMstarMethod2CartId(), "", multipartBody);
                        } else {
                            mAttachPrescriptionListener.vmShowProgress();
                            basePreference.setCartCount(0);
                            APIServiceManager.getInstance().mstarUploadPrescription(this, basePreference.getMstarBasicHeaderMap(), 0, AppConstant.YES, multipartBody);

                        }
                    } else {
                        navigateToM2SelectCallOrAddMed();
                    }
                    break;

                case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                    mAttachPrescriptionListener.vmShowProgress();
                    APIServiceManager.getInstance().mStartDetachPrescription(this, basePreference.getMstarBasicHeaderMap(), detachPrescriptionMap, basePreference.getMstarMethod2CartId());
                    break;
            }
        } else {
            failedTransactionId = transactionId;
        }
    }


    @Override
    public void onSyncData(String data, int transactionId) {
        mAttachPrescriptionListener.vmDismissProgress();
        switch (transactionId) {
            case APIServiceManager.MSTAR_GET_METHOD_2_CARTS:
                getAllMethod2CartsResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_CART_DETAILS:
                getCartDetailsResponse(data);
                break;
            case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
                pastPrescriptionResponse(data);
                break;
            case APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION:
                uploadPrescriptionResponse(data);
                break;
            case APIServiceManager.MSTAR_DETACH_PRESCRIPTION:
                detachPrescriptionResponse(data);
        }
    }


    @Override
    public void onFailed(int transactionId, String data) {
        mAttachPrescriptionListener.vmDismissProgress();
        failedTransactionId = transactionId;
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_PAST_PRESCRIPTION:
                mAttachPrescriptionListener.showWebserviceErrorView(true);
                break;
            case APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION:
                break;
        }
    }


    private void getAllMethod2CartsResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MstarBasicResponseTemplateModeForM2 response = new Gson().fromJson(data, MstarBasicResponseTemplateModeForM2.class);
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getMethod_2_carts() != null && !response.getMethod_2_carts().isEmpty()) {
                basePreference.setMstarMethod2CartId(response.getMethod_2_carts().get(0));
                initiateApiCall(APIServiceManager.MSTAR_GET_CART_DETAILS);
                isMethod2WithCartID = true;
            } else {
                //for creating method 2's cart
                basePreference.clearM2CartId();
                isMethod2WithCartID = false;
            }
        }
    }


    private MultipartBody getPastPrescriptionMultiPartRequest() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart(AppConstant.PAGE_INDEX, AppConstant.PAGE_INDEX_RX);
        builder.addFormDataPart(AppConstant.PAGESIZE, AppConstant.PAGE_SIZE_RX); // pagesize is 12
        return builder.build();
    }

    private void getCartDetailsResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null && response.getResult().getCartDetails() != null
                    && !response.getResult().getCartDetails().getPrescriptions().isEmpty()) {

                for (String prescriptionId : response.getResult().getCartDetails().getPrescriptions()) {
                    MStarUploadPrescription prescriptionDetails = new MStarUploadPrescription();
                    prescriptionDetails.setMethod2(true);
                    prescriptionDetails.setPrescriptionUploaded(true);
                    prescriptionDetails.setUploadedPrescriptionId(prescriptionId);
                    m2PrescriptionList.add(prescriptionDetails);
                }
                if (response.getResult().getCartDetails().getLines() != null && !response.getResult().getCartDetails().getLines().isEmpty()) {
                    basePreference.setM2CartCount(response.getResult().getCartDetails().getLines().size());
                } else {
                    basePreference.setM2CartCount(0);
                }
            }

            /**
             * To get the bitmap image from the glide and setting it to adapter */
            MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
            asyncThread = new CommonUtils.BitmapImageAsync(this, m2PrescriptionList, true).execute();
        }
    }

    private void uploadPrescriptionResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);

            //Successfully attached prescription
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null
                    && response.getResult().getAttachment_result() != null && !response.getResult().getAttachment_result().isEmpty()) {

                if (response.getResult().getCartDetails() != null) {
                    MStarCartDetails cartDetails = response.getResult().getCartDetails();
                    basePreference.setMstarMethod2CartId(cartDetails.isIsmethod2() ? cartDetails.getId() : 0);
                    isM2CartOpen = AppConstant.CART_STATUS_OPEN.equalsIgnoreCase(cartDetails.getCartStatus());
                }

                Map<String, MstarPrescriptionDetails> prescriptionDetails = response.getResult().getAttachment_result();
                for (Map.Entry<String, MstarPrescriptionDetails> detailsEntry : prescriptionDetails.entrySet()) {
                    for (MStarUploadPrescription prescription : m2PrescriptionList) {
                        if (prescription.getUploadKey() != null && prescription.getUploadKey().equalsIgnoreCase(detailsEntry.getKey())) {
                            prescription.setPrescriptionUploaded(true);
                            prescription.setPrescriptionUploaded(AppConstant.PRESCRIPTION_ATTACHED.equalsIgnoreCase(detailsEntry.getValue().getStatus()) || AppConstant.PRESCRIPTION_UPLOADED.equalsIgnoreCase(detailsEntry.getValue().getStatus()));
                            prescription.setUploadedPrescriptionId(detailsEntry.getValue().getPrescription_id());
                            prescription.setMethod2(true);
                            m2PrescriptionList.set(m2PrescriptionList.indexOf(prescription), prescription);
                        }
                    }
                }
                MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(m2PrescriptionList);
                /*if (MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().isEmpty()) {
                    MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(m2PrescriptionList);
                }*/

                /*WebEngage Upload Prescription Event*/
                if (getUploadPrescriptionList().size() > 0)
                    WebEngageHelper.getInstance().prescriptionUploadedEvent(mAttachPrescriptionListener.getContext(), getUploadPrescriptionList(), AppConstant.M2);

                navigateToM2SelectCallOrAddMed();
            }
        }
    }

    private void detachPrescriptionResponse(String data) {
        if (!TextUtils.isEmpty(data)) {
            boolean isNotDetached = false;
            MStarBasicResponseTemplateModel response = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null
                    && response.getResult().getDetachment_result() != null && !response.getResult().getDetachment_result().isEmpty()) {
                Map<String, MstarPrescriptionDetails> detachDetails = response.getResult().getDetachment_result();

                for (Map.Entry<String, MstarPrescriptionDetails> detachEntry : detachDetails.entrySet()) {
                    if (!detachEntry.getValue().getStatus().equalsIgnoreCase(AppConstant.DETACHED))
                        isNotDetached = true;
                }

            }
            if (isNotDetached) {
                //TODO
                // failure case for detach prescription
            }
        }

    }

    @Override
    public void showProgressBar() {
        mAttachPrescriptionListener.vmDismissProgress();
    }

    @Override
    public void hideProgressBar() {
        mAttachPrescriptionListener.vmDismissProgress();
    }

    @Override
    public Context getContext() {
        return mAttachPrescriptionListener.getContext();
    }

    @Override
    public void setBitmapImageAdapter() {
        setAdapter();
    }

    private void setAdapter() {
        if (m2PrescriptionList.size() > 0) {
            mAttachPrescriptionListener.enablePrescriptionView(true);
            mAttachPrescriptionListener.setPrescriptionListAdapter(m2PrescriptionList);
        } else {
            mAttachPrescriptionListener.enablePrescriptionView(false);
        }
    }

    private void pastPrescriptionResponse(String response) {
        if (!TextUtils.isEmpty(response)) {
            GetPastPrescriptionResponse pastPrescriptionResponse = new Gson().fromJson(response, GetPastPrescriptionResponse.class);
            if (pastPrescriptionResponse != null && pastPrescriptionResponse.getStatus() != null && pastPrescriptionResponse.getStatus().equalsIgnoreCase("Success")) {
                showPastPrescription(pastPrescriptionResponse.getResult());
            }
        }
        mAttachPrescriptionListener.vmDismissProgress();
    }

    private void showPastPrescription(GetPastPrescriptionResult result) {
        if (result != null && (result.getDigitizedPrescriptionCount() > 0 || result.getPastOneWeekUnDigitizedRxCount() > 0)) {
            mAttachPrescriptionListener.setDigitizedPrescription(result);
        } else
            mAttachPrescriptionListener.setError(getApplication().getResources().getString(R.string.text_past_prescriotion_error));
    }


    public void cameraView() {
        if (isMaximumPrescriptionUploaded()) {
            mAttachPrescriptionListener.dismissSnackBar();
            if (mAttachPrescriptionListener.checkCameraPermission()) {
                mAttachPrescriptionListener.initCameraIntent();
            } else
                mAttachPrescriptionListener.requestPermission(PermissionConstants.CAMERA_PERMISSION);

        }
    }

    public void galleryView() {
        if (isMaximumPrescriptionUploaded()) {
            mAttachPrescriptionListener.dismissSnackBar();
            if (mAttachPrescriptionListener.checkGalleryPermission()) {
                mAttachPrescriptionListener.launchGalleryIntent();
            } else
                mAttachPrescriptionListener.requestPermission(PermissionConstants.GALLERY_PERMISSION);
        }

    }

    public void pastRxView() {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        mAttachPrescriptionListener.showNoNetworkView(isConnected);
        if (isConnected) {
            mAttachPrescriptionListener.dismissSnackBar();
            if (isMaximumPrescriptionUploaded()) {
                getPastPrescription();
            }
        }
    }

    private boolean isMaximumPrescriptionUploaded() {
        ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(getApplication().getApplicationContext()).getConfiguration(), ConfigurationResponse.class);
        int prescriptionLimit = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null ? response.getResult().getConfigDetails().getUploadPrescriptionLimit() : 0;
        if (m2PrescriptionList.size() >= prescriptionLimit) {
            mAttachPrescriptionListener.setError(getApplication().getResources().getString(R.string.text_maximum_prescription_uploaded));
            return false;
        } else {
            return true;
        }
    }

    public void continueAction() {
        /*Google Analytics Click Event*/
        //uploadPrescription();
        isFromContinueButton = true;
        mAttachPrescriptionListener.isContinueButtonEnabled(false);
        updatePrescription();
        basePreference.setMStarM2CartIsOpen(isM2CartOpen);
        GoogleAnalyticsHelper.getInstance().postEvent(mAttachPrescriptionListener.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_UPLOAD_CONTINUED, GoogleAnalyticsHelper.EVENT_LABEL_M2_UPLOAD_CONTINUED);


    }

    public void updatePrescription() {
        if (!detachPrescriptionMap.isEmpty()) {
            initiateApiCall(APIServiceManager.MSTAR_DETACH_PRESCRIPTION);
        }
        initiateApiCall(APIServiceManager.MSTAR_UPLOAD_PRESCRIPTION);
    }


    private void navigateToM2SelectCallOrAddMed() {
        if (m2PrescriptionList.size() > 0 && M2Helper.getInstance() != null && isFromContinueButton) {
            mAttachPrescriptionListener.launchOrderConfirmation();
        }
    }

    public void setCapturedImage(int resultCode) {
        validateImage(mAttachPrescriptionListener.getUriFromCameraImage(resultCode));
    }

    public void setGalleryImage(int resultCode, Intent data) {
        validateImage(mAttachPrescriptionListener.getUriFromGalleryImage(resultCode, data));
    }

    private void validateImage(Uri uri) {
        if (uri != null) {
            String path = FileUtils.getPath(getApplication(), uri);
            if (mAttachPrescriptionListener.validateUploadedImage(path, getApplication(), mAttachPrescriptionListener.getViewGroup(), FileUtils.MAX_IMAGE_SIZE_IN_BYTES, getApplication().getResources().getString(com.nms.netmeds.base.R.string.text_max_file_size))) {
                Resources r = mAttachPrescriptionListener.getContext().getResources();
                int px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, r.getDimension(R.dimen.density_size_64), r.getDisplayMetrics()));
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap1 = BitmapFactory.decodeFile(path, bmOptions);
                if (bitmap1 != null) {
                    Bitmap bitmap = ImageUtils.getScaledBitmap(CommonUtils.rotateImage(mAttachPrescriptionListener.getContext(), bitmap1, path));
                    OutputStream outputStream = null;
                    try {
                        outputStream = mAttachPrescriptionListener.getContext().getContentResolver().openOutputStream(uri);
                        if (outputStream != null) {
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                        }
                    } catch (Exception e) {

                    } finally {
                        closeSilently(outputStream);
                        if (bitmap != null && !bitmap.isRecycled()) {
                            bitmap.recycle();
                        }
                    }
                    saveSelectedPrescriptionInList(CommonUtils.decodeSampledBitmapFromResource(path, px, px), path);
                }
            }
        }
        setAdapter();
    }

    private void saveSelectedPrescriptionInList(Bitmap bitmap, String path) {
        MStarUploadPrescription prescription = new MStarUploadPrescription();
        prescription.setBitmapFromGalleryOrCamera(true);
        prescription.setBitmapImage(bitmap);
        prescription.setFile(new File(path));
        m2PrescriptionList.add(prescription);
    }


    private static void closeSilently(@Nullable Closeable c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        } catch (Throwable t) {
            // Do nothing
        }
    }

    private void savePrescriptionFromPastRx(Bitmap bitmap, String prescriptionID) {
        MStarUploadPrescription prescription = new MStarUploadPrescription();
        prescription.setBitmapImage(bitmap);
        prescription.setPastPrescriptionId(prescriptionID);
        prescription.setUploadedPrescriptionId(prescriptionID);
        m2PrescriptionList.add(prescription);
    }


    public void resetPrescriptionAdapter(int position, ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        if (!prescriptionArrayList.isEmpty()) {
            for (MStarUploadPrescription prescription : prescriptionArrayList) {
                pos = prescriptionArrayList.indexOf(prescription);
                if (position == pos) {
                    prescriptionArrayList.remove(position);
                    if (prescription.isPrescriptionUploaded()) {
                        detachPrescriptionMap.put(AppConstant.PID + getProperIndexValue(detachPrescriptionMap.size()), prescription.getUploadedPrescriptionId());
                    }
                    break;
                }
            }
            mAttachPrescriptionListener.resetPrescriptionAdapter(prescriptionArrayList, position);
        }
    }


    private void getPastPrescription() {
        boolean isConnected = NetworkUtils.isConnected(getApplication().getApplicationContext());
        mAttachPrescriptionListener.showNoNetworkView(isConnected);
        if (isConnected) {
            mAttachPrescriptionListener.vmShowProgress();
            initiateApiCall(APIServiceManager.C_MSTAR_PAST_PRESCRIPTION);
        } else {
            failedTransactionId = APIServiceManager.C_MSTAR_PAST_PRESCRIPTION;
        }
    }


    @Override
    public void onRetryClickListener() {
        onRetry(failedTransactionId);
    }

    private void onRetry(int failedTransactionId) {
        initiateApiCall(failedTransactionId);
    }

    public void setPastPrescription(List<MStarUploadPrescription> prescriptionList) {

        for (MStarUploadPrescription prescription : prescriptionList) {
            if (prescription.isChecked()) {
                savePrescriptionFromPastRx(prescription.getBitmapImage(), prescription.getUploadedPrescriptionId());
            }
        }
        setAdapter();
    }


    private MultipartBody getMultiPartRequestBody() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        boolean isNewPrescriptionAvailableForUpload = false;
        if (!m2PrescriptionList.isEmpty()) {
            int position = 0;
            String index = "";

            for (MStarUploadPrescription prescription : m2PrescriptionList) {

                position = m2PrescriptionList.indexOf(prescription);

                index = getProperIndexValue(position);
                if (!prescription.isPrescriptionUploaded()) {
                    if (!isNewPrescriptionAvailableForUpload) {
                        isNewPrescriptionAvailableForUpload = true;
                    }
                    if (prescription.isBitmapFromGalleryOrCamera()) {
                        prescription.setUploadKey(AppConstant.PIMG + index);
                        builder.addFormDataPart(prescription.getUploadKey(), prescription.getFile().getName(), RequestBody.create(MediaType.parse(FileUtils.MULTI_PART_TYPE), prescription.getFile()));
                    } else {
                        prescription.setUploadKey(AppConstant.PID + index);
                        builder.addFormDataPart(prescription.getUploadKey(), prescription.getPastPrescriptionId());
                    }
                }
                m2PrescriptionList.set(position, prescription);
            }
        }
        if (isNewPrescriptionAvailableForUpload) {
            return builder.build();
        } else {
            return null;
        }
    }

    private String getProperIndexValue(int position) {
        if (position < 9 && position != 0) {
            return "0" + (position + 1);
        } else if (position == 9) {
            return "10";
        } else if (position == 0) {
            return "01";
        } else {
            return String.valueOf(position + 1);
        }
    }

    private List<String> getUploadPrescriptionList() {
        List<String> uploadPrescriptionList = new ArrayList<>();
        for (MStarUploadPrescription mStarUploadPrescription : m2PrescriptionList) {
            uploadPrescriptionList.add(mStarUploadPrescription.getUploadedPrescriptionId());
        }
        return uploadPrescriptionList;
    }

    public interface AttachPrescriptionListener {

        void requestPermission(int permissionCode);

        void setPrescriptionListAdapter(ArrayList<MStarUploadPrescription> prescriptionList);

        void initCameraIntent();

        void setDigitizedPrescription(GetPastPrescriptionResult result);

        void launchOrderConfirmation();

        void vmShowProgress();

        void vmDismissProgress();

        void dismissSnackBar();

        void setError(String error);

        Uri getUriFromCameraImage(int resultCode);

        Uri getUriFromGalleryImage(int resultCode, Intent data);

        boolean validateUploadedImage(String path, Application application, ViewGroup mainLayout, Long uploadSizeLimit, String maxFileSizeError);

        void launchGalleryIntent();

        boolean checkCameraPermission();

        boolean checkGalleryPermission();

        Context getContext();

        void showWebserviceErrorView(boolean isWebServiceError);

        void showNoNetworkView(boolean isConnected);

        void enablePrescriptionView(boolean isEnable);

        ViewGroup getViewGroup();

        void resetPrescriptionAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList, int position);

        void isContinueButtonEnabled(boolean isEnabled);

    }
}


