package com.netmedsmarketplace.netmeds;

public class RestAPIPathConstant {
    public static final String VERIFY_USER = "api/checkuser";
    public static final String SOCIAL_LOGIN = "api/verifysociallogin";
    public static final String SEND_OTP = "netmedsapp-otp/otpmessage";
    public static final String RESET = "api/resetpassword";
    public static final String FAQ_ALL_CATEGORY = "get-all-faq-categories.html";
    public static final String FAQ_CATEGORY_CONTENT = "get-faq-contents.html";
    public static final String CLICK_POST = "{username}/{key}/{cp_id}/{waybill}";
    public static final String PUSH_UN_REGISTER = "newApi/push/unregister";
    public static final String BOOMRANG = "api/get_expected_dod";
}
