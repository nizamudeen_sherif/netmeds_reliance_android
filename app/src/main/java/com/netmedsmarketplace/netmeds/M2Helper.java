package com.netmedsmarketplace.netmeds;

import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarUploadPrescription;

import java.util.ArrayList;

public class M2Helper {
    private static M2Helper helper;
    private ArrayList<MStarUploadPrescription> m2PrescriptionList = new ArrayList<>();
    private static MStarAddressModel mCustomerAddress = new MStarAddressModel();
    private static MStarAddressModel mCustomerBillingAddress = new MStarAddressModel();
    private int m2MethodCartId;
    private boolean isM2Order = false;
    private boolean isPrescriptionUploaded = false;
    private boolean isPaynowinitated = false;
    private boolean isAppOpenForFirstTime = false;

    //for PayNow & Retry

    private boolean isRetry = false;
    private boolean isPayNow = false;

    public static M2Helper getInstance() {
        if (helper == null) {
            helper = new M2Helper();
        }
        return helper;
    }

    public static MStarAddressModel getCustomerAddress() {
        return mCustomerAddress;
    }

    public static void setCustomerAddress(MStarAddressModel customerAddress) {
        mCustomerAddress = customerAddress;
    }

    public boolean isM2Order() {
        return isM2Order;
    }

    public void setM2Order(boolean m2Order) {
        isM2Order = m2Order;
    }

    public void clearData() {
        isM2Order = false;
        mCustomerAddress = new MStarAddressModel();
    }

    public static MStarAddressModel getmCustomerBillingAddress() {
        return mCustomerBillingAddress;
    }

    public static void setmCustomerBillingAddress(MStarAddressModel mCustomerBillingAddress) {
        M2Helper.mCustomerBillingAddress = mCustomerBillingAddress;
    }

    public boolean isPrescriptionUploaded() {
        return isPrescriptionUploaded;
    }

    public void setPrescriptionUploaded(boolean prescriptionUploaded) {
        isPrescriptionUploaded = prescriptionUploaded;
    }

    public boolean isPaynowinitated() {
        return isPaynowinitated;
    }

    public void setPaynowinitated(boolean paynowinitated) {
        isPaynowinitated = paynowinitated;
    }

    public int getM2MethodCartId() {
        return m2MethodCartId;
    }

    public void setM2MethodCartId(int m2MethodCartId) {
        this.m2MethodCartId = m2MethodCartId;
    }

    public ArrayList<MStarUploadPrescription> getM2PrescriptionList() {
        return m2PrescriptionList;
    }

    public void setM2PrescriptionList(ArrayList<MStarUploadPrescription> m2PrescriptionList) {
        this.m2PrescriptionList = m2PrescriptionList;
    }

    public boolean isRetry() {
        return isRetry;
    }

    public void setRetry(boolean retry) {
        isRetry = retry;
    }

    public boolean isPayNow() {
        return isPayNow;
    }

    public void setPayNow(boolean payNow) {
        isPayNow = payNow;
    }

    public boolean isAppOpenForFirstTime() {
        return isAppOpenForFirstTime;
    }

    public void setAppOpenForFirstTime(boolean appOpenForFirstTime) {
        isAppOpenForFirstTime = appOpenForFirstTime;
    }
}