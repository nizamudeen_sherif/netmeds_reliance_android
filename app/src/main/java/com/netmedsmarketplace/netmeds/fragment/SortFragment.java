package com.netmedsmarketplace.netmeds.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentSortBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SortViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.model.SortOptionLabel;

import java.util.List;

@SuppressLint("ValidFragment")
public class SortFragment extends BaseBottomSheetFragment implements SortViewModel.SortListener {

    private final List<SortOptionLabel> sortOptionList;
    private final Context mContext;
    private final ApplySortListener applySortListener;

    public SortFragment(Context context, List<SortOptionLabel> sortOptionLabels, ApplySortListener applySortListener) {
        this.sortOptionList = sortOptionLabels;
        this.mContext = context;
        this.applySortListener = applySortListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentSortBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sort, container, false);
        SortViewModel viewModel = new SortViewModel(getActivity().getApplication());
        viewModel.init(this, binding, sortOptionList, mContext);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void dismissView() {
        SortFragment.this.dismiss();
    }

    @Override
    public void applySort(SortOptionLabel sortOption) {
        SortFragment.this.dismiss();
        applySortListener.applySort(sortOption);
    }

    @Override
    public void clearSortSelection() {
        applySortListener.clearSort();
    }

    public interface ApplySortListener {
        void applySort(SortOptionLabel sortOption);

        void clearSort();
    }
}