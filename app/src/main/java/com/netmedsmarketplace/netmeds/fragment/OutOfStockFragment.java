package com.netmedsmarketplace.netmeds.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentOutOfStockBinding;
import com.nms.netmeds.base.BaseBottomSheetFragment;

@SuppressLint("ValidFragment")
public class OutOfStockFragment extends BaseBottomSheetFragment {

    private String errorMessage;
    private OutOfStockListener listener;
    private double totalPrice;

    public OutOfStockFragment(String message, OutOfStockListener listener, double totalPrice) {
        this.errorMessage = message;
        this.listener = listener;
        this.totalPrice = totalPrice;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentOutOfStockBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_out_of_stock, container, false);
        binding.description.setText(!TextUtils.isEmpty(errorMessage) ? errorMessage : "");
        binding.positiveButton.setVisibility(totalPrice == 0 ? View.GONE : View.VISIBLE);
        binding.cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        binding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                listener.addProducts();
            }
        });
        binding.closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return binding.getRoot();
    }

    public interface OutOfStockListener {
        void addProducts();
    }
}