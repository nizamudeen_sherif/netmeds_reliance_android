package com.netmedsmarketplace.netmeds.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.FilterOptionAdapter;
import com.netmedsmarketplace.netmeds.adpater.FilterTitleAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentFilterBinding;
import com.netmedsmarketplace.netmeds.model.MstarAlgoliaFilterTitle;
import com.netmedsmarketplace.netmeds.model.MstarFilterOption;
import com.netmedsmarketplace.netmeds.viewmodel.FilterViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SuppressLint("ValidFragment")
public class FilterFragment extends BaseDialogFragment implements FilterViewModel.FilterListener, FilterTitleAdapter.FilterTitleListener, FilterOptionAdapter.FilterOptionListener {
    private FilterViewModel viewModel;
    private FragmentFilterBinding binding;
    private String titleSelection = "";
    private FilterInterface mFilterInterface;
    private FilterOptionAdapter filterOptionAdapter;
    private FilterTitleAdapter filterTitleAdapter;
    private Map<String, Map<String, String>> filterOptionList;
    private String intentFrom = "";

    public FilterFragment() {
    }

    public FilterFragment(Map<String, Map<String, String>> filterOptionList, FilterInterface filterInterface, String intentFrom) {
        this.filterOptionList = filterOptionList;
        mFilterInterface = filterInterface;
        this.intentFrom = intentFrom;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false);
        if (getActivity() != null && getActivity().getApplication() != null) {
            viewModel = new FilterViewModel(getActivity().getApplication());
            viewModel.init(binding, filterOptionList, this, intentFrom);
            binding.setViewModel(viewModel);
            binding.search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (filterOptionAdapter != null)
                        filterOptionAdapter.getFilter().filter(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
        return binding.getRoot();
    }

    @Override
    public void closeDialog() {
        FilterFragment.this.dismiss();
    }

    @Override
    public void setFilterTitleAdapter(List<MstarAlgoliaFilterTitle> titleList, String selection) {
        this.titleSelection = selection;
        filterTitleAdapter = new FilterTitleAdapter(getActivity(), titleList, this, titleSelection);
        binding.filterTitle.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.filterTitle.setAdapter(filterTitleAdapter);
    }

    @Override
    public void setFilterOptionAdapter(ArrayList<MstarFilterOption> filterOptions) {
        viewModel.setFilterVisibility(titleSelection);
        filterOptionAdapter = new FilterOptionAdapter(filterOptions, this, titleSelection);
        binding.filterOption.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.filterOption.setAdapter(filterOptionAdapter);
    }

    @Override
    public void vmShowProgress() {
        showProgress(getActivity());
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void applyFilter() {
        dismissProgress();
        mFilterInterface.applyFilter();
        FilterFragment.this.dismiss();
    }

    @Override
    public void resetFilterView() {
        resetFilterOptions();
        if (filterTitleAdapter != null)
            filterTitleAdapter.notifyDataSetChanged();
        if (filterOptionAdapter != null)
            filterOptionAdapter.notifyDataSetChanged();
        mFilterInterface.clearFilter();
        FilterFragment.this.dismiss();
    }

    @Override
    public void setError(String error) {
        SnackBarHelper.snackBarCallBack(binding.searchView, getActivity(), error);
    }

    @Override
    public void toggleKeyBoard() {
        if (getActivity() != null)
            CommonUtils.hideKeyboard(getActivity(), binding.getRoot());
    }

    @Override
    public void setAlert(String errorMessage) {
        mFilterInterface.showError(errorMessage);
    }

    private void resetFilterOptions() {
        MstarFilterHelper.clearSelection();
    }

    @Override
    public void setFilterOption(MstarAlgoliaFilterTitle filterTitle) {
        titleSelection = filterTitle.getKey();
        viewModel.setFilterOption(filterOptionList.get(filterTitle.getKey()), filterTitle.getKey(), filterTitle.getTitle());
    }

    @Override
    public void setFilerOptionSelection(String option) {
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME)) {
            MstarFilterHelper.setManufactureSelection(option);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_BRAND)) {
            MstarFilterHelper.setBrandSelection(option);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE)) {
            MstarFilterHelper.setPriceSelection(option);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT)) {
            MstarFilterHelper.setDiscountSelection(option);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL)) {
            MstarFilterHelper.setCategorySelection(option);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK)) {
            MstarFilterHelper.setAvailabilitySelection(option);
        }
    }

    @Override
    public void removeFilterOptionSelection(String id) {
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME)) {
            MstarFilterHelper.removeSelection(MstarFilterHelper.getManufactureSelection(), id);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_BRAND)) {
            MstarFilterHelper.removeSelection(MstarFilterHelper.getBrandSelection(), id);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE)) {
            MstarFilterHelper.removeSelection(MstarFilterHelper.getPriceSelection(), id);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT)) {
            MstarFilterHelper.removeSelection(MstarFilterHelper.getDiscountSelection(), id);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL)) {
            MstarFilterHelper.removeSelection(MstarFilterHelper.getCategorySelection(), id);
        }
        if (titleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK)) {
            MstarFilterHelper.removeSelection(MstarFilterHelper.getAvailabilitySelection(), id);
        }
    }

    private ArrayList<String> removeDuplicates(ArrayList<String> selectionList) {
        ArrayList<String> list = new ArrayList<>();
        HashSet<String> hashSet = new HashSet<>(selectionList);
        list.clear();
        list.addAll(hashSet);
        return list;
    }

    public void itemRemoveFromArrayList(ArrayList<String> removeList, String removeItem) {
        for (Iterator<String> iterator = removeList.iterator(); iterator.hasNext(); ) {
            String item = iterator.next();
            if (item.equals(removeItem))
                iterator.remove();
        }
    }

    public interface FilterInterface {
        void applyFilter();

        void clearFilter();

        void showError(String errorMessage);
    }
}
