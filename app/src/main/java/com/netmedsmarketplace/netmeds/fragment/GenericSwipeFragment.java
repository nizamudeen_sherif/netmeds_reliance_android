package com.netmedsmarketplace.netmeds.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.GenericsSlideWindowBinding;
import com.nms.netmeds.base.utils.BasePreference;

public class GenericSwipeFragment extends DialogFragment {
    private String genericWorksUrl;

    private GenericsSlideWindowBinding binding;
    private Context context;
    private BasePreference basePreference;

    public GenericSwipeFragment(String genericWorks, Context baseContext, BasePreference preference) {
        super();
        this.genericWorksUrl = genericWorks;
        this.context = baseContext;
        this.basePreference = preference;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.generics_slide_window, container, true);
        Glide.with(context).load(genericWorksUrl).error(Glide.with(context).load(R.drawable.ic_no_image)).into(binding.genericWorks);
        binding.imgDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GenericSwipeFragment.this.dismiss();
            }
        });
        binding.outerLayoutSwip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GenericSwipeFragment.this.dismiss();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setGravity(Gravity.END);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }
    }


}
