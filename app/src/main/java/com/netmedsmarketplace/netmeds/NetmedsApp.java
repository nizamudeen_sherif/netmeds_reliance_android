package com.netmedsmarketplace.netmeds;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.nmp.android.netmeds.navigation.RouteController;
import com.nms.netmeds.base.BuildConfig;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.MATHelper;
import com.tune.application.TuneActivityLifecycleCallbacks;
import com.webengage.sdk.android.WebEngage;
import com.webengage.sdk.android.WebEngageActivityLifeCycleCallbacks;
import com.webengage.sdk.android.WebEngageConfig;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;

public class NetmedsApp extends Application {
    String variant;
    String flavor;
    String str = BuildConfig.BUILD_TYPE;

    private static NetmedsApp netmedsApp;
    private boolean addAddress;
    private AdvertisingIdClient.Info advertisingIdClient;

    public static NetmedsApp getInstance() {
        if (netmedsApp == null)
            netmedsApp = new NetmedsApp();
        return netmedsApp;
    }

    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onCreate() {
        super.onCreate();
        ConfigMap.getInstance().initialize(this);

        //Social Media Facebook initialization
        FacebookSdk.sdkInitialize(this);
        //Launch Intent Manager Initiation
        RouteController.initRouteController(this);
        FirebaseApp.initializeApp(this);
        //Crashlytics initialization
        variant = str.substring(0, 1).toUpperCase() + str.substring(1);
        flavor = ConfigMap.getInstance().getProperty(ConfigConstant.PRODUCT_FLAVOR);
        str = flavor + variant;
        if (str.equals("prodRelease")) {
            Fabric.with(this, new Crashlytics());

            //MAT initialization
            MATHelper.getInstance().initTune(this);
            if (Build.VERSION.SDK_INT >= 14)
                registerActivityLifecycleCallbacks(new TuneActivityLifecycleCallbacks());
            MATHelper.getInstance().setExistingUserMAT(this);

            //Facebook Event initialization
            AppEventsLogger.activateApp(this);
        }
        getGoogleAdvertisingId();
        initWebEngage();
        //Fire base cloud message
        initFcm();
        setWebEngageLogin();
    }

    private void getGoogleAdvertisingId() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    advertisingIdClient = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                    if (advertisingIdClient != null && !TextUtils.isEmpty(advertisingIdClient.getId())) {
                        BasePreference.getInstance(getApplicationContext()).setMstarGoogleAdvertisingId(advertisingIdClient.getId());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean isAddAddress() {
        return addAddress;
    }

    public void setAddAddress(boolean addAddress) {
        this.addAddress = addAddress;
    }

    public void initWebEngage() {
        //Initialize WebEngage SDK with license code
        String webEngageKey = ConfigMap.getInstance().getProperty(ConfigConstant.WEB_ENGAGE_KEY);
        WebEngageConfig webEngageConfig = new WebEngageConfig.Builder()
                .setWebEngageKey(webEngageKey)
                .setDebugMode(false) // only in development mode
                .setLocationTracking(false)
                .build();
        registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(getApplicationContext(), webEngageConfig));
    }

    public void initFcm() {
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("getInstanceId failed", task.getException());
                            return;
                        }
                        sendTokenToWebEngage(task);
                    }
                });
    }

    private void sendTokenToWebEngage(Task<InstanceIdResult> task) {
        // Get new Instance ID token
        if (task.getResult() != null && !TextUtils.isEmpty(task.getResult().getToken())) {
            String token = task.getResult().getToken();
            WebEngage.get().setRegistrationID(token);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void setWebEngageLogin() {
        MStarCustomerDetails customerDetails = new Gson().fromJson(BasePreference.getInstance(this).getCustomerDetails(), MStarCustomerDetails.class);
        String userId = customerDetails != null ? Integer.toString(customerDetails.getId()) : "";
        if (!TextUtils.isEmpty(userId))
            WebEngage.get().user().login(userId);
    }
}
