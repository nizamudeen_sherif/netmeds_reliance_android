package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogAddCartBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AddCartBottomSheetDialogViewModel;
import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.widget.CustomNumberPicker;
import com.nms.netmeds.base.widget.CustomNumberPickerListener;

import java.math.BigDecimal;

@SuppressLint("ValidFragment")
public class AddCartBottomSheetDialog extends BaseDialogFragment implements AddCartBottomSheetDialogViewModel.AddCartBottomSheetListener, CustomNumberPickerListener {

    private final MStarProductDetails productDetails;
    private final BottomSheetDialogListener dialogListener;
    private DialogAddCartBinding addCartBinding;
    private AddCartBottomSheetDialogViewModel bottomSheetDialogViewModel;
    private boolean isFromSearch;

    public AddCartBottomSheetDialog(MStarProductDetails productDetails, boolean isFromSearch, BottomSheetDialogListener dialogListener) {
        this.productDetails = productDetails;
        this.dialogListener = dialogListener;
        this.isFromSearch = isFromSearch;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        addCartBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_cart, container, false);
        bottomSheetDialogViewModel = new AddCartBottomSheetDialogViewModel(getActivity().getApplication());
        bottomSheetDialogViewModel.init(BasePreference.getInstance(getActivity()), productDetails, this);
        addCartBinding.setViewModel(bottomSheetDialogViewModel);
        addCartBinding.strikePrice.setPaintFlags(addCartBinding.strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        return addCartBinding.getRoot();
    }

    @Override
    public void vmBottomSheetDismiss() {
        this.dismissAllowingStateLoss();
    }


    @Override
    public void vmBottomSheetAddCart() {
        dismissProgress();
        this.dismissAllowingStateLoss();
        dialogListener.bsAddCartCallBack();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    vmBottomSheetDismiss();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void vmBottomSheetShowProgress() {
        showProgress(getActivity());
    }

    @Override
    public void vmBottomSheetDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmShowAlert(String message) {
        dialogListener.showAlert(message);
    }

    @Override
    public void updateCartCount(int count) {
        dialogListener.updateCartCount(count);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(getContext());
    }

    @Override
    public void loadNumberPicker() {
        CustomNumberPicker customNumberPicker = new CustomNumberPicker(getContext());
        customNumberPicker.setListener(this);
        customNumberPicker.setMaxValue(productDetails.getStockQty() < productDetails.getMaxQtyInOrder() ? productDetails.getStockQty() : productDetails.getMaxQtyInOrder());
        this.addCartBinding.qtyPicker.removeAllViews();
        this.addCartBinding.qtyPicker.addView(customNumberPicker);
    }

    @Override
    public void loadProductImage(String url) {
        Glide.with(this)
                .load(url)
                .error(Glide.with(addCartBinding.productImage).load(R.drawable.ic_no_image))
                .into(addCartBinding.productImage);
    }

    @Override
    public boolean isFromSearchActivity() {
        return isFromSearch;
    }

    @Override
    public void onHorizontalNumberPickerChanged(CustomNumberPicker customNumberPicker, int value) {
        bottomSheetDialogViewModel.setQuantity(value);
        addCartBinding.totalAmount.setText(CommonUtils.getPriceInFormat(productDetails.getSellingPrice().multiply(BigDecimal.valueOf(value))));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        dialogListener.addToCartBottomSheetClose();
    }

    public interface BottomSheetDialogListener {
        void bsAddCartCallBack();

        void showAlert(String message);

        void updateCartCount(int count);

        void addToCartBottomSheetClose();
    }
}
