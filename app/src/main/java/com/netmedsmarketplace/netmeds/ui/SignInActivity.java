package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.login.LoginManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySigninBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SignInViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.Arrays;

import static com.nms.netmeds.base.IntentConstant.DOMAIN;
import static com.nms.netmeds.base.IntentConstant.SOCIAL_LOGIN_FLAG;
import static com.nms.netmeds.base.IntentConstant.TOKEN;

public class SignInActivity extends BaseViewModelActivity<SignInViewModel> implements SignInViewModel.OnSignInListener {

    private static final int GET_ACCOUNTS_PERMISSION_REQUEST_CODE = 4;
    private static final int RC_SIGN_IN = 0;
    private final String[] permissions = {android.Manifest.permission.GET_ACCOUNTS};
    private ActivitySigninBinding signinBinding;
    private SignInViewModel signInViewModel;
    private boolean isNavigateToCart = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signinBinding = DataBindingUtil.setContentView(this, R.layout.activity_signin);
        signinBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(signinBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected SignInViewModel onCreateViewModel() {
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel.class);
        signInViewModel.init(signinBinding, this);
        onRetry(signInViewModel);
        return signInViewModel;
    }

    public void checkIntentValue() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.RESEND_OTP_ERROR_MESSAGE)) {
            vmSnackBarMessage(getIntent().getStringExtra(IntentConstant.RESEND_OTP_ERROR_MESSAGE));
            signinBinding.name.setText(getIntent().getStringExtra(IntentConstant.RESEND_OTP_ERROR_MOBILE_NO));
            signinBinding.name.clearFocus();
            CommonUtils.hideKeyboard(this, signinBinding.siginViewContent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            SignInActivity.this.finish();
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            signInViewModel.googleSignInResult(data);
        } else {
            signInViewModel.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setTermsAndCondition() {
        final SpannableString clickableText = new SpannableString(getString(R.string.text_signin_terms));
        clickableText.setSpan(getSpanClick(ConfigConstant.TERMS_CONDITION, 31, 48), 31, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickableText.setSpan(getSpanClick(ConfigConstant.PRIVACY_POLICY, 53, 75), 53, 75, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickableText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorMediumPink)), 31, 48, 0);
        clickableText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorMediumPink)), 53, 75, 0);
        signinBinding.terms.setText(clickableText);
        signinBinding.terms.setMovementMethod(LinkMovementMethod.getInstance());
        signinBinding.terms.setHighlightColor(Color.TRANSPARENT);
    }

    private void setNavigation() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.FROM_CART) && getIntent().getBooleanExtra(IntentConstant.FROM_CART, false))
            isNavigateToCart = getIntent().getBooleanExtra(IntentConstant.FROM_CART, false);
    }

    private ClickableSpan getSpanClick(final String navigatePageFor, final int startPosition, final int endPosition) {
        return new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startWebPage(ConfigMap.getInstance().getProperty(navigatePageFor), getString(R.string.text_signin_terms).substring(startPosition, endPosition));
            }
        };
    }

    private void startWebPage(String url, String title) {
        Intent intent = new Intent(this, NetmedsWebViewActivity.class);
        intent.putExtra(IntentConstant.WEB_PAGE_URL, url);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, title);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permsRequestCode == GET_ACCOUNTS_PERMISSION_REQUEST_CODE) {
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    vmGoogleSignIn();
                }
            }
        }
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmPermissionForGoogleGetAccount() {
        ActivityCompat.requestPermissions(this, permissions, GET_ACCOUNTS_PERMISSION_REQUEST_CODE);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void vmGoogleSignIn() {
        showProgress(this);
        Intent signInIntent = signInViewModel.getmGoogleSignInClient().getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        signInViewModel.getmGoogleSignInClient().signOut();
    }

    @Override
    public void vmFacebookSignIn() {
        showProgress(this);
        String email = "email";
        String publicProfile = "public_profile";
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(email, publicProfile));
    }

    @Override
    public void vmNavigateToMobileVerification() {
        Intent intent = new Intent(SignInActivity.this, MobileNumberActivity.class);
        intent.putExtra(SOCIAL_LOGIN_FLAG, true);
        intent.putExtra(DOMAIN, signInViewModel.getSocialFlag() != null && !TextUtils.isEmpty(signInViewModel.getSocialFlag()) ? signInViewModel.getSocialFlag() : "");
        intent.putExtra(TOKEN, signInViewModel.getAccessToken() != null && !TextUtils.isEmpty(signInViewModel.getAccessToken()) ? signInViewModel.getAccessToken() : "");
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        startActivity(intent);
    }

    @Override
    public void vmCallBackOnNavigationSignInWithPasswordActivity() {
        Intent intent = new Intent(SignInActivity.this, SignInWithPasswordActivity.class);
        intent.putExtra(IntentConstant.FROM_CART, isNavigateToCart);
        intent.putExtra(IntentConstant.PHONE_NUMBER, signinBinding.name != null && signinBinding.name.getText() != null ? signinBinding.name.getText().toString() : "");
        startActivity(intent);
    }

    @Override
    public void vmCallBackOnNavigationSignUpActivity(String randomKey) {
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        intent.putExtra(IntentConstant.FROM_CART, isNavigateToCart);
        intent.putExtra(IntentConstant.PHONE_NUMBER, signinBinding.name != null && signinBinding.name.getText() != null ? signinBinding.name.getText().toString() : "");
        intent.putExtra(IntentConstant.RANDOM_KEY, randomKey);
        startActivity(intent);
    }

    @Override
    public void vmCallBackOnNavigationActivity() {
        if (isNavigateToCart)
            launchCart();
        else launchNavigation();
        finishAffinity();
    }

    private void launchCart() {
        Intent intent = new Intent(this, MStarCartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void launchNavigation() {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        signInViewModel.setButtonProperties(true);
        setTermsAndCondition();
        setNavigation();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_SIGN_IN_SIGN_UP);
    }

    @Override
    public void vmSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(signinBinding.siginViewContent, this, message);
    }

    @Override
    public void vmShowNoNetworkView(boolean isConnected) {
        signinBinding.siginViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        signinBinding.siginNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void vmWebServiceError(boolean isWebserviceError) {
        signinBinding.siginViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        signinBinding.siginApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public Context getContext() {
        return SignInActivity.this;
    }
}
