package com.netmedsmarketplace.netmeds.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogCodWarningBinding;
import com.netmedsmarketplace.netmeds.viewmodel.CODWarningDialogViewModel;
import com.nms.netmeds.base.BaseDialogFragment;

public class CODWarningDialog extends BaseDialogFragment implements CODWarningDialogViewModel.CODWarningDialogViewModelListener {

    private DialogCodWarningBinding binding;

    private CODWarningDialogListener listener;

    private CODWarningDialogViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        viewModel = ViewModelProviders.of(this).get(CODWarningDialogViewModel.class);
        viewModel.setListener(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_cod_warning, container, false);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    onNegativeButtonClicked();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void onPositiveButtonClicked() {
        this.dismissAllowingStateLoss();
        listener.initiateCashOnDelivery();
    }

    @Override
    public void onNegativeButtonClicked() {
        listener.dismissDialog();
        this.dismissAllowingStateLoss();
    }

    public void setCODWarningDialog(CODWarningDialog.CODWarningDialogListener listener) {
        this.listener = listener;
    }

    interface CODWarningDialogListener {

        void initiateCashOnDelivery();

        void dismissDialog();

    }
}
