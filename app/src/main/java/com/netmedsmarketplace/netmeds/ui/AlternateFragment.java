package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterAlternateProductBinding;
import com.netmedsmarketplace.netmeds.databinding.FragmentAlternateProductBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AlternateViewModel;
import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.base.widget.CustomNumberPicker;

import java.math.BigDecimal;


public class AlternateFragment extends BaseDialogFragment implements AlternateViewModel.AlternateInterface {

    private int outOfStockProductCode;
    private AlternateProductsListener mListener;
    private FragmentAlternateProductBinding fragmentBinding;
    private AlternateViewModel viewModel;
    private String imageBasePath;

    @SuppressLint("ValidFragment")
    public AlternateFragment(int outOfStockProductCode, BasePreference basePreference, AlternateProductsListener listener) {
        this.outOfStockProductCode = outOfStockProductCode;
        this.mListener = listener;
        MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = new Gson().fromJson(basePreference.getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageBasePath = mStarBasicResponseTemplateModel != null && mStarBasicResponseTemplateModel.getResult() != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCatalogImageUrlBasePath()) ? mStarBasicResponseTemplateModel.getResult().getCatalogImageUrlBasePath() : "";
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_alternate_product, container, false);
        viewModel = ViewModelProviders.of(this).get(AlternateViewModel.class);
        viewModel.init(this, outOfStockProductCode);
        fragmentBinding.setViewModel(viewModel);
        return fragmentBinding.getRoot();
    }

    @Override
    public void closeView() {
        AlternateFragment.this.dismissAllowingStateLoss();
        mListener.onViewClosed(outOfStockProductCode, true);
    }

    @Override
    public void addAlternateToCart(int productCodeToAdd, int quantity, int productCodeToRemove) {
        mListener.addAlternateProduct(productCodeToAdd, quantity, productCodeToRemove);
        AlternateFragment.this.dismissAllowingStateLoss();
    }

    @Override
    public void bindAlternativeView(MStarProductDetails productItem, boolean isAlternateSalt) {
        AdapterAlternateProductBinding binding = isAlternateSalt ? fragmentBinding.alternateView : fragmentBinding.outOfStockView;
        binding.drugName.setText(!TextUtils.isEmpty(productItem.getDisplayName()) ? productItem.getDisplayName() : "");
        binding.strikePrice.setText((BigDecimal.ZERO.compareTo(productItem.getMrp()) == 0) ? viewModel.getStrikePrice(productItem) : "");
        binding.strikePrice.setVisibility((BigDecimal.ZERO.compareTo(productItem.getMrp()) == 0) && productItem.getMrp().compareTo(productItem.getSellingPrice()) == 0 ? View.GONE : View.VISIBLE);
        binding.strikePrice.setPaintFlags(binding.strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        binding.algoliaPrice.setText(!(BigDecimal.ZERO.compareTo(productItem.getSellingPrice()) == 0) ? viewModel.getPrice(productItem) : "");
        binding.drugDetail.setVisibility(View.GONE);
        binding.manufacturerName.setVisibility(!TextUtils.isEmpty(productItem.getManufacturerName()) ? View.VISIBLE : View.GONE);
        binding.manufacturerName.setText(!TextUtils.isEmpty(productItem.getManufacturerName()) ? String.format("By %s", productItem.getManufacturerName()) : "");
        binding.qtyPicker.setVisibility(isAlternateSalt ? View.VISIBLE : View.GONE);
        binding.stockInfo.setVisibility(isAlternateSalt ? View.GONE : View.VISIBLE);
        binding.stockInfo.setText(getContext().getResources().getString(R.string.text_out_of_stock));
        binding.categoryName.setText(productItem.getCategories() != null && productItem.getCategories().size() > 0 && productItem.getCategories().get(productItem.getCategories().size() - 1) != null ? productItem.getCategories().get(productItem.getCategories().size() - 1).getName() : "");
        binding.categoryName.setVisibility(productItem.getCategories() != null && productItem.getCategories().size() > 0 && productItem.getCategories().get(productItem.getCategories().size() - 1) != null && !TextUtils.isEmpty(productItem.getCategories().get(productItem.getCategories().size() - 1).getName()) ? View.VISIBLE : View.GONE);
        binding.rxRequired.setVisibility(productItem.isRxRequired() ? View.VISIBLE : View.GONE);
        Glide.with(getContext()).load(ImageUtils.checkImage(String.format("%s%s", imageBasePath, productItem.getProduct_image_path()))).error(Glide.with(getContext()).load(R.drawable.ic_no_image)).apply(new RequestOptions().circleCrop()).into(binding.productImage);
        loadNumberPicker(productItem, binding);
    }

    @Override
    public void enableDisableErrorView(boolean enableErrorView) {
        fragmentBinding.mainLayout.setVisibility(enableErrorView ? View.GONE : View.VISIBLE);
        fragmentBinding.errorText.setVisibility(enableErrorView ? View.VISIBLE : View.GONE);
        fragmentBinding.errorText.setText("");
    }

    @Override
    public void enableOutOfStockView(boolean enable) {
        fragmentBinding.outOfStockView.mainLayout.setVisibility(enable ? View.VISIBLE : View.GONE);
        fragmentBinding.outOfStockView.title.setText(getContext().getResources().getString(R.string.text_medicine_out_of_stock));
    }

    @Override
    public void enableAlternativeSalt(boolean enable) {
        fragmentBinding.alternateView.mainLayout.setVisibility(enable ? View.VISIBLE : View.GONE);
        fragmentBinding.alternateView.title.setText(getResources().getString(R.string.text_suggested_alternate));
    }

    @Override
    public void setTotalAmount(MStarProductDetails item, int quantity) {
        if (item != null && BigDecimal.ZERO.compareTo(item.getMrp()) != 0) {
            double total = item.getMrp().doubleValue() * quantity;
            fragmentBinding.totalAmount.setText(CommonUtils.getPriceInFormat(total));
        } else {
            fragmentBinding.addToCartView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setVisibilityToAddToCart(boolean isGone) {
        fragmentBinding.addToCartView.setVisibility(View.GONE);
        fragmentBinding.divider.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        SnackBarHelper.snackBarCallBack(fragmentBinding.mainLayout, getContext(), message);
        AlternateFragment.this.dismissAllowingStateLoss();
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(getContext());
    }

    @Override
    public void dismissAlternateDialog() {
        Toast.makeText(getActivity(), getActivity().getString(R.string.text_no_alternate_product_available), Toast.LENGTH_SHORT).show();
        closeView();
    }

    @Override
    public void showLoader() {
        fragmentBinding.closeView.setClickable(false);
        fragmentBinding.addToCartView.setClickable(false);
        showProgress(getActivity());
    }

    @Override
    public void dismissLoader() {
        fragmentBinding.closeView.setClickable(true);
        fragmentBinding.addToCartView.setClickable(true);
        dismissProgress();
    }

    private void loadNumberPicker(MStarProductDetails productItem, AdapterAlternateProductBinding binding) {
        if (getContext() != null) {
            CustomNumberPicker customNumberPicker = new CustomNumberPicker(getContext());
            customNumberPicker.setListener(viewModel);
            customNumberPicker.setValue(1);
            customNumberPicker.setMaxValue(productItem.getMaxQtyInOrder());
            binding.qtyPicker.removeAllViews();
            binding.qtyPicker.addView(customNumberPicker);
        }
    }

    public interface AlternateProductsListener {
        void addAlternateProduct(int productToAdd, int quantity, int outOfStockProductCode);

        void onViewClosed(Integer outOfStockProductCode, boolean isJustDismiss);
    }
}