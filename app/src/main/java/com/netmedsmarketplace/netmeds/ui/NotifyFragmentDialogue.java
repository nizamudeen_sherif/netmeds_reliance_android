package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.NotifyDialogueBinding;
import com.netmedsmarketplace.netmeds.viewmodel.NotifyDialogueViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.utils.BasePreference;

@SuppressLint("ValidFragment")
public class NotifyFragmentDialogue extends BaseBottomSheetFragment implements NotifyDialogueViewModel.NotifyListener {
    private String productName;
    private String drugType;
    private String drugCode;
    private NotifyDialogueBinding binding;
    private NotifyDialogueViewModel.NofifyFragmentDialogueListener listener;
    private NotifyDialogListener notifyDialogListener;

    public NotifyFragmentDialogue(String productName, String drugType, String drugCode, NotifyDialogueViewModel.NofifyFragmentDialogueListener listener) {
        this.productName = productName;
        this.drugType = drugType;
        this.listener = listener;
        this.drugCode = drugCode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.notify_dialogue, container, false);
        binding.setNotifyViewModel(onCreateViewModel());
        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }


    private NotifyDialogueViewModel onCreateViewModel() {
        NotifyDialogueViewModel viewModel = ViewModelProviders.of(this).get(NotifyDialogueViewModel.class);
        viewModel.init(this, productName, drugType, drugCode, listener, getBasePreference());
        return viewModel;
    }

    @Override
    public void dismissFragment() {
        this.dismissAllowingStateLoss();
    }

    @Override
    public void loderPrgress() {
        showProgress(getActivity());
    }

    @Override
    public void loderDismiss() {
        dismissProgress();
    }

    @Override
    public NotifyDialogueBinding binding() {
        return binding;
    }

    @Override
    public void navigateSiginActivity() {
        Intent intent = new Intent(getActivity(), SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private BasePreference getBasePreference() {
        return BasePreference.getInstance(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            notifyDialogListener = (NotifyDialogListener) getActivity();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        notifyDialogListener.notifyDialogClose();
    }

    public interface NotifyDialogListener {
        void notifyDialogClose();
    }
}
