package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogCancelOrderBinding;
import com.netmedsmarketplace.netmeds.viewmodel.CancelOrderDialogviewModel;

@SuppressLint("ValidFragment")
public class CancelOrderFragmentDialog extends BottomSheetDialogFragment implements CancelOrderDialogviewModel.CancelOrderViewModelListener {

    private CancelOrderDialogListener listener;

    public CancelOrderFragmentDialog() {
    }

    public CancelOrderFragmentDialog(CancelOrderDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, com.nms.netmeds.payment.R.style.CustomBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogCancelOrderBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_cancel_order, container, false);
        CancelOrderDialogviewModel viewModel = ViewModelProviders.of(this).get(CancelOrderDialogviewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(getActivity(), binding, this);
        return binding.getRoot();
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    listener.dismissDialog();
                    dismissAllowingStateLoss();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void dismissCancelDialog() {
        this.dismissAllowingStateLoss();
        listener.dismissDialog();
    }

    @Override
    public void onClickCancelOrder(String message, int messageIndex) {
        listener.onCancelInitiated(message, messageIndex);
    }

    public interface CancelOrderDialogListener {
        void onCancelInitiated(String cancelMessage, int calcelMessageIndex);

        void dismissDialog();
    }
}
