package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.BuildConfig;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.AttachPrescriptionProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.M2AttachPrescriptionAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityM1AttachPrescriptionBinding;
import com.netmedsmarketplace.netmeds.viewmodel.M1AttachPrescriptionViewModel;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.ui.M3DeliveryDateIntervalActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class M1AttachPrescriptionActivity extends BaseUploadPrescription<M1AttachPrescriptionViewModel> implements M1AttachPrescriptionViewModel.M1AttachPrescriptionListener,
        M1UploadPrescriptionFragment.UploadPrescriptionListener, M2AttachPrescriptionAdapter.PrescriptionListener, PastPrescriptionFragment.SelectedPrescriptionListener, SnackBarHelper.SnackBarHelperListener {

    private ActivityM1AttachPrescriptionBinding attachPrescriptionBinding;
    private M1AttachPrescriptionViewModel attachPrescriptionViewModel;
    private M2AttachPrescriptionAdapter attachPrescriptionAdapter;
    private String prescriptionBaseUrl = "";
    private static final int INFLATE_PRESCRIPTION_HANDLER = 2000;
    private boolean isOrderReviewBtnEnabled;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        attachPrescriptionBinding = DataBindingUtil.setContentView(this, R.layout.activity_m1_attach_prescription);
        attachPrescriptionBinding.setAttachViewModel(onCreateViewModel());
        toolBarSetUp(attachPrescriptionBinding.toolBarCustomLayout.toolbar);
        initToolBar(attachPrescriptionBinding.toolBarCustomLayout.collapsingToolbar,this.getResources().getString(R.string.text_attach_prescription));
        getIntentData();

        if (!TextUtils.isEmpty(prescriptionBaseUrl)) {
            if (checkCameraPermission() && checkGalleryPermission()) {
                getPrescriptionBitmapAndStore();
            } else {
                requestPermission(PermissionConstants.CAMERA_PERMISSION);
                requestPermission(PermissionConstants.GALLERY_PERMISSION);
            }
        }
    }

    private void getIntentData() {
        if (getIntent() != null) {
            setProductList();
            if (getIntent().hasExtra("PRESCRIPTION_BASE_URL"))
                prescriptionBaseUrl = getIntent().getStringExtra("PRESCRIPTION_BASE_URL");
        }
    }

    private void setProductList() {
        if (getIntent().hasExtra(IntentConstant.RX_MEDICINE_LIST) && getIntent().getStringArrayListExtra(IntentConstant.RX_MEDICINE_LIST).size() > 0) {
            setProductNameListAdapter(getIntent().getStringArrayListExtra(IntentConstant.RX_MEDICINE_LIST));
        }
    }

    private void setProductNameListAdapter(ArrayList<String> productList) {
        AttachPrescriptionProductAdapter prescriptionProductAdapter = new AttachPrescriptionProductAdapter(productList);
        attachPrescriptionBinding.prescriptionProductList.setLayoutManager(new LinearLayoutManager(this));
        attachPrescriptionBinding.prescriptionProductList.setAdapter(prescriptionProductAdapter);
        attachPrescriptionBinding.prescriptionProductList.setVisibility(View.VISIBLE);
    }


    @Override
    protected M1AttachPrescriptionViewModel onCreateViewModel() {
        attachPrescriptionViewModel = ViewModelProviders.of(this).get(M1AttachPrescriptionViewModel.class);
        attachPrescriptionViewModel.initViewModel(BasePreference.getInstance(this), this, getSkuList(), getRxProductName());
        onRetry(attachPrescriptionViewModel);
        return attachPrescriptionViewModel;
    }


    private ArrayList<String> getSkuList() {
        ArrayList<String> skuList = new ArrayList<>();
        if (getIntent().hasExtra(IntentConstant.SKU_LIST) && getIntent().getStringArrayListExtra(IntentConstant.SKU_LIST).size() > 0) {
            skuList.addAll(getIntent().getStringArrayListExtra(IntentConstant.SKU_LIST));
        }
        return skuList;
    }

    private ArrayList<String> getRxProductName() {
        ArrayList<String> rxDrugNames = new ArrayList<>();
        if (getIntent().hasExtra(IntentConstant.RX_MEDICINE_LIST) && getIntent().getStringArrayListExtra(IntentConstant.RX_MEDICINE_LIST).size() > 0) {
            rxDrugNames.addAll(getIntent().getStringArrayListExtra(IntentConstant.RX_MEDICINE_LIST));
        }

        return rxDrugNames;
    }


    private void getPrescriptionBitmapAndStore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(prescriptionBaseUrl)) return;
                Bitmap bitmap = ImageUtils.convertBase64StringToBitmap(prescriptionBaseUrl, M1AttachPrescriptionActivity.this);
                if (bitmap == null) return;
                Uri uri = ImageUtils.getImageUri(M1AttachPrescriptionActivity.this, bitmap);
                attachPrescriptionViewModel.setDownloadImage(uri);
            }
        }, INFLATE_PRESCRIPTION_HANDLER);
    }


    @Override
    public void setProductNameListView(ArrayList<String> productList) {
        setProductNameListAdapter(productList);
    }

    @Override
    public void enableSelectAddress(boolean isEnableShimmer) {
        if (!isEnableShimmer)
            attachPrescriptionBinding.selectAddressShimmer.shimmer.startShimmerAnimation();
        else
            attachPrescriptionBinding.selectAddressShimmer.shimmer.stopShimmerAnimation();

        attachPrescriptionBinding.setAddressButton.setVisibility(isEnableShimmer ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.selectAddressShimmer.shimmer.setVisibility(isEnableShimmer ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setfullDigitalizedView() {
        attachPrescriptionBinding.prescriptionProductList.setVisibility(View.GONE);
        attachPrescriptionBinding.alertText.setText(getApplication().getResources().getString(R.string.text_fully_digitized));
    }

    @Override
    public void enablePrescriptionView(boolean isEnable) {
        attachPrescriptionBinding.m1ViewDivider.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.prescriptionList.setVisibility(isEnable ? View.VISIBLE : View.GONE);
    }

    @Override
    public ViewGroup getViewGroup() {
        return attachPrescriptionBinding.mainLayout;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void selectAddressButton(boolean isEnable) {
        isOrderReviewBtnEnabled = isEnable;
        attachPrescriptionBinding.setAddressButton.setBackground(isEnable ? getApplication().getResources().getDrawable(R.drawable.accent_button) : getApplication().getResources().getDrawable(R.drawable.secondary_button));
    }

    @Override
    public void setUploadPrescriptionVisibility(int visibility) {
        attachPrescriptionBinding.uploadPrescription.setVisibility(visibility);
    }

    @Override
    public void showWebserviceErrorView(boolean enable) {
        attachPrescriptionBinding.mainLayout.setVisibility(enable ? View.GONE : View.VISIBLE);
        attachPrescriptionBinding.m1AttachPrescriptionApiErrorView.setVisibility(enable ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.totalAmountLayout.setVisibility(!enable?View.GONE :View.VISIBLE);
        if(!enable){
            attachPrescriptionBinding.m1AttachPrescriptionNetworkErrorView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        attachPrescriptionBinding.mainLayout.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.m1AttachPrescriptionNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
        attachPrescriptionBinding.totalAmountLayout.setVisibility(isConnected? View.VISIBLE:View.GONE);
    }

    @Override
    public boolean isDontHavePrescriptionChecked() {
        return attachPrescriptionBinding.dontHavePrescriptionCheckBox.isChecked();
    }

    @Override
    public void setDisableDontHavePrescription(boolean check) {
        attachPrescriptionBinding.dontHavePrescriptionCheckBox.setChecked(check);
    }

    @Override
    public void enableSetAddressOption(boolean enable) {
        boolean isEnable = (MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().size() > 0) || (attachPrescriptionBinding.dontHavePrescriptionCheckBox.isChecked() || enable);
        selectAddressButton(isEnable);
    }

    @Override
    public void resetPrescriptionAdapter(ArrayList<MStarUploadPrescription> mStarUploadPrescriptionsList, int position) {
        if (mStarUploadPrescriptionsList.isEmpty()) {
            enablePrescriptionView(false);
            attachPrescriptionBinding.uploadPrescription.setVisibility(View.VISIBLE);
            selectAddressButton(false);
            MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(new ArrayList<String>());
        } else {
            enablePrescriptionView(true);
            selectAddressButton(true);
            attachPrescriptionBinding.uploadPrescription.setVisibility(View.GONE);
        }
        attachPrescriptionAdapter.notifyItemRemoved(position);
        attachPrescriptionAdapter.notifyItemRangeRemoved(position, mStarUploadPrescriptionsList.size());
        attachPrescriptionViewModel.isFromOrderReviewButtton = false;
        attachPrescriptionViewModel.updatePrescription();
    }

    @Override
    public boolean isSelectAddressButtonEnabled() {
        return isOrderReviewBtnEnabled;
    }

    private void setTotalAmount() {
        attachPrescriptionBinding.totalAmount.setText(CommonUtils.getPriceInFormat(PaymentHelper.getTotalAmount()));
    }

    @Override
    public void uploadPrescriptionView() {
        M1UploadPrescriptionFragment uploadPrescriptionFragment = new M1UploadPrescriptionFragment(this);
        getSupportFragmentManager().beginTransaction().add(uploadPrescriptionFragment, "UploadPrescriptionFragment").commitAllowingStateLoss();
    }

    @Override
    public void initCameraIntent() {
        launchCameraIntent(getContentUri());
    }

    private Uri getContentUri() {
        File photo = new File(FileUtils.getTempDirectoryPath(this), FileUtils.TEMP_DIRECTORY_PATH);
        return FileProvider.getUriForFile(this, String.format("%s%s", BuildConfig.APPLICATION_ID, FileUtils.PROVIDER), photo);
    }

    @Override
    public void setPrescriptionListAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(prescriptionArrayList);
        if (attachPrescriptionAdapter != null) {
            attachPrescriptionAdapter.notifyDataSetChanged();
        } else initAdapter(prescriptionArrayList);
    }

    private void initAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        attachPrescriptionAdapter = new M2AttachPrescriptionAdapter(prescriptionArrayList, this, true);
        attachPrescriptionBinding.prescriptionList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        attachPrescriptionBinding.prescriptionList.setAdapter(attachPrescriptionAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (checkPermissionResponse(grantResults)) {
            switch (requestCode) {
                case PermissionConstants.CAMERA_PERMISSION:
                    if (!TextUtils.isEmpty(prescriptionBaseUrl)) {
                        if (checkGalleryPermission()) {
                            getPrescriptionBitmapAndStore();
                            return;
                        }
                        return;
                    }
                    launchCameraIntent(getContentUri());
                    break;
                case PermissionConstants.GALLERY_PERMISSION:
                    if (!TextUtils.isEmpty(prescriptionBaseUrl)) {
                        if (checkCameraPermission()) {
                            getPrescriptionBitmapAndStore();
                            return;
                        }
                        return;
                    }
                    launchGalleryIntent();
                    break;
            }
        } else
            SnackBarHelper.snackBarCallBackSetAction(attachPrescriptionBinding.mainLayout, this, (requestCode == PermissionConstants.CAMERA_PERMISSION ? (getResources().getString(R.string.text_camera_permission_alert)) :
                    (getResources().getString(R.string.text_gallery_permission_alert))), "view", this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PermissionConstants.ACTION_IMAGE_CAPTURE:
                attachPrescriptionViewModel.setCapturedImage(resultCode);
                break;
            case PermissionConstants.ACTION_GALLERY_REQUEST:
                attachPrescriptionViewModel.setGalleryImage(resultCode, data);
                break;
        }
    }

    @Override
    public void initCameraView() {
        dismissSnackBar();
        if (checkCameraPermission()) {
            initCameraIntent();
        } else requestPermission(PermissionConstants.CAMERA_PERMISSION);
    }

    @Override
    public void initGalleryView() {
        dismissSnackBar();
        if (checkGalleryPermission()) {
            launchGalleryIntent();
        } else
            requestPermission(PermissionConstants.GALLERY_PERMISSION);
    }

    @Override
    public void initPastPrescription() {
        dismissSnackBar();
        attachPrescriptionViewModel.getPastPrescription();
    }

    @Override
    public void clickOnPrescriptionToPreview(Bitmap bitmap, int pos) {
        launchPreviewPrescription(bitmap);
    }

    @Override
    public void removePrescription(int position, ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        attachPrescriptionViewModel.resetPrescriptionAdapter(position, prescriptionArrayList);
        if (prescriptionArrayList.isEmpty() && attachPrescriptionBinding.dontHavePrescriptionCheckBox.isChecked()) {
            enableSetAddressOption(true);
        } else if (prescriptionArrayList.isEmpty() && !attachPrescriptionBinding.dontHavePrescriptionCheckBox.isChecked()) {
            enableSetAddressOption(false);
        } else {
            enableSetAddressOption(true);
        }

    }

    @Override
    public void addPrescription() {
        if (!attachPrescriptionViewModel.isMaximumPrescriptionUploaded())
            uploadPrescriptionView();
    }

    private void initFreeDoctorConsultationView() {
        FreeDoctorConsultationFragment doctorConsultationFragment = new FreeDoctorConsultationFragment(true, this);
        getSupportFragmentManager().beginTransaction().add(doctorConsultationFragment, "FreeDoctorConsultationFragment").commitAllowingStateLoss();
    }

    @Override
    public void pastPrescriptionView(GetPastPrescriptionResult result) {
        PastPrescriptionFragment pastPrescriptionFragment = new PastPrescriptionFragment(this, this, result);
        getSupportFragmentManager().beginTransaction().add(pastPrescriptionFragment, "PastPrescriptionFragment").commitAllowingStateLoss();
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,attachPrescriptionBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,attachPrescriptionBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void launchAddressView(boolean finishActivity) {
        Intent intent;
        if (!TextUtils.isEmpty(PaymentHelper.getSingle_address())) {
            intent = new Intent(this, SubscriptionHelper.getInstance().isCreateNewFillFlag() ? M3DeliveryDateIntervalActivity.class : OrderConfirmationActivity.class);
        } else {
            intent = new Intent(this, AddOrUpdateAddressActivity.class);
            intent.putExtra(IntentConstant.ADD_ADDRESS_FLAG, false);
            intent.putExtra(IntentConstant.IS_FROM_NEW_CUSTOMER, true);
            intent.putExtra(IntentConstant.ADDRESS, new Gson().toJson(new MStarAddressModel()));

        }
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST, getIntent().getStringExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST));
        startActivity(intent);


        if (finishActivity)
            this.finish();
    }

    @Override
    public void launchValidPrescriptionView() {
        ValidPrescriptionFragment validPrescriptionFragment = new ValidPrescriptionFragment();
        getSupportFragmentManager().beginTransaction().add(validPrescriptionFragment, "ValidPrescriptionFragment").commitAllowingStateLoss();
    }

    @Override
    public void launchFreeConsultationView() {
        initFreeDoctorConsultationView();
    }

    @Override
    public void dismissSnackBar() {
        SnackBarHelper.dismissSnackBar();
    }

    @Override
    public void setError(String error) {
        SnackBarHelper.snackBarCallBack(attachPrescriptionBinding.mainLayout, this, error);
    }

    @Override
    public void selectedPrescription(List<MStarUploadPrescription> prescriptionList) {
        enableSelectAddress(true);
        attachPrescriptionViewModel.setPastPrescription(prescriptionList);
    }

    @Override
    public void launchPreviewPrescription(Object previewImage) {
        if (getSupportFragmentManager().findFragmentByTag("ImagePreviewDialog") == null) {
            ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(previewImage);
            getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitAllowingStateLoss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void snackBarOnClick() {
        navigateToSetting();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        if (attachPrescriptionViewModel != null)
            GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_ATTACH_PRESCRIPTION_M1);
        setTotalAmount();
        //attachPrescriptionViewModel.getDuplicatePrescriptionList();
        dismissProgress();
    }


    @Override
    public void onBackPressed() {
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(new ArrayList<MStarUploadPrescription>());
        MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(new ArrayList<String>());
        super.onBackPressed();
        finish();
    }
}