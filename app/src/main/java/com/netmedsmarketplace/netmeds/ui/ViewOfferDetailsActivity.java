package com.netmedsmarketplace.netmeds.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityViewOfferDetailsBinding;
import com.netmedsmarketplace.netmeds.viewmodel.ViewOfferDetailsViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeRouter;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeUtil;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MstarNetmedsOffer;

public class ViewOfferDetailsActivity extends BaseViewModelActivity<ViewOfferDetailsViewModel> implements ViewOfferDetailsViewModel.ViewOfferDetailsListener {

    private ViewOfferDetailsViewModel viewModel;
    private ActivityViewOfferDetailsBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_offer_details);
        onCreateViewModel();
        initToolBar();
    }

    @Override
    protected ViewOfferDetailsViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(ViewOfferDetailsViewModel.class);
        viewModel.init(binding, this, getIntent());
        viewModel.getOfferMutableData().observe(this, new OfferObserver());
        onRetry(viewModel);
        return viewModel;
    }

    private void initToolBar() {
        toolBarSetUp(binding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    @Override
    public void showLoadingProgress() {
        showProgress(this);
    }

    @Override
    public void dismissLoadingProgress() {
        dismissProgress();
    }

    @Override
    public void forMoreDetailsClick(MstarNetmedsOffer details) {
        MstarNetmedsOffer model;
        if (getIntent().hasExtra(IntentConstant.OFFER_DETAILS)) {
            model = new Gson().fromJson(getIntent().getStringExtra(IntentConstant.OFFER_DETAILS), MstarNetmedsOffer.class);
        } else {
            model = details;
        }
        if (model != null) {
            Intent intent = new Intent(this, NetmedsWebViewActivity.class);
            intent.putExtra(IntentConstant.PAGE_TITLE_KEY, model.getTitle());
            intent.putExtra(IntentConstant.WEB_PAGE_URL, model.getUrl());
            startActivity(intent);
        }
    }

    @Override
    public void vmNavigateToOfferPage(String offerList,String title) {
        if (!TextUtils.isEmpty(offerList)) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstant.OFFER_LIST, offerList);
            intent.putExtra(IntentConstant.INDIVIDUAL_OFFER_SUB_LIST, false);
            intent.putExtra(IntentConstant.OFFER_PAGE_TITLE, !TextUtils.isEmpty(title)? title:AppConstant.MEDICINE);
            LaunchIntentManager.routeToActivity(ViewOfferDetailsActivity.this.getResources().getString(R.string.route_offers), intent, ViewOfferDetailsActivity.this);
        } else
            LaunchIntentManager.routeToActivity(ViewOfferDetailsActivity.this.getResources().getString(R.string.route_offers), this);

        ViewOfferDetailsActivity.this.finish();
    }

    @Override
    public Context getContext() {
        return ViewOfferDetailsActivity.this;
    }

    @Override
    public void onClickOfCopyCodeAndShop(String couponCode, boolean doCopyClipboard, String shopUrl) {
        if (!TextUtils.isEmpty(couponCode) && doCopyClipboard) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(AppConstant.OFFER_COUPON, couponCode);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, getString(R.string.text_coupon_copied), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, SearchActivity.class));
        } else if (!TextUtils.isEmpty(shopUrl)) {
            navigateToScreen(shopUrl);
        } else
            startActivity(new Intent(this, SearchActivity.class));
    }

    private void navigateToScreen(String shopUrl) {
        try {
            UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(Uri.parse(shopUrl)), this, false, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            navigateToOffersPage();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (binding != null && binding.cvForMoreDetails != null)
            binding.cvForMoreDetails.setClickable(true);
    }

    private class OfferObserver implements Observer<MstarNetmedsOffer> {
        @Override
        public void onChanged(@Nullable MstarNetmedsOffer obj) {
            viewModel.loadOffer(obj);
            binding.setViewModel(viewModel);
        }
    }

    @Override
    public void onBackPressed() {
        navigateToOffersPage();
    }

    private void navigateToOffersPage() {
        if (viewModel.isFromDeepLink())
            LaunchIntentManager.routeToActivity(ViewOfferDetailsActivity.this.getResources().getString(R.string.route_offers), this);
        ViewOfferDetailsActivity.this.finish();
    }
}
