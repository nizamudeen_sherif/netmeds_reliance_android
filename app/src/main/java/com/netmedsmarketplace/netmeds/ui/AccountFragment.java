package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.amazon.identity.auth.device.AuthError;
import com.amazon.identity.auth.device.api.Listener;
import com.amazon.identity.auth.device.api.authorization.AuthorizationManager;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogEditProfileBinding;
import com.netmedsmarketplace.netmeds.databinding.FragmentAccountBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AccountViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.view.HistoryActivity;


@SuppressLint("ValidFragment")
public class AccountFragment extends BaseViewModelFragment<AccountViewModel> implements AccountViewModel.AccountListener, EditProfileDialog.EditProfileDialogListener {

    private final int ACCOUNT_FRAGMENT_REQUEST_CODE = 12345;
    private FragmentAccountBinding binding;
    private AccountViewModel viewModel;
    private AccountCallBackListener listener;
    private DialogEditProfileBinding editProfileBinding;
    private EditProfileDialog editProfileDialog;


    public AccountFragment(AccountCallBackListener listener) {
        this.listener = listener;
    }

    public AccountFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false);
        onCreateViewModel();
        if (getActivity() != null) {
            ((NavigationActivity) getActivity()).setSupportActionBar(binding.toolbar);
            if (((NavigationActivity) getActivity()).getSupportActionBar() != null) {
                ((NavigationActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }
        onRetry(viewModel, binding.getRoot());
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }


    @Override
    protected AccountViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        viewModel.getCustomerDetailsLiveData().observe(this, new CustomerDetailsObserver());
        viewModel.init(getActivity(), binding, this);
        return viewModel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showSnackMessage(String resId, boolean isForEditDialog) {
        SnackBarHelper.snackBarCallBack(isForEditDialog ? editProfileBinding.clParentView : binding.clHeaderView, getActivity(), resId);
    }

    @Override
    public void onEditProfile() {
        if (getActivity() != null) {
            editProfileDialog = new EditProfileDialog(this, getContext());
            getActivity().getSupportFragmentManager().beginTransaction().add(editProfileDialog, IntentConstant.EDIT_PROFILE_DIALOG_FRAGMENT_TAG_NAME).commitAllowingStateLoss();
        }
    }

    @Override
    public void onPaymentClick() {
        startActivity(new Intent(getActivity(), PaymentHistoryActivity.class));
    }

    @Override
    public void onMedicine() {
        if (listener != null)
            listener.callOrder();
    }

    @Override
    public void onConsultation() {
        startActivity(new Intent(getActivity(), HistoryActivity.class));
    }

    @Override
    public void onOfferClick() {
        startActivity(new Intent(getActivity(), OfferActivity.class));
    }

    @Override
    public void onHelp() {
        startActivity(new Intent(getActivity(), NeedHelpActivity.class));
    }

    @Override
    public void onLegalInfo() {
        Intent intent = new Intent(getActivity(), LegalInfoWithHelpActivity.class);
        intent.putExtra(IntentConstant.LEGAL_INFO_HELP_TITLE_KEY, getString(R.string.text_legal_info));
        startActivity(intent);
    }

    @Override
    public void pastPrescriptions() {
        startActivity(new Intent(getActivity(), MyPrescriptionView.class));
    }

    @Override
    public void dismissEditDialog() {
        if (editProfileDialog != null)
            editProfileDialog.dismiss();
    }

    @Override
    public void navigateToRateActivity() {
        Intent intent = new Intent(getActivity(), RateAppActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLogout() {
        if (getActivity() != null) {
            getActivity().finish();
            BasePreference.getInstance(this.getActivity()).clear();
            M2Helper.getInstance().setM2Order(false);
            MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
            startActivity(new Intent(getActivity(), OnBoardingActivity.class));
        }
    }

    @Override
    public void deLinkAmazonPay() {
        if (getActivity() != null)
            AuthorizationManager.signOut(getActivity(), new Listener<Void, AuthError>() {
                @Override
                public void onSuccess(Void aVoid) {
                }

                @Override
                public void onError(AuthError authError) {
                }
            });
    }

    @Override
    public void loadMobileNoUpdateScreen() {
        navigateVerifyMobileNo("");
    }

    @Override
    public void onDiagnostic() {
        if (getActivity() != null)
            LaunchIntentManager.routeToActivity(getActivity().getString(R.string.route_diagnostic_order_history), getActivity());
    }

    @Override
    public void onElite() {
        if (getActivity() != null)
            LaunchIntentManager.routeToActivity(getActivity().getString(R.string.route_prime_membership_activity), getActivity());
    }

    @Override
    public void navigateVerifyMobileNo(String mobileNo) {
        Intent intent = new Intent(getActivity(), mobileNo.isEmpty() ? MobileNumberActivity.class : SocialLoginOtpActivity.class);
        intent.putExtra(IntentConstant.SOCIAL_LOGIN_FLAG, true);
        intent.putExtra(IntentConstant.FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY, mobileNo.isEmpty());
        intent.putExtra(IntentConstant.PHONE_NUMBER, mobileNo);
        intent.putExtra(IntentConstant.MOBILE_NO_UPDATE_FLAG, true);
        startActivityForResult(intent, ACCOUNT_FRAGMENT_REQUEST_CODE);
    }

    @Override
    public void networkError(int failedTransactionId, boolean isNetworkFailure, boolean isAPIFailure) {
        if (isAPIFailure) {
            viewModel.showApiError(failedTransactionId);
        } else {
            viewModel.failedTransactionId = failedTransactionId;
            viewModel.showNoNetworkView(!isNetworkFailure);
        }

    }

    @Override
    public void showMessage(String message, boolean isForEditDialog, DialogEditProfileBinding binding) {
        this.editProfileBinding = binding;
        showSnackMessage(message, isForEditDialog);
        if (!isForEditDialog) {
            viewModel.isFromEditCustomer = true;
            viewModel.fetchCustomerDetailsFirstTime();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        if (getActivity() != null)
            GoogleAnalyticsHelper.getInstance().postScreen(getActivity(), GoogleAnalyticsHelper.POST_SCREEN_MY_ACCOUNTS);
    }

    public interface AccountCallBackListener {
        void callOrder();
    }

    private class CustomerDetailsObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            if (response != null && response.getResult() != null && response.getResult().getCustomerDetails() != null) {
                viewModel.onCustomerDetailAvailable(response.getResult().getCustomerDetails());
                binding.setViewModel(viewModel);
            }

        }
    }
}
