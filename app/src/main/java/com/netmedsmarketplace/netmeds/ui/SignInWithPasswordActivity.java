package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySignWithPasswordBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SignInWithPasswordViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

import static com.nms.netmeds.base.IntentConstant.PHONE_NUMBER;
import static com.nms.netmeds.base.IntentConstant.RANDOM_KEY;
import static com.nms.netmeds.base.IntentConstant.RESET_PASSWORD_FLAG;

public class SignInWithPasswordActivity extends BaseViewModelActivity<SignInWithPasswordViewModel> implements SignInWithPasswordViewModel.SignInWithPasswordListener {

    private ActivitySignWithPasswordBinding withPasswordBinding;
    private String phoneNumber;
    private boolean isNavigateToCart = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        withPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_with_password);
        phoneNumber = getIntent().getStringExtra(IntentConstant.PHONE_NUMBER);
        withPasswordBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(withPasswordBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        setNavigation();
    }

    @Override
    protected SignInWithPasswordViewModel onCreateViewModel() {
        SignInWithPasswordViewModel withPasswordViewModel = ViewModelProviders.of(this).get(SignInWithPasswordViewModel.class);
        withPasswordViewModel.init(withPasswordBinding, this, getIntent());
        onRetry(withPasswordViewModel);
        return withPasswordViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                SignInWithPasswordActivity.this.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void vmCallBackOnNavigationSigInWithOtpActivity(String randomKey) {
        Intent intent = new Intent(SignInWithPasswordActivity.this, SignInWithOtpActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        intent.putExtra(IntentConstant.PHONE_NUMBER, phoneNumber);
        intent.putExtra(IntentConstant.RANDOM_KEY, randomKey);
        intent.putExtra(IntentConstant.FROM_CART, isNavigateToCart);
        startActivity(intent);
        SignInWithPasswordActivity.this.finish();
    }

    private void setNavigation() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.FROM_CART) && getIntent().getBooleanExtra(IntentConstant.FROM_CART, false))
            isNavigateToCart = getIntent().getBooleanExtra(IntentConstant.FROM_CART, false);
    }

    private boolean isPrimeNavigation() {
        return getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false);
    }

    @Override
    public void vmCallBackOnNavigationMobileNumberActivity() {
        Intent intent = new Intent(this, MobileNumberActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, isPrimeNavigation());
        SignInWithPasswordActivity.this.finish();
    }

    @Override
    public void vmCallBackOnNavigationSocialLoginActivity(String randomKey) {
        Intent intent = new Intent(this, SocialLoginOtpActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, isPrimeNavigation());
        intent.putExtra(RESET_PASSWORD_FLAG, true);
        intent.putExtra(RANDOM_KEY, randomKey);
        intent.putExtra(PHONE_NUMBER, phoneNumber);
        startActivity(intent);
        SignInWithPasswordActivity.this.finish();
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmNavigateToHome() {
        if (isNavigateToCart)
            launchCart();
        else if (isPrimeNavigation()) {
            primeNavigation();
        } else
            launchNavigation();
        finishAffinity();
    }

    @Override
    public void vmSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(withPasswordBinding.siginInViewContent, this, message);
    }

    @Override
    public Context getContext() {
        return SignInWithPasswordActivity.this;
    }

    private void launchCart() {
        Intent intent = new Intent(this, MStarCartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void launchNavigation() {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void primeNavigation() {
        Intent intent = new Intent(this, PrimeMemberShipActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_SIGN_IN_WITH_PASSWORD);
    }

}
