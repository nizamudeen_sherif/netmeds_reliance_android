package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.AlternateCartHeaderAdapter;
import com.netmedsmarketplace.netmeds.adpater.AlternateCartProductListAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityAlternateCartBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AlternateCartActivityViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.Alternative;
import com.nms.netmeds.base.model.CartSubstitutionResponseModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.LinePagerIndicatorDecoration;

import java.util.List;

public class AlternateCartActivity extends BaseViewModelActivity<AlternateCartActivityViewModel> implements AlternateCartActivityViewModel.AlternateCartActivityViewModelListener {

    private AlternateCartActivityViewModel viewModel;
    private ActivityAlternateCartBinding binding;
    private AlternateCartHeaderAdapter alternateCartHeaderAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alternate_cart);
        onCreateViewModel();
        toolBarSetUp(binding.toolbar);
        initToolBar();
    }

    @Override
    protected AlternateCartActivityViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(AlternateCartActivityViewModel.class);
        viewModel.initiateAlternateCart(this, BasePreference.getInstance(this));
        binding.setViewModel(viewModel);
        return viewModel;
    }

    private void initToolBar() {
        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(getString(R.string.text_alternate_cart));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setHeaderAdapter(AlternateCartHeaderAdapter alternateCartHeaderAdapter) {
        this.alternateCartHeaderAdapter = alternateCartHeaderAdapter;
        binding.rvHeader.setAdapter(alternateCartHeaderAdapter);
        binding.rvHeader.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvHeader.addItemDecoration(new LinePagerIndicatorDecoration(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorSecondary)));
        CommonUtils.snapHelper(binding.rvHeader);
    }

    @Override
    public AlternateCartHeaderAdapter getAdapter() {
        return alternateCartHeaderAdapter;
    }


    @Override
    public CartSubstitutionResponseModel getIndentData() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.CART_SUBSTITUTION) && !TextUtils.isEmpty(getIntent().getExtras().getString(IntentConstant.CART_SUBSTITUTION))) {
            return new Gson().fromJson(getIntent().getExtras().getString(IntentConstant.CART_SUBSTITUTION), CartSubstitutionResponseModel.class);
        }
        return null;
    }

    @Override
    public void setAdapter(AlternateCartProductListAdapter adapter) {
        binding.rvProductList.setAdapter(adapter);
        binding.rvProductList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void changeHeadingProperties(boolean isNewSuggestionSelected) {
        binding.tvNewCartSuggestionHeading.setBackground(ContextCompat.getDrawable(this, isNewSuggestionSelected ? R.drawable.white_background_with_top_round_corner : R.drawable.grey_background_with_top_round_corner));
        binding.tvNewCartSuggestionHeading.setTextColor(ContextCompat.getColor(this, isNewSuggestionSelected ? R.color.colorDeepAqua : R.color.colorLightBlueGrey));
        binding.tvOriginalCartHeading.setBackground(ContextCompat.getDrawable(this, !isNewSuggestionSelected ? R.drawable.white_background_with_top_round_corner : R.drawable.grey_background_with_top_round_corner));
        binding.tvOriginalCartHeading.setTextColor(ContextCompat.getColor(this, !isNewSuggestionSelected ? R.color.colorDeepAqua : R.color.colorLightBlueGrey));
    }

    @Override
    public Context getContext() {
        return AlternateCartActivity.this;
    }

    @Override
    public void updateBottomSaveAmountTextAndUpdateCartButton(CartSubstitutionResponseModel cartSubstitutionResponseModel) {
        double originalAndUpdateCartDifference = cartSubstitutionResponseModel.getTotalSavings().doubleValue();
        binding.tvOrinialCartAmount.setText(CommonUtils.getPriceInFormat(cartSubstitutionResponseModel.getOriginalCartTotalValue()));
        binding.tvAlternateCartAmount.setText(CommonUtils.getPriceInFormat(cartSubstitutionResponseModel.getAlternateCartTotalValue()));
        binding.tvAlternateCartSaveAmount.setVisibility(originalAndUpdateCartDifference > 0 ? View.VISIBLE : View.GONE);
        binding.tvAlternateCartSaveAmount.setText(String.format(getString(R.string.text_you_save) + " %s", CommonUtils.getPriceInFormat(originalAndUpdateCartDifference)));
        binding.btnUpdate.setClickable(originalAndUpdateCartDifference > 0);
        binding.btnUpdate.setTextColor(ContextCompat.getColor(this, originalAndUpdateCartDifference > 0 ? R.color.colorPrimary : R.color.colorLightPaleBlueGrey));
        binding.btnUpdate.setBackground(ContextCompat.getDrawable(this, originalAndUpdateCartDifference > 0 ? R.drawable.accent_button : R.drawable.grey_background_button));
    }

    @Override
    public void showSnackBar(String errorMessage) {

    }

    @Override
    public void showLoader() {
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoader() {
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        binding.llParentView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.alternateCartNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.llParentView.setVisibility(!isWebserviceError ? View.VISIBLE : View.GONE);
        binding.alternateCartApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void launchAfterUpdateCartSuccessful(List<Alternative> outOfStockProductList, List<Alternative> maxLimitReachedProductList, MStarBasicResponseTemplateModel updateCartDetails) {
        GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_ALT_CART, GoogleAnalyticsHelper.EVENT_ACTION_ALTERNATE_CART_UPDATE_CART, GoogleAnalyticsHelper.POST_SCREEN_ALTERNATE_CART);
        Intent intent = new Intent();
        //todo
        // intent.putExtra(IntentConstant.OUT_OF_STOCK_LIST, outOfStockProductList);
        // intent.putExtra(IntentConstant.MAX_LIMIT_REACH_LIST, maxLimitReachedProductList);
        intent.putExtra(IntentConstant.CART_DETAILS, new Gson().toJson(updateCartDetails));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finish();
    }
}
