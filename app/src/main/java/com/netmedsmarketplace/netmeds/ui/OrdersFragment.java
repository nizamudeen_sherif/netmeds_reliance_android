package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.OrderAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentOrderBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OrdersViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MstarOrders;
import com.nms.netmeds.base.model.MstarStatusItem;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.payment.ui.PaymentActivity;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrdersFragment extends BaseViewModelFragment<OrdersViewModel> implements OrdersViewModel.OrdersListener, OrderFilterFragmentDialog.OrderFilterFragmentDialogListener {

    private OrdersViewModel viewModel;
    private FragmentOrderBinding binding;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order, container, false);
        onCreateViewModel();
        if (getActivity() != null)
            ((NavigationActivity) getActivity()).setSupportActionBar(binding.toolBarCustomLayout.toolbar);
        initToolBar();
        return binding.getRoot();
    }

    private void initToolBar() {
        binding.toolBarCustomLayout.collapsingToolbar.setTitle(getActivity().getString(R.string.text_my_orders));
        binding.toolBarCustomLayout.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.toolBarCustomLayout.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.toolBarCustomLayout.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(getActivity(), "font/Lato-Bold.ttf"));
        binding.toolBarCustomLayout.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(getActivity(), "font/Lato-Bold.ttf"));
    }

    @Override
    protected OrdersViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(OrdersViewModel.class);
        viewModel.init(BasePreference.getInstance(getContext()), getString(R.string.text_order_recent), this);
        viewModel.getMutableOrderListData().observe(this, new OrderListResponseObserver());
        viewModel.getIsPrimeUserLiveData().observe(this, new CheckPrimeUserResponseObserver());
        onRetry(viewModel, binding.getRoot());
        viewModel.fetchOrderListFromServer();
        return viewModel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstant.CANCEL_ORDER_RQUEST_CODE) {
            if (resultCode == IntentConstant.CANCEL_ORDER_RESULT_OK) {
                viewModel.fetchOrderListFromServer();
            }
        }
    }

    @Override
    public void showLoader() {
        showHorizontalProgressBar(false, binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void hideLoader() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showHorizontalProgressBar(true, binding.toolBarCustomLayout.progressBar);
            }
        });
    }

    @Override
    public void onLaunchOrderDetails(MstarOrders order) {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getActivity(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_3, GoogleAnalyticsHelper.EVENT_ACTION_VIEW_DETAILS, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER);
        Intent intent = new Intent(getActivity(), ViewOrderDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentConstant.ORDER_ID, order.getOrderId());
        intent.putExtra(IntentConstant.IS_PRIME_USER, viewModel.isPrimeMember);
        startActivityForResult(intent, IntentConstant.CANCEL_ORDER_RQUEST_CODE);
    }

    @Override
    public void onLaunchTrackOrder(String orderId, MstarOrders order) {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(getActivity(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_3, GoogleAnalyticsHelper.EVENT_ACTION_TRACK_ORDER, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER);
        Intent intent = new Intent(getActivity(), TrackOrderDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentConstant.ORDER_ID, orderId);
        intent.putExtra(IntentConstant.ORDER_DETAILS, order);
        startActivityForResult(intent, IntentConstant.CANCEL_ORDER_RQUEST_CODE);
    }

    @Override
    public void redirectToPayment() {
        if (getActivity() != null) {
            /*Google Analytics Event*/
            GoogleAnalyticsHelper.getInstance().postEvent(getActivity(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_3, GoogleAnalyticsHelper.EVENT_ACTION_PAY_NOW, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER);
            Intent intent = new Intent(getContext(), PaymentActivity.class);
            intent.putExtra(PaymentIntentConstant.IS_FROM_PAYMENT, true);
            intent.putExtra(PaymentIntentConstant.FROM_PAYMENT_FAILURE, false);
            intent.putExtra(IntentConstant.IS_M2_ORDER, M2Helper.getInstance().isM2Order());
            intent.putExtra(IntentConstant.IS_MSTAR_TEMP_ORDER, PaymentHelper.isIsTempCart());
            startActivity(intent);
        }
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> addedProductsResultMap, String orderId) {
        M2Helper.getInstance().clearData();
        Intent intent = new Intent(getActivity(), MStarCartActivity.class);
        if (addedProductsResultMap != null && !addedProductsResultMap.isEmpty()) {
            RefillHelper.getOutOfStockProductsMap().putAll(addedProductsResultMap);
        }
        intent.putExtra(IntentConstant.ORDER_ID, orderId);
        startActivity(intent);
    }

    @Override
    public void launchFilterOptionForOrder(String orderFilterStatus, List<MstarStatusItem> statusList) {
        if (getActivity() != null) {
            OrderFilterFragmentDialog orderFilterFragmentDialog = new OrderFilterFragmentDialog(this, orderFilterStatus, statusList);
            getActivity().getSupportFragmentManager().beginTransaction().add(orderFilterFragmentDialog, "orderFilterFragmentDialog").commitAllowingStateLoss();
        }
    }

    @Override
    public void launchM2UploadPrescription() {
        startActivity(new Intent(getActivity(), M2AttachPrescriptionActivity.class));
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void onNavigationPrime() {
        if (getActivity() != null)
            LaunchIntentManager.routeToActivity(getActivity().getString(R.string.route_prime_membership_activity), getActivity());
    }

    @Override
    public void initiateSwipeToRefresh() {
        binding.swipeRefresh.setSwipeableChildren(R.id.order_list);
        binding.swipeRefresh.setColorSchemeColors(getContext().getResources().getColor(R.color.colorMediumPink));
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                viewModel.startAPICommunication();
            }
        });
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(getContext());
    }

    @Override
    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(binding.orderList, CommonUtils.fromHtml(message), 1000 * 10);
        View snackBarView = snackbar.getView(); //get your snackbar view
        snackBarView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorDarkBlueGrey));
        TextView textView = snackBarView.findViewById(R.id.snackbar_text); //Get reference of snackbar textview
        textView.setMaxLines(5);
        snackbar.setActionTextColor(ContextCompat.getColor(getActivity(), R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void setOrderContainerVisibility(int visibility) {
        binding.orderContainer.setVisibility(visibility);
    }

    @Override
    public void setFilterLayoutVisibility(int visibility) {
        binding.llFilterView.setVisibility(visibility);
    }

    @Override
    public void setPrimeVisibility(int visibility) {
        binding.primeBanner.setVisibility(visibility);
    }

    @Override
    public void enableEmptyView(boolean enable) {
        binding.emptyCartView.setVisibility(enable ? View.VISIBLE : View.GONE);
        binding.orderContainer.setVisibility(enable ? View.GONE : View.VISIBLE);
        enableShimmer(false);
    }

    @Override
    public void setEmptyCartViewVisibility(int visibility) {
        binding.emptyCartView.setVisibility(visibility);
    }

    @Override
    public void enableShimmer(boolean isShimmerEnable) {
        if (View.VISIBLE != binding.emptyCartView.getVisibility()) {
            if (isShimmerEnable) {
                binding.orderContainer.setVisibility(View.GONE);
                binding.shimmerView.setVisibility(View.VISIBLE);
                binding.shimmerView.startShimmerAnimation();
            } else {
                binding.shimmerView.setVisibility(View.GONE);
                binding.shimmerView.stopShimmerAnimation();
                binding.orderContainer.setVisibility(View.VISIBLE);
            }
        } else {
            binding.shimmerView.setVisibility(View.GONE);
            binding.shimmerView.stopShimmerAnimation();
        }
    }

    @Override
    public void setrefreshing(boolean refresh) {
        binding.swipeRefresh.setRefreshing(refresh);
    }

    @Override
    public OrderAdapter initiateOrderList(List<MstarOrders> ordersList, OrderAdapter orderAdapter) {
        if (orderAdapter == null) {
            orderAdapter = new OrderAdapter(getContext(), ordersList, viewModel);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            binding.orderList.setLayoutManager(linearLayoutManager);
            binding.orderList.setAdapter(orderAdapter);
            binding.orderList.addOnScrollListener(viewModel.paginationForOrderList(linearLayoutManager));
        } else {
            orderAdapter.updateOrderList(ordersList);
        }
        return orderAdapter;
    }

    @Override
    public void showNetworkErrorView(boolean isConnected) {
        binding.orderContainer.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.fragmentOrderNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.orderContainer.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.fragmentOrderApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void navigateToAddAddress() {
        startActivity(new Intent(getContext(), AddressActivity.class));
        hideLoader();
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        Intent intent = new Intent(getContext(), MStarCartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putStringArrayListExtra(IntentConstant.INTER_CITY_PRODUCT_LIST, interCityProductList);
        intent.putExtra(IntentConstant.IS_OUT_OF_STOCK, false);
        startActivity(intent);
        hideLoader();
    }


    @Override
    public void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        if (getActivity() != null)
            GoogleAnalyticsHelper.getInstance().postScreen(getActivity(), GoogleAnalyticsHelper.POST_SCREEN_MY_ORDERS);
        PaymentHelper.setIsNMSCashApplied(false);
        PaymentHelper.setAppliedNMSCash(0);
        PaymentHelper.setCouponCode("");
        PaymentHelper.setBoomDate(null);
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        /**TO DO set digitized Presciption ID*/
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
        //PrescriptionHelper.getInstance().setDigitizedPrescriptionRxId(new ArrayList<String>());
    }

    @Override
    public void onApplyFilter(String filterType) {
        viewModel.applyFilter(filterType);
    }

    private class OrderListResponseObserver implements Observer<List<MstarOrders>> {
        @Override
        public void onChanged(@Nullable List<MstarOrders> ordersList) {
            viewModel.initiateOrderFromServer(ordersList);
            binding.setViewModel(viewModel);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_MY_ORDER);
    }

    private class CheckPrimeUserResponseObserver implements Observer<Boolean> {
        @Override
        public void onChanged(@Nullable Boolean isPrimeUSer) {
            viewModel.onPrimeDataAvailable(isPrimeUSer);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        M2Helper.getInstance().clearData();
    }
}
