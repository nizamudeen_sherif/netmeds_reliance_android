package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.M2AttachPrescriptionAdapter;
import com.netmedsmarketplace.netmeds.adpater.MStarReviewPageItemAdapter;
import com.netmedsmarketplace.netmeds.adpater.OrderConfirmationPrescriptionAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityConfirmationBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OrderConfirmationViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.BuildConfig;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarSubmitMethod2;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.payment.ui.OrderPlacedSuccessfullyActivity;
import com.nms.netmeds.payment.ui.PaymentActivity;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderConfirmationActivity extends BaseUploadPrescription<OrderConfirmationViewModel> implements OrderConfirmationViewModel.OrderConfirmationListener,
        M1UploadPrescriptionFragment.UploadPrescriptionListener, PastPrescriptionFragment.SelectedPrescriptionListener, M2AttachPrescriptionAdapter.PrescriptionListener,
        CommonUtils.BitmapImageConversionInterface, OrderConfirmationPrescriptionAdapter.PrescriptionAdapterCallback {

    private OrderConfirmationViewModel confirmationViewModel;
    private ActivityConfirmationBinding confirmationBinding;
    private OrderConfirmationPrescriptionAdapter attachPrescriptionAdapter;
    private boolean enableResume = false;
    private MStarAddressModel addressModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        confirmationBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirmation);
        toolBarSetUp(confirmationBinding.toolBarCustomLayout.toolbar);
        initToolBar();
        onBackListener();
        setDataView(false);
        confirmationBinding.setConfirmationViewModel(onCreateViewModel());
        setTermsAndCondition();
    }

    private void initToolBar() {
        if (getSupportActionBar() != null) {
            if (SubscriptionHelper.getInstance().isSubscriptionFlag() || getIntent().getBooleanExtra(SubscriptionIntentConstant.REFILL_INITIATE_FROM_BANNER, false)) {
                initToolBar(confirmationBinding.toolBarCustomLayout.collapsingToolbar, getString(R.string.text_subscription_review));
                confirmationBinding.proceedBtn.setText(SubscriptionHelper.getInstance().isThisIssueOrder() ? R.string.text_pay : R.string.text_place_order);
                confirmationBinding.subscriptionReview.setVisibility(View.VISIBLE);
            } else {
                initToolBar(confirmationBinding.toolBarCustomLayout.collapsingToolbar, getString(R.string.text_order_review));
                confirmationBinding.proceedBtn.setText(R.string.text_proceed);
                confirmationBinding.orderConfirmationOrderInfo.setText(R.string.text_order_confirmation_title);
                confirmationBinding.oderDescription.setText(R.string.text_our_pharmacist_doctor_will_call_you_to_confirm_the_medicines);
            }
        }

    }

    @Override
    protected OrderConfirmationViewModel onCreateViewModel() {
        confirmationViewModel = ViewModelProviders.of(this).get(OrderConfirmationViewModel.class);
        setAddress();
        confirmationViewModel.init(BasePreference.getInstance(this), addressModel != null ? addressModel : new MStarAddressModel(), this, getIntent());
        onRetry(confirmationViewModel);
        return confirmationViewModel;
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false, confirmationBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true, confirmationBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmPlaceOrder(MstarSubmitMethod2 mstarSubmitMethod2) {
        MStarBasicResponseTemplateModel responseTemplateModel = new MStarBasicResponseTemplateModel();
        MstarBasicResponseResultTemplateModel mstarBasicResponseResultTemplateModel = new MstarBasicResponseResultTemplateModel();
        responseTemplateModel.setResult(mstarBasicResponseResultTemplateModel);
        responseTemplateModel.getResult().setDisplayStatus(mstarSubmitMethod2.getDisplay_status());
        responseTemplateModel.getResult().setDescription(mstarSubmitMethod2.getMessage());
        Intent intent = new Intent(this, OrderPlacedSuccessfullyActivity.class);
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, PaymentHelper.getSingle_address());
        intent.putExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG, PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M2);
        intent.putExtra(PaymentIntentConstant.M2_FLAG, M2Helper.getInstance().isM2Order());
        intent.putExtra(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE, new Gson().toJson(responseTemplateModel));
        intent.putExtra(PaymentIntentConstant.M2_FLAG, true);
        startActivity(intent);
    }

    @Override
    public void vmProceedFromConfirmation() {
        /*WebEngage checkout Continued Event*/
        confirmationViewModel.onCheckoutContinueEvent();
        Intent intent = new Intent(getContext(), PaymentActivity.class);
        intent.putExtra(PaymentIntentConstant.IS_FROM_PAYMENT, true);
        intent.putExtra(PaymentIntentConstant.FROM_PAYMENT_FAILURE, false);
        intent.putExtra(IntentConstant.IS_M2_ORDER, M2Helper.getInstance().isM2Order());
        startActivity(intent);
        enableOrDisableProceedButton(true);
    }

    @Override
    public void vmShowAlert(String alertMessage) {
        Snackbar snackbar = Snackbar.make(confirmationBinding.paymentDetail, alertMessage, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.text_update), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        TextView textView = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setSingleLine(false);
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Snackbar snackbar = Snackbar.make(confirmationBinding.paymentDetail, errorMessage, 1000 * 10);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void executeBinding() {
        confirmationBinding.setConfirmationViewModel(confirmationViewModel);
    }


    @Override
    public void vmNavigateToHome() {
        Intent intent = new Intent(OrderConfirmationActivity.this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void productUnAvailabilityAlert(boolean isOutOfStockAvailableInCart, ArrayList<String> interCityProductList) {
        if (SubscriptionHelper.getInstance().isCreateNewFillFlag() && !(confirmationViewModel.isOrderAmountAboveMinimumPrice() && confirmationViewModel.isOrderAmountNotExceedMaxPrice())) {
            navigateToCart(false, interCityProductList);
        } else if (isOutOfStockAvailableInCart || !interCityProductList.isEmpty()) {
            navigateToCart(isOutOfStockAvailableInCart, interCityProductList);
        } else {
            //due to pin code error (Pincode blocked or Unsupported pincode or cod not supported pin code ) need to change pincode in address.
            changeAddress();
        }

    }

    @Override
    public void clickOnPrescriptionToPreview(Bitmap bitmap, int pos) {
        launchPreviewPrescription(bitmap);
    }

    @Override
    public void removePrescription(int position, ArrayList<MStarUploadPrescription> prescriptionArrayList) {
    }

    @Override
    public void addPrescription() {
        M1UploadPrescriptionFragment uploadPrescriptionFragment = new M1UploadPrescriptionFragment(this);
        getSupportFragmentManager().beginTransaction().add(uploadPrescriptionFragment, "UploadPrescriptionFragment").commitAllowingStateLoss();
    }

    @Override
    public void sellerDetailDialog(String sellerName, String sellerAddress) {
        FreeDoctorConsultationFragment doctorConsultationFragment = new FreeDoctorConsultationFragment(false, sellerName, this, sellerAddress);
        getSupportFragmentManager().beginTransaction().add(doctorConsultationFragment, "FreeDoctorConsultationFragment").commitAllowingStateLoss();
    }

    @Override
    public void vmOderSuccessFullyActivity(String successData, boolean isFromRefill) {
        Intent intent = new Intent(this, OrderPlacedSuccessfullyActivity.class);
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, PaymentHelper.getSingle_address());
        intent.putExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG, isFromRefill ? SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_REFILL : SubscriptionIntentConstant.PLACED_SUCCESSFUL_FROM_CREATE_NEW_SUB_ORDER);
        intent.putExtra(SubscriptionIntentConstant.REFILL_SUBSCRIPTION_RESPONSE, successData);
        intent.putExtra(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE, successData);
        startActivity(intent);
    }

    @Override
    public void updateCartPaymentDetails(MStarCartDetails cartDetails, boolean isFromRefillSubscription) {
        if (cartDetails != null) {
            PaymentHelper.setTotalAmount(cartDetails.getNetPayableAmount().doubleValue());
            BigDecimal walletAndSuperCashAmount = cartDetails.getUsedWalletAmount() != null ? cartDetails.getUsedWalletAmount().getTotalWallet() : BigDecimal.ZERO;
            confirmationBinding.mrpTotal.setText(CommonUtils.getPriceInFormat(cartDetails.getSubTotalAmount()));
            confirmationBinding.tvDeliveryCharges.setText(CommonUtils.getPriceInFormat(cartDetails.getShippingChargesFinal()));
            //Strike discount
            confirmationBinding.tvStrikeDeliveryCharges.setVisibility(cartDetails.getShippingChargesFinal().compareTo(BigDecimal.ZERO) == 0 && cartDetails.getShippingChargesOriginal().compareTo(BigDecimal.ZERO) != 0 ? View.VISIBLE : View.GONE);
            confirmationBinding.tvStrikeDeliveryCharges.setPaintFlags(confirmationBinding.tvStrikeDeliveryCharges.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            confirmationBinding.tvStrikeDeliveryCharges.setText(CommonUtils.getPriceInFormat(cartDetails.getShippingChargesOriginal()));
            confirmationBinding.additionalDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(confirmationViewModel.getIndividualProductDiscount()));
            confirmationBinding.netmedsDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(confirmationViewModel.getCouponDiscount()));
            confirmationBinding.nmsWallet.setText(CommonUtils.getPriceInFormatWithHyphen(walletAndSuperCashAmount));
            confirmationBinding.voucherDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(cartDetails.getUsedVoucherAmount()));
            confirmationBinding.totalAmount.setText(isFromRefillSubscription ? getString(R.string.text_to_be_decided) : CommonUtils.getPriceInFormat(cartDetails.getNetPayableAmount()));
            confirmationBinding.tvOrderAmount.setText(CommonUtils.getPriceInFormat(cartDetails.getNetPayableAmount()));
            confirmationBinding.totalSaving.setText(CommonUtils.getPriceInFormat(cartDetails.getTotalSavings()));
            confirmationBinding.llAdditionDiscount.setVisibility(confirmationViewModel.getIndividualProductDiscount().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
            confirmationBinding.netmedsDiscountLayout.setVisibility(confirmationViewModel.getCouponDiscount().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
            confirmationBinding.nmsWalletLayout.setVisibility(walletAndSuperCashAmount.compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
            confirmationBinding.voucherLayout.setVisibility(cartDetails.getUsedVoucherAmount().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
            confirmationBinding.llTotalSaving.setVisibility(cartDetails.getTotalSavings().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void setEmptyView(boolean isEmpty) {
        confirmationBinding.orderConfirmationCartView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        if (!isEmpty)
            confirmationBinding.llAddressView.setVisibility(View.VISIBLE);
        confirmationBinding.emptyCartView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setcartItemsAdapter(MStarReviewPageItemAdapter itemAdapter) {
        confirmationBinding.productList.setLayoutManager(new LinearLayoutManager(this));
        confirmationBinding.productList.setNestedScrollingEnabled(false);
        confirmationBinding.productList.setAdapter(itemAdapter);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void enableOrDisableProceedButton(boolean isEnableProceedButton) {
        confirmationBinding.proceedBtn.setBackground(getContext().getResources().getDrawable(isEnableProceedButton ? R.drawable.accent_button : R.drawable.grey_background_button));
        confirmationBinding.proceedBtn.setTextColor(ContextCompat.getColor(getContext(), isEnableProceedButton ? R.color.colorPrimary : R.color.colorLightPaleBlueGrey));
        confirmationBinding.proceedBtn.setEnabled(isEnableProceedButton);
        confirmationBinding.proceedBtn.setClickable(isEnableProceedButton);
    }

    @Override
    public void enableOrDisableM2ProceedButton(boolean isEnableProceedButton) {
        confirmationBinding.placeOrder.setBackground(getContext().getResources().getDrawable(isEnableProceedButton ? R.drawable.accent_button : R.drawable.grey_background_button));
        confirmationBinding.placeOrder.setTextColor(ContextCompat.getColor(getContext(), isEnableProceedButton ? R.color.colorPrimary : R.color.colorLightPaleBlueGrey));
        confirmationBinding.placeOrder.setEnabled(isEnableProceedButton);
        confirmationBinding.placeOrder.setClickable(isEnableProceedButton);
        confirmationViewModel.checkRxInfoViewVisibilityForOnyPrime();
    }


    @Override
    public void setPrescriptionViewVisibility(boolean visibility) {
        confirmationBinding.prescriptionList.setVisibility(visibility ? View.VISIBLE : View.GONE);
        confirmationBinding.subPrescriptionList.setVisibility(visibility ? View.VISIBLE : View.GONE);

    }

    @Override
    public void checkForSubscriptionProceed(boolean eligibleForCod, boolean orderAmountAboveMinimumPrice, boolean orderAmountNotExceedMaxPrice) {
        if (SubscriptionHelper.getInstance().isCreateNewFillFlag() || SubscriptionHelper.getInstance().isSubscriptionFlag() && !SubscriptionHelper.getInstance().isPayNowSubscription()) {
            if (!eligibleForCod) {
                if (!orderAmountAboveMinimumPrice) {
                    confirmationBinding.stockInfoTitle.setText(getContext().getResources().getString(R.string.text_order_cant_placed));
                    confirmationBinding.stockInfoDescription.setText(getContext().getString(R.string.text_min_cod_error));
                    confirmationBinding.actionBtn.setVisibility(SubscriptionHelper.getInstance().isSubscriptionFlag() ? View.VISIBLE : View.GONE);
                    changePlaceOrderButtonProperties(false);
                    return;
                }
                if (!orderAmountNotExceedMaxPrice) {
                    confirmationBinding.stockInfoTitle.setText(getContext().getResources().getString(R.string.text_order_cant_placed));
                    confirmationBinding.stockInfoDescription.setText(getContext().getString(R.string.text_max_cod_error));
                    confirmationBinding.actionBtn.setVisibility(SubscriptionHelper.getInstance().isSubscriptionFlag() ? View.VISIBLE : View.GONE);
                    changePlaceOrderButtonProperties(false);
                    return;
                }
                scrollToTop();
            }
        }
    }

    private void changePlaceOrderButtonProperties(boolean isProceedFurther) {
        rxInfoVisibility(View.GONE);
        confirmationBinding.orderConfirmationStockInfo.setVisibility(isProceedFurther ? View.GONE : View.VISIBLE);
        enableOrDisableProceedButton(isProceedFurther);
    }

    @Override
    public void setAlertMessage(String title, String description, String buttonText) {
        confirmationBinding.stockInfoTitle.setText(title);
        confirmationBinding.stockInfoDescription.setText(description);
        confirmationBinding.actionBtn.setText(buttonText);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        confirmationBinding.orderConfirmationCartView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        confirmationBinding.confirmationNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
        if (!isConnected)
            confirmationBinding.llAddressView.setVisibility(View.GONE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        confirmationBinding.orderConfirmationCartView.setVisibility(!isWebserviceError ? View.VISIBLE : View.GONE);
        if (isWebserviceError)
            confirmationBinding.llAddressView.setVisibility(View.GONE);
        confirmationBinding.confirmationApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void rxInfoVisibility(int visibility) {
        confirmationBinding.rxInfoView.setVisibility((visibility == View.VISIBLE && PaymentHelper.isExternalDoctor()) || confirmationViewModel.isFromRefillSubscription() ? View.VISIBLE : View.GONE);
        confirmationViewModel.checkRxInfoViewVisibilityForOnyPrime();
        if (confirmationViewModel.isFromRefillSubscription()) {
            confirmationBinding.orderConfirmationOrderInfo.setText(getContext().getString(R.string.text_order_info));
            confirmationBinding.oderDescription.setText(getContext().getString(R.string.text_m3_order_info_desc));
        }
    }

    @Override
    public void stockInfoVisibility(int visibility) {
        confirmationBinding.orderConfirmationStockInfo.setVisibility(visibility);
        enableOrDisableProceedButton(!(visibility == View.VISIBLE));
        if (visibility == View.VISIBLE) {
            confirmationBinding.proceedBtn.setBackground(getContext().getResources().getDrawable(R.drawable.grey_background_button));
        } else {
            confirmationBinding.proceedBtn.setBackground(getContext().getResources().getDrawable(R.drawable.accent_button));
        }
    }

    @Override
    public void codStatusUndeliveredPinCodeView(String errorMessage) {
        stockInfoVisibility(View.VISIBLE);
        rxInfoVisibility(View.GONE);
        confirmationBinding.stockInfoTitle.setText(getString(R.string.text_undeliverable));
        confirmationBinding.orderConfirmationStockInfo.setCardBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        confirmationBinding.stockInfoDescription.setText(CommonUtils.fromHtml(errorMessage));
        confirmationBinding.actionBtn.setTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        confirmationBinding.actionBtn.setText(getString(R.string.text_take_Action));
    }

    @Override
    public void rxInfoSetVisibility(int view) {
        confirmationBinding.rxInfoView.setVisibility(view);
    }

    @Override
    public int getRxInfoVisibility() {
        return confirmationBinding.rxInfoView.getVisibility();
    }

    @Override
    public ActivityConfirmationBinding getBinding() {
        return confirmationBinding;
    }

    @Override
    public void enableShimmer(boolean isEnable) {
        confirmationBinding.proceedBtn.setVisibility(isEnable ? View.GONE : View.VISIBLE);
    }

    private void scrollToTop() {
        confirmationBinding.orderConfirmationCartView.post(new Runnable() {
            @Override
            public void run() {
                confirmationBinding.orderConfirmationCartView.fullScroll(confirmationBinding.orderConfirmationCartView.FOCUS_UP);
            }
        });
    }

    @Override
    public void pastPrescriptionView(GetPastPrescriptionResult result) {
        PastPrescriptionFragment pastPrescriptionFragment = new PastPrescriptionFragment(this, this, result);
        getSupportFragmentManager().beginTransaction().add(pastPrescriptionFragment, "PastPrescriptionFragment").commitAllowingStateLoss();
    }

    @Override
    public void setUploadedPrescriptionView(ArrayList<String> prescription) {
        initAdapter(prescription);
    }

    private void initAdapter(ArrayList<String> prescriptionList) {
        attachPrescriptionAdapter = new OrderConfirmationPrescriptionAdapter(prescriptionList, this);
        confirmationBinding.prescriptionList.setLayoutManager(setLayoutManager());
        confirmationBinding.prescriptionList.setAdapter(attachPrescriptionAdapter);
        confirmationBinding.subPrescriptionList.setLayoutManager(setLayoutManager());
        confirmationBinding.subPrescriptionList.setAdapter(attachPrescriptionAdapter);
    }

    private LinearLayoutManager setLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackMenuButton();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        if (confirmationViewModel != null && !confirmationViewModel.isCartProductEmpty()) {
            this.finish();
        } else {
            vmNavigateToHome();
        }
    }

    private void onBackMenuButton() {
        if (confirmationViewModel != null && !confirmationViewModel.isCartProductEmpty()) {
            this.finish();
        } else {
            vmNavigateToHome();
        }
    }

    @Override
    public void initCameraView() {
        if (checkCameraPermission()) {
            initCameraIntent();
        } else requestPermission(PermissionConstants.CAMERA_PERMISSION);
    }

    private void initCameraIntent() {
        launchCameraIntent(getContentUri());
    }

    private Uri getContentUri() {
        File photo = new File(FileUtils.getTempDirectoryPath(this), FileUtils.TEMP_DIRECTORY_PATH);
        return FileProvider.getUriForFile(this, String.format("%s%s", BuildConfig.APPLICATION_ID, FileUtils.PROVIDER), photo);
    }

    @Override
    public void initGalleryView() {
        if (checkGalleryPermission()) {
            launchGalleryIntent();
        } else
            requestPermission(PermissionConstants.GALLERY_PERMISSION);
    }

    @Override
    public void initPastPrescription() {
        confirmationViewModel.getPastPrescription();
    }

    @Override
    public void selectedPrescription(List<MStarUploadPrescription> prescription) {
        ArrayList<Bitmap> prescriptionBitmapList = new ArrayList<>();
        ArrayList<String> prescriptionList = new ArrayList<>();
        for (MStarUploadPrescription list : prescription) {
            if (list.isChecked()) {
                prescriptionBitmapList.add(list.getBitmapImage());
                prescriptionList.add(list.getUploadedPrescriptionId());
            }
        }
        confirmationViewModel.setPastPrescription(prescriptionBitmapList, prescriptionList);
    }

    @Override
    public void launchPreviewPrescription(Object previewImage) {
        if (getSupportFragmentManager().findFragmentByTag("ImagePreviewDialog") == null) {
            ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(previewImage);
            getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitAllowingStateLoss();
        }
    }

    private void navigateToCart(boolean isOutOfStock, ArrayList<String> interCityProductList) {
        Intent intent = new Intent(this, MStarCartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putStringArrayListExtra(IntentConstant.INTER_CITY_PRODUCT_LIST, interCityProductList);
        intent.putExtra(IntentConstant.IS_OUT_OF_STOCK, isOutOfStock);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAddress();
        /*Facebook Pixel Checkout Event*/
        FacebookPixelHelper.getInstance().logCheckoutEvent(this, FacebookPixelHelper.EVENT_PARAM_CHECKOUT_ADDRESS, "");
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, M2Helper.getInstance().isM2Order() ? GoogleAnalyticsHelper.POST_SCREEN_ORDER_REVIEW_M2 : GoogleAnalyticsHelper.POST_SCREEN_ORDER_REVIEW);
        confirmationBinding.setConfirmationViewModel(onCreateViewModel());
        if (!enableResume) {
            if (PaymentHelper.isReloadCart() || (PaymentHelper.isNMSCashApplied() || PaymentHelper.isVoucherApplied()) || PaymentHelper.isReloadTotalInformation()) {
                confirmationViewModel.fetchCartDetailsForReview();
            }
        } else enableResume = false;

    }

    @Override
    public void showProgressBar() {
        showHorizontalProgressBar(false, confirmationBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void hideProgressBar() {
        showHorizontalProgressBar(true, confirmationBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void changeAddress() {
        startActivity(new Intent(this, AddressActivity.class));
    }

    private void onBackListener() {
        AddressActivity.setAddressBackListener(new AddressActivity.AddressBackListener() {
            @Override
            public void onBackAddress() {
                OrderConfirmationActivity.this.finish();
            }
        });
        AddOrUpdateAddressActivity.setUpdateAddressListener(new AddOrUpdateAddressActivity.updateAddressListener() {
            @Override
            public void onBackPress() {
                OrderConfirmationActivity.this.finish();
            }
        });
    }

    @Override
    public void setBitmapImageAdapter() {

    }

    @Override
    public void previewPrescription(String rxUrl) {
        if (getSupportFragmentManager().findFragmentByTag("ImagePreviewDialog") == null) {
            ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(rxUrl);
            getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitAllowingStateLoss();
        }
    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            navigateToCart(false, new ArrayList<String>());
        }
    }

    private void setAddress() {
        if (!TextUtils.isEmpty(PaymentHelper.getSingle_address())) {
            addressModel = new Gson().fromJson(PaymentHelper.getSingle_address(), MStarAddressModel.class);
            confirmationBinding.tvDeliverName.setText(addressModel.getFirstname() + " " + addressModel.getLastname());
            confirmationBinding.tvAddress.setText(CommonUtils.getAddressFromObject(addressModel));
        }
    }

    @Override
    public void setDataView(boolean doShow) {
        confirmationBinding.orderConfirmationCartView.setVisibility(doShow ? View.VISIBLE : View.GONE);

    }

    @Override
    public void showPaymentDetails(boolean isVisible) {
        confirmationBinding.paymentDetail.setVisibility(isVisible ? View.GONE : View.VISIBLE);
        confirmationBinding.m2PaymentDetails.m2PaymentLayout.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showSubscriptionView(boolean isVisible) {
        confirmationBinding.subscriptionReview.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPrescriptionLayout(boolean isPrescriptionUploaded) {
        confirmationBinding.prescriptionLayout.setVisibility(isPrescriptionUploaded ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showOffersViewLayout(boolean couponApplied) {
        confirmationBinding.offersView.setVisibility(couponApplied ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showSubPrescLayout(boolean prescriptionUploadedForSub) {
        confirmationBinding.subPrescriptionLayout.setVisibility(prescriptionUploadedForSub ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDisclaimerText(boolean isVisible) {
        confirmationBinding.disclaimerText.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showPLaceOrderLayout(boolean isVisible) {
        confirmationBinding.placeOrderLayout.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showTotalAmountLayout(boolean isVisible) {
        confirmationBinding.orderConfirmationTotalAmountLayout.setVisibility(isVisible ? View.GONE : View.VISIBLE);
        confirmationBinding.totalAmountTextView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showProductListView(boolean isVisible) {
        confirmationBinding.orderConfirmationProductView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEmergencyMessageVisibility(boolean emergencyMessageVisible) {
        confirmationBinding.cvEmergencyMessage.setVisibility(emergencyMessageVisible ? View.VISIBLE : View.GONE);
    }

    private void setTermsAndCondition() {
        String termsAndCondition = getString(R.string.text_order_review_terms_and_condition);
        int startPosition = getOrderReviewTermsAndCondition().length() + 1;
        String combinedText = getOrderReviewTermsAndCondition() + " " + termsAndCondition;
        int endPosition = combinedText.length();
        final SpannableString clickableText = new SpannableString(combinedText);
        clickableText.setSpan(getSpanClick(), startPosition, endPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickableText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorMediumPink)), startPosition, endPosition, 0);
        confirmationBinding.disclaimerText.setText(clickableText);
        confirmationBinding.disclaimerText.setMovementMethod(LinkMovementMethod.getInstance());
        confirmationBinding.disclaimerText.setHighlightColor(Color.TRANSPARENT);
    }

    private ClickableSpan getSpanClick() {
        return new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Intent intent = new Intent();
                intent.putExtra("WEB_PAGE_URL", ConfigMap.getInstance().getProperty(ConfigConstant.TERMS_CONDITION));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                LaunchIntentManager.routeToActivity(getString(com.nms.netmeds.base.R.string.route_netmeds_web_view), intent, OrderConfirmationActivity.this);
            }
        };
    }

    public String getOrderReviewTermsAndCondition() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(this).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getOrderReviewTermsAndCondition()) ? configurationResponse.getResult().getConfigDetails().getOrderReviewTermsAndCondition() : "";
    }
}
