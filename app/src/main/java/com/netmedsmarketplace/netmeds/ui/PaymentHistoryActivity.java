package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.amazon.identity.auth.device.AuthError;
import com.amazon.identity.auth.device.api.Listener;
import com.amazon.identity.auth.device.api.authorization.AuthorizationManager;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityPaymentHistoryBinding;
import com.netmedsmarketplace.netmeds.viewmodel.PaymentHistoryViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.LinkedWallet;
import com.nms.netmeds.base.model.MStarLinkedPaymentResponse;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;

public class PaymentHistoryActivity extends BaseViewModelActivity<PaymentHistoryViewModel> implements PaymentHistoryViewModel.PaymentHistoryListener, DeleteCardConfirmDialog.DeleteCardConfirmDialogListener {

    private PaymentHistoryViewModel viewModel;
    private ActivityPaymentHistoryBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_history);
        onCreateViewModel();
        toolBarSetUp(binding.toolbar);
        initToolBar();
    }

    @Override
    protected PaymentHistoryViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(PaymentHistoryViewModel.class);
        viewModel.init(binding, this);
        viewModel.getLinkedPaymentMutableLiveData().observe(this, new LinkedPaymentObserver());
        onRetry(viewModel);
        return viewModel;
    }

    private void initToolBar() {
        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(getString(R.string.text_payments_history_title));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoader() {
        showProgress(this);
    }

    @Override
    public void dismissLoader() {
        dismissProgress();
    }

    @Override
    public void showSnackBar(String message) {
        SnackBarHelper.snackBarCallBack(binding.contentView, this, message);
    }

    @Override
    public void onDeLinkOfJustPayWallet(Bundle deLinkBundle) {
        in.juspay.godel.PaymentActivity.preFetch(this, deLinkBundle.getString(in.juspay.godel.core.PaymentConstants.CLIENT_ID));
        Intent intent = new Intent(this, in.juspay.godel.PaymentActivity.class);
        intent.putExtras(deLinkBundle);
        startActivityForResult(intent, PaymentIntentConstant.DE_LINK_WALLET_JUST_PAY);
        dismissProgress();
    }

    @Override
    public void onDeLinkAmazonPay() {
        AuthorizationManager.signOut(this, new Listener<Void, AuthError>() {
            @Override
            public void onSuccess(Void aVoid) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewModel.deLinkUpdate(RESULT_OK, true);
                    }
                });
            }

            @Override
            public void onError(AuthError authError) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewModel.deLinkUpdate(RESULT_CANCELED, true);
                    }
                });
            }
        });
    }

    @Override
    public void onDeLinkPayTM(LinkedWallet wallet) {
        viewModel.deLinkPayTm(wallet);
    }

    @Override
    public void showDeleteSavedCardsAlert(String value, String clickFrom, Object obj) {
        DeleteCardConfirmDialog deleteCardConfirmDialog = new DeleteCardConfirmDialog(value, clickFrom, obj);
        getSupportFragmentManager().beginTransaction().add(deleteCardConfirmDialog, IntentConstant.DELETE_CARD_CONFIRM_DILAOG).commitAllowingStateLoss();
    }

    @Override
    public Context getContext() {
        return PaymentHistoryActivity.this;
    }

    @Override
    public void vmEmptyViewEnableDisable(boolean visible) {
        binding.emptyView.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void vmContentViewEnableDisable(boolean visible) {
        binding.contentView.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void vmWebserviceErrorView(boolean isWebserviceError) {
        binding.contentView.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void vmNoNetworkErrorView(boolean isConnected) {
        binding.contentView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void vmAmazonPayCheckBalanceResponse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewModel.fetchLinkedPaymentsFromServer();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PaymentIntentConstant.DE_LINK_WALLET_JUST_PAY) {
            viewModel.deLinkUpdate(resultCode, false);
        }
    }

    @Override
    public void onProceedDeleteCard(String clickFrom, Object obj) {
        viewModel.proceedDeleteCards(obj, clickFrom);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_PAYMENT_METHODS);
    }

    class LinkedPaymentObserver implements Observer<MStarLinkedPaymentResponse> {

        @Override
        public void onChanged(@Nullable MStarLinkedPaymentResponse paymentHistoryResponse) {
            viewModel.onLinkedPaymentDataAvailable(paymentHistoryResponse);
            binding.setViewModel(viewModel);
        }
    }
}
