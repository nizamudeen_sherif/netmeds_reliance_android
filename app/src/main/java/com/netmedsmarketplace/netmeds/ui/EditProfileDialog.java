package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogEditProfileBinding;
import com.netmedsmarketplace.netmeds.viewmodel.DialogEditProfileViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.ValidationUtils;

import java.util.Calendar;
import java.util.Map;

@SuppressLint("ValidFragment")
public class EditProfileDialog extends BaseBottomSheetFragment implements DialogEditProfileViewModel.DialogEditProfileViewModelListener {

    private final EditProfileDialogListener listener;
    private DialogEditProfileBinding binding;
    private DialogEditProfileViewModel viewModel;
    private Context context;

    public EditProfileDialog(EditProfileDialogListener listener, Context context) {
        this.listener = listener;
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_profile, container, false);
        viewModel = ViewModelProviders.of(this).get(DialogEditProfileViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(this, BasePreference.getInstance(getContext()));
        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    @Override
    public void onClickDismissDialog() {
        dismiss();
    }

    @Override
    public void showLoader() {
        showProgress(getActivity());
    }

    @Override
    public void dismissLoader() {
        dismissProgress();
    }

    @Override
    public void navigateToVerifyMobileNo(String mobileNo) {
        dismiss();
        listener.navigateVerifyMobileNo(mobileNo);
    }

    @Override
    public void networkError(int failedTransactionId, boolean isNetworkFailure, boolean isAPIFailure) {
        dismiss();
        listener.networkError(failedTransactionId, isNetworkFailure, isAPIFailure);
    }

    @Override
    public String getFirstName() {
        return binding.edtFirstName.getText().toString().trim();
    }

    @Override
    public String getLastName() {
        return binding.edtLastName.getText().toString().trim();
    }

    @Override
    public String getGender() {
        return binding.spGender.getSelectedItemPosition() == 1 ? AppConstant.MALE : binding.spGender.getSelectedItemPosition() == 2 ? AppConstant.FEMALE : AppConstant.OTHERS;
    }

    @Override
    public boolean validateChanges() {
        if (TextUtils.isEmpty(binding.edtFirstName.getText().toString().trim())) {
            binding.ilFirstName.setError(getContext().getString(R.string.text_error_enter_first_name));
            return false;
        } else if (binding.edtFirstName.getText().toString().trim().contains("  ")) {
            binding.ilFirstName.setError(getContext().getResources().getString(R.string.text_continuous_whitespace_error));
            return false;
        } else if (binding.edtFirstName.getText().toString().trim().length() < 3) {
            binding.ilFirstName.setError(getContext().getResources().getString(R.string.text_minimum_char));
            return false;
        } else if (TextUtils.isEmpty(binding.edtLastName.getText().toString().trim())) {
            binding.ilLastName.setError(getContext().getString(R.string.text_error_enter_last_name));
            return false;
        } else if (binding.edtLastName.getText().toString().trim().length() < 2) {
            binding.ilLastName.setError(getContext().getResources().getString(R.string.text_min_char_error));
            return false;
        } else if (binding.edtLastName.getText().toString().trim().contains(" ")) {
            binding.ilLastName.setError(getContext().getResources().getString(R.string.text_white_space_error));
            return false;
        } else if (TextUtils.isEmpty(viewModel.getAge())) {
            binding.ilAge.setError(getContext().getString(R.string.text_error_enter_age));
            return false;
        } else if (binding.spGender.getSelectedItemPosition() == 0) {
            Toast.makeText(getContext(), getContext().getString(R.string.text_select_gender), Toast.LENGTH_SHORT).show();
            return false;
        } else
            return ValidationUtils.checkTextInputLayoutPhonenumber(binding.edtMobileNo, true, getContext().getString(R.string.error_mobile), 10, 10, binding.ilMobileNo);
    }

    @Override
    public String getDob() {
        return binding.edtAge.getText().toString().trim();
    }

    @Override
    public void setAgeText(String age) {
        binding.edtAge.setText(age);
    }

    @Override
    public String getMobileNumber() {
        return binding.edtMobileNo.getText().toString().trim();
    }

    @Override
    public void setErrorWatcher() {
        CommonUtils.errorWatcher(binding.edtFirstName, binding.ilFirstName);
        CommonUtils.errorWatcher(binding.edtLastName, binding.ilLastName);
        CommonUtils.errorWatcher(binding.edtAge, binding.ilAge);
        CommonUtils.errorWatcher(binding.edtMailId, binding.ilMailId);
        CommonUtils.errorWatcher(binding.edtMobileNo, binding.ilMobileNo);
    }

    @Override
    public int getGenderSelectedValue() {
        return binding.spGender.getSelectedItemPosition();
    }

    @Override
    public void setSpinnerAdapter(String[] genderArray) {
        if (genderArray == null || genderArray.length == 0) return;
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, genderArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spGender.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public DialogEditProfileBinding getBinding() {
        return binding;
    }

    @Override
    public void agaPickerDialog(DatePickerDialog.OnDateSetListener date, Calendar calendardata) {
        DatePickerDialog datePicker = new DatePickerDialog(context, date, calendardata.get(Calendar.YEAR), calendardata.get(Calendar.MONTH), calendardata.get(Calendar.DAY_OF_MONTH));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -18);
        calendar.add(Calendar.DATE, -1);
        datePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePicker.show();
    }

    @Override
    public String getEmail() {
        return binding.edtMailId.getText().toString().trim();
    }

    @Override
    public void showSnackMessage(String message, boolean isForEditDialog) {
        listener.showMessage(message, isForEditDialog, binding);
    }

    @Override
    public void showTextErrors(Map<String, String> additional_info) {
        for (String field : additional_info.keySet()) {
            switch (field) {
                case "firstname":
                    binding.ilFirstName.setError(additional_info.get(field));
                    break;
                case "email":
                    binding.ilMailId.setError(additional_info.get(field));
                    break;
                case "date_of_birth":
                    binding.ilAge.setError(additional_info.get(field));
                    break;
                case "lastname":
                    binding.ilLastName.setError(additional_info.get(field));
                    break;
                case "gender":
                    listener.showMessage(additional_info.get(field), true, binding);
                    break;
            }
        }
    }

    public interface EditProfileDialogListener {

        void navigateVerifyMobileNo(String mobileNo);

        void networkError(int failedTransactionId, boolean isNetworkFailure, boolean isAPIFailure);

        void showMessage(String message, boolean isForEditDialog, DialogEditProfileBinding binding);
    }
}
