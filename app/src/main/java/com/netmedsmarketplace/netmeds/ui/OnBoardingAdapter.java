package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterOnboardBinding;
import com.nms.netmeds.base.model.OnBoardingResult;

import java.util.List;

public class OnBoardingAdapter extends RecyclerView.Adapter<OnBoardingAdapter.MyViewHolder> {
    private final List<OnBoardingResult> onBoardResults;
    private final Context context;
    private final boolean isProductImageView;
    private final List<String> productImageList;
    private final OnItemClickListener listener;

    OnBoardingAdapter(Context context, List<OnBoardingResult> onBoardData, List<String> productImageList, boolean isProductImageView, OnItemClickListener listener) {
        this.context = context;
        this.onBoardResults = onBoardData;
        this.isProductImageView = isProductImageView;
        this.productImageList = productImageList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterOnboardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_onboard, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (isProductImageView) {
            Glide.with(context).load(productImageList.get(position))
                    .error(Glide.with(holder.binding.boardingImage).load(R.drawable.ic_no_image))
                    .into(holder.binding.boardingImage);
            holder.binding.boardingTitle.setVisibility(View.GONE);
            holder.binding.boardingDescription.setVisibility(View.GONE);
            holder.binding.boardingParentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickListener(productImageList,position);
                }
            });
        } else {
            OnBoardingResult boardingResult = onBoardResults.get(position);
            float density = context.getResources().getDisplayMetrics().density;
            Glide.with(context).load(boardingResult.getOnBoardingImage()).apply(new RequestOptions().override(Math.round(330 * density), Math.round(290 * density)))
                    .into(holder.binding.boardingImage);
            holder.binding.boardingTitle.setText(boardingResult.getOnBoardingTitle());
            holder.binding.boardingDescription.setText(boardingResult.getOnBoardingDescription());
        }
    }

    @Override
    public int getItemCount() {
        if (isProductImageView)
            return productImageList.size();
        else
            return onBoardResults.size();
    }

    public interface OnItemClickListener {
        void onItemClickListener(List<String> urlList, int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private AdapterOnboardBinding binding;

        MyViewHolder(AdapterOnboardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
