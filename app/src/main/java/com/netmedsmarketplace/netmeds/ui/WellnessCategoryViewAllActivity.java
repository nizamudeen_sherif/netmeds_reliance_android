package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.WellnessCategoryViewAllAdapter;
import com.netmedsmarketplace.netmeds.databinding.WellnessViewAllBinding;
import com.netmedsmarketplace.netmeds.viewmodel.WellnessCategoryViewAllViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;

public class WellnessCategoryViewAllActivity extends BaseViewModelActivity<WellnessCategoryViewAllViewModel> implements WellnessCategoryViewAllAdapter.WellnessCategoryAdapterListener, WellnessCategoryViewAllViewModel.CategoryViewAllCallback {

    WellnessViewAllBinding binding;
    WellnessCategoryViewAllViewModel viewModel;
    private String viewAllCodes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.wellness_view_all);
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar,getString(R.string.text_all_categories));
        getIntentData();
        binding.setViewModel(onCreateViewModel());
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(IntentConstant.WELLNESS_VIEW_ALL_CODES))
                viewAllCodes = getIntent().getStringExtra(IntentConstant.WELLNESS_VIEW_ALL_CODES);
        }
    }

    @Override
    protected WellnessCategoryViewAllViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(WellnessCategoryViewAllViewModel.class);
        viewModel.getCategoryMutableLiveData().observe(this, new CategoryObserver());
        viewModel.init(binding, this, this, viewAllCodes);
        return viewModel;
    }

    @Override
    public void navigateCategoryActivity(Integer catID, String catName) {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(IntentConstant.CATEGORY_ID, catID);
        intent.putExtra(IntentConstant.CATEGORY_NAME, catName);
        startActivity(intent);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.wellnessCategoriesView.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.wellnessApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNoNetworkError(boolean isNetworkAvailable) {
        binding.wellnessCategoriesView.setVisibility(isNetworkAvailable ? View.VISIBLE : View.GONE);
        binding.wellnessNetworkErrorView.setVisibility(isNetworkAvailable ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showLoader() {
        showHorizontalProgressBar(false,binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void dismissLoader() {
        showHorizontalProgressBar(true,binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public BasePreference getBasePreference() {
        return BasePreference.getInstance(this);
    }

    private class CategoryObserver implements Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            viewModel.onAvailableDiscount(response);
            binding.setViewModel(viewModel);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }
}
