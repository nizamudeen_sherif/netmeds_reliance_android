package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.PrimeMemberShipAdapter;
import com.netmedsmarketplace.netmeds.adpater.PrimeOffersAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityEliteMemberShipBinding;
import com.netmedsmarketplace.netmeds.viewmodel.PrimeMemberShipViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarPrimeProduct;
import com.nms.netmeds.base.model.MstarPrimeProductResult;
import com.nms.netmeds.base.model.PrimeConfig;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.List;

public class PrimeMemberShipActivity extends BaseViewModelActivity<PrimeMemberShipViewModel> implements PrimeMemberShipViewModel.OnPrimeMembershipListener {
    private PrimeMemberShipViewModel viewModel;
    private ActivityEliteMemberShipBinding binding;
    public static final String KEY_INTENT_FROM_CART = "INTENT_FROM_CART";
    public String intentFrom = "";
    private PrimeMemberShipViewModel.OnPrimeMembershipListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BasePreference.getInstance(this).isGuestCart() && TextUtils.isEmpty(BasePreference.getInstance(this).getMStarSessionId())) {
            navigateToSignIn();
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_elite_member_ship);
        binding.setLifecycleOwner(this);
        listener = this;
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, "");
        viewModel = onCreateViewModel();
    }

    @Override
    protected PrimeMemberShipViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(PrimeMemberShipViewModel.class);
        intentValue();
        viewModel.getCustomerDetailsMutableLiveData().observe(this, new PrimeUserObserver());
        viewModel.getPrimeProductMutableLiveData().observe(this, new PrimeProductObserver());
        viewModel.init(this, BasePreference.getInstance(this));
        binding.setViewModel(viewModel);
        onRetry(viewModel);
        return viewModel;
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra("isFromDeepLink"))
                onDeepLinking();
            else
                onIntent(intent);
        }
    }

    @Override
    public void onShowProgress() {
        showHorizontalProgressBar(false,binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void onDismissProgress() {
        showHorizontalProgressBar(true,binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void onShowMessage(String message) {
        SnackBarHelper.snackBarCallBack(binding.benefitsCardView, this, message);
    }

    @Override
    public void onAddToCartCallBack() {
        dismissProgress();
        if (intentFrom.equals(KEY_INTENT_FROM_CART)) {
            setResult(AppConstant.PRIME_REQUEST_CODE);
            PrimeMemberShipActivity.this.finish();
        } else
            LaunchIntentManager.routeToActivity(PrimeMemberShipActivity.this.getResources().getString(R.string.route_netmeds_mstar_cart), PrimeMemberShipActivity.this);
    }

    @Override
    public void onNavigateSignActivity() {
        navigateToSignIn();
    }

    @Override
    public void setPrimeProductAdapterCallback(List<MstarPrimeProduct> primeMembershipProduct, MStarProductDetails primeProduct, PrimeMemberShipViewModel model) {
        if (primeMembershipProduct != null && primeMembershipProduct.size() > 0) {
            binding.eliteRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            PrimeMemberShipAdapter primeMemberShipAdapter = new PrimeMemberShipAdapter(primeMembershipProduct, primeProduct != null && primeProduct.getProductCode() > 0 ? primeProduct.getProductCode() : 0, model);
            binding.eliteRecyclerView.setAdapter(primeMemberShipAdapter);
            binding.primeMemberCardView.setVisibility(View.GONE);
            binding.eliteRecyclerView.setVisibility(View.VISIBLE);
            binding.headerText.setVisibility(View.VISIBLE);
            binding.imageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initPrimeMemberShipIconAdapter(PrimeConfig primeConfig) {
        if (primeConfig != null && primeConfig.getNetmedsMembership() != null && primeConfig.getNetmedsMembership().getMemberShipIcons() != null && primeConfig.getNetmedsMembership().getMemberShipIcons().size() > 0) {
            binding.offersRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
            PrimeOffersAdapter adapter = new PrimeOffersAdapter(this, primeConfig.getNetmedsMembership().getMemberShipIcons());
            binding.offersRecyclerView.setAdapter(adapter);
            binding.offersRecyclerView.setVisibility(View.VISIBLE);
        } else {
            binding.offersRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoNetworkViewCallback(boolean isConnected) {
        binding.lytViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebServiceError(boolean isWebError) {
        binding.lytViewContent.setVisibility(isWebError ? View.GONE : View.VISIBLE);
        binding.apiErrorView.setVisibility(isWebError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void checkPrimeUserOrNotCallback(boolean primeCustomer, MStarCustomerDetails customer) {
        if (CommonUtils.isExpiryDate(customer) && primeCustomer) {
            listener.onDismissProgress();
            setToolbarTitle(getString(R.string.text_elite_member_title));
            binding.primeMemberCardView.setVisibility(View.VISIBLE);
            binding.eliteRecyclerView.setVisibility(View.GONE);
            binding.headerText.setVisibility(View.VISIBLE);
            binding.imageView.setVisibility(View.VISIBLE);
            binding.setViewModel(viewModel);
        } else {
            setToolbarTitle(getString(R.string.text_elite_membership));
        }
    }

    private void setToolbarTitle(String title) {
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, title);
    }

    @Override
    public void loadFlagCallback() {
        viewModel.webViewSetting(binding.faqWebView);
        binding.faqWebView.setWebViewClient(new PrimeMemberShipViewModel.NetmedsWebViewClient(this));
        binding.faqWebView.loadUrl(viewModel.getFagUrl());
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private class PrimeUserObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            if (response != null && response.getResult() != null && response.getResult().getCustomerDetails() != null) {
                viewModel.onCheckUserDataAvailable(response.getResult().getCustomerDetails());
                binding.setViewModel(viewModel);
            }
        }
    }

    private class PrimeProductObserver implements Observer<MstarPrimeProductResult> {
        @Override
        public void onChanged(@Nullable MstarPrimeProductResult mstarPrimeProductResult) {
            viewModel.primeProductDataAvailable(mstarPrimeProductResult);
            binding.setViewModel(viewModel);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((BasePreference.getInstance(this).isGuestCart()))
            viewModel.getPrimeProducts();
        else
            viewModel.checkUser();
    }

    private void onIntent(Intent intent) {
        if (intent.hasExtra(KEY_INTENT_FROM_CART)) {
            intentFrom = intent.getStringExtra(KEY_INTENT_FROM_CART);
        }
    }

    private void onDeepLinking() {
        if (viewModel.getPrimeConfig() != null && !viewModel.getPrimeConfig().getPrimeEnableFlag())
            PrimeMemberShipActivity.this.finish();
    }

    private void navigateToSignIn() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LaunchIntentManager.routeToActivity(this.getString(R.string.route_sign_in_activity), intent, this);
        PrimeMemberShipActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false)) {
            this.finish();
            startActivity(new Intent(this, NavigationActivity.class));
        } else {
            this.finish();
        }
    }
}
