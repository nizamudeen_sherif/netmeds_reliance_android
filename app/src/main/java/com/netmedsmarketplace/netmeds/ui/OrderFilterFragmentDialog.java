package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogOrderFilterBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OrderFilterFragmentDialogViewModel;
import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.model.MstarStatusItem;

import java.util.List;

@SuppressLint("ValidFragment")
public class OrderFilterFragmentDialog extends BaseDialogFragment implements OrderFilterFragmentDialogViewModel.OrderFilterFragmentDialogViewModelListener {

    private OrderFilterFragmentDialogListener callback;
    private String selectedFilter;
    private List<MstarStatusItem> statusList;

    public OrderFilterFragmentDialog() {
    }

    public OrderFilterFragmentDialog(OrderFilterFragmentDialogListener listener, String selectedFilter, List<MstarStatusItem> statusList) {
        this.callback = listener;
        this.selectedFilter = selectedFilter;
        this.statusList = statusList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogOrderFilterBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_order_filter, container, false);
        OrderFilterFragmentDialogViewModel viewModel = ViewModelProviders.of(this).get(OrderFilterFragmentDialogViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(binding, this, selectedFilter, statusList);
        return binding.getRoot();
    }

    @Override
    public void dismissDialog() {
        OrderFilterFragmentDialog.this.dismiss();
    }

    @Override
    public void onApplyFilter(String filter) {
        OrderFilterFragmentDialog.this.dismiss();
        callback.onApplyFilter(filter);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    dismissDialog();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    public interface OrderFilterFragmentDialogListener {
        void onApplyFilter(String filterType);
    }
}
