package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityM1ProductRequestPopupBinding;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.ValidationUtils;

@SuppressLint("ValidFragment")
public class M1ProductRequsetPopupDialogue extends BaseBottomSheetFragment {

    private final ProductRequestPopupListener requestPopupListener;
    private ActivityM1ProductRequestPopupBinding activityM1ProductRequestPopupBinding;
    private boolean highTicketFlag;

    public M1ProductRequsetPopupDialogue(ProductRequestPopupListener requestPopupListener, boolean highTicketFlag) {
        this.requestPopupListener = requestPopupListener;
        this.highTicketFlag = highTicketFlag;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activityM1ProductRequestPopupBinding = DataBindingUtil.inflate(inflater, R.layout.activity_m1_product_request_popup, container, false);
        onclickViewListener();
        setText();
        textWatcher();
        imeOptionButtonClick();
        return activityM1ProductRequestPopupBinding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    private void onclickViewListener() {
        activityM1ProductRequestPopupBinding.request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndProceed();

            }
        });
        activityM1ProductRequestPopupBinding.textCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

        activityM1ProductRequestPopupBinding.btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

    }

    private void textWatcher() {
        activityM1ProductRequestPopupBinding.patientName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activityM1ProductRequestPopupBinding.patientError.setText(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activityM1ProductRequestPopupBinding.phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activityM1ProductRequestPopupBinding.errorText.setText(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activityM1ProductRequestPopupBinding.medicineName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activityM1ProductRequestPopupBinding.medicineError.setText(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private boolean validateForm() {
        boolean isValidate = true;
        if (!ValidationUtils.userName(activityM1ProductRequestPopupBinding.patientName, true, getString(R.string.text_enter_patient_name), 3, 30, activityM1ProductRequestPopupBinding.patientError)) {
            isValidate = false;
        } else if (!ValidationUtils.checkText(activityM1ProductRequestPopupBinding.phoneNumber, true, getString(R.string.error_mobile), 10, 10, activityM1ProductRequestPopupBinding.errorText)) {
            isValidate = false;
        } else if (!ValidationUtils.userName(activityM1ProductRequestPopupBinding.medicineName, true, getString(R.string.text_error_medicine_name), 3, 500, activityM1ProductRequestPopupBinding.medicineError)) {
            isValidate = false;
        }
        return isValidate;
    }

    private void setText() {
        MStarCustomerDetails addressData = new Gson().fromJson(BasePreference.getInstance(getActivity()).getCustomerDetails(), MStarCustomerDetails.class);
        if (addressData != null) {
            BasePreference.getInstance(getActivity()).setPreviousCartShippingAddressId(addressData.getPreferredShippingAddress());
            String firstName = !TextUtils.isEmpty(addressData.getFirstName()) ? addressData.getFirstName() : "";
            String lastName = !TextUtils.isEmpty(addressData.getLastName()) ? addressData.getLastName() : "";
            activityM1ProductRequestPopupBinding.patientName.setText(String.format("%s %s", firstName, lastName));
            activityM1ProductRequestPopupBinding.phoneNumber.setText(!TextUtils.isEmpty(addressData.getMobileNo()) ? addressData.getMobileNo() : "");
        }
        activityM1ProductRequestPopupBinding.medicineName.setText(requestPopupListener != null && !TextUtils.isEmpty(requestPopupListener.medicineName()) ? requestPopupListener.medicineName() : "");
        if (!highTicketFlag)
            activityM1ProductRequestPopupBinding.tvHighTicket.setVisibility(View.GONE);
        else
            activityM1ProductRequestPopupBinding.tvHighTicket.setVisibility(View.VISIBLE);

    }

    private void imeOptionButtonClick() {
        activityM1ProductRequestPopupBinding.medicineName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkAndProceed();
                    handled = true;
                }
                return handled;
            }
        });
    }

    private void checkAndProceed() {
        if (validateForm()) {
            CommonUtils.hideKeyboard(getActivity(), activityM1ProductRequestPopupBinding.productRequestViewContent);
            dismissAllowingStateLoss();
            String patientName = activityM1ProductRequestPopupBinding.patientName.getText().toString();
            String phoneNumber = activityM1ProductRequestPopupBinding.phoneNumber.getText().toString();
            String medicineName = activityM1ProductRequestPopupBinding.medicineName.getText().toString();
            requestPopupListener.onClickRequest(patientName, phoneNumber, medicineName);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        requestPopupListener.productRequestPopupClose();
    }

    public interface ProductRequestPopupListener {
        void onClickRequest(String patientName, String phoneNumber, String medicineName);

        String medicineName();

        void productRequestPopupClose();
    }
}
