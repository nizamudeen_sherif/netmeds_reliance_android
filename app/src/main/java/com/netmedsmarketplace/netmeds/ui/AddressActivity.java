package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.AddressAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityM2AddressBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AddressViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.Test;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.diagnostic.model.AvailableLab;
import com.nms.netmeds.diagnostic.model.DiagnosticPackage;
import com.nms.netmeds.diagnostic.model.LabDescription;
import com.nms.netmeds.diagnostic.model.PatientDetail;
import com.nms.netmeds.diagnostic.model.PriceDescription;
import com.nms.netmeds.diagnostic.ui.DiagnosticBottomFragment;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.webengage.sdk.android.WebEngage;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddressActivity extends BaseViewModelActivity<AddressViewModel> implements AddressViewModel.AddressListener, AddressAdapter.AddressListListener, DiagnosticBottomFragment.DiagnosticBottomFragmentListener {

    private ActivityM2AddressBinding addressBinding;
    private AddressAdapter addressAdapter;
    private AddressViewModel viewModel;
    private BasePreference basePreference;
    public static AddressBackListener addressBackListener;

    public static void setAddressBackListener(AddressBackListener Listener) {
        addressBackListener = Listener;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addressBinding = DataBindingUtil.setContentView(this, R.layout.activity_m2_address);
        toolBarSetUp(addressBinding.toolbar);
        initToolBar(addressBinding.collapsingToolbar,AddressActivity.this.getResources().getString(R.string.text_select_address));
        addressBinding.setAddressViewModel(onCreateViewModel());
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, M2Helper.getInstance().isM2Order() ? GoogleAnalyticsHelper.POST_SCREEN_SELECT_ADDRESS_M2 : GoogleAnalyticsHelper.POST_SCREEN_SELECT_ADDRESS);
        viewModel.getCustomerAllAddress();
        // finish activity if user coming from order review page for change lab
        if(BasePreference.getInstance(this).isPreviousActivitiesCleared()){
            finish();
        }
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(DiagnosticConstant.KEY_DIAGNOSTIC))
                viewModel.setIntentFrom(intent.getStringExtra(DiagnosticConstant.KEY_DIAGNOSTIC));
            if (intent.hasExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL))
                viewModel.setDiagnosticOrderCreation(intent.getBundleExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL));
            if (intent.hasExtra(DiagnosticConstant.KEY_SELECTED_TYPE))
                viewModel.setDiagnosticSelectedType(intent.getStringExtra(DiagnosticConstant.KEY_SELECTED_TYPE));
            if (intent.hasExtra(DiagnosticConstant.INTENT_IS_RADIO) && intent.getBooleanExtra(DiagnosticConstant.INTENT_IS_RADIO, false)) {
                vmCallBackNext();
                this.finish();
            }
            if(intent.hasExtra(DiagnosticConstant.KEY_FROM_PATIENT_DETAILS) && intent.getBooleanExtra(DiagnosticConstant.KEY_FROM_PATIENT_DETAILS, false))
                viewModel.setFromDiagPatientDetails(true);
            if (intent.hasExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAG_HOME) && intent.getBooleanExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAG_HOME, false)) {
                viewModel.setFromDiagnosticHome(true);
            }
        }
    }

    @Override
    protected AddressViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(AddressViewModel.class);
        basePreference = BasePreference.getInstance(this);
        intentValue();
        viewModel.initViewModel(BasePreference.getInstance(this), this);
        onRetry(viewModel);
        enableContinueButton(false);
        return viewModel;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        basePreference.setAddOrEditAddressCallback(false);
        if (requestCode == DiagnosticConstant.ADDRESS_REQUEST_CODE && data != null && data.hasExtra(DiagnosticConstant.KEY_CUSTOMER_ADDRESS)) {
            MStarAddressModel addressModel = (MStarAddressModel) data.getSerializableExtra(DiagnosticConstant.KEY_CUSTOMER_ADDRESS);
            if (addressModel == null && viewModel.isFromDiagnosticHome()) {
                BasePreference.getInstance(this).setChangesDone(false);
                BasePreference.getInstance(this).setChangeDiagnosticHome(false);
                finish();
            }
            BasePreference.getInstance(this).setDiagnosticPinCode(addressModel != null && !TextUtils.isEmpty(addressModel.getPin()) ? addressModel.getPin() : "");
            viewModel.setDiagnosticCustomerAddress(addressModel);
            viewModel.setBottomView();
            /*Diagnostic WebEngage PinCode Enter Event*/
            WebEngageHelper.getInstance().diagnosticEnterPinCodeEvent(BasePreference.getInstance(this), DiagnosticHelper.getCity(), this);
            if (viewModel.isFromDiagnosticHome() && addressModel != null) {
                BasePreference.getInstance(this).setChangesDone(true);
                BasePreference.getInstance(this).setChangeDiagnosticHome(true);
                DiagnosticHelper.setDefaultAddress(addressModel, this);
                finish();
            }
        } else {
            if (viewModel.isFromDiagnosticHome()) {
                finish();
                return;
            }
        }
        if (RESULT_OK == resultCode && AppConstant.MSTAR_ADDRESS_REQUEST_CODE == requestCode) {
            viewModel.getCustomerAllAddress();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void setAddressListAdapter(List<MStarAddressModel> addresses) {
        addressAdapter = new AddressAdapter(viewModel.getAddressWithSelection(addresses), this, this, viewModel.getIntentFrom() != null && viewModel.getIntentFrom().equals(DiagnosticConstant.KEY_DIAGNOSTIC), basePreference);
        addressBinding.addressList.setLayoutManager(new LinearLayoutManager(this));
        addressBinding.addressList.setNestedScrollingEnabled(false);
        addressBinding.addressList.setAdapter(addressAdapter);
        enableAddressList(false);

        if (viewModel.getIntentFrom() != null && viewModel.getIntentFrom().equals(DiagnosticConstant.KEY_DIAGNOSTIC)) {
            addressBinding.button.setVisibility(View.GONE);
        } else {
            addressBinding.button.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void navigateToAddOrUpdateAddress(MStarAddressModel addressModel, boolean isAddAddress) {
        basePreference.setAddOrEditAddressCallback(true);
        Intent intent = new Intent(this, AddOrUpdateAddressActivity.class);
        intent.putExtra(IntentConstant.ADD_ADDRESS_FLAG, isAddAddress);
        intent.putExtra(IntentConstant.ADDRESS, new Gson().toJson(addressModel));
        intent.putExtra(DiagnosticConstant.KEY_FROM_PATIENT_DETAILS, viewModel.isFromDiagPatientDetails());
        if (viewModel.isFromDiagnosticHome() || (viewModel.getIntentFrom() != null && viewModel.getIntentFrom().equals(DiagnosticConstant.KEY_DIAGNOSTIC))) {
            intent.putExtra(DiagnosticConstant.KEY_DIAGNOSTIC, DiagnosticConstant.KEY_DIAGNOSTIC);
            intent.putExtra(DiagnosticConstant.KEY_LAB_ID, viewModel.getLabId());
            startActivityForResult(intent, DiagnosticConstant.ADDRESS_REQUEST_CODE);
        } else {
            if (viewModel.isFromDiagnosticHome())
                intent.putExtra(DiagnosticConstant.INTENT_KEY_FROM_DIAG_HOME, true);
            intent.putExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST, getIntent().getStringExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST));
            startActivityForResult(intent, AppConstant.MSTAR_ADDRESS_REQUEST_CODE);
        }
        if (viewModel.isNewAddress) {
            finish();
        }
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,addressBinding.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,addressBinding.progressBar);
    }


    private void enableContinueButton(boolean enable) {
        addressBinding.continueButton.setBackground(enable ? getApplication().getResources().getDrawable(com.nms.netmeds.payment.R.drawable.accent_button) : getApplication().getResources().getDrawable(com.nms.netmeds.payment.R.drawable.secondary_button));
        addressBinding.continueButton.setEnabled(enable);
    }

    private void navigateToReviewPage() {
        /*WebEngage Delivery Information Filled Event*/
        WebEngageHelper.getInstance().deliveryInformationFilledEvent(this, viewModel.getSelectedAddress());
        /*WebEngage billing Information Filled Event*/
        WebEngageHelper.getInstance().billingInformationFilledEvent(this, viewModel.getSelectedAddress());
        PaymentHelper.setCustomerBillingAddress(viewModel.getSelectedAddress());
        if (addressBackListener != null)
            addressBackListener.onBackAddress();
        Intent intent = new Intent(this,  OrderConfirmationActivity.class);
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, new Gson().toJson(viewModel.getSelectingAddress()));
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST, getIntent().getStringExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST));
        startActivity(intent);
        this.finish();
    }

    @Override
    public void vmCallBackNext() {
        /*Diagnostic WebEngage Address Event*/
        setWebEngageCollectionAddressEvent();
        Bundle bundle = new Bundle();
        bundle.putAll(viewModel.getDiagnosticOrderCreation());
        bundle.putSerializable(DiagnosticConstant.KEY_CUSTOMER_ADDRESS, viewModel.getDiagnosticCustomerAddress());
        Intent intent = new Intent();
        intent.putExtra(DiagnosticConstant.KEY_ORDER_CREATION_DETAIL, bundle);
        intent.putExtra(DiagnosticConstant.KEY_SELECTED_TYPE, viewModel.getDiagnosticSelectedType());
        if(viewModel.getDiagnosticOrderCreation()!=null && viewModel.getDiagnosticOrderCreation().containsKey(DiagnosticConstant.INTENT_KEY_DIA_SELECTED_SLOTS)) {
            intent.putExtra(DiagnosticConstant.INTENT_KEY_DIA_SELECTED_SLOTS, viewModel.getDiagnosticOrderCreation().getSerializable(DiagnosticConstant.INTENT_KEY_DIA_SELECTED_SLOTS));
        }
        LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_diagnostic_time_slot), intent, this);
    }

    private void setWebEngageCollectionAddressEvent() {
        PatientDetail patientDetail = (PatientDetail) viewModel.getDiagnosticOrderCreation().getSerializable(DiagnosticConstant.KEY_PATIENT_DETAIL);
        String patientName = patientDetail != null && !TextUtils.isEmpty(patientDetail.getName()) ? patientDetail.getName() : "";
        String age = patientDetail != null && !TextUtils.isEmpty(patientDetail.getAge()) ? patientDetail.getAge() : "";
        String gender = patientDetail != null && !TextUtils.isEmpty(patientDetail.getGender()) ? patientDetail.getGender() : "";
        HashMap<String, Object> addressMap = null;
        if (viewModel.getDiagnosticCustomerAddress() != null) {
            String jsonObject = new Gson().toJson(viewModel.getDiagnosticCustomerAddress());
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            addressMap = new Gson().fromJson(jsonObject, type);
        }

        if (viewModel.getDiagnosticSelectedType().equalsIgnoreCase(DiagnosticConstant.TEST)) {
            AvailableLab availableLab = (AvailableLab) viewModel.getDiagnosticOrderCreation().getSerializable(DiagnosticConstant.KEY_SELECTED_LAB);
            LabDescription labDescription = availableLab != null && availableLab.getLabDescription() != null ? availableLab.getLabDescription() : new LabDescription();
            String labName = labDescription != null && !TextUtils.isEmpty(labDescription.getLabName()) ? labDescription.getLabName() : "";
            PriceDescription priceDescription = availableLab != null && availableLab.getPriceDescription() != null ? availableLab.getPriceDescription() : new PriceDescription();
            double price = priceDescription != null && !TextUtils.isEmpty(priceDescription.getFinalPrice()) ? Double.parseDouble(priceDescription.getFinalPrice()) : 0;
            List<Test> testList = availableLab != null && availableLab.getTestList() != null && availableLab.getTestList().size() > 0 ? availableLab.getTestList() : new ArrayList<Test>();
            WebEngageHelper.getInstance().collectionAddressEvent(BasePreference.getInstance(this), DiagnosticHelper.getCity(), age, gender, patientName, true, testList, null, labName, price, addressMap, this);
        } else {
            DiagnosticPackage diagnosticPackage = (DiagnosticPackage) viewModel.getDiagnosticOrderCreation().getSerializable(DiagnosticConstant.KEY_SELECTED_PACKAGE);
            String jsonObject = new Gson().toJson(diagnosticPackage);
            Type type = new TypeToken<HashMap<String, Object>>() {
            }.getType();
            HashMap<String, Object> packageMap = new Gson().fromJson(jsonObject, type);
            String labName = diagnosticPackage != null && diagnosticPackage.getLabDescription() != null && !TextUtils.isEmpty(diagnosticPackage.getLabDescription().getLabName()) ? diagnosticPackage.getLabDescription().getLabName() : "";
            double price = diagnosticPackage != null && diagnosticPackage.getPriceDescription() != null && !TextUtils.isEmpty(diagnosticPackage.getPriceDescription().getFinalPrice()) ? Double.parseDouble(diagnosticPackage.getPriceDescription().getFinalPrice()) : 0;
            WebEngageHelper.getInstance().collectionAddressEvent(BasePreference.getInstance(this), DiagnosticHelper.getCity(), age, gender, patientName, false, null, packageMap, labName, price, addressMap, this);
        }
    }

    @Override
    public void vmInitBottomView(List<Test> testList) {
        DiagnosticBottomFragment bottomFragment = new DiagnosticBottomFragment(getApplication(), testList, viewModel.getDiagnosticSelectedType(), DiagnosticConstant.ADDRESS, this);
        getSupportFragmentManager().beginTransaction().add(bottomFragment, "Bottom_fragment").commitAllowingStateLoss();
    }

    @Override
    public void settingBillingAndShippingAddressResponse(boolean isShippingAddressSet, boolean isBillingAddressSet, boolean deleteFlag) {
        if (!isBillingAddressSet) {
            showErrorMessage(this.getResources().getString(R.string.text_billing_error));
        }
        if (!isShippingAddressSet) {
            showErrorMessage(this.getResources().getString(R.string.text_shipping_error));
        }
        if (isBillingAddressSet && isShippingAddressSet) {
            if (M2Helper.getInstance().isM2Order())
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_ADDRESS_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_M2_ADDRESS);
            else
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_PROCEED_ADDRESS, GoogleAnalyticsHelper.EVENT_LABEL_SELECT_ADDRESS_PAGE);
            basePreference.setPreviousCartShippingAddressId(viewModel.getBillingAndShippingAddressId());
            basePreference.setPreviousCartBillingAddressId(viewModel.getBillingAndShippingAddressId());
            viewModel.setSelectedAddress(viewModel.getSelectingAddress());
        }
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        SnackBarHelper.snackBarCallBack(addressBinding.parentLayout, this, errorMessage);
    }

    @Override
    public void enableAddressList(boolean isAddressList) {
        addressBinding.addressList.setVisibility(isAddressList ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        addressBinding.parentLayout.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        addressBinding.button.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        addressBinding.m2AddressNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isError) {
        addressBinding.parentLayout.setVisibility(isError ? View.GONE : View.VISIBLE);
        addressBinding.button.setVisibility(isError ? View.GONE : View.VISIBLE);
        addressBinding.m2AddressApiErrorView.setVisibility(isError ? View.VISIBLE : View.GONE);

    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    public void hideBottomView() {
        addressBinding.lytTestCount.setVisibility(View.GONE);
    }

    @Override
    public void setBottomViewTestAndPackage(String format) {
        addressBinding.textTestCount.setText(format);

    }

    @Override
    public void showTextLabel() {
        addressBinding.textLabel.setText(this.getResources().getString(R.string.text_total_amount));
        addressBinding.lytTestCount.setVisibility(View.VISIBLE);
    }

    @Override
    public void deleteAddress(int id) {
        viewModel.deleteAddressFromList(id);
    }

    @Override
    public void setDeliveryAddress(MStarAddressModel addressModel) {
        viewModel.setSelectedAddress(addressModel);
        viewModel.setSelectingAddress(addressModel);
        viewModel.setShippingAndBillingAddress(addressModel.getId());
        viewModel.setDiagnosticCustomerAddress(addressModel);
        viewModel.setPreferedBillingAndShippingAddressCall();
        DiagnosticHelper.setDefaultAddress(addressModel, getContext());
        BasePreference.getInstance(getContext()).setChangeDiagnosticHome(true);
        enableContinueButton(true);
    }

    @Override
    public void editDeliveryAddress(MStarAddressModel addressModel) {
        navigateToAddOrUpdateAddress(addressModel, true);
    }

    @Override
    public void diagnosticPinCodeCheck(MStarAddressModel addressModel) {
        if(!TextUtils.isEmpty(basePreference.getDefaultAddress())) {
            DefaultAddress address = new Gson().fromJson(basePreference.getDefaultAddress(), DefaultAddress.class);
            String pinCode = (address!=null && address.getResult()!=null&& address.getResult().getCustomer_details()!=null
                    && address.getResult().getCustomer_details().getAddress()!=null
                    &&  !TextUtils.isEmpty(address.getResult().getCustomer_details().getAddress().getPostcode())) ? address.getResult().getCustomer_details().getAddress().getPostcode():"";
            viewModel.pinCodeServiceCheck(pinCode);
        }
    }

    @Override
    public void onNavigation() {
        vmCallBackNext();
    }

    @Override
    public void onUpdatePopularTest(Test removeTest) {

    }

    @Override
    public void onUpdateSearchList(Test removeTest) {

    }

    @Override
    public void onUpdateLabList() {

    }

    @Override
    public void navigateToReviewPages() {
        navigateToReviewPage();
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        if (viewModel.getIntentFrom() != null && viewModel.getIntentFrom().equals(DiagnosticConstant.KEY_DIAGNOSTIC))
            WebEngage.get().analytics().screenNavigated(GoogleAnalyticsHelper.POST_SCREEN_DIAGNOSTIC_ADDRESS);
    }
    public interface AddressBackListener {
        void onBackAddress();
    }
}