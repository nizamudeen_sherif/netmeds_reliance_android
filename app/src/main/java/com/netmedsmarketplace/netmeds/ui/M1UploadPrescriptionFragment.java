package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentM1UploadPrescriptionBinding;
import com.netmedsmarketplace.netmeds.viewmodel.M1UploadPrescriptionViewModel;
import com.nms.netmeds.base.BaseDialogFragment;

public class M1UploadPrescriptionFragment extends BaseDialogFragment implements M1UploadPrescriptionViewModel.M1PrescriptionListener {

    private UploadPrescriptionListener mUploadPrescriptionListener;

    //Default Constructor
    public M1UploadPrescriptionFragment() {
    }

    @SuppressLint("ValidFragment")
    public M1UploadPrescriptionFragment(UploadPrescriptionListener uploadPrescriptionListener) {
        this.mUploadPrescriptionListener = uploadPrescriptionListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentM1UploadPrescriptionBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_m1_upload_prescription, container, false);
        M1UploadPrescriptionViewModel viewModel = ViewModelProviders.of(this).get(M1UploadPrescriptionViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(this);
        return binding.getRoot();
    }

    @Override
    public void closeDialog() {
        dismissDialog();
    }

    private void dismissDialog() {
        this.dismissAllowingStateLoss();
    }

    @Override
    public void cameraView() {
        dismissDialog();
        mUploadPrescriptionListener.initCameraView();
    }

    @Override
    public void galleryView() {
        dismissDialog();
        if (mUploadPrescriptionListener != null)
            mUploadPrescriptionListener.initGalleryView();
    }

    @Override
    public void pastPrescriptionView() {
        dismissDialog();
        if (mUploadPrescriptionListener != null)
            mUploadPrescriptionListener.initPastPrescription();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    dismissDialog();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    public interface UploadPrescriptionListener {

        void initCameraView();

        void initGalleryView();

        void initPastPrescription();

    }
}