package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MstarWellnessAdapter;
import com.netmedsmarketplace.netmeds.adpater.PopularProductsAdapter;
import com.netmedsmarketplace.netmeds.adpater.WellnessActivityAdapterV2;
import com.netmedsmarketplace.netmeds.databinding.ActivityWellnessBinding;
import com.netmedsmarketplace.netmeds.viewmodel.WellnessViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarWellnessSectionDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.webengage.sdk.android.WebEngage;

import java.util.List;

public class WellnessActivity extends BaseViewModelActivity<WellnessViewModel> implements WellnessViewModel.WellnessListener, AddCartBottomSheetDialog.BottomSheetDialogListener, PopularProductsAdapter.PopularProductAddToCartListener, MstarWellnessAdapter.WellnessAdaterCallBack, WellnessActivityAdapterV2.WellnessActivityAdapterCallback {

    private ActivityWellnessBinding wellnessBinding;
    private WellnessViewModel wellnessViewModel;
    private AddCartBottomSheetDialog.BottomSheetDialogListener bottomSheetDialogListener;
    private boolean isAddToCartBottomSheetOpen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wellnessBinding = DataBindingUtil.setContentView(this, R.layout.activity_wellness);
        toolBarSetUp(wellnessBinding.toolBarCustomLayout.toolbar);
        initToolBar(wellnessBinding.toolBarCustomLayout.collapsingToolbar,getString(R.string.text_wellness));
        bottomSheetDialogListener = this;
        wellnessBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected WellnessViewModel onCreateViewModel() {
        wellnessViewModel = ViewModelProviders.of(this).get(WellnessViewModel.class);
        wellnessViewModel.init(this);
        wellnessViewModel.getWellnessPageLiveData().observe(this, new WellnessDetailObserver());
        onRetry(wellnessViewModel);
        return wellnessViewModel;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
        else
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                navigateToCart();
                return true;
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            default:
                return false;
        }
    }


    private void navigateToCart() {
        startActivity(new Intent(this, MStarCartActivity.class));
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,wellnessBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,wellnessBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmCategoryViewAll(String viewAll) {
        Intent intent = new Intent(this, WellnessCategoryViewAllActivity.class);
        intent.putExtra(IntentConstant.WELLNESS_VIEW_ALL_CODES, viewAll);
        startActivity(intent);
    }

    @Override
    public void viewAllbuttonCallBack(int id, String from, String name) {
        Intent intent = new Intent(this, ProductList.class);
        if (AppConstant.MANUFACTURE.equalsIgnoreCase(from)) {
            intent.putExtra(IntentConstant.MANUFACTURER_ID, id);
            intent.putExtra(IntentConstant.MANUFACTURER_NAME, name);
        } else {
            intent.putExtra(IntentConstant.CATEGORY_ID, id);
            intent.putExtra(IntentConstant.CATEGORY_NAME, name);
        }
        startActivity(intent);
    }

    @Override
    public void routeToScreen(String viewAll) {

        CommonUtils.activityNavigation(this, Uri.parse(viewAll));
    }


    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        wellnessBinding.wellnessActivityRecyclerView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        wellnessBinding.wellnessNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showApiErrorView(boolean isAPIerror) {
        wellnessBinding.wellnessActivityRecyclerView.setVisibility(isAPIerror ? View.GONE : View.VISIBLE);
        wellnessBinding.wellnessApiErrorView.setVisibility(isAPIerror ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setWellnessAdapter(final List<MstarWellnessSectionDetails> adapterList) {
        wellnessBinding.wellnessActivityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        WellnessActivityAdapterV2 adapter = new WellnessActivityAdapterV2(adapterList, this, this, this);
        wellnessBinding.wellnessActivityRecyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_WELLNESS_PAGE);
    }

    @Override
    public void bsAddCartCallBack() {
        Snackbar snackbar = Snackbar.make(wellnessBinding.wellnessActivityRecyclerView, getString(R.string.text_added_to_cart), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.text_view_cart), new WellnessActivity.ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void showAlert(String message) {
        SnackBarHelper.snackBarCallBack(wellnessBinding.wellnessActivityRecyclerView, this, message);
    }

    @Override
    public void updateCartCount(int count) {

    }

    @Override
    public void addToCartBottomSheetClose() {
        isAddToCartBottomSheetOpen = false;
    }

    private void addToCartPopup(MStarProductDetails product) {
        if (!isFinishing() && !isAddToCartBottomSheetOpen) {
            isAddToCartBottomSheetOpen = true;
            AddCartBottomSheetDialog couponFragment = new AddCartBottomSheetDialog(product, false, bottomSheetDialogListener);
            getSupportFragmentManager().beginTransaction().add(couponFragment, "AddCartBottomSheetDialog").commitAllowingStateLoss();
        }
    }

    @Override
    public void OnCategoryClickCallback(int id, int level, String categoryName) {
        Intent intent = new Intent(this, level == 1 ? CategoryActivity.class : ProductList.class); // for level 1 display sub-category and for level 2 & 3 show only product list and
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentConstant.CATEGORY_ID, id);
        intent.putExtra(IntentConstant.CATEGORY_NAME, categoryName);
        startActivity(intent);
    }

    @Override
    public void PopularProductAddToCart(MStarProductDetails product) {
        addToCartPopup(product);
    }

    @Override
    public void popularProductDetailsPageCallback(MStarProductDetails product) {
        Intent intent = new Intent(this, ProductDetail.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, product.getProductCode());
        startActivity(intent);

    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(WellnessActivity.this, MStarCartActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(WellnessActivity.this, GoogleAnalyticsHelper.POST_WELLNES_HOME);
    }

    private class WellnessDetailObserver implements Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel) {
            if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getResult() != null)
                wellnessViewModel.onWellPageDetailsAvailable(mstarBasicResponseTemplateModel.getResult());
            wellnessBinding.setViewModel(wellnessViewModel);
        }
    }
}
