package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityTrackOrderDetailsBinding;
import com.netmedsmarketplace.netmeds.viewmodel.TrackOrderDetailsViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;
import com.nms.netmeds.base.model.MstarOrders;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;

import java.util.ArrayList;

public class TrackOrderDetailsActivity extends BaseViewModelActivity<TrackOrderDetailsViewModel> implements TrackOrderDetailsViewModel.TrackOrderDetailsListener, CancelOrderFragmentDialog.CancelOrderDialogListener {

    private TrackOrderDetailsViewModel viewModel;
    private ActivityTrackOrderDetailsBinding binding;
    private String orderId = "";
    private int trackOrderPosition = 0;
    private boolean isFromSubscsription = false;
    private boolean isFromDeeplinking = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_track_order_details);
        onCreateViewModel();
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, getString(R.string.text_track_order));
    }

    @Override
    protected TrackOrderDetailsViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(TrackOrderDetailsViewModel.class);
        intentValue();
        viewModel.init(this, binding, orderId, trackOrderPosition, isFromSubscsription, this, isFromDeeplinking);
        viewModel.getTrackOrderMutableLiveData().observe(this, new TrackOrderObserver());
        onRetry(viewModel);
        return viewModel;
    }

    @Override
    public void showLoader() {
        showHorizontalProgressBar(false, binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void dismissLoader() {
        showHorizontalProgressBar(true, binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void onCallbackOfCancelOrder(String orderId) {
        setButtonDisableAndEnableProperties(false);
        CancelOrderFragmentDialog cancelOrderFragmentDialog = new CancelOrderFragmentDialog(this);
        getSupportFragmentManager().beginTransaction().add(cancelOrderFragmentDialog, IntentConstant.REVIEW_ORDER_AGAIN_DIALOG_FRAGMENT_TAG_NAME).commitAllowingStateLoss();
    }

    @Override
    public void onCancelInitiated(String cancelMessage, int cancelMessageIndex) {
        viewModel.cancelOrder(cancelMessage, cancelMessageIndex);
    }

    @Override
    public void dismissDialog() {
        setButtonDisableAndEnableProperties(true);
    }

    @Override
    public void onCallbackOfNeedHelpForOrder(String orderId) {
        setButtonDisableAndEnableProperties(false);
        Intent intent = new Intent(this, NeedHelpActivity.class);
        startActivity(intent);
    }

    private void setButtonDisableAndEnableProperties(boolean isEnable) {
        binding.btnCancelOrder.setEnabled(isEnable);
        binding.btnCancelOrder.setClickable(isEnable);
        binding.btnNeedHelp.setEnabled(isEnable);
        binding.btnNeedHelp.setClickable(isEnable);
    }

    @Override
    public void onCallbackOfViewItemsInOrder(String orderId) {
        if (SubscriptionHelper.getInstance() != null && !TextUtils.isEmpty(SubscriptionHelper.getInstance().getIssueId())) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstant.PRIMARY_ORDER_ID, orderId);
            intent.putExtra(IntentConstant.ISSUE_ID, SubscriptionHelper.getInstance().getIssueId());
            intent.putExtra(IntentConstant.SUBSCRIPTION_CANCEL_FLAG, true);
            LaunchIntentManager.routeToActivity(getString(R.string.route_subscription_manage_activity), intent, this);
            this.finish();
        } else {
            Intent intent = new Intent(this, ViewOrderDetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(IntentConstant.ORDER_ID, orderId);
            startActivity(intent);
        }
    }

    @Override
    public void cancelOrderSuccessful(String message) {
        setButtonDisableAndEnableProperties(true);
        setResult(IntentConstant.CANCEL_ORDER_RESULT_OK, new Intent());
        finish();
    }

    @Override
    public void onLaunchTrackStatusInfo(String url) {
        Intent intent = new Intent(this, NetmedsWebViewActivity.class);
        intent.putExtra(IntentConstant.WEB_PAGE_URL, url);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, getString(R.string.text_track_order));
        startActivity(intent);
    }

    @Override
    public void showSnackbar(String message) {
        setButtonDisableAndEnableProperties(true);
        SnackBarHelper.snackBarCallBack(binding.llDetailsView, this, message);
    }

    @Override
    public void refreshPage() {
        viewModel.initiateOrderTrackDetails();
        binding.setViewModel(viewModel);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            TrackOrderDetailsActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        setButtonDisableAndEnableProperties(true);
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_TRACK_ORDER);
    }

    private class TrackOrderObserver implements Observer<MstarOrderDetailsResult> {
        @Override
        public void onChanged(@Nullable MstarOrderDetailsResult orderTrackDetailsResponse) {
            viewModel.initiateOrderTrackDetails();
            binding.setViewModel(viewModel);
        }
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            checkDeepLink(intent);
            if (intent.hasExtra(IntentConstant.ORDER_DETAILS))
                viewModel.setCancelOrderDetail((MstarOrders) intent.getSerializableExtra(IntentConstant.ORDER_DETAILS));
        }
    }

    private void checkDeepLink(Intent intent) {
        if (intent.hasExtra(IntentConstant.ORDER_ID)) {
            orderId = intent.getStringExtra(IntentConstant.ORDER_ID);
            trackOrderPosition = intent.getIntExtra(IntentConstant.TRACK_ORDER_POSITION, 0);
            isFromSubscsription = intent.getBooleanExtra(IntentConstant.FROM_SUBSCRIPTION, false);
        } else
            onIntent(intent);
    }

    @Override
    public void onEmptyOrderCallBackFromDeeplinking() {
        Intent intent = new Intent();
        intent.putExtra(PaymentIntentConstant.HOME_FLAG_FROM_ORDER_PLACED_SUCCESSFUL, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        LaunchIntentManager.routeToActivity(getString(com.nms.netmeds.m3subscription.R.string.route_navigation_activity), intent, this);
    }

    private void onIntent(Intent intent) {
        if (intent.hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
            orderId = intentParamList != null && intentParamList.size() > 0 ? intentParamList.get(0) : "";
            isFromDeeplinking = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstant.CANCEL_ORDER_RQUEST_CODE) {
            if (resultCode == IntentConstant.CANCEL_ORDER_RESULT_OK) {
                cancelOrderSuccessful(null);
            }
        }
    }
}
