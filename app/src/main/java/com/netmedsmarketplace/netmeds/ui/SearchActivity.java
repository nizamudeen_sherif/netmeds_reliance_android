package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.SearchAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivitySearchBinding;
import com.netmedsmarketplace.netmeds.db.DatabaseClient;
import com.netmedsmarketplace.netmeds.db.SearchHistory;
import com.netmedsmarketplace.netmeds.viewmodel.SearchViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.utils.AlgoliaClickAndConversionHelper;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SearchActivity extends BaseViewModelActivity<SearchViewModel> implements SearchAdapter.SearchAdapterListener, SearchViewModel.SearchListener, AddCartBottomSheetDialog.BottomSheetDialogListener, M1ProductRequsetPopupDialogue.ProductRequestPopupListener {
    private ActivitySearchBinding searchBinding;
    private SearchViewModel searchViewModel;
    private AddCartBottomSheetDialog.BottomSheetDialogListener bottomSheetDialogListener;
    private boolean isAddToCartBottomSheetOpen = false;
    private String searchQuery = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchBinding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        searchBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(searchBinding.searchToolbar);
        SubscriptionHelper.getInstance().setSubscriptionIssueOrderProductDetailFlag(true);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        bottomSheetDialogListener = this;
        initIntentValue();
        searchBinding.noResultView.setVisibility(M2Helper.getInstance().isM2Order() || searchViewModel.isFromGeneric ? GONE : VISIBLE);
    }

    @Override
    protected SearchViewModel onCreateViewModel() {
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        searchViewModel.init(this, this, BasePreference.getInstance(this));
        searchViewModel.getMstarAlgoliaMutableLiveData().observe(this, new AlgoliaSearchListObserver());
        searchViewModel.getProductDetailResponseMutableLiveData().observe(this, new ProductDetailObserver());
        onRetry(searchViewModel);
        return searchViewModel;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);
        if (!M2Helper.getInstance().isM2Order()) {
            if (BasePreference.getInstance(this).getCartCount() > 0)
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
            else
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));
        } else {
            menu.getItem(0).setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.cart:
                /*Google Analytics Click Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_VIEW_CART_SEARCH_PAGE, GoogleAnalyticsHelper.EVENT_LABEL_SEARCH_PAGE);
                navigateToCart();
                return true;
            default:
                return false;
        }
    }

    //Navigate to cart page
    private void navigateToCart() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.IS_FROM_HOME_SCREEN) && getIntent().getBooleanExtra(IntentConstant.IS_FROM_HOME_SCREEN, false)) {
            PaymentHelper.setIsInitiateCheckoutCalled(false);
        }
        LaunchIntentManager.routeToActivity(SearchActivity.this.getResources().getString(R.string.route_netmeds_mstar_cart), SearchActivity.this);
    }

    @Override
    public void adOnAddCartCallBack(MstarAlgoliaResult algoliaHitResults, int position) {
        showProgress(this);
        searchViewModel.getDetail(algoliaHitResults.getProductCode());

        //Algolia click event
        postAlgoliaClickEvent(AlgoliaClickAndConversionHelper.EVENT_NAME_ADDED_TO_CART, position, algoliaHitResults);
        /*Google Tag Manager + FireBase Product Click Event*/
        onGoogleTagManagerEvent(algoliaHitResults, position);
    }

    @Override
    public void adOnResultClickCallBack(MstarAlgoliaResult algoliaHitResults, int position) {
        new StoreSearchHistory(algoliaHitResults).execute();
        searchViewModel.onSearchAutoCompleteEvent(algoliaHitResults);

        //Algolia click event
        postAlgoliaClickEvent(AlgoliaClickAndConversionHelper.EVENT_NAME_PRODUCT_VIEWED, position, algoliaHitResults);
        /*Google Tag Manager + FireBase Product Click Event*/
        onGoogleTagManagerEvent(algoliaHitResults, position);

        if (!searchViewModel.isFromGeneric) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, getProductCode(algoliaHitResults));
            intent.putExtra(IntentConstant.IS_FROM_SEARCH, true);
            LaunchIntentManager.routeToActivity(SearchActivity.this.getResources().getString(R.string.route_product_detail), intent, SearchActivity.this);
        } else {
            Intent intent = new Intent(this, GenericProductDetailActivity.class);
            intent.putExtra(IntentConstant.PRODUCT_CODE, algoliaHitResults.getProductCode());
            startActivity(intent);
        }
    }

    @Override
    public void bsAddCartCallBack() {
        CommonUtils.initiateVibrate(this);
        SubscriptionHelper.getInstance().setEditorderquantityflag(SubscriptionHelper.getInstance().isPayNowSubscription());
        Snackbar snackbar = Snackbar.make(searchBinding.searchParentView, getString(R.string.text_added_to_cart), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.text_view_cart), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OTHER_ACTIONS, GoogleAnalyticsHelper.EVENT_ACTION_ADD_PRODUCT_SEARCH, GoogleAnalyticsHelper.EVENT_LABEL_SEARCH_PAGE);
    }

    @Override
    public void showAlert(String message) {
        SnackBarHelper.snackBarCallBack(searchBinding.searchParentView, this, message);
    }

    @Override
    public void updateCartCount(int count) {
        Menu menu = searchBinding.searchToolbar.getMenu();
        if (!M2Helper.getInstance().isM2Order()) {
            if (BasePreference.getInstance(this).getCartCount() > 0)
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
            else
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));
        } else {
            if (BasePreference.getInstance(this).getM2CartCount() > 0)
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
            else
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));
        }
    }

    @Override
    public void addToCartBottomSheetClose() {
        isAddToCartBottomSheetOpen = false;
    }

    @Override
    public void vmUploadPrescription() {
        if (!BasePreference.getInstance(this).isGuestCart()) {
            LaunchIntentManager.routeToActivity(SearchActivity.this.getResources().getString(R.string.route_m2_attach_prescription), SearchActivity.this);
        } else {
            Intent intent = new Intent();
            LaunchIntentManager.routeToActivity(SearchActivity.this.getResources().getString(R.string.route_sign_in_activity), intent, SearchActivity.this);
        }
    }

    @Override
    public void vmRequestPopup() {
        searchBinding.requestPopupBtn.setEnabled(false);
        if (!BasePreference.getInstance(this).isGuestCart()) {
            M1ProductRequsetPopupDialogue reschedulePopUpDialog = new M1ProductRequsetPopupDialogue(this, false);
            getSupportFragmentManager().beginTransaction().add(reschedulePopUpDialog, "RequestDialog").commitAllowingStateLoss();
        } else {
            Intent intent = new Intent(this, SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void vmSearchResultCountWithText() {
        searchBinding.noProductViewSearch.setVisibility(GONE);
        searchBinding.noResultView.setVisibility(GONE);
        searchBinding.searchProductView.setVisibility(VISIBLE);
        searchBinding.resultText.setVisibility(VISIBLE);
        String result = String.format(SearchActivity.this.getString(R.string.text_showing_result), searchBinding.searchBar.getText() != null && !TextUtils.isEmpty(searchBinding.searchBar.getText().toString()) ? searchBinding.searchBar.getText().toString().trim() : "");
        searchBinding.resultText.setText(result);
        searchBinding.searchHistoryTitle.setVisibility(GONE);
    }

    @Override
    public void vmRequestProductView() {
        if (!searchViewModel.isFromGeneric) {
            searchBinding.noProductViewGeneric.setVisibility(GONE);
            searchBinding.noProductViewSearch.setVisibility(VISIBLE);
            if (searchViewModel.uploadVisibility())
                searchBinding.noResultView.setVisibility(GONE);
            searchBinding.searchProductView.setVisibility(GONE);
        } else {
            searchBinding.noProductViewSearch.setVisibility(GONE);
            searchBinding.noProductViewGeneric.setVisibility(VISIBLE);
            searchBinding.noResultView.setVisibility(GONE);
            searchBinding.searchProductView.setVisibility(GONE);
        }
    }

    @Override
    public void vmNoProductView() {
        if (searchViewModel.uploadVisibility())
            searchBinding.noResultView.setVisibility(SubscriptionHelper.getInstance().isCreateNewFillFlag() || searchViewModel.isFromGeneric ? GONE : VISIBLE);
        searchBinding.noProductViewSearch.setVisibility(GONE);
        searchBinding.searchProductView.setVisibility(GONE);
    }

    @Override
    public void vmNoNetworkView() {
        searchBinding.lytSearchView.setVisibility(NetworkUtils.isConnected(SearchActivity.this) ? View.VISIBLE : View.GONE);
        searchBinding.searchNetworkErrorView.setVisibility(NetworkUtils.isConnected(SearchActivity.this) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void vmWebserviceErrorView(boolean isWebServiceError) {
        searchBinding.lytSearchView.setVisibility(isWebServiceError ? View.GONE : View.VISIBLE);
        searchBinding.searchApiErrorView.setVisibility(isWebServiceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void vmSetNoResultText() {
        searchBinding.resultText.setVisibility(GONE);
        searchBinding.searchNoResult.setVisibility(VISIBLE);
        String result = String.format(getApplicationContext().getString(R.string.text_no_product_description), searchBinding.searchBar.getText() != null && !TextUtils.isEmpty(searchBinding.searchBar.getText().toString()) ? searchBinding.searchBar.getText().toString() : "");
        searchBinding.searchNoResult.setText(result);
    }

    @Override
    public String getSearchKeyWord() {
        return searchBinding.searchBar.getText() != null && !TextUtils.isEmpty(searchBinding.searchBar.getText()) ? searchBinding.searchBar.getText().toString() : "";
    }

    @Override
    public TextWatcher getSearchWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (searchBinding.searchBar.getText() != null && !TextUtils.isEmpty(searchBinding.searchBar.getText().toString().trim())) {
                    searchBinding.search.setImageResource(R.drawable.ic_clear);
                    searchViewModel.pageIndex = 0;
                    searchViewModel.totalPageCount = 0;
                    searchBinding.noResultView.setVisibility(GONE);
                    searchViewModel.searchApiCall(false);
                } else {
                    searchBinding.search.setImageResource(R.drawable.ic_search);
                    searchViewModel.getHistoryList();
                    searchBinding.noResultView.setVisibility(M2Helper.getInstance().isM2Order() || searchViewModel.isFromGeneric ? GONE : VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    @Override
    public SearchAdapter setSearchAdapter(List<MstarAlgoliaResult> algoliaHitList) {
        SearchAdapter searchAdapter = new SearchAdapter(algoliaHitList, this, searchViewModel.isFromGeneric);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchActivity.this);
        searchBinding.rvSearchList.setLayoutManager(linearLayoutManager);
        searchBinding.rvSearchList.setAdapter(searchAdapter);
        searchBinding.rvSearchList.setNestedScrollingEnabled(false);
        searchBinding.rvSearchList.addOnScrollListener(searchViewModel.onPageScroll());
        return searchAdapter;
    }

    @Override
    public void initEditorListener() {
        searchBinding.searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchViewModel.searchApiCall(false);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void setEmptyView(boolean isEmpty) {
        searchBinding.noProductViewSearch.setVisibility(!isEmpty ? GONE : VISIBLE);
        searchBinding.searchProductView.setVisibility(!isEmpty ? VISIBLE : GONE);
    }

    @Override
    public void resetViews(boolean reset) {
        searchBinding.searchHistoryTitle.setVisibility(reset ? VISIBLE : GONE);
        searchBinding.resultText.setVisibility(reset ? GONE : VISIBLE);
    }

    @Override
    public void setNoResultView(boolean isNoResult) {
        searchBinding.noResultView.setVisibility(isNoResult ? GONE : VISIBLE);
    }

    @Override
    public boolean uploadPrescriptionVisibilty() {
        return SubscriptionHelper.getInstance().isCreateNewFillFlag() || M2Helper.getInstance().isM2Order();
    }

    @Override
    public void snackBarCallback(String message) {
        SnackBarHelper.snackBarCallBack(searchBinding.searchParentView, this, message);
    }

    @Override
    public void vmShowProgressBar() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public Context getContext() {
        return SearchActivity.this;
    }

    @Override
    public void openNormalSearch() {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentConstant.GENERIC_SEARCH_TEXT, !TextUtils.isEmpty(getSearchKeyWord()) ? getSearchKeyWord() : "");
        startActivity(intent);
    }


    @Override
    public void onClickRequest(String patientName, String phoneNumber, String medicineName) {
        searchViewModel.onClickRequest(patientName, medicineName, phoneNumber);
    }

    @Override
    public String medicineName() {
        return null;
    }

    @Override
    public void productRequestPopupClose() {
        searchBinding.requestPopupBtn.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_SEARCH_PAGE);
        if (searchViewModel.isFromGeneric)
            GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_GENERIC_SEARCH);

    }

    private class AlgoliaSearchListObserver implements Observer<MstarAlgoliaResponse> {

        @Override
        public void onChanged(@Nullable MstarAlgoliaResponse algoliaResponse) {
            /*Google Analytics Click Event*/
            GoogleAnalyticsHelper.getInstance().postEvent(SearchActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OTHER_ACTIONS, GoogleAnalyticsHelper.EVENT_ACTION_PRODUCT_SEARCHED, GoogleAnalyticsHelper.EVENT_LABEL_SEARCH_PAGE);
            searchViewModel.onDataAvailable(algoliaResponse);
            searchBinding.setViewModel(searchViewModel);
        }
    }

    private class ProductDetailObserver implements Observer<MStarProductDetails> {

        @Override
        public void onChanged(@Nullable MStarProductDetails mStarProductDetails) {
            dismissProgress();
            if (!searchViewModel.isProductAvailableWithStock(mStarProductDetails)) {
                showAlert(SearchActivity.this.getResources().getString(R.string.text_out_of_stock));
                return;
            }
            showAddToCartBottomSheet(mStarProductDetails);
        }
    }

    private void showAddToCartBottomSheet(MStarProductDetails mStarProductDetails) {
        if (!isFinishing() && !isAddToCartBottomSheetOpen) {
            isAddToCartBottomSheetOpen = true;
            AddCartBottomSheetDialog couponFragment = new AddCartBottomSheetDialog(mStarProductDetails, true, bottomSheetDialogListener);
            getSupportFragmentManager().beginTransaction().add(couponFragment, "AddCartBottomSheetDialog").commitAllowingStateLoss();
        }
    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            navigateToCart();
        }
    }

    /**
     * This asyncTask used to store search keyword at local database
     */
    @SuppressLint("StaticFieldLeak")
    class StoreSearchHistory extends AsyncTask<Void, Void, Void> {

        private final MstarAlgoliaResult algoliaHitResult;

        StoreSearchHistory(MstarAlgoliaResult algoliaHitResults) {
            this.algoliaHitResult = algoliaHitResults;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //creating a searchHistory
            SearchHistory searchHistory = new SearchHistory();
            searchHistory.setId(algoliaHitResult.getProductCode());
            searchHistory.setAlgoliaResult(new Gson().toJson(algoliaHitResult));
            searchHistory.setCurrentTimestamp(System.currentTimeMillis());

            //adding to database
            DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().searchHistoryDAO()
                    .insert(searchHistory);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void initIntentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(IntentConstant.SEARCH_QUERY_TEXT)) {
                searchQuery = intent.getStringExtra(IntentConstant.SEARCH_QUERY_TEXT);
            } else if (intent.hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
                ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
                searchQuery = intentParamList != null && intentParamList.size() > 0 ? intentParamList.get(0) : "";
            }
            if (!TextUtils.isEmpty(searchQuery)) {
                searchBinding.searchBar.setText(searchQuery);
                searchBinding.searchBar.setSelection(searchQuery.length());
                searchViewModel.searchApiCall(false);
            }
            if (intent.hasExtra(IntentConstant.IS_FROM_GENERIC)) {
                searchViewModel.isFromGeneric = intent.getBooleanExtra(IntentConstant.IS_FROM_GENERIC, false);
            }
            if (intent.hasExtra(IntentConstant.GENERIC_SEARCH_TEXT)) {
                if (!TextUtils.isEmpty(intent.getStringExtra(IntentConstant.GENERIC_SEARCH_TEXT))) {
                    searchBinding.searchBar.setText(intent.getStringExtra(IntentConstant.GENERIC_SEARCH_TEXT));
                    searchViewModel.searchApiCall(false);
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //Clear search field
    public void clearSearch(View view) {
        if (searchBinding.searchBar.getText() != null && !TextUtils.isEmpty(searchBinding.searchBar.getText().toString())) {
            searchBinding.searchBar.setText("");
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null)
                inputManager.showSoftInput(searchBinding.searchBar, 0);
            searchBinding.search.setImageResource(R.drawable.ic_search);
            searchViewModel.getHistoryList();
        }
    }

    /**
     * Algolia analytic click event send to the algolia server
     *
     * @param eventName         name of the event
     * @param position          product selected position
     * @param algoliaHitResults details of the product
     */
    private void postAlgoliaClickEvent(String eventName, int position, MstarAlgoliaResult algoliaHitResults) {
        AlgoliaClickAndConversionHelper.getInstance().onAlgoliaClickEvent(SearchActivity.this, Integer.toString(getProductCode(algoliaHitResults)), position, BasePreference.getInstance(this).getAlgoliaQueryId(), eventName);
    }

    private int getProductCode(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null ? algoliaHitResults.getProductCode() : 0;
    }

    private void onGoogleTagManagerEvent(MstarAlgoliaResult algoliaHitResults, int position) {
        FireBaseAnalyticsHelper.getInstance().logFireBaseProductClickAndSelectionEvent(this, algoliaHitResults, position, FireBaseAnalyticsHelper.EVENT_PARAM_SEARCH_RESULTS);
    }
}
