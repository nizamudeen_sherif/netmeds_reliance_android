package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityLegalInfoHelpBinding;
import com.netmedsmarketplace.netmeds.viewmodel.LegalInfoViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;

public class LegalInfoWithHelpActivity extends BaseViewModelActivity<LegalInfoViewModel> implements LegalInfoViewModel.LegalInfoListener {

    private ActivityLegalInfoHelpBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_legal_info_help);
        binding.setViewModel(onCreateViewModel());
        toolBarSetUp(binding.toolbar);
        initToolBar();
    }

    @Override
    protected LegalInfoViewModel onCreateViewModel() {
        LegalInfoViewModel viewModel = ViewModelProviders.of(this).get(LegalInfoViewModel.class);
        viewModel.init(binding, this);
        return viewModel;
    }

    private void initToolBar() {
        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(getIntent().getStringExtra(IntentConstant.LEGAL_INFO_HELP_TITLE_KEY));
        }
    }

    @Override
    public void onClickInfo(String url, String title) {
        Intent intent = new Intent(this, NetmedsWebViewActivity.class);
        intent.putExtra(IntentConstant.WEB_PAGE_URL, url);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, title);
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
