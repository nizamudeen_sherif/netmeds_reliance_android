package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.BuyAgainAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityBuyAgainBinding;
import com.netmedsmarketplace.netmeds.viewmodel.BuyAgainViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.List;

public class BuyAgainActivity extends BaseViewModelActivity<BuyAgainViewModel> implements BuyAgainViewModel.BuyAgainViewModelListener {


    private ActivityBuyAgainBinding buyAgainBinding;
    private BuyAgainViewModel buyAgainViewModel;
    private MStarBasicResponseTemplateModel model;
    private BuyAgainAdapter buyAgainAdapter;
    private boolean isBtnAnimate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buyAgainBinding = DataBindingUtil.setContentView(this,R.layout.activity_buy_again);
        toolBarSetUp(buyAgainBinding.buyAgainToolbar);
        initToolBar();
        buyAgainBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected BuyAgainViewModel onCreateViewModel() {
        buyAgainViewModel = ViewModelProviders.of(this).get(BuyAgainViewModel.class);
        model = new Gson().fromJson(BasePreference.getInstance(this).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        buyAgainViewModel.initiateModel(this,BasePreference.getInstance(this));
        onRetry(buyAgainViewModel);
        buyAgainBinding.setViewModel(buyAgainViewModel);
        return buyAgainViewModel;
    }

    private void initToolBar(){
        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(getString(R.string.text_products_ordered));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0) {
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
        }else{
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                navigateToCart();
                return true;
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            default:
                return false;
        }

    }


    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        buyAgainBinding.rvBuyAgainListView.setVisibility(isConnected?View.VISIBLE:View.GONE);
        buyAgainBinding.buyAgainNetworkErrorView.setVisibility(isConnected?View.GONE:View.VISIBLE);

    }

    @Override
    public void showLoader() {
        buyAgainBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoader() {
        buyAgainBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String message) {
        SnackBarHelper.snackBarCallBack(buyAgainBinding.buyAgainView,this,message);
    }

    @Override
    public void showMessageWithAction(String message) {
        SnackBarHelper.snackBarCallBack(buyAgainBinding.buyAgainView, this, getString(R.string.text_added_to_cart));
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        buyAgainBinding.rvBuyAgainListView.setVisibility(isWebserviceError? View.GONE:View.VISIBLE);
        buyAgainBinding.proceedToCheckoutBtn.setVisibility(isWebserviceError && BasePreference.getInstance(this).getCartCount()==0?View.GONE:View.VISIBLE);
        buyAgainBinding.buyAgainApiErrorView.setVisibility(isWebserviceError?View.VISIBLE:View.GONE);
    }

    @Override
    public void setBuyAgainAdapter(List<MStarProductDetails> productDetailsList, BuyAgainAdapter.BuyAgainAdapterListener listener) {
        buyAgainBinding.rvBuyAgainListView.setLayoutManager(new LinearLayoutManager(this));
        buyAgainAdapter = new BuyAgainAdapter(buyAgainViewModel.analyzeProductDetailsAndUpdateList(productDetailsList), model,listener);
        buyAgainBinding.rvBuyAgainListView.setAdapter(buyAgainAdapter);
        if(!isBtnAnimate){
            animateProceedBtn();
        }
        buyAgainBinding.proceedToCheckoutBtn.setVisibility(BasePreference.getInstance(this).getCartCount() > 0 ? View.VISIBLE : View.GONE);

    }

    @Override
    public void hideProceedbutton(String productIdList) {
        buyAgainBinding.proceedToCheckoutBtn.setVisibility(!TextUtils.isEmpty(productIdList) && BasePreference.getInstance(this).getCartCount() > 0 ? View.VISIBLE : View.GONE);
        buyAgainBinding.buyAgainEmptyView.setVisibility(TextUtils.isEmpty(productIdList) ? View.VISIBLE:View.GONE);
    }

    @Override
    public void navigateToCart() {
        startActivity(new Intent(BuyAgainActivity.this, MStarCartActivity.class));
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void animateProceedButton() {
        if(!isBtnAnimate)
            animateProceedBtn();
        buyAgainBinding.proceedToCheckoutBtn.setVisibility(BasePreference.getInstance(this).getCartCount() > 0 ? View.VISIBLE : View.GONE);
    }


    private void animateProceedBtn() {
        isBtnAnimate = true;
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        animation.setDuration(700);
        buyAgainBinding.proceedToCheckoutBtn.setAnimation(animation);
        animation.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_PRODUCTS_ORDERED);
        buyAgainViewModel.onResumeActivity();
    }

    @Override
    public void onProductView(int productCode) {
        Intent intent = new Intent(this,ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE,productCode);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY,getString(R.string.text_products_ordered));
        startActivity(intent);
    }

    @Override
    public void updateAdapter(MStarProductDetails productDetails, int productQty, int selectedQty) {
        if(buyAgainAdapter!=null){
            buyAgainAdapter.setProperQuantity(productQty,selectedQty,productDetails); // updating adapter
        }
    }


}
