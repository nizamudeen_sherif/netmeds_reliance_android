package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentOfferTabBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OfferTabFragmentViewModel;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MstarNetmedsOffer;

public class OfferTabFragment extends BaseViewModelFragment<OfferTabFragmentViewModel> implements OfferTabFragmentViewModel.OfferTabFragmentViewModelListener {


    private OfferTabFragmentViewModel viewModel;
    private FragmentOfferTabBinding binding;


    public static OfferTabFragment newInstance(String titleName, String data) {
        OfferTabFragment offerTabFragment = new OfferTabFragment();
        Bundle args = new Bundle();
        args.putString(IntentConstant.OFFER_PARTICULAR_LIST, data);
        args.putString(IntentConstant.OFFER_PAGE_TITLE, titleName);
        offerTabFragment.setArguments(args);
        return offerTabFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offer_tab, container, false);
        binding.setViewModel(onCreateViewModel());
        return binding.getRoot();
    }

    @Override
    protected OfferTabFragmentViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(OfferTabFragmentViewModel.class);
        viewModel.init(getActivity(), getArguments().getString(IntentConstant.OFFER_PARTICULAR_LIST), binding, this);
        return viewModel;
    }

    @Override
    public void onViewParticularOfferDetails(MstarNetmedsOffer resultModel) {
        Intent intent;
        if (resultModel != null && resultModel.getOfferList() != null && !resultModel.getOfferList().isEmpty()) {
            intent = new Intent(getActivity(), OfferActivity.class);
            intent.putExtra(IntentConstant.OFFER_LIST, new Gson().toJson(resultModel.getOfferList()));
            intent.putExtra(IntentConstant.OFFER_PAGE_TITLE, getArguments().getString(IntentConstant.OFFER_PAGE_TITLE));
        } else {
            intent = new Intent(getActivity(), ViewOfferDetailsActivity.class);
            intent.putExtra(IntentConstant.OFFER_DETAILS, new Gson().toJson(resultModel));
        }
        startActivity(intent);
    }
}
