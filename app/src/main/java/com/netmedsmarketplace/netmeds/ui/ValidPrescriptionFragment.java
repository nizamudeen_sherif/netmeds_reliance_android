package com.netmedsmarketplace.netmeds.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogValidPrescriptionBinding;
import com.nms.netmeds.base.BaseDialogFragment;

public class ValidPrescriptionFragment extends BaseDialogFragment {

    //Default Constructor
    public ValidPrescriptionFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogValidPrescriptionBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_valid_prescription, container, false);
        binding.closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return binding.getRoot();
    }

}
