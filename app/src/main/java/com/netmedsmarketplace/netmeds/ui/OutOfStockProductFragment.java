package com.netmedsmarketplace.netmeds.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentOutOfStockPopUpBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OutOfStockPopupViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.CartItemResult;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.List;

public class OutOfStockProductFragment extends BaseBottomSheetFragment implements OutOfStockPopupViewModel.OutOfStockPopupViewModelListener {
    private OutOfStockProductFragmentListener outOfStockProductFragmentListener;
    private FragmentOutOfStockPopUpBinding fragmentOutOfStockPopUpBinding;

    public OutOfStockProductFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            outOfStockProductFragmentListener = (OutOfStockProductFragmentListener) getActivity();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(true);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentOutOfStockPopUpBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_out_of_stock_pop_up, container, false);
        OutOfStockPopupViewModel outOfStockPopupViewModel = new OutOfStockPopupViewModel(getActivity().getApplication());
        List<CartItemResult> outOfStockProductList = (List<CartItemResult>) getArguments().getSerializable(AppConstant.KEY_OUT_OF_STOCK_LIST);
        boolean fromCartPage = getArguments() != null && getArguments().getBoolean(AppConstant.KEY_IS_FROM_CART);
        outOfStockPopupViewModel.init(getActivity(), fragmentOutOfStockPopUpBinding, outOfStockPopupViewModel, outOfStockProductList, fromCartPage, this);
        fragmentOutOfStockPopUpBinding.setViewModel(outOfStockPopupViewModel);
        return fragmentOutOfStockPopUpBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        PaymentHelper.setIsOutOfStockPopUpShown(false);
        super.onDetach();
    }

    @Override
    public void vmDismiss() {
        PaymentHelper.setIsOutOfStockPopUpShown(false);
        OutOfStockProductFragment.this.dismissAllowingStateLoss();
    }

    @Override
    public void vmSubstitute(CartItemResult cartItemResult) {
        OutOfStockProductFragment.this.dismissAllowingStateLoss();
        outOfStockProductFragmentListener.onSubstituteCallback(cartItemResult);
    }

    @Override
    public void vmRemoveAndProceed() {
        OutOfStockProductFragment.this.dismissAllowingStateLoss();
        outOfStockProductFragmentListener.onRemoveAndProceedCallback();
    }

    @Override
    public void vmShowProgress() {
        if (getActivity() != null)
            showProgress(getActivity());
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmShowAlertMessage(String message) {
        SnackBarHelper.snackBarCallBack(fragmentOutOfStockPopUpBinding.lytParent, getActivity(), message);
    }

    @Override
    public void vmRemoveOutOfStockProductFromLocal(List<CartItemResult> outOfStockProductList) {
        OutOfStockProductFragment.this.dismissAllowingStateLoss();
        outOfStockProductFragmentListener.onRemoveOutOfStockProductFromLocalCallback(outOfStockProductList);
    }

    public interface OutOfStockProductFragmentListener {

        void onSubstituteCallback(CartItemResult cartItemResult);

        void onRemoveAndProceedCallback();

        void onRemoveOutOfStockProductFromLocalCallback(List<CartItemResult> outOfStockProductList);
    }
}
