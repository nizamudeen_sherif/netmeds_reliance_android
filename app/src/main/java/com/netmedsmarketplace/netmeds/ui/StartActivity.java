package com.netmedsmarketplace.netmeds.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityStartBinding;
import com.netmedsmarketplace.netmeds.viewmodel.StartActivityViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.ConfigurationResult;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;

public class StartActivity extends BaseViewModelActivity<StartActivityViewModel> implements StartActivityViewModel.StartListener {

    private ActivityStartBinding startBinding;
    private StartActivityViewModel detailViewModel;
    private BasePreference basePreference;
    private AppUpdateManager appUpdateManager;
    private Task<AppUpdateInfo> appUpdateInfoTask;
    private static int REQUEST_CODE_FOR_IMMEDIATE = 555;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appUpdateManager = AppUpdateManagerFactory.create(this);
        basePreference = BasePreference.getInstance(this);
        appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        startBinding = DataBindingUtil.setContentView(this, R.layout.activity_start);
        startBinding.setViewModel(onCreateViewModel());
        FireBaseAnalyticsHelper.getInstance().openApp(this, AppConstant.APP_OPEN);
        initJusPay();
    }

    private void initJusPay() {
        ConfigMap configMap = ConfigMap.getInstance();
        in.juspay.godel.PaymentActivity.preFetch(this, configMap.getProperty(ConfigConstant.JUSPAY_CLIENT_ID));
    }

    private void checkAndNavigateScreen() {
        Intent intent;
        APIServiceManager.getInstance().getConfigURLPaths(BasePreference.getInstance(getApplication()));
        if (basePreference.isMstarSessionIdAvailable()) {
            intent = new Intent(StartActivity.this, NavigationActivity.class);
        } else {
            intent = new Intent(StartActivity.this, OnBoardingActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    protected StartActivityViewModel onCreateViewModel() {
        detailViewModel = ViewModelProviders.of(this).get(StartActivityViewModel.class);
        clearPreference();
        detailViewModel.init(this, basePreference);
        detailViewModel.getmStarCartDetailsMutableLiveData().observe(this, new CartCounterObserver());
        onRetry(detailViewModel);
        detailViewModel.getConfigurationResponseMutableLiveData().observe(this, new ConfigurationObserver());
        return detailViewModel;
    }

    private void clearPreference() {
        if (basePreference != null && !basePreference.getClearPreference()) {
            basePreference.clear();
            basePreference.setClearPreference(true);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showNoNetworkView(boolean isShowNoNetworkView) {
        startBinding.logo.setVisibility(isShowNoNetworkView ? View.VISIBLE : View.GONE);
        startBinding.startNetworkErrorView.setVisibility(isShowNoNetworkView ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isShowWebserviceErrorView) {
        startBinding.logo.setVisibility(isShowWebserviceErrorView ? View.GONE : View.VISIBLE);
        startBinding.startApiErrorView.setVisibility(isShowWebserviceErrorView ? View.VISIBLE : View.GONE);
    }


    private class ConfigurationObserver implements Observer<ConfigurationResponse> {

        @Override
        public void onChanged(@Nullable ConfigurationResponse configurationResponse) {
            BasePreference basePreference = BasePreference.getInstance(StartActivity.this);
            basePreference.setConfiguration(null);
            basePreference.setConfiguration(new Gson().toJson(configurationResponse));
            if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) {
                if (!TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getAndroidForceUpdateVersion()))
                    checkVersion(configurationResponse.getResult(), true);
                else checkAndNavigateScreen();
            } else checkAndNavigateScreen();
        }
    }

    private void checkVersion(ConfigurationResult result, boolean isRedirection) {
        String currentVersion = ConfigMap.getInstance().getProperty(ConfigConstant.FORCE_UPDATE_VERSION).replace("v", "");
        String newVersion = result.getConfigDetails().getAndroidForceUpdateVersion().replace("v", "");
        if (Double.parseDouble(currentVersion) < Double.parseDouble(newVersion)) {
            showForceUpdate(result);
        } else if (isRedirection) {
            if (!basePreference.getIsAppStartFirst()) {
                basePreference.setAppStartFirst(true);
                basePreference.clear();
                basePreference.setClearPreference(true);
            }
            checkAndNavigateScreen();
        }
    }

    private void showForceUpdate(ConfigurationResult result) {
        AlertDialog updateApp;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(result.getConfigDetails().getAppUpdateTitle())
                .setMessage(result.getConfigDetails().getAppUpdateMesssage())
                .setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.text_update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openStoreAction();
                dialog.dismiss();
            }
        });

        updateApp = alertDialog.create();
        updateApp.show();
    }

    private void openStoreAction() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(applicationLink())));
    }

    private String applicationLink() {
        return String.format(AppConstant.PLAY_STORE_LINK, getPackageName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForForceUpdate();
    }

    private void checkForForceUpdate() {
        BasePreference basePreference = BasePreference.getInstance(StartActivity.this);
        ConfigurationResponse response = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        if (response != null && response.getResult() != null && response.getResult().getConfigDetails() != null && !TextUtils.isEmpty(response.getResult().getConfigDetails().getAndroidForceUpdateVersion())) {
            ConfigurationResult result = response.getResult();
            checkVersion(result, false);
        }
    }

    private class CartCounterObserver implements Observer<MStarCartDetails> {

        @Override
        public void onChanged(@Nullable MStarCartDetails mStarCartDetails) {
            if (mStarCartDetails != null && mStarCartDetails.getLines() != null && !mStarCartDetails.getLines().isEmpty()) {
                BasePreference.getInstance(StartActivity.this).setCartCount(mStarCartDetails.getLines().size());
            } else {
                BasePreference.getInstance(StartActivity.this).setCartCount(0);
            }
        }
    }

}