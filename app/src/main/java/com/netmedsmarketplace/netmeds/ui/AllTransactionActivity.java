package com.netmedsmarketplace.netmeds.ui;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityAllTransactionBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AllTransactionViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarWalletHistory;

import java.util.ArrayList;

public class AllTransactionActivity extends BaseViewModelActivity<AllTransactionViewModel> {

    private ActivityAllTransactionBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_all_transaction);
        binding.setViewModel(onCreateViewModel());
        toolBarSetUp(binding.toolbar);
        initToolBar();
    }

    @Override
    protected AllTransactionViewModel onCreateViewModel() {
        AllTransactionViewModel viewModel = ViewModelProviders.of(this).get(AllTransactionViewModel.class);
        MStarBasicResponseTemplateModel response = new Gson().fromJson(getIntent().getExtras().getString(IntentConstant.TRANSACTION_RESULT), MStarBasicResponseTemplateModel.class);
        viewModel.init(this, binding, response != null && response.getResult() != null && response.getResult().getWalletHistoryList() != null && response.getResult().getWalletHistoryList().size() > 0 ? response.getResult().getWalletHistoryList() : new ArrayList<MstarWalletHistory>());
        return viewModel;
    }

    private void initToolBar() {
        binding.collapsingToolbar.setTitle(getString(R.string.text_all_transaction));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
