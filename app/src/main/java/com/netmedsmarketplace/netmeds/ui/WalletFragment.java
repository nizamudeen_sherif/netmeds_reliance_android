package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.AllTransactionAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentMyWalletBinding;
import com.netmedsmarketplace.netmeds.viewmodel.WalletViewModel;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarWalletHistory;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.webengage.sdk.android.WebEngage;

import java.util.List;

public class WalletFragment extends BaseViewModelFragment<WalletViewModel> implements WalletViewModel.WalletViewListener {

    private FragmentMyWalletBinding binding;
    private WalletViewModel viewModel;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_wallet, container, false);
        viewModel = onCreateViewModel();
        if (getActivity() != null) {
            ((NavigationActivity) getActivity()).setSupportActionBar(binding.toolbar);
            ((NavigationActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initToolBar();
        return binding.getRoot();
    }

    @Override
    protected WalletViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(WalletViewModel.class);
        viewModel.init(BasePreference.getInstance(getActivity()), this);
        viewModel.getWalletMutableLiveData().observe(this, new WalletObserver());
        onRetry(viewModel, binding.getRoot());
        return viewModel;
    }

    private void initToolBar() {
        binding.collapsingToolbar.setExpandedTitleTextAppearance(com.nms.netmeds.consultation.R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(com.nms.netmeds.consultation.R.style.CollapseTitleTextStyle);
        if (getActivity() != null) {
            binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(getActivity(), "font/Lato-Bold.ttf"));
            binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(getActivity(), "font/Lato-Bold.ttf"));
        }
    }

    @Override
    public void onViewAllTransaction() {
        Intent intent = new Intent(getActivity(), AllTransactionActivity.class);
        intent.putExtra(IntentConstant.TRANSACTION_RESULT, new Gson().toJson(viewModel.getBasicResponse()));
        startActivity(intent);
    }

    @Override
    public void onHowToUse(int stringId) {
        HowToUseFragment howToUseFragment = new HowToUseFragment(getString(stringId));
        if (getFragmentManager() != null)
            getFragmentManager().beginTransaction().add(howToUseFragment, IntentConstant.REVIEW_ORDER_AGAIN_DIALOG_FRAGMENT_TAG_NAME).commitAllowingStateLoss();
    }

    @Override
    public void onReferFriend() {
        startActivity(new Intent(getActivity(), ReferEarnActivity.class));
    }

    @Override
    public void showSnackBarMessage(int messageId) {
    }

    @Override
    public boolean isNetworkConnetedCallback() {
        return NetworkUtils.isConnected(getActivity());
    }

    @Override
    public void showNoNetworkErrorCallback(boolean isConnected) {
        binding.contentView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.myWalletNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorViewCallback(boolean isWebserviceError) {
        binding.contentView.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.myWalletApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void toolBarProperties() {
        binding.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    binding.toolbar.setVisibility(View.VISIBLE);
                } else {
                    binding.toolbar.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean availableBalanceVisibilityCallback() {
        if (!TextUtils.isEmpty(viewModel.getAvailableBalance()))
            binding.availableBalanceShimmer.shimmer.stopShimmerAnimation();
        return !TextUtils.isEmpty(viewModel.getAvailableBalance());
    }

    @Override
    public boolean nmsCashVisibilityCallback() {
        if (!TextUtils.isEmpty(viewModel.getNmsNormalCash()))
            binding.cashShimmer.shimmer.stopShimmerAnimation();
        return !TextUtils.isEmpty(viewModel.getNmsNormalCash());
    }

    @Override
    public boolean nmsSuperCashVisibilityCallback() {
        if (!TextUtils.isEmpty(viewModel.getNmsSuperCash()))
            binding.superCashShimmer.shimmer.stopShimmerAnimation();
        return !TextUtils.isEmpty(viewModel.getNmsSuperCash());

    }

    @Override
    public void initShimmer() {
        setTransactionListVisibility(false);
        binding.availableBalanceShimmer.shimmer.startShimmerAnimation();
        binding.cashShimmer.shimmer.startShimmerAnimation();
        binding.superCashShimmer.shimmer.startShimmerAnimation();
        binding.transactionListOne.shimmer.startShimmerAnimation();
        binding.transactionListTwo.shimmer.startShimmerAnimation();
        binding.transactionListThree.shimmer.startShimmerAnimation();
    }

    public void setTransactionListVisibility(boolean show) {
        binding.rvTransactionList.setVisibility(show ? View.VISIBLE : View.GONE);
        binding.transactionListShimmer.setVisibility(show ? View.GONE : View.VISIBLE);
        if (!show) {
            binding.transactionListOne.shimmer.stopShimmerAnimation();
            binding.transactionListTwo.shimmer.stopShimmerAnimation();
            binding.transactionListThree.shimmer.stopShimmerAnimation();
        }
    }

    @Override
    public void initiateAllTransactionAdapter(List<MstarWalletHistory> transactionList) {
        if (transactionList.size() != 0) {
            binding.cvAllTransaction.setVisibility(View.VISIBLE);
            AllTransactionAdapter adapter = new AllTransactionAdapter(transactionList, transactionList.size() > 3 ? 3 : transactionList.size());
            binding.rvTransactionList.setLayoutManager(new LinearLayoutManager(getActivity()));
            binding.rvTransactionList.setNestedScrollingEnabled(false);
            binding.rvTransactionList.setAdapter(adapter);
            setTransactionListVisibility(true);
        } else {
            binding.cvAllTransaction.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        if (getActivity() != null)
            GoogleAnalyticsHelper.getInstance().postScreen(getActivity(), GoogleAnalyticsHelper.POST_SCREEN_WALLETS);
    }

    class WalletObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {

            if (response != null) {
                viewModel.inflateTransactionDetails(response);
                binding.setWalletViewModel(viewModel);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_MY_WALLET);
    }
}
