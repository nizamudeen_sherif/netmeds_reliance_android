package com.netmedsmarketplace.netmeds.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MyPrescriptionAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityMyPrescriptionBinding;
import com.netmedsmarketplace.netmeds.viewmodel.MyPrescriptionViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarMyPrescription;
import com.nms.netmeds.base.retrofit.APIPathConstant;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.List;

public class MyPrescriptionView extends BaseViewModelActivity<MyPrescriptionViewModel> implements MyPrescriptionViewModel.MyPrescriptionListener, MyPrescriptionAdapter.MyPrescriptionAdapterListener {

    private ActivityMyPrescriptionBinding prescriptionBinding;
    private MyPrescriptionViewModel viewModel;
    private MstarMyPrescription rxPrescription;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prescriptionBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_prescription);
        prescriptionBinding.setPrescriptionViewModel(onCreateViewModel());
        toolBarSetUp(prescriptionBinding.toolBarCustomLayout.toolbar);
        initToolBar(prescriptionBinding.toolBarCustomLayout.collapsingToolbar,getString(R.string.text_my_prescription));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected MyPrescriptionViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(MyPrescriptionViewModel.class);
        viewModel.getResponseMutableLiveData().observe(this, new PrescriptionObserver());
        viewModel.initViewModel(BasePreference.getInstance(this), this);
        onRetry(viewModel);
        return viewModel;
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,prescriptionBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,prescriptionBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void onRetry() {
        viewModel.onRetryClickListener();
    }

    @Override
    public void downloadPrescription(MstarMyPrescription singlePrescriptionDetails) {
        String rxUrl = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.DOWNLOAD_PRESCRIPTION + singlePrescriptionDetails.getRxId();
        new MyPrescriptionViewModel.DownloadPrescriptionTask(singlePrescriptionDetails.getRxId(), rxUrl, this, this).execute();
    }

    @Override
    public void previewPrescriptionImage(String url) {
        ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(url);
        getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitAllowingStateLoss();
    }

    @Override
    public void requestPremission(MstarMyPrescription rx) {
        this.rxPrescription = rx;
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 190);
    }

    @Override
    public void finishActivity() {
        SnackBarHelper.snackBarCallBack(prescriptionBinding.prescriptionView, this, getString(R.string.text_no_past_prescription));
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 2500);
    }

    @Override
    public void viewDownloadedImage(String imagePath) {
        CommonUtils.initiateVibrate(this);
        this.imagePath = imagePath;
        SubscriptionHelper.getInstance().setEditorderquantityflag(SubscriptionHelper.getInstance().isPayNowSubscription());
        Snackbar snackbar = Snackbar.make(prescriptionBinding.prescriptionView, getString(R.string.text_download_successful), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.text_view_image), new ViewImageListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void showMessage(String mesaage) {
        SnackBarHelper.snackBarCallBack(prescriptionBinding.prescriptionView, this, getString(R.string.text_download_cancelled));
    }

    @Override
    public MyPrescriptionAdapter setPrescriptionAdapter(List<MstarMyPrescription> rxList) {
        MyPrescriptionAdapter adapter = new MyPrescriptionAdapter(rxList, this, BasePreference.getInstance(this));
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        prescriptionBinding.presRecyclerview.setLayoutManager(linearLayoutManager);
        prescriptionBinding.presRecyclerview.setAdapter(adapter);
        prescriptionBinding.presRecyclerview.addOnScrollListener(viewModel.paginationForOrderList(linearLayoutManager));
        return adapter;
    }

    @Override
    public void showNoNetworkError(boolean isNetworkError) {
        prescriptionBinding.prescriptionView.setVisibility(isNetworkError ? View.GONE : View.VISIBLE);
        prescriptionBinding.myPrescriptionNetworkErrorView.setVisibility(isNetworkError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showWebViewError(boolean isError) {
        prescriptionBinding.prescriptionView.setVisibility(isError ? View.GONE : View.VISIBLE);
        prescriptionBinding.myPrescriptionApiErrorView.setVisibility(isError ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 190) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                SnackBarHelper.snackBarCallBack(prescriptionBinding.prescriptionView, this, getString(R.string.text_write_permission_error));
            } else {
                downloadPrescription(rxPrescription);
            }
        }
    }

    private class PrescriptionObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            if (response != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(response.getStatus()) && response.getResult() != null) {
                viewModel.onDetailsAvailable(response);
                prescriptionBinding.setPrescriptionViewModel(viewModel);
            }

        }
    }

    private class ViewImageListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Uri uri = Uri.parse(Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/" + imagePath);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "image/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
