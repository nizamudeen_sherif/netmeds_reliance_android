package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogOrderReviewBinding;
import com.netmedsmarketplace.netmeds.viewmodel.DialogOrderReviewViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;

@SuppressLint("ValidFragment")
public class OrderReviewDialog extends BaseBottomSheetFragment implements DialogOrderReviewViewModel.DialogOrderReviewViewModelListener {

    private int ratingCount;
    private OrderReviewDialogListener listener;

    public OrderReviewDialog(int ratingCount, OrderReviewDialogListener listener) {
        this.ratingCount = ratingCount;
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogOrderReviewBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_order_review, container, false);
        DialogOrderReviewViewModel viewModel = ViewModelProviders.of(this).get(DialogOrderReviewViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(getActivity(), binding, this, ratingCount);
        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    @Override
    public void onSubmitReviewWithFeedBack(String reviewFeedback, int rating) {
        this.dismissAllowingStateLoss();
        listener.submitReview(reviewFeedback, ratingCount);
    }

    @Override
    public void onCancelReview() {
        this.dismissAllowingStateLoss();
        listener.cancelRating();
    }

    public interface OrderReviewDialogListener {
        void submitReview(String feedback, int ratingCount);

        void cancelRating();
    }
}
