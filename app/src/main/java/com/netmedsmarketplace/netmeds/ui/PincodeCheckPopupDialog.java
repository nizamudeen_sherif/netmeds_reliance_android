package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogPincodeCheckPoupBinding;
import com.netmedsmarketplace.netmeds.viewmodel.PincodeCheckPopupDialogViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.DeliveryEstimateProductDetail;
import com.nms.netmeds.base.model.PincodeDeliveryEstimate;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;

@SuppressLint("ValidFragment")
public class PincodeCheckPopupDialog extends BaseBottomSheetFragment implements PincodeCheckPopupDialogViewModel.PincodeCheckPopupDialogViewModelCallBack {

    private DialogPincodeCheckPoupBinding binding;
    private PincodeCheckPopupDialogViewModel viewModel;
    private PincodeCheckPopupDialogCallBack callBack;
    private BasePreference basePreference;

    private boolean isFromColdStorageCheck;
    private int productCode;
    private int selectedQuantity;
    private String productName;

    //Constructor function with checkExpiry and checkDeliveryDate
    public PincodeCheckPopupDialog(PincodeCheckPopupDialogCallBack callBack, boolean isFromColdStorageCheck, int productCode, int selectedQuantity, String productName) {
        this.isFromColdStorageCheck = isFromColdStorageCheck;
        this.callBack = callBack;
        this.productCode = productCode;
        this.selectedQuantity = selectedQuantity;
        this.productName = productName;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_pincode_check_poup, container, false);
        viewModel = ViewModelProviders.of(this).get(PincodeCheckPopupDialogViewModel.class);
        binding.setViewModel(viewModel);
        basePreference = BasePreference.getInstance(getActivity());
        viewModel.init(this, basePreference, productCode, selectedQuantity);
        return binding.getRoot();
    }

    @Override
    public void initiateViewBasedOnFlow() {
        binding.description.setVisibility(isFromColdStorageCheck ? View.VISIBLE : View.GONE);
        binding.title.setText(getActivity().getString(isFromColdStorageCheck ? R.string.text_check_available : R.string.text_check_expiry_delivery_dates));
        binding.checkAvailability.setText(getActivity().getString(isFromColdStorageCheck ? R.string.text_check_available : R.string.text_check_expiry_delivery_dates));
        binding.pincode.setText(!TextUtils.isEmpty(basePreference.getPinCode()) ? basePreference.getPinCode() : "");
    }

    @Override
    public boolean isEnterPincodeIsValid() {
        return !ValidationUtils.checkPinCode(binding.pincode, true, getActivity().getString(R.string.text_error_pincode), getActivity().getString(R.string.text_valid_pincode), 6, binding.textError);
    }

    public void imeOptionButtonClick() {
        binding.pincode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (getActivity() != null) {
                        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.toggleSoftInput(0, 0);
                    }
                    if (!isEnterPincodeIsValid()) {
                        viewModel.onclickCheckAvailability();
                        handled = true;
                    }

                }
                return handled;
            }
        });
    }

    @Override
    public void clickDismiss() {
        this.dismissAllowingStateLoss();
    }

    public void onPincodeError(String errorMessage, String specialMessage) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null) {
            if (getActivity() != null && !TextUtils.isEmpty(errorMessage) && errorMessage.equalsIgnoreCase(getActivity().getString(R.string.text_cold_storage_not_supported))) {
                binding.textError.setText(CommonUtils.fromHtml(getActivity().getString(R.string.text_cold_storage_default_error_message)));
            } else if (getActivity() != null && !TextUtils.isEmpty(errorMessage) && errorMessage.equalsIgnoreCase(getActivity().getString(R.string.text_a_status_blockedpin))) {
                binding.textError.setText(CommonUtils.fromHtml(specialMessage));
            } else {
                binding.textError.setText(getActivity().getString(R.string.text_product_delivery_default_error_message));
            }
        }
    }

    @Override
    public void vmShowProgress() {
        callBack.showProgress();
    }

    @Override
    public void dismissLoader() {
        callBack.dismissLoader();
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(getActivity());
    }

    @Override
    public String getEnteredPinCode() {
        return binding.pincode.getText().toString();
    }

    @Override
    public void onAvailabilityCheck(boolean isAvailable, DeliveryEstimateProductDetail expiryDetails, PincodeDeliveryEstimate delieryDetails) {
        dismissLoader();
        clickDismiss();
        if (isFromColdStorageCheck) {
            callBack.pinCodeSupportColdStorage(isAvailable);
        } else {
            callBack.expiryAndDeliveryEstimate(expiryDetails, delieryDetails);
        }
        /*WebEngage Pin code Entered Event*/
        WebEngageHelper.getInstance().pinCodeEnteredEvent(getActivity(), getEnteredPinCode(), productName, delieryDetails != null && delieryDetails.getFormatted() != null && !TextUtils.isEmpty(delieryDetails.getFormatted().getFormat2()) ? delieryDetails.getFormatted().getFormat2() : "");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callBack.pinCodeCheckPopupClose();
    }

    public interface PincodeCheckPopupDialogCallBack {
        void showProgress();

        void dismissLoader();

        void pinCodeSupportColdStorage(boolean isAvailable);

        void expiryAndDeliveryEstimate(DeliveryEstimateProductDetail expiryDetails, PincodeDeliveryEstimate delieryDetails);

        void pinCodeCheckPopupClose();
    }
}
