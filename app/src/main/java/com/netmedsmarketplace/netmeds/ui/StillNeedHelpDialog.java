package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogStillNeedHelpBinding;
import com.netmedsmarketplace.netmeds.viewmodel.StillNeedHelpViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;

public class StillNeedHelpDialog extends BaseBottomSheetFragment implements StillNeedHelpViewModel.StillNeedHelpListener {

    private StillNeedHelpListener listener;
    private DialogStillNeedHelpBinding binding;

    //Default Constructor
    public StillNeedHelpDialog() {
    }

    @SuppressLint("ValidFragment")
    public StillNeedHelpDialog(StillNeedHelpListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_still_need_help, container, false);
        StillNeedHelpViewModel viewModel = ViewModelProviders.of(this).get(StillNeedHelpViewModel.class);
        viewModel.init(this, isChatEnable());
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    private boolean isChatEnable() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(getContext()).getConfiguration(), ConfigurationResponse.class);
        return configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().isKaptureChatEnable();
    }

    @Override
    public void closeDialog() {
        dismissDialog();
    }

    private void dismissDialog() {
        dismiss();
    }

    @Override
    public void eMail() {
        dismissDialog();
        listener.initEmail();
    }

    @Override
    public void call() {
        dismissDialog();
        listener.initCall();
    }

    @Override
    public boolean checkCallPermissionEnable() {
        return listener.checkCallPermissionEnable();
    }

    @Override
    public void requestEnableCallPermission() {
        listener.requestEnableCallPermission();
    }

    @Override
    public void openKapChat() {
        dismissDialog();
        listener.openKapChat();
    }

    public interface StillNeedHelpListener {

        void initEmail();

        void initCall();

        boolean checkCallPermissionEnable();

        void requestEnableCallPermission();

        void openKapChat();
    }
}