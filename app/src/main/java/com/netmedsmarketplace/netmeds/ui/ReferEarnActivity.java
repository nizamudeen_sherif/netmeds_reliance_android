package com.netmedsmarketplace.netmeds.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityReferEarnBinding;
import com.netmedsmarketplace.netmeds.viewmodel.ReferEarnViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;

public class ReferEarnActivity extends BaseViewModelActivity<ReferEarnViewModel> implements ReferEarnViewModel.ReferEarnListener {
    private ActivityReferEarnBinding activityReferEarnBinding;
    private ReferEarnViewModel referEarnViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BasePreference.getInstance(this).isGuestCart()) {
            navigateToSignIn();
            return;
        }
        activityReferEarnBinding = DataBindingUtil.setContentView(this, R.layout.activity_refer_earn);
        activityReferEarnBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(activityReferEarnBinding.toolBarCustomLayout.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        initToolBar(activityReferEarnBinding.toolBarCustomLayout.collapsingToolbar,getString(R.string.text_refer_and_earn));
        setTermsAndCondition();
    }

    @Override
    protected ReferEarnViewModel onCreateViewModel() {
        referEarnViewModel = ViewModelProviders.of(this).get(ReferEarnViewModel.class);
        referEarnViewModel.init(this, BasePreference.getInstance(this));
        onRetry(referEarnViewModel);
        referEarnViewModel.getReferralCodeResponseMutableLiveData().observe(this, new ReferralObserver());
        return referEarnViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                ReferEarnActivity.this.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void vmShowProgress() {
       showHorizontalProgressBar(false,activityReferEarnBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,activityReferEarnBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void shareIntent(String title, String mailContent, String message, String url) {
        PackageManager pm = this.getPackageManager();
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        List<ResolveInfo> resInfo = pm.queryIntentActivities(sharingIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<>();
        for (int i = 0; i < resInfo.size(); i++) {
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("whatsapp")) {
                switch (packageName) {
                    case "com.twitter.android":
                        intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + url);
                        fireBaseEvent(AppConstant.TWITTER);
                        break;
                    case "com.facebook.katana":
                        fireBaseEvent(AppConstant.FACEBOOK);
                        intent.putExtra(Intent.EXTRA_TEXT, url);
                        break;
                    case "com.google.android.gm":
                        fireBaseEvent(AppConstant.GMAIL);
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, title != null && !TextUtils.isEmpty(title) ? title : "");
                        intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(mailContent != null && !TextUtils.isEmpty(mailContent) ? mailContent : ""));
                        intent.setType("message/rfc822");
                        break;
                    case "com.whatsapp":
                        fireBaseEvent(AppConstant.WHATSAPP);
                        intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + url);
                        intent.setPackage(packageName);
                        break;
                    case "com.android.mms":
                        fireBaseEvent(AppConstant.SMS);
                        intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + url);
                        intent.setPackage(packageName);
                        break;
                }
            } else {
                intent.putExtra(Intent.EXTRA_TEXT, title + "\n" + url);
            }
            intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
        }
        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);
        Intent openInChooser = Intent.createChooser(sharingIntent, getString(R.string.text_invite_friends));
        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }

    private void fireBaseEvent(String method) {
        /*FireBase Analytics Event*/
        FireBaseAnalyticsHelper.getInstance().shareEvent(this, AppConstant.REFERRAL_SHARE, !TextUtils.isEmpty(referEarnViewModel.referralCode()) ? referEarnViewModel.referralCode() : "", !TextUtils.isEmpty(method) ? method : "");
    }

    @Override
    public boolean isNetworkConnectCallback() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void showNoNetworkErrorCallback(boolean isConnected) {
        activityReferEarnBinding.referEarnViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        activityReferEarnBinding.referEarnNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorViewCallback(boolean isWebserviceError) {
        activityReferEarnBinding.referEarnViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        activityReferEarnBinding.referEarnApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void copyReferalCode() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", referEarnViewModel.referralCode());
        clipboard.setPrimaryClip(clip);
        Snackbar snackbar = Snackbar.make(activityReferEarnBinding.referEarnViewContent, getApplicationContext().getString(R.string.text_code_copied), Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.show();
    }


    private void setTermsAndCondition() {
        final SpannableString clickableText = new SpannableString(getString(R.string.text_tap_enter_into));
        clickableText.setSpan(getSpanClick(), 57, 76, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickableText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorLightBlueGrey)), 57, 76, 0);
        activityReferEarnBinding.terms.setText(clickableText);
        activityReferEarnBinding.terms.setMovementMethod(LinkMovementMethod.getInstance());
        activityReferEarnBinding.terms.setHighlightColor(Color.TRANSPARENT);
    }

    private ClickableSpan getSpanClick() {
        return new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(ReferEarnActivity.this).getConfiguration(), ConfigurationResponse.class);
                String url = (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getReferralTerms())) ? configurationResponse.getResult().getConfigDetails().getReferralTerms() : "";
                startWebPage(url, getString(R.string.text_tap_enter_into).substring(57, 76));
            }
        };
    }

    private void startWebPage(String url, String title) {
        Intent intent = new Intent(this, NetmedsWebViewActivity.class);
        intent.putExtra(IntentConstant.WEB_PAGE_URL, url);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, title);
        startActivity(intent);
    }

    private class ReferralObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            referEarnViewModel.onReferralAvailable(response);
            activityReferEarnBinding.setViewModel(referEarnViewModel);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_REFER_AND_EARN);
    }

    private void navigateToSignIn() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LaunchIntentManager.routeToActivity(this.getString(R.string.route_sign_in_activity), intent, this);
        ReferEarnActivity.this.finish();
    }
}
