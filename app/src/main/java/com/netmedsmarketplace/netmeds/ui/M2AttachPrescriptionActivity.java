package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.BuildConfig;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.M2AttachPrescriptionAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityM2AttachPrescriptionBinding;
import com.netmedsmarketplace.netmeds.viewmodel.M2AttachPrescriptionViewModel;
import com.nms.netmeds.base.BaseUploadPrescription;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FacebookPixelHelper;
import com.nms.netmeds.base.utils.FileUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class M2AttachPrescriptionActivity extends BaseUploadPrescription<M2AttachPrescriptionViewModel> implements M2AttachPrescriptionViewModel.AttachPrescriptionListener,
        M2AttachPrescriptionAdapter.PrescriptionListener, PastPrescriptionFragment.SelectedPrescriptionListener, SnackBarHelper.SnackBarHelperListener {

    private ActivityM2AttachPrescriptionBinding attachPrescriptionBinding;
    private M2AttachPrescriptionViewModel attachPrescriptionViewModel;
    private M2AttachPrescriptionAdapter attachPrescriptionAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        attachPrescriptionBinding = DataBindingUtil.setContentView(this, R.layout.activity_m2_attach_prescription);
        attachPrescriptionBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(attachPrescriptionBinding.toolBarCustomLayout.toolbar);
        initToolBar(attachPrescriptionBinding.toolBarCustomLayout.collapsingToolbar,M2AttachPrescriptionActivity.this.getResources().getString(R.string.text_attach_prescription));
    }

    @Override
    protected M2AttachPrescriptionViewModel onCreateViewModel() {
        attachPrescriptionViewModel = ViewModelProviders.of(this).get(M2AttachPrescriptionViewModel.class);
        attachPrescriptionViewModel.initViewModel(this, BasePreference.getInstance(this));
        onRetry(attachPrescriptionViewModel);
        return attachPrescriptionViewModel;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (checkPermissionResponse(grantResults)) {
            switch (requestCode) {
                case PermissionConstants.CAMERA_PERMISSION:
                    launchCameraIntent(getContentUri());
                    break;
                case PermissionConstants.GALLERY_PERMISSION:
                    launchGalleryIntent();
                    break;
            }
        } else {
            SnackBarHelper.snackBarCallBackSetAction(attachPrescriptionBinding.mainLayout, this, (requestCode == PermissionConstants.CAMERA_PERMISSION ? (getResources().getString(R.string.text_camera_permission_alert)) :
                    (getResources().getString(R.string.text_gallery_permission_alert))), "view", this);
        }
    }

    private Uri getContentUri() {
        File photo = new File(FileUtils.getTempDirectoryPath(this), FileUtils.TEMP_DIRECTORY_PATH);
        return FileProvider.getUriForFile(this, String.format("%s%s", BuildConfig.APPLICATION_ID, FileUtils.PROVIDER), photo);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case PermissionConstants.ACTION_IMAGE_CAPTURE:
                attachPrescriptionViewModel.setCapturedImage(resultCode);
                break;
            case PermissionConstants.ACTION_GALLERY_REQUEST:
                attachPrescriptionViewModel.setGalleryImage(resultCode, data);
                break;
        }
    }

    @Override
    public void setPrescriptionListAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(prescriptionArrayList);
        if (attachPrescriptionAdapter != null) {
            attachPrescriptionAdapter.notifyDataSetChanged();
        } else initAdapter(prescriptionArrayList);
    }


    private void initAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        attachPrescriptionAdapter = new M2AttachPrescriptionAdapter(prescriptionArrayList, this, false);
        attachPrescriptionBinding.prescriptionList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        attachPrescriptionBinding.prescriptionList.setAdapter(attachPrescriptionAdapter);
    }

    private LinearLayoutManager setLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    @Override
    public void initCameraIntent() {
        launchCameraIntent(getContentUri());
    }

    @Override
    public void setDigitizedPrescription(GetPastPrescriptionResult result) {
        PastPrescriptionFragment pastPrescriptionFragment = new PastPrescriptionFragment(this, this, result);
        getSupportFragmentManager().beginTransaction().add(pastPrescriptionFragment, "PastPrescriptionFragment").commitAllowingStateLoss();
    }

    @Override
    public void launchOrderConfirmation() {
        startActivity(new Intent(this, M2SelectAddOrCallActivity.class));
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,attachPrescriptionBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,attachPrescriptionBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void dismissSnackBar() {
        SnackBarHelper.dismissSnackBar();
    }

    @Override
    public void setError(String error) {
        SnackBarHelper.snackBarCallBack(attachPrescriptionBinding.mainLayout, this, error);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showWebserviceErrorView(boolean isWebServiceError) {
        attachPrescriptionBinding.mainLayout.setVisibility(isWebServiceError ? View.GONE : View.VISIBLE);
        attachPrescriptionBinding.continuebtnLayout.setVisibility(isWebServiceError ? View.GONE : View.VISIBLE);
        attachPrescriptionBinding.m2AttachApiErrorView.setVisibility(isWebServiceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        attachPrescriptionBinding.mainLayout.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.continuebtnLayout.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.m2AttachNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void enablePrescriptionView(boolean isEnable) {
        attachPrescriptionBinding.m2ViewDivider.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        attachPrescriptionBinding.prescriptionList.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        isContinueButtonEnabled(isEnable);
    }

    @Override
    public ViewGroup getViewGroup() {
        return attachPrescriptionBinding.mainLayout;
    }

    @Override
    public void resetPrescriptionAdapter(ArrayList<MStarUploadPrescription> prescriptionArrayList, int position) {
        if (prescriptionArrayList.isEmpty()) {
            enablePrescriptionView(false);
            MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(new ArrayList<String>());
        } else {
            enablePrescriptionView(true);
        }
        attachPrescriptionAdapter.notifyItemRemoved(position);
        attachPrescriptionAdapter.notifyItemRangeRemoved(position, prescriptionArrayList.size());
        attachPrescriptionViewModel.isFromContinueButton = false;
        attachPrescriptionViewModel.updatePrescription();
    }

    @Override
    public void isContinueButtonEnabled(boolean isEnable) {
        attachPrescriptionBinding.continueButton.setEnabled(isEnable);
        attachPrescriptionBinding.continueButton.setBackground(isEnable ? getApplication().getResources().getDrawable(R.drawable.accent_button) : getApplication().getResources().getDrawable(R.drawable.secondary_button));
    }

    @Override
    public void clickOnPrescriptionToPreview(Bitmap bitmap, int pos) {
        launchPreviewPrescription(bitmap);
    }

    @Override
    public void removePrescription(int position, ArrayList<MStarUploadPrescription> prescriptionArrayList) {
        if (prescriptionArrayList.isEmpty()) {
            attachPrescriptionBinding.continueButton.setEnabled(false);
            attachPrescriptionBinding.continueButton.setBackground(getResources().getDrawable(R.drawable.secondary_button));
        } else {
            attachPrescriptionBinding.continueButton.setEnabled(true);
            attachPrescriptionBinding.continueButton.setBackground(getResources().getDrawable(R.drawable.accent_button));
        }
        attachPrescriptionViewModel.resetPrescriptionAdapter(position, prescriptionArrayList);
    }

    @Override
    public void addPrescription() {

    }

    @Override
    public void selectedPrescription(List<MStarUploadPrescription> prescription) {
        ArrayList<Bitmap> prescriptionBitmapList = new ArrayList<>();
        ArrayList<String> prescriptionList = new ArrayList<>();
        for (MStarUploadPrescription list : prescription) {
            if (list.isChecked()) {
                prescriptionBitmapList.add(list.getBitmapImage());
                prescriptionList.add(list.getUploadedPrescriptionId());
            }
        }
        attachPrescriptionViewModel.setPastPrescription(prescription);
    }

    @Override
    public void launchPreviewPrescription(Object previewImage) {
        if (getSupportFragmentManager().findFragmentByTag("ImagePreviewDialog") == null) {
            ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(previewImage);
            getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitAllowingStateLoss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void snackBarOnClick() {
        navigateToSetting();
    }

    @Override
    public void onBackPressed() {
        M2Helper.getInstance().setM2Order(false);
        MstarPrescriptionHelper.getInstance().setMStarUploadPrescriptionList(new ArrayList<MStarUploadPrescription>());
        MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(new ArrayList<String>());
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dismissProgress();
        isContinueButtonEnabled(true);
        /*Facebook Pixel Checkout Event*/
        FacebookPixelHelper.getInstance().logCheckoutEvent(this, FacebookPixelHelper.EVENT_PARAM_CHECKOUT_PRESCRIPTION, "");
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_ATTACH_PRESCRIPTION_M2);
    }
}
