package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityNeedHelpBinding;
import com.netmedsmarketplace.netmeds.model.FAQCategoryModel;
import com.netmedsmarketplace.netmeds.viewmodel.NeedHelpViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.io.Serializable;
import java.util.List;

public class NeedHelpActivity extends BaseViewModelActivity<NeedHelpViewModel> implements NeedHelpViewModel.NeedHelpViewModelListener {

    private NeedHelpViewModel viewModel;
    private ActivityNeedHelpBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_need_help);
        initToolBar();
        onCreateViewModel();
        toolBarSetUp(binding.toolbar);
    }

    @Override
    protected NeedHelpViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(NeedHelpViewModel.class);
        viewModel.init(this, binding, this, getIntent());
        viewModel.getFAQCategoryMutableData().observe(this, new FAQCategoryObserver());
        onRetry(viewModel);
        return viewModel;
    }

    private void initToolBar() {
        binding.collapsingToolbar.setTitle(getString(R.string.text_need_help));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public void showCategoryContent(FAQCategoryModel model) {
        Intent intent;
        if (viewModel.isSubCategoryFound(model)) {
            intent = new Intent(this, NeedHelpActivity.class);
            intent.putExtra(IntentConstant.FAQ_CATEGORY_LIST, (Serializable) viewModel.getSubCategoryListToShareToIntent(model));
        } else {
            intent = new Intent(this, FAQMessageActivity.class);
            intent.putExtra(IntentConstant.FAQ_CONTENT_DATA, new Gson().toJson(model));
        }
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_HELPS);
    }

    private class FAQCategoryObserver implements Observer<List<FAQCategoryModel>> {
        @Override
        public void onChanged(@Nullable List<FAQCategoryModel> categoryList) {
            viewModel.loadIteratedCategoryList(categoryList);
            binding.setViewModel(viewModel);
        }
    }
}
