package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogNeedHelpEmailBinding;
import com.netmedsmarketplace.netmeds.viewmodel.NeedHelpThroughEmailDialogViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;

@SuppressLint("ValidFragment")
public class NeedHelpThroughEmailDialog extends BaseBottomSheetFragment implements NeedHelpThroughEmailDialogViewModel.NeedHelpThroughEmailDialogViewModelListener {


    private DialogNeedHelpEmailBinding binding;
    private NeedHelpThroughEmailDialogListener listener;
    private String subject;

    public NeedHelpThroughEmailDialog() {
    }

    public NeedHelpThroughEmailDialog(NeedHelpThroughEmailDialogListener listener, String subject) {
        this.listener = listener;
        this.subject = subject;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_need_help_email, container, false);
        NeedHelpThroughEmailDialogViewModel viewModel = ViewModelProviders.of(this).get(NeedHelpThroughEmailDialogViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(binding, this, subject);
        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        return dialog;
    }

    @Override
    public void onSubmit(String reason) {
        dismiss();
        if (listener != null)
            listener.onSubmitReason(reason);
    }

    @Override
    public void onCancel() {
        dismiss();
        if (listener != null)
            listener.onCancelEmailEnquiry();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getActivity(), getString(R.string.text_description_empty), Toast.LENGTH_SHORT).show();
    }

    public interface NeedHelpThroughEmailDialogListener {

        void onSubmitReason(String reason);

        void onCancelEmailEnquiry();
    }

}
