package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.JavaScriptInterface;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityNetmedsWebViewBinding;
import com.netmedsmarketplace.netmeds.viewmodel.NetmedsWebViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseWebViewActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DeepLinkConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.ArrayList;

public class NetmedsWebViewActivity extends BaseWebViewActivity implements NetmedsWebViewModel.webViewCallback, JavaScriptInterface.InterfaceCallback {
    private ActivityNetmedsWebViewBinding binding;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_netmeds_web_view);
        context = this;
        toolBarSetUp(binding.toolbar);
        setWebView(binding.webview);
        setWebView(binding.webviewVideo);
        initJavaScriptCallBack(binding.webview);
        intentValue();
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.hasExtra("WEB_PAGE_URL")) {
            loadURL(getIntent().getStringExtra("WEB_PAGE_URL"));
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(intent.getExtras() != null && intent.hasExtra("PAGE_TITLE_KEY") ? intent.getStringExtra("PAGE_TITLE_KEY") : "");
                getSupportActionBar().setTitle(intent.getExtras() != null && intent.hasExtra(AppConstant.BANNER_TITLE) ? intent.getStringExtra(AppConstant.BANNER_TITLE) : "");
            }
            checkIntentFromHealthArticle(intent);
        } else
            onNewIntent(intent);
    }

    private void setToolbarText(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
        else
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));

        menu.getItem(0).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    private void checkIntentFromHealthArticle(Intent intent) {
        if (intent.hasExtra("IS_FROM_ARTICLE") && intent.getBooleanExtra("IS_FROM_ARTICLE", false))
            hideToolbarTitle();
    }

    protected void onNewIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null && intent.hasExtra("extraPathParams")) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get("extraPathParams");
            pathParamCheck(intentParamList);
        }
    }

    private void pathParamCheck(ArrayList<String> intentParamList) {
        if (intentParamList != null && intentParamList.size() > 0) {
            setUrl(intentParamList.get(0));
            checkTitleFromPathParam(intentParamList);
        }
    }

    private void checkTitleFromPathParam(ArrayList<String> intentParamList) {
        if (intentParamList.size() > 1)
            setToolbarText(intentParamList.get(1));
        //initToolBar(binding.collapsingToolbar, intentParamList.get(1));
    }

    private void setUrl(String path) {
        ConfigMap getConfig = ConfigMap.getInstance();
        switch (path) {
            case DeepLinkConstant.ABOUT_US_URL:
            case DeepLinkConstant.HEALTH_LIBRARY_URL:
                loadURL(getConfig.getProperty(ConfigConstant.WEBSITE_DOMAIN) + path);
                if (path.equals(DeepLinkConstant.HEALTH_LIBRARY_URL))
                    hideToolbarTitle();
                break;
            case DeepLinkConstant.FAQS_URL:
                loadURL(getConfig.getProperty(ConfigConstant.FAQ_URL));
                setToolbarText(context.getResources().getString(com.nms.netmeds.payment.R.string.title_faq));
                //initToolBar(binding.collapsingToolbar, context.getResources().getString(com.nms.netmeds.payment.R.string.title_faq));
                break;
            case DeepLinkConstant.PRIVACY_POLICY:
                loadURL(getConfig.getProperty(ConfigConstant.PRIVACY_POLICY));
                setToolbarText(context.getResources().getString(com.nms.netmeds.payment.R.string.title_privacy_policy));
                //initToolBar(binding.collapsingToolbar, context.getResources().getString(com.nms.netmeds.payment.R.string.title_privacy_policy));
                break;
            case DeepLinkConstant.TERMS_AND_CONDITIONS:
                loadURL(getConfig.getProperty(ConfigConstant.TERMS_CONDITION));
                context.getResources().getString(com.nms.netmeds.payment.R.string.title_terms_and_conditions);
                //initToolBar(binding.collapsingToolbar, context.getResources().getString(com.nms.netmeds.payment.R.string.title_terms_and_conditions));
                break;
            case DeepLinkConstant.REWARD_POINTS_URL:
                loadURL(getConfig.getProperty(ConfigConstant.TERMS_CONDITION));
                context.getResources().getString(com.nms.netmeds.payment.R.string.title_reward_points);
                //initToolBar(binding.collapsingToolbar, context.getResources().getString(com.nms.netmeds.payment.R.string.title_reward_points));
                break;
            case DeepLinkConstant.SPECIAL_OFFERS:
                loadURL(getConfig.getProperty(ConfigConstant.MOBILE_DOMAIN) + path);
                hideToolbarTitle();
                break;
            default:
                loadURL(getConfig.getProperty(ConfigConstant.HEALTH_LIBRARY_URL) + "/post/" + path);
                hideToolbarTitle();
                break;
        }
    }

    void loadURL(String url) {
        boolean isYoutubeUrl = url.contains(AppConstant.YOUTUBE);
        binding.slWebview.setVisibility(isYoutubeUrl ? View.GONE : View.VISIBLE);
        binding.flWebviewVideo.setVisibility(isYoutubeUrl ? View.VISIBLE : View.GONE);
        if (isYoutubeUrl)
            binding.webviewVideo.loadUrl(url);
        else
            binding.webview.loadUrl(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                /*Google Analytics Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(NetmedsWebViewActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_CART_ICON_PRODUCT_DETAILS, GoogleAnalyticsHelper.EVENT_LABEL_PRODUCT_DETAILS_PAGE);
                startActivity(new Intent(this, MStarCartActivity.class));
                return true;
            default:
                return false;
        }
    }


    @Override
    public void onBackPressed() {
        if (binding.webview.canGoBack()) {
            binding.webview.goBack();
        } else {
            finish();
        }
    }

    /**
     * Method to inject the java script call back to the web view
     */
    private void initJavaScriptCallBack(WebView webView) {
        JavaScriptInterface javaScriptInterface = new JavaScriptInterface(NetmedsWebViewActivity.this, this);
        webView.addJavascriptInterface(javaScriptInterface, NetmedsWebViewActivity.this.getResources().getString(R.string.java_script_interface));
        webView.addJavascriptInterface(javaScriptInterface, NetmedsWebViewActivity.this.getResources().getString(R.string.java_script_webview_interface));
    }

    private void hideToolbarTitle() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        binding.appBar.setExpanded(false, false);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void dismissLoader() {
        dismissProgress();
    }

    @Override
    public void showMessage(int resId, String reason) {
        SnackBarHelper.snackBarCallBack(binding.clWebviewParent, this, resId != 0 ? getString(resId) : !TextUtils.isEmpty(reason) ? reason : "");
    }

    @Override
    public void showLoader() {
        showProgress(this);
    }

    @Override
    public void showAddCartSuccessfullMsg() {
        CommonUtils.initiateVibrate(this);
        Snackbar snackbar = Snackbar.make(binding.clWebviewParent, getString(R.string.text_added_to_cart), Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.text_view_cart), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void addToCart(String productCode) {
        new NetmedsWebViewModel(getApplication()).addProductToCart(productCode, BasePreference.getInstance(this), this);
    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(NetmedsWebViewActivity.this, MStarCartActivity.class);
            startActivity(intent);
        }
    }
}
