package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySocialLoginOtpBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SocialLoginOtpViewModel;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

import static com.nms.netmeds.base.IntentConstant.PHONE_NUMBER;

public class SocialLoginOtpActivity extends BaseViewModelActivity<SocialLoginOtpViewModel> implements SocialLoginOtpViewModel.SocialLoginOtpListener, AppViewModel.CountDownTimerListener {

    private final int ACCOUNT_FRAGMENT_REQUEST_CODE = 12345;
    private ActivitySocialLoginOtpBinding activitySocialLoginOtpBinding;
    private SocialLoginOtpViewModel socialLoginOtpViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySocialLoginOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_social_login_otp);
        activitySocialLoginOtpBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(activitySocialLoginOtpBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected SocialLoginOtpViewModel onCreateViewModel() {
        socialLoginOtpViewModel = ViewModelProviders.of(this).get(SocialLoginOtpViewModel.class);
        socialLoginOtpViewModel.init(activitySocialLoginOtpBinding, this, getIntent());
        onRetry(socialLoginOtpViewModel);
        vmCounterTimer();
        return socialLoginOtpViewModel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCOUNT_FRAGMENT_REQUEST_CODE) {
            if (resultCode == IntentConstant.UPDATE_MOBILE_NO_RESULT_OK) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(IntentConstant.PHONE_NUMBER, data.getExtras().get(IntentConstant.PHONE_NUMBER).toString());
                setResult(IntentConstant.UPDATE_MOBILE_NO_RESULT_OK, resultIntent);
                finish();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void vmCallBackOnNavigationMobileNumberActivity() {
        if (getIntent().hasExtra(IntentConstant.FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY)) {
            Intent intent = new Intent(SocialLoginOtpActivity.this, MobileNumberActivity.class);
            intent.putExtra(IntentConstant.SOCIAL_LOGIN_FLAG, true);
            intent.putExtra(IntentConstant.FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY, getIntent().hasExtra(IntentConstant.FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY));
            intent.putExtra(IntentConstant.PHONE_NUMBER, getIntent().getStringExtra(PHONE_NUMBER));
            intent.putExtra(IntentConstant.MOBILE_NO_UPDATE_FLAG, true);
            startActivityForResult(intent, ACCOUNT_FRAGMENT_REQUEST_CODE);
        } else if (socialLoginOtpViewModel.isFromSocialLogin()) {
            Intent intent = new Intent(SocialLoginOtpActivity.this, MobileNumberActivity.class);
            intent.putExtra(IntentConstant.SOCIAL_LOGIN_FLAG, true);
            intent.putExtra(IntentConstant.DOMAIN, socialLoginOtpViewModel.getSocialFlag());
            intent.putExtra(IntentConstant.TOKEN, socialLoginOtpViewModel.getAccessToken());
            startActivity(intent);
            SocialLoginOtpActivity.this.finish();
        } else {
            startActivity(new Intent(SocialLoginOtpActivity.this, MobileNumberActivity.class));
            SocialLoginOtpActivity.this.finish();
        }
    }

    public void onPasswordEditTextChanged() {
        activitySocialLoginOtpBinding.inputPassword.setError(null);
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    private boolean isPrimeNavigation() {
        return getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false);
    }

    @Override
    public void vmNavigateToHome() {
        if (isPrimeNavigation()) {
            primeNavigation();
        } else {
            Intent intent = new Intent(this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
        }
    }

    private void primeNavigation() {
        Intent intent = new Intent(this, PrimeMemberShipActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void vmCounterTimer() {
        socialLoginOtpViewModel.onCountDownTimer(this);
    }

    @Override
    public void vmCallbackSignInWithPasswordActivity() {
        Intent intent = new Intent(this, SignInWithPasswordActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, isPrimeNavigation());
        intent.putExtra(PHONE_NUMBER, socialLoginOtpViewModel.getPhoneNo());
        startActivity(intent);
        SocialLoginOtpActivity.this.finish();
    }

    @Override
    public void vmUpdateMobileNO() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(IntentConstant.PHONE_NUMBER, socialLoginOtpViewModel.getPhoneNo());
        setResult(IntentConstant.UPDATE_MOBILE_NO_RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public Context getContext() {
        return SocialLoginOtpActivity.this;
    }

    @Override
    public void vmNoNetworkView(boolean isConnected) {
        activitySocialLoginOtpBinding.socialViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        activitySocialLoginOtpBinding.networkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void vmWebserviceErrorView(boolean isWebserviceError) {
        activitySocialLoginOtpBinding.socialViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        activitySocialLoginOtpBinding.apiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void vmSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(activitySocialLoginOtpBinding.socialViewContent, this, message);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        activitySocialLoginOtpBinding.reSendOtp.setEnabled(false);
        activitySocialLoginOtpBinding.reSendOtp.setTextColor(getResources().getColor(R.color.colorLightPink));
        String message = String.format(getString(R.string.text_waiting_for_otp), String.valueOf(millisUntilFinished / 1000));
        activitySocialLoginOtpBinding.waitingOtp.setText(message);
        activitySocialLoginOtpBinding.waitingOtp.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinish() {
        activitySocialLoginOtpBinding.reSendOtp.setEnabled(true);
        activitySocialLoginOtpBinding.reSendOtp.setTextColor(getResources().getColor(R.color.colorMediumPink));
        String message = String.format(getString(R.string.text_waiting_for_otp), String.valueOf(0));
        activitySocialLoginOtpBinding.waitingOtp.setText(message);
        activitySocialLoginOtpBinding.waitingOtp.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(IntentConstant.FROM_ACCOUNT_SCREEN_NEW_MOBILE_ENTRY)) {
            Intent intent = new Intent(this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finishAffinity();
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_FORGOT_PASSWORD_VERIFY_OTP);
    }
}
