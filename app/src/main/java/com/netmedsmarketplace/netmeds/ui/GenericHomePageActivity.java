package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityGenericMedicinesHomePageBinding;
import com.netmedsmarketplace.netmeds.fragment.GenericSwipeFragment;
import com.netmedsmarketplace.netmeds.viewmodel.GenericMedicinesHomePageViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MstarGenericConfiguration;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;

public class GenericHomePageActivity extends BaseViewModelActivity<GenericMedicinesHomePageViewModel> implements GenericMedicinesHomePageViewModel.GenericListener {

    private ActivityGenericMedicinesHomePageBinding binding;
    private GenericMedicinesHomePageViewModel viewModel;
    private GenericSwipeFragment genericSwipeFragment;
    private MstarGenericConfiguration genericConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_generic_medicines_home_page);
        toolBarSetUp(binding.genericToolbar);
        getSupportActionBar().setTitle(getString(R.string.string_generic_meds_tilte));
        setDataFromConfig(BasePreference.getInstance(this));
        binding.setViewModel(onCreateViewModel());
    }


    @Override
    protected GenericMedicinesHomePageViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(GenericMedicinesHomePageViewModel.class);
        viewModel.init(this);
        onRetry(viewModel);
        return viewModel;
    }

    private void setDataFromConfig(BasePreference basePreference) {
        ConfigurationResponse configurationResponse = new Gson().fromJson(basePreference.getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getGenericConfiguration() != null) {
            this.genericConfiguration = configurationResponse.getResult().getConfigDetails().getGenericConfiguration();
            binding.saveUpto.setText(!TextUtils.isEmpty(genericConfiguration.getSaveUpTo()) ? genericConfiguration.getSaveUpTo() : "");
            binding.additonalOfferText.setText(genericConfiguration.getGenericContent() != null && !TextUtils.isEmpty(genericConfiguration.getGenericContent().getHeader()) ? genericConfiguration.getGenericContent().getHeader() : "");
            binding.tcAppliedText.setText(genericConfiguration.getGenericContent() != null && !TextUtils.isEmpty(genericConfiguration.getGenericContent().getSubHeader()) ? genericConfiguration.getGenericContent().getSubHeader() : "");
            Glide.with(this).load(genericConfiguration.getGenericBannerList().get(0).getUrl()).error(Glide.with(this).load(R.drawable.ic_no_image)).into(binding.genericBanner);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);
        if (!M2Helper.getInstance().isM2Order()) {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this,
                    BasePreference.getInstance(this).getCartCount() > 0 ? R.drawable.ic_cart : R.drawable.ic_shopping_cart));
        } else {
            menu.getItem(0).setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.cart:
                startActivity(new Intent(this, MStarCartActivity.class));
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void showLoader() {
        showProgress(this);
    }

    @Override
    public void dismissLoader() {
        dismissProgress();
    }

    @Override
    public void navigateToSearch(boolean isGeneric) {
        Intent intent = new Intent(this, SearchActivity.class);
        if (isGeneric) {
            intent.putExtra(IntentConstant.IS_FROM_GENERIC, true);
        }
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_GENERIC_HOME);
    }

    @Override
    public void openSlideFragment() {
        if (!TextUtils.isEmpty(genericConfiguration.getGenericWorks())) {
            genericSwipeFragment = new GenericSwipeFragment(genericConfiguration.getGenericWorks(), this, BasePreference.getInstance(this));
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right).add(genericSwipeFragment, "filter_fragment").commitNowAllowingStateLoss();
        }
    }
}
