package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MedicineBannerAdapter;
import com.netmedsmarketplace.netmeds.adpater.MedicineBuyAgainAdapter;
import com.netmedsmarketplace.netmeds.adpater.MstarWellnessAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityMedicinePageBinding;
import com.netmedsmarketplace.netmeds.viewmodel.MedicineHomePageViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarWellnessSectionDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.List;

public class MedicineHomePageActivity extends BaseViewModelActivity<MedicineHomePageViewModel> implements MedicineHomePageViewModel.MedicineHomeListener, MedicineBuyAgainAdapter.MedicineHomePageAdapterListener, SnackBarHelper.SnackBarHelperListener, MstarWellnessAdapter.WellnessAdaterCallBack {

    private ActivityMedicinePageBinding binding;
    private MedicineHomePageViewModel viewModel;
    private MStarBasicResponseTemplateModel model;
    private boolean isAddtoCart = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_medicine_page);
        binding.setViewModel(onCreateViewModel());
        model = new Gson().fromJson(BasePreference.getInstance(this).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        onCreateViewModel();
        initToolBar();
    }

    private void initToolBar() {
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            binding.toolbar.setTitle("");
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white);
        }
    }

    @Override
    protected MedicineHomePageViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(MedicineHomePageViewModel.class);
        viewModel.init(this);
        viewModel.getPgOfferResponseMutableLiveData().observe(this, new MedicineBanner());
        return viewModel;
    }

    @Override
    public void vmUploadPrescription() {
        startActivity(new Intent(this, M2AttachPrescriptionActivity.class));
    }

    @Override
    public void redirectToLogin() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public BasePreference basePreference() {
        return BasePreference.getInstance(this);
    }

    public LinearLayoutManager linearLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    @Override
    public void medicineBannerAdapter(final List<MstarWellnessSectionDetails> offerList, int index) {
        if (offerList != null && offerList.get(index) != null && offerList.get(index).getProductDetails() != null && !offerList.get(index).getProductDetails().isEmpty()) {
            binding.paymentOfferView.setVisibility(View.VISIBLE);
            binding.paymentOfferView.setLayoutManager(linearLayoutManager());
            MedicineBannerAdapter wellnessAdapter = new MedicineBannerAdapter(this, offerList.get(index).getProductDetails());
            binding.paymentOfferView.setAdapter(wellnessAdapter);
            binding.paymentOfferView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                        if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < offerList.size()) {
                            List<MstarWellnessSectionDetails> impressionList = offerList.subList(firstVisiblePosition, lastVisiblePosition + 1);
                        }
                    }
                }
            });
        } else
            binding.paymentOfferView.setVisibility(View.GONE);
    }

    @Override
    public void setBuyAgainAdapter(List<MStarProductDetails> buyAgainProductDetailsList) {
        if (buyAgainProductDetailsList != null && buyAgainProductDetailsList.size() > 0 && viewModel.isBuyAgainVisible()) {
            binding.cvMedicineView.setVisibility(View.VISIBLE);
            binding.buyAgainView.setLayoutManager(linearLayoutManager());
            MedicineBuyAgainAdapter wellnessAdapter = new MedicineBuyAgainAdapter(this, buyAgainProductDetailsList, model, this);
            binding.buyAgainView.setAdapter(wellnessAdapter);
        } else {
            binding.cvMedicineView.setVisibility(View.GONE);
        }
    }

    @Override
    public void WellnessAdapter(List<MstarWellnessSectionDetails> wellnessCategory, int index) {
        if (wellnessCategory != null && wellnessCategory.get(index) != null && wellnessCategory.get(index).getProductDetails() != null && !wellnessCategory.get(index).getProductDetails().isEmpty()) {
            binding.cvCategoryView.setVisibility(View.VISIBLE);
            binding.tvMedicineCategories.setText(!TextUtils.isEmpty(wellnessCategory.get(index).getSubHeader()) ? wellnessCategory.get(index).getSubHeader() : "");
            binding.buyAgainWellnessView.setLayoutManager(new GridLayoutManager(this, 4));
            MstarWellnessAdapter wellnessAdapter = new MstarWellnessAdapter(wellnessCategory.get(index).getProductDetails(), this);
            binding.buyAgainWellnessView.setAdapter(wellnessAdapter);
        } else {
            binding.cvCategoryView.setVisibility(View.GONE);
        }
    }

    @Override
    public void navigateSearch() {
        startActivity(new Intent(this, SearchActivity.class));
    }

    @Override
    public void navigateWellnessActivity(String wellnessViewAll) {
        if (!TextUtils.isEmpty(wellnessViewAll)) {
            Intent intent = new Intent(this, WellnessCategoryViewAllActivity.class);
            intent.putExtra(IntentConstant.WELLNESS_VIEW_ALL_CODES, wellnessViewAll);
            startActivity(intent);
        }
    }

    @Override
    public void navigateBuyAgainActivity() {
        startActivity(new Intent(this, BuyAgainActivity.class));
    }

    @Override
    public void snackBarMessage(String message, boolean isSuccess) {
        if (isSuccess) {
            SnackBarHelper.snackBarCallBackSetAction(binding.medicineParentView, this, getString(R.string.text_added_to_cart), getString(R.string.text_view_cart), this);
            PaymentHelper.setIsMedicineIncreaseQuantity(true);
        } else {
            if (!TextUtils.isEmpty(message) && AppConstant.NO_RECORD_FOUND.equalsIgnoreCase(message))
                binding.cvMedicineView.setVisibility(View.GONE);
            else
                SnackBarHelper.snackBarCallBack(binding.medicineParentView, this, message);
        }
    }

    @Override
    public void showProgressBar() {
        showHorizontalProgressBar(false, binding.medicineProgressBar); // while loading the view should be non clickable
    }

    @Override
    public void dismissProgressBar() {
        showHorizontalProgressBar(true, binding.medicineProgressBar);// after loading the view should be clickable

    }

    public void showHorizontalProgressBar(boolean isClickable, ProgressBar progressBar) {
        if (!isClickable) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void addToCart(int productCode) {
        isAddtoCart = true;
        showProgressBar();
        viewModel.productCode(productCode);
    }

    @Override
    public void navigateProductDetailActivity(int productCode) {
        Intent intent = new Intent(this, ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, getString(R.string.text_products_ordered));
        startActivity(intent);
    }

    @Override
    public void snackBarOnClick() {
        startActivity(new Intent(this, MStarCartActivity.class));
    }

    @Override
    public void OnCategoryClickCallback(int id, int level, String categoryName) {
        Intent intent = new Intent(this, level == 1 ? CategoryActivity.class : ProductList.class); // for level 1 display sub-category and for level 2 & 3 show only product list and
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentConstant.CATEGORY_ID, id);
        intent.putExtra(IntentConstant.CATEGORY_NAME, categoryName);
        startActivity(intent);

    }

    private class MedicineBanner implements Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            viewModel.onMedicineAvailable(response);
            binding.setViewModel(viewModel);
        }
    }

    @Override
    public void navigateGenericPage() {
        startActivity(new Intent(this, GenericHomePageActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.medicine_menu, menu);
        if (!M2Helper.getInstance().isM2Order()) {
            if (BasePreference.getInstance(this).getCartCount() > 0)
                if (BasePreference.getInstance(this).getCartCount() > 0)
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart_white));
                else
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart_white));
        } else {
            menu.getItem(0).setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.medicine_cart:
                /*Google Analytics Click Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_VIEW_CART_SEARCH_PAGE, GoogleAnalyticsHelper.EVENT_LABEL_SEARCH_PAGE);
                snackBarOnClick();
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PaymentHelper.setIsMedicineIncreaseQuantity(true);
        if (isAddtoCart) {
            showProgressBar();
            viewModel.initMstarApi(APIServiceManager.C_MSTAR_PAST_MEDICINES);
        }
    }
}
