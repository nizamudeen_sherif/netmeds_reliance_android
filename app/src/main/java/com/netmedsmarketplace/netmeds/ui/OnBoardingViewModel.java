package com.netmedsmarketplace.netmeds.ui;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseFailureReasonTemplateModel;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarOtpDetails;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MATHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.truecaller.android.sdk.TrueProfile;

import java.util.HashMap;
import java.util.Map;

public class OnBoardingViewModel extends AppViewModel {

    private OnBoardingListener callback;
    private TrueProfile trueProfile;
    private int failedTransactionId;

    public OnBoardingViewModel(@NonNull Application application) {
        super(application);
    }

    public void letsStart() {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_GET_STARTED_CLICKED, GoogleAnalyticsHelper.EVENT_LABEL_ON_BOARDING_SCREEN);
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.showNoNetworkView(isConnected);
        if (isConnected) {
            callback.letsStartClick(false);
            callback.vmCallbackOnStart();
        }
    }

    void init(OnBoardingListener listener) {
        this.callback = listener;
        setAdapter();
    }

    @Override
    public void onRetryClickListener() {
        super.onRetryClickListener();
        callback.vmCallbackOnStart();
        callback.showWebserviceErrorView(false);
    }

    @Override
    public void onSyncData(String data, int transactionId) {
        callback.letsStartClick(true);
        switch (transactionId) {
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                socialLoginResponse(data);
                break;
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                customerDetailsResponse(data);
                break;
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                getUserStatusResponse(data);
                break;
        }
    }

    private void setAdapter() {
        ConfigurationResponse boardingResponse = new Gson().fromJson(getPreference().getConfiguration(), ConfigurationResponse.class);
        if (boardingResponse != null && boardingResponse.getResult() != null && boardingResponse.getResult().getConfigDetails() != null) {
            OnBoardingAdapter adapter = new OnBoardingAdapter(callback.getContext(), boardingResponse.getResult().getConfigDetails().getOnBoarding(), null, false, null);
            callback.setRecyclerViewAdapter(adapter);
        }
    }

    @Override
    public void onFailed(int transactionId, String data) {
        callback.vmDismissProgress();
        callback.letsStartClick(true);
        switch (transactionId) {
            case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
            case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
            case APIServiceManager.MSTAR_GET_USER_STATUS:
                vmOnError(transactionId);
                break;
        }
    }

    TrueProfile getTrueProfile() {
        return trueProfile;
    }

    void setTrueProfile(TrueProfile trueProfile) {
        this.trueProfile = trueProfile;
        socialLogin();
    }

    private void socialLoginResponse(String data) {

        /*FireBase Analytics Event*/
        FireBaseAnalyticsHelper.getInstance().loginEvent(callback.getContext(), AppConstant.TRUE_CALLER);
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel socialLoginResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (socialLoginResponse != null && socialLoginResponse.getStatus() != null) {
                if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_SUCCESS_STATUS)) {
                    setSocialLoginSuccessResponse(socialLoginResponse);
                } else if (socialLoginResponse.getStatus().equalsIgnoreCase(AppConstant.API_MSTAR_FAILURE_STATUS)) {
                    setSocialLoginFailureResponse(socialLoginResponse);
                }
            } else {
                callback.vmDismissProgress();
                vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
            }
        } else {
            callback.vmDismissProgress();
            vmOnError(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
        }
    }

    private void setSocialLoginSuccessResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        if (socialLoginResponse.getResult() != null) {
            MstarBasicResponseResultTemplateModel mStarSocialLoginResult = socialLoginResponse.getResult();
            if (!TextUtils.isEmpty(mStarSocialLoginResult.getCode())) {
                switch (mStarSocialLoginResult.getCode()) {
                    case AppConstant.EXISTS:
                        String sessionId = socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null && !TextUtils.isEmpty(socialLoginResponse.getResult().getSession().getId()) ? socialLoginResponse.getResult().getSession().getId() : "";
                        long customerId = socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null ? socialLoginResponse.getResult().getSession().getCustomerId() : 0;
                        String loganSessionId = socialLoginResponse.getResult() != null && socialLoginResponse.getResult().getSession() != null && !TextUtils.isEmpty(socialLoginResponse.getResult().getSession().getLoganSessionId()) ? socialLoginResponse.getResult().getSession().getLoganSessionId() : "";

                        getPreference().setMStarSessionId(sessionId, Long.toString(customerId));
                        getPreference().setGuestCart(false);
                        getPreference().setMstarCustomerId(Long.toString(customerId));
                        getPreference().setLoganSession(loganSessionId);

                        getCustomerDetails();
                        break;
                    case AppConstant.USER_NOT_EXISTS:
                        mStarGetUserStatus();
                        break;
                }
            } else callback.vmDismissProgress();
        } else callback.vmDismissProgress();
    }

    private void setSocialLoginFailureResponse(MStarBasicResponseTemplateModel socialLoginResponse) {
        callback.vmDismissProgress();
        if (socialLoginResponse.getReason() != null && socialLoginResponse.getReason().getReason_eng() != null && !TextUtils.isEmpty(socialLoginResponse.getReason().getReason_eng())) {
            callback.vmSnackBarMessage(socialLoginResponse.getReason().getReason_eng());
        }
    }

    private BasePreference getPreference() {
        return BasePreference.getInstance(callback.getContext());
    }

    private void socialLogin() {
        callback.vmShowProgress();
        initApiCall(APIServiceManager.C_MSTAR_SOCIAL_LOGIN);
    }

    private void getCustomerDetails() {
        initApiCall(APIServiceManager.MSTAR_CUSTOMER_DETAILS);
    }

    private void initApiCall(int transactionId) {
        boolean isConnected = NetworkUtils.isConnected(callback.getContext());
        callback.showNoNetworkView(isConnected);
        if (isConnected) {
            switch (transactionId) {
                case APIServiceManager.C_MSTAR_SOCIAL_LOGIN:
                    APIServiceManager.getInstance().mStarSocialLogin(this, getSocialLoginRequest(),getPreference().getMstarGoogleAdvertisingId());
                    break;
                case APIServiceManager.MSTAR_CUSTOMER_DETAILS:
                    APIServiceManager.getInstance().mStarCustomerDetails(this, getPreference().getMstarBasicHeaderMap());
                    break;
                case APIServiceManager.MSTAR_GET_USER_STATUS:
                    APIServiceManager.getInstance().mStarGetUserStatus(this, getMobileNo());
                    break;
            }
        } else {
            callback.vmDismissProgress();
            callback.letsStartClick(true);
            failedTransactionId = transactionId;
        }
    }

    private Map<String, String> getSocialLoginRequest() {
        Map<String, String> socialLoginRequest = new HashMap<>();
        socialLoginRequest.put(AppConstant.SIGNATURE, !TextUtils.isEmpty(trueProfile.signature) ? trueProfile.signature : "");
        socialLoginRequest.put(AppConstant.PAYLOAD, !TextUtils.isEmpty(trueProfile.payload) ? trueProfile.payload : "");
        socialLoginRequest.put(AppConstant.ACCESS_TOKEN, "");
        socialLoginRequest.put(AppConstant.SOCIAL_FLAG, ConfigMap.getInstance().getProperty(ConfigConstant.TRUECALLER_FLAG));
        socialLoginRequest.put(AppConstant.SOURCE, AppConstant.SOURCE_NAME);
        socialLoginRequest.put(AppConstant.MOBILE_NUMBER, "");
        socialLoginRequest.put(AppConstant.RANDOM_KEY, "");
        return socialLoginRequest;
    }

    private void customerDetailsResponse(String data) {
        callback.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel customerDetailResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (customerDetailResponse != null && AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(customerDetailResponse.getStatus()) && customerDetailResponse.getResult() != null && customerDetailResponse.getResult().getCustomerDetails() != null) {
                getPreference().setPreviousCartShippingAddressId(customerDetailResponse.getResult().getCustomerDetails().getPreferredShippingAddress());
                getPreference().setCustomerDetails(new Gson().toJson(customerDetailResponse.getResult().getCustomerDetails()));

                //Set WebEngage Login
                WebEngageHelper.getInstance().setWebEngageLogin(getPreference(), WebEngageHelper.SOCIAL_WIDGET, false, callback.getContext());
                //MAT Login Event
                MATHelper.getInstance().loginAndRegistrationMATEvent(getPreference(), false);
                /*Google Analytics Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_TRUE_CALLER_LOGIN, GoogleAnalyticsHelper.EVENT_LABEL_TRUE_CALLER);
                callback.vmCallbackExistingUser();
            }
        }
    }

    private void vmOnError(int transactionId) {
        failedTransactionId = transactionId;
        callback.showWebserviceErrorView(true);
    }

    private void mStarGetUserStatus() {
        initApiCall(APIServiceManager.MSTAR_GET_USER_STATUS);
    }

    private String getMobileNo() {
        return trueProfile != null && !TextUtils.isEmpty(trueProfile.phoneNumber) ? trueProfile.phoneNumber.substring(3) : "";
    }

    private void getUserStatusResponse(String data) {
        callback.vmDismissProgress();
        if (!TextUtils.isEmpty(data)) {
            MStarBasicResponseTemplateModel userStatusResponse = new Gson().fromJson(data, MStarBasicResponseTemplateModel.class);
            if (userStatusResponse != null && userStatusResponse.getStatus() != null && AppConstant.API_MSTAR_FAILURE_STATUS.equalsIgnoreCase(userStatusResponse.getStatus())) {
                checkFailureReason(userStatusResponse);
                PaymentHelper.setSigup_method(AppConstant.TRUE_CALLER);
            }
        } else
            vmOnError(APIServiceManager.MSTAR_GET_USER_STATUS);
    }

    private void checkFailureReason(MStarBasicResponseTemplateModel userStatusResponse) {
        if (userStatusResponse.getReason() != null) {
            MstarBasicResponseFailureReasonTemplateModel mStarUserStatusReason = userStatusResponse.getReason();
            if (!TextUtils.isEmpty(mStarUserStatusReason.getReason_code()) && mStarUserStatusReason.getReason_code().equalsIgnoreCase(AppConstant.NOT_FOUND)) {
                if (userStatusResponse.getResult() != null && userStatusResponse.getResult().getMstarOtpDetails() != null) {
                    MstarOtpDetails mstarOtpDetails = userStatusResponse.getResult().getMstarOtpDetails();
                    if (!TextUtils.isEmpty(mstarOtpDetails.getRandomKey())) {
                        /*Google Analytics Event*/
                        GoogleAnalyticsHelper.getInstance().postEvent(callback.getContext(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_TRUE_CALLER_SIGN_UP, GoogleAnalyticsHelper.EVENT_LABEL_TRUE_CALLER);
                        callback.vmCallbackNewUser(mstarOtpDetails.getRandomKey());
                    }
                }
            } else {
                callback.vmSnackBarMessage(!TextUtils.isEmpty(mStarUserStatusReason.getReason_eng()) ? mStarUserStatusReason.getReason_eng() : "");
            }
        }
    }

    public interface OnBoardingListener {
        void vmCallbackOnStart();

        void vmCallbackNewUser(String randomKey);

        void vmCallbackExistingUser();

        Context getContext();

        void showNoNetworkView(boolean isShowNoNetworkView);

        void showWebserviceErrorView(boolean isShowWebserviceErrorView);

        void letsStartClick(boolean isEnable);

        void setRecyclerViewAdapter(OnBoardingAdapter adapter);

        void vmSnackBarMessage(String message);

        void vmDismissProgress();

        void vmShowProgress();

    }
}
