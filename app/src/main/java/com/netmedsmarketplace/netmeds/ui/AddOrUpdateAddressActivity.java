package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityAddUpdateAddressBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AddOrUpdateAddressViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.model.MstarEditAddressRequest;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DiagnosticConstant;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.ui.M3DeliveryDateIntervalActivity;

public class AddOrUpdateAddressActivity extends BaseViewModelActivity<AddOrUpdateAddressViewModel> implements AddOrUpdateAddressViewModel.ViewModelListener {

    private ActivityAddUpdateAddressBinding binding;
    private AddOrUpdateAddressViewModel viewModel;
    private BasePreference basePreference;
    public static updateAddressListener updateAddressListener;

    public static void setUpdateAddressListener(updateAddressListener listener) {
        updateAddressListener = listener;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_update_address);
        binding.setViewModel(onCreateViewModel());
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, getString(getIntent().getBooleanExtra(IntentConstant.ADD_ADDRESS_FLAG, false) ? R.string.text_edit_address : R.string.text_add_address));
    }

    @Override
    protected AddOrUpdateAddressViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(AddOrUpdateAddressViewModel.class);
        basePreference = BasePreference.getInstance(this);
        intentValue();
        viewModel.initViewModel(basePreference, this);
        onRetry(viewModel);
        return viewModel;
    }

    private void intentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(DiagnosticConstant.KEY_DIAGNOSTIC))
                viewModel.setIntentFromDiagnostic(intent.getStringExtra(DiagnosticConstant.KEY_DIAGNOSTIC));
            if (intent.hasExtra(DiagnosticConstant.KEY_LAB_ID))
                viewModel.setDiagnosticLabId(intent.getIntExtra(DiagnosticConstant.KEY_LAB_ID, 0));
            if (intent.hasExtra(IntentConstant.ADDRESS)) {
                String address = intent.getStringExtra(IntentConstant.ADDRESS);
                viewModel.setAddress(TextUtils.isEmpty(address) ? null : new Gson().fromJson(address, MStarAddressModel.class));
            }
            if(intent.hasExtra(DiagnosticConstant.KEY_FROM_PATIENT_DETAILS) && intent.getBooleanExtra(DiagnosticConstant.KEY_FROM_PATIENT_DETAILS, false)){
                viewModel.setFromDiagPatientDetails(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void settingBillingAndShippingAddressResponse(boolean isShippingAddressSet, boolean isBillingAddressSet) {
        if (!isBillingAddressSet) {
            SnackBarHelper.snackBarCallBack(binding.cvParentAddAddress, this, "Billing Address not set Properly. Please try after some times");
        }
        if (!isShippingAddressSet) {
            SnackBarHelper.snackBarCallBack(binding.cvParentAddAddress, this, "Shipping Address not set Properly. Please try after some times");
        }
        if (isBillingAddressSet && isShippingAddressSet) {
            if (M2Helper.getInstance().isM2Order())
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_ADDRESS_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_M2_ADDRESS);
            else
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_PROCEED_ADDRESS, GoogleAnalyticsHelper.EVENT_LABEL_SELECT_ADDRESS_PAGE);
            basePreference.setPreviousCartShippingAddressId(viewModel.getBillingAndShippingAddressId());
            basePreference.setPreviousCartBillingAddressId(viewModel.getBillingAndShippingAddressId());
            basePreference.setDiagDefaultAddressId(viewModel.getBillingAndShippingAddressId());
            viewModel.initiateAPICall(APIServiceManager.MSTAR_GET_ADDRESS_BY_ID);
        }

    }

    @Override
    public void continueButtonVisibility(int view) {
        binding.saveAddressButton.setVisibility(view);
    }

    @Override
    public String getPincode() {
        return !TextUtils.isEmpty(binding.pinCode.getText()) ? binding.pinCode.getText().toString() : "";
    }

    @Override
    public void setPincodeError(String message) {
        binding.pinCodeLayout.setError(message);
    }

    @Override
    public MstarEditAddressRequest getMstarEditAddressRequest() {
        MstarEditAddressRequest addressModel = new MstarEditAddressRequest();
        addressModel.setFirstname(binding.firstName.getText() != null ? ValidationUtils.validateString(binding.firstName.getText().toString()) : "");
        addressModel.setLastname(binding.lastName.getText() != null ? ValidationUtils.validateString(binding.lastName.getText().toString()) : "");
        addressModel.setStreet(binding.houseNumber.getText() != null ? ValidationUtils.validateString(binding.houseNumber.getText().toString()) : "");
        addressModel.setLandmark(binding.landMark.getText() != null ? ValidationUtils.validateString(binding.landMark.getText().toString()) : "");
        addressModel.setPin(binding.pinCode.getText() != null ? ValidationUtils.validateString(binding.pinCode.getText().toString()) : "");
        addressModel.setMobileNo(binding.mobileNumber.getText() != null ? ValidationUtils.validateString(binding.mobileNumber.getText().toString()) : "");
        return addressModel;
    }

    @Override
    public void initiateNewAddressViews(MStarCustomerDetails customerDetails) {
        binding.firstName.setText(!TextUtils.isEmpty(customerDetails.getFirstName()) ? customerDetails.getFirstName() : "");
        binding.lastName.setText(!TextUtils.isEmpty(customerDetails.getLastName()) ? customerDetails.getLastName() : "");
        binding.mobileNumber.setText(!TextUtils.isEmpty(customerDetails.getMobileNo()) ? customerDetails.getMobileNo() : "");
    }

    @Override
    public void initiateEditAddressViews(MStarAddressModel updatingAddress) {
        binding.pinCode.setText(!TextUtils.isEmpty(updatingAddress.getPin()) ? updatingAddress.getPin() : "");
        binding.firstName.setText(!TextUtils.isEmpty(updatingAddress.getFirstname()) ? updatingAddress.getFirstname() : "");
        binding.lastName.setText(!TextUtils.isEmpty(updatingAddress.getLastname()) ? updatingAddress.getLastname() : "");
        binding.houseNumber.setText(!TextUtils.isEmpty(updatingAddress.getStreet()) ? updatingAddress.getStreet() : "");
        binding.landMark.setText(!TextUtils.isEmpty(updatingAddress.getLandmark()) ? updatingAddress.getLandmark() : "");
        binding.mobileNumber.setText(!TextUtils.isEmpty(updatingAddress.getMobileNo()) ? updatingAddress.getMobileNo() : "");
    }

    @Override
    public void showWebserviceErrorView(boolean enable) {
        binding.mainLayout.setVisibility(enable ? View.GONE : View.VISIBLE);
        binding.addressApiErrorView.setVisibility(enable ? View.VISIBLE : View.GONE);
        binding.addressNetworkErrorView.setVisibility(View.GONE);
    }

    @Override
    public MStarAddressModel getEnteredAddress() {
        MStarAddressModel addressModel = new MStarAddressModel();
        addressModel.setFirstname(binding.firstName.getText() != null ? ValidationUtils.validateString(binding.firstName.getText().toString()) : "");
        addressModel.setLastname(binding.lastName.getText() != null ? ValidationUtils.validateString(binding.lastName.getText().toString()) : "");
        addressModel.setStreet(binding.houseNumber.getText() != null ? ValidationUtils.validateString(binding.houseNumber.getText().toString()) : "");
        addressModel.setLandmark(binding.landMark.getText() != null ? ValidationUtils.validateString(binding.landMark.getText().toString()) : "");
        addressModel.setPin(binding.pinCode.getText() != null ? ValidationUtils.validateString(binding.pinCode.getText().toString()) : "");
        addressModel.setMobileNo(binding.mobileNumber.getText() != null ? ValidationUtils.validateString(binding.mobileNumber.getText().toString()) : "");
        return addressModel;
    }

    @Override
    public void setErrorWatcher() {
        CommonUtils.errorWatcher(binding.firstName, binding.firstNameLayout);
        CommonUtils.errorWatcher(binding.lastName, binding.lastNameLayout);
        CommonUtils.errorWatcher(binding.houseNumber, binding.houseNumberLayout);
        CommonUtils.errorWatcher(binding.landMark, binding.landMarkLayout);
        CommonUtils.errorWatcher(binding.city, binding.cityLayout);
        CommonUtils.errorWatcher(binding.mobileNumber, binding.landMarkLayout);
    }

    @Override
    public void showSnackBar(String message) {
        SnackBarHelper.snackBarCallBack(binding.mainLayout, this, message);
    }

    @Override
    public void applyCityStateFromPinCode() {
        binding.pinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.city.setText("");
                if (binding.pinCode.getText() != null && binding.pinCode.getText().toString().length() > 0 && binding.pinCodeLayout.isErrorEnabled()) {
                    binding.pinCodeLayout.setError(null);
                    binding.pinCodeLayout.setErrorEnabled(false);
                }
                if (binding.pinCode.getText() != null && binding.pinCode.getText().toString().length() == 6) {
                    viewModel.callPinCodeService(binding.pinCode.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void setCityState(String cityState) {
        binding.city.setText(cityState);
    }

    @Override
    public void setHouseNumberLayoutError(String message) {
        binding.houseNumberLayout.setError(message);
    }

    @Override
    public void setLandMarkLayoutError(String message) {
        binding.landMarkLayout.setError(message);
    }

    @Override
    public void setcityLayoutError(String message) {
        binding.cityLayout.setError(message);
    }

    @Override
    public void setmobileNumberLayoutError(String message) {
        binding.mobileNumberLayout.setError(message);
    }

    @Override
    public String getHouseNumber() {
        return binding.houseNumber.getText().toString();
    }

    @Override
    public String getLandMark() {
        return binding.landMark.getText().toString();
    }

    @Override
    public String getCity() {
        return binding.city.getText().toString();
    }

    @Override
    public String getMobileNumber() {
        return binding.mobileNumber.getText().toString();
    }

    @Override
    public boolean validateFirstName() {
        return ValidationUtils.checkTextInputLayout(binding.firstName, true, this.getResources().getString(R.string.text_error_enter_first_name), this.getResources().getString(R.string.text_name_white_space_error), this.getResources().getString(R.string.text_continuous_whitespace_error), 3, 30, binding.firstNameLayout);
    }

    @Override
    public boolean validateLastName() {
        return ValidationUtils.checkTextInputLayout(binding.lastName, true, this.getResources().getString(R.string.text_error_enter_last_name), this.getResources().getString(R.string.text_name_white_space_error), this.getResources().getString(R.string.text_continuous_whitespace_error), 1, 30, binding.lastNameLayout);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void isAddressButtonEnabled(boolean isEnabled) {
        binding.saveAddressButton.setEnabled(isEnabled);
        binding.saveAddressButton.setBackground(isEnabled? getContext().getResources().getDrawable(R.drawable.accent_button): getContext().getResources().getDrawable(R.drawable.secondary_button));
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void navigateToConfirmation(MStarAddressModel addressModel) {
        if (!viewModel.getIntentFromDiagnostic().equals(DiagnosticConstant.KEY_DIAGNOSTIC)) {
            /*WebEngage Delivery Information Filled Event*/
            WebEngageHelper.getInstance().deliveryInformationFilledEvent(this, addressModel);
            /*Google Tag Manager + FireBase Checkout Progress Step3 Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBaseCheckoutProgressEvent(this, PaymentHelper.getCartLineItems(),PaymentHelper.getAnalyticalTrackingProductList(),FireBaseAnalyticsHelper.CHECKOUT_STEP_3,FireBaseAnalyticsHelper.EVENT_PARAM_SELECT_ADDRESS);
            if (updateAddressListener != null)
                updateAddressListener.onBackPress();
            Intent intent = new Intent(this, SubscriptionHelper.getInstance().isCreateNewFillFlag() && getIntent().getBooleanExtra(IntentConstant.IS_FROM_NEW_CUSTOMER, false) ? M3DeliveryDateIntervalActivity.class : OrderConfirmationActivity.class);
            intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, new Gson().toJson(addressModel));
            intent.putExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST, getIntent().getStringExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST));
            startActivity(intent);
            this.finish();
        } else {
            Intent intent = new Intent();
            intent.putExtra(DiagnosticConstant.KEY_CUSTOMER_ADDRESS, addressModel);
            setResult(DiagnosticConstant.ADDRESS_REQUEST_CODE, intent);
            this.finish();
        }
    }

    @Override
    public void updateSuccessful() {
        isAddressButtonEnabled(true);
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        this.finish();
    }

    public interface updateAddressListener {
        void onBackPress();
    }

}