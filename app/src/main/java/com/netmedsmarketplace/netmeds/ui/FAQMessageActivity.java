package com.netmedsmarketplace.netmeds.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.adjetter.kapchatsdk.KapchatHelper;
import com.adjetter.kapchatsdk.interfaces.CallBackResponse;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityFaqMessageBinding;
import com.netmedsmarketplace.netmeds.model.FAQCategoryCondentResponseModel;
import com.netmedsmarketplace.netmeds.model.FAQCategoryModel;
import com.netmedsmarketplace.netmeds.viewmodel.FAQMessageViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.PermissionConstants;
import com.nms.netmeds.base.view.SnackBarHelper;

import static com.nms.netmeds.base.BaseUploadPrescription.PACKAGE;

public class FAQMessageActivity extends BaseViewModelActivity<FAQMessageViewModel> implements FAQMessageViewModel.FAQMessageViewModelListener, SnackBarHelper.SnackBarHelperListener,
        StillNeedHelpDialog.StillNeedHelpListener, NeedHelpThroughEmailDialog.NeedHelpThroughEmailDialogListener {

    private static final int KAPTURE_CHAT_REQUEST_CODE = 12345;
    private FAQMessageViewModel viewModel;
    private ActivityFaqMessageBinding binding;
    private FAQCategoryModel faqCategoryModel = new FAQCategoryModel();
    private String subject, titleTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_faq_message);
        onCreateViewModel();
        initToolBar();
    }

    @Override
    protected FAQMessageViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(FAQMessageViewModel.class);
        if (getIntent().getExtras() != null) {
            String categoryModel = getIntent().getExtras().getString(IntentConstant.FAQ_CONTENT_DATA);
            faqCategoryModel = new Gson().fromJson(categoryModel, FAQCategoryModel.class);
        }
        viewModel.init(this, binding, this, String.valueOf(faqCategoryModel.getId()));
        viewModel.getFAQContentMutableData().observe(this, new FAQContentObserver());
        onRetry(viewModel);
        return viewModel;
    }

    private void initToolBar() {
        toolBarSetUp(binding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(faqCategoryModel.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoadingProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoadingProgress() {
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void needHelpAgain() {
        StillNeedHelpDialog needHelpDialog = new StillNeedHelpDialog(this);
        getSupportFragmentManager().beginTransaction().add(needHelpDialog, "StillNeedHelpDialog").commitAllowingStateLoss();
    }

    @Override
    public void showSnackBar(String message) {
        SnackBarHelper.snackBarCallBack(binding.llBottomButtonView, this, message);
    }

    @Override
    public void initEmail() {
        subject = String.format("%s - %s", faqCategoryModel.getParentCategoryName(), faqCategoryModel.getName());
        titleTxt = String.format("%s", faqCategoryModel.getParentCategoryName());
        NeedHelpThroughEmailDialog needHelpThroughEmailDialog = new NeedHelpThroughEmailDialog(this, subject);
        getSupportFragmentManager().beginTransaction().add(needHelpThroughEmailDialog, "NeedHelpThroughEmailDialog").commitAllowingStateLoss();
    }

    @Override
    public void initCall() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstant.TEL + getEnquiryNumber()));
        startActivity(intent);
    }

    private String getEnquiryNumber() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(this).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getEnquiryMobileNo()))
            return configurationResponse.getResult().getConfigDetails().getEnquiryMobileNo();
        else return "";
    }

    public void navigateToSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts(PACKAGE, getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (checkCallPermissionEnable()) {
            switch (requestCode) {
                case PermissionConstants.CALL_PHONE:
                    initCall();
                    break;
            }
        } else {
            denyCallPermissionIntimation();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PermissionConstants.CALL_PHONE) {
            initCall();
        } else if (requestCode == KAPTURE_CHAT_REQUEST_CODE) {
            KapchatHelper.logoutKapchat(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void denyCallPermissionIntimation() {
        SnackBarHelper.snackBarCallBackSetAction(binding.llBottomButtonView, this, getString(R.string.text_call_permission_alert), "view", this);
    }

    @Override
    public boolean checkCallPermissionEnable() {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void requestEnableCallPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PermissionConstants.CALL_PHONE);
    }


    //KapChat screen view
    @Override
    public void openKapChat() {
        showLoadingProgress();
        KapchatHelper.initialise(this, BasePreference.getInstance(this).getMstarCustomerId(),
                getString(R.string.kapture_chat_support_key), getString(R.string.kapture_chat_encription_key), new CallBackResponse() {
                    @Override
                    public void intialiseResponse(String responseStatus) {
                        if (AppConstant.API_SUCCESS_STATUS.equalsIgnoreCase(responseStatus)) {
                            KapchatHelper.startChatScreen(FAQMessageActivity.this, KAPTURE_CHAT_REQUEST_CODE);
                            dismissProgress();
                        } else {
                            dismissProgress();
                            showSnackBar(responseStatus);
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        if (!TextUtils.isEmpty(faqCategoryModel.getName()))
            GoogleAnalyticsHelper.getInstance().postScreen(this, faqCategoryModel.getName());
    }

    @Override
    public void onSubmitReason(String reason) {
        viewModel.submitNeedHelpEmailReason(subject, reason, titleTxt);
    }

    @Override
    public void onCancelEmailEnquiry() {
        needHelpAgain();
    }

    @Override
    public void snackBarOnClick() {
        navigateToSettings();
    }

    private class FAQContentObserver implements Observer<FAQCategoryCondentResponseModel> {
        @Override
        public void onChanged(@Nullable FAQCategoryCondentResponseModel obj) {
            viewModel.loadPageAfterResponse(obj);
            binding.setViewModel(viewModel);
        }
    }
}
