package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityOfferBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OfferViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OfferActivity extends BaseViewModelActivity<OfferViewModel> implements OfferViewModel.OfferListener {

    private ActivityOfferBinding binding;
    private OfferViewModel viewModel;
    private boolean isAgainLoadOfferPage = false;
    private boolean isFromDeepLinkSubList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_offer);
        onCreateViewModel();
        toolBarSetUp(binding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        initIntentValue();
    }

    @Override
    protected OfferViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(OfferViewModel.class);
        isAgainLoadOfferPage = getIntent().hasExtra(IntentConstant.OFFER_LIST);
        viewModel.init(this, binding, this, isAgainLoadOfferPage);
        viewModel.getOfferMutableData().observe(this, new OfferObserver());
        onRetry(viewModel);
        return viewModel;
    }

    @Override
    public void showLoadingProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoadingProgress() {
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initiateTabAndFragment(List<MstarNetmedsOffer> result) {
        if (result != null) {
            binding.viewpager.setAdapter(offersTabViewPagerAdapter(result));
            binding.tabs.setupWithViewPager(binding.viewpager);
            binding.tabs.setTabTextColors(getResources().getColor(R.color.colorLightBlueGrey), getResources().getColor(R.color.colorBlueLight));
            binding.tabs.setVisibility(View.VISIBLE);
            binding.tvTitle.setText(OfferActivity.this.getResources().getString(R.string.text_all_offers));
        } else {
            String orderData = getIntent().getStringExtra(IntentConstant.OFFER_LIST);
            binding.tvTitle.setText(getIntent().getStringExtra(IntentConstant.OFFER_PAGE_TITLE));
            binding.tabs.setVisibility(View.GONE);
            binding.tabs.setupWithViewPager(binding.viewpager);
            OfferTabViewPagerAdapter offerTabViewPagerAdapter = new OfferTabViewPagerAdapter(getSupportFragmentManager());
            offerTabViewPagerAdapter.addFragment(AppConstant.MEDICINE, OfferTabFragment.newInstance(AppConstant.MEDICINE, orderData));
            binding.viewpager.setAdapter(offerTabViewPagerAdapter);
        }
    }

    private OfferTabViewPagerAdapter offersTabViewPagerAdapter(List<MstarNetmedsOffer> result) {
        OfferTabViewPagerAdapter offerTabViewPagerAdapter = new OfferTabViewPagerAdapter(getSupportFragmentManager());
        Map<String, List<MstarNetmedsOffer>> offerList = viewModel.getClarifiedOfferList(result);
        offerTabViewPagerAdapter.addFragment(AppConstant.MEDICINE, OfferTabFragment.newInstance(AppConstant.MEDICINE, new Gson().toJson(offerList.get(AppConstant.S_FLAG_MEDICINE))));
        offerTabViewPagerAdapter.addFragment(AppConstant.DIAGNOSTICS, OfferTabFragment.newInstance(AppConstant.DIAGNOSTICS, new Gson().toJson(offerList.get(AppConstant.S_FLAG_DIAGNOSTICS))));
        offerTabViewPagerAdapter.addFragment(AppConstant.CONSULTATION, OfferTabFragment.newInstance(AppConstant.CONSULTATION, new Gson().toJson(offerList.get(AppConstant.S_FLAG_CONSULTATION))));
        return offerTabViewPagerAdapter;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_OFFERS);
    }

    private class OfferObserver implements Observer<MStarBasicResponseTemplateModel> {
        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel obj) {
            viewModel.loadOffer(obj);
            binding.setViewModel(viewModel);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_OFFER_PAGE);
    }

    private class OfferTabViewPagerAdapter extends FragmentStatePagerAdapter {
        List<Fragment> fragmentList = new ArrayList<>();
        List<String> tabTitleList = new ArrayList<>();

        public OfferTabViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return fragmentList.get(i);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitleList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(String tabTitle, Fragment fragment) {
            fragmentList.add(fragment);
            tabTitleList.add(tabTitle);
        }
    }

    private void initIntentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.hasExtra(IntentConstant.INDIVIDUAL_OFFER_SUB_LIST))
            setFromDeepLinkSubList(intent.getBooleanExtra(IntentConstant.INDIVIDUAL_OFFER_SUB_LIST, false));
    }

    public boolean isFromDeepLinkSubList() {
        return isFromDeepLinkSubList;
    }

    public void setFromDeepLinkSubList(boolean fromDeepLinkSubList) {
        isFromDeepLinkSubList = fromDeepLinkSubList;
    }

    @Override
    public void onBackPressed() {
        if (!isFromDeepLinkSubList()) {
            finish();
        } else {
            setFromDeepLinkSubList(false);
            viewModel.fetchOfferFromServer();
        }
    }
}
