package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogConfirmDeleteSavedCardsBinding;
import com.netmedsmarketplace.netmeds.viewmodel.DeleteCardConfirmDialogViewModel;
import com.nms.netmeds.base.BaseDialogFragment;

import org.jetbrains.annotations.NotNull;

@SuppressLint("ValidFragment")
public class DeleteCardConfirmDialog extends BaseDialogFragment implements DeleteCardConfirmDialogViewModel.DeleteCardConfirmDialogViewModelListener {
    private DeleteCardConfirmDialogListener listener;
    private final String value;
    private final String clickFrom;
    private final Object obj;

    public DeleteCardConfirmDialog(String value, String clickFrom, Object obj) {
        this.value = value;
        this.clickFrom = clickFrom;
        this.obj = obj;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (DeleteCardConfirmDialogListener) getActivity();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogConfirmDeleteSavedCardsBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_confirm_delete_saved_cards, container, false);
        DeleteCardConfirmDialogViewModel viewModel = ViewModelProviders.of(this).get(DeleteCardConfirmDialogViewModel.class);
        viewModel.init(getActivity(), binding, this, value, clickFrom);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onDismissConfirmDialog() {
        DeleteCardConfirmDialog.this.dismiss();
    }

    @Override
    public void onProceedDeleteCard() {
        DeleteCardConfirmDialog.this.dismiss();
        listener.onProceedDeleteCard(clickFrom, obj);
    }

    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    onDismissConfirmDialog();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    public interface DeleteCardConfirmDialogListener {
        void onProceedDeleteCard(String clickFrom, Object obj);
    }
}
