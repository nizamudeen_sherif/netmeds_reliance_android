package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogHowToUseBinding;
import com.netmedsmarketplace.netmeds.viewmodel.HowToUseDialogViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;

@SuppressLint("ValidFragment")
public class HowToUseFragment extends BaseBottomSheetFragment implements HowToUseDialogViewModel.HowToUseDialogListener {

    private final String dialogFor;

    public HowToUseFragment(String dialogFor) {
        this.dialogFor = dialogFor;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getDialog().setCanceledOnTouchOutside(false);
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogHowToUseBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_how_to_use, container, false);
        HowToUseDialogViewModel viewModel = ViewModelProviders.of(this).get(HowToUseDialogViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init(getActivity(), this, binding);
        viewModel.setDialogFor(dialogFor);
        return binding.getRoot();
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

}
