package com.netmedsmarketplace.netmeds.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.ArticleAdapter;
import com.netmedsmarketplace.netmeds.adpater.ConcernAdapter;
import com.netmedsmarketplace.netmeds.adpater.DealsOnTopBrandsAdapter;
import com.netmedsmarketplace.netmeds.adpater.MstarHomeBannerAdapter;
import com.netmedsmarketplace.netmeds.adpater.OfferAdapter;
import com.netmedsmarketplace.netmeds.adpater.PGOfferAdapter;
import com.netmedsmarketplace.netmeds.adpater.PrimeHomeOfferAdapter;
import com.netmedsmarketplace.netmeds.adpater.WellnessAdapter;
import com.netmedsmarketplace.netmeds.databinding.FragmentxHomeBinding;
import com.netmedsmarketplace.netmeds.viewmodel.HomeViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelFragment;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ArticleList;
import com.nms.netmeds.base.model.ConcernSlides;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.consultation.model.DefaultAddress;
import com.nms.netmeds.consultation.view.HistoryActivity;
import com.webengage.sdk.android.WebEngage;

import java.util.List;

public class HomeFragment extends BaseViewModelFragment<HomeViewModel> implements HomeViewModel.HomeListener, OfferAdapter.BannerOfferInterface, InstallStateUpdatedListener, DealsOnTopBrandsAdapter.DealsAdapterListener {

    private FragmentxHomeBinding homeBinding;
    private HomeViewModel homeViewModel;
    private ConfigurationResponse configurationResponse;

    private AppUpdateManager appUpdateManager;
    private Task<AppUpdateInfo> appUpdateInfoTask;
    private static int REQUEST_CODE_FOR_FLEXIBLE = 556;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        appUpdateManager = AppUpdateManagerFactory.create(getActivity());
        appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateManager.registerListener(this);
        // store initial object to avoid null - > default address
        if (TextUtils.isEmpty(BasePreference.getInstance(getContext()).getDefaultAddress())) {
            DefaultAddress defaultAddress = new DefaultAddress();
            BasePreference.getInstance(getContext()).saveDefaultAddress(new Gson().toJson(defaultAddress));
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (savedInstanceState == null || homeBinding == null) {
            homeBinding = DataBindingUtil.inflate(inflater, R.layout.fragmentx_home, container, false);
            homeBinding.lLDiagnostic.setVisibility(View.GONE);
            homeBinding.lLConsultation.setVisibility(View.GONE);
            homeBinding.setViewModel(onCreateViewModel());

            if (getActivity() != null) {
                ((NavigationActivity) getActivity()).setSupportActionBar(homeBinding.toolbar);
                homeBinding.toolbar.setLogo(R.drawable.ic_netmeds_home_logo);
            }
        }
        return homeBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (homeBinding != null) {
            homeBinding.swipeRefresh.setSwipeableChildren(R.id.offer_view, R.id.wellness_view);
            homeBinding.swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorMediumPink));
            homeBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initiateRefresh();
                }
            });
        }
    }

    private void initiateRefresh() {
        setDashboardView();
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void swipeRefresh(boolean swipeRefresh) {
        homeBinding.swipeRefresh.setRefreshing(swipeRefresh);
    }

    @Override
    public void offerAdapter(List<MstarBanner> homeMstarBanner) {
        if (homeMstarBanner != null && homeMstarBanner.size() > 0) {
            homeBinding.offerView.setLayoutManager(linearLayoutManager());
            MstarHomeBannerAdapter adapter = new MstarHomeBannerAdapter(homeMstarBanner, GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS, getActivity());
            homeBinding.offerView.setAdapter(adapter);
            CommonUtils.snapHelper(homeBinding.offerView);
            /*Google Tag Manager + FireBase Promotion Impression Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(getActivity(), homeMstarBanner, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_MAIN, 1);
        } else
            homeBinding.offerView.setVisibility(View.GONE);

    }

    @Override
    public void pgOfferAdapter(final List<MstarNetmedsOffer> offerList) {
        if (offerList != null && offerList.size() > 0) {
            homeBinding.paymentOfferView.setLayoutManager(linearLayoutManager());
            PGOfferAdapter wellnessAdapter = new PGOfferAdapter(getActivity(), offerList);
            homeBinding.paymentOfferView.setAdapter(wellnessAdapter);
            homeBinding.paymentOfferView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                        if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < offerList.size()) {
                            List<MstarNetmedsOffer> impressionList = offerList.subList(firstVisiblePosition, lastVisiblePosition + 1);
                            /*Google Tag Manager + FireBase Promotion Impression Event*/
                            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(getActivity(), impressionList, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_PAYMENT, firstVisiblePosition + 1);
                        }
                    }
                }
            });
        } else
            homeBinding.paymentOfferView.setVisibility(View.GONE);
    }

    @Override
    public void discountAdapter(final MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel) {
        if (mstarBasicResponseTemplateModel != null && mstarBasicResponseTemplateModel.getResult() != null && mstarBasicResponseTemplateModel.getResult().getPromotionBanners() != null && mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getPromotionBanners() != null && mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getPromotionBanners().size() > 0) {
            homeBinding.discountView.setLayoutManager(linearLayoutManager());
            OfferAdapter wellnessAdapter = new OfferAdapter(mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getPromotionBanners(), GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER, null, this);
            homeBinding.discountView.setAdapter(wellnessAdapter);
            homeBinding.lvPromotion.setVisibility(View.VISIBLE);
            homeBinding.discountView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                        if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getPromotionBanners().size()) {
                            List<MstarBanner> impressionList = mstarBasicResponseTemplateModel.getResult().getPromotionBanners().getPromotionBanners().subList(firstVisiblePosition, lastVisiblePosition + 1);
                            /*Google Tag Manager + FireBase Promotion Impression Event*/
                            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(getActivity(), impressionList, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_TRENDING, firstVisiblePosition + 1);
                        }
                    }
                }
            });
        } else
            homeBinding.lvPromotion.setVisibility(View.GONE);
    }

    @Override
    public void healthConcernAdapter(final MStarBasicResponseTemplateModel healthConcernCategory) {
        //Health concern cetegory
        if (healthConcernCategory != null && healthConcernCategory.getResult() != null && healthConcernCategory.getResult().getMstarHealthConcernCategory() != null && healthConcernCategory.getResult().getMstarHealthConcernCategory().getHealthConcernCategory() != null && healthConcernCategory.getResult().getMstarHealthConcernCategory().getHealthConcernCategory().size() > 0) {
            homeBinding.rlHealthConcern.setVisibility(View.VISIBLE);
            homeBinding.healthConcernView.setLayoutManager(linearLayoutManager());
            WellnessAdapter wellnessAdapter = new WellnessAdapter(getActivity(), healthConcernCategory.getResult().getMstarHealthConcernCategory().getHealthConcernCategory());
            homeBinding.healthConcernView.setAdapter(wellnessAdapter);
        } else {
            homeBinding.rlHealthConcern.setVisibility(View.GONE);
        }
        //Wellness products
        if (healthConcernCategory != null && healthConcernCategory.getResult() != null && healthConcernCategory.getResult().getWellnessProducts() != null && healthConcernCategory.getResult().getWellnessProducts().getWellnessProducts() != null && healthConcernCategory.getResult().getWellnessProducts().getWellnessProducts().size() > 0) {
            homeBinding.wellnessView.setVisibility(View.VISIBLE);
            homeBinding.wellnessView.setLayoutManager(linearLayoutManager());
            WellnessAdapter wellnessAdapter = new WellnessAdapter(getActivity(), healthConcernCategory.getResult().getWellnessProducts().getWellnessProducts());
            homeBinding.wellnessView.setAdapter(wellnessAdapter);
        } else {
            homeBinding.wellnessView.setVisibility(View.GONE);
        }

        //Deals on top brands
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null
                && healthConcernCategory != null && healthConcernCategory.getResult() != null && healthConcernCategory.getResult().getDealsOnTopBrands() != null
                && healthConcernCategory.getResult().getDealsOnTopBrands().getProductDetails() != null
                && healthConcernCategory.getResult().getDealsOnTopBrands().getProductDetails().size() > 0) {
            Glide.with(homeBinding.getRoot()).load(healthConcernCategory.getResult().getDealsOnTopBrands().getBgImage())
                    .into(homeBinding.dealsImage);
            homeBinding.dealsOnTopBrandsRl.setVisibility(View.VISIBLE);
            homeBinding.dealsOnTopBrandsCardView.setCardBackgroundColor(Color.parseColor(!TextUtils.isEmpty(healthConcernCategory.getResult().getDealsOnTopBrands().getBgColor()) ? healthConcernCategory.getResult().getDealsOnTopBrands().getBgColor() : "#ffffff"));
            homeBinding.dealsOnTopBrandsView.setVisibility(View.VISIBLE);
            homeBinding.dealsOnTopBrandsView.setLayoutManager(linearLayoutManager());
            DealsOnTopBrandsAdapter dealsOnTopBrandsAdapter = new DealsOnTopBrandsAdapter(getActivity(), healthConcernCategory.getResult().getDealsOnTopBrands().getProductDetails(), this, healthConcernCategory.getResult().getDealsOnTopBrands().getType());
            homeBinding.dealsOnTopBrandsView.setAdapter(dealsOnTopBrandsAdapter);
            homeBinding.dealsOnTopBrandsView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                        if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < healthConcernCategory.getResult().getDealsOnTopBrands().getProductDetails().size()) {
                            List<MStarProductDetails> impressionList = healthConcernCategory.getResult().getDealsOnTopBrands().getProductDetails().subList(firstVisiblePosition, lastVisiblePosition + 1);
                            /*Google Tag Manager + FireBase Promotion Impression Event*/
                            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(getActivity(), impressionList, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_DEALS, firstVisiblePosition + 1);
                        }
                    }
                }
            });
        } else {
            homeBinding.dealsOnTopBrandsRl.setVisibility(View.GONE);
            homeBinding.dealsOnTopBrandsView.setVisibility(View.GONE);
            homeBinding.dealsOnTopBrandsCardView.setVisibility(View.GONE);
        }
        //Brands
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null
                && healthConcernCategory != null && healthConcernCategory.getResult() != null && healthConcernCategory.getResult().getBrands() != null
                && healthConcernCategory.getResult().getBrands().getProductDetails() != null
                && healthConcernCategory.getResult().getBrands().getProductDetails().size() > 0) {
            homeBinding.brandsSale.setVisibility(View.VISIBLE);
            homeBinding.brandsSaleCardView.setCardBackgroundColor(Color.parseColor(!TextUtils.isEmpty(healthConcernCategory.getResult().getBrands().getBgColor()) ? healthConcernCategory.getResult().getBrands().getBgColor() : "#ffffff"));
            homeBinding.brandsSaleView.setVisibility(View.VISIBLE);
            homeBinding.brandsSaleView.setLayoutManager(linearLayoutManager());
            DealsOnTopBrandsAdapter dealsOnTopBrandsAdapter = new DealsOnTopBrandsAdapter(getActivity(), healthConcernCategory.getResult().getBrands().getProductDetails(), this, healthConcernCategory.getResult().getBrands().getType());
            homeBinding.brandsSaleView.setAdapter(dealsOnTopBrandsAdapter);
            homeBinding.brandsSaleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                        if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < healthConcernCategory.getResult().getBrands().getProductDetails().size()) {
                            List<MStarProductDetails> impressionList = healthConcernCategory.getResult().getBrands().getProductDetails().subList(firstVisiblePosition, lastVisiblePosition + 1);
                            /*Google Tag Manager + FireBase Promotion Impression Event*/
                            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(getActivity(), impressionList, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_DEALS, firstVisiblePosition + 1);
                        }
                    }
                }
            });
        } else {
            homeBinding.brandsSale.setVisibility(View.GONE);
        }
    }

    @Override
    public LinearLayoutManager linearLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return linearLayoutManager;
    }

    @Override
    public FragmentxHomeBinding binding() {
        return homeBinding;
    }

    @Override
    public void primeHomeAdapter(List<String> asList) {
        if (asList != null && asList.size() > 0) {
            PrimeHomeOfferAdapter primeHomeOfferAdapter = new PrimeHomeOfferAdapter(getActivity(), asList);
            homeBinding.primeOfferList.setAdapter(primeHomeOfferAdapter);
        } else
            homeBinding.primeOfferList.setVisibility(View.GONE);
    }

    @Override
    public void healthArticleAdapter(List<ArticleList> articleList) {
        if (articleList != null && articleList.size() > 0) {
            homeBinding.articleView.setLayoutManager(linearLayoutManager());
            ArticleAdapter offerAdapter = new ArticleAdapter(getActivity(), articleList);
            homeBinding.articleView.setAdapter(offerAdapter);
            CommonUtils.snapHelper(homeBinding.articleView);
        } else
            homeBinding.articleView.setVisibility(View.GONE);
    }

    @Override
    public void showUpdateAppBanner(boolean isUpdateAvailable) {
        homeBinding.updateAppBanner.setVisibility(isUpdateAvailable ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onFlexibleUpdate() {
        showUpdateAppBanner(false);
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo result) {
                try {
                    appUpdateManager.startUpdateFlowForResult(result, AppUpdateType.FLEXIBLE, getActivity(), REQUEST_CODE_FOR_FLEXIBLE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void showSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(homeBinding.swipeRefresh, getContext(), message);
    }

    @Override
    public void vmNavigateToBuyAgainProducts() {
        startActivity(new Intent(getActivity(), BuyAgainActivity.class));
    }

    @Override
    public void navigateGenericPage() {
        startActivity(new Intent(getActivity(), GenericHomePageActivity.class));
    }

    @Override
    public void navigateMedicinePageActivity() {
        startActivity(new Intent(getActivity(), MedicineHomePageActivity.class));
    }

    @Override
    protected HomeViewModel onCreateViewModel() {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.init(this, BasePreference.getInstance(getActivity()));
        homeViewModel.getDashboardResponseMutableLiveData().observe(this, new DashboardDataObserver());
        homeViewModel.getHealthconcernAndWellnessProductsResponseMutableLiveData().observe(this, new HealthConcernObserver());
        homeViewModel.getPromotionBannerResponseMutableLiveData().observe(this, new DiscountSaleObserver());
        homeViewModel.getHealthArticleResponseMutableLiveData().observe(this, new HealthArticleObserver());
        homeViewModel.getPgOfferResponseMutableLiveData().observe(this, new PGOfferObserver());
        homeViewModel.getInstallStateMutableLiveData().observe(this, new AppUpdateObserver());
        configurationResponse = new Gson().fromJson(BasePreference.getInstance(getActivity()).getConfiguration(), ConfigurationResponse.class);
        setDashboardView();
        consultationImage();
        setWinterBackGround();
        setImage();
        return homeViewModel;
    }

    private void consultationImage() {
        Glide.with(this)
                .load(configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getTopPrescriptionIcon()) ? configurationResponse.getResult().getConfigDetails().getConsultationUploadConfigs().getTopPrescriptionIcon() : "")
                .error(Glide.with(homeBinding.consultationImage).load(R.drawable.ic_online_consult_illustration)).into(homeBinding.consultationImage);
    }

    private void setWinterBackGround() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getWinterBackgroundColor()))
            homeBinding.winterSaleLayout.setBackgroundColor(Color.parseColor(configurationResponse.getResult().getConfigDetails().getWinterBackgroundColor()));
    }

    private void setImage() {
        Glide.with(this).load(configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getTopPrescriptionIcon()) ? configurationResponse.getResult().getConfigDetails().getM2PrescriptionUploadConfigs().getTopPrescriptionIcon() : "").error(Glide.with(homeBinding.topIcon).load(R.drawable.ic_online_consult_illustration)).into(homeBinding.topIcon);
    }


    private void setDashboardView() {
        homeViewModel.getHomeBannerData();
        homeViewModel.getHealthConcerns();
        homeViewModel.getDiscountSale();
        homeViewModel.getHealthArticle();
        onPopularConcern();
        homeViewModel.getSeasonSale();
        homeViewModel.getWellness();
        homeViewModel.getPGOffer();
        homeViewModel.getPaymentGatewayDetails();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        setOfferMenuItem(menu);
        if (BasePreference.getInstance(getActivity()).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_cart_white));
        else
            menu.getItem(1).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shopping_cart_white));
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setOfferMenuItem(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.allOffer).setActionView(R.layout.home_action_view);
        menuItem.getActionView().findViewById(R.id.offer_image_view_icon).startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.anim_shake));
        menuItem.getActionView().findViewById(R.id.offer_image_view_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToAllOffer();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.cart:
                /*Google Analytics Click Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(getActivity(), GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_CART, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                navigateToCart();
                return true;
            default:
                return false;
        }

    }

    private void navigateToCart() {
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        startActivity(new Intent(getActivity(), MStarCartActivity.class));
    }

    @Override
    public void vmNavigateSearch() {
        /**
         * IntentConstant.IS_FROM_HOME_SCREEN is used to check whether navigation of cart page is initiate from home page.
         */
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        intent.putExtra(IntentConstant.IS_FROM_HOME_SCREEN, true);
        startActivity(intent);
    }

    @Override
    public void vmNavigateWellness() {
        startActivity(new Intent(getActivity(), WellnessActivity.class));
    }

    @Override
    public void vmNavigateConsultation() {
        startActivity(new Intent(getActivity(), HistoryActivity.class));
    }

    @Override
    public void vmNavigateDiagnostic() {
        if (getActivity() != null)
            LaunchIntentManager.routeToActivity(getString(R.string.route_diagnostic_home), getActivity());
    }

    @Override
    public void vmUploadPrescription() {
        startActivity(new Intent(getActivity(), M2AttachPrescriptionActivity.class));
    }

    @Override
    public void vmNavigateHealthArticle(String title) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.WEB_PAGE_URL, ConfigMap.getInstance().getProperty(ConfigConstant.HEALTH_LIBRARY_URL));
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, "");
        intent.putExtra(IntentConstant.IS_FROM_ARTICLE, true);
        if (getActivity() != null)
            LaunchIntentManager.routeToActivity(getString(R.string.route_netmeds_web_view), intent, getActivity());
    }

    @Override
    public void vmReferralNavigation() {
        if (homeViewModel.checkUserLogin())
            startActivity(new Intent(getActivity(), ReferEarnActivity.class));
    }

    @Override
    public void vmShowProgress() {
        homeBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        homeBinding.progressBar.setVisibility(View.GONE);
    }

    public void smoothScrollTop() {
        homeBinding.scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                homeBinding.scrollView.fullScroll(View.FOCUS_UP);
            }
        }, 100);
    }

    @Override
    public void redirectToLogin() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), SignInActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void vmNavigationPrime(View view) {
        LaunchIntentManager.routeToActivity(view.getContext().getResources().getString(R.string.route_prime_membership_activity), view.getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
            /*Google Analytics Post Screen*/
            GoogleAnalyticsHelper.getInstance().postScreen(getActivity(), GoogleAnalyticsHelper.POST_SCREEN_HOME_PAGE);
            BasePreference.getInstance(getActivity()).setOutOfStockProductMessage("");
        }
        resetFlag();
    }

    private void resetFlag() {
        M2Helper.getInstance().clearData();
        PaymentHelper.setTempCartId(0);
        PaymentHelper.setIsTempCart(false);
        MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().clear();
    }

    @Override
    public void imageClickListener(String bannerFrom, MstarBanner mstarBanner) {
        homeViewModel.googleEvent(bannerFrom);
        CommonUtils.navigateToScreens(getContext(), mstarBanner, (Activity) getContext());

    }

    @Override
    public String getTitleText() {
        return "";
    }

    private void onStateUpdateListener(InstallState state) {
        if (state != null) {
            switch (state.installStatus()) {
                case InstallStatus.DOWNLOADING:
                    notifyUser();
                    break;
                case InstallStatus.DOWNLOADED:
                    if (appUpdateManager != null) {
                        appUpdateManager.completeUpdate();
                    }
                    break;
                case InstallStatus.INSTALLED:
                    appUpdateManager.unregisterListener(this);
                    break;
            }
        }
    }

    private void notifyUser() {
        showUpdateAppBanner(true);
        homeBinding.downloadNowLayout.setVisibility(View.GONE);
        homeBinding.downloadingNowLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStateUpdate(InstallState state) {
        homeViewModel.setInstallStateMutableLiveData(state);
    }

    @Override
    public void onDealsBannerClick(MStarProductDetails productDetails, String type) {
        if (!TextUtils.isEmpty(type) && AppConstant.CATEGORY.equalsIgnoreCase(type)) {
            Intent intent = new Intent(getActivity(), CategoryActivity.class);
            intent.putExtra(IntentConstant.CATEGORY_ID, productDetails.getId());
            intent.putExtra(IntentConstant.CATEGORY_NAME, productDetails.getName());
            startActivity(intent);
        } else if (!TextUtils.isEmpty(type) && AppConstant.MANUFACTURER.equalsIgnoreCase(type)) {
            Intent intent = new Intent(getActivity(), ProductList.class);
            intent.putExtra(IntentConstant.MANUFACTURER_ID, productDetails.getId());
            intent.putExtra(IntentConstant.MANUFACTURER_NAME, productDetails.getName());
            startActivity(intent);
        } else if (!TextUtils.isEmpty(type) && AppConstant.BRAND.equalsIgnoreCase(type)) {
            Intent intent = new Intent(getActivity(), ProductList.class);
            intent.putExtra(IntentConstant.BRAND_URL, !TextUtils.isEmpty(productDetails.getUrl()) ? productDetails.getUrl() : "");
            intent.putExtra(IntentConstant.BRAND_NAME, !TextUtils.isEmpty(productDetails.getName()) ? productDetails.getName() : "");
            intent.putExtra(IntentConstant.INTENT_FROM_BRAND_URL, AppConstant.URL);
            startActivity(intent);
        }
    }

    private class AppUpdateObserver implements Observer<InstallState> {

        @Override
        public void onChanged(InstallState installState) {
            onStateUpdateListener(installState);
        }
    }

    private class DashboardDataObserver implements Observer<MstarBasicResponseResultTemplateModel> {

        @Override
        public void onChanged(@Nullable MstarBasicResponseResultTemplateModel response) {
            homeViewModel.onOfferDataAvailable(response);
            homeBinding.setViewModel(homeViewModel);
        }
    }

    private class DiscountSaleObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel) {
            homeViewModel.onDiscountSaleAvailable(mstarBasicResponseTemplateModel);
            homeBinding.setViewModel(homeViewModel);
        }
    }

    private class HealthArticleObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            homeViewModel.onHealthArticleAvailable(response);
            homeBinding.setViewModel(homeViewModel);
        }
    }

    private class PGOfferObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel response) {
            homeViewModel.onPGOfferAvailable(response);
            homeBinding.setViewModel(homeViewModel);
        }
    }

    private class HealthConcernObserver implements Observer<MStarBasicResponseTemplateModel> {

        @Override
        public void onChanged(@Nullable MStarBasicResponseTemplateModel mstarBasicResponseTemplateModel) {
            homeViewModel.onHealthConcernAvailable(mstarBasicResponseTemplateModel);
            homeBinding.setViewModel(homeViewModel);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_HOME_PAGE);
    }

    private void navigateToAllOffer() {
        startActivity(new Intent(getActivity(), OfferActivity.class));
    }

    private void onPopularConcern() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getConcernConfigs() != null && configurationResponse.getResult().getConfigDetails().getConcernConfigs().getSlidesList() != null && configurationResponse.getResult().getConfigDetails().getConcernConfigs().getSlidesList().size() > 0) {
            homeBinding.title1Tv.setText(configurationResponse.getResult().getConfigDetails().getConcernConfigs().getTitle1() != null ? configurationResponse.getResult().getConfigDetails().getConcernConfigs().getTitle1() : "");
            homeBinding.title2Tv.setText(configurationResponse.getResult().getConfigDetails().getConcernConfigs().getTitle2() != null ? configurationResponse.getResult().getConfigDetails().getConcernConfigs().getTitle2() : "");
            homeBinding.descpTv.setText(configurationResponse.getResult().getConfigDetails().getConcernConfigs().getDescp() != null ? configurationResponse.getResult().getConfigDetails().getConcernConfigs().getDescp() : "");
            homeBinding.popularView.setLayoutManager(linearLayoutManager());
            ConcernAdapter concernAdapter = new ConcernAdapter(getActivity(), configurationResponse.getResult().getConfigDetails().getConcernConfigs().getSlidesList(), true, false);
            homeBinding.popularView.setAdapter(concernAdapter);
        } else {
            homeBinding.popularConcernRl.setVisibility(View.GONE);
        }
        onPopularDiagnosisConcern();
        initConsultationBanner();
        initDiagnosisBanner();
    }


    public void onPopularDiagnosisConcern() {
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getSlidesList() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getSlidesList().size() > 0) {
            homeBinding.title1Diagnosis.setText(configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getTitle1() != null ? configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getTitle1() : "");
            homeBinding.title2Diagnosis.setText(configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getTitle2() != null ? configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getTitle2() : "");
            homeBinding.descDiagnosis.setText(configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getDescp() != null ? configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getDescp() : "");
            homeBinding.popularTestPackages.setLayoutManager(linearLayoutManager());
            ConcernAdapter concernAdapter = new ConcernAdapter(getActivity(), configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getSlidesList(), true, true);
            homeBinding.popularTestPackages.setAdapter(concernAdapter);
            homeBinding.popularTestPackages.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                        if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getSlidesList().size()) {
                            List<ConcernSlides> impressionList = configurationResponse.getResult().getConfigDetails().getDiagnosisConfigs().getSlidesList().subList(firstVisiblePosition, lastVisiblePosition + 1);
                            /*Google Tag Manager + FireBase Promotion Impression Event*/
                            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(getActivity(), impressionList, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_DIAGNOSTIC_POPULAR, firstVisiblePosition + 1);
                        }
                    }
                }
            });
        } else {
            homeBinding.popularDiagnosisRl.setVisibility(View.GONE);
        }
    }


    private void initConsultationBanner() {
        if (getActivity() != null && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getSliderConfigs() != null && configurationResponse.getResult().getConfigDetails().getSliderConfigs().getSlidesList() != null && configurationResponse.getResult().getConfigDetails().getSliderConfigs().getSlidesList().size() > 0) {
            final ConcernSlides slide = configurationResponse.getResult().getConfigDetails().getSliderConfigs().getSlidesList().get(0);
            Glide.with(getActivity()).load(slide.getImage()).apply(new RequestOptions().fitCenter()).into(homeBinding.consultationBannerImg);
            homeBinding.consultationBannerImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonUtils.consultationNavigateToScreens(getActivity(), slide, getActivity());
                }
            });
        } else {
            homeBinding.consultationBannerImg.setVisibility(View.GONE);
        }
    }


    private void initDiagnosisBanner() {
        if (getActivity() != null && configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisSliderConfigs() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisSliderConfigs().getSlidesList() != null && configurationResponse.getResult().getConfigDetails().getDiagnosisSliderConfigs().getSlidesList().size() > 0) {
            final ConcernSlides slide = configurationResponse.getResult().getConfigDetails().getDiagnosisSliderConfigs().getSlidesList().get(0);
            Glide.with(getActivity()).load(slide.getImage()).apply(new RequestOptions().fitCenter()).into(homeBinding.diagnosisBannerImg);
            homeBinding.diagnosisBannerImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonUtils.consultationNavigateToScreens(getActivity(), slide, getActivity());
                }
            });
        } else {
            homeBinding.diagnosisBannerImg.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        appUpdateManager.unregisterListener(this);
    }
}
