package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.SubscriptionAddPopupBinding;
import com.nms.netmeds.base.BaseDialogFragment;

@SuppressLint("ValidFragment")
public class SubscriptionPopUpDialog extends BaseDialogFragment {
    private SubscriptionAddPopupBinding binding;
    private SubscriptionPopupListener listener;
    private boolean isSubscriptionCancel;


    public SubscriptionPopUpDialog(SubscriptionPopupListener subscriptionPopupListener, boolean isSubscriptionCancel) {
        this.listener = subscriptionPopupListener;
        this.isSubscriptionCancel = isSubscriptionCancel;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.subscription_add_popup, container, false);
        binding.tvTitleDescription.setText(isSubscriptionCancel ? getString(R.string.text_do_you_want_cancel_subscription) : getString(R.string.text_do_you_want_add));

        onClickListener();
        return binding.getRoot();
    }

    private void onClickListener() {
        binding.btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDismissDialog("");
                dismissAllowingStateLoss();
            }
        });
        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
                listener.SubscriptionNo();
            }
        });

        binding.btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
                listener.subscriptionAdd();
            }
        });
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    listener.onDismissDialog("");
                    dismissAllowingStateLoss();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }


    public interface SubscriptionPopupListener {
        void subscriptionAdd();

        void SubscriptionNo();

        void onDismissDialog(String from);
    }
}
