package com.netmedsmarketplace.netmeds.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.PrescriptionItemListAdapter;
import com.netmedsmarketplace.netmeds.databinding.DialogM2UpdatedWarningBinding;
import com.netmedsmarketplace.netmeds.viewmodel.CommonWarningViewModel;
import com.nms.netmeds.base.BaseDialogFragment;

import java.util.ArrayList;

public class CommonWarningDialog extends BaseDialogFragment implements CommonWarningViewModel.M2UpdatedWarningDialogViewModelListener {

    private DialogM2UpdatedWarningBinding binding;
    private CommonWarningViewModel viewModel;
    private CommonWarningDialogListener listener;
    private String errorTitle;
    private String errorDescription;
    private String positiveBtnText;
    private String negativeBtnText;
    private String from;
    private ArrayList<String> outofStockProductNameList;

    public void setM2UpdatedWarningDialog(CommonWarningDialogListener listener, String errorTitle, String errorDescription, String positiveBtnText,
                                          String negativeBtnText, String from, ArrayList<String> outofStockProductNameList) {
        this.outofStockProductNameList = outofStockProductNameList;
        this.listener = listener;
        this.errorTitle = errorTitle;
        this.errorDescription = errorDescription;
        this.positiveBtnText = positiveBtnText;
        this.negativeBtnText = negativeBtnText;
        this.from = from;
    }

    private void setValues() {

        binding.tvTitle.setText(errorTitle);
        if (errorDescription != null) {
            binding.tvTitleDescription.setVisibility(View.VISIBLE);
            binding.outOfStockNameList.setVisibility(View.GONE);
            binding.tvTitleDescription.setText(errorDescription);
        } else {
            binding.tvTitleDescription.setVisibility(View.GONE);
            binding.outOfStockNameList.setVisibility(View.VISIBLE);
            setOutOfProductListAdapter();
        }

        if (positiveBtnText != null)
            binding.btnYes.setText(positiveBtnText);
        else
            binding.btnYes.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(negativeBtnText))
            binding.btnNo.setText(negativeBtnText);
        else
            binding.btnNo.setVisibility(View.GONE);
    }

    private void setOutOfProductListAdapter() {
        if (!outofStockProductNameList.isEmpty()) {
            PrescriptionItemListAdapter itemListAdapter = new PrescriptionItemListAdapter(getContext(), outofStockProductNameList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            binding.outOfStockNameList.setLayoutManager(linearLayoutManager);
            binding.outOfStockNameList.setAdapter(itemListAdapter);
        } else
            binding.outOfStockNameList.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(CommonWarningViewModel.class);
        viewModel.setM2UpdatedWarningDialogViewModelListener(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_m2_updated_warning, container, false);
        binding.setViewModel(viewModel);
        setValues();
        return binding.getRoot();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    onDismissButtonClicked();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public void onPositiveButtonClicked() {
        this.dismissAllowingStateLoss();
        listener.onPositiveButtonClicked(from);
    }

    @Override
    public void onNegativeButtonClicked() {
        this.dismissAllowingStateLoss();
        listener.onNegativeButtonClicked(from);
    }

    @Override
    public void onDismissButtonClicked() {
        listener.onDismissDialog(from);
        this.dismissAllowingStateLoss();
    }

    interface CommonWarningDialogListener {

        void onPositiveButtonClicked(String from);

        void onNegativeButtonClicked(String from);

        void onDismissDialog(String from);
    }
}
