package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityRateAppBinding;
import com.netmedsmarketplace.netmeds.viewmodel.RateAppViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

public class RateAppActivity extends BaseViewModelActivity<RateAppViewModel> implements RateAppViewModel.RateAppCallback, RateInPlayStorePopUp.RateInPlayStoreCallback {

    private ActivityRateAppBinding binding;
    private RateAppViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rate_app);
        binding.setViewModel(onCreateViewModel());
        toolBarSetUp(binding.raToolBarCustomLayout.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        initToolBar(binding.raToolBarCustomLayout.collapsingToolbar, getString(R.string.string_rate_app));
        vmDismissProgress();
    }

    @Override
    protected RateAppViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(RateAppViewModel.class);
        viewModel.init(BasePreference.getInstance(this), this);
        binding.btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.submitFeedback();
            }
        });
        onRetry(viewModel);
        return viewModel;
    }

    @Override
    public String getFeedback() {
        return !TextUtils.isEmpty(binding.etFeedback.getText()) ? binding.etFeedback.getText().toString().trim() : "";
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false, binding.raToolBarCustomLayout.progressBar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void updateRatingView(int ratingCount) {
        binding.imgRating1.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 1 ? com.nms.netmeds.payment.R.drawable.ic_rating_big_active : com.nms.netmeds.payment.R.drawable.ic_rating_light_bg_unfilled));
        binding.imgRating2.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 2 ? com.nms.netmeds.payment.R.drawable.ic_rating_big_active : com.nms.netmeds.payment.R.drawable.ic_rating_light_bg_unfilled));
        binding.imgRating3.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 3 ? com.nms.netmeds.payment.R.drawable.ic_rating_big_active : com.nms.netmeds.payment.R.drawable.ic_rating_light_bg_unfilled));
        binding.imgRating4.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 4 ? com.nms.netmeds.payment.R.drawable.ic_rating_big_active : com.nms.netmeds.payment.R.drawable.ic_rating_light_bg_unfilled));
        binding.imgRating5.setImageDrawable(ContextCompat.getDrawable(this, ratingCount >= 5 ? com.nms.netmeds.payment.R.drawable.ic_rating_big_active : com.nms.netmeds.payment.R.drawable.ic_rating_light_bg_unfilled));
        if (ratingCount > 0) {
            binding.btnSubmitFeedback.setBackground(getDrawable(R.drawable.teal_rectangle_background));
            binding.btnSubmitFeedback.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    @Override
    public void navigateBackToAccounts() {
        SnackBarHelper.snackBarCallBack(binding.clParentRating, this, getString(R.string.string_thanks_note));
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1500);
    }

    @Override
    public void showSnackbarMessage(String message) {
        SnackBarHelper.snackBarCallBack(binding.clParentRating, this, message);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true, binding.raToolBarCustomLayout.progressBar);
    }

    @Override
    public void openPlayStoreRatingPopUp() {
        RateInPlayStorePopUp rateInPlayStorePopUp = new RateInPlayStorePopUp(this);
        getSupportFragmentManager().beginTransaction().add(rateInPlayStorePopUp, "RateInPlayStore").commitNowAllowingStateLoss();
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }


    @Override
    public void navigateToPlayStore() {
        String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + (CommonUtils.isBuildVariantProdRelease() ? appPackageName : "com.NetmedsMarketplace.Netmeds"))));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + (CommonUtils.isBuildVariantProdRelease() ? appPackageName : "com.NetmedsMarketplace.Netmeds"))));
        }
        finish();
    }

    @Override
    public void navigateToAccountFragment() {
        finish();
    }

    @Override
    public void showNetworkErrorView(boolean isConnected) {
        binding.clParentRating.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.ratingNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.clParentRating.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.ratingApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }


}
