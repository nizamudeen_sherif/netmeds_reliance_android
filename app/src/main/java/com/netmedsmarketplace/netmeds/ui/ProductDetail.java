package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.JavaScriptInterface;
import com.netmedsmarketplace.netmeds.NetmedsApp;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.FrequentlyBroughtTogetherProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.MstarCouponCodeAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductBundleAdapter;
import com.netmedsmarketplace.netmeds.adpater.ProductVariantAdapter;
import com.netmedsmarketplace.netmeds.adpater.RelatedProductListAdapter;
import com.netmedsmarketplace.netmeds.adpater.SimilarProductsAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityProductDetailBinding;
import com.netmedsmarketplace.netmeds.viewmodel.MStarProductDetailsViewModel;
import com.netmedsmarketplace.netmeds.viewmodel.NotifyDialogueViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.DeliveryEstimateProductDetail;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.PincodeDeliveryEstimate;
import com.nms.netmeds.base.model.Variant;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;
import com.nms.netmeds.base.view.LinePagerIndicatorDecoration;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.base.widget.CustomNumberPicker;
import com.nms.netmeds.base.widget.CustomNumberPickerListener;
import com.nms.netmeds.base.zoomdismiss.ImageViewer;
import com.nms.netmeds.base.zoomdismiss.drawee.Fresco;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.VISIBLE;

public class ProductDetail extends BaseViewModelActivity<MStarProductDetailsViewModel> implements MStarProductDetailsViewModel.MStarProductDetailViewModelCallBack,
        CustomNumberPickerListener, PincodeCheckPopupDialog.PincodeCheckPopupDialogCallBack, OnBoardingAdapter.OnItemClickListener, M1ProductRequsetPopupDialogue.ProductRequestPopupListener,
        AddCartBottomSheetDialog.BottomSheetDialogListener, NotifyDialogueViewModel.NofifyFragmentDialogueListener, NotifyFragmentDialogue.NotifyDialogListener, AlternateFragment.AlternateProductsListener,
        ProductVariantAdapter.ProductVariantListener {

    private ActivityProductDetailBinding binding;
    private MStarProductDetailsViewModel viewModel;
    private Snackbar snackbar;
    private ImageViewer imageViewer;
    private int productCode;
    private boolean isBottomSheetOpen = false;
    private String pageName;
    private boolean isFromVariant = false;
    private boolean isAttached = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail);
        binding.setProductViewModel(onCreateViewModel());
        toolBarSetUp(binding.productToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected MStarProductDetailsViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(MStarProductDetailsViewModel.class);
        initIntentValue();
        viewModel.initiateViewModel(this, this, BasePreference.getInstance(this), productCode, pageName);
        viewModel.getMstarAlgoliaMutableLiveData().observe(this, new AlternateSaltObserver());
        onRetry(viewModel);
        return viewModel;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
        else
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                /*Google Analytics Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(ProductDetail.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_CART_ICON_PRODUCT_DETAILS, GoogleAnalyticsHelper.EVENT_LABEL_PRODUCT_DETAILS_PAGE);
                navigateToCart();
                return true;
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            default:
                return false;
        }
    }

    private void navigateToCart() {
        startActivity(new Intent(this, MStarCartActivity.class));
    }

    @Override
    public void hideAndShowParentView(boolean isVisible) {
        binding.productViewContent.setVisibility(isVisible && !isFromVariant ? View.GONE : View.VISIBLE);
    }

    @Override
    public void openManufacturerList(int manufacturerId, String manufacturerName, String productType) {
        Intent intent = new Intent(this, ProductList.class);
        intent.putExtra(IntentConstant.MANUFACTURER_ID, manufacturerId);
        intent.putExtra(IntentConstant.MANUFACTURER_NAME, manufacturerName);
        intent.putExtra(IntentConstant.PRODUCT_TYPE, productType);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void notifyDialogue() {
        if (BasePreference.getInstance(this).isGuestCart()) {
            MStarProductDetails.getInstance().setNotifyGuestUser(true);
            startActivity(new Intent(ProductDetail.this, SignInActivity.class));
        } else {
            MStarProductDetails.getInstance().setNotifyGuestUser(false);
            viewModel.submitProductRequest();
        }
    }

    @Override
    public void snackBarCallback(String message) {
        SnackBarHelper.snackBarCallBack(binding.productViewContent, this, message);

    }

    @Override
    public void hideAndShowViewAll(boolean isVisible) {
        binding.productClearAll.setVisibility(isVisible ? VISIBLE : View.GONE);
    }

    @Override
    public void hideRelatedProductView(boolean isVisible) {
        binding.productSearchHistoryTitle.setVisibility(isVisible ? VISIBLE : View.GONE);
    }

    @Override
    public void promoCodeAdapter(MstarCouponCodeAdapter promoCodeAdapter, boolean isClickOffer) {
        binding.tvAllOffer.setText(isClickOffer ? getString(R.string.view_all_less) : getString(R.string.view_all_offer));
        binding.rvPromocodeView.setVisibility(View.VISIBLE);
        binding.rvPromocodeView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.rvPromocodeView.setAdapter(promoCodeAdapter);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void offerViewVisibility(boolean isVisible) {
        binding.productOffersView.setVisibility(isVisible ? VISIBLE : View.GONE);
    }

    @Override
    public void navigateSiginActivity() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showAlternateProductForOutOfStock(String outOfStockProduct) {
        AlternateFragment alternateFragment = new AlternateFragment(Integer.valueOf(outOfStockProduct), BasePreference.getInstance(this), this);
        getSupportFragmentManager().beginTransaction().add(alternateFragment, "AlternateFragment").commitNowAllowingStateLoss();
    }

    @Override
    public boolean isFromSearchActivity() {
        Intent intent = getIntent();
        return intent != null && intent.getExtras() != null && intent.hasExtra(IntentConstant.IS_FROM_SEARCH) && intent.getBooleanExtra(IntentConstant.IS_FROM_SEARCH, false);
    }

    @Override
    public void setProductVariantVisibility(boolean isVariantVisible) {
        binding.cvProductVariant.setVisibility(isVariantVisible ? View.VISIBLE : View.GONE);
    }


    @Override
    public void navigateToProductDetailsPage(int productCode) {
        Intent intent = new Intent(this, ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
        startActivity(intent);
    }

    @Override
    public void launchAddToCartBottomSheet(MStarProductDetails productDetails) {
        if (!isBottomSheetOpen) {
            isBottomSheetOpen = true;
            AddCartBottomSheetDialog couponFragment = new AddCartBottomSheetDialog(productDetails, false, this);
            getSupportFragmentManager().beginTransaction().add(couponFragment, "AddCartBottomSheetDialog").commitNowAllowingStateLoss();
        }
    }

    @Override
    public String getImageMoreText(int imageListSize) {
        return String.format(getString(R.string.text_more_photos), imageListSize);
    }

    @Override
    public void navigateToHomePage() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        LaunchIntentManager.routeToActivity(getString(R.string.route_navigation_activity), intent, this);
    }

    @Override
    public void vmAddCartCallBack() {
        dismissProgress();
        PaymentHelper.setProductDetailReloadCart(true);
        PaymentHelper.setReloadCart(true);
        CommonUtils.initiateVibrate(this);
        SubscriptionHelper.getInstance().setEditorderquantityflag(SubscriptionHelper.getInstance().isPayNowSubscription());
        showMessage(true, null, R.string.text_view_cart, R.string.text_added_to_cart, null);
    }

    @Override
    public void reInitiateDataUsingViewModel(boolean productAvailableWithStock) {
        binding.setProductViewModel(viewModel);
        if (!productAvailableWithStock) {
            binding.productPackProducts.packProductLayout.setVisibility(View.GONE);
            binding.similarProductInclude.similarLayout.setVisibility(View.GONE);
            binding.productCombinationInclude.bundleProductLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void checkPinCodeAvailability(boolean isFromColdStorageCheck, String productName) {
        if (isFromColdStorageCheck) {
            binding.availablePincode.setVisibility(View.GONE);
        }
        if (!isBottomSheetOpen) {
            isBottomSheetOpen = true;
            PincodeCheckPopupDialog productCheckPincodePopupDialogue = new PincodeCheckPopupDialog(this, isFromColdStorageCheck, productCode, viewModel.getProductSelectedQuantity(), productName);
            getSupportFragmentManager().beginTransaction().add(productCheckPincodePopupDialogue, "RequestDialog").commitNowAllowingStateLoss();
        }
    }

    @Override
    public void loadProductImages(List<String> imagePathList) {
        if (isAttached) {
            if (NetmedsApp.getInstance() != null) {
                Glide.with(NetmedsApp.getInstance())
                        .load(ImageUtils.checkImage(imagePathList.size() > 0 ? imagePathList.get(0) : null))
                        .error(Glide.with(binding.imgProductImageForRxProduct).load(R.drawable.ic_no_image))
                        .into(binding.imgProductImageForRxProduct);
            }
            loadProductImagesWithViews(imagePathList, binding.productRecyclerView);
        }
    }

    private void loadProductImagesWithViews(List<String> imagePathList, RecyclerView view) {
        OnBoardingAdapter adapter = new OnBoardingAdapter(this, null, imagePathList, true, this);
        view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        view.setAdapter(adapter);
        if (imagePathList.size() > 1) {
            view.addItemDecoration(new LinePagerIndicatorDecoration(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorSecondary)));
        }
        CommonUtils.snapHelper(view);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        isAttached = true;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        isAttached = false;
    }

    @Override
    public void requestProduct() {
        if (!isBottomSheetOpen) {
            isBottomSheetOpen = true;
            if (!BasePreference.getInstance(this).isGuestCart()) {
                M1ProductRequsetPopupDialogue reschedulePopUpDialog = new M1ProductRequsetPopupDialogue(this, true);
                getSupportFragmentManager().beginTransaction().add(reschedulePopUpDialog, "RequestDialog").commitNowAllowingStateLoss();
            } else {
                Intent intent = new Intent(this, SignInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    //PEOPLE ALSO VIEWED
    @Override
    public void setRelatedProductAdapter(RelatedProductListAdapter relatedProductListAdapter) {
        if (relatedProductListAdapter != null) {
            binding.relatedProductView.setVisibility(View.VISIBLE);
            binding.rvRelatedProdcutList.setLayoutManager(new LinearLayoutManager(this));
            binding.rvRelatedProdcutList.setAdapter(relatedProductListAdapter);
        } else
            binding.relatedProductView.setVisibility(View.GONE);
    }

    //PACK OF BUY
    @Override
    public void setPayOfBuyViewWithAdapter(ProductBundleAdapter adapter) {
        if (adapter != null) {
            binding.productPackProducts.tvPackOfBuyTitle.setText(viewModel.getPackOfBuyTitle());
            binding.productPackProducts.packProductLayout.setVisibility(View.VISIBLE);
            binding.productPackProducts.productCombination.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            binding.productPackProducts.productCombination.setAdapter(adapter);
        } else {
            binding.productPackProducts.packProductLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public String getPackStringWithQty(Integer qty) {
        return String.format(getString(R.string.text_pack_of), qty);
    }

    //SIMILAR PRODUCT
    @Override
    public void initiateSimilarProductList(SimilarProductsAdapter adapter) {
        if (adapter != null) {
            binding.similarProductInclude.similarProductTitle.setText(viewModel.getSimilarEnableTitle());
            binding.similarProductInclude.similarProduct.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            binding.similarProductInclude.similarProduct.setAdapter(adapter);
            binding.similarProductInclude.similarLayout.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        } else {
            binding.similarProductInclude.similarLayout.setVisibility(View.GONE);
        }
    }

    //FREQUENTLY BROUGHT TOGETHER
    @Override
    public void initiateFrequentlyBroughtTogetherProductList(FrequentlyBroughtTogetherProductAdapter adapter) {
        if (adapter != null) {
            binding.productCombinationInclude.combinationAmount.setText(String.valueOf(viewModel.getCombinationProductTotalAmount()));
            binding.productCombinationInclude.bundleTitle.setText(viewModel.getComboEnableTitleText());
            binding.productCombinationInclude.includeBundleProductCombination.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            binding.productCombinationInclude.includeBundleProductCombination.setAdapter(adapter);
            binding.productCombinationInclude.bundleProductLayout.setVisibility(View.VISIBLE);
            binding.productCombinationInclude.includeBundleBuyPack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewModel.addAllProducts();
                }
            });
            adapter.notifyDataSetChanged();
        } else {
            binding.productCombinationInclude.bundleProductLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClickListenerFromSearch(List<String> urlList) {
        Fresco.initialize(this);
        imageViewer = new ImageViewer.Builder(ProductDetail.this, urlList).setStartPosition(0).build();
        imageViewer.show();
    }

    @Override
    public void onItemClickListener(List<String> urlList, int position) {
        Fresco.initialize(this);
        imageViewer = new ImageViewer.Builder(ProductDetail.this, urlList).setStartPosition(position).build();
        imageViewer.show();

    }

    @Override
    public void loadProductContentWithUrl(String url) {
        binding.productWebview.loadUrl(url);
        setWebView(binding.productWebview);
        initJavaScriptCallBack();
    }

    public void initJavaScriptCallBack() {
        JavaScriptInterface javaScriptInterface = new JavaScriptInterface(this, null);
        binding.productWebview.addJavascriptInterface(javaScriptInterface, getString(R.string.java_script_interface));
    }

    @Override
    public void setPaintForStrikePrize() {
        binding.productDetailsStrikePrice.setPaintFlags(binding.productDetailsStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public void initiateProductQuantityPicker(int maxSelectableQuantity) {
        CustomNumberPicker numberPicker = new CustomNumberPicker(this);
        numberPicker.setMaxValue(maxSelectableQuantity);
        numberPicker.setListener(this);
        binding.productQtyPicker.removeAllViews();
        binding.productQtyPicker.addView(numberPicker);
    }

    @Override
    public String showAppliedDefaultDiscount(BigDecimal discount) {
        return String.format(getString(R.string.text_product_details_applied_default_discount), CommonUtils.getRoundedOffBigDecimal(discount) + getString(R.string.text_off));
    }

    @Override
    public void onHorizontalNumberPickerChanged(CustomNumberPicker customNumberPicker, int value) {
        viewModel.setProductSelectedInQtyPicker(value);
        viewModel.setProductSelectedQuantity(value);
    }

    @Override
    public void showProgress() {
        vmShowProgress();
    }

    @Override
    public void dismissLoader() {
        vmDismissProgress();
    }

    @Override
    public void pinCodeSupportColdStorage(boolean isAvailable) {
        binding.availablePincode.setVisibility(View.VISIBLE);
        binding.availablePincode.setTextColor(getResources().getColor(R.color.colorGreen));
        binding.availablePincode.setText(getString(R.string.text_available));
        binding.availablePincode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle_green, 0, 0, 0);
    }

    @Override
    public void expiryAndDeliveryEstimate(DeliveryEstimateProductDetail expiryDetails, PincodeDeliveryEstimate delieryDetails) {
        binding.productExpiryAndDeliveryDateLayout.checkExpiryAndDeliveryDateLayout.setVisibility(View.GONE);
        binding.productExpiryAndDeliveryDateLayout.resultExpiryDeliveryDateLayout.setVisibility(View.VISIBLE);
        binding.productExpiryAndDeliveryDateLayout.expiryDate.setText(!TextUtils.isEmpty(expiryDetails.getExpiryDate()) ? DateTimeUtils.getInstance().stringDate(expiryDetails.getExpiryDate(), DateTimeUtils.yyyyMMdd, DateTimeUtils.MMMyyyy) : "");
        binding.productExpiryAndDeliveryDateLayout.deliveryDate.setText(!TextUtils.isEmpty(delieryDetails.getFormatted().getFormat2()) ? delieryDetails.getFormatted().getFormat2() : "");
    }

    @Override
    public void pinCodeCheckPopupClose() {
        isBottomSheetOpen = false;
    }

    @Override
    public void vmShowProgress() {
        binding.progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        binding.productViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.productDetailsNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.productViewContent.setVisibility(!isWebserviceError ? View.VISIBLE : View.GONE);
        binding.productDetailsApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showMessage(boolean isActionEnabled, String message, int actionMessageId, int id, Object data) {
        snackbar = Snackbar.make(binding.productDetailsSvView, message != null ? message : getString(id), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(isActionEnabled ? getString(actionMessageId) : "", isActionEnabled ? snackBackActionListener(data, actionMessageId) : null);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void snackBarCallBack(String message) {
        SnackBarHelper.snackBarCallBack(binding.productViewContent, this, message);
    }

    @Override
    public void notifyDialogClose() {
        isBottomSheetOpen = false;
    }

    @Override
    public void addAlternateProduct(int productToAdd, int quantity, int outOfStockProductCode) {
        viewModel.addAlternateSalt(productToAdd, quantity, outOfStockProductCode);
    }

    @Override
    public void onViewClosed(Integer outOfStockProductCode, boolean isJustDismiss) {
        if (!viewModel.outOfStockProductCodeList.isEmpty() && viewModel.outOfStockProductCodeList.contains(String.valueOf(outOfStockProductCode)))
            viewModel.outOfStockProductCodeList.remove(String.valueOf(outOfStockProductCode));
        viewModel.showAlternateProduct();
    }

    @Override
    public void onSelectedVariant(int productId) {
        setIsFromVariantFlagChange(true);
        viewModel.resetDataAndInitiateProductDetailsCall(productId);
    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            /*Google Analytics Event*/
            GoogleAnalyticsHelper.getInstance().postEvent(ProductDetail.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_VIEW_CART_PRODUCT_DETAILS, GoogleAnalyticsHelper.EVENT_LABEL_PRODUCT_DETAILS_PAGE);
            Intent intent = new Intent(ProductDetail.this, MStarCartActivity.class);
            startActivity(intent);
        }
    }

    private View.OnClickListener snackBackActionListener(final Object data, final int actionMessageId) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
                switch (actionMessageId) {
                    case R.string.text_view_cart:
                        GoogleAnalyticsHelper.getInstance().postEvent(ProductDetail.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_VIEW_CART_PRODUCT_DETAILS, GoogleAnalyticsHelper.EVENT_LABEL_PRODUCT_DETAILS_PAGE);
                        navigateToCart();
                        break;
                }
            }
        };
    }

    @Override
    public void onClickRequest(String patientName, String phoneNumber, String medicineName) {
        viewModel.initiateRequestProductAPICall(patientName, medicineName, phoneNumber);
    }

    @Override
    public String medicineName() {
        return viewModel.productName();
    }

    @Override
    public void productRequestPopupClose() {
        isBottomSheetOpen = false;
    }

    @Override
    public void bsAddCartCallBack() {
        CommonUtils.initiateVibrate(this);
        Snackbar snackbar = Snackbar.make(binding.productViewContent, ProductDetail.this.getResources().getString(R.string.text_added_to_cart), Snackbar.LENGTH_LONG);
        snackbar.setAction(ProductDetail.this.getResources().getString(R.string.text_view_cart), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void showAlert(String message) {
        SnackBarHelper.snackBarCallBack(binding.productViewContent, this, message);
    }

    @Override
    public void updateCartCount(int count) {

    }

    @Override
    public void addToCartBottomSheetClose() {
        isBottomSheetOpen = false;
    }

    @Override
    public void onBackPressed() {
        if (binding.productWebview.canGoBack()) {
            binding.productWebview.goBack();
        } else {
            finish();
        }
    }

    private class AlternateSaltObserver implements Observer<MstarAlgoliaResponse> {

        @Override
        public void onChanged(@Nullable MstarAlgoliaResponse mstarAlgoliaResponse) {
            viewModel.onAlternateDataAvailable(mstarAlgoliaResponse);
            binding.setProductViewModel(viewModel);
        }
    }

    public void onViewAll(View view) {
        Intent intent = new Intent();
        if (viewModel.isProductTypeO()) {
            intent.putExtra(IntentConstant.INTENT_FROM_PEOPLE_ALSO_VIEWED, AppConstant.PEOPLE_ALSO_VIEWED);
            intent.putExtra(IntentConstant.CATEGORY_NAME, getString(R.string.text_people_also_viewed));
            intent.putExtra(IntentConstant.PEOPLE_ALSO_VIEWED_LIST, (Serializable) viewModel.getPeopleAlsoViewedList());
        } else {
            intent.putExtra(IntentConstant.INTENT_FROM_ALTERNATE_SALT, AppConstant.ALTERNATE_SALT);
            intent.putExtra(IntentConstant.ALTERNATE_SALT_FACET_FILTER_QUERY, viewModel.getGenericWithDosage());
            intent.putExtra(IntentConstant.ALTERNATE_SALT_FILTER_QUERY, viewModel.getFilterQuery());
            intent.putExtra(IntentConstant.ALTERNATE_SALT_PRODUCT_CODE, viewModel.getProductCode());
            intent.putExtra(IntentConstant.ALTERNATE_SALT_LIST, (Serializable) viewModel.getAlternateSaltList());
        }

        LaunchIntentManager.routeToActivity(view.getContext().getResources().getString(R.string.route_product_list), intent, view.getContext());
    }

    private void initIntentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE)) {
                productCode = intent.getIntExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, 0);
            }
            if (intent.hasExtra(IntentConstant.PAGE_TITLE_KEY)) {
                pageName = intent.getStringExtra(IntentConstant.PAGE_TITLE_KEY);
            } else {
                onIntentFromDeepLink(intent);
            }
        }
    }

    private void onIntentFromDeepLink(Intent intent) {
        if (intent.hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
            if (intentParamList != null && intentParamList.size() > 0) {
                productCode = Integer.parseInt(intentParamList.get(0));
            }
        }
    }

    public void initProductVariant(List<Variant> productVariantList) {
        binding.lytProductVariant.removeAllViews();
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (Variant variant : productVariantList) {
            View view = layoutInflater.inflate(R.layout.inflate_product_variant, null);
            TextView variantTitle = view.findViewById(R.id.variantTitle);
            variantTitle.setText(variant.getVariantTitle());
            final int position = viewModel.getSelectedVariantPosition(variant);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            final RecyclerView variantListView = view.findViewById(R.id.variantList);
            variantListView.setLayoutManager(linearLayoutManager);
            ProductVariantAdapter productVariantAdapter = new ProductVariantAdapter(this, variant.getVariantFacetList(), this);
            variantListView.setAdapter(productVariantAdapter);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    variantListView.smoothScrollToPosition(position);
                }
            }, 1000);
            binding.lytProductVariant.addView(view);
        }
    }

    @Override
    public void setIsFromVariantFlagChange(boolean isVariantResetFlag) {
        isFromVariant = isVariantResetFlag;
    }
}
