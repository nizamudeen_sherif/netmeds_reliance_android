package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityResetPasswordBinding;
import com.netmedsmarketplace.netmeds.viewmodel.ResetPasswordViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import static com.nms.netmeds.base.IntentConstant.PHONE_NUMBER;

public class ResetPasswordActivity extends BaseViewModelActivity<ResetPasswordViewModel> implements ResetPasswordViewModel.ResetPasswordListener {

    private ActivityResetPasswordBinding activityResetPasswordBinding;
    private String phoneNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityResetPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        activityResetPasswordBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(activityResetPasswordBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected ResetPasswordViewModel onCreateViewModel() {
        ResetPasswordViewModel resetPasswordViewModel = ViewModelProviders.of(this).get(ResetPasswordViewModel.class);
        resetPasswordViewModel.init(activityResetPasswordBinding, this, getIntent(), this);
        phoneNumber = getIntent().getStringExtra(PHONE_NUMBER);
        onRetry(resetPasswordViewModel);
        return resetPasswordViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                ResetPasswordActivity.this.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmCallbackSignInWithPasswordActivity() {
        Intent intent = new Intent(this, SignInWithPasswordActivity.class);
        intent.putExtra(PHONE_NUMBER, phoneNumber);
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        startActivity(intent);
        ResetPasswordActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_RESET_PASSWORD_FORGOT_USER);
    }
}