package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputLayout;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySignupBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SignUpViewModel;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.ValidationUtils;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.Map;

public class SignUpActivity extends BaseViewModelActivity<SignUpViewModel> implements SignUpViewModel.SignUpListener, AppViewModel.CountDownTimerListener {

    private ActivitySignupBinding activitySignupBinding;
    private String phoneNumber;
    private SignUpViewModel signUpViewModel;
    private boolean isNavigateToCart = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySignupBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        activitySignupBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(activitySignupBinding.toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected SignUpViewModel onCreateViewModel() {
        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);
        initIntentValue();
        signUpViewModel.init(this, phoneNumber, getIntent());
        imageOptionButtonClick();
        onRetry(signUpViewModel);
        vmCounterTimer();
        return signUpViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            SignUpActivity.this.finish();
            return true;
        }
        return false;
    }

    @Override
    public void vmCallBackOnNavigationMobileNumberActivity() {
        onBackPressed();
        SignUpActivity.this.finish();
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmCounterTimer() {
        signUpViewModel.onCountDownTimer(this);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        activitySignupBinding.reSendOtp.setEnabled(false);
        activitySignupBinding.reSendOtp.setTextColor(getResources().getColor(R.color.colorLightPink));
        String message = String.format(getString(R.string.text_waiting_for_otp), String.valueOf(millisUntilFinished / 1000));
        activitySignupBinding.waitingOtp.setText(message);
        activitySignupBinding.waitingOtp.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinish() {
        activitySignupBinding.reSendOtp.setEnabled(true);
        activitySignupBinding.reSendOtp.setTextColor(getResources().getColor(R.color.colorMediumPink));
        String message = String.format(getString(R.string.text_waiting_for_otp), String.valueOf(0));
        activitySignupBinding.waitingOtp.setText(message);
        activitySignupBinding.waitingOtp.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_CREATE_ACCOUNT);
    }

    @Override
    public void vmSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(activitySignupBinding.signupViewContent, this, message);
    }

    @Override
    public Context getContext() {
        return SignUpActivity.this;
    }

    @Override
    public void vmNavigateToHome() {
        if (isNavigateToCart)
            launchCart();
        else if (isPrimeNavigation())
            primeNavigation();
        else launchNavigation();
        finishAffinity();
    }

    @Override
    public void vmNoNetworkView(boolean isConnected) {
        activitySignupBinding.signupViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        activitySignupBinding.signupNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void vmWebserviceErrorView(boolean isWebserviceError) {
        activitySignupBinding.signupViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        activitySignupBinding.signupApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    private void initIntentValue() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.hasExtra(IntentConstant.RANDOM_KEY))
                signUpViewModel.setRandomKey(intent.getStringExtra(IntentConstant.RANDOM_KEY));
            if (intent.hasExtra(IntentConstant.PHONE_NUMBER))
                phoneNumber = intent.getStringExtra(IntentConstant.PHONE_NUMBER);
            if (intent.hasExtra(IntentConstant.FROM_CART))
                isNavigateToCart = intent.getBooleanExtra(IntentConstant.FROM_CART, false);
            if (intent.hasExtra(IntentConstant.TRUE_CALLER_FLAG))
                signUpViewModel.setFromTrueCaller(intent.getBooleanExtra(IntentConstant.TRUE_CALLER_FLAG, false));
        }
    }

    private void launchCart() {
        Intent intent = new Intent(this, MStarCartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void launchNavigation() {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean isPrimeNavigation() {
        return getIntent() != null && getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false);
    }

    private void primeNavigation() {
        Intent intent = new Intent(this, PrimeMemberShipActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void showTextErrors(Map<String, String> additional_info) {
        for (String field : additional_info.keySet()) {
            switch (field) {
                case "firstname":
                    activitySignupBinding.inputFirstName.setError(additional_info.get(field));
                    break;
                case "email":
                    activitySignupBinding.inputEmail.setError(additional_info.get(field));
                    break;
                case "lastname":
                    activitySignupBinding.inputLastName.setError(additional_info.get(field));
                    break;
                case "gender":
                    vmSnackBarMessage(additional_info.get(field));
                    break;
            }
        }
    }

    public void onEmailEditTextChanged() {
        activitySignupBinding.inputEmail.setError(null);

    }

    @Override
    public boolean validateForm() {
        boolean isValidate = true;
        if (!checkEmail()) {
            isValidate = false;
        } else if (!checkEditText(activitySignupBinding.firstName, activitySignupBinding.inputFirstName, getString(R.string.text_error_enter_first_name))) {
            isValidate = false;
        } else if (!checkEditText(activitySignupBinding.lastName, activitySignupBinding.inputLastName, getString(R.string.text_error_enter_last_name))) {
            isValidate = false;
        } else if (TextUtils.isEmpty(activitySignupBinding.password.getText().toString())) {
            activitySignupBinding.inputPassword.setError(getString(R.string.text_password_validate));
            isValidate = false;
        } else if (!ValidationUtils.checkLoginSetPassword(activitySignupBinding.confirmPassword, activitySignupBinding.confirmInputPassword, true, getString(R.string.error_password_empty), getString(R.string.error_password_length))) {
            isValidate = false;
        } else if (!ValidationUtils.checkConfirmPassword(activitySignupBinding.password, activitySignupBinding.confirmPassword, activitySignupBinding.confirmInputPassword, getResources().getString(R.string.text_password_mismatch))) {
            isValidate = false;
        } else if (TextUtils.isEmpty(getOtp()) || getOtp().length() < 6) {
            isValidate = false;
            SnackBarHelper.snackBarCallBack(activitySignupBinding.signupViewContent, this, getString(R.string.text_enter_otp));
        }

        return isValidate;
    }

    @Override
    public void resetPinEditText() {
        activitySignupBinding.txtOtp1.setText("");
        activitySignupBinding.txtOtp2.setText("");
        activitySignupBinding.txtOtp3.setText("");
        activitySignupBinding.txtOtp4.setText("");
        activitySignupBinding.txtOtp1.requestFocus();
    }

    private boolean checkEditText(final EditText editText, TextInputLayout inputUsername, final String errorText) {
        int minLength;
        if (editText.getId() == activitySignupBinding.firstName.getId()) {
            minLength = 3;
        } else {
            minLength = 1;
        }
        return ValidationUtils.checkTextInputLayout(editText, true, errorText, getString(R.string.text_name_white_space_error), getString(R.string.text_continuous_whitespace_error), minLength, 30, inputUsername);
    }

    private boolean checkEmail() {
        return ValidationUtils.checkEmailValidation(activitySignupBinding.email, activitySignupBinding.inputEmail, this);
    }

    public String getOtp() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(activitySignupBinding.txtOtp1.getText() != null ? activitySignupBinding.txtOtp1.getText().toString() : "");
        stringBuilder.append(activitySignupBinding.txtOtp2.getText() != null ? activitySignupBinding.txtOtp2.getText().toString() : "");
        stringBuilder.append(activitySignupBinding.txtOtp3.getText() != null ? activitySignupBinding.txtOtp3.getText().toString() : "");
        stringBuilder.append(activitySignupBinding.txtOtp4.getText() != null ? activitySignupBinding.txtOtp4.getText().toString() : "");
        stringBuilder.append(activitySignupBinding.txtOtp5.getText() != null ? activitySignupBinding.txtOtp5.getText().toString() : "");
        stringBuilder.append(activitySignupBinding.txtOtp6.getText() != null ? activitySignupBinding.txtOtp6.getText().toString() : "");
        return String.valueOf(stringBuilder);
    }

    @Override
    public String getFirstName() {
        return activitySignupBinding.firstName.getText() != null ? activitySignupBinding.firstName.getText().toString() : "";
    }

    @Override
    public String getLastName() {
        return activitySignupBinding.lastName.getText() != null ? activitySignupBinding.lastName.getText().toString() : "";
    }

    @Override
    public String getEmail() {
        return activitySignupBinding.email.getText() != null ? activitySignupBinding.email.getText().toString() : "";
    }

    @Override
    public String getPassword() {
        return activitySignupBinding.password.getText() != null ? activitySignupBinding.password.getText().toString() : "";
    }

    @Override
    public void onPinCodeEdited(int position, int count) {
        switch (position) {
            case 1:
                if (count > 0)
                    activitySignupBinding.txtOtp2.requestFocus();
                break;
            case 2:
                if (count > 0)
                    activitySignupBinding.txtOtp3.requestFocus();
                else
                    activitySignupBinding.txtOtp1.requestFocus();
                break;
            case 3:
                if (count > 0)
                    activitySignupBinding.txtOtp4.requestFocus();
                else
                    activitySignupBinding.txtOtp2.requestFocus();
                break;
            case 4:
                if (count > 0)
                    activitySignupBinding.txtOtp5.requestFocus();
                else
                    activitySignupBinding.txtOtp3.requestFocus();
                break;
            case 5:
                if (count > 0)
                    activitySignupBinding.txtOtp6.requestFocus();
                else
                    activitySignupBinding.txtOtp4.requestFocus();
                break;
            case 6:
                if (count <= 0)
                    activitySignupBinding.txtOtp5.requestFocus();
                break;
        }
    }

    @Override
    public void resetError(SignUpViewModel.EDIT_TEXT type) {
        switch (type) {
            case F_NAME:
                activitySignupBinding.inputFirstName.setError(null);
                break;
            case EMAIL:
                activitySignupBinding.inputEmail.setError(null);
                break;
            case PASSWORD:
                activitySignupBinding.inputPassword.setError(null);
                break;
            case CONFIRM_PASSWORD:
                activitySignupBinding.confirmInputPassword.setError(null);
                break;
            case L_NAME:
                activitySignupBinding.inputLastName.setError(null);
                break;
        }
    }

    private void imageOptionButtonClick() {
        activitySignupBinding.txtOtp6.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateForm()) {
                        signUpViewModel.registration();
                    }
                    handled = true;
                }
                return handled;
            }
        });
    }

}
