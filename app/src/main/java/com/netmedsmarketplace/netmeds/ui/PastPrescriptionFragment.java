package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentPastPrescriptionBinding;
import com.netmedsmarketplace.netmeds.viewmodel.PastPrescriptionViewModel;
import com.nms.netmeds.base.BaseDialogFragment;
import com.nms.netmeds.base.adapter.PastPrescriptionAdapter;
import com.nms.netmeds.base.model.GetPastPrescriptionResult;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.utils.MstarPrescriptionHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.ArrayList;
import java.util.List;

public class PastPrescriptionFragment extends BaseDialogFragment implements PastPrescriptionViewModel.PastPrescriptionListener, PastPrescriptionAdapter.UploadedPrescriptionListener {

    private final ArrayList<MStarUploadPrescription> mStarUploadPrescriptionList = new ArrayList<>();
    private final List<String> prescriptionSelected = new ArrayList<>();
    private Context mContext;
    private FragmentPastPrescriptionBinding pastPrescriptionBinding;
    private SelectedPrescriptionListener selectedPrescriptionListener;
    private PastPrescriptionViewModel viewModel;
    private GetPastPrescriptionResult prescriptionList;

    @SuppressLint("ValidFragment")
    public PastPrescriptionFragment(Context mContext, SelectedPrescriptionListener selectedPrescriptionListener, GetPastPrescriptionResult prescriptionList) {
        this.mContext = mContext;
        this.selectedPrescriptionListener = selectedPrescriptionListener;
        this.prescriptionList = prescriptionList;
    }

    //Default Constructor
    public PastPrescriptionFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        pastPrescriptionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_past_prescription, container, false);
        viewModel = ViewModelProviders.of(this).get(PastPrescriptionViewModel.class);
        pastPrescriptionBinding.setViewModel(viewModel);
        viewModel.init(prescriptionList, this, pastPrescriptionBinding);
        return pastPrescriptionBinding.getRoot();
    }

    @Override
    public void setRecentPrescription(List<String> recentPrescriptionList, boolean isDigitized) {
        PastPrescriptionAdapter pastPrescriptionAdapter = new PastPrescriptionAdapter(getMstarUplpadPrescriptionList(recentPrescriptionList, isDigitized), mContext, this, View.VISIBLE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        pastPrescriptionBinding.recentPrescriptionList.setLayoutManager(gridLayoutManager);
        pastPrescriptionBinding.recentPrescriptionList.setAdapter(pastPrescriptionAdapter);
    }

    @Override
    public void setPastPrescriptionAdapter(List<String> digitizedPrescriptionList, boolean isDigitized) {
        PastPrescriptionAdapter pastPrescriptionAdapter = new PastPrescriptionAdapter(getMstarUplpadPrescriptionList(digitizedPrescriptionList, isDigitized), mContext, this, View.VISIBLE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        pastPrescriptionBinding.verifiedPrescriptionList.setLayoutManager(gridLayoutManager);
        pastPrescriptionBinding.verifiedPrescriptionList.setAdapter(pastPrescriptionAdapter);
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @Override
    public List<MStarUploadPrescription> getSelectedPrescriptionList() {
        return mStarUploadPrescriptionList;
    }

    @Override
    public void setSelectedPrescription() {
        selectedPrescriptionListener.selectedPrescription(mStarUploadPrescriptionList);
    }


    private List<MStarUploadPrescription> getMstarUplpadPrescriptionList(List<String> prescriptionIDList, boolean isDigitized) {

        List<String> pastRxIds = new ArrayList<>();
        List<MStarUploadPrescription> prescriptionsList = new ArrayList<>();
        for (String prescriptionId : prescriptionIDList) {
            MStarUploadPrescription prescriptionDetail = new MStarUploadPrescription();
            if (isDigitized) {
                prescriptionDetail.setDigitalizedPrescriptionId(prescriptionId);
                prescriptionDetail.setDigitalized(true);
            }
            //add_uploadedPrescriptionId
            prescriptionDetail.setUploadedPrescriptionId(prescriptionId);
            prescriptionDetail.setPastPrescriptionId(prescriptionId);
            prescriptionsList.add(prescriptionDetail);
        }
        mStarUploadPrescriptionList.addAll(prescriptionsList);
        if (!MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().isEmpty() && MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().size() > 0) {
            for (MStarUploadPrescription prescriptionDetail : MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList()) {
                for (MStarUploadPrescription details : mStarUploadPrescriptionList) {
                    if (prescriptionDetail.getUploadedPrescriptionId() != null && prescriptionDetail.getUploadedPrescriptionId().equalsIgnoreCase(details.getUploadedPrescriptionId())) {
                        pastRxIds.add(prescriptionDetail.getUploadedPrescriptionId());
                    }
                }
            }
            MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(pastRxIds);
        }
        return prescriptionsList;
    }

    @Override
    public void prescriptionSelected(String id) {
        prescriptionSelected.add(id);
        MstarPrescriptionHelper.getInstance().setSelectedPastRxIds(prescriptionSelected);
        if (mStarUploadPrescriptionList != null && id != null && mStarUploadPrescriptionList.size() > 0) {
            for (MStarUploadPrescription list : mStarUploadPrescriptionList) {
                if (list.getUploadedPrescriptionId().equalsIgnoreCase(id)) {
                    list.setChecked(true);
                }
            }
        }
        viewModel.enableContinue(prescriptionSelected.size() > 0);
    }

    @Override
    public void removePrescription(String id) {
        prescriptionSelected.remove(id);
        if (mStarUploadPrescriptionList != null && id != null) {
            for (MStarUploadPrescription list : mStarUploadPrescriptionList) {
                if (list.getUploadedPrescriptionId().equalsIgnoreCase(id)) {
                    list.setChecked(false);
                }
            }
        }
        viewModel.enableContinue(prescriptionSelected.size() > 0);
    }

    @Override
    public void launchPreviewPrescription(String url) {
        selectedPrescriptionListener.launchPreviewPrescription(url);
    }

    @Override
    public void setAlert() {
        SnackBarHelper.snackBarCallBack(pastPrescriptionBinding.mainLayout, getActivity(), getResources().getString(R.string.text_maximum_prescription_uploaded));
    }

    @Override
    public int getPrescriptionCount() {
        if (!MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().isEmpty() && MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().size() > 0) {
            return prescriptionSelected.size() + MstarPrescriptionHelper.getInstance().getMStarUploadPrescriptionList().size();
        } else {
            return prescriptionSelected.size();
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    dismissDialog();
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    interface SelectedPrescriptionListener {

        void selectedPrescription(List<MStarUploadPrescription> prescriptionLists);

        void launchPreviewPrescription(Object previewImage);
    }
}