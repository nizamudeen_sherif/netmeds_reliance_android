package com.netmedsmarketplace.netmeds.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.OrderConfirmationPrescriptionAdapter;
import com.netmedsmarketplace.netmeds.adpater.ViewOrderPaymentDetailsAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityViewOrderDetailsBinding;
import com.netmedsmarketplace.netmeds.model.BoomrangResponse;
import com.netmedsmarketplace.netmeds.model.ViewOrderPaymentDetailsModel;
import com.netmedsmarketplace.netmeds.viewmodel.ViewOrderDetailsViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.adapter.OrderInsideItemListAdapter;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MStarAddedProductsResult;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarOrderDetail;
import com.nms.netmeds.base.model.MstarOrderDetailsResult;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.view.ImagePreviewBottomSheetDialog;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.payment.ui.OrderPlacedSuccessfullyActivity;
import com.nms.netmeds.payment.ui.PaymentActivity;
import com.nms.netmeds.payment.ui.PaymentIntentConstant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.nms.netmeds.base.BaseUploadPrescription.PACKAGE;


public class ViewOrderDetailsActivity extends BaseViewModelActivity<ViewOrderDetailsViewModel> implements ViewOrderDetailsViewModel.ViewOrderDetailsListener,
        CancelOrderFragmentDialog.CancelOrderDialogListener, OrderReviewDialog.OrderReviewDialogListener, SnackBarHelper.SnackBarHelperListener, CODWarningDialog.CODWarningDialogListener, OrderConfirmationPrescriptionAdapter.PrescriptionAdapterCallback, OrderInsideItemListAdapter.OrderItemListCallBack, SubscriptionPopUpDialog.SubscriptionPopupListener {

    private ViewOrderDetailsViewModel viewModel;
    private ActivityViewOrderDetailsBinding binding;

    private String orderId = "";
    private boolean isPayNowInititatedFromDeeplinking = false;
    private boolean isFromDeeplinking = false;
    private String userEmailId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_order_details);
        getOrderId();
        onCreateViewModel();
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, "Order Details");
        SubscriptionHelper.setHelper(new SubscriptionHelper());
    }

    @Override
    protected ViewOrderDetailsViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(ViewOrderDetailsViewModel.class);
        viewModel.setOrderId(orderId);
        viewModel.initiatePaymentFromDeeplinking(isPayNowInititatedFromDeeplinking);
        viewModel.setFromDeepinking(isFromDeeplinking);
        viewModel.init(BasePreference.getInstance(this), this);
        viewModel.getViewOrderMutableData().observe(this, new ViewOrderDetailsObserver());
        viewModel.getMutableBoomDateLiveData().observe(this, new BoomDetailsObserver());
        onRetry(viewModel);
        return viewModel;
    }

    @Override
    public void showLoader() {
        showHorizontalProgressBar(false, binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void dismissLoader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showHorizontalProgressBar(true, binding.toolBarCustomLayout.progressBar);
            }
        });
    }

    public boolean isLoaderinProgress() {
        return getProgressDialog() != null && getProgressDialog().isShowing();
    }

    @Override
    public void onCallbackOfShowDetails(String orderId, int position) {
        Intent intent = new Intent(this, TrackOrderDetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentConstant.ORDER_ID, orderId);
        intent.putExtra(IntentConstant.TRACK_ORDER_POSITION, position);
        startActivity(intent);
    }

    @Override
    public void onCallbackOfCancelOrder(String orderId) {
        setButtonDisableAndEnableProperties(false);
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(ViewOrderDetailsActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_CANCEL_ORDER, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER_DETAILS);
        CancelOrderFragmentDialog cancelOrderFragmentDialog = new CancelOrderFragmentDialog(this);
        getSupportFragmentManager().beginTransaction().add(cancelOrderFragmentDialog, IntentConstant.REVIEW_ORDER_AGAIN_DIALOG_FRAGMENT_TAG_NAME).commitNowAllowingStateLoss();
    }

    @Override
    public void onCancelInitiated(String cancelMessage, int cancelMessageIndex) {
        viewModel.cancelOrder(cancelMessage, cancelMessageIndex);
    }

    @Override
    public void dismissDialog() {
        setButtonDisableAndEnableProperties(true);
    }

    @Override
    public void onCallbackOfNeedHelpForOrder(String orderId) {
        setButtonDisableAndEnableProperties(false);
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(ViewOrderDetailsActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_NEED_HELP, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER_DETAILS);
        Intent intent = new Intent(this, NeedHelpActivity.class);
        startActivity(intent);
    }

    private void setButtonDisableAndEnableProperties(boolean isEnable) {
        binding.btnCancelOrder.setEnabled(isEnable);
        binding.btnCancelOrder.setClickable(isEnable);
        if (!viewModel.isPaymentPendingInProgress()) {
            binding.btnNeedHelp.setEnabled(isEnable);
            binding.btnNeedHelp.setClickable(isEnable);
        }
        binding.btnNeedHelpForOther.setEnabled(isEnable);
        binding.btnNeedHelpForOther.setClickable(isEnable);
    }

    @Override
    public void showSnackBar(String message) {
        setButtonDisableAndEnableProperties(true);
        Snackbar snackbar = Snackbar.make(binding.bodyView, CommonUtils.fromHtml(message), 1000 * 10);
        View snackBarView = snackbar.getView(); //get your snackbar view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        TextView textView = snackBarView.findViewById(R.id.snackbar_text); //Get reference of snackbar textview
        textView.setMaxLines(5);
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void navigateToM2Flow() {
        startActivity(new Intent(this, M2AttachPrescriptionActivity.class));
    }

    @Override
    public void navigateToCartActivity(Map<String, MStarAddedProductsResult> addedProductsResultMap, boolean isEditOrder, String orderId) {
        M2Helper.getInstance().clearData();
        Intent intent = new Intent(this, MStarCartActivity.class);
        if (addedProductsResultMap != null && !addedProductsResultMap.isEmpty()) {
            RefillHelper.getOutOfStockProductsMap().putAll(addedProductsResultMap);
        }
        PaymentHelper.setIsEditOrder(isEditOrder);
        intent.putExtra(IntentConstant.ORDER_ID, orderId);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.need_help_menu, menu);
        MenuItem item = menu.getItem(0);
        if (viewModel.isEnablePayment()) {
            item.setVisible(true);
            SpannableString s = new SpannableString(getString(R.string.text_need_help_menu));
            s.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.colorMediumPink)), 0, s.length(), 0);
            item.setTitle(s);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.need_help:
                onCallbackOfNeedHelpForOrder(viewModel.getOrderId());
                return true;
            case android.R.id.home:
                ViewOrderDetailsActivity.this.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_ORDER_DETAILS);
        PaymentHelper.setItemInsideOrderMap(null);
        PaymentHelper.setIsIncompleteOrder(false);
        PaymentHelper.setInCompleteOrderId("");
        PaymentHelper.setIsEditOrder(false);
        PaymentHelper.setIsInitiateCheckoutCalled(false);
        BasePreference.getInstance(this).setOutOfStockProductMessage("");
        SubscriptionHelper.getInstance().setEditorderquantityflag(false);
        SubscriptionHelper.getInstance().setPayNowSubscription(false);
        PaymentHelper.setIsNMSCashApplied(false);
        PaymentHelper.setAppliedNMSCash(0);
        setButtonDisableAndEnableProperties(true);
    }

    @Override
    public void submitReview(String feedback, int ratingCount) {
    }

    @Override
    public void cancelRating() {
    }

    @Override
    public void snackBarOnClick() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts(PACKAGE, getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void previewPrescription(String rxUrl) {
        ImagePreviewBottomSheetDialog imagePreviewBottomSheetDialog = new ImagePreviewBottomSheetDialog(rxUrl);
        getSupportFragmentManager().beginTransaction().add(imagePreviewBottomSheetDialog, "ImagePreviewDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void onItemClickedCallBack(int productCode, boolean isPrimeProduct) {
        if (isPrimeProduct) {
            Intent intent = new Intent(this, PrimeMemberShipActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ProductDetail.class);
            intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
            startActivity(intent);
        }
    }

    @Override
    public void subscriptionAdd() {
        onCallbackOfCancelOrder(orderId);

    }

    @Override
    public void SubscriptionNo() {
        setButtonDisableAndEnableProperties(true);
    }

    @Override
    public void onDismissDialog(String from) {
        setButtonDisableAndEnableProperties(true);
    }

    @Override
    public void subscriptionDialogue(String orderId) {
        this.orderId = orderId;
        SubscriptionPopUpDialog subscriptionPopUpDialog = new SubscriptionPopUpDialog(this, true);
        getSupportFragmentManager().beginTransaction().add(subscriptionPopUpDialog, "SubscriptionPopUpDialog").commitNowAllowingStateLoss();
    }

    private class ViewOrderDetailsObserver implements Observer<MstarOrderDetailsResult> {
        @Override
        public void onChanged(@Nullable MstarOrderDetailsResult responseObject) {
            viewModel.loadOrderDetailsInPage(responseObject);
            binding.setViewModel(viewModel);
            invalidateOptionsMenu();
        }
    }

    private class BoomDetailsObserver implements Observer<BoomrangResponse> {
        @Override
        public void onChanged(@Nullable BoomrangResponse responseObject) {
            viewModel.boomRangResponse(responseObject);
            binding.setViewModel(viewModel);
        }
    }

    private void getOrderId() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            checkDeepLink(intent);
        }
    }

    private void checkDeepLink(Intent intent) {
        if (intent.hasExtra(IntentConstant.ORDER_ID))
            orderId = intent.getStringExtra(IntentConstant.ORDER_ID);
        else
            onIntent(intent);
    }

    private void onIntent(Intent intent) {
        if (intent.hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
            isFromDeeplinking = true;
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
            orderId = intentParamList != null && intentParamList.size() > 0 ? intentParamList.get(0) : "";
            String flag = intentParamList != null && intentParamList.size() > 1 ? intentParamList.get(1) : "0";
            isPayNowInititatedFromDeeplinking = flag.equals("1");
        }
    }

    @Override
    public void setError(String error) {
        dismissLoader();
        SnackBarHelper.snackBarCallBack(binding.parentLayout, this, error);
    }

    @Override
    public void showCashOnDeliveryWarningDialog() {
        setButtonDisableAndEnableProperties(false);
        CODWarningDialog codWarningDialog = new CODWarningDialog();
        codWarningDialog.setCODWarningDialog(this);
        getSupportFragmentManager().beginTransaction().add(codWarningDialog, "m2UpdatedWarningDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void onEmptyOrderCallBackFromDeeplinking() {
        Intent intent = new Intent();
        intent.putExtra(PaymentIntentConstant.HOME_FLAG_FROM_ORDER_PLACED_SUCCESSFUL, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        LaunchIntentManager.routeToActivity(getString(com.nms.netmeds.m3subscription.R.string.route_navigation_activity), intent, this);
    }

    @Override
    public void navigateToPayment() {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_PAY_NOW, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER_DETAILS);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PaymentIntentConstant.IS_FROM_PAYMENT, true);
        intent.putExtra(PaymentIntentConstant.FROM_PAYMENT_FAILURE, false);
        intent.putExtra(IntentConstant.IS_M2_ORDER, M2Helper.getInstance().isM2Order());
        intent.putExtra(IntentConstant.IS_MSTAR_TEMP_ORDER, PaymentHelper.isIsTempCart());
        startActivity(intent);
    }

    @Override
    public String getStringValue(int stringId) {
        return getString(stringId);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }


    @Override
    public void showNoNetworkView(boolean isConnected) {
        binding.bodyView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.emptyOrderDetailsView.setVisibility(View.GONE);
        binding.bottomLayout.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.viewOrderDetailsNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.emptyOrderDetailsView.setVisibility(View.GONE);
        binding.bodyView.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.bottomLayout.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        binding.viewOrderDetailsApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public LinearLayout getContainer() {
        return binding.container;
    }

    @Override
    public void enableShimmer(boolean isEnable) {
        binding.bodyView.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        binding.bottomLayout.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        binding.shimmerView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        if (isEnable) {
            binding.shimmerView.startShimmerAnimation();
        } else {
            binding.shimmerView.stopShimmerAnimation();
        }
    }

    @Override
    public void setAdapterForOrderList(RecyclerView rvOrderList, MstarOrderDetail orderDetail, Map<String, MStarProductDetails> productCodeWithPriceMap, boolean isM2Order, boolean paymentPending) {
        OrderInsideItemListAdapter itemListAdapter = new OrderInsideItemListAdapter(this, orderDetail.getDrugDetails(), productCodeWithPriceMap, isM2Order, viewModel.isOrderDelivered(), paymentPending, this);
        rvOrderList.setLayoutManager(new LinearLayoutManager(this));
        rvOrderList.setNestedScrollingEnabled(false);
        rvOrderList.setAdapter(itemListAdapter);
    }

    @Override
    public void showEmptyView(boolean isEmpty) {
        binding.shimmerView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        binding.bodyView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        binding.emptyOrderDetailsView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        binding.bottomLayout.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
    }

    @Override
    public void loadOrderPaymentDetails(MstarOrderDetailsResult singleOrderResponse) {
        if (singleOrderResponse != null && viewModel.isNegativeOrderStatus()) {
            if (singleOrderResponse.getOrderdetail() != null && !singleOrderResponse.getOrderdetail().isEmpty() && singleOrderResponse.getOrderdetail().get(0).getDrugDetails() != null && !singleOrderResponse.getOrderdetail().get(0).getDrugDetails().isEmpty()) {
                getPaymentDetailsAndLoad();
            } else
                binding.cvPaymentDetails.setVisibility(View.GONE);
        } else
            getPaymentDetailsAndLoad();
    }

    private void getPaymentDetailsAndLoad() {
        binding.cvPaymentDetails.setVisibility(View.VISIBLE);
        List<ViewOrderPaymentDetailsModel> paymentDetailsList = viewModel.getPaymentDetails();
        ViewOrderPaymentDetailsAdapter detailsAdapter = new ViewOrderPaymentDetailsAdapter(paymentDetailsList);
        binding.rvPaymentDetails.setAdapter(detailsAdapter);
        binding.rvPaymentDetails.setLayoutManager(new LinearLayoutManager(this));
        binding.rvPaymentDetails.setNestedScrollingEnabled(false);
    }

    @Override
    public void loadCancellationAndRefundOrderDetails(MstarOrderDetailsResult singleOrderResponse, boolean paymentPending) {
        if (singleOrderResponse != null && singleOrderResponse.getCanceledItemsDetails() != null && !singleOrderResponse.getCanceledItemsDetails().isEmpty()) {
            binding.cvCancellationOrderDetails.setVisibility(View.VISIBLE);
            OrderInsideItemListAdapter itemListAdapter = new OrderInsideItemListAdapter(this, singleOrderResponse.getCanceledItemsDetails(), null,
                    true, viewModel.isOrderDelivered(), false, this);
            binding.rvCancellationRefundOrderDetails.setLayoutManager(new LinearLayoutManager(this));
            binding.rvCancellationRefundOrderDetails.setNestedScrollingEnabled(false);
            binding.rvCancellationRefundOrderDetails.setAdapter(itemListAdapter);
        } else {
            binding.cvCancellationOrderDetails.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadPrescription(MstarOrderDetailsResult singleOrderResponse) {
        if (singleOrderResponse != null && singleOrderResponse.getRxId() != null && singleOrderResponse.getRxId().size() != 0) {
            OrderConfirmationPrescriptionAdapter pastPrescriptionAdapter = new OrderConfirmationPrescriptionAdapter(singleOrderResponse.getRxId(), this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            binding.rvPrescription.setLayoutManager(linearLayoutManager);
            binding.rvPrescription.setAdapter(pastPrescriptionAdapter);
        }
    }

    @Override
    public void navigateToAddress() {
        startActivity(new Intent(this, AddressActivity.class));
    }

    @Override
    public int getColors(int colorId) {
        return ContextCompat.getColor(this, colorId);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void cancelOrderSuccess(String message) {
        setButtonDisableAndEnableProperties(true);
        setResult(IntentConstant.CANCEL_ORDER_RESULT_OK, new Intent());
        finish();
    }

    @Override
    public void navigateToSuccessPageAfterCOD(String data) {
        Intent intent = new Intent(this, OrderPlacedSuccessfullyActivity.class);
        intent.putExtra(PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_FLAG, PaymentIntentConstant.PLACED_SUCCESSFUL_FROM_M1);
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ADDRESS, new Gson().toJson(PaymentHelper.getCustomerAddress()));
        intent.putExtra(SubscriptionIntentConstant.ORDER_ID, viewModel.getOrderId());
        intent.putExtra(PaymentIntentConstant.COMPLETE_ORDER_RESPONSE, data);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToCodFailurePage(BigDecimal totalAmount, Integer cartId, String orderId, boolean isPrimeProduct) {
        setButtonDisableAndEnableProperties(true);
        Toast.makeText(this, "COD failed, Try again", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onTrackOrderStatus(String orderId) {
        Intent intent = new Intent(this, TrackOrderDetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentConstant.ORDER_ID, orderId);
        startActivityForResult(intent, IntentConstant.CANCEL_ORDER_RQUEST_CODE);
    }

    @Override
    public void downLoadInvoice(String userEmailId, String orderId) {
        setButtonDisableAndEnableProperties(false);
        new ViewOrderDetailsViewModel.DownloadInvoice(ConfigMap.getInstance().getProperty(ConfigConstant.INVOICE_DOWNLOAD_BASE_URL) + AppConstant.ORDER_ID_QUERY + orderId + AppConstant.LOGIN_NAME_QUERY + userEmailId, orderId, this).execute();
    }

    @Override
    public void navigateToCartForColdStorage(ArrayList<String> interCityProductList) {
        Intent intent = new Intent(this, MStarCartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putStringArrayListExtra(IntentConstant.INTER_CITY_PRODUCT_LIST, interCityProductList);
        intent.putExtra(IntentConstant.IS_OUT_OF_STOCK, false);
        startActivity(intent);
    }

    @Override
    public boolean isStoragePermissionAllowed() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 190);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 190) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                SnackBarHelper.snackBarCallBack(binding.bodyView, this, getString(R.string.text_gallery_permission_alert));
            } else {
                viewModel.downloadInvoice();
            }
        }
    }

    @Override
    public void initiateCashOnDelivery() {
        /*Google Analytics Event*/
        GoogleAnalyticsHelper.getInstance().postEvent(ViewOrderDetailsActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_COD, GoogleAnalyticsHelper.EVENT_LABEL_MY_ORDER_DETAILS);
        viewModel.initCOD();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstant.CANCEL_ORDER_RQUEST_CODE) {
            if (resultCode == IntentConstant.CANCEL_ORDER_RESULT_OK) {
                cancelOrderSuccess(null);
            }
        }
    }
}
