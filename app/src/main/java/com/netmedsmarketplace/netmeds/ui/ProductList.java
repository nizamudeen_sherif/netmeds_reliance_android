package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.AppUriSchemeHandler;
import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.MstarCategoryProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.OfferAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityProductBinding;
import com.netmedsmarketplace.netmeds.fragment.FilterFragment;
import com.netmedsmarketplace.netmeds.fragment.SortFragment;
import com.netmedsmarketplace.netmeds.viewmodel.ProductListViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarCategoryDetails;
import com.nms.netmeds.base.model.MstarManufacturerDetails;
import com.nms.netmeds.base.model.SortOptionLabel;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;

public class ProductList extends BaseViewModelActivity<ProductListViewModel> implements MstarCategoryProductAdapter.AlternateProductAdapterListener, AddCartBottomSheetDialog.BottomSheetDialogListener,
        ProductListViewModel.ProductListListener, FilterFragment.FilterInterface, SortFragment.ApplySortListener, OfferAdapter.BannerOfferInterface {

    private ActivityProductBinding productBinding;
    private ProductListViewModel productListViewModel;
    private MStarProductDetails productDetails;
    private AddCartBottomSheetDialog.BottomSheetDialogListener bottomSheetDialogListener;
    private boolean isAddToCartBottomSheetOpen = false;
    private int mstarCategoryId = 0;
    private String mTitle = "";
    private String deepLinkIntentFrom = "";
    private String productType = "";
    private int contentId;
    private String contentUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productBinding = DataBindingUtil.setContentView(this, R.layout.activity_product);
        toolBarSetUp(productBinding.toolbar);
        initToolBar();
        productBinding.setViewModel(onCreateViewModel());
        MstarFilterHelper.clearSelection();
        MstarFilterHelper.clearSortSelection();
        bottomSheetDialogListener = this;
    }

    @Override
    protected ProductListViewModel onCreateViewModel() {
        productListViewModel = ViewModelProviders.of(this).get(ProductListViewModel.class);
        productListViewModel.init(this, BasePreference.getInstance(this));
        productListViewModel.setSortOptionList(sortOptionList());
        productListViewModel.setSortOption(getPopularProductIndex());
        productListViewModel.mstarCategoryProductAdapter = null;
        getList();
        onRetry(productListViewModel);
        productListViewModel.getMstarCategoryDetailsMutableLiveData().observe(this, new MstartCategoryListObserver());
        productListViewModel.getManufacturerDetailsMutableLiveData().observe(this, new ManufacturerDetailsObserver());
        productListViewModel.getmStarProductDetailsMutableLiveData().observe(this, new MstarProductDetailObserver());
        productListViewModel.getAlgoliaMutableLiveData().observe(this, new MstarAlgoliaFilterOptionObserver());
        return productListViewModel;
    }

    private void getList() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            int mstarManufacturerId;
            int mstarBrandId;

            if (getIntent().hasExtra(IntentConstant.CATEGORY_ID)) {
                mstarCategoryId = getIntent().getIntExtra(IntentConstant.CATEGORY_ID, 0);
                if (getIntent().hasExtra(IntentConstant.CATEGORY_NAME)) {
                    mTitle = getIntent().getStringExtra(IntentConstant.CATEGORY_NAME);
                }
                productListViewModel.setIntentFrom(AppConstant.CATEGORY);
                productListViewModel.getProductList(mstarCategoryId);
            } else if (getIntent().hasExtra(IntentConstant.MANUFACTURER_ID)) {
                mstarManufacturerId = getIntent().getIntExtra(IntentConstant.MANUFACTURER_ID, 0);
                if (getIntent().hasExtra(IntentConstant.MANUFACTURER_NAME)) {
                    mTitle = getIntent().getStringExtra(IntentConstant.MANUFACTURER_NAME);
                }
                if (getIntent().hasExtra(IntentConstant.PRODUCT_TYPE)) {
                    productType = getIntent().getStringExtra(IntentConstant.PRODUCT_TYPE);
                }
                getManufacturerProductList(mstarManufacturerId);
            } else if (getIntent().hasExtra(IntentConstant.BRAND_ID)) {
                mstarBrandId = getIntent().getIntExtra(IntentConstant.BRAND_ID, 0);
                mTitle = getIntent().getStringExtra(IntentConstant.BRAND_NAME);
                getBrandProductList(mstarBrandId);
            } else if (getIntent().hasExtra(IntentConstant.INTENT_FROM_ALTERNATE_SALT)) {
                productListViewModel.setIntentFrom(getIntent().getStringExtra(IntentConstant.INTENT_FROM_ALTERNATE_SALT));
                mTitle = ProductList.this.getResources().getString(R.string.text_alternate_salt);
                productListViewModel.onAlternateSaltDataAvailable(getIntent());
            } else if (getIntent().hasExtra(IntentConstant.INTENT_FROM_PEOPLE_ALSO_VIEWED)) {
                productListViewModel.setIntentFrom(getIntent().getStringExtra(IntentConstant.INTENT_FROM_PEOPLE_ALSO_VIEWED));
                mTitle = ProductList.this.getResources().getString(R.string.text_people_also_viewed);
                productListViewModel.onPeopleAlsoViewedDataAvailable(getIntent());
            } else if (getIntent().hasExtra(IntentConstant.INTENT_FROM_BRAND_URL)) {
                productListViewModel.setIntentFrom(getIntent().getStringExtra(IntentConstant.INTENT_FROM_BRAND_URL));
                productListViewModel.getProductsFromBrandURL(getIntent().getStringExtra(IntentConstant.BRAND_URL).contains(AppConstant.BRANDS_URL) ? getIntent().getStringExtra(IntentConstant.BRAND_URL) : AppConstant.BRANDS_URL + getIntent().getStringExtra(IntentConstant.BRAND_URL));
                mTitle = getIntent().hasExtra(IntentConstant.BRAND_NAME) ? getIntent().getStringExtra(IntentConstant.BRAND_NAME) : getIntent().hasExtra(IntentConstant.BRAND_URL) ? getIntent().getStringExtra(IntentConstant.BRAND_URL).replace(AppConstant.BRANDS_URL, "") : "";
            } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
                onIntentFromDeepLink(getIntent());
            }
            //setTitle(mTitle);
        }
    }

    private void getBrandProductList(int mstarBrandID) {
        productListViewModel.setBrandId(mstarBrandID);
        productListViewModel.setIntentFrom(AppConstant.BRAND);
        productListViewModel.setClearList(true);
        productListViewModel.initMstarAPIcall(APIServiceManager.MSTAR_BRAND_PRODUCT_LIST);

    }

    private void initToolBar() {
        productBinding.title.setText(!TextUtils.isEmpty(getIntent().getStringExtra(IntentConstant.CATEGORY_NAME)) ? getIntent().getStringExtra(IntentConstant.CATEGORY_NAME) : "");
        productBinding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        productBinding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        productBinding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        productBinding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));

        /*Google Analytics Post Screen*/
        if (getIntent() != null)
            GoogleAnalyticsHelper.getInstance().postScreen(this, getIntent().getStringExtra(IntentConstant.CATEGORY_NAME));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
        else
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                navigateToCart();
                return true;
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            default:
                return false;
        }
    }

    private void navigateToCart() {
        startActivity(new Intent(this, MStarCartActivity.class));
    }


    @Override
    public void OnAddCartCallBack(MStarProductDetails product, int position) {
        this.productDetails = product;
        productListViewModel.getProductDetailRequest(product.getProductCode());

        /*Google Tag Manager + FireBase Product Click Event*/
        onGoogleTagManagerEvent(product, position);
    }


    @Override
    public void openManufacturerProductList(Integer manufacturerId, String manufacturerName) {
        Intent intent = new Intent(this, ProductList.class);
        intent.putExtra(IntentConstant.MANUFACTURER_ID, manufacturerId);
        intent.putExtra(IntentConstant.MANUFACTURER_NAME, manufacturerName);
        startActivity(intent);
    }

    @Override
    public void openProductDetailsPage(int productCode, MStarProductDetails productDetails, int position) {
        /*Google Tag Manager + FireBase Product Click Event*/
        onGoogleTagManagerEvent(productDetails, position);

        Intent intent = new Intent(this, ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
        startActivity(intent);
    }

    @Override
    public boolean isFilterApplied() {
        return productListViewModel.isFromAlgolia;
    }

    private void getManufacturerProductList(Integer manufacturerId) {
        productListViewModel.setManufacturerId(manufacturerId);
        productListViewModel.setIntentFrom(AppConstant.MANUFACTURER);
        productListViewModel.setClearList(true);
        productListViewModel.getManufactureList(manufacturerId, productType);
    }

    @Override
    public void bsAddCartCallBack() {
        CommonUtils.initiateVibrate(this);
        Snackbar snackbar = Snackbar.make(productBinding.productView, getString(R.string.text_added_to_cart), Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.text_view_cart), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void showAlert(String message) {
        SnackBarHelper.snackBarCallBack(productBinding.productView, this, message);
    }

    @Override
    public void updateCartCount(int count) {

    }

    @Override
    public void addToCartBottomSheetClose() {
        isAddToCartBottomSheetOpen = false;
    }

    @Override
    public void vmShowProgress() {
        productBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void vmDismissProgress() {
        productBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        getList();
    }

    @Override
    public void showEmptyView(boolean isEmpty) {
        productBinding.searchList.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        productBinding.emptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setTitle(String title) {
        productBinding.title.setVisibility(!TextUtils.isEmpty(title) ? View.VISIBLE : View.GONE);
        //productBinding.title.setText(!TextUtils.isEmpty(title) ? title : "");
        productBinding.title.setText(!TextUtils.isEmpty(getIntent().getStringExtra(IntentConstant.CATEGORY_NAME)) ? getIntent().getStringExtra(IntentConstant.CATEGORY_NAME) : !TextUtils.isEmpty(mTitle) ? mTitle : !TextUtils.isEmpty(title) ? title : "");
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(ProductList.this, !TextUtils.isEmpty(getIntent().getStringExtra(IntentConstant.CATEGORY_NAME)) ? getIntent().getStringExtra(IntentConstant.CATEGORY_NAME) : !TextUtils.isEmpty(mTitle) ? mTitle : !TextUtils.isEmpty(title) ? title : "");
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        productBinding.productView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        productBinding.productNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceError(boolean isWebError) {
        productBinding.productView.setVisibility(isWebError ? View.GONE : View.VISIBLE);
        productBinding.productApiErrorView.setVisibility(isWebError ? View.VISIBLE : View.GONE);
    }

    @Override
    public MstarCategoryProductAdapter setAdapter(List<MStarProductDetails> mStarProductDetailsList, List<String> defaultBannerList, List<MstarBanner> mstarBannersList, boolean isPeopleAlsoView) {
        MstarCategoryProductAdapter mstarCategoryProductAdapter = new MstarCategoryProductAdapter(this, mStarProductDetailsList, this, true, productListViewModel.getIntentFrom(), null, mstarBannersList, isPeopleAlsoView);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        productBinding.searchList.setLayoutManager(linearLayoutManager);
        productBinding.searchList.setAdapter(mstarCategoryProductAdapter);
        productBinding.searchList.addOnScrollListener(productListViewModel.getProductListPagenation(linearLayoutManager));
        return mstarCategoryProductAdapter;
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        LaunchIntentManager.routeToActivity(this.getString(R.string.route_navigation_activity), intent, this);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public Context getContext() {
        return ProductList.this;
    }

    @Override
    public void applyFilter() {
        productListViewModel.onApplyFilterDataAvailable();
    }

    @Override
    public void clearFilter() {
        productListViewModel.setFilterQuery("");
        resetFilterValue();
    }

    @Override
    public void showError(String errorMessage) {
        showAlert(errorMessage);
    }

    @Override
    public void applySort(SortOptionLabel sortOption) {
        productListViewModel.onSortFilterApplied(sortOption);
    }

    @Override
    public void clearSort() {
        productListViewModel.setSortOption(getPopularProductIndex());
        resetFilterValue();
    }

    @Override
    public void imageClickListener(String bannerFrom, MstarBanner mstarBanner) {
        googleEvent(bannerFrom);
        CommonUtils.wellnessNavigateToScreens(this, mstarBanner, this);
    }

    @Override
    public String getPageTitle() {
        return mTitle;
    }

    @Override
    public String getTitleText() {
        return mTitle;
    }

    private void googleEvent(String bannerFrom) {
        switch (bannerFrom) {
            case GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS:
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_BANNER, GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
            case GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER:
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_WELLNESS_SECTION, GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
        }
    }


    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductList.this, MStarCartActivity.class);
            startActivity(intent);
        }
    }


    public class ManufacturerDetailsObserver implements Observer<MstarManufacturerDetails> {

        @Override
        public void onChanged(@Nullable MstarManufacturerDetails mstarManufacturerDetails) {
            if (mstarManufacturerDetails != null) {
                productListViewModel.onManufacturerDataAvailable(mstarManufacturerDetails);
            }
        }
    }


    private class MstartCategoryListObserver implements Observer<MstarCategoryDetails> {


        @Override
        public void onChanged(@Nullable MstarCategoryDetails mstarCategoryDetails) {
            if (mstarCategoryDetails != null) {
                productListViewModel.onProductListDataAvailable(mstarCategoryDetails);
            }
        }
    }

    private class MstarProductDetailObserver implements Observer<MStarProductDetails> {

        @Override
        public void onChanged(@Nullable MStarProductDetails productDetails) {
            if (productDetails != null && !isAddToCartBottomSheetOpen) {
                isAddToCartBottomSheetOpen = true;
                AddCartBottomSheetDialog couponFragment = new AddCartBottomSheetDialog(productDetails, false, bottomSheetDialogListener);
                getSupportFragmentManager().beginTransaction().add(couponFragment, "AddCartBottomSheetDialog").commitNowAllowingStateLoss();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_Product_List_Page);
    }

    public void onFilterNavigation(View view) {
        if (productListViewModel.getAlgoliaFilterOption() != null) {
            FilterFragment filterFragment = new FilterFragment(productListViewModel.getAlgoliaFilterOption(), this, productListViewModel.getIntentFrom());
            getSupportFragmentManager().beginTransaction().add(filterFragment, "filter_fragment").commitNowAllowingStateLoss();
        }
    }

    private class MstarAlgoliaFilterOptionObserver implements Observer<MstarAlgoliaResponse> {

        @Override
        public void onChanged(@Nullable MstarAlgoliaResponse mstarAlgoliaResponse) {
            productListViewModel.onAlgoliaFilterDataAvailable(mstarAlgoliaResponse);
            productBinding.setViewModel(productListViewModel);
        }
    }

    public void onSortingNavigation(View view) {
        if (productListViewModel.getSortOptionList() != null && productListViewModel.getSortOptionList().size() > 0) {
            SortFragment sortFragment = new SortFragment(ProductList.this, productListViewModel.getSortOptionList(), this);
            getSupportFragmentManager().beginTransaction().add(sortFragment, "sort_fragment").commitNowAllowingStateLoss();
        }
    }

    /*Reset filter and sort values while click clear*/
    private void resetFilterValue() {
        productListViewModel.setClearList(true);
        productListViewModel.isFromAlgolia = true;
        productListViewModel.fetchFacetValueFromAlgolia();
    }

    private void onIntentFromDeepLink(Intent intent) {
        ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
        if (intentParamList != null && intentParamList.size() > 0) {
            for (int i = 0; i < intentParamList.size(); i++) {
                setDeepLinkValues(i, intentParamList.get(i));
            }
            mTitle = TextUtils.isEmpty(mTitle) && !TextUtils.isEmpty(contentUrl) ? contentUrl : "";
            loadData();
        }
    }

    private void setDeepLinkValues(int position, String value) {
        switch (position) {
            case 0:
                if (TextUtils.isDigitsOnly(value))
                    contentId = Integer.parseInt(value);
                else
                    contentUrl = value;
                break;
            case 1:
                deepLinkIntentFrom = value;
                break;
            case 2:
                mTitle = value;
                break;
        }
    }

    private void loadData() {
        switch (deepLinkIntentFrom) {
            case AppUriSchemeHandler.MANUFACTURER:
                getManufacturerProductList(contentId);
                break;
            case AppUriSchemeHandler.OTC:
            case AppUriSchemeHandler.PRESCRIPTION:
                productListViewModel.setIntentFrom(AppConstant.CATEGORY);
                productListViewModel.getProductList(contentId);
                break;
            case AppUriSchemeHandler.BRAND:
                productListViewModel.setIntentFrom(AppConstant.BRAND);
                productListViewModel.getProductsFromBrandURL(AppConstant.BRANDS_URL + contentUrl);
                break;
        }
    }

    /**
     * Getting sort options from string array xml
     *
     * @return list
     */
    public List<SortOptionLabel> sortOptionList() {
        List<SortOptionLabel> sortOptionList = new ArrayList<>();
        String[] labelList = getApplication().getResources().getStringArray(R.array.sort_label);
        String[] indexList = getApplication().getResources().getStringArray(R.array.sort_index);
        String algoliaIndex = CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_INDEX);
        if (labelList.length > 0) {
            int position = 0;
            for (String labelName : labelList) {
                SortOptionLabel optionLabel = new SortOptionLabel();
                optionLabel.setKey(labelName);
                optionLabel.setValue(algoliaIndex + "_" + indexList[position]);
                sortOptionList.add(optionLabel);
                position++;
            }
        }
        return sortOptionList;
    }

    private SortOptionLabel getPopularProductIndex() {
        return sortOptionList() != null && sortOptionList().size() > 0 && sortOptionList().get(0) != null ? sortOptionList().get(0) : new SortOptionLabel();
    }

    private void onGoogleTagManagerEvent(MStarProductDetails productDetails, int position) {
        FireBaseAnalyticsHelper.getInstance().logFireBaseProductClickAndSelectionEvent(this, productDetails, position, mTitle);
    }
}
