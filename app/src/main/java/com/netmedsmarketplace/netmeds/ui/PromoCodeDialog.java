package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DialogPromoCodeBinding;
import com.netmedsmarketplace.netmeds.viewmodel.PromoCodeDialogViewModel;
import com.nms.netmeds.base.BaseBottomSheetFragment;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.PromoCodeList;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class PromoCodeDialog extends BaseBottomSheetFragment implements PromoCodeDialogViewModel.PromoCodeListener {

    private final String promoCode;
    private final BSPromoCodeListener bsPromoCodeListener;
    private List<PromoCodeList> promoCodeLists = new ArrayList<>();

    public PromoCodeDialog(String promoCode, BSPromoCodeListener bsPromoCodeListener, List<PromoCodeList> promoCodeLists) {
        this.promoCode = promoCode;
        this.bsPromoCodeListener = bsPromoCodeListener;
        this.promoCodeLists = promoCodeLists;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogPromoCodeBinding promoCodeBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_promo_code, container, false);
        PromoCodeDialogViewModel bottomSheetDialogViewModel = new PromoCodeDialogViewModel(getActivity().getApplication());
        bottomSheetDialogViewModel.init(getActivity(), promoCodeBinding, this, promoCode, promoCodeLists);
        promoCodeBinding.setViewModel(bottomSheetDialogViewModel);
        return promoCodeBinding.getRoot();
    }

    @Override
    public void vmPromoCodeDismiss() {
        PromoCodeDialog.this.dismissAllowingStateLoss();
    }

    @Override
    public void vmRemovePromoCode(String message) {
        bsPromoCodeListener.promoCodeRemoved(message);
    }

    @Override
    public void vmShowProgress() {
        showProgress(getActivity());
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmPromoCodeApplied(String message) {
        bsPromoCodeListener.promoCodeApplied(message);
    }

    @Override
    public void vmPromoCodeAppliedFailed(String message) {
        bsPromoCodeListener.promoCodeAppliedFailed(message);
    }

    @Override
    public void setError(String error) {
        PromoCodeDialog.this.dismissAllowingStateLoss();
        bsPromoCodeListener.setError(error);
    }

    @Override
    public void applyMStarPromoCode(String promoCode) {
        PromoCodeDialog.this.dismissAllowingStateLoss();
        bsPromoCodeListener.applyMStarPromoCode(promoCode);
    }

    @Override
    public void unApplyMStarPromoCode(String removingPromoCode) {
        PromoCodeDialog.this.dismissAllowingStateLoss();
        bsPromoCodeListener.unApplyMStarPromoCode(removingPromoCode);
    }

    @Override
    public void onDestroy() {
        PaymentHelper.setPromoCodeDialoguePop(false);
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        bsPromoCodeListener.promoCodeDialogClose();
    }

    public interface BSPromoCodeListener {
        void promoCodeApplied(String message);

        void promoCodeRemoved(String message);

        void promoCodeAppliedFailed(String message);

        void setError(String error);

        void applyMStarPromoCode(String promoCode);

        void unApplyMStarPromoCode(String removingPromoCode);

        void promoCodeDialogClose();
    }
}
