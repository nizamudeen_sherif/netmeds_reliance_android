package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityMobileNumberBinding;
import com.netmedsmarketplace.netmeds.viewmodel.MobileNumberViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.view.SnackBarHelper;

import static com.nms.netmeds.base.IntentConstant.DOMAIN;
import static com.nms.netmeds.base.IntentConstant.PHONE_NUMBER;
import static com.nms.netmeds.base.IntentConstant.TOKEN;

public class MobileNumberActivity extends BaseViewModelActivity<MobileNumberViewModel> implements MobileNumberViewModel.OnMobileNumberListener {

    private ActivityMobileNumberBinding mobileNumberBinding;
    private MobileNumberViewModel mobileNumberViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mobileNumberBinding = DataBindingUtil.setContentView(this, R.layout.activity_mobile_number);
        mobileNumberBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(mobileNumberBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        setTermsAndCondition();
        mobileNumberBinding.phone.requestFocus();
    }

    @Override
    protected MobileNumberViewModel onCreateViewModel() {
        mobileNumberViewModel = ViewModelProviders.of(this).get(MobileNumberViewModel.class);
        mobileNumberViewModel.init(mobileNumberBinding, this, getIntent());
        onRetry(mobileNumberViewModel);
        return mobileNumberViewModel;
    }

    private void setTermsAndCondition() {
        final SpannableString clickableText = new SpannableString(getString(R.string.text_signin_terms));
        clickableText.setSpan(getSpanClick(ConfigConstant.TERMS_CONDITION, 31, 48), 31, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickableText.setSpan(getSpanClick(ConfigConstant.PRIVACY_POLICY, 53, 75), 53, 75, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickableText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorMediumPink)), 31, 48, 0);
        clickableText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorMediumPink)), 53, 75, 0);
        mobileNumberBinding.terms.setText(clickableText);
        mobileNumberBinding.terms.setMovementMethod(LinkMovementMethod.getInstance());
        mobileNumberBinding.terms.setHighlightColor(Color.TRANSPARENT);
    }

    private ClickableSpan getSpanClick(final String navigatePageFor, final int startPosition, final int endPosition) {
        return new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startWebPage(ConfigMap.getInstance().getProperty(navigatePageFor), getString(R.string.text_signin_terms).substring(startPosition, endPosition));
            }
        };
    }

    private void startWebPage(String url, String title) {
        Intent intent = new Intent(this, NetmedsWebViewActivity.class);
        intent.putExtra(IntentConstant.WEB_PAGE_URL, url);
        intent.putExtra(IntentConstant.PAGE_TITLE_KEY, title);
        startActivity(intent);
    }


    @Override
    public void vmCallBackOnNavigationSignInWithPasswordActivity() {
        Intent intent = new Intent(MobileNumberActivity.this, SignInWithPasswordActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        intent.putExtra(IntentConstant.PHONE_NUMBER, mobileNumberBinding.phone.getText() != null ? mobileNumberBinding.phone.getText().toString() : "");
        startActivity(intent);
        finish();
    }

    @Override
    public void vmCallBackOnNavigationSignUpActivity(String randomKey) {
        Intent intent = new Intent(MobileNumberActivity.this, SignUpActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        intent.putExtra(IntentConstant.PHONE_NUMBER, mobileNumberBinding.phone.getText() != null ? mobileNumberBinding.phone.getText().toString() : "");
        intent.putExtra(IntentConstant.RANDOM_KEY, randomKey);
        startActivity(intent);
        finish();
    }

    @Override
    public void vnCallBackOnSocialLoginOtpActivity(int randomKey) {
        Intent intent = new Intent(MobileNumberActivity.this, SocialLoginOtpActivity.class);
        intent.putExtra(DOMAIN, !TextUtils.isEmpty(getIntent().getStringExtra(DOMAIN)) ? getIntent().getStringExtra(DOMAIN) : "");
        intent.putExtra(TOKEN, !TextUtils.isEmpty(getIntent().getStringExtra(TOKEN)) ? getIntent().getStringExtra(TOKEN) : "");
        intent.putExtra(PHONE_NUMBER, mobileNumberBinding.phone.getText() != null && !TextUtils.isEmpty(mobileNumberBinding.phone.getText().toString()) ? mobileNumberBinding.phone.getText().toString() : "");
        intent.putExtra(IntentConstant.RANDOM_KEY, Integer.toString(randomKey));
        intent.putExtra(IntentConstant.SOCIAL_LOGIN_FLAG, true);
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmNoNetworkView(boolean isConnected) {
        mobileNumberBinding.mobileNumberViewContent.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        mobileNumberBinding.mobileNumberNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void vmWebServiceErrorView(boolean isWebserviceError) {
        mobileNumberBinding.mobileNumberViewContent.setVisibility(isWebserviceError ? View.GONE : View.VISIBLE);
        mobileNumberBinding.mobileNumberApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    @Override
    public Context getContext() {
        return MobileNumberActivity.this;
    }

    @Override
    public void vmSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(mobileNumberBinding.mobileNumberViewContent, this, message);
    }

    @Override
    public void onBackPressed() {
        MobileNumberActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mobileNumberViewModel.setButtonProperties(true);
    }
}
