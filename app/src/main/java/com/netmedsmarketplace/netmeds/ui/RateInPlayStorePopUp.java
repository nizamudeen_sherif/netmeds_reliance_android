package com.netmedsmarketplace.netmeds.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.BaseDialogFragment;

public class RateInPlayStorePopUp extends BaseDialogFragment {

    private RateInPlayStoreCallback callback;

    public RateInPlayStorePopUp(RateInPlayStoreCallback callback) {
        super();
        this.callback = callback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        com.netmedsmarketplace.netmeds.databinding.DialogRateInPlaystoreBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_rate_in_playstore, container, false);
        binding.btnRateLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.navigateToAccountFragment();
                dismiss();
            }
        });

        binding.btnRateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.navigateToPlayStore();
                dismiss();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCanceledOnTouchOutside(true);
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        callback.navigateToAccountFragment();
    }

    public interface RateInPlayStoreCallback {
        void navigateToPlayStore();

        void navigateToAccountFragment();
    }

}
