package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityNavigationBinding;
import com.netmedsmarketplace.netmeds.viewmodel.NavigationViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.MStarCustomerDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DeepLinkConstant;
import com.nms.netmeds.diagnostic.utils.DiagnosticHelper;
import com.nms.netmeds.m3subscription.fragment.SubscriptionFragment;

import java.util.ArrayList;

public class NavigationActivity extends BaseViewModelActivity<NavigationViewModel> implements NavigationViewModel.NavigationListener, AccountFragment.AccountCallBackListener {

    private ActivityNavigationBinding navigationBinding;
    private NavigationViewModel navigationViewModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redirectToHistoryIfPossible();
        navigationBinding = DataBindingUtil.setContentView(this, R.layout.activity_navigation);
        setPrimaryFragment(new HomeFragment());
        navigationBinding.setViewModel(onCreateViewModel());
        if (getIntent() != null)
            onIntent(getIntent());
    }

    @Override
    protected NavigationViewModel onCreateViewModel() {
        navigationViewModel = ViewModelProviders.of(this).get(NavigationViewModel.class);
        SubscriptionHelper.setHelper(new SubscriptionHelper());
        crashAnalyticsLog();
        navigationViewModel.init(this, navigationBinding, this, getIntent());
        return navigationViewModel;
    }

    @Override
    public void vmNavigateHome() {
        if (SubscriptionHelper.getInstance() != null) {
            SubscriptionHelper.setHelper(new SubscriptionHelper());
        }
        M2Helper.getInstance().setPaynowinitated(false);
        PaymentHelper.setIsRetryClick(false);
        setPrimaryFragment(new HomeFragment());
    }

    @Override
    public void vmNavigateOrders() {
        if (SubscriptionHelper.getInstance() != null) {
            SubscriptionHelper.setHelper(new SubscriptionHelper());
        }
        M2Helper.getInstance().setPaynowinitated(false);
        PaymentHelper.setIsRetryClick(false);
        if (checkUserLogin())
            setPrimaryFragment(new OrdersFragment());
    }

    @Override
    public void vmNavigateSubscription() {
        M2Helper.getInstance().setPaynowinitated(false);
        PaymentHelper.setIsRetryClick(false);
        if (checkUserLogin())
            setPrimaryFragment(new SubscriptionFragment());
    }

    @Override
    public void vmNavigateAccount() {
        if (SubscriptionHelper.getInstance() != null) {
            SubscriptionHelper.setHelper(new SubscriptionHelper());
        }
        if (checkUserLogin())
            setPrimaryFragment(new AccountFragment(this));
    }

    @Override
    public void vmNavigateWallet() {
        if (SubscriptionHelper.getInstance() != null) {
            SubscriptionHelper.setHelper(new SubscriptionHelper());
        }
        if (checkUserLogin())
            setPrimaryFragment(new WalletFragment());
    }

    private boolean checkUserLogin() {
        if (BasePreference.getInstance(this).isGuestCart()) {
            navigateToLogin();
            return false;
        } else
            return true;
    }

    private void navigateToLogin() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

    /**
     * Sets primary fragment.
     *
     * @param primaryFragment the primary fragment
     */
    private void setPrimaryFragment(final Fragment primaryFragment) {
        if (!isSimilarFragment(primaryFragment)) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.container, primaryFragment, primaryFragment.getClass().getName());
            ft.addToBackStack(primaryFragment.getClass().getName());
            if (!isFinishing()) {
                ft.commitAllowingStateLoss();
            }
        }
    }

    @Override
    public void callOrder() {
        navigationViewModel.navigateOrders();
    }

    @Override
    public void onBackPressed() {
        if (HomeFragment.class.getName().equals(getActiveFragment().getClass().getName())) {
            finish();
        } else {
            navigationViewModel.navigateHome();
        }
    }

    private void onIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null && intent.hasExtra("extraPathParams")) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get("extraPathParams");
            if (intentParamList != null && intentParamList.size() > 0)
                onNavigation(intentParamList.get(0));
        }
    }

    private void onNavigation(String path) {
        switch (path) {
            case DeepLinkConstant.WALLET:
            case DeepLinkConstant.MY_WALLET:
                navigationViewModel.setWalletActive();
                if (checkUserLogin())
                    setPrimaryFragment(new WalletFragment());
                break;
            case DeepLinkConstant.ORDER_HISTORY:
                navigationViewModel.setOrderActive();
                if (checkUserLogin())
                    setPrimaryFragment(new OrdersFragment());
                break;
            case DeepLinkConstant.ACCOUNT:
            case DeepLinkConstant.MY_ACCOUNT:
                navigationViewModel.setAccountActive();
                if (checkUserLogin())
                    setPrimaryFragment(new AccountFragment());
                break;
            case DeepLinkConstant.SUBSCRIPTION:
                navigationViewModel.navigateSubscription();
                break;
            case DeepLinkConstant.HOME:
                setPrimaryFragment(new HomeFragment());
                break;
        }
    }

    private void crashAnalyticsLog() {
        if (CommonUtils.isBuildVariantProdRelease()) {
            if (BasePreference.getInstance(this).getCustomerDetails() != null) {
                MStarCustomerDetails mStarCustomerDetails = new Gson().fromJson(BasePreference.getInstance(this).getCustomerDetails(), MStarCustomerDetails.class);
                Crashlytics.setString(AppConstant.USER_EMAIL, !TextUtils.isEmpty(mStarCustomerDetails.getEmail()) ? mStarCustomerDetails.getEmail() : "");
                Crashlytics.setString(AppConstant.USER_PHONE, !TextUtils.isEmpty(mStarCustomerDetails.getMobileNo()) ? mStarCustomerDetails.getMobileNo() : "");
                Crashlytics.setString(AppConstant.USER_ID, String.valueOf(mStarCustomerDetails.getId()));

            }
        }

    }

    // redirect the user to history if user come from order success medicine flow
    private void redirectToHistoryIfPossible() {
        if (getIntent() != null && getIntent().hasExtra(DeepLinkConstant.REDIRECT_TO_HISTORY) && getIntent().getBooleanExtra(DeepLinkConstant.REDIRECT_TO_HISTORY, false)) {
            LaunchIntentManager.routeToActivity(getResources().getString(R.string.route_history_activity), this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // redirect the user to diagnostic home page after selecting the lab test's (first time login to diagnostic)
        if (CommonUtils.isFirstTimeLoginToDiagnostic(this) && DiagnosticHelper.getSelectedList()!=null && DiagnosticHelper.getSelectedList().size() >0) {
            LaunchIntentManager.routeToActivity(getString(R.string.route_diagnostic_home), this);
        }else{
            BasePreference.getInstance(this).setDiaFirstTimeLogin(false);
        }
    }
}
