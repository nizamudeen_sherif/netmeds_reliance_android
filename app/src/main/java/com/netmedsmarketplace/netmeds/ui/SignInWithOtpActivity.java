package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivitySigninWithOtpBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SignInWithOtpViewModel;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.SnackBarHelper;

public class SignInWithOtpActivity extends BaseViewModelActivity<SignInWithOtpViewModel> implements SignInWithOtpViewModel.SignInWithOtpListener, AppViewModel.CountDownTimerListener {

    private ActivitySigninWithOtpBinding otpBinding;
    private SignInWithOtpViewModel signInWithOtpViewModel;
    private boolean isNavigateToCart = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        otpBinding = DataBindingUtil.setContentView(this, R.layout.activity_signin_with_otp);
        otpBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(otpBinding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        setNavigation();
    }

    @Override
    protected SignInWithOtpViewModel onCreateViewModel() {
        signInWithOtpViewModel = ViewModelProviders.of(this).get(SignInWithOtpViewModel.class);
        signInWithOtpViewModel.init(otpBinding, this, getIntent().getStringExtra(IntentConstant.PHONE_NUMBER), getIntent());
        onRetry(signInWithOtpViewModel);
        vmCounterTimer();
        return signInWithOtpViewModel;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            SignInWithOtpActivity.this.finish();
            return true;
        }
        return false;
    }

    private void setNavigation() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.FROM_CART) && getIntent().getBooleanExtra(IntentConstant.FROM_CART, false))
            isNavigateToCart = getIntent().getBooleanExtra(IntentConstant.FROM_CART, false);
    }

    @Override
    public void vmCallBackOnNavigationSignInWithPasswordActivity() {
        Intent intent = new Intent(SignInWithOtpActivity.this, SignInWithPasswordActivity.class);
        intent.putExtra(IntentConstant.PHONE_NUMBER, getIntent().getStringExtra(IntentConstant.PHONE_NUMBER));
        intent.putExtra(IntentConstant.FROM_PRIME, getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false));
        startActivity(intent);
        SignInWithOtpActivity.this.finish();
    }

    @Override
    public void vmCallBackOnNavigationMobileNumberActivity() {
        Intent intent = new Intent(SignInWithOtpActivity.this, SignInActivity.class);
        onNewIntent(intent);
        SignInWithOtpActivity.this.finish();
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmNavigateToHome() {
        if (isPrimeNavigation()) {
            primeNavigation();
        } else if (isNavigateToCart)
            launchCart();
        else launchNavigation();
        finishAffinity();
    }

    private boolean isPrimeNavigation() {
        return getIntent().getBooleanExtra(IntentConstant.FROM_PRIME, false);
    }

    private void primeNavigation() {
        Intent intent = new Intent(this, PrimeMemberShipActivity.class);
        intent.putExtra(IntentConstant.FROM_PRIME, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void launchCart() {
        Intent intent = new Intent(this, MStarCartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void launchNavigation() {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void vmCounterTimer() {
        signInWithOtpViewModel.onCountDownTimer(this);
    }

    @Override
    public void vmSnackbarMessage(String message) {
        SnackBarHelper.snackBarCallBack(otpBinding.signOtpViewContent, this, message);

    }

    @Override
    public Context getContext() {
        return SignInWithOtpActivity.this;
    }

    @Override
    public void navigateToLoginPageWithErrorMessage(String errorMessage) {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(IntentConstant.RESEND_OTP_ERROR_MESSAGE, errorMessage);
        intent.putExtra(IntentConstant.RESEND_OTP_ERROR_MOBILE_NO, signInWithOtpViewModel.getPhoneNumber());
        startActivity(intent);
        SignInWithOtpActivity.this.finish();
    }

    @Override
    public void onTick(long millisUntilFinished) {
        otpBinding.waitingOtp.setVisibility(View.VISIBLE);
        otpBinding.reSendOtp.setEnabled(false);
        otpBinding.reSendOtp.setTextColor(getResources().getColor(R.color.colorLightPink));
        String message = String.format(getString(R.string.text_waiting_for_otp), String.valueOf(millisUntilFinished / 1000));
        otpBinding.waitingOtp.setText(message);
    }

    @Override
    public void onFinish() {
        otpBinding.reSendOtp.setEnabled(true);
        otpBinding.reSendOtp.setTextColor(getResources().getColor(R.color.colorMediumPink));
        String message = String.format(getString(R.string.text_waiting_for_otp), String.valueOf(0));
        otpBinding.waitingOtp.setText(message);
        otpBinding.waitingOtp.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_SIGN_IN_WITH_OTP);
    }
}
