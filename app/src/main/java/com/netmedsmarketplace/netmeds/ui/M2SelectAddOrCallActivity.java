package com.netmedsmarketplace.netmeds.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityOrderConfirmationBinding;
import com.netmedsmarketplace.netmeds.viewmodel.M2OrderConfirmationViewModel;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

public class M2SelectAddOrCallActivity extends BaseViewModelActivity<M2OrderConfirmationViewModel> implements M2OrderConfirmationViewModel.Listener {

    private ActivityOrderConfirmationBinding orderConfirmationBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderConfirmationBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_confirmation);
        orderConfirmationBinding.setViewModel(onCreateViewModel());
        toolBarSetUp(orderConfirmationBinding.toolBarCustomLayout.toolbar);
        initToolBar(orderConfirmationBinding.toolBarCustomLayout.collapsingToolbar,this.getResources().getString(R.string.text_m2_order_confirmation));
    }

    @Override
    protected M2OrderConfirmationViewModel onCreateViewModel() {
        M2OrderConfirmationViewModel viewModel = ViewModelProviders.of(this).get(M2OrderConfirmationViewModel.class);
        viewModel.initViewModel(this, orderConfirmationBinding, this, basePreference());
        return viewModel;
    }

    @Override
    public void initSearch(int count) {
        startActivity(new Intent(M2SelectAddOrCallActivity.this, SearchActivity.class));
    }

    @Override
    public void initDeliveryAddress() {
        startActivity(new Intent(this, OrderConfirmationActivity.class));
    }

    @Override
    public void onClickContinueButton() {
        Intent intent = new Intent(this, AddOrUpdateAddressActivity.class);
        intent.putExtra(IntentConstant.ADD_ADDRESS_FLAG, false);
        intent.putExtra(IntentConstant.ADDRESS, new Gson().toJson(new MStarAddressModel()));
        startActivity(intent);
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false,orderConfirmationBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true,orderConfirmationBinding.toolBarCustomLayout.progressBar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }

    private BasePreference basePreference() {
        return BasePreference.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, GoogleAnalyticsHelper.POST_SCREEN_ORDER_CONFIRMATION_M2);
    }
}
