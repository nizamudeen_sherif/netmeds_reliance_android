package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.CategoryAdapter;
import com.netmedsmarketplace.netmeds.adpater.MstarCategoryAdapter;
import com.netmedsmarketplace.netmeds.adpater.OfferAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityCategoryBinding;
import com.netmedsmarketplace.netmeds.fragment.FilterFragment;
import com.netmedsmarketplace.netmeds.fragment.SortFragment;
import com.netmedsmarketplace.netmeds.viewmodel.CategoryViewModel;
import com.netmedsmarketplace.netmeds.viewmodel.CategoryViewModel.CategoryListener;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarFacetItemDetails;
import com.nms.netmeds.base.model.MstarSubCategoryResult;
import com.nms.netmeds.base.model.SortOptionLabel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.webengage.sdk.android.WebEngage;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends BaseViewModelActivity<CategoryViewModel> implements CategoryListener, AddCartBottomSheetDialog.BottomSheetDialogListener, OfferAdapter.BannerOfferInterface, FilterFragment.FilterInterface, SortFragment.ApplySortListener {

    private ActivityCategoryBinding binding;
    private CategoryViewModel viewModel;
    private boolean isAddToCartBottomSheetOpen = false;
    private String title = "";
    private String intentFrom = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category);
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
        binding.setViewModel(onCreateViewModel());
        MstarFilterHelper.clearSelection();
        MstarFilterHelper.clearSortSelection();
    }

    @Override
    protected CategoryViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        viewModel.categoryAdapter = null;
        viewModel.init(BasePreference.getInstance(this), this);
        viewModel.setSortOptionList(sortOptionList());
        viewModel.setSortOption(getPopularProductIndex());
        viewModel.getAlgoliaMutableLiveData().observe(this, new MstarAlgoliaFilterOptionObserver());
        onRetry(viewModel);
        getIntentFrom();
        return viewModel;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_cart));
        else
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                startActivity(new Intent(this, MStarCartActivity.class));
                return true;
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            default:
                return false;
        }
    }

    private void getIntentFrom() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(IntentConstant.CATEGORY_ID)) {
            title = getIntent().getStringExtra(IntentConstant.CATEGORY_NAME);
            viewModel.getCategoryList(intent.getIntExtra(IntentConstant.CATEGORY_ID, 0));
        } else if (getIntent() != null && getIntent().hasExtra(IntentConstant.BRAND_ID)) {
            intentFrom = AppConstant.BRAND;
            title = getIntent().getStringExtra(IntentConstant.BRAND_NAME);
            viewModel.getBrandProductList(getIntent().getIntExtra(IntentConstant.BRAND_ID, 0), getIntent().getStringExtra(IntentConstant.BANNER_AND_CATEGORY_RESPONSE));
        } else if (intent != null && intent.getExtras() != null && intent.hasExtra("extraPathParams")) {
            ArrayList<String> intentParamList = (ArrayList<String>) intent.getExtras().get("extraPathParams");
            String categoryId = "";
            if (intentParamList != null) {
                for (int i = 0; i < intentParamList.size(); i++) {
                    switch (i) {
                        case 0:
                            categoryId = intentParamList.get(i);
                            break;
                        case 1:
                            break;
                        case 2:
                            title = intentParamList.get(i);
                            break;
                    }
                }
            }
            try {
                viewModel.getCategoryList(Integer.parseInt(categoryId));
            } catch (NumberFormatException ignored) {
            }
        }
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, title);
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(CategoryActivity.this, title);
    }


    @Override
    public void OnAddCartCallBack(MStarProductDetails productDetails) {
        if (!isAddToCartBottomSheetOpen) {
            isAddToCartBottomSheetOpen = true;
            AddCartBottomSheetDialog couponFragment = new AddCartBottomSheetDialog(productDetails, false, this);
            getSupportFragmentManager().beginTransaction().add(couponFragment, "AddCartBottomSheetDialog").commitNowAllowingStateLoss();
        }
    }

    @Override
    public void openManufacturerProductList(Integer manufacturerId, String ManufacturerName, String productType) {
        Intent intent = new Intent(this, ProductList.class);
        intent.putExtra(IntentConstant.MANUFACTURER_ID, manufacturerId);
        intent.putExtra(IntentConstant.MANUFACTURER_NAME, ManufacturerName);
        intent.putExtra(IntentConstant.PRODUCT_TYPE, productType);
        startActivity(intent);
    }

    @Override
    public void openProductDetailsPage(int productCode) {
        Intent intent = new Intent(this, ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void applyBrandFilter(Integer brandId, String brandName, String categoryResponse) {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(IntentConstant.BRAND_ID, brandId);
        intent.putExtra(IntentConstant.BRAND_NAME, brandName);
        intent.putExtra(IntentConstant.BANNER_AND_CATEGORY_RESPONSE, categoryResponse);
        startActivity(intent);
    }

    @Override
    public void vmShowProgress() {
        showHorizontalProgressBar(false, binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public void vmDismissProgress() {
        showHorizontalProgressBar(true, binding.toolBarCustomLayout.progressBar);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void setSubCategoryView(List<MstarSubCategoryResult> subCategoryList) {
        if (subCategoryList != null && subCategoryList.size() > 0) {
            binding.llSubCategoryList.setVisibility(View.VISIBLE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            CategoryAdapter categoryAdapter = new CategoryAdapter(this, subCategoryList);
            binding.subCategoryList.setLayoutManager(layoutManager);
            binding.subCategoryList.setAdapter(categoryAdapter);
        } else {
            binding.llSubCategoryList.setVisibility(View.GONE);
        }
    }

    @Override
    public void setBannerView(List<String> defaultBannerLists, List<MstarBanner> mstarBanners) {
        if (mstarBanners != null && !mstarBanners.isEmpty()) {
            binding.bannerList.setVisibility(View.VISIBLE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            OfferAdapter offerAdapter = new OfferAdapter(mstarBanners, "", null, this);
            binding.bannerList.setLayoutManager(layoutManager);
            binding.bannerList.setAdapter(offerAdapter);
            CommonUtils.snapHelper(binding.bannerList);
            /*Google Tag Manager + FireBase Promotion Impression Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(this, mstarBanners, title + "_" + FireBaseAnalyticsHelper.MAIN, 1);
        } else
            binding.bannerList.setVisibility(View.GONE);

    }

    @Override
    public void showNoNetworkView(boolean isNoNetwork) {
        binding.categoryNetworkErrorView.setVisibility(isNoNetwork ? View.VISIBLE : View.GONE);
        binding.mainLayout.setVisibility(isNoNetwork ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebError(boolean isWebError) {
        binding.categoryApiErrorView.setVisibility(isWebError ? View.VISIBLE : View.GONE);
        binding.mainLayout.setVisibility(isWebError ? View.GONE : View.VISIBLE);
    }

    @Override
    public MstarCategoryAdapter setAdapter(List<MStarProductDetails> productList, MstarCategoryAdapter categoryAdapter, List<MstarFacetItemDetails> brandFilterList, int categoryId, String from) {
        if (productList != null && !productList.isEmpty()) {
            binding.cardViewPopularList.setVisibility(View.VISIBLE);
            categoryAdapter = new MstarCategoryAdapter(this, productList, brandFilterList, categoryId, viewModel, from, title);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            binding.popularProductsList.setLayoutManager(linearLayoutManager);
            binding.popularProductsList.setAdapter(categoryAdapter);
            binding.popularProductsList.setNestedScrollingEnabled(false);
            binding.popularProductsList.getViewTreeObserver().addOnScrollChangedListener(viewModel.paginationForOrderList(binding.nestedScrollView, linearLayoutManager));
        } else {
            binding.cardViewPopularList.setVisibility(View.GONE);
        }
        return categoryAdapter;
    }

    @Override
    public Context getContext() {
        return CategoryActivity.this;
    }

    @Override
    public void showEmptyView(boolean isEmpty) {
        binding.cardViewPopularList.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        binding.emptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setToolbarTitle(String toolbarTitle) {
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, toolbarTitle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_CATEGORY_LIST);
    }


    @Override
    public void bsAddCartCallBack() {
        CommonUtils.initiateVibrate(this);
        Snackbar snackbar = Snackbar.make(binding.mainLayout, getString(R.string.text_added_to_cart), Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.text_view_cart), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void showAlert(String message) {
        SnackBarHelper.snackBarCallBack(binding.mainLayout, this, message);
    }

    @Override
    public void updateCartCount(int count) {
        BasePreference.getInstance(CategoryActivity.this).setCartCount(count);
    }

    @Override
    public void addToCartBottomSheetClose() {
        isAddToCartBottomSheetOpen = false;
    }

    @Override
    public void imageClickListener(String bannerFrom, MstarBanner mstarBanner) {
        googleEvent(bannerFrom);
        CommonUtils.wellnessNavigateToScreens(this, mstarBanner, this);
    }

    @Override
    public String getTitleText() {
        return title;
    }

    private void googleEvent(String bannerFrom) {
        switch (bannerFrom) {
            case GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS:
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_BANNER, GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
            case GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER:
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_WELLNESS_SECTION, GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
        }
    }

    @Override
    public void applyFilter() {
        viewModel.onApplyFilterDataAvailable();
    }

    @Override
    public void clearFilter() {
        viewModel.setFilterQuery("");
        resetFilterValue();
    }

    @Override
    public void showError(String errorMessage) {
        showAlert(errorMessage);
    }

    @Override
    public void applySort(SortOptionLabel sortOption) {
        viewModel.onSortFilterApplied(sortOption);
    }

    @Override
    public void clearSort() {
        viewModel.setSortOption(getPopularProductIndex());
        resetFilterValue();
    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CategoryActivity.this, MStarCartActivity.class);
            startActivity(intent);
        }
    }

    public void onClickSorting(View view) {
        if (viewModel.getSortOptionList() != null && viewModel.getSortOptionList().size() > 0) {
            SortFragment sortFragment = new SortFragment(CategoryActivity.this, viewModel.getSortOptionList(), this);
            getSupportFragmentManager().beginTransaction().add(sortFragment, "sort_fragment").commitNowAllowingStateLoss();
        }
    }

    public void onClickFilter(View view) {
        if (viewModel.getAlgoliaFilterOption() != null) {
            FilterFragment filterFragment = new FilterFragment(viewModel.getAlgoliaFilterOption(), this, intentFrom);
            getSupportFragmentManager().beginTransaction().add(filterFragment, "filter_fragment").commitNowAllowingStateLoss();
        }
    }

    /**
     * Getting sort options from string array xml
     *
     * @return list
     */
    public List<SortOptionLabel> sortOptionList() {
        List<SortOptionLabel> sortOptionList = new ArrayList<>();
        String[] labelList = getApplication().getResources().getStringArray(R.array.sort_label);
        String[] indexList = getApplication().getResources().getStringArray(R.array.sort_index);
        String algoliaIndex = CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_INDEX);
        if (labelList.length > 0) {
            int position = 0;
            for (String labelName : labelList) {
                SortOptionLabel optionLabel = new SortOptionLabel();
                optionLabel.setKey(labelName);
                optionLabel.setValue(algoliaIndex + "_" + indexList[position]);
                sortOptionList.add(optionLabel);
                position++;
            }
        }
        return sortOptionList;
    }

    private SortOptionLabel getPopularProductIndex() {
        return sortOptionList() != null && sortOptionList().size() > 0 && sortOptionList().get(0) != null ? sortOptionList().get(0) : new SortOptionLabel();
    }


    private class MstarAlgoliaFilterOptionObserver implements Observer<MstarAlgoliaResponse> {

        @Override
        public void onChanged(@Nullable MstarAlgoliaResponse mstarAlgoliaResponse) {
            viewModel.onAlgoliaDataAvailable(mstarAlgoliaResponse);
            binding.setViewModel(viewModel);
        }
    }

    /*Reset filter and sort values while click clear*/
    private void resetFilterValue() {
        viewModel.setClearList(true);
        viewModel.fetchFacetValueFromAlgolia();
    }
}

