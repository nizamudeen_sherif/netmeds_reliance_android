package com.netmedsmarketplace.netmeds.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.CartItemQtyAdapter;
import com.netmedsmarketplace.netmeds.adpater.GenericProductAdapter;
import com.netmedsmarketplace.netmeds.adpater.MStarCartAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityGenericProductDetailsBinding;
import com.netmedsmarketplace.netmeds.viewmodel.GenericProductDetailViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.SimpleTooltip;
import com.nms.netmeds.base.view.SnackBarHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;

public class GenericProductDetailActivity extends BaseViewModelActivity<GenericProductDetailViewModel> implements GenericProductDetailViewModel.DetailPageListener, CartItemQtyAdapter.QuantityListListener {

    private ActivityGenericProductDetailsBinding binding;
    private GenericProductDetailViewModel viewModel;
    private int productCode;
    private boolean isExpanded = false;
    private int scrolledPosition = 0;
    private Dialog genDialog;
    private Menu menu;
    private CartItemQtyAdapter cartItemQtyAdapter;
    private MStarProductDetails mainProduct;
    private BasePreference basePreference;
    private static int TOOLTIP_WIDTH = 350;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_generic_product_details);
        toolBarSetUp(binding.toolbar);
        initToolBar();
        getIntentData();
        this.basePreference = BasePreference.getInstance(this);
        binding.setViewModel(onCreateViewModel());
    }


    private void getIntentData() {
        if (getIntent() != null && getIntent().hasExtra(IntentConstant.PRODUCT_CODE)) {
            productCode = getIntent().getIntExtra(IntentConstant.PRODUCT_CODE, 0);
        } else if (getIntent().hasExtra(AppConstant.EXTRA_PATH_PARAMS)) {
            ArrayList<String> intentParamList = (ArrayList<String>) getIntent().getExtras().get(AppConstant.EXTRA_PATH_PARAMS);
            productCode = Integer.parseInt(intentParamList != null && intentParamList.size() > 0 ? intentParamList.get(0) : "0");
        }

    }

    private void initToolBar() {
        binding.collapsingToolbar.setTitle(getString(R.string.string_generic_meds_tilte));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandTitleTextStyle);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapseTitleTextStyle);
        binding.collapsingToolbar.setExpandedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
        binding.collapsingToolbar.setCollapsedTitleTypeface(CommonUtils.getTypeface(this, "font/Lato-Bold.ttf"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.default_menu, menu);
        if (BasePreference.getInstance(this).getCartCount() > 0)
            menu.getItem(1).setIcon(ContextCompat.getDrawable(this, BasePreference.getInstance(this).getCartCount() > 0 ? R.drawable.ic_cart : R.drawable.ic_shopping_cart));
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cart:
                /*Google Analytics Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_CART_ICON_PRODUCT_DETAILS, GoogleAnalyticsHelper.EVENT_LABEL_PRODUCT_DETAILS_PAGE);
                navigateToCart();
                return true;
            case R.id.search:
                navigateToGenericSearch();
                return true;
            default:
                return false;
        }
    }

    private void navigateToGenericSearch() {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(IntentConstant.IS_FROM_GENERIC, true);
        startActivity(intent);
    }

    private void navigateToCart() {
        Intent intent = new Intent(this, MStarCartActivity.class);
        startActivity(intent);
    }

    @Override
    protected GenericProductDetailViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(GenericProductDetailViewModel.class);
        viewModel.init(basePreference, this, productCode);
        viewModel.getGenericMutableLiveData().observe(this, new GenericProductObserver());
        setVisibility(false);
        return viewModel;
    }


    private void setVisibility(boolean visible) {
        binding.productDetailView.setVisibility(visible ? View.VISIBLE : View.GONE);
        binding.genericCardView.setVisibility(visible ? View.VISIBLE : View.GONE);
        binding.infoCardView.setVisibility(visible ? View.VISIBLE : View.GONE);
        if (visible) {
            setShowMoreIconVisibility();
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void showLoader() {
        showProgress(this);
    }

    @Override
    public void dismissLoader() {
        dismissProgress();
    }

    @Override
    public void setProductsAdapter(List<MStarProductDetails> productDetailsList, Map<Integer, Integer> cartProductMap, GenericProductDetailViewModel genericProductDetailViewModel) {
        if (productDetailsList != null && productDetailsList.size() > 0 && !productDetailsList.isEmpty()) {
            binding.suggestedGenerics.setVisibility(View.VISIBLE);
            binding.genericBrandTitle.setVisibility(View.VISIBLE);
            binding.noGenericView.setVisibility(View.GONE);
            GenericProductAdapter adapter = new GenericProductAdapter(productDetailsList, cartProductMap, genericProductDetailViewModel);
            binding.suggestedGenerics.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            binding.suggestedGenerics.setAdapter(adapter);
        } else {
            binding.suggestedGenerics.setVisibility(View.GONE);
            binding.genericBrandTitle.setVisibility(View.GONE);
            binding.noGenericView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProductAddedCallBack() {
        CommonUtils.initiateVibrate(this);
        Snackbar snackbar = Snackbar.make(binding.parentViewGeneric, getString(R.string.text_added_to_cart), Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.text_view_cart), new ViewCartListener());
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorMediumPink));
        snackbar.show();
    }

    @Override
    public void hideSaveIcon() {
        binding.featuresLayout.setVisibility(View.GONE);
        binding.dividerGeneric.setVisibility(View.GONE);
    }

    @Override
    public void showMOre() {
        if (isExpanded) {
            isExpanded = false;
            CommonUtils.resizeView(binding.compositionLyt, 15, 500);
            CommonUtils.resizeView(binding.arrowBothSide, 15, 500);
            binding.expandCompositionText.setVisibility(View.GONE);
            binding.collapseCompositionText.setVisibility(View.VISIBLE);
            binding.showMoreIcon.setImageResource(R.drawable.ic_down_arrow);
        } else {
            isExpanded = true;
            CommonUtils.resizeView(binding.compositionLyt, -15, 500);
            CommonUtils.resizeView(binding.arrowBothSide, -15, 500);
            binding.collapseCompositionText.setVisibility(View.GONE);
            binding.expandCompositionText.setVisibility(View.VISIBLE);
            binding.showMoreIcon.setImageResource(R.drawable.ic_down_arrow_reverse);
        }
    }

    @Override
    public void navigateToProductDetailPage(int productCode) {
        Intent intent = new Intent(this, ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
        startActivity(intent);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        binding.productSvView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        binding.cartNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.productSvView.setVisibility(!isWebserviceError ? View.VISIBLE : View.GONE);
        binding.cartApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
    }

    public void setShowMoreIconVisibility() {
        final ViewTreeObserver observer = binding.collapseCompositionText.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Layout l = binding.collapseCompositionText.getLayout();
                if (l != null) {
                    int lines = l.getLineCount();
                    if (lines > 0)
                        binding.showMoreIcon.setVisibility(l.getEllipsisCount(lines - 1) > 0 ? View.VISIBLE : GONE);
                }
                return true;
            }
        });
    }

    @Override
    public void HideOrShowItemPicker(Map<Integer, Integer> cartProductMap, MStarProductDetails mainProductDetails) {
        this.mainProduct = mainProductDetails;
        if (AppConstant.PRODUCT_AVAILABLE_CODE.equalsIgnoreCase(mainProductDetails.getAvailabilityStatus())) {
            if (cartProductMap.containsKey(mainProductDetails.getProductCode())) {
                binding.addToCartMainGeneric.setVisibility(GONE);
                binding.qtyPickerMainLayout.setVisibility(View.VISIBLE);
                mainProductDetails.setCartQuantity(cartProductMap.get(mainProductDetails.getProductCode()));
                binding.mainProductQtyPicker.setText(String.format(binding.getRoot().getContext().getString(R.string.text_item_qty_with_label), String.valueOf(mainProductDetails.getCartQuantity())));
            } else {
                binding.addToCartMainGeneric.setVisibility(View.VISIBLE);
                binding.qtyPickerMainLayout.setVisibility(GONE);
            }
            binding.cartDeleteMainProduct.setOnClickListener(viewModel.removeProductFromCart(mainProductDetails));
        } else {
            binding.addToCartMainGeneric.setVisibility(GONE);
            binding.qtyPickerMainLayout.setVisibility(GONE);
            binding.genericStockInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void itemQuantityPickerDialog(MStarProductDetails mainProductDetails) {
        if (mainProductDetails != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View dialogView = LayoutInflater.from(this).inflate(R.layout.cart_item_qty_picker_dialog, null);
            builder.setView(dialogView);
            ImageView closeDialog = dialogView.findViewById(R.id.img_close_qty_list_dialog);
            RecyclerView rvQuantityList = dialogView.findViewById(R.id.rv_qty_list);
            setRVProperties(rvQuantityList, mainProduct);
            genDialog = builder.create();
            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    genDialog.dismiss();
                }
            });
            genDialog.show();
        }
    }

    @Override
    public void showErrorMessage(int text_medicine_max_limit, String message) {
        SnackBarHelper.snackBarCallBack(binding.parentViewGeneric, this, text_medicine_max_limit > 0 ? getString(text_medicine_max_limit) : !TextUtils.isEmpty(message) ? message : "");
    }

    @Override
    public void showToolTip(String message) {
        new SimpleTooltip.Builder(this)
                .anchorView(binding.llExtraOffer)
                .text(message)
                .gravity(Gravity.BOTTOM)
                .transparentOverlay(true)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setWidth(TOOLTIP_WIDTH)
                .build()
                .show();
    }


    private void setRVProperties(RecyclerView rvQuantityList, MStarProductDetails cartItems) {
        rvQuantityList.setNestedScrollingEnabled(false);
        rvQuantityList.setLayoutManager(new LinearLayoutManager(this));
        int maxQuantity = cartItems.getStockQty() < cartItems.getMaxQtyInOrder() ? cartItems.getStockQty() : cartItems.getMaxQtyInOrder();
        rvQuantityList.setAdapter(getQuantityAdapter(getQuantityList(maxQuantity, cartItems.getCartQuantity()), cartItems));
        rvQuantityList.scrollToPosition(scrolledPosition);
    }

    private List<MStarCartAdapter.ItemQuantity> getQuantityList(Integer quantity, Integer cartResultQuantity) {
        List<MStarCartAdapter.ItemQuantity> itemQuantityList = new ArrayList<>();
        for (int i = 1; i <= quantity; i++) {
            MStarCartAdapter.ItemQuantity itemQuantity = new MStarCartAdapter.ItemQuantity();
            itemQuantity.setQuantity(String.valueOf(i));
            itemQuantity.setQuantitySelected((i == cartResultQuantity));
            scrolledPosition = (i == cartResultQuantity) ? i - 1 : scrolledPosition;
            itemQuantityList.add(itemQuantity);
        }
        return itemQuantityList;
    }


    private CartItemQtyAdapter getQuantityAdapter(List<MStarCartAdapter.ItemQuantity> quantityList, MStarProductDetails cartItems) {
        cartItemQtyAdapter = new CartItemQtyAdapter(this, quantityList, cartItems, this);
        return cartItemQtyAdapter;
    }

    @Override
    public void itemQuantitySelectionCallback(int position, MStarCartAdapter.ItemQuantity itemQuantity, MStarProductDetails cartItems) {
        if (genDialog != null) {
            genDialog.dismiss();
        }
        scrolledPosition = position;
        cartItemQtyAdapter.updateList(position);
        int previousQty = cartItems.getCartQuantity();
        int newQty = position + 1;
        if (previousQty > newQty) {
            viewModel.decreaseQuantity(cartItems.getProductCode(), previousQty - newQty, cartItems, false);
        } else if (previousQty < newQty) {
            viewModel.increaseQuantity(cartItems.getProductCode(), newQty - previousQty, cartItems, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getCartDetails();
    }

    private class ViewCartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(GenericProductDetailActivity.this, MStarCartActivity.class);
            startActivity(intent);
        }
    }

    private class GenericProductObserver implements Observer<String> {

        @Override
        public void onChanged(@Nullable String data) {
            viewModel.onGenericDataAvailable(data);
            binding.setViewModel(viewModel);
            /*Google Analytics Post Screen*/
            GoogleAnalyticsHelper.getInstance().postScreen(GenericProductDetailActivity.this, GoogleAnalyticsHelper.POST_SCREEN_GENERIC + mainProduct.getDisplayName());
            setVisibility(true);
            if (menu != null)
                menu.getItem(1).setIcon(ContextCompat.getDrawable(getBaseContext(), basePreference.getCartCount() > 0 ? R.drawable.ic_cart : R.drawable.ic_shopping_cart));
        }
    }

}
