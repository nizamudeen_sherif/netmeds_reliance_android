package com.netmedsmarketplace.netmeds.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ActivityOnboardBinding;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.view.LinePagerIndicatorDecoration;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TrueSDK;
import com.truecaller.android.sdk.TrueSdkScope;

public class OnBoardingActivity extends BaseViewModelActivity<OnBoardingViewModel> implements OnBoardingViewModel.OnBoardingListener {

    private static final int TRUE_CALLER_REQUEST_CODE = 100;
    private ActivityOnboardBinding onboardBinding;
    private OnBoardingViewModel onBoardingViewModel;
    private final ITrueCallback trueCallerCallback = new ITrueCallback() {

        @Override
        public void onSuccessProfileShared(@NonNull final TrueProfile trueProfile) {
            onBoardingViewModel.setTrueProfile(trueProfile);
        }

        @Override
        public void onFailureProfileShared(@NonNull final TrueError trueError) {
            /*Google Analytics Event*/
            if (trueError.getErrorType() == TrueError.ERROR_TYPE_USER_DENIED) {
                GoogleAnalyticsHelper.getInstance().postEvent(OnBoardingActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_LOGIN, GoogleAnalyticsHelper.EVENT_ACTION_NEW_NUMBER, GoogleAnalyticsHelper.EVENT_LABEL_TRUE_CALLER);
            }
            navigateToSignIn();
        }

        @Override
        public void onOtpRequired() {
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_onboard);
        onboardBinding.setViewModel(onCreateViewModel());
    }

    @Override
    protected OnBoardingViewModel onCreateViewModel() {
        onBoardingViewModel = ViewModelProviders.of(this).get(OnBoardingViewModel.class);
        onBoardingViewModel.init(this);
        initTrueCaller();
        onRetry(onBoardingViewModel);
        return onBoardingViewModel;
    }

    @Override
    public void vmCallbackOnStart() {
        if (TrueSDK.getInstance().isUsable()) {
            TrueSDK.getInstance().getUserProfile(this);
        } else {
            if ((!BasePreference.getInstance(this).isMstarSessionIdAvailable())) {
                navigateToSignIn();
            } else {
                Intent intent = new Intent(this, NavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void vmCallbackNewUser(String randomKey) {
        Intent intent = new Intent(OnBoardingActivity.this, SignUpActivity.class);
        intent.putExtra(IntentConstant.PHONE_NUMBER, onBoardingViewModel.getTrueProfile() != null && !TextUtils.isEmpty(onBoardingViewModel.getTrueProfile().phoneNumber.substring(3)) ? onBoardingViewModel.getTrueProfile().phoneNumber.substring(3) : "");
        intent.putExtra(IntentConstant.EMAIL, onBoardingViewModel.getTrueProfile() != null && !TextUtils.isEmpty(onBoardingViewModel.getTrueProfile().email) ? onBoardingViewModel.getTrueProfile().email : "");
        intent.putExtra(IntentConstant.FIRSTNAME, onBoardingViewModel.getTrueProfile() != null && !TextUtils.isEmpty(onBoardingViewModel.getTrueProfile().firstName) ? onBoardingViewModel.getTrueProfile().firstName : "");
        intent.putExtra(IntentConstant.LASTNAME, onBoardingViewModel.getTrueProfile() != null && !TextUtils.isEmpty(onBoardingViewModel.getTrueProfile().lastName) ? onBoardingViewModel.getTrueProfile().lastName : "");
        intent.putExtra(IntentConstant.TRUE_CALLER_FLAG, true);
        intent.putExtra(IntentConstant.RANDOM_KEY, randomKey);
        startActivity(intent);
        finish();
    }

    @Override
    public void vmCallbackExistingUser() {
        BasePreference.getInstance(this).setGuestCart(false);
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finishAffinity();
    }

    private void navigateToSignIn() {
        Intent intent = new Intent(OnBoardingActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TRUE_CALLER_REQUEST_CODE:
                TrueSDK.getInstance().onActivityResultObtained(this, resultCode, data);
                break;
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showNoNetworkView(boolean isShowNoNetworkView) {
        onboardBinding.onboardViewContent.setVisibility(isShowNoNetworkView ? View.VISIBLE : View.GONE);
        onboardBinding.onboardNetworkErrorView.setVisibility(isShowNoNetworkView ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isShowWebserviceErrorView) {
        onboardBinding.onboardViewContent.setVisibility(isShowWebserviceErrorView ? View.GONE : View.VISIBLE);
        onboardBinding.onboardApiErrorView.setVisibility(isShowWebserviceErrorView ? View.VISIBLE : View.GONE);
    }

    @Override
    public void letsStartClick(boolean isEnable) {
        onboardBinding.letsStart.setClickable(isEnable);
        onboardBinding.letsStart.setEnabled(isEnable);
    }

    @Override
    public void setRecyclerViewAdapter(OnBoardingAdapter adapter) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        onboardBinding.recyclerView.setLayoutManager(linearLayoutManager);
        onboardBinding.recyclerView.setAdapter(adapter);
        onboardBinding.recyclerView.addItemDecoration(new LinePagerIndicatorDecoration(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorSecondary)));
        CommonUtils.snapHelper(onboardBinding.recyclerView);
    }

    @Override
    public void vmSnackBarMessage(String message) {
        SnackBarHelper.snackBarCallBack(onboardBinding.onboardViewContent, this, message);
    }

    @Override
    public void vmDismissProgress() {
        dismissProgress();
    }

    @Override
    public void vmShowProgress() {
        showProgress(this);
    }

    private BasePreference basePreference() {
        return BasePreference.getInstance(this);
    }

    private void initTrueCaller() {
        TrueSdkScope trueScope = new TrueSdkScope.Builder(this, trueCallerCallback)
                .consentMode(TrueSdkScope.CONSENT_MODE_POPUP)
                .consentTitleOption(TrueSdkScope.SDK_CONSENT_TITLE_VERIFY)
                .footerType(TrueSdkScope.FOOTER_TYPE_CONTINUE)
                .build();
        TrueSDK.init(trueScope);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(OnBoardingActivity.this, GoogleAnalyticsHelper.POST_SCREEN_ON_BOARDING);
    }
}
