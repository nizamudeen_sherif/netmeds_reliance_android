package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.M2Helper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.adpater.CartSuggestionAdapter;
import com.netmedsmarketplace.netmeds.adpater.MStarCartAdapter;
import com.netmedsmarketplace.netmeds.databinding.ActivityMstarCartBinding;
import com.netmedsmarketplace.netmeds.viewmodel.MStarCartViewModel;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.BaseViewModelActivity;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.RefillHelper;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarCartDetails;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBrainSinConfigDetails;
import com.nms.netmeds.base.model.PromoCodeList;
import com.nms.netmeds.base.retrofit.APIServiceManager;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;
import com.nms.netmeds.base.utils.NetworkUtils;
import com.nms.netmeds.base.utils.WebEngageHelper;
import com.nms.netmeds.base.view.SnackBarHelper;
import com.nms.netmeds.m3subscription.SubscriptionIntentConstant;
import com.nms.netmeds.m3subscription.ui.M3DeliveryDateIntervalActivity;
import com.webengage.sdk.android.WebEngage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.nms.netmeds.consultation.IntentConstant.PRESCRIPTION_BASE_URL;

public class MStarCartActivity extends BaseViewModelActivity<MStarCartViewModel> implements MStarCartViewModel.MStarCartViewModelCallback,
        PromoCodeDialog.BSPromoCodeListener, AlternateFragment.AlternateProductsListener, CartSuggestionAdapter.CartSuggestionInterface,
        CommonWarningDialog.CommonWarningDialogListener, SubscriptionPopUpDialog.SubscriptionPopupListener {

    private static final int PRIME_PRODUCT_CODE_6_MON = 2001;
    private static final int PRIME_PRODUCT_CODE_12_MON = 2002;

    private MStarCartViewModel viewModel;
    private ActivityMstarCartBinding binding;

    private boolean isPromoCodeDialogOpen = false;

    private List<PromoCodeList> promoCodeLists = new ArrayList<>();

    private String prescription_base_url = "";
    private static String M2UPDATE = "m2UpdateWarning";
    private static String MAXLIMIT = "maxLimitBreach";
    private static String ALTERNATE_CART = "AlternateCart";
    private static String OUT_OF_STOCK = "out_of_stock";
    private boolean isActivityResultCalled = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mstar_cart);
        toolBarSetUp(binding.toolBarCustomLayout.toolbar);
        binding.setViewModel(onCreateViewModel());
        getIntentData(getIntent());
        initToolBar(binding.toolBarCustomLayout.collapsingToolbar, getString(R.string.text_my_cart));

        if (getIntent() != null && getIntent().hasExtra("PRESCRIPTION_BASE_URL")) {
            prescription_base_url = getIntent().getStringExtra("PRESCRIPTION_BASE_URL");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getIntentData(intent);
        viewModel.checkM1OrM2ThenProceedFurther();
    }

    private void getIntentData(Intent intent) {
        Intent newIntent = intent;
        if (newIntent != null && newIntent.hasExtra(IntentConstant.IS_OUT_OF_STOCK)) {
            viewModel.interCityProductList = newIntent.getStringArrayListExtra(IntentConstant.INTER_CITY_PRODUCT_LIST);
            viewModel.isOutOfStock = newIntent.getBooleanExtra(IntentConstant.IS_OUT_OF_STOCK, false);
        }
        if (newIntent != null && newIntent.hasExtra(IntentConstant.ORDER_ID)) {
            viewModel.orderId = newIntent.getStringExtra(IntentConstant.ORDER_ID);
        }
    }

    @Override
    protected MStarCartViewModel onCreateViewModel() {
        viewModel = ViewModelProviders.of(this).get(MStarCartViewModel.class);
        viewModel.initiateCart(this, getBasePreference());
        onRetry(viewModel);
        viewModel.getCartDetailsForPrimeMutableLiveData().observe(this, new CartDetailsForPrimeObserver());
        return viewModel;
    }

    private BasePreference getBasePreference() {
        return BasePreference.getInstance(this);
    }

    @Override
    public void showLoader() {
        binding.cartProceedBtn.setEnabled(false);
        binding.cartProceedBtn.setBackground(getContext().getResources().getDrawable(R.drawable.grey_background_button));
        showHorizontalProgressBar(false, binding.toolBarCustomLayout.progressBar); // while loading the view should be non clickable
    }

    @Override
    public void dismissLoader() {
        binding.cartProceedBtn.setEnabled(true);
        binding.cartProceedBtn.setBackground(getContext().getResources().getDrawable(R.drawable.accent_button));
        showHorizontalProgressBar(true, binding.toolBarCustomLayout.progressBar);// after loading the view should be clickable
    }

    @Override
    public void showErrorMessage(String message) {
        SnackBarHelper.snackBarCallBack(binding.cartViewContent, this, message);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isConnected(this);
    }

    @Override
    public void showNoNetworkView(boolean isConnected) {
        binding.lvCartView.setVisibility(isConnected ? View.VISIBLE : View.GONE);
        if (!isConnected)
            binding.cartTotalAmountLayout.setVisibility(View.GONE);
        binding.cartNetworkErrorView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showWebserviceErrorView(boolean isWebserviceError) {
        binding.lvCartView.setVisibility(!isWebserviceError ? View.VISIBLE : View.GONE);
        binding.cartApiErrorView.setVisibility(isWebserviceError ? View.VISIBLE : View.GONE);
        if (isWebserviceError)
            binding.cartTotalAmountLayout.setVisibility(View.GONE);
    }

    @Override
    public void enableNoRecordView(boolean isNoRecordFound) {
        if (isNoRecordFound) {
            RefillHelper.getOutOfStockProductsMap().clear();
            binding.cartTotalAmountLayout.setVisibility(View.GONE);
        }
        parentViewVisible(true);
        binding.lvCartView.setVisibility(isNoRecordFound ? View.GONE : View.VISIBLE);
        binding.cartEmptyCartView.setVisibility(isNoRecordFound ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onStart() {
        /*In-app message screen name*/
        WebEngage.get().analytics().screenNavigated(WebEngageHelper.IN_APP_NOTIFICATION_SCREEN_NAME_CART_PAGE);
        if (!isActivityResultCalled) {
            viewModel.checkM1OrM2ThenProceedFurther();
        } else {
            isActivityResultCalled = false;
        }
        super.onStart();
    }

    @Override
    public void onClickProceedButton(boolean isNewCustomer) {
        /*WebEngage Checkout Stared Event*/
        viewModel.onCheckoutStartEvent();
        /*Google Analytics Click Event*/
        setGoogleAnalyticsCartProceedEvent();
        Intent intent;
        if (!M2Helper.getInstance().isM2Order() && ((PaymentHelper.isIsTempCart() && viewModel.isEdited && !viewModel.getRxProductNameList().isEmpty() && !viewModel.getRxProductIdList().isEmpty()) || (!viewModel.getRxProductNameList().isEmpty() && !viewModel.getRxProductIdList().isEmpty()))) {
            intent = new Intent(this, M1AttachPrescriptionActivity.class);
            if (!TextUtils.isEmpty(prescription_base_url) && getIntent().hasExtra(com.nms.netmeds.consultation.IntentConstant.IS_FROM_CONSULTATION_TO_CART)
                    && getIntent().getBooleanExtra(com.nms.netmeds.consultation.IntentConstant.IS_FROM_CONSULTATION_TO_CART, false)) {
                intent.putExtra(PRESCRIPTION_BASE_URL, prescription_base_url);
            }
            intent.putExtra(IntentConstant.RX_MEDICINE_LIST, viewModel.getRxProductNameList());
            intent.putExtra(IntentConstant.SKU_LIST, viewModel.getRxProductIdList());
        } else {
            PaymentHelper.setIsExternalDoctor(false);
            if ((SubscriptionHelper.getInstance().isSubscriptionFlag() && !isNewCustomer) || (SubscriptionHelper.getHelper().isSubscriptionEdit() && !viewModel.getRxProductNameList().isEmpty())) {
                intent = new Intent(this, M3DeliveryDateIntervalActivity.class);
            } else if (isNewCustomer) {
                intent = new Intent(this, AddOrUpdateAddressActivity.class);
                intent.putExtra(IntentConstant.ADD_ADDRESS_FLAG, false);
                intent.putExtra(IntentConstant.ADDRESS, new Gson().toJson(new MStarAddressModel()));
                intent.putExtra(IntentConstant.IS_FROM_NEW_CUSTOMER, true);
            } else
                intent = new Intent(this, OrderConfirmationActivity.class);
        }
        intent.putExtra(SubscriptionIntentConstant.DELIVERY_ESTIMATE_REQUEST, new Gson().toJson(viewModel.getDeliveryEstimateRequest()));
        startActivity(intent);
    }

    private void setGoogleAnalyticsCartProceedEvent() {
        if (M2Helper.getInstance().isM2Order())
            GoogleAnalyticsHelper.getInstance().postEvent(MStarCartActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION_2, GoogleAnalyticsHelper.EVENT_ACTION_CART_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_M2_CART);
        else
            GoogleAnalyticsHelper.getInstance().postEvent(MStarCartActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NAVIGATION, GoogleAnalyticsHelper.EVENT_ACTION_CART_PROCEED, GoogleAnalyticsHelper.EVENT_LABEL_CART_PAGE);
    }

    @Override
    public void onClickSearchButton() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void updateCartProductAdapter(MStarCartAdapter adapter) {
        viewModel.getRxProductNameList().clear();
        viewModel.getRxProductIdList().clear();
        binding.cartProductList.setLayoutManager(new LinearLayoutManager(this));
        binding.cartProductList.setNestedScrollingEnabled(false);
        binding.cartProductList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showAppliedPromocodeView(String promoCode, MStarCartDetails cartDetails) {
        binding.trApplyPromoCode.setClickable(!viewModel.isM2Order() && !SubscriptionHelper.getInstance().isSubscriptionFlag() && !PaymentHelper.isIsEditOrder());
        binding.trApplyPromoCode.setEnabled(!viewModel.isM2Order() && !SubscriptionHelper.getInstance().isSubscriptionFlag() && !PaymentHelper.isIsEditOrder());
        binding.chkApplyPromoCode.setChecked(!TextUtils.isEmpty(promoCode));
        binding.cartTvPromoCode.setText(TextUtils.isEmpty(promoCode) ? getString(R.string.text_apply_promo_code) : promoCode);
        binding.tvPromoCodeDescription.setText(TextUtils.isEmpty(promoCode) ? getString(R.string.text_apply_promo_code_description) : "");
        binding.tvPromoCodeDiscount.setText("");

        if (promoCodeLists != null && promoCodeLists.size() > 0) {
            for (PromoCodeList promoCodeList : promoCodeLists) {
                if (promoCode.equalsIgnoreCase(promoCodeList.getCouponCode())) {
                    binding.tvPromoCodeDescription.setText(!TextUtils.isEmpty(promoCodeList.getDescription()) ? promoCodeList.getDescription() : "");
                    binding.tvPromoCodeDiscount.setText(String.format(getString(R.string.text_promo_saving), !TextUtils.isEmpty(promoCodeList.getDiscount()) ? promoCodeList.getDiscount() : "") + "%");
                }
            }
        }
        if (M2Helper.getInstance().isM2Order() || PaymentHelper.isIsEditOrder()) {
            binding.tvPromoCodeDiscount.setText(String.format(getApplication().getResources().getString(R.string.text_promo_saving), viewModel.getM2OrderCouponDiscount() + "%"));
        }
        if (SubscriptionHelper.getInstance().isSubscriptionFlag() && cartDetails != null) {
            binding.tvPromoCodeDiscount.setText(String.format(getApplication().getResources().getString(R.string.text_promo_saving), (cartDetails.getCoupon_discount_total().compareTo(BigDecimal.ZERO) == 0 ? 0 : viewModel.getSubscriptionCouponDiscount()) + "%"));
        }
        binding.imPromoShow.setVisibility(SubscriptionHelper.getInstance().isSubscriptionFlag() ? View.GONE : View.VISIBLE);
        binding.trApplyPromoCode.setVisibility(!TextUtils.isEmpty(promoCode) || !SubscriptionHelper.getInstance().isSubscriptionFlag() ? View.VISIBLE : View.GONE);
        binding.promocodeTitle.setVisibility(!TextUtils.isEmpty(promoCode) || !SubscriptionHelper.getInstance().isSubscriptionFlag() ? View.VISIBLE : View.GONE);
        binding.cvParentPromoCodeSuperCash.setVisibility(!TextUtils.isEmpty(promoCode) || !SubscriptionHelper.getInstance().isSubscriptionFlag() ? View.VISIBLE : View.GONE);


    }

    @Override
    public void onLaunchApplyPromocodeDialog(String alreadySelectedPromocode, List<PromoCodeList> promoCodeLists) {
        if (!isPromoCodeDialogOpen) {
            isPromoCodeDialogOpen = true;
            /*Google Analytics Click Events*/
            GoogleAnalyticsHelper.getInstance().postEvent(MStarCartActivity.this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_PROMO_CODE, GoogleAnalyticsHelper.EVENT_ACTION_APPLY_PROMOCODE, GoogleAnalyticsHelper.EVENT_LABEL_CART_PAGE);
            PromoCodeDialog couponFragment = new PromoCodeDialog(alreadySelectedPromocode, this, promoCodeLists);
            getSupportFragmentManager().beginTransaction().add(couponFragment, "PromoCodeDialog").commitNowAllowingStateLoss();
        }
    }

    @Override
    public void updateCartPaymentDetails(MStarCartDetails cartDetails) {
        binding.cartPaymentDetail.setVisibility(cartDetails != null && !M2Helper.getInstance().isM2Order() ? View.VISIBLE : View.GONE);
        binding.cartTotalAmountLayout.setVisibility(View.VISIBLE);
        if (cartDetails != null) {
            if (M2Helper.getInstance().isM2Order()) {
                binding.cartTotalAmount.setText(getResources().getString(R.string.text_m2_total_amount));
            } else {
                BigDecimal walletAndSuperCashAmount = cartDetails.getUsedWalletAmount() != null ? cartDetails.getUsedWalletAmount().getTotalWallet() : BigDecimal.ZERO;
                PaymentHelper.setTotalAmount(cartDetails.getNetPayableAmount().doubleValue());
                binding.cartMrpTotal.setText(CommonUtils.getPriceInFormat(cartDetails.getSubTotalAmount()));
                //Delivery Charges
                binding.cartDeliveryCharges.setText(CommonUtils.getPriceInFormat(cartDetails.getShippingChargesFinal()));
                //Additional Discount
                binding.llAdditionalDiscount.setVisibility(cartDetails.getProduct_discount_total().compareTo(BigDecimal.ZERO) != 0 ? View.VISIBLE : View.GONE);
                binding.cartAdditionalDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(cartDetails.getProduct_discount_total()));
                //Netmeds coupon discount
                binding.cartNetmedsDiscountLayout.setVisibility(cartDetails.getCoupon_discount_total().compareTo(BigDecimal.ZERO) != 0 ? View.VISIBLE : View.GONE);
                binding.cartNetmedsDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(cartDetails.getCoupon_discount_total()));
                //Strike discount
                binding.cartTvStrikeDeliveryCharges.setVisibility(cartDetails.getShippingChargesFinal().compareTo(BigDecimal.ZERO) == 0 && cartDetails.getShippingChargesOriginal().compareTo(BigDecimal.ZERO) != 0 ? View.VISIBLE : View.GONE);
                binding.cartTvStrikeDeliveryCharges.setPaintFlags(binding.cartTvStrikeDeliveryCharges.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                binding.cartTvStrikeDeliveryCharges.setText(CommonUtils.getPriceInFormat(cartDetails.getShippingChargesOriginal()));

                binding.cartNmsWallet.setText(CommonUtils.getPriceInFormatWithHyphen(walletAndSuperCashAmount));
                binding.cartVoucherDiscount.setText(CommonUtils.getPriceInFormatWithHyphen(cartDetails.getUsedVoucherAmount()));
                binding.cartTotalAmount.setText(CommonUtils.getPriceInFormat(cartDetails.getNetPayableAmount()));
                binding.cartOrderAmount.setText(CommonUtils.getPriceInFormat(cartDetails.getNetPayableAmount()));
                binding.cartTotalSaving.setText(CommonUtils.getPriceInFormat(cartDetails.getTotalSavings()));
                binding.cartNmsWalletVisibility.setVisibility(walletAndSuperCashAmount.compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
                binding.cartVoucherLayout.setVisibility(cartDetails.getUsedVoucherAmount().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
                binding.cartTotalSavingLayout.setVisibility(cartDetails.getTotalSavings().compareTo(BigDecimal.ZERO) > 0 ? View.VISIBLE : View.GONE);
                binding.cartPaymentText.setVisibility(cartDetails.getSubTotalAmount().compareTo(BigDecimal.ZERO) != 0 ? View.VISIBLE : View.GONE);
            }
        }
    }

    @Override
    public void navigateToProductDetails(int productCode) {
        if (productCode == PRIME_PRODUCT_CODE_6_MON || productCode == PRIME_PRODUCT_CODE_12_MON) {
            Intent intent = new Intent(this, PrimeMemberShipActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ProductDetail.class);
            intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, productCode);
            startActivity(intent);
        }
    }

    public void onPrimeBannerClicked(View view) {
        Intent intent = new Intent(this, PrimeMemberShipActivity.class);
        intent.putExtra(PrimeMemberShipActivity.KEY_INTENT_FROM_CART, PrimeMemberShipActivity.KEY_INTENT_FROM_CART);
        startActivityForResult(intent, AppConstant.PRIME_REQUEST_CODE);
    }

    @Override
    public void showPrimeMemberShipBanner(boolean isPrimeCustomer) {
        binding.cartPrimeBanner.setVisibility(isPrimeCustomer ? View.GONE : View.VISIBLE);
    }

    @Override
    public void navigateToSignIn() {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.FROM_CART, true);
        LaunchIntentManager.routeToActivity(MStarCartActivity.this.getResources().getString(R.string.route_sign_in_activity), intent, MStarCartActivity.this);
    }

    @Override
    public void walletBalance(String balance, boolean isVisibleNMSCash) {
        int visibility = !TextUtils.isEmpty(balance) && isVisibleNMSCash && !SubscriptionHelper.getInstance().isSubscriptionFlag() && !PaymentHelper.isIsEditOrder() && !M2Helper.getInstance().isM2Order() ? View.VISIBLE : View.GONE;
        binding.trApplyNmsSuperCash.setVisibility(visibility);
        binding.trViewVisibility.setVisibility(visibility);
        binding.tvNmsCashBalance.setVisibility(!TextUtils.isEmpty(balance) ? View.VISIBLE : View.GONE);
        binding.tvNmsCashBalance.setText(String.format("%s  ₹ %s", getApplication().getResources().getString(R.string.nms_use), !TextUtils.isEmpty(balance) ? balance : ""));

    }

    @Override
    public void showNNMSSuperCashView(boolean isNmsSuperCash) {
        binding.chkNmsSuperCash.setChecked(isNmsSuperCash);
        binding.trApplyNmsSuperCash.setEnabled(!isNmsSuperCash);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void nmsSuperCashDescription(PromoCodeList mstarSuperCash) {
        if (mstarSuperCash != null) {
            binding.tvNmsSuperCashDescription.setText(!TextUtils.isEmpty(mstarSuperCash.getDescription()) ? mstarSuperCash.getDescription() : "");
            binding.tvNmsSuperCash.setText(!TextUtils.isEmpty(mstarSuperCash.getCouponCode()) ? mstarSuperCash.getCouponCode() : "");
        }
    }

    @Override
    public void poromocodeDescription(List<PromoCodeList> promoCodeLists) {
        this.promoCodeLists = promoCodeLists;
    }

    @Override
    public void showAlternateSalt(int outOfStockProduct) {
        AlternateFragment alternateFragment = new AlternateFragment(outOfStockProduct, BasePreference.getInstance(this), this);
        getSupportFragmentManager().beginTransaction().add(alternateFragment, "AlternateFragment").commitNowAllowingStateLoss();
    }

    @Override
    public void setProductSuggestionView(MstarBrainSinConfigDetails configDetails, boolean isVisible) {
        if (configDetails != null && isVisible && !M2Helper.getInstance().isM2Order()) {
            binding.cartProductSuggestion.cartMainLayout.setVisibility(View.VISIBLE);
            binding.cartProductSuggestion.cartTitle.setText(!TextUtils.isEmpty(configDetails.getTitle()) ? configDetails.getTitle() : "");
            binding.cartProductSuggestion.cartSubTitle.setText(!TextUtils.isEmpty(configDetails.getSubTitle()) ? configDetails.getSubTitle() : "");
            binding.cartProductSuggestion.cartDescription.setText(!TextUtils.isEmpty(configDetails.getDescription()) ? configDetails.getDescription() : "");
        } else {
            binding.cartProductSuggestion.cartMainLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void setProductSuggestionAdapter(ArrayList<HRVProduct> hrvProductList) {
        CartSuggestionAdapter cartSuggestionAdapter = new CartSuggestionAdapter(hrvProductList, this, this);
        binding.cartProductSuggestion.cartSuggestionList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.cartProductSuggestion.cartSuggestionList.setAdapter(cartSuggestionAdapter);
    }

    @Override
    public void showM2UpdatedWarning() {
        CommonWarningDialog warningDialog = new CommonWarningDialog();
        warningDialog.setM2UpdatedWarningDialog(this, getResources().getString(R.string.text_new_prescription_needed_title), getResources().getString(R.string.text_new_prescription_needed_desc),
                getResources().getString(R.string.text_yes_subscription), getResources().getString(R.string.text_no_and_revert), M2UPDATE, null);
        getSupportFragmentManager().beginTransaction().add(warningDialog, "M2UpdationWarningDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void parentViewVisible(boolean isVisible) {
        binding.cartViewContent.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void subscriptionEditDialogue() {
        SubscriptionPopUpDialog subscriptionPopUpDialog = new SubscriptionPopUpDialog(this, true);
        getSupportFragmentManager().beginTransaction().add(subscriptionPopUpDialog, "SubscriptionPopUpDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void couponDescription(String couponDescription) {
        binding.tvPromoCodeDescription.setText(couponDescription);
    }

    @Override
    public void productViewVisible(boolean isVisible) {
        binding.cartProductView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void couponViewVisible(boolean isVisible) {
        binding.cvParentPromoCodeSuperCash.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void updateCartSubstitutionView(boolean isEnableCartSubstitutionView, boolean isAlternateCartForward, BigDecimal alternateCartSaveAndOriginalCartAmount) {
        binding.cvCartSubstitution.setVisibility(isEnableCartSubstitutionView ? View.VISIBLE : View.GONE);
        if (isEnableCartSubstitutionView) {
            binding.imgAlternateCart.setVisibility(isAlternateCartForward ? View.VISIBLE : View.GONE);
            binding.flAlternateCart.setVisibility(!isAlternateCartForward ? View.VISIBLE : View.INVISIBLE);
            binding.tvAlternateSaveAmount.setTextColor(ContextCompat.getColor(this, isAlternateCartForward ? R.color.colorGreen : R.color.colorDarkBlueGrey));
            binding.tvAlternateSaveAmount.setText(isAlternateCartForward ? String.format(getString(R.string.text_alternate_cart_save_amount), CommonUtils.getPriceInFormat(alternateCartSaveAndOriginalCartAmount)) : getString(R.string.text_revert_cart));
            binding.tvSaveAmountDesc.setText(String.format(getString(isAlternateCartForward ? R.string.text_alternate_cart_save_amount_desc : R.string.text_revert_cart_desc), CommonUtils.getPriceInFormat(alternateCartSaveAndOriginalCartAmount)));
        }
    }

    @Override
    public void onLaunchOfCartSubstitution() {
        GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_ALT_CART, GoogleAnalyticsHelper.EVENT_ACTION_ALTERNATE_CART_YOU_SAVE, GoogleAnalyticsHelper.POST_SCREEN_CART_PAGE);
        Intent intent = new Intent(this, AlternateCartActivity.class);
        intent.putExtra(IntentConstant.CART_SUBSTITUTION, new Gson().toJson(viewModel.getCartSubstitutionResponseModel()));
        startActivityForResult(intent, AppConstant.ALTERNATE_CART_REQUEST_CODE);
    }

    @Override
    public void showConfirmationDialogForRevertToOriginalCart() {
        CommonWarningDialog warningDialog = new CommonWarningDialog();
        warningDialog.setM2UpdatedWarningDialog(this, "", getString(R.string.text_revt_org_cart_conf_desc),
                getString(R.string.text_yes_subscription), getString(R.string.text_no), ALTERNATE_CART, null);
        getSupportFragmentManager().beginTransaction().add(warningDialog, "M2UpdationWarningDialog").commitNowAllowingStateLoss();
    }

    @Override
    public void disableProceedButton(boolean isOutOfStockProductInCart) {
        binding.cartProceedBtn.setBackground(getContext().getDrawable(!isOutOfStockProductInCart ? R.drawable.accent_button : R.drawable.grey_background_button));
        binding.cartProceedBtn.setTextColor(ContextCompat.getColor(getContext(), !isOutOfStockProductInCart ? R.color.colorPrimary : R.color.colorLightPaleBlueGrey));
        binding.cartProceedBtn.setEnabled(!isOutOfStockProductInCart);
        binding.cartProceedBtn.setClickable(!isOutOfStockProductInCart);
    }

    @Override
    public void showOutOfStockWarning(ArrayList<String> outOfStockProductNameList, Map<Integer, MStarProductDetails> outOfStockProductCodeMap) {
        CommonWarningDialog warningDialog = new CommonWarningDialog();
        boolean isRxProductAvailable = false;
        for (Map.Entry<Integer, MStarProductDetails> detailsEntry : outOfStockProductCodeMap.entrySet()) {
            if (!isRxProductAvailable) {
                isRxProductAvailable = detailsEntry.getValue().isRxRequired();
            } else {
                break;
            }
        }
        warningDialog.setM2UpdatedWarningDialog(this, getString(R.string.text_out_of_stock_warning), null,
                getString(R.string.text_remove_and_proceed), isRxProductAvailable ? getString(R.string.text_substitue) : "", OUT_OF_STOCK, outOfStockProductNameList);
        getSupportFragmentManager().beginTransaction().add(warningDialog, "OutOfStockWarningDialog").commitNowAllowingStateLoss();
        dismissLoader();
    }

    @Override
    public void showMaxLimitBreachedMessage() {
        CommonWarningDialog warningDialog = new CommonWarningDialog();
        //todo String.format(getString(R.string.text_max_limit_description), getMaxLimitWarningDescription())
        warningDialog.setM2UpdatedWarningDialog(this, getString(R.string.text_max_limit_warning), getString(R.string.text_max_limit_description_product), null, null, MAXLIMIT, null);
        getSupportFragmentManager().beginTransaction().add(warningDialog, "MaxLimitWarningDialog").commitNowAllowingStateLoss();
    }

    private String getMaxLimitWarningDescription() {
        StringBuilder productNameList = new StringBuilder();
        if (!RefillHelper.getMaxLimitBreachedProductsMap().isEmpty()) {
            for (String productCode : RefillHelper.getMaxLimitBreachedProductsMap().keySet()) {
                productNameList.append(",").append(RefillHelper.getMaxLimitBreachedProductsMap().get(productCode));
            }
        }
        return productNameList.toString().replaceFirst(",", "");
    }

    @Override
    public void promoCodeApplied(String message) {
        viewModel.initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
        Snackbar snackbar = Snackbar.make(binding.lvCartView, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.show();
    }

    @Override
    public void promoCodeRemoved(String message) {
        viewModel.initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
        Snackbar snackbar = Snackbar.make(binding.lvCartView, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.show();
    }

    @Override
    public void promoCodeAppliedFailed(String message) {
        viewModel.initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
        Snackbar snackbar = Snackbar.make(binding.lvCartView, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkBlueGrey));
        snackbar.show();
    }

    @Override
    public void setError(String error) {
        SnackBarHelper.snackBarCallBack(binding.lvCartView, this, error);
    }

    @Override
    public void applyMStarPromoCode(String promoCode) {
        viewModel.onApplyPromoCodeFromDialog(promoCode);
    }

    @Override
    public void unApplyMStarPromoCode(String removingPromoCode) {
        viewModel.unApplyPromoCode(removingPromoCode);
    }

    @Override
    public void promoCodeDialogClose() {
        isPromoCodeDialogOpen = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.cart_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (AppConstant.PRIME_REQUEST_CODE == requestCode) {
            viewModel.initiateAPICalls(APIServiceManager.MSTAR_GET_CART_DETAILS);
        } else if (AppConstant.ALTERNATE_CART_REQUEST_CODE == requestCode && RESULT_OK == resultCode && data != null &&
                data.hasExtra(IntentConstant.CART_DETAILS) && !TextUtils.isEmpty(data.getExtras().getString(IntentConstant.CART_DETAILS))) {
            MStarBasicResponseTemplateModel responseTemplateModel = new Gson().fromJson(data.getExtras().getString(IntentConstant.CART_DETAILS), MStarBasicResponseTemplateModel.class);
            MStarCartDetails cartDetails = responseTemplateModel.getResult().getCartDetails();
            WebEngageHelper.getInstance().updateAlternateCartEvent(cartDetails);
            isActivityResultCalled = true;
            PaymentHelper.setIsCartSwitched(true);
            viewModel.updateCartDetailsAfterCartUpdationFromAlternate(responseTemplateModel);
        }
    }

    @Override
    public void addAlternateProduct(int productToAdd, int quantity, int outOfStockProductCode) {
        viewModel.addAlternateProduct(productToAdd, quantity, outOfStockProductCode);
    }

    @Override
    public void onViewClosed(Integer outOfStockProductCode, boolean isJustDismiss) {
        if (isJustDismiss) {
            dismissLoader();
            disableProceedButton(false);
        } else {
            viewModel.outOfStockProductCodeMap.remove(outOfStockProductCode);
            viewModel.checkOutOfStockDetails();
        }
    }

    @Override
    public void addProductToCart(String id) {
        //to add suggested product in cart
        viewModel.increaseQuantity(Integer.valueOf(id), 1);
    }

    @Override
    public void redirectToProductPage(String sku) {
        Intent intent = new Intent(this, ProductDetail.class);
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, Integer.valueOf(sku));
        startActivity(intent);
    }

    @Override
    public void onPositiveButtonClicked(String from) {
        if (M2UPDATE.equalsIgnoreCase(from))
            viewModel.doM2Updation();
        else if (OUT_OF_STOCK.equalsIgnoreCase(from)) {
            showLoader();
            viewModel.removeAllOutOfStockProduct();
        } else if (ALTERNATE_CART.equalsIgnoreCase(from)) {
            GoogleAnalyticsHelper.getInstance().postEvent(this, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_ALT_CART, GoogleAnalyticsHelper.EVENT_ACTION_ALTERNATE_CART_REVERT_CART, GoogleAnalyticsHelper.POST_SCREEN_CART_PAGE);
            viewModel.initiateAPICalls(APIServiceManager.MSTAR_REVERT_ORIGINAL_CART);
        }
    }

    @Override
    public void onNegativeButtonClicked(String from) {
        if (M2UPDATE.equalsIgnoreCase(from))
            viewModel.getOriginalM2RetryCart();
        else if (OUT_OF_STOCK.equalsIgnoreCase(from))
            viewModel.startViewAlternateProcess();
    }

    @Override
    public void onDismissDialog(String from) {
        dismissLoader();
        disableProceedButton(false);
    }

    @Override
    public void subscriptionAdd() {
        SubscriptionHelper.getInstance().setSubscriptionThisIssue(AppConstant.THIS_FUTURE_ONLY);
        viewModel.proceedToNextPage();
    }

    @Override
    public void SubscriptionNo() {
        SubscriptionHelper.getInstance().setSubscriptionThisIssue(AppConstant.THIS_ISSUE_ONLY);
        viewModel.proceedToNextPage();


    }

    private class CartDetailsForPrimeObserver implements Observer<MStarCartDetails> {

        @Override
        public void onChanged(@Nullable MStarCartDetails mStarCartDetails) {
            if (mStarCartDetails.getLines() != null && !mStarCartDetails.getLines().isEmpty()) {
                viewModel.onMStarProductDetailsForPrimeAvailable(mStarCartDetails.getLines());
                binding.setViewModel(viewModel);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Google Analytics Post Screen*/
        GoogleAnalyticsHelper.getInstance().postScreen(this, M2Helper.getInstance().isM2Order() ? GoogleAnalyticsHelper.POST_SCREEN_CART_M2 : GoogleAnalyticsHelper.POST_SCREEN_CART);
    }
}
