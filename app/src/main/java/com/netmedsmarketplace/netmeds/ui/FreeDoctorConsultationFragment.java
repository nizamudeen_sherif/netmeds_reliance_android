package com.netmedsmarketplace.netmeds.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FragmentFreeDoctorConsultationBinding;
import com.nms.netmeds.base.BaseDialogFragment;

@SuppressLint("ValidFragment")
public class FreeDoctorConsultationFragment extends BaseDialogFragment {

    private boolean isFreeConsultation;
    private String sellerName = "";
    private String sellerAddress = "";
    private Context mContext;

    //Default Constructor
    public FreeDoctorConsultationFragment(boolean isFreeConsultation, Context context) {
        this.isFreeConsultation = isFreeConsultation;
        this.mContext = context;
    }

    public FreeDoctorConsultationFragment(boolean isFreeConsultation, String sellerName, Context context, String sellerAddress) {
        this.isFreeConsultation = isFreeConsultation;
        this.sellerName = sellerName;
        this.sellerAddress = sellerAddress;
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentFreeDoctorConsultationBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_free_doctor_consultation, container, false);
        binding.freeDoctorView.setVisibility(isFreeConsultation ? View.VISIBLE : View.GONE);
        binding.sellerAddress.setVisibility(isFreeConsultation ? View.GONE : View.VISIBLE);
        binding.sellerAddress.setText(!isFreeConsultation ? Html.fromHtml(sellerAddress) : "");
        if (mContext != null)
            binding.title.setText(isFreeConsultation ? mContext.getResources().getString(R.string.text_free_doctor_consultation) : sellerName);
        binding.closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return binding.getRoot();
    }
}
