package com.netmedsmarketplace.netmeds;

import com.netmedsmarketplace.netmeds.model.BoomrangResponse;
import com.netmedsmarketplace.netmeds.model.FAQCategoryCondentResponseModel;
import com.netmedsmarketplace.netmeds.model.FAQResponseModel;
import com.netmedsmarketplace.netmeds.model.LoginResponse;
import com.netmedsmarketplace.netmeds.model.OtpVerifyResponse;
import com.netmedsmarketplace.netmeds.model.SocialLoginResponse;
import com.netmedsmarketplace.netmeds.model.SocialRequest;
import com.netmedsmarketplace.netmeds.model.UpdateJusPayCustomerDetailsResponse;
import com.netmedsmarketplace.netmeds.model.VerifyUserResponse;
import com.netmedsmarketplace.netmeds.model.request.FAQCatgoryContentRequest;
import com.netmedsmarketplace.netmeds.model.request.OTPRequest;
import com.netmedsmarketplace.netmeds.model.request.ResetPasswordRequest;
import com.netmedsmarketplace.netmeds.model.request.VerifyUserRequest;
import com.nms.netmeds.base.model.GetCityStateFromPinCodeResponse;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.SendFcmTokenResponse;
import com.nms.netmeds.payment.ui.PaymentConstants;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RetrofitAppInterface {

    @POST(RestAPIPathConstant.VERIFY_USER)
    Call<VerifyUserResponse> verifyUser(@Body VerifyUserRequest verifyUserRequest);

    @POST(RestAPIPathConstant.SEND_OTP)
    Call<OtpVerifyResponse[]> sendOtp(@Body OTPRequest otpRequest);

    @POST(RestAPIPathConstant.SOCIAL_LOGIN)
    Call<SocialLoginResponse> socialLogin(@Body SocialRequest socialLoginRequest);

    @POST(RestAPIPathConstant.RESET)
    Call<LoginResponse> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);

    @GET("{pinCode}")
    Call<GetCityStateFromPinCodeResponse> getCityStateFromPinCode(@Path("pinCode") String pinCode);

    @POST(RestAPIPathConstant.FAQ_ALL_CATEGORY)
    Call<FAQResponseModel> getFAQData();

    @POST(RestAPIPathConstant.FAQ_CATEGORY_CONTENT)
    Call<FAQCategoryCondentResponseModel> getFAQCategoryContentData(@Body List<FAQCatgoryContentRequest> request);

    @FormUrlEncoded
    @POST()
    Call<UpdateJusPayCustomerDetailsResponse> updateJusPayCustomerDetails(@Field(PaymentConstants.MOBILE_NUMBER) String mobileNumber, @Field(PaymentConstants.EMAIL_ADDRESS) String email, @Field(PaymentConstants.FIRST_NAME) String firstName,
                                                                          @Field(PaymentConstants.LAST_NAME) String lastName, @Field(PaymentConstants.CUSTOMER_ID) String customerId, @Url String url);

    @POST(RestAPIPathConstant.PUSH_UN_REGISTER)
    Call<SendFcmTokenResponse> pushUnRegisterFromConsultation(@HeaderMap Map<String, String> header, @Body SendFcmTokenRequest request);

    @GET(RestAPIPathConstant.CLICK_POST)
    Call<Object> getClickPostData(@Query(value = "username") String userName, @Query(value = "key") String key, @Query(value = "cp_id") String id, @Query(value = "waybill") String trackNo);

    @GET(RestAPIPathConstant.BOOMRANG)
    Call<BoomrangResponse> getBoomrang(@HeaderMap Map<String, String> authorization, @Query("orderId") String categoryId);
}