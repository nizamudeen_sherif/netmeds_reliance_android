package com.netmedsmarketplace.netmeds;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConsultationNotification;
import com.nms.netmeds.base.utils.BasePreference;
import com.webengage.sdk.android.WebEngage;

import java.util.Map;

public class NetmedsFirebaseMessagingService extends FirebaseMessagingService {
    private static final String SOURCE_WEB_ENGAGE = "webengage";
    private static final String SOURCE_CONSULTATION = "CONSULT";
    private static final String KEY_NOTIFICATION = "notification";
    public static final String KEY_CONSULTATION_NOTIFICATION_DATA = "CONSULTATION_NOTIFICATION_DATA";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        WebEngage.get().setRegistrationID(s);
        /*Store fcm token in shared preference*/
        if (!TextUtils.isEmpty(s)) {
            BasePreference basePreference = BasePreference.getInstance(getApplicationContext());
            basePreference.setFcmToken(s);
            basePreference.setFcmTokenRefresh(true);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage != null && remoteMessage.getData() != null) {
            Map<String, String> data = remoteMessage.getData();
            if (data != null)
                checkNotificationFromWebEngageOrConsultation(data);
        }
    }

    private void checkNotificationFromWebEngageOrConsultation(Map<String, String> data) {
        if (data.containsKey("source")) {
            String value = !TextUtils.isEmpty(data.get("source")) ? data.get("source") : "";
            validateSource(value, data);
        }
    }

    private void validateSource(String value, Map<String, String> data) {
        if (!TextUtils.isEmpty(value)) {
            switch (value) {
                case SOURCE_WEB_ENGAGE:
                    WebEngage.get().receive(data);
                    break;
                case SOURCE_CONSULTATION:
                    showNotification(data);
                    break;
            }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void showNotification(Map<String, String> messageBody) {
        if (messageBody.size() > 0 && messageBody.containsKey(KEY_NOTIFICATION) && !TextUtils.isEmpty(messageBody.get(KEY_NOTIFICATION))) {
            ConsultationNotification consultationNotification = new Gson().fromJson(CommonUtils.isJSONValid(messageBody.get(KEY_NOTIFICATION)) ? messageBody.get(KEY_NOTIFICATION) : "", ConsultationNotification.class);

            Intent intent = new Intent(this, AppUriSchemeHandler.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(KEY_CONSULTATION_NOTIFICATION_DATA, consultationNotification);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            String notificationTitle = consultationNotification != null && !TextUtils.isEmpty(consultationNotification.getTitle()) ? consultationNotification.getTitle() : "";
            String message = consultationNotification != null && consultationNotification.getBody() != null && !TextUtils.isEmpty(consultationNotification.getBody().getMessage()) ? consultationNotification.getBody().getMessage() : "";

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(notificationTitle)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}
