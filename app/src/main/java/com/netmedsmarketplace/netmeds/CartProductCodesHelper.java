package com.netmedsmarketplace.netmeds;

import java.util.HashSet;
import java.util.Set;

public class CartProductCodesHelper {

    private Set<String> universalCartProductCodes;
    private Set<String> m2CartProductCodes;
    private static CartProductCodesHelper helper;

    public static  CartProductCodesHelper getInstance(){
        if(helper==null){
            helper = new CartProductCodesHelper();
        }
        return helper;
    }

    public Set<String> getUniversalCartProductCodes() {
        return universalCartProductCodes;
    }

    public void setUniversalCartProductCodes(Set<String> universalCartProductCodes) {
        this.universalCartProductCodes = universalCartProductCodes;
    }

    public Set<String> getM2CartProductCodes() {
        return m2CartProductCodes;
    }

    public void setM2CartProductCodes(Set<String> m2CartProductCodes) {
        this.m2CartProductCodes = m2CartProductCodes;
    }

    public static CartProductCodesHelper getHelper() {
        return helper;
    }

    public static void setHelper(CartProductCodesHelper helper) {
        CartProductCodesHelper.helper = helper;
    }
}
