package com.netmedsmarketplace.netmeds;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeRouter;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeUtil;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.chrometab.CustomTabActivityHelper;
import com.nms.netmeds.base.chrometab.shared.CustomTabsHelper;
import com.nms.netmeds.base.utils.DeepLinkConstant;

public class JavaScriptInterface {
    private Context context;
    private InterfaceCallback callback;

    /**
     * Instantiates a JavaScriptInterface.
     *
     * @param context the context
     */
    public JavaScriptInterface(Context context, InterfaceCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    /**
     * Launch the Product Detail page based on SKU ID
     */
    @android.webkit.JavascriptInterface
    public void launchProductDetail(String productCode) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.PRODUCT_DETAILS_NAVIGATION_PRODUCT_CODE, Integer.parseInt(productCode));
        LaunchIntentManager.routeToActivity(context.getResources().getString(R.string.route_product_detail), intent, context);
    }

    @android.webkit.JavascriptInterface
    public void launchURLDetail(String url) {
        if (!TextUtils.isEmpty(url)) {
            if (url.contains(DeepLinkConstant.EXTERNAL_BROWSER)) {
                url = url.replace(DeepLinkConstant.EXTERNAL_BROWSER, "");
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(CustomTabActivityHelper.getInstance().getSession());
                builder.setToolbarColor(ContextCompat.getColor(context, com.nms.netmeds.base.R.color.colorPrimary)).setShowTitle(true);
                CustomTabsIntent customTabsIntent = builder.build();
                CustomTabsHelper.addKeepAliveExtra(context, customTabsIntent.intent);
                customTabsIntent.launchUrl(context, Uri.parse(url));
            } else {
                try {
                    UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(Uri.parse(url)), context, false, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void launchExternalBrowser(String url) {
        if (!TextUtils.isEmpty(url)) {
            CommonUtils.launchChromeTab(url, (Activity) context);
        }
    }

    @android.webkit.JavascriptInterface
    public void launchUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            CommonUtils.activityNavigation(context, Uri.parse(url));
        }
    }

    @android.webkit.JavascriptInterface
    public void Addtocart(String productCode) {
        if (!TextUtils.isEmpty(productCode)) {
            callback.addToCart(productCode);
        }
    }

    public interface InterfaceCallback {
        void addToCart(String productCode);
    }
}
