package com.netmedsmarketplace.netmeds;

public class WellnessCatogoryConstants {

    /**
     * id
     */

    public static final String PRESCRIPTION_ID = "3";
    public static final String AYUSH_ID = "481";
    public static final String SPOTLIGHT_ID = "493";
    public static final String FITNESS_ID = "500";
    public static final String PERSONAL_CARE_ID = "524";
    public static final String FAMILY_CARE_ID = "575";
    public static final String TREATMENTS_ID = "624";
    public static final String DEVICES_ID = "665";
    public static final String LIFESTYLE_ID = "600";
    public static final String FAMILY_NUTRITION_ID = "501";
    public static final String AYURVEDIC_ID = "482";
    public static final String HOMEOPATHY_ID = "491";
    public static final String BABY_AND_INFANT_ID = "576";
    public static final String HAIR_CARE_ID = "547";
    public static final String SUPPLEMENTS_ID = "517";
    public static final String SEXUAL_WELLNESS_ID = "614";

    /**
     * imageUrl
     */

    public static final String PRESCRIPTION_IMAGE_URL = "/pub/menuicon/ic_prescription.png";
    public static final String AYUSH_IMAGE_URL = "/pub/menuicon/Icon_Ayush_64x64.png";
    public static final String SPOTLIGHT_IMAGE_URL = "/pub/menuicon/Icon_Spot_Light_64x64.png";
    public static final String FITNESS_IMAGE_URL = "/pub/menuicon/Icon_Fitness_64x64.png";
    public static final String PERSONAL_CARE_IMAGE_URL = "/pub/menuicon/Personal_tumb.png";
    public static final String FAMILY_CARE_IMAGE_URL = "/pub/menuicon/Icon_Family_Care_64x64.png";
    public static final String TREATMENTS_IMAGE_URL = "/pub/menuicon/Icon_Treatments_64x64.png";
    public static final String DEVICES_IMAGE_URL = "/pub/menuicon/Icon_Device_64x64.png";
    public static final String LIFESTYLE_IMAGE_URL = "/pub/menuicon/Icon_Life_Style_64x64.png";

    /**
     * pageType
     */

    public static final String PRESCRIPTION_PAGE_TYPE = "P";
    public static final String AYUSH_PAGE_TYPE = "O";
    public static final String SPOTLIGHT_PAGE_TYPE = "O";
    public static final String FITNESS_PAGE_TYPE = "O";
    public static final String PERSONAL_CARE_PAGE_TYPE = "O";
    public static final String FAMILY_CARE_PAGE_TYPE = "O";
    public static final String TREATMENTS_PAGE_TYPE = "O";
    public static final String DEVICES_PAGE_TYPE = "O";
    public static final String LIFESTYLE_PAGE_TYPE = "O";
}
