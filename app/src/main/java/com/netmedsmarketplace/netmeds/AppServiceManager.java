package com.netmedsmarketplace.netmeds;

import android.content.Context;

import androidx.annotation.NonNull;

import com.algolia.search.saas.AlgoliaException;
import com.algolia.search.saas.Client;
import com.algolia.search.saas.CompletionHandler;
import com.algolia.search.saas.Index;
import com.algolia.search.saas.Query;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.model.BoomrangResponse;
import com.netmedsmarketplace.netmeds.model.FAQCategoryCondentResponseModel;
import com.netmedsmarketplace.netmeds.model.FAQResponseModel;
import com.netmedsmarketplace.netmeds.model.LoginResponse;
import com.netmedsmarketplace.netmeds.model.OtpVerifyResponse;
import com.netmedsmarketplace.netmeds.model.SocialLoginResponse;
import com.netmedsmarketplace.netmeds.model.SocialRequest;
import com.netmedsmarketplace.netmeds.model.UpdateJusPayCustomerDetailsResponse;
import com.netmedsmarketplace.netmeds.model.VerifyUserResponse;
import com.netmedsmarketplace.netmeds.model.request.FAQCatgoryContentRequest;
import com.netmedsmarketplace.netmeds.model.request.OTPRequest;
import com.netmedsmarketplace.netmeds.model.request.ResetPasswordRequest;
import com.netmedsmarketplace.netmeds.model.request.VerifyUserRequest;
import com.netmedsmarketplace.netmeds.viewmodel.AccountViewModel;
import com.netmedsmarketplace.netmeds.viewmodel.AddOrUpdateAddressViewModel;
import com.netmedsmarketplace.netmeds.viewmodel.TrackOrderDetailsViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.AppViewModel;
import com.nms.netmeds.base.BaseServiceManager;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.GetCityStateFromPinCodeResponse;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.SendFcmTokenRequest;
import com.nms.netmeds.base.model.SendFcmTokenResponse;
import com.nms.netmeds.base.retrofit.BaseApiClient;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.payment.ui.PaymentConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppServiceManager extends BaseServiceManager {

    public static final int TRANSACTION_VERIFY_USER = 101;
    public static final int SOCIAL_LOGIN = 102;
    public static final int ALGOLIA_SEARCH = 104;
    public static final int SENT_OTP = 109;
    public static final int GET_CITY_STATE_FROM_PINCODE = 115;

    public static final int MOBILE_NO_UPDATE = 133;

    public static final int FAQ_ALL_CATEGORY = 157;
    public static final int FAQ_CATEGORY_CONTENT = 158;
    public static final int GET_PRODUCT_RECOMMENDATION_ENGINE = 165;
    public static final int UPDATE_JUS_PAY_CUSTOMER = 168;
    public static final int DOWNLOAD_INVOICE = 177;
    public static final int RESET_PASSWORD = 123;
    public static final int PUSH_UN_REGISTER = 172;
    public static final int CLICK_POST = 178;
    public static final int BRAINSINS_SUGGESTED_PRODUCTS = 179;
    public static final int BOOMERANG = 197;

    private static AppServiceManager appServiceManager;

    public static AppServiceManager getInstance() {
        if (appServiceManager == null)
            appServiceManager = new AppServiceManager();
        return appServiceManager;
    }

    public <T extends AppViewModel> void verifyUser(final T viewModel, VerifyUserRequest verifyUserRequest) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL)).create(RetrofitAppInterface.class);
        Call<VerifyUserResponse> call = apiService.verifyUser(verifyUserRequest);
        call.enqueue(new Callback<VerifyUserResponse>() {
            @Override
            public void onResponse(@NonNull Call<VerifyUserResponse> call, @NonNull Response<VerifyUserResponse> response) {
                if (response.isSuccessful()) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), TRANSACTION_VERIFY_USER);
                } else {
                    viewModel.onFailed(TRANSACTION_VERIFY_USER, errorHandling(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<VerifyUserResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(TRANSACTION_VERIFY_USER, null);

            }
        });
    }

    public <T extends AppViewModel> void socialLogin(final T viewModel, SocialRequest socialLoginRequest) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL)).create(RetrofitAppInterface.class);
        Call<SocialLoginResponse> call = apiService.socialLogin(socialLoginRequest);
        call.enqueue(new Callback<SocialLoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<SocialLoginResponse> call, @NonNull Response<SocialLoginResponse> response) {
                if (response.isSuccessful()) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), SOCIAL_LOGIN);
                } else {
                    viewModel.onFailed(SOCIAL_LOGIN, errorHandling(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<SocialLoginResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(SOCIAL_LOGIN, null);
            }
        });
    }

    public <T extends AppViewModel> void algoliaSearch(final T viewModel, String searchQuery, int page, int algoliaHitCount, String algoliaApiKey, String algoliaIndex, String algoliaAppId, boolean isFromSearch, JSONArray facetFilterQuery, String filterQuery) {
        Client client = new Client(algoliaAppId, algoliaApiKey);
        Index index = client.getIndex(algoliaIndex);
        Query query;
        if (isFromSearch) {
            query = new Query(searchQuery);
            query.setClickAnalytics(true);
            query.setAnalyticsTags(AppConstant.ANDROID);
        } else {
            query = new Query();
            query.setFacetFilters(facetFilterQuery);
        }
        query.setPage(page);
        query.setFacets("*");
        query.setHitsPerPage(algoliaHitCount);
        query.setFilters(filterQuery);
        index.searchAsync(query, new CompletionHandler() {
            @Override
            public void requestCompleted(JSONObject content, AlgoliaException error) {
                if (content == null) {
                    viewModel.onFailed(ALGOLIA_SEARCH, null);
                } else {
                    viewModel.onSyncData(content.toString(), ALGOLIA_SEARCH);
                }
            }
        });
    }

    public <T extends AppViewModel> void sendOtp(final T viewModel, OTPRequest otpRequest) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL)).create(RetrofitAppInterface.class);
        Call<OtpVerifyResponse[]> call = apiService.sendOtp(otpRequest);
        call.enqueue(new Callback<OtpVerifyResponse[]>() {
            @Override
            public void onResponse(@NonNull Call<OtpVerifyResponse[]> call, @NonNull Response<OtpVerifyResponse[]> response) {
                if (response.isSuccessful() && response.body() != null) {
                    for (OtpVerifyResponse otpVerifyResponse : response.body())
                        viewModel.onSyncData(new Gson().toJson(otpVerifyResponse), SENT_OTP);
                } else {
                    viewModel.onFailed(SENT_OTP, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<OtpVerifyResponse[]> call, @NonNull Throwable t) {
                viewModel.onFailed(SENT_OTP, new Gson().toJson(null));

            }
        });
    }

    public void getCityStateFromPinCode(final AddOrUpdateAddressViewModel viewModel, String pinCode, String url) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(url).create(RetrofitAppInterface.class);
        Call<GetCityStateFromPinCodeResponse> call = apiService.getCityStateFromPinCode(pinCode);
        call.enqueue(new Callback<GetCityStateFromPinCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCityStateFromPinCodeResponse> call, @NonNull Response<GetCityStateFromPinCodeResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), GET_CITY_STATE_FROM_PINCODE);
                } else {
                    viewModel.onFailed(GET_CITY_STATE_FROM_PINCODE, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCityStateFromPinCodeResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(GET_CITY_STATE_FROM_PINCODE, null);
            }
        });
    }

    public <T extends AppViewModel> void resetPassword(final T viewModel, ResetPasswordRequest resetPasswordRequest) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BASE_URL)).create(RetrofitAppInterface.class);
        Call<LoginResponse> call = apiService.resetPassword(resetPasswordRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), RESET_PASSWORD);
                } else {
                    viewModel.onFailed(RESET_PASSWORD, errorHandling(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(RESET_PASSWORD, new Gson().toJson(null));
            }
        });
    }

    public <T extends AppViewModel> void getFAQData(final T viewModel) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.FAQ_KAPTURE_CRM), ConfigMap.getInstance().getProperty(ConfigConstant.FAQ_ALL_CATEGORY_AUTH_TOKEN)).create(RetrofitAppInterface.class);
        Call<FAQResponseModel> call = apiService.getFAQData();
        call.enqueue(new Callback<FAQResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<FAQResponseModel> call, @NonNull Response<FAQResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), FAQ_ALL_CATEGORY);
                } else {
                    viewModel.onFailed(FAQ_ALL_CATEGORY, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<FAQResponseModel> call, @NonNull Throwable t) {
                viewModel.onFailed(FAQ_ALL_CATEGORY, null);
            }
        });
    }

    public <T extends AppViewModel> void getFAQCatrgoryContentData(final T viewModel, List<FAQCatgoryContentRequest> request) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().createService(ConfigMap.getInstance().getProperty(ConfigConstant.FAQ_KAPTURE_CRM), ConfigMap.getInstance().getProperty(ConfigConstant.FAQ_CATEGORY_CONTENT_AUTH_TOKEN)).create(RetrofitAppInterface.class);
        Call<FAQCategoryCondentResponseModel> call = apiService.getFAQCategoryContentData(request);
        call.enqueue(new Callback<FAQCategoryCondentResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<FAQCategoryCondentResponseModel> call, @NonNull Response<FAQCategoryCondentResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), FAQ_CATEGORY_CONTENT);
                } else {
                    viewModel.onFailed(FAQ_CATEGORY_CONTENT, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<FAQCategoryCondentResponseModel> call, @NonNull Throwable t) {
                viewModel.onFailed(FAQ_CATEGORY_CONTENT, null);
            }
        });
    }


    public void updateJusPayCustomerDetails(final AccountViewModel viewModel, HashMap<String, String> updateCustomerRequest, String url) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().dynamicClient().create(RetrofitAppInterface.class);
        Call<UpdateJusPayCustomerDetailsResponse> call = apiService.updateJusPayCustomerDetails(updateCustomerRequest.get(PaymentConstants.MOBILE_NUMBER), updateCustomerRequest.get(PaymentConstants.EMAIL_ADDRESS),
                updateCustomerRequest.get(PaymentConstants.FIRST_NAME), updateCustomerRequest.get(PaymentConstants.LAST_NAME), updateCustomerRequest.get(PaymentConstants.CUSTOMER_ID), url);
        call.enqueue(new Callback<UpdateJusPayCustomerDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateJusPayCustomerDetailsResponse> call, @NonNull Response<UpdateJusPayCustomerDetailsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), UPDATE_JUS_PAY_CUSTOMER);
                } else {
                    viewModel.onFailed(UPDATE_JUS_PAY_CUSTOMER, "");
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateJusPayCustomerDetailsResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(UPDATE_JUS_PAY_CUSTOMER, null);
            }
        });
    }

    public void pushUnRegisterFromConsultationServer(SendFcmTokenRequest sendFcmTokenRequest, final BasePreference basePreference) {
        JustDocUserResponse consultationLoginResult = new Gson().fromJson(basePreference.getJustDocUserResponse(), JustDocUserResponse.class);
        String justDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
        String sessionId = consultationLoginResult != null && consultationLoginResult.getSession() != null && consultationLoginResult.getSession().getId() != null ? consultationLoginResult.getSession().getId() : "";
        Map<String, String> header = new HashMap<>();
        header.put(AppConstant.KEY_AUTHORIZATION, AppConstant.KEY_BEARER + justDocToken);
        header.put(AppConstant.KEY_SESSION, sessionId);

        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.CONSULTATION_BASE_URL)).create(RetrofitAppInterface.class);
        Call<SendFcmTokenResponse> call = apiService.pushUnRegisterFromConsultation(header, sendFcmTokenRequest);
        call.enqueue(new Callback<SendFcmTokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<SendFcmTokenResponse> call, @NonNull Response<SendFcmTokenResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    basePreference.setFcmTokenRefresh(true);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SendFcmTokenResponse> call, @NonNull Throwable t) {
            }
        });
    }

    public void getClickPostDetails(final TrackOrderDetailsViewModel viewModel, String trackUrl, String clickPostId, String trackNo, Context context) {
        RetrofitAppInterface appInterface = BaseApiClient.getInstance().getClient(trackUrl).create(RetrofitAppInterface.class);
        Call<Object> call = appInterface.getClickPostData(ConfigMap.getInstance().getProperty(context.getString(R.string.click_post_username)), context.getString(R.string.click_post_key), clickPostId, trackNo);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), CLICK_POST);
                } else {
                    viewModel.onFailed(CLICK_POST, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                viewModel.onFailed(CLICK_POST, "");
            }
        });
    }

    public <T extends AppViewModel> void boomerang(final T viewModel, String orderId) {
        RetrofitAppInterface apiService = BaseApiClient.getInstance().getClient(ConfigMap.getInstance().getProperty(ConfigConstant.BOOMRANG_BASE_URL)).create(RetrofitAppInterface.class);
        Call<BoomrangResponse> call = apiService.getBoomrang(getAuthorization(), orderId);
        call.enqueue(new Callback<BoomrangResponse>() {
            @Override
            public void onResponse(@NonNull Call<BoomrangResponse> call, @NonNull Response<BoomrangResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    viewModel.onSyncData(new Gson().toJson(response.body()), BOOMERANG);
                } else {
                    viewModel.onFailed(BOOMERANG, new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<BoomrangResponse> call, @NonNull Throwable t) {
                viewModel.onFailed(BOOMERANG, null);
            }
        });
    }

    private Map<String, String> getAuthorization() {
        Map<String, String> header = new HashMap<>();
        header.put(AppConstant.BOOMRANG_AUTH_TOKEN, ConfigMap.getInstance().getProperty(ConfigConstant.BOOMRANG_AUTH_TOKEN));
        return header;
    }
}