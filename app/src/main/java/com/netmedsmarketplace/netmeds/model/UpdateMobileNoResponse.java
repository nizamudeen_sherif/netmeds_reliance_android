package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UpdateMobileNoResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private List<UpdateMobileNoStatusResponse> updateMobileNoStatusResponse = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public List<UpdateMobileNoStatusResponse> getUpdateMobileNoStatusResponse() {
        return updateMobileNoStatusResponse;
    }
}
