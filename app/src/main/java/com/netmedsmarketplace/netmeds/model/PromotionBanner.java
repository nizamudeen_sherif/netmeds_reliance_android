package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.BaseResponse;

import java.util.ArrayList;

public class PromotionBanner extends BaseResponse {

    @SerializedName("promotionBanners")
    private ArrayList<MstarBanner> promotionBanners = null;

    public ArrayList<MstarBanner> getPromotionBanners() {
        return promotionBanners;
    }

    public void setPromotionBanners(ArrayList<MstarBanner> promotionBanners) {
        this.promotionBanners = promotionBanners;
    }
}
