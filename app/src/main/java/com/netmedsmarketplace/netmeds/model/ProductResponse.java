package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class ProductResponse extends BaseResponse {

    @SerializedName("result")
    private ProductResult result;

    public ProductResult getResult() {
        return result;
    }

    public void setResult(ProductResult result) {
        this.result = result;
    }
}
