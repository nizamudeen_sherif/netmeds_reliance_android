/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

public class UpdateCartItem {
    @SerializedName("item_id")
    private String itemId;
    @SerializedName("qty")
    private String qty;
    @SerializedName("quote_id")
    private String quoteId;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }
}
