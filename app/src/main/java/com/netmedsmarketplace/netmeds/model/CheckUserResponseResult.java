package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class CheckUserResponseResult {
    @SerializedName("sessionToken")
    private String sessionToken;
    @SerializedName("appToken")
    private String appToken;
    @SerializedName("customer")
    private CheckUserDetail customer;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public CheckUserDetail getCustomer() {
        return customer;
    }

    public void setCustomer(CheckUserDetail customer) {
        this.customer = customer;
    }

}
