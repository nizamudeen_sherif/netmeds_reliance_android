package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class SocialLoginResponse extends BaseResponse {

    @SerializedName("updatedOn")
    private Integer updatedOn;
    @SerializedName("result")
    private SocialLoginResult result;

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Integer updatedOn) {
        this.updatedOn = updatedOn;
    }

    public SocialLoginResult getResult() {
        return result;
    }

    public void setResult(SocialLoginResult result) {
        this.result = result;
    }

}
