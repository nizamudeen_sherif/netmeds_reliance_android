package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class FilterOptionResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private FilterOptionData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FilterOptionData getResponseData() {
        return responseData;
    }

    public void setResponseData(FilterOptionData responseData) {
        this.responseData = responseData;
    }
}
