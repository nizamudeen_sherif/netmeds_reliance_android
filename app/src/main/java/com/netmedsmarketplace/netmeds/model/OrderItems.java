package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class OrderItems {
    @SerializedName("sku")
    private long sku;
    @SerializedName("brandName")
    private String brandName;

    public long getSku() {
        return sku;
    }

    public void setSku(long sku) {
        this.sku = sku;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
