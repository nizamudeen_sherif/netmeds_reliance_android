/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.model;

public class MstarAlgoliaFilterTitle {
    private String title;
    private String key;
    private int value;

    public MstarAlgoliaFilterTitle(String title, String key, int value) {
        this.title = title;
        this.key = key;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
