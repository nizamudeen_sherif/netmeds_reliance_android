package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class CustomerAddressResponse {

    @SerializedName("id")
    private Integer addressId;

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }
}
