package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

public class WalletHistoryResponse {
    @SerializedName("updatedOn")
    private Integer updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus status;
    @SerializedName("result")
    private WalletHistoryResponseData responseData;

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public WalletHistoryResponseData getResponseData() {
        return responseData;
    }
}
