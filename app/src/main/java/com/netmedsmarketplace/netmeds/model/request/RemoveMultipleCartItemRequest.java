package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RemoveMultipleCartItemRequest {
    @SerializedName("addparam")
    private ArrayList<String> addParamList;
    @SerializedName("rmparam")
    private ArrayList<RemoveItemId> removeItemIdList;
    @SerializedName("quoteId")
    private String quoteId;

    public ArrayList<String> getAddParamList() {
        return addParamList;
    }

    public void setAddParamList(ArrayList<String> addParamList) {
        this.addParamList = addParamList;
    }

    public ArrayList<RemoveItemId> getRemoveItemIdList() {
        return removeItemIdList;
    }

    public void setRemoveItemIdList(ArrayList<RemoveItemId> removeItemIdList) {
        this.removeItemIdList = removeItemIdList;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }
}
