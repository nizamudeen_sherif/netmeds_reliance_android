package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

public class OrderHeaderResponse {

    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private OrderHeaderResult result;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public OrderHeaderResult getResult() {
        return result;
    }
}
