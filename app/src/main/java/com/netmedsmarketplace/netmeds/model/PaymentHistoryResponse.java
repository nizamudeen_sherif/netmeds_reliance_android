package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class PaymentHistoryResponse extends BaseResponse {

    @SerializedName("result")
    private PaymentHistoryResponseResult result;

    public PaymentHistoryResponseResult getResult() {
        return result;
    }
}
