package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpVerifyRequest {
    @SerializedName("otp")
    private String otp;
    @SerializedName("phoneno")
    private String phoneno;
    @SerializedName("loginwithotp")
    private boolean loginwithotp;
    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public boolean isLoginwithotp() {
        return loginwithotp;
    }

    public void setLoginwithotp(boolean loginwithotp) {
        this.loginwithotp = loginwithotp;
    }
}
