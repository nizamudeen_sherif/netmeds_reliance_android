package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerNewAddressIdResponse {

    @SerializedName("addresses")
    public List<CustomerAddressResponse> addressResponseList;

    public List<CustomerAddressResponse> getAddressResponseList() {
        return addressResponseList;
    }

    public void setAddressResponseList(List<CustomerAddressResponse> addressResponseList) {
        this.addressResponseList = addressResponseList;
    }
}
