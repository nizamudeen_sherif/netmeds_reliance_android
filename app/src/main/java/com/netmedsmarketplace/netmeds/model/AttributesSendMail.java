package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class AttributesSendMail {
    @SerializedName("[NAME]")
    private String name;
    @SerializedName("[USERNAME]")
    private String userName;
    @SerializedName("[PASSWORD]")
    private String password;
    @SerializedName("[NEWPASSWORD]")
    private String newPassword;
    @SerializedName("[OLDPASSWORD]")
    private String oldPassword;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
