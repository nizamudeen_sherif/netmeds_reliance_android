package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class FAQResponseModel {

    @SerializedName("message")
    private FAQMessageResponse message;
    @SerializedName("status")
    private String status;

    public FAQMessageResponse getMessage() {
        return message;
    }

    public void setMessage(FAQMessageResponse message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}