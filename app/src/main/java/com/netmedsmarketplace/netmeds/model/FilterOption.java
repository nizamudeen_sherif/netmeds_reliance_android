package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class FilterOption {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("prodcutCount")
    private Integer productCount;
    private Boolean isChecked;

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }
}
