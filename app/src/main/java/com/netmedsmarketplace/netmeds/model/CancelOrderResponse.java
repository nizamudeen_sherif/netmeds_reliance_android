package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class CancelOrderResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private ResponseData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

}