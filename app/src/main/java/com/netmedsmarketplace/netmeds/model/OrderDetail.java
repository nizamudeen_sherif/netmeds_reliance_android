package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.DrugDetail;

import java.util.List;

public class OrderDetail {

    @SerializedName("orderId")
    private String orderId;
    @SerializedName("shipLocation")
    private String shipLocation;
    @SerializedName("drugStatus")
    private String drugStatus;
    @SerializedName("trackingNumber")
    private String trackingNumber;
    @SerializedName("trackingUrl")
    private String trackingUrl;
    @SerializedName("trackingCompany")
    private String trackingCompany;
    @SerializedName("clickPostId")
    private String clickPostId;
    @SerializedName("promisedDeliveryDate")
    private String promisedDeliveryDate;
    @SerializedName("drugDetails")
    private List<DrugDetail> drugDetails = null;
    @SerializedName("trackShortStatus")
    private String trackShortStatus;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getShipLocation() {
        return shipLocation;
    }

    public void setShipLocation(String shipLocation) {
        this.shipLocation = shipLocation;
    }

    public String getDrugStatus() {
        return drugStatus;
    }

    public void setDrugStatus(String drugStatus) {
        this.drugStatus = drugStatus;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getTrackingCompany() {
        return trackingCompany;
    }

    public void setTrackingCompany(String trackingCompany) {
        this.trackingCompany = trackingCompany;
    }

    public String getClickPostId() {
        return clickPostId;
    }

    public void setClickPostId(String clickPostId) {
        this.clickPostId = clickPostId;
    }

    public String getPromisedDeliveryDate() {
        return promisedDeliveryDate;
    }

    public void setPromisedDeliveryDate(String promisedDeliveryDate) {
        this.promisedDeliveryDate = promisedDeliveryDate;
    }

    public List<DrugDetail> getDrugDetails() {
        return drugDetails;
    }

    public void setDrugDetails(List<DrugDetail> drugDetails) {
        this.drugDetails = drugDetails;
    }

    public String getTrackShortStatus() {
        return trackShortStatus;
    }

    public void setTrackShortStatus(String trackShortStatus) {
        this.trackShortStatus = trackShortStatus;
    }

}
