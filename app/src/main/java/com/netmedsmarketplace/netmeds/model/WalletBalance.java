package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class WalletBalance {
    @SerializedName("NMSCASH")
    private double nmsCash;
    @SerializedName("NMSSUPERCASH")
    private double nmsSuperCash;
    @SerializedName("TOTALBALANCE")
    private double totalBalance;
    @SerializedName("NMSCASH_MSG")
    private String nmsCashMessage = "";
    @SerializedName("NMSSUPERCASH_MSG")
    private String nmsSuperCashMessage = "";

    public double getNmsCash() {
        return nmsCash;
    }

    public double getNmsSuperCash() {
        return nmsSuperCash;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public String getNmsCashMessage() {
        return nmsCashMessage;
    }

    public String getNmsSuperCashMessage() {
        return nmsSuperCashMessage;
    }
}
