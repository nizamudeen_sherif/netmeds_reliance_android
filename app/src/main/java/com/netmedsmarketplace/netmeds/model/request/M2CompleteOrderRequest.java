package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.AppConstant;

import java.util.List;

public class M2CompleteOrderRequest {
    @SerializedName("ship_postcode")
    private String shipPostcode;
    @SerializedName("bill_lasttname")
    private String billLasttname;
    @SerializedName("image_source_name")
    private List<String> imageSourceName = null;
    @SerializedName("ship_country_id")
    private String shipCountryId;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("ship_firstname")
    private String shipFirstname;
    @SerializedName("customer_billingaddress_id")
    private Integer customerBillingaddressId;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("bill_country_id")
    private String billCountryId;
    @SerializedName("ship_street")
    private String shipStreet;
    @SerializedName("items")
    private List<M2LineItems> items;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("ship_lasttname")
    private String shipLasttname;
    @SerializedName("ship_city")
    private String shipCity;
    @SerializedName("coupon_code")
    private String couponCode;
    @SerializedName("ship_telephone")
    private String shipTelephone;
    @SerializedName("bill_telephone")
    private String billTelephone;
    @SerializedName("bill_street")
    private String billStreet;
    @SerializedName("bill_city")
    private String billCity;
    @SerializedName("ship_region")
    private String shipRegion;
    @SerializedName("bill_region")
    private String billRegion;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("bill_firstname")
    private String billFirstname;
    @SerializedName("bill_postcode")
    private String billPostcode;
    @SerializedName("customer_shippingaddress_id")
    private Integer customerShippingaddressId;
    @SerializedName("source_name")
    private String sourceName = AppConstant.SOURCE_NAME;

    public String getShipPostcode() {
        return shipPostcode;
    }

    public void setShipPostcode(String shipPostcode) {
        this.shipPostcode = shipPostcode;
    }

    public String getBillLasttname() {
        return billLasttname;
    }

    public void setBillLasttname(String billLasttname) {
        this.billLasttname = billLasttname;
    }

    public List<String> getImageSourceName() {
        return imageSourceName;
    }

    public void setImageSourceName(List<String> imageSourceName) {
        this.imageSourceName = imageSourceName;
    }

    public String getShipCountryId() {
        return shipCountryId;
    }

    public void setShipCountryId(String shipCountryId) {
        this.shipCountryId = shipCountryId;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getShipFirstname() {
        return shipFirstname;
    }

    public void setShipFirstname(String shipFirstname) {
        this.shipFirstname = shipFirstname;
    }

    public Integer getCustomerBillingaddressId() {
        return customerBillingaddressId;
    }

    public void setCustomerBillingaddressId(Integer customerBillingaddressId) {
        this.customerBillingaddressId = customerBillingaddressId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBillCountryId() {
        return billCountryId;
    }

    public void setBillCountryId(String billCountryId) {
        this.billCountryId = billCountryId;
    }

    public String getShipStreet() {
        return shipStreet;
    }

    public void setShipStreet(String shipStreet) {
        this.shipStreet = shipStreet;
    }

    public List<M2LineItems> getItems() {
        return items;
    }

    public void setItems(List<M2LineItems> items) {
        this.items = items;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getShipLasttname() {
        return shipLasttname;
    }

    public void setShipLasttname(String shipLasttname) {
        this.shipLasttname = shipLasttname;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getShipTelephone() {
        return shipTelephone;
    }

    public void setShipTelephone(String shipTelephone) {
        this.shipTelephone = shipTelephone;
    }

    public String getBillTelephone() {
        return billTelephone;
    }

    public void setBillTelephone(String billTelephone) {
        this.billTelephone = billTelephone;
    }

    public String getBillStreet() {
        return billStreet;
    }

    public void setBillStreet(String billStreet) {
        this.billStreet = billStreet;
    }

    public String getBillCity() {
        return billCity;
    }

    public void setBillCity(String billCity) {
        this.billCity = billCity;
    }

    public String getShipRegion() {
        return shipRegion;
    }

    public void setShipRegion(String shipRegion) {
        this.shipRegion = shipRegion;
    }

    public String getBillRegion() {
        return billRegion;
    }

    public void setBillRegion(String billRegion) {
        this.billRegion = billRegion;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBillFirstname() {
        return billFirstname;
    }

    public void setBillFirstname(String billFirstname) {
        this.billFirstname = billFirstname;
    }

    public String getBillPostcode() {
        return billPostcode;
    }

    public void setBillPostcode(String billPostcode) {
        this.billPostcode = billPostcode;
    }

    public Integer getCustomerShippingaddressId() {
        return customerShippingaddressId;
    }

    public void setCustomerShippingaddressId(Integer customerShippingaddressId) {
        this.customerShippingaddressId = customerShippingaddressId;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
}
