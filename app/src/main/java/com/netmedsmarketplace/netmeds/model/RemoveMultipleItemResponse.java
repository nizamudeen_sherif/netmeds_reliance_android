package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class RemoveMultipleItemResponse extends BaseResponse {
    @SerializedName("result")
    private String removeItemResult;

    public String getRemoveItemResult() {
        return removeItemResult;
    }

    public void setRemoveItemResult(String removeItemResult) {
        this.removeItemResult = removeItemResult;
    }
}
