package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class IncompleteOrderResult {
    @SerializedName("orderId")
    private IncompleteOrderId orderId;
    @SerializedName("Message")
    private String message;

    public IncompleteOrderId getOrderId() {
        return orderId;
    }

    public void setOrderId(IncompleteOrderId orderId) {
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
