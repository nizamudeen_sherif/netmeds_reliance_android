package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class IncompleteOrderDetailResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private IncompleteOrderItemData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public IncompleteOrderItemData getResponseData() {
        return responseData;
    }

    public void setResponseData(IncompleteOrderItemData responseData) {
        this.responseData = responseData;
    }
}
