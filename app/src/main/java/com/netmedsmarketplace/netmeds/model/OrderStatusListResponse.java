package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class OrderStatusListResponse {

    @SerializedName("value")
    private String value;
    @SerializedName("title")
    private String title;
    @SerializedName("count")
    private String count;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
