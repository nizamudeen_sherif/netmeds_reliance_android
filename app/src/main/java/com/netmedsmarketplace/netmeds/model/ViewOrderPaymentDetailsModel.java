package com.netmedsmarketplace.netmeds.model;

public class ViewOrderPaymentDetailsModel {

    private String label;
    private String value;
    private int background;
    private String strikeValue;
    private boolean isStrikeValueVisible;
    private boolean isDeliveryChargeDescriptionVisibility;


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getStrikeValue() {
        return strikeValue;
    }

    public void setStrikeValue(String strikeValue) {
        this.strikeValue = strikeValue;
    }

    public boolean isStrikeValueVisible() {
        return isStrikeValueVisible;
    }

    public void setStrikeValueVisible(boolean strikeValueVisible) {
        isStrikeValueVisible = strikeValueVisible;
    }

    public boolean isDeliveryChargeDescriptionVisibility() {
        return isDeliveryChargeDescriptionVisibility;
    }

    public void setDeliveryChargeDescriptionVisibility(boolean deliveryChargeDescriptionVisibility) {
        isDeliveryChargeDescriptionVisibility = deliveryChargeDescriptionVisibility;
    }
}
