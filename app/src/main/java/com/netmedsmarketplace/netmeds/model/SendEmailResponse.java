package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class SendEmailResponse extends BaseResponse {

    @SerializedName("Providercode")
    private String providercode;
    @SerializedName("Status")
    private Boolean status;
    @SerializedName("Message")
    private String message;
    @SerializedName("MessageCode")
    private String messageCode;
    @SerializedName("Result")
    private String result;

    public String getProvidercode() {
        return providercode;
    }

    public void setProvidercode(String providercode) {
        this.providercode = providercode;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
