package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneno")
    private String phoneno;
    @SerializedName("message")
    private String message;
    @SerializedName("customerStatus")
    private Boolean customerStatus;
    @SerializedName("lastname")
    private String lastname;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(Boolean customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
