package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class M2CompleteOrderResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("content")
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
