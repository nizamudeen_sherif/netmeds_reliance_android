package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class IncompleteOrderDetailRequest {
    @SerializedName("OrderId")
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
