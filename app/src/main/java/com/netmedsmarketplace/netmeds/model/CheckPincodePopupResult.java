package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class CheckPincodePopupResult {
    @SerializedName("cod_supported")
    private Boolean codSupported;
    @SerializedName("cold_storage_delivery_supported")
    private Boolean coldStorageDeliverySupported;

    public Boolean getCodSupported() {
        return codSupported;
    }

    public void setCodSupported(Boolean codSupported) {
        this.codSupported = codSupported;
    }

    public Boolean getColdStorageDeliverySupported() {
        return coldStorageDeliverySupported;
    }

    public void setColdStorageDeliverySupported(Boolean coldStorageDeliverySupported) {
        this.coldStorageDeliverySupported = coldStorageDeliverySupported;
    }

}
