package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyPrescriptionResponseResult {

    @SerializedName("rxCount")
    private int rxCount;
    @SerializedName("rxList")
    private List<RxPrescription> rxList;

    public int getRxCount() {
        return rxCount;
    }

    public List<RxPrescription> getRxList() {
        return rxList;
    }

    public void setRxCount(int rxCount) {
        this.rxCount = rxCount;
    }

    public void setRxList(List<RxPrescription> rxList) {
        this.rxList = rxList;
    }
}
