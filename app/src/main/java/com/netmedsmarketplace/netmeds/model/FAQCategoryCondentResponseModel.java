package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class FAQCategoryCondentResponseModel {

    @SerializedName("message")
    private FAQMessageContentModel message;
    @SerializedName("status")
    private String status;

    public FAQMessageContentModel getMessage() {
        return message;
    }

    public void setMessage(FAQMessageContentModel message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}