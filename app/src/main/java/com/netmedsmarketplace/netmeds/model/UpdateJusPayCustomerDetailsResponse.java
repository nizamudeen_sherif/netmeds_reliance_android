package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class UpdateJusPayCustomerDetailsResponse {
    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
