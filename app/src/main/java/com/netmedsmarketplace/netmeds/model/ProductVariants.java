package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductVariants {
    @SerializedName("types")
    private List<VariantType> types = null;
    @SerializedName("ageGroup")
    private List<String> ageGroup = null;
    @SerializedName("size")
    private List<String> size = null;
    @SerializedName("color")
    private List<String> color = null;
    @SerializedName("flavour")
    private List<String> flavour = null;
    @SerializedName("units")
    private List<String> units = null;
    @SerializedName("productMapping")
    private List<ProductMapping> productMapping = null;

    public List<VariantType> getTypes() {
        return types;
    }

    public void setTypes(List<VariantType> types) {
        this.types = types;
    }

    public List<String> getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(List<String> ageGroup) {
        this.ageGroup = ageGroup;
    }

    public List<String> getSize() {
        return size;
    }

    public void setSize(List<String> size) {
        this.size = size;
    }

    public List<String> getColor() {
        return color;
    }

    public void setColor(List<String> color) {
        this.color = color;
    }

    public List<String> getFlavour() {
        return flavour;
    }

    public void setFlavour(List<String> flavour) {
        this.flavour = flavour;
    }

    public List<String> getUnits() {
        return units;
    }

    public void setUnits(List<String> units) {
        this.units = units;
    }

    public List<ProductMapping> getProductMapping() {
        return productMapping;
    }

    public void setProductMapping(List<ProductMapping> productMapping) {
        this.productMapping = productMapping;
    }
}
