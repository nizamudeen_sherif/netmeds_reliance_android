package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.HRVProduct;

import java.util.ArrayList;

public class RelatedResult {
    @SerializedName("items")
    private ArrayList<HRVProduct> list = null;

    @SerializedName("count")
    private int count;
    @SerializedName("totalcount")
    private int totalcount;

    public ArrayList<HRVProduct> getList() {
        return list;
    }

    public void setList(ArrayList<HRVProduct> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }
}
