package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class ProductDetailResponse extends BaseResponse {

    @SerializedName("result")
    private ProductListResult result;

    public ProductListResult getResult() {
        return result;
    }

    public void setResult(ProductListResult result) {
        this.result = result;
    }
}
