package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class IncompleteOrderId {

    @SerializedName("orderId")
    private String orderId;
    @SerializedName("date")
    private String date;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
