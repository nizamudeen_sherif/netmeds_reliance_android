package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;
import com.nms.netmeds.base.model.PromoCodeList;

import java.util.List;

public class PromoCodeResponse extends BaseResponse {

    @SerializedName("updatedOn")
    private Integer updatedOn;
    @SerializedName("result")
    private List<PromoCodeList> result = null;

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Integer updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<PromoCodeList> getResult() {
        return result;
    }

    public void setResult(List<PromoCodeList> result) {
        this.result = result;
    }
}
