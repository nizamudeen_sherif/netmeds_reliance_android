package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class HealthArticleResponse extends BaseResponse {

    @SerializedName("result")
    private HealthArticleResult result;

    public HealthArticleResult getResult() {
        return result;
    }

    public void setResult(HealthArticleResult result) {
        this.result = result;
    }
}
