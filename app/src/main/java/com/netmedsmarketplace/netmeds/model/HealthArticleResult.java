package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ArticleList;

import java.util.List;

public class HealthArticleResult {
    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("description")
    private String description;

    @SerializedName("articleList")
    private List<ArticleList> result = null;

    public List<ArticleList> getResult() {
        return result;
    }

    public void setResult(List<ArticleList> result) {
        this.result = result;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
