package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

public class WellnessHealthConcernResponse {

    @SerializedName("updatedOn")
    private Long updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private WellnessHealthConcernResponseResult result;

    public Long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public WellnessHealthConcernResponseResult getResult() {
        return result;
    }

    public void setResult(WellnessHealthConcernResponseResult result) {
        this.result = result;
    }
}
