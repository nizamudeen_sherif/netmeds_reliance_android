package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.CustomerResponse;

public class PutCustomerDetailsRequest {

    @SerializedName("customer")
    private CustomerResponse customerDetailsData;

    public CustomerResponse getCustomerDetailsData() {
        return customerDetailsData;
    }

    public void setCustomerDetailsData(CustomerResponse customerDetailsData) {
        this.customerDetailsData = customerDetailsData;
    }
}
