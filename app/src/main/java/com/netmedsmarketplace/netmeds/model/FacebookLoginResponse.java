package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseRequest;

import java.util.List;

public class FacebookLoginResponse extends BaseRequest {

    @SerializedName("applicationId")
    @Expose
    private String applicationId;
    @SerializedName("dataAccessExpirationTime")
    @Expose
    private String dataAccessExpirationTime;
    @SerializedName("declinedPermissions")
    @Expose
    private List<Object> declinedPermissions = null;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("lastRefresh")
    @Expose
    private String lastRefresh;
    @SerializedName("permissions")
    @Expose
    private List<String> permissions = null;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getDataAccessExpirationTime() {
        return dataAccessExpirationTime;
    }

    public void setDataAccessExpirationTime(String dataAccessExpirationTime) {
        this.dataAccessExpirationTime = dataAccessExpirationTime;
    }

    public List<Object> getDeclinedPermissions() {
        return declinedPermissions;
    }

    public void setDeclinedPermissions(List<Object> declinedPermissions) {
        this.declinedPermissions = declinedPermissions;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getLastRefresh() {
        return lastRefresh;
    }

    public void setLastRefresh(String lastRefresh) {
        this.lastRefresh = lastRefresh;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
