package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class ProductDetailResult {

    @SerializedName("shortDescription")
    private String shortDescription;
    @SerializedName("content")
    private String content;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
