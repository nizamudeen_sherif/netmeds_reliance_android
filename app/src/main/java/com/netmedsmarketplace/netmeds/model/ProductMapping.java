package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class ProductMapping {
    @SerializedName("id")
    private String id;
    @SerializedName("sku")
    private String sku;
    @SerializedName("brandName")
    private String brandName;
    @SerializedName("price")
    private String price;
    @SerializedName("color")
    private String color;
    @SerializedName("size")
    private String size;
    @SerializedName("flavour")
    private String flavour;
    @SerializedName("units")
    private String units;
    @SerializedName("groupName")
    private String groupName;
    @SerializedName("groupId")
    private String groupId;
    @SerializedName("ageGroup")
    private String ageGroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFlavour() {
        return flavour;
    }

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }
}
