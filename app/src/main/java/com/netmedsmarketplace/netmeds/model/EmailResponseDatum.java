package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class EmailResponseDatum {

    @SerializedName("customerStatus")
    private Boolean customerStatus;
    @SerializedName("message")
    private String message;

    public Boolean getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(Boolean customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
