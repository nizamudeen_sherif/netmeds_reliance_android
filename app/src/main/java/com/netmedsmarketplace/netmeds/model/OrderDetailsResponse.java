package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.ServiceStatus;

public class OrderDetailsResponse {

    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("serviceStatus")
    private ServiceStatus serviceStatus;
    @SerializedName("result")
    private OrderDetailsResult result;

    public String getUpdatedOn() {
        return updatedOn;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public OrderDetailsResult getResult() {
        return result;
    }
}
