package com.netmedsmarketplace.netmeds.model;

public class ReviewOrderItems {

    private String drugName;
    private boolean isDrugSelected;

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public boolean isDrugSelected() {
        return isDrugSelected;
    }

    public void setDrugSelected(boolean drugSelected) {
        isDrugSelected = drugSelected;
    }
}
