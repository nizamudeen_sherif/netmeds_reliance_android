package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WalletHistoryResponseData {

    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String message = "";
    @SerializedName("walletbalance")
    private WalletBalance walletBalance = new WalletBalance();
    @SerializedName("result")
    private List<WalletTransactionResult> resultList = new ArrayList<>();

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WalletBalance getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(WalletBalance walletBalance) {
        this.walletBalance = walletBalance;
    }

    public List<WalletTransactionResult> getResultList() {
        return resultList;
    }

    public void setResultList(List<WalletTransactionResult> resultList) {
        this.resultList = resultList;
    }
}
