package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class VerifyUserResponse extends BaseResponse {
    @SerializedName("result")
    private VerifyUserResult result;

    public VerifyUserResult getResult() {
        return result;
    }

    public void setResult(VerifyUserResult result) {
        this.result = result;
    }
}
