/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

public class UpdateCartItemRequest {
    @SerializedName("cart_item")
    private UpdateCartItem cartItem;

    public UpdateCartItem getCartItem() {
        return cartItem;
    }

    public void setCartItem(UpdateCartItem cartItem) {
        this.cartItem = cartItem;
    }

}
