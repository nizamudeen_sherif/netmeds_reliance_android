package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class PromotionBannerResponse extends BaseResponse {

    @SerializedName("result")
    private PromotionBanner result;

    public PromotionBanner getResult() {
        return result;
    }

    public void setResult(PromotionBanner result) {
        this.result = result;
    }
}
