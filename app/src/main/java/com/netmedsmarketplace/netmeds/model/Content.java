package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class Content {
    @SerializedName("id")
    private long id;
    @SerializedName("title")
    private String title;
    @SerializedName("detail")
    private String detail;
    @SerializedName("type")
    private String type;
    @SerializedName("categoryId")
    private long categoryId;
    @SerializedName("categoryName")
    private String categoryName;
    @SerializedName("categoryPath")
    private String categoryPath;
    @SerializedName("enabled")
    private String enabled;
    @SerializedName("createDate")
    private String createDate;
    @SerializedName("attachmentDetail")
    private String attachmentDetail;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getAttachmentDetail() {
        return attachmentDetail;
    }

    public void setAttachmentDetail(String attachmentDetail) {
        this.attachmentDetail = attachmentDetail;
    }

}
