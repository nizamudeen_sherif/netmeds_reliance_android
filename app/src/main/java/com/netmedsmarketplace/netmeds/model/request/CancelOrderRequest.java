package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

public class CancelOrderRequest {

    @SerializedName("Orderid")
    private String orderid;
    @SerializedName("reason")
    private String reason;
    @SerializedName("customermsg")
    private String customermsg;
    @SerializedName("mobileno")
    private String mobileno;
    @SerializedName("cod")
    private String cod;
    @SerializedName("Emailid")
    private String emailid;
    @SerializedName("name")
    private String name;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCustomermsg() {
        return customermsg;
    }

    public void setCustomermsg(String customermsg) {
        this.customermsg = customermsg;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}