package com.netmedsmarketplace.netmeds.model;

import android.text.SpannableString;

public class AlternateCartHeaderModel {

    private String firstValue;
    private String thirdValue;
    private String medicine1Image;
    private String medicine2Image;
    private SpannableString secondDescription;
    private String thirdDescription;
    private boolean isPositionOneVisible;
    private boolean isPositionTwoVisible;
    private boolean isPositionThreeVisible;

    public String getFirstValue() {
        return firstValue;
    }

    public void setFirstValue(String firstValue) {
        this.firstValue = firstValue;
    }

    public String getThirdValue() {
        return thirdValue;
    }

    public void setThirdValue(String thirdValue) {
        this.thirdValue = thirdValue;
    }

    public SpannableString getSecondDescription() {
        return secondDescription;
    }

    public void setSecondDescription(SpannableString secondDescription) {
        this.secondDescription = secondDescription;
    }

    public String getThirdDescription() {
        return thirdDescription;
    }

    public void setThirdDescription(String thirdDescription) {
        this.thirdDescription = thirdDescription;
    }

    public boolean isPositionOneVisible() {
        return isPositionOneVisible;
    }

    public void setPositionOneVisible(boolean positionOneVisible) {
        isPositionOneVisible = positionOneVisible;
    }

    public boolean isPositionTwoVisible() {
        return isPositionTwoVisible;
    }

    public void setPositionTwoVisible(boolean positionTwoVisible) {
        isPositionTwoVisible = positionTwoVisible;
    }

    public boolean isPositionThreeVisible() {
        return isPositionThreeVisible;
    }

    public void setPositionThreeVisible(boolean positionThreeVisible) {
        isPositionThreeVisible = positionThreeVisible;
    }

    public String getMedicine1Image() {
        return medicine1Image;
    }

    public void setMedicine1Image(String medicine1Image) {
        this.medicine1Image = medicine1Image;
    }

    public String getMedicine2Image() {
        return medicine2Image;
    }

    public void setMedicine2Image(String medicine2Image) {
        this.medicine2Image = medicine2Image;
    }
}
