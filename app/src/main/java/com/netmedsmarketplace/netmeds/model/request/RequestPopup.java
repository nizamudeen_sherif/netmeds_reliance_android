package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseRequest;

public class RequestPopup extends BaseRequest {

    @SerializedName("patientname")
    private String patientname;
    @SerializedName("customerphone")
    private String customerphone;
    @SerializedName("productname")
    private String productname;

    public String getPatientname() {
        return patientname;
    }

    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

    public String getCustomerphone() {
        return customerphone;
    }

    public void setCustomerphone(String customerphone) {
        this.customerphone = customerphone;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }
}
