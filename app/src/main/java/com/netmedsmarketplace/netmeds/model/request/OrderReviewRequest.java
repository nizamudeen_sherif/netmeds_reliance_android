package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderReviewRequest {
    @SerializedName("id")
    private String id;
    @SerializedName("user")
    private String user;
    @SerializedName("locationId")
    private String locationId;
    @SerializedName("responseDateTime")
    private String responseDateTime;
    @SerializedName("responses")
    private List<ReviewResponse> responses = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getResponseDateTime() {
        return responseDateTime;
    }

    public void setResponseDateTime(String responseDateTime) {
        this.responseDateTime = responseDateTime;
    }

    public List<ReviewResponse> getResponses() {
        return responses;
    }

    public void setResponses(List<ReviewResponse> responses) {
        this.responses = responses;
    }
}
