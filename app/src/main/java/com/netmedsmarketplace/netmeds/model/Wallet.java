package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class Wallet {

    @SerializedName("sequence")
    private int sequence;
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("offer")
    private String offer;
    @SerializedName("enabled")
    private boolean enabled;
    @SerializedName("image")
    private String image;
    @SerializedName("token")
    private String token;
    @SerializedName("currentBalance")
    private double currentBalance;
    @SerializedName("linked")
    private boolean linked;
    @SerializedName("cardType")
    private String cardType;
    @SerializedName("cardBrand")
    private String cardBrand;
    @SerializedName("linkToken")
    private String linkToken;

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public boolean isLinked() {
        return linked;
    }

    public void setLinked(boolean linked) {
        this.linked = linked;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getLinkToken() {
        return linkToken;
    }

    public void setLinkToken(String linkToken) {
        this.linkToken = linkToken;
    }

}