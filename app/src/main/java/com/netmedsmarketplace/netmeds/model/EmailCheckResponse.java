package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

import java.util.List;

public class EmailCheckResponse extends BaseResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private List<EmailResponseDatum> responseData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<EmailResponseDatum> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<EmailResponseDatum> responseData) {
        this.responseData = responseData;
    }

}
