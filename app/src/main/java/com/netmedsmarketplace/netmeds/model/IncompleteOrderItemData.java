package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.MethodTwoAddress;

import java.util.List;

public class IncompleteOrderItemData {
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("status")
    private String status;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("emailId")
    private String emailId;
    @SerializedName("customerDob")
    private String customerDob;
    @SerializedName("mobileNo")
    private String mobileNo;
    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("coupon_code")
    private String couponCode;
    @SerializedName("prescriptions")
    private List<String> prescriptions = null;
    @SerializedName("Product")
    private List<IncompleteOrderDetailProduct> product = null;
    @SerializedName("getBillingAddress")
    private MethodTwoAddress getBillingAddress;
    @SerializedName("getShippingAddress")
    private MethodTwoAddress getShippingAddress;
    @SerializedName("voucher_code")
    private String voucherCode;
    @SerializedName("nms_cash")
    private String nmsCash;
    @SerializedName("nms_supercash")
    private String nmsSuperCash;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCustomerDob() {
        return customerDob;
    }

    public void setCustomerDob(String customerDob) {
        this.customerDob = customerDob;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public List<String> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<String> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public List<IncompleteOrderDetailProduct> getProduct() {
        return product;
    }

    public void setProduct(List<IncompleteOrderDetailProduct> product) {
        this.product = product;
    }

    public MethodTwoAddress getGetBillingAddress() {
        return getBillingAddress;
    }

    public void setGetBillingAddress(MethodTwoAddress getBillingAddress) {
        this.getBillingAddress = getBillingAddress;
    }

    public MethodTwoAddress getGetShippingAddress() {
        return getShippingAddress;
    }

    public void setGetShippinggAddress(MethodTwoAddress getShippingAddress) {
        this.getShippingAddress = getShippingAddress;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getNmsCash() {
        return nmsCash;
    }

    public void setNmsCash(String nmsCash) {
        this.nmsCash = nmsCash;
    }

    public String getNmsSuperCash() {
        return nmsSuperCash;
    }

    public void setNmsSuperCash(String nmsSuperCash) {
        this.nmsSuperCash = nmsCash;
    }
}
