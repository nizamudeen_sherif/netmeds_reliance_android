package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderListResponse {

    @SerializedName("orderId")
    private String orderId;
    @SerializedName("name")
    private String name;
    @SerializedName("purchasedDate")
    private String purchasedDate;
    @SerializedName("orderAmount")
    private double orderAmount;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("statusDescription")
    private String statusDescription;
    @SerializedName("statusColor")
    private String statusColor;
    @SerializedName("item")
    private List<String> item = null;
    @SerializedName("items")
    private List<OrderItems> itemList = new ArrayList<>();
    @SerializedName("itemCount")
    private long itemCount;
    @SerializedName("isPrimeMembershipOrder")
    private boolean primeMembershipOrder;
    @SerializedName("isPrimeCustomerOrder")
    private boolean primeCustomerOrder;
    @SerializedName("isTempOrder")
    private boolean isTempOrder = false;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public List<String> getItem() {
        return item;
    }

    public void setItem(List<String> item) {
        this.item = item;
    }

    public long getItemCount() {
        return itemCount;
    }


    public List<OrderItems> getItemList() {
        return itemList;
    }

    public boolean isPrimeMembershipOrder() {
        return primeMembershipOrder;
    }

    public void setPrimeMembershipOrder(boolean primeMembershipOrder) {
        this.primeMembershipOrder = primeMembershipOrder;
    }

    public boolean isPrimeCustomerOrder() {
        return primeCustomerOrder;
    }

    public void setPrimeCustomerOrder(boolean primeCustomerOrder) {
        this.primeCustomerOrder = primeCustomerOrder;
    }
    public void setItemList(List<OrderItems> itemList) {
        this.itemList = itemList;
    }

    public boolean isTempOrder() {
        return isTempOrder;
    }

    public void setTempOrder(boolean tempOrder) {
        isTempOrder = tempOrder;
    }
}
