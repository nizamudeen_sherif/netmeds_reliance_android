package com.netmedsmarketplace.netmeds.model;

public class MstarFilterOption {
    private String name;
    private String value;
    private int count;
    private boolean checked;

    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }


    public boolean getChecked() {
        return checked;
    }


    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
