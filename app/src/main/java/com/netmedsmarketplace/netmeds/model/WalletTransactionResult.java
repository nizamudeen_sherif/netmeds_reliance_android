package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class WalletTransactionResult {
    @SerializedName("customerId")
    private String customerId = "";
    @SerializedName("orderId")
    private String orderId = "";
    @SerializedName("description")
    private String description = "";
    @SerializedName("descriptionNew")
    private String descriptionNew = "";
    @SerializedName("type")
    private String type = "";
    @SerializedName("updatedOn")
    private String updatedOn = "";
    @SerializedName("amount")
    private double amount;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("expiredDate")
    private String expiredDate;

    public String getCustomerId() {
        return customerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public double getAmount() {
        return amount;
    }

    public String getDescriptionNew() {
        return descriptionNew;
    }

    public void setDescriptionNew(String descriptionNew) {
        this.descriptionNew = descriptionNew;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }
}
