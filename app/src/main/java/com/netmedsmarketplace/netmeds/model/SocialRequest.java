package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseRequest;

public class SocialRequest extends BaseRequest {
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("payload")
    @Expose
    private String payload;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;
    @SerializedName("socialFlag")
    @Expose
    private String socialFlag;
    @SerializedName("sourceApp")
    @Expose
    private String sourceApp;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSocialFlag() {
        return socialFlag;
    }

    public void setSocialFlag(String socialFlag) {
        this.socialFlag = socialFlag;
    }

    public String getSourceApp() {
        return sourceApp;
    }

    public void setSourceApp(String sourceApp) {
        this.sourceApp = sourceApp;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
