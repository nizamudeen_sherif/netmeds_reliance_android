package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

public class ReviewResponse {
    @SerializedName("questionId")
    private String questionId;
    @SerializedName("questionText")
    private String questionText;
    @SerializedName("textInput")
    private String textInput;
    @SerializedName("numberInput")
    private Integer numberInput;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getTextInput() {
        return textInput;
    }

    public void setTextInput(String textInput) {
        this.textInput = textInput;
    }

    public Integer getNumberInput() {
        return numberInput;
    }

    public void setNumberInput(Integer numberInput) {
        this.numberInput = numberInput;
    }
}
