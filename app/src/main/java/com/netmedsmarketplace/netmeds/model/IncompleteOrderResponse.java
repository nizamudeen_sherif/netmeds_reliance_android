package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IncompleteOrderResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("ResponseData")
    private List<IncompleteOrderResult> responseData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<IncompleteOrderResult> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<IncompleteOrderResult> responseData) {
        this.responseData = responseData;
    }
}
