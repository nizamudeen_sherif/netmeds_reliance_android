package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.DrugDetail;

import java.util.List;

public class ReOrderSingleOrder  {
    @SerializedName("Orderid")
    private String orderid;
    @SerializedName("ship_location")
    private String shipLocation;
    @SerializedName("drug_status")
    private String drugStatus;
    @SerializedName("tracking_no")
    private String trackingNo;
    @SerializedName("tracking_url")
    private String trackingUrl;
    @SerializedName("trackingCompany")
    private String trackingCompany;
    @SerializedName("clickPostId")
    private String clickPostId;
    @SerializedName("drugdetails")
    private List<DrugDetail> drugdetails = null;
    @SerializedName("trackShortStatus")
    private String trackShortStatus;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getShipLocation() {
        return shipLocation;
    }

    public void setShipLocation(String shipLocation) {
        this.shipLocation = shipLocation;
    }

    public String getDrugStatus() {
        return drugStatus;
    }

    public void setDrugStatus(String drugStatus) {
        this.drugStatus = drugStatus;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getTrackingCompany() {
        return trackingCompany;
    }

    public void setTrackingCompany(String trackingCompany) {
        this.trackingCompany = trackingCompany;
    }

    public String getClickPostId() {
        return clickPostId;
    }

    public void setClickPostId(String clickPostId) {
        this.clickPostId = clickPostId;
    }

    public List<DrugDetail> getDrugdetails() {
        return drugdetails;
    }

    public void setDrugdetails(List<DrugDetail> drugdetails) {
        this.drugdetails = drugdetails;
    }

    public String getTrackShortStatus() {
        return trackShortStatus;
    }

    public void setTrackShortStatus(String trackShortStatus) {
        this.trackShortStatus = trackShortStatus;
    }

}
