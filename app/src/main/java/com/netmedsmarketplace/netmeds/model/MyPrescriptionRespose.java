package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class MyPrescriptionRespose {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("messageCode")
    private String messageCode;
    @SerializedName("result")
    private MyPrescriptionResponseResult result;

    public MyPrescriptionResponseResult getResult() {
        return result;
    }

    public void setResult(MyPrescriptionResponseResult result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public String getStatus() {
        return status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

