/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;
import com.netmedsmarketplace.netmeds.model.AttributesSendMail;
import com.nms.netmeds.base.model.BaseRequest;

public class SendEmailRequest extends BaseRequest {
    @SerializedName("Subject")
    private String subject;
    @SerializedName("typeid")
    private String typeId;
    @SerializedName("recipients")
    private String recipients;
    @SerializedName("Fromemail")
    private String fromEmail;
    @SerializedName("APIkey")
    private String apiKey;
    @SerializedName("OS")
    private String os;
    @SerializedName("attributes")
    private AttributesSendMail attribute;

    public AttributesSendMail getAttribute() {
        return attribute;
    }

    public void setAttribute(AttributesSendMail attribute) {
        this.attribute = attribute;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
