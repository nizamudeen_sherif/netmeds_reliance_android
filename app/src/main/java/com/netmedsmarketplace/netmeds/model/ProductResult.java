package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.HRVProduct;

import java.util.ArrayList;

public class ProductResult {

    @SerializedName("title")
    private String title;
    @SerializedName("type")
    private String type;
    @SerializedName("list")
    private ArrayList<HRVProduct> seasonalProducts = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<HRVProduct> getSeasonalProducts() {
        return seasonalProducts;
    }

    public void setSeasonalProducts(ArrayList<HRVProduct> seasonalProducts) {
        this.seasonalProducts = seasonalProducts;
    }
}
