package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class ApplyFilterRequest {
    @SerializedName("page")
    private Integer page;
    @SerializedName("id")
    private String id;
    @SerializedName("category")
    private String categoryKey;
    @SerializedName("manufac")
    private String manufactureKey;
    @SerializedName("price")
    private String priceKey;
    @SerializedName("discount")
    private String discountKey;
    @SerializedName("sortkey")
    private String sortKey;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getManufactureKey() {
        return manufactureKey;
    }

    public void setManufactureKey(String manufactureKey) {
        this.manufactureKey = manufactureKey;
    }

    public String getPriceKey() {
        return priceKey;
    }

    public void setPriceKey(String priceKey) {
        this.priceKey = priceKey;
    }

    public String getDiscountKey() {
        return discountKey;
    }

    public void setDiscountKey(String discountKey) {
        this.discountKey = discountKey;
    }

    public String getSortKey() {
        return sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }
}
