package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class CheckUserResponse extends BaseResponse {
    @SerializedName("updatedOn")
    private Integer updatedOn;
    @SerializedName("result")
    private CheckUserResponseResult result;

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Integer updatedOn) {
        this.updatedOn = updatedOn;
    }

    public CheckUserResponseResult getResult() {
        return result;
    }

    public void setResult(CheckUserResponseResult result) {
        this.result = result;
    }

}
