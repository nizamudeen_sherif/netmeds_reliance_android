package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BoomrangResponse implements Serializable {
    @SerializedName("result")
    private BoomrangResult result;
    @SerializedName("time")
    private String time;
    @SerializedName("status")
    private String status;

    public BoomrangResult getResult() {
        return result;
    }

    public void setResult(BoomrangResult result) {
        this.result = result;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
