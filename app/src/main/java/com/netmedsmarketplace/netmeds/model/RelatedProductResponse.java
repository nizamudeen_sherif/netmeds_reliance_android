package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class RelatedProductResponse extends BaseResponse {
    @SerializedName("result")
    private RelatedResult result;

    public RelatedResult getResult() {
        return result;
    }

    public void setResult(RelatedResult result) {
        this.result = result;
    }
}
