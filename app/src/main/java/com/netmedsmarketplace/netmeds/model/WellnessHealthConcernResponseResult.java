package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WellnessHealthConcernResponseResult {

    @SerializedName("header")
    private String header;
    @SerializedName("subHeader")
    private String subHeader;
    @SerializedName("description")
    private String description;
    @SerializedName("redirectionCatid")
    private String redirectionCatid;
    @SerializedName("list")
    private java.util.List<WellnessHealthConcernCategoryListResponse> list = null;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedirectionCatid() {
        return redirectionCatid;
    }

    public void setRedirectionCatid(String redirectionCatid) {
        this.redirectionCatid = redirectionCatid;
    }

    public List<WellnessHealthConcernCategoryListResponse> getList() {
        return list;
    }

    public void setList(List<WellnessHealthConcernCategoryListResponse> list) {
        this.list = list;
    }

}
