package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class IncompleteOrderDetailProduct {
    @SerializedName("productId")
    private Integer productId;
    @SerializedName("productQty")
    private Integer productQty;
    @SerializedName("productName")
    private String productName;
    @SerializedName("alternativeDrugCode")
    private Integer alternativeDrugCode;
    @SerializedName("isColdStorage")
    private boolean isColdStorage;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductQty() {
        return productQty;
    }

    public void setProductQty(Integer productQty) {
        this.productQty = productQty;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getAlternativeDrugCode() {
        return alternativeDrugCode;
    }

    public void setAlternativeDrugCode(Integer alternativeDrugCode) {
        this.alternativeDrugCode = alternativeDrugCode;
    }

    public boolean isColdStorage() {
        return isColdStorage;
    }

    public void setIsColdStorage(boolean isColdStorage) {
        this.isColdStorage = isColdStorage;
    }
}
