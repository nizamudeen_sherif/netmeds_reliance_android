package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderHeaderResult {

    @SerializedName("orderList")
    private List<OrderListResponse> orderList;
    @SerializedName("tempOrder")
    private List<TempOrder> tempOrder;
    @SerializedName("statusList")
    private List<OrderStatusListResponse> statusList;

    public List<OrderListResponse> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderListResponse> orderList) {
        this.orderList = orderList;
    }

    public List<OrderStatusListResponse> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OrderStatusListResponse> statusList) {
        this.statusList = statusList;
    }

    public List<TempOrder> getTempOrder() {
        return tempOrder;
    }

    public void setTempOrder(List<TempOrder> tempOrder) {
        this.tempOrder = tempOrder;
    }
}
