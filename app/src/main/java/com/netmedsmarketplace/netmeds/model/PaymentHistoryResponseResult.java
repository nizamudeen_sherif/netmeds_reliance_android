package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentHistoryResponseResult {
    @SerializedName("clientID")
    private String clientID;
    @SerializedName("MerchantID")
    private String merchantID;
    @SerializedName("clientAuthToken")
    private String clientAuthToken;
    @SerializedName("wallet")
    private List<Wallet> wallet = null;
    @SerializedName("savedCards")
    private List<SavedCard> savedCards = null;

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getClientAuthToken() {
        return clientAuthToken;
    }

    public void setClientAuthToken(String clientAuthToken) {
        this.clientAuthToken = clientAuthToken;
    }

    public List<Wallet> getWallet() {
        return wallet;
    }

    public void setWallet(List<Wallet> wallet) {
        this.wallet = wallet;
    }

    public List<SavedCard> getSavedCards() {
        return savedCards;
    }

    public void setSavedCards(List<SavedCard> savedCards) {
        this.savedCards = savedCards;
    }

}

