package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;
import com.nms.netmeds.base.model.ProductItem;

import java.util.List;

public class ProductListResult extends BaseResponse {
    @SerializedName("product")
    private ProductItem product;
    @SerializedName("variantDrugDetails")
    private List<ProductItem> alternateProduct;
    @SerializedName("productDetail")
    private ProductDetailResult productDetail;
    @SerializedName("brainsinFlag")
    private Boolean brainsinFlag;
    @SerializedName("tryAndBuyEnableFlag")
    private boolean tryAndBuyEnableFlag;
    @SerializedName("tryAndBuyTemplateId")
    private String tryAndBuyTemplateId;
    @SerializedName("alternateSaltMessage")
    private String alternateSaltMessage;
    @SerializedName("packSizeVariant")
    private List<ProductItem> packSizeVariant;
    @SerializedName("packProducts")
    private List<PackProducts> packProducts;
    @SerializedName("bundleDrugDetails")
    private BundleDrugDetails bundleDrugDetails;
    @SerializedName("similarProduct")
    private List<BundleDrugDetailsList> similarProduct;


    public ProductItem getProduct() {
        return product;
    }

    public void setProduct(ProductItem product) {
        this.product = product;
    }

    public ProductDetailResult getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetailResult productDetail) {
        this.productDetail = productDetail;
    }

    public Boolean getBrainsinFlag() {
        return brainsinFlag;
    }

    public void setBrainsinFlag(Boolean brainsinFlag) {
        this.brainsinFlag = brainsinFlag;
    }

    public List<ProductItem> getAlternateProduct() {
        return alternateProduct;
    }

    public void setAlternateProduct(List<ProductItem> alternateProduct) {
        this.alternateProduct = alternateProduct;
    }

    public boolean getTryAndBuyEnableFlag() {
        return tryAndBuyEnableFlag;
    }

    public void setTryAndBuyEnableFlag(boolean tryAndBuyEnableFlag) {
        this.tryAndBuyEnableFlag = tryAndBuyEnableFlag;
    }

    public String getTryAndBuyTemplateId() {
        return tryAndBuyTemplateId;
    }

    public void setTryAndBuyTemplateId(String tryAndBuyTemplateId) {
        this.tryAndBuyTemplateId = tryAndBuyTemplateId;
    }

    public String getAlternateSaltMessage() {
        return alternateSaltMessage;
    }

    public void setAlternateSaltMessage(String alternateSaltMessage) {
        this.alternateSaltMessage = alternateSaltMessage;
    }

    public List<ProductItem> getPackSizeVariant() {
        return packSizeVariant;
    }

    public void setPackSizeVariant(List<ProductItem> packSizeVariant) {
        this.packSizeVariant = packSizeVariant;
    }

    public List<PackProducts> getPackProducts() {
        return packProducts;
    }

    public void setPackProducts(List<PackProducts> packProducts) {
        this.packProducts = packProducts;
    }

    public BundleDrugDetails getBundleDrugDetails() {
        return bundleDrugDetails;
    }

    public void setBundleDrugDetails(BundleDrugDetails bundleDrugDetails) {
        this.bundleDrugDetails = bundleDrugDetails;
    }

    public List<BundleDrugDetailsList> getSimilarProduct() {
        return similarProduct;
    }

    public void setSimilarProduct(List<BundleDrugDetailsList> similarProduct) {
        this.similarProduct = similarProduct;
    }
}
