package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class PackProducts extends BaseResponse {
    @SerializedName("packSize")
    private Integer packSize;
    @SerializedName("packText")
    private String packText;
    @SerializedName("packPrice")
    private String packPrice;
    @SerializedName("packStrikePrice")
    private String packStrikePrice;
    @SerializedName("discount")
    private String discount;

    public Integer getPackSize() {
        return packSize;
    }

    public void setPackSize(Integer packSize) {
        this.packSize = packSize;
    }

    public String getPackText() {
        return packText;
    }

    public void setPackText(String packText) {
        this.packText = packText;
    }

    public String getPackPrice() {
        return packPrice;
    }

    public void setPackPrice(String packPrice) {
        this.packPrice = packPrice;
    }

    public String getPackStrikePrice() {
        return packStrikePrice;
    }

    public void setPackStrikePrice(String packStrikePrice) {
        this.packStrikePrice = packStrikePrice;
    }


    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
