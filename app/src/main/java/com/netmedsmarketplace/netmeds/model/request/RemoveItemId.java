package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

public class RemoveItemId {
    @SerializedName("itemid")
    private String itemId;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
