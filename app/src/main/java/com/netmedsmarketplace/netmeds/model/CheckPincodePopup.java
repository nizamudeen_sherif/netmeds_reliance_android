package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseRequest;

public class CheckPincodePopup extends BaseRequest {

    @SerializedName("result")
    private CheckPincodePopupResult result;
    @SerializedName("status")
    private String status;

    public CheckPincodePopupResult getResult() {
        return result;
    }

    public void setResult(CheckPincodePopupResult result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
