package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class RxPrescription {

    @SerializedName("rxId")
    private String rxId;
    @SerializedName("uploadDate")
    private String uploadDate;
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("prescriptionStatus")
    private String prescriptionStatus;
    @SerializedName("orderStatusDesc")
    private String orderStatusDescription;
    @SerializedName("customerId")
    private Integer customerId;
    @SerializedName("drugCode")
    private String drugCodeList;
    @SerializedName("brandName")
    private String brandNameList;
    @SerializedName("drugCodebrandName")
    private String drugCodebrandName;

    public Integer getCustomerId() {
        return customerId;
    }

    public String getBrandNameList() {
        return brandNameList;
    }

    public String getDrugCodebrandName() {
        return drugCodebrandName;
    }

    public String getDrugCodeList() {
        return drugCodeList;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getOrderStatusDescription() {
        return orderStatusDescription;
    }

    public String getRxId() {
        return rxId;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public String getPrescriptionStatus() {
        return prescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
        this.prescriptionStatus = prescriptionStatus;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public void setBrandNameList(String brandNameList) {
        this.brandNameList = brandNameList;
    }

    public void setDrugCodebrandName(String drugCodebrandName) {
        this.drugCodebrandName = drugCodebrandName;
    }

    public void setDrugCodeList(String drugCodeList) {
        this.drugCodeList = drugCodeList;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderStatusDescription(String orderStatusDescription) {
        this.orderStatusDescription = orderStatusDescription;
    }

    public void setRxId(String rxId) {
        this.rxId = rxId;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }
}
