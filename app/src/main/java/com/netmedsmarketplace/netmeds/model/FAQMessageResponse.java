package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FAQMessageResponse {
    @SerializedName("category")
    private List<FAQCategoryModel> category = null;

    public List<FAQCategoryModel> getCategory() {
        return category;
    }

    public void setCategory(List<FAQCategoryModel> category) {
        this.category = category;
    }
}

