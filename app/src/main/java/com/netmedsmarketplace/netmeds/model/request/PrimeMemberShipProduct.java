package com.netmedsmarketplace.netmeds.model.request;

import com.google.gson.annotations.SerializedName;

public class PrimeMemberShipProduct {
    @SerializedName("skuId")
    private String skuId;
    @SerializedName("price")
    public String price;
    @SerializedName("quantity")
    public Integer quantity;
    @SerializedName("minimumQuantity")
    private Integer minimumQuantity;
    @SerializedName("maximumSelectableQuantity")
    private Integer maximumSelectableQuantity;
    @SerializedName("availableStatus")
    private String availableStatus;
    @SerializedName("duration")
    private String duration;
    @SerializedName("recommended")
    private Boolean recommended;
    @SerializedName("name")
    public String name;
    @SerializedName("originalPrice")
    private String originalPrice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalPrice() {
        return this.originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(Integer minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public Integer getMaximumSelectableQuantity() {
        return maximumSelectableQuantity;
    }

    public void setMaximumSelectableQuantity(Integer maximumSelectableQuantity) {
        this.maximumSelectableQuantity = maximumSelectableQuantity;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Boolean getRecommended() {
        return recommended;
    }

    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }
}
