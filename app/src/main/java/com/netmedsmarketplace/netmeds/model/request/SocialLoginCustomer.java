/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.model.request;


import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.CustomAttribute;

import java.util.List;

public class SocialLoginCustomer {
    @SerializedName("email")
    private String email;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("storeId")
    private String storeId;
    @SerializedName("customAttributes")
    private List<CustomAttribute> customAttributes = null;
    @SerializedName("gender")
    private int gender;
    @SerializedName("dob")
    private String dob;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}