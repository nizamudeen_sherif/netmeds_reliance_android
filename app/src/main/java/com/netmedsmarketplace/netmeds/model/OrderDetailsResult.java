package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.DrugDetail;

import java.util.List;

public class OrderDetailsResult {

    @SerializedName("orderId")
    private String orderId;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("isPrimeMembershipOrder")
    private String isPrimeMembershipOrder;
    @SerializedName("isPrimeCustomerOrder")
    private String isPrimeCustomerOrder;
    @SerializedName("orderDate")
    private String orderDate;
    @SerializedName("orderStatusCode")
    private String orderStatusCode;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("cancelStatus")
    private boolean cancelStatus;
    @SerializedName("expectedDeliveryDateString")
    private String promisedDeliveryDate;
    @SerializedName("orderBillingInfo")
    private OrderBillingInfo orderBillingInfo;
    @SerializedName("orderShippingInfo")
    private OrderShippingInfo orderShippingInfo;
    @SerializedName("orderdetail")
    private List<OrderDetail> orderdetail = null;
    @SerializedName("canceledItemsDetails")
    private List<DrugDetail> canceledItemsDetails = null;
    @SerializedName("productAmount")
    private double productAmount;
    @SerializedName("shippingAmount")
    private double shippingAmount;
    @SerializedName("codCharge")
    private long codCharge;
    @SerializedName("totalAmount")
    private double totalAmount;
    @SerializedName("voucherAmount")
    private double voucherAmount;
    @SerializedName("usedWalletAmount")
    private double usedWalletAmount;
    @SerializedName("transactionAmount")
    private double transactionAmount;
    @SerializedName("discountAmount")
    private double discountAmount;
    @SerializedName("couponDiscount")
    private double couponDiscount;
    @SerializedName("savingsAmount")
    private double savingsAmount;
    @SerializedName("rxId")
    private List<String> rxId = null;
    @SerializedName("expressStatus")
    private String expressStatus;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatusCode() {
        return orderStatusCode;
    }

    public void setOrderStatusCode(String orderStatusCode) {
        this.orderStatusCode = orderStatusCode;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean isCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(boolean cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getPromisedDeliveryDate() {
        return promisedDeliveryDate;
    }

    public void setPromisedDeliveryDate(String promisedDeliveryDate) {
        this.promisedDeliveryDate = promisedDeliveryDate;
    }

    public OrderBillingInfo getOrderBillingInfo() {
        return orderBillingInfo;
    }

    public void setOrderBillingInfo(OrderBillingInfo orderBillingInfo) {
        this.orderBillingInfo = orderBillingInfo;
    }

    public OrderShippingInfo getOrderShippingInfo() {
        return orderShippingInfo;
    }

    public void setOrderShippingInfo(OrderShippingInfo orderShippingInfo) {
        this.orderShippingInfo = orderShippingInfo;
    }

    public List<OrderDetail> getOrderdetail() {
        return orderdetail;
    }

    public void setOrderdetail(List<OrderDetail> orderdetail) {
        this.orderdetail = orderdetail;
    }

    public double getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(double productAmount) {
        this.productAmount = productAmount;
    }

    public double getShippingAmount() {
        return shippingAmount;
    }

    public void setShippingAmount(double shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public long getCodCharge() {
        return codCharge;
    }

    public void setCodCharge(long codCharge) {
        this.codCharge = codCharge;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public double getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(double usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public double getSavingsAmount() {
        return savingsAmount;
    }

    public void setSavingsAmount(double savingsAmount) {
        this.savingsAmount = savingsAmount;
    }

    public List<String> getRxId() {
        return rxId;
    }

    public void setRxId(List<String> rxId) {
        this.rxId = rxId;
    }

    public String getExpressStatus() {
        return expressStatus;
    }

    public void setExpressStatus(String expressStatus) {
        this.expressStatus = expressStatus;
    }

    public List<DrugDetail> getCanceledItemsDetails() {
        return canceledItemsDetails;
    }

    public void setCanceledItemsDetails(List<DrugDetail> canceledItemsDetails) {
        this.canceledItemsDetails = canceledItemsDetails;
    }
}