package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BundleDrugDetailsList {
    @SerializedName("id")
    private String id;
    @SerializedName("skuId")
    private String skuId;
    @SerializedName("name")
    private String name;
    @SerializedName("genericName")
    private String genericName;
    @SerializedName("categoryId")
    private String categoryId;
    @SerializedName("categoryName")
    private String categoryName;
    @SerializedName("subCategoryName")
    private String subCategoryName;
    @SerializedName("rxRequired")
    private String rxRequired;
    @SerializedName("returnableMessage")
    private String returnableMessage;
    @SerializedName("manufacturerName")
    private String manufacturerName;
    @SerializedName("strikePrice")
    private String strikePrice;
    @SerializedName("actualPrice")
    private String actualPrice;
    @SerializedName("quantity")
    private Integer quantity;
    @SerializedName("schedule")
    private String schedule;
    @SerializedName("coldStorage")
    private boolean coldStorage;
    @SerializedName("drugType")
    private String drugType;
    @SerializedName("minimumQuantity")
    private Integer minimumQuantity;
    @SerializedName("maximumSelectableQuantity")
    private Integer maximumSelectableQuantity;
    @SerializedName("packSizeTypeLabel")
    private String packSizeTypeLabel;
    @SerializedName("availableStatus")
    private String availableStatus;
    @SerializedName("availableStatusCode")
    private String availableStatusCode;
    @SerializedName("imageURL")
    private String imageURL = null;
    @SerializedName("typeId")
    private String typeId;
    @SerializedName("groupId")
    private Object groupId;
    @SerializedName("pageName")
    private String pageName;
    @SerializedName("selectionId")
    private String selectionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getRxRequired() {
        return rxRequired;
    }

    public void setRxRequired(String rxRequired) {
        this.rxRequired = rxRequired;
    }

    public String getReturnableMessage() {
        return returnableMessage;
    }

    public void setReturnableMessage(String returnableMessage) {
        this.returnableMessage = returnableMessage;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(String strikePrice) {
        this.strikePrice = strikePrice;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public boolean getColdStorage() {
        return coldStorage;
    }

    public void setColdStorage(boolean coldStorage) {
        this.coldStorage = coldStorage;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public Integer getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(Integer minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public Integer getMaximumSelectableQuantity() {
        return maximumSelectableQuantity;
    }

    public void setMaximumSelectableQuantity(Integer maximumSelectableQuantity) {
        this.maximumSelectableQuantity = maximumSelectableQuantity;
    }

    public String getPackSizeTypeLabel() {
        return packSizeTypeLabel;
    }

    public void setPackSizeTypeLabel(String packSizeTypeLabel) {
        this.packSizeTypeLabel = packSizeTypeLabel;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus;
    }

    public String getAvailableStatusCode() {
        return availableStatusCode;
    }

    public void setAvailableStatusCode(String availableStatusCode) {
        this.availableStatusCode = availableStatusCode;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public Object getGroupId() {
        return groupId;
    }

    public void setGroupId(Object groupId) {
        this.groupId = groupId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }


    public String getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(String selectionId) {
        this.selectionId = selectionId;
    }
}
