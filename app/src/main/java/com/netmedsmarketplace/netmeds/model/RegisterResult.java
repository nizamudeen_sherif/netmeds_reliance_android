package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class RegisterResult {

    @SerializedName("sessionToken")
    private String sessionToken;
    @SerializedName("appToken")
    private String appToken;
    @SerializedName("customerInfo")
    private RegisterUserDetailsResult userDetails;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public RegisterUserDetailsResult getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(RegisterUserDetailsResult userDetails) {
        this.userDetails = userDetails;
    }
}
