package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterOptionData {
    @SerializedName("manufacture")
    private ArrayList<FilterTitle> manufactureList = null;
    @SerializedName("sub_div")
    private ArrayList<FilterTitle> subDivisionList = null;
    @SerializedName("Price_array")
    private ArrayList<FilterTitle> priceList = null;
    @SerializedName("Category")
    private ArrayList<FilterTitle> categoryList = null;
    @SerializedName("discount_array")
    private ArrayList<FilterTitle> discountArray = null;

    public ArrayList<FilterTitle> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<FilterTitle> categoryList) {
        this.categoryList = categoryList;
    }

    public ArrayList<FilterTitle> getManufactureList() {
        return manufactureList;
    }

    public void setManufactureList(ArrayList<FilterTitle> manufactureList) {
        this.manufactureList = manufactureList;
    }

    public ArrayList<FilterTitle> getSubDivisionList() {
        return subDivisionList;
    }

    public void setSubDivisionList(ArrayList<FilterTitle> subDivisionList) {
        this.subDivisionList = subDivisionList;
    }

    public ArrayList<FilterTitle> getPriceList() {
        return priceList;
    }

    public void setPriceList(ArrayList<FilterTitle> priceList) {
        this.priceList = priceList;
    }

    public ArrayList<FilterTitle> getDiscountArray() {
        return discountArray;
    }

    public void setDiscountArray(ArrayList<FilterTitle> discountArray) {
        this.discountArray = discountArray;
    }
}
