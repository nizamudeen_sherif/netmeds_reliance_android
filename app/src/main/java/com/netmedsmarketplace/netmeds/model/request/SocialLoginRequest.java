/*
 * "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.model.request;


import com.google.gson.annotations.SerializedName;

public class SocialLoginRequest {
    @SerializedName("password")
    private String password;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("customer")
    private SocialLoginCustomer customer;
    @SerializedName("source")
    private String source;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public SocialLoginCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(SocialLoginCustomer customer) {
        this.customer = customer;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}