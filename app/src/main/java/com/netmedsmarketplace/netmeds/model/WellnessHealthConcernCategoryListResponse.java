package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class WellnessHealthConcernCategoryListResponse {


    @SerializedName("categoryId")
    private String categoryId;
    @SerializedName("discount")
    private String discount;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
