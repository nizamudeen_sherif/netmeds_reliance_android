package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.BaseResponse;

public class RegisterResponse extends BaseResponse {

    @SerializedName("updatedOn")
    private Integer updatedOn;
    @SerializedName("result")
    private RegisterResult result;

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Integer updatedOn) {
        this.updatedOn = updatedOn;
    }

    public RegisterResult getResult() {
        return result;
    }

    public void setResult(RegisterResult result) {
        this.result = result;
    }
}
