package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterTitle {
    @SerializedName("title")
    private String title;
    @SerializedName("key")
    private String key;
    @SerializedName("options")
    private ArrayList<FilterOption> filterOptionList = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<FilterOption> getFilterOptionList() {
        return filterOptionList;
    }

    public void setFilterOptionList(ArrayList<FilterOption> filterOptionList) {
        this.filterOptionList = filterOptionList;
    }
}