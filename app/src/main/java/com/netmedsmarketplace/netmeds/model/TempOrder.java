package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;
import com.nms.netmeds.base.model.DrugDetail;

import java.util.List;

public class TempOrder {
    @SerializedName("orderId")
    private String orderId;
    @SerializedName("paymentMode")
    private String paymentMode;
    @SerializedName("orderDate")
    private String orderDate;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("customerName")
    private String customerName;
    @SerializedName("drugDetails")
    private List<DrugDetail> drugDetailList = null;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<DrugDetail> getDrugDetailList() {
        return drugDetailList;
    }

    public void setDrugDetailList(List<DrugDetail> drugDetailList) {
        this.drugDetailList = drugDetailList;
    }
}
