package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class UpdateMobileNoStatusResponse {
    @SerializedName("customerStatus")
    private boolean customerStatus;
    @SerializedName("message")
    private String message;

    public boolean isCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(boolean customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
