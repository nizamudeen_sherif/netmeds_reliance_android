package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BundleDrugDetails {
    @SerializedName("totalAmount")
    private String totalAmount;
    @SerializedName("list")
    private List<BundleDrugDetailsList> list = null;
    @SerializedName("bundleSku")
    private String bundleSku;
    @SerializedName("optionId")
    private String optionId;

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<BundleDrugDetailsList> getList() {
        return list;
    }

    public void setList(List<BundleDrugDetailsList> list) {
        this.list = list;
    }

    public String getBundleSku() {
        return bundleSku;
    }

    public void setBundleSku(String bundleSku) {
        this.bundleSku = bundleSku;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }
}