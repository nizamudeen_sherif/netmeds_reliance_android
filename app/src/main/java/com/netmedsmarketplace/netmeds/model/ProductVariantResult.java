package com.netmedsmarketplace.netmeds.model;

import com.google.gson.annotations.SerializedName;

public class ProductVariantResult {

    @SerializedName("variants")
    private ProductVariants variants;

    public ProductVariants getVariants() {
        return variants;
    }

    public void setVariants(ProductVariants variants) {
        this.variants = variants;
    }
}
