package com.netmedsmarketplace.netmeds;

import com.nms.netmeds.base.model.SortOptionLabel;

import java.util.ArrayList;
import java.util.Iterator;

public class MstarFilterHelper {
    private static MstarFilterHelper filterHelper;
    private static ArrayList<String> discountSelection = new ArrayList<>();
    private static ArrayList<String> priceSelection = new ArrayList<>();
    private static ArrayList<String> manufactureSelection = new ArrayList<>();
    private static ArrayList<String> brandSelection = new ArrayList<>();
    private static ArrayList<String> categorySelection = new ArrayList<>();
    private static ArrayList<String> availabilitySelection = new ArrayList<>();
    private static SortOptionLabel sortOptionLabel;

    public static MstarFilterHelper getInstance() {
        if (filterHelper != null) {
            filterHelper = new MstarFilterHelper();
        }
        return filterHelper;
    }

    public static void clearSelection() {
        availabilitySelection = new ArrayList<>();
        manufactureSelection = new ArrayList<>();
        priceSelection = new ArrayList<>();
        discountSelection = new ArrayList<>();
        categorySelection = new ArrayList<>();
        brandSelection = new ArrayList<>();
    }

    public static ArrayList<String> getDiscountSelection() {
        return discountSelection;
    }

    public static void setDiscountSelection(String discountSelection) {
        MstarFilterHelper.discountSelection.add(discountSelection);
    }

    public static ArrayList<String> getPriceSelection() {
        return priceSelection;
    }

    public static void setPriceSelection(String priceSelection) {
        MstarFilterHelper.priceSelection.add(priceSelection);
    }

    public static ArrayList<String> getManufactureSelection() {
        return manufactureSelection;
    }

    public static void setManufactureSelection(String manufactureSelection) {
        MstarFilterHelper.manufactureSelection.add(manufactureSelection);
    }

    public static ArrayList<String> getCategorySelection() {
        return categorySelection;
    }

    public static void setCategorySelection(String categorySelection) {
        MstarFilterHelper.categorySelection.add(categorySelection);
    }

    public static ArrayList<String> getBrandSelection() {
        return brandSelection;
    }

    public static void setBrandSelection(String brandSelection) {
        MstarFilterHelper.brandSelection.add(brandSelection);
    }

    public static ArrayList<String> getAvailabilitySelection() {
        return availabilitySelection;
    }

    public static void setAvailabilitySelection(String availabilitySelection) {
        MstarFilterHelper.availabilitySelection.add(availabilitySelection);
    }

    public static void removeSelection(ArrayList<String> removeList, String removeItem) {
        for (Iterator<String> iterator = removeList.iterator(); iterator.hasNext(); ) {
            String item = iterator.next();
            if (item.equals(removeItem))
                iterator.remove();
        }
    }

    public static void clearSortSelection() {
        sortOptionLabel = new SortOptionLabel();
    }

    public static SortOptionLabel getSortOptionLabel() {
        return sortOptionLabel;
    }

    public static void setSortOptionLabel(SortOptionLabel sortOptionLabel) {
        MstarFilterHelper.sortOptionLabel = sortOptionLabel;
    }
}
