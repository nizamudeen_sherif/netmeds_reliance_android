package com.netmedsmarketplace.netmeds;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.webengage.sdk.android.WebEngage;

public class InstallReceiverProxy extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String var5 = intent.getStringExtra("referrer");
        String var6 = intent.getAction();

        //find out the installer for your app package name.
        String installer = context.getPackageManager().getInstallerPackageName(context.getPackageName());

        if ("com.android.vending.INSTALL_REFERRER".equals(var6) && !TextUtils.isEmpty(var5) && !TextUtils.isEmpty(installer)) {
            //GA
            new CampaignTrackingReceiver().onReceive(context, intent);
        }
    }
}
