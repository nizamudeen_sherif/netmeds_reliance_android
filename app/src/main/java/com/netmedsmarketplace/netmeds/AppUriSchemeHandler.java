package com.netmedsmarketplace.netmeds;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.algolia.search.saas.AlgoliaException;
import com.algolia.search.saas.Client;
import com.algolia.search.saas.CompletionHandler;
import com.algolia.search.saas.Index;
import com.algolia.search.saas.Query;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.ui.StartActivity;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeHandler;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeRouter;
import com.nmp.android.netmeds.navigation.urischeme.UriSchemeUtil;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.R;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConsultationNotification;
import com.nms.netmeds.base.model.JustDocUserResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResponse;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.DeepLinkConstant;
import com.nms.netmeds.base.utils.DiagnosticConstant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppUriSchemeHandler extends UriSchemeHandler {
    private String routeNode;
    private String url;
    private ConfigMap configMap;
    private String query;
    private static final String SLASH_SYMBOL = "/";
    private static final String HYPHEN_SYMBOL = "-";
    private static final String VIEW_ORDER = "view";
    private static final String TRACK_ORDER = "track";
    private static final String INDEX = "index";
    public static final String MANUFACTURER = "manufacturer";
    public static final String PRESCRIPTION = "prescription";
    public static final String BRAND = "brand";
    public static final String OTC = "otc";
    public static final String UTM_CAMPAIGN = "utm_campaign";
    public static final String PAY_NOW = "paynow";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configMap = ConfigMap.getInstance();
        Intent intent = getIntent();
        String action = intent != null && !TextUtils.isEmpty(intent.getAction()) ? intent.getAction() : null;
        if (action != null && action.equals("android.intent.action.MAIN")) {
            startActivity(new Intent(AppUriSchemeHandler.this, StartActivity.class));
            AppUriSchemeHandler.this.finish();
        } else {
            checkAppLink(intent);
        }
    }

    private void checkAppLink(Intent intent) {
        if (checkPrime(intent)) {
            routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_home_activity) + SLASH_SYMBOL;
            String baseUrl = checkBaseUrl(url);
            url = url.replace(baseUrl, "");
            BasePreference.getInstance(this).setGuestCart(true);
            processCurrentPage();
        } else if (BasePreference.getInstance(this).isMstarSessionIdAvailable() || BasePreference.getInstance(this).isGuestCart()) {
            checkIntent(intent);
        } else {
            routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + getString(R.string.route_sign_in_activity);
            onPageNavigation();
        }
    }

    private void checkIntent(Intent intent) {
        if (intent != null && intent.hasExtra(NetmedsFirebaseMessagingService.KEY_CONSULTATION_NOTIFICATION_DATA)) {
            navigateToConsultation(intent);
        } else {
            routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_home_activity) + SLASH_SYMBOL;
            handleIntent(intent);
        }
    }

    private void navigateToConsultation(Intent intent) {
        JustDocUserResponse consultationLoginResult = new Gson().fromJson(BasePreference.getInstance(this).getJustDocUserResponse(), JustDocUserResponse.class);
        String jusDocToken = consultationLoginResult != null && consultationLoginResult.getToken() != null ? consultationLoginResult.getToken() : "";
        ConsultationNotification consultationNotification = (ConsultationNotification) intent.getSerializableExtra(NetmedsFirebaseMessagingService.KEY_CONSULTATION_NOTIFICATION_DATA);

        if (TextUtils.isEmpty(jusDocToken)) {
            routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_home_activity) + SLASH_SYMBOL + AppUriSchemeHandler.this.getString(R.string.route_history_activity);
        } else {
            routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_home_activity) + SLASH_SYMBOL + AppUriSchemeHandler.this.getString(R.string.route_chat_activity) + SLASH_SYMBOL + new Gson().toJson(consultationNotification);
        }
        onPageNavigation();
    }

    @Override
    protected void handleIntent(Intent intent) {
        Uri uri = intent != null ? intent.getData() : null;
        if (uri != null) {
            url = uri.toString();
            String baseUrl = checkBaseUrl(url);
            url = url.replace(baseUrl, "");
            if (url.contains(SLASH_SYMBOL) && (url.contains(DeepLinkConstant.PRESCRIPTION_URL) || url.contains(DeepLinkConstant.NON_PRESCRIPTION_URL) || url.contains(DeepLinkConstant.PRESCRIPTION_GENERICS_URL)))
                checkEndPoint();
            else
                processCurrentPage();
        }
    }

    private boolean checkPrime(Intent intent) {
        Uri uri = intent != null ? intent.getData() : null;
        if (uri != null) {
            url = uri.toString();
            return url.contains(DeepLinkConstant.NETMEDS_FIRST) || url.contains(DeepLinkConstant.OFFERS) || url.contains(DeepLinkConstant.SPECIAL_OFFERS);
        }
        return false;

    }

    private String checkBaseUrl(String url) {
        String baseUrl = "";
        if (url.contains(configMap.getProperty(ConfigConstant.WEBSITE_DOMAIN))) {
            baseUrl = configMap.getProperty(ConfigConstant.WEBSITE_DOMAIN);
        } else if (url.contains(configMap.getProperty(ConfigConstant.MOBILE_DOMAIN))) {
            baseUrl = configMap.getProperty(ConfigConstant.MOBILE_DOMAIN);
        } else if (url.contains(configMap.getProperty(ConfigConstant.CONSULTATION_DOMAIN))) {
            baseUrl = configMap.getProperty(ConfigConstant.CONSULTATION_DOMAIN);
        } else if (url.contains(configMap.getProperty(ConfigConstant.SHORT_URL_DOMAIN))) {
            baseUrl = configMap.getProperty(ConfigConstant.SHORT_URL_DOMAIN);
        } else if (url.contains(configMap.getProperty(ConfigConstant.DIAGNOSTIC_DOMAIN)))
            baseUrl = configMap.getProperty(ConfigConstant.DIAGNOSTIC_DOMAIN);
        return baseUrl;
    }

    private void checkEndPoint() {
        ArrayList<String> endPointList = getEndPoint(url);
        if (endPointList.size() > 0) {
            query = endPointList.get(endPointList.size() - 1);
            if (query.contains("?")) {
                query = query.substring(0, query.indexOf("?"));
            }
            algoliaSearch(query.contains(HYPHEN_SYMBOL) ? query.replace(HYPHEN_SYMBOL, " ") : query);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void processCurrentPage() {
        String path = getPathFromUrl();
        switch (path) {
            case DeepLinkConstant.HEALTH_LIBRARY_URL:
                routeNode += getString(R.string.route_netmeds_web_view) + SLASH_SYMBOL + (getPathArray() != null && getPathArray().length == 3 ? getPathArray()[2] : path);
                break;
            case DeepLinkConstant.ABOUT_US_URL:
            case DeepLinkConstant.FAQS_URL:
            case DeepLinkConstant.REWARD_POINTS_URL:
            case DeepLinkConstant.REWARD:
            case DeepLinkConstant.TERMS_AND_CONDITIONS:
            case DeepLinkConstant.PRIVACY_POLICY:
            case DeepLinkConstant.SPECIAL_OFFERS:
                routeNode += getString(R.string.route_netmeds_web_view) + SLASH_SYMBOL + path;
                break;
            case DeepLinkConstant.REFER_EARN_URL:
                routeNode += getString(R.string.route_refer_earn);
                break;
            case DeepLinkConstant.OFFERS:
                setOfferPath(getPathArray());
                break;
            case DeepLinkConstant.SEARCH:
                routeNode += getString(R.string.route_search);
                routeNode += getPathArray() != null && getPathArray().length == 3 && !TextUtils.isEmpty(getPathArray()[2].replace("/?q=", ""))
                        ? SLASH_SYMBOL + getPathArray()[2].replace("?q=", "") : "";
                break;
            case DeepLinkConstant.CUSTOMIZED_CART:
                checkOrderHistory(getPathArray());
                break;
            case DeepLinkConstant.CUSTOMER:
                onCustomerIntent(getPathArray());
                break;
            case DeepLinkConstant.CHECKOUT:
                routeNode += getString(R.string.route_netmeds_mstar_cart);
                break;
            case DeepLinkConstant.UPLOAD_PRESCRIPTION:
                routeNode += getString(R.string.route_m2_attach_prescription);
                break;
            case DeepLinkConstant.WELLNESS:
                routeNode += getString(R.string.route_wellness);
                break;
            case DeepLinkConstant.HELP_SUPPORT:
                routeNode += getString(R.string.route_need_help_activity);
                break;
            case DeepLinkConstant.ONLINE_CONSULTATION:
                routeNode += getString(R.string.route_history_activity);
                break;
            case DeepLinkConstant.HOME:
                routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_navigation_activity);
                break;
            case DeepLinkConstant.DIAGNOSTIC:
                getDiagnosticPath(getPathArray());
                break;
            case DeepLinkConstant.LAB_TEST:
                getDiagnosticLabTestPath(getPathArray());
                break;
            case DeepLinkConstant.PRESCRIPTION_URL:
                routeNode += getString(R.string.route_search);
                break;
            case DeepLinkConstant.NON_PRESCRIPTION_URL:
                routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_navigation_activity);
                break;
            case DeepLinkConstant.GENERIC_BRAND:
                routeNode += getGenericDeepLink();
                break;
            default:
                routeNode = getString(R.string.route_schema) + getString(R.string.route_navigation_activity) + SLASH_SYMBOL + path;
                break;
        }
        onPageNavigation();
    }

    private void onCustomerIntent(String[] pathArray) {
        if (pathArray != null && pathArray.length > 1 && !TextUtils.isEmpty(pathArray[1])) {
            switch (pathArray[1]) {
                case DeepLinkConstant.ORDER_HISTORY:
                    checkOrderHistory(getPathArray());
                    break;
                case DeepLinkConstant.NMSPAYLIST:
                    routeNode += getString(R.string.route_payment_history);
                    break;
                case DeepLinkConstant.NETMEDS_FIRST:
                    routeNode += getString(R.string.route_prime_membership_activity);
                    break;
                case DeepLinkConstant.SUBSCRIPTION:
                    getSubscriptionPath(url);
                    break;
                case DeepLinkConstant.WALLET:
                case DeepLinkConstant.ACCOUNT:
                    routeNode = getString(R.string.route_schema) + getString(R.string.route_navigation_activity) + SLASH_SYMBOL + pathArray[1];
                    break;
                case DeepLinkConstant.INVITATION:
                    routeNode += getString(R.string.route_refer_earn);
                    break;
                case DeepLinkConstant.MY_PRESCRIPTION:
                    routeNode += getString(R.string.route_my_prescription_view);
                    break;
            }
        }
    }

    private String getGenericDeepLink() {
        if (getPathArray().length > 1)
            return getString(R.string.route_generic_product_detail) + SLASH_SYMBOL + (getPathArray() != null && getPathArray().length > 1 ? getPathArray()[getPathArray().length - 1] : "");
        else
            return getString(R.string.route_generic_home);
    }

    private String getPathFromUrl() {
        if (!TextUtils.isEmpty(url)) {
            String[] splitPath = url.split("/");
            return splitPath.length > 0 ? splitPath[0] : "";
        }
        return "";
    }

    private String[] getPathArray() {
        if (!TextUtils.isEmpty(url)) {
            return url.split("/");
        }
        return null;
    }

    private void checkOrderHistory(String[] splitPath) {
        if (url.contains(VIEW_ORDER) && splitPath != null && splitPath.length > 3) {
            routeNode += getString(R.string.route_view_order_detail) + SLASH_SYMBOL + replaceSpecialCharacter(splitPath[3]) + SLASH_SYMBOL + "0";
        } else if (url.contains(DeepLinkConstant.VIEW_ORDER_ID) && splitPath != null && splitPath.length > 1 && splitPath[1] != null) {
            routeNode += getString(R.string.route_view_order_detail) + SLASH_SYMBOL + replaceSpecialCharacter(splitPath[1].replace(DeepLinkConstant.VIEW_ORDER_ID_INDEX, "")) + SLASH_SYMBOL + "1";
        } else if (url.contains(TRACK_ORDER) && splitPath != null && splitPath.length > 3) {
            routeNode += getString(R.string.route_track_order_detail) + SLASH_SYMBOL + replaceSpecialCharacter(splitPath[3]);
        } else {
            routeNode = getString(R.string.route_schema) + getString(R.string.route_navigation_activity) + SLASH_SYMBOL + DeepLinkConstant.ORDER_HISTORY;
        }
    }

    private void getSubscriptionPath(String url) {
        if (!TextUtils.isEmpty(url)) {
            Uri uri = Uri.parse(url);
            String path = uri != null && uri.getPath() != null ? uri.getPath() : "";
            List<String> pathSegmentList = uri != null && uri.getPathSegments() != null ? uri.getPathSegments() : null;
            if (path != null && path.contains(VIEW_ORDER) && pathSegmentList != null && pathSegmentList.size() > 6) {
                routeNode += getString(R.string.route_subscription_manage_activity) + SLASH_SYMBOL + pathSegmentList.get(4) + SLASH_SYMBOL + pathSegmentList.get(6) + SLASH_SYMBOL + getSubscriptionPayNow(uri);
            } else {
                routeNode = getString(R.string.route_schema) + getString(R.string.route_navigation_activity) + SLASH_SYMBOL + DeepLinkConstant.SUBSCRIPTION;
            }
        }
    }

    private String getSubscriptionPayNow(Uri uri) {
        if (uri == null)
            return "";
        String query = uri.getQuery() != null ? uri.getQuery() : "";
        String lastSegment = !TextUtils.isEmpty(uri.getLastPathSegment()) ? uri.getLastPathSegment() : "";
        if (!TextUtils.isEmpty(query) && query.length() > 0) {
            return uri.getQueryParameter(UTM_CAMPAIGN);
        } else if (lastSegment != null && lastSegment.equalsIgnoreCase(PAY_NOW)) {
            return lastSegment;
        }
        return "";
    }

    private void onPageNavigation() {
        try {
            UriSchemeRouter.route(UriSchemeUtil.trimUriScheme(Uri.parse(routeNode)), this, true, true);
            AppUriSchemeHandler.this.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> getEndPoint(String url) {
        ArrayList<String> endPoint = new ArrayList<>();
        String splitEndPoint = "";
        if (url.contains(DeepLinkConstant.NON_PRESCRIPTION_URL)) {
            splitEndPoint = url.replace(DeepLinkConstant.NON_PRESCRIPTION_URL, "");
        } else if (url.contains(DeepLinkConstant.PRESCRIPTION_URL)) {
            splitEndPoint = url.replace(DeepLinkConstant.PRESCRIPTION_URL, "");
        } else if (url.contains(DeepLinkConstant.PRESCRIPTION_GENERICS_URL)) {
            splitEndPoint = url.replace(DeepLinkConstant.PRESCRIPTION_GENERICS_URL, "");
        }

        String[] endpoints = splitEndPoint.split(SLASH_SYMBOL);
        Collections.addAll(endPoint, endpoints);
        return endPoint;
    }

    public void algoliaSearch(String searchQuery) {
        Client client = new Client(CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_APP_ID), CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_API_KEY));
        Index index = client.initIndex(CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_INDEX));
        Query query;
        query = new Query(searchQuery);
        query.setFacets("*");
        query.setHitsPerPage(!TextUtils.isEmpty(CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_HITS)) ? Integer.valueOf(CommonUtils.getAlgoliaCredentials(BasePreference.getInstance(this), AppConstant.ALGOLIA_HITS)) : AppConstant.DEFAULT_ALGOLIA_HITS);
        index.searchAsync(query, new CompletionHandler() {
            @Override
            public void requestCompleted(JSONObject content, AlgoliaException error) {
                if (content == null) {
                    return;
                }
                algoliaResponse(content.toString());
            }
        });
    }

    private void algoliaResponse(String response) {
        if (response != null) {
            MstarAlgoliaResponse algoliaResponse = new Gson().fromJson(response, MstarAlgoliaResponse.class);
            if (algoliaResponse != null && algoliaResponse.getAlgoliaResultList() != null && algoliaResponse.getAlgoliaResultList().size() > 0) {
                if (algoliaResponse.getAlgoliaResultList().size() == 1) {
                    routeNode += getString(R.string.route_product_detail) + SLASH_SYMBOL + algoliaResponse.getAlgoliaResultList().get(0).getProductCode();
                } else if (url.contains(DeepLinkConstant.BRAND_URL) && getPathArray() != null && getPathArray().length > 1) {
                    routeNode += getString(R.string.route_product_list) + SLASH_SYMBOL + getPathArray()[getPathArray().length - 1] + SLASH_SYMBOL + BRAND;
                } else {
                    MstarAlgoliaResult algoliaHitResult = checkBaseUrl(algoliaResponse.getAlgoliaResultList());
                    Category category = setCategory(algoliaResponse.getAlgoliaResultList());
                    setRouteNodes(algoliaHitResult, category);
                }
            } else
                routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_navigation_activity);
        } else
            routeNode = AppUriSchemeHandler.this.getString(R.string.route_schema) + AppUriSchemeHandler.this.getString(R.string.route_navigation_activity);

        onPageNavigation();
    }

    private Category setCategory(List<MstarAlgoliaResult> algoliaHitResultList) {
        Category category;
        if (url.contains(DeepLinkConstant.MANUFACTURER_URL)) {
            category = new Category();
            category.setName(algoliaHitResultList.get(0).getManufacturerName());
            category.setId(Integer.toString(algoliaHitResultList.get(0).getManufacturerId()));
        } else if (url.contains(DeepLinkConstant.PRESCRIPTION_GENERICS_URL)) {
            category = new Category();
            category.setName(algoliaHitResultList.get(0).getGeneric());
            if (algoliaHitResultList.get(0).getCategoryIds() != null && algoliaHitResultList.get(0).getCategoryIds().size() > 0)
                category.setId(Long.toString(algoliaHitResultList.get(0).getCategoryIds().get(0)));
        } else
            category = getCategoryId(algoliaHitResultList);
        return category;
    }

    private MstarAlgoliaResult checkBaseUrl(List<MstarAlgoliaResult> algoliaHitResultsList) {
        if (!url.contains(DeepLinkConstant.PRESCRIPTION_GENERICS_URL)) {
            for (MstarAlgoliaResult algoliaHitResults : algoliaHitResultsList) {
                String containBase = url.contains(DeepLinkConstant.PRESCRIPTION_URL) ? DeepLinkConstant.PRESCRIPTION_URL : DeepLinkConstant.NON_PRESCRIPTION_URL;
                String[] productUrl = algoliaHitResults.getUrlPath().split(containBase);
                if (productUrl.length > 1 && url.contains(productUrl[1]))
                    return algoliaHitResults;
            }
        }
        return null;
    }

    private Category getCategoryId(List<MstarAlgoliaResult> algoliaHitResultList) {
        String removeSpecialCharacterFromUrl = query.replace(HYPHEN_SYMBOL, " ");
        for (MstarAlgoliaResult algoliaHitResult : algoliaHitResultList) {
            int i = 0;
            for (String path : algoliaHitResult.getCategories()) {
                String categoryName = path.replace(HYPHEN_SYMBOL, " ");
                if (categoryName.equalsIgnoreCase(removeSpecialCharacterFromUrl)) {
                    Category category = new Category();
                    category.setId(Long.toString(algoliaHitResult.getCategoryIds().get(i)));
                    category.setName(categoryName);
                    return category;
                }
                i++;
            }
        }
        return null;
    }

    private void setRouteNodes(MstarAlgoliaResult algoliaHitResults, Category category) {
        if (algoliaHitResults != null)
            routeNode += getString(R.string.route_product_detail) + SLASH_SYMBOL + algoliaHitResults.getProductCode();
        else
            checkManufacture(category);
    }

    private void checkManufacture(Category category) {
        if (url.contains(DeepLinkConstant.MANUFACTURER_URL)) {
            routeNode += getString(R.string.route_product_list) + SLASH_SYMBOL + getCategoryId(category) + SLASH_SYMBOL + MANUFACTURER + SLASH_SYMBOL + getCategoryName(category);
        } else
            routeNode += getString(R.string.route_category_activity) + SLASH_SYMBOL + getCategoryId(category) + SLASH_SYMBOL + getType() + SLASH_SYMBOL + getCategoryName(category);
    }

    private String getCategoryId(Category category) {
        return category != null && !TextUtils.isEmpty(category.getId()) ? category.getId() : "";
    }

    private String getCategoryName(Category category) {
        return category != null && !TextUtils.isEmpty(category.getName()) ? category.getName() : "";
    }

    private String getType() {
        return url.contains(DeepLinkConstant.NON_PRESCRIPTION_URL) ? OTC : PRESCRIPTION;
    }

    private class Category {
        private String name;
        private String id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    private void getDiagnosticPath(String[] path) {
        if (url.contains(DeepLinkConstant.DIAGNOSTIC_ORDER_HISTORY)) {
            routeNode += getString(R.string.route_diagnostic_order_history);
        } else if (url.contains(DeepLinkConstant.DIAGNOSTIC_ORDER_TRACK) && path != null && path.length > 1) {
            routeNode += getString(R.string.route_diagnostic_track_order) + SLASH_SYMBOL + path[2];
        } else {
            routeNode += getString(R.string.route_diagnostic_home);
        }
    }

    private void getDiagnosticLabTestPath(String[] pathArray) {
        String name = "";
        String type = "";
        if (pathArray != null && pathArray.length > 1 && !TextUtils.isEmpty(pathArray[1])) {
            String[] nameSplit = pathArray[1].split("\\?");
            name = nameSplit.length > 0 ? nameSplit[0] : "";
            if (nameSplit.length > 1) {
                String[] typeSplit = nameSplit[1].split("=");
                type = typeSplit.length > 1 && !TextUtils.isEmpty(typeSplit[1]) ? typeSplit[1] : "";
            }

            switch (type) {
                case DiagnosticConstant.TEST_TYPE_TEST:
                    routeNode += getString(R.string.route_diagnostic_home) + SLASH_SYMBOL + name + SLASH_SYMBOL + DiagnosticConstant.TEST_TYPE_TEST;
                    break;
                case DiagnosticConstant.TEST_TYPE_PROFILE:
                case DiagnosticConstant.TEST_TYPE_PACKAGES:
                case DiagnosticConstant.TEST_TYPE_PACKAGE:
                    routeNode += getString(R.string.route_diagnostic_home) + SLASH_SYMBOL + name + SLASH_SYMBOL + DiagnosticConstant.TEST_TYPE_PACKAGE;
                    break;
                default:
                    routeNode += getString(R.string.route_diagnostic_home);
                    break;
            }
        }
    }

    private String replaceSpecialCharacter(String orderId) {
        String[] splitPath = null;
        if (orderId.contains("?"))
            splitPath = orderId.split("\\?");
        else if (orderId.contains("&"))
            splitPath = orderId.split("&");
        return splitPath != null && splitPath.length > 0 ? splitPath[0] : orderId;
    }

    private void setOfferPath(String[] OfferPath) {
        if (OfferPath != null && OfferPath.length > 1) {
            routeNode += getString(R.string.route_view_offer_details) + SLASH_SYMBOL + OfferPath[1];
        } else {
            routeNode += getString(R.string.route_offers);
        }
    }
}
