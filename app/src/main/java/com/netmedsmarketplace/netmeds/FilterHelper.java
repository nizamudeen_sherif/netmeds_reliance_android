package com.netmedsmarketplace.netmeds;

import com.nms.netmeds.base.model.SortOptionLabel;

import java.util.ArrayList;

public class FilterHelper {
    private static FilterHelper helperClass;
    private static ArrayList<String> discount;
    private static ArrayList<String> price;
    private static ArrayList<String> manufacture;
    private static ArrayList<String> subCategory;
    private static ArrayList<String> category;
    private static ArrayList<String> discountSelection;
    private static ArrayList<String> priceSelection;
    private static ArrayList<String> manufactureSelection;
    private static ArrayList<String> subCategorySelection;
    private static ArrayList<String> categorySelection;
    private static SortOptionLabel sortOptionLabel;

    public static FilterHelper getInstance() {
        if (helperClass != null) {
            helperClass = new FilterHelper();
        }
        return helperClass;
    }

    public static ArrayList<String> getDiscount() {
        return discount;
    }

    public static void setDiscount(ArrayList<String> discount) {
        FilterHelper.discount = discount;
    }

    public static ArrayList<String> getPrice() {
        return price;
    }

    public static void setPrice(ArrayList<String> price) {
        FilterHelper.price = price;
    }

    public static ArrayList<String> getManufacture() {
        return manufacture;
    }

    public static void setManufacture(ArrayList<String> manufacture) {
        FilterHelper.manufacture = manufacture;
    }

    public static ArrayList<String> getSubCategory() {
        return subCategory;
    }

    public static void setSubCategory(ArrayList<String> subCategory) {
        FilterHelper.subCategory = subCategory;
    }

    public static void clearFilterSelection() {
        subCategory = new ArrayList<>();
        manufacture = new ArrayList<>();
        price = new ArrayList<>();
        discount = new ArrayList<>();
        category = new ArrayList<>();
    }

    public static void clearSelection(){
        subCategorySelection = new ArrayList<>();
        manufactureSelection = new ArrayList<>();
        priceSelection = new ArrayList<>();
        discountSelection = new ArrayList<>();
        categorySelection = new ArrayList<>();
    }

    public static void clearSortSelection() {
        sortOptionLabel = new SortOptionLabel();
    }

    public static SortOptionLabel getSortOptionLabel() {
        return sortOptionLabel;
    }

    public static void setSortOptionLabel(SortOptionLabel sortOptionLabel) {
        FilterHelper.sortOptionLabel = sortOptionLabel;
    }

    public static ArrayList<String> getDiscountSelection() {
        return discountSelection;
    }

    public static void setDiscountSelection(ArrayList<String> discountSelection) {
        FilterHelper.discountSelection = discountSelection;
    }

    public static ArrayList<String> getPriceSelection() {
        return priceSelection;
    }

    public static void setPriceSelection(ArrayList<String> priceSelection) {
        FilterHelper.priceSelection = priceSelection;
    }

    public static ArrayList<String> getManufactureSelection() {
        return manufactureSelection;
    }

    public static void setManufactureSelection(ArrayList<String> manufactureSelection) {
        FilterHelper.manufactureSelection = manufactureSelection;
    }

    public static ArrayList<String> getSubCategorySelection() {
        return subCategorySelection;
    }

    public static void setSubCategorySelection(ArrayList<String> subCategorySelection) {
        FilterHelper.subCategorySelection = subCategorySelection;
    }

    public static ArrayList<String> getCategorySelection() {
        return categorySelection;
    }

    public static void setCategorySelection(ArrayList<String> categorySelection) {
        FilterHelper.categorySelection = categorySelection;
    }

    public static ArrayList<String> getCategory() {
        return category;
    }

    public static void setCategory(ArrayList<String> category) {
        FilterHelper.category = category;
    }
}
