package com.netmedsmarketplace.netmeds.adpater;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterOfferBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.SlideBanner;

import java.util.List;

public class SlideBannerAdapter extends RecyclerView.Adapter<SlideBannerAdapter.BannerViewHolder> {
    private Context context;
    private List<SlideBanner> slideBannerList;

    SlideBannerAdapter(Context context, List<SlideBanner> slideBannerList) {
        this.context = context;
        this.slideBannerList = slideBannerList;
    }

    @NonNull
    @Override
    public BannerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterOfferBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_offer, viewGroup, false);
        return new SlideBannerAdapter.BannerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerViewHolder bannerViewHolder, int i) {
        final SlideBanner banner = slideBannerList.get(i);
        if (banner != null && !TextUtils.isEmpty(banner.getImageUrl())) {
            Glide.with(context.getApplicationContext()).load(banner.getImageUrl()).error(Glide.with(context.getApplicationContext()).load(R.drawable.ic_no_image)).into(bannerViewHolder.binding.imgOffer);
        }
        bannerViewHolder.binding.imgOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.consultationNavigateToScreens(context, banner, (Activity) context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return slideBannerList.size();
    }

    class BannerViewHolder extends RecyclerView.ViewHolder {
        private AdapterOfferBinding binding;

        BannerViewHolder(@NonNull AdapterOfferBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
