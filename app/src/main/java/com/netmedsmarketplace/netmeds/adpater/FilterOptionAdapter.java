package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.MstarFilterHelper;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.model.MstarFilterOption;
import com.nms.netmeds.base.AppConstant;

import java.util.ArrayList;

public class FilterOptionAdapter extends RecyclerView.Adapter<FilterOptionAdapter.FilterTitleHolder> implements Filterable {
    private final FilterOptionAdapter.FilterOptionListener mListener;
    private final String mTitleSelection;
    private ArrayList<MstarFilterOption> filterOptionsList;
    private ArrayList<MstarFilterOption> sortedFilterOption;
    private SearchFilter searchFilter;

    public FilterOptionAdapter(ArrayList<MstarFilterOption> titleList, FilterOptionListener listener, String titleSelection) {
        this.filterOptionsList = titleList;
        this.sortedFilterOption = titleList;
        this.mListener = listener;
        this.mTitleSelection = titleSelection;
    }

    @NonNull
    @Override
    public FilterOptionAdapter.FilterTitleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_filter_option, viewGroup, false);
        return new FilterOptionAdapter.FilterTitleHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterOptionAdapter.FilterTitleHolder holder, int i) {
        final MstarFilterOption option = filterOptionsList.get(i);
        holder.name.setText(option.getName());
        if (mTitleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_SELLING_PRICE)) {
            for (String key : MstarFilterHelper.getPriceSelection()) {
                if (key.equalsIgnoreCase(option.getValue()))
                    option.setChecked(true);
            }
        }
        if (mTitleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_MANUFACTURER_NAME)) {
            for (String key : MstarFilterHelper.getManufactureSelection()) {
                if (key.equalsIgnoreCase(option.getValue()))
                    option.setChecked(true);
            }
        }
        if (mTitleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_BRAND)) {
            for (String key : MstarFilterHelper.getBrandSelection()) {
                if (key.equalsIgnoreCase(option.getValue()))
                    option.setChecked(true);
            }
        }
        if (mTitleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_DISCOUNT_PCT)) {
            for (String key : MstarFilterHelper.getDiscountSelection()) {
                if (key.equalsIgnoreCase(option.getValue()))
                    option.setChecked(true);
            }
        }
        if (mTitleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_CATEGORY_TREE_LEVEL)) {
            for (String key : MstarFilterHelper.getCategorySelection()) {
                if (key.equalsIgnoreCase(option.getValue()))
                    option.setChecked(true);
            }
        }
        if (mTitleSelection.equalsIgnoreCase(AppConstant.ALGOLIA_FILTER_KEY_IN_STOCK)) {
            for (String key : MstarFilterHelper.getAvailabilitySelection()) {
                if (key.equalsIgnoreCase(option.getValue()))
                    option.setChecked(true);
            }
        }

        holder.checked.setChecked(option.getChecked());

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (option.getChecked()) {
                    option.setChecked(false);
                    mListener.removeFilterOptionSelection(option.getValue());
                } else {
                    option.setChecked(true);
                    mListener.setFilerOptionSelection(option.getValue());
                }
                notifyDataSetChanged();
            }
        });

        holder.checked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (option.getChecked()) {
                    option.setChecked(false);
                    mListener.removeFilterOptionSelection(option.getValue());
                } else {
                    option.setChecked(true);
                    mListener.setFilerOptionSelection(option.getValue());
                }
                notifyDataSetChanged();
            }
        });
    }

    private void updateList() {

    }

    @Override
    public int getItemCount() {
        return filterOptionsList.size();
    }

    @Override
    public Filter getFilter() {
        if (searchFilter == null) {
            searchFilter = new SearchFilter(FilterOptionAdapter.this);
        }
        return searchFilter;

    }

    public interface FilterOptionListener {
        void setFilerOptionSelection(String optionID);

        void removeFilterOptionSelection(String id);
    }

    class FilterTitleHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final CheckBox checked;

        FilterTitleHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            checked = itemView.findViewById(R.id.checked);
        }
    }

    private class SearchFilter extends Filter {
        private final FilterOptionAdapter adapter;

        SearchFilter(FilterOptionAdapter adapter) {
            super();
            this.adapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                filterResults.values = sortedFilterOption;
                filterResults.count = sortedFilterOption.size();
            } else {
                ArrayList<MstarFilterOption> filteredList = new ArrayList<>();
                final String filterPattern = constraint.toString().toLowerCase();
                for (final MstarFilterOption orderItem : sortedFilterOption) {
                    if (orderItem.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(orderItem);
                    }
                }
                filterResults.values = filteredList;
                filterResults.count = filteredList.size();
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filterOptionsList = (ArrayList<MstarFilterOption>) results.values;
            adapter.notifyDataSetChanged();
        }
    }
}

