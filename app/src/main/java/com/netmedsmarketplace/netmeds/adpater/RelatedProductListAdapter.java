package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterSearchBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.BrainSinsMisc;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.ImageUtils;

import java.util.ArrayList;

public class RelatedProductListAdapter extends RecyclerView.Adapter<RelatedProductListAdapter.AlternateProductViewHolder> {

    private final Context context;
    private final ArrayList<HRVProduct> hrvProducts;
    private final RelatedProductListAdapterListener callback;
    private boolean isPeopleAlsoViewedProduct;
    private String alternateImageBasePath = "";

    public RelatedProductListAdapter(Context context, ArrayList<HRVProduct> hrvProducts, RelatedProductListAdapterListener callback, boolean isPeopleAlsoViewedProduct) {
        this.context = context;
        this.callback = callback;
        this.hrvProducts = hrvProducts;
        this.isPeopleAlsoViewedProduct = isPeopleAlsoViewedProduct;
        MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = new Gson().fromJson(BasePreference.getInstance(context).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        alternateImageBasePath = mStarBasicResponseTemplateModel != null && mStarBasicResponseTemplateModel.getResult() != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCatalogImageUrlBasePath()) ? mStarBasicResponseTemplateModel.getResult().getCatalogImageUrlBasePath() : "";
    }

    @NonNull
    @Override
    public RelatedProductListAdapter.AlternateProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterSearchBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_search, parent, false);
        return new RelatedProductListAdapter.AlternateProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final RelatedProductListAdapter.AlternateProductViewHolder holder, final int position) {
        holder.bindData();
    }

    @Override
    public int getItemCount() {
        return hrvProducts.size() < 3 ? hrvProducts.size() : 3;
    }


    class AlternateProductViewHolder extends RecyclerView.ViewHolder {
        private final AdapterSearchBinding binding;

        AlternateProductViewHolder(final AdapterSearchBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }

        public void bindData() {
            HRVProduct product = hrvProducts.get(getAdapterPosition());
            if (context != null) {
                Glide.with(context)
                        .load(ImageUtils.checkImage(product.getImageUrl().contains(AppConstant.HTTPS) ? product.getImageUrl() : alternateImageBasePath + product.getImageUrl()))
                        .error(Glide.with(binding.alternateBrainProductImage).load(R.drawable.ic_no_image))
                        .into(binding.alternateBrainProductImage);
            }
            if (product != null) {
                binding.alternateBrainProductImage.setVisibility(View.VISIBLE);
                binding.alternateBrainDrugName.setText(!TextUtils.isEmpty(product.getBrandName()) ? product.getBrandName() : "");
                if (isPeopleAlsoViewedProduct) {
                    BrainSinsMisc miscValue = new Gson().fromJson(product.getMisc(), BrainSinsMisc.class);
                    boolean isDiscountAvailable = !TextUtils.isEmpty(miscValue.getDiscount()) && Double.valueOf(miscValue.getDiscount()) > 0;
                    binding.searchAlternateBrainStrikePrice.setText(isPeopleAlsoViewedProduct ? !TextUtils.isEmpty(miscValue.getOriginalPrice()) ? CommonUtils.getPriceInFormat(miscValue.getOriginalPrice()) : "" : !TextUtils.isEmpty(product.getPrice()) ? CommonUtils.getPriceInFormat(product.getPrice()) : "");
                    binding.searchAlternateBrainalgoliaPrice.setText(isPeopleAlsoViewedProduct ? !TextUtils.isEmpty(product.getPrice()) ? CommonUtils.getPriceInFormat(product.getPrice()) : "" : "");
                    binding.alternateBrainAlgoliaPriceMrp.setVisibility(isDiscountAvailable ? View.GONE : View.VISIBLE);
                    binding.alternateBrainStrikePriceMrp.setVisibility(isDiscountAvailable ? View.VISIBLE : View.GONE);
                    binding.liMrpVisibility.setVisibility(miscValue.getDiscount().compareTo("0") > 0 ? View.VISIBLE : View.GONE);
                } else {
                    binding.searchAlternateBrainalgoliaPrice.setText(!TextUtils.isEmpty(product.getPrice()) ? CommonUtils.getPriceInFormat(product.getPrice()) : "");
                    binding.searchAlternateBrainStrikePrice.setText(!TextUtils.isEmpty(product.getActualPrice()) ? CommonUtils.getPriceInFormat(product.getActualPrice()) : "");
                    binding.alternateBrainStrikePriceMrp.setVisibility(TextUtils.isEmpty(product.getActualPrice()) || product.getPrice().equals(product.getActualPrice()) ? View.GONE : View.VISIBLE);
                    binding.alternateBrainAlgoliaPriceMrp.setVisibility(binding.alternateBrainStrikePriceMrp.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    binding.searchAlternateBrainStrikePrice.setVisibility(TextUtils.isEmpty(product.getActualPrice()) || product.getPrice().equals(product.getActualPrice()) ? View.GONE : View.VISIBLE);
                }
                binding.searchAlternateBrainStrikePrice.setPaintFlags(binding.searchAlternateBrainStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                binding.alternateBrainCategoryName.setText(product.getParentCategoryName());
                binding.alternateBrainManufacturerName.setText(AppConstant.MFR + product.getManufacturerName());
                binding.alternateBrainDrugDetail.setText(product.getPackSizeTypeLabel());

                binding.llDrugDetailsRxLabelCategoryName.setVisibility(isPeopleAlsoViewedProduct ? View.GONE : View.VISIBLE);
                binding.alternateBrainRxImage.setVisibility(product.getRxRequired() == 1 ? View.VISIBLE : View.GONE);
                binding.alternateBrainProductImage.setVisibility(product.getRxRequired() == 1 ? View.GONE : View.VISIBLE);
                binding.alternateBrainCategoryName.setVisibility(isPeopleAlsoViewedProduct ? View.GONE : View.VISIBLE);
                binding.alternateBrainManufacturerName.setVisibility(isPeopleAlsoViewedProduct ? View.GONE : View.VISIBLE);
                binding.alternateBrainDrugDetail.setVisibility(isPeopleAlsoViewedProduct ? View.GONE : View.VISIBLE);
                binding.alternateBrainRxRequired.setVisibility(isPeopleAlsoViewedProduct ? View.GONE : product.getRxRequired() == 1 ? View.VISIBLE : View.GONE);
                binding.searchDivider.setVisibility(getAdapterPosition() == getItemCount() - 1 ? View.GONE : View.VISIBLE);

                binding.alternateBrainProductView.setOnClickListener(callback.navigateToProduct(Integer.parseInt(product.getId())));
                binding.alternateBrainAddCartButton.setOnClickListener(callback.getProductDetailsForAddToCart(Integer.parseInt(product.getId())));
            }
        }
    }

    public interface RelatedProductListAdapterListener {
        View.OnClickListener navigateToProduct(int productId);

        View.OnClickListener getProductDetailsForAddToCart(int productId);
    }
}

