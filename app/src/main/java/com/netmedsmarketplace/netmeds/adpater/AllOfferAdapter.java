package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterAllOfferItemRowBinding;
import com.netmedsmarketplace.netmeds.viewmodel.OfferTabFragmentViewModel;
import com.nms.netmeds.base.model.MstarNetmedsOffer;

import java.util.List;

public class AllOfferAdapter extends RecyclerView.Adapter<AllOfferAdapter.OrderViewHolder> {

    private final List<MstarNetmedsOffer> offerList;
    private final OfferTabFragmentViewModel viewModel;

    public AllOfferAdapter(List<MstarNetmedsOffer> offerList, OfferTabFragmentViewModel viewModel) {
        this.offerList = offerList;
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterAllOfferItemRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_all_offer_item_row, viewGroup, false);
        return new OrderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {
        private final AdapterAllOfferItemRowBinding binding;

        OrderViewHolder(final AdapterAllOfferItemRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }

        void bind(OfferTabFragmentViewModel viewModel, int position) {
            binding.setItem(offerList.get(position));
            binding.setPosition(position);
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
            binding.cvOfferImage.setVisibility(offerList != null && offerList.get(position) != null && offerList.get(position).getBannerImg() != null && !offerList.get(position).getBannerImg().isEmpty() ? View.VISIBLE : View.GONE);
        }
    }
}
