package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterPaymentHistorySavedCardsItemsBinding;
import com.nms.netmeds.base.model.LinkedSavedCard;
import com.nms.netmeds.base.utils.ImageUtils;

import java.util.List;

public class SavedCardsListAdapter extends RecyclerView.Adapter<SavedCardsListAdapter.ViewHolder> {
    private Context context;
    private List<LinkedSavedCard> savedCardList;
    private LinkedWalletListAdapter.LinkedPaymentAdapterListener listener;
    private String itemFrom;

    public SavedCardsListAdapter(List<LinkedSavedCard> savedCardList, LinkedWalletListAdapter.LinkedPaymentAdapterListener listener, String itemFrom) {
        this.savedCardList = savedCardList;
        this.listener = listener;
        this.itemFrom = itemFrom;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        this.context = viewGroup.getContext();
        AdapterPaymentHistorySavedCardsItemsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_payment_history_saved_cards_items, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LinkedSavedCard linkedSavedCard = savedCardList.get(position);
        Glide.with(context).load(ImageUtils.checkImage(linkedSavedCard.getImage())).into(holder.binding.imgCardTypeImage);
        holder.binding.tvCardHolderName.setText(linkedSavedCard.getNameOnCard());
        holder.binding.tvCardNo.setText(linkedSavedCard.getCardNumber());
        holder.binding.tvCardExpires.setText(String.format("%s%s%s", linkedSavedCard.getCardExpMonth(), "/", linkedSavedCard.getCardExpYear()));
        holder.binding.imgDeleteCard.setOnClickListener(deleteCard(linkedSavedCard));
    }

    private View.OnClickListener deleteCard(final LinkedSavedCard linkedSavedCard) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(linkedSavedCard, itemFrom);
            }
        };
    }

    private String getLastFourDigitText(String title) {
        String[] titleSplit = title.split("-");
        return titleSplit[titleSplit.length - 1];
    }

    @Override
    public int getItemCount() {
        return savedCardList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final AdapterPaymentHistorySavedCardsItemsBinding binding;

        ViewHolder(AdapterPaymentHistorySavedCardsItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
