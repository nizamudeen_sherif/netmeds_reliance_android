package com.netmedsmarketplace.netmeds.adpater;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ItemAlternateCartRowBinding;
import com.netmedsmarketplace.netmeds.viewmodel.AlternateCartActivityViewModel;
import com.nms.netmeds.base.model.Alternative;

import java.util.List;

public class AlternateCartProductListAdapter extends RecyclerView.Adapter<AlternateCartProductListAdapter.ViewHolder> {

    private List<Alternative> currentCartProductAndAlternateCartProductList;
    private AlternateCartActivityViewModel viewModel;

    private boolean isSuggestedCartHeadingSelected;
    private String imageUrl;


    public AlternateCartProductListAdapter(AlternateCartActivityViewModel viewModel, String imageUrl, List<Alternative> currentCartProductAndAlternateCartProductList, boolean isSuggestedCartHeadingSelected) {
        this.viewModel = viewModel;
        this.imageUrl = imageUrl;
        this.currentCartProductAndAlternateCartProductList = currentCartProductAndAlternateCartProductList;
        this.isSuggestedCartHeadingSelected = isSuggestedCartHeadingSelected;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemAlternateCartRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_alternate_cart_row, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind();
    }

    @Override
    public int getItemCount() {
        return currentCartProductAndAlternateCartProductList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemAlternateCartRowBinding binding;

        public ViewHolder(@NonNull ItemAlternateCartRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind() {
            Alternative alternative = currentCartProductAndAlternateCartProductList.get(getAdapterPosition());
            binding.viewBottomLine.setVisibility(getItemCount() - 1 != getAdapterPosition() ? View.VISIBLE : View.GONE);
            binding.setIsCartSuggestedHeadingSelected(isSuggestedCartHeadingSelected);
            binding.setModel(alternative);
            binding.setViewModel(viewModel);
            binding.executePendingBindings();

            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);
            String imagePath = isSuggestedCartHeadingSelected ? alternative.getAlternateProductImagePath() : alternative.getCartProductImagePath();
            Glide.with(viewModel.getContext()).load(imageUrl + imagePath)
                    .apply(options)
                    .into(binding.productImage);
        }
    }
}
