package com.netmedsmarketplace.netmeds.adpater;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterBuyAgainBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BuyAgainAdapter extends RecyclerView.Adapter<BuyAgainAdapter.BuyAgainViewHolder> implements CartItemQtyAdapter.QuantityListListener {

    private BuyAgainAdapterListener listener;
    private List<MStarProductDetails> mStarProductDetailsList;
    private CartItemQtyAdapter cartItemQtyAdapter;

    private String imageUrl;

    private Dialog dialog;
    private int scrolledPosition = 0;

    private boolean do_animate = true;
    private List<MStarProductDetails> mStarProductDetails = new ArrayList<>();
    private Context context;

    public BuyAgainAdapter(List<MStarProductDetails> mStarProductDetailsList, MStarBasicResponseTemplateModel templateModel, BuyAgainAdapterListener listener) {
        this.mStarProductDetailsList = mStarProductDetailsList;
        this.listener = listener;
        imageUrl = templateModel != null && templateModel.getResult() != null && !TextUtils.isEmpty(templateModel.getResult().getProductImageUrlBasePath()) ? templateModel.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" : "";
    }

    @NonNull
    @Override
    public BuyAgainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterBuyAgainBinding buyAgainBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_buy_again, parent, false);
        this.context = parent.getContext();
        return new BuyAgainViewHolder(buyAgainBinding);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull final BuyAgainViewHolder holder, final int position) {
        holder.bindViewData(null);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyAgainViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (!payloads.isEmpty()) {
            MStarProductDetails updateProductDetails = (MStarProductDetails) payloads.get(0);
            holder.bindViewData(updateProductDetails);
        } else {
            holder.bindViewData(null);
        }
    }

    @Override
    public int getItemCount() {
        return mStarProductDetailsList.size();
    }

    @Override
    public void itemQuantitySelectionCallback(int position, MStarCartAdapter.ItemQuantity itemQuantity, MStarProductDetails cartItems) {
        if (dialog != null) {
            dialog.dismiss();
        }
        scrolledPosition = position;
        cartItemQtyAdapter.updateList(position);
        int previousQty = cartItems.getCartQuantity();
        int selectedQty = position + 1;

        if (previousQty > selectedQty) {
            listener.decreaseQuantity(cartItems, previousQty - selectedQty, selectedQty);
            //updateAdapter(cartItems.getProductCode(), previousQty - selectedQty <= maxQty ? selectedQty : previousQty);
        } else if (previousQty < selectedQty) {
            listener.increaseQuantity(cartItems, selectedQty - previousQty, selectedQty);
            //updateAdapter(cartItems.getProductCode(), selectedQty);
        }
    }

    private void updateAdapter(int productCode, int quantity) {
        int position = 0;
        for (MStarProductDetails productDetails : mStarProductDetailsList) {
            position = mStarProductDetailsList.indexOf(productDetails);
            if (productCode == productDetails.getProductCode()) {
                productDetails.setCartQuantity(quantity);
                productDetails.setQuantityPickerVisibility(productDetails.getCartQuantity() > 0 && !productDetails.isNotAbleToAddToCartStatus() ? View.VISIBLE : View.GONE);
                productDetails.setAddToCartVisibility(!productDetails.isNotAbleToAddToCartStatus() && productDetails.getCartQuantity() == 0 ? View.VISIBLE : View.GONE);
                mStarProductDetailsList.set(position, productDetails);
                break;
            }
        }
        notifyItemChanged(position);
    }

    public void setProperQuantity(int quantity, int selectedQty, MStarProductDetails productDetails) {
        int maxQty = productDetails.getMaxQtyInOrder();
        int previousQty = productDetails.getCartQuantity();

        if (previousQty > selectedQty) {
            updateAdapter(productDetails.getProductCode(), quantity <= maxQty ? selectedQty : previousQty);
        } else if (previousQty < selectedQty) {
            updateAdapter(productDetails.getProductCode(), selectedQty);
        }
    }


    public class BuyAgainViewHolder extends RecyclerView.ViewHolder {

        private AdapterBuyAgainBinding binding;

        public BuyAgainViewHolder(AdapterBuyAgainBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bindViewData(MStarProductDetails updateProductDetails) {
            CommonUtils.setAnimation(binding.buyAgainItemView, do_animate, 100, context);
            MStarProductDetails productDetails;
            if (updateProductDetails == null) {
                productDetails = mStarProductDetailsList.get(getAdapterPosition());
            } else {
                productDetails = updateProductDetails;
            }
            binding.setProductDetails(productDetails);
            binding.setViewHolder(this);
            binding.executePendingBindings();
            bindImage(productDetails);
            initializeClickListeners(productDetails);
        }

        private void initializeClickListeners(final MStarProductDetails productDetails) {
            binding.buyAgainItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProductView(productDetails.getProductCode());
                }
            });

            binding.prodAddToCart.setOnClickListener(itemQuantityPickerDialog(productDetails));

            binding.buyAgainTvItemQtyLl.setOnClickListener(itemQuantityPickerDialog(productDetails));

            binding.buyAgainDeleteProduct.setOnClickListener(removeProduct(productDetails));
        }

        private void bindImage(MStarProductDetails productDetails) {
            String imagePath = productDetails.getImagePaths() != null ? productDetails.getImagePaths().get(0) : "";
            RequestOptions options = new RequestOptions()
                    .circleCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(binding.getRoot().getContext()).load(imagePath.contains(AppConstant.HTTPS) ? ImageUtils.checkImage(imagePath) : ImageUtils.checkImage(imageUrl + imagePath))
                    .apply(options).into(binding.buyAgainProductImage);
        }

        public String getProductManufactureName(MStarProductDetails productDetails) {
            return productDetails.getManufacturer() != null && !TextUtils.isEmpty(productDetails.getManufacturer().getName()) ? String.format("" + AppConstant.MFR + " %s", productDetails.getManufacturer().getName()) : "";
        }

        public String getSellingPrice(MStarProductDetails productDetails) {
            return CommonUtils.getPriceInFormat(productDetails != null ? productDetails.getSellingPrice() : BigDecimal.ZERO);
        }

        public boolean getDiscountVisibility(MStarProductDetails productDetails) {
            return productDetails != null && productDetails.getSellingPrice().doubleValue() < productDetails.getMrp().doubleValue();
        }

        public String getDiscountValue(MStarProductDetails productDetails) {
            binding.discountPriceText.setPaintFlags(binding.discountPriceText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            return CommonUtils.getPriceInFormat(productDetails != null ? productDetails.getMrp() : BigDecimal.ZERO);
        }

        public String getProductCartQuantity(MStarProductDetails productDetails) {
            return String.format(binding.getRoot().getContext().getString(R.string.text_item_qty_with_label), String.valueOf(productDetails.getCartQuantity()));
        }

        private View.OnClickListener itemQuantityPickerDialog(final MStarProductDetails cartItems) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mStarProductDetails != null && mStarProductDetails.size() > 0) {
                        for (int i = 0; i < mStarProductDetails.size(); i++) {
                            if (mStarProductDetails.get(i).getDisplayName().equalsIgnoreCase(cartItems.getDisplayName())) {
                                mStarProductDetails.remove(cartItems);
                            }
                        }
                    }
                    mStarProductDetails.add(cartItems);
                    PaymentHelper.setmStarProductDetails(mStarProductDetails);
                    AlertDialog.Builder builder = new AlertDialog.Builder(binding.getRoot().getContext());
                    View dialogView = LayoutInflater.from(binding.getRoot().getContext()).inflate(R.layout.cart_item_qty_picker_dialog, null);
                    builder.setView(dialogView);
                    ImageView closeDialog = dialogView.findViewById(R.id.img_close_qty_list_dialog);
                    RecyclerView rvQuantityList = dialogView.findViewById(R.id.rv_qty_list);
                    setRVProperties(rvQuantityList, cartItems);
                    dialog = builder.create();
                    closeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            };
        }

        private void setRVProperties(RecyclerView rvQuantityList, MStarProductDetails cartItems) {
            rvQuantityList.setNestedScrollingEnabled(false);
            rvQuantityList.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));
            int maxQuantity = cartItems.getStockQty() < cartItems.getMaxQtyInOrder() ? cartItems.getStockQty() : cartItems.getMaxQtyInOrder();
            rvQuantityList.setAdapter(getQuantityAdapter(binding.getRoot().getContext(), getQuantityList(maxQuantity, cartItems.getCartQuantity()), cartItems));
            rvQuantityList.scrollToPosition(scrolledPosition);
        }

        private List<MStarCartAdapter.ItemQuantity> getQuantityList(Integer quantity, Integer cartResultQuantity) {
            List<MStarCartAdapter.ItemQuantity> itemQuantityList = new ArrayList<>();
            for (int i = 1; i <= quantity; i++) {
                MStarCartAdapter.ItemQuantity itemQuantity = new MStarCartAdapter.ItemQuantity();
                itemQuantity.setQuantity(String.valueOf(i));
                itemQuantity.setQuantitySelected((i == cartResultQuantity));
                scrolledPosition = (i == cartResultQuantity) ? i - 1 : scrolledPosition;
                itemQuantityList.add(itemQuantity);
            }
            return itemQuantityList;
        }

        public View.OnClickListener removeProduct(final MStarProductDetails productDetails) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRemoveProductFromCart(productDetails, productDetails.getCartQuantity(), true);
                    productDetails.setCartQuantity(0);
                    binding.buyAgainDeleteProduct.setVisibility(View.GONE);
                    binding.buyAgainTvItemQtyLl.setVisibility(View.GONE);
                    binding.prodAddToCart.setVisibility(View.VISIBLE);
                    binding.cartDivider.setVisibility(View.VISIBLE);


                }

            };
        }
    }

    private CartItemQtyAdapter getQuantityAdapter(Context context, List<MStarCartAdapter.ItemQuantity> quantityList, MStarProductDetails cartItems) {
        cartItemQtyAdapter = new CartItemQtyAdapter(context, quantityList, cartItems, this);
        return cartItemQtyAdapter;

    }

    public interface BuyAgainAdapterListener {

        void onProductView(int productCode);

        void onRemoveProductFromCart(MStarProductDetails productDetails, int quantity, boolean isRemoveAll);

        void decreaseQuantity(MStarProductDetails productDetails, int quantity, int selectedQty);

        void increaseQuantity(MStarProductDetails productDetails, int quantity, int selectedQty);
    }
}
