package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterSearchBinding;
import com.netmedsmarketplace.netmeds.viewmodel.ProductListViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.VisibleItemListener;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.model.MstarBasicResponseResultTemplateModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MstarCategoryProductAdapter extends RecyclerView.Adapter<MstarCategoryProductAdapter.PopularProductHolder> {

    private final Context context;
    private List<MStarProductDetails> mStarProductDetailsList;
    private final MstarCategoryProductAdapter.AlternateProductAdapterListener adapterListener;
    private final boolean viewAll;
    private boolean do_animate = true;
    private MstarBasicResponseResultTemplateModel templateModel;
    private String imageUrl;
    private String intentFrom;
    private List<String> defaultBannerList;
    private List<MstarBanner> mstarBannersList;
    private boolean isPeopleAlsoView = false;

    public MstarCategoryProductAdapter(final Context context,List<MStarProductDetails> mStarProductDetailsList, MstarCategoryProductAdapter.AlternateProductAdapterListener adapterListener, boolean viewAll, String intentFrom, List<String> defaultBannerList, List<MstarBanner> mstarBannersList, boolean isPeopleAlsoView) {
        this.context = context;
        this.adapterListener = adapterListener;
        this.mStarProductDetailsList = mStarProductDetailsList;
        this.viewAll = viewAll;
        this.intentFrom = intentFrom;
        this.defaultBannerList = defaultBannerList;
        this.mstarBannersList = mstarBannersList;
        this.isPeopleAlsoView = isPeopleAlsoView;
        MStarBasicResponseTemplateModel model = new Gson().fromJson(BasePreference.getInstance(context).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        if (model != null) {
            this.templateModel = model.getResult();
            imageUrl = templateModel.getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/";
        }

        ProductListViewModel.setVisibleItemListener(new VisibleItemListener() {
            @Override
            public void viewVisiblePosition(int firstVisiblePosition, int lastVisiblePosition) {
                if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < getProductDetailList().size()) {
                    List<MStarProductDetails> impressionList = getProductDetailList().subList(firstVisiblePosition, lastVisiblePosition + 1);
                    /*Google Tag Manager + FireBase Product Impression Event*/
                    FireBaseAnalyticsHelper.getInstance().logFireBaseProductImpressionEvent(context, impressionList, firstVisiblePosition+1, FireBaseAnalyticsHelper.VIEW_ON_PAGE, getTitle());
                }
            }
        });
    }

    private List<MStarProductDetails> getProductDetailList(){
        return mStarProductDetailsList;
    }

    @NonNull
    @Override
    public MstarCategoryProductAdapter.PopularProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterSearchBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_search, parent, false);
        return new MstarCategoryProductAdapter.PopularProductHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PopularProductHolder holder, int position) {
        if (mstarBannersList != null && !mstarBannersList.isEmpty()) {
            if (position == 0) {
                holder.bindBannerInFirstPosition();
            } else {
                holder.BindViewData(position - 1);
            }
        } else {
            holder.BindViewData(position);
        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private boolean isRxRequired(MStarProductDetails productDetails) {
        return productDetails != null && productDetails.isRxRequired();
    }

    @Override
    public int getItemCount() {
        return mStarProductDetailsList.size() < 3 || viewAll ? mStarProductDetailsList.size() : 3;
    }


    public void updateProductList(List<MStarProductDetails> productDetailsList, boolean clearList) {
        if (clearList) {
            this.mStarProductDetailsList = new ArrayList<>();
            notifyDataSetChanged();
        }
        if (mStarProductDetailsList != null) {
            int size = mStarProductDetailsList.size();
            this.mStarProductDetailsList.addAll(productDetailsList);
            notifyItemRangeInserted(size, productDetailsList.size());
        }
    }

    class PopularProductHolder extends RecyclerView.ViewHolder implements OfferAdapter.BannerOfferInterface {
        private final AdapterSearchBinding adapterSearchBinding;

        PopularProductHolder(final AdapterSearchBinding itemBinding) {
            super(itemBinding.getRoot());
            this.adapterSearchBinding = itemBinding;
        }

        private void bindBannerInFirstPosition() {
            adapterSearchBinding.alternateBrainProductView.setVisibility(View.GONE);
            adapterSearchBinding.bannerList.setVisibility(View.VISIBLE);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            OfferAdapter offerAdapter = new OfferAdapter(mstarBannersList, "", defaultBannerList, this);
            adapterSearchBinding.bannerList.setLayoutManager(layoutManager);
            adapterSearchBinding.bannerList.setAdapter(offerAdapter);
            CommonUtils.snapHelper(adapterSearchBinding.bannerList);
            /*Google Tag Manager + FireBase Promotion Impression Event*/
            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(context,mstarBannersList,getTitle()+"_"+FireBaseAnalyticsHelper.MAIN,1);
        }

        private void BindViewData(final int position) {
            CommonUtils.setAnimation(adapterSearchBinding.alternateBrainProductView, do_animate, 100, context);
            final MStarProductDetails mStarProductDetails = mStarProductDetailsList.get(position);
            adapterSearchBinding.bannerList.setVisibility(View.GONE);
            adapterSearchBinding.alternateBrainProductView.setVisibility(View.VISIBLE);

            String imagePath = mStarProductDetails != null && mStarProductDetails.getImagePaths() != null ? mStarProductDetails.getImagePaths().get(0) : "";

            RequestOptions options = new RequestOptions()
                    .circleCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);


            if (context != null) {
                if (TextUtils.isEmpty(imagePath) && isPeopleAlsoView) {
                    Glide.with(context).load(mStarProductDetails != null && !TextUtils.isEmpty(mStarProductDetails.getUrlPath()) ? mStarProductDetails.getUrlPath() : "")
                            .apply(options).into(adapterSearchBinding.alternateBrainProductImage);
                } else {
                    Glide.with(context).load(imagePath.contains(AppConstant.HTTPS) ? ImageUtils.checkImage(imagePath) : ImageUtils.checkImage(getImageBasePath() + imagePath))
                            .apply(options).into(adapterSearchBinding.alternateBrainProductImage);
                }

            }

            if (isRxRequired(mStarProductDetails)) {
                adapterSearchBinding.alternateBrainProductImage.setVisibility(View.GONE);
                adapterSearchBinding.alternateBrainRxImage.setVisibility(View.VISIBLE);
            } else {
                adapterSearchBinding.alternateBrainProductImage.setVisibility(View.VISIBLE);
                adapterSearchBinding.alternateBrainRxImage.setVisibility(View.GONE);
            }

            adapterSearchBinding.alternateBrainDrugName.setText(!TextUtils.isEmpty(mStarProductDetails.getDisplayName()) ? mStarProductDetails.getDisplayName() : "");

            adapterSearchBinding.alternateBrainStrikePriceMrp.setVisibility(BigDecimal.ZERO.equals(mStarProductDetails.getMrp()) || mStarProductDetails.getSellingPrice().equals(mStarProductDetails.getMrp()) ? View.GONE : View.VISIBLE);
            if (isPeopleAlsoView) {
                String sellingPrice = mStarProductDetails.getSellingPrice().toString();
                String mrp = mStarProductDetails.getMrp().toString();
                String[] separatedSellingPrice = sellingPrice.split("\\.");
                String[] separatedMrp = mrp.split("\\.");
                if (separatedSellingPrice.length > 0 && separatedMrp.length > 0) {
                    adapterSearchBinding.searchAlternateBrainStrikePrice.setVisibility(BigDecimal.ZERO.equals(mStarProductDetails.getMrp()) || separatedSellingPrice[0].equals(separatedMrp[0]) ? View.GONE : View.VISIBLE);
                    adapterSearchBinding.alternateBrainStrikePriceMrp.setVisibility(BigDecimal.ZERO.equals(mStarProductDetails.getMrp()) || separatedSellingPrice[0].equals(separatedMrp[0]) ? View.GONE : View.VISIBLE);
                }
            } else {
                adapterSearchBinding.searchAlternateBrainStrikePrice.setVisibility(BigDecimal.ZERO.equals(mStarProductDetails.getMrp()) || mStarProductDetails.getSellingPrice().equals(mStarProductDetails.getMrp()) ? View.GONE : View.VISIBLE);
                adapterSearchBinding.alternateBrainStrikePriceMrp.setVisibility(BigDecimal.ZERO.equals(mStarProductDetails.getMrp()) || mStarProductDetails.getSellingPrice().equals(mStarProductDetails.getMrp()) ? View.GONE : View.VISIBLE);
            }
            adapterSearchBinding.alternateBrainAlgoliaPriceMrp.setVisibility(adapterSearchBinding.searchAlternateBrainStrikePrice.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

            adapterSearchBinding.searchAlternateBrainalgoliaPrice.setText(!TextUtils.isEmpty(mStarProductDetails.getSellingPrice().toString()) ? String.format("\u20B9 %.2f", mStarProductDetails.getSellingPrice()) : "\u20B9 0.00");
            adapterSearchBinding.searchAlternateBrainStrikePrice.setText(!TextUtils.isEmpty(mStarProductDetails.getMrp().toString()) ? String.format("\u20B9 %.2f", mStarProductDetails.getMrp()) : "");
            adapterSearchBinding.searchAlternateBrainStrikePrice.setPaintFlags(adapterSearchBinding.searchAlternateBrainStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if (!TextUtils.isEmpty(mStarProductDetails.getAvailabilityStatus()) && mStarProductDetails.getAvailabilityStatus().equalsIgnoreCase("A") || mStarProductDetails.isForPeopleAlsoViewed()) {
                adapterSearchBinding.alternateBrainAddCartButton.setVisibility(View.VISIBLE);
                adapterSearchBinding.alternateBrainCartDivider.setVisibility(View.VISIBLE);
            } else {
                adapterSearchBinding.alternateBrainAddCartButton.setVisibility(View.GONE);
                adapterSearchBinding.alternateBrainCartDivider.setVisibility(View.GONE);
            }

            adapterSearchBinding.alternateBrainCategoryName.setText(getCategoryName(mStarProductDetails,AppConstant.CATEGORY_LEVEL_1));
            adapterSearchBinding.categoryName2.setText(getCategoryName(mStarProductDetails,AppConstant.CATEGORY_LEVEL_2));
            adapterSearchBinding.alternateBrainCategoryName.setVisibility(TextUtils.isEmpty(getCategoryName(mStarProductDetails,AppConstant.CATEGORY_LEVEL_1))?View.GONE:View.VISIBLE);
            adapterSearchBinding.categoryName2.setVisibility(TextUtils.isEmpty(getCategoryName(mStarProductDetails,AppConstant.CATEGORY_LEVEL_2))?View.GONE:View.VISIBLE);

            adapterSearchBinding.alternateBrainManufacturerName.setText(mStarProductDetails.getManufacturer() != null && !TextUtils.isEmpty(mStarProductDetails.getManufacturer().getName()) ? String.format("" + AppConstant.MFR + " %s", mStarProductDetails.getManufacturer().getName()) : "");
            adapterSearchBinding.alternateBrainManufacturerName.setVisibility(mStarProductDetails.getManufacturer() != null && !TextUtils.isEmpty(mStarProductDetails.getManufacturer().getName()) ? View.VISIBLE : View.GONE);
            adapterSearchBinding.alternateBrainDrugDetail.setText(!TextUtils.isEmpty(mStarProductDetails.getPackLabel()) ? mStarProductDetails.getPackLabel() : "");
            adapterSearchBinding.alternateBrainDrugDetail.setVisibility(TextUtils.isEmpty(mStarProductDetails.getPackLabel()) ? View.GONE : View.VISIBLE);

            adapterSearchBinding.alternateBrainManufacturerName.setOnClickListener(manufactureNameClickListener(position));

            adapterSearchBinding.alternateBrainRxRequired.setVisibility(isRxRequired(mStarProductDetails) ? View.VISIBLE : View.GONE);

            adapterSearchBinding.alternateBrainAddCartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterListener.OnAddCartCallBack(mStarProductDetails, position + 1);
                }
            });
            adapterSearchBinding.alternateBrainProductView.setOnClickListener(productDetailsListener(mStarProductDetails.getProductCode(), mStarProductDetails, position));
            adapterSearchBinding.searchDivider.setVisibility(position == mStarProductDetailsList.size() - 1 ? View.GONE : View.VISIBLE);

        }

        @Override
        public void imageClickListener(String bannerFrom, MstarBanner mstarBanner) {
            adapterListener.imageClickListener(bannerFrom, mstarBanner);
        }

        @Override
        public String getTitleText() {
            return null;
        }
    }

    private View.OnClickListener productDetailsListener(final int productCode, final MStarProductDetails productDetails, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterListener.openProductDetailsPage(productCode, productDetails, position);
            }
        };
    }

    private View.OnClickListener manufactureNameClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppConstant.MANUFACTURER.equalsIgnoreCase(intentFrom)) {
                    adapterListener.openManufacturerProductList(mStarProductDetailsList.get(position).getManufacturer().getId(), mStarProductDetailsList.get(position).getManufacturer().getName());
                }
            }
        };
    }

    private String getImageBasePath() {
        return !adapterListener.isFilterApplied() ? templateModel != null && !TextUtils.isEmpty(templateModel.getProductImageUrlBasePath()) ? templateModel.getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" : "" : !TextUtils.isEmpty(templateModel.getCatalogImageUrlBasePath()) ? templateModel.getCatalogImageUrlBasePath() : "";
    }

    private String getTitle(){
        return adapterListener.getPageTitle();
    }

    private String getCategoryName(MStarProductDetails productDetails, int level) {
        String categoryName = "";
        if (productDetails != null && productDetails.getCategories() != null && productDetails.getCategories().size() > 0) {
            MStarCategory mStarCategory = productDetails.getCategories().get(0);
            if (mStarCategory != null && mStarCategory.getBreadCrumbs() != null && mStarCategory.getBreadCrumbs().size() > 0) {
                List<MStarBreadCrumb> mStarBreadCrumbs = mStarCategory.getBreadCrumbs();
                for (MStarBreadCrumb breadCrumb : mStarBreadCrumbs) {
                    if (breadCrumb.getLevel() == level) {
                        return breadCrumb.getName();
                    }
                }
            }
        }
        return categoryName;
    }

    public interface AlternateProductAdapterListener {

        void OnAddCartCallBack(MStarProductDetails productDetails, int position);

        void openManufacturerProductList(Integer manufacturerId, String ManufacturerName);

        void openProductDetailsPage(int productCode, MStarProductDetails productDetails, int position);

        boolean isFilterApplied();

        void imageClickListener(String bannerFrom, MstarBanner mstarBanner);

        String getPageTitle();
    }
}