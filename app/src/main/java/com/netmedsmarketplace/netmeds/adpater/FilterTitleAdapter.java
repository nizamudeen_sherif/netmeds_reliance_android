package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.model.MstarAlgoliaFilterTitle;

import java.util.List;

public class FilterTitleAdapter extends RecyclerView.Adapter<FilterTitleAdapter.FilterTitleHolder> {
    private List<MstarAlgoliaFilterTitle> filterTitleList;
    private final FilterTitleListener mListener;
    private final Context mContext;
    private String titleSelection;

    public FilterTitleAdapter(Context context, List<MstarAlgoliaFilterTitle> filterTitleList, FilterTitleListener listener, String titleSelection) {
        this.mContext = context;
        this.filterTitleList = filterTitleList;
        this.mListener = listener;
        this.titleSelection = titleSelection;
    }


    @NonNull
    @Override
    public FilterTitleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_filter_title, viewGroup, false);
        return new FilterTitleHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterTitleHolder holder, int position) {
        MstarAlgoliaFilterTitle mstarAlgoliaFilterTitle = filterTitleList.get(position);
        holder.name.setText(mstarAlgoliaFilterTitle.getTitle());
        if (mstarAlgoliaFilterTitle.getKey().equalsIgnoreCase(titleSelection))
            holder.name.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        else
            holder.name.setBackgroundColor(mContext.getResources().getColor(R.color.colorPaleGrey));
        if (filterTitleList.size() - 1 == position)
            holder.viewSeparator.setVisibility(View.INVISIBLE);
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleSelection = filterTitleList.get(holder.getAdapterPosition()).getKey();
                mListener.setFilterOption(filterTitleList.get(holder.getAdapterPosition()));
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return filterTitleList.size();
    }

    public interface FilterTitleListener {
        void setFilterOption(MstarAlgoliaFilterTitle filterTitle);
    }

    class FilterTitleHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final View viewSeparator;

        FilterTitleHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            viewSeparator = itemView.findViewById(R.id.viewSeperator);
        }
    }
}
