package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.PrimeHomeOfferItemBinding;

import java.util.List;
import java.util.Locale;

public class PrimeHomeOfferAdapter extends RecyclerView.Adapter<PrimeHomeOfferAdapter.PrimeHomeOffersViewHolder> {
    private List<String> offerList;
    private Context context;

    public PrimeHomeOfferAdapter(Context context, List<String> offerList) {
        this.context = context;
        this.offerList = offerList;
    }

    @NonNull
    @Override
    public PrimeHomeOffersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        PrimeHomeOfferItemBinding primeHomeOfferItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.prime_home_offer_item, viewGroup, false);
        return new PrimeHomeOffersViewHolder(primeHomeOfferItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PrimeHomeOffersViewHolder viewHolder, int position) {
        String offerText = offerList.get(position);
        viewHolder.primeHomeOfferItemBinding.primeHomeOffer.setText(String.format(Locale.getDefault(), "%s %s", context.getResources().getString(R.string.text_bullet_point_diagnostic), offerText.trim()));
    }

    @Override
    public int getItemCount() {
        return offerList != null ? offerList.size() : 0;
    }

    class PrimeHomeOffersViewHolder extends RecyclerView.ViewHolder {
        private PrimeHomeOfferItemBinding primeHomeOfferItemBinding;

        PrimeHomeOffersViewHolder(PrimeHomeOfferItemBinding primeHomeOfferItemBinding) {
            super(primeHomeOfferItemBinding.getRoot());
            this.primeHomeOfferItemBinding = primeHomeOfferItemBinding;
        }
    }
}
