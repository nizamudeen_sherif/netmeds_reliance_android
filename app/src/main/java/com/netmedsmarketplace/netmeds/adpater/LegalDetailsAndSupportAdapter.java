package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterLegalHelpItemBinding;
import com.netmedsmarketplace.netmeds.model.FAQCategoryModel;

import java.util.List;

public class LegalDetailsAndSupportAdapter extends RecyclerView.Adapter<LegalDetailsAndSupportAdapter.LegalDetailsAndSupportAdapterViewHolder> {

    private final List<FAQCategoryModel> modelList;
    private final LegalDetailsAndSupportAdapterListener listener;

    public LegalDetailsAndSupportAdapter(List<FAQCategoryModel> modelList, LegalDetailsAndSupportAdapterListener listener) {
        this.modelList = modelList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public LegalDetailsAndSupportAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterLegalHelpItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_legal_help_item, viewGroup, false);
        return new LegalDetailsAndSupportAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LegalDetailsAndSupportAdapterViewHolder holder, int i) {
        FAQCategoryModel model = modelList.get(i);
        holder.binding.tvTitle.setText(model.getName());
        holder.binding.divider.setVisibility(i + 1 == getItemCount() ? View.GONE : View.VISIBLE);
        holder.binding.getRoot().setOnClickListener(onItemClick(model));
    }

    private View.OnClickListener onItemClick(final FAQCategoryModel model) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(model);
            }
        };
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public interface LegalDetailsAndSupportAdapterListener {
        void onItemClick(FAQCategoryModel model);
    }

    class LegalDetailsAndSupportAdapterViewHolder extends RecyclerView.ViewHolder {
        private final AdapterLegalHelpItemBinding binding;

        LegalDetailsAndSupportAdapterViewHolder(AdapterLegalHelpItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
