/*
 *  "(c) NetmedsMarketPlace.  All rights reserved"
 */

package com.netmedsmarketplace.netmeds.adpater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterOfferBinding;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.util.ArrayList;
import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferHolder> {

    private final List<MstarBanner> offerList;
    private String bannerFrom;
    private List<String> defaultBanner;
    private List<MstarBanner> finalBannerList = new ArrayList<>();
    private BannerOfferInterface listener;
    private Context context;

    public OfferAdapter(List<MstarBanner> offerList, String bannerFrom, List<String> defaultBanner, BannerOfferInterface listener) {
        this.offerList = offerList;
        this.bannerFrom = bannerFrom;
        this.defaultBanner = defaultBanner;
        this.listener = listener;
        setFinalBannerList();
    }

    private void setFinalBannerList() {
        if (offerList != null && !offerList.isEmpty()) {
            finalBannerList.addAll(offerList);
        } else if (defaultBanner != null && !defaultBanner.isEmpty()) {
            for (String defaultBannerUrl : defaultBanner) {
                MstarBanner banner = new MstarBanner();
                banner.setDefaultBannerUrl(defaultBannerUrl);
                finalBannerList.add(banner);
            }
        }
    }

    @NonNull
    @Override
    public OfferHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        AdapterOfferBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_offer, parent, false);
        return new OfferHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.onBindData(position);
    }

    @Override
    public int getItemCount() {
        return finalBannerList.size();
    }

    class OfferHolder extends RecyclerView.ViewHolder {
        private AdapterOfferBinding binding;

        OfferHolder(AdapterOfferBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void onBindData(final int position) {
            if (finalBannerList != null && !finalBannerList.isEmpty()) {
                final MstarBanner banner = finalBannerList.get(position);
                if (banner.getImageUrl() != null) {
                    setGlideWithUrl(banner.getImageUrl(), binding.imgOffer);
                    binding.imgOffer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            /*Google Tag Manager + FireBase Promotion click Event*/
                            promotionClickEvent(banner, position);
                            listener.imageClickListener(bannerFrom, banner);
                        }
                    });
                } else {
                    setGlideWithUrl(banner.getDefaultBannerUrl(), binding.imgOffer);
                }
            }
        }
    }

    private void setGlideWithUrl(String url, ImageView imageView) {
        Glide.with(context)
                .load(url)
                .error(Glide.with(imageView).load(R.drawable.ic_no_image)).into(imageView);
    }

    private void promotionClickEvent(MstarBanner banner, int position) {
        if (bannerFrom.equals(GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER))
            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(context, banner, FireBaseAnalyticsHelper.EVENT_PARAM_HOME_TRENDING, position + 1);
        else
            FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(context, banner, listener.getTitleText() + "_" + FireBaseAnalyticsHelper.MAIN, position + 1);
    }

    public interface BannerOfferInterface {
        void imageClickListener(String bannerFrom, MstarBanner mstarBanner);

        String getTitleText();
    }
}
