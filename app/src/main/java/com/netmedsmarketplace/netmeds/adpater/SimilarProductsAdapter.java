package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterSimilarProductBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.List;

public class SimilarProductsAdapter extends RecyclerView.Adapter<SimilarProductsAdapter.SimilarProductViewHolder> {

    private List<MStarProductDetails> productList;
    private ProductCombinationInterface callback;
    private Context context;

    public SimilarProductsAdapter(List<MStarProductDetails> list, ProductCombinationInterface callback, Context context) {
        this.productList = list;
        this.callback = callback;
        this.context = context;
    }

    @NonNull
    @Override
    public SimilarProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterSimilarProductBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_similar_product, viewGroup, false);
        return new SimilarProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SimilarProductViewHolder holder, int i) {
        holder.bindSimilarProduct();
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class SimilarProductViewHolder extends RecyclerView.ViewHolder {
        AdapterSimilarProductBinding binding;

        SimilarProductViewHolder(@NonNull AdapterSimilarProductBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bindSimilarProduct() {
            MStarProductDetails productDetails = productList.get(getAdapterPosition());
            boolean isDiscountAvailable = productDetails != null && productDetails.getDiscount().compareTo(BigDecimal.ZERO) > 0;
            if (context != null) {
                Glide.with(context)
                        .load(ImageUtils.checkImage(callback.getImageUrl(productDetails)))
                        .error(Glide.with(binding.spSimilarProductImage).load(R.drawable.ic_no_image))
                        .into(binding.spSimilarProductImage);
            }
            binding.spSimilarProductName.setText(productDetails != null && !TextUtils.isEmpty(productDetails.getDisplayName()) ? productDetails.getDisplayName() : "");
            binding.spSimilarProductDescription.setText(productDetails != null && !TextUtils.isEmpty(productDetails.getPackLabel()) ? productDetails.getPackLabel() : "");
            binding.spSimilarProductDescription.setVisibility(productDetails != null && !TextUtils.isEmpty(productDetails.getPackLabel()) ? View.VISIBLE : View.GONE);
            binding.spSimilarPrice.setText(productDetails != null ? CommonUtils.getPriceInFormat(productDetails.getSellingPrice()) : "");
            binding.spSimilarStrikePrice.setText(isDiscountAvailable ? CommonUtils.getPriceInFormat(productDetails.getMrp()) : "");
            binding.spSimilarStrikePrice.setPaintFlags(binding.spSimilarStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.spSimilarStrikePrice.setVisibility(isDiscountAvailable ? View.VISIBLE : View.GONE);
            binding.mrpSpSimilarStrikePrice.setVisibility(isDiscountAvailable ? View.VISIBLE : View.GONE);
            binding.mrpSpSimilarPrice.setVisibility(isDiscountAvailable ? View.GONE : View.VISIBLE);
            binding.spSimilarAddCartButton.setOnClickListener(callback.launchAddToCartBottomSheet(productDetails));
            binding.spMainLayout.setOnClickListener(callback.navigateToProduct(productDetails.getProductCode()));
        }
    }

    public interface ProductCombinationInterface {
        Context getContext();

        String getImageUrl(MStarProductDetails productDetails);

        View.OnClickListener navigateToProduct(int sku);

        View.OnClickListener launchAddToCartBottomSheet(MStarProductDetails skuId);
    }
}