package com.netmedsmarketplace.netmeds.adpater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FilterBrandItemBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarFacetItemDetails;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.List;

public class FilterBrandAdapter extends RecyclerView.Adapter<FilterBrandAdapter.WellnessHolder> {
    private Context context;
    private List<MstarFacetItemDetails> brandList;
    private String imageURLPath;
    private Integer categoryId;
    private BrandFilterListener listener;

    FilterBrandAdapter(Context context, List<MstarFacetItemDetails> brandList, Integer categoryId, BrandFilterListener listener) {
        this.context = context;
        this.brandList = brandList;
        this.categoryId = categoryId;
        this.listener = listener;
        MStarBasicResponseTemplateModel response = new Gson().fromJson(BasePreference.getInstance(context).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageURLPath = response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getCatalogImageUrlBasePath()) ? response.getResult().getManufactureImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" : "";
    }

    @NonNull
    @Override
    public WellnessHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FilterBrandItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.filter_brand_item, parent, false);
        return new WellnessHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull WellnessHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (brandList != null) {
            if (context != null && context.getApplicationContext() != null)
                Glide.with(context).load(brandList.get(position).getUrlPath() != null ? imageURLPath + brandList.get(position).getUrlPath() : "").apply(RequestOptions.circleCropTransform()).error(Glide.with(holder.binding.image).load(R.drawable.ic_no_image).apply(RequestOptions.circleCropTransform())).into(holder.binding.image);
            holder.binding.textView.setText(!TextUtils.isEmpty(brandList.get(position).getName()) ? brandList.get(position).getName() : "");
            holder.binding.textView.setVisibility(!TextUtils.isEmpty(brandList.get(position).getUrlPath()) ? View.GONE : View.VISIBLE);
            holder.binding.image.setVisibility(!TextUtils.isEmpty(brandList.get(position).getUrlPath()) ? View.VISIBLE : View.GONE);
            holder.binding.textView.setVisibility(View.VISIBLE);
            holder.binding.image.setVisibility(View.GONE);
            holder.binding.constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.applyBrandFilter(brandList.get(position).getId(), brandList.get(position).getName());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return brandList.size();
    }

    class WellnessHolder extends RecyclerView.ViewHolder {
        private FilterBrandItemBinding binding;

        WellnessHolder(FilterBrandItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public FilterBrandItemBinding getBinding() {
            return binding;
        }
    }

    public interface BrandFilterListener {
        void applyBrandFilter(Integer brandId, String name);
    }
}
