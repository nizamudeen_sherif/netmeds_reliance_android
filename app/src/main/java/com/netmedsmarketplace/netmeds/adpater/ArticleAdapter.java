package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.nmp.android.netmeds.navigation.LaunchIntentManager;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.ArticleList;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleHolder> {
    private final List<ArticleList> offerList;
    private final Context mContext;

    public ArticleAdapter(Context mContext, List<ArticleList> offerList) {
        this.offerList = offerList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ArticleAdapter.ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_article, parent, false);
        return new ArticleAdapter.ArticleHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleAdapter.ArticleHolder holder, int position) {
        final ArticleList articleList = offerList.get(position);
        if (mContext.getApplicationContext() != null) {
            Glide.with(mContext).load(articleList.getImageName()).into(holder.imgOffer);
        }
        holder.description.setText(articleList.getTitle());
        holder.imgOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Google Analytics Click Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(mContext, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_OTHER_ACTIONS, GoogleAnalyticsHelper.EVENT_ACTION_BLOGS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(IntentConstant.WEB_PAGE_URL, articleList.getUrl());
                intent.putExtra(IntentConstant.PAGE_TITLE_KEY, "");
                intent.putExtra(IntentConstant.IS_FROM_ARTICLE, true);
                LaunchIntentManager.routeToActivity(mContext.getResources().getString(R.string.route_netmeds_web_view), intent, mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    class ArticleHolder extends RecyclerView.ViewHolder {
        private final ImageView imgOffer;
        private final TextView description;

        ArticleHolder(View itemView) {
            super(itemView);
            imgOffer = itemView.findViewById(R.id.image);
            description = itemView.findViewById(R.id.description);
        }
    }
}
