package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.SubscriptionHelper;
import com.nms.netmeds.base.databinding.AdapterMstarReviewPageItemRowBinding;
import com.nms.netmeds.base.model.MStarProductDetails;

import java.math.BigDecimal;
import java.util.List;

public class MStarReviewPageItemAdapter extends RecyclerView.Adapter<MStarReviewPageItemAdapter.MStarRevivePageItemViewHolder> {

    private Context context;
    private List<MStarProductDetails> lineItemList;
    private List<String> primeProductCodeList;
    private MStarReviewPageItemAdapterCallback callback;

    private String coldStorageProduct = "";
    private boolean isFromRefillSubscription = false;
    private String imageUrl;

    public MStarReviewPageItemAdapter(List<MStarProductDetails> lineItemList,
                                      boolean isFromRefillSubscription, MStarReviewPageItemAdapterCallback callback, List<String> primeProductCodeList, String imageUrl) {
        this.lineItemList = lineItemList;
        this.callback = callback;
        this.isFromRefillSubscription = isFromRefillSubscription;
        this.primeProductCodeList = primeProductCodeList;
        this.imageUrl = imageUrl;
    }

    @NonNull
    @Override
    public MStarRevivePageItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        context = viewGroup.getContext();
        AdapterMstarReviewPageItemRowBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_mstar_review_page_item_row, viewGroup, false);
        return new MStarRevivePageItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MStarRevivePageItemViewHolder holder, int i) {
        holder.onBindViewsAndProperties();
    }

    @Override
    public int getItemCount() {
        return lineItemList.size();
    }

    class MStarRevivePageItemViewHolder extends RecyclerView.ViewHolder {
        private final AdapterMstarReviewPageItemRowBinding binding;

        MStarRevivePageItemViewHolder(final AdapterMstarReviewPageItemRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }

        public void onBindViewsAndProperties() {
            final MStarProductDetails cartLineItem = lineItemList.get(getAdapterPosition());
            boolean isItemIsAPrimeProduct = primeProductCodeList.contains(String.valueOf(cartLineItem.getProductCode()));
            binding.drugName.setText(cartLineItem.getDisplayName());
            binding.manufacturerName.setText(AppConstant.MFR.concat(" ").concat(cartLineItem.getManufacturerName()));
            binding.quantity.setText(String.valueOf(cartLineItem.getCartQuantity()));
            binding.manufacturerName.setVisibility(isItemIsAPrimeProduct || TextUtils.isEmpty(cartLineItem.getPackSize()) ? View.GONE : View.VISIBLE);
            binding.drugDetail.setVisibility(View.GONE);
            binding.tvItemSeller.setText(!TextUtils.isEmpty(cartLineItem.getManufacturerName()) ? String.format("By %s", cartLineItem.getManufacturerName()) : "");
            binding.confirmationView.setVisibility(isItemIsAPrimeProduct ? View.GONE : View.VISIBLE);
            binding.rxImage.setVisibility(cartLineItem.isRxRequired() ? View.VISIBLE : View.GONE);
            binding.algoliaPrice.setText(CommonUtils.getPriceInFormat(cartLineItem.getSellingPrice().multiply(BigDecimal.valueOf(cartLineItem.getCartQuantity()))));
            binding.strikePrice.setPaintFlags(binding.strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.strikePrice.setVisibility(isDiscountAvailable(cartLineItem.getLineProductDiscount()) ? View.VISIBLE : View.GONE);
            binding.strikePrice.setText(CommonUtils.getPriceInFormat(cartLineItem.getMrp().multiply(BigDecimal.valueOf(cartLineItem.getCartQuantity()))));
            binding.tvItemExpiry.setText(String.format(context.getString(R.string.text_items_expiry), cartLineItem.getItemExpiry()));
            binding.tvItemExpiry.setVisibility(View.VISIBLE);
            binding.deliveryEstimationView.setVisibility(!TextUtils.isEmpty(cartLineItem.getDeliveryEstimate()) && !isFromRefillSubscription && !SubscriptionHelper.getInstance().isThisIssueOrder() && !SubscriptionHelper.getInstance().isSubscriptionEdit() && !SubscriptionHelper.getInstance().isPayNowSubscription() ? View.VISIBLE : View.GONE);
            binding.tvErrorMessage.setVisibility(cartLineItem.isColdStorage() && (View.GONE == binding.deliveryEstimationView.getVisibility()) ? View.VISIBLE : View.GONE);
            binding.llSellerExpiry.setVisibility(getSellerExpiryVisibility(cartLineItem, isItemIsAPrimeProduct));
            binding.divider.setVisibility(getAdapterPosition() == getItemCount() - 1 ? View.GONE : View.VISIBLE);
            if (cartLineItem.isRxRequired()) {
                callback.isRxRequired();
                binding.drugDetail.setText(!TextUtils.isEmpty(cartLineItem.getPackSize()) ? cartLineItem.getPackSize() + context.getString(R.string.text_tablets_strip) : "");
            } else {
                binding.drugDetail.setText(!TextUtils.isEmpty(cartLineItem.getPackSize()) ? context.getString(R.string.text_pack_of_size) + cartLineItem.getPackSize() + context.getString(R.string.text_s) : "");
            }
            if (!TextUtils.isEmpty(cartLineItem.getItemSeller())) {
                boolean isSellerAvailable = !TextUtils.isEmpty(cartLineItem.getSellerAddress());
                binding.tvItemSeller.setEnabled(isSellerAvailable);
                binding.tvItemSeller.setText(getSellerName(String.format(context.getString(R.string.text_items_seller), (cartLineItem.getItemSeller())), isSellerAvailable));
            }
            if (!TextUtils.isEmpty(cartLineItem.getDeliveryEstimate())) {
                binding.deliveryEstimate.setText(String.format(" %s", cartLineItem.getDeliveryEstimate()));
            }

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context).load(!TextUtils.isEmpty(cartLineItem.getProduct_image_path()) ? imageUrl + cartLineItem.getProduct_image_path() : R.drawable.ic_no_image)
                    .apply(options)
                    .into(binding.productImage);

            binding.tvItemSeller.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.sellerDetailView(cartLineItem.getItemSeller(), cartLineItem.getSellerAddress());
                }
            });

        }

        private boolean isDiscountAvailable(BigDecimal discount) {
            return discount.compareTo(BigDecimal.ZERO) > 0;
        }

        private int getSellerExpiryVisibility(MStarProductDetails cartLineItem, boolean isPrimProduct) {
            if (isPrimProduct) {
                return View.GONE;
            }
            return !TextUtils.isEmpty(cartLineItem.getDeliveryEstimate()) && !isFromRefillSubscription ? View.VISIBLE : View.GONE;
        }


        private SpannableString getSellerName(final String sellerName, boolean isSellerAvailable) {
            SpannableString name = new SpannableString(sellerName);
            if (isSellerAvailable)
                name.setSpan(new UnderlineSpan(), 9, name.length(), 0);
            return name;
        }
    }

    public interface MStarReviewPageItemAdapterCallback {

        void sellerDetailView(String sellerName, String sellerAddress);

        void isRxRequired();
    }
}
