package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterSubCategoryItemBinding;
import com.nms.netmeds.base.model.MstarStatusItem;

import java.util.List;

public class OrderFilterSubCategoryAdapter extends RecyclerView.Adapter<OrderFilterSubCategoryAdapter.ViewHolder> {

    private OrderFilterSubCategoryAdapterListener listener;
    private List<MstarStatusItem> orderStatusList;

    public OrderFilterSubCategoryAdapter(List<MstarStatusItem> orderStatusList, OrderFilterSubCategoryAdapterListener listener) {
        this.orderStatusList = orderStatusList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterSubCategoryItemBinding orderBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_sub_category_item, viewGroup, false);
        return new ViewHolder(orderBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return orderStatusList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private AdapterSubCategoryItemBinding binding;

        public ViewHolder(AdapterSubCategoryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(onClickCategory);
        }

        public void bind() {
            binding.setItem(orderStatusList.get(getAdapterPosition()));
        }

        View.OnClickListener onClickCategory = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.checked.isChecked()) {
                    binding.checked.setChecked(!binding.checked.isChecked());
                    orderStatusList.get(getAdapterPosition()).setChecked(binding.checked.isChecked());
                    listener.onOrderStatusSelected(orderStatusList, getAdapterPosition());
                }
            }
        };
    }

    public interface OrderFilterSubCategoryAdapterListener {
        void onOrderStatusSelected(List<MstarStatusItem> OrderStatus, int adapterPosition);
    }
}
