package com.netmedsmarketplace.netmeds.adpater;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.NetmedsApp;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterAddPrescriptionBinding;
import com.netmedsmarketplace.netmeds.databinding.AdapterUploadedImageBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MStarUploadPrescription;
import com.nms.netmeds.base.retrofit.APIPathConstant;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;

public class M2AttachPrescriptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_PRESCRIPTION = 0;
    private static final int VIEW_ADD_PRESCRIPTION = 1;
    private final ArrayList<MStarUploadPrescription> mPrescriptionList;
    private final PrescriptionListener prescriptionListener;
    private final boolean showContinueUpload;
    private final int prescriptionLimit;
    private String prescriptionUrl;

    public M2AttachPrescriptionAdapter(ArrayList<MStarUploadPrescription> prescriptionList, PrescriptionListener prescriptionListener, boolean showContinueUpload) {
        this.mPrescriptionList = prescriptionList;
        this.prescriptionListener = prescriptionListener;
        this.showContinueUpload = showContinueUpload;
        ConfigurationResponse response = new Gson().fromJson(BasePreference.getInstance(NetmedsApp.getInstance()).getConfiguration(), ConfigurationResponse.class);
        prescriptionLimit = response != null && response.getResult() != null && response.getResult().getConfigDetails() != null ? response.getResult().getConfigDetails().getUploadPrescriptionLimit() : 0;
        prescriptionUrl = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.MSTAR_DOWNLOAD_PRESCRIPTION;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == VIEW_PRESCRIPTION) {
            AdapterUploadedImageBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_uploaded_image, viewGroup, false);
            return new M2AttachPrescriptionHolder(binding);
        } else {
            AdapterAddPrescriptionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_add_prescription, viewGroup, false);
            return new M2AddPrescriptionHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof M2AttachPrescriptionHolder) {
            M2AttachPrescriptionHolder prescriptionHolder = (M2AttachPrescriptionHolder) holder;
            final MStarUploadPrescription prescription = mPrescriptionList.get(position);

            if (prescription.getBitmapImage() != null) {
                prescriptionHolder.binding.prescriptionImage.setImageBitmap(prescription.getBitmapImage());
            } else {
                GlideUrl glideUrl = CommonUtils.getGlideUrl(prescriptionUrl + prescription.getUploadedPrescriptionId(), BasePreference.getInstance(NetmedsApp.getInstance()).getMstarBasicHeaderMap());
                Glide.with(prescriptionHolder.itemView).load(glideUrl).into(prescriptionHolder.binding.prescriptionImage);
            }

            if (prescription.isOrderReview()) {
                // When coming from Order Review Page
                prescriptionHolder.binding.removePrescription.setVisibility(View.GONE);
            } else {
                prescriptionHolder.binding.removePrescription.setVisibility(prescription.isDigitalized() ? View.GONE : View.VISIBLE);
            }


            prescriptionHolder.binding.prescriptionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    prescriptionListener.clickOnPrescriptionToPreview(prescription.getBitmapImage(), position);
                }
            });
            prescriptionHolder.binding.removePrescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prescriptionListener != null)
                        prescriptionListener.removePrescription(position, mPrescriptionList);
                }
            });
        } else if (holder instanceof M2AddPrescriptionHolder) {
            M2AddPrescriptionHolder addPrescriptionHolder = (M2AddPrescriptionHolder) holder;
            addPrescriptionHolder.binding.continueUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prescriptionListener != null)
                        prescriptionListener.addPrescription();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (!showContinueUpload) {
            return mPrescriptionList.size();
        } else if (mPrescriptionList.size() == prescriptionLimit) {
            return mPrescriptionList.size();

        } else return mPrescriptionList.size() + 1;

    }

    @Override
    public int getItemViewType(int position) {
        if (!showContinueUpload) {
            return VIEW_PRESCRIPTION;
        } else {
            if (position == mPrescriptionList.size() && mPrescriptionList.size() < prescriptionLimit)
                return VIEW_ADD_PRESCRIPTION;
            else
                return VIEW_PRESCRIPTION;
        }
    }

    class M2AttachPrescriptionHolder extends RecyclerView.ViewHolder {

        AdapterUploadedImageBinding binding;

        M2AttachPrescriptionHolder(@NonNull AdapterUploadedImageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private class M2AddPrescriptionHolder extends RecyclerView.ViewHolder {

        AdapterAddPrescriptionBinding binding;

        M2AddPrescriptionHolder(AdapterAddPrescriptionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface PrescriptionListener {

        void clickOnPrescriptionToPreview(Bitmap bitmap, int pos);

        void removePrescription(int position, ArrayList<MStarUploadPrescription> prescriptionArrayList);

        void addPrescription();
    }
}
