package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterProductVariantBinding;
import com.nms.netmeds.base.model.VariantFacet;

import java.util.List;

public class ProductVariantAdapter extends RecyclerView.Adapter<ProductVariantAdapter.ProductVariantHolder> {
    private final List<VariantFacet> variantList;
    private final Context context;
    private ProductVariantListener productVariantListener;

    public ProductVariantAdapter(Context context, List<VariantFacet> variantList, ProductVariantListener productVariantListener) {
        this.context = context;
        this.variantList = variantList;
        this.productVariantListener = productVariantListener;
    }

    @NonNull
    @Override
    public ProductVariantAdapter.ProductVariantHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterProductVariantBinding adapterProductVariantBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_product_variant, parent, false);
        return new ProductVariantHolder(adapterProductVariantBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductVariantAdapter.ProductVariantHolder holder, int position) {
        holder.bindValue();
    }


    @Override
    public int getItemCount() {
        return variantList.size();
    }

    class ProductVariantHolder extends RecyclerView.ViewHolder {
        private AdapterProductVariantBinding binding;

        ProductVariantHolder(AdapterProductVariantBinding adapterProductVariantBinding) {
            super(adapterProductVariantBinding.getRoot());
            this.binding = adapterProductVariantBinding;
        }

        public void bindValue() {
            binding.setVariantData(variantList.get(getAdapterPosition()));
            initiateBinding();
        }

        private void initiateBinding() {
            final VariantFacet variantFacet = variantList.get(getAdapterPosition());
            final boolean isVariantEnable = -1 != variantFacet.getProductCode();
            binding.tvVariantName.setText(variantFacet.getValue());
            binding.getRoot().setBackground(context.getResources().getDrawable(variantFacet.isCurrentSelectedVariant() ? R.drawable.pale_blue_rounded_corner_background : R.drawable.variant_background));
            binding.tvVariantName.setTextColor(context.getResources().getColor(variantFacet.isCurrentSelectedVariant() ? R.color.colorDeepAqua : R.color.colorLightBlueGrey));
            binding.imgVariant.setImageResource(variantFacet.isCurrentSelectedVariant() ? R.drawable.ic_radio_checked : R.drawable.ic_radio_unchecked);
            disableVariantView(isVariantEnable);
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isVariantEnable) {
                        productVariantListener.onSelectedVariant(variantFacet.getProductCode());
                    }
                }
            });
        }

        private void disableVariantView(boolean isVariantEnable) {
            binding.getRoot().setAlpha(isVariantEnable ? 1f : 0.4f);
            binding.getRoot().setEnabled(isVariantEnable);
            binding.getRoot().setFocusable(isVariantEnable);
            binding.getRoot().setClickable(isVariantEnable);
        }
    }

    public interface ProductVariantListener {
        void onSelectedVariant(int variantFacet);
    }
}
