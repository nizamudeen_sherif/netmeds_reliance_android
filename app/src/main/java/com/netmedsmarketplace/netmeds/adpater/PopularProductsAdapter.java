package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.PopularProductItemBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;

import java.math.BigDecimal;
import java.util.List;

import static com.nms.netmeds.base.CommonUtils.getPriceInFormat;

public class PopularProductsAdapter extends RecyclerView.Adapter<PopularProductsAdapter.PopularProductsAdapterViewHolder> {
    private List<MStarProductDetails> list;
    private Context context;
    private PopularProductAddToCartListener listener;
    private String subHeader="";

    public PopularProductsAdapter(List<MStarProductDetails> popularProductsList,String subHeader, PopularProductAddToCartListener listener) {
        this.list = popularProductsList;
        this.subHeader=subHeader;
        this.listener = listener;
    }


    @NonNull
    @Override
    public PopularProductsAdapter.PopularProductsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        PopularProductItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.popular_product_item, viewGroup, false);
        context = viewGroup.getContext();
        return new PopularProductsAdapter.PopularProductsAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PopularProductsAdapter.PopularProductsAdapterViewHolder holder, final int position) {
        final MStarProductDetails product = list.get(position);
        int discountInPercent = (int) (product.getDiscount().doubleValue() * 100);
        holder.binding.tvUpToPercentage.setText(BigDecimal.ZERO.compareTo(product.getDiscount()) != 0 ? discountInPercent + context.getResources().getString(R.string.text_off) : "");
        holder.binding.tvActualPrice.setText(getPriceInFormat(String.valueOf(product.getSellingPrice())));
        holder.binding.tvProductName.setText(!TextUtils.isEmpty(product.getDisplayName()) ? product.getDisplayName() : "");
        holder.binding.discountPrice.setText(getPriceInFormat(String.valueOf(product.getMrp())));
        holder.binding.mrpDiscountPrice.setVisibility(!(BigDecimal.ZERO.compareTo(product.getDiscount()) == 0) ? View.VISIBLE : View.GONE);
        holder.binding.mrpTvActualPrice.setVisibility(!(BigDecimal.ZERO.compareTo(product.getDiscount()) == 0) ? View.GONE : View.VISIBLE);
        holder.binding.discountPrice.setPaintFlags(holder.binding.discountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.binding.discountPrice.setVisibility(!(BigDecimal.ZERO.compareTo(product.getDiscount()) == 0) ? View.VISIBLE : View.GONE);
        holder.binding.cvCardView.setVisibility(!(BigDecimal.ZERO.compareTo(product.getDiscount()) == 0) ? View.VISIBLE : View.INVISIBLE);
        holder.binding.lvCart.setVisibility(CommonUtils.isProductAvailableWithStock(product) ? View.VISIBLE : View.GONE);
        if (context != null && context.getApplicationContext() != null)
            Glide.with(context.getApplicationContext()).load(!TextUtils.isEmpty(product.getProduct_image_path()) ? product.getProduct_image_path() : "").into(holder.binding.productImage);
        holder.binding.parentCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoogleTagManagerEvent(product,position+1);
                listener.popularProductDetailsPageCallback(product);
            }
        });
        holder.binding.lvCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoogleTagManagerEvent(product,position+1);
                listener.PopularProductAddToCart(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PopularProductsAdapterViewHolder extends RecyclerView.ViewHolder {
        private final PopularProductItemBinding binding;

        PopularProductsAdapterViewHolder(PopularProductItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    /*Google Tag Manager + FireBase Product Click Event*/
    private void onGoogleTagManagerEvent(MStarProductDetails productDetails,int position){
        FireBaseAnalyticsHelper.getInstance().logFireBaseProductClickAndSelectionEvent(context,productDetails,position,FireBaseAnalyticsHelper.WELLNESS+"_"+subHeader);
    }

    public interface PopularProductAddToCartListener {
        void PopularProductAddToCart(MStarProductDetails product);

        void popularProductDetailsPageCallback(MStarProductDetails product);
    }
}

