package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ItemPrimeProductBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MstarPrimeProduct;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

public class PrimeMemberShipAdapter extends RecyclerView.Adapter<PrimeMemberShipAdapter.PrimeMemberShipViewHolder> {

    private List<MstarPrimeProduct> products;
    private Context context;
    private int productCodeInCart;
    private PrimeMemberShipAdapterListener memberShipAdapterListener;

    public PrimeMemberShipAdapter(List<MstarPrimeProduct> products, int productCodeInCart, PrimeMemberShipAdapterListener memberShipAdapterListener) {
        this.products = products;
        this.productCodeInCart = productCodeInCart;
        this.memberShipAdapterListener = memberShipAdapterListener;
    }

    @NonNull
    @Override
    public PrimeMemberShipViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemPrimeProductBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_prime_product, viewGroup, false);
        context = viewGroup.getContext();
        return new PrimeMemberShipViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PrimeMemberShipViewHolder holder, int position) {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return products == null ? 0 : products.size();
    }

    class PrimeMemberShipViewHolder extends RecyclerView.ViewHolder {

        private ItemPrimeProductBinding binding;

        PrimeMemberShipViewHolder(@NonNull ItemPrimeProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindView(int position) {
            final MstarPrimeProduct product = products.get(position);
            binding.productMonth.setText(product.getDuration() > 0 ? String.format(Locale.getDefault(), "%s %s", product.getDuration(), "months") : "");
            binding.txtMembership.setText(context.getResources().getString(R.string.text_membership));
            binding.price.setText(String.format("₹ %s", CommonUtils.getRoundedOffBigDecimal(product.getSellingPrice())));

            if (product.getMrp().compareTo(BigDecimal.ZERO) > 0) {
                binding.originalPrice.setText(String.format("₹ %s", CommonUtils.getRoundedOffBigDecimal(product.getMrp())));
            } else {
                binding.originalPrice.setVisibility(View.GONE);
                binding.price.setGravity(Gravity.CENTER);
            }

            binding.originalPrice.setPaintFlags(binding.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.txtRecommended.setVisibility(product.isRecommended() ? View.VISIBLE : View.GONE);

            binding.btnAddToCart.setText(product.getProductCode() == productCodeInCart ? context.getResources().getString(R.string.text_added) : context.getResources().getString(R.string.text_add_to_cart));
            binding.btnAddToCart.setBackgroundDrawable(context.getResources().getDrawable(product.getProductCode() == productCodeInCart ? R.drawable.grey_background_button : R.drawable.accent_button));
            binding.btnAddToCart.setTextColor(product.getProductCode() == productCodeInCart ? context.getResources().getColor(R.color.colorLightPaleBlueGrey) : context.getResources().getColor(R.color.colorPrimary));
            binding.btnAddToCart.setEnabled(!(product.getProductCode() == productCodeInCart));
            binding.btnAddToCart.setFocusable(!(product.getProductCode() == productCodeInCart));
            binding.btnAddToCart.setClickable(!(product.getProductCode() == productCodeInCart));

            binding.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //GA Post Event
                    String price = String.valueOf(product.getSellingPrice());
                    GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_NETMEDS_FIRST, price + "-" + GoogleAnalyticsHelper.EVENT_ACTION_ADD_TO_CART_BUTTON, GoogleAnalyticsHelper.EVENT_LABEL_ELITE_Membership);
                    PaymentHelper.setPrimePlan(product.getDuration() > 0 ? String.valueOf(product.getDuration()) : "");
                    if (!(productCodeInCart > 0)) {
                        memberShipAdapterListener.onAddToCart(product.getProductCode());
                    } else {
                        memberShipAdapterListener.setProductCodeToAdd(product.getProductCode());
                        memberShipAdapterListener.onDeleteCart(productCodeInCart);
                    }
                }
            });
        }
    }

    public interface PrimeMemberShipAdapterListener {

        void onAddToCart(int primeMemberShipProductCode);

        void onDeleteCart(int primeMemberShipProductCode);

        void setProductCodeToAdd(int productCodeToAdd);
    }
}