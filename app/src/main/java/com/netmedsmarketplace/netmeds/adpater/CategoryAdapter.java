package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterCategoryBinding;
import com.netmedsmarketplace.netmeds.ui.ProductList;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarSubCategoryResult;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {

    private Context context;
    private List<MstarSubCategoryResult> subCategoryList;
    private String imageURLPath;


    public CategoryAdapter(Context context, List<MstarSubCategoryResult> subCategoryList) {
        this.context = context;
        this.subCategoryList = subCategoryList;
        MStarBasicResponseTemplateModel response = new Gson().fromJson(BasePreference.getInstance(context).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageURLPath = response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getCatalogImageUrlBasePath()) ? response.getResult().getCatalogImageUrlBasePath() : "";
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterCategoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),R.layout.adapter_category,viewGroup,false);
        return new CategoryHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder categoryHolder, int i) {
        final MstarSubCategoryResult subCategory = subCategoryList.get(i);
        String thumbnailURL = "";
        categoryHolder.binding.subCategoryName.setText(subCategory.getName());

        if (!TextUtils.isEmpty(subCategory.getThumbnailImagePath())) {
            thumbnailURL = imageURLPath + subCategory.getThumbnailImagePath();
            Glide.with(context).load(thumbnailURL).into(categoryHolder.binding.subCategoryImageView);
        }else{
            Glide.with(context).load(R.drawable.ic_no_image).into(categoryHolder.binding.subCategoryImageView);
        }

        categoryHolder.binding.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToProductList(subCategory);
            }
        });
    }

    private void redirectToProductList(MstarSubCategoryResult subCategory) {
        Intent intent = new Intent(context, ProductList.class);
        intent.putExtra(IntentConstant.CATEGORY_ID, subCategory.getId());
        intent.putExtra(IntentConstant.FROM_CATEGORY, true);
        intent.putExtra(IntentConstant.CATEGORY_NAME, subCategory.getName());
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return subCategoryList.size();
    }

    class CategoryHolder extends RecyclerView.ViewHolder {

        private AdapterCategoryBinding binding;

        public CategoryHolder( AdapterCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
