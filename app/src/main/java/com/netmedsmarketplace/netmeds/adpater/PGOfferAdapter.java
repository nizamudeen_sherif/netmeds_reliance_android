package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.ui.ViewOfferDetailsActivity;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MstarNetmedsOffer;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.util.List;

public class PGOfferAdapter extends RecyclerView.Adapter<PGOfferAdapter.PGOfferHolder> {
    private final List<MstarNetmedsOffer> offerList;
    private final Context mContext;

    public PGOfferAdapter(Context mContext, List<MstarNetmedsOffer> offerList) {
        this.offerList = offerList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public PGOfferAdapter.PGOfferHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pg_offer, parent, false);
        return new PGOfferAdapter.PGOfferHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PGOfferAdapter.PGOfferHolder holder, final int position) {
        final MstarNetmedsOffer offerResult = offerList.get(position);
        if (mContext != null && offerResult.getImage() != null) {
            Glide.with(mContext).load(offerResult.getImage()).into(holder.imgOffer);
        }
        holder.offer.setText(offerResult.getTitle());
        holder.description.setText(offerResult.getCouponDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Google Analytics Click Event*/
                GoogleAnalyticsHelper.getInstance().postEvent(mContext, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_BANNER, GoogleAnalyticsHelper.EVENT_ACTION_OFFER_BANNER_PG, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                /*Google Tag Manager + FireBase Promotion Click Event*/
                FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(mContext,offerResult,FireBaseAnalyticsHelper.EVENT_PARAM_HOME_PAYMENT,position+1);
                if (!TextUtils.isEmpty(offerResult.getPageId()) && mContext != null) {
                    Intent intent = new Intent(mContext, ViewOfferDetailsActivity.class);
                    intent.putExtra(IntentConstant.OFFER_DETAILS_PAGE_ID, !TextUtils.isEmpty(offerResult.getPageId()) ? offerResult.getPageId() : 0);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    class PGOfferHolder extends RecyclerView.ViewHolder {
        private final ImageView imgOffer;
        private final TextView offer;
        private final TextView description;

        PGOfferHolder(View itemView) {
            super(itemView);
            imgOffer = itemView.findViewById(R.id.image);
            offer = itemView.findViewById(R.id.offer);
            description = itemView.findViewById(R.id.description);
        }
    }
}
