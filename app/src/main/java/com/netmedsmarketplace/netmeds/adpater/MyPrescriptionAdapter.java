package com.netmedsmarketplace.netmeds.adpater;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterMyPrescriptionsBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MstarMyPrescription;
import com.nms.netmeds.base.retrofit.APIPathConstant;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.Arrays;
import java.util.List;

public class MyPrescriptionAdapter extends RecyclerView.Adapter<MyPrescriptionAdapter.PrescriptionHolder> {

    private Context context;
    private List<MstarMyPrescription> rxLists;
    private MyPrescriptionAdapterListener adapterCallBackListener;
    private BasePreference basePreference;
    private boolean do_animate = true;

    public MyPrescriptionAdapter(List<MstarMyPrescription> rxLists, MyPrescriptionAdapterListener adapterCallBackListener, BasePreference basePreference) {
        this.rxLists = rxLists;
        this.adapterCallBackListener = adapterCallBackListener;
        this.basePreference = basePreference;
    }

    @NonNull
    @Override
    public PrescriptionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        AdapterMyPrescriptionsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_my_prescriptions, viewGroup, false);
        return new MyPrescriptionAdapter.PrescriptionHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final PrescriptionHolder prescriptionHolder, int position) {
        prescriptionHolder.onBind();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rxLists.size();
    }

    public void updatePrescriptionData(List<MstarMyPrescription> rxList) {
        if (rxList != null) {
            int size = rxLists.size();
            this.rxLists.addAll(rxList);
            notifyItemRangeInserted(size, rxLists.size());
        }
    }

    class PrescriptionHolder extends RecyclerView.ViewHolder {
        private AdapterMyPrescriptionsBinding binding;

        PrescriptionHolder(@NonNull AdapterMyPrescriptionsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void onBind() {
            CommonUtils.setAnimation(binding.prescriptionCv, do_animate, 100, context);
            final MstarMyPrescription rx = rxLists.get(getAdapterPosition());
            final String rxUrl = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.DOWNLOAD_PRESCRIPTION + rx.getRxId();
            Glide.with(context.getApplicationContext()).load(CommonUtils.getGlideUrl(rxUrl, basePreference.getMstarBasicHeaderMap())).apply(new RequestOptions().fitCenter()).into(binding.prescriptionImage);
            binding.tvOrderId.setText(!TextUtils.isEmpty(rx.getOrderId()) ? context.getResources().getString(R.string.text_order_id) + rx.getOrderId() : context.getResources().getString(R.string.text_order_id));
            binding.tvOrderDesc.setText(!TextUtils.isEmpty(rx.getOrderStatusDescription()) ? rx.getOrderStatusDescription() : "");
            binding.orderDate.setText(!TextUtils.isEmpty(rx.getUploadDate()) ? rx.getUploadDate() : "");
            binding.prescriptionStatus.setText(!TextUtils.isEmpty(rx.getPrescriptionStatus()) ? rx.getPrescriptionStatus() : context.getResources().getString(R.string.text_new_rx));
            initiateValidPrescriptionWithMedicineList(rx);

            // Download the prescription
            binding.downloadPres.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        adapterCallBackListener.downloadPrescription(rx);
                    } else {
                        adapterCallBackListener.requestPremission(rx);
                    }
                }
            });

            binding.prescriptionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterCallBackListener.previewPrescriptionImage(rxUrl);
                }
            });
        }

        private void initiateValidPrescriptionWithMedicineList(MstarMyPrescription rx) {
            if (rx.getBrandNameList() != null && !TextUtils.isEmpty(rx.getBrandNameList())) {
                binding.medicineRecyclerview.setVisibility(View.VISIBLE);
                String[] product = rx.getBrandNameList().split(", ");
                if (product.length > 0) {
                    PrescriptionItemListAdapter itemListAdapter = new PrescriptionItemListAdapter(context, Arrays.asList(product));
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                    binding.medicineRecyclerview.setLayoutManager(linearLayoutManager);
                    binding.medicineRecyclerview.setAdapter(itemListAdapter);
                }
            } else {
                binding.medicineRecyclerview.setVisibility(View.GONE);
            }
        }
    }

    public interface MyPrescriptionAdapterListener {
        void downloadPrescription(MstarMyPrescription singlePrescriptionDetails);

        void previewPrescriptionImage(String rxUrl);

        void requestPremission(MstarMyPrescription rx);
    }
}
