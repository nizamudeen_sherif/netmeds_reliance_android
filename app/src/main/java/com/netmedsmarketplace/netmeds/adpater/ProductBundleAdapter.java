package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterPackProductsBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.ProductPackRulesModel;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.List;

public class ProductBundleAdapter extends RecyclerView.Adapter<ProductBundleAdapter.ProductBundleHolder> {

    private List<ProductPackRulesModel> productListResult;
    private ProductBundleInterface bundleInterface;
    private Context context;

    public ProductBundleAdapter(List<ProductPackRulesModel> productListResult, ProductBundleInterface bundleInterface, Context context) {
        this.productListResult = productListResult;
        this.bundleInterface = bundleInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductBundleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterPackProductsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_pack_products, viewGroup, false);
        return new ProductBundleHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductBundleHolder holder, int i) {
        holder.bindData();
    }

    @Override
    public int getItemCount() {
        return productListResult.size();
    }

    class ProductBundleHolder extends RecyclerView.ViewHolder {
        AdapterPackProductsBinding binding;

        ProductBundleHolder(@NonNull AdapterPackProductsBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }

        public void bindData() {
            ProductPackRulesModel productPackRulesModel = productListResult.get(getAdapterPosition());
            boolean isDiscountAvailable = productPackRulesModel.getDiscount().compareTo(BigDecimal.ZERO) > 0;
            if (context != null) {
                Glide.with(context)
                        .load(ImageUtils.checkImage(productPackRulesModel.getProductImageUrl()))
                        .error(Glide.with(binding.bundleProductImage).load(R.drawable.ic_no_image))
                        .into(binding.bundleProductImage);
            }

            binding.bundlePackSize.setText(String.format(bundleInterface.getPackString(productPackRulesModel.getQty())));
            binding.bundlePrice.setText(CommonUtils.getPriceInFormat(productPackRulesModel.getSellingPrice()));
            binding.bundleStrikePrice.setText(CommonUtils.getPriceInFormat(isDiscountAvailable ? productPackRulesModel.getMrp() : BigDecimal.ZERO));
            binding.bundleStrikePrice.setPaintFlags(binding.bundleStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.bundleStrikePrice.setVisibility(!isDiscountAvailable ? View.GONE : View.VISIBLE);
            binding.bundleMrpStrikePrice.setVisibility(!isDiscountAvailable ? View.GONE : View.VISIBLE);
            binding.bundleMrpPrice.setVisibility(!isDiscountAvailable ? View.VISIBLE : View.GONE);
            binding.cvCardViewPack.setVisibility(!isDiscountAvailable ? View.GONE : View.VISIBLE);
            binding.bundleDiscount.setVisibility(!isDiscountAvailable ? View.GONE : View.VISIBLE);
            if (productPackRulesModel.getQty() == 2) {
                binding.bundleDiscount.setText(!TextUtils.isEmpty(productPackRulesModel.getDiscountPct()) ? productPackRulesModel.getDiscountPct() : "");
                binding.tvAmountPack.setText(!TextUtils.isEmpty(getProdPackTwoText()) ? getProdPackTwoText() : "");
                binding.cvCardViewPack.setVisibility(!TextUtils.isEmpty(getProdPackTwoText()) ? View.VISIBLE : View.INVISIBLE);
            } else if (productPackRulesModel.getQty() == 4) {
                binding.bundleDiscount.setText(!TextUtils.isEmpty(productPackRulesModel.getDiscountPct()) ? productPackRulesModel.getDiscountPct() : "");
                binding.tvAmountPack.setText(!TextUtils.isEmpty(getProdPackFourText()) ? getProdPackFourText() : "");
                binding.cvCardViewPack.setVisibility(!TextUtils.isEmpty(getProdPackFourText()) ? View.VISIBLE : View.INVISIBLE);
            }
            binding.bundleBuyPack.setOnClickListener(bundleInterface.onAddBundleClickListener(productPackRulesModel));
        }
    }

    private String getProdPackTwoText() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getProdPackTwoText())) {
            return configurationResponse.getResult().getConfigDetails().getProdPackTwoText();
        }
        return "";

    }

    private String getProdPackFourText() {
        ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
        if (configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && !TextUtils.isEmpty(configurationResponse.getResult().getConfigDetails().getProdPackFourText())) {
            return configurationResponse.getResult().getConfigDetails().getProdPackFourText();
        }
        return "";

    }

    public interface ProductBundleInterface {
        String getPackString(Integer qty);

        View.OnClickListener onAddBundleClickListener(ProductPackRulesModel packRulesModel);

        Context getContext();

    }
}