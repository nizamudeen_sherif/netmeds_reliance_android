package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.MedicineAdapterBuyAgainBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.List;

public class MedicineBuyAgainAdapter extends RecyclerView.Adapter<MedicineBuyAgainAdapter.MedicineBuyAgainViewHolder> {
    private Context context;
    private List<MStarProductDetails> buyAgainProductDetailsList;
    private String imageUrl;
    private MedicineHomePageAdapterListener listener;

    public MedicineBuyAgainAdapter(Context context, List<MStarProductDetails> buyAgainProductDetailsList, MStarBasicResponseTemplateModel model, MedicineHomePageAdapterListener listener) {
        this.context = context;
        this.buyAgainProductDetailsList = buyAgainProductDetailsList;
        imageUrl = model.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/";
        this.listener = listener;
    }

    @NonNull
    @Override
    public MedicineBuyAgainAdapter.MedicineBuyAgainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MedicineAdapterBuyAgainBinding buyAgainBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.medicine_adapter_buy_again, parent, false);
        return new MedicineBuyAgainAdapter.MedicineBuyAgainViewHolder(buyAgainBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicineBuyAgainAdapter.MedicineBuyAgainViewHolder holder, final int position) {
        MStarProductDetails mstarPrescriptionDetails = buyAgainProductDetailsList.get(position);
        holder.bindImage(mstarPrescriptionDetails);
        holder.bindString(mstarPrescriptionDetails);
    }

    @Override
    public int getItemCount() {
        return buyAgainProductDetailsList.size();
    }

    class MedicineBuyAgainViewHolder extends RecyclerView.ViewHolder {
        private MedicineAdapterBuyAgainBinding binding;

        MedicineBuyAgainViewHolder(MedicineAdapterBuyAgainBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bindImage(MStarProductDetails productDetails) {
            String imagePath = productDetails.getImagePaths() != null ? productDetails.getImagePaths().get(0) : "";
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(binding.getRoot().getContext()).load(imagePath.contains(AppConstant.HTTPS) ? ImageUtils.checkImage(imagePath) : ImageUtils.checkImage(imageUrl + imagePath))
                    .apply(options).into(binding.medicineImage);
        }

        void bindString(final MStarProductDetails mstarPrescriptionDetails) {
            if (mstarPrescriptionDetails != null) {
                binding.tvBuyAgainMedicineName.setText(mstarPrescriptionDetails.getDisplayName() != null && !TextUtils.isEmpty(mstarPrescriptionDetails.getDisplayName()) ? mstarPrescriptionDetails.getDisplayName() : "");
                binding.medicineBuyAgainAlgoliaPrice.setText(CommonUtils.getPriceInFormat(mstarPrescriptionDetails.getSellingPrice()));
                binding.medicineBuyAgainMrpAlgoliaPrice.setVisibility(!isDiscountAvailable(mstarPrescriptionDetails.getDiscount()) ? View.VISIBLE : View.GONE);
                binding.medicineBuyAgainMrpStrikePrice.setVisibility(isDiscountAvailable(mstarPrescriptionDetails.getDiscount()) ? View.VISIBLE : View.GONE);
                binding.medicineBuyAgainStrikePrice.setVisibility(isDiscountAvailable(mstarPrescriptionDetails.getDiscount()) ? View.VISIBLE : View.GONE);
                binding.medicineBuyAgainStrikePrice.setText(CommonUtils.getPriceInFormat(mstarPrescriptionDetails.getMrp()));
                int discountInPercent = (int) (mstarPrescriptionDetails.getDiscount().doubleValue() * 100);
                binding.tvMedicineUpToPercentage.setText(BigDecimal.ZERO.compareTo(mstarPrescriptionDetails.getDiscount()) != 0 ? discountInPercent + context.getResources().getString(R.string.text_percentage) : "");
                binding.medicineBuyAgainStrikePrice.setPaintFlags(binding.medicineBuyAgainStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                binding.medicineAddToCart.setEnabled(PaymentHelper.isIsMedicineIncreaseQuantity());

                binding.medicineAddToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PaymentHelper.setIsMedicineIncreaseQuantity(false);
                        listener.addToCart(mstarPrescriptionDetails.getProductCode());
                    }
                });
                binding.parentCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.navigateProductDetailActivity(mstarPrescriptionDetails.getProductCode());
                    }
                });
            }

        }

        private boolean isDiscountAvailable(BigDecimal discount) {
            return discount.compareTo(BigDecimal.ZERO) > 0;
        }
    }

    public interface MedicineHomePageAdapterListener {
        void addToCart(int productCode);

        void navigateProductDetailActivity(int productCode);
    }
}
