package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterReviewOrderAgainBinding;
import com.netmedsmarketplace.netmeds.model.ReviewOrderItems;

import java.util.List;

public class ReviewOrderAgainAdapter extends RecyclerView.Adapter<ReviewOrderAgainAdapter.ReviewOrderAgainHolder> {

    private final List<ReviewOrderItems> reviewOrderItemsList;
    private final ReviewOrderAgainAdapterListener listener;

    public ReviewOrderAgainAdapter(List<ReviewOrderItems> reviewOrderItemsList, ReviewOrderAgainAdapterListener listener) {
        this.reviewOrderItemsList = reviewOrderItemsList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ReviewOrderAgainHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterReviewOrderAgainBinding listBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_review_order_again, viewGroup, false);
        return new ReviewOrderAgainHolder(listBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewOrderAgainHolder holder, int position) {
        ReviewOrderItems items = reviewOrderItemsList.get(position);
        holder.binding.tvDrugName.setText(items.getDrugName());
        holder.binding.cbDrugSelected.setChecked(items.isDrugSelected());
        holder.binding.cbDrugSelected.setOnCheckedChangeListener(checkedChangeListener(position));
        holder.binding.divider.setVisibility(position + 1 == getItemCount() ? View.GONE : View.VISIBLE);
    }

    private CompoundButton.OnCheckedChangeListener checkedChangeListener(final int position) {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                listener.onSelectItems(position, isChecked);
            }
        };
    }

    @Override
    public int getItemCount() {
        return reviewOrderItemsList.size();
    }

    interface ReviewOrderAgainAdapterListener {
        void onSelectItems(int position, boolean isSelected);
    }

    class ReviewOrderAgainHolder extends RecyclerView.ViewHolder {

        private final AdapterReviewOrderAgainBinding binding;

        ReviewOrderAgainHolder(@NonNull AdapterReviewOrderAgainBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
