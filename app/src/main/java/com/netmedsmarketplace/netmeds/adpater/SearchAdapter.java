package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterSearchBinding;
import com.netmedsmarketplace.netmeds.viewmodel.SearchViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.VisibleItemListener;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MstarAlgoliaResult;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Adapter[Recycler view] for Search Autocomplete in search page
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    private Context context;
    private SearchAdapter.SearchAdapterListener adapterListener;
    private List<MstarAlgoliaResult> algoliaHitList;
    private String imageBasePath = "";
    private boolean do_animate = true;
    private boolean isFromGeneric;

    public SearchAdapter(List<MstarAlgoliaResult> algoliaHitList, SearchAdapterListener adapterListener, boolean isFromGeneric) {
        this.adapterListener = adapterListener;
        this.algoliaHitList = algoliaHitList;
        this.isFromGeneric = isFromGeneric;
        MStarBasicResponseTemplateModel mStarBasicResponseTemplateModel = new Gson().fromJson(BasePreference.getInstance(context).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageBasePath = mStarBasicResponseTemplateModel != null && mStarBasicResponseTemplateModel.getResult() != null && !TextUtils.isEmpty(mStarBasicResponseTemplateModel.getResult().getCatalogImageUrlBasePath()) ? mStarBasicResponseTemplateModel.getResult().getCatalogImageUrlBasePath() : "";
        SearchViewModel.setVisibleItemListener(new VisibleItemListener() {
            @Override
            public void viewVisiblePosition(int firstVisiblePosition, int lastVisiblePosition) {
                if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < getAlgoliaHitList().size()) {
                    List<MstarAlgoliaResult> impressionList = getAlgoliaHitList().subList(firstVisiblePosition, lastVisiblePosition + 1);
                    /*Google Tag Manager + FireBase Product Impression Event*/
                    FireBaseAnalyticsHelper.getInstance().logFireBaseProductImpressionEvent(context, impressionList, firstVisiblePosition + 1, FireBaseAnalyticsHelper.VIEW_SEARCH_RESULTS, "");
                }
            }
        });
    }

    private List<MstarAlgoliaResult> getAlgoliaHitList() {
        return algoliaHitList;
    }

    public void updateAlgoliaHitList(List<MstarAlgoliaResult> algoliaList, boolean isPagenationCall) {
        if (!isPagenationCall) {
            algoliaHitList = new ArrayList<>();
            notifyDataSetChanged();
        }
        if (algoliaHitList != null) {
            int size = algoliaHitList.size();
            this.algoliaHitList.addAll(algoliaList);
            notifyItemRangeInserted(size, algoliaList.size());
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @NonNull
    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterSearchBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_search, parent, false);
        return new SearchAdapter.SearchViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.SearchViewHolder holder, int position) {
        if (isFromGeneric) {
            holder.adapterSearchBinding.alternateBrainProductView.setVisibility(View.GONE);
            holder.adapterSearchBinding.llGenericProductView.setVisibility(View.VISIBLE);
            bindGenericView(holder, position);
        } else {
            holder.adapterSearchBinding.alternateBrainProductView.setVisibility(View.VISIBLE);
            holder.adapterSearchBinding.llGenericProductView.setVisibility(View.GONE);
            bindNormalSearch(holder, position);
        }

    }

    private void bindNormalSearch(SearchAdapter.SearchViewHolder holder, int position) {
        CommonUtils.setAnimation(holder.adapterSearchBinding.alternateBrainProductView, do_animate, 100, context);
        final int viewPosition = position;
        final MstarAlgoliaResult algoliaHitResults = getAlgoliaHitList().get(position);
        if (algoliaHitResults != null) {
            if (context != null)
                Glide.with(context).load(ImageUtils.checkImage(String.format("%s%s", imageBasePath, algoliaHitResults.getThumbnailUrl()))).apply(new RequestOptions().circleCrop()).into(holder.adapterSearchBinding.alternateBrainProductImage);
            holder.adapterSearchBinding.alternateBrainDrugName.setText(getProductName(algoliaHitResults));
            holder.adapterSearchBinding.searchAlternateBrainalgoliaPrice.setText(getPrice(algoliaHitResults));
            holder.adapterSearchBinding.searchAlternateBrainStrikePrice.setText(getStrikePrice(algoliaHitResults));
            holder.adapterSearchBinding.searchAlternateBrainStrikePrice.setPaintFlags(holder.adapterSearchBinding.searchAlternateBrainStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.adapterSearchBinding.alternateBrainCategoryName.setText(getCategoryLevel(algoliaHitResults, AppConstant.LEVEL_1));
            holder.adapterSearchBinding.alternateBrainManufacturerName.setText(getManufacturerName(algoliaHitResults));
            holder.adapterSearchBinding.alternateBrainDrugDetail.setText(getDrugDetail(algoliaHitResults));
            holder.adapterSearchBinding.categoryName2.setText(getCategoryLevel(algoliaHitResults, AppConstant.LEVEL_2));

            holder.adapterSearchBinding.alternateBrainDrugName.setVisibility(TextUtils.isEmpty(getProductName(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.searchAlternateBrainalgoliaPrice.setVisibility(TextUtils.isEmpty(getPrice(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.searchAlternateBrainStrikePrice.setVisibility(TextUtils.isEmpty(getStrikePrice(algoliaHitResults)) || getPrice(algoliaHitResults).equals(getStrikePrice(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainProductImage.setVisibility(View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainRxImage.setVisibility(View.GONE);
            holder.adapterSearchBinding.alternateBrainStrikePriceMrp.setVisibility(TextUtils.isEmpty(getStrikePrice(algoliaHitResults)) || getPrice(algoliaHitResults).equals(getStrikePrice(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainAlgoliaPriceMrp.setVisibility(holder.adapterSearchBinding.searchAlternateBrainStrikePrice.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainRxRequired.setVisibility(isRxRequired(algoliaHitResults) ? View.VISIBLE : View.GONE);
            holder.adapterSearchBinding.alternateBrainCategoryName.setVisibility(TextUtils.isEmpty(getCategoryLevel(algoliaHitResults, AppConstant.LEVEL_1)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainManufacturerName.setVisibility(TextUtils.isEmpty(getManufacturerName(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainDrugDetail.setVisibility(TextUtils.isEmpty(getDrugDetail(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.alternateBrainAddCartButton.setVisibility(isProductAvailable(algoliaHitResults) ? View.VISIBLE : View.GONE);
            holder.adapterSearchBinding.alternateBrainCartDivider.setVisibility(isProductAvailable(algoliaHitResults) ? View.VISIBLE : View.GONE);
            holder.adapterSearchBinding.searchDivider.setVisibility(position == algoliaHitList.size() - 1 ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.categoryName2.setVisibility(TextUtils.isEmpty(getCategoryLevel(algoliaHitResults, AppConstant.LEVEL_2)) ? View.GONE : View.VISIBLE);

            holder.adapterSearchBinding.alternateBrainProductView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewPosition < getAlgoliaHitList().size() && !TextUtils.isEmpty(getPrice(algoliaHitResults)))
                        adapterListener.adOnResultClickCallBack(getAlgoliaHitList().get(viewPosition), viewPosition + 1);
                }
            });


            holder.adapterSearchBinding.alternateBrainAddCartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewPosition < getAlgoliaHitList().size() && isProductAvailable(algoliaHitResults)) {
                        adapterListener.adOnAddCartCallBack(getAlgoliaHitList().get(viewPosition), viewPosition + 1);
                    }
                }
            });
        }
    }

    public void bindGenericView(SearchAdapter.SearchViewHolder holder, final int position) {
        CommonUtils.setAnimation(holder.adapterSearchBinding.llGenericProductView, do_animate, 100, context);
        final int viewPosition = position;
        final MstarAlgoliaResult algoliaHitResults = getAlgoliaHitList().get(position);
        if (algoliaHitResults != null) {
            if (context != null)
                Glide.with(context).load(ImageUtils.checkImage(String.format("%s%s", imageBasePath, algoliaHitResults.getThumbnailUrl()))).apply(new RequestOptions().circleCrop()).into(holder.adapterSearchBinding.imgGenericProductImage);
            holder.adapterSearchBinding.tvGenericDrugName.setText(getProductName(algoliaHitResults));
            holder.adapterSearchBinding.tvGenericManufactureName.setText(getManufacturerName(algoliaHitResults));

            holder.adapterSearchBinding.tvGenericManufactureName.setVisibility(TextUtils.isEmpty(getManufacturerName(algoliaHitResults)) ? View.GONE : View.VISIBLE);
            holder.adapterSearchBinding.searchDivider.setVisibility(position == algoliaHitList.size() - 1 ? View.GONE : View.VISIBLE);

            holder.adapterSearchBinding.llGenericRxImage.setVisibility(isRxRequired(algoliaHitResults) ? View.VISIBLE : View.GONE);
            holder.adapterSearchBinding.imgGenericProductImage.setVisibility(isRxRequired(algoliaHitResults) ? View.GONE : View.VISIBLE);

            holder.adapterSearchBinding.tvViewGenericBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterListener.adOnResultClickCallBack(algoliaHitResults, position);
                }
            });
            holder.adapterSearchBinding.llGenericProductView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterListener.adOnResultClickCallBack(algoliaHitResults, position);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return algoliaHitList != null ? algoliaHitList.size() : 0;
    }

    private boolean isRxRequired(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && algoliaHitResults.getRxRequired() == 1;
    }

    private String getDrugDetail(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && !TextUtils.isEmpty(algoliaHitResults.getPackLabel()) ? algoliaHitResults.getPackLabel() : "";
    }

    private String getManufacturerName(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && !TextUtils.isEmpty(algoliaHitResults.getManufacturerName()) ? String.format("%s %s", AppConstant.MFR, algoliaHitResults.getManufacturerName()) : "";
    }

    private String getProductName(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && !TextUtils.isEmpty(algoliaHitResults.getDisplayName()) ? algoliaHitResults.getDisplayName() : "";
    }

    private String getStrikePrice(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && algoliaHitResults.getMrp().compareTo(BigDecimal.ZERO) != 0 ? String.format(Locale.getDefault(), "%s%.2f", context.getResources().getString(R.string.text_mrp), algoliaHitResults.getMrp()) : "";
    }

    private String getPrice(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && algoliaHitResults.getSellingPrice().compareTo(BigDecimal.ZERO) != 0 ? String.format(Locale.getDefault(), "%s%.2f", context.getResources().getString(R.string.text_mrp), algoliaHitResults.getSellingPrice()) : "";
    }

    private boolean isProductAvailable(MstarAlgoliaResult algoliaHitResults) {
        return algoliaHitResults != null && !TextUtils.isEmpty(algoliaHitResults.getAvailabilityStatus()) && algoliaHitResults.getAvailabilityStatus().equalsIgnoreCase(AppConstant.PRODUCT_AVAILABLE_CODE);
    }

    private String getCategoryLevel(MstarAlgoliaResult algoliaHitResults, String level) {
        if (algoliaHitResults != null && algoliaHitResults.getCategoryTreeList() != null && algoliaHitResults.getCategoryTreeList().size() > 0) {
            for (Map.Entry<String, ArrayList<Object>> entry : algoliaHitResults.getCategoryTreeList().entrySet()) {
                String key = entry != null && !TextUtils.isEmpty(entry.getKey()) ? entry.getKey() : "";
                if (key.equalsIgnoreCase(level)) {
                    ArrayList<Object> levelList = entry != null && entry.getValue() != null && entry.getValue().size() > 0 ? entry.getValue() : new ArrayList<Object>();
                    if (levelList != null && levelList.size() > 0 && !TextUtils.isEmpty(levelList.get(0).toString().trim())) {
                        String[] categoryList = levelList.get(0).toString().split("///");
                        return categoryList.length > 0 ? !TextUtils.isEmpty(Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1)) ? Arrays.asList(categoryList).get(Arrays.asList(categoryList).size() - 1) : "" : "";
                    }
                }
            }
        }
        return "";
    }

    public interface SearchAdapterListener {
        void adOnAddCartCallBack(MstarAlgoliaResult algoliaHitResults, int position);

        void adOnResultClickCallBack(MstarAlgoliaResult algoliaHitResults, int position);
    }

    class SearchViewHolder extends RecyclerView.ViewHolder {
        private AdapterSearchBinding adapterSearchBinding;

        SearchViewHolder(AdapterSearchBinding itemBinding) {
            super(itemBinding.getRoot());
            this.adapterSearchBinding = itemBinding;
        }
    }
}
