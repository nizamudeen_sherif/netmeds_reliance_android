package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterWellnessActivityBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarWellnessSectionDetails;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;

import java.util.List;

public class WellnessActivityAdapterV2 extends RecyclerView.Adapter<WellnessActivityAdapterV2.WellnessViewHolderV2> {

    private List<MstarWellnessSectionDetails> wellnessSectionDetailsList;
    private static int GRID_COUNT = 4;
    private Context context;
    private MstarWellnessAdapter.WellnessAdaterCallBack callBack;
    private PopularProductsAdapter.PopularProductAddToCartListener listener;
    private WellnessActivityAdapterCallback wellnessCallback;

    public WellnessActivityAdapterV2(List<MstarWellnessSectionDetails> wellnessSectionDetailsList, MstarWellnessAdapter.WellnessAdaterCallBack callBack, PopularProductsAdapter.PopularProductAddToCartListener listener, WellnessActivityAdapterCallback wellnessCallback) {
        this.wellnessSectionDetailsList = wellnessSectionDetailsList;
        this.listener = listener;
        this.callBack = callBack;
        this.wellnessCallback = wellnessCallback;
    }

    @NonNull
    @Override
    public WellnessViewHolderV2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterWellnessActivityBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_wellness_activity, parent, false);
        this.context = binding.getRoot().getContext();
        return new WellnessViewHolderV2(binding.getRoot(), binding);
    }

    @Override
    public void onBindViewHolder(@NonNull WellnessViewHolderV2 holder, int position) {
        final MstarWellnessSectionDetails wellnessSectionDetails = wellnessSectionDetailsList.get(position);
        if (wellnessSectionDetails == null) {
            holder.binding.contentView.setVisibility(View.GONE);
        } else if (AppConstant.CATEGORY.equalsIgnoreCase(wellnessSectionDetails.getParseType())) {
            exploreCategoryBinding(wellnessSectionDetails, holder);
        } else if (AppConstant.PRODUCTS.equalsIgnoreCase(wellnessSectionDetails.getParseType())) {
            popularProductBinding(wellnessSectionDetails, holder);
        } else if (AppConstant.BANNER.equalsIgnoreCase(wellnessSectionDetails.getParseType())) {
            bannerViewBinding(wellnessSectionDetails, holder);
        }
    }

    private void exploreCategoryBinding(final MstarWellnessSectionDetails category, WellnessViewHolderV2 wellnessViewHolder) {
        wellnessViewHolder.binding.wellnessRecycleView.setLayoutManager(getLayoutManager(!TextUtils.isEmpty(category.getDesignType()) ? category.getDesignType() : ""));
        MstarWellnessAdapter wellnessAdapter = new MstarWellnessAdapter(category.getProductDetails(), callBack);
        wellnessViewHolder.binding.wellnessRecycleView.setAdapter(wellnessAdapter);
        wellnessViewHolder.binding.wellnessHeader.setText(!TextUtils.isEmpty(category.getHeader()) ? category.getHeader() : "");
        wellnessViewHolder.binding.wellnessSubHeader.setText(!TextUtils.isEmpty(category.getSubHeader()) ? category.getSubHeader() : "");
        wellnessViewHolder.binding.contentView.setVisibility(wellnessAdapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessTagline.setVisibility(!TextUtils.isEmpty(category.getTagline()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessTagline.setText(!TextUtils.isEmpty(category.getTagline()) ? category.getTagline() : "");
        wellnessViewHolder.binding.wellnessViewAll.setVisibility(!TextUtils.isEmpty(category.getViewAll()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category.getViewAll().contains("https://")) {
                    wellnessCallback.routeToScreen(category.getViewAll());
                } else wellnessCallback.vmCategoryViewAll(category.getViewAll());

            }
        });
    }

    private void popularProductBinding(final MstarWellnessSectionDetails products, final WellnessViewHolderV2 wellnessViewHolder) {
        wellnessViewHolder.binding.wellnessRecycleView.setLayoutManager(getLayoutManager(!TextUtils.isEmpty(products.getDesignType()) ? products.getDesignType() : ""));
        PopularProductsAdapter adapter = new PopularProductsAdapter(products.getProductDetails(), !TextUtils.isEmpty(products.getSubHeader()) ? products.getSubHeader() : "", listener);
        wellnessViewHolder.binding.wellnessRecycleView.setAdapter(adapter);
        wellnessViewHolder.binding.wellnessHeader.setVisibility(!TextUtils.isEmpty(products.getHeader()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessHeader.setText(!TextUtils.isEmpty(products.getHeader()) ? products.getHeader() : "");
        wellnessViewHolder.binding.wellnessSubHeader.setVisibility(!TextUtils.isEmpty(products.getSubHeader()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessSubHeader.setText(!TextUtils.isEmpty(products.getSubHeader()) ? products.getSubHeader() : "");
        wellnessViewHolder.binding.wellnessTagline.setVisibility(!TextUtils.isEmpty(products.getTagline()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessTagline.setText(!TextUtils.isEmpty(products.getTagline()) ? products.getTagline() : "");

        wellnessViewHolder.binding.wellnessViewAll.setVisibility(View.GONE);
        wellnessViewHolder.binding.wellnessDescription.setVisibility(View.GONE);
        wellnessViewHolder.binding.contentView.setVisibility(adapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessViewAll.setVisibility(!TextUtils.isEmpty(products.getViewAll()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (products.getViewAll().contains("https://")) {
                    wellnessCallback.routeToScreen(products.getViewAll());
                } else
                    wellnessCallback.viewAllbuttonCallBack(Integer.parseInt(products.getViewAll()), products.getRedirectType(), AppConstant.MANUFACTURE.equalsIgnoreCase(products.getRedirectType()) ? "" : products.getProductDetails().get(0) != null ? products.getProductDetails().get(0).getCategories().get(0).getName() : "");
            }
        });

        wellnessViewHolder.binding.wellnessRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && products.getProductDetails() != null && products.getProductDetails().size() > 0) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                    int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();

                    if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < products.getProductDetails().size()) {
                        List<MStarProductDetails> impressionList = products.getProductDetails().subList(firstVisiblePosition, lastVisiblePosition + 1);
                        /*Google Tag Manager + FireBase Product Impression Event*/
                        FireBaseAnalyticsHelper.getInstance().logFireBaseProductImpressionEvent(context, impressionList, firstVisiblePosition, FireBaseAnalyticsHelper.VIEW_ON_PAGE, !TextUtils.isEmpty(products.getSubHeader()) ? products.getSubHeader() : "");
                    }
                }
            }
        });
    }


    private void bannerViewBinding(final MstarWellnessSectionDetails banner, WellnessViewHolderV2 wellnessViewHolder) {
        CommonUtils.snapHelper(wellnessViewHolder.binding.wellnessRecycleView);
        wellnessViewHolder.binding.llContent.setPadding(0,0,0,0);
        wellnessViewHolder.binding.wellnessRecycleView.setLayoutManager(getLayoutManager(!TextUtils.isEmpty(banner.getDesignType()) ? banner.getDesignType() : ""));
        DiscountAdapter adapter = new DiscountAdapter(banner.getProductDetails(), !TextUtils.isEmpty(banner.getSubHeader()) ? banner.getSubHeader() : "");
        wellnessViewHolder.binding.wellnessRecycleView.setAdapter(adapter);
        wellnessViewHolder.binding.wellnessHeader.setVisibility(!TextUtils.isEmpty(banner.getHeader()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessHeader.setText(!TextUtils.isEmpty(banner.getHeader()) ? banner.getHeader() : "");
        wellnessViewHolder.binding.wellnessSubHeader.setVisibility(!TextUtils.isEmpty(banner.getSubHeader()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessSubHeader.setText(!TextUtils.isEmpty(banner.getSubHeader()) ? banner.getSubHeader() : "");
        wellnessViewHolder.binding.wellnessDescription.setVisibility(View.GONE);
        wellnessViewHolder.binding.wellnessViewAll.setVisibility(View.GONE);
        wellnessViewHolder.binding.wellnessTagline.setVisibility(!TextUtils.isEmpty(banner.getTagline()) ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.wellnessTagline.setText(!TextUtils.isEmpty(banner.getTagline()) ? banner.getTagline() : "");
        wellnessViewHolder.binding.contentView.setVisibility(adapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
        wellnessViewHolder.binding.headerLayout.setVisibility(!TextUtils.isEmpty(banner.getHeader()) && !TextUtils.isEmpty(banner.getSubHeader()) ? View.VISIBLE : View.GONE);
        if (TextUtils.isEmpty(banner.getHeader()) && TextUtils.isEmpty(banner.getSubHeader()))
            wellnessViewHolder.binding.contentView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPaleGrey));

        wellnessViewHolder.binding.wellnessRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                    int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
                    if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition <banner.getProductDetails().size()) {
                        List<MStarProductDetails> impressionList = banner.getProductDetails().subList(firstVisiblePosition, lastVisiblePosition + 1);
                        /*Google Tag Manager + FireBase Promotion Impression Event*/
                        FireBaseAnalyticsHelper.getInstance().logFireBasePromotionImpressionEvent(context,impressionList,FireBaseAnalyticsHelper.WELLNESS+"_"+(!TextUtils.isEmpty(banner.getSubHeader()) ? banner.getSubHeader() : ""),firstVisiblePosition+1);
                    }
                }
            }
        });
    }

    private RecyclerView.LayoutManager getLayoutManager(String designType) {
        if (AppConstant.GRID.equalsIgnoreCase(designType))
            return new GridLayoutManager(context, GRID_COUNT);
        else
            return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

    }


    @Override
    public int getItemCount() {
        return wellnessSectionDetailsList.size();
    }

    class WellnessViewHolderV2 extends RecyclerView.ViewHolder {

        public AdapterWellnessActivityBinding binding;

        public WellnessViewHolderV2(@NonNull View itemView, AdapterWellnessActivityBinding binding) {
            super(itemView);
            this.binding = binding;
        }
    }

    public interface WellnessActivityAdapterCallback {
        void vmCategoryViewAll(String viewAll);

        void viewAllbuttonCallBack(int id, String from, String name);

        void routeToScreen(String viewAll);
    }
}
