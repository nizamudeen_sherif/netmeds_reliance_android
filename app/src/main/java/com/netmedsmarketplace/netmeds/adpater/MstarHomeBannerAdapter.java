package com.netmedsmarketplace.netmeds.adpater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterOfferBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.util.Date;
import java.util.List;

public class MstarHomeBannerAdapter extends RecyclerView.Adapter<MstarHomeBannerAdapter.AdapterViewHolder> {

    private final List<MstarBanner> offerList;
    private final String bannerFrom;
    private AdapterOfferBinding binding;
    private Context context;

    public MstarHomeBannerAdapter(List<MstarBanner> offerList, String bannerFrom, Context context) {
        this.offerList = offerList;
        this.bannerFrom = bannerFrom;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_offer, viewGroup, false);
        return new MstarHomeBannerAdapter.AdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder adapterViewHolder, int i) {
        onBindData(i, adapterViewHolder);
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    private void onBindData(final int position, AdapterViewHolder adapterViewHolder) {
        if (offerList != null && !offerList.isEmpty()) {
            final MstarBanner banner = offerList.get(position);
            if (context != null) {
                String offerImage = banner.getUrl() != null && banner.getImageUrl().startsWith("http") ? banner.getImageUrl() : banner.getImageName().startsWith("http") ? banner.getImageName() : ConfigMap.getInstance().getProperty(ConfigConstant.BANNER_BASE_URL) + banner.getImageName();
                setGlideWithUrl(offerImage, adapterViewHolder.binding.imgOffer, context, banner.getDisplayFrom(), banner.getDisplayTo());
            }
            adapterViewHolder.binding.imgOffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    googleEvent(bannerFrom);
                    /*Google Tag Manager + FireBase Promotion Click Event*/
                    FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(context,banner,FireBaseAnalyticsHelper.EVENT_PARAM_HOME_MAIN,position+1);
                    CommonUtils.navigateToScreens(context, banner, (Activity) context);
                }
            });
        }
    }


    private void setGlideWithUrl(String url, ImageView imageView, Context context, String displayFrom, String displayTo) {

        if (new Date().getTime() > DateTimeUtils.getInstance().convertDateToMilliSecond(displayFrom, DateTimeUtils.yyyyMMddHHmmss) && new Date().getTime() < DateTimeUtils.getInstance().convertDateToMilliSecond(displayTo, DateTimeUtils.yyyyMMddHHmmss)) {
            Glide.with(context)
                    .load(url)
                    .error(Glide.with(imageView).load(R.drawable.ic_no_image)).into(imageView);
        }
    }

    class AdapterViewHolder extends RecyclerView.ViewHolder {
        AdapterOfferBinding binding;

        AdapterViewHolder(@NonNull AdapterOfferBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

    private void googleEvent(String bannerFrom) {
        switch (bannerFrom) {
            case GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS:
                GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_BANNER, GoogleAnalyticsHelper.EVENT_ACTION_PRIMARY_BANNERS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
            case GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER:
                GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_WELLNESS_SECTION, GoogleAnalyticsHelper.EVENT_ACTION_SECONDARY_BANNER, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                break;
        }
    }


}
