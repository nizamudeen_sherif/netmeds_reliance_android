package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ItemAlternateCartHeaderRowBinding;
import com.netmedsmarketplace.netmeds.model.AlternateCartHeaderModel;

import java.util.List;

public class AlternateCartHeaderAdapter extends RecyclerView.Adapter<AlternateCartHeaderAdapter.ViewHolder> {

    private List<AlternateCartHeaderModel> headerList;
    private AlternateCartHeaderAdapterCallback callback;

    public AlternateCartHeaderAdapter(List<AlternateCartHeaderModel> headerList, AlternateCartHeaderAdapterCallback callback) {
        this.headerList = headerList;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemAlternateCartHeaderRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_alternate_cart_header_row, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind();
    }

    @Override
    public int getItemCount() {
        return headerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemAlternateCartHeaderRowBinding binding;

        public ViewHolder(@NonNull ItemAlternateCartHeaderRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind() {
            AlternateCartHeaderModel alternateCartHeaderModel = headerList.get(getAdapterPosition());
            binding.setModel(alternateCartHeaderModel);
            loadGlide(binding.imgMedicine1, alternateCartHeaderModel.getMedicine1Image());
            loadGlide(binding.imgMedicine2, alternateCartHeaderModel.getMedicine2Image());
            binding.executePendingBindings();
        }

        private void loadGlide(ImageView imageView, String imageUrl) {
            Glide.with(callback.getContext()).load(imageUrl)
                    .apply(new RequestOptions().circleCrop())
                    .error(Glide.with(imageView).load(R.drawable.ic_no_image))
                    .into(imageView);
        }
    }

    public interface AlternateCartHeaderAdapterCallback {
        Context getContext();
    }
}
