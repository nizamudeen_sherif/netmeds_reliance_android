package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.FitnessItemViewBinding;
import com.netmedsmarketplace.netmeds.model.WellnessHealthConcernCategoryListResponse;
import com.netmedsmarketplace.netmeds.ui.ProductList;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.WellnessDiscountList;

import java.util.List;

public class FitNessAdapter extends RecyclerView.Adapter<FitNessAdapter.FitNessAdapterViewHolder> {

    private Context context;
    private List<WellnessDiscountList> list;
    private List<WellnessHealthConcernCategoryListResponse> categoryListResponses;
    private boolean isWellnessHealthConcern;


    public FitNessAdapter(Context mContext, List<WellnessDiscountList> list, List<WellnessHealthConcernCategoryListResponse> categoryListResponses, boolean isWellnessHealthConcern) {
        this.context = mContext;
        this.list = list;
        this.categoryListResponses = categoryListResponses;
        this.isWellnessHealthConcern = isWellnessHealthConcern;
    }

    @NonNull
    @Override
    public FitNessAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        FitnessItemViewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.fitness_item_view, viewGroup, false);
        return new FitNessAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FitNessAdapterViewHolder holder, final int position) {
        if (context != null && context.getApplicationContext() != null)
            Glide.with(context.getApplicationContext()).load(isWellnessHealthConcern ? categoryListResponses.get(position).getImage() : !TextUtils.isEmpty(list.get(position).getImageUrl()) ? list.get(position).getImageUrl() : "")
                    .error(Glide.with(holder.binding.productImage).load(R.drawable.ic_no_image)).into(holder.binding.productImage);
        holder.binding.tvMedicineName.setText(isWellnessHealthConcern ? categoryListResponses.get(position).getName() : !TextUtils.isEmpty(list.get(position).getImageName()) ? list.get(position).getImageName() : "");
        String discount = isWellnessHealthConcern ? categoryListResponses.get(position) != null && !TextUtils.isEmpty(categoryListResponses.get(position).getDiscount()) && !(categoryListResponses.get(position).getDiscount().equalsIgnoreCase("0")) ? categoryListResponses.get(position).getDiscount() : ""
                : list != null && !TextUtils.isEmpty(list.get(position).getDiscount()) && !list.get(position).getDiscount().equalsIgnoreCase("0") ? list.get(position).getDiscount() : "";
        holder.binding.tvUpToPercentage.setText(discount);
        holder.binding.tvUpToPercentage.setVisibility(TextUtils.isEmpty(discount) ? View.GONE : View.VISIBLE);
        if (isWellnessHealthConcern) {
            holder.binding.cvCardView.setBackground(ContextCompat.getDrawable(context, R.drawable.concern_background));
        }
        holder.binding.cvCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductList.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(IntentConstant.CATEGORY_ID, isWellnessHealthConcern ? categoryListResponses.get(position).getCategoryId() : !TextUtils.isEmpty(list.get(position).getId()) ? list.get(position).getId() : "");
                intent.putExtra(IntentConstant.FROM_CATEGORY, true);
                intent.putExtra(IntentConstant.CATEGORY_NAME, isWellnessHealthConcern ? categoryListResponses.get(position).getName() : !TextUtils.isEmpty(list.get(position).getImageName()) ? list.get(position).getImageName() : "");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return isWellnessHealthConcern ? categoryListResponses.size() : list.size();
    }

    class FitNessAdapterViewHolder extends RecyclerView.ViewHolder {
        private final FitnessItemViewBinding binding;

        FitNessAdapterViewHolder(FitnessItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
