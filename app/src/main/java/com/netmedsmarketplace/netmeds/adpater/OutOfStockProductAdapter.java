package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.OutOfStockProductListItemBinding;
import com.nms.netmeds.base.model.CartItemResult;

import java.util.List;

public class OutOfStockProductAdapter extends RecyclerView.Adapter<OutOfStockProductAdapter.OutOfStockViewHolder> {

    private List<CartItemResult> outOfStockProductList;
    private Context context;

    public OutOfStockProductAdapter(Context context, List<CartItemResult> outOfStockProductList) {
        this.context = context;
        this.outOfStockProductList = outOfStockProductList;
    }

    @NonNull
    @Override
    public OutOfStockViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        OutOfStockProductListItemBinding outOfStockProductListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.out_of_stock_product_list_item, viewGroup, false);
        return new OutOfStockViewHolder(outOfStockProductListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull OutOfStockViewHolder holder, int position) {
        CartItemResult cartItemResult = outOfStockProductList.get(position);
        holder.outOfStockProductListItemBinding.itemName.setText(cartItemResult != null && !TextUtils.isEmpty(cartItemResult.getName()) ? cartItemResult.getName() : "");
        holder.outOfStockProductListItemBinding.itemName.setVisibility(getProductNameVisibility(cartItemResult) ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return outOfStockProductList == null ? 0 : outOfStockProductList.size();
    }

    class OutOfStockViewHolder extends RecyclerView.ViewHolder {

        private OutOfStockProductListItemBinding outOfStockProductListItemBinding;

        OutOfStockViewHolder(@NonNull OutOfStockProductListItemBinding outOfStockProductListItemBinding) {
            super(outOfStockProductListItemBinding.getRoot());
            this.outOfStockProductListItemBinding = outOfStockProductListItemBinding;
        }
    }

    private boolean getProductNameVisibility(CartItemResult cartItemResult) {
        return cartItemResult != null && !TextUtils.isEmpty(cartItemResult.getName());
    }
}