package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterParentCategoryItemBinding;

import java.util.List;

public class OrderFilterMainCategoryAdapter extends RecyclerView.Adapter<OrderFilterMainCategoryAdapter.ViewHolder> {

    private List<String> categoryList;
    private OrderFilterMainCategoryAdapterListener listener;

    public OrderFilterMainCategoryAdapter(List<String> categoryList, OrderFilterMainCategoryAdapterListener listener) {
        this.categoryList = categoryList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public OrderFilterMainCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterParentCategoryItemBinding orderBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_parent_category_item, viewGroup, false);
        return new ViewHolder(orderBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderFilterMainCategoryAdapter.ViewHolder viewHolder, int i) {
        viewHolder.bind(categoryList.get(i));
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private AdapterParentCategoryItemBinding binding;

        public ViewHolder(AdapterParentCategoryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(onClickCategory);
        }

        public void bind(String category) {
            binding.setCategory(category);
        }

        View.OnClickListener onClickCategory = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCategoryClickListener(getAdapterPosition(), categoryList.get(getAdapterPosition()));
            }
        };
    }

    public interface OrderFilterMainCategoryAdapterListener {
        void onCategoryClickListener(int position, String category);
    }
}
