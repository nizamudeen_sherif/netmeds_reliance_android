package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapaterProductSuggestionBinding;
import com.nms.netmeds.base.model.BrainSinsMisc;
import com.nms.netmeds.base.model.HRVProduct;

import java.util.ArrayList;

public class CartSuggestionAdapter extends RecyclerView.Adapter<CartSuggestionAdapter.CartSuggestionViewHolder> {

    private ArrayList<HRVProduct> recommendedProductList;
    private Context mContext;
    private CartSuggestionInterface suggestionInterface;

    public CartSuggestionAdapter(ArrayList<HRVProduct> recommendedProductList, Context context, CartSuggestionInterface suggestionInterface) {
        this.recommendedProductList = recommendedProductList;
        this.mContext = context;
        this.suggestionInterface = suggestionInterface;
    }

    @NonNull
    @Override
    public CartSuggestionAdapter.CartSuggestionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        AdapaterProductSuggestionBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapater_product_suggestion, viewGroup, false);
        return new CartSuggestionAdapter.CartSuggestionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CartSuggestionAdapter.CartSuggestionViewHolder holder, int i) {
        final HRVProduct product = recommendedProductList.get(i);
        holder.adapterCartBinding.suggestionName.setText(!TextUtils.isEmpty(product.getBrandName()) ? product.getBrandName() : "");

        RequestOptions options = new RequestOptions()
                .fitCenter()
                .placeholder(R.drawable.ic_no_image)
                .error(R.drawable.ic_no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(mContext)
                .load(!TextUtils.isEmpty(product.getImageUrl()) ? product.getImageUrl() : R.drawable.ic_no_image)
                .apply(options)
                .into(holder.adapterCartBinding.suggestionImage);

        holder.adapterCartBinding.SuggestionPrice.setText(!TextUtils.isEmpty(product.getPrice()) ? "\u20B9 " + product.getPrice() : "");
        if (!TextUtils.isEmpty(product.getMisc())) {
            BrainSinsMisc miscValue = new Gson().fromJson(product.getMisc(), BrainSinsMisc.class);
            String strikePrice = (miscValue != null && !TextUtils.isEmpty(miscValue.getOriginalPrice()) && Double.valueOf(miscValue.getOriginalPrice().replace(",", "").replace("Rs.", "")) > Double.valueOf(product.getPrice().replace(",", "").replace("Rs.", ""))) ?
                    String.format("₹ %s", miscValue.getOriginalPrice()) : "";
            holder.adapterCartBinding.strikeSuggestionPrice.setText(strikePrice);
            holder.adapterCartBinding.strikeSuggestionPrice.setPaintFlags(holder.adapterCartBinding.strikeSuggestionPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.adapterCartBinding.strikeSuggestionPrice.setVisibility(TextUtils.isEmpty(strikePrice) ? View.GONE : View.VISIBLE);
            holder.adapterCartBinding.mrpStrikeSuggestionPrice.setVisibility(!TextUtils.isEmpty(strikePrice) ? View.VISIBLE : View.GONE);
            holder.adapterCartBinding.SuggestionMrpPrice.setVisibility(!TextUtils.isEmpty(strikePrice) ? View.GONE : View.VISIBLE);
            String discount = (miscValue != null && !TextUtils.isEmpty(miscValue.getDiscount()) && Double.valueOf(miscValue.getDiscount()) > 0) ? String.format("%s %%", miscValue.getDiscount()) : "";
            holder.adapterCartBinding.SuggestionDiscount.setText(discount);
            holder.adapterCartBinding.SuggestionDiscount.setVisibility(!TextUtils.isEmpty(discount) ? View.VISIBLE : View.GONE);
        } else {
            holder.adapterCartBinding.SuggestionDiscount.setVisibility(View.GONE);
            holder.adapterCartBinding.strikeSuggestionPrice.setVisibility(View.GONE);
        }
        holder.adapterCartBinding.suggestionAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suggestionInterface.addProductToCart(product.getId());
            }
        });

        holder.adapterCartBinding.suggestionMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(product.getId()))
                    suggestionInterface.redirectToProductPage(product.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return recommendedProductList.size();
    }

    class CartSuggestionViewHolder extends RecyclerView.ViewHolder {
        private final AdapaterProductSuggestionBinding adapterCartBinding;

        CartSuggestionViewHolder(final AdapaterProductSuggestionBinding itemBinding) {
            super(itemBinding.getRoot());
            this.adapterCartBinding = itemBinding;
        }
    }

    public interface CartSuggestionInterface {
        void addProductToCart(String id);

        void redirectToProductPage(String sku);
    }
}
