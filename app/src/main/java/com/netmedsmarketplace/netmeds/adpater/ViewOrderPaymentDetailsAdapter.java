package com.netmedsmarketplace.netmeds.adpater;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterViewOrderPaymentDetailsBinding;
import com.netmedsmarketplace.netmeds.model.ViewOrderPaymentDetailsModel;

import java.util.List;

public class ViewOrderPaymentDetailsAdapter extends RecyclerView.Adapter<ViewOrderPaymentDetailsAdapter.ViewHolder> {
    private List<ViewOrderPaymentDetailsModel> paymentDetailsList;

    public ViewOrderPaymentDetailsAdapter(List<ViewOrderPaymentDetailsModel> paymentDetailsList) {
        this.paymentDetailsList = paymentDetailsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterViewOrderPaymentDetailsBinding orderBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_view_order_payment_details, viewGroup, false);
        return new ViewHolder(orderBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.binding.tvStrikeValue.setPaintFlags(viewHolder.binding.tvStrikeValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.bind(paymentDetailsList.get(i));
    }

    @Override
    public int getItemCount() {
        return paymentDetailsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private AdapterViewOrderPaymentDetailsBinding binding;

        public ViewHolder(AdapterViewOrderPaymentDetailsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ViewOrderPaymentDetailsModel detailsModel) {
            binding.setItem(detailsModel);
        }
    }
}
