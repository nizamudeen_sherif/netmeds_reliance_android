package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterWellnessBinding;
import com.netmedsmarketplace.netmeds.ui.CategoryActivity;
import com.netmedsmarketplace.netmeds.ui.ProductList;
import com.nms.netmeds.base.IntentConstant;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.util.ArrayList;

public class WellnessAdapter extends RecyclerView.Adapter<WellnessAdapter.WellnessHolder> {

    private final Context context;
    private ArrayList<MstarBanner> mstarBanners;


    public WellnessAdapter(Context context, ArrayList<MstarBanner> mstarBanners) {
        this.context = context;
        this.mstarBanners = mstarBanners;
    }

    @NonNull
    @Override
    public WellnessAdapter.WellnessHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterWellnessBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.adapter_wellness, parent, false);
        return new WellnessAdapter.WellnessHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull WellnessAdapter.WellnessHolder holder, int position) {
        final int pos = position;
        holder.binding.tvName.setText(mstarBanners.get(pos).getName());
        if (context != null && context.getApplicationContext() != null)
            Glide.with(context.getApplicationContext()).load(mstarBanners.get(pos).getImageUrl())
                    .error(Glide.with(holder.binding.image).load(R.drawable.ic_no_image)).into(holder.binding.image);
        holder.binding.lvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, mstarBanners != null && mstarBanners.get(pos).getLevel() == 1 ? CategoryActivity.class : ProductList.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(IntentConstant.CATEGORY_ID, Integer.valueOf(mstarBanners.get(pos).getId()));
                intent.putExtra(IntentConstant.CATEGORY_NAME, mstarBanners.get(pos).getName());
                intent.putExtra(IntentConstant.FROM_CATEGORY, (mstarBanners != null && mstarBanners.get(pos).getLevel() != 1));
                if ((mstarBanners != null && mstarBanners.get(pos).getLevel() != 1)) {
                    /*Google Analytics Event*/
                    GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_WELLNESS_SECTION, GoogleAnalyticsHelper.EVENT_ACTION_WELLNESS_PRODUCTS, GoogleAnalyticsHelper.EVENT_LABEL_HOME_PAGE);
                }
                if (context != null) {
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mstarBanners.size();
    }

    class WellnessHolder extends RecyclerView.ViewHolder {
        private AdapterWellnessBinding binding;

        WellnessHolder(AdapterWellnessBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public AdapterWellnessBinding getBinding() {
            return binding;
        }
    }
}
