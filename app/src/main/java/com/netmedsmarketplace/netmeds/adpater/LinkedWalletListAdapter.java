package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterPaymentHistoryLinkedWalletItemsBinding;
import com.nms.netmeds.base.model.LinkedWallet;
import com.nms.netmeds.base.utils.ImageUtils;

import java.util.List;

public class LinkedWalletListAdapter extends RecyclerView.Adapter<LinkedWalletListAdapter.ViewHolder> {
    private Context context;
    private List<LinkedWallet> walletList;
    private LinkedPaymentAdapterListener callback;
    private String itemFrom;

    public LinkedWalletListAdapter(Context context, List<LinkedWallet> walletList, LinkedPaymentAdapterListener callback, String itemFrom) {
        this.context = context;
        this.walletList = walletList;
        this.callback = callback;
        this.itemFrom = itemFrom;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        AdapterPaymentHistoryLinkedWalletItemsBinding linkedWalletItemsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_payment_history_linked_wallet_items, viewGroup, false);
        return new ViewHolder(linkedWalletItemsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LinkedWallet linkedWallet = walletList.get(position);
        holder.linkedWalletItemsBinding.tvWalletName.setText(linkedWallet.getDisplayName());
        Glide.with(context).load(ImageUtils.checkImage(linkedWallet.getImage())).into(holder.linkedWalletItemsBinding.imgWalletIcon);
        holder.linkedWalletItemsBinding.imgDelete.setOnClickListener(onItemCLick(linkedWallet));
        holder.linkedWalletItemsBinding.divider.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);

    }

    private View.OnClickListener onItemCLick(final LinkedWallet linkedWallet) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemClick(linkedWallet, itemFrom);
            }
        };
    }

    @Override
    public int getItemCount() {
        return walletList.size();
    }

    public interface LinkedPaymentAdapterListener {
        void onItemClick(Object obj, String clickFrom);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AdapterPaymentHistoryLinkedWalletItemsBinding linkedWalletItemsBinding;

        public ViewHolder(AdapterPaymentHistoryLinkedWalletItemsBinding linkedWalletItemsBinding) {
            super(linkedWalletItemsBinding.getRoot());
            this.linkedWalletItemsBinding = linkedWalletItemsBinding;
        }
    }
}
