package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.ItemPrimeOfferBinding;
import com.nms.netmeds.base.model.MemberShipIcon;

import java.util.List;

public class PrimeOffersAdapter extends RecyclerView.Adapter<PrimeOffersAdapter.PrimeOffersViewHolder> {

    private List<MemberShipIcon> items;
    private Context context;


    public PrimeOffersAdapter(Context context, List<MemberShipIcon> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public PrimeOffersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemPrimeOfferBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_prime_offer, viewGroup, false);
        return new PrimeOffersViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PrimeOffersViewHolder holder, int position) {
        MemberShipIcon memberShipIcon = items.get(position);
        holder.binding.linkPage.setText(memberShipIcon.linkpage);
        holder.binding.linkType.setText(memberShipIcon.linktype);
        Glide.with(context).load(memberShipIcon.getImageUrl()).into(holder.getBinding().imgOffer);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public class PrimeOffersViewHolder extends RecyclerView.ViewHolder {

        private ItemPrimeOfferBinding binding;

        public PrimeOffersViewHolder(ItemPrimeOfferBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ItemPrimeOfferBinding getBinding() {
            return binding;
        }
    }
}
