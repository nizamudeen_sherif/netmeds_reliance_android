package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.WellnessViewAllItemBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MstarSubCategory;
import com.nms.netmeds.base.model.MstarSubCategoryResult;

import java.util.List;

public class WellnessCategoryViewAllAdapter extends RecyclerView.Adapter<WellnessCategoryViewAllAdapter.WellnessCategoryViewAllAdapterViewHolder> {
    private List<MstarSubCategoryResult> categoryList;
    private Context context;
    private WellnessCategoryAdapterListener listener;
    private WellnessCategoryInterface categoryInterface;
    private boolean do_animate = true;


    public WellnessCategoryViewAllAdapter(Context mContext, List<MstarSubCategoryResult> list, WellnessCategoryAdapterListener listener, WellnessCategoryInterface categoryInterface) {
        this.context = mContext;
        this.categoryList = list;
        this.listener = listener;
        this.categoryInterface = categoryInterface;
    }

    @NonNull
    @Override
    public WellnessCategoryViewAllAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        WellnessViewAllItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.wellness_view_all_item, viewGroup, false);
        return new WellnessCategoryViewAllAdapterViewHolder(binding);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull WellnessCategoryViewAllAdapterViewHolder holder, int position) {
        CommonUtils.setAnimation(holder.binding.constraintLayout, do_animate, 100, context);
        final MstarSubCategoryResult result = categoryList.get(position);

        if (context != null)
            Glide.with(context.getApplicationContext())
                    .load(result !=null && !TextUtils.isEmpty(result.getThumbnailImagePath()) ? categoryInterface.getImageUrl(result)+result.getThumbnailImagePath():"")
                    .error(Glide.with(holder.binding.categoryImage).load(R.drawable.ic_no_image))
                    .into(holder.binding.categoryImage);

        StringBuilder sb = new StringBuilder();
        String prefix = "";
        if (result != null && result.getSubCategories().size() > 0) {
            for (MstarSubCategory child : result.getSubCategories()) {
                sb.append(prefix);
                prefix = ", ";
                sb.append(child.getName());
                holder.binding.tvSubCategory.setText(sb.toString());
            }
        }
        holder.binding.tvCategoryName.setText(result != null && !TextUtils.isEmpty(result.getName()) ? result.getName() : "");
        holder.binding.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.navigateCategoryActivity(result != null && result.getId() > 0 ? result.getId() : 1, result != null && !TextUtils.isEmpty(result.getName()) ? result.getName() : "");
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    class WellnessCategoryViewAllAdapterViewHolder extends RecyclerView.ViewHolder {
        private final WellnessViewAllItemBinding binding;

        WellnessCategoryViewAllAdapterViewHolder(WellnessViewAllItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface WellnessCategoryAdapterListener {
        void navigateCategoryActivity(Integer cateID, String catName);

    }

    public interface WellnessCategoryInterface {
        String getImageUrl(MstarSubCategoryResult result);
    }
}