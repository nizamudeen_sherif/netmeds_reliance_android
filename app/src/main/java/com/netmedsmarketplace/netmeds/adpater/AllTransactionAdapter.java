package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterTransactionItemBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MstarWalletHistory;
import com.nms.netmeds.base.utils.DateTimeUtils;

import java.util.List;

public class AllTransactionAdapter extends RecyclerView.Adapter<AllTransactionAdapter.TransactionViewHolder> {
    private Context context;
    private final List<MstarWalletHistory> transactionResultList;
    private final int listCount;

    public AllTransactionAdapter(List<MstarWalletHistory> transactionList, int listCount) {
        this.transactionResultList = transactionList;
        this.listCount = listCount;
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        AdapterTransactionItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_transaction_item, viewGroup, false);
        return new TransactionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position) {
        MstarWalletHistory result = transactionResultList.get(position);
        boolean isCredit = AppConstant.CREDIT.equalsIgnoreCase(result.getType());
        holder.binding.tvTrName.setText(result.getDescriptionNew());
        holder.binding.tvTrExpDateTime.setText(TextUtils.isEmpty(result.getExpiredDate()) ? "" : getSpanString(result.getExpiredDate(), context.getString(R.string.text_transaction_expiry_date_time), true));
        String amount = CommonUtils.getPriceInFormat(String.valueOf(result.getAmount()));
        holder.binding.tvTrAmount.setText(isCredit ? amount : "- " + amount);
        holder.binding.tvTrAmount.setTextColor(ContextCompat.getColor(context, isCredit ? R.color.colorGreen : R.color.colorRed));
        holder.binding.flTrType.setBackground(ContextCompat.getDrawable(context, isCredit ? R.drawable.green_round_background : R.drawable.red_round_background));
        holder.binding.imgTrType.setImageDrawable(ContextCompat.getDrawable(context, isCredit ? R.drawable.ic_cash_credit : R.drawable.ic_cash_debit));
        holder.binding.divider.setVisibility(position == listCount - 1 ? View.GONE : View.VISIBLE);
        holder.binding.orderId.setText(!TextUtils.isEmpty(result.getOrderId()) ? getSpanString(result.getOrderId(), context.getString(R.string.text_transaction_order_id), false) : "");
        holder.binding.orderId.setVisibility(TextUtils.isEmpty(result.getOrderId()) ? View.GONE : View.VISIBLE);
    }

    private SpannableString getSpanString(String text, String prefixText, boolean reformatDate) {
        if (reformatDate) {
            text = DateTimeUtils.getInstance().stringDate(text, DateTimeUtils.MMddyyyy, DateTimeUtils.ddMMMyyyy);
        }
        String totalString = String.format(prefixText, text);
        SpannableString resultString = new SpannableString(totalString);
        resultString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorDarkBlueGrey)), totalString.length() - text.length(), totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return resultString;
    }

    @Override
    public int getItemCount() {
        return listCount;
    }

    class TransactionViewHolder extends RecyclerView.ViewHolder {
        private final AdapterTransactionItemBinding binding;

        TransactionViewHolder(@NonNull AdapterTransactionItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
