package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.model.MStarProductDetails;

import java.util.List;

public class CartItemQtyAdapter extends RecyclerView.Adapter<CartItemQtyAdapter.QuantityViewHolder> {

    private final Context context;
    private final List<MStarCartAdapter.ItemQuantity> quantityList;
    private final MStarProductDetails cartItems;
    private final QuantityListListener listener;

    public CartItemQtyAdapter(Context context, List<MStarCartAdapter.ItemQuantity> quantityList, MStarProductDetails cartItems, QuantityListListener listener) {
        this.context = context;
        this.quantityList = quantityList;
        this.cartItems = cartItems;
        this.listener = listener;
    }

    @NonNull
    @Override
    public QuantityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new QuantityViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cart_quanity_dialog_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuantityViewHolder holder, int i) {
        MStarCartAdapter.ItemQuantity itemQuantity = quantityList.get(i);
        holder.tvQuantity.setText(itemQuantity.getQuantity());
        holder.checkBox.setChecked(itemQuantity.isQuantitySelected());
        holder.checkBox.setVisibility(itemQuantity.isQuantitySelected() ? View.VISIBLE : View.GONE);
        holder.cnParentItem.setBackgroundColor(ContextCompat.getColor(context, itemQuantity.isQuantitySelected() ? R.color.colorPaleBlueGrey : R.color.colorPrimary));
    }

    @Override
    public int getItemCount() {
        return quantityList.size();
    }

    public void updateList(int position) {
        for (int i = 0; i < quantityList.size(); i++) {
            quantityList.get(i).setQuantitySelected(i == position);
        }
        notifyDataSetChanged();
    }

    class QuantityViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvQuantity;
        private final CheckBox checkBox;
        private final LinearLayout cnParentItem;

        QuantityViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQuantity = itemView.findViewById(R.id.tv_qty);
            checkBox = itemView.findViewById(R.id.chk_qty);
            cnParentItem = itemView.findViewById(R.id.cn_item_parent);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.itemQuantitySelectionCallback(getAdapterPosition(), quantityList.get(getAdapterPosition()), cartItems);
                }
            });
        }
    }


    public interface QuantityListListener {
        void itemQuantitySelectionCallback(int position, MStarCartAdapter.ItemQuantity itemQuantity, MStarProductDetails cartItems);
    }
}

