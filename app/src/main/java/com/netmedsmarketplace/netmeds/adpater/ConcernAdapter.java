package com.netmedsmarketplace.netmeds.adpater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterConcernBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.ConcernSlides;
import com.nms.netmeds.base.model.HRVProduct;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;

import java.util.List;

public class ConcernAdapter extends RecyclerView.Adapter<ConcernAdapter.ConcernHolder> {

    private final Context context;
    private List<ConcernSlides> concernList;
    private boolean isConsultation;
    private List<HRVProduct> productList;
    private boolean isFromDiagnosticBanner;

    public ConcernAdapter(Context context, List<ConcernSlides> concernList, boolean isConsultation,boolean isFromDiagnosticBanner) {
        this.context = context;
        this.concernList = concernList;
        this.isConsultation = isConsultation;
        this.isFromDiagnosticBanner=isFromDiagnosticBanner;
    }

    public ConcernAdapter(Context context, List<HRVProduct> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public ConcernAdapter.ConcernHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterConcernBinding concernBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_concern, parent, false);
        return new ConcernAdapter.ConcernHolder(concernBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ConcernAdapter.ConcernHolder holder, final int position) {
        final ConcernSlides slide = concernList.get(position);
        Glide.with(context.getApplicationContext()).load(slide.getImage()).into(holder.concernBinding.imageView);

        holder.concernBinding.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFromDiagnosticBanner){
                    /*Google Tag Manager + FireBase Promotion Click Event*/
                    FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(context,slide,FireBaseAnalyticsHelper.EVENT_PARAM_HOME_DIAGNOSTIC_POPULAR,position+1);
                }
                CommonUtils.consultationNavigateToScreens(context,slide,(Activity)context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return isConsultation ? concernList.size() : productList.size();
    }

    class ConcernHolder extends RecyclerView.ViewHolder {
        private AdapterConcernBinding concernBinding;

        ConcernHolder(AdapterConcernBinding concernBinding) {
            super(concernBinding.getRoot());
            this.concernBinding = concernBinding;
        }
    }
}
