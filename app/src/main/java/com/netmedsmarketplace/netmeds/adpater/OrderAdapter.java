package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterOrderBinding;
import com.netmedsmarketplace.netmeds.databinding.AdapterOrderItemListItemBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.OrderListenerTypeEnum;
import com.nms.netmeds.base.model.ConfigurationResponse;
import com.nms.netmeds.base.model.MstarItems;
import com.nms.netmeds.base.model.MstarOrders;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private final Context context;
    private final List<MstarOrders> orderList;
    private boolean do_animate = true;
    private final OrderAdapter.OrderAdapterListener listener;

    public OrderAdapter(Context context, List<MstarOrders> orderList, OrderAdapterListener listener) {
        this.context = context;
        this.orderList = orderList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterOrderBinding orderBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_order, viewGroup, false);
        return new OrderViewHolder(orderBinding);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        CommonUtils.setAnimation(holder.adapterOrderBinding.orderCardView, do_animate, 100, context);
        MstarOrders orderListResponse = orderList.get(position);
        holder.adapterOrderBinding.tvPersonName.setText(orderListResponse.getName());
        holder.adapterOrderBinding.tvOrderStatus.setText(orderListResponse.getDisplayStatus());
        if (!TextUtils.isEmpty(orderListResponse.getStatusColor()))
            holder.adapterOrderBinding.tvOrderStatus.setTextColor(Color.parseColor(orderListResponse.getStatusColor()));
        holder.adapterOrderBinding.tvOrderStatus.setVisibility(orderListResponse.getPrimeMembershipOrder() ? View.GONE : View.VISIBLE);
        holder.adapterOrderBinding.tvDoctorConsultation.setVisibility(checkOrderStatus(orderListResponse) ? View.VISIBLE : View.GONE);
        holder.adapterOrderBinding.rvItemList.setVisibility(checkOrderStatus(orderListResponse) ? View.GONE : View.VISIBLE);
        setItemListAdapter(holder.adapterOrderBinding.rvItemList, orderListResponse, holder);

        holder.adapterOrderBinding.btnViewOrder.setBackground(ContextCompat.getDrawable(context, R.drawable.grey_background_button));
        holder.adapterOrderBinding.btnViewOrder.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlueGrey));
        holder.adapterOrderBinding.btnViewOrder.setOnClickListener(onClickListener(position, OrderListenerTypeEnum.VIEW_ORDER_DETAILS));

        holder.adapterOrderBinding.btnTrackOrder.setBackground(ContextCompat.getDrawable(context, R.drawable.accent_button));
        holder.adapterOrderBinding.btnTrackOrder.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        holder.adapterOrderBinding.btnTrackOrder.setOnClickListener(onClickListener(position, getOrderTypeEnum(orderListResponse)));
        holder.adapterOrderBinding.btnTrackOrder.setVisibility(orderListResponse.getPrimeMembershipOrder() ? View.INVISIBLE : View.VISIBLE);
        holder.adapterOrderBinding.btnTrackOrder.setText(getSecondButtonText(orderListResponse));
        holder.adapterOrderBinding.btnTrackOrder.setBackground(getBackgroundForSecondButton(orderListResponse));
        holder.adapterOrderBinding.btnTrackOrder.setEnabled(setEnableForSecondButton(orderListResponse));
        holder.adapterOrderBinding.btnTrackOrder.setClickable(setEnableForSecondButton(orderListResponse));
        holder.adapterOrderBinding.btnTrackOrder.setTextColor(ContextCompat.getColor(context, setEnableForSecondButton(orderListResponse) ? R.color.colorPrimary : R.color.colorLightPaleBlueGrey));
    }

    private boolean setEnableForSecondButton(MstarOrders orderListResponse) {
        if (AppConstant.DECLINED.equalsIgnoreCase(orderListResponse.getOrderStatus())) {
            if (AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(orderListResponse.getStatusDescription()))
                return true;
            else
                return !AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(orderListResponse.getStatusDescription());
        } else
            return true;
    }

    private Drawable getBackgroundForSecondButton(MstarOrders orderListResponse) {
        if (AppConstant.DECLINED.equalsIgnoreCase(orderListResponse.getOrderStatus())) {
            if (AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(orderListResponse.getStatusDescription())) {
                return context.getResources().getDrawable(R.drawable.accent_button);
            } else if (AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(orderListResponse.getStatusDescription()))
                return context.getResources().getDrawable(R.drawable.grey_background_button);
            else
                return context.getResources().getDrawable(R.drawable.accent_button);
        } else
            return context.getResources().getDrawable(R.drawable.accent_button);
    }

    private String getSecondButtonText(MstarOrders orderListResponse) {
        return context.getString(AppConstant.CANCELLED.equalsIgnoreCase(orderListResponse.getOrderStatus()) || (AppConstant.DECLINED.equalsIgnoreCase(orderListResponse.getOrderStatus())
                && !isOrderStatusForDecline(orderListResponse)) || AppConstant.DELIVERED.equalsIgnoreCase(orderListResponse.getOrderStatus())
                && !orderListResponse.getPrimeMembershipOrder() ? R.string.text_re_order : AppConstant.PAYMENT_PENDING_DESC.equals(orderListResponse.getStatusDescription())
                ? R.string.text_pay_now : isOrderStatusForDecline(orderListResponse) ? R.string.text_retry : R.string.text_track_order);
    }

    private boolean isOrderStatusForDecline(MstarOrders orderListResponse) {
        return (isPaymentPendingInProgress(orderListResponse) || isPaymentPendingFailed(orderListResponse));
    }

    private boolean isPaymentPendingInProgress(MstarOrders orderListResponse) {
        return orderListResponse != null && AppConstant.PAYMENT_CONFIRMATION_PENDING_DESC.equalsIgnoreCase(orderListResponse.getStatusDescription());
    }

    private boolean isPaymentPendingFailed(MstarOrders orderListResponse) {
        return orderListResponse != null && AppConstant.PAYMENT_FAILED_DESC.equalsIgnoreCase(orderListResponse.getStatusDescription());
    }


    private OrderListenerTypeEnum getOrderTypeEnum(MstarOrders orderListResponse) {
        return AppConstant.PAYMENT_PENDING_DESC.equals(orderListResponse.getStatusDescription()) ? OrderListenerTypeEnum.PAY :
                isOrderStatusForDecline(orderListResponse) ? OrderListenerTypeEnum.M1_RETRY : (AppConstant.CANCELLED.equalsIgnoreCase(orderListResponse.getOrderStatus()) ||
                        AppConstant.DECLINED.equalsIgnoreCase(orderListResponse.getOrderStatus()) || AppConstant.DELIVERED.equalsIgnoreCase(orderListResponse.getOrderStatus())) ?
                        orderListResponse.getItemsList().isEmpty() ? OrderListenerTypeEnum.M2_RE_ORDER : OrderListenerTypeEnum.RE_ORDER : OrderListenerTypeEnum.TRACK_ORDER;
    }

    private boolean checkOrderStatus(MstarOrders orderListResponse) {
        return orderListResponse.getItemsList() != null && orderListResponse.getItemsList().size() == 0 && !AppConstant.CANCELLED.equalsIgnoreCase(orderListResponse.getOrderStatus());
    }

    private void setItemListAdapter(RecyclerView rvItemList, MstarOrders orderListResponse, OrderViewHolder holder) {
        if (orderListResponse != null && orderListResponse.getItemsList() != null && !orderListResponse.getItemsList().isEmpty()) {
            rvItemList.setVisibility(View.VISIBLE);
            OrderItemAdapter orderItemAdapter = new OrderItemAdapter(orderListResponse.getItemsList(), AppConstant.DELIVERED.equalsIgnoreCase(orderListResponse.getOrderStatus()));
            holder.adapterOrderBinding.rvItemList.setLayoutManager(new LinearLayoutManager(context));
            holder.adapterOrderBinding.rvItemList.setNestedScrollingEnabled(false);
            rvItemList.setAdapter(orderItemAdapter);
        } else {
            rvItemList.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener onClickListener(final int position, final OrderListenerTypeEnum orderFromType) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null && orderList.get(position) != null) {
                    listener.onClickOfTrackAndViewOrder(position, orderList.get(position), orderFromType);
                }
            }
        };
    }

    public void updateOrderList(List<MstarOrders> list) {
        int size = this.orderList.size();
        this.orderList.addAll(list);
        notifyItemRangeInserted(size, list.size());
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }


    public interface OrderAdapterListener {
        void onClickOfTrackAndViewOrder(int position, MstarOrders order, OrderListenerTypeEnum orderFromType);

        void showLoader();
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {
        private final AdapterOrderBinding adapterOrderBinding;

        OrderViewHolder(final AdapterOrderBinding itemBinding) {
            super(itemBinding.getRoot());
            this.adapterOrderBinding = itemBinding;
        }
    }

    private class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.ViewHolder> {

        private final List<MstarItems> itemList;
        private boolean isDelivered;

        OrderItemAdapter(List<MstarItems> itemList, boolean isDelivered) {
            this.itemList = itemList;
            this.isDelivered = isDelivered;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            AdapterOrderItemListItemBinding orderBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_order_item_list_item, viewGroup, false);
            return new ViewHolder(orderBinding);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
            viewHolder.itemBinding.imgProductImage.setImageDrawable(ContextCompat.getDrawable(context, i > 1 ? R.drawable.ic_no_image : isPrimeProduct(itemList.get(i).getSku()) ? isDelivered ? R.drawable.ic_super_member : R.drawable.ic_crown_circle : R.drawable.ic_no_image));
            viewHolder.itemBinding.tvItemName1.setText(i > 1 && itemList.size()>3 ? context.getResources().getQuantityString(R.plurals.text_product_count, (itemList.size() - i), (itemList.size() - i)) : itemList.get(i).getBrandName());
        }

        private boolean isPrimeProduct(long drugCode) {
            ConfigurationResponse configurationResponse = new Gson().fromJson(BasePreference.getInstance(context).getConfiguration(), ConfigurationResponse.class);
            List<String> primeCodeList = configurationResponse != null && configurationResponse.getResult() != null && configurationResponse.getResult().getConfigDetails() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode() != null && configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode().size() > 0 ? configurationResponse.getResult().getConfigDetails().getPrimeConfig().getPrimeProduceCode() : new ArrayList<String>();
            return primeCodeList.contains(String.valueOf(drugCode));
        }

        @Override
        public int getItemCount() {
            return itemList.size() > 3 ? 3 : itemList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private final AdapterOrderItemListItemBinding itemBinding;

            ViewHolder(AdapterOrderItemListItemBinding binding) {
                super(binding.getRoot());
                this.itemBinding = binding;
            }
        }
    }
}
