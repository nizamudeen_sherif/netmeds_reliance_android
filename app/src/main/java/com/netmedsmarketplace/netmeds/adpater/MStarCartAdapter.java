package com.netmedsmarketplace.netmeds.adpater;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterCartBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MStarCartAdapter extends RecyclerView.Adapter<MStarCartAdapter.ViewHolder> implements CartItemQtyAdapter.QuantityListListener {

    private Context context;
    private Dialog dialog;
    private MStarCartAdapterCallBack callBack;
    private CartItemQtyAdapter cartItemQtyAdapter;

    private List<MStarProductDetails> lineItemsList;
    private List<String> primeProductCodeList;
    private ArrayList<String> interCityProductsList;

    private String imageUrl;
    private int scrolledPosition = 0;

    public MStarCartAdapter(Context context, MStarCartAdapterCallBack callBack,
                            List<MStarProductDetails> lineItemsList, String imageUrl, List<String> primeProductCodeList, ArrayList<String> interCityProductsList) {
        this.context = context;
        this.callBack = callBack;
        this.lineItemsList = lineItemsList;
        this.imageUrl = imageUrl;
        this.primeProductCodeList = primeProductCodeList;
        this.interCityProductsList = interCityProductsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        AdapterCartBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_cart, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bindValue();
    }

    @Override
    public int getItemCount() {
        return lineItemsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private AdapterCartBinding binding;

        public ViewHolder(final AdapterCartBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindValue() {
            final MStarProductDetails cartItems = lineItemsList.get(getAdapterPosition());
            boolean isItemIsAPrimeProduct = primeProductCodeList.contains(String.valueOf(cartItems.getProductCode()));
            binding.cartLlItemQtyPicker.setVisibility(callBack.isPrimeProduct(cartItems.getProductCode()) ? View.GONE : View.VISIBLE);
            if (!interCityProductsList.isEmpty() && interCityProductsList.contains(String.valueOf(cartItems.getProductCode()))) {
                binding.cartStockInfo.setText(context.getString(R.string.text_undeliverable));
                binding.cartStockInfo.setVisibility(View.VISIBLE);
                binding.cartLlItemQtyPicker.setVisibility(View.GONE);
                binding.cartAdapterMainLayout.setBackgroundColor(context.getResources().getColor(R.color.colorOpacityBlueGrey));
            }
            if (!AppConstant.PRODUCT_AVAILABLE_CODE.equalsIgnoreCase(cartItems.getAvailabilityStatus()) || ((cartItems.getCartQuantity() > cartItems.getStockQty()))) {
                binding.cartStockInfo.setText(context.getString((CommonUtils.isProductOutOfStock(cartItems.getAvailabilityStatus()) || ((cartItems.getCartQuantity() > cartItems.getStockQty()))) ? R.string.text_out_of_stock :
                        CommonUtils.isProductNotAvailable(cartItems.getAvailabilityStatus()) ? R.string.text_stock_not_available : CommonUtils.isProductNotForSale(cartItems.getAvailabilityStatus()) ? R.string.text_stock_not_for_sale : R.string.text_available));
                binding.cartStockInfo.setVisibility(View.VISIBLE);
                binding.cartLlItemQtyPicker.setVisibility(View.GONE);
                binding.cartAdapterMainLayout.setBackgroundColor(context.getResources().getColor(R.color.colorOpacityBlueGrey));
            }

            binding.viewAlternatives.setVisibility(AppConstant.OUT_OF_STOCK_STATUS.equalsIgnoreCase(cartItems.getAvailabilityStatus()) && cartItems.isRxRequired() ? View.VISIBLE : View.INVISIBLE);
            binding.viewAlternatives.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.viewAlternateClickCallback(cartItems.getProductCode());
                }
            });
            binding.cartAdapterDrugName.setText(cartItems.getDisplayName());
            binding.cartAlgoliaPrice.setText(CommonUtils.getPriceInFormat(cartItems.getSellingPrice().multiply(BigDecimal.valueOf(cartItems.getCartQuantity()))));
            binding.strikeCartAdapterPrice.setText(CommonUtils.getPriceInFormat(cartItems.getMrp().multiply(BigDecimal.valueOf(cartItems.getCartQuantity()))));
            binding.strikeCartAdapterPrice.setPaintFlags(binding.strikeCartAdapterPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.cartTvItemQty.setText(String.format(context.getString(R.string.text_item_qty_with_label), String.valueOf(cartItems.getCartQuantity())));
            binding.cartAdapterManufacturerName.setText(AppConstant.MFR.concat(" ").concat(!TextUtils.isEmpty(cartItems.getManufacturerName()) ? cartItems.getManufacturerName() : cartItems.getManufacturer() != null && !TextUtils.isEmpty(cartItems.getManufacturer().getName()) ? cartItems.getManufacturer().getName() : ""));
            binding.cartAdapterManufacturerName.setVisibility(isItemIsAPrimeProduct || TextUtils.isEmpty(cartItems.getPackSize()) ? View.GONE : View.VISIBLE);
            binding.cartLlItemQtyPicker.setOnClickListener(itemQuantityPickerDialog(cartItems));
            binding.mrpStrikeCartPrice.setVisibility(isDiscountAvailable(cartItems.getLineProductDiscount()) ? View.VISIBLE : View.GONE);
            binding.cartMrpAlgoliacartPrice.setVisibility(!isDiscountAvailable(cartItems.getLineProductDiscount()) ? View.VISIBLE : View.GONE);
            binding.strikeCartAdapterPrice.setVisibility(isDiscountAvailable(cartItems.getLineProductDiscount()) ? View.VISIBLE : View.GONE);
            binding.cartAdapterDrugDetail.setVisibility(View.GONE);
            binding.cartAdapterRxImage.setVisibility(cartItems.isRxRequired() ? View.VISIBLE : View.GONE);
            if (cartItems.isRxRequired()) {
                binding.cartAdapterDrugDetail.setText(!TextUtils.isEmpty(cartItems.getPackSize()) ? cartItems.getPackSize() + context.getString(R.string.text_tablets_strip) : "");
            } else {
                binding.cartAdapterDrugDetail.setText(!TextUtils.isEmpty(cartItems.getPackSize()) ? context.getString(R.string.text_pack_of_size) + cartItems.getPackSize() + context.getString(R.string.text_s) : "");
            }
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context).load(!TextUtils.isEmpty(cartItems.getProduct_image_path()) ? imageUrl + cartItems.getProduct_image_path() : R.drawable.ic_no_image)
                    .apply(options)
                    .into(binding.cartAdapterProductImage);
            binding.cartDeleteProduct.setOnClickListener(callBack.removeProductFromCart(cartItems, getAdapterPosition()));
            binding.cartAdapterProductView.setOnClickListener(callBack.navigateToProductDetails(cartItems.getProductCode()));

            if (cartItems.isRxRequired())
                callBack.isRxRequiredProductAvailableInCart(cartItems.isRxRequired(), cartItems);
            if (cartItems.isOutofStock()) {
                callBack.setOutOfStockCodeList(cartItems.getProductCode(), cartItems);
            }

            //Alternate Product Revert View
            binding.clRevertView.setVisibility(cartItems.isAlternateSwitched() ? View.VISIBLE : View.GONE);
            if (cartItems.isAlternateSwitched()) {
                String productName = cartItems.getAlternateProductName();
                if (!TextUtils.isEmpty(productName)) {
                    binding.tvCartAdapterOriginalProductName.setText(productName);
                    binding.tvCartOriginalProductPrice.setText(CommonUtils.getPriceInFormat(cartItems.getAlternateProductLineValue()));
                } else {
                    binding.clRevertView.setVisibility(View.GONE);
                }
                binding.tvTabToRevert.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_ALT_CART, GoogleAnalyticsHelper.EVENT_ACTION_ALTERNATE_CART_TAP_TO_REVERT, GoogleAnalyticsHelper.POST_SCREEN_CART_PAGE);
                        callBack.initiateTapToRevert(cartItems.getProductCode());
                    }
                });
            }
        }

        private boolean isDiscountAvailable(BigDecimal discount) {
            return discount.compareTo(BigDecimal.ZERO) > 0;
        }


        private View.OnClickListener itemQuantityPickerDialog(final MStarProductDetails cartItems) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View dialogView = LayoutInflater.from(context).inflate(R.layout.cart_item_qty_picker_dialog, null);
                    builder.setView(dialogView);
                    ImageView closeDialog = dialogView.findViewById(R.id.img_close_qty_list_dialog);
                    RecyclerView rvQuantityList = dialogView.findViewById(R.id.rv_qty_list);
                    setRVProperties(rvQuantityList, cartItems);
                    dialog = builder.create();
                    closeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            };
        }

        private void setRVProperties(RecyclerView rvQuantityList, MStarProductDetails cartItems) {
            rvQuantityList.setNestedScrollingEnabled(false);
            rvQuantityList.setLayoutManager(new LinearLayoutManager(context));
            int maxQuantity = cartItems.getStockQty() < cartItems.getMaxQtyInOrder() ? cartItems.getStockQty() : cartItems.getMaxQtyInOrder();
            rvQuantityList.setAdapter(getQuantityAdapter(getQuantityList(maxQuantity, cartItems.getCartQuantity()), cartItems));
            rvQuantityList.scrollToPosition(scrolledPosition);
        }

        private List<MStarCartAdapter.ItemQuantity> getQuantityList(Integer quantity, Integer cartResultQuantity) {
            List<MStarCartAdapter.ItemQuantity> itemQuantityList = new ArrayList<>();
            for (int i = 1; i <= quantity; i++) {
                MStarCartAdapter.ItemQuantity itemQuantity = new MStarCartAdapter.ItemQuantity();
                itemQuantity.setQuantity(String.valueOf(i));
                itemQuantity.setQuantitySelected((i == cartResultQuantity));
                scrolledPosition = (i == cartResultQuantity) ? i - 1 : scrolledPosition;
                itemQuantityList.add(itemQuantity);
            }
            return itemQuantityList;
        }
    }

    private CartItemQtyAdapter getQuantityAdapter(List<MStarCartAdapter.ItemQuantity> quantityList, MStarProductDetails cartItems) {
        cartItemQtyAdapter = new CartItemQtyAdapter(context, quantityList, cartItems, this);
        return cartItemQtyAdapter;
    }

    @Override
    public void itemQuantitySelectionCallback(int count, ItemQuantity itemQuantity, MStarProductDetails cartItems) {
        if (dialog != null) {
            dialog.dismiss();
        }
        scrolledPosition = count;
        cartItemQtyAdapter.updateList(count);
        int previousQty = cartItems.getCartQuantity();
        int newQty = count + 1;
        if (previousQty > newQty) {
            callBack.decreaseQuantity(cartItems.getProductCode(), previousQty - newQty);
        } else if (previousQty < newQty) {
            callBack.increaseQuantity(cartItems.getProductCode(), newQty - previousQty);
        }
    }

    public static class ItemQuantity {
        private String quantity;
        private boolean isQuantitySelected;

        String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        boolean isQuantitySelected() {
            return isQuantitySelected;
        }

        public void setQuantitySelected(boolean quantitySelected) {
            isQuantitySelected = quantitySelected;
        }
    }

    public interface MStarCartAdapterCallBack {
        void increaseQuantity(int productCode, int updatedQuantity);

        void decreaseQuantity(int productCode, int updatedQuantity);

        void isRxRequiredProductAvailableInCart(boolean rxRequiredProductAvailability, MStarProductDetails cartItem);

        View.OnClickListener navigateToProductDetails(int productCode);

        View.OnClickListener removeProductFromCart(MStarProductDetails product, int position);

        void viewAlternateClickCallback(int productCode);

        void setOutOfStockCodeList(Integer outOfStockProductCode, MStarProductDetails productDetails);

        boolean isPrimeProduct(int productCode);

        void initiateTapToRevert(int productCode);
    }
}
