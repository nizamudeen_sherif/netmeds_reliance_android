package com.netmedsmarketplace.netmeds.adpater;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterAttachPrescriptionProductBinding;

import java.util.ArrayList;

public class AttachPrescriptionProductAdapter extends RecyclerView.Adapter<AttachPrescriptionProductAdapter.AttachPrescriptionProductHolder> {

    private final ArrayList<String> mPrescriptionProductList;

    public AttachPrescriptionProductAdapter(ArrayList<String> prescriptionProductList) {
        this.mPrescriptionProductList = prescriptionProductList;
    }

    @NonNull
    @Override
    public AttachPrescriptionProductHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterAttachPrescriptionProductBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_attach_prescription_product, viewGroup, false);
        return new AttachPrescriptionProductHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AttachPrescriptionProductHolder holder, int position) {
        holder.binding.productName.setText(!TextUtils.isEmpty(mPrescriptionProductList.get(position)) ? mPrescriptionProductList.get(position) : "");
    }

    @Override
    public int getItemCount() {
        return mPrescriptionProductList.size();
    }

    class AttachPrescriptionProductHolder extends RecyclerView.ViewHolder {
        private final AdapterAttachPrescriptionProductBinding binding;

        AttachPrescriptionProductHolder(AdapterAttachPrescriptionProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
