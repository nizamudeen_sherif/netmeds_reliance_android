package com.netmedsmarketplace.netmeds.adpater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.MstarPromoCodeItemBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.PromoCodeList;

import java.util.List;

public class MstarCouponCodeAdapter extends RecyclerView.Adapter<MstarCouponCodeAdapter.PromoCodeHolder> {
    private final List<PromoCodeList> couponList;
    private final Context context;
    private boolean isClickOffer;

    public MstarCouponCodeAdapter(Context context, List<PromoCodeList> couponList, boolean isClickOffer) {
        this.couponList = couponList;
        this.context = context;
        this.isClickOffer = isClickOffer;
    }

    @NonNull
    @Override
    public PromoCodeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MstarPromoCodeItemBinding appliedCouponItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.mstar_promo_code_item, parent, false);
        return new PromoCodeHolder(appliedCouponItemBinding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final PromoCodeHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PromoCodeList promoCodeList = couponList.get(position);
        holder.binding.imgChecked.setImageResource(R.drawable.ic_offer_blue);
        holder.binding.txtCouponCode.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
        holder.binding.txtCouponCode.setText(promoCodeList.getCouponCode());
        holder.binding.txtCouponMessage.setText(promoCodeList.getDescription());
        holder.binding.tvSave.setText(String.format(context.getString(R.string.text_promo_saving), !TextUtils.isEmpty(promoCodeList.getDiscount()) ? promoCodeList.getDiscount() : "") + "%");
    }


    @Override
    public int getItemCount() {
        return isClickOffer ? couponList.size() : 1;
    }

    class PromoCodeHolder extends RecyclerView.ViewHolder {
        private final MstarPromoCodeItemBinding binding;

        PromoCodeHolder(MstarPromoCodeItemBinding appliedCouponItemBinding) {
            super(appliedCouponItemBinding.getRoot());
            this.binding = appliedCouponItemBinding;
        }
    }
}
