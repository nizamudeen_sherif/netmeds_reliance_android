package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterUploadedImageBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.config.ConfigConstant;
import com.nms.netmeds.base.config.ConfigMap;
import com.nms.netmeds.base.retrofit.APIPathConstant;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.List;

public class OrderConfirmationPrescriptionAdapter extends RecyclerView.Adapter<OrderConfirmationPrescriptionAdapter.PrescriptionViewHolder> {

    private List<String> prescriptions;
    private Context context;
    private PrescriptionAdapterCallback callback;

    public OrderConfirmationPrescriptionAdapter(List<String> prescriptions, PrescriptionAdapterCallback callback) {
        this.prescriptions = prescriptions;
        this.callback = callback;
    }

    @NonNull
    @Override
    public PrescriptionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterUploadedImageBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_uploaded_image, viewGroup, false);
        this.context = viewGroup.getContext();
        return new PrescriptionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final PrescriptionViewHolder prescriptionViewHolder, final int position) {
        final String prescriptionId = prescriptions.get(position);

        if (!TextUtils.isEmpty(prescriptionId)) {
            final String rxUrl = ConfigMap.getInstance().getProperty(ConfigConstant.MSTAR_BASE_URL) + APIPathConstant.DOWNLOAD_PRESCRIPTION + prescriptionId;
            Glide.with(context.getApplicationContext()).load(CommonUtils.getGlideUrl(rxUrl, BasePreference.getInstance(context).getMstarBasicHeaderMap())).apply(new RequestOptions().fitCenter()).into(prescriptionViewHolder.binding.prescriptionImage);
            prescriptionViewHolder.binding.removePrescription.setVisibility(View.GONE);
            prescriptionViewHolder.binding.contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.previewPrescription(CommonUtils.getGlideUrl(rxUrl, BasePreference.getInstance(context).getMstarBasicHeaderMap()).toStringUrl());
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return prescriptions != null ? prescriptions.size() : 0;
    }

    class PrescriptionViewHolder extends RecyclerView.ViewHolder {
        AdapterUploadedImageBinding binding;

        PrescriptionViewHolder(@NonNull AdapterUploadedImageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface PrescriptionAdapterCallback {
        void previewPrescription(String rxUrl);
    }
}
