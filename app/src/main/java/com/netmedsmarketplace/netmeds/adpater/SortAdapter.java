package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.model.SortOptionLabel;

import java.util.List;

public class SortAdapter extends RecyclerView.Adapter<SortAdapter.SortHolder> {

    private final List<SortOptionLabel> sortOptionList;
    private final SortListListener listener;
    private final Context context;

    public SortAdapter(List<SortOptionLabel> sortOptionList, SortListListener listener, Context context) {
        this.sortOptionList = sortOptionList;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public SortHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_sort, viewGroup, false);
        return new SortAdapter.SortHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SortHolder sortHolder, int i) {
        final SortOptionLabel sortOption = sortOptionList.get(i);
        sortHolder.sortTitle.setText(sortOption.getKey());
        sortHolder.sortTitle.setTextColor(sortOption.isSelected() ? context.getResources().getColor(R.color.colorBlueLight) :
                context.getResources().getColor(R.color.colorDarkBlueGrey));
        sortHolder.mainLayout.setBackground(sortOption.isSelected() ? context.getResources().getDrawable(R.drawable.teal_button_background) :
                context.getResources().getDrawable(R.drawable.concern_background));
        sortHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (SortOptionLabel label : sortOptionList) {
                    label.setSelected(sortOption.getValue().equalsIgnoreCase(label.getValue()));
                }
                listener.setSortSelection(sortOption);
                notifyDataSetChanged();
            }
        });
        if(sortOption.isSelected())
            listener.setSortSelection(sortOption);
    }

    @Override
    public int getItemCount() {
        return sortOptionList.size();
    }

    public interface SortListListener {
        void setSortSelection(SortOptionLabel sortOption);
    }

    class SortHolder extends RecyclerView.ViewHolder {

        private final TextView sortTitle;
        private final LinearLayout mainLayout;

        SortHolder(@NonNull View itemView) {
            super(itemView);
            sortTitle = itemView.findViewById(R.id.sortTitle);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }
}