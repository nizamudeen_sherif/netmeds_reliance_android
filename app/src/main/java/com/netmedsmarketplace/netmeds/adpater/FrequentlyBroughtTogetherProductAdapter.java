package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterProductCombinationBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.ImageUtils;

import java.math.BigDecimal;
import java.util.List;

public class FrequentlyBroughtTogetherProductAdapter extends RecyclerView.Adapter<FrequentlyBroughtTogetherProductAdapter.FrequentlyBroughtTogetherProductViewHolder> {

    private List<MStarProductDetails> productList;
    private ProductCombinationInterface callback;
    private Context context;

    public FrequentlyBroughtTogetherProductAdapter(List<MStarProductDetails> list, ProductCombinationInterface callback, Context context) {
        this.productList = list;
        this.callback = callback;
        this.context = context;
    }

    @NonNull
    @Override
    public FrequentlyBroughtTogetherProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterProductCombinationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_product_combination, viewGroup, false);
        return new FrequentlyBroughtTogetherProductViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull FrequentlyBroughtTogetherProductViewHolder holder, int i) {
        holder.bindFrequentlyBroughtTogetherProduct();
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class FrequentlyBroughtTogetherProductViewHolder extends RecyclerView.ViewHolder {
        AdapterProductCombinationBinding binding;

        FrequentlyBroughtTogetherProductViewHolder(@NonNull AdapterProductCombinationBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bindFrequentlyBroughtTogetherProduct() {
            MStarProductDetails productDetails = productList.get(getAdapterPosition());
            boolean isDiscountAvailable = productDetails != null && productDetails.getDiscount().compareTo(BigDecimal.ZERO) > 0;
            if (context != null) {
                Glide.with(context)
                        .load(ImageUtils.checkImage(callback.getImageUrl(productDetails)))
                        .error(Glide.with(binding.productImage).load(R.drawable.ic_no_image))
                        .into(binding.productImage);
            }
            binding.productName.setText(productDetails != null && !TextUtils.isEmpty(productDetails.getDisplayName()) ? productDetails.getDisplayName() : "");
            binding.productDescription.setText(productDetails != null && !TextUtils.isEmpty(productDetails.getPackLabel()) ? productDetails.getPackLabel() : "");
            binding.productDescription.setVisibility(productDetails != null && !TextUtils.isEmpty(productDetails.getPackLabel()) ? View.VISIBLE : View.GONE);
            binding.productPrice.setText(productDetails != null ? CommonUtils.getPriceInFormat(productDetails.getSellingPrice()) : "");
            binding.productStrikePrice.setText(isDiscountAvailable ? CommonUtils.getPriceInFormat(productDetails.getMrp()) : "");
            binding.productStrikePrice.setPaintFlags(binding.productStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            binding.productStrikePrice.setVisibility(isDiscountAvailable ? View.VISIBLE : View.GONE);
            binding.productMrpStrikePrice.setVisibility(isDiscountAvailable ? View.VISIBLE : View.GONE);
            binding.productMrpPrice.setVisibility(isDiscountAvailable ? View.GONE : View.VISIBLE);
            binding.productAdd.setVisibility(getAdapterPosition() != productList.size() - 1 ? View.VISIBLE : View.GONE);
            binding.productMainLayout.setOnClickListener(callback.navigateToProduct(productDetails.getProductCode()));
        }
    }

    public interface ProductCombinationInterface {
        Context getContext();

        View.OnClickListener navigateToProduct(int sku);

        String getImageUrl(MStarProductDetails productDetails);
    }
}