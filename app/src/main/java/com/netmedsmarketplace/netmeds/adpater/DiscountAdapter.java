package com.netmedsmarketplace.netmeds.adpater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.DiscountItemViewBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBanner;
import com.nms.netmeds.base.utils.DateTimeUtils;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;

import java.util.Date;
import java.util.List;

public class DiscountAdapter extends RecyclerView.Adapter<DiscountAdapter.DiscountAdapterViewHolder> {
    List<MStarProductDetails> list;
    private Context context;
    private String subHeader;

    public DiscountAdapter(List<MStarProductDetails> wellnessBannerList,String subHeader) {
        this.list = wellnessBannerList;
        this.subHeader=subHeader;
    }

    @NonNull
    @Override
    public DiscountAdapter.DiscountAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        DiscountItemViewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.discount_item_view, viewGroup, false);
        context = viewGroup.getContext();
        return new DiscountAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DiscountAdapter.DiscountAdapterViewHolder holder, final int position) {
        final MStarProductDetails banner = list.get(position);
        final MstarBanner bannerDetails = new MstarBanner();
        bannerDetails.setImageName(banner.getImageName());
        bannerDetails.setImageUrl(banner.getProduct_image_path());
        bannerDetails.setUrl(banner.getUrl());
        bannerDetails.setLinktype(banner.getLinktype());
        bannerDetails.setDisplayFrom(banner.getDisplayFrom());
        bannerDetails.setDisplayTo(banner.getDisplayTo());
        if (context != null && context.getApplicationContext() != null)
            setGlideWithUrl(banner.getProduct_image_path(), holder.binding.discountImage, context, banner.getDisplayFrom(), banner.getDisplayTo());
        holder.binding.discountImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Google Tag Manger + FireBase Promotion Click Event*/
                FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(context,banner,subHeader,position+1);
                CommonUtils.wellnessNavigateToScreens(context, bannerDetails, (Activity) context);

            }
        });
    }

    private void setGlideWithUrl(String url, ImageView imageView, Context context, String displayFrom, String displayTo) {

        if (new Date().getTime() > DateTimeUtils.getInstance().convertDateToMilliSecond(displayFrom, DateTimeUtils.yyyyMMddHHmmss) && new Date().getTime() < DateTimeUtils.getInstance().convertDateToMilliSecond(displayTo, DateTimeUtils.yyyyMMddHHmmss)) {
            Glide.with(context)
                    .load(url)
                    .error(Glide.with(imageView).load(R.drawable.ic_no_image)).into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class DiscountAdapterViewHolder extends RecyclerView.ViewHolder {
        private final DiscountItemViewBinding binding;

        DiscountAdapterViewHolder(DiscountItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}