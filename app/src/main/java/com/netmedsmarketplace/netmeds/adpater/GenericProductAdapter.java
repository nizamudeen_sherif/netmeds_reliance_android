package com.netmedsmarketplace.netmeds.adpater;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.GenericMedicinesAdapterBinding;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;

public class GenericProductAdapter extends RecyclerView.Adapter<GenericProductAdapter.GenericViewHolder> implements CartItemQtyAdapter.QuantityListListener {

    private Context context;
    private GenericMedicinesAdapterBinding binding;
    private List<MStarProductDetails> productDetailsList;
    private Map<Integer, Integer> cartProductMap;
    private Dialog dialog;
    private int scrolledPosition = 0;
    private CartItemQtyAdapter cartItemQtyAdapter;
    private GenericAdapterCallback callBack;


    public GenericProductAdapter(List<MStarProductDetails> productDetailsList, Map<Integer, Integer> cartProductMap, GenericAdapterCallback callback) {
        this.callBack = callback;
        this.productDetailsList = productDetailsList;
        this.cartProductMap = cartProductMap;
    }

    @NonNull
    @Override
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.generic_medicines_adapter, parent, false);
        this.context = parent.getContext();
        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        bindValue(position);
    }

    void bindValue(int position) {

        final MStarProductDetails cartItems = productDetailsList.get(position);
        cartItems.setCartQuantity(1);

        binding.drugName.setText(cartItems.getDisplayName());

        binding.algoliaPricex.setText(CommonUtils.getPriceInFormat(cartItems.getSellingPrice().multiply(BigDecimal.valueOf(cartItems.getCartQuantity()))));
        binding.algoliaPricex.setVisibility(isDiscountAvailable(cartItems) ? GONE : View.VISIBLE);
        binding.algoliaPriceMrp.setVisibility(isDiscountAvailable(cartItems) ? GONE : View.VISIBLE);

        binding.strikePricex.setText(CommonUtils.getPriceInFormat(cartItems.getMrp().multiply(BigDecimal.valueOf(cartItems.getCartQuantity()))));
        binding.strikePricex.setPaintFlags(binding.strikePricex.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        binding.strikePriceMrp.setVisibility(isDiscountAvailable(cartItems) ? View.VISIBLE : GONE);
        binding.strikePricex.setVisibility(isDiscountAvailable(cartItems) ? View.VISIBLE : GONE);

        binding.manufacturerNamex.setText(AppConstant.MFR.concat(" ").concat(!TextUtils.isEmpty(cartItems.getManufacturerName()) ? cartItems.getManufacturerName() : cartItems.getManufacturer() != null && !TextUtils.isEmpty(cartItems.getManufacturer().getName()) ? cartItems.getManufacturer().getName() : ""));

        binding.packlabelNamex.setText(cartItems.getPackLabel());

        binding.youSaveText.setText(!TextUtils.isEmpty(cartItems.getYouSave()) ? context.getString(R.string.text_you_save_generic) + CommonUtils.getRoundedOffBigDecimal(new BigDecimal(cartItems.getYouSave())) : "");
        binding.youSaveText.setVisibility(!TextUtils.isEmpty(cartItems.getYouSave()) && Double.parseDouble(cartItems.getYouSave()) > 1.0 ? View.VISIBLE : View.INVISIBLE);

        binding.gGenericDetail.setText(!TextUtils.isEmpty(cartItems.getPerTabletCost()) ? cartItems.getPerTabletCost() : "");

        binding.cheaperText.setText(!TextUtils.isEmpty(cartItems.getCheaper()) ? cartItems.getCheaper() + context.getString(R.string.string_cheaper) : "");
        binding.cheaperTextLayout.setVisibility(!TextUtils.isEmpty(cartItems.getCheaper()) && Double.parseDouble(cartItems.getCheaper().split("%")[0]) > 0.0 ? View.VISIBLE : View.INVISIBLE);


        if (cartProductMap.containsKey(cartItems.getProductCode())) {
            binding.addToCartGeneric.setVisibility(GONE);
            binding.qtyPickerLayoutx.setVisibility(View.VISIBLE);
            cartItems.setCartQuantity(cartProductMap.get(cartItems.getProductCode()));
            binding.qtyTvItemQty.setText(String.format(binding.getRoot().getContext().getString(R.string.text_item_qty_with_label), String.valueOf(cartItems.getCartQuantity())));
            binding.qtyPickerLayoutx.setOnClickListener(itemQuantityPickerDialog(cartItems));
        } else {
            binding.addToCartGeneric.setVisibility(View.VISIBLE);
            binding.qtyPickerLayoutx.setVisibility(GONE);
            binding.addToCartGeneric.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.increaseQuantity(cartItems.getProductCode(), 1, cartItems, true);
                }
            });
        }
        RequestOptions options = new RequestOptions()
                .fitCenter()
                .placeholder(R.drawable.ic_no_image)
                .error(R.drawable.ic_no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(!TextUtils.isEmpty(cartItems.getProduct_image_path()) ? cartItems.getProduct_image_path() : R.drawable.ic_no_image)
                .apply(options)
                .into(binding.cartAdapterProductImage);

        binding.adapterRx.setVisibility(cartItems.isRxRequired() ? View.VISIBLE : GONE);

        binding.cartDeleteProduct.setOnClickListener(callBack.removeProductFromCart(cartItems));

        binding.productView.setOnClickListener(callBack.navigateToProductDetails(cartItems.getProductCode()));


    }


    private View.OnClickListener itemQuantityPickerDialog(final MStarProductDetails cartItems) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View dialogView = LayoutInflater.from(context).inflate(R.layout.cart_item_qty_picker_dialog, null);
                builder.setView(dialogView);
                ImageView closeDialog = dialogView.findViewById(R.id.img_close_qty_list_dialog);
                RecyclerView rvQuantityList = dialogView.findViewById(R.id.rv_qty_list);
                setRVProperties(rvQuantityList, cartItems);
                dialog = builder.create();
                closeDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        };
    }

    private void setRVProperties(RecyclerView rvQuantityList, MStarProductDetails cartItems) {
        rvQuantityList.setNestedScrollingEnabled(false);
        rvQuantityList.setLayoutManager(new LinearLayoutManager(context));
        int maxQuantity = cartItems.getStockQty() < cartItems.getMaxQtyInOrder() ? cartItems.getStockQty() : cartItems.getMaxQtyInOrder();
        rvQuantityList.setAdapter(getQuantityAdapter(getQuantityList(maxQuantity, cartItems.getCartQuantity()), cartItems));
        rvQuantityList.scrollToPosition(scrolledPosition);
    }

    private List<MStarCartAdapter.ItemQuantity> getQuantityList(Integer quantity, Integer cartResultQuantity) {
        List<MStarCartAdapter.ItemQuantity> itemQuantityList = new ArrayList<>();
        for (int i = 1; i <= quantity; i++) {
            MStarCartAdapter.ItemQuantity itemQuantity = new MStarCartAdapter.ItemQuantity();
            itemQuantity.setQuantity(String.valueOf(i));
            itemQuantity.setQuantitySelected((i == cartResultQuantity));
            scrolledPosition = (i == cartResultQuantity) ? i - 1 : scrolledPosition;
            itemQuantityList.add(itemQuantity);
        }
        return itemQuantityList;
    }


    private CartItemQtyAdapter getQuantityAdapter(List<MStarCartAdapter.ItemQuantity> quantityList, MStarProductDetails cartItems) {
        cartItemQtyAdapter = new CartItemQtyAdapter(context, quantityList, cartItems, this);
        return cartItemQtyAdapter;
    }

    private boolean isDiscountAvailable(MStarProductDetails productDetails) {
        return productDetails.getMrp().compareTo(productDetails.getSellingPrice()) > 0;
    }

    @Override
    public void itemQuantitySelectionCallback(int position, MStarCartAdapter.ItemQuantity itemQuantity, MStarProductDetails cartItems) {
        if (dialog != null) {
            dialog.dismiss();
        }
        scrolledPosition = position;
        cartItemQtyAdapter.updateList(position);
        int previousQty = cartItems.getCartQuantity();
        int newQty = position + 1;
        if (previousQty > newQty) {
            callBack.decreaseQuantity(cartItems.getProductCode(), previousQty - newQty, cartItems, true);
        } else if (previousQty < newQty) {
            callBack.increaseQuantity(cartItems.getProductCode(), newQty - previousQty, cartItems, true);
        }
    }


    @Override
    public int getItemCount() {
        return productDetailsList != null ? productDetailsList.size() : 0;
    }


    public class GenericViewHolder extends RecyclerView.ViewHolder {

        GenericMedicinesAdapterBinding binding;

        public GenericViewHolder(@NonNull GenericMedicinesAdapterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface GenericAdapterCallback {

        View.OnClickListener removeProductFromCart(MStarProductDetails cartItems);

        void increaseQuantity(int productCode, int updatedQuantity, MStarProductDetails productDetails, boolean isFromGenericAdapter);

        void decreaseQuantity(int productCode, int updatedQuantity, MStarProductDetails productDetails, boolean isFromGenericAdapter);

        View.OnClickListener navigateToProductDetails(int productCode);

    }
}
