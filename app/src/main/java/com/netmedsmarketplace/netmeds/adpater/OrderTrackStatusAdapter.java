package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterTrackOrderStatusItemBinding;
import com.nms.netmeds.base.model.MstarOrderTrackStatus;
import com.nms.netmeds.base.utils.DateTimeUtils;

import java.util.List;
import java.util.Map;

public class OrderTrackStatusAdapter extends RecyclerView.Adapter<OrderTrackStatusAdapter.ViewHolder> {
    private final Context context;
    private OrderTrackStatusAdapterListener listener;
    private final List<MstarOrderTrackStatus> trackStatusDetailsList;
    private final Map<String, Integer> activeDrawables;
    private final Map<String, Integer> inActiveDrawables;

    public OrderTrackStatusAdapter(Context context, List<MstarOrderTrackStatus> trackStatusDetailsList, Map<String, Integer> activeDrawables, Map<String, Integer> inActiveDrawables, OrderTrackStatusAdapterListener listener) {
        this.context = context;
        this.trackStatusDetailsList = trackStatusDetailsList;
        this.activeDrawables = activeDrawables;
        this.inActiveDrawables = inActiveDrawables;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        AdapterTrackOrderStatusItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_track_order_status_item, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MstarOrderTrackStatus trackStatusDetail = trackStatusDetailsList.get(position);
        holder.binding.tvStatusValue.setVisibility(TextUtils.isEmpty(trackStatusDetail.getDate()) ? View.GONE : View.VISIBLE);
        holder.binding.tvStatusValue.setText(TextUtils.isEmpty(trackStatusDetail.getDate()) ? "" : DateTimeUtils.getInstance().stringDate(trackStatusDetail.getDate(), DateTimeUtils.yyyyMMddTHHmmss, DateTimeUtils.ddMMMhhmma).replace("AM", "am").replace("PM", "pm"));
        holder.binding.flStatus.setBackground(ContextCompat.getDrawable(context, TextUtils.isEmpty(trackStatusDetail.getDate()) ? R.drawable.subscription_disabled_button_background : R.drawable.pale_blue_button_round_background));
        holder.binding.imgStatus.setImageDrawable(ContextCompat.getDrawable(context, TextUtils.isEmpty(trackStatusDetail.getDate()) ? inActiveDrawables.get(trackStatusDetail.getShortStatus()) : activeDrawables.get(trackStatusDetail.getShortStatus())));
        holder.binding.tvStatusLabel.setText(trackStatusDetail.getStatus());
        holder.binding.tvStatusLabel.setTypeface(Typeface.createFromAsset(context.getAssets(), TextUtils.isEmpty(trackStatusDetail.getDate()) ? "font/Lato-Regular.ttf" : "font/Lato-Bold.ttf"));
        holder.binding.tvStatusLabel.setTextColor(ContextCompat.getColor(context, TextUtils.isEmpty(trackStatusDetail.getDate()) ? R.color.colorLightBlueGrey : R.color.colorDarkBlueGrey));
        holder.binding.tvLocation.setText(trackStatusDetail.getTrackId());
        holder.binding.tvLocation.setVisibility(!TextUtils.isEmpty(trackStatusDetail.getTrackId()) ? View.VISIBLE : View.GONE);
        if (!TextUtils.isEmpty(trackStatusDetail.getTrackUrl())) {
            holder.binding.tvLocation.setPaintFlags(holder.binding.tvLocation.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
        holder.binding.tvLocation.setTypeface(Typeface.createFromAsset(context.getAssets(), TextUtils.isEmpty(trackStatusDetail.getDate()) ? "font/Lato-Regular.ttf" : "font/Lato-Bold.ttf"));
        holder.binding.tvLocation.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        holder.binding.tvLocation.setOnClickListener(trackIdClickListener(trackStatusDetail.getTrackUrl()));
        holder.binding.viewLine.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);

    }

    private View.OnClickListener trackIdClickListener(final String trackUrl) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(trackUrl))
                    listener.onClickTrackStatus(trackUrl);
            }
        };
    }

    @Override
    public int getItemCount() {
        return trackStatusDetailsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final AdapterTrackOrderStatusItemBinding binding;

        ViewHolder(AdapterTrackOrderStatusItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OrderTrackStatusAdapterListener {
        void onClickTrackStatus(String url);
    }
}