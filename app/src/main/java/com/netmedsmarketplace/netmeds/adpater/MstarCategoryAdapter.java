package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterSearchBinding;
import com.netmedsmarketplace.netmeds.databinding.BrandFilterViewBinding;
import com.netmedsmarketplace.netmeds.viewmodel.CategoryViewModel;
import com.nms.netmeds.base.AppConstant;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.VisibleItemListener;
import com.nms.netmeds.base.model.MStarBasicResponseTemplateModel;
import com.nms.netmeds.base.model.MStarBreadCrumb;
import com.nms.netmeds.base.model.MStarCategory;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarFacetItemDetails;
import com.nms.netmeds.base.utils.BasePreference;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;
import com.nms.netmeds.base.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

public class MstarCategoryAdapter extends RecyclerView.Adapter implements FilterBrandAdapter.BrandFilterListener {
    private List<MStarProductDetails> productList;
    private List<MstarFacetItemDetails> brandFilterList;
    private Context context;
    private Integer categoryId;
    private MStarBasicResponseTemplateModel response;
    private String imageURLPath;
    private PopularProductAdapterListener adapterListener;
    private boolean do_animate = true;
    private static int TYPE_PRODUCT = 1;
    private static int TYPE_BRAND_FILTER = 2;
    private String title;

    public MstarCategoryAdapter(final Context context, List<MStarProductDetails> productList, List<MstarFacetItemDetails> brandFilterList, Integer categoryId, PopularProductAdapterListener adapterListener, String from, final String title) {
        this.productList = productList;
        this.context = context;
        this.categoryId = categoryId;
        this.adapterListener = adapterListener;
        this.brandFilterList = brandFilterList;
        this.title = title;
        if (AppConstant.CATEGORY.equalsIgnoreCase(from)) {
            setBrandFilter();
        }
        response = new Gson().fromJson(BasePreference.getInstance(context).getConfigURLPath(), MStarBasicResponseTemplateModel.class);
        imageURLPath = response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getProductImageUrlBasePath()) ? response.getResult().getProductImageUrlBasePath() + AppConstant.MSTAR_IMAGE_SIZE_120 + "/" : "";

        CategoryViewModel.setVisibleItemListener(new VisibleItemListener() {
            @Override
            public void viewVisiblePosition(int firstVisiblePosition, int lastVisiblePosition) {
                if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0 && lastVisiblePosition < getProductList().size()) {
                    List<MStarProductDetails> impressionList = getProductList().subList(firstVisiblePosition, lastVisiblePosition + 1);
                    /*Google Tag Manager + FireBase Product Impression Event*/
                    FireBaseAnalyticsHelper.getInstance().logFireBaseProductImpressionEvent(context, impressionList, firstVisiblePosition + 1, FireBaseAnalyticsHelper.VIEW_ON_PAGE, title);
                }
            }
        });
    }

    private List<MStarProductDetails> getProductList() {
        return productList;
    }

    private void setBrandFilter() {
        if (brandFilterList != null && productList.size() > 10) {
            MStarProductDetails product = new MStarProductDetails();
            product.setBrandList(brandFilterList);
            this.productList.add(10, product);
        }
    }

    public void updateList(List<MStarProductDetails> productList, boolean isClear) {
        if (isClear) {
            this.productList = new ArrayList<>();
            notifyDataSetChanged();
        }

        if (this.productList != null) {
            int size = this.productList.size();
            this.productList.addAll(productList);
            notifyItemRangeInserted(size, productList.size());
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        if (itemType == TYPE_PRODUCT) {
            AdapterSearchBinding adapterSearchBinding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_search, viewGroup, false);
            return new ProductViewHolder(adapterSearchBinding);
        } else {
            BrandFilterViewBinding brandBinding = DataBindingUtil.inflate(layoutInflater, R.layout.brand_filter_view, viewGroup, false);
            return new BrandFilterHolder(brandBinding);
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                do_animate = dy > 0;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        int TYPE = getItemViewType(position);
        if (TYPE == TYPE_BRAND_FILTER) {
            FilterBrandAdapter brandAdapter = new FilterBrandAdapter(context, productList.get(position).getBrandList().size() > 11 ? productList.get(position).getBrandList().subList(0, 11) : productList.get(position).getBrandList(), categoryId, this);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            ((BrandFilterHolder) viewHolder).brandBinding.brandFilter.setLayoutManager(linearLayoutManager);
            ((BrandFilterHolder) viewHolder).brandBinding.brandFilter.setAdapter(brandAdapter);
        } else {
            ((ProductViewHolder) viewHolder).bindViewData(position);
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public int getItemViewType(int position) {
        MStarProductDetails product = productList.get(position);
        if (product.getBrandList() != null && product.getBrandList().size() > 0) {
            return TYPE_BRAND_FILTER;
        } else {
            return TYPE_PRODUCT;
        }
    }


    private View.OnClickListener productDetailsListener(final int productCode, final MStarProductDetails productDetails, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterListener.openProductDetailsPage(productCode, productDetails, position + 1);
            }
        };
    }

    private boolean isRxRequired(MStarProductDetails productDetails) {
        return productDetails != null && productDetails.isRxRequired();
    }

    private View.OnClickListener manufactureNameClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterListener.openManufacturerProductList(productList.get(position).getManufacturer().getId(), productList.get(position).getManufacturer().getName(), productList.get(position).getProductType());
            }
        };
    }

    @Override
    public void applyBrandFilter(Integer brandId, String brandName) {
        adapterListener.applyBrandFilter(brandId, brandName);
    }

    public interface PopularProductAdapterListener {

        void OnAddCartCallBack(MStarProductDetails productDetails, int position);

        void openManufacturerProductList(Integer manufacturerId, String ManufacturerName, String productType);

        void openProductDetailsPage(int productCode, MStarProductDetails productDetails, int position);

        void applyBrandFilter(Integer brandId, String brandName);

        boolean isFromAlgolia();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        private AdapterSearchBinding adapterSearchBinding;

        ProductViewHolder(@NonNull AdapterSearchBinding binding) {
            super(binding.getRoot());
            this.adapterSearchBinding = binding;
        }

        private void bindViewData(final int position) {
            CommonUtils.setAnimation(adapterSearchBinding.alternateBrainProductView, do_animate, 100, context);
            final MStarProductDetails mStarProductDetails = productList.get(position);

            if (mStarProductDetails != null) {
                adapterSearchBinding.bannerList.setVisibility(View.GONE);
                adapterSearchBinding.alternateBrainProductView.setVisibility(View.VISIBLE);

                RequestOptions options = new RequestOptions()
                        .circleCrop()
                        .placeholder(R.drawable.ic_no_image)
                        .error(R.drawable.ic_no_image)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);

                String imagePath = mStarProductDetails.getImagePaths() != null ? mStarProductDetails.getImagePaths().get(0) : "";
                if (context != null && context.getApplicationContext() != null)
                    Glide.with(context.getApplicationContext()).load(ImageUtils.checkImage(getImageBasePath() + imagePath)).apply(options).into(adapterSearchBinding.alternateBrainProductImage);

                if (isRxRequired(mStarProductDetails)) {
                    adapterSearchBinding.alternateBrainProductImage.setVisibility(View.GONE);
                    adapterSearchBinding.alternateBrainRxImage.setVisibility(View.VISIBLE);
                } else {
                    adapterSearchBinding.alternateBrainProductImage.setVisibility(View.VISIBLE);
                    adapterSearchBinding.alternateBrainRxImage.setVisibility(View.GONE);
                }

                adapterSearchBinding.alternateBrainDrugName.setText(!TextUtils.isEmpty(mStarProductDetails.getDisplayName()) ? mStarProductDetails.getDisplayName() : "");
                if (mStarProductDetails.getMrp() != null && mStarProductDetails.getSellingPrice() != null) {
                    if (mStarProductDetails.getDiscount().doubleValue() == 0.0) {
                        adapterSearchBinding.searchAlternateBrainStrikePrice.setVisibility(View.GONE);
                        adapterSearchBinding.alternateBrainStrikePriceMrp.setVisibility(View.GONE);
                        adapterSearchBinding.alternateBrainAlgoliaPriceMrp.setVisibility(View.VISIBLE);
                    }
                } else {
                    adapterSearchBinding.searchAlternateBrainStrikePrice.setVisibility(View.VISIBLE);
                    adapterSearchBinding.alternateBrainAlgoliaPriceMrp.setVisibility(View.GONE);
                    adapterSearchBinding.alternateBrainStrikePriceMrp.setVisibility(View.VISIBLE);
                }
                adapterSearchBinding.searchAlternateBrainalgoliaPrice.setText(!TextUtils.isEmpty(mStarProductDetails.getSellingPrice().toString()) ? String.format("\u20B9 %.2f", mStarProductDetails.getSellingPrice()) : "\u20B9 0.00");
                adapterSearchBinding.searchAlternateBrainStrikePrice.setText(!TextUtils.isEmpty(mStarProductDetails.getMrp().toString()) ? String.format("\u20B9 %.2f", mStarProductDetails.getMrp()) : "");
                adapterSearchBinding.searchAlternateBrainStrikePrice.setPaintFlags(adapterSearchBinding.searchAlternateBrainStrikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                if (!TextUtils.isEmpty(mStarProductDetails.getAvailabilityStatus()) && mStarProductDetails.getAvailabilityStatus().equalsIgnoreCase("A")) {
                    adapterSearchBinding.alternateBrainAddCartButton.setVisibility(View.VISIBLE);
                    adapterSearchBinding.alternateBrainCartDivider.setVisibility(View.VISIBLE);
                } else {
                    adapterSearchBinding.alternateBrainAddCartButton.setVisibility(View.GONE);
                    adapterSearchBinding.alternateBrainCartDivider.setVisibility(View.GONE);
                }
                if (mStarProductDetails.getCategories() != null && mStarProductDetails.getCategories().size() > 0) {

                    if (TextUtils.isEmpty(mStarProductDetails.getCategories().get(0).getName()))
                        adapterSearchBinding.alternateBrainCategoryName.setVisibility(View.GONE);
                } else {
                    adapterSearchBinding.alternateBrainCategoryName.setVisibility(View.VISIBLE);
                }

                adapterSearchBinding.alternateBrainCategoryName.setText(getCategoryName(mStarProductDetails, AppConstant.CATEGORY_LEVEL_1));
                adapterSearchBinding.categoryName2.setText(getCategoryName(mStarProductDetails, AppConstant.CATEGORY_LEVEL_2));
                adapterSearchBinding.alternateBrainCategoryName.setVisibility(TextUtils.isEmpty(getCategoryName(mStarProductDetails, AppConstant.CATEGORY_LEVEL_1)) ? View.GONE : View.VISIBLE);
                adapterSearchBinding.categoryName2.setVisibility(TextUtils.isEmpty(getCategoryName(mStarProductDetails, AppConstant.CATEGORY_LEVEL_2)) ? View.GONE : View.VISIBLE);

                adapterSearchBinding.alternateBrainManufacturerName.setText(mStarProductDetails.getManufacturer() != null && !TextUtils.isEmpty(mStarProductDetails.getManufacturer().getName()) ? String.format("" + AppConstant.MFR + " %s", mStarProductDetails.getManufacturer().getName()) : "");
                adapterSearchBinding.alternateBrainManufacturerName.setVisibility(mStarProductDetails.getManufacturer() != null && !TextUtils.isEmpty(mStarProductDetails.getManufacturer().getName()) ? View.VISIBLE : View.GONE);
                adapterSearchBinding.alternateBrainDrugDetail.setText(TextUtils.isEmpty(mStarProductDetails.getPackLabel()) ? mStarProductDetails.getPackLabel() : "");
                adapterSearchBinding.alternateBrainDrugDetail.setVisibility(TextUtils.isEmpty(mStarProductDetails.getPackLabel()) ? View.GONE : View.VISIBLE);

                adapterSearchBinding.alternateBrainManufacturerName.setOnClickListener(manufactureNameClickListener(position));

                adapterSearchBinding.alternateBrainRxRequired.setVisibility(isRxRequired(mStarProductDetails) ? View.VISIBLE : View.GONE);
                adapterSearchBinding.alternateBrainAddCartButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        adapterListener.OnAddCartCallBack(mStarProductDetails, position + 1);
                    }
                });
                adapterSearchBinding.alternateBrainProductView.setOnClickListener(productDetailsListener(mStarProductDetails.getProductCode(), mStarProductDetails, position));
                adapterSearchBinding.searchDivider.setVisibility(position == productList.size() - 1 ? View.GONE : View.VISIBLE);
            }
        }
    }

    private String getImageBasePath() {
        return !adapterListener.isFromAlgolia() ? imageURLPath : response != null && response.getResult() != null && !TextUtils.isEmpty(response.getResult().getCatalogImageUrlBasePath()) ? response.getResult().getCatalogImageUrlBasePath() : "";
    }

    private String getCategoryName(MStarProductDetails productDetails, int level) {
        String categoryName = "";
        if (productDetails != null && productDetails.getCategories() != null && productDetails.getCategories().size() > 0) {
            MStarCategory mStarCategory = productDetails.getCategories().get(0);
            if (mStarCategory != null && mStarCategory.getBreadCrumbs() != null && mStarCategory.getBreadCrumbs().size() > 0) {
                List<MStarBreadCrumb> mStarBreadCrumbs = mStarCategory.getBreadCrumbs();
                for (MStarBreadCrumb breadCrumb : mStarBreadCrumbs) {
                    if (breadCrumb.getLevel() == level) {
                        return breadCrumb.getName();
                    }
                }
            }
        }
        return categoryName;
    }

    class BrandFilterHolder extends RecyclerView.ViewHolder {
        BrandFilterViewBinding brandBinding;

        BrandFilterHolder(@NonNull BrandFilterViewBinding binding) {
            super(binding.getRoot());
            this.brandBinding = binding;
        }
    }
}

