package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterWellnessBinding;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarCategoryDetails;

import java.util.List;

public class MstarWellnessAdapter extends RecyclerView.Adapter<MstarWellnessAdapter.ViewHolder> {

    private List<MStarProductDetails> productDetailsList;
    private Context context;
    private WellnessAdaterCallBack callBack;

    public MstarWellnessAdapter(List<MStarProductDetails> productDetailsList, WellnessAdaterCallBack callBack) {
        this.productDetailsList = productDetailsList;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        AdapterWellnessBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_wellness, parent, false);
        this.context = parent.getContext();
        return new MstarWellnessAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final MStarProductDetails productDetails = productDetailsList.get(i);
        Glide.with(context).load(productDetails.getProduct_image_path()).apply(new RequestOptions().fitCenter()).error(Glide.with(viewHolder.binding.image).load(R.drawable.ic_no_image)).into(viewHolder.binding.image);

        viewHolder.binding.tvName.setText(!TextUtils.isEmpty(productDetails.getName()) ? productDetails.getName() : "");

        viewHolder.binding.lvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.OnCategoryClickCallback(productDetails.getId(), productDetails.getLevel(), productDetails.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return productDetailsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AdapterWellnessBinding binding;

        public ViewHolder(@NonNull AdapterWellnessBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface WellnessAdaterCallBack {
        void OnCategoryClickCallback(int id, int level, String categorName);
    }
}
