package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterDealsOntopBrandsBinding;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.utils.FireBaseAnalyticsHelper;

import java.util.List;

public class DealsOnTopBrandsAdapter extends RecyclerView.Adapter<DealsOnTopBrandsAdapter.DealsViewHolder> {

    private List<MStarProductDetails> productDetailsList;
    private DealsAdapterListener listener;
    private String type;
    private Context context;

    public DealsOnTopBrandsAdapter(Context context,List<MStarProductDetails> productDetailsList,DealsAdapterListener listener,String type) {
        this.context=context;
        this.productDetailsList = productDetailsList;
        this.listener = listener;
        this.type = type;
    }

    @NonNull
    @Override
    public DealsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterDealsOntopBrandsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_deals_ontop_brands,parent,false);
        return new DealsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DealsViewHolder holder, int position) {
        final MStarProductDetails productDetails = productDetailsList.get(position);
        holder.bindValue(productDetails,position);
    }

    @Override
    public int getItemCount() {
        return productDetailsList.size();
    }

    class DealsViewHolder extends RecyclerView.ViewHolder{
        private AdapterDealsOntopBrandsBinding dealsOntopBrandsBinding;

        public DealsViewHolder(AdapterDealsOntopBrandsBinding dealsOntopBrandsBinding) {
            super(dealsOntopBrandsBinding.getRoot());
            this.dealsOntopBrandsBinding = dealsOntopBrandsBinding;
        }

        private void bindValue(final MStarProductDetails productDetails, final int position){
            Glide.with(dealsOntopBrandsBinding.getRoot().getContext()).load(productDetails.getProduct_image_path())
                    .error(Glide.with(dealsOntopBrandsBinding.dealsOnTopBrandsImage).load(R.drawable.ic_no_image))
                    .into(dealsOntopBrandsBinding.dealsOnTopBrandsImage);

            dealsOntopBrandsBinding.discountRate.setVisibility(!TextUtils.isEmpty(productDetails.getDiscountUpto()) ? View.VISIBLE:View.GONE);
            dealsOntopBrandsBinding.discountRate.setText(!TextUtils.isEmpty(productDetails.getDiscountUpto())? productDetails.getDiscountUpto():"");

            dealsOntopBrandsBinding.dealsOnTopBrandsImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Google Tag Manager + FireBase Promotion Click Event*/
                    FireBaseAnalyticsHelper.getInstance().logFireBasePromotionClickAndSelectionEvent(context,productDetails,FireBaseAnalyticsHelper.EVENT_PARAM_HOME_DEALS,position+1);
                    listener.onDealsBannerClick(productDetails,type);
                }
            });
        }

    }

   public interface DealsAdapterListener{
        void onDealsBannerClick(MStarProductDetails productDetails, String type);
    }
}
