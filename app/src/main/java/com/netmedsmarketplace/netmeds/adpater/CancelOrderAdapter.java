package com.netmedsmarketplace.netmeds.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterReviewOrderAgainBinding;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.CancelOrderReason;

import java.util.List;

public class CancelOrderAdapter extends RecyclerView.Adapter<CancelOrderAdapter.ReviewOrderAgainHolder> {

    private final List<CancelOrderReason> cancelOrderMessageList;
    private final CancelOrderAdapterListener listener;

    public CancelOrderAdapter(List<CancelOrderReason> cancelOrderMessageList, CancelOrderAdapterListener listener) {
        this.cancelOrderMessageList = cancelOrderMessageList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ReviewOrderAgainHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterReviewOrderAgainBinding listBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_review_order_again, viewGroup, false);
        return new ReviewOrderAgainHolder(listBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewOrderAgainHolder holder, int position) {
        CancelOrderReason messageModel = cancelOrderMessageList.get(position);
        holder.binding.flItemImage.setVisibility(View.GONE);
        holder.binding.tvDrugName.setText(messageModel.getValue());
        holder.binding.cbDrugSelected.setChecked(messageModel.isSelected());
        holder.binding.getRoot().setOnClickListener(itemClick(position));
        holder.binding.cbDrugSelected.setOnClickListener(itemClick(position));
        holder.binding.divider.setVisibility(position + 1 == getItemCount() ? View.GONE : View.VISIBLE);
    }

    private View.OnClickListener itemClick(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelOrderReason messageModel = cancelOrderMessageList.get(position);
                PaymentHelper.setCancel_reason(messageModel.getValue());
                listener.onSelectItems(position);
            }
        };
    }

    @Override
    public int getItemCount() {
        return cancelOrderMessageList.size();
    }

    public interface CancelOrderAdapterListener {
        void onSelectItems(int position);
    }

    class ReviewOrderAgainHolder extends RecyclerView.ViewHolder {

        private final AdapterReviewOrderAgainBinding binding;

        ReviewOrderAgainHolder(@NonNull AdapterReviewOrderAgainBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
