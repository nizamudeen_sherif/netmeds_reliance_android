package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterOrderItemListItemBinding;

import java.util.List;

public class PrescriptionItemListAdapter extends RecyclerView.Adapter<PrescriptionItemListAdapter.ItemViewHolder> {
    private Context mContext;
    private List<String> itemList;

    public PrescriptionItemListAdapter(Context context, List<String> itemList) {
        this.itemList = itemList;
        this.mContext = context;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterOrderItemListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext.getApplicationContext()), R.layout.adapter_order_item_list_item, viewGroup, false);
        return new PrescriptionItemListAdapter.ItemViewHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {
        String drugName = itemList.get(position);
        itemViewHolder.binding.tvItemName1.setText(drugName);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private AdapterOrderItemListItemBinding binding;

        ItemViewHolder(@NonNull AdapterOrderItemListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
