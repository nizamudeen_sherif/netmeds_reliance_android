package com.netmedsmarketplace.netmeds.adpater;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.netmedsmarketplace.netmeds.R;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.MStarProductDetails;
import com.nms.netmeds.base.model.MstarBanner;

import java.util.List;

public class MedicineBannerAdapter extends RecyclerView.Adapter<MedicineBannerAdapter.MedicineHolder> {
    private final List<MStarProductDetails> offerList;
    private final Context mContext;

    public MedicineBannerAdapter(Context mContext, List<MStarProductDetails> offerList) {
        this.offerList = offerList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MedicineBannerAdapter.MedicineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.medicine_adapter, parent, false);
        return new MedicineBannerAdapter.MedicineHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicineBannerAdapter.MedicineHolder holder, final int position) {
        final MStarProductDetails banner = offerList.get(position);
        final MstarBanner bannerDetails = new MstarBanner();
        bannerDetails.setImageName(banner.getImageName());
        bannerDetails.setImageUrl(banner.getProduct_image_path());
        bannerDetails.setUrl(banner.getUrl());
        bannerDetails.setLinktype(banner.getLinktype());
        bannerDetails.setDisplayFrom(banner.getDisplayFrom());
        bannerDetails.setDisplayTo(banner.getDisplayTo());
        if (mContext != null && banner.getImageName() != null) {
            Glide.with(mContext).load(!TextUtils.isEmpty(banner.getProduct_image_path()) ? banner.getProduct_image_path() : "").into(holder.imgOffer);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.wellnessNavigateToScreens(mContext, bannerDetails, (Activity) mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    class MedicineHolder extends RecyclerView.ViewHolder {
        private final ImageView imgOffer;

        MedicineHolder(View itemView) {
            super(itemView);
            imgOffer = itemView.findViewById(R.id.medicine_adapter_image);

        }
    }
}