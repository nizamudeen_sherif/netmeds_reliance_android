package com.netmedsmarketplace.netmeds.adpater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterLocaleBinding;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.ArrayList;

public class LocaleListAdapter extends RecyclerView.Adapter<LocaleListAdapter.LocaleViewHolder> {

    private final ArrayList<String> localeList;
    private final Context context;
    private final LocaleListener localeListener;
    private int currentPosition = 0;

    public LocaleListAdapter(Context context, ArrayList<String> localeList, LocaleListener localeListener) {
        this.context = context;
        this.localeList = localeList;
        this.localeListener = localeListener;
    }

    @NonNull
    @Override
    public LocaleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterLocaleBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_locale, parent, false);
        return new LocaleViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final LocaleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.localeBinding.translationText.setText(localeList.get(position));
        if (currentPosition == position) {
            textSelection(holder);
            localeListener.adCallBackOnLocaleSelection(BasePreference.PREF_KEY_LOCALE_ENGLISH);
        } else {
            unSelectionText(holder);
        }

        holder.localeBinding.localTextItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = position;
                notifyDataSetChanged();
                switch (position) {
                    case 0:
                        localeListener.adCallBackOnLocaleSelection(BasePreference.PREF_KEY_LOCALE_ENGLISH);
                        break;
                    case 1:
                        localeListener.adCallBackOnLocaleSelection(BasePreference.PREF_KEY_LOCALE_HINDI);
                        break;
                    case 2:
                        localeListener.adCallBackOnLocaleSelection(BasePreference.PREF_KEY_LOCALE_TAMIL);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return localeList.size();
    }

    private void textSelection(LocaleViewHolder holder) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Lato-Bold.ttf");
        holder.localeBinding.translationText.setTypeface(typeface);
        holder.localeBinding.tick.setVisibility(View.VISIBLE);
        holder.localeBinding.translationText.setTextColor(context.getResources().getColor(R.color.colorDarkBlueGrey));
    }

    private void unSelectionText(LocaleViewHolder holder) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Lato-Regular.ttf");
        holder.localeBinding.translationText.setTypeface(typeface);
        holder.localeBinding.tick.setVisibility(View.GONE);
        holder.localeBinding.translationText.setTextColor(context.getResources().getColor(R.color.colorLightBlueGrey));
    }

    public interface LocaleListener {
        void adCallBackOnLocaleSelection(String locale);
    }

    class LocaleViewHolder extends RecyclerView.ViewHolder {
        private final AdapterLocaleBinding localeBinding;

        LocaleViewHolder(final AdapterLocaleBinding itemBinding) {
            super(itemBinding.getRoot());
            this.localeBinding = itemBinding;
        }
    }
}