package com.netmedsmarketplace.netmeds.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterAddressBinding;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.PaymentHelper;
import com.nms.netmeds.base.model.MStarAddressModel;
import com.nms.netmeds.base.utils.BasePreference;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.M2AddressAdapterHolder> {

    private final List<MStarAddressModel> mAddressList;
    private final Context mContext;
    private final AddressListListener mAddressListListener;
    private boolean onPageLoad;
    private boolean fromDiagnostic;
    private BasePreference basePreference;

    public AddressAdapter(List<MStarAddressModel> addressList, Context context, AddressListListener addressListListener, boolean fromDiagnostic, BasePreference basePreference) {
        this.mAddressList = addressList;
        this.mContext = context;
        this.mAddressListListener = addressListListener;
        this.onPageLoad = true;
        this.basePreference = basePreference;
        this.fromDiagnostic = fromDiagnostic;
    }

    @NonNull
    @Override
    public M2AddressAdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterAddressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_address, viewGroup, false);
        return new M2AddressAdapterHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final M2AddressAdapterHolder holder, int i) {
        holder.onBindAddressData();
    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }

    class M2AddressAdapterHolder extends RecyclerView.ViewHolder {
        private final AdapterAddressBinding addressBinding;

        M2AddressAdapterHolder(AdapterAddressBinding addressBinding) {
            super(addressBinding.getRoot());
            this.addressBinding = addressBinding;
        }

        public void onBindAddressData() {
            final MStarAddressModel addressModel = mAddressList.get(getAdapterPosition());
            addressBinding.name.setText((!TextUtils.isEmpty(addressModel.getFirstname()) ? addressModel.getFirstname() : "") + " " + (!TextUtils.isEmpty(addressModel.getFirstname()) ? addressModel.getLastname() : ""));
            addressBinding.address.setText(CommonUtils.getAddressFromObject(addressModel));
            setAddressSelection(addressModel, addressBinding);
            addressBinding.mainLayout.setOnClickListener(onAddressEditOrDeleteOrSelectionListener(addressModel, true, false, false));
            addressBinding.edit.setOnClickListener(onAddressEditOrDeleteOrSelectionListener(addressModel, false, true, false));
            addressBinding.delete.setOnClickListener(onAddressEditOrDeleteOrSelectionListener(addressModel, false, false, true));

            if (fromDiagnostic && basePreference.getPreviousCartShippingAddressId() == addressModel.getId()) {
                mAddressListListener.diagnosticPinCodeCheck(addressModel);
            }
        }

        private View.OnClickListener onAddressEditOrDeleteOrSelectionListener(final MStarAddressModel addressModel, final boolean isAddressSelected, boolean isEditAddress, final boolean isDeleteAddress) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAddressSelected || mAddressList.size() == 0) {
                        resetAdapter(getAdapterPosition());
                        mAddressListListener.setDeliveryAddress(addressModel);
                        if (fromDiagnostic)
                            mAddressListListener.diagnosticPinCodeCheck(addressModel);
                    } else if (isDeleteAddress) {
                        mAddressListListener.deleteAddress(addressModel.getId());
                    } else {
                        mAddressListListener.editDeliveryAddress(addressModel);
                    }
                }
            };
        }
    }

    private void setAddressSelection(MStarAddressModel addressModel, AdapterAddressBinding addressBinding) {
        if (addressModel.getSelectedAddress()) {
            mAddressListListener.setDeliveryAddress(addressModel);
            PaymentHelper.setSingle_address(new Gson().toJson(addressModel));
            PaymentHelper.setCustomerBillingAddress(addressModel);

        }
        addressBinding.addressCheckbox.setImageDrawable(addressModel.getSelectedAddress() ? mContext.getResources().getDrawable(R.drawable.ic_radio_checked) : mContext.getResources().getDrawable(R.drawable.ic_radio_unchecked));
        addressBinding.mainLayout.setBackground(addressModel.getSelectedAddress() ? mContext.getResources().getDrawable(R.drawable.accent_curved_rectangle) : null);
        addressBinding.delete.setVisibility(addressModel.getSelectedAddress() ? View.GONE : View.VISIBLE);
    }

    private void resetAdapter(int adapterPosition) {
        onPageLoad = false;
        for (int i = 0; i < mAddressList.size(); i++) {
            MStarAddressModel customerAddress = mAddressList.get(i);
            customerAddress.setSelectedAddress(i == adapterPosition);
        }
        notifyDataSetChanged();
    }

    public interface AddressListListener {

        void deleteAddress(int id);

        void setDeliveryAddress(MStarAddressModel addressModel);

        void editDeliveryAddress(MStarAddressModel addressModel);

        void diagnosticPinCodeCheck(MStarAddressModel addressModel);
    }
}
