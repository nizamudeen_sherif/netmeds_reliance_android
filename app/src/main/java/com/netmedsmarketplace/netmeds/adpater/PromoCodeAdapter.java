package com.netmedsmarketplace.netmeds.adpater;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.netmedsmarketplace.netmeds.R;
import com.netmedsmarketplace.netmeds.databinding.AdapterPromoCodeBinding;
import com.netmedsmarketplace.netmeds.viewmodel.PromoCodeViewModel;
import com.nms.netmeds.base.CommonUtils;
import com.nms.netmeds.base.model.PromoCodeList;
import com.nms.netmeds.base.utils.GoogleAnalyticsHelper;

import java.util.List;

@SuppressWarnings("deprecation")
public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.PromoCodeHolder> {
    private final List<PromoCodeList> couponList;
    private final Activity context;
    private final SelectedCoupon selectedCoupon;
    private String promoCode;

    public PromoCodeAdapter(Activity context, List<PromoCodeList> couponList, String promoCode, SelectedCoupon selectedCoupon) {
        this.couponList = couponList;
        this.context = context;
        this.selectedCoupon = selectedCoupon;
        this.promoCode = promoCode;
    }

    @NonNull
    @Override
    public PromoCodeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterPromoCodeBinding appliedCouponItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_promo_code, parent, false);
        return new PromoCodeHolder(appliedCouponItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final PromoCodeHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PromoCodeList consultationCoupon = couponList.get(position);
        boolean isSelectedCoupon = couponList.get(position).getCouponCode() != null && couponList.get(position).getCouponCode().equalsIgnoreCase(promoCode);
        holder.fragmentAppliedCouponItemBinding.lytCoupon.setEnabled(!isSelectedCoupon);
        holder.fragmentAppliedCouponItemBinding.btnCouponApplied.setVisibility(isSelectedCoupon ? View.VISIBLE : View.GONE);
        holder.fragmentAppliedCouponItemBinding.imgChecked.setImageResource(isSelectedCoupon ? R.drawable.ic_radio_checked : R.drawable.ic_radio_unchecked);
        holder.fragmentAppliedCouponItemBinding.lytCoupon.setBackgroundDrawable(context.getResources().getDrawable(isSelectedCoupon ? R.drawable.list_selection_active : R.drawable.list_selection_inactive));
        holder.fragmentAppliedCouponItemBinding.txtCouponCode.setTypeface(CommonUtils.getTypeface(context, isSelectedCoupon ? "font/Lato-Bold.ttf" : "font/Lato-Regular.ttf"));

        PromoCodeViewModel appliedCouponViewModel = new PromoCodeViewModel(context.getApplication());
        appliedCouponViewModel.onDataAvailable(consultationCoupon);
        holder.fragmentAppliedCouponItemBinding.setViewModel(appliedCouponViewModel);

        holder.fragmentAppliedCouponItemBinding.lytCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promoCode = couponList.get(position).getCouponCode();
                notifyDataSetChanged();
                selectedCoupon.pdAppliedCouponCode(couponList.get(position));
                updateCouponAdapter(holder);
                /*Google Analytics Click Event*/
                if (consultationCoupon.getCouponId() != null)
                    GoogleAnalyticsHelper.getInstance().postEvent(context, GoogleAnalyticsHelper.EVENT_CATEGORY_NAME_PROMO_CODE, consultationCoupon.getCouponId().equals("1") ? GoogleAnalyticsHelper.EVENT_ACTION_NMS_SUPERCASH_APPLIED : GoogleAnalyticsHelper.EVENT_ACTION_COUPON_APPLIED, GoogleAnalyticsHelper.EVENT_LABEL_CART_PAGE);
            }
        });
    }

    private void updateCouponAdapter(PromoCodeHolder holder) {
        holder.fragmentAppliedCouponItemBinding.btnCouponApplied.setVisibility(View.VISIBLE);
        holder.fragmentAppliedCouponItemBinding.imgChecked.setImageResource(R.drawable.ic_radio_checked);
        holder.fragmentAppliedCouponItemBinding.lytCoupon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selection_active));
        holder.fragmentAppliedCouponItemBinding.txtCouponCode.setTypeface(CommonUtils.getTypeface(context, "font/Lato-Bold.ttf"));
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    public interface SelectedCoupon {
        void pdAppliedCouponCode(PromoCodeList promoCodeList);
    }

    class PromoCodeHolder extends RecyclerView.ViewHolder {
        private final AdapterPromoCodeBinding fragmentAppliedCouponItemBinding;

        PromoCodeHolder(AdapterPromoCodeBinding appliedCouponItemBinding) {
            super(appliedCouponItemBinding.getRoot());
            this.fragmentAppliedCouponItemBinding = appliedCouponItemBinding;
        }
    }

}